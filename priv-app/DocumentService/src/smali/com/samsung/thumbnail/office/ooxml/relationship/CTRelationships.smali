.class public Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationships;
.super Ljava/lang/Object;
.source "CTRelationships.java"


# instance fields
.field private Relationship:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationships;->Relationship:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addRelationship(Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;)V
    .locals 1
    .param p1, "relationship"    # Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationships;->Relationship:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    return-void
.end method

.method public getRelationshipList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationships;->Relationship:Ljava/util/List;

    return-object v0
.end method
