.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;
.source "FontHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler$ITextFontConsumer;
    }
.end annotation


# instance fields
.field private textConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler$ITextFontConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler$ITextFontConsumer;Ljava/lang/String;)V
    .locals 1
    .param p1, "textConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler$ITextFontConsumer;
    .param p2, "fontType"    # Ljava/lang/String;

    .prologue
    .line 18
    const/16 v0, 0x1f

    invoke-direct {p0, v0, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;-><init>(ILjava/lang/String;)V

    .line 19
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;->textConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler$ITextFontConsumer;

    .line 20
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 25
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementAttrOnlyHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 27
    const-string/jumbo v3, "typeface"

    invoke-virtual {p0, p3, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 28
    .local v2, "typeFace":Ljava/lang/String;
    const-string/jumbo v3, "pitchFamily"

    invoke-virtual {p0, p3, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 30
    .local v1, "pitchFamily":Ljava/lang/String;
    const-string/jumbo v3, "charset"

    invoke-virtual {p0, p3, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "charSetFamily":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;->textConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler$ITextFontConsumer;

    if-eqz v3, :cond_0

    .line 37
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;->textConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler$ITextFontConsumer;

    invoke-interface {v3, v2, v1, v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler$ITextFontConsumer;->consumeFont(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V

    .line 40
    :cond_0
    return-void
.end method
