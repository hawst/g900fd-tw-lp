.class public Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;
.super Ljava/lang/Object;
.source "PatternFill.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/excel/IFill;


# instance fields
.field private bgColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

.field private fgColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBGColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;->bgColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    return-object v0
.end method

.method public getBGColor(Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)Ljava/lang/String;
    .locals 2
    .param p1, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    .line 48
    const/4 v0, 0x0

    .line 49
    .local v0, "color":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;->bgColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    if-eqz v1, :cond_0

    .line 52
    :cond_0
    return-object v0
.end method

.method public getFGColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;->fgColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    return-object v0
.end method

.method public getFGColor(Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)Ljava/lang/String;
    .locals 2
    .param p1, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    .line 40
    const/4 v0, 0x0

    .line 41
    .local v0, "color":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;->fgColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    if-eqz v1, :cond_0

    .line 44
    :cond_0
    return-object v0
.end method

.method public setBGColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V
    .locals 0
    .param p1, "bgColor"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;->bgColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .line 27
    return-void
.end method

.method public setFGColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V
    .locals 0
    .param p1, "fgColor"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;->fgColor:Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .line 19
    return-void
.end method
