.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XTblRowHeightHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler$IRowHeightObserver;
    }
.end annotation


# instance fields
.field private observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler$IRowHeightObserver;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler$IRowHeightObserver;)V
    .locals 1
    .param p1, "observer"    # Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler$IRowHeightObserver;

    .prologue
    .line 17
    const-string/jumbo v0, "trHeight"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler$IRowHeightObserver;

    .line 19
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 24
    const-string/jumbo v2, "val"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v1

    .line 25
    .local v1, "val":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler$IRowHeightObserver;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler$IRowHeightObserver;->setRowHeight(I)V

    .line 26
    const-string/jumbo v2, "hRule"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v0

    .line 27
    .local v0, "hRule":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 28
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler;->observer:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler$IRowHeightObserver;

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XTblRowHeightHandler$IRowHeightObserver;->setHeightRule(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties$HRule;)V

    .line 30
    :cond_0
    return-void
.end method
