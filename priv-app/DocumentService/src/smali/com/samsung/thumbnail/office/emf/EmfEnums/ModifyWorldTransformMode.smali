.class public final enum Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;
.super Ljava/lang/Enum;
.source "ModifyWorldTransformMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

.field public static final enum MWT_IDENTITY:Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

.field public static final enum MWT_LEFTMULTIPLY:Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

.field public static final enum MWT_RIGHTMULTIPLY:Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

.field public static final enum MWT_SET:Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;


# instance fields
.field private ModifyWorldTransformModeType:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 5
    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    const-string/jumbo v1, "MWT_IDENTITY"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->MWT_IDENTITY:Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    const-string/jumbo v1, "MWT_LEFTMULTIPLY"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->MWT_LEFTMULTIPLY:Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    const-string/jumbo v1, "MWT_RIGHTMULTIPLY"

    invoke-direct {v0, v1, v3, v4}, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->MWT_RIGHTMULTIPLY:Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    new-instance v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    const-string/jumbo v1, "MWT_SET"

    invoke-direct {v0, v1, v4, v6}, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->MWT_SET:Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    .line 3
    new-array v0, v6, [Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->MWT_IDENTITY:Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->MWT_LEFTMULTIPLY:Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->MWT_RIGHTMULTIPLY:Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->MWT_SET:Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->$VALUES:[Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10
    iput p3, p0, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->ModifyWorldTransformModeType:I

    .line 11
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->$VALUES:[Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;

    return-object v0
.end method


# virtual methods
.method public getModifyWorldTransformModeType()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfEnums/ModifyWorldTransformMode;->ModifyWorldTransformModeType:I

    return v0
.end method
