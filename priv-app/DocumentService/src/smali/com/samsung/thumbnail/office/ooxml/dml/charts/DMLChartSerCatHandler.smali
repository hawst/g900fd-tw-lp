.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerCatHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "DMLChartSerCatHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;


# instance fields
.field private seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    const/16 v0, 0x23

    const-string/jumbo v1, "cat"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 29
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    .line 32
    const/4 v5, 0x2

    new-array v2, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 34
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrRefHandler;

    invoke-direct {v3, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrRefHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;)V

    .line 35
    .local v3, "strrefHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartStrRefHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v5, "strRef"

    invoke-direct {v4, v5, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 37
    .local v4, "strrefSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v5, 0x0

    aput-object v4, v2, v5

    .line 39
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumRefHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumRefHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartPtVHandler$IPtVConsumer;)V

    .line 40
    .local v0, "numrefHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartNumRefHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v5, "numRef"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 42
    .local v1, "numrefSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v5, 0x1

    aput-object v1, v2, v5

    .line 44
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerCatHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 45
    return-void
.end method


# virtual methods
.method public chartNumVal(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "val"    # Ljava/lang/String;

    .prologue
    .line 70
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addChartCatNumVal(Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public chartStrVal(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "str"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->addChartCatStrVal(Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public setFormatCode(Ljava/lang/String;)V
    .locals 1
    .param p1, "formatCode"    # Ljava/lang/String;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerCatHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->setformatCode(Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public setPtVal(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "strVal"    # Ljava/lang/String;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerCatHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->addPtVal(Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method public setRefType(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;I)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "refType"    # I

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerCatHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;->setRefType(I)V

    .line 87
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 50
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 51
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerCatHandler;->init()V

    .line 54
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v0, :cond_0

    .line 55
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerCatHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    .line 56
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->getCurSeries()Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    move-result-object v0

    const-string/jumbo v1, "cat"

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerCatHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->putSeriesVal(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;)V

    .line 61
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-eqz v0, :cond_1

    .line 62
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerCatHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    .line 63
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getCurSeries()Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;

    move-result-object v0

    const-string/jumbo v1, "cat"

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerCatHandler;->seriesVal:Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries;->putSeriesVal(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/chart/ChartSeries$Series;)V

    .line 66
    :cond_1
    return-void
.end method
