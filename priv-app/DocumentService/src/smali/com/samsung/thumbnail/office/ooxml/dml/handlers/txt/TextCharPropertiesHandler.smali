.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "TextCharPropertiesHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler$IHlinkCharPropConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler$ITextFontConsumer;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow$IEffectConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler$ITxtCharPropConsumer;
    }
.end annotation


# instance fields
.field private charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

.field private consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler$ITxtCharPropConsumer;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler$ITxtCharPropConsumer;)V
    .locals 22
    .param p1, "ele"    # Ljava/lang/String;
    .param p2, "shInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "consumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler$ITxtCharPropConsumer;

    .prologue
    .line 40
    const/16 v20, 0x1f

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 41
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler$ITxtCharPropConsumer;

    .line 43
    const/16 v20, 0x8

    move/from16 v0, v20

    new-array v11, v0, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 46
    .local v11, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;

    const-string/jumbo v20, "latin"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v5, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler$ITextFontConsumer;Ljava/lang/String;)V

    .line 47
    .local v5, "fontHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v20, "latin"

    move-object/from16 v0, v20

    invoke-direct {v6, v0, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 49
    .local v6, "fontLatinSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v20, 0x0

    aput-object v6, v11, v20

    .line 52
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;

    const-string/jumbo v20, "sym"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler$ITextFontConsumer;Ljava/lang/String;)V

    .line 53
    .local v18, "symFontHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FontHandler;
    new-instance v19, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v20, "sym"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 55
    .local v19, "symSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v20, 0x1

    aput-object v19, v11, v20

    .line 57
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v12, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 59
    .local v12, "solidFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrSolidFillHandler;
    new-instance v14, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v20, "solidFill"

    move-object/from16 v0, v20

    invoke-direct {v14, v0, v12}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 61
    .local v14, "solidFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v20, 0x2

    aput-object v14, v11, v20

    .line 63
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;

    const/16 v20, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 64
    .local v16, "spprNoFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/SpPrNoFillHandler;
    new-instance v17, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v20, "noFill"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 66
    .local v17, "spprNoFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v20, 0x3

    aput-object v17, v11, v20

    .line 68
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler$IHlinkCharPropConsumer;)V

    .line 69
    .local v9, "hLinkHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/HyperlinkHandler;
    new-instance v10, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v20, "hlinkClick"

    move-object/from16 v0, v20

    invoke-direct {v10, v0, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 71
    .local v10, "hLinkSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v20, 0x4

    aput-object v10, v11, v20

    .line 73
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/EffectLstHandler;

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v3, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/EffectLstHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow$IEffectConsumer;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;)V

    .line 74
    .local v3, "effctLstHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/EffectLstHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v20, "effectLst"

    move-object/from16 v0, v20

    invoke-direct {v4, v0, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 76
    .local v4, "effctLstSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v20, 0x5

    aput-object v4, v11, v20

    .line 78
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v20

    invoke-direct {v7, v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GsHandler$GsConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 80
    .local v7, "gradFillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/GradFillHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v20, "gradFill"

    move-object/from16 v0, v20

    invoke-direct {v8, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 82
    .local v8, "gradFillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v20, 0x6

    aput-object v8, v11, v20

    .line 84
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;)V

    .line 86
    .local v13, "solidFillHandlerDgm":Lcom/samsung/thumbnail/office/ooxml/diagram/DiagramSpprSolidFillHandler;
    new-instance v15, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v20, "solidFill"

    move-object/from16 v0, v20

    invoke-direct {v15, v0, v13}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 88
    .local v15, "solidFillSeqDgm":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/16 v20, 0x7

    aput-object v15, v11, v20

    .line 113
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 114
    return-void
.end method


# virtual methods
.method public consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 3
    .param p1, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 215
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    if-nez v1, :cond_0

    .line 234
    :goto_0
    return-void

    .line 218
    :cond_0
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 219
    .local v0, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 221
    iget-boolean v1, p1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->isSchemeClr:Z

    if-eqz v1, :cond_1

    .line 222
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setSchemeClr(Z)V

    .line 225
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 226
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getLumMod()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setLumMod(Ljava/lang/String;)V

    .line 229
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getTint()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 230
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getTint()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setTint(Ljava/lang/String;)V

    .line 233
    :cond_3
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    goto :goto_0
.end method

.method public consumeFont(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "typeFace"    # Ljava/lang/String;
    .param p2, "fontFamily"    # Ljava/lang/String;
    .param p3, "charSetFamily"    # Ljava/lang/String;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 202
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontName(Ljava/lang/String;)V

    .line 211
    :cond_0
    return-void
.end method

.method public consumeGS(Ljava/lang/String;FLjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 2
    .param p1, "element"    # Ljava/lang/String;
    .param p2, "pos"    # F
    .param p3, "val"    # Ljava/lang/String;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 260
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 261
    .local v0, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual {v0, p3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 262
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 263
    return-void
.end method

.method public effectConsumer(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "element"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .param p3, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 238
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setShadowColor(Ljava/lang/String;)V

    .line 242
    :cond_0
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 193
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler$ITxtCharPropConsumer;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->consumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler$ITxtCharPropConsumer;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler$ITxtCharPropConsumer;->consumeCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 197
    :cond_0
    return-void
.end method

.method public setLinkId(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 246
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setHLink(Ljava/lang/String;)V

    .line 250
    :cond_0
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 12
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v11, 0x1

    .line 124
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 125
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-direct {v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;-><init>()V

    iput-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 128
    const-string/jumbo v9, "dirty"

    invoke-virtual {p0, p3, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 130
    .local v8, "val":Ljava/lang/String;
    const-string/jumbo v9, "b"

    invoke-virtual {p0, p3, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 131
    if-eqz v8, :cond_0

    .line 132
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;

    invoke-direct {v1, v8}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;-><init>(Ljava/lang/String;)V

    .line 133
    .local v1, "boldValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->getValue()Z

    move-result v10

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setBold(Z)V

    .line 136
    .end local v1    # "boldValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    :cond_0
    const-string/jumbo v9, "i"

    invoke-virtual {p0, p3, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 137
    if-eqz v8, :cond_1

    .line 138
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;

    invoke-direct {v2, v8}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;-><init>(Ljava/lang/String;)V

    .line 139
    .local v2, "italicValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;->getValue()Z

    move-result v10

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setItalic(Z)V

    .line 142
    .end local v2    # "italicValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLBooleanValue;
    :cond_1
    const-string/jumbo v9, "sz"

    invoke-virtual {p0, p3, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 143
    if-eqz v8, :cond_2

    .line 144
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 145
    .local v3, "size":I
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    div-int/lit8 v10, v3, 0x64

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontSize(I)V

    .line 148
    .end local v3    # "size":I
    :cond_2
    const-string/jumbo v9, "u"

    invoke-virtual {p0, p3, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 149
    if-eqz v8, :cond_3

    .line 150
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    invoke-direct {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;-><init>(Ljava/lang/String;)V

    .line 151
    .local v7, "underlineValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "none"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 152
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v9, v11}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setUndreline(Z)V

    .line 156
    .end local v7    # "underlineValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    :cond_3
    const-string/jumbo v9, "baseline"

    invoke-virtual {p0, p3, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 157
    if-eqz v8, :cond_4

    .line 158
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    div-int/lit16 v0, v9, 0x3e8

    .line 159
    .local v0, "baseline":I
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setBaseLine(I)V

    .line 162
    .end local v0    # "baseline":I
    :cond_4
    const-string/jumbo v9, "strike"

    invoke-virtual {p0, p3, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 163
    if-eqz v8, :cond_6

    .line 164
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    invoke-direct {v6, v8}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;-><init>(Ljava/lang/String;)V

    .line 165
    .local v6, "strikeValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "sngStrike"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 166
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v9, v11}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setStrike(Z)V

    .line 168
    :cond_5
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "dblStrike"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 169
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v9, v11}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setDStrike(Z)V

    .line 173
    .end local v6    # "strikeValue":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    :cond_6
    const-string/jumbo v9, "cap"

    invoke-virtual {p0, p3, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 174
    if-eqz v8, :cond_8

    .line 175
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    invoke-direct {v4, v8}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;-><init>(Ljava/lang/String;)V

    .line 176
    .local v4, "smallCaps":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "small"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 177
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v9, v11}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setSmallCaps(Z)V

    .line 179
    :cond_7
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "all"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 180
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v9, v11}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setCaps(Z)V

    .line 184
    .end local v4    # "smallCaps":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    :cond_8
    const-string/jumbo v9, "spc"

    invoke-virtual {p0, p3, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 185
    if-eqz v8, :cond_9

    .line 186
    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    .line 187
    .local v5, "spacing":F
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextCharPropertiesHandler;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    const/high16 v10, 0x42c80000    # 100.0f

    div-float v10, v5, v10

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setSpacing(F)V

    .line 189
    .end local v5    # "spacing":F
    :cond_9
    return-void
.end method
