.class public Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "ChartElementHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler$ChartGroupingHandler;
    }
.end annotation


# instance fields
.field private chart:Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

.field private xssfChart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "nameSpaceId"    # I
    .param p2, "elementName"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;)Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->chart:Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;)Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->xssfChart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    return-object v0
.end method

.method private init()V
    .locals 6

    .prologue
    .line 31
    const/4 v5, 0x2

    new-array v2, v5, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 33
    .local v2, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;-><init>()V

    .line 34
    .local v3, "serHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/DMLChartSerHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v5, "ser"

    invoke-direct {v4, v5, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 36
    .local v4, "serSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v5, 0x0

    aput-object v4, v2, v5

    .line 38
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler$ChartGroupingHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler$ChartGroupingHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;)V

    .line 39
    .local v0, "groupingHandler":Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler$ChartGroupingHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;

    const-string/jumbo v5, "grouping"

    invoke-direct {v1, v5, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 41
    .local v1, "gruSeq":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartSeqDescriptor;
    const/4 v5, 0x1

    aput-object v1, v2, v5

    .line 43
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 44
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 64
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 65
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 49
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 50
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->init()V

    .line 52
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->chart:Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    .line 56
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-eqz v0, :cond_1

    .line 57
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/charts/ChartElementHandler;->xssfChart:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .line 59
    :cond_1
    return-void
.end method
