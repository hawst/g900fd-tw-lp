.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "GraphicHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0x1f

    .line 15
    const-string/jumbo v3, "graphic"

    invoke-direct {p0, v4, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 17
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 19
    .local v2, "seqDescriptors":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicDataHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicDataHandler;-><init>()V

    .line 20
    .local v1, "graphicDataHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicDataHandler;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    const-string/jumbo v3, "graphicData"

    invoke-direct {v0, v4, v3, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 23
    .local v0, "graphicData":Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 25
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 26
    return-void
.end method
