.class public Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBackgrndHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XPPTSldBackgrndHandler.java"


# instance fields
.field private background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    const-string/jumbo v0, "bg"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 16
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBackgrndHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->setBackground(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;)V

    .line 41
    return-void
.end method

.method protected init()V
    .locals 4

    .prologue
    .line 19
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBackgrndHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;)V

    .line 21
    .local v0, "sldBckFillHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillPropHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBackgrndHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "bgPr"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillRefHandler;

    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBackgrndHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillRefHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;)V

    .line 25
    .local v1, "sldBckRefHandler":Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBckFillRefHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBackgrndHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "bgRef"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 32
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBackgrndHandler;->background:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFBackground;

    .line 34
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/XPPTSldBackgrndHandler;->init()V

    .line 35
    return-void
.end method
