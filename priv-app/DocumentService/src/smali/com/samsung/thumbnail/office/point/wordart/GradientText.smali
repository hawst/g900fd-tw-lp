.class public Lcom/samsung/thumbnail/office/point/wordart/GradientText;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "GradientText.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private assets:Landroid/content/res/AssetManager;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/io/File;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "folderPath"    # Ljava/io/File;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 33
    const-string/jumbo v0, "GradientText"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->TAG:Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->mDrawOnCanvas:Z

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->assets:Landroid/content/res/AssetManager;

    .line 42
    iput-object p2, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->folderPath:Ljava/io/File;

    .line 43
    return-void
.end method


# virtual methods
.method protected applyGradientTextEffect(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .prologue
    const v6, -0xff0100

    const/high16 v5, -0x1000000

    const/high16 v2, 0x41a00000    # 20.0f

    const/4 v1, 0x0

    .line 104
    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->assets:Landroid/content/res/AssetManager;

    const-string/jumbo v4, "impact.ttf"

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v9

    .line 106
    .local v9, "tf":Landroid/graphics/Typeface;
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    .line 108
    .local v8, "paint":Landroid/graphics/Paint;
    const/4 v3, 0x1

    invoke-virtual {v8, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 109
    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 110
    invoke-virtual {v8, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 112
    iget v3, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->adjval0:I

    if-nez v3, :cond_0

    .line 113
    const/16 v3, 0x2a30

    iput v3, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->adjval0:I

    .line 125
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    move-result-object v3

    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->GRADIENT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 127
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v3, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->width:F

    sget-object v7, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v4, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    .line 129
    .local v0, "linearGradientShader":Landroid/graphics/Shader;
    invoke-virtual {v8, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 142
    .end local v0    # "linearGradientShader":Landroid/graphics/Shader;
    :cond_1
    :goto_0
    return-void

    .line 131
    :cond_2
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    move-result-object v3

    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->GRADIENT_RADIAL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 134
    new-instance v1, Landroid/graphics/RadialGradient;

    sget-object v7, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    move v3, v2

    move v4, v2

    invoke-direct/range {v1 .. v7}, Landroid/graphics/RadialGradient;-><init>(FFFIILandroid/graphics/Shader$TileMode;)V

    .line 136
    .local v1, "radialGradientShader":Landroid/graphics/Shader;
    invoke-virtual {v8, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto :goto_0
.end method

.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    const/4 v1, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    .line 80
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->mDrawOnCanvas:Z

    if-nez v0, :cond_0

    .line 81
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 83
    .local v5, "mtx":Landroid/graphics/Matrix;
    iget v0, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->rotation:I

    int-to-float v0, v0

    iget v2, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->width:F

    div-float/2addr v2, v4

    iget v3, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->height:F

    div-float/2addr v3, v4

    invoke-virtual {v5, v0, v2, v3}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 85
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->bitmap:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->bitmapWidth:I

    iget v4, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->bitmapHight:I

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 89
    .local v9, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v8, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->folderPath:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pic_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->shapeName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v8, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 93
    .local v8, "out":Ljava/io/FileOutputStream;
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x64

    invoke-virtual {v9, v0, v1, v8}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 94
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    .end local v5    # "mtx":Landroid/graphics/Matrix;
    .end local v8    # "out":Ljava/io/FileOutputStream;
    .end local v9    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-void

    .line 95
    .restart local v5    # "mtx":Landroid/graphics/Matrix;
    .restart local v9    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v7

    .line 96
    .local v7, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Exception while writing pictures to out file"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 47
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 48
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->draw(I)V

    .line 49
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;I)V
    .locals 1
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 52
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 53
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->draw(I)V

    .line 54
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIII)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # I
    .param p7, "height"    # I

    .prologue
    .line 58
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->mDrawOnCanvas:Z

    .line 60
    iput p4, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->mLeftMarg:I

    .line 61
    iput p5, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->mTopMarg:I

    .line 62
    int-to-float v0, p6

    iput v0, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->width:F

    .line 63
    int-to-float v0, p7

    iput v0, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->height:F

    .line 64
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 65
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;->GRADIENT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setFillType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EFillType;)V

    .line 66
    invoke-virtual {p0, p3, p1}, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->applyGradientTextEffect(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    .line 68
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->draw(Landroid/graphics/Canvas;I)V

    .line 69
    iget v0, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/wordart/GradientText;->mDrawOnCanvas:Z

    .line 71
    return-void
.end method
