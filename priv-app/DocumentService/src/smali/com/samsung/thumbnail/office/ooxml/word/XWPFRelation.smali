.class public final Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;
.super Lorg/apache/poi/POIXMLRelation;
.source "XWPFRelation.java"


# static fields
.field public static final CHART:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final COMMENT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final DATA_MODEL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final DOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final ENDNOTE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final FOOTER:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final FOOTNOTE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final HEADER:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final HYPERLINK:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final IMAGES:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final IMAGE_DIB:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final IMAGE_EMF:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final IMAGE_GIF:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final IMAGE_JPEG:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final IMAGE_PICT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final IMAGE_PNG:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final IMAGE_WMF:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final MACRO_DOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final MACRO_TEMPLATE_DOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final NUMBERING:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final SETTINGS:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final SMART_ART:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final STYLES:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final TEMPLATE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field public static final THEME:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

.field protected static _table:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->_table:Ljava/util/Map;

    .line 42
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    const-string/jumbo v3, "/word/document.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->DOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 46
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.template.main+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    const-string/jumbo v3, "/word/document.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->TEMPLATE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 50
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "application/vnd.ms-word.document.macroEnabled.main+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    const-string/jumbo v3, "/word/document.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->MACRO_DOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 54
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "application/vnd.ms-word.template.macroEnabledTemplate.main+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    const-string/jumbo v3, "/word/document.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->MACRO_TEMPLATE_DOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 58
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"

    const-string/jumbo v3, "/word/styles.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->STYLES:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 64
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering"

    const-string/jumbo v3, "/word/numbering.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->NUMBERING:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 69
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings"

    const-string/jumbo v3, "/word/settings.xml"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->SETTINGS:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 93
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.drawingml.drawing+xml"

    const-string/jumbo v2, "http://schemas.microsoft.com/office/2007/relationships/diagramDrawing"

    const-string/jumbo v3, "/diagrams/drawing#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->SMART_ART:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 98
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.drawingml.dataModel+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/diagramData"

    const-string/jumbo v3, "/diagrams/data#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->DATA_MODEL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 103
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.theme+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"

    const-string/jumbo v3, "/theme/theme#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->THEME:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 108
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.drawingml.chart+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/chart"

    const-string/jumbo v3, "/charts/chart#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->CHART:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 113
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.header+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/header"

    const-string/jumbo v3, "/word/header#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->HEADER:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 117
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.footer+xml"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/footer"

    const-string/jumbo v3, "/word/footer#.xml"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->FOOTER:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 121
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink"

    invoke-direct {v0, v5, v1, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->HYPERLINK:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 125
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/comments"

    invoke-direct {v0, v5, v1, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->COMMENT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 129
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/footnotes"

    invoke-direct {v0, v5, v1, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->FOOTNOTE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 133
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/endnotes"

    invoke-direct {v0, v5, v1, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->ENDNOTE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 138
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "image/x-emf"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/word/media/image#.emf"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGE_EMF:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 142
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "image/x-wmf"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/word/media/image#.wmf"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGE_WMF:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 146
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "image/pict"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/word/media/image#.pict"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGE_PICT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 150
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "image/jpeg"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/word/media/image#.jpeg"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGE_JPEG:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 154
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "image/png"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/word/media/image#.png"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGE_PNG:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 158
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "image/dib"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/word/media/image#.dib"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGE_DIB:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 162
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "image/gif"

    const-string/jumbo v2, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    const-string/jumbo v3, "/word/media/image#.gif"

    const-class v4, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGE_GIF:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    .line 166
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    const-string/jumbo v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    invoke-direct {v0, v5, v1, v5, v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->IMAGES:Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "rel"    # Ljava/lang/String;
    .param p3, "defaultName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/poi/POIXMLDocumentPart;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 173
    .local p4, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/poi/POIXMLDocumentPart;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/poi/POIXMLRelation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    .line 175
    if-eqz p4, :cond_0

    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->_table:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->_table:Ljava/util/Map;

    invoke-interface {v0, p2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    :cond_0
    return-void
.end method

.method public static getInstance(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;
    .locals 1
    .param p0, "rel"    # Ljava/lang/String;

    .prologue
    .line 188
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;->_table:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRelation;

    return-object v0
.end method
