.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLStrictSequenceHandler;
.source "TblCellBordersHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler$ICellBrdrConsumer;
    }
.end annotation


# instance fields
.field private cellBrdrConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler$ICellBrdrConsumer;

.field private tcBrdr:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler$ICellBrdrConsumer;)V
    .locals 2
    .param p1, "cellBrdrConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler$ICellBrdrConsumer;

    .prologue
    .line 28
    const/16 v0, 0x1f

    const-string/jumbo v1, "tcBdr"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStrictSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 29
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler;->cellBrdrConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler$ICellBrdrConsumer;

    .line 30
    return-void
.end method


# virtual methods
.method public borderCosumer(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V
    .locals 1
    .param p1, "element"    # Ljava/lang/String;
    .param p2, "lineProps"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler;->tcBrdr:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;->addBrdrLineProps(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;)V

    .line 80
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 2
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler;->cellBrdrConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler$ICellBrdrConsumer;

    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler;->tcBrdr:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    invoke-interface {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler$ICellBrdrConsumer;->handleBorder(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;)V

    .line 73
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStrictSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method protected init()V
    .locals 10

    .prologue
    .line 33
    const/4 v9, 0x4

    new-array v8, v9, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 35
    .local v8, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;

    const-string/jumbo v9, "left"

    invoke-direct {v2, v9, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;)V

    .line 37
    .local v2, "lnLHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v9, "left"

    invoke-direct {v3, v9, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 39
    .local v3, "lnLSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v9, 0x0

    aput-object v3, v8, v9

    .line 41
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;

    const-string/jumbo v9, "right"

    invoke-direct {v4, v9, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;)V

    .line 43
    .local v4, "lnRHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v9, "right"

    invoke-direct {v5, v9, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 45
    .local v5, "lnRSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v9, 0x1

    aput-object v5, v8, v9

    .line 47
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;

    const-string/jumbo v9, "top"

    invoke-direct {v6, v9, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;)V

    .line 49
    .local v6, "lnTHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v9, "top"

    invoke-direct {v7, v9, v6}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 51
    .local v7, "lnTSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v9, 0x2

    aput-object v7, v8, v9

    .line 53
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;

    const-string/jumbo v9, "bottom"

    invoke-direct {v0, v9, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler$IBorderConsumer;)V

    .line 55
    .local v0, "lnBLHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblStylesLineHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v9, "bottom"

    invoke-direct {v1, v9, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 57
    .local v1, "lnBSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v9, 0x3

    aput-object v1, v8, v9

    .line 59
    iput-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 60
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 65
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStrictSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 66
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler;->tcBrdr:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcBorder;

    .line 67
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler;->init()V

    .line 69
    return-void
.end method
