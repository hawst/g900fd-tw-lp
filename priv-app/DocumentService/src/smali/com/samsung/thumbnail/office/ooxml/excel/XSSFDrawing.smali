.class public final Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
.super Lorg/apache/poi/POIXMLDocumentPart;
.source "XSSFDrawing.java"


# static fields
.field protected static final NAMESPACE_A:Ljava/lang/String; = "http://schemas.openxmlformats.org/drawingml/2006/main"

.field protected static final NAMESPACE_C:Ljava/lang/String; = "http://schemas.openxmlformats.org/drawingml/2006/chart"

.field private static final TAG:Ljava/lang/String; = "XSSFDrawing"


# instance fields
.field private actualFileName:Ljava/io/File;

.field private bubblesize:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private cellAnchorLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;",
            ">;"
        }
    .end annotation
.end field

.field private chartCatNum:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private chartCatStr:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private chartName:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private chartRelId:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private chartTxNum:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private chartTxStr:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private chartXVal:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private chartXstrVal:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private chartYVal:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public chartcnt:I

.field private chartnumCount:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

.field private chartstrCount:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private colorList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected colorSchema:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private colormap:Ljava/lang/String;

.field private colorstyle:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private currentCellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

.field private exPicFileName:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private excelVar:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

.field private folderName:Ljava/lang/String;

.field public gradcolorLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;",
            ">;"
        }
    .end annotation
.end field

.field public gradcolorsize:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public gradientPos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public gradpossize:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public imgcount:I

.field private linefill:Ljava/lang/String;

.field private lumModList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mChartList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;",
            ">;"
        }
    .end annotation
.end field

.field private mTheme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

.field private orderValList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected pictData:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

.field protected picturesData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;",
            ">;"
        }
    .end annotation
.end field

.field private shapeslinefill:Z

.field private shapessolidfill:Z

.field private solidfill:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 124
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->exPicFileName:Ljava/util/ArrayList;

    .line 64
    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->imgcount:I

    .line 65
    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartcnt:I

    .line 69
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->colormap:Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartName:Ljava/util/ArrayList;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartstrCount:Ljava/util/ArrayList;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartnumCount:Ljava/util/ArrayList;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartRelId:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartCatStr:Ljava/util/ArrayList;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartCatNum:Ljava/util/ArrayList;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartTxStr:Ljava/util/ArrayList;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartTxNum:Ljava/util/ArrayList;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartXVal:Ljava/util/ArrayList;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartYVal:Ljava/util/ArrayList;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartXstrVal:Ljava/util/ArrayList;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->orderValList:Ljava/util/ArrayList;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->bubblesize:Ljava/util/ArrayList;

    .line 91
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->shapessolidfill:Z

    .line 92
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->shapeslinefill:Z

    .line 93
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->solidfill:Ljava/lang/String;

    .line 94
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->linefill:Ljava/lang/String;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->colorList:Ljava/util/ArrayList;

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->lumModList:Ljava/util/ArrayList;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradcolorLst:Ljava/util/ArrayList;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradcolorsize:Ljava/util/ArrayList;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradientPos:Ljava/util/ArrayList;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradpossize:Ljava/util/ArrayList;

    .line 106
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->colorstyle:Ljava/lang/String;

    .line 127
    return-void
.end method

.method protected constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 3
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 143
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->exPicFileName:Ljava/util/ArrayList;

    .line 64
    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->imgcount:I

    .line 65
    iput v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartcnt:I

    .line 69
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->colormap:Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartName:Ljava/util/ArrayList;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartstrCount:Ljava/util/ArrayList;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartnumCount:Ljava/util/ArrayList;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartRelId:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartCatStr:Ljava/util/ArrayList;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartCatNum:Ljava/util/ArrayList;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartTxStr:Ljava/util/ArrayList;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartTxNum:Ljava/util/ArrayList;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartXVal:Ljava/util/ArrayList;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartYVal:Ljava/util/ArrayList;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartXstrVal:Ljava/util/ArrayList;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->orderValList:Ljava/util/ArrayList;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->bubblesize:Ljava/util/ArrayList;

    .line 91
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->shapessolidfill:Z

    .line 92
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->shapeslinefill:Z

    .line 93
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->solidfill:Ljava/lang/String;

    .line 94
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->linefill:Ljava/lang/String;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->colorList:Ljava/util/ArrayList;

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->lumModList:Ljava/util/ArrayList;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradcolorLst:Ljava/util/ArrayList;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradcolorsize:Ljava/util/ArrayList;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradientPos:Ljava/util/ArrayList;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradpossize:Ljava/util/ArrayList;

    .line 106
    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->colorstyle:Ljava/lang/String;

    .line 145
    return-void
.end method


# virtual methods
.method public addBubbleSz(Ljava/lang/String;)V
    .locals 1
    .param p1, "orderVal"    # Ljava/lang/String;

    .prologue
    .line 503
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->bubblesize:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 504
    return-void
.end method

.method public addCellAnchor(Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V
    .locals 1
    .param p1, "cellAnchor"    # Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->cellAnchorLst:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 186
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->cellAnchorLst:Ljava/util/ArrayList;

    .line 188
    :cond_0
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->currentCellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    .line 189
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->cellAnchorLst:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    return-void
.end method

.method public addChartCatnum(Ljava/lang/String;)V
    .locals 1
    .param p1, "eximgprstname"    # Ljava/lang/String;

    .prologue
    .line 439
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartCatNum:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    return-void
.end method

.method public addChartCatstr(Ljava/lang/String;)V
    .locals 1
    .param p1, "eximgprstname"    # Ljava/lang/String;

    .prologue
    .line 459
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartCatStr:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 460
    return-void
.end method

.method public addChartName(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 415
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartName:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 416
    return-void
.end method

.method public addChartNumCount(Ljava/lang/String;)V
    .locals 1
    .param p1, "cnt"    # Ljava/lang/String;

    .prologue
    .line 431
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartnumCount:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 432
    return-void
.end method

.method public addChartRelId(Ljava/lang/String;)V
    .locals 1
    .param p1, "relId"    # Ljava/lang/String;

    .prologue
    .line 587
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartRelId:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    return-void
.end method

.method public addChartTxnum(Ljava/lang/String;)V
    .locals 1
    .param p1, "eximgprstname"    # Ljava/lang/String;

    .prologue
    .line 447
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartTxNum:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    return-void
.end method

.method public addChartTxstr(Ljava/lang/String;)V
    .locals 1
    .param p1, "eximgprstname"    # Ljava/lang/String;

    .prologue
    .line 463
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartTxStr:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 464
    return-void
.end method

.method public addChartXVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "eximgprstname"    # Ljava/lang/String;

    .prologue
    .line 471
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartXVal:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 472
    return-void
.end method

.method public addChartXstrVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "eximgprstname"    # Ljava/lang/String;

    .prologue
    .line 487
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartXstrVal:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 488
    return-void
.end method

.method public addChartYVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "eximgprstname"    # Ljava/lang/String;

    .prologue
    .line 479
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartYVal:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 480
    return-void
.end method

.method public addChartstrCount(Ljava/lang/String;)V
    .locals 1
    .param p1, "cnt"    # Ljava/lang/String;

    .prologue
    .line 423
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartstrCount:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 424
    return-void
.end method

.method public addClrStyle(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 370
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->colorstyle:Ljava/lang/String;

    .line 371
    return-void
.end method

.method public addColorVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "colorVal"    # Ljava/lang/String;

    .prologue
    .line 546
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->colorList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 547
    return-void
.end method

.method public addColormap(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 365
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->colormap:Ljava/lang/String;

    .line 366
    return-void
.end method

.method public addGradClrsize(Ljava/lang/String;)V
    .locals 1
    .param p1, "size"    # Ljava/lang/String;

    .prologue
    .line 390
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradcolorsize:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    return-void
.end method

.method public addGradFillClr(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V
    .locals 1
    .param p1, "clr"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    .prologue
    .line 382
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradcolorLst:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 383
    return-void
.end method

.method public addGradPosClr(Ljava/lang/Float;)V
    .locals 1
    .param p1, "gradientPositions"    # Ljava/lang/Float;

    .prologue
    .line 398
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradientPos:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 399
    return-void
.end method

.method public addGradPossize(Ljava/lang/String;)V
    .locals 1
    .param p1, "size"    # Ljava/lang/String;

    .prologue
    .line 406
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradpossize:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407
    return-void
.end method

.method public addLumMod(Ljava/lang/String;)V
    .locals 1
    .param p1, "lumModVal"    # Ljava/lang/String;

    .prologue
    .line 554
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->lumModList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 555
    return-void
.end method

.method public addOrderVal(Ljava/lang/String;)V
    .locals 1
    .param p1, "orderVal"    # Ljava/lang/String;

    .prologue
    .line 495
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->orderValList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 496
    return-void
.end method

.method public addPara(Ljava/lang/Integer;Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;)V
    .locals 1
    .param p1, "shapeIndex"    # Ljava/lang/Integer;
    .param p2, "shapePara"    # Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;

    .prologue
    .line 581
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->currentCellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->currentCellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->setPara(Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;)V

    .line 584
    :cond_0
    return-void
.end method

.method public addThemeColorSchema(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 570
    .local p1, "colorSchema":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->colorSchema:Ljava/util/Map;

    .line 571
    return-void
.end method

.method public addlinefill(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 532
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->shapeslinefill:Z

    .line 533
    return-void
.end method

.method public addlinefills(Ljava/lang/String;)V
    .locals 0
    .param p1, "color"    # Ljava/lang/String;

    .prologue
    .line 524
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->linefill:Ljava/lang/String;

    .line 525
    return-void
.end method

.method public addsolidfill(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 507
    iput-boolean p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->shapessolidfill:Z

    .line 508
    return-void
.end method

.method public addsolidfills(Ljava/lang/String;)V
    .locals 0
    .param p1, "color"    # Ljava/lang/String;

    .prologue
    .line 516
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->solidfill:Ljava/lang/String;

    .line 517
    return-void
.end method

.method public addthememap(Ljava/lang/String;)V
    .locals 0
    .param p1, "color"    # Ljava/lang/String;

    .prologue
    .line 558
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->linefill:Ljava/lang/String;

    .line 559
    return-void
.end method

.method public getAllPictureData()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 341
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->picturesData:Ljava/util/HashMap;

    return-object v0
.end method

.method public getBubbleSz()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 499
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->bubblesize:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCellAnchor()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->cellAnchorLst:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartCatnum()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 435
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartCatNum:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartCatstr()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 451
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartCatStr:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartData()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    return-object v0
.end method

.method public getChartName()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 411
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartName:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartNumCount()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 427
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartnumCount:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartRelId()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 591
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartRelId:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartTxnum()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartTxNum:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartTxstr()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 455
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartTxStr:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartXVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 467
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartXVal:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartXstrVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 483
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartXstrVal:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartYVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 475
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartYVal:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getChartstrCount()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 419
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartstrCount:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getClrStyle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->colorstyle:Ljava/lang/String;

    return-object v0
.end method

.method public getColorSchemakeyval(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 577
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getThemeClrSchema()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getColorVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 542
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->colorList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getColormap()Ljava/lang/String;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->colormap:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentCellAnchor()Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->currentCellAnchor:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    return-object v0
.end method

.method public getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->excelVar:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    return-object v0
.end method

.method public getGradClrsize()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 386
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradcolorsize:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGradFillClr()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 378
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradcolorLst:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGradPosClr()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 394
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradientPos:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGradPossize()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->gradpossize:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLumMod()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 550
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->lumModList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getOrderVal()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 491
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->orderValList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPicData()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->pictData:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    return-object v0
.end method

.method public getPicFilename()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 357
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->exPicFileName:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->mTheme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    return-object v0
.end method

.method public getThemeClrSchema()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 566
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->colorSchema:Ljava/util/Map;

    return-object v0
.end method

.method public getWorkbook()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getParent()Lorg/apache/poi/POIXMLDocumentPart;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    return-object v0
.end method

.method public getextactedsolidfills()Ljava/lang/String;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->solidfill:Ljava/lang/String;

    return-object v0
.end method

.method public getlinefill()Z
    .locals 1

    .prologue
    .line 536
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->shapeslinefill:Z

    return v0
.end method

.method public getlinefills()Ljava/lang/String;
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->linefill:Ljava/lang/String;

    return-object v0
.end method

.method public getsolidfill()Z
    .locals 1

    .prologue
    .line 511
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->shapessolidfill:Z

    return v0
.end method

.method public getthemeclr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->linefill:Ljava/lang/String;

    return-object v0
.end method

.method protected onDocumentRead()V
    .locals 6

    .prologue
    .line 205
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->picturesData:Ljava/util/HashMap;

    .line 207
    const/4 v1, 0x0

    .line 209
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 210
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->read(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    if-eqz v1, :cond_0

    .line 216
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 217
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 211
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 212
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_2
    new-instance v2, Lorg/apache/poi/POIXMLException;

    invoke-direct {v2, v0}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 214
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_1

    .line 216
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 219
    :cond_1
    :goto_1
    throw v2

    .line 217
    :catch_2
    move-exception v0

    .line 218
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected read(Ljava/io/InputStream;)V
    .locals 12
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 226
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelDrawingParser;

    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->context:Landroid/content/Context;

    invoke-direct {v3, p0, p1, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelDrawingParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;Ljava/io/InputStream;Landroid/content/Context;)V

    .line 229
    .local v3, "drawingobj":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelDrawingParser;
    invoke-virtual {v3, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelDrawingParser;->onDrawingRead(Ljava/io/InputStream;)V

    .line 231
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getRelations()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_10

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/poi/POIXMLDocumentPart;

    .line 232
    .local v7, "p":Lorg/apache/poi/POIXMLDocumentPart;
    instance-of v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    if-eqz v9, :cond_2

    move-object v9, v7

    .line 234
    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    iput-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->pictData:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    .line 236
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->exPicFileName:Ljava/util/ArrayList;

    iget v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->imgcount:I

    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->pictData:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;->getFileName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 241
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->picturesData:Ljava/util/HashMap;

    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->pictData:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->pictData:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;->getPictureType()I

    move-result v8

    .line 244
    .local v8, "pictype":I
    const/4 v9, 0x2

    if-eq v8, v9, :cond_1

    .line 245
    iget v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->imgcount:I

    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->pictData:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    invoke-virtual {p0, v9, v10}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->writeImgFile(ILcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;)V

    .line 248
    :cond_1
    iget v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->imgcount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->imgcount:I

    .line 256
    .end local v8    # "pictype":I
    :cond_2
    instance-of v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    if-eqz v9, :cond_0

    .line 257
    invoke-virtual {v7}, Lorg/apache/poi/POIXMLDocumentPart;->getPackageRelationship()Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    move-result-object v0

    .line 259
    .local v0, "chartID":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartRelId()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    iget v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartcnt:I

    if-le v9, v10, :cond_0

    .line 263
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getChartRelId()Ljava/util/ArrayList;

    move-result-object v9

    iget v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartcnt:I

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 264
    .local v2, "chartRelID":Ljava/lang/String;
    const/4 v1, 0x0

    .line 266
    .local v1, "chartMatched":Z
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 267
    const/4 v1, 0x1

    .line 270
    :cond_3
    if-eqz v1, :cond_0

    .line 271
    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .end local v7    # "p":Lorg/apache/poi/POIXMLDocumentPart;
    iput-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    .line 272
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->parseChart()V

    .line 273
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->mChartList:Ljava/util/ArrayList;

    if-nez v9, :cond_4

    .line 274
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->mChartList:Ljava/util/ArrayList;

    .line 279
    :cond_4
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getExChartName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addChartName(Ljava/lang/String;)V

    .line 280
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getExChartNumval()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addChartNumCount(Ljava/lang/String;)V

    .line 281
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getExChartStrnum()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addChartstrCount(Ljava/lang/String;)V

    .line 282
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartCatNumVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_5

    .line 283
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartCatNumVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addChartCatnum(Ljava/lang/String;)V

    .line 282
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 284
    :cond_5
    const/4 v4, 0x0

    :goto_2
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartCatStrVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_6

    .line 285
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartCatStrVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addChartCatstr(Ljava/lang/String;)V

    .line 284
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 286
    :cond_6
    const/4 v4, 0x0

    :goto_3
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartTxNumVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_7

    .line 287
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartTxNumVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addChartTxnum(Ljava/lang/String;)V

    .line 286
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 288
    :cond_7
    const/4 v4, 0x0

    :goto_4
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartTxStrVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_8

    .line 289
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getChartTxStrVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addChartTxstr(Ljava/lang/String;)V

    .line 288
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 290
    :cond_8
    const/4 v4, 0x0

    :goto_5
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getXVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_9

    .line 291
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getXVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addChartXVal(Ljava/lang/String;)V

    .line 290
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 292
    :cond_9
    const/4 v4, 0x0

    :goto_6
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getYVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_a

    .line 293
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getYVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addChartYVal(Ljava/lang/String;)V

    .line 292
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 294
    :cond_a
    const/4 v4, 0x0

    :goto_7
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getXStrVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_b

    .line 295
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getXStrVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addChartXstrVal(Ljava/lang/String;)V

    .line 294
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 296
    :cond_b
    const/4 v4, 0x0

    :goto_8
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getLumMod()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_c

    .line 297
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getLumMod()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addLumMod(Ljava/lang/String;)V

    .line 296
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 300
    :cond_c
    const/4 v4, 0x0

    :goto_9
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getColorVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_d

    .line 301
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getColorVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addColorVal(Ljava/lang/String;)V

    .line 300
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 304
    :cond_d
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_e

    .line 306
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getOrderVal()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 307
    .local v6, "orderval":Ljava/lang/String;
    invoke-virtual {p0, v6}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addOrderVal(Ljava/lang/String;)V

    .line 309
    .end local v6    # "orderval":Ljava/lang/String;
    :cond_e
    const/4 v4, 0x0

    :goto_a
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getBubbleSz()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_f

    .line 310
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFChart;->getBubbleSz()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addBubbleSz(Ljava/lang/String;)V

    .line 309
    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    .line 313
    :cond_f
    iget v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartcnt:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartcnt:I

    goto/16 :goto_0

    .line 319
    .end local v0    # "chartID":Ljava/lang/String;
    .end local v1    # "chartMatched":Z
    .end local v2    # "chartRelID":Ljava/lang/String;
    .end local v4    # "i":I
    :cond_10
    return-void
.end method

.method public setActualFileName(Ljava/io/File;)V
    .locals 0
    .param p1, "actualFileName"    # Ljava/io/File;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->actualFileName:Ljava/io/File;

    .line 174
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "mContex"    # Landroid/content/Context;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->context:Landroid/content/Context;

    .line 166
    return-void
.end method

.method public setExcelVariable(Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V
    .locals 0
    .param p1, "excelVar"    # Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->excelVar:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    .line 178
    return-void
.end method

.method public setFolderName(Ljava/lang/String;)V
    .locals 0
    .param p1, "folderName"    # Ljava/lang/String;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->folderName:Ljava/lang/String;

    .line 170
    return-void
.end method

.method public setTheme(Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V
    .locals 0
    .param p1, "mTheme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->mTheme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 158
    return-void
.end method

.method public writeImgFile(ILcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;)V
    .locals 6
    .param p1, "i"    # I
    .param p2, "pictData"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    .prologue
    .line 322
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->exPicFileName:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 323
    .local v1, "htmlImgFileName":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 325
    :try_start_0
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 327
    .local v2, "is":Ljava/io/InputStream;
    if-nez v2, :cond_0

    .line 328
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getEntryName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->actualFileName:Ljava/io/File;

    invoke-static {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getInputStream(Ljava/lang/String;Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v2

    .line 330
    :cond_0
    if-eqz v2, :cond_1

    .line 331
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->context:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->folderName:Ljava/lang/String;

    invoke-static {v2, v3, v1, v4}, Lorg/apache/poi/util/IOUtils;->writeToFile(Ljava/io/InputStream;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    .end local v2    # "is":Ljava/io/InputStream;
    :cond_1
    :goto_0
    return-void

    .line 334
    :catch_0
    move-exception v0

    .line 335
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "XSSFDrawing"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
