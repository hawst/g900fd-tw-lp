.class public abstract Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "OOXMLSequenceHandler.java"


# instance fields
.field protected sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "nameSpaceId"    # I
    .param p2, "elementName"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(ILjava/lang/String;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "elementName"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 16
    return-void
.end method


# virtual methods
.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v4, 0x0

    .line 25
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    if-nez v3, :cond_0

    .line 46
    :goto_0
    return-void

    .line 28
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 29
    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    aget-object v2, v3, v1

    .line 30
    .local v2, "seqDescriptor":Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    const/4 v3, 0x1

    new-array v0, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 32
    .local v0, "eleHandler":[Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    invoke-virtual {v2, p2, p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;->checkElement(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 33
    aget-object v3, v0, v4

    if-eqz v3, :cond_1

    .line 36
    aget-object v3, v0, v4

    invoke-virtual {p1, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->pushHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 37
    aget-object v3, v0, v4

    invoke-virtual {v3, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    goto :goto_0

    .line 40
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->startUnknownElement()V

    goto :goto_0

    .line 28
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 45
    .end local v0    # "eleHandler":[Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
    .end local v2    # "seqDescriptor":Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->startUnknownElement()V

    goto :goto_0
.end method
