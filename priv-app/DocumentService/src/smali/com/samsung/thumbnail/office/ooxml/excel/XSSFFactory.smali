.class public final Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;
.super Lorg/apache/poi/POIXMLFactory;
.source "XSSFFactory.java"


# static fields
.field private static final inst:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;

.field private static final logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;->logger:Lorg/apache/poi/util/POILogger;

    .line 45
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;-><init>()V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;->inst:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/poi/POIXMLFactory;-><init>()V

    .line 43
    return-void
.end method

.method public static getInstance()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;->inst:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;

    return-object v0
.end method


# virtual methods
.method public createDocumentPart(Lorg/apache/poi/POIXMLDocumentPart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;Lorg/apache/poi/openxml4j/opc/PackagePart;)Lorg/apache/poi/POIXMLDocumentPart;
    .locals 8
    .param p1, "parent"    # Lorg/apache/poi/POIXMLDocumentPart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .param p3, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;

    .prologue
    const/4 v7, 0x1

    .line 54
    invoke-virtual {p2}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getRelationshipType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;->getInstance(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/excel/XSSFRelation;

    move-result-object v2

    .line 56
    .local v2, "descriptor":Lorg/apache/poi/POIXMLRelation;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lorg/apache/poi/POIXMLRelation;->getRelationClass()Ljava/lang/Class;

    move-result-object v4

    if-nez v4, :cond_1

    .line 57
    :cond_0
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFactory;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "using default POIXMLDocumentPart for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;->getRelationshipType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v7, v5}, Lorg/apache/poi/util/POILogger;->log(ILjava/lang/Object;)V

    .line 59
    new-instance v4, Lorg/apache/poi/POIXMLDocumentPart;

    invoke-direct {v4, p3, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 68
    :goto_0
    return-object v4

    .line 63
    :cond_1
    :try_start_0
    invoke-virtual {v2}, Lorg/apache/poi/POIXMLRelation;->getRelationClass()Ljava/lang/Class;

    move-result-object v0

    .line 65
    .local v0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/poi/POIXMLDocumentPart;>;"
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Lorg/apache/poi/openxml4j/opc/PackagePart;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    aput-object v6, v4, v5

    invoke-virtual {v0, v4}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 68
    .local v1, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lorg/apache/poi/POIXMLDocumentPart;>;"
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p3, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    invoke-virtual {v1, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/poi/POIXMLDocumentPart;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 69
    .end local v0    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/poi/POIXMLDocumentPart;>;"
    .end local v1    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lorg/apache/poi/POIXMLDocumentPart;>;"
    :catch_0
    move-exception v3

    .line 70
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Lorg/apache/poi/POIXMLException;

    invoke-direct {v4, v3}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method public newDocumentPart(Lorg/apache/poi/POIXMLRelation;)Lorg/apache/poi/POIXMLDocumentPart;
    .locals 4
    .param p1, "descriptor"    # Lorg/apache/poi/POIXMLRelation;

    .prologue
    .line 77
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/poi/POIXMLRelation;->getRelationClass()Ljava/lang/Class;

    move-result-object v0

    .line 79
    .local v0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/poi/POIXMLDocumentPart;>;"
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 81
    .local v1, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lorg/apache/poi/POIXMLDocumentPart;>;"
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/POIXMLDocumentPart;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 82
    .end local v0    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/poi/POIXMLDocumentPart;>;"
    .end local v1    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<+Lorg/apache/poi/POIXMLDocumentPart;>;"
    :catch_0
    move-exception v2

    .line 83
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Lorg/apache/poi/POIXMLException;

    invoke-direct {v3, v2}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method
