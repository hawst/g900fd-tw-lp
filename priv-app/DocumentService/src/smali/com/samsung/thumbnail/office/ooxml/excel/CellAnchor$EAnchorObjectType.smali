.class public final enum Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;
.super Ljava/lang/Enum;
.source "CellAnchor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EAnchorObjectType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

.field public static final enum CHART:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

.field public static final enum GROUP_SHAPE:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

.field public static final enum PICTURE:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

.field public static final enum SHAPE:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 112
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    const-string/jumbo v1, "SHAPE"

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->SHAPE:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    .line 113
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    const-string/jumbo v1, "GROUP_SHAPE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->GROUP_SHAPE:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    .line 114
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    const-string/jumbo v1, "PICTURE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->PICTURE:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    .line 115
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    const-string/jumbo v1, "CHART"

    invoke-direct {v0, v1, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->CHART:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    .line 111
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->SHAPE:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->GROUP_SHAPE:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->PICTURE:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->CHART:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 111
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    return-object v0
.end method
