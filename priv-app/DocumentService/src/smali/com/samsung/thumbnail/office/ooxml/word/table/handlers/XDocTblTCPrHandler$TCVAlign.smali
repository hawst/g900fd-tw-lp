.class public Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCVAlign;
.super Ljava/lang/Object;
.source "XDocTblTCPrHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/values/IAttrValueConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TCVAlign"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCVAlign;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consumeValue(Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;)V
    .locals 3
    .param p1, "value"    # Lcom/samsung/thumbnail/office/ooxml/values/OOXMLValue;

    .prologue
    .line 126
    instance-of v1, p1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 127
    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    .line 128
    .local v0, "strVal":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler$TCVAlign;->this$0:Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;

    # getter for: Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->tblCellProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;->access$000(Lcom/samsung/thumbnail/office/ooxml/word/table/handlers/XDocTblTCPrHandler;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->setVAlign(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VAlign;)V

    .line 131
    .end local v0    # "strVal":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    :cond_0
    return-void
.end method
