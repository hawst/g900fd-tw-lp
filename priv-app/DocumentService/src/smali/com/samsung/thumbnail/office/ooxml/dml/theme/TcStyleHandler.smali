.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcStyleHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLStrictSequenceHandler;
.source "TcStyleHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;


# instance fields
.field private cellBrdrConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler$ICellBrdrConsumer;

.field private fillChConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;)V
    .locals 2
    .param p1, "fillChConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/PartTableStyleHandler;

    .prologue
    .line 23
    const/16 v0, 0x1f

    const-string/jumbo v1, "tcStyle"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStrictSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 24
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcStyleHandler;->fillChConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    .line 25
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcStyleHandler;->cellBrdrConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler$ICellBrdrConsumer;

    .line 26
    return-void
.end method


# virtual methods
.method public consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V
    .locals 1
    .param p1, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcStyleHandler;->fillChConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    invoke-interface {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;->consumeFill(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 65
    return-void
.end method

.method protected init()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 31
    const/4 v7, 0x3

    new-array v4, v7, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 33
    .local v4, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcStyleHandler;->cellBrdrConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler$ICellBrdrConsumer;

    invoke-direct {v5, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler$ICellBrdrConsumer;)V

    .line 35
    .local v5, "tcBdrHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/TblCellBordersHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "tcBdr"

    invoke-direct {v6, v7, v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 37
    .local v6, "tcBdrSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x0

    aput-object v6, v4, v7

    .line 39
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/FillHandler;

    const/16 v7, 0x1f

    iget-object v8, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcStyleHandler;->fillChConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    invoke-direct {v0, v7, v8, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/FillHandler;-><init>(ILcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 41
    .local v0, "fillHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/FillHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "fill"

    invoke-direct {v3, v7, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 43
    .local v3, "fillSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x1

    aput-object v3, v4, v7

    .line 45
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcStyleHandler;->fillChConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;

    invoke-direct {v1, v7, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/IFillChoiceConsumer;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 46
    .local v1, "fillRefHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/FillRefHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v7, "fillRef"

    invoke-direct {v2, v7, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 48
    .local v2, "fillRefSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v7, 0x2

    aput-object v2, v4, v7

    .line 50
    iput-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcStyleHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 51
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 56
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLStrictSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 58
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/TcStyleHandler;->init()V

    .line 60
    return-void
.end method
