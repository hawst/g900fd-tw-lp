.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelSheetdataRowHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "XExcelSheetdataRowHandler"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 27
    const-string/jumbo v1, "row"

    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 28
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCHandler;-><init>()V

    .line 29
    .local v0, "sheetrowCHandler":Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowCHandler;
    iget-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v2, "c"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 8
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x10

    .line 111
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v3

    .line 114
    .local v3, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    iget-object v5, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->rowindex:Ljava/lang/String;

    iget v6, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    invoke-virtual {v4, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetRowid(Ljava/lang/String;I)V

    .line 117
    const-string/jumbo v1, ":"

    .line 118
    .local v1, "delims":Ljava/lang/String;
    const-string/jumbo v0, ""

    .line 119
    .local v0, "colstr":Ljava/lang/String;
    iget-object v4, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->span:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 120
    const-string/jumbo v4, "1:16"

    iput-object v4, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->span:Ljava/lang/String;

    .line 123
    :cond_0
    new-instance v2, Ljava/util/StringTokenizer;

    iget-object v4, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->span:Ljava/lang/String;

    invoke-direct {v2, v4, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    .local v2, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 125
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-gt v4, v7, :cond_1

    .line 128
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    iget v5, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    invoke-virtual {v4, v0, v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetRowspan(Ljava/lang/String;I)V

    .line 135
    :goto_0
    iget v4, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    .line 137
    :goto_1
    iget v4, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    iget v5, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    if-ge v4, v5, :cond_2

    .line 138
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetRowid()Ljava/util/ArrayList;

    move-result-object v5

    iget v6, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget v6, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    invoke-virtual {v4, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetRowid(Ljava/lang/String;I)V

    .line 142
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetRowspan()Ljava/util/ArrayList;

    move-result-object v5

    iget v6, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget v6, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    invoke-virtual {v4, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetRowspan(Ljava/lang/String;I)V

    .line 137
    iget v4, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    goto :goto_1

    .line 131
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    iget v6, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    invoke-virtual {v4, v5, v6}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetRowspan(Ljava/lang/String;I)V

    goto :goto_0

    .line 147
    :cond_2
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 12
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 36
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 37
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v8

    .line 39
    .local v8, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    const-string/jumbo v9, "r"

    invoke-virtual {p0, p3, v9, p1}, Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetdataRowHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->rowindex:Ljava/lang/String;

    .line 41
    const-string/jumbo v9, "spans"

    invoke-interface {p3, v9}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->span:Ljava/lang/String;

    .line 42
    const-string/jumbo v9, "ht"

    invoke-interface {p3, v9}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 46
    .local v5, "ht":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    const-string/jumbo v10, "true"

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->setBlankrow(Ljava/lang/String;)V

    .line 47
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    const/4 v10, 0x0

    iget v11, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    invoke-virtual {v9, v10, v11}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetStyleid(Ljava/lang/String;I)V

    .line 49
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    const-string/jumbo v10, ""

    iget v11, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countv:I

    invoke-virtual {v9, v10, v11}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetVTextid(Ljava/lang/String;I)V

    .line 50
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    const/4 v10, 0x0

    iget v11, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    invoke-virtual {v9, v10, v11}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetTextDataType(Ljava/lang/String;I)V

    .line 52
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    const/4 v10, 0x0

    iget v11, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    invoke-virtual {v9, v10, v11}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetColumnid(Ljava/lang/String;I)V

    .line 54
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_0

    .line 56
    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 58
    .local v4, "heightVal":Ljava/lang/Double;
    :try_start_0
    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 62
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    iget-object v10, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->rowindex:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v9, v10, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetRowHeight(ILjava/lang/Double;)V

    .line 65
    .end local v4    # "heightVal":Ljava/lang/Double;
    :cond_0
    iget v9, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countv:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countv:I

    .line 66
    iget v9, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    .line 68
    iget v9, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->count:I

    if-nez v9, :cond_2

    .line 69
    const-string/jumbo v2, ":"

    .line 70
    .local v2, "delims":Ljava/lang/String;
    const-string/jumbo v0, ""

    .line 71
    .local v0, "colstr":Ljava/lang/String;
    iget-object v9, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->span:Ljava/lang/String;

    if-nez v9, :cond_1

    .line 72
    const-string/jumbo v9, "1:16"

    iput-object v9, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->span:Ljava/lang/String;

    .line 75
    :cond_1
    new-instance v7, Ljava/util/StringTokenizer;

    iget-object v9, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->span:Ljava/lang/String;

    invoke-direct {v7, v9, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .local v7, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    const/16 v10, 0x10

    if-gt v9, v10, :cond_4

    .line 80
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    iput v9, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->columcount:I

    .line 87
    .end local v0    # "colstr":Ljava/lang/String;
    .end local v2    # "delims":Ljava/lang/String;
    .end local v7    # "st":Ljava/util/StringTokenizer;
    :cond_2
    :goto_1
    iget-object v9, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->rowindex:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 88
    .local v6, "rowount":I
    const/16 v9, 0x14

    if-le v6, v9, :cond_3

    .line 89
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v9

    instance-of v9, v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    if-eqz v9, :cond_3

    .line 90
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v1

    .line 92
    .local v1, "creator":Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    if-eqz v1, :cond_3

    .line 93
    const/4 v9, 0x1

    iput-boolean v9, v1, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    .line 99
    .end local v1    # "creator":Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    :cond_3
    return-void

    .line 59
    .end local v6    # "rowount":I
    .restart local v4    # "heightVal":Ljava/lang/Double;
    :catch_0
    move-exception v3

    .line 60
    .local v3, "e":Ljava/lang/Exception;
    const-string/jumbo v9, "XExcelSheetdataRowHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 82
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "heightVal":Ljava/lang/Double;
    .restart local v0    # "colstr":Ljava/lang/String;
    .restart local v2    # "delims":Ljava/lang/String;
    .restart local v7    # "st":Ljava/util/StringTokenizer;
    :cond_4
    const/16 v9, 0x10

    iput v9, v8, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->columcount:I

    goto :goto_1
.end method
