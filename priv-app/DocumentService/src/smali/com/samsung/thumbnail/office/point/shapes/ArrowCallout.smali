.class public Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "ArrowCallout.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 28
    const-string/jumbo v0, "ArrowCallout"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->TAG:Ljava/lang/String;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 34
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->folderPath:Ljava/io/File;

    .line 35
    return-void
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 24
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 160
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->shapeName:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 582
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "RightArrowCallout"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 177
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    if-nez v2, :cond_2

    .line 178
    const/16 v2, 0x3840

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    .line 179
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    if-nez v2, :cond_3

    .line 180
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    .line 181
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    if-nez v2, :cond_4

    .line 182
    const/16 v2, 0x4650

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    .line 183
    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    if-nez v2, :cond_5

    .line 184
    const/16 v2, 0x1fa4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    .line 186
    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v16, v2, v3

    .line 187
    .local v16, "cell_width":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v9, v2, v3

    .line 188
    .local v9, "arrow_height":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v10, v2, v3

    .line 189
    .local v10, "arrow_width":F
    const/16 v23, 0x0

    .line 190
    .local v23, "trunk":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    if-le v2, v3, :cond_a

    .line 191
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v23, v2, v3

    .line 196
    :cond_6
    :goto_1
    new-instance v21, Landroid/graphics/Path;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Path;-><init>()V

    .line 197
    .local v21, "path":Landroid/graphics/Path;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 198
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float v2, v2, v16

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 199
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float v2, v2, v16

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float v3, v3, v23

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 201
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float v3, v3, v23

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 203
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 205
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 207
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    sub-float/2addr v3, v9

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 209
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    sub-float v3, v3, v23

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 211
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float v2, v2, v16

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    sub-float v3, v3, v23

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 213
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float v2, v2, v16

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 215
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 216
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Path;->close()V

    .line 218
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_7

    .line 219
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 220
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_8

    .line 221
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 230
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 231
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 232
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x463dd800    # 12150.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 233
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    .line 545
    .end local v9    # "arrow_height":F
    .end local v10    # "arrow_width":F
    .end local v16    # "cell_width":F
    .end local v21    # "path":Landroid/graphics/Path;
    .end local v23    # "trunk":F
    :cond_9
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    if-nez v2, :cond_0

    .line 546
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 549
    .local v7, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->rotation:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v7, v2, v3, v4}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 551
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->bitmap:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->bitmapHight:I

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v22

    .line 556
    .local v22, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v20, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->folderPath:Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "pic_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->shapeName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".png"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 576
    .local v20, "out":Ljava/io/FileOutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 577
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 578
    .end local v20    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v17

    .line 579
    .local v17, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception while writing pictures to out file"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 192
    .end local v7    # "mtx":Landroid/graphics/Matrix;
    .end local v17    # "e":Ljava/lang/Exception;
    .end local v22    # "rotatedBMP":Landroid/graphics/Bitmap;
    .restart local v9    # "arrow_height":F
    .restart local v10    # "arrow_width":F
    .restart local v16    # "cell_width":F
    .restart local v23    # "trunk":F
    :cond_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    if-gt v2, v3, :cond_6

    .line 193
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v23, v2, v3

    goto/16 :goto_1

    .line 236
    .end local v9    # "arrow_height":F
    .end local v10    # "arrow_width":F
    .end local v16    # "cell_width":F
    .end local v23    # "trunk":F
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "LeftArrowCallout"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 250
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    if-nez v2, :cond_c

    .line 251
    const/16 v2, 0x1c20

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    .line 252
    :cond_c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    if-nez v2, :cond_d

    .line 253
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    .line 254
    :cond_d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    if-nez v2, :cond_e

    .line 255
    const/16 v2, 0xe10

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    .line 256
    :cond_e
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    if-nez v2, :cond_f

    .line 257
    const/16 v2, 0x1fa4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    .line 259
    :cond_f
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v16, v2, v3

    .line 260
    .restart local v16    # "cell_width":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v9, v2, v3

    .line 261
    .restart local v9    # "arrow_height":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v10, v2, v3

    .line 262
    .restart local v10    # "arrow_width":F
    const/16 v23, 0x0

    .line 263
    .restart local v23    # "trunk":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    if-le v2, v3, :cond_13

    .line 264
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v23, v2, v3

    .line 269
    :cond_10
    :goto_3
    new-instance v21, Landroid/graphics/Path;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Path;-><init>()V

    .line 270
    .restart local v21    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float v2, v2, v16

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 271
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 272
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 273
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float v2, v2, v16

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 275
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float v2, v2, v16

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    sub-float v3, v3, v23

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 277
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    sub-float v3, v3, v23

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 279
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    sub-float/2addr v3, v9

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 281
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 282
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 284
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float v3, v3, v23

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 286
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float v2, v2, v16

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float v3, v3, v23

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 288
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Path;->close()V

    .line 290
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_11

    .line 291
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 292
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_12

    .line 293
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 302
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x4613a800    # 9450.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 303
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 304
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 305
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_2

    .line 265
    .end local v21    # "path":Landroid/graphics/Path;
    :cond_13
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    if-gt v2, v3, :cond_10

    .line 266
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v23, v2, v3

    goto/16 :goto_3

    .line 308
    .end local v9    # "arrow_height":F
    .end local v10    # "arrow_width":F
    .end local v16    # "cell_width":F
    .end local v23    # "trunk":F
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "UpArrowCallout"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 321
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    if-nez v2, :cond_15

    .line 322
    const/16 v2, 0x1c20

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    .line 323
    :cond_15
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    if-nez v2, :cond_16

    .line 324
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    .line 325
    :cond_16
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    if-nez v2, :cond_17

    .line 326
    const/16 v2, 0xe10

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    .line 327
    :cond_17
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    if-nez v2, :cond_18

    .line 328
    const/16 v2, 0x1fa4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    .line 330
    :cond_18
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v15, v2, v3

    .line 331
    .local v15, "cell_height":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v10, v2, v3

    .line 332
    .restart local v10    # "arrow_width":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v9, v2, v3

    .line 333
    .restart local v9    # "arrow_height":F
    const/16 v23, 0x0

    .line 334
    .restart local v23    # "trunk":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    if-le v2, v3, :cond_1c

    .line 335
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v23, v2, v3

    .line 340
    :cond_19
    :goto_4
    new-instance v21, Landroid/graphics/Path;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Path;-><init>()V

    .line 341
    .restart local v21    # "path":Landroid/graphics/Path;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v15

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 342
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float v2, v2, v23

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v15

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 344
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float v2, v2, v23

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 346
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 348
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 349
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    sub-float/2addr v2, v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 351
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    sub-float v2, v2, v23

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 353
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    sub-float v2, v2, v23

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v15

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 355
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v15

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 357
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 358
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 359
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Path;->close()V

    .line 361
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_1a

    .line 362
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 363
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_1b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_1b

    .line 364
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 373
    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 374
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x4613a800    # 9450.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 375
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 376
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_2

    .line 336
    .end local v21    # "path":Landroid/graphics/Path;
    :cond_1c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    if-gt v2, v3, :cond_19

    .line 337
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v23, v2, v3

    goto/16 :goto_4

    .line 379
    .end local v9    # "arrow_height":F
    .end local v10    # "arrow_width":F
    .end local v15    # "cell_height":F
    .end local v23    # "trunk":F
    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "DownArrowCallout"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 392
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    if-nez v2, :cond_1e

    .line 393
    const/16 v2, 0x3840

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    .line 394
    :cond_1e
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    if-nez v2, :cond_1f

    .line 395
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    .line 396
    :cond_1f
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    if-nez v2, :cond_20

    .line 397
    const/16 v2, 0x4650

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    .line 398
    :cond_20
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    if-nez v2, :cond_21

    .line 399
    const/16 v2, 0x1fa4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    .line 401
    :cond_21
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v15, v2, v3

    .line 402
    .restart local v15    # "cell_height":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v10, v2, v3

    .line 403
    .restart local v10    # "arrow_width":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v9, v2, v3

    .line 404
    .restart local v9    # "arrow_height":F
    const/16 v23, 0x0

    .line 405
    .restart local v23    # "trunk":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    if-le v2, v3, :cond_25

    .line 406
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v23, v2, v3

    .line 411
    :cond_22
    :goto_5
    new-instance v21, Landroid/graphics/Path;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Path;-><init>()V

    .line 412
    .restart local v21    # "path":Landroid/graphics/Path;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 413
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v15

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 414
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float v2, v2, v23

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v15

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 416
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float v2, v2, v23

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 418
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v2, v2

    add-float/2addr v2, v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 420
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 422
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    sub-float/2addr v2, v10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 424
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    sub-float v2, v2, v23

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v9

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 426
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    sub-float v2, v2, v23

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v15

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 428
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v3, v15

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 430
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 431
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Path;->close()V

    .line 433
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_23

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_23

    .line 434
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 435
    :cond_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_24

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_24

    .line 436
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 447
    :cond_24
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 448
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 449
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 450
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x463dd800    # 12150.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_2

    .line 407
    .end local v21    # "path":Landroid/graphics/Path;
    :cond_25
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    if-gt v2, v3, :cond_22

    .line 408
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    div-float v23, v2, v3

    goto/16 :goto_5

    .line 453
    .end local v9    # "arrow_height":F
    .end local v10    # "arrow_width":F
    .end local v15    # "cell_height":F
    .end local v23    # "trunk":F
    :cond_26
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "LeftRightArrowCallout"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 467
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    if-nez v2, :cond_27

    .line 468
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    .line 469
    :cond_27
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    if-nez v2, :cond_28

    .line 470
    const/16 v2, 0x1518

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    .line 471
    :cond_28
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    if-nez v2, :cond_29

    .line 472
    const/16 v2, 0xa8c

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    .line 473
    :cond_29
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    if-nez v2, :cond_2a

    .line 474
    const/16 v2, 0x1fa4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    .line 476
    :cond_2a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    const v3, 0x46a8c000    # 21600.0f

    div-float v19, v2, v3

    .line 477
    .local v19, "mulFactor_Width":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    const v3, 0x46a8c000    # 21600.0f

    div-float v18, v2, v3

    .line 479
    .local v18, "mulFactor_Height":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    rsub-int v2, v2, 0x5460

    int-to-float v11, v2

    .line 480
    .local v11, "at4":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    rsub-int v2, v2, 0x5460

    int-to-float v12, v2

    .line 481
    .local v12, "at5":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    rsub-int v2, v2, 0x5460

    int-to-float v13, v2

    .line 482
    .local v13, "at8":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    rsub-int v2, v2, 0x5460

    int-to-float v14, v2

    .line 486
    .local v14, "at9":F
    new-instance v21, Landroid/graphics/Path;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Path;-><init>()V

    .line 487
    .restart local v21    # "path":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    int-to-float v2, v2

    mul-float v2, v2, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 489
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    int-to-float v2, v2

    mul-float v2, v2, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    int-to-float v3, v3

    mul-float v3, v3, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 491
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    int-to-float v2, v2

    mul-float v2, v2, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    int-to-float v3, v3

    mul-float v3, v3, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 493
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    int-to-float v2, v2

    mul-float v2, v2, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    int-to-float v3, v3

    mul-float v3, v3, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 495
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const v3, 0x4628c000    # 10800.0f

    mul-float v3, v3, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 497
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    int-to-float v2, v2

    mul-float v2, v2, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    mul-float v3, v11, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 499
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval2:I

    int-to-float v2, v2

    mul-float v2, v2, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    mul-float v3, v12, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 501
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    int-to-float v2, v2

    mul-float v2, v2, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    mul-float v3, v12, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 503
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval0:I

    int-to-float v2, v2

    mul-float v2, v2, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 505
    mul-float v2, v13, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const v3, 0x46a8c000    # 21600.0f

    mul-float v3, v3, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 507
    mul-float v2, v13, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    mul-float v3, v12, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 509
    mul-float v2, v14, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    mul-float v3, v12, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 511
    mul-float v2, v14, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    mul-float v3, v11, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 513
    const v2, 0x46a8c000    # 21600.0f

    mul-float v2, v2, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const v3, 0x4628c000    # 10800.0f

    mul-float v3, v3, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 515
    mul-float v2, v14, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval1:I

    int-to-float v3, v3

    mul-float v3, v3, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 517
    mul-float v2, v14, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    int-to-float v3, v3

    mul-float v3, v3, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 519
    mul-float v2, v13, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->adjval3:I

    int-to-float v3, v3

    mul-float v3, v3, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 521
    mul-float v2, v13, v19

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v3, v4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineWidth:D

    double-to-float v4, v4

    add-float/2addr v3, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 524
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->shapeColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_2b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintFill:Landroid/graphics/Paint;

    if-eqz v2, :cond_2b

    .line 525
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintFill:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 526
    :cond_2b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->lineColor:Lorg/apache/poi/java/awt/Color;

    if-eqz v2, :cond_2c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_2c

    .line 527
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 537
    :cond_2c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x45d2f000    # 6750.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 538
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x44a8c000    # 1350.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 539
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x46680800    # 14850.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 540
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->textArea:Landroid/graphics/RectF;

    const v3, 0x469e3400    # 20250.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    mul-float/2addr v3, v4

    const v4, 0x46a8c000    # 21600.0f

    div-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_2
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 80
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 82
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mLeftMarg:I

    .line 83
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mTopMarg:I

    .line 84
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 85
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->draw(Landroid/graphics/Canvas;I)V

    .line 86
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 88
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 71
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 73
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->draw(I)V

    .line 74
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 146
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 148
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mLeftMarg:I

    .line 149
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mTopMarg:I

    .line 150
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    .line 151
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    .line 152
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 153
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->draw(Landroid/graphics/Canvas;I)V

    .line 154
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 156
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 40
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->draw(I)V

    .line 41
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V
    .locals 2
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 47
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mLeftMarg:I

    .line 48
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mTopMarg:I

    .line 49
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 50
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->draw(Landroid/graphics/Canvas;I)V

    .line 51
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 53
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 101
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 103
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mLeftMarg:I

    .line 104
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mTopMarg:I

    .line 105
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 106
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->draw(Landroid/graphics/Canvas;I)V

    .line 107
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 109
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 113
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 115
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mLeftMarg:I

    .line 116
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mTopMarg:I

    .line 117
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 118
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->draw(Landroid/graphics/Canvas;I)V

    .line 119
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 121
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 95
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 96
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->draw(I)V

    .line 97
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 58
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move v4, p8

    move-object v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 61
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mLeftMarg:I

    .line 62
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mTopMarg:I

    .line 63
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 64
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->draw(Landroid/graphics/Canvas;I)V

    .line 65
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 67
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;I)V
    .locals 1
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 124
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 125
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->draw(I)V

    .line 126
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 130
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 131
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 132
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mLeftMarg:I

    .line 133
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mTopMarg:I

    .line 134
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->width:F

    .line 135
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->height:F

    .line 136
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 137
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->draw(Landroid/graphics/Canvas;I)V

    .line 138
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->mDrawOnCanvas:Z

    .line 140
    return-void
.end method
