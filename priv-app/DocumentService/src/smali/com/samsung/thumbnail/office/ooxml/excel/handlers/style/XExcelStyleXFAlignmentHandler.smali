.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/style/XExcelStyleXFAlignmentHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelStyleXFAlignmentHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    const-string/jumbo v0, "alignment"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 23
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 8
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 29
    const-string/jumbo v6, "wrapText"

    invoke-interface {p3, v6}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 30
    .local v5, "wraptext":Ljava/lang/String;
    const-string/jumbo v6, "horizontal"

    invoke-interface {p3, v6}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32
    .local v0, "horizontal":Ljava/lang/String;
    const-string/jumbo v6, "vertical"

    invoke-interface {p3, v6}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 34
    .local v4, "vertical":Ljava/lang/String;
    const-string/jumbo v6, "shrinkToFit"

    invoke-interface {p3, v6}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 36
    .local v2, "shrinkToFit":Ljava/lang/String;
    const-string/jumbo v6, "indent"

    invoke-interface {p3, v6}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 38
    .local v1, "indent":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v3

    .line 40
    .local v3, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    iget v6, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    add-int/lit8 v6, v6, -0x1

    iput v6, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    .line 41
    if-eqz v5, :cond_0

    .line 42
    const-string/jumbo v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_0

    .line 43
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v7, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    invoke-virtual {v6, v5, v7}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addExStyleCellXFWraptext(Ljava/lang/String;I)V

    .line 46
    :cond_0
    if-eqz v0, :cond_1

    .line 47
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v7, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    invoke-virtual {v6, v0, v7}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addAlingmentHorizontal(Ljava/lang/String;I)V

    .line 51
    :cond_1
    if-eqz v4, :cond_2

    .line 52
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v7, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    invoke-virtual {v6, v4, v7}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addAlingmentVertical(Ljava/lang/String;I)V

    .line 57
    :cond_2
    if-eqz v2, :cond_3

    .line 58
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v7, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    invoke-virtual {v6, v2, v7}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addShrinkToFont(Ljava/lang/String;I)V

    .line 62
    :cond_3
    if-eqz v1, :cond_4

    .line 63
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    iget v7, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    invoke-virtual {v6, v1, v7}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->addStyleTextAlignmentIndent(Ljava/lang/String;I)V

    .line 67
    :cond_4
    iget v6, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->numalignment:I

    .line 71
    return-void
.end method
