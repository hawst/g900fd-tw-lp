.class public final enum Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
.super Ljava/lang/Enum;
.source "EBulletAutoNumType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ALPHA_LC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ALPHA_LC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ALPHA_LC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ALPHA_UC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ALPHA_UC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ALPHA_UC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ARABIC1_MINUS:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ARABIC2_MINUS:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ARABIC_DB_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ARABIC_DB_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ARABIC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ARABIC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ARABIC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ARABIC_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum CIRCLE_NUM_DB_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum CIRCLE_NUM_WD_BLACK_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum CIRCLE_NUM_WD_WHITE_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum EA1_CHS_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum EA1_CHS_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum EA1_CHT_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum EA1_CHT_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum EA1_JPN_CHS_DB_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum EA1_JPN_KOR_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum EA1_JPN_KOR_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum HEBREW2_MINUS:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum HINDI_ALPHA1_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum HINDI_ALPHA_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum HINDI_NUM_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum HINDI_NUM_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ROMAN_LC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ROMAN_LC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ROMAN_LC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ROMAN_UC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ROMAN_UC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum ROMAN_UC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum THAI_ALPHA_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum THAI_ALPHA_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum THAI_ALPHA_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum THAI_NUM_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum THAI_NUM_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

.field public static final enum THAI_NUM_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 68
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ALPHA_LC_PAREN_BOTH"

    const-string/jumbo v2, "alphaLcParenBoth"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ALPHA_LC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 72
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ALPHA_UC_PAREN_BOTH"

    const-string/jumbo v2, "alphaUcParenBoth"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ALPHA_UC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 76
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ALPHA_LC_PAREN_R"

    const-string/jumbo v2, "alphaLcParenR"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ALPHA_LC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 79
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ALPHA_UC_PAREN_R"

    const-string/jumbo v2, "alphaUcParenR"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ALPHA_UC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 82
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ALPHA_LC_PERIOD"

    const-string/jumbo v2, "alphaLcPeriod"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ALPHA_LC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 85
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ALPHA_UC_PERIOD"

    const/4 v2, 0x5

    const-string/jumbo v3, "alphaUcPeriod"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ALPHA_UC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 88
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ARABIC_PAREN_BOTH"

    const/4 v2, 0x6

    const-string/jumbo v3, "arabicParenBoth"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 91
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ARABIC_PAREN_R"

    const/4 v2, 0x7

    const-string/jumbo v3, "arabicParenR"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 94
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ARABIC_PERIOD"

    const/16 v2, 0x8

    const-string/jumbo v3, "arabicPeriod"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 97
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ARABIC_PLAIN"

    const/16 v2, 0x9

    const-string/jumbo v3, "arabicPlain"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 100
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ROMAN_LC_PAREN_BOTH"

    const/16 v2, 0xa

    const-string/jumbo v3, "romanLcParenBoth"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ROMAN_LC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 104
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ROMAN_UC_PAREN_BOTH"

    const/16 v2, 0xb

    const-string/jumbo v3, "romanUcParenBoth"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ROMAN_UC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 108
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ROMAN_LC_PAREN_R"

    const/16 v2, 0xc

    const-string/jumbo v3, "romanLcParenR"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ROMAN_LC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 111
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ROMAN_UC_PAREN_R"

    const/16 v2, 0xd

    const-string/jumbo v3, "romanUcParenR"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ROMAN_UC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 114
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ROMAN_LC_PERIOD"

    const/16 v2, 0xe

    const-string/jumbo v3, "romanLcPeriod"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ROMAN_LC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 117
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ROMAN_UC_PERIOD"

    const/16 v2, 0xf

    const-string/jumbo v3, "romanUcPeriod"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ROMAN_UC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 120
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "CIRCLE_NUM_DB_PLAIN"

    const/16 v2, 0x10

    const-string/jumbo v3, "circleNumDbPlain"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->CIRCLE_NUM_DB_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 124
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "CIRCLE_NUM_WD_BLACK_PLAIN"

    const/16 v2, 0x11

    const-string/jumbo v3, "circleNumWdBlackPlain"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->CIRCLE_NUM_WD_BLACK_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 128
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "CIRCLE_NUM_WD_WHITE_PLAIN"

    const/16 v2, 0x12

    const-string/jumbo v3, "circleNumWdWhitePlain"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->CIRCLE_NUM_WD_WHITE_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 132
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ARABIC_DB_PERIOD"

    const/16 v2, 0x13

    const-string/jumbo v3, "arabicDbPeriod"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC_DB_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 135
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ARABIC_DB_PLAIN"

    const/16 v2, 0x14

    const-string/jumbo v3, "arabicDbPlain"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC_DB_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 138
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "EA1_CHS_PERIOD"

    const/16 v2, 0x15

    const-string/jumbo v3, "ea1ChsPeriod"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->EA1_CHS_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 141
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "EA1_CHS_PLAIN"

    const/16 v2, 0x16

    const-string/jumbo v3, "ea1ChsPlain"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->EA1_CHS_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 144
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "EA1_CHT_PERIOD"

    const/16 v2, 0x17

    const-string/jumbo v3, "ea1ChtPeriod"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->EA1_CHT_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 147
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "EA1_CHT_PLAIN"

    const/16 v2, 0x18

    const-string/jumbo v3, "ea1ChtPlain"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->EA1_CHT_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 150
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "EA1_JPN_CHS_DB_PERIOD"

    const/16 v2, 0x19

    const-string/jumbo v3, "ea1JpnChsDbPeriod"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->EA1_JPN_CHS_DB_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 154
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "EA1_JPN_KOR_PLAIN"

    const/16 v2, 0x1a

    const-string/jumbo v3, "ea1JpnKorPlain"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->EA1_JPN_KOR_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 158
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "EA1_JPN_KOR_PERIOD"

    const/16 v2, 0x1b

    const-string/jumbo v3, "ea1JpnKorPeriod"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->EA1_JPN_KOR_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 162
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ARABIC1_MINUS"

    const/16 v2, 0x1c

    const-string/jumbo v3, "arabic1Minus"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC1_MINUS:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 165
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "ARABIC2_MINUS"

    const/16 v2, 0x1d

    const-string/jumbo v3, "arabic2Minus"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC2_MINUS:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 168
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "HEBREW2_MINUS"

    const/16 v2, 0x1e

    const-string/jumbo v3, "hebrew2Minus"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->HEBREW2_MINUS:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 171
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "THAI_ALPHA_PERIOD"

    const/16 v2, 0x1f

    const-string/jumbo v3, "thaiAlphaPeriod"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->THAI_ALPHA_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 175
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "THAI_ALPHA_PAREN_R"

    const/16 v2, 0x20

    const-string/jumbo v3, "thaiAlphaParenR"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->THAI_ALPHA_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 179
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "THAI_ALPHA_PAREN_BOTH"

    const/16 v2, 0x21

    const-string/jumbo v3, "thaiAlphaParenBoth"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->THAI_ALPHA_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 183
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "THAI_NUM_PERIOD"

    const/16 v2, 0x22

    const-string/jumbo v3, "thaiNumPeriod"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->THAI_NUM_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 186
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "THAI_NUM_PAREN_R"

    const/16 v2, 0x23

    const-string/jumbo v3, "thaiNumParenR"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->THAI_NUM_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 190
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "THAI_NUM_PAREN_BOTH"

    const/16 v2, 0x24

    const-string/jumbo v3, "thaiNumParenBoth"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->THAI_NUM_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 194
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "HINDI_ALPHA_PERIOD"

    const/16 v2, 0x25

    const-string/jumbo v3, "hindiAlphaPeriod"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->HINDI_ALPHA_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 198
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "HINDI_NUM_PERIOD"

    const/16 v2, 0x26

    const-string/jumbo v3, "hindiNumPeriod"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->HINDI_NUM_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 201
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "HINDI_NUM_PAREN_R"

    const/16 v2, 0x27

    const-string/jumbo v3, "hindiNumParenR"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->HINDI_NUM_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 205
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    const-string/jumbo v1, "HINDI_ALPHA1_PERIOD"

    const/16 v2, 0x28

    const-string/jumbo v3, "hindiAlpha1Period"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->HINDI_ALPHA1_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    .line 64
    const/16 v0, 0x29

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ALPHA_LC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ALPHA_UC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ALPHA_LC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ALPHA_UC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ALPHA_LC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ALPHA_UC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ROMAN_LC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ROMAN_UC_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ROMAN_LC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ROMAN_UC_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ROMAN_LC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ROMAN_UC_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->CIRCLE_NUM_DB_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->CIRCLE_NUM_WD_BLACK_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->CIRCLE_NUM_WD_WHITE_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC_DB_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC_DB_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->EA1_CHS_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->EA1_CHS_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->EA1_CHT_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->EA1_CHT_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->EA1_JPN_CHS_DB_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->EA1_JPN_KOR_PLAIN:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->EA1_JPN_KOR_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC1_MINUS:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->ARABIC2_MINUS:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->HEBREW2_MINUS:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->THAI_ALPHA_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->THAI_ALPHA_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->THAI_ALPHA_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->THAI_NUM_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->THAI_NUM_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->THAI_NUM_PAREN_BOTH:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->HINDI_ALPHA_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->HINDI_NUM_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->HINDI_NUM_PAREN_R:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->HINDI_ALPHA1_PERIOD:Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 208
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 209
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->value:Ljava/lang/String;

    .line 210
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 217
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->values()[Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 218
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 222
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    :goto_1
    return-object v2

    .line 217
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 222
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/EBulletAutoNumType;->value:Ljava/lang/String;

    return-object v0
.end method
