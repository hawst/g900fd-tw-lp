.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow$ColorConsumer;
.super Ljava/lang/Object;
.source "OuterShadow.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/ColorChoiceHandler$IColorChoiceConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ColorConsumer"
.end annotation


# instance fields
.field effectConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow$IEffectConsumer;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow$IEffectConsumer;)V
    .locals 0
    .param p1, "effectConsumer"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow$IEffectConsumer;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow$ColorConsumer;->effectConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow$IEffectConsumer;

    .line 38
    return-void
.end method


# virtual methods
.method public consumeColor(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "clrScheme"    # Ljava/lang/String;
    .param p2, "val"    # Ljava/lang/String;
    .param p3, "fillProperties"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow$ColorConsumer;->effectConsumer:Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow$IEffectConsumer;

    invoke-interface {v0, p1, p2, p4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/OuterShadow$IEffectConsumer;->effectConsumer(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V

    .line 45
    return-void
.end method
