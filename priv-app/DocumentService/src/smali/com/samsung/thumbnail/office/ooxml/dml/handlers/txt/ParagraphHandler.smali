.class public Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "ParagraphHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;


# instance fields
.field private para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

.field private textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;)V
    .locals 2
    .param p1, "textShape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    .prologue
    .line 32
    const/16 v0, 0x1f

    const-string/jumbo v1, "p"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 33
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    .line 34
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 6
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 82
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    if-eqz v4, :cond_1

    .line 83
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    iget-object v5, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->addParagraph(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    if-eqz v4, :cond_0

    .line 85
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v2

    .line 87
    .local v2, "runLst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 89
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;-><init>()V

    .line 90
    .local v3, "runob":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->setText(Ljava/lang/String;)V

    .line 91
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    .end local v3    # "runob":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getCurrentCellAnchor()Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    move-result-object v0

    .line 96
    .local v0, "cellAnchor":Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getPara()Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;

    move-result-object v1

    .line 97
    .local v1, "excelPara":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;
    if-nez v1, :cond_3

    .line 98
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;

    .end local v1    # "excelPara":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;
    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;-><init>()V

    .line 99
    .restart local v1    # "excelPara":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v1, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;->addPara(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V

    .line 100
    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->setPara(Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;)V

    goto :goto_0

    .line 102
    :cond_3
    iget-object v4, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v1, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;->addPara(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V

    goto :goto_0
.end method

.method protected init()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 37
    const/4 v9, 0x4

    new-array v6, v9, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 39
    .local v6, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;

    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-direct {v8, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V

    .line 40
    .local v8, "txtRunHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextRunHandler;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v9, "r"

    invoke-direct {v7, v9, v8}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 42
    .local v7, "textRunSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v9, 0x0

    aput-object v7, v6, v9

    .line 44
    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    if-eqz v9, :cond_0

    .line 45
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v9, "pPr"

    iget-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->textShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v10

    invoke-direct {v4, v9, v10, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 47
    .local v4, "paraPropHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v9, "pPr"

    invoke-direct {v5, v9, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 49
    .local v5, "paraPropSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    aput-object v5, v6, v11

    .line 58
    :goto_0
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextLineBreakHandler;

    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-direct {v0, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextLineBreakHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V

    .line 59
    .local v0, "breakHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextLineBreakHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v9, "br"

    invoke-direct {v1, v9, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 61
    .local v1, "breakPropSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v9, 0x2

    aput-object v1, v6, v9

    .line 63
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FieldHandler;

    iget-object v9, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-direct {v2, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FieldHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V

    .line 64
    .local v2, "fldHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FieldHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v9, "fld"

    invoke-direct {v3, v9, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 66
    .local v3, "fldSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v9, 0x3

    aput-object v3, v6, v9

    .line 68
    iput-object v6, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 69
    return-void

    .line 51
    .end local v0    # "breakHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextLineBreakHandler;
    .end local v1    # "breakPropSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v2    # "fldHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/FieldHandler;
    .end local v3    # "fldSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    .end local v4    # "paraPropHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    .end local v5    # "paraPropSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    :cond_0
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;

    const-string/jumbo v9, "pPr"

    const/4 v10, 0x0

    invoke-direct {v4, v9, v10, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler$IListStylesPropConsumer;)V

    .line 53
    .restart local v4    # "paraPropHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/TextParaPropertiesHandler;
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v9, "pPr"

    invoke-direct {v5, v9, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 55
    .restart local v5    # "paraPropSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    aput-object v5, v6, v11

    goto :goto_0
.end method

.method public setPropertiesForLvl(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 2
    .param p1, "level"    # Ljava/lang/String;
    .param p2, "paraProps"    # Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;
    .param p3, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/handlers/txt/XPPTParaProperties;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->setParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    .line 113
    :cond_0
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 74
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 75
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->para:Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    .line 76
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/txt/ParagraphHandler;->init()V

    .line 77
    return-void
.end method
