.class public Lcom/samsung/thumbnail/office/point/shapes/BentConnector;
.super Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;
.source "BentConnector.java"


# static fields
.field public static final ARROWSIZEHIGH:I = 0xc

.field public static final ARROWSIZELOW:I = 0x6

.field public static final ARROWSIZEMEDIUM:I = 0x9

.field public static final ARROWTYPECIRCLE:I = 0x4

.field public static final ARROWTYPEDIAMOND:I = 0x3

.field public static final ARROWTYPENARROW:I = 0x5

.field public static final ARROWTYPENOARROW:I = 0x0

.field public static final ARROWTYPESLANTING:I = 0x2

.field public static final ARROWTYPESOLID:I = 0x1


# instance fields
.field private TAG:Ljava/lang/String;

.field private endArrowType:I

.field private endArrowX:I

.field private endArrowY:I

.field private lineWidth:I

.field private mDrawOnCanvas:Z

.field private mLeftMarg:I

.field private mTopMarg:I

.field private startArrowType:I

.field private startArrowX:I

.field private startArrowY:I


# direct methods
.method public constructor <init>(ILjava/io/File;)V
    .locals 2
    .param p1, "connectorShapeIndex"    # I
    .param p2, "folderPath"    # Ljava/io/File;

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 38
    const-string/jumbo v0, "BentConnector"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->TAG:Ljava/lang/String;

    .line 52
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->lineWidth:I

    .line 57
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowType:I

    .line 58
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowType:I

    .line 60
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    .line 74
    iput-object p2, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->folderPath:Ljava/io/File;

    .line 76
    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 2
    .param p1, "folderPath"    # Ljava/io/File;

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;-><init>()V

    .line 38
    const-string/jumbo v0, "BentConnector"

    iput-object v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->TAG:Ljava/lang/String;

    .line 52
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->lineWidth:I

    .line 57
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowType:I

    .line 58
    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowType:I

    .line 60
    iput-boolean v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    .line 68
    iput-object p1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->folderPath:Ljava/io/File;

    .line 69
    return-void
.end method

.method private drawArrow(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIIZ)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "xMax"    # I
    .param p4, "yMax"    # I
    .param p5, "startX"    # I
    .param p6, "startY"    # I
    .param p7, "type"    # I
    .param p8, "isStartArrow"    # Z

    .prologue
    .line 327
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 329
    .local v1, "path":Landroid/graphics/Path;
    packed-switch p7, :pswitch_data_0

    .line 398
    :goto_0
    return-void

    .line 333
    :pswitch_0
    const/4 v0, 0x0

    .line 334
    .local v0, "arrowAdjValue":F
    int-to-float v3, p5

    int-to-float v4, p6

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 335
    const/4 v3, 0x5

    if-ne p7, v3, :cond_0

    .line 336
    int-to-float v3, p3

    const/high16 v4, 0x40800000    # 4.0f

    div-float v0, v3, v4

    .line 342
    :goto_1
    if-eqz p8, :cond_2

    .line 343
    add-int v3, p5, p3

    int-to-float v3, v3

    div-int/lit8 v4, p4, 0x2

    sub-int v4, p6, v4

    int-to-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 344
    div-int/lit8 v3, p3, 0x2

    add-int/2addr v3, p5

    int-to-float v3, v3

    add-float/2addr v3, v0

    int-to-float v4, p6

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 345
    add-int v3, p5, p3

    int-to-float v3, v3

    div-int/lit8 v4, p4, 0x2

    add-int/2addr v4, p6

    int-to-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 346
    int-to-float v3, p5

    int-to-float v4, p6

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 353
    :goto_2
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 354
    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 337
    :cond_0
    const/4 v3, 0x1

    if-ne p7, v3, :cond_1

    .line 338
    int-to-float v3, p3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v0, v3, v4

    goto :goto_1

    .line 340
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 348
    :cond_2
    sub-int v3, p5, p3

    int-to-float v3, v3

    div-int/lit8 v4, p4, 0x2

    sub-int v4, p6, v4

    int-to-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 349
    div-int/lit8 v3, p3, 0x2

    sub-int v3, p5, v3

    int-to-float v3, v3

    sub-float/2addr v3, v0

    int-to-float v4, p6

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 350
    sub-int v3, p5, p3

    int-to-float v3, v3

    div-int/lit8 v4, p4, 0x2

    add-int/2addr v4, p6

    int-to-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 351
    int-to-float v3, p5

    int-to-float v4, p6

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_2

    .line 359
    .end local v0    # "arrowAdjValue":F
    :pswitch_1
    int-to-float v3, p5

    int-to-float v4, p6

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 360
    if-eqz p8, :cond_3

    .line 361
    div-int/lit8 v3, p3, 0x2

    add-int/2addr v3, p5

    int-to-float v3, v3

    div-int/lit8 v4, p4, 0x2

    sub-int v4, p6, v4

    int-to-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 362
    add-int v3, p5, p3

    int-to-float v3, v3

    int-to-float v4, p6

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 363
    div-int/lit8 v3, p3, 0x2

    add-int/2addr v3, p5

    int-to-float v3, v3

    div-int/lit8 v4, p4, 0x2

    add-int/2addr v4, p6

    int-to-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 364
    int-to-float v3, p5

    int-to-float v4, p6

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 371
    :goto_3
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 372
    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 366
    :cond_3
    div-int/lit8 v3, p3, 0x2

    sub-int v3, p5, v3

    int-to-float v3, v3

    div-int/lit8 v4, p4, 0x2

    sub-int v4, p6, v4

    int-to-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 367
    sub-int v3, p5, p3

    int-to-float v3, v3

    int-to-float v4, p6

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 368
    div-int/lit8 v3, p3, 0x2

    sub-int v3, p5, v3

    int-to-float v3, v3

    div-int/lit8 v4, p4, 0x2

    add-int/2addr v4, p6

    int-to-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 369
    int-to-float v3, p5

    int-to-float v4, p6

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_3

    .line 377
    :pswitch_2
    int-to-float v3, p5

    int-to-float v4, p6

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 378
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 380
    .local v2, "rect":Landroid/graphics/RectF;
    if-eqz p8, :cond_4

    .line 381
    int-to-float v3, p5

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 382
    add-int v3, p5, p3

    int-to-float v3, v3

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 383
    div-int/lit8 v3, p4, 0x2

    sub-int v3, p6, v3

    int-to-float v3, v3

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 384
    div-int/lit8 v3, p4, 0x2

    add-int/2addr v3, p6

    int-to-float v3, v3

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    .line 392
    :goto_4
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 393
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 394
    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 386
    :cond_4
    sub-int v3, p5, p3

    int-to-float v3, v3

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 387
    int-to-float v3, p5

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 388
    div-int/lit8 v3, p4, 0x2

    sub-int v3, p6, v3

    int-to-float v3, v3

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 389
    div-int/lit8 v3, p4, 0x2

    add-int/2addr v3, p6

    int-to-float v3, v3

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    goto :goto_4

    .line 329
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private getSize(Lorg/apache/poi/ddf/EscherSimpleProperty;)I
    .locals 2
    .param p1, "inProperty"    # Lorg/apache/poi/ddf/EscherSimpleProperty;

    .prologue
    .line 222
    const/16 v0, 0xc

    .line 223
    .local v0, "sizeOut":I
    if-nez p1, :cond_0

    .line 224
    const/16 v0, 0x9

    .line 237
    :goto_0
    return v0

    .line 226
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v1

    .line 227
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    .line 233
    const/16 v0, 0xc

    goto :goto_0

    .line 229
    :pswitch_0
    const/4 v0, 0x6

    .line 230
    goto :goto_0

    .line 227
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private setHSSFArrowSize(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    .prologue
    .line 243
    const/16 v1, 0x1d2

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getLineSimpleProperty(S)Lorg/apache/poi/ddf/EscherSimpleProperty;

    move-result-object v0

    .line 245
    .local v0, "shapeProperty":Lorg/apache/poi/ddf/EscherSimpleProperty;
    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->getSize(Lorg/apache/poi/ddf/EscherSimpleProperty;)I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowY:I

    .line 247
    const/16 v1, 0x1d3

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getLineSimpleProperty(S)Lorg/apache/poi/ddf/EscherSimpleProperty;

    move-result-object v0

    .line 249
    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->getSize(Lorg/apache/poi/ddf/EscherSimpleProperty;)I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowX:I

    .line 251
    const/16 v1, 0x1d4

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getLineSimpleProperty(S)Lorg/apache/poi/ddf/EscherSimpleProperty;

    move-result-object v0

    .line 253
    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->getSize(Lorg/apache/poi/ddf/EscherSimpleProperty;)I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowY:I

    .line 255
    const/16 v1, 0x1d5

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getLineSimpleProperty(S)Lorg/apache/poi/ddf/EscherSimpleProperty;

    move-result-object v0

    .line 257
    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->getSize(Lorg/apache/poi/ddf/EscherSimpleProperty;)I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowX:I

    .line 258
    return-void
.end method

.method private setHSSFArrowType(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;)V
    .locals 3
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    .prologue
    const/4 v2, 0x0

    .line 206
    const/16 v1, 0x1d0

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getLineSimpleProperty(S)Lorg/apache/poi/ddf/EscherSimpleProperty;

    move-result-object v0

    .line 208
    .local v0, "shapeProperty":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-nez v0, :cond_0

    .line 209
    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowType:I

    .line 213
    :goto_0
    const/16 v1, 0x1d1

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getLineSimpleProperty(S)Lorg/apache/poi/ddf/EscherSimpleProperty;

    move-result-object v0

    .line 215
    if-nez v0, :cond_1

    .line 216
    iput v2, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowType:I

    .line 219
    :goto_1
    return-void

    .line 211
    :cond_0
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowType:I

    goto :goto_0

    .line 218
    :cond_1
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowType:I

    goto :goto_1
.end method


# virtual methods
.method protected draw(Landroid/graphics/Canvas;I)V
    .locals 32
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCount"    # I

    .prologue
    .line 405
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 406
    .local v7, "paint":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->lineColor:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 410
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Line"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 411
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->lineWidth:I

    int-to-float v2, v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 419
    :cond_0
    const/4 v15, 0x0

    .line 420
    .local v15, "heightAdjValueStart":I
    const/16 v29, 0x0

    .line 422
    .local v29, "heightAdjValueEnd":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowType:I

    if-eqz v2, :cond_1

    .line 423
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowY:I

    div-int/lit8 v15, v2, 0x2

    .line 425
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowType:I

    if-eqz v2, :cond_2

    .line 426
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowY:I

    div-int/lit8 v29, v2, 0x2

    .line 428
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "StraightConnector1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "Line"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 430
    :cond_3
    const/16 v28, 0x0

    .line 432
    .local v28, "heightAdj":I
    move/from16 v0, v29

    if-le v15, v0, :cond_7

    .line 433
    move/from16 v28, v15

    .line 438
    :goto_0
    const/4 v3, 0x0

    add-int/lit8 v2, v28, 0x1

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    int-to-float v5, v2

    add-int/lit8 v2, v28, 0x1

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 440
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowType:I

    if-eqz v2, :cond_4

    .line 441
    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowX:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowY:I

    const/4 v10, 0x0

    add-int/lit8 v11, v28, 0x1

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowType:I

    const/4 v13, 0x1

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-direct/range {v5 .. v13}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->drawArrow(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIIZ)V

    .line 444
    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowType:I

    if-eqz v2, :cond_5

    .line 445
    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowX:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowY:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    add-int/lit8 v11, v28, 0x1

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowType:I

    const/4 v13, 0x0

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-direct/range {v5 .. v13}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->drawArrow(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIIZ)V

    .line 508
    .end local v28    # "heightAdj":I
    :cond_5
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    if-nez v2, :cond_6

    .line 509
    new-instance v21, Landroid/graphics/Matrix;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Matrix;-><init>()V

    .line 512
    .local v21, "mtx":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->rotation:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->width:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->height:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 514
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmap:Landroid/graphics/Bitmap;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapHight:I

    move/from16 v20, v0

    const/16 v22, 0x1

    invoke-static/range {v16 .. v22}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v31

    .line 519
    .local v31, "rotatedBMP":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v30, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->folderPath:Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "pic_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->shapeName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".png"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 523
    .local v30, "out":Ljava/io/FileOutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 524
    invoke-virtual/range {v30 .. v30}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 529
    .end local v21    # "mtx":Landroid/graphics/Matrix;
    .end local v30    # "out":Ljava/io/FileOutputStream;
    .end local v31    # "rotatedBMP":Landroid/graphics/Bitmap;
    :cond_6
    :goto_2
    return-void

    .line 435
    .restart local v28    # "heightAdj":I
    :cond_7
    move/from16 v28, v29

    goto/16 :goto_0

    .line 448
    .end local v28    # "heightAdj":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "BentConnector2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 449
    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v5, v2

    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 450
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v3, v2

    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapHight:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 452
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "BentConnector3"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 453
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->adjval0:I

    if-nez v2, :cond_b

    .line 454
    const/4 v3, 0x0

    add-int/lit8 v2, v15, 0x1

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    int-to-float v2, v2

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v2, v5

    add-int/lit8 v2, v15, 0x1

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 457
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v2, v3

    add-int/lit8 v2, v15, 0x1

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    int-to-float v2, v2

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v2, v5

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapHight:I

    add-int/lit8 v2, v2, -0x1

    sub-int v2, v2, v29

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 460
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v2, v3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapHight:I

    add-int/lit8 v2, v2, -0x1

    sub-int v2, v2, v29

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    int-to-float v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapHight:I

    add-int/lit8 v2, v2, -0x1

    sub-int v2, v2, v29

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 475
    :goto_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowType:I

    if-eqz v2, :cond_a

    .line 476
    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowX:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowY:I

    const/4 v10, 0x0

    add-int/lit8 v11, v15, 0x1

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowType:I

    const/4 v13, 0x1

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-direct/range {v5 .. v13}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->drawArrow(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIIZ)V

    .line 479
    :cond_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowType:I

    if-eqz v2, :cond_5

    .line 480
    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowX:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowY:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapHight:I

    add-int/lit8 v2, v2, -0x1

    sub-int v11, v2, v29

    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowType:I

    const/4 v13, 0x0

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-direct/range {v5 .. v13}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->drawArrow(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIIZ)V

    goto/16 :goto_1

    .line 464
    :cond_b
    const v2, 0x46a8c000    # 21600.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    int-to-float v3, v3

    div-float v25, v2, v3

    .line 465
    .local v25, "dividevalue":F
    const/4 v3, 0x0

    add-int/lit8 v2, v15, 0x1

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->adjval0:I

    int-to-float v2, v2

    div-float v5, v2, v25

    add-int/lit8 v2, v15, 0x1

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 467
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->adjval0:I

    int-to-float v2, v2

    div-float v3, v2, v25

    add-int/lit8 v2, v15, 0x1

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->adjval0:I

    int-to-float v2, v2

    div-float v5, v2, v25

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapHight:I

    add-int/lit8 v2, v2, -0x1

    sub-int v2, v2, v29

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 470
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->adjval0:I

    int-to-float v2, v2

    div-float v3, v2, v25

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapHight:I

    add-int/lit8 v2, v2, -0x1

    sub-int v2, v2, v29

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    int-to-float v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapHight:I

    add-int/lit8 v2, v2, -0x1

    sub-int v2, v2, v29

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 484
    .end local v25    # "dividevalue":F
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->shapeName:Ljava/lang/String;

    const-string/jumbo v3, "CurvedConnector3"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 486
    const v2, 0x46a8c000    # 21600.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    int-to-float v3, v3

    div-float v26, v2, v3

    .line 487
    .local v26, "dividevalue_width":F
    new-instance v8, Landroid/graphics/Path;

    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 488
    .local v8, "path":Landroid/graphics/Path;
    const/4 v2, 0x0

    int-to-float v3, v15

    invoke-virtual {v8, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 489
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->adjval0:I

    int-to-float v2, v2

    div-float v9, v2, v26

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->adjval0:I

    int-to-float v2, v2

    div-float v11, v2, v26

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapHight:I

    int-to-float v12, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    int-to-float v13, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapHight:I

    sub-int v2, v2, v29

    int-to-float v14, v2

    invoke-virtual/range {v8 .. v14}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 492
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->paintLine:Landroid/graphics/Paint;

    if-eqz v2, :cond_d

    .line 493
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->paintLine:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 496
    :cond_d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowType:I

    if-eqz v2, :cond_e

    .line 497
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowX:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowY:I

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->startArrowType:I

    move/from16 v16, v0

    const/16 v17, 0x1

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object v11, v7

    invoke-direct/range {v9 .. v17}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->drawArrow(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIIZ)V

    .line 500
    :cond_e
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowType:I

    if-eqz v2, :cond_5

    .line 501
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowX:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowY:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapWidth:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->bitmapHight:I

    sub-int v22, v2, v29

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->endArrowType:I

    move/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    move-object/from16 v18, v7

    invoke-direct/range {v16 .. v24}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->drawArrow(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIIZ)V

    goto/16 :goto_1

    .line 525
    .end local v8    # "path":Landroid/graphics/Path;
    .end local v26    # "dividevalue_width":F
    .restart local v21    # "mtx":Landroid/graphics/Matrix;
    .restart local v31    # "rotatedBMP":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v27

    .line 526
    .local v27, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception while writing pictures to out file"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method public drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F

    .prologue
    .line 280
    invoke-super {p0, p3, p4, p7, p8}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IFF)V

    .line 281
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    .line 282
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mLeftMarg:I

    .line 283
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mTopMarg:I

    .line 284
    invoke-virtual {p0, p3}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->setLineWidth(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;)V

    .line 285
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 286
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->draw(Landroid/graphics/Canvas;I)V

    .line 287
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 288
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    .line 289
    return-void
.end method

.method public drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "shapeCount"    # I

    .prologue
    .line 265
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHSSFShapes(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;I)V

    .line 268
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->setHSSFArrowType(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;)V

    .line 269
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->setHSSFArrowSize(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;)V

    .line 272
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->draw(I)V

    .line 273
    return-void
.end method

.method public drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 310
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    .line 311
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    .line 312
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mLeftMarg:I

    .line 313
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mTopMarg:I

    .line 314
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->width:F

    .line 315
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->height:F

    .line 316
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 317
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->draw(Landroid/graphics/Canvas;I)V

    .line 318
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 319
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    .line 320
    return-void
.end method

.method public drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V
    .locals 0
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "shapeCount"    # I

    .prologue
    .line 83
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;I)V

    .line 84
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->draw(I)V

    .line 85
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 180
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 181
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    .line 182
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mLeftMarg:I

    .line 183
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mTopMarg:I

    .line 184
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 185
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->draw(Landroid/graphics/Canvas;I)V

    .line 186
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    .line 188
    return-void
.end method

.method public drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p3, "shapeCount"    # I
    .param p4, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I

    .prologue
    .line 192
    invoke-super {p0, p2, p3, p4}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;)V

    .line 193
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    .line 194
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mLeftMarg:I

    .line 195
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mTopMarg:I

    .line 196
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 197
    invoke-virtual {p0, p1, p3}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->draw(Landroid/graphics/Canvas;I)V

    .line 198
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 199
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    .line 200
    return-void
.end method

.method public drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "shapeCount"    # I
    .param p3, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 174
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSLFShape(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)V

    .line 175
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->draw(I)V

    .line 176
    return-void
.end method

.method public drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # I
    .param p6, "topMargin"    # I
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 139
    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p7

    move/from16 v4, p8

    move-object/from16 v5, p9

    invoke-super/range {v0 .. v5}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXSSExcelShape(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    .line 142
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mLeftMarg:I

    .line 143
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mTopMarg:I

    .line 145
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->lineWidth:I

    .line 146
    invoke-virtual/range {p9 .. p9}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v6

    .line 147
    .local v6, "lineProp":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    if-eqz v6, :cond_0

    .line 148
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getLineWidth()Ljava/lang/String;

    move-result-object v7

    .line 149
    .local v7, "lnWidth":Ljava/lang/String;
    if-eqz v7, :cond_0

    .line 150
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->lineWidth:I

    .line 153
    .end local v7    # "lnWidth":Ljava/lang/String;
    :cond_0
    int-to-float v0, p5

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 154
    invoke-virtual {p0, p1, p4}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->draw(Landroid/graphics/Canvas;I)V

    .line 155
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    .line 157
    return-void
.end method

.method public drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V
    .locals 2
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeCount"    # I
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "leftMargin"    # I
    .param p5, "topMargin"    # I
    .param p6, "width"    # F
    .param p7, "height"    # F

    .prologue
    .line 295
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;)V

    .line 296
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    .line 297
    iput p4, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mLeftMarg:I

    .line 298
    iput p5, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mTopMarg:I

    .line 299
    iput p6, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->width:F

    .line 300
    iput p7, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->height:F

    .line 301
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 302
    invoke-virtual {p0, p3, p2}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->draw(Landroid/graphics/Canvas;I)V

    .line 303
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mLeftMarg:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mTopMarg:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 304
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->mDrawOnCanvas:Z

    .line 305
    return-void
.end method

.method protected getLineWidth()I
    .locals 1

    .prologue
    .line 541
    iget v0, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->lineWidth:I

    return v0
.end method

.method protected setLineWidth(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;)V
    .locals 2
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    .prologue
    .line 535
    const/16 v1, 0x1cb

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getLineSimpleProperty(S)Lorg/apache/poi/ddf/EscherSimpleProperty;

    move-result-object v0

    .line 537
    .local v0, "shapeProperty":Lorg/apache/poi/ddf/EscherSimpleProperty;
    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->getSize(Lorg/apache/poi/ddf/EscherSimpleProperty;)I

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->lineWidth:I

    .line 538
    return-void
.end method
