.class public Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;
.super Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;
.source "XSLFPictureShape.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "XSLFPictureShape"


# instance fields
.field private blipEmbed:Ljava/lang/String;

.field private file:Ljava/io/File;

.field private isDiagramShape:Z

.field private isTile:Z

.field private picData:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

.field private relArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V
    .locals 0
    .param p1, "shapeProperties"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "sheet"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFSimpleShape;-><init>(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;)V

    .line 79
    return-void
.end method


# virtual methods
.method public drawPicture(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;)Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .locals 12
    .param p1, "folderName"    # Ljava/lang/String;
    .param p2, "slide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .prologue
    .line 339
    const/4 v4, 0x0

    .line 341
    .local v4, "bitcontents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getAnchor()Lorg/apache/poi/java/awt/geom/Rectangle2D;

    move-result-object v10

    .line 343
    .local v10, "anchor":Lorg/apache/poi/java/awt/geom/Rectangle2D;
    if-nez v10, :cond_2

    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShapeForStyle(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 346
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShapeForStyle(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getAnchor()Lorg/apache/poi/java/awt/geom/Rectangle2D;

    move-result-object v10

    .line 358
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->isDiagramShape:Z

    if-eqz v0, :cond_3

    .line 359
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getPictureDataFromRelArray()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v2

    .line 364
    .local v2, "picData":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;
    :goto_1
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->file:Ljava/io/File;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->setFile(Ljava/io/File;)V

    .line 365
    if-eqz v10, :cond_1

    .line 366
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getEntryName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getInputStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 368
    .local v3, "is":Ljava/io/InputStream;
    invoke-virtual {v10}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v0

    double-to-int v0, v0

    int-to-float v4, v0

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    .end local v4    # "bitcontents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    move-result-wide v0

    double-to-int v0, v0

    int-to-float v5, v0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->writeImage(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;Ljava/io/InputStream;FF)Ljava/lang/String;

    move-result-object v11

    .line 370
    .local v11, "path":Ljava/lang/String;
    invoke-virtual {v10}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v0

    double-to-int v0, v0

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v6

    double-to-int v1, v6

    invoke-static {v11, v0, v1}, Lcom/samsung/thumbnail/util/Utils;->decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 374
    .local v5, "bi":Landroid/graphics/Bitmap;
    new-instance v4, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getX()D

    move-result-wide v0

    double-to-int v6, v0

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getY()D

    move-result-wide v0

    double-to-int v7, v0

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v0

    double-to-int v8, v0

    invoke-virtual {v10}, Lorg/apache/poi/java/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v0

    double-to-int v9, v0

    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;-><init>(Landroid/graphics/Bitmap;IIII)V

    .line 378
    .restart local v4    # "bitcontents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    sget-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 381
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v5    # "bi":Landroid/graphics/Bitmap;
    .end local v11    # "path":Ljava/lang/String;
    :cond_1
    return-object v4

    .line 349
    .end local v2    # "picData":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;
    :cond_2
    if-nez v10, :cond_0

    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShapeForAnchor(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 352
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getPlaceHoler()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getShapeForAnchor(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShape$PlaceHolder;)Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getAnchor()Lorg/apache/poi/java/awt/geom/Rectangle2D;

    move-result-object v10

    goto :goto_0

    .line 361
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v2

    .restart local v2    # "picData":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;
    goto :goto_1
.end method

.method public getInputStream(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 10
    .param p1, "entryName"    # Ljava/lang/String;

    .prologue
    .line 301
    const/4 v1, 0x0

    .line 303
    .local v1, "inputStream":Ljava/io/InputStream;
    if-eqz p1, :cond_0

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->file:Ljava/io/File;

    if-nez v7, :cond_1

    :cond_0
    move-object v6, v1

    .line 325
    :goto_0
    return-object v6

    .line 307
    :cond_1
    const/4 v5, 0x0

    .line 309
    .local v5, "zipInput":Ljava/util/zip/ZipInputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v7, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->file:Ljava/io/File;

    invoke-direct {v2, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 311
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .local v2, "inputStream":Ljava/io/InputStream;
    :try_start_1
    new-instance v6, Ljava/util/zip/ZipInputStream;

    invoke-direct {v6, v2}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 314
    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .local v6, "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_2
    :try_start_2
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v4

    .local v4, "zipEntry":Ljava/util/zip/ZipEntry;
    if-eqz v4, :cond_3

    .line 315
    invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v3

    .line 316
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-result v7

    if-eqz v7, :cond_2

    move-object v1, v2

    .line 317
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_0

    .end local v1    # "inputStream":Ljava/io/InputStream;
    .end local v3    # "name":Ljava/lang/String;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    :cond_3
    move-object v5, v6

    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .end local v4    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    :goto_1
    move-object v6, v5

    .line 325
    goto :goto_0

    .line 320
    :catch_0
    move-exception v0

    .line 321
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_2
    const-string/jumbo v7, "XSLFPictureShape"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: FNFE"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 322
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 323
    .local v0, "e":Ljava/io/IOException;
    :goto_3
    const-string/jumbo v7, "XSLFPictureShape"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 322
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    :catch_2
    move-exception v0

    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_3

    .end local v1    # "inputStream":Ljava/io/InputStream;
    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_3
    move-exception v0

    move-object v5, v6

    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_3

    .line 320
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_2

    .end local v1    # "inputStream":Ljava/io/InputStream;
    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_5
    move-exception v0

    move-object v5, v6

    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    move-object v1, v2

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    goto :goto_2
.end method

.method public getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;
    .locals 4

    .prologue
    .line 195
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->picData:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    if-nez v2, :cond_2

    .line 196
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->blipEmbed:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 197
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->picData:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    .line 207
    :goto_0
    return-object v2

    .line 199
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->getRelations()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/POIXMLDocumentPart;

    .line 200
    .local v1, "part":Lorg/apache/poi/POIXMLDocumentPart;
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->getRelationId(Lorg/apache/poi/POIXMLDocumentPart;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 201
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->getRelationId(Lorg/apache/poi/POIXMLDocumentPart;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->blipEmbed:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 202
    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    .end local v1    # "part":Lorg/apache/poi/POIXMLDocumentPart;
    iput-object v1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->picData:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    goto :goto_1

    .line 207
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->picData:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    goto :goto_0
.end method

.method public getPictureDataFromRelArray()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;
    .locals 19

    .prologue
    .line 106
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->picData:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    if-nez v2, :cond_8

    .line 107
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->blipEmbed:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->picData:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    .line 191
    :goto_0
    return-object v2

    .line 110
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->relArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 111
    .local v12, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;>;"
    const/4 v14, 0x0

    .line 112
    .local v14, "rel":Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;
    const/4 v11, 0x0

    .line 114
    .local v11, "imgPath":Ljava/lang/String;
    :cond_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 115
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "rel":Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;
    check-cast v14, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;

    .line 116
    .restart local v14    # "rel":Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->getId()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->blipEmbed:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 117
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->getTarget()Ljava/lang/String;

    move-result-object v11

    .line 122
    :cond_2
    if-nez v11, :cond_3

    .line 123
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->picData:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    goto :goto_0

    .line 126
    :cond_3
    const/16 v2, 0x6d

    invoke-virtual {v11, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {v11, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 128
    const/4 v9, 0x0

    .line 129
    .local v9, "fileInputStream":Ljava/io/FileInputStream;
    const/16 v16, 0x0

    .line 131
    .local v16, "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    :try_start_0
    new-instance v10, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->file:Ljava/io/File;

    invoke-direct {v10, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .local v10, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v17, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;

    new-instance v2, Ljava/util/zip/ZipInputStream;

    invoke-direct {v2, v10}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v17

    invoke-direct {v0, v11, v2}, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;-><init>(Ljava/lang/String;Ljava/util/zip/ZipInputStream;)V
    :try_end_1
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 134
    .end local v16    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .local v17, "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    :try_start_2
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;->getFakeZipEntry()Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;

    move-result-object v18

    .line 136
    .local v18, "zipEntry":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    const/4 v3, 0x0

    .line 137
    .local v3, "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    const/4 v1, 0x0

    .line 138
    .local v1, "packRel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    if-eqz v18, :cond_6

    .line 139
    invoke-static/range {v18 .. v18}, Lorg/apache/poi/openxml4j/opc/ZipPackage;->buildPartName(Ljava/util/zip/ZipEntry;)Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v13

    .line 143
    .local v13, "pkgPartName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    if-eqz v13, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v2

    iget-object v2, v2, Lorg/apache/poi/openxml4j/opc/OPCPackage;->contentTypeManager:Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;

    invoke-virtual {v2, v13}, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->getContentType(Lorg/apache/poi/openxml4j/opc/PackagePartName;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 146
    new-instance v3, Lorg/apache/poi/openxml4j/opc/ZipPackagePart;

    .end local v3    # "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v4

    iget-object v4, v4, Lorg/apache/poi/openxml4j/opc/OPCPackage;->contentTypeManager:Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;

    invoke-virtual {v4, v13}, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->getContentType(Lorg/apache/poi/openxml4j/opc/PackagePartName;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-direct {v3, v2, v0, v13, v4}, Lorg/apache/poi/openxml4j/opc/ZipPackagePart;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Ljava/util/zip/ZipEntry;Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;)V

    .line 154
    .restart local v3    # "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    const/4 v15, 0x0

    .line 155
    .local v15, "targetModeAttr":Ljava/lang/String;
    if-eqz v14, :cond_4

    .line 156
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->getTargeMode()Ljava/lang/String;

    move-result-object v15

    .line 159
    :cond_4
    sget-object v5, Lorg/apache/poi/openxml4j/opc/TargetMode;->INTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;

    .line 160
    .local v5, "targetMode":Lorg/apache/poi/openxml4j/opc/TargetMode;
    if-eqz v15, :cond_5

    .line 161
    const-string/jumbo v2, "internal"

    invoke-virtual {v15, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    sget-object v5, Lorg/apache/poi/openxml4j/opc/TargetMode;->INTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;

    .line 165
    :cond_5
    :goto_1
    new-instance v1, Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .end local v1    # "packRel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v2

    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->getTarget()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->toURI(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v4

    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->getType()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->blipEmbed:Ljava/lang/String;

    invoke-direct/range {v1 .. v7}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Lorg/apache/poi/openxml4j/opc/PackagePart;Ljava/net/URI;Lorg/apache/poi/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    .end local v5    # "targetMode":Lorg/apache/poi/openxml4j/opc/TargetMode;
    .end local v13    # "pkgPartName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .end local v15    # "targetModeAttr":Ljava/lang/String;
    .restart local v1    # "packRel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    :cond_6
    if-eqz v3, :cond_7

    if-eqz v1, :cond_7

    .line 173
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    invoke-direct {v2, v3, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->picData:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;
    :try_end_2
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_b
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 182
    :cond_7
    if-eqz v10, :cond_8

    .line 184
    :try_start_3
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 191
    .end local v1    # "packRel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .end local v3    # "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v11    # "imgPath":Ljava/lang/String;
    .end local v12    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;>;"
    .end local v14    # "rel":Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;
    .end local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .end local v18    # "zipEntry":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    :cond_8
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->picData:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    goto/16 :goto_0

    .line 161
    .restart local v1    # "packRel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .restart local v3    # "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    .restart local v5    # "targetMode":Lorg/apache/poi/openxml4j/opc/TargetMode;
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v11    # "imgPath":Ljava/lang/String;
    .restart local v12    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;>;"
    .restart local v13    # "pkgPartName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .restart local v14    # "rel":Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;
    .restart local v15    # "targetModeAttr":Ljava/lang/String;
    .restart local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v18    # "zipEntry":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    :cond_9
    :try_start_4
    sget-object v5, Lorg/apache/poi/openxml4j/opc/TargetMode;->EXTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;
    :try_end_4
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_4 .. :try_end_4} :catch_d
    .catch Ljava/net/URISyntaxException; {:try_start_4 .. :try_end_4} :catch_b
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_9
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_1

    .line 185
    .end local v5    # "targetMode":Lorg/apache/poi/openxml4j/opc/TargetMode;
    .end local v13    # "pkgPartName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .end local v15    # "targetModeAttr":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 186
    .local v8, "e":Ljava/io/IOException;
    const-string/jumbo v2, "XSLFPictureShape"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 175
    .end local v1    # "packRel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .end local v3    # "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .end local v18    # "zipEntry":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v16    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    :catch_1
    move-exception v8

    .line 176
    .local v8, "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    :goto_3
    :try_start_5
    const-string/jumbo v2, "XSLFPictureShape"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 182
    if-eqz v9, :cond_8

    .line 184
    :try_start_6
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_2

    .line 185
    :catch_2
    move-exception v8

    .line 186
    .local v8, "e":Ljava/io/IOException;
    const-string/jumbo v2, "XSLFPictureShape"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 177
    .end local v8    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v8

    .line 178
    .local v8, "e":Ljava/net/URISyntaxException;
    :goto_4
    :try_start_7
    const-string/jumbo v2, "XSLFPictureShape"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 182
    if-eqz v9, :cond_8

    .line 184
    :try_start_8
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_2

    .line 185
    :catch_4
    move-exception v8

    .line 186
    .local v8, "e":Ljava/io/IOException;
    const-string/jumbo v2, "XSLFPictureShape"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 179
    .end local v8    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v8

    .line 180
    .local v8, "e":Ljava/io/FileNotFoundException;
    :goto_5
    :try_start_9
    const-string/jumbo v2, "XSLFPictureShape"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 182
    if-eqz v9, :cond_8

    .line 184
    :try_start_a
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto/16 :goto_2

    .line 185
    :catch_6
    move-exception v8

    .line 186
    .local v8, "e":Ljava/io/IOException;
    const-string/jumbo v2, "XSLFPictureShape"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 182
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    :goto_6
    if-eqz v9, :cond_a

    .line 184
    :try_start_b
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 187
    :cond_a
    :goto_7
    throw v2

    .line 185
    :catch_7
    move-exception v8

    .line 186
    .restart local v8    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "XSLFPictureShape"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 182
    .end local v8    # "e":Ljava/io/IOException;
    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v2

    move-object v9, v10

    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_6

    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v16    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    :catchall_2
    move-exception v2

    move-object/from16 v16, v17

    .end local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v16    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    move-object v9, v10

    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_6

    .line 179
    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_8
    move-exception v8

    move-object v9, v10

    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_5

    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v16    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    :catch_9
    move-exception v8

    move-object/from16 v16, v17

    .end local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v16    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    move-object v9, v10

    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_5

    .line 177
    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_a
    move-exception v8

    move-object v9, v10

    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    goto/16 :goto_4

    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v16    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    :catch_b
    move-exception v8

    move-object/from16 v16, v17

    .end local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v16    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    move-object v9, v10

    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    goto/16 :goto_4

    .line 175
    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_c
    move-exception v8

    move-object v9, v10

    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    goto/16 :goto_3

    .end local v9    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v16    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    :catch_d
    move-exception v8

    move-object/from16 v16, v17

    .end local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v16    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    move-object v9, v10

    .end local v10    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v9    # "fileInputStream":Ljava/io/FileInputStream;
    goto/16 :goto_3
.end method

.method public isDiagramShape()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->isDiagramShape:Z

    return v0
.end method

.method public isTile()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->isTile:Z

    return v0
.end method

.method public setBlipEmbed(Ljava/lang/String;)V
    .locals 0
    .param p1, "blipEmbed"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->blipEmbed:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public setFile(Ljava/io/File;)V
    .locals 0
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 329
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->file:Ljava/io/File;

    .line 330
    return-void
.end method

.method public setIsDiagramShape()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->isDiagramShape:Z

    .line 95
    return-void
.end method

.method public setRelList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p1, "relArray":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;>;"
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->relArray:Ljava/util/List;

    .line 103
    return-void
.end method

.method public setTile()V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->isTile:Z

    .line 87
    return-void
.end method

.method public writeImage(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;Ljava/io/InputStream;FF)Ljava/lang/String;
    .locals 21
    .param p1, "folderName"    # Ljava/lang/String;
    .param p2, "picData"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;
    .param p3, "is"    # Ljava/io/InputStream;
    .param p4, "width"    # F
    .param p5, "height"    # F

    .prologue
    .line 218
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getPictureType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 265
    if-eqz p3, :cond_0

    .line 266
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    move-result-object v4

    instance-of v4, v4, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    if-eqz v4, :cond_6

    .line 267
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getFileName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-static {v0, v4, v5, v1}, Lorg/apache/poi/util/IOUtils;->writeToFile(Ljava/io/InputStream;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 285
    :cond_0
    :goto_0
    if-eqz p3, :cond_1

    .line 286
    :try_start_1
    invoke-virtual/range {p3 .. p3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    .line 293
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x8001

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getFileName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 220
    :pswitch_0
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x8001

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v11

    .line 222
    .local v11, "dir":Ljava/io/File;
    new-instance v16, Ljava/io/FileOutputStream;

    new-instance v4, Ljava/io/File;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getPictureData()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getFileName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v11, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 224
    .local v16, "fos":Ljava/io/FileOutputStream;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->file:Ljava/io/File;

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->setFile(Ljava/io/File;)V

    .line 225
    new-instance v18, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getData()[B

    move-result-object v4

    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;-><init>([B)V

    .line 226
    .local v18, "readEMFFile":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawImage()Landroid/graphics/Bitmap;

    move-result-object v10

    .line 227
    .local v10, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v10, :cond_2

    .line 228
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    move-object/from16 v0, v16

    invoke-virtual {v10, v4, v5, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 229
    :cond_2
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto/16 :goto_0

    .line 277
    .end local v10    # "bitmap":Landroid/graphics/Bitmap;
    .end local v11    # "dir":Ljava/io/File;
    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .end local v18    # "readEMFFile":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;
    :catch_0
    move-exception v14

    .line 278
    .local v14, "ex":Ljava/io/FileNotFoundException;
    :try_start_3
    const-string/jumbo v4, "XSLFPictureShape"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "FileNotFoundException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v14}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 285
    if-eqz p3, :cond_1

    .line 286
    :try_start_4
    invoke-virtual/range {p3 .. p3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_1

    .line 288
    :catch_1
    move-exception v13

    .line 289
    .local v13, "e":Ljava/io/IOException;
    const-string/jumbo v4, "XSLFPictureShape"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v13}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 232
    .end local v13    # "e":Ljava/io/IOException;
    .end local v14    # "ex":Ljava/io/FileNotFoundException;
    :pswitch_1
    :try_start_5
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getData()[B

    move-result-object v4

    if-eqz v4, :cond_0

    .line 233
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x8001

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v12

    .line 235
    .local v12, "directory":Ljava/io/File;
    new-instance v20, Ljava/io/FileOutputStream;

    new-instance v4, Ljava/io/File;

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getFileName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v12, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 237
    .local v20, "wmfFos":Ljava/io/FileOutputStream;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->file:Ljava/io/File;

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->setFile(Ljava/io/File;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 239
    :try_start_6
    new-instance v19, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getData()[B

    move-result-object v4

    move-object/from16 v0, v19

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-direct {v0, v4, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;-><init>([BFF)V

    .line 241
    .local v19, "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawImage()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 242
    .local v3, "wmfBitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->flipTheImage()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 243
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 244
    .local v8, "matrix":Landroid/graphics/Matrix;
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, -0x40800000    # -1.0f

    invoke-virtual {v8, v4, v5}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 245
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    const/4 v9, 0x1

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 248
    .local v15, "flippedBitmap":Landroid/graphics/Bitmap;
    if-eqz v15, :cond_3

    .line 249
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x19

    move-object/from16 v0, v20

    invoke-virtual {v15, v4, v5, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 257
    .end local v8    # "matrix":Landroid/graphics/Matrix;
    .end local v15    # "flippedBitmap":Landroid/graphics/Bitmap;
    :cond_3
    :goto_2
    if-eqz v20, :cond_0

    .line 258
    :try_start_7
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 259
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 252
    :cond_4
    if-eqz v3, :cond_3

    .line 253
    :try_start_8
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x19

    move-object/from16 v0, v20

    invoke-virtual {v3, v4, v5, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2

    .line 257
    .end local v3    # "wmfBitmap":Landroid/graphics/Bitmap;
    .end local v19    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    :catchall_0
    move-exception v4

    if-eqz v20, :cond_5

    .line 258
    :try_start_9
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V

    .line 259
    const/16 v20, 0x0

    :cond_5
    throw v4
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 279
    .end local v12    # "directory":Ljava/io/File;
    .end local v20    # "wmfFos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v17

    .line 280
    .local v17, "ioe":Ljava/io/IOException;
    :try_start_a
    const-string/jumbo v4, "XSLFPictureShape"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 285
    if-eqz p3, :cond_1

    .line 286
    :try_start_b
    invoke-virtual/range {p3 .. p3}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3

    goto/16 :goto_1

    .line 288
    :catch_3
    move-exception v13

    .line 289
    .restart local v13    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "XSLFPictureShape"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v13}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 271
    .end local v13    # "e":Ljava/io/IOException;
    .end local v17    # "ioe":Ljava/io/IOException;
    :cond_6
    :try_start_c
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureShape;->getSheet()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSheet;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFPictureData;->getFileName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-static {v0, v4, v5, v1}, Lorg/apache/poi/util/IOUtils;->writeToFile(Ljava/io/InputStream;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_0

    .line 281
    :catch_4
    move-exception v13

    .line 282
    .local v13, "e":Ljava/lang/Exception;
    :try_start_d
    const-string/jumbo v4, "XSLFPictureShape"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v13}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 285
    if-eqz p3, :cond_1

    .line 286
    :try_start_e
    invoke-virtual/range {p3 .. p3}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5

    goto/16 :goto_1

    .line 288
    :catch_5
    move-exception v13

    .line 289
    .local v13, "e":Ljava/io/IOException;
    const-string/jumbo v4, "XSLFPictureShape"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v13}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 288
    .end local v13    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v13

    .line 289
    .restart local v13    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "XSLFPictureShape"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v13}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 284
    .end local v13    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v4

    .line 285
    if-eqz p3, :cond_7

    .line 286
    :try_start_f
    invoke-virtual/range {p3 .. p3}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_7

    .line 290
    :cond_7
    :goto_3
    throw v4

    .line 288
    :catch_7
    move-exception v13

    .line 289
    .restart local v13    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "XSLFPictureShape"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v13}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 218
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
