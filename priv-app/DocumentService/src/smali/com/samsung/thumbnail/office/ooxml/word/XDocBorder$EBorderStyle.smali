.class public final enum Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
.super Ljava/lang/Enum;
.source "XDocBorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EBorderStyle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum APPLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum ARCHED_SCALLOPS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BABY_PACIFIER:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BABY_RATTLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BALLOONS3_COLORS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BALLOONS_HOT_AIR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BASIC_BLACK_DASHES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BASIC_BLACK_DOTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BASIC_BLACK_SQUARES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BASIC_THIN_LINES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BASIC_WHITE_DASHES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BASIC_WHITE_DOTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BASIC_WHITE_SQUARES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BASIC_WIDE_INLINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BASIC_WIDE_MIDLINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BASIC_WIDE_OUTLINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BATS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BIRDS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum BIRDS_FLIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CABINS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CAKE_SLICE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CANDY_CORN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CELTIC_KNOTWORK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CERTIFICATE_BANNER:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CHAIN_LINK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CHAMPAGNE_BOTTLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CHECKED_BAR_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CHECKED_BAR_COLOR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CHECKERED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CHRISTMAS_TREE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CIRCLES_LINES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CIRCLES_RECTANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CLASSICAL_WAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CLOCKS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum COMPASS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CONFETTI:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CONFETTI_GRAYS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CONFETTI_OUTLINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CONFETTI_STREAMERS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CONFETTI_WHITE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CORNER_TRIANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum COUPON_CUTOUT_DASHES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum COUPON_CUTOUT_DOTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CRAZY_MAZE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CREATURES_BUTTERFLY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CREATURES_FISH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CREATURES_INSECTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CREATURES_LADY_BUG:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CROSS_STITCH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum CUP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DASHED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DASH_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DASH_DOT_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DASH_DOT_STROKED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DASH_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DECO_ARCH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DECO_ARCH_COLOR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DECO_BLOCKS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DIAMONDS_GRAY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DOTTED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DOT_DASH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DOT_DOT_DASH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DOUBLE_D:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DOUBLE_DIAMONDS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum DOUBLE_WAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum EARTH1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum EARTH2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum ECLIPSING_SQUARES1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum ECLIPSING_SQUARES2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum EGGS_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum FANS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum FILM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum FIRECRACKERS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum FLOWERS_BLOCK_PRINT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum FLOWERS_DAISIES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum FLOWERS_MODERN1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum FLOWERS_MODERN2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum FLOWERS_PANSY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum FLOWERS_RED_ROSE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum FLOWERS_ROSES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum FLOWERS_TEACUP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum FLOWERS_TINY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum GEMS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum GINGERBREAD_MAN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum GRADIENT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum HANDMADE1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum HANDMADE2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum HEARTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum HEART_BALLOON:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum HEART_GRAY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum HEEBIE_JEEBIES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum HOLLY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum HOUSE_FUNKY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum HYPNOTIC:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum ICE_CREAM_CONES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum INSET:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum LIGHTNING1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum LIGHTNING2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum LIGHT_BULB:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum MAPLE_LEAF:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum MAPLE_MUFFINS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum MAP_PINS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum MARQUEE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum MARQUEE_TOOTHED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum MEDIUM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum MEDIUM_DASHED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum MEDIUM_DASH_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum MEDIUM_DASH_DOT_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum MOONS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum MOSAIC:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum MUSIC_NOTES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum NIL:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum NORTHWEST:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum OUTSET:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum OVALS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PACKAGES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PALMS_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PALMS_COLOR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PAPER_CLIPS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PAPYRUS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PARTY_FAVOR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PARTY_GLASS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PENCILS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PEOPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PEOPLE_HATS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PEOPLE_WAVING:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum POINSETTIAS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum POSTAGE_STAMP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PUMPKIN1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PUSH_PIN_NOTE1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PUSH_PIN_NOTE2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PYRAMIDS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum PYRAMIDS_ABOVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum QUADRANTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum RINGS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SAFARI:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SAWTOOTH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SAWTOOTH_GRAY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SCARED_CAT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SEATTLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SHADOWED_SQUARES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SHARKS_TEETH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SHOREBIRD_TRACKS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SKYROCKET:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SNOWFLAKES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SNOWFLAKE_FANCY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SOMBRERO:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SOUTHWEST:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum STARS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum STARS3D:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum STARS_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum STARS_SHADOWED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum STARS_TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SUN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum SWIRLIGIG:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum THICK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum THIN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum THIN_THICK_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum THIN_THICK_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum THIN_THICK_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum THIN_THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum THIN_THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum THIN_THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum THREE_D_EMBOSS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum THREE_D_ENGRAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum TORN_PAPER:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum TORN_PAPER_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum TREES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum TRIANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum TRIANGLE_PARTY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum TRIBAL1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum TRIBAL2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum TRIBAL3:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum TRIBAL4:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum TRIBAL5:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum TRIBAL6:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum TRIPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum TWISTED_LINES1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum TWISTED_LINES2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum VINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum WAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum WAVELINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum WEAVING_ANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum WEAVING_BRAID:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum WEAVING_RIBBON:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum WEAVING_STRIPS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum WHITE_FLOWERS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum WOODWORK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum X_ILLUSIONS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum ZANY_TRIANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum ZIG_ZAG:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field public static final enum ZIG_ZAG_STITCH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 185
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "NIL"

    const-string/jumbo v2, "nil"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NIL:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 188
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "NONE"

    const-string/jumbo v2, "none"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 191
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SINGLE"

    const-string/jumbo v2, "single"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 194
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "THICK"

    const-string/jumbo v2, "thick"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 197
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DOUBLE"

    const-string/jumbo v2, "double"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 200
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DOTTED"

    const/4 v2, 0x5

    const-string/jumbo v3, "dotted"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOTTED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 203
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DASHED"

    const/4 v2, 0x6

    const-string/jumbo v3, "dashed"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASHED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 206
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DOT_DASH"

    const/4 v2, 0x7

    const-string/jumbo v3, "dotDash"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOT_DASH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 209
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DOT_DOT_DASH"

    const/16 v2, 0x8

    const-string/jumbo v3, "dotDotDash"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOT_DOT_DASH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 212
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "TRIPLE"

    const/16 v2, 0x9

    const-string/jumbo v3, "triple"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 215
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "THIN_THICK_SMALL_GAP"

    const/16 v2, 0xa

    const-string/jumbo v3, "thinThickSmallGap"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 218
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "THICK_THIN_SMALL_GAP"

    const/16 v2, 0xb

    const-string/jumbo v3, "thickThinSmallGap"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 222
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "THIN_THICK_THIN_SMALL_GAP"

    const/16 v2, 0xc

    const-string/jumbo v3, "thinThickThinSmallGap"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 226
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "THIN_THICK_MEDIUM_GAP"

    const/16 v2, 0xd

    const-string/jumbo v3, "thinThickMediumGap"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 229
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "THICK_THIN_MEDIUM_GAP"

    const/16 v2, 0xe

    const-string/jumbo v3, "thickThinMediumGap"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 233
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "THIN_THICK_THIN_MEDIUM_GAP"

    const/16 v2, 0xf

    const-string/jumbo v3, "thinThickThinMediumGap"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 237
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "THIN_THICK_LARGE_GAP"

    const/16 v2, 0x10

    const-string/jumbo v3, "thinThickLargeGap"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 240
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "THICK_THIN_LARGE_GAP"

    const/16 v2, 0x11

    const-string/jumbo v3, "thickThinLargeGap"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 244
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "THIN_THICK_THIN_LARGE_GAP"

    const/16 v2, 0x12

    const-string/jumbo v3, "thinThickThinLargeGap"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 247
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "WAVE"

    const/16 v2, 0x13

    const-string/jumbo v3, "wave"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 250
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DOUBLE_WAVE"

    const/16 v2, 0x14

    const-string/jumbo v3, "doubleWave"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE_WAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 253
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DASH_SMALL_GAP"

    const/16 v2, 0x15

    const-string/jumbo v3, "dashSmallGap"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASH_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 256
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DASH_DOT_STROKED"

    const/16 v2, 0x16

    const-string/jumbo v3, "dashDotStroked"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASH_DOT_STROKED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 259
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "THREE_D_EMBOSS"

    const/16 v2, 0x17

    const-string/jumbo v3, "threeDEmboss"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THREE_D_EMBOSS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 262
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "THREE_D_ENGRAVE"

    const/16 v2, 0x18

    const-string/jumbo v3, "threeDEngrave"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THREE_D_ENGRAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 265
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "OUTSET"

    const/16 v2, 0x19

    const-string/jumbo v3, "outset"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->OUTSET:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 268
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "INSET"

    const/16 v2, 0x1a

    const-string/jumbo v3, "inset"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->INSET:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 271
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "APPLES"

    const/16 v2, 0x1b

    const-string/jumbo v3, "apples"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->APPLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 274
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "ARCHED_SCALLOPS"

    const/16 v2, 0x1c

    const-string/jumbo v3, "archedScallops"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ARCHED_SCALLOPS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 277
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BABY_PACIFIER"

    const/16 v2, 0x1d

    const-string/jumbo v3, "babyPacifier"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BABY_PACIFIER:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 280
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BABY_RATTLE"

    const/16 v2, 0x1e

    const-string/jumbo v3, "babyRattle"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BABY_RATTLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 283
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BALLOONS3_COLORS"

    const/16 v2, 0x1f

    const-string/jumbo v3, "balloons3Colors"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BALLOONS3_COLORS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 286
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BALLOONS_HOT_AIR"

    const/16 v2, 0x20

    const-string/jumbo v3, "balloonsHotAir"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BALLOONS_HOT_AIR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 289
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BASIC_BLACK_DASHES"

    const/16 v2, 0x21

    const-string/jumbo v3, "basicBlackDashes"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_BLACK_DASHES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 292
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BASIC_BLACK_DOTS"

    const/16 v2, 0x22

    const-string/jumbo v3, "basicBlackDots"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_BLACK_DOTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 295
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BASIC_BLACK_SQUARES"

    const/16 v2, 0x23

    const-string/jumbo v3, "basicBlackSquares"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_BLACK_SQUARES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 298
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BASIC_THIN_LINES"

    const/16 v2, 0x24

    const-string/jumbo v3, "basicThinLines"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_THIN_LINES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 301
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BASIC_WHITE_DASHES"

    const/16 v2, 0x25

    const-string/jumbo v3, "basicWhiteDashes"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_WHITE_DASHES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 304
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BASIC_WHITE_DOTS"

    const/16 v2, 0x26

    const-string/jumbo v3, "basicWhiteDots"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_WHITE_DOTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 307
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BASIC_WHITE_SQUARES"

    const/16 v2, 0x27

    const-string/jumbo v3, "basicWhiteSquares"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_WHITE_SQUARES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 310
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BASIC_WIDE_INLINE"

    const/16 v2, 0x28

    const-string/jumbo v3, "basicWideInline"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_WIDE_INLINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 313
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BASIC_WIDE_MIDLINE"

    const/16 v2, 0x29

    const-string/jumbo v3, "basicWideMidline"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_WIDE_MIDLINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 316
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BASIC_WIDE_OUTLINE"

    const/16 v2, 0x2a

    const-string/jumbo v3, "basicWideOutline"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_WIDE_OUTLINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 319
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BATS"

    const/16 v2, 0x2b

    const-string/jumbo v3, "bats"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BATS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 322
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BIRDS"

    const/16 v2, 0x2c

    const-string/jumbo v3, "birds"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BIRDS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 325
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "BIRDS_FLIGHT"

    const/16 v2, 0x2d

    const-string/jumbo v3, "birdsFlight"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BIRDS_FLIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 328
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CABINS"

    const/16 v2, 0x2e

    const-string/jumbo v3, "cabins"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CABINS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 331
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CAKE_SLICE"

    const/16 v2, 0x2f

    const-string/jumbo v3, "cakeSlice"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CAKE_SLICE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 334
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CANDY_CORN"

    const/16 v2, 0x30

    const-string/jumbo v3, "candyCorn"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CANDY_CORN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 337
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CELTIC_KNOTWORK"

    const/16 v2, 0x31

    const-string/jumbo v3, "celticKnotwork"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CELTIC_KNOTWORK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 340
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CERTIFICATE_BANNER"

    const/16 v2, 0x32

    const-string/jumbo v3, "certificateBanner"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CERTIFICATE_BANNER:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 343
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CHAIN_LINK"

    const/16 v2, 0x33

    const-string/jumbo v3, "chainLink"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CHAIN_LINK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 346
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CHAMPAGNE_BOTTLE"

    const/16 v2, 0x34

    const-string/jumbo v3, "champagneBottle"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CHAMPAGNE_BOTTLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 349
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CHECKED_BAR_BLACK"

    const/16 v2, 0x35

    const-string/jumbo v3, "checkedBarBlack"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CHECKED_BAR_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 352
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CHECKED_BAR_COLOR"

    const/16 v2, 0x36

    const-string/jumbo v3, "checkedBarColor"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CHECKED_BAR_COLOR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 355
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CHECKERED"

    const/16 v2, 0x37

    const-string/jumbo v3, "checkered"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CHECKERED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 358
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CHRISTMAS_TREE"

    const/16 v2, 0x38

    const-string/jumbo v3, "christmasTree"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CHRISTMAS_TREE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 361
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CIRCLES_LINES"

    const/16 v2, 0x39

    const-string/jumbo v3, "circlesLines"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CIRCLES_LINES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 364
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CIRCLES_RECTANGLES"

    const/16 v2, 0x3a

    const-string/jumbo v3, "circlesRectangles"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CIRCLES_RECTANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 367
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CLASSICAL_WAVE"

    const/16 v2, 0x3b

    const-string/jumbo v3, "classicalWave"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CLASSICAL_WAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 370
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CLOCKS"

    const/16 v2, 0x3c

    const-string/jumbo v3, "clocks"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CLOCKS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 373
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "COMPASS"

    const/16 v2, 0x3d

    const-string/jumbo v3, "compass"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->COMPASS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 376
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CONFETTI"

    const/16 v2, 0x3e

    const-string/jumbo v3, "confetti"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CONFETTI:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 379
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CONFETTI_GRAYS"

    const/16 v2, 0x3f

    const-string/jumbo v3, "confettiGrays"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CONFETTI_GRAYS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 382
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CONFETTI_OUTLINE"

    const/16 v2, 0x40

    const-string/jumbo v3, "confettiOutline"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CONFETTI_OUTLINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 385
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CONFETTI_STREAMERS"

    const/16 v2, 0x41

    const-string/jumbo v3, "confettiStreamers"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CONFETTI_STREAMERS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 388
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CONFETTI_WHITE"

    const/16 v2, 0x42

    const-string/jumbo v3, "confettiWhite"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CONFETTI_WHITE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 391
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CORNER_TRIANGLES"

    const/16 v2, 0x43

    const-string/jumbo v3, "cornerTriangles"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CORNER_TRIANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 394
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "COUPON_CUTOUT_DASHES"

    const/16 v2, 0x44

    const-string/jumbo v3, "couponCutoutDashes"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->COUPON_CUTOUT_DASHES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 397
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "COUPON_CUTOUT_DOTS"

    const/16 v2, 0x45

    const-string/jumbo v3, "couponCutoutDots"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->COUPON_CUTOUT_DOTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 400
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CRAZY_MAZE"

    const/16 v2, 0x46

    const-string/jumbo v3, "crazyMaze"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CRAZY_MAZE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 403
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CREATURES_BUTTERFLY"

    const/16 v2, 0x47

    const-string/jumbo v3, "creaturesButterfly"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CREATURES_BUTTERFLY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 406
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CREATURES_FISH"

    const/16 v2, 0x48

    const-string/jumbo v3, "creaturesFish"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CREATURES_FISH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 409
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CREATURES_INSECTS"

    const/16 v2, 0x49

    const-string/jumbo v3, "creaturesInsects"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CREATURES_INSECTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 412
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CREATURES_LADY_BUG"

    const/16 v2, 0x4a

    const-string/jumbo v3, "creaturesLadyBug"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CREATURES_LADY_BUG:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 415
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CROSS_STITCH"

    const/16 v2, 0x4b

    const-string/jumbo v3, "crossStitch"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CROSS_STITCH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 418
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "CUP"

    const/16 v2, 0x4c

    const-string/jumbo v3, "cup"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CUP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 421
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DECO_ARCH"

    const/16 v2, 0x4d

    const-string/jumbo v3, "decoArch"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DECO_ARCH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 424
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DECO_ARCH_COLOR"

    const/16 v2, 0x4e

    const-string/jumbo v3, "decoArchColor"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DECO_ARCH_COLOR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 427
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DECO_BLOCKS"

    const/16 v2, 0x4f

    const-string/jumbo v3, "decoBlocks"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DECO_BLOCKS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 430
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DIAMONDS_GRAY"

    const/16 v2, 0x50

    const-string/jumbo v3, "diamondsGray"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DIAMONDS_GRAY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 433
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DOUBLE_D"

    const/16 v2, 0x51

    const-string/jumbo v3, "doubleD"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE_D:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 436
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DOUBLE_DIAMONDS"

    const/16 v2, 0x52

    const-string/jumbo v3, "doubleDiamonds"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE_DIAMONDS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 439
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "EARTH1"

    const/16 v2, 0x53

    const-string/jumbo v3, "earth1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->EARTH1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 442
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "EARTH2"

    const/16 v2, 0x54

    const-string/jumbo v3, "earth2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->EARTH2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 445
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "ECLIPSING_SQUARES1"

    const/16 v2, 0x55

    const-string/jumbo v3, "eclipsingSquares1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ECLIPSING_SQUARES1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 448
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "ECLIPSING_SQUARES2"

    const/16 v2, 0x56

    const-string/jumbo v3, "eclipsingSquares2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ECLIPSING_SQUARES2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 451
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "EGGS_BLACK"

    const/16 v2, 0x57

    const-string/jumbo v3, "eggsBlack"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->EGGS_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 454
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "FANS"

    const/16 v2, 0x58

    const-string/jumbo v3, "fans"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FANS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 457
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "FILM"

    const/16 v2, 0x59

    const-string/jumbo v3, "film"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FILM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 460
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "FIRECRACKERS"

    const/16 v2, 0x5a

    const-string/jumbo v3, "firecrackers"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FIRECRACKERS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 463
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "FLOWERS_BLOCK_PRINT"

    const/16 v2, 0x5b

    const-string/jumbo v3, "flowersBlockPrint"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_BLOCK_PRINT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 466
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "FLOWERS_DAISIES"

    const/16 v2, 0x5c

    const-string/jumbo v3, "flowersDaisies"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_DAISIES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 469
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "FLOWERS_MODERN1"

    const/16 v2, 0x5d

    const-string/jumbo v3, "flowersModern1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_MODERN1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 472
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "FLOWERS_MODERN2"

    const/16 v2, 0x5e

    const-string/jumbo v3, "flowersModern2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_MODERN2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 475
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "FLOWERS_PANSY"

    const/16 v2, 0x5f

    const-string/jumbo v3, "flowersPansy"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_PANSY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 478
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "FLOWERS_RED_ROSE"

    const/16 v2, 0x60

    const-string/jumbo v3, "flowersRedRose"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_RED_ROSE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 481
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "FLOWERS_ROSES"

    const/16 v2, 0x61

    const-string/jumbo v3, "flowersRoses"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_ROSES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 484
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "FLOWERS_TEACUP"

    const/16 v2, 0x62

    const-string/jumbo v3, "flowersTeacup"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_TEACUP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 487
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "FLOWERS_TINY"

    const/16 v2, 0x63

    const-string/jumbo v3, "flowersTiny"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_TINY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 490
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "GEMS"

    const/16 v2, 0x64

    const-string/jumbo v3, "gems"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->GEMS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 493
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "GINGERBREAD_MAN"

    const/16 v2, 0x65

    const-string/jumbo v3, "gingerbreadMan"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->GINGERBREAD_MAN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 496
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "GRADIENT"

    const/16 v2, 0x66

    const-string/jumbo v3, "gradient"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->GRADIENT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 499
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "HANDMADE1"

    const/16 v2, 0x67

    const-string/jumbo v3, "handmade1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HANDMADE1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 502
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "HANDMADE2"

    const/16 v2, 0x68

    const-string/jumbo v3, "handmade2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HANDMADE2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 505
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "HEART_BALLOON"

    const/16 v2, 0x69

    const-string/jumbo v3, "heartBalloon"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HEART_BALLOON:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 508
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "HEART_GRAY"

    const/16 v2, 0x6a

    const-string/jumbo v3, "heartGray"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HEART_GRAY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 511
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "HEARTS"

    const/16 v2, 0x6b

    const-string/jumbo v3, "hearts"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HEARTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 514
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "HEEBIE_JEEBIES"

    const/16 v2, 0x6c

    const-string/jumbo v3, "heebieJeebies"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HEEBIE_JEEBIES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 517
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "HOLLY"

    const/16 v2, 0x6d

    const-string/jumbo v3, "holly"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HOLLY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 520
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "HOUSE_FUNKY"

    const/16 v2, 0x6e

    const-string/jumbo v3, "houseFunky"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HOUSE_FUNKY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 523
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "HYPNOTIC"

    const/16 v2, 0x6f

    const-string/jumbo v3, "hypnotic"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HYPNOTIC:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 526
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "ICE_CREAM_CONES"

    const/16 v2, 0x70

    const-string/jumbo v3, "iceCreamCones"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ICE_CREAM_CONES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 529
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "LIGHT_BULB"

    const/16 v2, 0x71

    const-string/jumbo v3, "lightBulb"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->LIGHT_BULB:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 532
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "LIGHTNING1"

    const/16 v2, 0x72

    const-string/jumbo v3, "lightning1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->LIGHTNING1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 535
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "LIGHTNING2"

    const/16 v2, 0x73

    const-string/jumbo v3, "lightning2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->LIGHTNING2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 538
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "MAP_PINS"

    const/16 v2, 0x74

    const-string/jumbo v3, "mapPins"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MAP_PINS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 541
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "MAPLE_LEAF"

    const/16 v2, 0x75

    const-string/jumbo v3, "mapleLeaf"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MAPLE_LEAF:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 544
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "MAPLE_MUFFINS"

    const/16 v2, 0x76

    const-string/jumbo v3, "mapleMuffins"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MAPLE_MUFFINS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 547
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "MARQUEE"

    const/16 v2, 0x77

    const-string/jumbo v3, "marquee"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MARQUEE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 550
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "MARQUEE_TOOTHED"

    const/16 v2, 0x78

    const-string/jumbo v3, "marqueeToothed"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MARQUEE_TOOTHED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 553
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "MOONS"

    const/16 v2, 0x79

    const-string/jumbo v3, "moons"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MOONS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 556
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "MOSAIC"

    const/16 v2, 0x7a

    const-string/jumbo v3, "mosaic"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MOSAIC:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 559
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "MUSIC_NOTES"

    const/16 v2, 0x7b

    const-string/jumbo v3, "musicNotes"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MUSIC_NOTES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 562
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "NORTHWEST"

    const/16 v2, 0x7c

    const-string/jumbo v3, "northwest"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NORTHWEST:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 565
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "OVALS"

    const/16 v2, 0x7d

    const-string/jumbo v3, "ovals"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->OVALS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 568
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PACKAGES"

    const/16 v2, 0x7e

    const-string/jumbo v3, "packages"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PACKAGES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 571
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PALMS_BLACK"

    const/16 v2, 0x7f

    const-string/jumbo v3, "palmsBlack"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PALMS_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 574
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PALMS_COLOR"

    const/16 v2, 0x80

    const-string/jumbo v3, "palmsColor"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PALMS_COLOR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 577
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PAPER_CLIPS"

    const/16 v2, 0x81

    const-string/jumbo v3, "paperClips"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PAPER_CLIPS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 580
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PAPYRUS"

    const/16 v2, 0x82

    const-string/jumbo v3, "papyrus"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PAPYRUS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 583
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PARTY_FAVOR"

    const/16 v2, 0x83

    const-string/jumbo v3, "partyFavor"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PARTY_FAVOR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 586
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PARTY_GLASS"

    const/16 v2, 0x84

    const-string/jumbo v3, "partyGlass"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PARTY_GLASS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 589
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PENCILS"

    const/16 v2, 0x85

    const-string/jumbo v3, "pencils"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PENCILS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 592
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PEOPLE"

    const/16 v2, 0x86

    const-string/jumbo v3, "people"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PEOPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 595
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PEOPLE_WAVING"

    const/16 v2, 0x87

    const-string/jumbo v3, "peopleWaving"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PEOPLE_WAVING:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 598
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PEOPLE_HATS"

    const/16 v2, 0x88

    const-string/jumbo v3, "peopleHats"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PEOPLE_HATS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 601
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "POINSETTIAS"

    const/16 v2, 0x89

    const-string/jumbo v3, "poinsettias"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->POINSETTIAS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 604
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "POSTAGE_STAMP"

    const/16 v2, 0x8a

    const-string/jumbo v3, "postageStamp"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->POSTAGE_STAMP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 607
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PUMPKIN1"

    const/16 v2, 0x8b

    const-string/jumbo v3, "pumpkin1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PUMPKIN1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 610
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PUSH_PIN_NOTE2"

    const/16 v2, 0x8c

    const-string/jumbo v3, "pushPinNote2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PUSH_PIN_NOTE2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 613
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PUSH_PIN_NOTE1"

    const/16 v2, 0x8d

    const-string/jumbo v3, "pushPinNote1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PUSH_PIN_NOTE1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 616
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PYRAMIDS"

    const/16 v2, 0x8e

    const-string/jumbo v3, "pyramids"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PYRAMIDS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 619
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "PYRAMIDS_ABOVE"

    const/16 v2, 0x8f

    const-string/jumbo v3, "pyramidsAbove"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PYRAMIDS_ABOVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 622
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "QUADRANTS"

    const/16 v2, 0x90

    const-string/jumbo v3, "quadrants"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->QUADRANTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 625
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "RINGS"

    const/16 v2, 0x91

    const-string/jumbo v3, "rings"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->RINGS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 628
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SAFARI"

    const/16 v2, 0x92

    const-string/jumbo v3, "safari"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SAFARI:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 631
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SAWTOOTH"

    const/16 v2, 0x93

    const-string/jumbo v3, "sawtooth"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SAWTOOTH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 634
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SAWTOOTH_GRAY"

    const/16 v2, 0x94

    const-string/jumbo v3, "sawtoothGray"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SAWTOOTH_GRAY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 637
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SCARED_CAT"

    const/16 v2, 0x95

    const-string/jumbo v3, "scaredCat"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SCARED_CAT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 640
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SEATTLE"

    const/16 v2, 0x96

    const-string/jumbo v3, "seattle"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SEATTLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 643
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SHADOWED_SQUARES"

    const/16 v2, 0x97

    const-string/jumbo v3, "shadowedSquares"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SHADOWED_SQUARES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 646
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SHARKS_TEETH"

    const/16 v2, 0x98

    const-string/jumbo v3, "sharksTeeth"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SHARKS_TEETH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 649
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SHOREBIRD_TRACKS"

    const/16 v2, 0x99

    const-string/jumbo v3, "shorebirdTracks"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SHOREBIRD_TRACKS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 652
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SKYROCKET"

    const/16 v2, 0x9a

    const-string/jumbo v3, "skyrocket"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SKYROCKET:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 655
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SNOWFLAKE_FANCY"

    const/16 v2, 0x9b

    const-string/jumbo v3, "snowflakeFancy"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SNOWFLAKE_FANCY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 658
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SNOWFLAKES"

    const/16 v2, 0x9c

    const-string/jumbo v3, "snowflakes"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SNOWFLAKES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 661
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SOMBRERO"

    const/16 v2, 0x9d

    const-string/jumbo v3, "sombrero"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SOMBRERO:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 664
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SOUTHWEST"

    const/16 v2, 0x9e

    const-string/jumbo v3, "southwest"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SOUTHWEST:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 667
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "STARS"

    const/16 v2, 0x9f

    const-string/jumbo v3, "stars"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->STARS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 670
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "STARS_TOP"

    const/16 v2, 0xa0

    const-string/jumbo v3, "starsTop"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->STARS_TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 673
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "STARS3D"

    const/16 v2, 0xa1

    const-string/jumbo v3, "stars3d"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->STARS3D:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 676
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "STARS_BLACK"

    const/16 v2, 0xa2

    const-string/jumbo v3, "starsBlack"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->STARS_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 679
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "STARS_SHADOWED"

    const/16 v2, 0xa3

    const-string/jumbo v3, "starsShadowed"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->STARS_SHADOWED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 682
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SUN"

    const/16 v2, 0xa4

    const-string/jumbo v3, "sun"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SUN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 685
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "SWIRLIGIG"

    const/16 v2, 0xa5

    const-string/jumbo v3, "swirligig"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SWIRLIGIG:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 688
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "TORN_PAPER"

    const/16 v2, 0xa6

    const-string/jumbo v3, "tornPaper"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TORN_PAPER:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 691
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "TORN_PAPER_BLACK"

    const/16 v2, 0xa7

    const-string/jumbo v3, "tornPaperBlack"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TORN_PAPER_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 694
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "TREES"

    const/16 v2, 0xa8

    const-string/jumbo v3, "trees"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TREES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 697
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "TRIANGLE_PARTY"

    const/16 v2, 0xa9

    const-string/jumbo v3, "triangleParty"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIANGLE_PARTY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 700
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "TRIANGLES"

    const/16 v2, 0xaa

    const-string/jumbo v3, "triangles"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 703
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "TRIBAL1"

    const/16 v2, 0xab

    const-string/jumbo v3, "tribal1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIBAL1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 706
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "TRIBAL2"

    const/16 v2, 0xac

    const-string/jumbo v3, "tribal2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIBAL2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 709
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "TRIBAL3"

    const/16 v2, 0xad

    const-string/jumbo v3, "tribal3"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIBAL3:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 712
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "TRIBAL4"

    const/16 v2, 0xae

    const-string/jumbo v3, "tribal4"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIBAL4:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 715
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "TRIBAL5"

    const/16 v2, 0xaf

    const-string/jumbo v3, "tribal5"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIBAL5:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 718
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "TRIBAL6"

    const/16 v2, 0xb0

    const-string/jumbo v3, "tribal6"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIBAL6:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 721
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "TWISTED_LINES1"

    const/16 v2, 0xb1

    const-string/jumbo v3, "twistedLines1"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TWISTED_LINES1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 724
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "TWISTED_LINES2"

    const/16 v2, 0xb2

    const-string/jumbo v3, "twistedLines2"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TWISTED_LINES2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 727
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "VINE"

    const/16 v2, 0xb3

    const-string/jumbo v3, "vine"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->VINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 730
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "WAVELINE"

    const/16 v2, 0xb4

    const-string/jumbo v3, "waveline"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WAVELINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 733
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "WEAVING_ANGLES"

    const/16 v2, 0xb5

    const-string/jumbo v3, "weavingAngles"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WEAVING_ANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 736
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "WEAVING_BRAID"

    const/16 v2, 0xb6

    const-string/jumbo v3, "weavingBraid"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WEAVING_BRAID:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 739
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "WEAVING_RIBBON"

    const/16 v2, 0xb7

    const-string/jumbo v3, "weavingRibbon"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WEAVING_RIBBON:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 742
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "WEAVING_STRIPS"

    const/16 v2, 0xb8

    const-string/jumbo v3, "weavingStrips"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WEAVING_STRIPS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 745
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "WHITE_FLOWERS"

    const/16 v2, 0xb9

    const-string/jumbo v3, "whiteFlowers"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WHITE_FLOWERS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 748
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "WOODWORK"

    const/16 v2, 0xba

    const-string/jumbo v3, "woodwork"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WOODWORK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 751
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "X_ILLUSIONS"

    const/16 v2, 0xbb

    const-string/jumbo v3, "xIllusions"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->X_ILLUSIONS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 754
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "ZANY_TRIANGLES"

    const/16 v2, 0xbc

    const-string/jumbo v3, "zanyTriangles"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ZANY_TRIANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 757
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "ZIG_ZAG"

    const/16 v2, 0xbd

    const-string/jumbo v3, "zigZag"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ZIG_ZAG:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 760
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "ZIG_ZAG_STITCH"

    const/16 v2, 0xbe

    const-string/jumbo v3, "zigZagStitch"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ZIG_ZAG_STITCH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 763
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DASH_DOT"

    const/16 v2, 0xbf

    const-string/jumbo v3, "dashDot"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASH_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "DASH_DOT_DOT"

    const/16 v2, 0xc0

    const-string/jumbo v3, "dashDotDot"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASH_DOT_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "THIN"

    const/16 v2, 0xc1

    const-string/jumbo v3, "thin"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "MEDIUM"

    const/16 v2, 0xc2

    const-string/jumbo v3, "Medium"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MEDIUM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 764
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "MEDIUM_DASHED"

    const/16 v2, 0xc3

    const-string/jumbo v3, "mediumDashed"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MEDIUM_DASHED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "MEDIUM_DASH_DOT"

    const/16 v2, 0xc4

    const-string/jumbo v3, "mediumDashDot"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MEDIUM_DASH_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 765
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const-string/jumbo v1, "MEDIUM_DASH_DOT_DOT"

    const/16 v2, 0xc5

    const-string/jumbo v3, "mediumDashDotDot"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MEDIUM_DASH_DOT_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 181
    const/16 v0, 0xc6

    new-array v0, v0, [Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NIL:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOTTED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASHED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOT_DASH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOT_DOT_DASH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE_WAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASH_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASH_DOT_STROKED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THREE_D_EMBOSS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THREE_D_ENGRAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->OUTSET:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->INSET:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->APPLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ARCHED_SCALLOPS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BABY_PACIFIER:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BABY_RATTLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BALLOONS3_COLORS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BALLOONS_HOT_AIR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_BLACK_DASHES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_BLACK_DOTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_BLACK_SQUARES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_THIN_LINES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_WHITE_DASHES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_WHITE_DOTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_WHITE_SQUARES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_WIDE_INLINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_WIDE_MIDLINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BASIC_WIDE_OUTLINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BATS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BIRDS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->BIRDS_FLIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CABINS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CAKE_SLICE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CANDY_CORN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CELTIC_KNOTWORK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CERTIFICATE_BANNER:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CHAIN_LINK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CHAMPAGNE_BOTTLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CHECKED_BAR_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CHECKED_BAR_COLOR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CHECKERED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CHRISTMAS_TREE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CIRCLES_LINES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CIRCLES_RECTANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CLASSICAL_WAVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CLOCKS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->COMPASS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CONFETTI:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CONFETTI_GRAYS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CONFETTI_OUTLINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CONFETTI_STREAMERS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CONFETTI_WHITE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CORNER_TRIANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->COUPON_CUTOUT_DASHES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->COUPON_CUTOUT_DOTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CRAZY_MAZE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CREATURES_BUTTERFLY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CREATURES_FISH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CREATURES_INSECTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CREATURES_LADY_BUG:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CROSS_STITCH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->CUP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DECO_ARCH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DECO_ARCH_COLOR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DECO_BLOCKS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DIAMONDS_GRAY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE_D:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE_DIAMONDS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->EARTH1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->EARTH2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ECLIPSING_SQUARES1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ECLIPSING_SQUARES2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->EGGS_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FANS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FILM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FIRECRACKERS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_BLOCK_PRINT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_DAISIES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_MODERN1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_MODERN2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_PANSY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_RED_ROSE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_ROSES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_TEACUP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->FLOWERS_TINY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->GEMS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->GINGERBREAD_MAN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->GRADIENT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HANDMADE1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HANDMADE2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HEART_BALLOON:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HEART_GRAY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HEARTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HEEBIE_JEEBIES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HOLLY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HOUSE_FUNKY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->HYPNOTIC:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ICE_CREAM_CONES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->LIGHT_BULB:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->LIGHTNING1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->LIGHTNING2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MAP_PINS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MAPLE_LEAF:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MAPLE_MUFFINS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MARQUEE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MARQUEE_TOOTHED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MOONS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MOSAIC:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MUSIC_NOTES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NORTHWEST:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->OVALS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PACKAGES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PALMS_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PALMS_COLOR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PAPER_CLIPS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PAPYRUS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PARTY_FAVOR:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PARTY_GLASS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PENCILS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PEOPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PEOPLE_WAVING:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PEOPLE_HATS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x89

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->POINSETTIAS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->POSTAGE_STAMP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PUMPKIN1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PUSH_PIN_NOTE2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PUSH_PIN_NOTE1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PYRAMIDS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->PYRAMIDS_ABOVE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x90

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->QUADRANTS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x91

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->RINGS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x92

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SAFARI:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x93

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SAWTOOTH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x94

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SAWTOOTH_GRAY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x95

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SCARED_CAT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x96

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SEATTLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x97

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SHADOWED_SQUARES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x98

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SHARKS_TEETH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x99

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SHOREBIRD_TRACKS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SKYROCKET:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SNOWFLAKE_FANCY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SNOWFLAKES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SOMBRERO:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SOUTHWEST:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->STARS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->STARS_TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->STARS3D:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->STARS_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->STARS_SHADOWED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SUN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SWIRLIGIG:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TORN_PAPER:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TORN_PAPER_BLACK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TREES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIANGLE_PARTY:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xab

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIBAL1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xac

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIBAL2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xad

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIBAL3:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xae

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIBAL4:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIBAL5:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIBAL6:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TWISTED_LINES1:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TWISTED_LINES2:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->VINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WAVELINE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WEAVING_ANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WEAVING_BRAID:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WEAVING_RIBBON:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WEAVING_STRIPS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WHITE_FLOWERS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xba

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->WOODWORK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->X_ILLUSIONS:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ZANY_TRIANGLES:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ZIG_ZAG:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ZIG_ZAG_STITCH:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASH_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASH_DOT_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MEDIUM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MEDIUM_DASHED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MEDIUM_DASH_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MEDIUM_DASH_DOT_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 769
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 770
    iput-object p3, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->value:Ljava/lang/String;

    .line 771
    return-void
.end method

.method public static convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .locals 5
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 778
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->values()[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 779
    .local v2, "inst":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->xmlValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 783
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    :goto_1
    return-object v2

    .line 778
    .restart local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 783
    .end local v2    # "inst":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 181
    const-class v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .locals 1

    .prologue
    .line 181
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->$VALUES:[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    return-object v0
.end method


# virtual methods
.method public xmlValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 774
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->value:Ljava/lang/String;

    return-object v0
.end method
