.class public Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;
.super Ljava/lang/Object;
.source "EMR_MoveToEx.java"


# instance fields
.field private Size:I

.field private Type:I

.field private curX:I

.field private curY:I

.field fileContent:[B

.field private offset:I


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->fileContent:[B

    .line 21
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->offset:I

    .line 22
    return-void
.end method


# virtual methods
.method public getRecordSize()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->Size:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->Type:I

    return v0
.end method

.method public getX()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->curX:I

    return v0
.end method

.method public getY()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->curY:I

    return v0
.end method

.method public readEmfMoveToEx(Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 11
    .param p1, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x0

    .line 62
    const/4 v4, 0x0

    .line 64
    .local v4, "i":I
    const/4 v6, 0x4

    new-array v5, v6, [B

    .line 66
    .local v5, "intConvert":[B
    const/4 v4, 0x0

    :goto_0
    if-gt v4, v10, :cond_0

    .line 67
    rsub-int/lit8 v6, v4, 0x3

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->fileContent:[B

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->offset:I

    add-int/2addr v8, v4

    aget-byte v7, v7, v8

    aput-byte v7, v5, v6

    .line 66
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 68
    :cond_0
    invoke-static {v5, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 69
    .local v1, "Type":I
    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->offset:I

    add-int/2addr v6, v4

    iput v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->offset:I

    .line 70
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->setRecordType(I)V

    .line 72
    const/4 v4, 0x0

    :goto_1
    if-gt v4, v10, :cond_1

    .line 73
    rsub-int/lit8 v6, v4, 0x3

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->fileContent:[B

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->offset:I

    add-int/2addr v8, v4

    aget-byte v7, v7, v8

    aput-byte v7, v5, v6

    .line 72
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 74
    :cond_1
    invoke-static {v5, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 75
    .local v0, "Size":I
    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->offset:I

    add-int/2addr v6, v4

    iput v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->offset:I

    .line 76
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->setRecordSize(I)V

    .line 78
    const/4 v4, 0x0

    :goto_2
    if-gt v4, v10, :cond_2

    .line 79
    rsub-int/lit8 v6, v4, 0x3

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->fileContent:[B

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->offset:I

    add-int/2addr v8, v4

    aget-byte v7, v7, v8

    aput-byte v7, v5, v6

    .line 78
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 80
    :cond_2
    invoke-static {v5, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 81
    .local v2, "X":I
    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->offset:I

    add-int/2addr v6, v4

    iput v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->offset:I

    .line 82
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->setX(I)V

    .line 84
    const/4 v4, 0x0

    :goto_3
    if-gt v4, v10, :cond_3

    .line 85
    rsub-int/lit8 v6, v4, 0x3

    iget-object v7, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->fileContent:[B

    iget v8, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->offset:I

    add-int/2addr v8, v4

    aget-byte v7, v7, v8

    aput-byte v7, v5, v6

    .line 84
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 86
    :cond_3
    invoke-static {v5, v9}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v3

    .line 87
    .local v3, "Y":I
    iget v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->offset:I

    add-int/2addr v6, v4

    iput v6, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->offset:I

    .line 88
    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->setY(I)V

    .line 89
    int-to-float v6, v2

    int-to-float v7, v3

    invoke-virtual {p0, v6, v7, p1}, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->setCurrentXY(FFLcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V

    .line 91
    return-void
.end method

.method public setCurrentXY(FFLcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;)V
    .locals 1
    .param p1, "currX"    # F
    .param p2, "currY"    # F
    .param p3, "emrGlobals"    # Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;

    .prologue
    .line 57
    iget v0, p3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->xScale:F

    div-float v0, p1, v0

    iput v0, p3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentX:F

    .line 58
    iget v0, p3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->yScale:F

    div-float v0, p2, v0

    iput v0, p3, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EMR_Globals;->currentY:F

    .line 59
    return-void
.end method

.method public setRecordSize(I)V
    .locals 0
    .param p1, "RecrdSize"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->Size:I

    .line 38
    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1, "RecrdType"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->Type:I

    .line 30
    return-void
.end method

.method public setX(I)V
    .locals 0
    .param p1, "X"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->curX:I

    .line 46
    return-void
.end method

.method public setY(I)V
    .locals 0
    .param p1, "Y"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/EmfStateRecords/EMR_MoveToEx;->curY:I

    .line 54
    return-void
.end method
