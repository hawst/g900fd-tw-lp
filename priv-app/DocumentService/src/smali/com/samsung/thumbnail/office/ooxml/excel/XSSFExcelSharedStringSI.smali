.class public Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;
.super Ljava/lang/Object;
.source "XSSFExcelSharedStringSI.java"


# instance fields
.field private checkRprStyles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private extextboldval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextfontname:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextfontsize:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextitalicval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextrgbcolorval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextstrikeval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextthemeval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extexttintval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extextunderlineval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private setrprstyle:Ljava/lang/String;

.field private text:Ljava/lang/String;

.field private textverticalalign:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextrgbcolorval:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextthemeval:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extexttintval:Ljava/util/ArrayList;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextitalicval:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextboldval:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextunderlineval:Ljava/util/ArrayList;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextstrikeval:Ljava/util/ArrayList;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextfontname:Ljava/util/ArrayList;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextfontsize:Ljava/util/ArrayList;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->textverticalalign:Ljava/util/ArrayList;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->checkRprStyles:Ljava/util/ArrayList;

    .line 56
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->setrprstyle:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addExSharedStringStyleBold(Ljava/lang/String;I)V
    .locals 1
    .param p1, "extext"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextboldval:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 123
    return-void
.end method

.method public addExSharedStringStyleFontSize(Ljava/lang/String;I)V
    .locals 1
    .param p1, "sharedstrstyle"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextfontsize:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 159
    return-void
.end method

.method public addExSharedStringStyleFontname(Ljava/lang/String;I)V
    .locals 1
    .param p1, "sharedstrstyle"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextfontname:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 150
    return-void
.end method

.method public addExSharedStringStyleItalic(Ljava/lang/String;I)V
    .locals 1
    .param p1, "extext"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextitalicval:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 113
    return-void
.end method

.method public addExSharedStringStyleStrike(Ljava/lang/String;I)V
    .locals 1
    .param p1, "sharedstrstyle"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextstrikeval:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 141
    return-void
.end method

.method public addExSharedStringStyleUnderline(Ljava/lang/String;I)V
    .locals 1
    .param p1, "sharedstrstyle"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextunderlineval:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 132
    return-void
.end method

.method public addExSharedStringTextColorRGB(Ljava/lang/String;I)V
    .locals 1
    .param p1, "extext"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextrgbcolorval:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 83
    return-void
.end method

.method public addExSharedStringTextColorTheme(Ljava/lang/String;I)V
    .locals 1
    .param p1, "extext"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextthemeval:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 93
    return-void
.end method

.method public addExSharedStringTextColorTint(Ljava/lang/String;I)V
    .locals 1
    .param p1, "extext"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extexttintval:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 103
    return-void
.end method

.method public addRprStyles(Ljava/lang/Boolean;I)V
    .locals 1
    .param p1, "check"    # Ljava/lang/Boolean;
    .param p2, "index"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->checkRprStyles:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 73
    return-void
.end method

.method public addStyleTextVerticalAlign(Ljava/lang/String;I)V
    .locals 1
    .param p1, "extextstrike"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->textverticalalign:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 178
    return-void
.end method

.method public getExSharedStringStyleBold()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextboldval:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExSharedStringStyleFontSize()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextfontsize:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExSharedStringStyleFontname()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextfontname:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExSharedStringStyleItalic()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextitalicval:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExSharedStringStyleStrike()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextstrikeval:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExSharedStringStyleUnderline()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextunderlineval:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExSharedStringTextColorRGB()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextrgbcolorval:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExSharedStringTextColorTheme()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extextthemeval:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getExSharedStringTextColorTint()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->extexttintval:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRprStyles()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->checkRprStyles:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getStyleTextVerticalAlign()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->textverticalalign:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->text:Ljava/lang/String;

    return-object v0
.end method

.method public getsharedstringstlye()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->setrprstyle:Ljava/lang/String;

    return-object v0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1, "inText"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->text:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public setsharedstringstlye(Ljava/lang/String;)V
    .locals 0
    .param p1, "setrprstyle"    # Ljava/lang/String;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->setrprstyle:Ljava/lang/String;

    .line 169
    return-void
.end method
