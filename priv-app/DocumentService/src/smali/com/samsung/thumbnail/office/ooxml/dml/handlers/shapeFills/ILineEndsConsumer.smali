.class public interface abstract Lcom/samsung/thumbnail/office/ooxml/dml/handlers/shapeFills/ILineEndsConsumer;
.super Ljava/lang/Object;
.source "ILineEndsConsumer.java"


# static fields
.field public static final ARROWSIZEHIGH:I = 0xc

.field public static final ARROWSIZELOW:I = 0x6

.field public static final ARROWSIZEMEDIUM:I = 0x9

.field public static final ARROWTYPECIRCLE:I = 0x4

.field public static final ARROWTYPEDIAMOND:I = 0x3

.field public static final ARROWTYPENARROW:I = 0x5

.field public static final ARROWTYPENOARROW:I = 0x0

.field public static final ARROWTYPESLANTING:I = 0x2

.field public static final ARROWTYPESOLID:I = 0x1


# virtual methods
.method public abstract consumeLineHeadEnd(III)V
.end method

.method public abstract consumeLineTailEnd(III)V
.end method
