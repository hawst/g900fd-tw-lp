.class public Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;
.super Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;
.source "EMR_CreateBrushIndirect.java"


# instance fields
.field private BrushHatch:I

.field private BrushStyle:I

.field private Color:I

.field private Hatch:I

.field private Size:I

.field private Type:I

.field fileContent:[B

.field private ihBrush:I

.field private offset:I


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "filecontent"    # [B
    .param p2, "Offset"    # I

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/emf/EmfGlobals/EmrObject;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->fileContent:[B

    .line 19
    iput p2, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    .line 20
    return-void
.end method


# virtual methods
.method public getBrushHatch()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->BrushHatch:I

    return v0
.end method

.method public getBrushStyle()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->BrushStyle:I

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->Color:I

    return v0
.end method

.method public getHatch()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->Hatch:I

    return v0
.end method

.method public getIhBrush()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->ihBrush:I

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->Size:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->Type:I

    return v0
.end method

.method public readEmfCreateBrushIndirect()V
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x0

    .line 79
    const/4 v6, 0x0

    .line 81
    .local v6, "i":I
    const/4 v9, 0x4

    new-array v8, v9, [B

    .line 83
    .local v8, "intConvert":[B
    const/4 v6, 0x0

    :goto_0
    if-gt v6, v13, :cond_0

    .line 84
    rsub-int/lit8 v9, v6, 0x3

    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->fileContent:[B

    iget v11, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    add-int/2addr v11, v6

    aget-byte v10, v10, v11

    aput-byte v10, v8, v9

    .line 83
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 85
    :cond_0
    invoke-static {v8, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v5

    .line 86
    .local v5, "Type":I
    iget v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    add-int/2addr v9, v6

    iput v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    .line 87
    invoke-virtual {p0, v5}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->setType(I)V

    .line 88
    invoke-virtual {p0, v5}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->setObjectType(I)V

    .line 90
    const/4 v6, 0x0

    :goto_1
    if-gt v6, v13, :cond_1

    .line 91
    rsub-int/lit8 v9, v6, 0x3

    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->fileContent:[B

    iget v11, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    add-int/2addr v11, v6

    aget-byte v10, v10, v11

    aput-byte v10, v8, v9

    .line 90
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 92
    :cond_1
    invoke-static {v8, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v4

    .line 93
    .local v4, "Size":I
    iget v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    add-int/2addr v9, v6

    iput v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    .line 94
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->setSize(I)V

    .line 96
    const/4 v6, 0x0

    :goto_2
    if-gt v6, v13, :cond_2

    .line 97
    rsub-int/lit8 v9, v6, 0x3

    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->fileContent:[B

    iget v11, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    add-int/2addr v11, v6

    aget-byte v10, v10, v11

    aput-byte v10, v8, v9

    .line 96
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 98
    :cond_2
    invoke-static {v8, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v7

    .line 99
    .local v7, "ihBrush":I
    iget v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    add-int/2addr v9, v6

    iput v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    .line 100
    invoke-virtual {p0, v7}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->setIhBrush(I)V

    .line 101
    invoke-virtual {p0, v7}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->sethandleNumber(I)V

    .line 103
    const/4 v6, 0x0

    :goto_3
    if-gt v6, v13, :cond_3

    .line 104
    rsub-int/lit8 v9, v6, 0x3

    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->fileContent:[B

    iget v11, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    add-int/2addr v11, v6

    aget-byte v10, v10, v11

    aput-byte v10, v8, v9

    .line 103
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 105
    :cond_3
    invoke-static {v8, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v1

    .line 106
    .local v1, "BrushStyle":I
    iget v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    add-int/2addr v9, v6

    iput v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    .line 107
    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->setBrushStyle(I)V

    .line 109
    const/4 v6, 0x0

    :goto_4
    const/4 v9, 0x2

    if-gt v6, v9, :cond_4

    .line 110
    add-int/lit8 v9, v6, 0x1

    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->fileContent:[B

    iget v11, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    add-int/2addr v11, v6

    aget-byte v10, v10, v11

    aput-byte v10, v8, v9

    .line 109
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 111
    :cond_4
    aput-byte v12, v8, v12

    .line 112
    invoke-static {v8, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v2

    .line 113
    .local v2, "Color":I
    add-int/lit8 v6, v6, 0x1

    .line 114
    iget v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    add-int/2addr v9, v6

    iput v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    .line 115
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->setColor(I)V

    .line 117
    const/4 v6, 0x0

    :goto_5
    if-gt v6, v13, :cond_5

    .line 118
    rsub-int/lit8 v9, v6, 0x3

    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->fileContent:[B

    iget v11, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    add-int/2addr v11, v6

    aget-byte v10, v10, v11

    aput-byte v10, v8, v9

    .line 117
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 119
    :cond_5
    invoke-static {v8, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v3

    .line 120
    .local v3, "Hatch":I
    iget v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    add-int/2addr v9, v6

    iput v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    .line 121
    invoke-virtual {p0, v3}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->setHatch(I)V

    .line 123
    const/4 v6, 0x0

    :goto_6
    if-gt v6, v13, :cond_6

    .line 124
    rsub-int/lit8 v9, v6, 0x3

    iget-object v10, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->fileContent:[B

    iget v11, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    add-int/2addr v11, v6

    aget-byte v10, v10, v11

    aput-byte v10, v8, v9

    .line 123
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 125
    :cond_6
    invoke-static {v8, v12}, Lcom/samsung/thumbnail/office/emf/Util/Utils;->getInt([BI)I

    move-result v0

    .line 126
    .local v0, "BrushHatch":I
    iget v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    add-int/2addr v9, v6

    iput v9, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->offset:I

    .line 127
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->setBrushHatch(I)V

    .line 128
    return-void
.end method

.method public setBrushHatch(I)V
    .locals 0
    .param p1, "brushHatch"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->BrushHatch:I

    .line 76
    return-void
.end method

.method public setBrushStyle(I)V
    .locals 0
    .param p1, "brushStyle"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->BrushStyle:I

    .line 52
    return-void
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->Color:I

    .line 60
    return-void
.end method

.method public setHatch(I)V
    .locals 0
    .param p1, "hatch"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->Hatch:I

    .line 68
    return-void
.end method

.method public setIhBrush(I)V
    .locals 0
    .param p1, "ihBrush"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->ihBrush:I

    .line 44
    return-void
.end method

.method public setSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->Size:I

    .line 36
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/samsung/thumbnail/office/emf/ObjectCreationRecords/EMR_CreateBrushIndirect;->Type:I

    .line 28
    return-void
.end method
