.class public Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "ThemeHandler.java"


# instance fields
.field private theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 20
    const-string/jumbo v3, "theme"

    invoke-direct {p0, v3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(Ljava/lang/String;)V

    .line 22
    const/4 v3, 0x1

    new-array v0, v3, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 24
    .local v0, "seqDescriptor":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeElementsHandler;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeElementsHandler;-><init>()V

    .line 25
    .local v1, "themeElementsHandler":Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeElementsHandler;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;

    const-string/jumbo v3, "themeElements"

    invoke-direct {v2, v3, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 27
    .local v2, "themeElementsSeq":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/DMLSeqDescriptor;
    const/4 v3, 0x0

    aput-object v2, v0, v3

    .line 29
    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 30
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 35
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 37
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    iput-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeHandler;->theme:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    const-string/jumbo v1, "name"

    invoke-virtual {p0, p3, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/ThemeHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->setThemeName(Ljava/lang/String;)V

    .line 44
    :cond_1
    return-void
.end method
