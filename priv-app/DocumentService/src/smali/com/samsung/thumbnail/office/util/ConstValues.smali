.class public Lcom/samsung/thumbnail/office/util/ConstValues;
.super Ljava/lang/Object;
.source "ConstValues.java"


# static fields
.field public static final BOTTOM:Ljava/lang/String; = "bottom"

.field public static final CENTER:Ljava/lang/String; = "center"

.field public static final GREATER_THAN:Ljava/lang/String; = ">"

.field public static final GREATER_THAN_HTML:Ljava/lang/String; = "&gt;"

.field public static final LEFT:Ljava/lang/String; = "left"

.field public static final LESS_THAN:Ljava/lang/String; = "<"

.field public static final LESS_THAN_HTML:Ljava/lang/String; = "&lt;"

.field public static final NIL:Ljava/lang/String; = "nil"

.field public static final NON_BREAKING_SPACE:Ljava/lang/String; = " "

.field public static final NON_BREAKING_SPACE_HTML:Ljava/lang/String; = "&nbsp;&nbsp;&nbsp;"

.field public static final RIGHT:Ljava/lang/String; = "right"

.field public static final STRING:Ljava/lang/String; = "string"

.field public static final TOP:Ljava/lang/String; = "top"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
