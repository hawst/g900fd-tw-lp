.class public interface abstract Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;
.super Ljava/lang/Object;
.source "IXDocConsumer.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;


# virtual methods
.method public abstract addBreak(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;)V
.end method

.method public abstract addChart(Ljava/lang/String;)V
.end method

.method public abstract addPTabCharacter(Ljava/lang/String;)V
.end method

.method public abstract addSectPr(Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;)V
.end method

.method public abstract addTabCharacter(Ljava/lang/String;I)V
.end method

.method public abstract endCell()V
.end method

.method public abstract endDocument()V
.end method

.method public abstract endFooter()V
.end method

.method public abstract endHeader()V
.end method

.method public abstract endHyperLink()V
.end method

.method public abstract endPara()V
.end method

.method public abstract endRow()V
.end method

.method public abstract endTable()V
.end method

.method public abstract endText()V
.end method

.method public abstract endTextBox()V
.end method

.method public abstract endTextBoxContents()V
.end method

.method public abstract endTextRun()V
.end method

.method public abstract getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
.end method

.method public abstract lastRenderedPageBreak()V
.end method

.method public abstract setBackgroundColor(Ljava/lang/String;)V
.end method

.method public abstract setCharacterProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
.end method

.method public abstract setDefaultTabStop(Ljava/lang/String;)V
.end method

.method public abstract setDocPartGalleryVal(Ljava/lang/String;)V
.end method

.method public abstract setFldSimple(Ljava/lang/String;)V
.end method

.method public abstract setParaCharProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
.end method

.method public abstract setParagraphProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V
.end method

.method public abstract setTblCellProperty(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V
.end method

.method public abstract setTblGridColWidth(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setTblProperty(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V
.end method

.method public abstract setTblRowProperty(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;)V
.end method

.method public abstract setTxtBxRotParam(Ljava/lang/String;)V
.end method

.method public abstract startCell()V
.end method

.method public abstract startDocument()V
.end method

.method public abstract startHyperLink(Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;)V
.end method

.method public abstract startPara()V
.end method

.method public abstract startRow()V
.end method

.method public abstract startTable()V
.end method

.method public abstract startTextBox()V
.end method

.method public abstract startTextBoxContents()V
.end method

.method public abstract startTextRun()V
.end method

.method public abstract text([CII)V
.end method
