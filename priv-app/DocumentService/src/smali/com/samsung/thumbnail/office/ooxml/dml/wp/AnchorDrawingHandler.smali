.class public Lcom/samsung/thumbnail/office/ooxml/dml/wp/AnchorDrawingHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;
.source "AnchorDrawingHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler$IPositionOffsetObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler$IEffectExtentObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler$IExtentObserver;
.implements Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler$IWrapSquare;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    const/16 v0, 0x1e

    const-string/jumbo v1, "anchor"

    invoke-direct {p0, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;-><init>(ILjava/lang/String;)V

    .line 29
    return-void
.end method

.method private init()V
    .locals 15

    .prologue
    .line 32
    const/4 v13, 0x6

    new-array v10, v13, [Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 34
    .local v10, "seqDescriptors":[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler;

    invoke-direct {v2, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler$IExtentObserver;)V

    .line 35
    .local v2, "extentHandler":Lcom/samsung/thumbnail/office/ooxml/dml/wp/ExtentHandler;
    new-instance v3, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;

    const-string/jumbo v13, "extent"

    invoke-direct {v3, v13, v2}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 37
    .local v3, "extentSeq":Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;
    const/4 v13, 0x0

    aput-object v3, v10, v13

    .line 39
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler;

    invoke-direct {v0, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler$IEffectExtentObserver;)V

    .line 40
    .local v0, "effectExtent":Lcom/samsung/thumbnail/office/ooxml/dml/wp/EffectExtentHandler;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;

    const-string/jumbo v13, "effectExtent"

    invoke-direct {v1, v13, v0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 42
    .local v1, "effectExtentSeq":Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;
    const/4 v13, 0x1

    aput-object v1, v10, v13

    .line 44
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionHHandler;

    invoke-direct {v7, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionHHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler$IPositionOffsetObserver;)V

    .line 45
    .local v7, "posHhandler":Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionHHandler;
    new-instance v6, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;

    const-string/jumbo v13, "positionH"

    invoke-direct {v6, v13, v7}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 47
    .local v6, "posHSeq":Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;
    const/4 v13, 0x2

    aput-object v6, v10, v13

    .line 49
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;

    invoke-direct {v9, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler$IPositionOffsetObserver;)V

    .line 50
    .local v9, "posVhandler":Lcom/samsung/thumbnail/office/ooxml/dml/wp/XDocPositionVHandler;
    new-instance v8, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;

    const-string/jumbo v13, "positionV"

    invoke-direct {v8, v13, v9}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 52
    .local v8, "posVSeq":Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;
    const/4 v13, 0x3

    aput-object v8, v10, v13

    .line 54
    new-instance v5, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicHandler;

    invoke-direct {v5}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicHandler;-><init>()V

    .line 55
    .local v5, "graphicHandler":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/GraphicHandler;
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    const/16 v13, 0x1f

    const-string/jumbo v14, "graphic"

    invoke-direct {v4, v13, v14, v5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;-><init>(ILjava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 58
    .local v4, "graphic":Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;
    const/4 v13, 0x4

    aput-object v4, v10, v13

    .line 60
    new-instance v12, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler;

    invoke-direct {v12, p0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler;-><init>(Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler$IWrapSquare;)V

    .line 61
    .local v12, "wrapSquareHandler":Lcom/samsung/thumbnail/office/ooxml/dml/wp/WrapSquareHandler;
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;

    const-string/jumbo v13, "wrapSquare"

    invoke-direct {v11, v13, v12}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;)V

    .line 63
    .local v11, "wrapSqrEle":Lcom/samsung/thumbnail/office/ooxml/dml/wp/WPDMLSeqDescriptor;
    const/4 v13, 0x5

    aput-object v11, v10, v13

    .line 65
    iput-object v10, p0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/AnchorDrawingHandler;->sequenceDescription:[Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceDescriptor;

    .line 66
    return-void
.end method


# virtual methods
.method public getWrapSquareText(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "wrapSqrTxt"    # Ljava/lang/String;

    .prologue
    .line 112
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setWrapSquareProp(Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 90
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;->endPicture()V

    .line 91
    return-void
.end method

.method public setEffectExtent(IIIILcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "l"    # I
    .param p2, "r"    # I
    .param p3, "b"    # I
    .param p4, "t"    # I
    .param p5, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 95
    invoke-virtual {p5}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setEffectExtent(IIII)V

    .line 96
    return-void
.end method

.method public setExtent(IILcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 1
    .param p1, "cx"    # I
    .param p2, "cy"    # I
    .param p3, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 100
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setPictureExtent(II)V

    .line 101
    return-void
.end method

.method public setOffsetPositionValue(Ljava/lang/String;JLcom/samsung/thumbnail/office/ooxml/OOXMLParser;)V
    .locals 2
    .param p1, "offsetRef"    # Ljava/lang/String;
    .param p2, "offsetVal"    # J
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 106
    invoke-virtual {p4}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->setPictPosOffset(Ljava/lang/String;J)V

    .line 108
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 71
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLSequenceHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 73
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    const-string/jumbo v2, "relativeHeight"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/AnchorDrawingHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;-><init>(Ljava/lang/String;)V

    .line 76
    .local v1, "relativeHeight":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;

    const-string/jumbo v2, "behindDoc"

    invoke-virtual {p0, p3, v2, p1}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/AnchorDrawingHandler;->getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;-><init>(Ljava/lang/String;)V

    .line 78
    .local v0, "behindTheDocState":Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/AnchorDrawingHandler;->init()V

    .line 79
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;

    invoke-interface {v2}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;->startPicture()V

    .line 80
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;

    const-string/jumbo v3, "anchor"

    invoke-interface {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;->setPictureType(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/values/OOXMLStringValue;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/samsung/thumbnail/office/ooxml/dml/wp/IXDocPicConsumer;->setPictAnchorProp(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    return-void
.end method
