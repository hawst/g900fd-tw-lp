.class public Lcom/samsung/thumbnail/office/ooxml/excel/handlers/sheet/XExcelSheetcolHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "XExcelSheetcolHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    const-string/jumbo v0, "col"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 22
    return-void
.end method


# virtual methods
.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 12
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v10, 0x1

    .line 27
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 28
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExcelVariable()Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-result-object v7

    .line 30
    .local v7, "var":Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;
    const-string/jumbo v9, "min"

    invoke-interface {p3, v9}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 31
    .local v5, "min":Ljava/lang/String;
    const-string/jumbo v9, "max"

    invoke-interface {p3, v9}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 32
    .local v4, "max":Ljava/lang/String;
    const-string/jumbo v9, "width"

    invoke-interface {p3, v9}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 33
    .local v8, "width":Ljava/lang/String;
    const-string/jumbo v9, "customWidth"

    invoke-interface {p3, v9}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "customwidth":Ljava/lang/String;
    const-string/jumbo v9, "hidden"

    invoke-interface {p3, v9}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 36
    .local v1, "hidden":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 38
    .local v6, "minCol":I
    iget-boolean v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->customCols:Z

    if-nez v9, :cond_1

    const/4 v9, 0x5

    if-le v6, v9, :cond_1

    .line 39
    const/16 v9, 0x10

    iput v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->maxCol:I

    .line 40
    sget v9, Lcom/samsung/index/parser/ThumbnailDocumentParser;->MAX_EXCEL_TABLE_WIDTH:F

    iput v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tableWidth:F

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    iput-boolean v10, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->customCols:Z

    .line 44
    const/4 v2, 0x0

    .line 45
    .local v2, "hide":Z
    if-eqz v1, :cond_2

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    if-ne v9, v10, :cond_2

    .line 46
    const/4 v2, 0x1

    .line 49
    :cond_2
    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    if-ne v9, v10, :cond_0

    iget v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tableWidth:F

    sget v10, Lcom/samsung/index/parser/ThumbnailDocumentParser;->MAX_EXCEL_TABLE_WIDTH:F

    cmpg-float v9, v9, v10

    if-gez v9, :cond_0

    .line 51
    move v3, v6

    .line 52
    .local v3, "i":I
    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    if-gt v3, v9, :cond_0

    .line 53
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    if-ne v3, v9, :cond_3

    .line 54
    iput v3, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->maxCol:I

    .line 57
    :cond_3
    iget v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tableWidth:F

    sget v10, Lcom/samsung/index/parser/ThumbnailDocumentParser;->MAX_EXCEL_TABLE_WIDTH:F

    cmpl-float v9, v9, v10

    if-ltz v9, :cond_4

    .line 58
    iput v3, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->maxCol:I

    goto :goto_0

    .line 62
    :cond_4
    if-eqz v2, :cond_5

    .line 63
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v9, v3, v10}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetCellWidth(ILjava/lang/Double;)V

    .line 73
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 66
    :cond_5
    iget v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tableWidth:F

    invoke-static {v8}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    const/high16 v11, 0x428c0000    # 70.0f

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    mul-float/2addr v10, v11

    const/high16 v11, 0x41100000    # 9.0f

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    div-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    add-float/2addr v9, v10

    iput v9, v7, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->tableWidth:F

    .line 69
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v9, v3, v10}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->addExWorkSheetCellWidth(ILjava/lang/Double;)V

    goto :goto_2
.end method
