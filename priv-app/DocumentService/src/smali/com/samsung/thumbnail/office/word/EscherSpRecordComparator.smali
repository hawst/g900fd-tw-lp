.class public Lcom/samsung/thumbnail/office/word/EscherSpRecordComparator;
.super Ljava/lang/Object;
.source "EscherSpRecordComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/poi/ddf/EscherSpRecord;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 7
    check-cast p1, Lorg/apache/poi/ddf/EscherSpRecord;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/poi/ddf/EscherSpRecord;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/thumbnail/office/word/EscherSpRecordComparator;->compare(Lorg/apache/poi/ddf/EscherSpRecord;Lorg/apache/poi/ddf/EscherSpRecord;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/poi/ddf/EscherSpRecord;Lorg/apache/poi/ddf/EscherSpRecord;)I
    .locals 2
    .param p1, "obj1"    # Lorg/apache/poi/ddf/EscherSpRecord;
    .param p2, "obj2"    # Lorg/apache/poi/ddf/EscherSpRecord;

    .prologue
    .line 13
    invoke-virtual {p1}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v0

    invoke-virtual {p2}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 14
    const/4 v0, 0x1

    .line 18
    :goto_0
    return v0

    .line 15
    :cond_0
    invoke-virtual {p1}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v0

    invoke-virtual {p2}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 16
    const/4 v0, -0x1

    goto :goto_0

    .line 18
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
