.class public Lcom/samsung/thumbnail/provider/ThumbnailProvider;
.super Landroid/content/ContentProvider;
.source "ThumbnailProvider.java"


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.samsung.docthumbnail.thumbnailprovider"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field private static final FILE_THUMBNAIL:I = 0xa

.field public static final PROJ_CONTENT:Ljava/lang/String; = "document_solution"

.field private static final TAG:Ljava/lang/String; = "ThumbnailProvider"

.field private static final THUMBNAILPATH_TABLE:Ljava/lang/String; = "thumbnailpath"

.field public static final THUMBNAIL_PATH:Ljava/lang/String; = "thumbnail_path"

.field public static isThumbnailOn:Z

.field private static final sURIMatcher:Landroid/content/UriMatcher;


# instance fields
.field private databaseMgr:Lcom/samsung/index/DocumentDatabaseManager;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/thumbnail/provider/ThumbnailProvider;->isThumbnailOn:Z

    .line 34
    const-string/jumbo v0, "content://com.samsung.docthumbnail.thumbnailprovider/thumbnailpath"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/thumbnail/provider/ThumbnailProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 36
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/samsung/thumbnail/provider/ThumbnailProvider;->sURIMatcher:Landroid/content/UriMatcher;

    .line 39
    sget-object v0, Lcom/samsung/thumbnail/provider/ThumbnailProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v1, "com.samsung.docthumbnail.thumbnailprovider"

    const-string/jumbo v2, "thumbnailpath"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 40
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 91
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 104
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 4

    .prologue
    .line 44
    const-string/jumbo v1, "ThumbnailProvider"

    const-string/jumbo v2, "ThumbnailProvider onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/provider/ThumbnailProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/index/DocumentDatabaseManager;->getInstance(Landroid/content/Context;)Lcom/samsung/index/DocumentDatabaseManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/provider/ThumbnailProvider;->databaseMgr:Lcom/samsung/index/DocumentDatabaseManager;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 48
    :catch_0
    move-exception v0

    .line 49
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string/jumbo v1, "ThumbnailProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "SQLiteException :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 58
    sget-object v3, Lcom/samsung/thumbnail/provider/ThumbnailProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 60
    .local v2, "uriType":I
    packed-switch v2, :pswitch_data_0

    .line 86
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 63
    :pswitch_0
    sput-boolean v5, Lcom/samsung/thumbnail/provider/ThumbnailProvider;->isThumbnailOn:Z

    .line 67
    invoke-virtual {p0}, Lcom/samsung/thumbnail/provider/ThumbnailProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/index/Utils;->startServiceFromProvider(Landroid/content/Context;)V

    .line 69
    const/4 v1, 0x0

    .line 71
    .local v1, "thumbnail_path":Ljava/lang/String;
    if-eqz p3, :cond_1

    iget-object v3, p0, Lcom/samsung/thumbnail/provider/ThumbnailProvider;->databaseMgr:Lcom/samsung/index/DocumentDatabaseManager;

    if-eqz v3, :cond_1

    .line 72
    iget-object v3, p0, Lcom/samsung/thumbnail/provider/ThumbnailProvider;->databaseMgr:Lcom/samsung/index/DocumentDatabaseManager;

    invoke-virtual {v3, p3}, Lcom/samsung/index/DocumentDatabaseManager;->getThumbnailPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 75
    :cond_1
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 76
    new-instance v0, Landroid/database/MatrixCursor;

    new-array v3, v7, [Ljava/lang/String;

    const-string/jumbo v4, "thumbnail_path"

    aput-object v4, v3, v6

    const-string/jumbo v4, "document_solution"

    aput-object v4, v3, v5

    invoke-direct {v0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 78
    .local v0, "thumbnailCursor":Landroid/database/MatrixCursor;
    new-array v3, v7, [Ljava/lang/String;

    aput-object v1, v3, v6

    const-string/jumbo v4, "document_solution"

    aput-object v4, v3, v5

    invoke-virtual {v0, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 60
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method
