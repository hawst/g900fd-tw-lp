.class public Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;
.super Ljava/lang/Object;
.source "ThumbnailParserThreadsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;
    }
.end annotation


# static fields
.field public static final OOXMLMAXDOCSIZE:I = 0x500000

.field public static final POIMAXDOCSIZE:I = 0xa00000

.field private static final SLEEPMSONEMPTY:J = 0xaL

.field private static final TAG:Ljava/lang/String; = "ThumbnailParserThreadsManager"


# instance fields
.field private indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

.field private m2003Executor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private m2003FilesToIndex:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private mFilesToIndex:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mIndexThread:Ljava/lang/Thread;

.field private mKeepRunning:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x2

    const/4 v0, 0x1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean v0, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mKeepRunning:Z

    .line 55
    iput-object p1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mContext:Landroid/content/Context;

    .line 56
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move v3, v2

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 59
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move v2, v0

    move v3, v0

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->m2003Executor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 63
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    .line 64
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->m2003FilesToIndex:Ljava/util/concurrent/BlockingQueue;

    .line 66
    new-instance v0, Ljava/lang/Thread;

    const-string/jumbo v1, "ThumbnailThreadsListManager"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mIndexThread:Ljava/lang/Thread;

    .line 67
    iget-object v0, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mIndexThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 68
    return-void
.end method

.method private createThumbnailParserThread(Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;ZLjava/util/concurrent/ThreadPoolExecutor;)Z
    .locals 10
    .param p1, "fileDetails"    # Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;
    .param p2, "isSynchronous"    # Z
    .param p3, "threadPoolExecutor"    # Ljava/util/concurrent/ThreadPoolExecutor;

    .prologue
    const/4 v8, 0x0

    .line 325
    const/4 v7, 0x1

    .line 327
    .local v7, "sucess":Z
    new-instance v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;

    iget-object v1, p1, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    iget-object v2, p1, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;->mFileToIndex:Ljava/io/File;

    iget-object v3, p1, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;->mExtention:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mContext:Landroid/content/Context;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;-><init>(Lcom/samsung/index/IFileIndexedNotifier;Ljava/io/File;Ljava/lang/String;Landroid/content/Context;Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;)V

    .line 332
    .local v0, "tempThread":Lcom/samsung/thumbnail/threads/ThumbnailParserThread;
    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->isFileProcessing(Lcom/samsung/thumbnail/threads/ThumbnailParserThread;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 333
    const/4 v7, 0x0

    .line 350
    .end local v7    # "sucess":Z
    :goto_0
    return v7

    .line 336
    .restart local v7    # "sucess":Z
    :cond_0
    if-eqz p2, :cond_1

    .line 337
    invoke-virtual {v0}, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->run()V

    goto :goto_0

    .line 339
    :cond_1
    invoke-static {v8, v8}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->createInstance(Landroid/content/Context;Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-result-object v6

    .line 343
    .local v6, "indexUtil":Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    if-eqz v6, :cond_2

    .line 344
    invoke-virtual {v6}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->addTotalThumbFileCount()I

    .line 345
    iget-object v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    iget-object v2, p1, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;->mExtention:Ljava/lang/String;

    iget-object v3, p1, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;->mFileToIndex:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v8, 0x400

    div-long/2addr v4, v8

    long-to-int v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->addTotalFilesCount(Ljava/lang/String;I)V

    .line 348
    :cond_2
    invoke-virtual {p3, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private initSingleThreadExecutor()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 71
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v4, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move v3, v2

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->m2003Executor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 74
    return-void
.end method

.method private isFileProcessing(Lcom/samsung/thumbnail/threads/ThumbnailParserThread;)Z
    .locals 4
    .param p1, "parserThread"    # Lcom/samsung/thumbnail/threads/ThumbnailParserThread;

    .prologue
    .line 278
    const/4 v1, 0x0

    .line 280
    .local v1, "isProcessing":Z
    :try_start_0
    iget-object v3, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v3}, Ljava/util/concurrent/ThreadPoolExecutor;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v2

    .line 281
    .local v2, "queue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Ljava/lang/Runnable;>;"
    invoke-interface {v2, p1}, Ljava/util/concurrent/BlockingQueue;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    move v3, v1

    .line 285
    .end local v2    # "queue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Ljava/lang/Runnable;>;"
    :goto_0
    return v3

    .line 282
    :catch_0
    move-exception v0

    .line 283
    .local v0, "ignore":Ljava/util/ConcurrentModificationException;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private iterateDirectory(Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;Ljava/util/concurrent/ThreadPoolExecutor;)Z
    .locals 7
    .param p1, "fileDetails"    # Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;
    .param p2, "threadPoolExecutor"    # Ljava/util/concurrent/ThreadPoolExecutor;

    .prologue
    .line 242
    const/4 v5, 0x0

    .line 245
    .local v5, "success":Z
    :try_start_0
    iget-object v0, p1, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;->mFileToIndex:Ljava/io/File;

    .line 246
    .local v0, "dataDir":Ljava/io/File;
    const/4 v3, 0x0

    .line 247
    .local v3, "files":[Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v6

    if-nez v6, :cond_0

    .line 248
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 250
    :cond_0
    if-nez v3, :cond_1

    .line 251
    const/4 v6, 0x1

    new-array v3, v6, [Ljava/io/File;

    .end local v3    # "files":[Ljava/io/File;
    const/4 v6, 0x0

    aput-object v0, v3, v6

    .line 254
    .restart local v3    # "files":[Ljava/io/File;
    :cond_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v6, v3

    if-ge v4, v6, :cond_3

    .line 255
    aget-object v2, v3, v4

    .line 257
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 258
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->iterateDirectory(Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;Ljava/util/concurrent/ThreadPoolExecutor;)Z

    .line 254
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 260
    :cond_2
    const/4 v6, 0x0

    invoke-direct {p0, p1, v6, p2}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->createThumbnailParserThread(Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;ZLjava/util/concurrent/ThreadPoolExecutor;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    goto :goto_1

    .line 264
    .end local v0    # "dataDir":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "files":[Ljava/io/File;
    .end local v4    # "i":I
    :catch_0
    move-exception v1

    .line 265
    .local v1, "e":Ljava/lang/Exception;
    const/4 v5, 0x0

    .line 268
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    return v5
.end method


# virtual methods
.method public clearQueue()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->clear()V

    .line 88
    return-void
.end method

.method public close()V
    .locals 4

    .prologue
    .line 360
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mKeepRunning:Z

    .line 363
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 364
    const-wide/16 v2, 0xa

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 366
    :catch_0
    move-exception v0

    .line 367
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ERROR: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget-object v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    if-eqz v1, :cond_2

    .line 371
    iget-object v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v1}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    .line 373
    :goto_1
    :try_start_1
    iget-object v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v1}, Ljava/util/concurrent/ThreadPoolExecutor;->isTerminated()Z

    move-result v1

    if-nez v1, :cond_1

    .line 374
    const-wide/16 v2, 0xa

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 376
    :catch_1
    move-exception v0

    .line 377
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ERROR: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 383
    :cond_2
    iget-object v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mIndexThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 384
    return-void
.end method

.method public closeNow()V
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mKeepRunning:Z

    .line 355
    iget-object v0, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    .line 356
    iget-object v0, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mIndexThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 357
    return-void
.end method

.method public getNumberOfFilesInQueue()I
    .locals 2

    .prologue
    .line 77
    const/4 v0, 0x0

    .line 79
    .local v0, "numberoffiles":I
    iget-object v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    if-eqz v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v0

    .line 83
    :cond_0
    return v0
.end method

.method public processThumbnailCreation(Ljava/io/File;Lcom/samsung/index/IFileIndexedNotifier;)Z
    .locals 7
    .param p1, "dataDir"    # Ljava/io/File;
    .param p2, "filenotifier"    # Lcom/samsung/index/IFileIndexedNotifier;

    .prologue
    const/4 v5, 0x0

    .line 92
    const/4 v3, 0x1

    .line 95
    .local v3, "retVal":Z
    new-instance v2, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;

    invoke-direct {v2}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;-><init>()V

    .line 96
    .local v2, "fileDetails":Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;
    iput-object p1, v2, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;->mFileToIndex:Ljava/io/File;

    .line 97
    iput-object p2, v2, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    .line 98
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/index/Utils;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 99
    .local v1, "exten":Ljava/lang/String;
    iput-object v1, v2, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;->mExtention:Ljava/lang/String;

    .line 102
    if-nez v1, :cond_0

    .line 103
    const/4 v4, 0x0

    .line 118
    :goto_0
    return v4

    .line 106
    :cond_0
    invoke-static {v5, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->createInstance(Landroid/content/Context;Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    .line 108
    :try_start_0
    invoke-static {v1}, Lcom/samsung/index/Utils;->checkPDFLOrPOI(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 109
    iget-object v4, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->m2003FilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v4, v2}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    :goto_1
    move v4, v3

    .line 118
    goto :goto_0

    .line 111
    :cond_1
    iget-object v4, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setOtherFilesStarted(Z)V

    .line 112
    iget-object v4, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v4, v2}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string/jumbo v4, "ThumbnailParserThreadsManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "ERROR: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public run()V
    .locals 6

    .prologue
    .line 289
    :goto_0
    iget-boolean v3, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mKeepRunning:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 291
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;

    .line 292
    .local v0, "details":Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;
    if-eqz v0, :cond_2

    .line 293
    iget-object v2, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 295
    .local v2, "executor":Ljava/util/concurrent/ThreadPoolExecutor;
    iget-object v3, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->is2003ParserThreadRunning()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 298
    iget-object v2, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->m2003Executor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 302
    :goto_1
    invoke-direct {p0, v0, v2}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->iterateDirectory(Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;Ljava/util/concurrent/ThreadPoolExecutor;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 309
    .end local v0    # "details":Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;
    .end local v2    # "executor":Ljava/util/concurrent/ThreadPoolExecutor;
    :catch_0
    move-exception v1

    .line 310
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string/jumbo v3, "ThumbnailParserThreadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "InterruptedException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    const-string/jumbo v3, "ThumbnailParserThreadsManager"

    const-string/jumbo v4, "Thumbnail Interrupt in run"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 300
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v0    # "details":Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;
    .restart local v2    # "executor":Ljava/util/concurrent/ThreadPoolExecutor;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    goto :goto_1

    .line 307
    .end local v2    # "executor":Ljava/util/concurrent/ThreadPoolExecutor;
    :cond_2
    const-wide/16 v4, 0x1

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 317
    .end local v0    # "details":Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;
    :cond_3
    return-void
.end method

.method public shutdownThread(Lcom/samsung/index/IFileIndexedNotifier;)V
    .locals 3
    .param p1, "mFilenotifier"    # Lcom/samsung/index/IFileIndexedNotifier;

    .prologue
    .line 214
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v1

    .line 215
    :try_start_0
    iget-object v0, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setRemainingThumbnailFileCount(I)V

    .line 216
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    iget-object v0, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->m2003Executor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    .line 218
    invoke-direct {p0}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->initSingleThreadExecutor()V

    .line 219
    invoke-interface {p1}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailCompleted()V

    .line 220
    return-void

    .line 216
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public startThumbnailFor2003Format()Z
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 128
    iget-object v7, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->m2003FilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v7}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v4

    .line 129
    .local v4, "size":I
    iget-object v7, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v7}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->isOtherFileTypesRunning()Z

    move-result v7

    if-nez v7, :cond_1

    if-lez v4, :cond_1

    .line 131
    const-string/jumbo v7, "ThumbnailParserThreadsManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Processing 2003 Parser for Thumbnail m2003FilesToIndex.... :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v7, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v7, v6}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->set2003FileStartedFlag(Z)V

    .line 139
    const/16 v2, 0x10

    .line 144
    .local v2, "flag":I
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 146
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_2

    .line 149
    :try_start_0
    iget-object v7, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->m2003FilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v7}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;

    .line 150
    .local v0, "details":Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;
    if-eqz v0, :cond_0

    .line 151
    iget-object v7, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v7, v0}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 153
    .end local v0    # "details":Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager$FileDetails;
    :catch_0
    move-exception v1

    .line 154
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string/jumbo v6, "ThumbnailParserThreadsManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "InterruptedException while adding the files to thread queue :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v2    # "flag":I
    .end local v3    # "i":I
    :cond_1
    :goto_1
    return v5

    .line 161
    .restart local v2    # "flag":I
    .restart local v3    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->m2003FilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v5}, Ljava/util/concurrent/BlockingQueue;->clear()V

    move v5, v6

    .line 162
    goto :goto_1
.end method

.method public startThumbnailFor2003FormatIfAny()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 175
    iget-object v4, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->m2003FilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v4}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 179
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v1

    .line 181
    .local v1, "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    const/16 v0, 0x10

    .line 187
    .local v0, "flag":I
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v1

    .line 189
    iget-object v4, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v4, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setOtherFilesStarted(Z)V

    .line 190
    iget-object v3, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v3, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->set2003FileStartedFlag(Z)V

    .line 192
    iget-object v3, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    iget-object v4, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->m2003FilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3, v4}, Ljava/util/concurrent/BlockingQueue;->addAll(Ljava/util/Collection;)Z

    .line 193
    iget-object v3, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->m2003FilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3}, Ljava/util/concurrent/BlockingQueue;->clear()V

    .line 199
    sget-object v3, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_INDEXING_THUMBNAIL_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v1, v3, :cond_0

    .line 200
    iget-object v3, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->startIndexinginParallel()V

    .line 206
    .end local v0    # "flag":I
    .end local v1    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_0
    :goto_0
    return v2

    .line 204
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v2, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setOtherFilesStarted(Z)V

    .line 205
    iget-object v2, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v2, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->set2003FileStartedFlag(Z)V

    move v2, v3

    .line 206
    goto :goto_0
.end method

.method public switchIndexToTwoThread()V
    .locals 5

    .prologue
    .line 226
    iget-object v4, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v4}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->removeTimerHandlerMessage()V

    .line 228
    iget-object v4, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->m2003Executor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v4}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    move-result-object v2

    .line 229
    .local v2, "remainingList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Runnable;>;"
    invoke-direct {p0}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->initSingleThreadExecutor()V

    .line 232
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 233
    .local v1, "listSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 234
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;

    .line 236
    .local v3, "thread":Lcom/samsung/thumbnail/threads/ThumbnailParserThread;
    iget-object v4, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 233
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238
    .end local v3    # "thread":Lcom/samsung/thumbnail/threads/ThumbnailParserThread;
    :cond_0
    return-void
.end method
