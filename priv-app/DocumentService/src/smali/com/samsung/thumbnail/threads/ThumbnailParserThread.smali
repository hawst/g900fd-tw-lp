.class public final Lcom/samsung/thumbnail/threads/ThumbnailParserThread;
.super Ljava/lang/Object;
.source "ThumbnailParserThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static final MINIMUM_FREE_SPACE_REQUIRED:I = 0x1900000

.field private static final TAG:Ljava/lang/String; = "ThumbnailParserThread"


# instance fields
.field private listManager:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

.field private mContext:Landroid/content/Context;

.field private mExtension:Ljava/lang/String;

.field private mFile:Ljava/io/File;

.field private mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

.field private parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

.field private tempFolder:Ljava/io/File;

.field private thumbnailPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/index/IFileIndexedNotifier;Ljava/io/File;Ljava/lang/String;Landroid/content/Context;Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;)V
    .locals 0
    .param p1, "filenotifier"    # Lcom/samsung/index/IFileIndexedNotifier;
    .param p2, "file"    # Ljava/io/File;
    .param p3, "extension"    # Ljava/lang/String;
    .param p4, "context"    # Landroid/content/Context;
    .param p5, "listManager"    # Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p3, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mExtension:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    .line 51
    iput-object p1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    .line 52
    iput-object p4, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mContext:Landroid/content/Context;

    .line 53
    iput-object p5, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->listManager:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    .line 54
    return-void
.end method

.method private deleteTempFiles(Ljava/io/File;)Z
    .locals 8
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 63
    const/4 v5, 0x0

    .line 64
    .local v5, "success":Z
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    move v6, v5

    .line 79
    .end local v5    # "success":Z
    .local v6, "success":I
    :goto_0
    return v6

    .line 67
    .end local v6    # "success":I
    .restart local v5    # "success":Z
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 69
    .local v2, "filesLst":[Ljava/io/File;
    if-nez v2, :cond_2

    .line 70
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v5

    move v6, v5

    .line 71
    .restart local v6    # "success":I
    goto :goto_0

    .line 73
    .end local v6    # "success":I
    :cond_2
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_4

    aget-object v1, v0, v3

    .line 74
    .local v1, "currentFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 75
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v5

    .line 73
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 78
    .end local v1    # "currentFile":Ljava/io/File;
    :cond_4
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v5

    move v6, v5

    .line 79
    .restart local v6    # "success":I
    goto :goto_0
.end method

.method private startThumbnailGenerator()I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 216
    const/4 v2, 0x0

    .line 218
    .local v2, "status":I
    iget-object v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mExtension:Ljava/lang/String;

    .line 219
    .local v1, "extension":Ljava/lang/String;
    new-instance v5, Lcom/samsung/index/parser/ThumbnailDocumentParser;

    invoke-direct {v5}, Lcom/samsung/index/parser/ThumbnailDocumentParser;-><init>()V

    iput-object v5, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    .line 221
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1

    .line 222
    :cond_0
    const-string/jumbo v5, "ThumbnailParserThread"

    const-string/jumbo v6, "File Extension is empty"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    const/4 v2, 0x4

    move v3, v2

    .line 254
    .end local v2    # "status":I
    .local v3, "status":I
    :goto_0
    return v3

    .line 228
    .end local v3    # "status":I
    .restart local v2    # "status":I
    :cond_1
    const-string/jumbo v5, "ThumbnailParserThread"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Thumbnail start File extension : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Size in KB: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v8

    const-wide/16 v10, 0x400

    div-long/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    const-string/jumbo v5, "startThumbnailGenerator"

    invoke-static {v5}, Lcom/samsung/index/indexservice/service/SystemInfo;->checkTemperatureLevel(Ljava/lang/String;)V

    .line 240
    const/4 v4, 0x1

    .line 242
    .local v4, "success":Z
    :try_start_0
    iget-object v5, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    iget-object v6, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    iget-object v7, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6, v7}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->Parse(Ljava/io/File;Landroid/content/Context;)Z

    move-result v4

    .line 243
    if-eqz v4, :cond_2

    .line 244
    iget-object v5, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    invoke-virtual {v5}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->getThumbnailPath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->thumbnailPath:Ljava/lang/String;

    .line 246
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v3, v2

    .line 254
    .end local v2    # "status":I
    .restart local v3    # "status":I
    goto :goto_0

    .line 248
    .end local v3    # "status":I
    .restart local v2    # "status":I
    :cond_2
    const/4 v2, 0x3

    goto :goto_1

    .line 250
    :catch_0
    move-exception v0

    .line 251
    .local v0, "e":Ljava/lang/UnsupportedOperationException;
    const/4 v2, 0x4

    .line 252
    const-string/jumbo v5, "ThumbnailParserThread"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "UnsupportedOperationException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/UnsupportedOperationException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 268
    if-ne p0, p1, :cond_1

    .line 269
    const/4 v3, 0x1

    .line 280
    :cond_0
    :goto_0
    return v3

    .line 270
    :cond_1
    instance-of v4, p1, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;

    if-eqz v4, :cond_0

    move-object v0, p1

    .line 271
    check-cast v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;

    .line 272
    .local v0, "parserThread":Lcom/samsung/thumbnail/threads/ThumbnailParserThread;
    iget-object v4, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 273
    .local v1, "path1":Ljava/lang/String;
    iget-object v4, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 274
    .local v2, "path2":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 275
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 259
    iget-object v1, p0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 260
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 261
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 263
    :goto_0
    return v1

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public run()V
    .locals 18

    .prologue
    .line 83
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 84
    .local v8, "startTime":J
    const/4 v3, 0x0

    .line 85
    .local v3, "indexUtil":Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    const/4 v7, -0x1

    .line 86
    .local v7, "status":I
    const/4 v6, 0x0

    .line 90
    .local v6, "isStopped":Z
    const/4 v10, 0x0

    const/4 v11, 0x0

    :try_start_0
    invoke-static {v10, v11}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->createInstance(Landroid/content/Context;Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-result-object v3

    .line 92
    if-nez v3, :cond_4

    .line 93
    const-string/jumbo v10, "ThumbnailParserThread"

    const-string/jumbo v11, "ERROR: indexutil null "

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_9

    .line 153
    if-eqz v6, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 157
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->thumbnailPath:Ljava/lang/String;

    invoke-interface {v10, v11, v7, v12}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailGeneratedFile(Ljava/lang/String;ILjava/lang/String;)V

    .line 163
    :cond_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    if-eqz v10, :cond_3

    .line 165
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    invoke-virtual {v10}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->getFolderName()Ljava/io/File;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    .line 166
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->deleteTempFiles(Ljava/io/File;)Z

    .line 169
    :cond_3
    if-eqz v3, :cond_0

    .line 171
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbnailCreationStatus(Z)V

    .line 173
    sget-object v11, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v11

    .line 174
    :try_start_1
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decTotalThumbFileCount()I

    .line 175
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v4, v10, v8

    .line 178
    .local v4, "endTime":J
    invoke-virtual {v3, v4, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbTotalTime(J)V

    .line 179
    const-string/jumbo v10, "ThumbnailParserThread"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Thumbnail End File extension: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mExtension:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Size in KB: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v12

    const-wide/16 v14, 0x400

    div-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Total Time : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getRemainingThumbFileCount()I

    move-result v10

    if-nez v10, :cond_0

    .line 186
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->listManager:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->startThumbnailFor2003FormatIfAny()Z

    move-result v10

    if-nez v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_0

    .line 187
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    invoke-interface {v10}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailCompleted()V

    goto/16 :goto_0

    .line 175
    .end local v4    # "endTime":J
    :catchall_0
    move-exception v10

    :try_start_2
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v10

    .line 98
    :cond_4
    :try_start_3
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getStopThumbnailFlag()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 99
    const-string/jumbo v10, "ThumbnailParserThread"

    const-string/jumbo v11, "Thumbnail Parser is stopping the process."

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const/4 v6, 0x1

    .line 101
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->listManager:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    invoke-virtual {v10, v11}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->shutdownThread(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_3
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Error; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_9

    .line 153
    if-nez v6, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_5

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 157
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->thumbnailPath:Ljava/lang/String;

    invoke-interface {v10, v11, v7, v12}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailGeneratedFile(Ljava/lang/String;ILjava/lang/String;)V

    .line 163
    :cond_5
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    if-eqz v10, :cond_6

    .line 165
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    invoke-virtual {v10}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->getFolderName()Ljava/io/File;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    .line 166
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->deleteTempFiles(Ljava/io/File;)Z

    .line 169
    :cond_6
    if-eqz v3, :cond_0

    .line 171
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbnailCreationStatus(Z)V

    .line 173
    sget-object v11, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v11

    .line 174
    :try_start_4
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decTotalThumbFileCount()I

    .line 175
    monitor-exit v11
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v4, v10, v8

    .line 178
    .restart local v4    # "endTime":J
    invoke-virtual {v3, v4, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbTotalTime(J)V

    .line 179
    const-string/jumbo v10, "ThumbnailParserThread"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Thumbnail End File extension: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mExtension:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Size in KB: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v12

    const-wide/16 v14, 0x400

    div-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Total Time : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getRemainingThumbFileCount()I

    move-result v10

    if-nez v10, :cond_0

    .line 186
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->listManager:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->startThumbnailFor2003FormatIfAny()Z

    move-result v10

    if-nez v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_0

    .line 187
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    invoke-interface {v10}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailCompleted()V

    goto/16 :goto_0

    .line 175
    .end local v4    # "endTime":J
    :catchall_1
    move-exception v10

    :try_start_5
    monitor-exit v11
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v10

    .line 104
    :cond_7
    const/4 v10, 0x1

    :try_start_6
    invoke-virtual {v3, v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbnailCreationStatus(Z)V

    .line 106
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_a

    .line 107
    invoke-direct/range {p0 .. p0}, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->startThumbnailGenerator()I
    :try_end_6
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Error; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_9

    move-result v7

    .line 153
    if-nez v6, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_8

    .line 157
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->thumbnailPath:Ljava/lang/String;

    invoke-interface {v10, v11, v7, v12}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailGeneratedFile(Ljava/lang/String;ILjava/lang/String;)V

    .line 163
    :cond_8
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    if-eqz v10, :cond_9

    .line 165
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    invoke-virtual {v10}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->getFolderName()Ljava/io/File;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    .line 166
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->deleteTempFiles(Ljava/io/File;)Z

    .line 169
    :cond_9
    if-eqz v3, :cond_0

    .line 171
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbnailCreationStatus(Z)V

    .line 173
    sget-object v11, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v11

    .line 174
    :try_start_7
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decTotalThumbFileCount()I

    .line 175
    monitor-exit v11
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v4, v10, v8

    .line 178
    .restart local v4    # "endTime":J
    invoke-virtual {v3, v4, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbTotalTime(J)V

    .line 179
    const-string/jumbo v10, "ThumbnailParserThread"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Thumbnail End File extension: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mExtension:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Size in KB: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v12

    const-wide/16 v14, 0x400

    div-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Total Time : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getRemainingThumbFileCount()I

    move-result v10

    if-nez v10, :cond_0

    .line 186
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->listManager:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->startThumbnailFor2003FormatIfAny()Z

    move-result v10

    if-nez v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_0

    .line 187
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    invoke-interface {v10}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailCompleted()V

    goto/16 :goto_0

    .line 153
    .end local v4    # "endTime":J
    :cond_a
    if-nez v6, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_b

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_b

    .line 157
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->thumbnailPath:Ljava/lang/String;

    invoke-interface {v10, v11, v7, v12}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailGeneratedFile(Ljava/lang/String;ILjava/lang/String;)V

    .line 163
    :cond_b
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    if-eqz v10, :cond_c

    .line 165
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    invoke-virtual {v10}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->getFolderName()Ljava/io/File;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    .line 166
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->deleteTempFiles(Ljava/io/File;)Z

    .line 169
    :cond_c
    if-eqz v3, :cond_0

    .line 171
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbnailCreationStatus(Z)V

    .line 173
    sget-object v11, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v11

    .line 174
    :try_start_8
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decTotalThumbFileCount()I

    .line 175
    monitor-exit v11
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v4, v10, v8

    .line 178
    .restart local v4    # "endTime":J
    invoke-virtual {v3, v4, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbTotalTime(J)V

    .line 179
    const-string/jumbo v10, "ThumbnailParserThread"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Thumbnail End File extension: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mExtension:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Size in KB: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v12

    const-wide/16 v14, 0x400

    div-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Total Time : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getRemainingThumbFileCount()I

    move-result v10

    if-nez v10, :cond_0

    .line 186
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->listManager:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->startThumbnailFor2003FormatIfAny()Z

    move-result v10

    if-nez v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_0

    .line 187
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    invoke-interface {v10}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailCompleted()V

    goto/16 :goto_0

    .line 175
    .end local v4    # "endTime":J
    :catchall_2
    move-exception v10

    :try_start_9
    monitor-exit v11
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw v10

    :catchall_3
    move-exception v10

    :try_start_a
    monitor-exit v11
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v10

    .line 127
    :catch_0
    move-exception v2

    .line 128
    .local v2, "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :try_start_b
    const-string/jumbo v10, "ThumbnailParserThread"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "EncryptedDocumentException: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Lcom/samsung/index/GeneralEncryptedDocException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_9

    .line 129
    const/4 v7, 0x2

    .line 153
    if-nez v6, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_d

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_d

    .line 157
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->thumbnailPath:Ljava/lang/String;

    invoke-interface {v10, v11, v7, v12}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailGeneratedFile(Ljava/lang/String;ILjava/lang/String;)V

    .line 163
    :cond_d
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    if-eqz v10, :cond_e

    .line 165
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    invoke-virtual {v10}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->getFolderName()Ljava/io/File;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    .line 166
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->deleteTempFiles(Ljava/io/File;)Z

    .line 169
    :cond_e
    if-eqz v3, :cond_0

    .line 171
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbnailCreationStatus(Z)V

    .line 173
    sget-object v11, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v11

    .line 174
    :try_start_c
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decTotalThumbFileCount()I

    .line 175
    monitor-exit v11
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v4, v10, v8

    .line 178
    .restart local v4    # "endTime":J
    invoke-virtual {v3, v4, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbTotalTime(J)V

    .line 179
    const-string/jumbo v10, "ThumbnailParserThread"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Thumbnail End File extension: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mExtension:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Size in KB: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v12

    const-wide/16 v14, 0x400

    div-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Total Time : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getRemainingThumbFileCount()I

    move-result v10

    if-nez v10, :cond_0

    .line 186
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->listManager:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->startThumbnailFor2003FormatIfAny()Z

    move-result v10

    if-nez v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_0

    .line 187
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    invoke-interface {v10}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailCompleted()V

    goto/16 :goto_0

    .line 175
    .end local v4    # "endTime":J
    :catchall_4
    move-exception v10

    :try_start_d
    monitor-exit v11
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    throw v10

    .line 130
    .end local v2    # "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :catch_1
    move-exception v2

    .line 131
    .local v2, "e":Lcom/samsung/index/PasswordProtectedDocException;
    :try_start_e
    const-string/jumbo v10, "ThumbnailParserThread"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "PasswordProtectedDocException: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Lcom/samsung/index/PasswordProtectedDocException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_9

    .line 133
    const/4 v7, 0x1

    .line 153
    if-nez v6, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_f

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_f

    .line 157
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->thumbnailPath:Ljava/lang/String;

    invoke-interface {v10, v11, v7, v12}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailGeneratedFile(Ljava/lang/String;ILjava/lang/String;)V

    .line 163
    :cond_f
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    if-eqz v10, :cond_10

    .line 165
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    invoke-virtual {v10}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->getFolderName()Ljava/io/File;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    .line 166
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->deleteTempFiles(Ljava/io/File;)Z

    .line 169
    :cond_10
    if-eqz v3, :cond_0

    .line 171
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbnailCreationStatus(Z)V

    .line 173
    sget-object v11, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v11

    .line 174
    :try_start_f
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decTotalThumbFileCount()I

    .line 175
    monitor-exit v11
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v4, v10, v8

    .line 178
    .restart local v4    # "endTime":J
    invoke-virtual {v3, v4, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbTotalTime(J)V

    .line 179
    const-string/jumbo v10, "ThumbnailParserThread"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Thumbnail End File extension: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mExtension:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Size in KB: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v12

    const-wide/16 v14, 0x400

    div-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Total Time : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getRemainingThumbFileCount()I

    move-result v10

    if-nez v10, :cond_0

    .line 186
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->listManager:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->startThumbnailFor2003FormatIfAny()Z

    move-result v10

    if-nez v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_0

    .line 187
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    invoke-interface {v10}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailCompleted()V

    goto/16 :goto_0

    .line 175
    .end local v4    # "endTime":J
    :catchall_5
    move-exception v10

    :try_start_10
    monitor-exit v11
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    throw v10

    .line 134
    .end local v2    # "e":Lcom/samsung/index/PasswordProtectedDocException;
    :catch_2
    move-exception v2

    .line 135
    .local v2, "e":Ljava/lang/UnsupportedOperationException;
    :try_start_11
    const-string/jumbo v10, "ThumbnailParserThread"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "UnsupportedOperationException: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/UnsupportedOperationException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_9

    .line 137
    const/4 v7, 0x4

    .line 153
    if-nez v6, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_11

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_11

    .line 157
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->thumbnailPath:Ljava/lang/String;

    invoke-interface {v10, v11, v7, v12}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailGeneratedFile(Ljava/lang/String;ILjava/lang/String;)V

    .line 163
    :cond_11
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    if-eqz v10, :cond_12

    .line 165
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    invoke-virtual {v10}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->getFolderName()Ljava/io/File;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    .line 166
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->deleteTempFiles(Ljava/io/File;)Z

    .line 169
    :cond_12
    if-eqz v3, :cond_0

    .line 171
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbnailCreationStatus(Z)V

    .line 173
    sget-object v11, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v11

    .line 174
    :try_start_12
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decTotalThumbFileCount()I

    .line 175
    monitor-exit v11
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_6

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v4, v10, v8

    .line 178
    .restart local v4    # "endTime":J
    invoke-virtual {v3, v4, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbTotalTime(J)V

    .line 179
    const-string/jumbo v10, "ThumbnailParserThread"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Thumbnail End File extension: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mExtension:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Size in KB: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v12

    const-wide/16 v14, 0x400

    div-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Total Time : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getRemainingThumbFileCount()I

    move-result v10

    if-nez v10, :cond_0

    .line 186
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->listManager:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->startThumbnailFor2003FormatIfAny()Z

    move-result v10

    if-nez v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_0

    .line 187
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    invoke-interface {v10}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailCompleted()V

    goto/16 :goto_0

    .line 175
    .end local v4    # "endTime":J
    :catchall_6
    move-exception v10

    :try_start_13
    monitor-exit v11
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    throw v10

    .line 138
    .end local v2    # "e":Ljava/lang/UnsupportedOperationException;
    :catch_3
    move-exception v2

    .line 139
    .local v2, "e":Ljava/lang/Exception;
    :try_start_14
    const-string/jumbo v10, "ThumbnailParserThread"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Exception: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_9

    .line 140
    const/4 v7, 0x3

    .line 153
    if-nez v6, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_13

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_13

    .line 157
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->thumbnailPath:Ljava/lang/String;

    invoke-interface {v10, v11, v7, v12}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailGeneratedFile(Ljava/lang/String;ILjava/lang/String;)V

    .line 163
    :cond_13
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    if-eqz v10, :cond_14

    .line 165
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    invoke-virtual {v10}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->getFolderName()Ljava/io/File;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    .line 166
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->deleteTempFiles(Ljava/io/File;)Z

    .line 169
    :cond_14
    if-eqz v3, :cond_0

    .line 171
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbnailCreationStatus(Z)V

    .line 173
    sget-object v11, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v11

    .line 174
    :try_start_15
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decTotalThumbFileCount()I

    .line 175
    monitor-exit v11
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_7

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v4, v10, v8

    .line 178
    .restart local v4    # "endTime":J
    invoke-virtual {v3, v4, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbTotalTime(J)V

    .line 179
    const-string/jumbo v10, "ThumbnailParserThread"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Thumbnail End File extension: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mExtension:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Size in KB: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v12

    const-wide/16 v14, 0x400

    div-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Total Time : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getRemainingThumbFileCount()I

    move-result v10

    if-nez v10, :cond_0

    .line 186
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->listManager:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->startThumbnailFor2003FormatIfAny()Z

    move-result v10

    if-nez v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_0

    .line 187
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    invoke-interface {v10}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailCompleted()V

    goto/16 :goto_0

    .line 175
    .end local v4    # "endTime":J
    :catchall_7
    move-exception v10

    :try_start_16
    monitor-exit v11
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_7

    throw v10

    .line 141
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v2

    .line 142
    .local v2, "e":Ljava/lang/Error;
    :try_start_17
    const-string/jumbo v10, "ThumbnailParserThread"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Error on parsing current file, saving file to db: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_9

    .line 150
    const/4 v7, 0x3

    .line 153
    if-nez v6, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_15

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_15

    .line 157
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->thumbnailPath:Ljava/lang/String;

    invoke-interface {v10, v11, v7, v12}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailGeneratedFile(Ljava/lang/String;ILjava/lang/String;)V

    .line 163
    :cond_15
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    if-eqz v10, :cond_16

    .line 165
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    invoke-virtual {v10}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->getFolderName()Ljava/io/File;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    .line 166
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->deleteTempFiles(Ljava/io/File;)Z

    .line 169
    :cond_16
    if-eqz v3, :cond_0

    .line 171
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbnailCreationStatus(Z)V

    .line 173
    sget-object v11, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v11

    .line 174
    :try_start_18
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decTotalThumbFileCount()I

    .line 175
    monitor-exit v11
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_8

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v4, v10, v8

    .line 178
    .restart local v4    # "endTime":J
    invoke-virtual {v3, v4, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbTotalTime(J)V

    .line 179
    const-string/jumbo v10, "ThumbnailParserThread"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Thumbnail End File extension: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mExtension:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Size in KB: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v12

    const-wide/16 v14, 0x400

    div-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " Total Time : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getRemainingThumbFileCount()I

    move-result v10

    if-nez v10, :cond_0

    .line 186
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->listManager:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->startThumbnailFor2003FormatIfAny()Z

    move-result v10

    if-nez v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_0

    .line 187
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    invoke-interface {v10}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailCompleted()V

    goto/16 :goto_0

    .line 175
    .end local v4    # "endTime":J
    :catchall_8
    move-exception v10

    :try_start_19
    monitor-exit v11
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_8

    throw v10

    .line 153
    .end local v2    # "e":Ljava/lang/Error;
    :catchall_9
    move-exception v10

    if-nez v6, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v11, :cond_17

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_17

    .line 157
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->thumbnailPath:Ljava/lang/String;

    invoke-interface {v11, v12, v7, v13}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailGeneratedFile(Ljava/lang/String;ILjava/lang/String;)V

    .line 163
    :cond_17
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    if-eqz v11, :cond_18

    .line 165
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->parser:Lcom/samsung/index/parser/ThumbnailDocumentParser;

    invoke-virtual {v11}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->getFolderName()Ljava/io/File;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    .line 166
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->tempFolder:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->deleteTempFiles(Ljava/io/File;)Z

    .line 169
    :cond_18
    if-eqz v3, :cond_19

    .line 171
    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbnailCreationStatus(Z)V

    .line 173
    sget-object v11, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v11

    .line 174
    :try_start_1a
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decTotalThumbFileCount()I

    .line 175
    monitor-exit v11
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_a

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long v4, v12, v8

    .line 178
    .restart local v4    # "endTime":J
    invoke-virtual {v3, v4, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setThumbTotalTime(J)V

    .line 179
    const-string/jumbo v11, "ThumbnailParserThread"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Thumbnail End File extension: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mExtension:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " Size in KB: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v13}, Ljava/io/File;->length()J

    move-result-wide v14

    const-wide/16 v16, 0x400

    div-long v14, v14, v16

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " Total Time : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getRemainingThumbFileCount()I

    move-result v11

    if-nez v11, :cond_19

    .line 186
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->listManager:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->startThumbnailFor2003FormatIfAny()Z

    move-result v10

    if-nez v10, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    if-eqz v10, :cond_0

    .line 187
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/threads/ThumbnailParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    invoke-interface {v10}, Lcom/samsung/index/IFileIndexedNotifier;->thumbnailCompleted()V

    goto/16 :goto_0

    .line 175
    .end local v4    # "endTime":J
    :catchall_a
    move-exception v10

    :try_start_1b
    monitor-exit v11
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_a

    throw v10

    .line 191
    :cond_19
    throw v10
.end method
