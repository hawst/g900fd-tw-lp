.class public Lcom/samsung/thumbnail/util/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field private static final BVAL:[I

.field public static final ENCRYPTED_EXCEPTION_MSG:Ljava/lang/String; = "Not a zip archive"

.field public static final ENCRYPTED_EXCEPTION_MSG_2003:Ljava/lang/String; = "Invalid header signature"

.field public static final INIT_SB_CAPACITY:I = 0x100

.field private static final ROMANLC:[Ljava/lang/String;

.field private static final ROMANUC:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0xd

    .line 47
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "M"

    aput-object v1, v0, v4

    const-string/jumbo v1, "CM"

    aput-object v1, v0, v5

    const-string/jumbo v1, "D"

    aput-object v1, v0, v6

    const-string/jumbo v1, "CD"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string/jumbo v2, "C"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "XC"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "L"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "XL"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "X"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "IX"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "V"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "IV"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "I"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/util/Utils;->ROMANUC:[Ljava/lang/String;

    .line 50
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "m"

    aput-object v1, v0, v4

    const-string/jumbo v1, "cm"

    aput-object v1, v0, v5

    const-string/jumbo v1, "d"

    aput-object v1, v0, v6

    const-string/jumbo v1, "cd"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string/jumbo v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "xc"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "l"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "xl"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "x"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "ix"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "v"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "iv"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "i"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/util/Utils;->ROMANLC:[Ljava/lang/String;

    .line 53
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/thumbnail/util/Utils;->BVAL:[I

    return-void

    :array_0
    .array-data 4
        0x3e8
        0x384
        0x1f4
        0x190
        0x64
        0x5a
        0x32
        0x28
        0xa
        0x9
        0x5
        0x4
        0x1
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static binaryToRomanLC(I)Ljava/lang/String;
    .locals 4
    .param p0, "binary"    # I

    .prologue
    .line 883
    if-lez p0, :cond_0

    const/16 v2, 0xfa0

    if-lt p0, v2, :cond_1

    .line 884
    :cond_0
    new-instance v2, Ljava/lang/NumberFormatException;

    const-string/jumbo v3, "Value outside roman numeral range."

    invoke-direct {v2, v3}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 888
    :cond_1
    new-instance v1, Ljava/lang/StringBuffer;

    const-string/jumbo v2, ""

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 892
    .local v1, "roman":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/samsung/thumbnail/util/Utils;->ROMANLC:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 893
    :goto_1
    sget-object v2, Lcom/samsung/thumbnail/util/Utils;->BVAL:[I

    aget v2, v2, v0

    if-lt p0, v2, :cond_2

    .line 894
    sget-object v2, Lcom/samsung/thumbnail/util/Utils;->BVAL:[I

    aget v2, v2, v0

    sub-int/2addr p0, v2

    .line 895
    sget-object v2, Lcom/samsung/thumbnail/util/Utils;->ROMANLC:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 892
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 898
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static binaryToRomanUC(I)Ljava/lang/String;
    .locals 4
    .param p0, "binary"    # I

    .prologue
    .line 864
    if-lez p0, :cond_0

    const/16 v2, 0xfa0

    if-lt p0, v2, :cond_1

    .line 865
    :cond_0
    new-instance v2, Ljava/lang/NumberFormatException;

    const-string/jumbo v3, "Value outside roman numeral range."

    invoke-direct {v2, v3}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 869
    :cond_1
    new-instance v1, Ljava/lang/StringBuffer;

    const-string/jumbo v2, ""

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 873
    .local v1, "roman":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/samsung/thumbnail/util/Utils;->ROMANUC:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 874
    :goto_1
    sget-object v2, Lcom/samsung/thumbnail/util/Utils;->BVAL:[I

    aget v2, v2, v0

    if-lt p0, v2, :cond_2

    .line 875
    sget-object v2, Lcom/samsung/thumbnail/util/Utils;->BVAL:[I

    aget v2, v2, v0

    sub-int/2addr p0, v2

    .line 876
    sget-object v2, Lcom/samsung/thumbnail/util/Utils;->ROMANUC:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 873
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 879
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 6
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    .line 232
    iget v2, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 233
    .local v2, "height":I
    iget v4, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 234
    .local v4, "width":I
    const/4 v3, 0x1

    .line 236
    .local v3, "inSampleSize":I
    if-gt v2, p2, :cond_0

    if-le v4, p1, :cond_1

    .line 237
    :cond_0
    div-int/lit8 v0, v2, 0x2

    .line 238
    .local v0, "halfHeight":I
    div-int/lit8 v1, v4, 0x2

    .line 243
    .local v1, "halfWidth":I
    :goto_0
    div-int v5, v0, v3

    if-le v5, p2, :cond_1

    div-int v5, v1, v3

    if-le v5, p1, :cond_1

    .line 244
    mul-int/lit8 v3, v3, 0x2

    goto :goto_0

    .line 247
    .end local v0    # "halfHeight":I
    .end local v1    # "halfWidth":I
    :cond_1
    return v3
.end method

.method private static checkEncryption(Ljava/io/File;)V
    .locals 6
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 287
    :try_start_0
    new-instance v2, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v4}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    .line 288
    .local v2, "fs":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    new-instance v3, Lorg/apache/poi/poifs/crypt/EncryptionInfo;

    invoke-direct {v3, v2}, Lorg/apache/poi/poifs/crypt/EncryptionInfo;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 289
    .local v3, "info":Lorg/apache/poi/poifs/crypt/EncryptionInfo;
    invoke-static {v3}, Lorg/apache/poi/poifs/crypt/Decryptor;->getInstance(Lorg/apache/poi/poifs/crypt/EncryptionInfo;)Lorg/apache/poi/poifs/crypt/Decryptor;

    move-result-object v0

    .line 290
    .local v0, "d":Lorg/apache/poi/poifs/crypt/Decryptor;
    const-string/jumbo v4, ""

    invoke-virtual {v0, v4}, Lorg/apache/poi/poifs/crypt/Decryptor;->verifyPassword(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 291
    new-instance v4, Lcom/samsung/index/PasswordProtectedDocException;

    const-string/jumbo v5, "Document is encrypted"

    invoke-direct {v4, v5}, Lcom/samsung/index/PasswordProtectedDocException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 293
    .end local v0    # "d":Lorg/apache/poi/poifs/crypt/Decryptor;
    .end local v2    # "fs":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .end local v3    # "info":Lorg/apache/poi/poifs/crypt/EncryptionInfo;
    :catch_0
    move-exception v1

    .line 294
    .local v1, "e":Lcom/samsung/index/PasswordProtectedDocException;
    throw v1

    .line 295
    .end local v1    # "e":Lcom/samsung/index/PasswordProtectedDocException;
    :catch_1
    move-exception v4

    .line 298
    :cond_0
    return-void
.end method

.method public static createMatrixFromArray([F)Landroid/graphics/Matrix;
    .locals 6
    .param p0, "m6"    # [F

    .prologue
    .line 93
    const/4 v0, 0x0

    aget v0, p0, v0

    const/4 v1, 0x1

    aget v1, p0, v1

    const/4 v2, 0x2

    aget v2, p0, v2

    const/4 v3, 0x3

    aget v3, p0, v3

    const/4 v4, 0x4

    aget v4, p0, v4

    const/4 v5, 0x5

    aget v5, p0, v5

    invoke-static/range {v0 .. v5}, Lcom/samsung/thumbnail/util/Utils;->createMatrixFromValues(FFFFFF)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public static createMatrixFromValues(FFFFFF)Landroid/graphics/Matrix;
    .locals 7
    .param p0, "m00"    # F
    .param p1, "m01"    # F
    .param p2, "m10"    # F
    .param p3, "m11"    # F
    .param p4, "m02"    # F
    .param p5, "m12"    # F

    .prologue
    .line 87
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .local v0, "result":Landroid/graphics/Matrix;
    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    .line 88
    invoke-static/range {v0 .. v6}, Lcom/samsung/thumbnail/util/Utils;->setMatrixFromValues(Landroid/graphics/Matrix;FFFFFF)V

    .line 89
    return-object v0
.end method

.method public static decodeSampledBitmapFromByteArray([BIIII)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "byteArray"    # [B
    .param p1, "offset"    # I
    .param p2, "arrayLength"    # I
    .param p3, "reqWidth"    # I
    .param p4, "reqHeight"    # I

    .prologue
    const/4 v2, 0x0

    .line 205
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 206
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 207
    invoke-static {p0, v2, p2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 210
    invoke-static {v0, p3, p4}, Lcom/samsung/thumbnail/util/Utils;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 214
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 215
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 216
    invoke-static {p0, v2, p2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public static decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "filepath"    # Ljava/lang/String;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    .line 175
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 176
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 177
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 180
    invoke-static {v0, p1, p2}, Lcom/samsung/thumbnail/util/Utils;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 184
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 185
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 186
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public static getDirectionalText(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "runText"    # Ljava/lang/String;

    .prologue
    .line 302
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/samsung/thumbnail/util/Utils;->isRTLText(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    invoke-static {p0}, Lcom/samsung/thumbnail/util/Utils;->reverseRTLText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 305
    :cond_0
    return-object p0
.end method

.method public static getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;
    .locals 3
    .param p0, "opt"    # Lorg/apache/poi/ddf/EscherOptRecord;
    .param p1, "propId"    # I

    .prologue
    .line 535
    if-eqz p0, :cond_1

    .line 536
    invoke-virtual {p0}, Lorg/apache/poi/ddf/EscherOptRecord;->getEscherProperties()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 537
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherProperty;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 538
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherProperty;

    .line 539
    .local v1, "prop":Lorg/apache/poi/ddf/EscherProperty;
    invoke-virtual {v1}, Lorg/apache/poi/ddf/EscherProperty;->getPropertyNumber()S

    move-result v2

    if-ne v2, p1, :cond_0

    .line 543
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherProperty;>;"
    .end local v1    # "prop":Lorg/apache/poi/ddf/EscherProperty;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getIntEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;S)I
    .locals 7
    .param p0, "opt"    # Lorg/apache/poi/ddf/EscherOptRecord;
    .param p1, "propId"    # S

    .prologue
    const/4 v4, 0x0

    .line 504
    invoke-static {p0, p1}, Lcom/samsung/thumbnail/util/Utils;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v2

    .line 505
    .local v2, "prop":Lorg/apache/poi/ddf/EscherProperty;
    if-nez v2, :cond_1

    .line 524
    :cond_0
    :goto_0
    return v4

    .line 507
    :cond_1
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherProperty;->isComplex()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 508
    invoke-static {p0, p1}, Lcom/samsung/thumbnail/util/Utils;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherComplexProperty;

    .line 510
    .local v0, "complexProperty":Lorg/apache/poi/ddf/EscherComplexProperty;
    if-eqz v0, :cond_0

    .line 511
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherComplexProperty;->getComplexData()[B

    move-result-object v1

    .line 513
    .local v1, "data":[B
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherComplexProperty;->getPropertyNumber()S

    move-result v5

    const/16 v6, 0x145

    if-ne v5, v6, :cond_0

    .line 514
    invoke-static {v1}, Lorg/apache/poi/util/LittleEndian;->getInt([B)I

    move-result v4

    goto :goto_0

    .line 522
    .end local v0    # "complexProperty":Lorg/apache/poi/ddf/EscherComplexProperty;
    .end local v1    # "data":[B
    :cond_2
    invoke-static {p0, p1}, Lcom/samsung/thumbnail/util/Utils;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 524
    .local v3, "propSimple":Lorg/apache/poi/ddf/EscherSimpleProperty;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v4

    goto :goto_0
.end method

.method public static getShapeCategory(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "shapeName"    # Ljava/lang/String;

    .prologue
    .line 678
    const-string/jumbo v0, ""

    .line 679
    .local v0, "shapeCategory":Ljava/lang/String;
    const-string/jumbo v1, "Rect"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Rectangle"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "RoundRectangle"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "RoundRect"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "round1Rect"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "round2DiagRect"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Diamond"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Ellipse"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "IsocelesTriangle"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Triangle"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "RightTriangle"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "rtTriangle"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Parallelogram"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Pentagon"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Hexagon"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Octagon"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Plus"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Can"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "mathPlus"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "TextBox"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Line"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Oval"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 702
    :cond_0
    const-string/jumbo v0, "BasicShapes"

    .line 847
    :cond_1
    :goto_0
    return-object v0

    .line 704
    :cond_2
    const-string/jumbo v1, "ActionButtonHome"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 706
    const-string/jumbo v0, "ActionButtonShapes"

    goto :goto_0

    .line 708
    :cond_3
    const-string/jumbo v1, "RightArrowCallout"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string/jumbo v1, "LeftArrowCallout"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string/jumbo v1, "UpArrowCallout"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string/jumbo v1, "DownArrowCallout"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string/jumbo v1, "LeftRightArrowCallout"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 714
    :cond_4
    const-string/jumbo v0, "ArrowCallout"

    goto :goto_0

    .line 716
    :cond_5
    const-string/jumbo v1, "BentConnector2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string/jumbo v1, "BentConnector3"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string/jumbo v1, "CurvedConnector3"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string/jumbo v1, "StraightConnector1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string/jumbo v1, "Line"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 723
    :cond_6
    const-string/jumbo v0, "BentConnector"

    goto :goto_0

    .line 724
    :cond_7
    const-string/jumbo v1, "DownArrow"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string/jumbo v1, "UpArrow"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string/jumbo v1, "Arrow"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string/jumbo v1, "RightArrow"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string/jumbo v1, "LeftArrow"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string/jumbo v1, "LeftRightArrow"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string/jumbo v1, "UpDownArrow"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string/jumbo v1, "NotchedRightArrow"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 733
    :cond_8
    const-string/jumbo v0, "BlockArrow"

    goto/16 :goto_0

    .line 734
    :cond_9
    const-string/jumbo v1, "HomePlate"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string/jumbo v1, "Chevron"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string/jumbo v1, "Plaque"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string/jumbo v1, "LightningBolt"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string/jumbo v1, "Moon"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string/jumbo v1, "Cube"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string/jumbo v1, "FoldedCorner"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string/jumbo v1, "Bevel"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string/jumbo v1, "Sun"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string/jumbo v1, "HomePlate"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string/jumbo v1, "SmileyFace"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 748
    :cond_a
    const-string/jumbo v0, "BlockShapes"

    goto/16 :goto_0

    .line 749
    :cond_b
    const-string/jumbo v1, "LeftBrace"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string/jumbo v1, "RightBrace"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string/jumbo v1, "BracePair"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 752
    :cond_c
    const-string/jumbo v0, "BraceShapes"

    goto/16 :goto_0

    .line 753
    :cond_d
    const-string/jumbo v1, "BracketPair"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string/jumbo v1, "LeftBracket"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string/jumbo v1, "RightBracket"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 756
    :cond_e
    const-string/jumbo v0, "BracketShapes"

    goto/16 :goto_0

    .line 757
    :cond_f
    const-string/jumbo v1, "Ribbon2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_10

    const-string/jumbo v1, "Ribbon"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_10

    const-string/jumbo v1, "EllipseRibbon"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_10

    const-string/jumbo v1, "EllipseRibbon2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 761
    :cond_10
    const-string/jumbo v0, "Ribbbonhapes"

    goto/16 :goto_0

    .line 762
    :cond_11
    const-string/jumbo v1, "Callout1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_12

    const-string/jumbo v1, "BorderCallout1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_12

    const-string/jumbo v1, "AccentCallout1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_12

    const-string/jumbo v1, "AccentBorderCallout1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 766
    :cond_12
    const-string/jumbo v0, "Callout1"

    goto/16 :goto_0

    .line 767
    :cond_13
    const-string/jumbo v1, "Callout2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_14

    const-string/jumbo v1, "BorderCallout2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_14

    const-string/jumbo v1, "AccentCallout2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_14

    const-string/jumbo v1, "AccentBorderCallout2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 771
    :cond_14
    const-string/jumbo v0, "Callout2"

    goto/16 :goto_0

    .line 772
    :cond_15
    const-string/jumbo v1, "Callout3"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string/jumbo v1, "BorderCallout3"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string/jumbo v1, "AccentCallout3"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string/jumbo v1, "AccentBorderCallout3"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 776
    :cond_16
    const-string/jumbo v0, "Callout3"

    goto/16 :goto_0

    .line 777
    :cond_17
    const-string/jumbo v1, "CurvedRightArrow"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_18

    const-string/jumbo v1, "CurvedLeftArrow"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_18

    const-string/jumbo v1, "CurvedUpArrow"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_18

    const-string/jumbo v1, "CurvedDownArrow"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 781
    :cond_18
    const-string/jumbo v0, "CurvedBlockArrow"

    goto/16 :goto_0

    .line 782
    :cond_19
    const-string/jumbo v1, "FlowChartMultidocument"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartDocument"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartPunchedTape"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartDisplay"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartMagneticDrum"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartMagneticDisk"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartMagneticTape"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartDelay"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartOnlineStorage"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartMerge"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartExtract"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartSort"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartCollate"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartOr"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartSummingJunction"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartPunchedCard"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartOffpageConnector"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartConnector"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartManualOperation"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartManualInput"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartPreparation"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartTerminator"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartInternalStorage"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartPredefinedProcess"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartInputOutput"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartDecision"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartAlternateProcess"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string/jumbo v1, "FlowChartProcess"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 810
    :cond_1a
    const-string/jumbo v0, "FlowChartProcess"

    goto/16 :goto_0

    .line 811
    :cond_1b
    const-string/jumbo v1, "IrregularSeal1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1c

    const-string/jumbo v1, "IrregularSeal2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 813
    :cond_1c
    const-string/jumbo v0, "IrregularSeal"

    goto/16 :goto_0

    .line 814
    :cond_1d
    const-string/jumbo v1, "Ribbon"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1e

    const-string/jumbo v1, "Ribbon2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1e

    const-string/jumbo v1, "EllipseRibbon2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1e

    const-string/jumbo v1, "EllipseRibbon"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 818
    :cond_1e
    const-string/jumbo v0, "Ribbbonhapes"

    goto/16 :goto_0

    .line 819
    :cond_1f
    const-string/jumbo v1, "HorizontalScroll"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_20

    const-string/jumbo v1, "VerticalScroll"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 821
    :cond_20
    const-string/jumbo v0, "ScrollShapes"

    goto/16 :goto_0

    .line 822
    :cond_21
    const-string/jumbo v1, "Star"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_22

    const-string/jumbo v1, "Star4"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_22

    const-string/jumbo v1, "Star8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_22

    const-string/jumbo v1, "Star16"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_22

    const-string/jumbo v1, "Star24"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_22

    const-string/jumbo v1, "Star32"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_22

    const-string/jumbo v1, "StarShapes"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 829
    :cond_22
    const-string/jumbo v0, "StarShapes"

    goto/16 :goto_0

    .line 831
    :cond_23
    const-string/jumbo v1, "Wave"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_24

    const-string/jumbo v1, "DoubleWave"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 833
    :cond_24
    const-string/jumbo v0, "WaveShapes"

    goto/16 :goto_0

    .line 834
    :cond_25
    const-string/jumbo v1, "WedgeRectCallout"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_26

    const-string/jumbo v1, "WedgeRRectCallout"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_26

    const-string/jumbo v1, "wedgeRoundRectCallout"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_26

    const-string/jumbo v1, "WedgeEllipseCallout"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_26

    const-string/jumbo v1, "CloudCallout"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 839
    :cond_26
    const-string/jumbo v0, "WedgeCallouts"

    goto/16 :goto_0

    .line 840
    :cond_27
    const-string/jumbo v1, "TEXTONPATH"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 841
    const-string/jumbo v0, "textOnPath"

    goto/16 :goto_0

    .line 842
    :cond_28
    const-string/jumbo v1, "arcConnector"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_29

    const-string/jumbo v1, "lineConnector"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 844
    :cond_29
    const-string/jumbo v0, "NonPrimitive"

    goto/16 :goto_0
.end method

.method public static getUnicodeForBullet(IZ)Ljava/lang/String;
    .locals 1
    .param p0, "code"    # I
    .param p1, "isPDF"    # Z

    .prologue
    .line 354
    sparse-switch p0, :sswitch_data_0

    .line 470
    if-eqz p1, :cond_1

    .line 471
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 473
    :goto_0
    return-object v0

    .line 362
    :sswitch_0
    const-string/jumbo v0, "\u25cf"

    goto :goto_0

    .line 364
    :sswitch_1
    if-eqz p1, :cond_0

    .line 365
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 367
    :cond_0
    const-string/jumbo v0, "\u25cb"

    goto :goto_0

    .line 375
    :sswitch_2
    const-string/jumbo v0, "\u25a0"

    goto :goto_0

    .line 379
    :sswitch_3
    const-string/jumbo v0, "\u25a1"

    goto :goto_0

    .line 382
    :sswitch_4
    const-string/jumbo v0, "\u25fb"

    goto :goto_0

    .line 386
    :sswitch_5
    const-string/jumbo v0, "\u2751"

    goto :goto_0

    .line 390
    :sswitch_6
    const-string/jumbo v0, "\u2752"

    goto :goto_0

    .line 393
    :sswitch_7
    const-string/jumbo v0, "\u25aa"

    goto :goto_0

    .line 401
    :sswitch_8
    const-string/jumbo v0, "\u25c6"

    goto :goto_0

    .line 405
    :sswitch_9
    const-string/jumbo v0, "\u2756"

    goto :goto_0

    .line 408
    :sswitch_a
    const-string/jumbo v0, "\u27fc"

    goto :goto_0

    .line 413
    :sswitch_b
    const-string/jumbo v0, "\u25ef"

    goto :goto_0

    .line 417
    :sswitch_c
    const-string/jumbo v0, "\u25c9"

    goto :goto_0

    .line 420
    :sswitch_d
    const-string/jumbo v0, "\u274d"

    goto :goto_0

    .line 422
    :sswitch_e
    const-string/jumbo v0, "\u2605"

    goto :goto_0

    .line 424
    :sswitch_f
    const-string/jumbo v0, "\u2736"

    goto :goto_0

    .line 427
    :sswitch_10
    const-string/jumbo v0, "\u2734"

    goto :goto_0

    .line 429
    :sswitch_11
    const-string/jumbo v0, "\u2739"

    goto :goto_0

    .line 433
    :sswitch_12
    const-string/jumbo v0, "\u27a2"

    goto :goto_0

    .line 435
    :sswitch_13
    const-string/jumbo v0, "\ud83e\udc68"

    goto :goto_0

    .line 437
    :sswitch_14
    const-string/jumbo v0, "\ud83e\udc6a"

    goto :goto_0

    .line 439
    :sswitch_15
    const-string/jumbo v0, "\ud83e\udc69"

    goto :goto_0

    .line 441
    :sswitch_16
    const-string/jumbo v0, "\ud83e\udc6b"

    goto :goto_0

    .line 443
    :sswitch_17
    const-string/jumbo v0, "\ud83e\udc6c"

    goto :goto_0

    .line 445
    :sswitch_18
    const-string/jumbo v0, "\ud83e\udc6d"

    goto :goto_0

    .line 447
    :sswitch_19
    const-string/jumbo v0, "\ud83e\udc6f"

    goto :goto_0

    .line 449
    :sswitch_1a
    const-string/jumbo v0, "\ud83e\udc6e"

    goto :goto_0

    .line 451
    :sswitch_1b
    const-string/jumbo v0, "\ud83e\udc78"

    goto :goto_0

    .line 453
    :sswitch_1c
    const-string/jumbo v0, "\ud83e\udc7a"

    goto :goto_0

    .line 455
    :sswitch_1d
    const-string/jumbo v0, "\ud83e\udc79"

    goto :goto_0

    .line 457
    :sswitch_1e
    const-string/jumbo v0, "\ud83e\udc7b"

    goto/16 :goto_0

    .line 459
    :sswitch_1f
    const-string/jumbo v0, "\u21e8"

    goto/16 :goto_0

    .line 461
    :sswitch_20
    const-string/jumbo v0, "\u2718"

    goto/16 :goto_0

    .line 465
    :sswitch_21
    const-string/jumbo v0, "\u2713"

    goto/16 :goto_0

    .line 468
    :sswitch_22
    const-string/jumbo v0, "\uff5e"

    goto/16 :goto_0

    .line 473
    :cond_1
    const-string/jumbo v0, "\u25cf"

    goto/16 :goto_0

    .line 354
    nop

    :sswitch_data_0
    .sparse-switch
        0x6f -> :sswitch_1
        0x83 -> :sswitch_2
        0x84 -> :sswitch_2
        0x88 -> :sswitch_6
        0x89 -> :sswitch_5
        0x8b -> :sswitch_8
        0x8d -> :sswitch_8
        0x99 -> :sswitch_9
        0xbe -> :sswitch_12
        0x2022 -> :sswitch_22
        0xf06c -> :sswitch_0
        0xf06e -> :sswitch_2
        0xf06f -> :sswitch_3
        0xf070 -> :sswitch_4
        0xf071 -> :sswitch_5
        0xf072 -> :sswitch_6
        0xf073 -> :sswitch_8
        0xf074 -> :sswitch_8
        0xf075 -> :sswitch_8
        0xf076 -> :sswitch_9
        0xf077 -> :sswitch_8
        0xf09e -> :sswitch_0
        0xf09f -> :sswitch_0
        0xf0a0 -> :sswitch_a
        0xf0a1 -> :sswitch_b
        0xf0a2 -> :sswitch_b
        0xf0a3 -> :sswitch_b
        0xf0a4 -> :sswitch_c
        0xf0a5 -> :sswitch_c
        0xf0a6 -> :sswitch_d
        0xf0a7 -> :sswitch_7
        0xf0a8 -> :sswitch_3
        0xf0ab -> :sswitch_e
        0xf0ac -> :sswitch_f
        0xf0ad -> :sswitch_10
        0xf0ae -> :sswitch_11
        0xf0b7 -> :sswitch_0
        0xf0d8 -> :sswitch_12
        0xf0df -> :sswitch_13
        0xf0e0 -> :sswitch_14
        0xf0e1 -> :sswitch_15
        0xf0e2 -> :sswitch_16
        0xf0e3 -> :sswitch_17
        0xf0e4 -> :sswitch_18
        0xf0e5 -> :sswitch_19
        0xf0e6 -> :sswitch_1a
        0xf0e7 -> :sswitch_1b
        0xf0e8 -> :sswitch_1c
        0xf0e9 -> :sswitch_1d
        0xf0ea -> :sswitch_1e
        0xf0f0 -> :sswitch_1f
        0xf0fb -> :sswitch_20
        0xf0fc -> :sswitch_21
    .end sparse-switch
.end method

.method public static isIntersecting(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 3
    .param p0, "src"    # Landroid/graphics/RectF;
    .param p1, "dst"    # Landroid/graphics/RectF;

    .prologue
    .line 143
    const/4 v0, 0x0

    .line 145
    .local v0, "isIntersecting":Z
    iget v1, p0, Landroid/graphics/RectF;->left:F

    iget v2, p1, Landroid/graphics/RectF;->left:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    iget v1, p0, Landroid/graphics/RectF;->left:F

    iget v2, p1, Landroid/graphics/RectF;->right:F

    cmpg-float v1, v1, v2

    if-lez v1, :cond_1

    :cond_0
    iget v1, p0, Landroid/graphics/RectF;->right:F

    iget v2, p1, Landroid/graphics/RectF;->left:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_4

    iget v1, p0, Landroid/graphics/RectF;->right:F

    iget v2, p1, Landroid/graphics/RectF;->right:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_4

    :cond_1
    iget v1, p0, Landroid/graphics/RectF;->top:F

    iget v2, p1, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_2

    iget v1, p0, Landroid/graphics/RectF;->top:F

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v1, v1, v2

    if-lez v1, :cond_3

    :cond_2
    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    iget v2, p1, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_4

    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_4

    .line 148
    :cond_3
    const/4 v0, 0x1

    .line 151
    :cond_4
    if-nez v0, :cond_9

    .line 152
    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget v2, p0, Landroid/graphics/RectF;->left:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_5

    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget v2, p0, Landroid/graphics/RectF;->right:F

    cmpg-float v1, v1, v2

    if-lez v1, :cond_6

    :cond_5
    iget v1, p1, Landroid/graphics/RectF;->right:F

    iget v2, p0, Landroid/graphics/RectF;->left:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_9

    iget v1, p1, Landroid/graphics/RectF;->right:F

    iget v2, p0, Landroid/graphics/RectF;->right:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_9

    :cond_6
    iget v1, p1, Landroid/graphics/RectF;->top:F

    iget v2, p0, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_7

    iget v1, p1, Landroid/graphics/RectF;->top:F

    iget v2, p0, Landroid/graphics/RectF;->bottom:F

    cmpg-float v1, v1, v2

    if-lez v1, :cond_8

    :cond_7
    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_9

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Landroid/graphics/RectF;->bottom:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_9

    .line 154
    :cond_8
    const/4 v0, 0x1

    .line 158
    :cond_9
    return v0
.end method

.method public static isRTLText(Ljava/lang/String;)Z
    .locals 2
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 316
    if-eqz p0, :cond_1

    .line 317
    new-instance v0, Ljava/text/Bidi;

    const/4 v1, -0x2

    invoke-direct {v0, p0, v1}, Ljava/text/Bidi;-><init>(Ljava/lang/String;I)V

    .line 318
    .local v0, "bidi":Ljava/text/Bidi;
    invoke-virtual {v0}, Ljava/text/Bidi;->isRightToLeft()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/text/Bidi;->isMixed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 319
    :cond_0
    const/4 v1, 0x1

    .line 322
    .end local v0    # "bidi":Ljava/text/Bidi;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isValid(Ljava/io/File;)Z
    .locals 8
    .param p0, "file"    # Ljava/io/File;

    .prologue
    const/4 v4, 0x0

    .line 257
    invoke-static {p0}, Lcom/samsung/thumbnail/util/Utils;->checkEncryption(Ljava/io/File;)V

    .line 259
    const/4 v2, 0x0

    .line 261
    .local v2, "zipfile":Ljava/util/zip/ZipFile;
    :try_start_0
    new-instance v3, Ljava/util/zip/ZipFile;

    invoke-direct {v3, p0}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/util/zip/ZipException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    .end local v2    # "zipfile":Ljava/util/zip/ZipFile;
    .local v3, "zipfile":Ljava/util/zip/ZipFile;
    const/4 v4, 0x1

    .line 275
    if-eqz v3, :cond_1

    .line 276
    :try_start_1
    invoke-virtual {v3}, Ljava/util/zip/ZipFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 277
    const/4 v2, 0x0

    .line 281
    .end local v3    # "zipfile":Ljava/util/zip/ZipFile;
    .restart local v2    # "zipfile":Ljava/util/zip/ZipFile;
    :cond_0
    :goto_0
    return v4

    .line 279
    .end local v2    # "zipfile":Ljava/util/zip/ZipFile;
    .restart local v3    # "zipfile":Ljava/util/zip/ZipFile;
    :catch_0
    move-exception v0

    .line 280
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v5, "Utils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    move-object v2, v3

    .end local v3    # "zipfile":Ljava/util/zip/ZipFile;
    .restart local v2    # "zipfile":Ljava/util/zip/ZipFile;
    goto :goto_0

    .line 263
    :catch_1
    move-exception v0

    .line 264
    .local v0, "e":Ljava/util/zip/ZipException;
    :try_start_2
    const-string/jumbo v5, "Utils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "ZipException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/util/zip/ZipException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-virtual {v0}, Ljava/util/zip/ZipException;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 266
    .local v1, "msg":Ljava/lang/String;
    if-eqz v1, :cond_3

    const-string/jumbo v5, "Not a zip archive"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 268
    new-instance v4, Lorg/apache/poi/EncryptedDocumentException;

    const-string/jumbo v5, "Document is encrypted"

    invoke-direct {v4, v5}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 274
    .end local v0    # "e":Ljava/util/zip/ZipException;
    .end local v1    # "msg":Ljava/lang/String;
    :catchall_0
    move-exception v4

    .line 275
    if-eqz v2, :cond_2

    .line 276
    :try_start_3
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    .line 277
    const/4 v2, 0x0

    .line 281
    :cond_2
    :goto_1
    throw v4

    .line 275
    .restart local v0    # "e":Ljava/util/zip/ZipException;
    .restart local v1    # "msg":Ljava/lang/String;
    :cond_3
    if-eqz v2, :cond_0

    .line 276
    :try_start_4
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 277
    const/4 v2, 0x0

    goto :goto_0

    .line 279
    :catch_2
    move-exception v0

    .line 280
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v5, "Utils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 271
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "msg":Ljava/lang/String;
    :catch_3
    move-exception v0

    .line 275
    .restart local v0    # "e":Ljava/io/IOException;
    if-eqz v2, :cond_0

    .line 276
    :try_start_5
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 277
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 279
    :catch_4
    move-exception v0

    .line 280
    const-string/jumbo v5, "Utils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 279
    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 280
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "Utils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static isValidInteger(Ljava/lang/String;)Z
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 486
    if-eqz p0, :cond_0

    .line 487
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 488
    const/4 v1, 0x1

    .line 493
    :cond_0
    :goto_0
    return v1

    .line 490
    :catch_0
    move-exception v0

    .line 491
    .local v0, "e":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method public static replaceThemeColorKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "clr"    # Ljava/lang/String;

    .prologue
    .line 851
    const-string/jumbo v0, "bg1"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 852
    const-string/jumbo p0, "lt1"

    .line 860
    :cond_0
    :goto_0
    return-object p0

    .line 853
    :cond_1
    const-string/jumbo v0, "tx1"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 854
    const-string/jumbo p0, "dk1"

    goto :goto_0

    .line 855
    :cond_2
    const-string/jumbo v0, "tx2"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 856
    const-string/jumbo p0, "dk2"

    goto :goto_0

    .line 857
    :cond_3
    const-string/jumbo v0, "bg2"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 858
    const-string/jumbo p0, "lt2"

    goto :goto_0
.end method

.method public static reverseRTLText(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 333
    new-instance v1, Ljava/util/ArrayList;

    const-string/jumbo v4, " "

    invoke-virtual {p0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 335
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 336
    new-instance v3, Ljava/lang/StringBuffer;

    const/16 v4, 0x100

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 338
    .local v3, "strBuf":Ljava/lang/StringBuffer;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 339
    .local v2, "s":Ljava/lang/String;
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 341
    .end local v2    # "s":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static setMatrixFromArray(Landroid/graphics/Matrix;[F)V
    .locals 7
    .param p0, "mat"    # Landroid/graphics/Matrix;
    .param p1, "m6"    # [F

    .prologue
    .line 82
    const/4 v0, 0x0

    aget v1, p1, v0

    const/4 v0, 0x1

    aget v2, p1, v0

    const/4 v0, 0x2

    aget v3, p1, v0

    const/4 v0, 0x3

    aget v4, p1, v0

    const/4 v0, 0x4

    aget v5, p1, v0

    const/4 v0, 0x5

    aget v6, p1, v0

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/samsung/thumbnail/util/Utils;->setMatrixFromValues(Landroid/graphics/Matrix;FFFFFF)V

    .line 83
    return-void
.end method

.method public static setMatrixFromValues(Landroid/graphics/Matrix;FFFFFF)V
    .locals 3
    .param p0, "mat"    # Landroid/graphics/Matrix;
    .param p1, "m00"    # F
    .param p2, "m01"    # F
    .param p3, "m10"    # F
    .param p4, "m11"    # F
    .param p5, "m02"    # F
    .param p6, "m12"    # F

    .prologue
    const/4 v2, 0x0

    .line 76
    const/16 v1, 0x9

    new-array v0, v1, [F

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p3, v0, v1

    const/4 v1, 0x2

    aput p5, v0, v1

    const/4 v1, 0x3

    aput p2, v0, v1

    const/4 v1, 0x4

    aput p4, v0, v1

    const/4 v1, 0x5

    aput p6, v0, v1

    const/4 v1, 0x6

    aput v2, v0, v1

    const/4 v1, 0x7

    aput v2, v0, v1

    const/16 v1, 0x8

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    .line 78
    .local v0, "fs":[F
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->setValues([F)V

    .line 79
    return-void
.end method

.method public static setPaintValues(FIIF[FF)Landroid/graphics/Paint;
    .locals 3
    .param p0, "width"    # F
    .param p1, "cap"    # I
    .param p2, "join"    # I
    .param p3, "miterlimit"    # F
    .param p4, "dash"    # [F
    .param p5, "dash_phase"    # F

    .prologue
    .line 98
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 99
    .local v0, "temp":Landroid/graphics/Paint;
    invoke-virtual {v0, p0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 101
    packed-switch p1, :pswitch_data_0

    .line 112
    sget-object v1, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 115
    :goto_0
    packed-switch p2, :pswitch_data_1

    .line 126
    sget-object v1, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 129
    :goto_1
    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setStrokeMiter(F)V

    .line 134
    if-eqz p4, :cond_0

    array-length v1, p4

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    array-length v1, p4

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_0

    .line 135
    new-instance v1, Landroid/graphics/DashPathEffect;

    invoke-direct {v1, p4, p5}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 138
    :cond_0
    return-object v0

    .line 103
    :pswitch_0
    sget-object v1, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    goto :goto_0

    .line 106
    :pswitch_1
    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    goto :goto_0

    .line 109
    :pswitch_2
    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    goto :goto_0

    .line 117
    :pswitch_3
    sget-object v1, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    goto :goto_1

    .line 120
    :pswitch_4
    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    goto :goto_1

    .line 123
    :pswitch_5
    sget-object v1, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    goto :goto_1

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 115
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static setPolarCoordinateOnRectangle(Landroid/graphics/Rect;D)[F
    .locals 23
    .param p0, "r"    # Landroid/graphics/Rect;
    .param p1, "degree"    # D

    .prologue
    .line 547
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Rect;->centerX()I

    move-result v17

    move/from16 v0, v17

    int-to-float v5, v0

    .line 548
    .local v5, "cX":F
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Rect;->centerY()I

    move-result v17

    move/from16 v0, v17

    int-to-float v8, v0

    .line 549
    .local v8, "cY":F
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Rect;->width()I

    move-result v17

    move/from16 v0, v17

    int-to-float v12, v0

    .line 550
    .local v12, "width":F
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Rect;->height()I

    move-result v17

    move/from16 v0, v17

    int-to-float v9, v0

    .line 552
    .local v9, "height":F
    const-wide/16 v2, 0x0

    .line 553
    .local v2, "angle":D
    float-to-int v0, v12

    move/from16 v17, v0

    if-eqz v17, :cond_0

    .line 554
    float-to-double v0, v9

    move-wide/from16 v18, v0

    float-to-double v0, v12

    move-wide/from16 v20, v0

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    .line 556
    :cond_0
    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    .line 558
    const/4 v4, 0x0

    .line 559
    .local v4, "base":F
    const-wide/16 v6, 0x0

    .line 560
    .local v6, "baseAngle":D
    const/4 v10, 0x0

    .line 562
    .local v10, "pp":F
    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Float;->floatValue()F

    move-result v16

    .local v16, "y1":F
    move/from16 v14, v16

    .local v14, "x1":F
    move/from16 v15, v16

    .local v15, "y0":F
    move/from16 v13, v16

    .line 564
    .local v13, "x0":F
    const-wide/16 v18, 0x0

    cmpl-double v17, p1, v18

    if-ltz v17, :cond_2

    cmpg-double v17, p1, v2

    if-gez v17, :cond_2

    .line 566
    const/high16 v17, 0x40000000    # 2.0f

    div-float v4, v12, v17

    .line 567
    move-wide/from16 v6, p1

    .line 569
    float-to-double v0, v4

    move-wide/from16 v18, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v10, v0

    .line 570
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v12, v17

    sub-float v13, v5, v17

    .line 571
    sub-float v15, v8, v10

    .line 572
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v12, v17

    add-float v14, v5, v17

    .line 573
    add-float v16, v8, v10

    .line 665
    :cond_1
    :goto_0
    const/16 v17, 0x4

    move/from16 v0, v17

    new-array v11, v0, [F

    const/16 v17, 0x0

    aput v13, v11, v17

    const/16 v17, 0x1

    aput v15, v11, v17

    const/16 v17, 0x2

    aput v14, v11, v17

    const/16 v17, 0x3

    aput v16, v11, v17

    .line 667
    .local v11, "values":[F
    return-object v11

    .line 574
    .end local v11    # "values":[F
    :cond_2
    cmpl-double v17, p1, v2

    if-ltz v17, :cond_3

    const-wide v18, 0x4056800000000000L    # 90.0

    cmpg-double v17, p1, v18

    if-gez v17, :cond_3

    .line 576
    const/high16 v17, 0x40000000    # 2.0f

    div-float v4, v9, v17

    .line 577
    const-wide v18, 0x4056800000000000L    # 90.0

    sub-double v6, v18, p1

    .line 579
    float-to-double v0, v4

    move-wide/from16 v18, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v10, v0

    .line 580
    sub-float v13, v5, v10

    .line 581
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v9, v17

    sub-float v15, v8, v17

    .line 582
    add-float v14, v5, v10

    .line 583
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v9, v17

    add-float v16, v8, v17

    goto :goto_0

    .line 585
    :cond_3
    const-wide v18, 0x4056800000000000L    # 90.0

    cmpl-double v17, p1, v18

    if-nez v17, :cond_4

    .line 587
    move v13, v5

    .line 588
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v9, v17

    sub-float v15, v8, v17

    .line 589
    move v14, v5

    .line 590
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v9, v17

    add-float v16, v8, v17

    goto :goto_0

    .line 592
    :cond_4
    const-wide v18, 0x4056800000000000L    # 90.0

    cmpl-double v17, p1, v18

    if-lez v17, :cond_5

    const-wide v18, 0x4066800000000000L    # 180.0

    sub-double v18, v18, v2

    cmpg-double v17, p1, v18

    if-gez v17, :cond_5

    .line 594
    const/high16 v17, 0x40000000    # 2.0f

    div-float v4, v9, v17

    .line 595
    const-wide v18, 0x4056800000000000L    # 90.0

    sub-double v6, p1, v18

    .line 597
    float-to-double v0, v4

    move-wide/from16 v18, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v10, v0

    .line 598
    add-float v13, v5, v10

    .line 599
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v9, v17

    sub-float v15, v8, v17

    .line 600
    sub-float v14, v5, v10

    .line 601
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v9, v17

    add-float v16, v8, v17

    goto/16 :goto_0

    .line 603
    :cond_5
    const-wide v18, 0x4066800000000000L    # 180.0

    sub-double v18, v18, v2

    cmpl-double v17, p1, v18

    if-ltz v17, :cond_6

    const-wide v18, 0x4066800000000000L    # 180.0

    cmpg-double v17, p1, v18

    if-gez v17, :cond_6

    .line 605
    const/high16 v17, 0x40000000    # 2.0f

    div-float v4, v12, v17

    .line 606
    const-wide v18, 0x4066800000000000L    # 180.0

    sub-double v6, v18, p1

    .line 608
    float-to-double v0, v4

    move-wide/from16 v18, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v10, v0

    .line 609
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v12, v17

    add-float v13, v5, v17

    .line 610
    sub-float v15, v8, v10

    .line 611
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v12, v17

    sub-float v14, v5, v17

    .line 612
    add-float v16, v8, v10

    goto/16 :goto_0

    .line 614
    :cond_6
    const-wide v18, 0x4066800000000000L    # 180.0

    cmpl-double v17, p1, v18

    if-ltz v17, :cond_7

    const-wide v18, 0x4066800000000000L    # 180.0

    add-double v18, v18, v2

    cmpg-double v17, p1, v18

    if-gez v17, :cond_7

    .line 615
    const/high16 v17, 0x40000000    # 2.0f

    div-float v4, v12, v17

    .line 616
    const-wide v18, 0x4066800000000000L    # 180.0

    sub-double v6, p1, v18

    .line 618
    float-to-double v0, v4

    move-wide/from16 v18, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v10, v0

    .line 619
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v12, v17

    add-float v13, v5, v17

    .line 620
    add-float v15, v8, v10

    .line 621
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v12, v17

    sub-float v14, v5, v17

    .line 622
    sub-float v16, v8, v10

    goto/16 :goto_0

    .line 624
    :cond_7
    const-wide v18, 0x4066800000000000L    # 180.0

    add-double v18, v18, v2

    cmpl-double v17, p1, v18

    if-ltz v17, :cond_8

    const-wide v18, 0x4070e00000000000L    # 270.0

    cmpg-double v17, p1, v18

    if-gez v17, :cond_8

    .line 626
    const/high16 v17, 0x40000000    # 2.0f

    div-float v4, v9, v17

    .line 627
    const-wide v18, 0x4070e00000000000L    # 270.0

    sub-double v6, v18, p1

    .line 629
    float-to-double v0, v4

    move-wide/from16 v18, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v10, v0

    .line 630
    add-float v13, v5, v10

    .line 631
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v9, v17

    add-float v15, v8, v17

    .line 632
    sub-float v14, v5, v10

    .line 633
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v9, v17

    sub-float v16, v8, v17

    goto/16 :goto_0

    .line 635
    :cond_8
    const-wide v18, 0x4070e00000000000L    # 270.0

    cmpl-double v17, p1, v18

    if-nez v17, :cond_9

    .line 637
    move v13, v5

    .line 638
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v9, v17

    add-float v15, v8, v17

    .line 639
    move v14, v5

    .line 640
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v9, v17

    sub-float v16, v8, v17

    goto/16 :goto_0

    .line 642
    :cond_9
    const-wide v18, 0x4070e00000000000L    # 270.0

    cmpl-double v17, p1, v18

    if-lez v17, :cond_a

    const-wide v18, 0x4076800000000000L    # 360.0

    sub-double v18, v18, v2

    cmpg-double v17, p1, v18

    if-gez v17, :cond_a

    .line 644
    const/high16 v17, 0x40000000    # 2.0f

    div-float v4, v9, v17

    .line 645
    const-wide v18, 0x4070e00000000000L    # 270.0

    sub-double v6, p1, v18

    .line 647
    float-to-double v0, v4

    move-wide/from16 v18, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v10, v0

    .line 648
    sub-float v13, v5, v10

    .line 649
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v9, v17

    add-float v15, v8, v17

    .line 650
    add-float v14, v5, v10

    .line 651
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v9, v17

    sub-float v16, v8, v17

    goto/16 :goto_0

    .line 653
    :cond_a
    const-wide v18, 0x4076800000000000L    # 360.0

    sub-double v18, v18, v2

    cmpl-double v17, p1, v18

    if-ltz v17, :cond_1

    const-wide v18, 0x4076800000000000L    # 360.0

    cmpg-double v17, p1, v18

    if-gez v17, :cond_1

    .line 655
    const/high16 v17, 0x40000000    # 2.0f

    div-float v4, v12, v17

    .line 656
    const-wide v18, 0x4076800000000000L    # 360.0

    sub-double v6, v18, p1

    .line 658
    float-to-double v0, v4

    move-wide/from16 v18, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v10, v0

    .line 659
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v12, v17

    sub-float v13, v5, v17

    .line 660
    add-float v15, v8, v10

    .line 661
    const/high16 v17, 0x40000000    # 2.0f

    div-float v17, v12, v17

    add-float v14, v5, v17

    .line 662
    sub-float v16, v8, v10

    goto/16 :goto_0
.end method
