.class final enum Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$4;
.super Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
.source "DocumentServiceState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;-><init>(Ljava/lang/String;ILcom/samsung/index/indexservice/service/DocumentServiceState$1;)V

    return-void
.end method


# virtual methods
.method nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 256
    packed-switch p1, :pswitch_data_0

    .line 282
    :cond_0
    :goto_0
    # getter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$100()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v0

    return-object v0

    .line 259
    :pswitch_0
    sget-boolean v0, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_INDEXING:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->bIsPowerConnected:Z

    if-eqz v0, :cond_0

    .line 261
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$4;->PROCESSING_INDEXING_THUMBNAIL_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    # setter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$102(Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    goto :goto_0

    .line 267
    :pswitch_1
    sget-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bStopDocService:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    if-eqz v0, :cond_1

    .line 268
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    .line 269
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$4;->PROCESSING_THUMBNAIL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    # setter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$102(Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    goto :goto_0

    .line 270
    :cond_1
    sget-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bStopDocService:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_INDEXING:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->bIsPowerConnected:Z

    if-eqz v0, :cond_2

    .line 273
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$4;->PROCESSING_INDEXING:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    # setter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$102(Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    goto :goto_0

    .line 275
    :cond_2
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$4;->STOPPING_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    # setter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$102(Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    goto :goto_0

    .line 256
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
