.class public Lcom/samsung/index/indexservice/service/SystemInfo$SIOPReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SystemInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/index/indexservice/service/SystemInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SIOPReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 285
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 289
    if-nez p2, :cond_1

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 292
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 294
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v2, "android.intent.action.CHECK_SIOP_LEVEL"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 295
    const-string/jumbo v2, "siop_level_broadcast"

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    # setter for: Lcom/samsung/index/indexservice/service/SystemInfo;->curSIOPLevel:I
    invoke-static {v2}, Lcom/samsung/index/indexservice/service/SystemInfo;->access$002(I)I

    .line 298
    :cond_2
    invoke-static {v4, v4}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->createInstance(Landroid/content/Context;Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-result-object v1

    .line 300
    .local v1, "indexUtil":Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    if-eqz v1, :cond_3

    .line 301
    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->printProcessingFileDetails()V

    .line 304
    :cond_3
    const-string/jumbo v2, "SystemInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "DocumentService [SIOP LEVEL] changed to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/samsung/index/indexservice/service/SystemInfo;->curSIOPLevel:I
    invoke-static {}, Lcom/samsung/index/indexservice/service/SystemInfo;->access$000()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    # getter for: Lcom/samsung/index/indexservice/service/SystemInfo;->curSIOPLevel:I
    invoke-static {}, Lcom/samsung/index/indexservice/service/SystemInfo;->access$000()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_4

    .line 308
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lcom/samsung/index/indexservice/service/SystemInfo;->isSIOPHigh:Ljava/lang/Boolean;

    .line 310
    # getter for: Lcom/samsung/index/indexservice/service/SystemInfo;->bSentStopServiceMessage:Z
    invoke-static {}, Lcom/samsung/index/indexservice/service/SystemInfo;->access$100()Z

    move-result v2

    if-nez v2, :cond_0

    .line 311
    # setter for: Lcom/samsung/index/indexservice/service/SystemInfo;->bSendStopServiceMessage:Z
    invoke-static {v6}, Lcom/samsung/index/indexservice/service/SystemInfo;->access$202(Z)Z

    goto :goto_0

    .line 314
    :cond_4
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lcom/samsung/index/indexservice/service/SystemInfo;->isSIOPHigh:Ljava/lang/Boolean;

    .line 315
    # setter for: Lcom/samsung/index/indexservice/service/SystemInfo;->bSentStopServiceMessage:Z
    invoke-static {v5}, Lcom/samsung/index/indexservice/service/SystemInfo;->access$102(Z)Z

    .line 316
    # setter for: Lcom/samsung/index/indexservice/service/SystemInfo;->bSendStopServiceMessage:Z
    invoke-static {v5}, Lcom/samsung/index/indexservice/service/SystemInfo;->access$202(Z)Z

    .line 318
    sget-object v3, Lcom/samsung/index/indexservice/service/SystemInfo;->tempObj:Ljava/lang/Object;

    monitor-enter v3

    .line 319
    :try_start_0
    const-string/jumbo v2, "SystemInfo"

    const-string/jumbo v4, "DocumentService Resume indexing as [SIOP LEVEL] changed to < 2"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    sget-object v2, Lcom/samsung/index/indexservice/service/SystemInfo;->tempObj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 322
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
