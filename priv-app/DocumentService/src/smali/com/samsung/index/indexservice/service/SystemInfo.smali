.class public Lcom/samsung/index/indexservice/service/SystemInfo;
.super Ljava/lang/Object;
.source "SystemInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/index/indexservice/service/SystemInfo$SIOPReceiver;
    }
.end annotation


# static fields
.field public static final ACTION_CHECK_SIOP_LEVEL:Ljava/lang/String; = "android.intent.action.CHECK_SIOP_LEVEL"

.field static final ERROR:I = -0x1

.field private static final MAX_THREAD_WAIT_TIME:J = 0x2bf20L

.field private static final MIN_MEMORY:J = 0xc8L

.field public static final SIOP_LEVEL_BROADCAST:Ljava/lang/String; = "siop_level_broadcast"

.field public static final SIOP_LIMIT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SystemInfo"

.field public static final THRESHHOLD_BATTERY_LEVEL:I = 0x14

.field public static final THRESHHOLD_STORAGE_LEVEL:I = 0xa

.field private static bSendStopServiceMessage:Z

.field private static bSentStopServiceMessage:Z

.field private static curSIOPLevel:I

.field public static isSIOPHigh:Ljava/lang/Boolean;

.field private static maxMemory:J

.field private static runtime:Ljava/lang/Runtime;

.field public static tempObj:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 30
    sput v0, Lcom/samsung/index/indexservice/service/SystemInfo;->curSIOPLevel:I

    .line 38
    sput-boolean v0, Lcom/samsung/index/indexservice/service/SystemInfo;->bSentStopServiceMessage:Z

    .line 56
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    sput-object v0, Lcom/samsung/index/indexservice/service/SystemInfo;->runtime:Ljava/lang/Runtime;

    .line 61
    sget-object v0, Lcom/samsung/index/indexservice/service/SystemInfo;->runtime:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    const-wide/32 v2, 0x100000

    div-long/2addr v0, v2

    sput-wide v0, Lcom/samsung/index/indexservice/service/SystemInfo;->maxMemory:J

    .line 64
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/index/indexservice/service/SystemInfo;->tempObj:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 285
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 17
    sget v0, Lcom/samsung/index/indexservice/service/SystemInfo;->curSIOPLevel:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 17
    sput p0, Lcom/samsung/index/indexservice/service/SystemInfo;->curSIOPLevel:I

    return p0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 17
    sget-boolean v0, Lcom/samsung/index/indexservice/service/SystemInfo;->bSentStopServiceMessage:Z

    return v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 17
    sput-boolean p0, Lcom/samsung/index/indexservice/service/SystemInfo;->bSentStopServiceMessage:Z

    return p0
.end method

.method static synthetic access$202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 17
    sput-boolean p0, Lcom/samsung/index/indexservice/service/SystemInfo;->bSendStopServiceMessage:Z

    return p0
.end method

.method private static canAllocate(IZ)Z
    .locals 14
    .param p0, "bytes"    # I
    .param p1, "doGC"    # Z

    .prologue
    const-wide/32 v10, 0x100000

    const-wide/16 v12, 0xc8

    const/4 v6, 0x1

    .line 145
    sget-object v7, Lcom/samsung/index/indexservice/service/SystemInfo;->runtime:Ljava/lang/Runtime;

    invoke-virtual {v7}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v8

    div-long v2, v8, v10

    .line 147
    .local v2, "freeMem":J
    sget-object v7, Lcom/samsung/index/indexservice/service/SystemInfo;->runtime:Ljava/lang/Runtime;

    invoke-virtual {v7}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v8

    div-long v4, v8, v10

    .line 149
    .local v4, "totalMem":J
    sub-long v0, v4, v2

    .line 151
    .local v0, "allocMem":J
    const-string/jumbo v7, "SystemInfo"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Total Memory :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " Max Memory :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-wide v10, Lcom/samsung/index/indexservice/service/SystemInfo;->maxMemory:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " Free Memory :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " Alloc Mem :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    int-to-long v8, p0

    sub-long v8, v2, v8

    cmp-long v7, v8, v12

    if-lez v7, :cond_1

    .line 167
    :cond_0
    :goto_0
    return v6

    .line 159
    :cond_1
    sget-wide v8, Lcom/samsung/index/indexservice/service/SystemInfo;->maxMemory:J

    cmp-long v7, v8, v4

    if-lez v7, :cond_2

    .line 160
    sget-wide v8, Lcom/samsung/index/indexservice/service/SystemInfo;->maxMemory:J

    sub-long/2addr v8, v4

    add-long/2addr v2, v8

    .line 161
    int-to-long v8, p0

    sub-long v8, v2, v8

    cmp-long v7, v8, v12

    if-gtz v7, :cond_0

    .line 166
    :cond_2
    const-string/jumbo v6, "SystemInfo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Failed - not enough Available Memory "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static checkSystemStatus(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 73
    invoke-static {p0}, Lcom/samsung/index/indexservice/service/SystemInfo;->setBatteryState(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    const-string/jumbo v1, "SystemInfo"

    const-string/jumbo v2, "[SYSTEM STATUS] Battery is Low:"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :goto_0
    return v0

    .line 77
    :cond_0
    invoke-static {}, Lcom/samsung/index/indexservice/service/SystemInfo;->setStorageState()Z

    move-result v1

    if-nez v1, :cond_1

    .line 78
    const-string/jumbo v1, "SystemInfo"

    const-string/jumbo v2, "[SYSTEM STATUS] Storage is Low:"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 81
    :cond_1
    const-string/jumbo v0, "SystemInfo"

    const-string/jumbo v1, "[SYSTEM STATUS] Battery and Storage levels are OK:"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static checkTemperatureLevel(Ljava/lang/String;)V
    .locals 10
    .param p0, "methodName"    # Ljava/lang/String;

    .prologue
    .line 354
    sget-object v6, Lcom/samsung/index/indexservice/service/SystemInfo;->tempObj:Ljava/lang/Object;

    monitor-enter v6

    .line 356
    :try_start_0
    sget-object v5, Lcom/samsung/index/indexservice/service/SystemInfo;->isSIOPHigh:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 357
    const-string/jumbo v5, "SystemInfo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "DocumentService [SIOP LEVEL] is >= 2, so going to wait mode from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    :cond_0
    :goto_0
    sget-object v5, Lcom/samsung/index/indexservice/service/SystemInfo;->isSIOPHigh:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_3

    .line 364
    :try_start_1
    sget-boolean v5, Lcom/samsung/index/indexservice/service/DocumentService;->bInterruptTemperatureCheck:Z

    if-eqz v5, :cond_1

    .line 365
    new-instance v5, Ljava/lang/InterruptedException;

    invoke-direct {v5}, Ljava/lang/InterruptedException;-><init>()V

    throw v5
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392
    :catch_0
    move-exception v2

    .line 393
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string/jumbo v5, "SystemInfo"

    const-string/jumbo v7, "Temperature check interrupted"

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 398
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 368
    :cond_1
    :try_start_3
    sget-boolean v5, Lcom/samsung/index/indexservice/service/SystemInfo;->bSendStopServiceMessage:Z

    if-eqz v5, :cond_2

    sget-boolean v5, Lcom/samsung/index/indexservice/service/SystemInfo;->bSentStopServiceMessage:Z

    if-nez v5, :cond_2

    .line 369
    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v7}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->createInstance(Landroid/content/Context;Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-result-object v1

    .line 371
    .local v1, "docServiceUtils":Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getDocServiceObj()Lcom/samsung/index/indexservice/service/DocumentService;

    move-result-object v0

    .line 373
    .local v0, "docServiceObj":Lcom/samsung/index/indexservice/service/DocumentService;
    if-eqz v0, :cond_2

    .line 374
    invoke-virtual {v0}, Lcom/samsung/index/indexservice/service/DocumentService;->getDocMsgHandler()Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    move-result-object v3

    .line 376
    .local v3, "mDocumentMessageHandler":Landroid/os/Handler;
    if-eqz v3, :cond_2

    .line 377
    const/4 v5, 0x0

    sput-boolean v5, Lcom/samsung/index/indexservice/service/SystemInfo;->bSendStopServiceMessage:Z

    .line 379
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v4

    .line 380
    .local v4, "msg":Landroid/os/Message;
    const/4 v5, 0x7

    iput v5, v4, Landroid/os/Message;->what:I

    .line 382
    const/4 v5, 0x1

    sput-boolean v5, Lcom/samsung/index/indexservice/service/SystemInfo;->bSentStopServiceMessage:Z

    .line 384
    const-wide/32 v8, 0x2bf20

    invoke-virtual {v3, v4, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 390
    .end local v0    # "docServiceObj":Lcom/samsung/index/indexservice/service/DocumentService;
    .end local v1    # "docServiceUtils":Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    .end local v3    # "mDocumentMessageHandler":Landroid/os/Handler;
    .end local v4    # "msg":Landroid/os/Message;
    :cond_2
    sget-object v5, Lcom/samsung/index/indexservice/service/SystemInfo;->tempObj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->wait()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 397
    :cond_3
    :try_start_4
    sget-object v5, Lcom/samsung/index/indexservice/service/SystemInfo;->tempObj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V

    .line 398
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 399
    return-void
.end method

.method public static externalMemoryAvailable()Z
    .locals 2

    .prologue
    .line 209
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static formatSize(J)Ljava/lang/String;
    .locals 8
    .param p0, "sizeToFormat"    # J

    .prologue
    const-wide/16 v6, 0x400

    .line 254
    const/4 v4, 0x0

    .line 256
    .local v4, "suffix":Ljava/lang/String;
    move-wide v2, p0

    .line 257
    .local v2, "size":J
    cmp-long v5, v2, v6

    if-ltz v5, :cond_0

    .line 258
    const-string/jumbo v4, "KiB"

    .line 259
    div-long/2addr v2, v6

    .line 260
    cmp-long v5, v2, v6

    if-ltz v5, :cond_0

    .line 261
    const-string/jumbo v4, "MiB"

    .line 262
    div-long/2addr v2, v6

    .line 266
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 268
    .local v1, "resultBuffer":Ljava/lang/StringBuilder;
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v0, v5, -0x3

    .line 269
    .local v0, "commaOffset":I
    :goto_0
    if-lez v0, :cond_1

    .line 270
    const/16 v5, 0x2c

    invoke-virtual {v1, v0, v5}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 271
    add-int/lit8 v0, v0, -0x3

    goto :goto_0

    .line 274
    :cond_1
    if-eqz v4, :cond_2

    .line 275
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static getAvailableExternalMemorySize()J
    .locals 8

    .prologue
    .line 230
    invoke-static {}, Lcom/samsung/index/indexservice/service/SystemInfo;->externalMemoryAvailable()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 231
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    .line 232
    .local v4, "path":Ljava/io/File;
    new-instance v5, Landroid/os/StatFs;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 233
    .local v5, "stat":Landroid/os/StatFs;
    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v2

    .line 234
    .local v2, "blockSize":J
    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v0

    .line 235
    .local v0, "availableBlocks":J
    mul-long v6, v0, v2

    .line 237
    :goto_0
    return-wide v6

    .end local v0    # "availableBlocks":J
    .end local v2    # "blockSize":J
    .end local v4    # "path":Ljava/io/File;
    .end local v5    # "stat":Landroid/os/StatFs;
    :cond_0
    const-wide/16 v6, -0x1

    goto :goto_0
.end method

.method public static getAvailableInternalMemorySize()J
    .locals 8

    .prologue
    .line 214
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v4

    .line 215
    .local v4, "path":Ljava/io/File;
    new-instance v5, Landroid/os/StatFs;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 216
    .local v5, "stat":Landroid/os/StatFs;
    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v2

    .line 217
    .local v2, "blockSize":J
    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v0

    .line 218
    .local v0, "availableBlocks":J
    mul-long v6, v0, v2

    return-wide v6
.end method

.method public static getTotalExternalMemorySize()J
    .locals 8

    .prologue
    .line 242
    invoke-static {}, Lcom/samsung/index/indexservice/service/SystemInfo;->externalMemoryAvailable()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 243
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    .line 244
    .local v2, "path":Ljava/io/File;
    new-instance v3, Landroid/os/StatFs;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 245
    .local v3, "stat":Landroid/os/StatFs;
    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v0

    .line 246
    .local v0, "blockSize":J
    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockCountLong()J

    move-result-wide v4

    .line 247
    .local v4, "totalBlocks":J
    mul-long v6, v4, v0

    .line 249
    :goto_0
    return-wide v6

    .end local v0    # "blockSize":J
    .end local v2    # "path":Ljava/io/File;
    .end local v3    # "stat":Landroid/os/StatFs;
    .end local v4    # "totalBlocks":J
    :cond_0
    const-wide/16 v6, -0x1

    goto :goto_0
.end method

.method public static getTotalInternalMemorySize()J
    .locals 8

    .prologue
    .line 222
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v2

    .line 223
    .local v2, "path":Ljava/io/File;
    new-instance v3, Landroid/os/StatFs;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 224
    .local v3, "stat":Landroid/os/StatFs;
    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v0

    .line 225
    .local v0, "blockSize":J
    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockCountLong()J

    move-result-wide v4

    .line 226
    .local v4, "totalBlocks":J
    mul-long v6, v4, v0

    return-wide v6
.end method

.method public static isLowMemory()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 178
    invoke-static {v1, v0}, Lcom/samsung/index/indexservice/service/SystemInfo;->canAllocate(IZ)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static setBatteryState(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, -0x1

    .line 93
    const/4 v3, 0x0

    .line 94
    .local v3, "isBatteryOk":Z
    if-nez p0, :cond_0

    move v4, v3

    .line 115
    .end local v3    # "isBatteryOk":Z
    .local v4, "isBatteryOk":I
    :goto_0
    return v4

    .line 97
    .end local v4    # "isBatteryOk":I
    .restart local v3    # "isBatteryOk":Z
    :cond_0
    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v7, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 98
    .local v2, "ifilter":Landroid/content/IntentFilter;
    const/4 v7, 0x0

    invoke-virtual {p0, v7, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 100
    .local v1, "batteryStatus":Landroid/content/Intent;
    if-nez v1, :cond_1

    .line 101
    const/4 v3, 0x0

    move v4, v3

    .line 102
    .restart local v4    # "isBatteryOk":I
    goto :goto_0

    .line 105
    .end local v4    # "isBatteryOk":I
    :cond_1
    const-string/jumbo v7, "level"

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 106
    .local v5, "level":I
    const-string/jumbo v7, "scale"

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 108
    .local v6, "scale":I
    mul-int/lit8 v7, v5, 0x64

    div-int v0, v7, v6

    .line 110
    .local v0, "batteryPct":I
    const/16 v7, 0x14

    if-lt v0, v7, :cond_2

    .line 111
    const/4 v3, 0x1

    :goto_1
    move v4, v3

    .line 115
    .restart local v4    # "isBatteryOk":I
    goto :goto_0

    .line 113
    .end local v4    # "isBatteryOk":I
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static setSiopLevel()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 331
    const-string/jumbo v0, "sys.siop.level"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/samsung/index/indexservice/service/SystemInfo;->curSIOPLevel:I

    .line 332
    const-string/jumbo v0, "SystemInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[SIOP LEVEL] : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/index/indexservice/service/SystemInfo;->curSIOPLevel:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    sget v0, Lcom/samsung/index/indexservice/service/SystemInfo;->curSIOPLevel:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    .line 334
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/samsung/index/indexservice/service/SystemInfo;->isSIOPHigh:Ljava/lang/Boolean;

    .line 336
    sget-boolean v0, Lcom/samsung/index/indexservice/service/SystemInfo;->bSentStopServiceMessage:Z

    if-nez v0, :cond_0

    .line 337
    sput-boolean v4, Lcom/samsung/index/indexservice/service/SystemInfo;->bSendStopServiceMessage:Z

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 340
    :cond_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/samsung/index/indexservice/service/SystemInfo;->isSIOPHigh:Ljava/lang/Boolean;

    .line 341
    sput-boolean v3, Lcom/samsung/index/indexservice/service/SystemInfo;->bSentStopServiceMessage:Z

    .line 342
    sput-boolean v3, Lcom/samsung/index/indexservice/service/SystemInfo;->bSendStopServiceMessage:Z

    goto :goto_0
.end method

.method private static setStorageState()Z
    .locals 8

    .prologue
    .line 126
    const/4 v2, 0x0

    .line 127
    .local v2, "isStorageOk":Z
    invoke-static {}, Lcom/samsung/index/indexservice/service/SystemInfo;->getTotalInternalMemorySize()J

    move-result-wide v4

    .line 128
    .local v4, "totalMemory":J
    invoke-static {}, Lcom/samsung/index/indexservice/service/SystemInfo;->getAvailableInternalMemorySize()J

    move-result-wide v0

    .line 130
    .local v0, "availMemory":J
    const-wide/16 v6, 0x64

    mul-long/2addr v6, v0

    div-long/2addr v6, v4

    long-to-int v3, v6

    .line 132
    .local v3, "memoryPercent":I
    const/16 v6, 0xa

    if-le v3, v6, :cond_0

    .line 133
    const/4 v2, 0x1

    .line 137
    :goto_0
    return v2

    .line 135
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
