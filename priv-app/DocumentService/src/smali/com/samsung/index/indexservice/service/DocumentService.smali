.class public Lcom/samsung/index/indexservice/service/DocumentService;
.super Landroid/app/Service;
.source "DocumentService.java"

# interfaces
.implements Lcom/samsung/index/IFileIndexedNotifier;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;,
        Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;,
        Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;,
        Lcom/samsung/index/indexservice/service/DocumentService$SystemMonitorReceiver;
    }
.end annotation


# static fields
.field public static final ACTION_DOCUMENT_SERVICE_START:Ljava/lang/String; = "com.sec.android.app.myfiles_demo.ACTION_INDEXING_START"

.field public static final ACTION_INDEXING_RESTART:Ljava/lang/String; = "com.sec.android.app.myfiles_demo.ACTION_INDEXING_RESTART"

.field public static final ACTION_INDEXING_STOP:Ljava/lang/String; = "com.sec.android.app.myfiles_demo.ACTION_INDEXING_STOP"

.field public static final ACTION_SCAN_SINGLE_FILE:Ljava/lang/String; = "action.media.scansingle"

.field public static final ACTION_THUMBNAIL_START:Ljava/lang/String; = "com.sec.android.app.myfiles_demo.ACTION_THUMBNAIL_START"

.field public static final ACTION_USER_SWITCHED:Ljava/lang/String; = "android.intent.action.USER_SWITCHED"

.field public static final APPNAME:Ljava/lang/String; = ".MyFilesContentSearch"

.field public static final DELETE_STATUS:Ljava/lang/String; = "delete_status"

.field public static final ENABLELOGS:Z = true

.field public static ENABLE_HIGHLIGHTTEXT_FEATURE:Z = false

.field public static ENABLE_INDEXING:Z = false

.field public static ENABLE_THUMBNAIL:Z = false

.field public static final EXTRA_USER_HANDLE:Ljava/lang/String; = "android.intent.extra.user_handle"

.field public static final FILE_EXTENSION:Ljava/lang/String; = "fileExtension"

.field public static final FILE_TYPE:Ljava/lang/String; = "fileType"

.field private static final MESSAGE_HANDLE_FILE_SYNC_NEW_SCAN:I = 0x5

.field public static final MESSAGE_HANDLE_FOLDER_PATH:I = 0x4

.field public static final MESSAGE_HANDLE_SIOP_HIGH_TIMER:I = 0x7

.field private static final MESSAGE_HANDLE_THUMBNAIL_TIMER:I = 0x6

.field public static final MODIFIED_TIME:Ljava/lang/String; = "modifiedTime"

.field public static final MYFILES_PKG:Ljava/lang/String; = "com.sec.android.app.myfiles"

.field private static final NONE:I = 0xff

.field public static final SINGLE_FILE_PATH:Ljava/lang/String; = "filePath"

.field private static final START:I = 0x8

.field private static final STOP:I = 0x9

.field private static final TAG:Ljava/lang/String; = "DocumentService"

.field public static bInterruptTemperatureCheck:Z

.field private static indexingStart:Ljava/lang/Thread;

.field private static mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

.field public static sSyncStartCleanupObj:Ljava/lang/Object;

.field public static startTime:J


# instance fields
.field beginIndexing:Ljava/lang/Runnable;

.field private mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

.field mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

.field mHandlerThread:Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;

.field private mIndexMgr:Lcom/samsung/index/ContentIndex;

.field private mSIOPReceiver:Lcom/samsung/index/indexservice/service/SystemInfo$SIOPReceiver;

.field private mSystemMonitorReceiver:Lcom/samsung/index/indexservice/service/DocumentService$SystemMonitorReceiver;

.field private mThreadMgr:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

.field private mUserSwitchReceiver:Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;

.field private mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 51
    sput-boolean v0, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_HIGHLIGHTTEXT_FEATURE:Z

    .line 56
    sput-boolean v0, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_INDEXING:Z

    .line 61
    sput-boolean v0, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    .line 157
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentService;->sSyncStartCleanupObj:Ljava/lang/Object;

    .line 179
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/index/indexservice/service/DocumentService;->bInterruptTemperatureCheck:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 151
    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    .line 186
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentService$1;

    invoke-direct {v0, p0}, Lcom/samsung/index/indexservice/service/DocumentService$1;-><init>(Lcom/samsung/index/indexservice/service/DocumentService;)V

    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->beginIndexing:Ljava/lang/Runnable;

    .line 274
    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mUserSwitchReceiver:Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;

    .line 455
    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mHandlerThread:Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;

    .line 456
    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    .line 484
    return-void
.end method

.method static synthetic access$000()Lcom/samsung/index/indexservice/service/DocumentServiceState;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/index/indexservice/service/DocumentService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/index/indexservice/service/DocumentService;
    .param p1, "x1"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/index/indexservice/service/DocumentService;->beginIndexing(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/index/indexservice/service/DocumentService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/index/indexservice/service/DocumentService;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->setFlagsToStopService()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/index/indexservice/service/DocumentService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/index/indexservice/service/DocumentService;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->isMediaScannerRunning()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/index/indexservice/service/DocumentService;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/index/indexservice/service/DocumentService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/index/indexservice/service/DocumentService;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->cleanupEverything()V

    return-void
.end method

.method private beginIndexing(I)V
    .locals 8
    .param p1, "which"    # I

    .prologue
    const/4 v7, 0x0

    .line 302
    const/4 v0, 0x0

    .line 304
    .local v0, "isProcessing":Z
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "[BEGIN SYNC] DocumentService beginIndexing :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "doc_service_pref"

    invoke-virtual {v4, v5, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 314
    .local v1, "myShPref":Landroid/content/SharedPreferences;
    const-string/jumbo v4, "IsIndexingFirstTime"

    const-string/jumbo v5, "YES"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 317
    .local v3, "strIsFirstTime":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-nez v4, :cond_2

    .line 318
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->stopService()V

    goto :goto_0

    .line 322
    :cond_2
    const-string/jumbo v4, "YES"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 323
    const-string/jumbo v4, "DocumentService"

    const-string/jumbo v5, "DocumentService beginIndexing starting indexing first time"

    invoke-static {v4, v5}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    iget-object v4, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    const/4 v5, 0x1

    invoke-virtual {v4, v5, p1}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->syncFilesWithFileSystem(ZI)Z

    move-result v0

    .line 328
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 329
    .local v2, "myShPrefEditor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v4, "IsIndexingFirstTime"

    const-string/jumbo v5, "NO"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 330
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 338
    .end local v2    # "myShPrefEditor":Landroid/content/SharedPreferences$Editor;
    :goto_1
    if-nez v0, :cond_5

    .line 339
    const/16 v4, 0x11

    if-ne p1, v4, :cond_4

    .line 340
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->thumbnailCompleted()V

    goto :goto_0

    .line 334
    :cond_3
    iget-object v4, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v4, v7, p1}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->syncFilesWithFileSystem(ZI)Z

    move-result v0

    goto :goto_1

    .line 341
    :cond_4
    const/16 v4, 0x10

    if-ne p1, v4, :cond_0

    .line 342
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->indexCompleted()V

    goto :goto_0

    .line 346
    :cond_5
    sget-object v4, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->acquireWakeLock(Landroid/content/Context;)V

    goto/16 :goto_0
.end method

.method private checkBuildProduct()V
    .locals 2

    .prologue
    .line 829
    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 830
    .local v0, "product":Ljava/lang/String;
    const-string/jumbo v1, "t"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 831
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/index/indexservice/service/DocumentService;->enableFeatures(Z)V

    .line 835
    :goto_0
    return-void

    .line 833
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/index/indexservice/service/DocumentService;->enableFeatures(Z)V

    goto :goto_0
.end method

.method private checkMyFilesInstalledStatus()V
    .locals 4

    .prologue
    .line 816
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 818
    .local v1, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string/jumbo v2, "com.sec.android.app.myfiles"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 823
    :goto_0
    return-void

    .line 820
    :catch_0
    move-exception v0

    .line 821
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/samsung/index/indexservice/service/DocumentService;->enableFeatures(Z)V

    goto :goto_0
.end method

.method private declared-synchronized cleanupEverything()V
    .locals 5

    .prologue
    .line 1003
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentService;->sSyncStartCleanupObj:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1005
    :try_start_1
    const-string/jumbo v1, "DocumentService"

    const-string/jumbo v3, "DocumentService cleanupEverything start"

    invoke-static {v1, v3}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-eqz v1, :cond_0

    .line 1009
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->deleteIndexedFileIfStopped()V

    .line 1014
    :cond_0
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    if-eqz v1, :cond_1

    .line 1015
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v1}, Lcom/samsung/index/ContentIndex;->closeReader()V

    .line 1016
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v1}, Lcom/samsung/index/ContentIndex;->commitAfterDelete()V

    .line 1017
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v1}, Lcom/samsung/index/ContentIndex;->closeWriter()V

    .line 1021
    :cond_1
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mThreadMgr:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    if-eqz v1, :cond_2

    .line 1022
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mThreadMgr:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->clearQueue()V

    .line 1027
    :cond_2
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentService;->indexingStart:Ljava/lang/Thread;

    if-eqz v1, :cond_3

    .line 1028
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentService;->indexingStart:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 1029
    const/4 v1, 0x0

    sput-object v1, Lcom/samsung/index/indexservice/service/DocumentService;->indexingStart:Ljava/lang/Thread;

    .line 1034
    :cond_3
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    if-eqz v1, :cond_4

    .line 1036
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->removeMessages(I)V

    .line 1038
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->removeMessages(I)V

    .line 1041
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    const/4 v3, 0x7

    invoke-virtual {v1, v3}, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->removeMessages(I)V

    .line 1044
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->removeTimerHandlerMessage()V

    .line 1045
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    .line 1047
    :cond_4
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mHandlerThread:Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;

    if-eqz v1, :cond_5

    .line 1048
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mHandlerThread:Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;->interrupt()V

    .line 1049
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mHandlerThread:Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;

    .line 1053
    :cond_5
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    if-eqz v1, :cond_6

    .line 1054
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v1}, Lcom/samsung/index/ContentIndex;->closeNow()V

    .line 1055
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    .line 1058
    :cond_6
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-eqz v1, :cond_7

    .line 1059
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->clearSingleton()V

    .line 1060
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    .line 1062
    :cond_7
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1065
    :try_start_2
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->nextState(IZLandroid/content/Context;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v1

    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->START_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v1, v2, :cond_8

    .line 1067
    const-string/jumbo v1, "DocumentService"

    const-string/jumbo v2, "DocumentService is process the pending files at end"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1070
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1071
    .local v0, "intentService":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.app.myfiles_demo.ACTION_INDEXING_START"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1072
    invoke-virtual {p0, v0}, Lcom/samsung/index/indexservice/service/DocumentService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1077
    .end local v0    # "intentService":Landroid/content/Intent;
    :goto_0
    monitor-exit p0

    return-void

    .line 1062
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1003
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1075
    :cond_8
    :try_start_5
    const-string/jumbo v1, "DocumentService"

    const-string/jumbo v2, "DocumentService cleanupEverything end"

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1076
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->killProcess()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0
.end method

.method private enableFeatures(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 838
    sput-boolean p1, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    .line 839
    sput-boolean p1, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_HIGHLIGHTTEXT_FEATURE:Z

    .line 840
    return-void
.end method

.method private getMode(Landroid/content/Intent;)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v1, 0xff

    .line 430
    if-eqz p1, :cond_0

    .line 431
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 433
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 452
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 437
    .restart local v0    # "action":Ljava/lang/String;
    :cond_1
    const-string/jumbo v2, "com.sec.android.app.myfiles_demo.ACTION_INDEXING_START"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 438
    const/16 v1, 0x8

    goto :goto_0

    .line 445
    :cond_2
    const-string/jumbo v2, "com.sec.android.app.myfiles_demo.ACTION_INDEXING_STOP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 446
    const/16 v1, 0x9

    goto :goto_0
.end method

.method private handleCommand(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 363
    invoke-direct {p0, p1}, Lcom/samsung/index/indexservice/service/DocumentService;->getMode(Landroid/content/Intent;)I

    move-result v0

    .line 373
    .local v0, "mode":I
    packed-switch v0, :pswitch_data_0

    .line 421
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 380
    :pswitch_1
    const-string/jumbo v2, "STOP_INDEXING"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 381
    .local v1, "val":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string/jumbo v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 382
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    if-eqz v2, :cond_0

    .line 383
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v2}, Lcom/samsung/index/ContentIndex;->shutDownIndexing()V

    goto :goto_0

    .line 386
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->stopService()V

    goto :goto_0

    .line 373
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private init()V
    .locals 5

    .prologue
    .line 871
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/index/DocumentDatabaseManager;->getInstance(Landroid/content/Context;)Lcom/samsung/index/DocumentDatabaseManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 882
    :try_start_1
    sget-boolean v1, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_HIGHLIGHTTEXT_FEATURE:Z

    if-eqz v1, :cond_2

    .line 883
    new-instance v1, Lcom/samsung/index/ContentIndex;

    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, ".MyFilesContentSearch"

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/index/ContentIndex;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 892
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->createInstance(Landroid/content/Context;Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    .line 895
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->registerReceivers()V

    .line 901
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    if-nez v1, :cond_0

    .line 902
    new-instance v1, Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;

    const-string/jumbo v2, "DocumentMessageHandler"

    invoke-direct {v1, p0, v2}, Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;-><init>(Lcom/samsung/index/indexservice/service/DocumentService;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mHandlerThread:Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;

    .line 903
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mHandlerThread:Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;->start()V

    .line 906
    :cond_0
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mThreadMgr:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-eqz v1, :cond_1

    .line 907
    new-instance v1, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mThreadMgr:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    .line 910
    :cond_1
    :goto_1
    return-void

    .line 873
    :catch_0
    move-exception v0

    .line 874
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception in getting db manager handle: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->stopService()V

    goto :goto_1

    .line 886
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_2
    new-instance v1, Lcom/samsung/index/ContentIndex;

    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, ".MyFilesContentSearch"

    invoke-direct {v1, v2, v3}, Lcom/samsung/index/ContentIndex;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 888
    :catch_1
    move-exception v0

    .line 889
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ERROR: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private isMediaScannerRunning()Z
    .locals 10

    .prologue
    .line 572
    const/4 v8, 0x0

    .line 573
    .local v8, "isRunning":Z
    const/4 v6, 0x0

    .line 575
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Landroid/provider/MediaStore;->getMediaScannerUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "volume"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 579
    if-eqz v6, :cond_0

    .line 580
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 582
    .local v9, "volumeName":Ljava/lang/String;
    if-eqz v9, :cond_0

    .line 583
    const/4 v8, 0x1

    .line 590
    .end local v9    # "volumeName":Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_1

    .line 591
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 593
    :cond_1
    :goto_0
    return v8

    .line 587
    :catch_0
    move-exception v7

    .line 588
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v0, "DocumentService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590
    if-eqz v6, :cond_1

    .line 591
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 590
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 591
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private registerReceivers()V
    .locals 6

    .prologue
    .line 916
    invoke-static {}, Lcom/samsung/index/indexservice/service/SystemInfo;->setSiopLevel()V

    .line 919
    new-instance v4, Lcom/samsung/index/indexservice/service/SystemInfo$SIOPReceiver;

    invoke-direct {v4}, Lcom/samsung/index/indexservice/service/SystemInfo$SIOPReceiver;-><init>()V

    iput-object v4, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mSIOPReceiver:Lcom/samsung/index/indexservice/service/SystemInfo$SIOPReceiver;

    .line 920
    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v4, "android.intent.action.CHECK_SIOP_LEVEL"

    invoke-direct {v1, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 921
    .local v1, "mSiopLevelFilter":Landroid/content/IntentFilter;
    iget-object v4, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mSIOPReceiver:Lcom/samsung/index/indexservice/service/SystemInfo$SIOPReceiver;

    invoke-virtual {p0, v4, v1}, Lcom/samsung/index/indexservice/service/DocumentService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 924
    new-instance v4, Lcom/samsung/index/indexservice/service/DocumentService$SystemMonitorReceiver;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/samsung/index/indexservice/service/DocumentService$SystemMonitorReceiver;-><init>(Lcom/samsung/index/indexservice/service/DocumentService;Lcom/samsung/index/indexservice/service/DocumentService$1;)V

    iput-object v4, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mSystemMonitorReceiver:Lcom/samsung/index/indexservice/service/DocumentService$SystemMonitorReceiver;

    .line 925
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v4, "android.intent.action.BATTERY_LOW"

    invoke-direct {v0, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 927
    .local v0, "batteryStateFilter":Landroid/content/IntentFilter;
    iget-object v4, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mSystemMonitorReceiver:Lcom/samsung/index/indexservice/service/DocumentService$SystemMonitorReceiver;

    invoke-virtual {p0, v4, v0}, Lcom/samsung/index/indexservice/service/DocumentService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 929
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 931
    .local v2, "storageStateFilter":Landroid/content/IntentFilter;
    const-string/jumbo v4, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 933
    iget-object v4, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mSystemMonitorReceiver:Lcom/samsung/index/indexservice/service/DocumentService$SystemMonitorReceiver;

    invoke-virtual {p0, v4, v2}, Lcom/samsung/index/indexservice/service/DocumentService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v3

    .line 935
    .local v3, "storageStatus":Landroid/content/Intent;
    if-eqz v3, :cond_0

    .line 936
    const-string/jumbo v4, "DocumentService"

    const-string/jumbo v5, "DocumentService storageStatus registered"

    invoke-static {v4, v5}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    :cond_0
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-lt v4, v5, :cond_1

    .line 941
    new-instance v4, Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;

    invoke-direct {v4, p0}, Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;-><init>(Lcom/samsung/index/indexservice/service/DocumentService;)V

    iput-object v4, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mUserSwitchReceiver:Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;

    .line 943
    :cond_1
    return-void
.end method

.method private removeDeletedFiles()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 608
    iget-object v6, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-nez v6, :cond_1

    .line 658
    :cond_0
    :goto_0
    return v5

    .line 611
    :cond_1
    const/4 v0, 0x0

    .line 613
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    invoke-virtual {v6}, Lcom/samsung/index/DocumentDatabaseManager;->getDeletedFileList()Landroid/database/Cursor;

    move-result-object v0

    .line 614
    if-eqz v0, :cond_a

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 615
    const-string/jumbo v6, "DocumentService"

    const-string/jumbo v7, "[REMOVE ENTRY] removeDeletedFiles"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    :cond_2
    sget-boolean v6, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    if-eqz v6, :cond_9

    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/index/indexservice/service/SystemInfo;->checkSystemStatus(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 619
    sget-object v6, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    const/16 v7, 0x11

    invoke-virtual {v6, v7}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v3

    .line 621
    .local v3, "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    sget-object v6, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_THUMBNAIL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-eq v3, v6, :cond_3

    sget-object v6, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_INDEXING:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v3, v6, :cond_9

    .line 623
    :cond_3
    const-string/jumbo v5, "DocumentService"

    const-string/jumbo v6, "[DELETE]Document service is processing the pending files..."

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 627
    .local v1, "msg":Landroid/os/Message;
    sget-boolean v5, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-eqz v5, :cond_6

    .line 628
    const/16 v5, 0x11

    iput v5, v1, Landroid/os/Message;->arg1:I

    .line 632
    :goto_1
    const/4 v5, 0x5

    iput v5, v1, Landroid/os/Message;->what:I

    .line 633
    iget-object v5, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    if-eqz v5, :cond_4

    .line 634
    iget-object v5, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    invoke-virtual {v5, v1}, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 636
    :cond_4
    const/4 v5, 0x0

    .line 650
    if-eqz v0, :cond_5

    .line 651
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 654
    :cond_5
    iget-object v6, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    if-eqz v6, :cond_0

    .line 655
    iget-object v6, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v6}, Lcom/samsung/index/ContentIndex;->commitAfterDelete()V

    goto :goto_0

    .line 630
    :cond_6
    const/16 v5, 0x10

    :try_start_1
    iput v5, v1, Landroid/os/Message;->arg1:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 650
    .end local v1    # "msg":Landroid/os/Message;
    .end local v3    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_7

    .line 651
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 654
    :cond_7
    iget-object v6, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    if-eqz v6, :cond_8

    .line 655
    iget-object v6, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v6}, Lcom/samsung/index/ContentIndex;->commitAfterDelete()V

    :cond_8
    throw v5

    .line 639
    :cond_9
    :try_start_2
    const-string/jumbo v6, "thumbnail_data"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 642
    .local v4, "thumbnailPath":Ljava/lang/String;
    const-string/jumbo v6, "file_path"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 644
    .local v2, "orgFilePath":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v6, v2, v4}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->deleteFileEntry(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v6

    if-nez v6, :cond_2

    .line 650
    .end local v2    # "orgFilePath":Ljava/lang/String;
    .end local v4    # "thumbnailPath":Ljava/lang/String;
    :cond_a
    if-eqz v0, :cond_b

    .line 651
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 654
    :cond_b
    iget-object v6, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    if-eqz v6, :cond_0

    .line 655
    iget-object v6, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v6}, Lcom/samsung/index/ContentIndex;->commitAfterDelete()V

    goto/16 :goto_0
.end method

.method private setFlagsToStopService()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 280
    sput-boolean v1, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bStopDocService:Z

    .line 281
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setStopThumbnail(Z)V

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    if-eqz v0, :cond_1

    .line 285
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v0}, Lcom/samsung/index/ContentIndex;->setStopIndexing()V

    .line 287
    :cond_1
    return-void
.end method

.method private unregisterReceivers()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 973
    :try_start_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_0

    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mUserSwitchReceiver:Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;

    if-eqz v2, :cond_0

    .line 975
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mUserSwitchReceiver:Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;

    invoke-virtual {v2}, Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;->unregisterSelf()V

    .line 976
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mUserSwitchReceiver:Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 983
    :cond_0
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mSystemMonitorReceiver:Lcom/samsung/index/indexservice/service/DocumentService$SystemMonitorReceiver;

    invoke-virtual {p0, v2}, Lcom/samsung/index/indexservice/service/DocumentService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 987
    :goto_1
    iput-object v5, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mSystemMonitorReceiver:Lcom/samsung/index/indexservice/service/DocumentService$SystemMonitorReceiver;

    .line 990
    :try_start_2
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mSIOPReceiver:Lcom/samsung/index/indexservice/service/SystemInfo$SIOPReceiver;

    invoke-virtual {p0, v2}, Lcom/samsung/index/indexservice/service/DocumentService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 994
    :goto_2
    iput-object v5, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mSIOPReceiver:Lcom/samsung/index/indexservice/service/SystemInfo$SIOPReceiver;

    .line 995
    return-void

    .line 978
    :catch_0
    move-exception v1

    .line 979
    .local v1, "e1":Ljava/lang/Exception;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception in un-registering mUserSwitchReceiver :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 984
    .end local v1    # "e1":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 985
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception in un-registering mSystemMonitorReceiver:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 991
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 992
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception in un-registering SIOP receiver:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private verifyThumbnailImages()Z
    .locals 22

    .prologue
    .line 707
    const-string/jumbo v19, "DocumentService"

    const-string/jumbo v20, "[VERIFY] verifyThumbnailImages"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    const/4 v9, 0x0

    .line 709
    .local v9, "isStarted":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/service/DocumentService;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    move-object/from16 v19, v0

    if-nez v19, :cond_0

    .line 710
    const/16 v19, 0x0

    .line 774
    :goto_0
    return v19

    .line 712
    :cond_0
    new-instance v17, Ljava/util/HashSet;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashSet;-><init>()V

    .line 713
    .local v17, "thumbnailSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ThumbnailUtils;->getThumbnailPath(Landroid/content/Context;)Ljava/io/File;

    move-result-object v15

    .line 716
    .local v15, "thumbnailFiles":Ljava/io/File;
    if-eqz v15, :cond_1

    .line 717
    invoke-virtual {v15}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 718
    .local v6, "files":[Ljava/io/File;
    if-eqz v6, :cond_1

    .line 719
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    array-length v0, v6

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v7, v0, :cond_1

    .line 720
    aget-object v19, v6, v7

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    .line 721
    .local v13, "path":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-interface {v0, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 719
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 726
    .end local v6    # "files":[Ljava/io/File;
    .end local v7    # "i":I
    .end local v13    # "path":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/service/DocumentService;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/index/DocumentDatabaseManager;->getAllThumbnailPath()Landroid/database/Cursor;

    move-result-object v3

    .line 727
    .local v3, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 728
    .restart local v7    # "i":I
    if-eqz v3, :cond_5

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v19

    if-eqz v19, :cond_5

    .line 730
    :cond_2
    const-string/jumbo v19, "thumbnail_data"

    move-object/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 733
    .local v16, "thumbnailPath":Ljava/lang/String;
    const-string/jumbo v19, "file_path"

    move-object/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 736
    .local v12, "orgFilePath":Ljava/lang/String;
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v8

    .line 737
    .local v8, "isPresent":Z
    if-nez v8, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-object/from16 v19, v0

    if-eqz v19, :cond_4

    .line 738
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 739
    .local v11, "orgFile":Ljava/io/File;
    const/4 v14, 0x0

    .line 740
    .local v14, "started":Z
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v19

    if-eqz v19, :cond_4

    .line 741
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-object/from16 v19, v0

    const/16 v20, 0x11

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v12, v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->processDocument(Ljava/lang/String;II)Z

    move-result v14

    .line 743
    if-eqz v14, :cond_4

    .line 744
    if-nez v7, :cond_3

    .line 745
    sget-object v19, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    const/16 v20, 0x14

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 746
    add-int/lit8 v7, v7, 0x1

    .line 748
    :cond_3
    const/4 v9, 0x1

    .line 752
    .end local v11    # "orgFile":Ljava/io/File;
    .end local v14    # "started":Z
    :cond_4
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v19

    if-nez v19, :cond_2

    .line 755
    .end local v8    # "isPresent":Z
    .end local v12    # "orgFilePath":Ljava/lang/String;
    .end local v16    # "thumbnailPath":Ljava/lang/String;
    :cond_5
    if-eqz v3, :cond_6

    .line 756
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 759
    :cond_6
    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->size()I

    move-result v19

    if-lez v19, :cond_8

    .line 760
    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 761
    .local v10, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_7
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_8

    .line 762
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 764
    .local v18, "val":Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, v18

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 766
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v19

    if-nez v19, :cond_7

    .line 767
    const-string/jumbo v19, "DocumentService"

    const-string/jumbo v20, "Could not delete file"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 768
    .end local v5    # "file":Ljava/io/File;
    :catch_0
    move-exception v4

    .line 769
    .local v4, "e":Ljava/lang/Exception;
    const-string/jumbo v19, "DocumentService"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "Exception in deleting the File :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .end local v4    # "e":Ljava/lang/Exception;
    .end local v10    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v18    # "val":Ljava/lang/String;
    :cond_8
    move/from16 v19, v9

    .line 774
    goto/16 :goto_0
.end method


# virtual methods
.method getDocMsgHandler()Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    return-object v0
.end method

.method public getIndexedDocCount()I
    .locals 5

    .prologue
    .line 850
    const/4 v0, -0x1

    .line 852
    .local v0, "count":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    if-eqz v2, :cond_0

    .line 853
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v2}, Lcom/samsung/index/ContentIndex;->getNumDocsIndexed()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 862
    :cond_0
    :goto_0
    return v0

    .line 855
    :catch_0
    move-exception v1

    .line 859
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getServiceDataBase()Lcom/samsung/index/DocumentDatabaseManager;
    .locals 1

    .prologue
    .line 1094
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    return-object v0
.end method

.method public getServiceIndexManager()Lcom/samsung/index/ContentIndex;
    .locals 1

    .prologue
    .line 1098
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    return-object v0
.end method

.method public indexCompleted()V
    .locals 5

    .prologue
    const/16 v4, 0x10

    .line 1118
    sget-boolean v1, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bStopDocService:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-eqz v1, :cond_0

    .line 1119
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setStopThumbnail(Z)V

    .line 1124
    :cond_0
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v4, v2, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->nextState(IZLandroid/content/Context;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v1

    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_THUMBNAIL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v1, v2, :cond_3

    .line 1126
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1127
    .local v0, "msg":Landroid/os/Message;
    sget-boolean v1, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-eqz v1, :cond_2

    .line 1128
    const/16 v1, 0x11

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 1132
    :goto_0
    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1133
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    if-eqz v1, :cond_1

    .line 1134
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    invoke-virtual {v1, v0}, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1139
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    :goto_1
    return-void

    .line 1130
    .restart local v0    # "msg":Landroid/os/Message;
    :cond_2
    iput v4, v0, Landroid/os/Message;->arg1:I

    goto :goto_0

    .line 1136
    .end local v0    # "msg":Landroid/os/Message;
    :cond_3
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v1

    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->STOPPING_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v1, v2, :cond_1

    .line 1137
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->stopService()V

    goto :goto_1
.end method

.method public declared-synchronized indexCorrupted()V
    .locals 1

    .prologue
    .line 1143
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-eqz v0, :cond_0

    .line 1144
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->resetAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1146
    :cond_0
    monitor-exit p0

    return-void

    .line 1143
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public indexedFile(Ljava/lang/String;I)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "indexStatus"    # I

    .prologue
    .line 1112
    invoke-virtual {p0, p1, p2}, Lcom/samsung/index/indexservice/service/DocumentService;->updateIndexedEntryInDB(Ljava/lang/String;I)V

    .line 1113
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 779
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 784
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 785
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentService;->sSyncStartCleanupObj:Ljava/lang/Object;

    monitor-enter v1

    .line 786
    const/4 v0, 0x0

    :try_start_0
    sput-boolean v0, Lcom/samsung/index/indexservice/service/DocumentService;->bInterruptTemperatureCheck:Z

    .line 787
    const-string/jumbo v0, "DocumentService"

    const-string/jumbo v2, "DocumentService onCreate ... service"

    invoke-static {v0, v2}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v0

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    .line 792
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->checkBuildProduct()V

    .line 795
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->checkMyFilesInstalledStatus()V

    .line 798
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    const/16 v2, 0x13

    invoke-virtual {v0, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 800
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->init()V

    .line 802
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/samsung/index/indexservice/service/DocumentService;->startTime:J

    .line 805
    new-instance v0, Ljava/lang/Thread;

    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService;->beginIndexing:Ljava/lang/Runnable;

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentService;->indexingStart:Ljava/lang/Thread;

    .line 806
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentService;->indexingStart:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 808
    monitor-exit v1

    .line 809
    return-void

    .line 808
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 947
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 948
    const-string/jumbo v1, "DocumentService"

    const-string/jumbo v2, "DocumentService onDestroy start"

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->releaseWakelock()V

    .line 952
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/index/indexservice/service/DocumentService$2;

    invoke-direct {v1, p0}, Lcom/samsung/index/indexservice/service/DocumentService$2;-><init>(Lcom/samsung/index/indexservice/service/DocumentService;)V

    const-string/jumbo v2, "DocumentServiceCleanupThread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 965
    .local v0, "cleanupThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 968
    const-string/jumbo v1, "DocumentService"

    const-string/jumbo v2, "DocumentService onDestroy end"

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    return-void
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 291
    const-string/jumbo v0, "DocumentService"

    const-string/jumbo v1, "[LOW MEMORY] DocumentService onLowMemory()...service"

    invoke-static {v0, v1}, Lcom/samsung/index/indexservice/Slog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    invoke-super {p0}, Landroid/app/Service;->onLowMemory()V

    .line 295
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->setFlagsToStopService()V

    .line 296
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 353
    if-eqz p1, :cond_0

    .line 354
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 356
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->checkMyFilesInstalledStatus()V

    .line 357
    invoke-direct {p0, p1}, Lcom/samsung/index/indexservice/service/DocumentService;->handleCommand(Landroid/content/Intent;)V

    .line 359
    :cond_0
    const/4 v0, 0x2

    return v0
.end method

.method public removeTimerHandlerMessage()V
    .locals 2

    .prologue
    .line 1085
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    if-eqz v0, :cond_0

    .line 1086
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->removeMessages(I)V

    .line 1089
    :cond_0
    return-void
.end method

.method public declared-synchronized start2003FileFormatOnly()Z
    .locals 1

    .prologue
    .line 1167
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mThreadMgr:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    if-eqz v0, :cond_0

    .line 1170
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mThreadMgr:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->startThumbnailFor2003Format()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1171
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->startIndexinginParallel()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1174
    :cond_0
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 1167
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public startIndexinginParallel()V
    .locals 2

    .prologue
    .line 1181
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    invoke-virtual {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v0

    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_INDEXING_THUMBNAIL_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v0, v1, :cond_0

    sget-boolean v0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->bIsPowerConnected:Z

    if-eqz v0, :cond_0

    .line 1183
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/samsung/index/indexservice/service/DocumentService;->beginIndexing(I)V

    .line 1185
    :cond_0
    return-void
.end method

.method public startThumbnailGenerator(Ljava/lang/String;)V
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1154
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1155
    .local v0, "file":Ljava/io/File;
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mThreadMgr:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    if-eqz v1, :cond_0

    .line 1156
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mThreadMgr:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {v1, v0, p0}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->processThumbnailCreation(Ljava/io/File;Lcom/samsung/index/IFileIndexedNotifier;)Z

    .line 1158
    :cond_0
    return-void
.end method

.method public stopService()V
    .locals 8

    .prologue
    .line 665
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v0

    .line 666
    .local v0, "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PRE_SERVICE_STOP:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->removeDeletedFiles()Z

    move-result v1

    if-nez v1, :cond_1

    .line 699
    :cond_0
    :goto_0
    return-void

    .line 671
    :cond_1
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PRE_SERVICE_STOP:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v0, v1, :cond_2

    sget-boolean v1, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->verifyThumbnailImages()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 672
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mThreadMgr:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    if-eqz v1, :cond_0

    .line 673
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mThreadMgr:Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/threads/ThumbnailParserThreadsManager;->startThumbnailFor2003Format()Z

    goto :goto_0

    .line 680
    :cond_2
    const-string/jumbo v1, "DocumentService"

    const-string/jumbo v2, "[STOP DOCUMENTSERVICE] Attempting to stop the Service"

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "[THUMBNAIL AND INDEX] Thumbnail and indexing is Completed Total Time taken for both :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-wide v6, Lcom/samsung/index/indexservice/service/DocumentService;->startTime:J

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-eqz v1, :cond_3

    .line 685
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->resetAllFileCounts()V

    .line 686
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "[THUMBNAIL] Total Time taken for each file :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getThumbTotalTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    const-string/jumbo v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "[INDEX] Total time taken for each file :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getIndexTotalTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    :cond_3
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mHandlerThread:Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;

    if-eqz v1, :cond_4

    .line 693
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mHandlerThread:Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;->interrupt()V

    .line 694
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mHandlerThread:Lcom/samsung/index/indexservice/service/DocumentService$DocHandlerThread;

    .line 696
    :cond_4
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->unregisterReceivers()V

    .line 698
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->stopSelf()V

    goto/16 :goto_0
.end method

.method public thumbnailCompleted()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0x11

    .line 1190
    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v4, v5, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->nextState(IZLandroid/content/Context;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v0

    .line 1192
    .local v0, "curState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_THUMBNAIL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v0, v2, :cond_1

    .line 1194
    const-string/jumbo v2, "DocumentService"

    const-string/jumbo v3, "[THUMBNAIL] Document service is processing the pending files..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 1198
    .local v1, "msg":Landroid/os/Message;
    iput v4, v1, Landroid/os/Message;->arg1:I

    .line 1199
    const/4 v2, 0x5

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1200
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    if-eqz v2, :cond_0

    .line 1201
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocumentMessageHandler:Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;

    invoke-virtual {v2, v1}, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1223
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 1204
    :cond_1
    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    invoke-virtual {v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v0

    .line 1209
    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_INDEXING:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v0, v2, :cond_4

    .line 1210
    sget-boolean v2, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->bIsPowerConnected:Z

    if-ne v2, v5, :cond_3

    .line 1211
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-eqz v2, :cond_2

    .line 1212
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v2}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->resetAllFileCounts()V

    .line 1214
    :cond_2
    const/16 v2, 0x10

    invoke-direct {p0, v2}, Lcom/samsung/index/indexservice/service/DocumentService;->beginIndexing(I)V

    goto :goto_0

    .line 1216
    :cond_3
    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    invoke-virtual {v2, v4}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 1217
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->stopService()V

    goto :goto_0

    .line 1219
    :cond_4
    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->STOPPING_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v0, v2, :cond_0

    .line 1220
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentService;->stopService()V

    goto :goto_0
.end method

.method public thumbnailGeneratedFile(Ljava/lang/String;ILjava/lang/String;)V
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "thumbnailPath"    # Ljava/lang/String;

    .prologue
    .line 1228
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    if-eqz v0, :cond_0

    .line 1230
    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    const/4 v2, -0x2

    const/4 v3, 0x1

    move-object v1, p1

    move v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/index/DocumentDatabaseManager;->updateEntryInDB(Ljava/lang/String;IZILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1237
    :cond_0
    :goto_0
    return-void

    .line 1233
    :catch_0
    move-exception v6

    .line 1234
    .local v6, "e":Ljava/lang/Exception;
    const-string/jumbo v0, "DocumentService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized updateIndexedEntryInDB(Ljava/lang/String;I)V
    .locals 3
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "indexStatus"    # I

    .prologue
    .line 1105
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    if-eqz v0, :cond_0

    .line 1106
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentService;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    const/4 v1, 0x0

    const/4 v2, -0x2

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/samsung/index/DocumentDatabaseManager;->updateEntryInDB(Ljava/lang/String;IZI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1108
    :cond_0
    monitor-exit p0

    return-void

    .line 1105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
