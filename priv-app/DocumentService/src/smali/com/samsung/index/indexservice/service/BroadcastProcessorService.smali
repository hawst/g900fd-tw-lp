.class public Lcom/samsung/index/indexservice/service/BroadcastProcessorService;
.super Landroid/app/IntentService;
.source "BroadcastProcessorService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/index/indexservice/service/BroadcastProcessorService$StartServiceTimerTask;
    }
.end annotation


# static fields
.field private static final ACTION_BROADCAST:Ljava/lang/String; = "broadcast_receiver"

.field private static final ACTION_START_SERVICE_FROM_PROVIDER:Ljava/lang/String; = "action.documentservice.start"

.field public static final EXTRA_FINISHED_SCANNING_SINGLE_FILE_PATH:Ljava/lang/String; = "file.path"

.field private static final INTENT_SCAN_SINGLE_FILE:Ljava/lang/String; = "action.media.scansingle"

.field private static final TAG:Ljava/lang/String; = "BroadcastProcessorService"

.field public static bIsPowerConnected:Z


# instance fields
.field private mServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->bIsPowerConnected:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 70
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->setIntentRedelivery(Z)V

    .line 72
    return-void
.end method

.method private declared-synchronized controlSingleIndex(Ljava/lang/String;)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 367
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/index/indexservice/service/SystemInfo;->checkSystemStatus(Landroid/content/Context;)Z

    move-result v1

    .line 368
    .local v1, "isSystemOk":Z
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v2

    .line 369
    .local v2, "serviceState":Lcom/samsung/index/indexservice/service/DocumentServiceState;
    invoke-virtual {v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v0

    .line 371
    .local v0, "currentState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    sget-object v3, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_NOT_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_STOPPED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v0, v3, :cond_2

    .line 373
    :cond_0
    if-eqz v1, :cond_1

    .line 377
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->startDocumentService()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388
    :goto_0
    monitor-exit p0

    return-void

    .line 379
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->killProcess()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 367
    .end local v0    # "currentState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .end local v1    # "isSystemOk":Z
    .end local v2    # "serviceState":Lcom/samsung/index/indexservice/service/DocumentServiceState;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 382
    .restart local v0    # "currentState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .restart local v1    # "isSystemOk":Z
    .restart local v2    # "serviceState":Lcom/samsung/index/indexservice/service/DocumentServiceState;
    :cond_2
    if-nez v1, :cond_3

    .line 383
    :try_start_2
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->systemStopDocumentService()V

    goto :goto_0

    .line 386
    :cond_3
    const/4 v3, 0x1

    sput-boolean v3, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized controlStartIndex(Ljava/lang/String;)V
    .locals 6
    .param p1, "broadcastAction"    # Ljava/lang/String;

    .prologue
    .line 313
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/index/indexservice/service/SystemInfo;->checkSystemStatus(Landroid/content/Context;)Z

    move-result v1

    .line 314
    .local v1, "isSystemOk":Z
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v2

    .line 315
    .local v2, "serviceState":Lcom/samsung/index/indexservice/service/DocumentServiceState;
    invoke-virtual {v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v0

    .line 316
    .local v0, "currentState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    const-string/jumbo v3, "BroadcastProcessorService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "[CURRENT_STATE] DocumentService state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    sget-object v3, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_NOT_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_STOPPED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v0, v3, :cond_3

    .line 320
    :cond_0
    if-eqz v1, :cond_2

    .line 321
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->startDocumentService()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 323
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->killProcess()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 313
    .end local v0    # "currentState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .end local v1    # "isSystemOk":Z
    .end local v2    # "serviceState":Lcom/samsung/index/indexservice/service/DocumentServiceState;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 326
    .restart local v0    # "currentState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .restart local v1    # "isSystemOk":Z
    .restart local v2    # "serviceState":Lcom/samsung/index/indexservice/service/DocumentServiceState;
    :cond_3
    if-nez v1, :cond_4

    .line 327
    :try_start_2
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->systemStopDocumentService()V

    goto :goto_0

    .line 332
    :cond_4
    const-string/jumbo v3, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v3, "action.media.scansingle"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v3, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 335
    :cond_5
    const/4 v3, 0x1

    sput-boolean v3, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    .line 337
    sget-boolean v3, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-eqz v3, :cond_1

    sget-boolean v3, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_INDEXING:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v3, :cond_1

    goto :goto_0
.end method

.method public static processBroadcastIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "broadcastIntent"    # Landroid/content/Intent;

    .prologue
    .line 87
    const-string/jumbo v1, "BroadcastProcessorService"

    const-string/jumbo v2, "[START] processBroadcastIntent ..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 90
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "broadcast_receiver"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 94
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 96
    return-void
.end method

.method public static processRestart(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    const-string/jumbo v1, "BroadcastProcessorService"

    const-string/jumbo v2, "[RESTART] processRestart called"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 101
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "broadcast_receiver"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    const-string/jumbo v1, "android.intent.extra.INTENT"

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "com.sec.android.app.myfiles_demo.ACTION_INDEXING_RESTART"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 104
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 105
    return-void
.end method

.method private startDocumentService()V
    .locals 3

    .prologue
    .line 416
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->mServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v1

    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->START_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v1, v2, :cond_0

    .line 417
    const-string/jumbo v1, "BroadcastProcessorService"

    const-string/jumbo v2, "[START] DocumentService startDocumentService"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 419
    .local v0, "intentService":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.app.myfiles_demo.ACTION_INDEXING_START"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 420
    invoke-virtual {p0, v0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 422
    .end local v0    # "intentService":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public static startServiceFromProvider(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 440
    const-string/jumbo v1, "BroadcastProcessorService"

    const-string/jumbo v2, "[START] DocumentService startServiceFromProvider"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 442
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "broadcast_receiver"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 443
    const-string/jumbo v1, "android.intent.extra.INTENT"

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "action.documentservice.start"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 445
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 446
    return-void
.end method

.method private startThumbnail()V
    .locals 3

    .prologue
    .line 428
    const-string/jumbo v1, "BroadcastProcessorService"

    const-string/jumbo v2, "[START] DocumentService startThumbnail"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 430
    .local v0, "intentService":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.app.myfiles_demo.ACTION_THUMBNAIL_START"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 431
    invoke-virtual {p0, v0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 432
    return-void
.end method

.method public static stopDocumentService(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 461
    if-eqz p0, :cond_0

    .line 462
    const-string/jumbo v1, "BroadcastProcessorService"

    const-string/jumbo v2, "[STOP] DocumentService stopDocumentService"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 464
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "broadcast_receiver"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 465
    const-string/jumbo v1, "android.intent.extra.INTENT"

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "com.sec.android.app.myfiles_demo.ACTION_INDEXING_STOP"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 467
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 469
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private systemStopDocumentService()V
    .locals 3

    .prologue
    .line 472
    const-string/jumbo v1, "BroadcastProcessorService"

    const-string/jumbo v2, "[STOP] DocumentService systemStopDocumentService"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 474
    .local v0, "intentService":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.app.myfiles_demo.ACTION_INDEXING_STOP"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 475
    invoke-virtual {p0, v0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 476
    return-void
.end method

.method private systemStopIndexingParser()V
    .locals 3

    .prologue
    .line 479
    const-string/jumbo v1, "BroadcastProcessorService"

    const-string/jumbo v2, "[STOP] DocumentService systemStopIndexingParser"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 481
    .local v0, "intentService":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.app.myfiles_demo.ACTION_INDEXING_STOP"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 482
    const-string/jumbo v1, "STOP_INDEXING"

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 483
    invoke-virtual {p0, v0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 484
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 18
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 116
    if-nez p1, :cond_1

    .line 117
    const-string/jumbo v14, "BroadcastProcessorService"

    const-string/jumbo v15, " onHandleIntent - intent is null"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 123
    .local v2, "action":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 124
    const-string/jumbo v14, "BroadcastProcessorService"

    const-string/jumbo v15, " onHandleIntent - intent action is null"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 128
    :cond_2
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->mServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    .line 130
    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static {v14, v15}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->createInstance(Landroid/content/Context;Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-result-object v8

    .line 131
    .local v8, "indexUtil":Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    if-eqz v8, :cond_3

    .line 132
    invoke-virtual {v8}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->printProcessingFileDetails()V

    .line 135
    :cond_3
    const-string/jumbo v14, "broadcast_receiver"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 137
    const-string/jumbo v14, "android.intent.extra.INTENT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/content/Intent;

    .line 140
    .local v4, "broadcastIntent":Landroid/content/Intent;
    if-nez v4, :cond_4

    .line 141
    const-string/jumbo v14, "BroadcastProcessorService"

    const-string/jumbo v15, "DocumentService onHandleIntent-broadcastIntent is null"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 149
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    const-string/jumbo v15, "doc_service_pref"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 151
    .local v10, "myShPref":Landroid/content/SharedPreferences;
    const-string/jumbo v14, "DevicePowerConnected"

    const-string/jumbo v15, "NO"

    invoke-interface {v10, v14, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 154
    .local v9, "isPowerConnected":Ljava/lang/String;
    const-string/jumbo v14, "YES"

    invoke-virtual {v14, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 155
    const/4 v14, 0x1

    sput-boolean v14, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->bIsPowerConnected:Z

    .line 160
    :goto_1
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 162
    .local v3, "broadcastAction":Ljava/lang/String;
    const-string/jumbo v14, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_5

    const-string/jumbo v14, "action.documentservice.start"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 165
    :cond_5
    const-string/jumbo v14, "BroadcastProcessorService"

    const-string/jumbo v15, "DocumentService onHandleIntent ACTION_MEDIA_SCANNER_FINISHED"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->controlStartIndex(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 157
    .end local v3    # "broadcastAction":Ljava/lang/String;
    :cond_6
    const/4 v14, 0x0

    sput-boolean v14, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->bIsPowerConnected:Z

    goto :goto_1

    .line 170
    .restart local v3    # "broadcastAction":Ljava/lang/String;
    :cond_7
    const-string/jumbo v14, "action.media.scansingle"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 173
    const-string/jumbo v14, "BroadcastProcessorService"

    const-string/jumbo v15, "DocumentService onHandleIntent INTENT_SCAN_SINGLE_FILE"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    const-string/jumbo v14, "file.path"

    invoke-virtual {v4, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 177
    .local v6, "filePath":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 188
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->controlStartIndex(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 216
    .end local v6    # "filePath":Ljava/lang/String;
    :cond_8
    const-string/jumbo v14, "android.intent.action.BATTERY_OKAY"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 217
    const-string/jumbo v14, "BroadcastProcessorService"

    const-string/jumbo v15, "DocumentService onHandleIntent ACTION_BATTERY_OKAY "

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->controlStartIndex(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 221
    :cond_9
    const-string/jumbo v14, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 222
    const-string/jumbo v14, "BroadcastProcessorService"

    const-string/jumbo v15, "DocumentService onHandleIntent ACTION_DEVICE_STORAGE_OK "

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->controlStartIndex(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 228
    :cond_a
    const-string/jumbo v14, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 229
    const-string/jumbo v14, "BroadcastProcessorService"

    const-string/jumbo v15, "DocumentService onHandleIntent ACTION_POWER_CONNECTED "

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    const/4 v14, 0x1

    sput-boolean v14, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->bIsPowerConnected:Z

    .line 234
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    .line 235
    .local v11, "myShPrefEditor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v14, "DevicePowerConnected"

    const-string/jumbo v15, "YES"

    invoke-interface {v11, v14, v15}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 237
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 239
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->controlStartIndex(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 241
    .end local v11    # "myShPrefEditor":Landroid/content/SharedPreferences$Editor;
    :cond_b
    const-string/jumbo v14, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_f

    .line 242
    const-string/jumbo v14, "BroadcastProcessorService"

    const-string/jumbo v15, "DocumentService onHandleIntent ACTION_POWER_DISCONNECTED "

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    const/4 v14, 0x0

    sput-boolean v14, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->bIsPowerConnected:Z

    .line 247
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    .line 248
    .restart local v11    # "myShPrefEditor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v14, "DevicePowerConnected"

    const-string/jumbo v15, "NO"

    invoke-interface {v11, v14, v15}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 250
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 252
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v12

    .line 254
    .local v12, "serviceState":Lcom/samsung/index/indexservice/service/DocumentServiceState;
    invoke-virtual {v12}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v5

    .line 255
    .local v5, "currentState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    const-string/jumbo v14, "BroadcastProcessorService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "[CURRENT_STATE] DocumentService state: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    sget-object v14, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_NOT_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-eq v5, v14, :cond_c

    sget-object v14, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_STOPPED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v5, v14, :cond_d

    .line 260
    :cond_c
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->killProcess()V

    goto/16 :goto_0

    .line 261
    :cond_d
    sget-object v14, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_INDEXING:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-eq v5, v14, :cond_e

    sget-object v14, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_INDEXING_THUMBNAIL_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-eq v5, v14, :cond_e

    sget-object v14, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v5, v14, :cond_0

    .line 264
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->systemStopIndexingParser()V

    goto/16 :goto_0

    .line 267
    .end local v5    # "currentState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .end local v11    # "myShPrefEditor":Landroid/content/SharedPreferences$Editor;
    .end local v12    # "serviceState":Lcom/samsung/index/indexservice/service/DocumentServiceState;
    :cond_f
    const-string/jumbo v14, "com.sec.android.app.myfiles_demo.ACTION_INDEXING_RESTART"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_11

    .line 269
    const-string/jumbo v14, "BroadcastProcessorService"

    const-string/jumbo v15, "DocumentService onReceive ACTION_INDEXING_RESTART"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Lcom/samsung/index/indexservice/service/SystemInfo;->checkSystemStatus(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_10

    .line 274
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v12

    .line 275
    .restart local v12    # "serviceState":Lcom/samsung/index/indexservice/service/DocumentServiceState;
    invoke-virtual {v12}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v5

    .line 276
    .restart local v5    # "currentState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    sget-object v14, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_NOT_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-eq v5, v14, :cond_0

    sget-object v14, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_STOPPED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-eq v5, v14, :cond_0

    .line 278
    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->systemStopDocumentService()V

    goto/16 :goto_0

    .line 285
    .end local v5    # "currentState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .end local v12    # "serviceState":Lcom/samsung/index/indexservice/service/DocumentServiceState;
    :cond_10
    const/4 v14, 0x1

    sput-boolean v14, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    .line 287
    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->systemStopDocumentService()V

    .line 292
    new-instance v7, Landroid/content/Intent;

    const-class v14, Lcom/samsung/index/indexservice/service/DocumentService;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 294
    .local v7, "indexServiceIntent":Landroid/content/Intent;
    const-string/jumbo v14, "com.sec.android.app.myfiles_demo.ACTION_INDEXING_START"

    invoke-virtual {v7, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 297
    new-instance v13, Ljava/util/Timer;

    const/4 v14, 0x1

    invoke-direct {v13, v14}, Ljava/util/Timer;-><init>(Z)V

    .line 298
    .local v13, "startServiceTimer":Ljava/util/Timer;
    new-instance v14, Lcom/samsung/index/indexservice/service/BroadcastProcessorService$StartServiceTimerTask;

    move-object/from16 v0, p0

    invoke-direct {v14, v7, v0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService$StartServiceTimerTask;-><init>(Landroid/content/Intent;Landroid/content/Context;)V

    const-wide/16 v16, 0x7d0

    move-wide/from16 v0, v16

    invoke-virtual {v13, v14, v0, v1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto/16 :goto_0

    .line 301
    .end local v7    # "indexServiceIntent":Landroid/content/Intent;
    .end local v13    # "startServiceTimer":Ljava/util/Timer;
    :cond_11
    const-string/jumbo v14, "com.sec.android.app.myfiles_demo.ACTION_INDEXING_STOP"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 303
    const-string/jumbo v14, "BroadcastProcessorService"

    const-string/jumbo v15, "DocumentService onHandleIntent ACTION_INDEXING_STOP"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->systemStopDocumentService()V

    goto/16 :goto_0
.end method
