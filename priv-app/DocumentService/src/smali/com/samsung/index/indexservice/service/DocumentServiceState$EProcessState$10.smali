.class final enum Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$10;
.super Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
.source "DocumentServiceState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 492
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;-><init>(Ljava/lang/String;ILcom/samsung/index/indexservice/service/DocumentServiceState$1;)V

    return-void
.end method


# virtual methods
.method nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .locals 2
    .param p1, "flag"    # I

    .prologue
    const/4 v1, 0x0

    .line 495
    sparse-switch p1, :sswitch_data_0

    .line 526
    :goto_0
    :sswitch_0
    # getter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$100()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v0

    return-object v0

    .line 503
    :sswitch_1
    sget-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    if-eqz v0, :cond_1

    .line 504
    sput-boolean v1, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    .line 505
    sget-boolean v0, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-eqz v0, :cond_0

    .line 506
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$10;->PROCESSING_THUMBNAIL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    # setter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$102(Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    goto :goto_0

    .line 508
    :cond_0
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$10;->PROCESSING_INDEXING:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    # setter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$102(Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    goto :goto_0

    .line 511
    :cond_1
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$10;->STOPPING_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    # setter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$102(Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    goto :goto_0

    .line 515
    :sswitch_2
    sget-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bStopDocService:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    if-eqz v0, :cond_2

    .line 516
    sput-boolean v1, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    .line 517
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$10;->START_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    # setter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$102(Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 521
    :goto_1
    # invokes: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->resetFlags()V
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$300()V

    goto :goto_0

    .line 519
    :cond_2
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$10;->SERVICE_STOPPED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    # setter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$102(Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    goto :goto_1

    .line 495
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x11 -> :sswitch_1
        0x14 -> :sswitch_0
    .end sparse-switch
.end method
