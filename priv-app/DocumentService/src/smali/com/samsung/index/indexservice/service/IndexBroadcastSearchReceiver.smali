.class public Lcom/samsung/index/indexservice/service/IndexBroadcastSearchReceiver;
.super Landroid/content/BroadcastReceiver;
.source "IndexBroadcastSearchReceiver.java"


# static fields
.field public static final INDEX_BROADCAST_ACTION:Ljava/lang/String; = "action.index.SEARCH"

.field static final TAG:Ljava/lang/String; = "IndexBroadcastSearchReceiver"

.field public static final THUMBNAIL_BROADCAST_ACTION:Ljava/lang/String; = "action.index.THUMBNAIL"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 22
    if-eqz p2, :cond_3

    .line 24
    const-string/jumbo v1, "action.index.SEARCH"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25
    sput-boolean v3, Lcom/samsung/index/provider/SearchProvider;->isSearch:Z

    .line 27
    :cond_0
    const-string/jumbo v1, "action.index.THUMBNAIL"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 28
    sput-boolean v3, Lcom/samsung/thumbnail/provider/ThumbnailProvider;->isThumbnailOn:Z

    .line 30
    :cond_1
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v0

    .line 32
    .local v0, "curState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_NOT_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_STOPPED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v0, v1, :cond_3

    .line 33
    :cond_2
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->killProcess()V

    .line 36
    .end local v0    # "curState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_3
    return-void
.end method
