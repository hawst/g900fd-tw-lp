.class Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;
.super Landroid/os/Handler;
.source "DocumentService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/index/indexservice/service/DocumentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DocumentMessageHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/index/indexservice/service/DocumentService;


# direct methods
.method public constructor <init>(Lcom/samsung/index/indexservice/service/DocumentService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 486
    iput-object p1, p0, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->this$0:Lcom/samsung/index/indexservice/service/DocumentService;

    .line 487
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 488
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    .line 492
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 568
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 495
    :cond_1
    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->this$0:Lcom/samsung/index/indexservice/service/DocumentService;

    # invokes: Lcom/samsung/index/indexservice/service/DocumentService;->isMediaScannerRunning()Z
    invoke-static {v3}, Lcom/samsung/index/indexservice/service/DocumentService;->access$300(Lcom/samsung/index/indexservice/service/DocumentService;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 496
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    .line 497
    .local v1, "newMsg":Landroid/os/Message;
    const-wide/16 v4, 0xbb8

    invoke-virtual {p0, v1, v4, v5}, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 501
    .end local v1    # "newMsg":Landroid/os/Message;
    :cond_2
    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->this$0:Lcom/samsung/index/indexservice/service/DocumentService;

    # getter for: Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    invoke-static {v3}, Lcom/samsung/index/indexservice/service/DocumentService;->access$400(Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 505
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 519
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 520
    .local v0, "folderPath":Ljava/lang/String;
    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 521
    .local v2, "which":I
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 522
    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->this$0:Lcom/samsung/index/indexservice/service/DocumentService;

    # getter for: Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    invoke-static {v3}, Lcom/samsung/index/indexservice/service/DocumentService;->access$400(Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->updateFolderIndex(Ljava/lang/String;I)V

    goto :goto_0

    .line 510
    .end local v0    # "folderPath":Ljava/lang/String;
    .end local v2    # "which":I
    :pswitch_2
    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 511
    .restart local v2    # "which":I
    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->this$0:Lcom/samsung/index/indexservice/service/DocumentService;

    # getter for: Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    invoke-static {v3}, Lcom/samsung/index/indexservice/service/DocumentService;->access$400(Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 512
    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->this$0:Lcom/samsung/index/indexservice/service/DocumentService;

    # getter for: Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    invoke-static {v3}, Lcom/samsung/index/indexservice/service/DocumentService;->access$400(Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->resetAllFileCounts()V

    .line 514
    :cond_3
    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->this$0:Lcom/samsung/index/indexservice/service/DocumentService;

    # invokes: Lcom/samsung/index/indexservice/service/DocumentService;->beginIndexing(I)V
    invoke-static {v3, v2}, Lcom/samsung/index/indexservice/service/DocumentService;->access$100(Lcom/samsung/index/indexservice/service/DocumentService;I)V

    goto :goto_0

    .line 535
    .end local v2    # "which":I
    :pswitch_3
    sget-object v3, Lcom/samsung/index/indexservice/service/SystemInfo;->isSIOPHigh:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 540
    const/4 v3, 0x1

    sput-boolean v3, Lcom/samsung/index/indexservice/service/DocumentService;->bInterruptTemperatureCheck:Z

    .line 545
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    sput-object v3, Lcom/samsung/index/indexservice/service/SystemInfo;->isSIOPHigh:Ljava/lang/Boolean;

    .line 548
    sput-boolean v4, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    .line 551
    sget-object v4, Lcom/samsung/index/indexservice/service/SystemInfo;->tempObj:Ljava/lang/Object;

    monitor-enter v4

    .line 552
    :try_start_0
    sget-object v3, Lcom/samsung/index/indexservice/service/SystemInfo;->tempObj:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 553
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 557
    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->this$0:Lcom/samsung/index/indexservice/service/DocumentService;

    # invokes: Lcom/samsung/index/indexservice/service/DocumentService;->setFlagsToStopService()V
    invoke-static {v3}, Lcom/samsung/index/indexservice/service/DocumentService;->access$200(Lcom/samsung/index/indexservice/service/DocumentService;)V

    .line 559
    const-string/jumbo v3, "DocumentService"

    const-string/jumbo v4, "[STOP SERVICE] [SIOP LEVEL] High for over 3 minutes"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentService$DocumentMessageHandler;->this$0:Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->stopDocumentService(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 553
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 505
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
