.class public final Lcom/samsung/index/indexservice/threads/IndexParserThread;
.super Ljava/lang/Object;
.source "IndexParserThread.java"

# interfaces
.implements Lcom/samsung/index/ITextContentObs;
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String; = "IndexParserThread"


# instance fields
.field private filePath:Ljava/lang/String;

.field private isIndexCompleted:Z

.field private listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

.field private mContext:Landroid/content/Context;

.field private mExtension:Ljava/lang/String;

.field private mFile:Ljava/io/File;

.field private mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

.field private mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

.field private mIsDataSaved:Z

.field private mIsFileCorrupted:Z

.field private mSetKeyValue:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;Lcom/samsung/index/IFileIndexedNotifier;Ljava/io/File;Ljava/util/HashMap;Ljava/lang/String;ZLandroid/content/Context;Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;)V
    .locals 1
    .param p1, "indexWriter"    # Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;
    .param p2, "filenotifier"    # Lcom/samsung/index/IFileIndexedNotifier;
    .param p3, "file"    # Ljava/io/File;
    .param p5, "extension"    # Ljava/lang/String;
    .param p6, "isSynchronous"    # Z
    .param p7, "context"    # Landroid/content/Context;
    .param p8, "listManager"    # Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;",
            "Lcom/samsung/index/IFileIndexedNotifier;",
            "Ljava/io/File;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Landroid/content/Context;",
            "Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;",
            ")V"
        }
    .end annotation

    .prologue
    .local p4, "setkeyvalue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-boolean v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsDataSaved:Z

    .line 47
    iput-boolean v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsFileCorrupted:Z

    .line 55
    iput-boolean v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->isIndexCompleted:Z

    .line 62
    iput-object p4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mSetKeyValue:Ljava/util/HashMap;

    .line 63
    iput-object p5, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    .line 64
    iput-object p3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    .line 65
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    .line 66
    iput-object p1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    .line 67
    iput-object p2, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    .line 68
    iput-object p7, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mContext:Landroid/content/Context;

    .line 69
    iput-object p8, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    .line 70
    return-void
.end method

.method private indexFileWithIndexWriter()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v14, 0x1

    .line 289
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    .line 290
    .local v4, "extension":Ljava/lang/String;
    new-instance v7, Lcom/samsung/index/parser/IndexDocumentParser;

    invoke-direct {v7}, Lcom/samsung/index/parser/IndexDocumentParser;-><init>()V

    .line 292
    .local v7, "parser":Lcom/samsung/index/parser/IndexDocumentParser;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_2

    .line 293
    :cond_0
    const-string/jumbo v8, "IndexParserThread"

    const-string/jumbo v9, "Extension is empty"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    :cond_1
    :goto_0
    return-void

    .line 297
    :cond_2
    invoke-static {v9, v9}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->createInstance(Landroid/content/Context;Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-result-object v6

    .line 299
    .local v6, "indexUtil":Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    const-string/jumbo v8, "IndexParserThread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Lucene start Indexing File:  Size in KB: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v10

    const-wide/16 v12, 0x400

    div-long/2addr v10, v12

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " extenstion "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    if-eqz v6, :cond_3

    .line 304
    invoke-virtual {v6}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->printProcessingFileDetails()V

    .line 310
    :cond_3
    const-string/jumbo v8, "IndexParserThread"

    invoke-static {v8}, Lcom/samsung/index/indexservice/service/SystemInfo;->checkTemperatureLevel(Ljava/lang/String;)V

    .line 312
    new-instance v2, Lorg/apache/lucene/document/Document;

    invoke-direct {v2}, Lorg/apache/lucene/document/Document;-><init>()V

    .line 313
    .local v2, "doc":Lorg/apache/lucene/document/Document;
    const/4 v5, 0x0

    .line 315
    .local v5, "field":Lorg/apache/lucene/document/Field;
    const-string/jumbo v1, ""

    .line 318
    .local v1, "contents":Ljava/lang/String;
    :try_start_0
    iget-object v8, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    iget-object v9, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v8, v9, p0}, Lcom/samsung/index/parser/IndexDocumentParser;->parse(Ljava/io/File;Landroid/content/Context;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 323
    :goto_1
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_4

    .line 324
    sget v8, Lcom/samsung/index/ContentIndex;->mHighlightText:I

    if-ne v8, v14, :cond_6

    .line 331
    new-instance v5, Lorg/apache/lucene/document/Field;

    .end local v5    # "field":Lorg/apache/lucene/document/Field;
    const-string/jumbo v8, "contents"

    sget-object v9, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    sget-object v10, Lorg/apache/lucene/document/Field$Index;->ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;

    invoke-direct {v5, v8, v1, v9, v10}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;)V

    .line 333
    .restart local v5    # "field":Lorg/apache/lucene/document/Field;
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 334
    new-instance v0, Lorg/apache/lucene/document/Field;

    const-string/jumbo v8, "hlcontents"

    invoke-static {v1}, Lorg/apache/lucene/document/CompressionTools;->compressString(Ljava/lang/String;)[B

    move-result-object v9

    invoke-direct {v0, v8, v9}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;[B)V

    .line 337
    .local v0, "contentValueField":Lorg/apache/lucene/document/Field;
    invoke-virtual {v2, v0}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/document/Fieldable;)V

    .line 345
    .end local v0    # "contentValueField":Lorg/apache/lucene/document/Field;
    :cond_4
    :goto_2
    if-eqz v5, :cond_5

    .line 347
    invoke-virtual {v5, v14}, Lorg/apache/lucene/document/Field;->setOmitNorms(Z)V

    .line 348
    invoke-virtual {v2, v5}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/document/Fieldable;)V

    .line 350
    iget-object v8, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    invoke-direct {p0, v8, v2}, Lcom/samsung/index/indexservice/threads/IndexParserThread;->setKeyValue(Ljava/io/File;Lorg/apache/lucene/document/Document;)V

    .line 353
    :try_start_1
    iget-object v8, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v8, v2}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->addDocumentSync(Lorg/apache/lucene/document/Document;)V

    .line 355
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsDataSaved:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 376
    :cond_5
    iput-boolean v14, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsDataSaved:Z

    goto/16 :goto_0

    .line 319
    :catch_0
    move-exception v3

    .line 320
    .local v3, "e":Ljava/lang/UnsupportedOperationException;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "UnsupportedOperationException: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/UnsupportedOperationException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 340
    .end local v3    # "e":Ljava/lang/UnsupportedOperationException;
    :cond_6
    new-instance v5, Lorg/apache/lucene/document/Field;

    .end local v5    # "field":Lorg/apache/lucene/document/Field;
    const-string/jumbo v8, "contents"

    sget-object v9, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    sget-object v10, Lorg/apache/lucene/document/Field$Index;->ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;

    invoke-direct {v5, v8, v1, v9, v10}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;)V

    .restart local v5    # "field":Lorg/apache/lucene/document/Field;
    goto :goto_2

    .line 356
    :catch_1
    move-exception v3

    .line 360
    .local v3, "e":Ljava/lang/Exception;
    const-string/jumbo v8, "IndexParserThread"

    const-string/jumbo v9, "indexFileWithIndexWriter Exception"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    instance-of v8, v3, Ljava/io/FileNotFoundException;

    if-nez v8, :cond_7

    instance-of v8, v3, Lorg/apache/lucene/index/CorruptIndexException;

    if-nez v8, :cond_7

    instance-of v8, v3, Ljava/lang/NumberFormatException;

    if-eqz v8, :cond_1

    .line 367
    :cond_7
    iget-object v8, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    invoke-interface {v8}, Lcom/samsung/index/IFileIndexedNotifier;->indexCorrupted()V

    goto/16 :goto_0
.end method

.method private setKeyValue(Ljava/io/File;Lorg/apache/lucene/document/Document;)V
    .locals 8
    .param p1, "file"    # Ljava/io/File;
    .param p2, "doc"    # Lorg/apache/lucene/document/Document;

    .prologue
    .line 270
    new-instance v3, Lorg/apache/lucene/document/Field;

    const-string/jumbo v4, "filepath"

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    sget-object v7, Lorg/apache/lucene/document/Field$Index;->NOT_ANALYZED:Lorg/apache/lucene/document/Field$Index;

    invoke-direct {v3, v4, v5, v6, v7}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;)V

    invoke-virtual {p2, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/document/Fieldable;)V

    .line 273
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mSetKeyValue:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 274
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v2, Lorg/apache/lucene/document/NumericField;

    const-string/jumbo v3, "modifiedTime"

    invoke-direct {v2, v3}, Lorg/apache/lucene/document/NumericField;-><init>(Ljava/lang/String;)V

    .line 276
    .local v2, "numeric":Lorg/apache/lucene/document/NumericField;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string/jumbo v4, "modifiedTime"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 277
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/document/NumericField;->setIntValue(I)Lorg/apache/lucene/document/NumericField;

    .line 278
    invoke-virtual {p2, v2}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/document/Fieldable;)V

    goto :goto_0

    .line 280
    :cond_0
    new-instance v5, Lorg/apache/lucene/document/Field;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    sget-object v6, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    sget-object v7, Lorg/apache/lucene/document/Field$Index;->NOT_ANALYZED:Lorg/apache/lucene/document/Field$Index;

    invoke-direct {v5, v3, v4, v6, v7}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;)V

    invoke-virtual {p2, v5}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/document/Fieldable;)V

    goto :goto_0

    .line 284
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "numeric":Lorg/apache/lucene/document/NumericField;
    :cond_1
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 442
    if-ne p0, p1, :cond_1

    .line 443
    const/4 v3, 0x1

    .line 454
    :cond_0
    :goto_0
    return v3

    .line 444
    :cond_1
    instance-of v4, p1, Lcom/samsung/index/indexservice/threads/IndexParserThread;

    if-eqz v4, :cond_0

    move-object v0, p1

    .line 445
    check-cast v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;

    .line 446
    .local v0, "parserThread":Lcom/samsung/index/indexservice/threads/IndexParserThread;
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    .line 447
    .local v1, "path1":Ljava/lang/String;
    iget-object v2, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    .line 448
    .local v2, "path2":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 449
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    .line 434
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 435
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 437
    :goto_0
    return v1

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public run()V
    .locals 24

    .prologue
    .line 73
    const/4 v15, -0x1

    .line 74
    .local v15, "status":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 75
    .local v12, "startTime":J
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsDataSaved:Z

    .line 76
    const/4 v10, 0x0

    .line 77
    .local v10, "indexUtil":Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    const/4 v11, 0x0

    .line 80
    .local v11, "isStopped":Z
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->getStopParserThread()Z

    move-result v16

    if-eqz v16, :cond_8

    .line 81
    const/4 v11, 0x1

    .line 82
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->shutDownIndexing(Lcom/samsung/index/IFileIndexedNotifier;)V

    .line 83
    const-string/jumbo v16, "IndexParserThread"

    const-string/jumbo v17, "Lucene exiting parserthread as app told so "

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/apache/lucene/index/CorruptIndexException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_d
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_0 .. :try_end_0} :catch_10
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_0 .. :try_end_0} :catch_13
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_16
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_19
    .catchall {:try_start_0 .. :try_end_0} :catchall_17

    .line 153
    if-eqz v11, :cond_1

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    if-eqz v16, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_3

    .line 157
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsFileCorrupted:Z

    move/from16 v16, v0

    if-eqz v16, :cond_2

    .line 158
    const/4 v15, 0x3

    .line 160
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v0, v1, v15}, Lcom/samsung/index/IFileIndexedNotifier;->indexedFile(Ljava/lang/String;I)V

    .line 164
    :cond_3
    if-eqz v10, :cond_4

    .line 165
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 167
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->removeCurrentFile(Ljava/lang/String;)V

    .line 168
    monitor-exit v17
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decIndexFileCount()I

    .line 173
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->decDocCount()V

    .line 174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v8, v16, v12

    .line 179
    .local v8, "endTime":J
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Lucene End Indexing File extension: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Size in KB: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v18

    const-wide/16 v20, 0x400

    div-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Total Time : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Ovar all Time: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sget-wide v20, Lcom/samsung/index/indexservice/service/DocumentService;->startTime:J

    sub-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    if-eqz v10, :cond_5

    .line 188
    invoke-virtual {v10, v8, v9}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setIndexTotalTime(J)V

    .line 192
    :cond_5
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->setIndexParserThreadCallback(Lcom/samsung/index/ITextContentObs;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 199
    :goto_1
    if-eqz v10, :cond_6

    .line 200
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 201
    :try_start_3
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setCount(I)I

    .line 202
    monitor-exit v17
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 205
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->isIndexCompleted:Z

    move/from16 v16, v0

    if-nez v16, :cond_0

    .line 213
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v14

    .line 216
    .local v14, "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->getInterrupt()Z

    move-result v16

    if-eqz v16, :cond_7

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->shutDownIndexing(Lcom/samsung/index/IFileIndexedNotifier;)V

    goto/16 :goto_0

    .line 168
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catchall_0
    move-exception v16

    :try_start_4
    monitor-exit v17
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v16

    .line 194
    .restart local v8    # "endTime":J
    :catch_0
    move-exception v4

    .line 195
    .local v4, "e":Ljava/io/IOException;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 202
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v16

    :try_start_5
    monitor-exit v17
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v16

    .line 227
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_7
    sget-boolean v16, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-nez v16, :cond_0

    sget-object v16, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-object/from16 v0, v16

    if-ne v14, v0, :cond_0

    .line 229
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->switchIndexToTwoThread()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_0

    .line 230
    :catch_1
    move-exception v7

    .line 231
    .local v7, "e3":Ljava/lang/Exception;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception in Switiching the thread.. :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 88
    .end local v7    # "e3":Ljava/lang/Exception;
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_8
    const/16 v16, 0x0

    const/16 v17, 0x0

    :try_start_7
    invoke-static/range {v16 .. v17}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->createInstance(Landroid/content/Context;Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-result-object v10

    .line 90
    if-nez v10, :cond_f

    .line 91
    const-string/jumbo v16, "IndexParserThread"

    const-string/jumbo v17, "ERROR: indexutil null "

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Lorg/apache/lucene/index/CorruptIndexException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_d
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_7 .. :try_end_7} :catch_10
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_7 .. :try_end_7} :catch_13
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_16
    .catch Ljava/lang/Error; {:try_start_7 .. :try_end_7} :catch_19
    .catchall {:try_start_7 .. :try_end_7} :catchall_17

    .line 153
    if-nez v11, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    if-eqz v16, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_a

    .line 157
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsFileCorrupted:Z

    move/from16 v16, v0

    if-eqz v16, :cond_9

    .line 158
    const/4 v15, 0x3

    .line 160
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v0, v1, v15}, Lcom/samsung/index/IFileIndexedNotifier;->indexedFile(Ljava/lang/String;I)V

    .line 164
    :cond_a
    if-eqz v10, :cond_b

    .line 165
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 167
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->removeCurrentFile(Ljava/lang/String;)V

    .line 168
    monitor-exit v17
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 169
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decIndexFileCount()I

    .line 173
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->decDocCount()V

    .line 174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v8, v16, v12

    .line 179
    .restart local v8    # "endTime":J
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Lucene End Indexing File extension: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Size in KB: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v18

    const-wide/16 v20, 0x400

    div-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Total Time : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Ovar all Time: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sget-wide v20, Lcom/samsung/index/indexservice/service/DocumentService;->startTime:J

    sub-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    if-eqz v10, :cond_c

    .line 188
    invoke-virtual {v10, v8, v9}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setIndexTotalTime(J)V

    .line 192
    :cond_c
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->setIndexParserThreadCallback(Lcom/samsung/index/ITextContentObs;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    .line 199
    :goto_2
    if-eqz v10, :cond_d

    .line 200
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 201
    :try_start_a
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setCount(I)I

    .line 202
    monitor-exit v17
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 205
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->isIndexCompleted:Z

    move/from16 v16, v0

    if-nez v16, :cond_0

    .line 213
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v14

    .line 216
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->getInterrupt()Z

    move-result v16

    if-eqz v16, :cond_e

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->shutDownIndexing(Lcom/samsung/index/IFileIndexedNotifier;)V

    goto/16 :goto_0

    .line 168
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catchall_2
    move-exception v16

    :try_start_b
    monitor-exit v17
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    throw v16

    .line 194
    .restart local v8    # "endTime":J
    :catch_2
    move-exception v4

    .line 195
    .restart local v4    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 202
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_3
    move-exception v16

    :try_start_c
    monitor-exit v17
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    throw v16

    .line 227
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_e
    sget-boolean v16, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-nez v16, :cond_0

    sget-object v16, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-object/from16 v0, v16

    if-ne v14, v0, :cond_0

    .line 229
    :try_start_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->switchIndexToTwoThread()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3

    goto/16 :goto_0

    .line 230
    :catch_3
    move-exception v7

    .line 231
    .restart local v7    # "e3":Ljava/lang/Exception;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception in Switiching the thread.. :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 98
    .end local v7    # "e3":Ljava/lang/Exception;
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_f
    :try_start_e
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17
    :try_end_e
    .catch Lorg/apache/lucene/index/CorruptIndexException; {:try_start_e .. :try_end_e} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_d
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_e .. :try_end_e} :catch_10
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_e .. :try_end_e} :catch_13
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_16
    .catch Ljava/lang/Error; {:try_start_e .. :try_end_e} :catch_19
    .catchall {:try_start_e .. :try_end_e} :catchall_17

    .line 99
    :try_start_f
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I

    move-result v16

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setCount(I)I

    .line 100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setCurrentFile(Ljava/lang/String;)V

    .line 101
    monitor-exit v17
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    .line 103
    :try_start_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mSetKeyValue:Ljava/util/HashMap;

    move-object/from16 v16, v0

    const-string/jumbo v17, "delete_status"

    invoke-virtual/range {v16 .. v17}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    const-string/jumbo v17, "2"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_10

    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->deleteOnlyFileIndex(Ljava/lang/String;)V

    .line 106
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->commitAfterDelete()V

    .line 109
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_1b

    .line 110
    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/indexservice/threads/IndexParserThread;->indexFileWithIndexWriter()V

    .line 118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v16

    if-nez v16, :cond_23

    .line 119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    const-string/jumbo v17, "filepath"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->deleteIndex(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->commitAfterDelete()V
    :try_end_10
    .catch Lorg/apache/lucene/index/CorruptIndexException; {:try_start_10 .. :try_end_10} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_d
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_10 .. :try_end_10} :catch_10
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_10 .. :try_end_10} :catch_13
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_16
    .catch Ljava/lang/Error; {:try_start_10 .. :try_end_10} :catch_19
    .catchall {:try_start_10 .. :try_end_10} :catchall_17

    .line 153
    if-nez v11, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    if-eqz v16, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_12

    .line 157
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsFileCorrupted:Z

    move/from16 v16, v0

    if-eqz v16, :cond_11

    .line 158
    const/4 v15, 0x3

    .line 160
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v0, v1, v15}, Lcom/samsung/index/IFileIndexedNotifier;->indexedFile(Ljava/lang/String;I)V

    .line 164
    :cond_12
    if-eqz v10, :cond_13

    .line 165
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 167
    :try_start_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->removeCurrentFile(Ljava/lang/String;)V

    .line 168
    monitor-exit v17
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_7

    .line 169
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decIndexFileCount()I

    .line 173
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->decDocCount()V

    .line 174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v8, v16, v12

    .line 179
    .restart local v8    # "endTime":J
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Lucene End Indexing File extension: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Size in KB: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v18

    const-wide/16 v20, 0x400

    div-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Total Time : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Ovar all Time: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sget-wide v20, Lcom/samsung/index/indexservice/service/DocumentService;->startTime:J

    sub-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    if-eqz v10, :cond_14

    .line 188
    invoke-virtual {v10, v8, v9}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setIndexTotalTime(J)V

    .line 192
    :cond_14
    :try_start_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->setIndexParserThreadCallback(Lcom/samsung/index/ITextContentObs;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_7

    .line 199
    :goto_3
    if-eqz v10, :cond_15

    .line 200
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 201
    :try_start_13
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setCount(I)I

    .line 202
    monitor-exit v17
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_8

    .line 205
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->isIndexCompleted:Z

    move/from16 v16, v0

    if-nez v16, :cond_0

    .line 213
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v14

    .line 216
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->getInterrupt()Z

    move-result v16

    if-eqz v16, :cond_22

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->shutDownIndexing(Lcom/samsung/index/IFileIndexedNotifier;)V

    goto/16 :goto_0

    .line 101
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catchall_4
    move-exception v16

    :try_start_14
    monitor-exit v17
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    :try_start_15
    throw v16
    :try_end_15
    .catch Lorg/apache/lucene/index/CorruptIndexException; {:try_start_15 .. :try_end_15} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_15 .. :try_end_15} :catch_d
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_15 .. :try_end_15} :catch_10
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_15 .. :try_end_15} :catch_13
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_16
    .catch Ljava/lang/Error; {:try_start_15 .. :try_end_15} :catch_19
    .catchall {:try_start_15 .. :try_end_15} :catchall_17

    .line 131
    :catch_4
    move-exception v5

    .line 132
    .local v5, "e1":Lorg/apache/lucene/index/CorruptIndexException;
    :try_start_16
    const-string/jumbo v16, "IndexParserThread"

    const-string/jumbo v17, "index is corrupted"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_17

    .line 153
    if-nez v11, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    if-eqz v16, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_17

    .line 157
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsFileCorrupted:Z

    move/from16 v16, v0

    if-eqz v16, :cond_16

    .line 158
    const/4 v15, 0x3

    .line 160
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v0, v1, v15}, Lcom/samsung/index/IFileIndexedNotifier;->indexedFile(Ljava/lang/String;I)V

    .line 164
    :cond_17
    if-eqz v10, :cond_18

    .line 165
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 167
    :try_start_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->removeCurrentFile(Ljava/lang/String;)V

    .line 168
    monitor-exit v17
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_b

    .line 169
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decIndexFileCount()I

    .line 173
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->decDocCount()V

    .line 174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v8, v16, v12

    .line 179
    .restart local v8    # "endTime":J
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Lucene End Indexing File extension: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Size in KB: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v18

    const-wide/16 v20, 0x400

    div-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Total Time : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Ovar all Time: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sget-wide v20, Lcom/samsung/index/indexservice/service/DocumentService;->startTime:J

    sub-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    if-eqz v10, :cond_19

    .line 188
    invoke-virtual {v10, v8, v9}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setIndexTotalTime(J)V

    .line 192
    :cond_19
    :try_start_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->setIndexParserThreadCallback(Lcom/samsung/index/ITextContentObs;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_b

    .line 199
    :goto_4
    if-eqz v10, :cond_1a

    .line 200
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 201
    :try_start_19
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setCount(I)I

    .line 202
    monitor-exit v17
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_c

    .line 205
    :cond_1a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->isIndexCompleted:Z

    move/from16 v16, v0

    if-nez v16, :cond_0

    .line 213
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v14

    .line 216
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->getInterrupt()Z

    move-result v16

    if-eqz v16, :cond_2b

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->shutDownIndexing(Lcom/samsung/index/IFileIndexedNotifier;)V

    goto/16 :goto_0

    .line 153
    .end local v5    # "e1":Lorg/apache/lucene/index/CorruptIndexException;
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_1b
    if-nez v11, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    if-eqz v16, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_1d

    .line 157
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsFileCorrupted:Z

    move/from16 v16, v0

    if-eqz v16, :cond_1c

    .line 158
    const/4 v15, 0x3

    .line 160
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v0, v1, v15}, Lcom/samsung/index/IFileIndexedNotifier;->indexedFile(Ljava/lang/String;I)V

    .line 164
    :cond_1d
    if-eqz v10, :cond_1e

    .line 165
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 167
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->removeCurrentFile(Ljava/lang/String;)V

    .line 168
    monitor-exit v17
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_5

    .line 169
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decIndexFileCount()I

    .line 173
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->decDocCount()V

    .line 174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v8, v16, v12

    .line 179
    .restart local v8    # "endTime":J
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Lucene End Indexing File extension: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Size in KB: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v18

    const-wide/16 v20, 0x400

    div-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Total Time : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Ovar all Time: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sget-wide v20, Lcom/samsung/index/indexservice/service/DocumentService;->startTime:J

    sub-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    if-eqz v10, :cond_1f

    .line 188
    invoke-virtual {v10, v8, v9}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setIndexTotalTime(J)V

    .line 192
    :cond_1f
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->setIndexParserThreadCallback(Lcom/samsung/index/ITextContentObs;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_5

    .line 199
    :goto_5
    if-eqz v10, :cond_20

    .line 200
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 201
    :try_start_1c
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setCount(I)I

    .line 202
    monitor-exit v17
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_6

    .line 205
    :cond_20
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->isIndexCompleted:Z

    move/from16 v16, v0

    if-nez v16, :cond_0

    .line 213
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v14

    .line 216
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->getInterrupt()Z

    move-result v16

    if-eqz v16, :cond_21

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->shutDownIndexing(Lcom/samsung/index/IFileIndexedNotifier;)V

    goto/16 :goto_0

    .line 168
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catchall_5
    move-exception v16

    :try_start_1d
    monitor-exit v17
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_5

    throw v16

    .line 194
    .restart local v8    # "endTime":J
    :catch_5
    move-exception v4

    .line 195
    .restart local v4    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 202
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_6
    move-exception v16

    :try_start_1e
    monitor-exit v17
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_6

    throw v16

    .line 227
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_21
    sget-boolean v16, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-nez v16, :cond_0

    sget-object v16, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-object/from16 v0, v16

    if-ne v14, v0, :cond_0

    .line 229
    :try_start_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->switchIndexToTwoThread()V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_6

    goto/16 :goto_0

    .line 230
    :catch_6
    move-exception v7

    .line 231
    .restart local v7    # "e3":Ljava/lang/Exception;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception in Switiching the thread.. :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 168
    .end local v7    # "e3":Ljava/lang/Exception;
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catchall_7
    move-exception v16

    :try_start_20
    monitor-exit v17
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_7

    throw v16

    .line 194
    .restart local v8    # "endTime":J
    :catch_7
    move-exception v4

    .line 195
    .restart local v4    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 202
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_8
    move-exception v16

    :try_start_21
    monitor-exit v17
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_8

    throw v16

    .line 227
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_22
    sget-boolean v16, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-nez v16, :cond_0

    sget-object v16, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-object/from16 v0, v16

    if-ne v14, v0, :cond_0

    .line 229
    :try_start_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->switchIndexToTwoThread()V
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_8

    goto/16 :goto_0

    .line 230
    :catch_8
    move-exception v7

    .line 231
    .restart local v7    # "e3":Ljava/lang/Exception;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception in Switiching the thread.. :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 125
    .end local v7    # "e3":Ljava/lang/Exception;
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_23
    :try_start_23
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsDataSaved:Z

    move/from16 v16, v0
    :try_end_23
    .catch Lorg/apache/lucene/index/CorruptIndexException; {:try_start_23 .. :try_end_23} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_23 .. :try_end_23} :catch_d
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_23 .. :try_end_23} :catch_10
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_23 .. :try_end_23} :catch_13
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_16
    .catch Ljava/lang/Error; {:try_start_23 .. :try_end_23} :catch_19
    .catchall {:try_start_23 .. :try_end_23} :catchall_17

    if-eqz v16, :cond_29

    .line 126
    const/4 v15, 0x0

    .line 153
    :goto_6
    if-nez v11, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    if-eqz v16, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_25

    .line 157
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsFileCorrupted:Z

    move/from16 v16, v0

    if-eqz v16, :cond_24

    .line 158
    const/4 v15, 0x3

    .line 160
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v0, v1, v15}, Lcom/samsung/index/IFileIndexedNotifier;->indexedFile(Ljava/lang/String;I)V

    .line 164
    :cond_25
    if-eqz v10, :cond_26

    .line 165
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 167
    :try_start_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->removeCurrentFile(Ljava/lang/String;)V

    .line 168
    monitor-exit v17
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_9

    .line 169
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decIndexFileCount()I

    .line 173
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->decDocCount()V

    .line 174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v8, v16, v12

    .line 179
    .restart local v8    # "endTime":J
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Lucene End Indexing File extension: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Size in KB: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v18

    const-wide/16 v20, 0x400

    div-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Total Time : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Ovar all Time: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sget-wide v20, Lcom/samsung/index/indexservice/service/DocumentService;->startTime:J

    sub-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    if-eqz v10, :cond_27

    .line 188
    invoke-virtual {v10, v8, v9}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setIndexTotalTime(J)V

    .line 192
    :cond_27
    :try_start_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->setIndexParserThreadCallback(Lcom/samsung/index/ITextContentObs;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_25
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_25} :catch_9

    .line 199
    :goto_7
    if-eqz v10, :cond_28

    .line 200
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 201
    :try_start_26
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setCount(I)I

    .line 202
    monitor-exit v17
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_a

    .line 205
    :cond_28
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->isIndexCompleted:Z

    move/from16 v16, v0

    if-nez v16, :cond_0

    .line 213
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v14

    .line 216
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->getInterrupt()Z

    move-result v16

    if-eqz v16, :cond_2a

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->shutDownIndexing(Lcom/samsung/index/IFileIndexedNotifier;)V

    goto/16 :goto_0

    .line 128
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_29
    const/4 v15, 0x3

    goto/16 :goto_6

    .line 168
    :catchall_9
    move-exception v16

    :try_start_27
    monitor-exit v17
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_9

    throw v16

    .line 194
    .restart local v8    # "endTime":J
    :catch_9
    move-exception v4

    .line 195
    .restart local v4    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 202
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_a
    move-exception v16

    :try_start_28
    monitor-exit v17
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_a

    throw v16

    .line 227
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_2a
    sget-boolean v16, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-nez v16, :cond_0

    sget-object v16, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-object/from16 v0, v16

    if-ne v14, v0, :cond_0

    .line 229
    :try_start_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->switchIndexToTwoThread()V
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_29} :catch_a

    goto/16 :goto_0

    .line 230
    :catch_a
    move-exception v7

    .line 231
    .restart local v7    # "e3":Ljava/lang/Exception;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception in Switiching the thread.. :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 168
    .end local v7    # "e3":Ljava/lang/Exception;
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .restart local v5    # "e1":Lorg/apache/lucene/index/CorruptIndexException;
    :catchall_b
    move-exception v16

    :try_start_2a
    monitor-exit v17
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_b

    throw v16

    .line 194
    .restart local v8    # "endTime":J
    :catch_b
    move-exception v4

    .line 195
    .restart local v4    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 202
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_c
    move-exception v16

    :try_start_2b
    monitor-exit v17
    :try_end_2b
    .catchall {:try_start_2b .. :try_end_2b} :catchall_c

    throw v16

    .line 227
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_2b
    sget-boolean v16, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-nez v16, :cond_0

    sget-object v16, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-object/from16 v0, v16

    if-ne v14, v0, :cond_0

    .line 229
    :try_start_2c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->switchIndexToTwoThread()V
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_2c} :catch_c

    goto/16 :goto_0

    .line 230
    :catch_c
    move-exception v7

    .line 231
    .restart local v7    # "e3":Ljava/lang/Exception;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception in Switiching the thread.. :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 133
    .end local v5    # "e1":Lorg/apache/lucene/index/CorruptIndexException;
    .end local v7    # "e3":Ljava/lang/Exception;
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catch_d
    move-exception v6

    .line 134
    .local v6, "e2":Ljava/lang/InterruptedException;
    :try_start_2d
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "InterruptedException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v6}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2d
    .catchall {:try_start_2d .. :try_end_2d} :catchall_17

    .line 153
    if-nez v11, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    if-eqz v16, :cond_2d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_2d

    .line 157
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsFileCorrupted:Z

    move/from16 v16, v0

    if-eqz v16, :cond_2c

    .line 158
    const/4 v15, 0x3

    .line 160
    :cond_2c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v0, v1, v15}, Lcom/samsung/index/IFileIndexedNotifier;->indexedFile(Ljava/lang/String;I)V

    .line 164
    :cond_2d
    if-eqz v10, :cond_2e

    .line 165
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 167
    :try_start_2e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->removeCurrentFile(Ljava/lang/String;)V

    .line 168
    monitor-exit v17
    :try_end_2e
    .catchall {:try_start_2e .. :try_end_2e} :catchall_d

    .line 169
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decIndexFileCount()I

    .line 173
    :cond_2e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->decDocCount()V

    .line 174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v8, v16, v12

    .line 179
    .restart local v8    # "endTime":J
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Lucene End Indexing File extension: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Size in KB: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v18

    const-wide/16 v20, 0x400

    div-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Total Time : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Ovar all Time: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sget-wide v20, Lcom/samsung/index/indexservice/service/DocumentService;->startTime:J

    sub-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    if-eqz v10, :cond_2f

    .line 188
    invoke-virtual {v10, v8, v9}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setIndexTotalTime(J)V

    .line 192
    :cond_2f
    :try_start_2f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->setIndexParserThreadCallback(Lcom/samsung/index/ITextContentObs;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_2f
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_2f} :catch_e

    .line 199
    :goto_8
    if-eqz v10, :cond_30

    .line 200
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 201
    :try_start_30
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setCount(I)I

    .line 202
    monitor-exit v17
    :try_end_30
    .catchall {:try_start_30 .. :try_end_30} :catchall_e

    .line 205
    :cond_30
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->isIndexCompleted:Z

    move/from16 v16, v0

    if-nez v16, :cond_0

    .line 213
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v14

    .line 216
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->getInterrupt()Z

    move-result v16

    if-eqz v16, :cond_31

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->shutDownIndexing(Lcom/samsung/index/IFileIndexedNotifier;)V

    goto/16 :goto_0

    .line 168
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catchall_d
    move-exception v16

    :try_start_31
    monitor-exit v17
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_d

    throw v16

    .line 194
    .restart local v8    # "endTime":J
    :catch_e
    move-exception v4

    .line 195
    .restart local v4    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 202
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_e
    move-exception v16

    :try_start_32
    monitor-exit v17
    :try_end_32
    .catchall {:try_start_32 .. :try_end_32} :catchall_e

    throw v16

    .line 227
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_31
    sget-boolean v16, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-nez v16, :cond_0

    sget-object v16, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-object/from16 v0, v16

    if-ne v14, v0, :cond_0

    .line 229
    :try_start_33
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->switchIndexToTwoThread()V
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_33} :catch_f

    goto/16 :goto_0

    .line 230
    :catch_f
    move-exception v7

    .line 231
    .restart local v7    # "e3":Ljava/lang/Exception;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception in Switiching the thread.. :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 135
    .end local v6    # "e2":Ljava/lang/InterruptedException;
    .end local v7    # "e3":Ljava/lang/Exception;
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catch_10
    move-exception v4

    .line 136
    .local v4, "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :try_start_34
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "EncryptedDocumentException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Lcom/samsung/index/GeneralEncryptedDocException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_34
    .catchall {:try_start_34 .. :try_end_34} :catchall_17

    .line 137
    const/4 v15, 0x2

    .line 153
    if-nez v11, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    if-eqz v16, :cond_33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_33

    .line 157
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsFileCorrupted:Z

    move/from16 v16, v0

    if-eqz v16, :cond_32

    .line 158
    const/4 v15, 0x3

    .line 160
    :cond_32
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v0, v1, v15}, Lcom/samsung/index/IFileIndexedNotifier;->indexedFile(Ljava/lang/String;I)V

    .line 164
    :cond_33
    if-eqz v10, :cond_34

    .line 165
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 167
    :try_start_35
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->removeCurrentFile(Ljava/lang/String;)V

    .line 168
    monitor-exit v17
    :try_end_35
    .catchall {:try_start_35 .. :try_end_35} :catchall_f

    .line 169
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decIndexFileCount()I

    .line 173
    :cond_34
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->decDocCount()V

    .line 174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v8, v16, v12

    .line 179
    .restart local v8    # "endTime":J
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Lucene End Indexing File extension: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Size in KB: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v18

    const-wide/16 v20, 0x400

    div-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Total Time : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Ovar all Time: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sget-wide v20, Lcom/samsung/index/indexservice/service/DocumentService;->startTime:J

    sub-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    if-eqz v10, :cond_35

    .line 188
    invoke-virtual {v10, v8, v9}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setIndexTotalTime(J)V

    .line 192
    :cond_35
    :try_start_36
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->setIndexParserThreadCallback(Lcom/samsung/index/ITextContentObs;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_36
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_36} :catch_11

    .line 199
    .end local v4    # "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :goto_9
    if-eqz v10, :cond_36

    .line 200
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 201
    :try_start_37
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setCount(I)I

    .line 202
    monitor-exit v17
    :try_end_37
    .catchall {:try_start_37 .. :try_end_37} :catchall_10

    .line 205
    :cond_36
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->isIndexCompleted:Z

    move/from16 v16, v0

    if-nez v16, :cond_0

    .line 213
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v14

    .line 216
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->getInterrupt()Z

    move-result v16

    if-eqz v16, :cond_37

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->shutDownIndexing(Lcom/samsung/index/IFileIndexedNotifier;)V

    goto/16 :goto_0

    .line 168
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .restart local v4    # "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :catchall_f
    move-exception v16

    :try_start_38
    monitor-exit v17
    :try_end_38
    .catchall {:try_start_38 .. :try_end_38} :catchall_f

    throw v16

    .line 194
    .restart local v8    # "endTime":J
    :catch_11
    move-exception v4

    .line 195
    .local v4, "e":Ljava/io/IOException;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 202
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_10
    move-exception v16

    :try_start_39
    monitor-exit v17
    :try_end_39
    .catchall {:try_start_39 .. :try_end_39} :catchall_10

    throw v16

    .line 227
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_37
    sget-boolean v16, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-nez v16, :cond_0

    sget-object v16, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-object/from16 v0, v16

    if-ne v14, v0, :cond_0

    .line 229
    :try_start_3a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->switchIndexToTwoThread()V
    :try_end_3a
    .catch Ljava/lang/Exception; {:try_start_3a .. :try_end_3a} :catch_12

    goto/16 :goto_0

    .line 230
    :catch_12
    move-exception v7

    .line 231
    .restart local v7    # "e3":Ljava/lang/Exception;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception in Switiching the thread.. :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 138
    .end local v7    # "e3":Ljava/lang/Exception;
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catch_13
    move-exception v4

    .line 139
    .local v4, "e":Lcom/samsung/index/PasswordProtectedDocException;
    :try_start_3b
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "PasswordProtectedDocException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Lcom/samsung/index/PasswordProtectedDocException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3b
    .catchall {:try_start_3b .. :try_end_3b} :catchall_17

    .line 141
    const/4 v15, 0x1

    .line 153
    if-nez v11, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    if-eqz v16, :cond_39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_39

    .line 157
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsFileCorrupted:Z

    move/from16 v16, v0

    if-eqz v16, :cond_38

    .line 158
    const/4 v15, 0x3

    .line 160
    :cond_38
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v0, v1, v15}, Lcom/samsung/index/IFileIndexedNotifier;->indexedFile(Ljava/lang/String;I)V

    .line 164
    :cond_39
    if-eqz v10, :cond_3a

    .line 165
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 167
    :try_start_3c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->removeCurrentFile(Ljava/lang/String;)V

    .line 168
    monitor-exit v17
    :try_end_3c
    .catchall {:try_start_3c .. :try_end_3c} :catchall_11

    .line 169
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decIndexFileCount()I

    .line 173
    :cond_3a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->decDocCount()V

    .line 174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v8, v16, v12

    .line 179
    .restart local v8    # "endTime":J
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Lucene End Indexing File extension: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Size in KB: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v18

    const-wide/16 v20, 0x400

    div-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Total Time : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Ovar all Time: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sget-wide v20, Lcom/samsung/index/indexservice/service/DocumentService;->startTime:J

    sub-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    if-eqz v10, :cond_3b

    .line 188
    invoke-virtual {v10, v8, v9}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setIndexTotalTime(J)V

    .line 192
    :cond_3b
    :try_start_3d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->setIndexParserThreadCallback(Lcom/samsung/index/ITextContentObs;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_3d
    .catch Ljava/io/IOException; {:try_start_3d .. :try_end_3d} :catch_14

    .line 199
    .end local v4    # "e":Lcom/samsung/index/PasswordProtectedDocException;
    :goto_a
    if-eqz v10, :cond_3c

    .line 200
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 201
    :try_start_3e
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setCount(I)I

    .line 202
    monitor-exit v17
    :try_end_3e
    .catchall {:try_start_3e .. :try_end_3e} :catchall_12

    .line 205
    :cond_3c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->isIndexCompleted:Z

    move/from16 v16, v0

    if-nez v16, :cond_0

    .line 213
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v14

    .line 216
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->getInterrupt()Z

    move-result v16

    if-eqz v16, :cond_3d

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->shutDownIndexing(Lcom/samsung/index/IFileIndexedNotifier;)V

    goto/16 :goto_0

    .line 168
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .restart local v4    # "e":Lcom/samsung/index/PasswordProtectedDocException;
    :catchall_11
    move-exception v16

    :try_start_3f
    monitor-exit v17
    :try_end_3f
    .catchall {:try_start_3f .. :try_end_3f} :catchall_11

    throw v16

    .line 194
    .restart local v8    # "endTime":J
    :catch_14
    move-exception v4

    .line 195
    .local v4, "e":Ljava/io/IOException;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 202
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_12
    move-exception v16

    :try_start_40
    monitor-exit v17
    :try_end_40
    .catchall {:try_start_40 .. :try_end_40} :catchall_12

    throw v16

    .line 227
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_3d
    sget-boolean v16, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-nez v16, :cond_0

    sget-object v16, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-object/from16 v0, v16

    if-ne v14, v0, :cond_0

    .line 229
    :try_start_41
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->switchIndexToTwoThread()V
    :try_end_41
    .catch Ljava/lang/Exception; {:try_start_41 .. :try_end_41} :catch_15

    goto/16 :goto_0

    .line 230
    :catch_15
    move-exception v7

    .line 231
    .restart local v7    # "e3":Ljava/lang/Exception;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception in Switiching the thread.. :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 142
    .end local v7    # "e3":Ljava/lang/Exception;
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catch_16
    move-exception v5

    .line 143
    .local v5, "e1":Ljava/lang/Exception;
    :try_start_42
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_42
    .catchall {:try_start_42 .. :try_end_42} :catchall_17

    .line 144
    const/4 v15, 0x3

    .line 153
    if-nez v11, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    if-eqz v16, :cond_3f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_3f

    .line 157
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsFileCorrupted:Z

    move/from16 v16, v0

    if-eqz v16, :cond_3e

    .line 158
    const/4 v15, 0x3

    .line 160
    :cond_3e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v0, v1, v15}, Lcom/samsung/index/IFileIndexedNotifier;->indexedFile(Ljava/lang/String;I)V

    .line 164
    :cond_3f
    if-eqz v10, :cond_40

    .line 165
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 167
    :try_start_43
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->removeCurrentFile(Ljava/lang/String;)V

    .line 168
    monitor-exit v17
    :try_end_43
    .catchall {:try_start_43 .. :try_end_43} :catchall_13

    .line 169
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decIndexFileCount()I

    .line 173
    :cond_40
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->decDocCount()V

    .line 174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v8, v16, v12

    .line 179
    .restart local v8    # "endTime":J
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Lucene End Indexing File extension: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Size in KB: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v18

    const-wide/16 v20, 0x400

    div-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Total Time : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Ovar all Time: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sget-wide v20, Lcom/samsung/index/indexservice/service/DocumentService;->startTime:J

    sub-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    if-eqz v10, :cond_41

    .line 188
    invoke-virtual {v10, v8, v9}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setIndexTotalTime(J)V

    .line 192
    :cond_41
    :try_start_44
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->setIndexParserThreadCallback(Lcom/samsung/index/ITextContentObs;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_44
    .catch Ljava/io/IOException; {:try_start_44 .. :try_end_44} :catch_17

    .line 199
    :goto_b
    if-eqz v10, :cond_42

    .line 200
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 201
    :try_start_45
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setCount(I)I

    .line 202
    monitor-exit v17
    :try_end_45
    .catchall {:try_start_45 .. :try_end_45} :catchall_14

    .line 205
    :cond_42
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->isIndexCompleted:Z

    move/from16 v16, v0

    if-nez v16, :cond_0

    .line 213
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v14

    .line 216
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->getInterrupt()Z

    move-result v16

    if-eqz v16, :cond_43

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->shutDownIndexing(Lcom/samsung/index/IFileIndexedNotifier;)V

    goto/16 :goto_0

    .line 168
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catchall_13
    move-exception v16

    :try_start_46
    monitor-exit v17
    :try_end_46
    .catchall {:try_start_46 .. :try_end_46} :catchall_13

    throw v16

    .line 194
    .restart local v8    # "endTime":J
    :catch_17
    move-exception v4

    .line 195
    .restart local v4    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 202
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_14
    move-exception v16

    :try_start_47
    monitor-exit v17
    :try_end_47
    .catchall {:try_start_47 .. :try_end_47} :catchall_14

    throw v16

    .line 227
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_43
    sget-boolean v16, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-nez v16, :cond_0

    sget-object v16, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-object/from16 v0, v16

    if-ne v14, v0, :cond_0

    .line 229
    :try_start_48
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->switchIndexToTwoThread()V
    :try_end_48
    .catch Ljava/lang/Exception; {:try_start_48 .. :try_end_48} :catch_18

    goto/16 :goto_0

    .line 230
    :catch_18
    move-exception v7

    .line 231
    .restart local v7    # "e3":Ljava/lang/Exception;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception in Switiching the thread.. :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 145
    .end local v5    # "e1":Ljava/lang/Exception;
    .end local v7    # "e3":Ljava/lang/Exception;
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catch_19
    move-exception v4

    .line 146
    .local v4, "e":Ljava/lang/Error;
    :try_start_49
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Error on parsing current file, "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_49
    .catchall {:try_start_49 .. :try_end_49} :catchall_17

    .line 151
    const/4 v15, 0x3

    .line 153
    if-nez v11, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    if-eqz v16, :cond_45

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_45

    .line 157
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsFileCorrupted:Z

    move/from16 v16, v0

    if-eqz v16, :cond_44

    .line 158
    const/4 v15, 0x3

    .line 160
    :cond_44
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-interface {v0, v1, v15}, Lcom/samsung/index/IFileIndexedNotifier;->indexedFile(Ljava/lang/String;I)V

    .line 164
    :cond_45
    if-eqz v10, :cond_46

    .line 165
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 167
    :try_start_4a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->removeCurrentFile(Ljava/lang/String;)V

    .line 168
    monitor-exit v17
    :try_end_4a
    .catchall {:try_start_4a .. :try_end_4a} :catchall_15

    .line 169
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decIndexFileCount()I

    .line 173
    :cond_46
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->decDocCount()V

    .line 174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v8, v16, v12

    .line 179
    .restart local v8    # "endTime":J
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Lucene End Indexing File extension: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Size in KB: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v18

    const-wide/16 v20, 0x400

    div-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Total Time : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " Ovar all Time: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sget-wide v20, Lcom/samsung/index/indexservice/service/DocumentService;->startTime:J

    sub-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    if-eqz v10, :cond_47

    .line 188
    invoke-virtual {v10, v8, v9}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setIndexTotalTime(J)V

    .line 192
    :cond_47
    :try_start_4b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->setIndexParserThreadCallback(Lcom/samsung/index/ITextContentObs;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_4b
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_4b} :catch_1a

    .line 199
    .end local v4    # "e":Ljava/lang/Error;
    :goto_c
    if-eqz v10, :cond_48

    .line 200
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 201
    :try_start_4c
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setCount(I)I

    .line 202
    monitor-exit v17
    :try_end_4c
    .catchall {:try_start_4c .. :try_end_4c} :catchall_16

    .line 205
    :cond_48
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->isIndexCompleted:Z

    move/from16 v16, v0

    if-nez v16, :cond_0

    .line 213
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v14

    .line 216
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->getInterrupt()Z

    move-result v16

    if-eqz v16, :cond_49

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->shutDownIndexing(Lcom/samsung/index/IFileIndexedNotifier;)V

    goto/16 :goto_0

    .line 168
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .restart local v4    # "e":Ljava/lang/Error;
    :catchall_15
    move-exception v16

    :try_start_4d
    monitor-exit v17
    :try_end_4d
    .catchall {:try_start_4d .. :try_end_4d} :catchall_15

    throw v16

    .line 194
    .restart local v8    # "endTime":J
    :catch_1a
    move-exception v4

    .line 195
    .local v4, "e":Ljava/io/IOException;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 202
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_16
    move-exception v16

    :try_start_4e
    monitor-exit v17
    :try_end_4e
    .catchall {:try_start_4e .. :try_end_4e} :catchall_16

    throw v16

    .line 227
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_49
    sget-boolean v16, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-nez v16, :cond_0

    sget-object v16, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-object/from16 v0, v16

    if-ne v14, v0, :cond_0

    .line 229
    :try_start_4f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->switchIndexToTwoThread()V
    :try_end_4f
    .catch Ljava/lang/Exception; {:try_start_4f .. :try_end_4f} :catch_1b

    goto/16 :goto_0

    .line 230
    :catch_1b
    move-exception v7

    .line 231
    .restart local v7    # "e3":Ljava/lang/Exception;
    const-string/jumbo v16, "IndexParserThread"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception in Switiching the thread.. :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 153
    .end local v7    # "e3":Ljava/lang/Exception;
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catchall_17
    move-exception v16

    if-nez v11, :cond_0

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    if-eqz v17, :cond_4b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_4b

    .line 157
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsFileCorrupted:Z

    move/from16 v17, v0

    if-eqz v17, :cond_4a

    .line 158
    const/4 v15, 0x3

    .line 160
    :cond_4a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v0, v1, v15}, Lcom/samsung/index/IFileIndexedNotifier;->indexedFile(Ljava/lang/String;I)V

    .line 164
    :cond_4b
    if-eqz v10, :cond_4c

    .line 165
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 167
    :try_start_50
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->filePath:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->removeCurrentFile(Ljava/lang/String;)V

    .line 168
    monitor-exit v17
    :try_end_50
    .catchall {:try_start_50 .. :try_end_50} :catchall_18

    .line 169
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->decIndexFileCount()I

    .line 173
    :cond_4c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->decDocCount()V

    .line 174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v8, v18, v12

    .line 179
    .restart local v8    # "endTime":J
    const-string/jumbo v17, "IndexParserThread"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Lucene End Indexing File extension: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mExtension:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, " Size in KB: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->length()J

    move-result-wide v20

    const-wide/16 v22, 0x400

    div-long v20, v20, v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, " Total Time : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, " Ovar all Time: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sget-wide v22, Lcom/samsung/index/indexservice/service/DocumentService;->startTime:J

    sub-long v20, v20, v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    if-eqz v10, :cond_4d

    .line 188
    invoke-virtual {v10, v8, v9}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setIndexTotalTime(J)V

    .line 192
    :cond_4d
    :try_start_51
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->setIndexParserThreadCallback(Lcom/samsung/index/ITextContentObs;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_51
    .catch Ljava/io/IOException; {:try_start_51 .. :try_end_51} :catch_1c

    .line 199
    :goto_d
    if-eqz v10, :cond_4e

    .line 200
    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    monitor-enter v17

    .line 201
    :try_start_52
    invoke-virtual {v10}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->setCount(I)I

    .line 202
    monitor-exit v17
    :try_end_52
    .catchall {:try_start_52 .. :try_end_52} :catchall_19

    .line 205
    :cond_4e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->isIndexCompleted:Z

    move/from16 v17, v0

    if-nez v17, :cond_0

    .line 213
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v14

    .line 216
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->getInterrupt()Z

    move-result v17

    if-eqz v17, :cond_4f

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->shutDownIndexing(Lcom/samsung/index/IFileIndexedNotifier;)V

    goto/16 :goto_0

    .line 168
    .end local v8    # "endTime":J
    .end local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :catchall_18
    move-exception v16

    :try_start_53
    monitor-exit v17
    :try_end_53
    .catchall {:try_start_53 .. :try_end_53} :catchall_18

    throw v16

    .line 194
    .restart local v8    # "endTime":J
    :catch_1c
    move-exception v4

    .line 195
    .restart local v4    # "e":Ljava/io/IOException;
    const-string/jumbo v17, "IndexParserThread"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "IOException: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 202
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_19
    move-exception v16

    :try_start_54
    monitor-exit v17
    :try_end_54
    .catchall {:try_start_54 .. :try_end_54} :catchall_19

    throw v16

    .line 227
    .restart local v14    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_4f
    sget-boolean v17, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-nez v17, :cond_50

    sget-object v17, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-object/from16 v0, v17

    if-ne v14, v0, :cond_50

    .line 229
    :try_start_55
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->listManager:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->switchIndexToTwoThread()V
    :try_end_55
    .catch Ljava/lang/Exception; {:try_start_55 .. :try_end_55} :catch_1d

    .line 236
    :cond_50
    :goto_e
    throw v16

    .line 230
    :catch_1d
    move-exception v7

    .line 231
    .restart local v7    # "e3":Ljava/lang/Exception;
    const-string/jumbo v17, "IndexParserThread"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Exception in Switiching the thread.. :"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_e
.end method

.method public setCorrupted(Z)V
    .locals 0
    .param p1, "isCorrupted"    # Z

    .prologue
    .line 460
    iput-boolean p1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsFileCorrupted:Z

    .line 461
    return-void
.end method

.method public setIndexCompleted()V
    .locals 1

    .prologue
    .line 465
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->isIndexCompleted:Z

    .line 466
    return-void
.end method

.method public updateText(Ljava/lang/String;)V
    .locals 8
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 381
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 382
    new-instance v4, Ljava/lang/RuntimeException;

    const-string/jumbo v5, "InterruptedException"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 384
    :cond_0
    new-instance v1, Lorg/apache/lucene/document/Document;

    invoke-direct {v1}, Lorg/apache/lucene/document/Document;-><init>()V

    .line 387
    .local v1, "doc":Lorg/apache/lucene/document/Document;
    sget v4, Lcom/samsung/index/ContentIndex;->mHighlightText:I

    if-ne v4, v7, :cond_1

    .line 394
    new-instance v3, Lorg/apache/lucene/document/Field;

    const-string/jumbo v4, "contents"

    sget-object v5, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    sget-object v6, Lorg/apache/lucene/document/Field$Index;->ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;

    invoke-direct {v3, v4, p1, v5, v6}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;)V

    .line 396
    .local v3, "field":Lorg/apache/lucene/document/Field;
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 397
    new-instance v0, Lorg/apache/lucene/document/Field;

    const-string/jumbo v4, "hlcontents"

    invoke-static {p1}, Lorg/apache/lucene/document/CompressionTools;->compressString(Ljava/lang/String;)[B

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;[B)V

    .line 400
    .local v0, "contentValueField":Lorg/apache/lucene/document/Field;
    invoke-virtual {v1, v0}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/document/Fieldable;)V

    .line 407
    .end local v0    # "contentValueField":Lorg/apache/lucene/document/Field;
    :goto_0
    invoke-virtual {v3, v7}, Lorg/apache/lucene/document/Field;->setOmitNorms(Z)V

    .line 411
    :try_start_0
    invoke-virtual {v1, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/document/Fieldable;)V

    .line 414
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mFile:Ljava/io/File;

    invoke-direct {p0, v4, v1}, Lcom/samsung/index/indexservice/threads/IndexParserThread;->setKeyValue(Ljava/io/File;Lorg/apache/lucene/document/Document;)V

    .line 416
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v4, v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->addDocumentSync(Lorg/apache/lucene/document/Document;)V

    .line 418
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThread;->mIsDataSaved:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 425
    :goto_1
    const-wide/16 v4, 0xa

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 429
    :goto_2
    return-void

    .line 402
    .end local v3    # "field":Lorg/apache/lucene/document/Field;
    :cond_1
    new-instance v3, Lorg/apache/lucene/document/Field;

    const-string/jumbo v4, "contents"

    sget-object v5, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    sget-object v6, Lorg/apache/lucene/document/Field$Index;->ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;

    invoke-direct {v3, v4, p1, v5, v6}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;)V

    .restart local v3    # "field":Lorg/apache/lucene/document/Field;
    goto :goto_0

    .line 419
    :catch_0
    move-exception v2

    .line 421
    .local v2, "e":Ljava/lang/Exception;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "ERROR: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 426
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 427
    .local v2, "e":Ljava/lang/InterruptedException;
    const-string/jumbo v4, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Thread is Interrupted: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
