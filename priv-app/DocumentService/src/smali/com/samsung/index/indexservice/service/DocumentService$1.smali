.class Lcom/samsung/index/indexservice/service/DocumentService$1;
.super Ljava/lang/Object;
.source "DocumentService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/index/indexservice/service/DocumentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/index/indexservice/service/DocumentService;


# direct methods
.method constructor <init>(Lcom/samsung/index/indexservice/service/DocumentService;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/samsung/index/indexservice/service/DocumentService$1;->this$0:Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 189
    const/16 v0, 0x11

    .line 190
    .local v0, "which":I
    sget-boolean v1, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-eqz v1, :cond_1

    .line 191
    const/16 v0, 0x11

    .line 202
    :cond_0
    :goto_0
    # getter for: Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentService;->access$000()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 203
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService$1;->this$0:Lcom/samsung/index/indexservice/service/DocumentService;

    # invokes: Lcom/samsung/index/indexservice/service/DocumentService;->beginIndexing(I)V
    invoke-static {v1, v0}, Lcom/samsung/index/indexservice/service/DocumentService;->access$100(Lcom/samsung/index/indexservice/service/DocumentService;I)V

    .line 204
    :goto_1
    return-void

    .line 192
    :cond_1
    sget-boolean v1, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_INDEXING:Z

    if-eqz v1, :cond_0

    .line 193
    sget-boolean v1, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->bIsPowerConnected:Z

    if-eqz v1, :cond_2

    .line 194
    const/16 v0, 0x10

    goto :goto_0

    .line 196
    :cond_2
    # getter for: Lcom/samsung/index/indexservice/service/DocumentService;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentService;->access$000()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 197
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService$1;->this$0:Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentService;->stopService()V

    goto :goto_1
.end method
