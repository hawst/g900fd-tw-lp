.class public Lcom/samsung/index/indexservice/service/DocumentServiceState;
.super Ljava/lang/Object;
.source "DocumentServiceState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/index/indexservice/service/DocumentServiceState$1;,
        Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    }
.end annotation


# static fields
.field public static final INDEX:I = 0x10

.field public static final NONE_FLAG:I = 0x2

.field public static final START_SERVICE:I = 0x13

.field public static final STOP_SERVICE_FLAG:I = 0x1

.field private static final TAG:Ljava/lang/String; = "DocumentServiceState"

.field public static final THUMBNAIL:I = 0x11

.field public static final THUMBNAIL_VERIFY:I = 0x14

.field public static bIsPendingEventAvailable:Z

.field public static bIsThumbnailPendingEventAvailable:Z

.field public static bStopDocService:Z

.field private static mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;


# instance fields
.field private curState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field private preState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field private wakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    sput-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bStopDocService:Z

    .line 53
    sput-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    .line 59
    sput-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsThumbnailPendingEventAvailable:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_NOT_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->curState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->preState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 71
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;
    .locals 2

    .prologue
    .line 74
    const-class v1, Lcom/samsung/index/indexservice/service/DocumentServiceState;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;

    invoke-direct {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState;-><init>()V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    .line 77
    :cond_0
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static killProcess()V
    .locals 5

    .prologue
    .line 146
    sget-boolean v2, Lcom/samsung/index/provider/SearchProvider;->isSearch:Z

    if-nez v2, :cond_1

    sget-boolean v2, Lcom/samsung/thumbnail/provider/ThumbnailProvider;->isThumbnailOn:Z

    if-nez v2, :cond_1

    .line 148
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v2}, Lcom/samsung/index/DocumentDatabaseManager;->getInstance(Landroid/content/Context;)Lcom/samsung/index/DocumentDatabaseManager;

    move-result-object v0

    .line 152
    .local v0, "dbMgr":Lcom/samsung/index/DocumentDatabaseManager;
    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {v0}, Lcom/samsung/index/DocumentDatabaseManager;->close()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :cond_0
    :goto_0
    const-string/jumbo v2, "DocumentServiceState"

    const-string/jumbo v3, "[KILL] Killing Document Service Process"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-static {v2}, Landroid/os/Process;->killProcess(I)V

    .line 162
    :cond_1
    return-void

    .line 156
    :catch_0
    move-exception v1

    .line 157
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    const-string/jumbo v2, "DocumentServiceState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "SQLiteException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public acquireWakeLock(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 171
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->wakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 172
    const-string/jumbo v1, "power"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 174
    .local v0, "pm":Landroid/os/PowerManager;
    const/4 v1, 0x1

    const-string/jumbo v2, "DocumentService WakeLock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->wakeLock:Landroid/os/PowerManager$WakeLock;

    .line 176
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 178
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    return-void
.end method

.method public declared-synchronized getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .locals 1

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPreviousState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .locals 2

    .prologue
    .line 135
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceState;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    monitor-enter v1

    .line 136
    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->preState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    monitor-exit v1

    return-object v0

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .locals 2
    .param p1, "flag"    # I

    .prologue
    .line 87
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->nextState(IZLandroid/content/Context;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v0

    return-object v0
.end method

.method public nextState(IZLandroid/content/Context;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .locals 4
    .param p1, "flag"    # I
    .param p2, "systemCheck"    # Z
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceState;->mDocServiceState:Lcom/samsung/index/indexservice/service/DocumentServiceState;

    monitor-enter v1

    .line 102
    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->curState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-eqz v0, :cond_0

    .line 105
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->curState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 106
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->curState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->preState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 107
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->curState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    invoke-virtual {v0, p1}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->curState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 109
    const-string/jumbo v0, "DocumentServiceState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Previous State:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->preState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " | Current State:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->curState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    if-eqz p2, :cond_0

    invoke-static {p3}, Lcom/samsung/index/indexservice/service/SystemInfo;->checkSystemStatus(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->NONE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    monitor-exit v1

    .line 116
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->curState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    monitor-exit v1

    goto :goto_0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public releaseWakelock()V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->wakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->wakeLock:Landroid/os/PowerManager$WakeLock;

    .line 188
    :cond_0
    return-void
.end method
