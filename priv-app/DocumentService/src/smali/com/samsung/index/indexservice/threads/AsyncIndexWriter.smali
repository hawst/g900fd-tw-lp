.class public Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;
.super Ljava/lang/Object;
.source "AsyncIndexWriter.java"


# static fields
.field private static final MAXMEMORYALLOWEDINQUE:I = 0xa00000

.field private static final TAG:Ljava/lang/String; = "AsyncIndexWriter"

.field public static count:I

.field private static syncObject:Ljava/lang/Object;


# instance fields
.field private mAlgorithm:Ljava/lang/String;

.field private mAppName:Ljava/lang/String;

.field public mContext:Landroid/content/Context;

.field private mDocuments:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;"
        }
    .end annotation
.end field

.field private mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

.field private mPassword:Ljava/lang/String;

.field public mStartTime:J

.field private mStopParseThread:Z

.field private mTotalDocumentsCount:J

.field public mTotalDocumentsIndexed:J

.field private mTotalIndexCount:J

.field private mTotalSize:J

.field private parserThreadCallback:Lcom/samsung/index/ITextContentObs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 265
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->syncObject:Ljava/lang/Object;

    .line 315
    const/4 v0, 0x0

    sput v0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->count:I

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLandroid/content/Context;)V
    .locals 10
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "appname"    # Ljava/lang/String;
    .param p3, "algorithm"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;
    .param p5, "queueSize"    # I
    .param p6, "sleepMilisecondOnEmpty"    # J
    .param p8, "context"    # Landroid/content/Context;

    .prologue
    .line 444
    new-instance v5, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v5, p5}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;-><init>(Lorg/apache/lucene/index/IndexWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/BlockingQueue;JLandroid/content/Context;)V

    .line 447
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 9
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "appname"    # Ljava/lang/String;
    .param p3, "algorithm"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;
    .param p5, "context"    # Landroid/content/Context;

    .prologue
    .line 429
    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;-><init>(Lorg/apache/lucene/index/IndexWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLandroid/content/Context;)V

    .line 431
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/BlockingQueue;JLandroid/content/Context;)V
    .locals 4
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "appname"    # Ljava/lang/String;
    .param p3, "algorithm"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;
    .param p6, "sleepMSOnEmpty"    # J
    .param p8, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/IndexWriter;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;J",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .local p5, "queue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Lorg/apache/lucene/document/Document;>;"
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 458
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-wide v2, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalSize:J

    .line 73
    iput-wide v2, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalIndexCount:J

    .line 74
    iput-wide v2, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsCount:J

    .line 76
    iput-object v1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mAppName:Ljava/lang/String;

    .line 77
    iput-object v1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mAlgorithm:Ljava/lang/String;

    .line 78
    iput-object v1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mPassword:Ljava/lang/String;

    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mStopParseThread:Z

    .line 85
    iput-wide v2, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mStartTime:J

    .line 86
    iput-wide v2, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsIndexed:J

    .line 88
    iput-object v1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mContext:Landroid/content/Context;

    .line 460
    iput-object p2, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mAppName:Ljava/lang/String;

    .line 461
    iput-object p3, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mAlgorithm:Ljava/lang/String;

    .line 462
    iput-object p4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mPassword:Ljava/lang/String;

    .line 463
    iput-object p1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    .line 464
    iput-object p5, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mDocuments:Ljava/util/concurrent/BlockingQueue;

    .line 466
    iput-object p8, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mContext:Landroid/content/Context;

    .line 469
    return-void
.end method

.method private declared-synchronized addDocumentSynchronous(Lorg/apache/lucene/document/Document;)V
    .locals 1
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/lucene/index/CorruptIndexException;
        }
    .end annotation

    .prologue
    .line 351
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 362
    :goto_0
    monitor-exit p0

    return-void

    .line 360
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->addDocument(Lorg/apache/lucene/document/Document;)V

    .line 361
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriterLocal(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 351
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized addSize(J)V
    .locals 5
    .param p1, "inSize"    # J

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalIndexCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalIndexCount:J

    .line 116
    iget-wide v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalSize:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalSize:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    monitor-exit p0

    return-void

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private broadcastIndexCommitted()V
    .locals 2

    .prologue
    .line 225
    sget-boolean v1, Lcom/samsung/index/provider/SearchProvider;->isSearch:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 226
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "action.documentservice.index.finished"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 228
    .local v0, "indexCommitIntent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.app.myfiles"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 231
    .end local v0    # "indexCommitIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private closeWriter()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 138
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    if-nez v1, :cond_0

    .line 150
    :goto_0
    return-void

    .line 141
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriter;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    iput-object v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    goto :goto_0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ERROR on Closing writer (AsynIndexWriter): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147
    iput-object v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iput-object v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    throw v1
.end method

.method private commitIndexWriter()V
    .locals 8

    .prologue
    .line 234
    const/4 v2, 0x0

    .line 235
    .local v2, "indexCorrupted":Z
    sget-object v4, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->syncObject:Ljava/lang/Object;

    monitor-enter v4

    .line 236
    :try_start_0
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    .line 238
    :try_start_1
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriter;->commit()V

    .line 239
    invoke-direct {p0}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->broadcastIndexCommitted()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 248
    :cond_0
    :goto_0
    const-wide/16 v6, 0x0

    :try_start_2
    iput-wide v6, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalSize:J

    .line 249
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalIndexCount:J

    .line 251
    if-eqz v2, :cond_2

    .line 252
    new-instance v1, Lcom/samsung/index/LuceneHelper;

    invoke-direct {v1}, Lcom/samsung/index/LuceneHelper;-><init>()V

    .line 253
    .local v1, "helper":Lcom/samsung/index/LuceneHelper;
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mAppName:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3, v5}, Lcom/samsung/index/LuceneHelper;->initLuceneConfiguration(Ljava/lang/String;Landroid/content/Context;)Z

    .line 254
    invoke-virtual {v1}, Lcom/samsung/index/LuceneHelper;->deleteCurrentIndexPath()Z

    .line 255
    invoke-virtual {v1}, Lcom/samsung/index/LuceneHelper;->closeIndexReader()V

    .line 258
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->processRestart(Landroid/content/Context;)V

    .line 259
    monitor-exit v4

    .line 262
    .end local v1    # "helper":Lcom/samsung/index/LuceneHelper;
    :goto_1
    return-void

    .line 240
    :catch_0
    move-exception v0

    .line 241
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "ERROR:  commitIndexWriter "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    instance-of v3, v0, Ljava/io/FileNotFoundException;

    if-nez v3, :cond_1

    instance-of v3, v0, Lorg/apache/lucene/index/CorruptIndexException;

    if-eqz v3, :cond_0

    .line 244
    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    .line 261
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    monitor-exit v4

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method private initWriter()V
    .locals 6

    .prologue
    .line 336
    new-instance v1, Lcom/samsung/index/LuceneHelper;

    invoke-direct {v1}, Lcom/samsung/index/LuceneHelper;-><init>()V

    .line 339
    .local v1, "helper":Lcom/samsung/index/LuceneHelper;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mAppName:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mAlgorithm:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mPassword:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/index/LuceneHelper;->getNewSyncIndexWriter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lorg/apache/lucene/index/IndexWriter;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 344
    invoke-virtual {v1}, Lcom/samsung/index/LuceneHelper;->closeIndexReader()V

    .line 347
    :goto_0
    return-void

    .line 341
    :catch_0
    move-exception v0

    .line 342
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception on initWriter: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344
    invoke-virtual {v1}, Lcom/samsung/index/LuceneHelper;->closeIndexReader()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/samsung/index/LuceneHelper;->closeIndexReader()V

    throw v2
.end method

.method private restartWriterLocal(Lcom/samsung/index/IFileIndexedNotifier;)V
    .locals 10
    .param p1, "indexCompleted"    # Lcom/samsung/index/IFileIndexedNotifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    .line 154
    iget-wide v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalSize:J

    const-wide/32 v6, 0xa00000

    cmp-long v4, v4, v6

    if-gtz v4, :cond_0

    iget-wide v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalIndexCount:J

    const-wide/16 v6, 0x32

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    iget-wide v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsCount:J

    cmp-long v4, v4, v8

    if-nez v4, :cond_2

    .line 156
    :cond_0
    sget v4, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->count:I

    add-int/lit8 v4, v4, 0x1

    sput v4, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->count:I

    .line 158
    const-string/jumbo v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Lucene Committing writer in AysncIndexWriter "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->count:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "docCount "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsCount:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    invoke-direct {p0}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->commitIndexWriter()V

    .line 165
    iget-wide v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsCount:J

    cmp-long v4, v8, v4

    if-nez v4, :cond_3

    .line 169
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->parserThreadCallback:Lcom/samsung/index/ITextContentObs;

    if-eqz v4, :cond_1

    .line 170
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->parserThreadCallback:Lcom/samsung/index/ITextContentObs;

    invoke-interface {v4}, Lcom/samsung/index/ITextContentObs;->setIndexCompleted()V

    .line 176
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->closeAndRestartIndexWriter()V

    .line 180
    const-wide/16 v0, 0x0

    .line 181
    .local v0, "endtime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 182
    const-string/jumbo v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Lucene Index End Time : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " time taken "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mStartTime:J

    sub-long v6, v0, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " total documents "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsIndexed:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    if-eqz p1, :cond_2

    .line 190
    iget-boolean v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mStopParseThread:Z

    if-nez v4, :cond_2

    .line 191
    invoke-interface {p1}, Lcom/samsung/index/IFileIndexedNotifier;->indexCompleted()V

    .line 216
    .end local v0    # "endtime":J
    :cond_2
    :goto_0
    return-void

    .line 197
    :cond_3
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_4

    .line 198
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/index/indexservice/service/SystemInfo;->checkSystemStatus(Landroid/content/Context;)Z

    move-result v2

    .line 200
    .local v2, "isSysStatusOk":Z
    if-nez v2, :cond_4

    .line 201
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v3

    .line 202
    .local v3, "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    sget-object v4, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_NOT_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-eq v3, v4, :cond_2

    sget-object v4, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_STOPPED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-eq v3, v4, :cond_2

    .line 205
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->stopDocumentService(Landroid/content/Context;)V

    goto :goto_0

    .line 213
    .end local v2    # "isSysStatusOk":Z
    .end local v3    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    :cond_4
    const-string/jumbo v4, "restartIndexWriterLocal"

    invoke-static {v4}, Lcom/samsung/index/indexservice/service/SystemInfo;->checkTemperatureLevel(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public addDocument(Lorg/apache/lucene/document/Document;)V
    .locals 5
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 400
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mDocuments:Ljava/util/concurrent/BlockingQueue;

    if-eqz v4, :cond_1

    .line 401
    const-string/jumbo v4, "contents"

    invoke-virtual {p1, v4}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/document/Field;

    move-result-object v0

    .line 402
    .local v0, "field":Lorg/apache/lucene/document/Field;
    if-eqz v0, :cond_0

    .line 403
    invoke-virtual {v0}, Lorg/apache/lucene/document/Field;->stringValue()Ljava/lang/String;

    move-result-object v1

    .line 404
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 405
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    int-to-long v2, v4

    .line 406
    .local v2, "size":J
    invoke-direct {p0, v2, v3}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->addSize(J)V

    .line 409
    .end local v1    # "value":Ljava/lang/String;
    .end local v2    # "size":J
    :cond_0
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mDocuments:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v4, p1}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    .line 417
    .end local v0    # "field":Lorg/apache/lucene/document/Field;
    :cond_1
    return-void
.end method

.method public addDocumentSync(Lorg/apache/lucene/document/Document;)V
    .locals 5
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/lucene/index/CorruptIndexException;
        }
    .end annotation

    .prologue
    .line 375
    if-eqz p1, :cond_0

    .line 376
    const-string/jumbo v4, "contents"

    invoke-virtual {p1, v4}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/document/Field;

    move-result-object v0

    .line 377
    .local v0, "field":Lorg/apache/lucene/document/Field;
    if-eqz v0, :cond_0

    .line 378
    invoke-virtual {v0}, Lorg/apache/lucene/document/Field;->stringValue()Ljava/lang/String;

    move-result-object v1

    .line 379
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 380
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    int-to-long v2, v4

    .line 381
    .local v2, "size":J
    invoke-direct {p0, v2, v3}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->addSize(J)V

    .line 385
    .end local v0    # "field":Lorg/apache/lucene/document/Field;
    .end local v1    # "value":Ljava/lang/String;
    .end local v2    # "size":J
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->addDocumentSynchronous(Lorg/apache/lucene/document/Document;)V

    .line 386
    return-void
.end method

.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 545
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v0, :cond_0

    .line 546
    invoke-direct {p0}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->closeWriter()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548
    :cond_0
    monitor-exit p0

    return-void

    .line 545
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public closeAndRestartIndexWriter()V
    .locals 8

    .prologue
    .line 268
    const/4 v2, 0x0

    .line 269
    .local v2, "indexCorrupted":Z
    sget-object v4, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->syncObject:Ljava/lang/Object;

    monitor-enter v4

    .line 270
    :try_start_0
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    .line 272
    :try_start_1
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriter;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 283
    :cond_0
    :goto_0
    const/4 v3, 0x0

    :try_start_2
    iput-object v3, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    .line 284
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalSize:J

    .line 285
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalIndexCount:J

    .line 287
    if-eqz v2, :cond_2

    .line 288
    new-instance v1, Lcom/samsung/index/LuceneHelper;

    invoke-direct {v1}, Lcom/samsung/index/LuceneHelper;-><init>()V

    .line 289
    .local v1, "helper":Lcom/samsung/index/LuceneHelper;
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mAppName:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3, v5}, Lcom/samsung/index/LuceneHelper;->initLuceneConfiguration(Ljava/lang/String;Landroid/content/Context;)Z

    .line 290
    invoke-virtual {v1}, Lcom/samsung/index/LuceneHelper;->deleteCurrentIndexPath()Z

    .line 291
    invoke-virtual {v1}, Lcom/samsung/index/LuceneHelper;->closeIndexReader()V

    .line 294
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->processRestart(Landroid/content/Context;)V

    .line 295
    monitor-exit v4

    .line 304
    .end local v1    # "helper":Lcom/samsung/index/LuceneHelper;
    :goto_1
    return-void

    .line 273
    :catch_0
    move-exception v0

    .line 275
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "DocumentService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "ERROR:  closeAndRestartIndexWriter "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    instance-of v3, v0, Ljava/io/FileNotFoundException;

    if-nez v3, :cond_1

    instance-of v3, v0, Lorg/apache/lucene/index/CorruptIndexException;

    if-eqz v3, :cond_0

    .line 278
    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    .line 301
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-direct {p0}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->initWriter()V

    .line 303
    monitor-exit v4

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public declared-synchronized closeNoMerge()V
    .locals 6

    .prologue
    .line 552
    monitor-enter p0

    const/4 v1, 0x0

    .line 553
    .local v1, "success":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    .line 555
    :try_start_1
    iget-object v2, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexWriter;->close(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 556
    const/4 v1, 0x1

    .line 561
    if-nez v1, :cond_0

    .line 563
    :try_start_2
    iget-object v2, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexWriter;->rollback()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 568
    :cond_0
    :goto_0
    const/4 v2, 0x0

    :try_start_3
    iput-object v2, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 571
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 564
    :catch_0
    move-exception v0

    .line 565
    .local v0, "e":Ljava/io/IOException;
    :try_start_4
    const-string/jumbo v2, "AsyncIndexWriter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "IOException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 552
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 557
    :catch_1
    move-exception v0

    .line 558
    .local v0, "e":Ljava/lang/Exception;
    :try_start_5
    const-string/jumbo v2, "AsyncIndexWriter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "ERROR while closing without merge: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 561
    if-nez v1, :cond_2

    .line 563
    :try_start_6
    iget-object v2, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexWriter;->rollback()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 568
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_2
    const/4 v2, 0x0

    :try_start_7
    iput-object v2, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    goto :goto_1

    .line 564
    .restart local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 565
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v2, "AsyncIndexWriter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "IOException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2

    .line 561
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v2

    if-nez v1, :cond_3

    .line 563
    :try_start_8
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriter;->rollback()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 568
    :cond_3
    :goto_3
    const/4 v3, 0x0

    :try_start_9
    iput-object v3, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    throw v2

    .line 564
    :catch_3
    move-exception v0

    .line 565
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v3, "AsyncIndexWriter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3
.end method

.method public declared-synchronized commitAfterDelete()V
    .locals 4

    .prologue
    .line 493
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 495
    :try_start_1
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriter;->commit()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 500
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 496
    :catch_0
    move-exception v0

    .line 497
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string/jumbo v1, "AsyncIndexWriter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception:  commitIndexWriter "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 493
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized decDocCount()V
    .locals 4

    .prologue
    .line 125
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsCount:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsCount:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    monitor-exit p0

    return-void

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized deleteIndex(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 480
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 487
    :goto_0
    monitor-exit p0

    return-void

    .line 483
    :cond_0
    :try_start_1
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    .local v0, "term":Lorg/apache/lucene/index/Term;
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/index/IndexWriter;->deleteDocuments(Lorg/apache/lucene/index/Term;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 480
    .end local v0    # "term":Lorg/apache/lucene/index/Term;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized deleteIndex(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 8
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "values"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 504
    monitor-enter p0

    :try_start_0
    iget-object v7, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v7, :cond_0

    .line 518
    :goto_0
    monitor-exit p0

    return-void

    .line 507
    :cond_0
    :try_start_1
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 508
    .local v5, "termList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    move-object v0, p2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 509
    .local v3, "param":Ljava/lang/String;
    new-instance v4, Lorg/apache/lucene/index/Term;

    invoke-direct {v4, p1, v3}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    .local v4, "term":Lorg/apache/lucene/index/Term;
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 508
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 513
    .end local v3    # "param":Ljava/lang/String;
    .end local v4    # "term":Lorg/apache/lucene/index/Term;
    :cond_1
    const/4 v6, 0x0

    .line 514
    .local v6, "terms":[Lorg/apache/lucene/index/Term;
    const/4 v7, 0x0

    new-array v7, v7, [Lorg/apache/lucene/index/Term;

    invoke-interface {v5, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "terms":[Lorg/apache/lucene/index/Term;
    check-cast v6, [Lorg/apache/lucene/index/Term;

    .line 516
    .restart local v6    # "terms":[Lorg/apache/lucene/index/Term;
    iget-object v7, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v7, v6}, Lorg/apache/lucene/index/IndexWriter;->deleteDocuments([Lorg/apache/lucene/index/Term;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 504
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v5    # "termList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    .end local v6    # "terms":[Lorg/apache/lucene/index/Term;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7
.end method

.method public declared-synchronized deleteIndex(Ljava/util/HashMap;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 522
    .local p1, "keyvalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v5, :cond_0

    .line 542
    :goto_0
    monitor-exit p0

    return-void

    .line 524
    :cond_0
    :try_start_1
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 525
    .local v0, "bq":Lorg/apache/lucene/search/BooleanQuery;
    new-instance v4, Lorg/apache/lucene/search/MultiPhraseQuery;

    invoke-direct {v4}, Lorg/apache/lucene/search/MultiPhraseQuery;-><init>()V

    .line 527
    .local v4, "mpQueryFolder":Lorg/apache/lucene/search/MultiPhraseQuery;
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 530
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v6, "AsyncIndexWriter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Search Key = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, ", Value = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    new-instance v3, Lorg/apache/lucene/index/Term;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {v3, v5, v6}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    .local v3, "inexTerm":Lorg/apache/lucene/index/Term;
    invoke-virtual {v4, v3}, Lorg/apache/lucene/search/MultiPhraseQuery;->add(Lorg/apache/lucene/index/Term;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 522
    .end local v0    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "inexTerm":Lorg/apache/lucene/index/Term;
    .end local v4    # "mpQueryFolder":Lorg/apache/lucene/search/MultiPhraseQuery;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 538
    .restart local v0    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v4    # "mpQueryFolder":Lorg/apache/lucene/search/MultiPhraseQuery;
    :cond_1
    :try_start_2
    sget-object v5, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 540
    iget-object v5, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v5, v0}, Lorg/apache/lucene/index/IndexWriter;->deleteDocuments(Lorg/apache/lucene/search/Query;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized getPendingDocCount()J
    .locals 2

    .prologue
    .line 332
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsCount:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getStopParserThread()Z
    .locals 1

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mStopParseThread:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getStopParserThreadAndDecCount(Lcom/samsung/index/IFileIndexedNotifier;)Z
    .locals 4
    .param p1, "indexCompleted"    # Lcom/samsung/index/IFileIndexedNotifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mStopParseThread:Z

    if-eqz v0, :cond_0

    .line 108
    iget-wide v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsCount:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsCount:J

    .line 109
    invoke-direct {p0, p1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriterLocal(Lcom/samsung/index/IFileIndexedNotifier;)V

    .line 111
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mStopParseThread:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized incDocCount()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 120
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsCount:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsCount:J

    .line 121
    iget-wide v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsIndexed:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsIndexed:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    monitor-exit p0

    return-void

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public maxDoc()I
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    if-nez v0, :cond_0

    .line 473
    const/4 v0, -0x1

    .line 474
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->maxDoc()I

    move-result v0

    goto :goto_0
.end method

.method public declared-synchronized restartWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    .locals 1
    .param p1, "indexCompleted"    # Lcom/samsung/index/IFileIndexedNotifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 319
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mIndexWriter:Lorg/apache/lucene/index/IndexWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 329
    :goto_0
    monitor-exit p0

    return-void

    .line 328
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriterLocal(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 319
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setDocCount(J)V
    .locals 1
    .param p1, "count"    # J

    .prologue
    .line 134
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mTotalDocumentsCount:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    monitor-exit p0

    return-void

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setIndexParserThreadCallback(Lcom/samsung/index/ITextContentObs;)V
    .locals 0
    .param p1, "callback"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    .line 312
    iput-object p1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->parserThreadCallback:Lcom/samsung/index/ITextContentObs;

    .line 313
    return-void
.end method

.method public declared-synchronized setStopParserThread(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mStopParseThread:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    monitor-exit p0

    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
