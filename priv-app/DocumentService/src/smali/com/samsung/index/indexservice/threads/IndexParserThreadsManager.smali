.class public Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;
.super Ljava/lang/Object;
.source "IndexParserThreadsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;
    }
.end annotation


# static fields
.field public static final MAX_MEDIA_FILE_SIZE:I = 0x6400000

.field public static final OOXMLMAXDOCSIZE:I = 0x500000

.field public static final POIMAXDOCSIZE:I = 0xa00000

.field private static final TAG:Ljava/lang/String; = "IndexParserThreadsManager"


# instance fields
.field private activeExecutor:I

.field private bInterrupt:Z

.field private indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

.field private mAsyncIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

.field private mContext:Landroid/content/Context;

.field private mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private mExecutorWithOneThread:Ljava/util/concurrent/ThreadPoolExecutor;

.field private mFilesToIndex:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mIndexThread:Ljava/lang/Thread;

.field private mKeepRunning:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "indexWritern"    # Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mKeepRunning:Z

    .line 51
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->activeExecutor:I

    .line 65
    iput-object p1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mContext:Landroid/content/Context;

    .line 67
    invoke-direct {p0}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->initDoubleThreadExecutor()V

    .line 68
    invoke-direct {p0}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->initSingleThreadExecutor()V

    .line 70
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    .line 72
    iput-object p2, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mAsyncIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    .line 73
    new-instance v0, Ljava/lang/Thread;

    const-string/jumbo v1, "IndexParserThreadsManager"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mIndexThread:Ljava/lang/Thread;

    .line 74
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mIndexThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 75
    return-void
.end method

.method private createIndex(Ljava/io/File;Ljava/lang/String;Ljava/util/HashMap;Lcom/samsung/index/IFileIndexedNotifier;ZLjava/util/concurrent/ThreadPoolExecutor;)Z
    .locals 14
    .param p1, "file"    # Ljava/io/File;
    .param p2, "attachmentName"    # Ljava/lang/String;
    .param p4, "filenotifier"    # Lcom/samsung/index/IFileIndexedNotifier;
    .param p5, "isSynchronous"    # Z
    .param p6, "threadPoolExecutor"    # Ljava/util/concurrent/ThreadPoolExecutor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/samsung/index/IFileIndexedNotifier;",
            "Z",
            "Ljava/util/concurrent/ThreadPoolExecutor;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 325
    .local p3, "setKeyValue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v13, 0x0

    .line 327
    .local v13, "success":Z
    invoke-static/range {p2 .. p2}, Lcom/samsung/index/Utils;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 328
    .local v7, "extension":Ljava/lang/String;
    if-nez v7, :cond_0

    move v3, v13

    .line 359
    :goto_0
    return v3

    .line 332
    :cond_0
    new-instance v2, Lcom/samsung/index/indexservice/threads/IndexParserThread;

    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mAsyncIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    iget-object v9, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mContext:Landroid/content/Context;

    move-object/from16 v4, p4

    move-object v5, p1

    move-object/from16 v6, p3

    move/from16 v8, p5

    move-object v10, p0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/index/indexservice/threads/IndexParserThread;-><init>(Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;Lcom/samsung/index/IFileIndexedNotifier;Ljava/io/File;Ljava/util/HashMap;Ljava/lang/String;ZLandroid/content/Context;Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;)V

    .line 337
    .local v2, "tempThread":Lcom/samsung/index/indexservice/threads/IndexParserThread;
    invoke-direct {p0, v2}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->isFileProcessing(Lcom/samsung/index/indexservice/threads/IndexParserThread;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 340
    const/4 v3, 0x1

    goto :goto_0

    .line 342
    :cond_1
    if-eqz p5, :cond_3

    .line 343
    invoke-virtual {v2}, Lcom/samsung/index/indexservice/threads/IndexParserThread;->run()V

    .line 347
    :goto_1
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-eqz v3, :cond_2

    .line 348
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->addIndexFileCount()I

    .line 349
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v12

    .line 350
    .local v12, "state":Lcom/samsung/index/indexservice/service/DocumentServiceState;
    invoke-virtual {v12}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v11

    .line 351
    .local v11, "currentState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    sget-object v3, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_INDEXING_THUMBNAIL_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-eq v11, v3, :cond_2

    .line 352
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v8, 0x400

    div-long/2addr v4, v8

    long-to-int v4, v4

    invoke-virtual {v3, v7, v4}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->addTotalFilesCount(Ljava/lang/String;I)V

    .line 356
    .end local v11    # "currentState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .end local v12    # "state":Lcom/samsung/index/indexservice/service/DocumentServiceState;
    :cond_2
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mAsyncIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v3}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->incDocCount()V

    .line 357
    const/4 v13, 0x1

    move v3, v13

    .line 359
    goto :goto_0

    .line 345
    :cond_3
    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method private indexDirectory(Ljava/io/File;Ljava/util/HashMap;Lcom/samsung/index/IFileIndexedNotifier;Ljava/util/concurrent/ThreadPoolExecutor;)Z
    .locals 15
    .param p1, "dataDir"    # Ljava/io/File;
    .param p3, "filenotifier"    # Lcom/samsung/index/IFileIndexedNotifier;
    .param p4, "threadPoolExecutor"    # Ljava/util/concurrent/ThreadPoolExecutor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/samsung/index/IFileIndexedNotifier;",
            "Ljava/util/concurrent/ThreadPoolExecutor;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 224
    .local p2, "setKeyValue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v14, 0x0

    .line 227
    .local v14, "success":Z
    const/4 v11, 0x0

    .line 228
    .local v11, "files":[Ljava/io/File;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->isFile()Z

    move-result v3

    if-nez v3, :cond_5

    .line 229
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->listFiles()[Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    move-object v12, v11

    .line 231
    .end local v11    # "files":[Ljava/io/File;
    .local v12, "files":[Ljava/io/File;
    :goto_0
    if-nez v12, :cond_4

    .line 232
    const/4 v3, 0x1

    :try_start_1
    new-array v11, v3, [Ljava/io/File;

    const/4 v3, 0x0

    aput-object p1, v11, v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 235
    .end local v12    # "files":[Ljava/io/File;
    .restart local v11    # "files":[Ljava/io/File;
    :goto_1
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    :try_start_2
    array-length v3, v11

    if-ge v13, v3, :cond_3

    .line 236
    aget-object v4, v11, v13

    .line 240
    .local v4, "file":Ljava/io/File;
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mAsyncIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v3}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->getStopParserThread()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 241
    const/4 v3, 0x0

    .line 262
    .end local v4    # "file":Ljava/io/File;
    .end local v13    # "i":I
    :goto_3
    return v3

    .line 244
    .restart local v4    # "file":Ljava/io/File;
    .restart local v13    # "i":I
    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 245
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-direct {p0, v4, v0, v1, v2}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->indexDirectory(Ljava/io/File;Ljava/util/HashMap;Lcom/samsung/index/IFileIndexedNotifier;Ljava/util/concurrent/ThreadPoolExecutor;)Z

    .line 235
    :cond_1
    :goto_4
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 248
    :cond_2
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    .line 250
    .local v5, "fileName":Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/index/Utils;->isSupported(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 252
    const/4 v8, 0x0

    move-object v3, p0

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v9, p4

    invoke-direct/range {v3 .. v9}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->createIndex(Ljava/io/File;Ljava/lang/String;Ljava/util/HashMap;Lcom/samsung/index/IFileIndexedNotifier;ZLjava/util/concurrent/ThreadPoolExecutor;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v14

    goto :goto_4

    .line 258
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "fileName":Ljava/lang/String;
    .end local v13    # "i":I
    :catch_0
    move-exception v10

    .line 259
    .local v10, "e":Ljava/lang/Exception;
    :goto_5
    const-string/jumbo v3, "IndexParserThreadsManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    const/4 v14, 0x0

    .end local v10    # "e":Ljava/lang/Exception;
    :cond_3
    move v3, v14

    .line 262
    goto :goto_3

    .line 258
    .end local v11    # "files":[Ljava/io/File;
    .restart local v12    # "files":[Ljava/io/File;
    :catch_1
    move-exception v10

    move-object v11, v12

    .end local v12    # "files":[Ljava/io/File;
    .restart local v11    # "files":[Ljava/io/File;
    goto :goto_5

    .end local v11    # "files":[Ljava/io/File;
    .restart local v12    # "files":[Ljava/io/File;
    :cond_4
    move-object v11, v12

    .end local v12    # "files":[Ljava/io/File;
    .restart local v11    # "files":[Ljava/io/File;
    goto :goto_1

    :cond_5
    move-object v12, v11

    .end local v11    # "files":[Ljava/io/File;
    .restart local v12    # "files":[Ljava/io/File;
    goto :goto_0
.end method

.method private initDoubleThreadExecutor()V
    .locals 8

    .prologue
    const/4 v2, 0x2

    .line 78
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v4, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move v3, v2

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 81
    return-void
.end method

.method private initSingleThreadExecutor()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 84
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v4, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move v3, v2

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutorWithOneThread:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 87
    return-void
.end method

.method private isFileProcessing(Lcom/samsung/index/indexservice/threads/IndexParserThread;)Z
    .locals 6
    .param p1, "parserThread"    # Lcom/samsung/index/indexservice/threads/IndexParserThread;

    .prologue
    .line 272
    const/4 v1, 0x0

    .line 274
    .local v1, "isProcessing":Z
    :try_start_0
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v3}, Ljava/util/concurrent/ThreadPoolExecutor;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v2

    .line 275
    .local v2, "queue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Ljava/lang/Runnable;>;"
    invoke-interface {v2, p1}, Ljava/util/concurrent/BlockingQueue;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    move v3, v1

    .line 281
    .end local v2    # "queue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Ljava/lang/Runnable;>;"
    :goto_0
    return v3

    .line 276
    :catch_0
    move-exception v0

    .line 277
    .local v0, "ignore":Ljava/util/ConcurrentModificationException;
    const-string/jumbo v3, "IndexParserThreadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "ConcurrentModificationException :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/util/ConcurrentModificationException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private restartIndexWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    .locals 6
    .param p1, "mFilenotifier"    # Lcom/samsung/index/IFileIndexedNotifier;

    .prologue
    const-wide/16 v4, 0x0

    .line 209
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mAsyncIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->getPendingDocCount()J

    move-result-wide v2

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 218
    :goto_0
    return-void

    .line 212
    :cond_0
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mAsyncIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v1, v4, v5}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->setDocCount(J)V

    .line 214
    :try_start_0
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mAsyncIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v1, p1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->restartWriter(Lcom/samsung/index/IFileIndexedNotifier;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 215
    :catch_0
    move-exception v0

    .line 216
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v1, "IndexParserThreadsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "IOException : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public clearQueue()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->clear()V

    .line 101
    return-void
.end method

.method public closeNow()V
    .locals 1

    .prologue
    .line 363
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mKeepRunning:Z

    .line 364
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    .line 367
    :cond_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutorWithOneThread:Ljava/util/concurrent/ThreadPoolExecutor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutorWithOneThread:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 369
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutorWithOneThread:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    .line 371
    :cond_1
    iget-object v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mIndexThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 372
    return-void
.end method

.method public getInterrupt()Z
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->bInterrupt:Z

    return v0
.end method

.method public getNumberOfFilesInQueue()I
    .locals 2

    .prologue
    .line 90
    const/4 v0, 0x0

    .line 92
    .local v0, "numberoffiles":I
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v0

    .line 96
    :cond_0
    return v0
.end method

.method public run()V
    .locals 7

    .prologue
    .line 285
    :cond_0
    :goto_0
    iget-boolean v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mKeepRunning:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v4}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 287
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v4}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;

    .line 288
    .local v2, "fileDetails":Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;
    if-eqz v2, :cond_3

    .line 289
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 290
    .local v1, "executor":Ljava/util/concurrent/ThreadPoolExecutor;
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v4}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->is2003ParserThreadRunning()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 292
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutorWithOneThread:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 293
    const/4 v4, 0x1

    iput v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->activeExecutor:I

    .line 298
    :goto_1
    iget-object v4, v2, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;->mFileToIndex:Ljava/io/File;

    iget-object v5, v2, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;->mSetKeyValue:Ljava/util/HashMap;

    iget-object v6, v2, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    invoke-direct {p0, v4, v5, v6, v1}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->indexDirectory(Ljava/io/File;Ljava/util/HashMap;Lcom/samsung/index/IFileIndexedNotifier;Ljava/util/concurrent/ThreadPoolExecutor;)Z

    move-result v3

    .line 303
    .local v3, "success":Z
    if-nez v3, :cond_0

    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mAsyncIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v4}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->getStopParserThread()Z

    move-result v4

    if-nez v4, :cond_0

    .line 304
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v4, v2}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 312
    .end local v1    # "executor":Ljava/util/concurrent/ThreadPoolExecutor;
    .end local v2    # "fileDetails":Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;
    .end local v3    # "success":Z
    :catch_0
    move-exception v0

    .line 313
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string/jumbo v4, "IndexParserThreadsManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "InterruptedException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 295
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .restart local v1    # "executor":Ljava/util/concurrent/ThreadPoolExecutor;
    .restart local v2    # "fileDetails":Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 296
    const/4 v4, 0x2

    iput v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->activeExecutor:I

    goto :goto_1

    .line 310
    .end local v1    # "executor":Ljava/util/concurrent/ThreadPoolExecutor;
    :cond_3
    const-wide/16 v4, 0x1

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 316
    .end local v2    # "fileDetails":Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;
    :cond_4
    return-void
.end method

.method public setInterrupt(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 151
    iput-boolean p1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->bInterrupt:Z

    .line 152
    return-void
.end method

.method public shutDownIndexing(Lcom/samsung/index/IFileIndexedNotifier;)V
    .locals 4
    .param p1, "mFilenotifier"    # Lcom/samsung/index/IFileIndexedNotifier;

    .prologue
    const/4 v3, 0x0

    .line 165
    iget v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->activeExecutor:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 166
    const-string/jumbo v1, "IndexParserThreadsManager"

    const-string/jumbo v2, "[SHUTDOWN] Indexing single thread executor"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    invoke-direct {p0, p1}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->restartIndexWriter(Lcom/samsung/index/IFileIndexedNotifier;)V

    .line 169
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutorWithOneThread:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v1}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    .line 170
    iput-boolean v3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->bInterrupt:Z

    .line 171
    invoke-direct {p0}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->initSingleThreadExecutor()V

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I

    move-result v1

    if-nez v1, :cond_2

    .line 178
    const-string/jumbo v1, "IndexParserThreadsManager"

    const-string/jumbo v2, "[SHUTDOWN] Indexing two thread executor"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-direct {p0, p1}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->restartIndexWriter(Lcom/samsung/index/IFileIndexedNotifier;)V

    .line 181
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v1}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    .line 182
    iput-boolean v3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->bInterrupt:Z

    .line 183
    invoke-direct {p0}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->initDoubleThreadExecutor()V

    goto :goto_0

    .line 186
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v1}, Ljava/util/concurrent/ThreadPoolExecutor;->isShutdown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 187
    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 190
    iget-object v1, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCount()I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 194
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string/jumbo v1, "IndexParserThreadsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "InterruptedException : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startIndexing(Ljava/io/File;Lcom/samsung/index/IFileIndexedNotifier;Ljava/util/HashMap;)Z
    .locals 8
    .param p1, "dataDir"    # Ljava/io/File;
    .param p2, "filenotifier"    # Lcom/samsung/index/IFileIndexedNotifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lcom/samsung/index/IFileIndexedNotifier;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p3, "keyvalueset":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 106
    const/4 v2, 0x1

    .line 109
    .local v2, "retVal":Z
    new-instance v1, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;

    invoke-direct {v1}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;-><init>()V

    .line 110
    .local v1, "fileDetails":Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;
    iput-object p1, v1, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;->mFileToIndex:Ljava/io/File;

    .line 111
    iput-object p2, v1, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;->mFilenotifier:Lcom/samsung/index/IFileIndexedNotifier;

    .line 112
    iput-object p3, v1, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager$IndexDetails;->mSetKeyValue:Ljava/util/HashMap;

    .line 114
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-nez v3, :cond_0

    .line 115
    invoke-static {v6, v6}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->createInstance(Landroid/content/Context;Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->indexUtils:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    .line 119
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 120
    .local v4, "startTime":J
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mAsyncIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    iput-wide v4, v3, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->mStartTime:J

    .line 125
    :try_start_0
    iget-object v3, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mFilesToIndex:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3, v1}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :goto_0
    return v2

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string/jumbo v3, "IndexParserThreadsManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "InterruptedException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public switchIndexToTwoThread()V
    .locals 5

    .prologue
    .line 139
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutorWithOneThread:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v4}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    move-result-object v2

    .line 140
    .local v2, "remainingList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Runnable;>;"
    invoke-direct {p0}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->initSingleThreadExecutor()V

    .line 142
    const/4 v4, 0x2

    iput v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->activeExecutor:I

    .line 143
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 144
    .local v1, "listSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 145
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/index/indexservice/threads/IndexParserThread;

    .line 146
    .local v3, "thread":Lcom/samsung/index/indexservice/threads/IndexParserThread;
    iget-object v4, p0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    .end local v3    # "thread":Lcom/samsung/index/indexservice/threads/IndexParserThread;
    :cond_0
    return-void
.end method
