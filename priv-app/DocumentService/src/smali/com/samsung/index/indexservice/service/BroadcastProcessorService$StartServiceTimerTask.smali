.class Lcom/samsung/index/indexservice/service/BroadcastProcessorService$StartServiceTimerTask;
.super Ljava/util/TimerTask;
.source "BroadcastProcessorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/index/indexservice/service/BroadcastProcessorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StartServiceTimerTask"
.end annotation


# instance fields
.field private currContext:Landroid/content/Context;

.field private intentObj:Landroid/content/Intent;


# direct methods
.method constructor <init>(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 400
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 396
    iput-object v0, p0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService$StartServiceTimerTask;->intentObj:Landroid/content/Intent;

    .line 397
    iput-object v0, p0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService$StartServiceTimerTask;->currContext:Landroid/content/Context;

    .line 401
    iput-object p1, p0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService$StartServiceTimerTask;->intentObj:Landroid/content/Intent;

    .line 402
    iput-object p2, p0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService$StartServiceTimerTask;->currContext:Landroid/content/Context;

    .line 403
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService$StartServiceTimerTask;->cancel()Z

    .line 408
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService$StartServiceTimerTask;->intentObj:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService$StartServiceTimerTask;->currContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService$StartServiceTimerTask;->currContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService$StartServiceTimerTask;->intentObj:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 412
    :cond_0
    return-void
.end method
