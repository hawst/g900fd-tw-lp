.class public abstract enum Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
.super Ljava/lang/Enum;
.source "DocumentServiceState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/index/indexservice/service/DocumentServiceState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4409
    name = "EProcessState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field public static final enum INDEX_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field public static final enum NONE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field public static final enum PRE_SERVICE_STOP:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field public static final enum PROCESSING_INDEXING:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field public static final enum PROCESSING_INDEXING_THUMBNAIL_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field public static final enum PROCESSING_THUMBNAIL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field public static final enum SERVICE_NOT_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field public static final enum SERVICE_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field public static final enum SERVICE_STOPPED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field public static final enum START_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field public static final enum STOPPING_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field public static final enum SYSTEM_CRITICAL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field public static final enum THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

.field private static currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 205
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$1;

    const-string/jumbo v1, "SERVICE_NOT_STARTED"

    invoke-direct {v0, v1, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_NOT_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 215
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$2;

    const-string/jumbo v1, "START_SERVICE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->START_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 227
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$3;

    const-string/jumbo v1, "SERVICE_STARTED"

    invoke-direct {v0, v1, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 253
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$4;

    const-string/jumbo v1, "PROCESSING_THUMBNAIL"

    invoke-direct {v0, v1, v6}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_THUMBNAIL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 288
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$5;

    const-string/jumbo v1, "PROCESSING_INDEXING"

    invoke-direct {v0, v1, v7}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$5;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_INDEXING:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 318
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$6;

    const-string/jumbo v1, "PROCESSING_INDEXING_THUMBNAIL_IN_PARALLEL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$6;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_INDEXING_THUMBNAIL_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 346
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$7;

    const-string/jumbo v1, "THUMBNAIL_COMPLETED_IN_PARALLEL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$7;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 371
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$8;

    const-string/jumbo v1, "INDEX_COMPLETED_IN_PARALLEL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$8;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->INDEX_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 466
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$9;

    const-string/jumbo v1, "STOPPING_SERVICE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$9;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->STOPPING_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 492
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$10;

    const-string/jumbo v1, "PRE_SERVICE_STOP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$10;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PRE_SERVICE_STOP:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 532
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$11;

    const-string/jumbo v1, "SERVICE_STOPPED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$11;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_STOPPED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 542
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$12;

    const-string/jumbo v1, "SYSTEM_CRITICAL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$12;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SYSTEM_CRITICAL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 553
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$13;

    const-string/jumbo v1, "NONE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$13;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->NONE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 200
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_NOT_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->START_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_THUMBNAIL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_INDEXING:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_INDEXING_THUMBNAIL_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->THUMBNAIL_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->INDEX_COMPLETED_IN_PARALLEL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->STOPPING_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PRE_SERVICE_STOP:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_STOPPED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SYSTEM_CRITICAL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->NONE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->$VALUES:[Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 580
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_NOT_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 200
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/samsung/index/indexservice/service/DocumentServiceState$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/samsung/index/indexservice/service/DocumentServiceState$1;

    .prologue
    .line 200
    invoke-direct {p0, p1, p2}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$100()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .locals 1

    .prologue
    .line 200
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .prologue
    .line 200
    sput-object p0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    return-object p0
.end method

.method static synthetic access$200()V
    .locals 0

    .prologue
    .line 200
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->processThumbnailOrIndex()V

    return-void
.end method

.method static synthetic access$300()V
    .locals 0

    .prologue
    .line 200
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->resetFlags()V

    return-void
.end method

.method public static getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .locals 1

    .prologue
    .line 583
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    return-object v0
.end method

.method private static processThumbnailOrIndex()V
    .locals 1

    .prologue
    .line 561
    sget-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    if-eqz v0, :cond_0

    .line 562
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    .line 563
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_THUMBNAIL:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 572
    :goto_0
    return-void

    .line 565
    :cond_0
    sget-boolean v0, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_INDEXING:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->bIsPowerConnected:Z

    if-eqz v0, :cond_1

    .line 567
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->PROCESSING_INDEXING:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    goto :goto_0

    .line 569
    :cond_1
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->STOPPING_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    goto :goto_0
.end method

.method private static resetFlags()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 575
    sput-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bStopDocService:Z

    .line 576
    sput-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsPendingEventAvailable:Z

    .line 577
    sput-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bIsThumbnailPendingEventAvailable:Z

    .line 578
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 200
    const-class v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .locals 1

    .prologue
    .line 200
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->$VALUES:[Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    invoke-virtual {v0}, [Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    return-object v0
.end method


# virtual methods
.method abstract nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
.end method
