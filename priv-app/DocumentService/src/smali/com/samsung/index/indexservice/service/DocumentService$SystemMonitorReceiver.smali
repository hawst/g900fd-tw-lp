.class Lcom/samsung/index/indexservice/service/DocumentService$SystemMonitorReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DocumentService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/index/indexservice/service/DocumentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SystemMonitorReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/index/indexservice/service/DocumentService;


# direct methods
.method private constructor <init>(Lcom/samsung/index/indexservice/service/DocumentService;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/samsung/index/indexservice/service/DocumentService$SystemMonitorReceiver;->this$0:Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/index/indexservice/service/DocumentService;Lcom/samsung/index/indexservice/service/DocumentService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/index/indexservice/service/DocumentService;
    .param p2, "x1"    # Lcom/samsung/index/indexservice/service/DocumentService$1;

    .prologue
    .line 213
    invoke-direct {p0, p1}, Lcom/samsung/index/indexservice/service/DocumentService$SystemMonitorReceiver;-><init>(Lcom/samsung/index/indexservice/service/DocumentService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 217
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 219
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "DocumentService SystemMonitorReceiver "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string/jumbo v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226
    :cond_2
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService$SystemMonitorReceiver;->this$0:Lcom/samsung/index/indexservice/service/DocumentService;

    # invokes: Lcom/samsung/index/indexservice/service/DocumentService;->setFlagsToStopService()V
    invoke-static {v1}, Lcom/samsung/index/indexservice/service/DocumentService;->access$200(Lcom/samsung/index/indexservice/service/DocumentService;)V

    goto :goto_0
.end method
