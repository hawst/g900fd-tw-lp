.class public Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
.super Ljava/lang/Object;
.source "DocumentServiceUtils.java"


# static fields
.field private static final ANDROID_INSTALL_PATH:Ljava/lang/String; = "Android/data"

.field public static final EXTERNAL_SD_CARD_PATH:I = 0x2

.field public static final FILES_URI_STRING:Ljava/lang/String;

.field public static final INTERNAL_STORAGE_PATH:I = 0x1

.field public static final KEY_PARENT:Ljava/lang/String; = "parent"

.field public static final KEY_STORAGE:Ljava/lang/String; = "storage"

.field public static final MEDIA_FILES_URI:Landroid/net/Uri;

.field private static final MTP_TEMP_PATH:Ljava/lang/String; = "mtptemp"

.field public static final PRIVATE_STORAGE_PATH:I = 0x3

.field private static final TAG:Ljava/lang/String;

.field public static final VIDEO_URI_STRING:Ljava/lang/String;

.field public static batchObj:Ljava/lang/Object;

.field public static batchObjForSet:Ljava/lang/Object;

.field private static sSingleTon:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;


# instance fields
.field private currentFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final formats:[Ljava/lang/String;

.field private indexThreadCount:I

.field private indexTotalTime:J

.field private is2003FileTypesStarted:Z

.field private isOtherFileTypesStarted:Z

.field private final lockStopThumbnailFlag:Ljava/lang/Object;

.field private mContext:Landroid/content/Context;

.field private mIndexMgr:Lcom/samsung/index/ContentIndex;

.field private mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

.field private mIsThumbnailCreationinProgress:Z

.field private mMediaDbSelectionString:Ljava/lang/StringBuilder;

.field private mStopThumbnail:Z

.field private mTotalThumbfileCount:I

.field public mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

.field private otherFilesCount:[I

.field private remainingIndexCount:I

.field private remainingThumbCount:I

.field private thumbnailTotalTime:J

.field private totalIndexCount:I

.field private totalOtherFileSize:J

.field private totalPDFFileCount:I

.field private totalPDFFileSize:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    .line 47
    const-string/jumbo v0, "content://media/external/file"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->MEDIA_FILES_URI:Landroid/net/Uri;

    .line 50
    const-string/jumbo v0, "content://media/external/file"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->FILES_URI_STRING:Ljava/lang/String;

    .line 53
    const-string/jumbo v0, "content://media/external/video/media"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->VIDEO_URI_STRING:Ljava/lang/String;

    .line 70
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->sSingleTon:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    .line 92
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObj:Ljava/lang/Object;

    .line 93
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->batchObjForSet:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/samsung/index/indexservice/service/DocumentService;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "indexObj"    # Lcom/samsung/index/indexservice/service/DocumentService;

    .prologue
    const-wide/16 v4, 0x0

    const/16 v1, 0xb

    const/4 v3, 0x2

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mMediaDbSelectionString:Ljava/lang/StringBuilder;

    .line 64
    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mContext:Landroid/content/Context;

    .line 67
    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    .line 85
    iput v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->indexThreadCount:I

    .line 98
    iput v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mTotalThumbfileCount:I

    .line 102
    iput-boolean v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIsThumbnailCreationinProgress:Z

    .line 107
    iput-boolean v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mStopThumbnail:Z

    .line 112
    iput-wide v4, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->indexTotalTime:J

    .line 117
    iput-wide v4, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->thumbnailTotalTime:J

    .line 145
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    .line 1004
    new-array v0, v1, [Ljava/lang/String;

    const-string/jumbo v1, ".txt"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string/jumbo v2, ".doc"

    aput-object v2, v0, v1

    const-string/jumbo v1, ".xls"

    aput-object v1, v0, v3

    const/4 v1, 0x3

    const-string/jumbo v2, ".ppt"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, ".docx"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, ".xlsx"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, ".pptx"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, ".pdf"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, ".csv"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, ".asc"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, ".rtf"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->formats:[Ljava/lang/String;

    .line 1406
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->lockStopThumbnailFlag:Ljava/lang/Object;

    .line 157
    iput-object p1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mContext:Landroid/content/Context;

    .line 158
    iput-object p2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    .line 159
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-virtual {v0}, Lcom/samsung/index/indexservice/service/DocumentService;->getServiceDataBase()Lcom/samsung/index/DocumentDatabaseManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    .line 160
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-virtual {v0}, Lcom/samsung/index/indexservice/service/DocumentService;->getServiceIndexManager()Lcom/samsung/index/ContentIndex;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    .line 161
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->currentFiles:Ljava/util/ArrayList;

    .line 162
    return-void
.end method

.method public static declared-synchronized createInstance(Landroid/content/Context;Lcom/samsung/index/indexservice/service/DocumentService;)Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "indexObj"    # Lcom/samsung/index/indexservice/service/DocumentService;

    .prologue
    .line 175
    const-class v1, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->sSingleTon:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    if-nez v0, :cond_0

    .line 176
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 177
    new-instance v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    invoke-direct {v0, p0, p1}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;-><init>(Landroid/content/Context;Lcom/samsung/index/indexservice/service/DocumentService;)V

    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->sSingleTon:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;

    .line 179
    :cond_0
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->sSingleTon:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 175
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private deleteAll()V
    .locals 4

    .prologue
    .line 767
    :try_start_0
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    const-string/jumbo v2, "storage"

    const-string/jumbo v3, "sdCard"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/index/ContentIndex;->deleteIndex(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    const-string/jumbo v2, "storage"

    const-string/jumbo v3, "extSdCard"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/index/ContentIndex;->deleteIndex(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    const-string/jumbo v2, "storage"

    const-string/jumbo v3, "private"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/index/ContentIndex;->deleteIndex(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    const-string/jumbo v1, "storage"

    const-string/jumbo v2, "sdCard"

    invoke-direct {p0, v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->deleteThumbnailFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 776
    const-string/jumbo v1, "storage"

    const-string/jumbo v2, "extSdCard"

    invoke-direct {p0, v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->deleteThumbnailFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 778
    const-string/jumbo v1, "storage"

    const-string/jumbo v2, "private"

    invoke-direct {p0, v1, v2}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->deleteThumbnailFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 780
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    invoke-virtual {v1}, Lcom/samsung/index/DocumentDatabaseManager;->deleteAll()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 799
    :cond_0
    :goto_0
    return-void

    .line 781
    :catch_0
    move-exception v0

    .line 784
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "IOException on deleting index : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    instance-of v1, v0, Ljava/io/FileNotFoundException;

    if-nez v1, :cond_1

    instance-of v1, v0, Lorg/apache/lucene/index/CorruptIndexException;

    if-eqz v1, :cond_0

    .line 787
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->resetAll()V

    goto :goto_0

    .line 789
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 792
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "NumberFormatException on deleting index : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->resetAll()V

    goto :goto_0

    .line 796
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_2
    move-exception v0

    .line 797
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ERROR: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private deleteThumbnailFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1368
    const/4 v7, 0x0

    .line 1369
    .local v7, "sucess":Z
    const-string/jumbo v9, "filepath"

    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 1374
    iget-object v9, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mContext:Landroid/content/Context;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 1375
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 1377
    .local v3, "filesLst":[Ljava/io/File;
    if-nez v3, :cond_0

    move v8, v7

    .line 1391
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "filesLst":[Ljava/io/File;
    .end local v7    # "sucess":Z
    .local v8, "sucess":I
    :goto_0
    return v8

    .line 1380
    .end local v8    # "sucess":I
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "filesLst":[Ljava/io/File;
    .restart local v7    # "sucess":Z
    :cond_0
    move-object v0, v3

    .local v0, "arr$":[Ljava/io/File;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v6, :cond_3

    aget-object v1, v0, v4

    .line 1381
    .local v1, "currentFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1382
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v7

    .line 1380
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1386
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "currentFile":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "filesLst":[Ljava/io/File;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    :cond_2
    if-eqz p2, :cond_3

    .line 1387
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1388
    .local v5, "jpegfiletoDelete":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v7

    .end local v5    # "jpegfiletoDelete":Ljava/io/File;
    :cond_3
    move v8, v7

    .line 1391
    .restart local v8    # "sucess":I
    goto :goto_0
.end method

.method private getMediaDBQuery()Ljava/lang/String;
    .locals 12

    .prologue
    .line 574
    iget-object v10, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mMediaDbSelectionString:Ljava/lang/StringBuilder;

    if-nez v10, :cond_1

    .line 576
    const-string/jumbo v8, "_data LIKE ?"

    .line 577
    .local v8, "STORAGE_PATH_INCLUDER":Ljava/lang/String;
    const-string/jumbo v5, "_data LIKE \'"

    .line 578
    .local v5, "LIKE_QUERY_WILDCARD_HELPER_BEGIN":Ljava/lang/String;
    const-string/jumbo v4, "%"

    .line 579
    .local v4, "LIKE_QUERY_WILDCARD_HELPER":Ljava/lang/String;
    const-string/jumbo v3, "\'"

    .line 580
    .local v3, "LIKE_QUERY_HELPER_END":Ljava/lang/String;
    const-string/jumbo v7, " OR "

    .line 581
    .local v7, "QUERY_HELPER_OR":Ljava/lang/String;
    const-string/jumbo v6, " AND "

    .line 582
    .local v6, "QUERY_HELPER_AND":Ljava/lang/String;
    const-string/jumbo v0, "("

    .line 583
    .local v0, "BEGIN_BRACES":Ljava/lang/String;
    const-string/jumbo v1, ")"

    .line 585
    .local v1, "END_BRACES":Ljava/lang/String;
    const-string/jumbo v2, "_data NOT LIKE \'%/.%\'"

    .line 587
    .local v2, "HIDDEN_FOLDER_REMOVAL_QUERY":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    const/16 v11, 0x400

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v10, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mMediaDbSelectionString:Ljava/lang/StringBuilder;

    .line 589
    iget-object v10, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mMediaDbSelectionString:Ljava/lang/StringBuilder;

    const-string/jumbo v11, "_data LIKE ?"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 590
    iget-object v10, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mMediaDbSelectionString:Ljava/lang/StringBuilder;

    const-string/jumbo v11, " AND "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 593
    iget-object v10, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mMediaDbSelectionString:Ljava/lang/StringBuilder;

    const-string/jumbo v11, "_data NOT LIKE \'%/.%\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 594
    iget-object v10, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mMediaDbSelectionString:Ljava/lang/StringBuilder;

    const-string/jumbo v11, " AND "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 596
    iget-object v10, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mMediaDbSelectionString:Ljava/lang/StringBuilder;

    const-string/jumbo v11, "("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 598
    sget-object v10, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 600
    .local v9, "allowedListIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    if-eqz v9, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 601
    iget-object v10, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mMediaDbSelectionString:Ljava/lang/StringBuilder;

    const-string/jumbo v11, "_data LIKE \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 607
    iget-object v10, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mMediaDbSelectionString:Ljava/lang/StringBuilder;

    const-string/jumbo v11, " OR "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "_data LIKE \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 614
    :cond_0
    iget-object v10, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mMediaDbSelectionString:Ljava/lang/StringBuilder;

    const-string/jumbo v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 620
    .end local v0    # "BEGIN_BRACES":Ljava/lang/String;
    .end local v1    # "END_BRACES":Ljava/lang/String;
    .end local v2    # "HIDDEN_FOLDER_REMOVAL_QUERY":Ljava/lang/String;
    .end local v3    # "LIKE_QUERY_HELPER_END":Ljava/lang/String;
    .end local v4    # "LIKE_QUERY_WILDCARD_HELPER":Ljava/lang/String;
    .end local v5    # "LIKE_QUERY_WILDCARD_HELPER_BEGIN":Ljava/lang/String;
    .end local v6    # "QUERY_HELPER_AND":Ljava/lang/String;
    .end local v7    # "QUERY_HELPER_OR":Ljava/lang/String;
    .end local v8    # "STORAGE_PATH_INCLUDER":Ljava/lang/String;
    .end local v9    # "allowedListIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1
    iget-object v10, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mMediaDbSelectionString:Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    return-object v10
.end method

.method public static isInvalidPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "pathToFile"    # Ljava/lang/String;

    .prologue
    .line 631
    if-eqz p0, :cond_0

    const-string/jumbo v1, "mtptemp"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Android/data"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 637
    .local v0, "isInvalidPath":Z
    :goto_0
    return v0

    .line 631
    .end local v0    # "isInvalidPath":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private markForDelete(Ljava/lang/String;I)V
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "deleteStatus"    # I

    .prologue
    .line 802
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    invoke-virtual {v2, p1, p2}, Lcom/samsung/index/DocumentDatabaseManager;->markForDelete(Ljava/lang/String;I)V

    .line 803
    const/4 v2, 0x2

    if-ne p2, v2, :cond_0

    .line 804
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    invoke-virtual {v2, p1}, Lcom/samsung/index/DocumentDatabaseManager;->getThumbnailPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 806
    .local v1, "pathtoDelete":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "[DELETE] deleteOnlyThumbnail "

    invoke-static {v2, v3}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    const-string/jumbo v2, "filepath"

    invoke-direct {p0, v2, v1}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->deleteThumbnailFile(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 813
    .end local v1    # "pathtoDelete":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 808
    .restart local v1    # "pathtoDelete":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 809
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "IOException on deleting Thumbnail : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private syncFolderPathWithMediaDB(Ljava/lang/String;I)V
    .locals 26
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "which"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1187
    const/16 v16, 0x0

    .line 1188
    .local v16, "docDBCursor":Landroid/database/Cursor;
    const/16 v20, 0x0

    .line 1189
    .local v20, "mediaDBCursor":Landroid/database/Cursor;
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "_data"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "date_modified"

    aput-object v3, v4, v2

    .line 1190
    .local v4, "PROJ_STRING":[Ljava/lang/String;
    const-string/jumbo v9, "_data COLLATE NOCASE ASC"

    .line 1191
    .local v9, "ORDERBY":Ljava/lang/String;
    const-string/jumbo v10, "%"

    .line 1193
    .local v10, "WILD_CARD":Ljava/lang/String;
    const-string/jumbo v8, "/"

    .line 1195
    .local v8, "FOLDER_SEPERATOR":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 1362
    :cond_0
    :goto_0
    return-void

    .line 1198
    :cond_1
    const/4 v15, 0x0

    .line 1199
    .local v15, "docCursorReachedLast":Z
    const/16 v19, 0x0

    .line 1201
    .local v19, "mediaCursorReachedLast":Z
    const/4 v12, 0x0

    .line 1202
    .local v12, "currentDocData":Ljava/lang/String;
    const/4 v13, 0x0

    .line 1203
    .local v13, "currentMediaDbData":Ljava/lang/String;
    const/4 v14, 0x0

    .line 1204
    .local v14, "deleteStatus":I
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    .line 1206
    .local v6, "storagePath":[Ljava/lang/String;
    const-string/jumbo v2, "/"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1207
    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "%"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 1215
    :goto_1
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1216
    new-instance v2, Ljava/lang/InterruptedException;

    invoke-direct {v2}, Ljava/lang/InterruptedException;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1353
    :catchall_0
    move-exception v2

    if-eqz v16, :cond_2

    .line 1354
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 1355
    const/16 v16, 0x0

    .line 1357
    :cond_2
    if-eqz v20, :cond_3

    .line 1358
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 1359
    const/16 v20, 0x0

    :cond_3
    throw v2

    .line 1209
    :cond_4
    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "%"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    goto :goto_1

    .line 1217
    :cond_5
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    const/4 v3, 0x0

    aget-object v3, v6, v3

    invoke-virtual {v2, v3}, Lcom/samsung/index/DocumentDatabaseManager;->getDocumentsEntryWithTime(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1220
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->MEDIA_FILES_URI:Landroid/net/Uri;

    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getMediaDBQuery()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v7, "_data COLLATE NOCASE ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v20

    .line 1223
    if-nez v20, :cond_7

    .line 1353
    if-eqz v16, :cond_6

    .line 1354
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 1355
    const/16 v16, 0x0

    .line 1357
    :cond_6
    if-eqz v20, :cond_0

    .line 1358
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 1359
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 1226
    :cond_7
    :try_start_2
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_b

    .line 1227
    if-eqz v16, :cond_9

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1228
    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mediaDBCursor is null or no entry  so deleting all the files:"

    invoke-static {v2, v3}, Lcom/samsung/index/indexservice/Slog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232
    :cond_8
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->markForDelete(Ljava/lang/String;I)V

    .line 1234
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    if-nez v2, :cond_8

    .line 1353
    :cond_9
    if-eqz v16, :cond_a

    .line 1354
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 1355
    const/16 v16, 0x0

    .line 1357
    :cond_a
    if-eqz v20, :cond_0

    .line 1358
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 1359
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 1240
    :cond_b
    const/16 v17, 0x1

    .line 1241
    .local v17, "iterateIndexCursor":Z
    const/16 v18, 0x1

    .line 1243
    .local v18, "iterateMediaCursor":Z
    if-eqz v16, :cond_c

    :try_start_3
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_13

    .line 1244
    :cond_c
    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "indexDBCursor reached last, breaking out"

    invoke-static {v2, v3}, Lcom/samsung/index/indexservice/Slog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1247
    const/4 v15, 0x1

    .line 1335
    :cond_d
    :goto_2
    if-nez v15, :cond_f

    .line 1340
    :cond_e
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->markForDelete(Ljava/lang/String;I)V

    .line 1342
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_e

    .line 1344
    :cond_f
    if-nez v19, :cond_11

    .line 1347
    :cond_10
    const/4 v2, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->processDocument(Ljava/lang/String;II)Z

    .line 1349
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v2

    if-nez v2, :cond_10

    .line 1353
    :cond_11
    if-eqz v16, :cond_12

    .line 1354
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 1355
    const/16 v16, 0x0

    .line 1357
    :cond_12
    if-eqz v20, :cond_0

    .line 1358
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 1359
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 1252
    :cond_13
    :try_start_4
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1253
    new-instance v2, Ljava/lang/InterruptedException;

    invoke-direct {v2}, Ljava/lang/InterruptedException;-><init>()V

    throw v2

    .line 1255
    :cond_14
    const/16 v17, 0x1

    .line 1256
    const/16 v18, 0x1

    .line 1258
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1259
    const/4 v2, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1260
    const-string/jumbo v2, "delete_status"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 1265
    if-eqz v12, :cond_15

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1267
    :cond_15
    const/4 v15, 0x1

    .line 1268
    if-eqz v13, :cond_d

    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_d

    .line 1270
    const/16 v19, 0x0

    goto :goto_2

    .line 1274
    :cond_16
    if-eqz v13, :cond_17

    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 1276
    :cond_17
    const/4 v15, 0x1

    .line 1277
    goto/16 :goto_2

    .line 1280
    :cond_18
    invoke-virtual {v12, v13}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v11

    .line 1283
    .local v11, "comparisonValue":I
    if-nez v11, :cond_1a

    .line 1289
    const/4 v2, 0x1

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 1290
    .local v22, "modifiedTimeFromIndexDB":J
    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 1291
    .local v24, "modifiedTimeFromMediaDB":J
    cmp-long v2, v24, v22

    if-lez v2, :cond_19

    .line 1294
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->markForDelete(Ljava/lang/String;I)V

    .line 1296
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v13, v1, v14}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->processDocument(Ljava/lang/String;II)Z

    .line 1319
    .end local v22    # "modifiedTimeFromIndexDB":J
    .end local v24    # "modifiedTimeFromMediaDB":J
    :cond_19
    :goto_3
    if-eqz v17, :cond_1c

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1c

    .line 1320
    const/4 v15, 0x1

    .line 1321
    if-eqz v18, :cond_d

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_d

    .line 1322
    const/16 v19, 0x1

    goto/16 :goto_2

    .line 1298
    :cond_1a
    if-gez v11, :cond_1b

    .line 1303
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->markForDelete(Ljava/lang/String;I)V

    .line 1305
    const/16 v18, 0x0

    goto :goto_3

    .line 1309
    :cond_1b
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v13, v1, v14}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->processDocument(Ljava/lang/String;II)Z

    .line 1310
    const/16 v17, 0x0

    goto :goto_3

    .line 1326
    :cond_1c
    if-eqz v18, :cond_13

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v2

    if-nez v2, :cond_13

    .line 1327
    const/16 v19, 0x1

    .line 1328
    goto/16 :goto_2
.end method

.method private syncWithMediaDatabase(II)Z
    .locals 34
    .param p1, "path"    # I
    .param p2, "which"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 338
    const/4 v15, 0x0

    .line 344
    .local v15, "bIsDocumentProcessed":Z
    const/16 v22, 0x0

    .line 345
    .local v22, "docServiceDBCursor":Landroid/database/Cursor;
    const/16 v28, 0x0

    .line 346
    .local v28, "mediaDBCursor":Landroid/database/Cursor;
    const/4 v4, 0x2

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "_data"

    aput-object v5, v6, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "date_modified"

    aput-object v5, v6, v4

    .line 347
    .local v6, "PROJ_STRING":[Ljava/lang/String;
    const-string/jumbo v12, "_data COLLATE NOCASE ASC"

    .line 348
    .local v12, "ORDERBY":Ljava/lang/String;
    const-string/jumbo v14, "%"

    .line 349
    .local v14, "WILD_CARD":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/index/indexservice/Const;->EXT_SD_PATH:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 350
    .local v10, "EXTERNAL_PATH":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/index/indexservice/Const;->ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 352
    .local v11, "INTERNAL_PATH":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/index/indexservice/Const;->PRIVATE_PATH:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 354
    .local v13, "PRIVATE_PATH":Ljava/lang/String;
    const/16 v21, 0x0

    .line 355
    .local v21, "docCursorReachedLast":Z
    const/16 v27, 0x0

    .line 357
    .local v27, "mediaCursorReachedLast":Z
    const/16 v18, 0x0

    .line 358
    .local v18, "currentDocData":Ljava/lang/String;
    const/16 v19, 0x0

    .line 359
    .local v19, "currentMediaData":Ljava/lang/String;
    const/16 v20, 0x0

    .line 360
    .local v20, "deleteStatus":I
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    .line 362
    .local v8, "storagePath":[Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 374
    sget-object v4, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "syncWithMediaDatabase: Wrong path "

    invoke-static {v4, v5}, Lcom/samsung/index/indexservice/Slog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v16, v15

    .line 563
    .end local v15    # "bIsDocumentProcessed":Z
    .local v16, "bIsDocumentProcessed":I
    :goto_0
    return v16

    .line 364
    .end local v16    # "bIsDocumentProcessed":I
    .restart local v15    # "bIsDocumentProcessed":Z
    :pswitch_0
    const/4 v4, 0x0

    aput-object v11, v8, v4

    .line 379
    :goto_1
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 380
    new-instance v4, Ljava/lang/InterruptedException;

    invoke-direct {v4}, Ljava/lang/InterruptedException;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 554
    :catchall_0
    move-exception v4

    if-eqz v22, :cond_0

    .line 555
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 556
    const/16 v22, 0x0

    .line 558
    :cond_0
    if-eqz v28, :cond_1

    .line 559
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->close()V

    .line 560
    const/16 v28, 0x0

    :cond_1
    throw v4

    .line 368
    :pswitch_1
    const/4 v4, 0x0

    aput-object v13, v8, v4

    goto :goto_1

    .line 371
    :pswitch_2
    const/4 v4, 0x0

    aput-object v10, v8, v4

    goto :goto_1

    .line 382
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v4, :cond_5

    .line 554
    if-eqz v22, :cond_3

    .line 555
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 556
    const/16 v22, 0x0

    .line 558
    :cond_3
    if-eqz v28, :cond_4

    .line 559
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->close()V

    .line 560
    const/16 v28, 0x0

    :cond_4
    move/from16 v16, v15

    .restart local v16    # "bIsDocumentProcessed":I
    goto :goto_0

    .line 384
    .end local v16    # "bIsDocumentProcessed":I
    :cond_5
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    const/4 v5, 0x0

    aget-object v5, v8, v5

    invoke-virtual {v4, v5}, Lcom/samsung/index/DocumentDatabaseManager;->getDocumentsEntryWithTime(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 388
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->MEDIA_FILES_URI:Landroid/net/Uri;

    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getMediaDBQuery()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v9, "_data COLLATE NOCASE ASC"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v28

    .line 392
    if-nez v28, :cond_8

    .line 554
    if-eqz v22, :cond_6

    .line 555
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 556
    const/16 v22, 0x0

    .line 558
    :cond_6
    if-eqz v28, :cond_7

    .line 559
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->close()V

    .line 560
    const/16 v28, 0x0

    :cond_7
    move/from16 v16, v15

    .restart local v16    # "bIsDocumentProcessed":I
    goto :goto_0

    .line 394
    .end local v16    # "bIsDocumentProcessed":I
    :cond_8
    :try_start_3
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_d

    .line 395
    if-eqz v22, :cond_a

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 399
    :cond_9
    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->markForDelete(Ljava/lang/String;I)V

    .line 401
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v4

    if-nez v4, :cond_9

    .line 554
    :cond_a
    if-eqz v22, :cond_b

    .line 555
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 556
    const/16 v22, 0x0

    .line 558
    :cond_b
    if-eqz v28, :cond_c

    .line 559
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->close()V

    .line 560
    const/16 v28, 0x0

    :cond_c
    move/from16 v16, v15

    .restart local v16    # "bIsDocumentProcessed":I
    goto/16 :goto_0

    .line 407
    .end local v16    # "bIsDocumentProcessed":I
    :cond_d
    const/16 v25, 0x1

    .line 408
    .local v25, "iterateIndexCursor":Z
    const/16 v26, 0x1

    .line 410
    .local v26, "iterateMediaCursor":Z
    if-eqz v22, :cond_e

    :try_start_4
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_17

    .line 412
    :cond_e
    sget-object v4, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "indexDBCursor reached last, breaking out"

    invoke-static {v4, v5}, Lcom/samsung/index/indexservice/Slog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const/16 v21, 0x1

    .line 534
    :cond_f
    :goto_2
    if-nez v21, :cond_11

    .line 539
    :cond_10
    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->markForDelete(Ljava/lang/String;I)V

    .line 540
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_10

    .line 542
    :cond_11
    if-nez v27, :cond_14

    .line 545
    :cond_12
    const/4 v4, 0x0

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v4, v1, v5}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->processDocument(Ljava/lang/String;II)Z

    move-result v24

    .line 547
    .local v24, "isProcessed":Z
    if-eqz v24, :cond_13

    .line 548
    const/4 v15, 0x1

    .line 550
    :cond_13
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v4

    if-nez v4, :cond_12

    .line 554
    .end local v24    # "isProcessed":Z
    :cond_14
    if-eqz v22, :cond_15

    .line 555
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 556
    const/16 v22, 0x0

    .line 558
    :cond_15
    if-eqz v28, :cond_16

    .line 559
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->close()V

    .line 560
    const/16 v28, 0x0

    :cond_16
    move/from16 v16, v15

    .line 563
    .restart local v16    # "bIsDocumentProcessed":I
    goto/16 :goto_0

    .line 420
    .end local v16    # "bIsDocumentProcessed":I
    :cond_17
    const/16 v24, 0x0

    .line 421
    .restart local v24    # "isProcessed":Z
    :try_start_5
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v4

    if-eqz v4, :cond_18

    .line 422
    new-instance v4, Ljava/lang/InterruptedException;

    invoke-direct {v4}, Ljava/lang/InterruptedException;-><init>()V

    throw v4

    .line 424
    :cond_18
    const/16 v25, 0x1

    .line 425
    const/16 v26, 0x1

    .line 427
    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 428
    const/4 v4, 0x0

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 429
    const-string/jumbo v4, "delete_status"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 439
    if-eqz v18, :cond_19

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 440
    :cond_19
    const/16 v21, 0x1

    .line 441
    if-eqz v19, :cond_f

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_f

    .line 443
    const/16 v27, 0x0

    goto/16 :goto_2

    .line 447
    :cond_1a
    if-eqz v19, :cond_1b

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 449
    :cond_1b
    const/16 v21, 0x1

    .line 450
    goto/16 :goto_2

    .line 453
    :cond_1c
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v17

    .line 456
    .local v17, "comparisonValue":I
    if-nez v17, :cond_24

    .line 462
    const/4 v4, 0x1

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    .line 464
    .local v30, "modifiedTimeFromIndexDB":J
    const/4 v4, 0x1

    move-object/from16 v0, v28

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v32

    .line 466
    .local v32, "modifiedTimeFromMediaDB":J
    const-string/jumbo v4, "index_status"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 469
    .local v23, "indexStatus":I
    const-string/jumbo v4, "thumbnail_status"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v29

    .line 473
    .local v29, "thumbnailStatus":I
    cmp-long v4, v32, v30

    if-lez v4, :cond_21

    .line 476
    const/16 v4, 0x11

    move/from16 v0, p2

    if-ne v0, v4, :cond_20

    .line 477
    const/4 v4, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v4}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->markForDelete(Ljava/lang/String;I)V

    .line 487
    :cond_1d
    :goto_3
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, p2

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->processDocument(Ljava/lang/String;II)Z

    move-result v24

    .line 508
    .end local v23    # "indexStatus":I
    .end local v29    # "thumbnailStatus":I
    .end local v30    # "modifiedTimeFromIndexDB":J
    .end local v32    # "modifiedTimeFromMediaDB":J
    :cond_1e
    :goto_4
    if-eqz v24, :cond_1f

    .line 509
    const/4 v15, 0x1

    .line 518
    :cond_1f
    if-eqz v25, :cond_26

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_26

    .line 519
    const/16 v21, 0x1

    .line 520
    if-eqz v26, :cond_f

    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_f

    .line 521
    const/16 v27, 0x1

    goto/16 :goto_2

    .line 479
    .restart local v23    # "indexStatus":I
    .restart local v29    # "thumbnailStatus":I
    .restart local v30    # "modifiedTimeFromIndexDB":J
    .restart local v32    # "modifiedTimeFromMediaDB":J
    :cond_20
    sget-boolean v4, Lcom/samsung/index/indexservice/service/DocumentService;->ENABLE_THUMBNAIL:Z

    if-nez v4, :cond_1d

    .line 482
    const/16 v20, 0x2

    .line 483
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Lcom/samsung/index/DocumentDatabaseManager;->markForDelete(Ljava/lang/String;I)V

    goto :goto_3

    .line 489
    :cond_21
    const/16 v4, 0x11

    move/from16 v0, p2

    if-eq v0, v4, :cond_22

    const/4 v4, -0x1

    move/from16 v0, v23

    if-eq v0, v4, :cond_23

    :cond_22
    const/16 v4, 0x11

    move/from16 v0, p2

    if-ne v0, v4, :cond_1e

    const/4 v4, -0x1

    move/from16 v0, v29

    if-ne v0, v4, :cond_1e

    .line 491
    :cond_23
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, p2

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->processDocument(Ljava/lang/String;II)Z

    move-result v24

    goto :goto_4

    .line 494
    .end local v23    # "indexStatus":I
    .end local v29    # "thumbnailStatus":I
    .end local v30    # "modifiedTimeFromIndexDB":J
    .end local v32    # "modifiedTimeFromMediaDB":J
    :cond_24
    if-gez v17, :cond_25

    .line 499
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v4}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->markForDelete(Ljava/lang/String;I)V

    .line 500
    const/16 v26, 0x0

    goto :goto_4

    .line 504
    :cond_25
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, p2

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->processDocument(Ljava/lang/String;II)Z

    move-result v24

    .line 505
    const/16 v25, 0x0

    goto :goto_4

    .line 525
    :cond_26
    if-eqz v26, :cond_17

    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v4

    if-nez v4, :cond_17

    .line 526
    const/16 v27, 0x1

    .line 527
    goto/16 :goto_2

    .line 362
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public declared-synchronized addIndexFileCount()I
    .locals 1

    .prologue
    .line 931
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalIndexCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalIndexCount:I

    .line 932
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingIndexCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingIndexCount:I

    .line 933
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalIndexCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 931
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized addTotalFilesCount(Ljava/lang/String;I)V
    .locals 5
    .param p1, "extension"    # Ljava/lang/String;
    .param p2, "size"    # I

    .prologue
    .line 1016
    monitor-enter p0

    :try_start_0
    const-string/jumbo v0, ".txt"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1017
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v3, 0x0

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    aput v4, v0, v1

    .line 1043
    :goto_0
    iget-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalOtherFileSize:J

    int-to-long v2, p2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalOtherFileSize:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1044
    monitor-exit p0

    return-void

    .line 1018
    :cond_0
    :try_start_1
    const-string/jumbo v0, ".doc"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1019
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v3, 0x1

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    aput v4, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1016
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1020
    :cond_1
    :try_start_2
    const-string/jumbo v0, ".xls"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1021
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v3, 0x2

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    aput v4, v0, v1

    goto :goto_0

    .line 1022
    :cond_2
    const-string/jumbo v0, ".ppt"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1023
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v3, 0x3

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    aput v4, v0, v1

    goto :goto_0

    .line 1024
    :cond_3
    const-string/jumbo v0, ".docx"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1025
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v3, 0x4

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    aput v4, v0, v1

    goto :goto_0

    .line 1026
    :cond_4
    const-string/jumbo v0, ".xlsx"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1027
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v3, 0x5

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    aput v4, v0, v1

    goto/16 :goto_0

    .line 1028
    :cond_5
    const-string/jumbo v0, ".pptx"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1029
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v3, 0x6

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    aput v4, v0, v1

    goto/16 :goto_0

    .line 1030
    :cond_6
    const-string/jumbo v0, ".pdf"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1031
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalPDFFileCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalPDFFileCount:I

    .line 1032
    iget-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalPDFFileSize:J

    int-to-long v2, p2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalPDFFileSize:J

    .line 1033
    const/4 p2, 0x0

    goto/16 :goto_0

    .line 1034
    :cond_7
    const-string/jumbo v0, ".csv"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1035
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/4 v3, 0x7

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    aput v4, v0, v1

    goto/16 :goto_0

    .line 1036
    :cond_8
    const-string/jumbo v0, ".asc"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1037
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/16 v3, 0x8

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    aput v4, v0, v1

    goto/16 :goto_0

    .line 1038
    :cond_9
    const-string/jumbo v0, ".rtf"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1039
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/16 v3, 0x9

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    aput v4, v0, v1

    goto/16 :goto_0

    .line 1041
    :cond_a
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    const/16 v3, 0xa

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    aput v4, v0, v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public declared-synchronized addTotalPDFCount(I)V
    .locals 4
    .param p1, "size"    # I

    .prologue
    .line 979
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalPDFFileCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalPDFFileCount:I

    .line 980
    iget-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalPDFFileSize:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalPDFFileSize:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 981
    monitor-exit p0

    return-void

    .line 979
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized addTotalThumbFileCount()I
    .locals 1

    .prologue
    .line 955
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mTotalThumbfileCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mTotalThumbfileCount:I

    .line 956
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingThumbCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingThumbCount:I

    .line 957
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mTotalThumbfileCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 955
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized clearSingleton()V
    .locals 1

    .prologue
    .line 203
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->sSingleTon:Lcom/samsung/index/indexservice/service/DocumentServiceUtils;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    monitor-exit p0

    return-void

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized commitAfterDelete()V
    .locals 1

    .prologue
    .line 816
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    if-eqz v0, :cond_0

    .line 817
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v0}, Lcom/samsung/index/ContentIndex;->commitAfterDelete()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 819
    :cond_0
    monitor-exit p0

    return-void

    .line 816
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized decIndexFileCount()I
    .locals 1

    .prologue
    .line 938
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingIndexCount:I

    if-gtz v0, :cond_0

    .line 939
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingIndexCount:I

    .line 940
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingIndexCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 943
    :goto_0
    monitor-exit p0

    return v0

    .line 942
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingIndexCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingIndexCount:I

    .line 943
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingIndexCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 938
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized decTotalThumbFileCount()I
    .locals 1

    .prologue
    .line 962
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingThumbCount:I

    if-gtz v0, :cond_0

    .line 963
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingThumbCount:I

    .line 964
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingThumbCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 967
    :goto_0
    monitor-exit p0

    return v0

    .line 966
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingThumbCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingThumbCount:I

    .line 967
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingThumbCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 962
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public deleteFileEntry(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "thumbnailPath"    # Ljava/lang/String;

    .prologue
    .line 828
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "[DELETE] deleteFileEntry "

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    :try_start_0
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    const-string/jumbo v2, "filepath"

    invoke-virtual {v1, v2, p1}, Lcom/samsung/index/ContentIndex;->deleteIndex(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    if-nez p2, :cond_0

    .line 834
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    invoke-virtual {v1, p1}, Lcom/samsung/index/DocumentDatabaseManager;->getThumbnailPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 836
    :cond_0
    const-string/jumbo v1, "filepath"

    invoke-direct {p0, v1, p2}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->deleteThumbnailFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 838
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    invoke-virtual {v1, p1}, Lcom/samsung/index/DocumentDatabaseManager;->deleteFileEntry(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 858
    :cond_1
    :goto_0
    return-void

    .line 839
    :catch_0
    move-exception v0

    .line 842
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "IOException on deleting index : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 844
    instance-of v1, v0, Ljava/io/FileNotFoundException;

    if-nez v1, :cond_2

    instance-of v1, v0, Lorg/apache/lucene/index/CorruptIndexException;

    if-eqz v1, :cond_1

    .line 846
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->resetAll()V

    goto :goto_0

    .line 848
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 851
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "NumberFormatException on deleting index : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->resetAll()V

    goto :goto_0

    .line 855
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_2
    move-exception v0

    .line 856
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized deleteIndexedFileIfStopped()V
    .locals 4

    .prologue
    .line 211
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->getCurrentFiles()Ljava/util/ArrayList;

    move-result-object v1

    .line 212
    .local v1, "current":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_1

    .line 213
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 214
    .local v0, "curFile":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->deleteOnlyFileIndex(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 211
    .end local v0    # "curFile":Ljava/lang/String;
    .end local v1    # "current":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 217
    .restart local v1    # "current":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v3}, Lcom/samsung/index/ContentIndex;->commitAfterDelete()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 219
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public deleteOnlyFileIndex(Ljava/lang/String;)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 868
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "[DELETE] deleteOnlyFileIndex "

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    :try_start_0
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    const-string/jumbo v2, "filepath"

    invoke-virtual {v1, v2, p1}, Lcom/samsung/index/ContentIndex;->deleteIndex(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 875
    :goto_0
    return-void

    .line 871
    :catch_0
    move-exception v0

    .line 872
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception on Deleting a file form Lucene Doc: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized getCount()I
    .locals 1

    .prologue
    .line 1103
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->indexThreadCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getCurrentFiles()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1179
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->currentFiles:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDocServiceObj()Lcom/samsung/index/indexservice/service/DocumentService;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    return-object v0
.end method

.method public declared-synchronized getIndexTotalTime()J
    .locals 2

    .prologue
    .line 192
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->indexTotalTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getOtherFilesCount()[I
    .locals 1

    .prologue
    .line 1047
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getRemainingIndexFileCount()I
    .locals 1

    .prologue
    .line 951
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingIndexCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getRemainingThumbFileCount()I
    .locals 1

    .prologue
    .line 975
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingThumbCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getStopThumbnailFlag()Z
    .locals 2

    .prologue
    .line 1415
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->lockStopThumbnailFlag:Ljava/lang/Object;

    monitor-enter v1

    .line 1416
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mStopThumbnail:Z

    monitor-exit v1

    return v0

    .line 1417
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized getThumbTotalTime()J
    .locals 2

    .prologue
    .line 196
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->thumbnailTotalTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTotalIndexFileCount()I
    .locals 1

    .prologue
    .line 947
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalIndexCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTotalOtherFileSize()J
    .locals 2

    .prologue
    .line 1051
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalOtherFileSize:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTotalPDFCount()I
    .locals 1

    .prologue
    .line 997
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalPDFFileCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTotalPDFSize()J
    .locals 2

    .prologue
    .line 1001
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalPDFFileSize:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTotalThumbFileCount()I
    .locals 1

    .prologue
    .line 971
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mTotalThumbfileCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized is2003ParserThreadRunning()Z
    .locals 1

    .prologue
    .line 1117
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->is2003FileTypesStarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isOtherFileTypesRunning()Z
    .locals 1

    .prologue
    .line 1136
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->isOtherFileTypesStarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isThumbnailInProgress()Z
    .locals 1

    .prologue
    .line 1400
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIsThumbnailCreationinProgress:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized printProcessingFileDetails()V
    .locals 4

    .prologue
    .line 1055
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1056
    .local v1, "strOtherFilesCount":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1057
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    aget v2, v2, v0

    if-lez v2, :cond_0

    .line 1058
    const-string/jumbo v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1059
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->formats:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1060
    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1061
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1056
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1064
    :cond_1
    iget v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalIndexCount:I

    if-lez v2, :cond_2

    .line 1065
    const-string/jumbo v2, " Total Index count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1066
    iget v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalIndexCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1067
    const-string/jumbo v2, " Remaining: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1068
    iget v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingIndexCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1071
    :cond_2
    iget v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mTotalThumbfileCount:I

    if-lez v2, :cond_3

    .line 1072
    const-string/jumbo v2, " Total Thumbnails count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1073
    iget v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mTotalThumbfileCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1074
    const-string/jumbo v2, " Remaining: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1075
    iget v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingThumbCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1078
    :cond_3
    const-string/jumbo v2, " Total PDF: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1079
    iget v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalPDFFileCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1080
    const-string/jumbo v2, " PDF size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1081
    iget-wide v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalPDFFileSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 1082
    const-string/jumbo v2, " OtherFiles Size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1083
    iget-wide v2, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalOtherFileSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 1085
    sget-object v2, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1086
    monitor-exit p0

    return-void

    .line 1055
    .end local v0    # "i":I
    .end local v1    # "strOtherFilesCount":Ljava/lang/StringBuffer;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public processDocument(Ljava/lang/String;II)Z
    .locals 17
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "which"    # I
    .param p3, "delStatus"    # I

    .prologue
    .line 675
    const/4 v2, 0x0

    .line 676
    .local v2, "bIsDocumentProcessed":Z
    new-instance v11, Ljava/text/SimpleDateFormat;

    const-string/jumbo v13, "yyyyMMdd"

    invoke-direct {v11, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 679
    .local v11, "sdf":Ljava/text/SimpleDateFormat;
    invoke-static/range {p1 .. p1}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->isInvalidPath(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_0

    move v3, v2

    .line 744
    .end local v2    # "bIsDocumentProcessed":Z
    .local v3, "bIsDocumentProcessed":I
    :goto_0
    return v3

    .line 683
    .end local v3    # "bIsDocumentProcessed":I
    .restart local v2    # "bIsDocumentProcessed":Z
    :cond_0
    :try_start_0
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 686
    .local v7, "fileToIndex":Ljava/io/File;
    const-string/jumbo v12, "sdCard"

    .line 687
    .local v12, "storage":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 688
    .local v6, "filePathTemp":Ljava/lang/String;
    sget-object v13, Lcom/samsung/index/indexservice/Const;->EXT_SD_PATH:Ljava/lang/String;

    invoke-virtual {v6, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 689
    const-string/jumbo v12, "extSdCard"

    .line 694
    :cond_1
    :goto_1
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v13

    if-eqz v13, :cond_3

    move v3, v2

    .line 695
    .restart local v3    # "bIsDocumentProcessed":I
    goto :goto_0

    .line 691
    .end local v3    # "bIsDocumentProcessed":I
    :cond_2
    sget-object v13, Lcom/samsung/index/indexservice/Const;->PRIVATE_PATH:Ljava/lang/String;

    invoke-virtual {v6, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 692
    const-string/jumbo v12, "private"

    goto :goto_1

    .line 697
    :cond_3
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v13

    if-nez v13, :cond_4

    .line 698
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    .line 700
    .local v5, "fileName":Ljava/lang/String;
    const/16 v13, 0x11

    move/from16 v0, p2

    if-ne v0, v13, :cond_6

    .line 701
    invoke-static/range {p1 .. p1}, Lcom/samsung/index/Utils;->isThumbnailSupported(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    if-eqz v13, :cond_5

    .line 703
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/samsung/index/indexservice/service/DocumentService;->startThumbnailGenerator(Ljava/lang/String;)V

    .line 704
    const/4 v2, 0x1

    .end local v5    # "fileName":Ljava/lang/String;
    :cond_4
    :goto_2
    move v3, v2

    .line 741
    .restart local v3    # "bIsDocumentProcessed":I
    goto :goto_0

    .line 707
    .end local v3    # "bIsDocumentProcessed":I
    .restart local v5    # "fileName":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    const/4 v14, -0x2

    const/4 v15, 0x1

    const/16 v16, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v13, v0, v14, v15, v1}, Lcom/samsung/index/DocumentDatabaseManager;->updateEntryInDB(Ljava/lang/String;IZI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 742
    .end local v5    # "fileName":Ljava/lang/String;
    .end local v6    # "filePathTemp":Ljava/lang/String;
    .end local v7    # "fileToIndex":Ljava/io/File;
    .end local v12    # "storage":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 743
    .local v4, "e":Ljava/lang/Exception;
    sget-object v13, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Error on indexing start : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/index/indexservice/Slog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 744
    .restart local v3    # "bIsDocumentProcessed":I
    goto/16 :goto_0

    .line 711
    .end local v3    # "bIsDocumentProcessed":I
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v5    # "fileName":Ljava/lang/String;
    .restart local v6    # "filePathTemp":Ljava/lang/String;
    .restart local v7    # "fileToIndex":Ljava/io/File;
    .restart local v12    # "storage":Ljava/lang/String;
    :cond_6
    const/16 v13, 0x11

    move/from16 v0, p2

    if-eq v0, v13, :cond_4

    :try_start_1
    invoke-static {v5}, Lcom/samsung/index/Utils;->isSupported(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 713
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 714
    .local v8, "keyvalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v13, "storage"

    invoke-virtual {v8, v13, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 715
    const-string/jumbo v13, "parent"

    invoke-virtual {v7}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v13, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 717
    invoke-virtual {v7}, Ljava/io/File;->lastModified()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 719
    .local v10, "modifiedTime":Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/index/Utils;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 723
    .local v9, "mfileExtension":Ljava/lang/String;
    const-string/jumbo v13, "fileExtension"

    invoke-virtual {v8, v13, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 725
    const-string/jumbo v13, "modifiedTime"

    invoke-virtual {v8, v13, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 726
    const-string/jumbo v13, "delete_status"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p3

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v13, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 729
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v13

    if-eqz v13, :cond_7

    move v3, v2

    .line 730
    .restart local v3    # "bIsDocumentProcessed":I
    goto/16 :goto_0

    .line 733
    .end local v3    # "bIsDocumentProcessed":I
    :cond_7
    const/16 v13, 0x10

    move/from16 v0, p2

    if-ne v0, v13, :cond_4

    sget-boolean v13, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->bIsPowerConnected:Z

    if-eqz v13, :cond_4

    .line 734
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-virtual {v13, v7, v8, v14}, Lcom/samsung/index/ContentIndex;->startIndexing(Ljava/io/File;Ljava/util/HashMap;Lcom/samsung/index/IFileIndexedNotifier;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 736
    const/4 v2, 0x1

    goto/16 :goto_2
.end method

.method public declared-synchronized removeCurrentFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1170
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->currentFiles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1171
    monitor-exit p0

    return-void

    .line 1170
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public removeTimerHandlerMessage()V
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-virtual {v0}, Lcom/samsung/index/indexservice/service/DocumentService;->removeTimerHandlerMessage()V

    .line 323
    :cond_0
    return-void
.end method

.method resetAll()V
    .locals 1

    .prologue
    .line 750
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    if-eqz v0, :cond_0

    .line 751
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    invoke-virtual {v0}, Lcom/samsung/index/DocumentDatabaseManager;->deleteAll()V

    .line 754
    :cond_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    if-eqz v0, :cond_1

    .line 755
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v0}, Lcom/samsung/index/ContentIndex;->clearAllIndex()V

    .line 759
    :cond_1
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->processRestart(Landroid/content/Context;)V

    .line 760
    return-void
.end method

.method public declared-synchronized resetAllFileCounts()V
    .locals 2

    .prologue
    .line 987
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mTotalThumbfileCount:I

    .line 988
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalIndexCount:I

    .line 990
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalPDFFileCount:I

    .line 991
    const/16 v0, 0xb

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->otherFilesCount:[I

    .line 992
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalOtherFileSize:J

    .line 993
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->totalPDFFileSize:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 994
    monitor-exit p0

    return-void

    .line 987
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set2003FileStartedFlag(Z)V
    .locals 1
    .param p1, "is2003FileTypesStarted"    # Z

    .prologue
    .line 1113
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->is2003FileTypesStarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1114
    monitor-exit p0

    return-void

    .line 1113
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setCount(I)I
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 1147
    monitor-enter p0

    if-gez p1, :cond_0

    .line 1148
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->indexThreadCount:I

    .line 1152
    :goto_0
    iget v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->indexThreadCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 1150
    :cond_0
    :try_start_1
    iput p1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->indexThreadCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setCurrentFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1161
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->currentFiles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1162
    monitor-exit p0

    return-void

    .line 1161
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setIndexTotalTime(J)V
    .locals 3
    .param p1, "indexTotalTime"    # J

    .prologue
    .line 184
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->indexTotalTime:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->indexTotalTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    monitor-exit p0

    return-void

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setOtherFilesStarted(Z)V
    .locals 1
    .param p1, "isOtherFilesStarted"    # Z

    .prologue
    .line 1126
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->isOtherFileTypesStarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1127
    monitor-exit p0

    return-void

    .line 1126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setRemainingThumbnailFileCount(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 1094
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->remainingThumbCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1095
    monitor-exit p0

    return-void

    .line 1094
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setStopThumbnail(Z)V
    .locals 2
    .param p1, "stopThumbnail"    # Z

    .prologue
    .line 1409
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->lockStopThumbnailFlag:Ljava/lang/Object;

    monitor-enter v1

    .line 1410
    :try_start_0
    iput-boolean p1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mStopThumbnail:Z

    .line 1411
    monitor-exit v1

    .line 1412
    return-void

    .line 1411
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized setThumbTotalTime(J)V
    .locals 3
    .param p1, "thumbnailTotalTime"    # J

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->thumbnailTotalTime:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->thumbnailTotalTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    monitor-exit p0

    return-void

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setThumbnailCreationStatus(Z)V
    .locals 1
    .param p1, "isInProgress"    # Z

    .prologue
    .line 1396
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIsThumbnailCreationinProgress:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1397
    monitor-exit p0

    return-void

    .line 1396
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public startIndexinginParallel()V
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-virtual {v0}, Lcom/samsung/index/indexservice/service/DocumentService;->startIndexinginParallel()V

    .line 314
    :cond_0
    return-void
.end method

.method public switchIndexToTwoThread()V
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v0}, Lcom/samsung/index/ContentIndex;->switchIndexToTwoThread()V

    .line 661
    return-void
.end method

.method public syncDBForThumbnail()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1427
    const-string/jumbo v3, ""

    const-string/jumbo v4, "syncDBForThumbnail...."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1428
    const/4 v0, 0x0

    .line 1429
    .local v0, "indexDBCursor":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 1431
    .local v1, "isStarted":Z
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1432
    new-instance v2, Ljava/lang/InterruptedException;

    invoke-direct {v2}, Ljava/lang/InterruptedException;-><init>()V

    throw v2

    .line 1434
    :cond_0
    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    if-nez v3, :cond_1

    .line 1450
    :goto_0
    return v2

    .line 1436
    :cond_1
    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    invoke-virtual {v3}, Lcom/samsung/index/DocumentDatabaseManager;->getProcessedDocCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 1438
    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1440
    :cond_2
    const/4 v1, 0x1

    .line 1441
    iget-object v3, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/index/indexservice/service/DocumentService;->startThumbnailGenerator(Ljava/lang/String;)V

    .line 1443
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1446
    :cond_3
    if-eqz v0, :cond_4

    .line 1448
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_4
    move v2, v1

    .line 1450
    goto :goto_0
.end method

.method public declared-synchronized syncFilesWithFileSystem(ZI)Z
    .locals 11
    .param p1, "isFirstTime"    # Z
    .param p2, "which"    # I

    .prologue
    .line 229
    monitor-enter p0

    const/4 v0, 0x0

    .line 230
    .local v0, "bIsDocumentProcessed":Z
    if-eqz p1, :cond_7

    .line 231
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->deleteAll()V

    .line 233
    iget-object v8, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v8}, Lcom/samsung/index/ContentIndex;->commitAfterDelete()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .line 260
    .local v3, "isProcessed":Z
    const/4 v8, 0x1

    :try_start_1
    invoke-direct {p0, v8, p2}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->syncWithMediaDatabase(II)Z

    move-result v3

    .line 261
    if-eqz v3, :cond_1

    .line 262
    const/4 v0, 0x1

    .line 265
    :cond_1
    new-instance v5, Ljava/io/File;

    sget-object v8, Lcom/samsung/index/indexservice/Const;->PRIVATE_PATH:Ljava/lang/String;

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 266
    .local v5, "privateFolder":Ljava/io/File;
    const/4 v4, 0x0

    .line 268
    .local v4, "privateFileCnt":I
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 269
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    array-length v4, v8

    .line 271
    :cond_2
    if-lez v4, :cond_3

    .line 272
    const/4 v8, 0x3

    invoke-direct {p0, v8, p2}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->syncWithMediaDatabase(II)Z

    move-result v3

    .line 274
    if-eqz v3, :cond_3

    .line 275
    const/4 v0, 0x1

    .line 279
    :cond_3
    new-instance v6, Ljava/io/File;

    sget-object v8, Lcom/samsung/index/indexservice/Const;->EXT_SD_PATH:Ljava/lang/String;

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 280
    .local v6, "sd":Ljava/io/File;
    const/4 v7, 0x0

    .line 282
    .local v7, "sdFileCnt":I
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 283
    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    array-length v7, v8

    .line 285
    :cond_4
    if-lez v7, :cond_5

    .line 286
    const/4 v8, 0x2

    invoke-direct {p0, v8, p2}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->syncWithMediaDatabase(II)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    .line 288
    if-eqz v3, :cond_5

    .line 289
    const/4 v0, 0x1

    .line 299
    .end local v4    # "privateFileCnt":I
    .end local v5    # "privateFolder":Ljava/io/File;
    .end local v6    # "sd":Ljava/io/File;
    .end local v7    # "sdFileCnt":I
    :cond_5
    :goto_1
    const/16 v8, 0x11

    if-ne p2, v8, :cond_6

    :try_start_2
    iget-object v8, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    if-eqz v8, :cond_6

    if-eqz v0, :cond_6

    .line 301
    iget-object v8, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-virtual {v8}, Lcom/samsung/index/indexservice/service/DocumentService;->start2003FileFormatOnly()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    .line 303
    :cond_6
    monitor-exit p0

    return v0

    .line 235
    .end local v3    # "isProcessed":Z
    :cond_7
    :try_start_3
    iget-object v8, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexService:Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-virtual {v8}, Lcom/samsung/index/indexservice/service/DocumentService;->getIndexedDocCount()I

    move-result v2

    .line 236
    .local v2, "indexDocCount":I
    if-gtz v2, :cond_0

    .line 240
    iget-object v8, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    if-eqz v8, :cond_0

    .line 244
    iget-object v8, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ThumbnailUtils;->isThumbnailFolderExists(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 245
    if-eqz v2, :cond_0

    .line 247
    iget-object v8, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    invoke-virtual {v8}, Lcom/samsung/index/DocumentDatabaseManager;->handleIndexFolderDeletion()J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 229
    .end local v2    # "indexDocCount":I
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 250
    .restart local v2    # "indexDocCount":I
    :cond_8
    :try_start_4
    iget-object v8, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mdbMgr:Lcom/samsung/index/DocumentDatabaseManager;

    invoke-virtual {v8}, Lcom/samsung/index/DocumentDatabaseManager;->deleteAll()V

    .line 252
    iget-object v8, p0, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->mIndexMgr:Lcom/samsung/index/ContentIndex;

    invoke-virtual {v8}, Lcom/samsung/index/ContentIndex;->commitAfterDelete()V

    goto/16 :goto_0

    .line 292
    .end local v2    # "indexDocCount":I
    .restart local v3    # "isProcessed":Z
    :catch_0
    move-exception v1

    .line 296
    .local v1, "e":Ljava/lang/Exception;
    sget-object v8, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public updateFolderIndex(Ljava/lang/String;I)V
    .locals 4
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "which"    # I

    .prologue
    .line 648
    invoke-static {p1}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->isInvalidPath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 657
    :goto_0
    return-void

    .line 652
    :cond_0
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->syncFolderPathWithMediaDB(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 653
    :catch_0
    move-exception v0

    .line 654
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception in Sync with Media DB : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
