.class Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DocumentService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/index/indexservice/service/DocumentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UserSwitchReceiver"
.end annotation


# instance fields
.field private indexServiceInstance:Lcom/samsung/index/indexservice/service/DocumentService;


# direct methods
.method constructor <init>(Lcom/samsung/index/indexservice/service/DocumentService;)V
    .locals 2
    .param p1, "indexService"    # Lcom/samsung/index/indexservice/service/DocumentService;

    .prologue
    .line 242
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 240
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;->indexServiceInstance:Lcom/samsung/index/indexservice/service/DocumentService;

    .line 243
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.USER_SWITCHED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 244
    .local v0, "filter":Landroid/content/IntentFilter;
    invoke-virtual {p1}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 245
    iput-object p1, p0, Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;->indexServiceInstance:Lcom/samsung/index/indexservice/service/DocumentService;

    .line 246
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 260
    const-string/jumbo v2, "android.intent.action.USER_SWITCHED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 261
    const-string/jumbo v2, "android.intent.extra.user_handle"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 262
    .local v0, "currentUserId":I
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    .line 264
    .local v1, "originalUser":I
    if-eq v0, v1, :cond_0

    .line 265
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "DocumentService onReceive USER_SWITCHED in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    iget-object v2, p0, Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;->indexServiceInstance:Lcom/samsung/index/indexservice/service/DocumentService;

    # invokes: Lcom/samsung/index/indexservice/service/DocumentService;->setFlagsToStopService()V
    invoke-static {v2}, Lcom/samsung/index/indexservice/service/DocumentService;->access$200(Lcom/samsung/index/indexservice/service/DocumentService;)V

    .line 271
    .end local v0    # "currentUserId":I
    .end local v1    # "originalUser":I
    :cond_0
    return-void
.end method

.method unregisterSelf()V
    .locals 4

    .prologue
    .line 250
    :try_start_0
    iget-object v1, p0, Lcom/samsung/index/indexservice/service/DocumentService$UserSwitchReceiver;->indexServiceInstance:Lcom/samsung/index/indexservice/service/DocumentService;

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    :goto_0
    return-void

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
