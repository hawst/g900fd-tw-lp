.class public Lcom/samsung/index/indexservice/Const;
.super Ljava/lang/Object;
.source "Const.java"


# static fields
.field public static final DEVICE_POWER_CONNECTED:Ljava/lang/String; = "DevicePowerConnected"

.field public static final DRM_PROCTECTED_MSG:Ljava/lang/String; = "<##"

.field public static final EXT_SDCARD_INDEX_ACCOUNT_ID:Ljava/lang/String; = "extSdCard"

.field public static final EXT_SD_PATH:Ljava/lang/String;

.field public static final LINE_BREAK_CHARS:Ljava/lang/String; = "(\u000c|\r|\n|\t|\r\n|\u000b)"

.field public static final MAX_PATH_LENGTH:I = 0xfa

.field public static final MY_SHARED_PREF_NAME:Ljava/lang/String; = "doc_service_pref"

.field public static final NO:Ljava/lang/String; = "NO"

.field public static final PREF_KEY_INDEXING_FIRST_TIME:Ljava/lang/String; = "IsIndexingFirstTime"

.field public static final PRIVATE_INDEX_ACCOUNT_ID:Ljava/lang/String; = "private"

.field public static final PRIVATE_PATH:Ljava/lang/String;

.field public static final ROOT_PATH:Ljava/lang/String;

.field public static final SDCARD_INDEX_ACCOUNT_ID:Ljava/lang/String; = "sdCard"

.field public static final SEARCH_PATTERN:Ljava/lang/String; = "[\u000c|\r|\n|\t|\r\n|\u000b|\' \']"

.field public static final SPACE_CHAR:C = ' '

.field public static final STOP_INDEXING:Ljava/lang/String; = "STOP_INDEXING"

.field public static final STRING_EMPTY:Ljava/lang/String; = ""

.field public static final STRING_PATTERN_EXTRACT_ALPHABETS:Ljava/lang/String; = "[^\\[A-Z]"

.field public static final STRING_PATTERN_EXTRACT_DIGITS:Ljava/lang/String; = "[^\\d]"

.field public static final YES:Ljava/lang/String; = "YES"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 96
    const-string/jumbo v0, "/storage/extSdCard/"

    sput-object v0, Lcom/samsung/index/indexservice/Const;->EXT_SD_PATH:Ljava/lang/String;

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/index/indexservice/Const;->ROOT_PATH:Ljava/lang/String;

    .line 99
    const-string/jumbo v0, "/storage/Private"

    sput-object v0, Lcom/samsung/index/indexservice/Const;->PRIVATE_PATH:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static printLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "strMessage"    # Ljava/lang/String;

    .prologue
    .line 117
    :try_start_0
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ERROR: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
