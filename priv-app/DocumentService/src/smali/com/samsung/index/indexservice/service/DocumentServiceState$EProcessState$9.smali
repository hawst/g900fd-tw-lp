.class final enum Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$9;
.super Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
.source "DocumentServiceState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 466
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;-><init>(Ljava/lang/String;ILcom/samsung/index/indexservice/service/DocumentServiceState$1;)V

    return-void
.end method


# virtual methods
.method nextState(I)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 469
    packed-switch p1, :pswitch_data_0

    .line 478
    sget-boolean v0, Lcom/samsung/index/indexservice/service/DocumentServiceState;->bStopDocService:Z

    if-nez v0, :cond_0

    .line 479
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$9;->PRE_SERVICE_STOP:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    # setter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$102(Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    .line 485
    :goto_0
    # getter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$100()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v0

    return-object v0

    .line 474
    :pswitch_0
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$9;->SERVICE_STOPPED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    # setter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$102(Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    goto :goto_0

    .line 481
    :cond_0
    sget-object v0, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState$9;->STOPPING_SERVICE:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    # setter for: Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->currentState:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    invoke-static {v0}, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->access$102(Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;)Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    goto :goto_0

    .line 469
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
