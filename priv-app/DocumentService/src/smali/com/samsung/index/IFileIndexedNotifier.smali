.class public interface abstract Lcom/samsung/index/IFileIndexedNotifier;
.super Ljava/lang/Object;
.source "IFileIndexedNotifier.java"


# virtual methods
.method public abstract indexCompleted()V
.end method

.method public abstract indexCorrupted()V
.end method

.method public abstract indexedFile(Ljava/lang/String;I)V
.end method

.method public abstract thumbnailCompleted()V
.end method

.method public abstract thumbnailGeneratedFile(Ljava/lang/String;ILjava/lang/String;)V
.end method
