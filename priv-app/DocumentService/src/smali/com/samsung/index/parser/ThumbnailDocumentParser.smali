.class public Lcom/samsung/index/parser/ThumbnailDocumentParser;
.super Ljava/lang/Object;
.source "ThumbnailDocumentParser.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final DOC_PAGE_HEIGHT:I = 0x2d0

.field private static final DOC_PAGE_WIDTH:I = 0x220

.field private static final EXCEL_SHEET_RESOLUTION:I = 0x250

.field public static final EXTRACT_RESTRICTED_CELL_DATA:Z = true

.field public static final MAX_CHAR_COUNT:I = 0x100000

.field private static MAX_DOC_SIZE:J = 0x0L

.field public static MAX_EXCEL_TABLE_WIDTH:F = 0.0f

.field public static final MAX_PERMISSIBLE_EXCEL_COLUMN:I = 0x10

.field public static final MAX_PERMISSIBLE_EXCEL_ROW:I = 0x14

.field public static final MAX_PERMISSIBLE_XLSX_XML_SIZE:I = 0x2d

.field private static MAX_XLS_SIZE:J = 0x0L

.field private static final PPT_SLIDE_HEIGHT:I = 0x1f0

.field private static final PPT_SLIDE_WIDTH:I = 0x250

.field private static final TAG:Ljava/lang/String; = "ThumbnailDocumentParser"

.field private static mContext:Landroid/content/Context;


# instance fields
.field private folderName:Ljava/io/File;

.field private thumbnailFilePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 66
    const/high16 v0, 0x43c80000    # 400.0f

    sput v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->MAX_EXCEL_TABLE_WIDTH:F

    .line 427
    const-wide/32 v0, 0xa00000

    sput-wide v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->MAX_XLS_SIZE:J

    .line 588
    const-wide/32 v0, 0x1900000

    sput-wide v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->MAX_DOC_SIZE:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private createAndStoreBitmapForExcel(Ljava/io/File;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 9
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "elementCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    const/16 v8, 0x250

    .line 225
    const/16 v2, 0x12c

    .line 226
    .local v2, "bitmapWidth":I
    const/16 v1, 0x145

    .line 228
    .local v1, "bitmapHeight":I
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v1, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 231
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 234
    .local v3, "canvas":Landroid/graphics/Canvas;
    const/4 v7, -0x1

    invoke-virtual {v3, v7}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 237
    new-instance v4, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;

    invoke-direct {v4, p3}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 239
    .local v4, "drawObj":Lcom/samsung/thumbnail/customview/draw/CanvasDraw;
    invoke-virtual {v4, v3, p2}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->handleDraw(Landroid/graphics/Canvas;Landroid/content/Context;)Z

    move-result v5

    .line 240
    .local v5, "isSuccess":Z
    if-nez v5, :cond_0

    .line 241
    new-instance v7, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v8, "Some of the features of this document are not supported"

    invoke-direct {v7, v8}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 245
    :cond_0
    move-object v6, v0

    .line 247
    .local v6, "thumbBitmap":Landroid/graphics/Bitmap;
    const/4 v7, 0x1

    invoke-static {v0, v8, v8, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 250
    invoke-direct {p0, p2, v6, p1}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->saveBitMap(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/io/File;)V

    .line 251
    return-void
.end method

.method private createAndStorePDFBitmap(Ljava/io/File;Landroid/content/Context;)V
    .locals 18
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 259
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v12

    .line 260
    .local v12, "filePath":Ljava/lang/String;
    sget-object v6, Lcom/samsung/index/parser/ThumbnailDocumentParser;->mContext:Landroid/content/Context;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, ""

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v10, 0x1

    invoke-virtual {v6, v7, v10}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    .line 264
    new-instance v4, Lorg/icepdf/core/pobjects/Document;

    move-object/from16 v0, p2

    invoke-direct {v4, v0}, Lorg/icepdf/core/pobjects/Document;-><init>(Landroid/content/Context;)V

    .line 266
    .local v4, "document":Lorg/icepdf/core/pobjects/Document;
    :try_start_0
    invoke-virtual {v4, v12}, Lorg/icepdf/core/pobjects/Document;->setFile(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/icepdf/core/exceptions/InvalidHeaderException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/icepdf/core/exceptions/PDFException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/icepdf/core/exceptions/PDFSecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    .line 283
    const/high16 v9, 0x3f800000    # 1.0f

    .line 284
    .local v9, "scale":F
    const/4 v8, 0x0

    .line 286
    .local v8, "rotation":F
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/Document;->getNumberOfPages()I

    move-result v14

    .line 287
    .local v14, "numPages":I
    const/4 v13, 0x0

    .line 289
    .local v13, "image":Landroid/graphics/Bitmap;
    if-gtz v14, :cond_1

    .line 292
    if-eqz v4, :cond_0

    .line 293
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/Document;->dispose()V

    .line 294
    const/4 v4, 0x0

    .line 296
    :cond_0
    new-instance v6, Lorg/apache/poi/EncryptedDocumentException;

    const-string/jumbo v7, "Encrypted Document Exception"

    invoke-direct {v6, v7}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 267
    .end local v8    # "rotation":F
    .end local v9    # "scale":F
    .end local v13    # "image":Landroid/graphics/Bitmap;
    .end local v14    # "numPages":I
    :catch_0
    move-exception v11

    .line 268
    .local v11, "ex":Lorg/icepdf/core/exceptions/InvalidHeaderException;
    const-string/jumbo v6, "ThumbnailDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Error parsing PDF document "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v11}, Lorg/icepdf/core/exceptions/InvalidHeaderException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    throw v11

    .line 270
    .end local v11    # "ex":Lorg/icepdf/core/exceptions/InvalidHeaderException;
    :catch_1
    move-exception v11

    .line 271
    .local v11, "ex":Lorg/icepdf/core/exceptions/PDFException;
    const-string/jumbo v6, "ThumbnailDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Error parsing PDF document "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v11}, Lorg/icepdf/core/exceptions/PDFException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    throw v11

    .line 273
    .end local v11    # "ex":Lorg/icepdf/core/exceptions/PDFException;
    :catch_2
    move-exception v11

    .line 274
    .local v11, "ex":Lorg/icepdf/core/exceptions/PDFSecurityException;
    throw v11

    .line 275
    .end local v11    # "ex":Lorg/icepdf/core/exceptions/PDFSecurityException;
    :catch_3
    move-exception v11

    .line 276
    .local v11, "ex":Ljava/io/FileNotFoundException;
    const-string/jumbo v6, "ThumbnailDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Error file not found "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v11}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    throw v11

    .line 278
    .end local v11    # "ex":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v11

    .line 279
    .local v11, "ex":Ljava/io/IOException;
    const-string/jumbo v6, "ThumbnailDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception handling PDF document"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    throw v11

    .line 300
    .end local v11    # "ex":Ljava/io/IOException;
    .restart local v8    # "rotation":F
    .restart local v9    # "scale":F
    .restart local v13    # "image":Landroid/graphics/Bitmap;
    .restart local v14    # "numPages":I
    :cond_1
    const/4 v14, 0x1

    .line 302
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v14, :cond_2

    .line 303
    const/4 v6, 0x1

    const/4 v7, 0x2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-virtual/range {v4 .. v10}, Lorg/icepdf/core/pobjects/Document;->getPageImage(IIIFFLjava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 305
    move-object v15, v13

    .line 307
    .local v15, "thumbBitmap":Landroid/graphics/Bitmap;
    const/16 v6, 0x220

    const/16 v7, 0x2d0

    const/4 v10, 0x1

    invoke-static {v13, v6, v7, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 310
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v15, v2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->saveBitMap(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/io/File;)V

    .line 302
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 312
    .end local v15    # "thumbBitmap":Landroid/graphics/Bitmap;
    :cond_2
    if-eqz v4, :cond_3

    .line 313
    invoke-virtual {v4}, Lorg/icepdf/core/pobjects/Document;->dispose()V

    .line 314
    const/4 v4, 0x0

    .line 317
    :cond_3
    return-void
.end method

.method private createAndStorePDFBitmapUsingL(Ljava/io/File;Landroid/content/Context;)V
    .locals 18
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 322
    const/4 v10, 0x0

    .line 323
    .local v10, "pdf":Landroid/os/ParcelFileDescriptor;
    const/4 v11, 0x0

    .line 324
    .local v11, "renderer":Landroid/graphics/pdf/PdfRenderer;
    const/4 v7, 0x0

    .line 325
    .local v7, "page":Landroid/graphics/pdf/PdfRenderer$Page;
    const/4 v6, 0x0

    .line 327
    .local v6, "numPages":I
    const/high16 v15, 0x10000000

    :try_start_0
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v10

    .line 329
    if-nez v10, :cond_2

    .line 330
    const-string/jumbo v15, "ThumbnailDocumentParser"

    const-string/jumbo v16, "ParcelFileDescriptor NULL "

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 380
    if-eqz v11, :cond_0

    .line 381
    invoke-virtual {v11}, Landroid/graphics/pdf/PdfRenderer;->close()V

    .line 383
    :cond_0
    if-eqz v10, :cond_1

    .line 384
    invoke-virtual {v10}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 388
    :cond_1
    :goto_0
    return-void

    .line 334
    :cond_2
    :try_start_1
    new-instance v12, Landroid/graphics/pdf/PdfRenderer;

    invoke-direct {v12, v10}, Landroid/graphics/pdf/PdfRenderer;-><init>(Landroid/os/ParcelFileDescriptor;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 335
    .end local v11    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    .local v12, "renderer":Landroid/graphics/pdf/PdfRenderer;
    if-nez v12, :cond_5

    .line 336
    :try_start_2
    const-string/jumbo v15, "ThumbnailDocumentParser"

    const-string/jumbo v16, "PdfRenderer NULL "

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 380
    if-eqz v12, :cond_3

    .line 381
    invoke-virtual {v12}, Landroid/graphics/pdf/PdfRenderer;->close()V

    .line 383
    :cond_3
    if-eqz v10, :cond_4

    .line 384
    invoke-virtual {v10}, Landroid/os/ParcelFileDescriptor;->close()V

    :cond_4
    move-object v11, v12

    .end local v12    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    .restart local v11    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    goto :goto_0

    .line 339
    .end local v11    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    .restart local v12    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    :cond_5
    :try_start_3
    invoke-virtual {v12}, Landroid/graphics/pdf/PdfRenderer;->getPageCount()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v6

    .line 341
    if-gtz v6, :cond_8

    .line 380
    if-eqz v12, :cond_6

    .line 381
    invoke-virtual {v12}, Landroid/graphics/pdf/PdfRenderer;->close()V

    .line 383
    :cond_6
    if-eqz v10, :cond_7

    .line 384
    invoke-virtual {v10}, Landroid/os/ParcelFileDescriptor;->close()V

    :cond_7
    move-object v11, v12

    .end local v12    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    .restart local v11    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    goto :goto_0

    .line 346
    .end local v11    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    .restart local v12    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    :cond_8
    const/4 v6, 0x1

    .line 348
    const/4 v9, 0x0

    .line 349
    .local v9, "pageWidth":I
    const/4 v8, 0x0

    .line 350
    .local v8, "pageHeight":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v6, :cond_9

    .line 351
    :try_start_4
    invoke-virtual {v12, v4}, Landroid/graphics/pdf/PdfRenderer;->openPage(I)Landroid/graphics/pdf/PdfRenderer$Page;

    move-result-object v7

    .line 352
    invoke-virtual {v7}, Landroid/graphics/pdf/PdfRenderer$Page;->getWidth()I

    move-result v9

    .line 353
    invoke-virtual {v7}, Landroid/graphics/pdf/PdfRenderer$Page;->getHeight()I

    move-result v8

    .line 356
    mul-int/lit8 v15, v9, 0x60

    div-int/lit8 v9, v15, 0x48

    .line 357
    mul-int/lit8 v15, v8, 0x60

    div-int/lit8 v8, v15, 0x48

    .line 358
    sget-object v15, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v8, v15}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 360
    .local v5, "image":Landroid/graphics/Bitmap;
    new-instance v14, Landroid/graphics/Canvas;

    invoke-direct {v14, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 361
    .local v14, "whiteCanvas":Landroid/graphics/Canvas;
    const/4 v15, -0x1

    invoke-virtual {v14, v15}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 362
    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v7, v5, v15, v0, v1}, Landroid/graphics/pdf/PdfRenderer$Page;->render(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Matrix;I)V

    .line 368
    invoke-virtual {v7}, Landroid/graphics/pdf/PdfRenderer$Page;->close()V

    .line 370
    const/16 v15, 0x220

    const/16 v16, 0x2d0

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v5, v15, v0, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 373
    .local v13, "thumbBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v13, v2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->saveBitMap(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/io/File;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 350
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 380
    .end local v5    # "image":Landroid/graphics/Bitmap;
    .end local v13    # "thumbBitmap":Landroid/graphics/Bitmap;
    .end local v14    # "whiteCanvas":Landroid/graphics/Canvas;
    :cond_9
    if-eqz v12, :cond_a

    .line 381
    invoke-virtual {v12}, Landroid/graphics/pdf/PdfRenderer;->close()V

    .line 383
    :cond_a
    if-eqz v10, :cond_b

    .line 384
    invoke-virtual {v10}, Landroid/os/ParcelFileDescriptor;->close()V

    :cond_b
    move-object v11, v12

    .line 388
    .end local v12    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    .restart local v11    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    goto/16 :goto_0

    .line 377
    .end local v4    # "i":I
    .end local v8    # "pageHeight":I
    .end local v9    # "pageWidth":I
    :catch_0
    move-exception v3

    .line 378
    .local v3, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 380
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v15

    :goto_3
    if-eqz v11, :cond_c

    .line 381
    invoke-virtual {v11}, Landroid/graphics/pdf/PdfRenderer;->close()V

    .line 383
    :cond_c
    if-eqz v10, :cond_d

    .line 384
    invoke-virtual {v10}, Landroid/os/ParcelFileDescriptor;->close()V

    :cond_d
    throw v15

    .line 380
    .end local v11    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    .restart local v12    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    :catchall_1
    move-exception v15

    move-object v11, v12

    .end local v12    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    .restart local v11    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    goto :goto_3

    .line 377
    .end local v11    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    .restart local v12    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    :catch_1
    move-exception v3

    move-object v11, v12

    .end local v12    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    .restart local v11    # "renderer":Landroid/graphics/pdf/PdfRenderer;
    goto :goto_2
.end method

.method private deleteTempFiles(Ljava/io/File;)Z
    .locals 8
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 673
    const/4 v5, 0x0

    .line 674
    .local v5, "success":Z
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    move v6, v5

    .line 689
    .end local v5    # "success":Z
    .local v6, "success":I
    :goto_0
    return v6

    .line 677
    .end local v6    # "success":I
    .restart local v5    # "success":Z
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 679
    .local v2, "filesLst":[Ljava/io/File;
    if-nez v2, :cond_2

    .line 680
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v5

    move v6, v5

    .line 681
    .restart local v6    # "success":I
    goto :goto_0

    .line 683
    .end local v6    # "success":I
    :cond_2
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_4

    aget-object v1, v0, v3

    .line 684
    .local v1, "currentFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 685
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v5

    .line 683
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 688
    .end local v1    # "currentFile":Ljava/io/File;
    :cond_4
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v5

    move v6, v5

    .line 689
    .restart local v6    # "success":I
    goto :goto_0
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private makeACopy(Ljava/io/File;Landroid/content/Context;Ljava/io/InputStream;)V
    .locals 8
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 849
    const/4 v2, 0x0

    .line 866
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-static {p3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 867
    .local v0, "btm":Landroid/graphics/Bitmap;
    const/16 v4, 0x250

    const/16 v5, 0x1f0

    const/4 v6, 0x1

    invoke-static {v0, v4, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 869
    .local v3, "scaled":Landroid/graphics/Bitmap;
    invoke-direct {p0, p2, v3, p1}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->saveBitMap(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 872
    if-eqz v2, :cond_0

    .line 874
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 880
    :cond_0
    :goto_0
    return-void

    .line 875
    :catch_0
    move-exception v1

    .line 876
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v4, "ThumbnailDocumentParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 872
    .end local v0    # "btm":Landroid/graphics/Bitmap;
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "scaled":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v4

    if-eqz v2, :cond_1

    .line 874
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 877
    :cond_1
    :goto_1
    throw v4

    .line 875
    :catch_1
    move-exception v1

    .line 876
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "ThumbnailDocumentParser"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private saveBitMap(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/io/File;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "file"    # Ljava/io/File;

    .prologue
    .line 205
    :try_start_0
    invoke-static {p1}, Lcom/samsung/thumbnail/office/ThumbnailUtils;->getPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->thumbnailFilePath:Ljava/lang/String;

    .line 207
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->thumbnailFilePath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 208
    .local v2, "outputFile":Ljava/io/File;
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 213
    .local v1, "out":Ljava/io/FileOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x19

    invoke-virtual {p2, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 214
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .end local v2    # "outputFile":Ljava/io/File;
    :goto_0
    return-void

    .line 215
    :catch_0
    move-exception v0

    .line 216
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v3, "ThumbnailDocumentParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception while writing pictures to out file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private sizeCheck(Ljava/io/InputStream;)Z
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 835
    const/4 v0, 0x1

    .line 838
    .local v0, "parsingCheck":Z
    :try_start_0
    new-instance v1, Ljava/util/zip/ZipInputStream;

    invoke-direct {v1, p1}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v1}, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;->checkExcel(Ljava/util/zip/ZipInputStream;)Z

    move-result v0

    .line 840
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 845
    :goto_0
    return v0

    .line 841
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public Parse(Ljava/io/File;Landroid/content/Context;)Z
    .locals 6
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 122
    sput-object p2, Lcom/samsung/index/parser/ThumbnailDocumentParser;->mContext:Landroid/content/Context;

    .line 123
    const/4 v0, 0x0

    .line 125
    .local v0, "contents":Z
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    .line 126
    .local v2, "fileName":Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/index/Utils;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 128
    .local v1, "extension":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 129
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v4, "File does not have proper extension"

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 133
    :cond_0
    invoke-static {}, Lcom/samsung/thumbnail/office/ThumbnailUtils;->initHighLightColorMap()V

    .line 135
    const-string/jumbo v3, ".txt"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, ".xml"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 137
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->readTXT(Ljava/io/File;Landroid/content/Context;)Z

    move-result v0

    .line 189
    :goto_0
    return v0

    .line 138
    :cond_2
    const-string/jumbo v3, ".docx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string/jumbo v3, ".docm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string/jumbo v3, ".dotm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string/jumbo v3, ".dotx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 142
    :cond_3
    invoke-virtual {p0, p1, p2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->readDOCX(Ljava/io/File;Landroid/content/Context;)Z

    move-result v0

    goto :goto_0

    .line 144
    :cond_4
    const-string/jumbo v3, ".xlsx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v3, ".xlsm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v3, ".xltm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v3, ".xltx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 148
    :cond_5
    invoke-virtual {p0, p1, p2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->readXLSX(Ljava/io/File;Landroid/content/Context;)Z

    move-result v0

    goto :goto_0

    .line 150
    :cond_6
    const-string/jumbo v3, ".pptx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".potm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".potx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".ppam"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".ppsm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".ppsx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".pptm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".sldm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".sldx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 159
    :cond_7
    invoke-virtual {p0, p1, p2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->readPPTX(Ljava/io/File;Landroid/content/Context;)Z

    move-result v0

    goto/16 :goto_0

    .line 161
    :cond_8
    const-string/jumbo v3, ".doc"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, ".dot"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 163
    :cond_9
    invoke-virtual {p0, p1, p2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->readDOC(Ljava/io/File;Landroid/content/Context;)Z

    move-result v0

    goto/16 :goto_0

    .line 165
    :cond_a
    const-string/jumbo v3, ".ppt"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string/jumbo v3, ".pot"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string/jumbo v3, ".pps"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 168
    :cond_b
    invoke-virtual {p0, p1, p2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->readPPT(Ljava/io/File;Landroid/content/Context;)Z

    move-result v0

    goto/16 :goto_0

    .line 170
    :cond_c
    const-string/jumbo v3, ".xls"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_d

    const-string/jumbo v3, ".xlm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_d

    const-string/jumbo v3, ".xlt"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 173
    :cond_d
    invoke-virtual {p0, p1, p2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->readXLS(Ljava/io/File;Landroid/content/Context;)Z

    move-result v0

    goto/16 :goto_0

    .line 175
    :cond_e
    const-string/jumbo v3, ".pdf"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 176
    invoke-virtual {p0, p1, p2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->readPDF(Ljava/io/File;Landroid/content/Context;)Z

    move-result v0

    goto/16 :goto_0

    .line 185
    :cond_f
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " is not supported"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public getFolderName()Ljava/io/File;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    return-object v0
.end method

.method public getThumbnailPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->thumbnailFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public readDOC(Ljava/io/File;Landroid/content/Context;)Z
    .locals 21
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 591
    const/4 v13, 0x0

    .line 592
    .local v13, "retValue":Z
    const/4 v15, 0x0

    .line 593
    .local v15, "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    const/4 v10, 0x0

    .line 595
    .local v10, "inputStream":Ljava/io/FileInputStream;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 597
    .local v6, "fileSize":J
    sget-wide v18, Lcom/samsung/index/parser/ThumbnailDocumentParser;->MAX_DOC_SIZE:J
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_17
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lorg/apache/poi/poifs/filesystem/OfficeXmlFileException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_a
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v17, v6, v18

    if-ltz v17, :cond_1

    .line 598
    const/16 v17, 0x0

    .line 659
    if-eqz v10, :cond_0

    .line 661
    :try_start_1
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 666
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    .line 668
    .end local v6    # "fileSize":J
    :goto_1
    return v17

    .line 662
    .restart local v6    # "fileSize":J
    :catch_0
    move-exception v4

    .line 663
    .local v4, "e":Ljava/io/IOException;
    const-string/jumbo v18, "ThumbnailDocumentParser"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "IOException : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 600
    .end local v4    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_2
    new-instance v11, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_17
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Lorg/apache/poi/poifs/filesystem/OfficeXmlFileException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_a
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 601
    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .local v11, "inputStream":Ljava/io/FileInputStream;
    :try_start_3
    new-instance v16, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;-><init>(Landroid/content/Context;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_18
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_3 .. :try_end_3} :catch_15
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_3 .. :try_end_3} :catch_13
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_11
    .catch Lorg/apache/poi/poifs/filesystem/OfficeXmlFileException; {:try_start_3 .. :try_end_3} :catch_f
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_d
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 603
    .end local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .local v16, "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    :try_start_4
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getFolderName()Ljava/io/File;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    .line 604
    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->setEmailPreview(Z)V

    .line 605
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->readDocument(Ljava/io/FileInputStream;)V

    .line 606
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getCanvas()Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    move-result-object v8

    .line 608
    .local v8, "firstPageCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->getCanvasDraw()Lcom/samsung/thumbnail/customview/draw/CanvasDraw;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->isDrawn()Z

    move-result v17

    if-nez v17, :cond_3

    .line 609
    new-instance v17, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v18, "Thumbnail Image is blank"

    invoke-direct/range {v17 .. v18}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v17
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_4 .. :try_end_4} :catch_16
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_4 .. :try_end_4} :catch_14
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_4 .. :try_end_4} :catch_12
    .catch Lorg/apache/poi/poifs/filesystem/OfficeXmlFileException; {:try_start_4 .. :try_end_4} :catch_10
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_e
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 630
    .end local v8    # "firstPageCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    :catch_1
    move-exception v4

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    move-object/from16 v15, v16

    .line 631
    .end local v6    # "fileSize":J
    .end local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .local v4, "e":Ljava/io/FileNotFoundException;
    .restart local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    :goto_2
    const/4 v13, 0x0

    .line 632
    :try_start_5
    const-string/jumbo v17, "ThumbnailDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Exception: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 659
    if-eqz v10, :cond_2

    .line 661
    :try_start_6
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 666
    .end local v4    # "e":Ljava/io/FileNotFoundException;
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    :goto_4
    move/from16 v17, v13

    .line 668
    goto/16 :goto_1

    .line 613
    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .end local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v6    # "fileSize":J
    .restart local v8    # "firstPageCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    :cond_3
    :try_start_7
    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->getCanvasBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    .line 615
    .local v12, "orgBitmap":Landroid/graphics/Bitmap;
    if-eqz v12, :cond_5

    .line 617
    const/16 v14, 0x220

    .line 618
    .local v14, "width":I
    const/16 v9, 0x2d0

    .line 619
    .local v9, "height":I
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v17

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_4

    .line 620
    const/16 v14, 0x2d0

    .line 621
    const/16 v9, 0x220

    .line 623
    :cond_4
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-static {v12, v14, v9, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 626
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v12, v2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->saveBitMap(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/io/File;)V

    .line 628
    .end local v9    # "height":I
    .end local v14    # "width":I
    :cond_5
    const/4 v13, 0x1

    .line 629
    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->clearCanvas()V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_7 .. :try_end_7} :catch_16
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_7 .. :try_end_7} :catch_14
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_7 .. :try_end_7} :catch_12
    .catch Lorg/apache/poi/poifs/filesystem/OfficeXmlFileException; {:try_start_7 .. :try_end_7} :catch_10
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_e
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 659
    if-eqz v11, :cond_6

    .line 661
    :try_start_8
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    .line 666
    :cond_6
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    move-object/from16 v15, v16

    .line 667
    .end local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    goto :goto_4

    .line 662
    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .end local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    :catch_2
    move-exception v4

    .line 663
    .local v4, "e":Ljava/io/IOException;
    const-string/jumbo v17, "ThumbnailDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "IOException : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 662
    .end local v6    # "fileSize":J
    .end local v8    # "firstPageCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .end local v12    # "orgBitmap":Landroid/graphics/Bitmap;
    .end local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .local v4, "e":Ljava/io/FileNotFoundException;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    .restart local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    :catch_3
    move-exception v4

    .line 663
    .local v4, "e":Ljava/io/IOException;
    const-string/jumbo v17, "ThumbnailDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "IOException : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 633
    .end local v4    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v4

    .line 635
    .local v4, "e":Lorg/apache/poi/EncryptedDocumentException;
    :goto_6
    :try_start_9
    new-instance v17, Lcom/samsung/index/PasswordProtectedDocException;

    invoke-virtual {v4}, Lorg/apache/poi/EncryptedDocumentException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Lcom/samsung/index/PasswordProtectedDocException;-><init>(Ljava/lang/String;)V

    throw v17
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 659
    .end local v4    # "e":Lorg/apache/poi/EncryptedDocumentException;
    :catchall_0
    move-exception v17

    :goto_7
    if-eqz v10, :cond_7

    .line 661
    :try_start_a
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_c

    .line 666
    :cond_7
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    throw v17

    .line 636
    :catch_5
    move-exception v4

    .line 638
    .local v4, "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :goto_9
    :try_start_b
    throw v4

    .line 639
    .end local v4    # "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :catch_6
    move-exception v4

    .line 641
    .local v4, "e":Ljava/lang/UnsupportedOperationException;
    :goto_a
    throw v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 642
    .end local v4    # "e":Ljava/lang/UnsupportedOperationException;
    :catch_7
    move-exception v4

    .line 646
    .local v4, "e":Lorg/apache/poi/poifs/filesystem/OfficeXmlFileException;
    :goto_b
    if-eqz v15, :cond_8

    .line 648
    :try_start_c
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/word/WordDocCustomParser;->getFolderName()Ljava/io/File;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    .line 650
    :cond_8
    invoke-virtual/range {p0 .. p2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->readDOCX(Ljava/io/File;Landroid/content/Context;)Z
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result v13

    .line 659
    :goto_c
    if-eqz v10, :cond_9

    .line 661
    :try_start_d
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9

    .line 666
    .end local v4    # "e":Lorg/apache/poi/poifs/filesystem/OfficeXmlFileException;
    :cond_9
    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    goto/16 :goto_4

    .line 651
    .restart local v4    # "e":Lorg/apache/poi/poifs/filesystem/OfficeXmlFileException;
    :catch_8
    move-exception v5

    .line 652
    .local v5, "e1":Ljava/lang/Exception;
    const/4 v13, 0x0

    .line 653
    :try_start_e
    const-string/jumbo v17, "ThumbnailDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Exception: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_c

    .line 662
    .end local v5    # "e1":Ljava/lang/Exception;
    :catch_9
    move-exception v4

    .line 663
    .local v4, "e":Ljava/io/IOException;
    const-string/jumbo v17, "ThumbnailDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "IOException : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 655
    .end local v4    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v4

    .line 656
    .local v4, "e":Ljava/lang/Exception;
    :goto_e
    :try_start_f
    const-string/jumbo v17, "ThumbnailDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Exception in Doc: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 657
    const/4 v13, 0x0

    .line 659
    if-eqz v10, :cond_a

    .line 661
    :try_start_10
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_b

    .line 666
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_a
    :goto_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    goto/16 :goto_4

    .line 662
    .restart local v4    # "e":Ljava/lang/Exception;
    :catch_b
    move-exception v4

    .line 663
    .local v4, "e":Ljava/io/IOException;
    const-string/jumbo v17, "ThumbnailDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "IOException : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_f

    .line 662
    .end local v4    # "e":Ljava/io/IOException;
    :catch_c
    move-exception v4

    .line 663
    .restart local v4    # "e":Ljava/io/IOException;
    const-string/jumbo v18, "ThumbnailDocumentParser"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "IOException : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    .line 659
    .end local v4    # "e":Ljava/io/IOException;
    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "fileSize":J
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v17

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_7

    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .end local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    :catchall_2
    move-exception v17

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    move-object/from16 v15, v16

    .end local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    goto/16 :goto_7

    .line 655
    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    :catch_d
    move-exception v4

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_e

    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .end local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    :catch_e
    move-exception v4

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    move-object/from16 v15, v16

    .end local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    goto/16 :goto_e

    .line 642
    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    :catch_f
    move-exception v4

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_b

    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .end local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    :catch_10
    move-exception v4

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    move-object/from16 v15, v16

    .end local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    goto/16 :goto_b

    .line 639
    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    :catch_11
    move-exception v4

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_a

    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .end local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    :catch_12
    move-exception v4

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    move-object/from16 v15, v16

    .end local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    goto/16 :goto_a

    .line 636
    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    :catch_13
    move-exception v4

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_9

    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .end local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    :catch_14
    move-exception v4

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    move-object/from16 v15, v16

    .end local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    goto/16 :goto_9

    .line 633
    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    :catch_15
    move-exception v4

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .end local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    :catch_16
    move-exception v4

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    move-object/from16 v15, v16

    .end local v16    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    .restart local v15    # "wordViewer":Lcom/samsung/thumbnail/office/word/WordDocCustomParser;
    goto/16 :goto_6

    .line 630
    .end local v6    # "fileSize":J
    :catch_17
    move-exception v4

    goto/16 :goto_2

    .end local v10    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "fileSize":J
    .restart local v11    # "inputStream":Ljava/io/FileInputStream;
    :catch_18
    move-exception v4

    move-object v10, v11

    .end local v11    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_2
.end method

.method public readDOCX(Ljava/io/File;Landroid/content/Context;)Z
    .locals 13
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    .line 703
    const/4 v7, 0x1

    .line 704
    .local v7, "retValue":Z
    const/4 v4, 0x0

    .line 706
    .local v4, "inputStream":Ljava/io/FileInputStream;
    :try_start_0
    invoke-static {p1}, Lcom/samsung/thumbnail/util/Utils;->isValid(Ljava/io/File;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 707
    const-string/jumbo v10, "ThumbnailDocumentParser"

    const-string/jumbo v11, "Invalid file, Cannot be parsed"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_e
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 756
    if-eqz v4, :cond_0

    .line 758
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 763
    :cond_0
    :goto_0
    iget-object v10, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v10}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    .line 765
    :goto_1
    return v9

    .line 759
    :catch_0
    move-exception v1

    .line 760
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v10, "ThumbnailDocumentParser"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "IOException : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 711
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_2
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_e
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 712
    .end local v4    # "inputStream":Ljava/io/FileInputStream;
    .local v5, "inputStream":Ljava/io/FileInputStream;
    :try_start_3
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    const/4 v9, 0x0

    invoke-direct {v0, v5, p2, v9, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;-><init>(Ljava/io/InputStream;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CustomView;Ljava/io/File;)V

    .line 715
    .local v0, "document":Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getFolderPath()Ljava/io/File;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    .line 716
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->setEmailPreview(Z)V

    .line 717
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->setFolderName()V

    .line 718
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->load()V

    .line 719
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvas()Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    move-result-object v2

    .line 721
    .local v2, "firstPageCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->getCanvasDraw()Lcom/samsung/thumbnail/customview/draw/CanvasDraw;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->isDrawn()Z

    move-result v9

    if-nez v9, :cond_3

    .line 722
    new-instance v9, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v10, "Thumbnail Image is blank"

    invoke-direct {v9, v10}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_3 .. :try_end_3} :catch_d
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_a
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 740
    .end local v0    # "document":Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;
    .end local v2    # "firstPageCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    :catch_1
    move-exception v1

    move-object v4, v5

    .line 741
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .local v1, "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "inputStream":Ljava/io/FileInputStream;
    :goto_2
    const/4 v7, 0x0

    .line 742
    :try_start_4
    const-string/jumbo v9, "ThumbnailDocumentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception in Docx: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 756
    if-eqz v4, :cond_2

    .line 758
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 763
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :cond_2
    :goto_3
    iget-object v9, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v9}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    :goto_4
    move v9, v7

    .line 765
    goto :goto_1

    .line 726
    .end local v4    # "inputStream":Ljava/io/FileInputStream;
    .restart local v0    # "document":Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;
    .restart local v2    # "firstPageCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :cond_3
    :try_start_6
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->getCanvasBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 729
    .local v6, "orgBitmap":Landroid/graphics/Bitmap;
    const/16 v8, 0x220

    .line 730
    .local v8, "width":I
    const/16 v3, 0x2d0

    .line 731
    .local v3, "height":I
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    if-ge v9, v10, :cond_4

    .line 732
    const/16 v8, 0x2d0

    .line 733
    const/16 v3, 0x220

    .line 735
    :cond_4
    const/4 v9, 0x1

    invoke-static {v6, v8, v3, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 738
    invoke-direct {p0, p2, v6, p1}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->saveBitMap(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/io/File;)V

    .line 739
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->clearCanvas()V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_6 .. :try_end_6} :catch_d
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_6 .. :try_end_6} :catch_c
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_6 .. :try_end_6} :catch_b
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_a
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 756
    if-eqz v5, :cond_5

    .line 758
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 763
    :cond_5
    :goto_5
    iget-object v9, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v9}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    move-object v4, v5

    .line 764
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v4    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_4

    .line 759
    .end local v4    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :catch_2
    move-exception v1

    .line 760
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v9, "ThumbnailDocumentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "IOException : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 759
    .end local v0    # "document":Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;
    .end local v2    # "firstPageCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .end local v3    # "height":I
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .end local v6    # "orgBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "width":I
    .local v1, "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "inputStream":Ljava/io/FileInputStream;
    :catch_3
    move-exception v1

    .line 760
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v9, "ThumbnailDocumentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "IOException : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 743
    .end local v1    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v1

    .line 745
    .local v1, "e":Lcom/samsung/index/PasswordProtectedDocException;
    :goto_6
    :try_start_8
    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 756
    .end local v1    # "e":Lcom/samsung/index/PasswordProtectedDocException;
    :catchall_0
    move-exception v9

    :goto_7
    if-eqz v4, :cond_6

    .line 758
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_9

    .line 763
    :cond_6
    :goto_8
    iget-object v10, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v10}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    throw v9

    .line 746
    :catch_5
    move-exception v1

    .line 748
    .local v1, "e":Lorg/apache/poi/EncryptedDocumentException;
    :goto_9
    :try_start_a
    new-instance v9, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-virtual {v1}, Lorg/apache/poi/EncryptedDocumentException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 749
    .end local v1    # "e":Lorg/apache/poi/EncryptedDocumentException;
    :catch_6
    move-exception v1

    .line 751
    .local v1, "e":Ljava/lang/UnsupportedOperationException;
    :goto_a
    throw v1

    .line 752
    .end local v1    # "e":Ljava/lang/UnsupportedOperationException;
    :catch_7
    move-exception v1

    .line 753
    .local v1, "e":Ljava/lang/Exception;
    :goto_b
    const/4 v7, 0x0

    .line 754
    const-string/jumbo v9, "ThumbnailDocumentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception in Docx: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 756
    if-eqz v4, :cond_7

    .line 758
    :try_start_b
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    .line 763
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_7
    :goto_c
    iget-object v9, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v9}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    goto/16 :goto_4

    .line 759
    .restart local v1    # "e":Ljava/lang/Exception;
    :catch_8
    move-exception v1

    .line 760
    .local v1, "e":Ljava/io/IOException;
    const-string/jumbo v9, "ThumbnailDocumentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "IOException : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 759
    .end local v1    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v1

    .line 760
    .restart local v1    # "e":Ljava/io/IOException;
    const-string/jumbo v10, "ThumbnailDocumentParser"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "IOException : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 756
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v9

    move-object v4, v5

    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v4    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_7

    .line 752
    .end local v4    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :catch_a
    move-exception v1

    move-object v4, v5

    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v4    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_b

    .line 749
    .end local v4    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :catch_b
    move-exception v1

    move-object v4, v5

    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v4    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_a

    .line 746
    .end local v4    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :catch_c
    move-exception v1

    move-object v4, v5

    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v4    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_9

    .line 743
    .end local v4    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :catch_d
    move-exception v1

    move-object v4, v5

    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v4    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .line 740
    :catch_e
    move-exception v1

    goto/16 :goto_2
.end method

.method public readPDF(Ljava/io/File;Landroid/content/Context;)Z
    .locals 5
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1014
    const/4 v1, 0x1

    .line 1017
    .local v1, "retValue":Z
    :try_start_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-le v2, v3, :cond_0

    .line 1018
    invoke-direct {p0, p1, p2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->createAndStorePDFBitmapUsingL(Ljava/io/File;Landroid/content/Context;)V
    :try_end_0
    .catch Lorg/icepdf/core/exceptions/PDFSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1035
    :goto_0
    iget-object v2, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    .line 1037
    :goto_1
    return v1

    .line 1020
    :cond_0
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->createAndStorePDFBitmap(Ljava/io/File;Landroid/content/Context;)V
    :try_end_1
    .catch Lorg/icepdf/core/exceptions/PDFSecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1022
    :catch_0
    move-exception v0

    .line 1023
    .local v0, "e":Lorg/icepdf/core/exceptions/PDFSecurityException;
    :try_start_2
    new-instance v2, Lcom/samsung/index/PasswordProtectedDocException;

    invoke-virtual {v0}, Lorg/icepdf/core/exceptions/PDFSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/index/PasswordProtectedDocException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1035
    .end local v0    # "e":Lorg/icepdf/core/exceptions/PDFSecurityException;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v3}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    throw v2

    .line 1024
    :catch_1
    move-exception v0

    .line 1025
    .local v0, "e":Lorg/apache/poi/EncryptedDocumentException;
    :try_start_3
    new-instance v2, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-virtual {v0}, Lorg/apache/poi/EncryptedDocumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1026
    .end local v0    # "e":Lorg/apache/poi/EncryptedDocumentException;
    :catch_2
    move-exception v0

    .line 1028
    .local v0, "e":Ljava/lang/UnsupportedOperationException;
    throw v0

    .line 1029
    .end local v0    # "e":Ljava/lang/UnsupportedOperationException;
    :catch_3
    move-exception v0

    .line 1031
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .line 1032
    const-string/jumbo v2, "ThumbnailDocumentParser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1035
    iget-object v2, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    goto :goto_1
.end method

.method public readPPT(Ljava/io/File;Landroid/content/Context;)Z
    .locals 11
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 501
    const/4 v7, 0x1

    .line 503
    .local v7, "retValue":Z
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 524
    .local v4, "inputStream":Ljava/io/FileInputStream;
    new-instance v6, Lcom/samsung/thumbnail/office/point/PPTCustomParser;

    const/4 v8, 0x1

    invoke-direct {v6, p2, v8}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;-><init>(Landroid/content/Context;Z)V

    .line 525
    .local v6, "ps":Lcom/samsung/thumbnail/office/point/PPTCustomParser;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getFolderPath()Ljava/io/File;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    .line 526
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->setCustomView(Landroid/widget/LinearLayout;)V

    .line 527
    invoke-virtual {v6, v4}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->readMySlideShow(Ljava/io/FileInputStream;)V

    .line 529
    new-instance v1, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getCanvasEleCreater()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v8

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getFirstPageCanvas()Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->getCanvasBitmap()Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-direct {v1, v8, v9}, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;-><init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;Landroid/graphics/Bitmap;)V

    .line 532
    .local v1, "drawObj":Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getFirstPageCanvas()Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v8

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getCanvasEleCreater()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v9

    invoke-virtual {v1, v8, v9, p2}, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->handleDraw(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Landroid/content/Context;)V

    .line 535
    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->getBitmapFromCanvas()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 538
    .local v0, "b":Landroid/graphics/Bitmap;
    invoke-direct {p0, p2, v0, p1}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->saveBitMap(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/io/File;)V

    .line 539
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/point/PPTCustomParser;->getFirstPageCanvas()Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->clearCanvas()V
    :try_end_0
    .catch Lorg/apache/poi/hslf/exceptions/EncryptedPowerPointFileException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/apache/poi/poifs/filesystem/OfficeXmlFileException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 571
    iget-object v8, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v8}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    .line 574
    .end local v0    # "b":Landroid/graphics/Bitmap;
    .end local v1    # "drawObj":Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;
    .end local v4    # "inputStream":Ljava/io/FileInputStream;
    .end local v6    # "ps":Lcom/samsung/thumbnail/office/point/PPTCustomParser;
    :goto_0
    return v7

    .line 540
    :catch_0
    move-exception v2

    .line 542
    .local v2, "e":Lorg/apache/poi/hslf/exceptions/EncryptedPowerPointFileException;
    :try_start_1
    new-instance v8, Lcom/samsung/index/PasswordProtectedDocException;

    invoke-virtual {v2}, Lorg/apache/poi/hslf/exceptions/EncryptedPowerPointFileException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/samsung/index/PasswordProtectedDocException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 571
    .end local v2    # "e":Lorg/apache/poi/hslf/exceptions/EncryptedPowerPointFileException;
    :catchall_0
    move-exception v8

    iget-object v9, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v9}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    throw v8

    .line 543
    :catch_1
    move-exception v2

    .line 545
    .local v2, "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :try_start_2
    throw v2

    .line 546
    .end local v2    # "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :catch_2
    move-exception v2

    .line 548
    .local v2, "e":Ljava/lang/UnsupportedOperationException;
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 549
    .end local v2    # "e":Ljava/lang/UnsupportedOperationException;
    :catch_3
    move-exception v2

    .line 553
    .local v2, "e":Lorg/apache/poi/poifs/filesystem/OfficeXmlFileException;
    :try_start_3
    invoke-virtual {p0, p1, p2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->readPPTX(Ljava/io/File;Landroid/content/Context;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v7

    .line 571
    :goto_1
    iget-object v8, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v8}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    goto :goto_0

    .line 554
    :catch_4
    move-exception v3

    .line 555
    .local v3, "e1":Ljava/lang/Exception;
    const/4 v7, 0x0

    .line 556
    :try_start_4
    const-string/jumbo v8, "ThumbnailDocumentParser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 558
    .end local v2    # "e":Lorg/apache/poi/poifs/filesystem/OfficeXmlFileException;
    .end local v3    # "e1":Ljava/lang/Exception;
    :catch_5
    move-exception v2

    .line 559
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    .line 560
    .local v5, "msg":Ljava/lang/String;
    const-string/jumbo v8, "ThumbnailDocumentParser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "IOException: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    if-eqz v5, :cond_0

    const-string/jumbo v8, "Invalid header signature"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 563
    new-instance v8, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-direct {v8, v5}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 565
    :cond_0
    const/4 v7, 0x0

    .line 571
    iget-object v8, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v8}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    goto :goto_0

    .line 566
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "msg":Ljava/lang/String;
    :catch_6
    move-exception v2

    .line 567
    .local v2, "e":Ljava/lang/Exception;
    :try_start_5
    const-string/jumbo v8, "ThumbnailDocumentParser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception in PPT: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 569
    const/4 v7, 0x0

    .line 571
    iget-object v8, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v8}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    goto/16 :goto_0
.end method

.method public readPPTX(Ljava/io/File;Landroid/content/Context;)Z
    .locals 19
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 893
    const/4 v10, 0x0

    .line 897
    .local v10, "retValue":Z
    const/4 v5, 0x0

    .line 898
    .local v5, "inputStream":Ljava/io/FileInputStream;
    const/4 v13, 0x0

    .line 900
    .local v13, "zipInput":Ljava/util/zip/ZipInputStream;
    :try_start_0
    invoke-static/range {p1 .. p1}, Lcom/samsung/thumbnail/util/Utils;->isValid(Ljava/io/File;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 901
    const-string/jumbo v15, "ThumbnailDocumentParser"

    const-string/jumbo v16, "Invalid file, Cannot be parsed"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_0 .. :try_end_0} :catch_1d
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 902
    const/4 v15, 0x0

    .line 936
    if-eqz v5, :cond_0

    .line 938
    :try_start_1
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 944
    :cond_0
    :goto_0
    if-eqz v13, :cond_1

    .line 946
    :try_start_2
    invoke-virtual {v13}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 996
    :cond_1
    :goto_1
    return v15

    .line 939
    :catch_0
    move-exception v3

    .line 940
    .local v3, "e":Ljava/io/IOException;
    const-string/jumbo v16, "ThumbnailDocumentParser"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 947
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 948
    .restart local v3    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "ThumbnailDocumentParser"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 904
    .end local v3    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_3
    new-instance v6, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_3 .. :try_end_3} :catch_1d
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 906
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .local v6, "inputStream":Ljava/io/FileInputStream;
    :try_start_4
    new-instance v14, Ljava/util/zip/ZipInputStream;

    invoke-direct {v14, v6}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_4
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_4 .. :try_end_4} :catch_1e
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_4 .. :try_end_4} :catch_1b
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_4 .. :try_end_4} :catch_19
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_17
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 908
    .end local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    .local v14, "zipInput":Ljava/util/zip/ZipInputStream;
    :goto_2
    :try_start_5
    invoke-virtual {v14}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v12

    .local v12, "zipEntry":Ljava/util/zip/ZipEntry;
    if-eqz v12, :cond_3

    .line 909
    invoke-virtual {v12}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v8

    .line 912
    .local v8, "name":Ljava/lang/String;
    const-string/jumbo v15, "docProps/thumbnail.jpeg"

    invoke-virtual {v8, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 913
    const/4 v10, 0x1

    .line 915
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v14}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->makeACopy(Ljava/io/File;Landroid/content/Context;Ljava/io/InputStream;)V

    .line 916
    invoke-virtual {v14}, Ljava/util/zip/ZipInputStream;->closeEntry()V
    :try_end_5
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_5 .. :try_end_5} :catch_1c
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_5 .. :try_end_5} :catch_1a
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_18
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 936
    .end local v8    # "name":Ljava/lang/String;
    :cond_3
    if-eqz v6, :cond_4

    .line 938
    :try_start_6
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 944
    :cond_4
    :goto_3
    if-eqz v14, :cond_f

    .line 946
    :try_start_7
    invoke-virtual {v14}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    move-object v13, v14

    .line 953
    .end local v12    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    :goto_4
    if-nez v10, :cond_d

    .line 955
    :try_start_8
    new-instance v5, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_8
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_8 .. :try_end_8} :catch_16
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_8 .. :try_end_8} :catch_e
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_8 .. :try_end_8} :catch_f
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_10
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 956
    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :try_start_9
    new-instance v11, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v11, v5, v0, v1}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;-><init>(Ljava/io/InputStream;Ljava/io/File;Landroid/content/Context;)V

    .line 957
    .local v11, "slideShow":Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getFolderPath()Ljava/io/File;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    .line 958
    const/4 v15, 0x0

    invoke-virtual {v11, v15}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->setEmailPreview(Z)V

    .line 959
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->readDocument(Ljava/io/File;)V

    .line 961
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;->getCanvas()Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    move-result-object v4

    .line 962
    .local v4, "firstPageCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->drawOnCanvas(Landroid/content/Context;)Z

    move-result v7

    .line 963
    .local v7, "isSuccess":Z
    if-nez v7, :cond_a

    .line 964
    new-instance v15, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v16, "Some of the features of this document are not supported"

    invoke-direct/range {v15 .. v16}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v15
    :try_end_9
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_9 .. :try_end_9} :catch_15
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_9 .. :try_end_9} :catch_14
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_13
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 972
    .end local v4    # "firstPageCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .end local v7    # "isSuccess":Z
    .end local v11    # "slideShow":Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;
    :catch_2
    move-exception v3

    .line 974
    .local v3, "e":Lorg/apache/poi/EncryptedDocumentException;
    :goto_5
    :try_start_a
    new-instance v15, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-virtual {v3}, Lorg/apache/poi/EncryptedDocumentException;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v15
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 985
    .end local v3    # "e":Lorg/apache/poi/EncryptedDocumentException;
    :catchall_0
    move-exception v15

    :goto_6
    if-eqz v5, :cond_5

    .line 987
    :try_start_b
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_12

    .line 992
    :cond_5
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    throw v15

    .line 920
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .end local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v8    # "name":Ljava/lang/String;
    .restart local v12    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_6
    :try_start_c
    invoke-virtual {v14}, Ljava/util/zip/ZipInputStream;->closeEntry()V
    :try_end_c
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_c .. :try_end_c} :catch_3
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_c .. :try_end_c} :catch_1c
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_c .. :try_end_c} :catch_1a
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_18
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    goto/16 :goto_2

    .line 923
    .end local v8    # "name":Ljava/lang/String;
    .end local v12    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_3
    move-exception v3

    move-object v13, v14

    .end local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    move-object v5, v6

    .line 925
    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v3    # "e":Lorg/apache/poi/EncryptedDocumentException;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :goto_8
    :try_start_d
    new-instance v15, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-virtual {v3}, Lorg/apache/poi/EncryptedDocumentException;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v15
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 936
    .end local v3    # "e":Lorg/apache/poi/EncryptedDocumentException;
    :catchall_1
    move-exception v15

    :goto_9
    if-eqz v5, :cond_7

    .line 938
    :try_start_e
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_b

    .line 944
    :cond_7
    :goto_a
    if-eqz v13, :cond_8

    .line 946
    :try_start_f
    invoke-virtual {v13}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_c

    .line 949
    :cond_8
    :goto_b
    throw v15

    .line 939
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .end local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v12    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_4
    move-exception v3

    .line 940
    .local v3, "e":Ljava/io/IOException;
    const-string/jumbo v15, "ThumbnailDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 947
    .end local v3    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v3

    .line 948
    .restart local v3    # "e":Ljava/io/IOException;
    const-string/jumbo v15, "ThumbnailDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v13, v14

    .line 949
    .end local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto/16 :goto_4

    .line 926
    .end local v3    # "e":Ljava/io/IOException;
    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .end local v12    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :catch_6
    move-exception v3

    .line 928
    .local v3, "e":Lcom/samsung/index/PasswordProtectedDocException;
    :goto_c
    :try_start_10
    throw v3

    .line 929
    .end local v3    # "e":Lcom/samsung/index/PasswordProtectedDocException;
    :catch_7
    move-exception v3

    .line 931
    .local v3, "e":Ljava/lang/UnsupportedOperationException;
    :goto_d
    throw v3

    .line 932
    .end local v3    # "e":Ljava/lang/UnsupportedOperationException;
    :catch_8
    move-exception v3

    .line 933
    .local v3, "e":Ljava/lang/Exception;
    :goto_e
    const-string/jumbo v15, "ThumbnailDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception in reading PPTX: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 936
    if-eqz v5, :cond_9

    .line 938
    :try_start_11
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_9

    .line 944
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_9
    :goto_f
    if-eqz v13, :cond_e

    .line 946
    :try_start_12
    invoke-virtual {v13}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_a

    move-object v6, v5

    .line 949
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_4

    .line 939
    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v3    # "e":Ljava/lang/Exception;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :catch_9
    move-exception v3

    .line 940
    .local v3, "e":Ljava/io/IOException;
    const-string/jumbo v15, "ThumbnailDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_f

    .line 947
    .end local v3    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v3

    .line 948
    .restart local v3    # "e":Ljava/io/IOException;
    const-string/jumbo v15, "ThumbnailDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v5

    .line 949
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_4

    .line 939
    .end local v3    # "e":Ljava/io/IOException;
    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :catch_b
    move-exception v3

    .line 940
    .restart local v3    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "ThumbnailDocumentParser"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    .line 947
    .end local v3    # "e":Ljava/io/IOException;
    :catch_c
    move-exception v3

    .line 948
    .restart local v3    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "ThumbnailDocumentParser"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_b

    .line 967
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v4    # "firstPageCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .restart local v7    # "isSuccess":Z
    .restart local v11    # "slideShow":Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;
    :cond_a
    :try_start_13
    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->getCanvasBitmap()Landroid/graphics/Bitmap;

    move-result-object v9

    .line 969
    .local v9, "orgBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v9, v2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->saveBitMap(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/io/File;)V

    .line 970
    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->clearCanvas()V
    :try_end_13
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_13 .. :try_end_13} :catch_2
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_13 .. :try_end_13} :catch_15
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_13 .. :try_end_13} :catch_14
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 971
    const/4 v10, 0x1

    .line 985
    if-eqz v5, :cond_b

    .line 987
    :try_start_14
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_d

    .line 992
    :cond_b
    :goto_10
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    .end local v4    # "firstPageCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .end local v7    # "isSuccess":Z
    .end local v9    # "orgBitmap":Landroid/graphics/Bitmap;
    .end local v11    # "slideShow":Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;
    :goto_11
    move v15, v10

    .line 996
    goto/16 :goto_1

    .line 988
    .restart local v4    # "firstPageCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .restart local v7    # "isSuccess":Z
    .restart local v9    # "orgBitmap":Landroid/graphics/Bitmap;
    .restart local v11    # "slideShow":Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;
    :catch_d
    move-exception v3

    .line 989
    .restart local v3    # "e":Ljava/io/IOException;
    const-string/jumbo v15, "ThumbnailDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_10

    .line 975
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "firstPageCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .end local v7    # "isSuccess":Z
    .end local v9    # "orgBitmap":Landroid/graphics/Bitmap;
    .end local v11    # "slideShow":Lcom/samsung/thumbnail/office/ooxml/pptx/XMLSlideShow;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    :catch_e
    move-exception v3

    move-object v5, v6

    .line 977
    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .local v3, "e":Lcom/samsung/index/PasswordProtectedDocException;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :goto_12
    :try_start_15
    throw v3

    .line 978
    .end local v3    # "e":Lcom/samsung/index/PasswordProtectedDocException;
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    :catch_f
    move-exception v3

    move-object v5, v6

    .line 980
    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .local v3, "e":Ljava/lang/UnsupportedOperationException;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :goto_13
    throw v3

    .line 981
    .end local v3    # "e":Ljava/lang/UnsupportedOperationException;
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    :catch_10
    move-exception v3

    move-object v5, v6

    .line 982
    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .local v3, "e":Ljava/lang/Exception;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :goto_14
    const-string/jumbo v15, "ThumbnailDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception in reading PPTX: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    .line 983
    const/4 v10, 0x0

    .line 985
    if-eqz v5, :cond_c

    .line 987
    :try_start_16
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_11

    .line 992
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_c
    :goto_15
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    goto :goto_11

    .line 988
    .restart local v3    # "e":Ljava/lang/Exception;
    :catch_11
    move-exception v3

    .line 989
    .local v3, "e":Ljava/io/IOException;
    const-string/jumbo v15, "ThumbnailDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_15

    .line 988
    .end local v3    # "e":Ljava/io/IOException;
    :catch_12
    move-exception v3

    .line 989
    .restart local v3    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "ThumbnailDocumentParser"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Exception : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 985
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v15

    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .line 981
    :catch_13
    move-exception v3

    goto :goto_14

    .line 978
    :catch_14
    move-exception v3

    goto :goto_13

    .line 975
    :catch_15
    move-exception v3

    goto :goto_12

    .line 972
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    :catch_16
    move-exception v3

    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_5

    .line 936
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    :catchall_3
    move-exception v15

    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_9

    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .end local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catchall_4
    move-exception v15

    move-object v13, v14

    .end local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_9

    .line 932
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    :catch_17
    move-exception v3

    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_e

    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .end local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_18
    move-exception v3

    move-object v13, v14

    .end local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_e

    .line 929
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    :catch_19
    move-exception v3

    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_d

    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .end local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_1a
    move-exception v3

    move-object v13, v14

    .end local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_d

    .line 926
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    :catch_1b
    move-exception v3

    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_c

    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .end local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_1c
    move-exception v3

    move-object v13, v14

    .end local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_c

    .line 923
    :catch_1d
    move-exception v3

    goto/16 :goto_8

    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    :catch_1e
    move-exception v3

    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_8

    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    :cond_d
    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_11

    :cond_e
    move-object v6, v5

    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_4

    .end local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v12    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_f
    move-object v13, v14

    .end local v14    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v13    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto/16 :goto_4
.end method

.method public readTXT(Ljava/io/File;Landroid/content/Context;)Z
    .locals 8
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 391
    const/4 v3, 0x1

    .line 393
    .local v3, "retValue":Z
    new-instance v4, Lcom/samsung/thumbnail/office/txt/TextFileParser;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/txt/TextFileParser;-><init>()V

    .line 394
    .local v4, "txtFileParser":Lcom/samsung/thumbnail/office/txt/TextFileParser;
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/txt/TextFileParser;->getFirstPageCanvas()Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    move-result-object v0

    .line 396
    .local v0, "fPgCanvas":Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    invoke-virtual {v4, p1, p2}, Lcom/samsung/thumbnail/office/txt/TextFileParser;->parseTxtFile(Ljava/io/File;Landroid/content/Context;)Z

    move-result v3

    .line 398
    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->getCanvasBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 400
    .local v2, "orgBitmap":Landroid/graphics/Bitmap;
    const/16 v5, 0x220

    .line 401
    .local v5, "width":I
    const/16 v1, 0x2d0

    .line 402
    .local v1, "height":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    if-ge v6, v7, :cond_0

    .line 403
    const/16 v5, 0x2d0

    .line 404
    const/16 v1, 0x220

    .line 406
    :cond_0
    const/4 v6, 0x1

    invoke-static {v2, v5, v1, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 410
    invoke-direct {p0, p2, v2, p1}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->saveBitMap(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/io/File;)V

    .line 411
    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->clearCanvas()V

    .line 413
    return v3
.end method

.method public readXLS(Ljava/io/File;Landroid/content/Context;)Z
    .locals 18
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 430
    const/4 v11, 0x1

    .line 432
    .local v11, "retValue":Z
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 434
    .local v6, "fileSize":J
    sget-wide v16, Lcom/samsung/index/parser/ThumbnailDocumentParser;->MAX_XLS_SIZE:J

    cmp-long v15, v6, v16

    if-ltz v15, :cond_0

    .line 435
    new-instance v15, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v16, "File size is too large to process"

    invoke-direct/range {v15 .. v16}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 440
    :cond_0
    :try_start_0
    new-instance v9, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 441
    .local v9, "inputStream":Ljava/io/FileInputStream;
    new-instance v8, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v8, v9}, Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    .line 442
    .local v8, "fs":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    new-instance v13, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    invoke-direct {v13, v8}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 443
    .local v13, "wb":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    .line 445
    new-instance v14, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v16, v0

    invoke-direct/range {v14 .. v16}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;-><init>(Lcom/samsung/thumbnail/customview/word/CustomView;Ljava/io/File;)V

    .line 446
    .local v14, "xlsCanvasView":Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;
    invoke-virtual {v13}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getNumberOfSheets()I

    move-result v15

    if-lez v15, :cond_1

    .line 447
    const/4 v15, 0x0

    invoke-virtual {v13, v15}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getSheetAt(I)Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    move-result-object v12

    .line 448
    .local v12, "sheet":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    invoke-virtual {v14, v12, v13}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->readExcelDoc(Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)I

    .line 451
    .end local v12    # "sheet":Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    :cond_1
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getCanvas()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v15

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v15}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->createAndStoreBitmapForExcel(Ljava/io/File;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    :try_end_0
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/apache/poi/poifs/filesystem/OfficeXmlFileException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 483
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    .line 486
    .end local v8    # "fs":Lorg/apache/poi/poifs/filesystem/POIFSFileSystem;
    .end local v9    # "inputStream":Ljava/io/FileInputStream;
    .end local v13    # "wb":Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .end local v14    # "xlsCanvasView":Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;
    :goto_0
    return v11

    .line 453
    :catch_0
    move-exception v4

    .line 455
    .local v4, "e":Lorg/apache/poi/EncryptedDocumentException;
    :try_start_1
    new-instance v15, Lcom/samsung/index/PasswordProtectedDocException;

    invoke-virtual {v4}, Lorg/apache/poi/EncryptedDocumentException;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lcom/samsung/index/PasswordProtectedDocException;-><init>(Ljava/lang/String;)V

    throw v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483
    .end local v4    # "e":Lorg/apache/poi/EncryptedDocumentException;
    :catchall_0
    move-exception v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    throw v15

    .line 456
    :catch_1
    move-exception v4

    .line 458
    .local v4, "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :try_start_2
    throw v4

    .line 459
    .end local v4    # "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :catch_2
    move-exception v4

    .line 461
    .local v4, "e":Ljava/lang/UnsupportedOperationException;
    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 462
    .end local v4    # "e":Ljava/lang/UnsupportedOperationException;
    :catch_3
    move-exception v4

    .line 466
    .local v4, "e":Lorg/apache/poi/poifs/filesystem/OfficeXmlFileException;
    :try_start_3
    invoke-virtual/range {p0 .. p2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->readXLSX(Ljava/io/File;Landroid/content/Context;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v11

    .line 483
    :goto_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    goto :goto_0

    .line 467
    :catch_4
    move-exception v5

    .line 468
    .local v5, "e1":Ljava/lang/Exception;
    const/4 v11, 0x0

    .line 469
    :try_start_4
    const-string/jumbo v15, "ThumbnailDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 471
    .end local v4    # "e":Lorg/apache/poi/poifs/filesystem/OfficeXmlFileException;
    .end local v5    # "e1":Ljava/lang/Exception;
    :catch_5
    move-exception v4

    .line 472
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    .line 473
    .local v10, "msg":Ljava/lang/String;
    const-string/jumbo v15, "ThumbnailDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    if-eqz v10, :cond_2

    const-string/jumbo v15, "Invalid header signature"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 476
    new-instance v15, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-direct {v15, v10}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v15
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 478
    :cond_2
    const/4 v11, 0x0

    .line 483
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    goto/16 :goto_0

    .line 479
    .end local v4    # "e":Ljava/io/IOException;
    .end local v10    # "msg":Ljava/lang/String;
    :catch_6
    move-exception v4

    .line 480
    .local v4, "e":Ljava/lang/Exception;
    :try_start_5
    const-string/jumbo v15, "ThumbnailDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception in XLS: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 481
    const/4 v11, 0x0

    .line 483
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    goto/16 :goto_0
.end method

.method public readXLSX(Ljava/io/File;Landroid/content/Context;)Z
    .locals 11
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 779
    const/4 v9, 0x1

    .line 780
    .local v9, "retValue":Z
    const/4 v8, 0x0

    .line 782
    .local v8, "inputStream":Ljava/io/FileInputStream;
    :try_start_0
    invoke-static {p1}, Lcom/samsung/thumbnail/util/Utils;->isValid(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 783
    const-string/jumbo v2, "ThumbnailDocumentParser"

    const-string/jumbo v3, "Invalid file, Cannot be parsed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 784
    const/4 v2, 0x0

    .line 822
    if-eqz v8, :cond_0

    .line 824
    :try_start_1
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 829
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v3}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    move-object v1, v8

    .line 831
    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .local v1, "inputStream":Ljava/io/FileInputStream;
    :goto_1
    return v2

    .line 825
    .end local v1    # "inputStream":Ljava/io/FileInputStream;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v7

    .line 826
    .local v7, "e":Ljava/io/IOException;
    const-string/jumbo v3, "ThumbnailDocumentParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "IOException : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 786
    .end local v7    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_2
    new-instance v10, Ljava/io/FileInputStream;

    invoke-direct {v10, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 787
    .local v10, "tempInputStream":Ljava/io/FileInputStream;
    invoke-direct {p0, v10}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->sizeCheck(Ljava/io/InputStream;)Z

    move-result v9

    .line 789
    if-eqz v9, :cond_6

    .line 790
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    .line 793
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 794
    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v1    # "inputStream":Ljava/io/FileInputStream;
    const/4 v2, 0x1

    :try_start_3
    sput-boolean v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->USE_CUSTOMVIEW:Z

    .line 796
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;-><init>(Ljava/io/FileInputStream;Ljava/io/File;Landroid/content/Context;Landroid/os/Handler;Lcom/samsung/thumbnail/customview/word/CustomView;Z)V

    .line 799
    .local v0, "worbookobjts":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getFolderPath()Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    .line 800
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->load()V

    .line 803
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getCanvas()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v2

    invoke-direct {p0, p1, p2, v2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->createAndStoreBitmapForExcel(Ljava/io/File;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    :try_end_3
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_3 .. :try_end_3} :catch_e
    .catch Lcom/samsung/index/PasswordProtectedDocException; {:try_start_3 .. :try_end_3} :catch_d
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_a
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 822
    .end local v0    # "worbookobjts":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;
    :goto_2
    if-eqz v1, :cond_2

    .line 824
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 829
    :cond_2
    :goto_3
    iget-object v2, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    .end local v10    # "tempInputStream":Ljava/io/FileInputStream;
    :goto_4
    move v2, v9

    .line 831
    goto :goto_1

    .line 825
    .restart local v10    # "tempInputStream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v7

    .line 826
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v2, "ThumbnailDocumentParser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "IOException : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 806
    .end local v1    # "inputStream":Ljava/io/FileInputStream;
    .end local v7    # "e":Ljava/io/IOException;
    .end local v10    # "tempInputStream":Ljava/io/FileInputStream;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    :catch_2
    move-exception v7

    move-object v1, v8

    .line 808
    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v1    # "inputStream":Ljava/io/FileInputStream;
    .local v7, "e":Lorg/apache/poi/EncryptedDocumentException;
    :goto_5
    :try_start_5
    new-instance v2, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-virtual {v7}, Lorg/apache/poi/EncryptedDocumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 822
    .end local v7    # "e":Lorg/apache/poi/EncryptedDocumentException;
    :catchall_0
    move-exception v2

    :goto_6
    if-eqz v1, :cond_3

    .line 824
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_9

    .line 829
    :cond_3
    :goto_7
    iget-object v3, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v3}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    throw v2

    .line 809
    .end local v1    # "inputStream":Ljava/io/FileInputStream;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    :catch_3
    move-exception v7

    move-object v1, v8

    .line 811
    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v1    # "inputStream":Ljava/io/FileInputStream;
    .local v7, "e":Lcom/samsung/index/PasswordProtectedDocException;
    :goto_8
    :try_start_7
    throw v7

    .line 812
    .end local v1    # "inputStream":Ljava/io/FileInputStream;
    .end local v7    # "e":Lcom/samsung/index/PasswordProtectedDocException;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    :catch_4
    move-exception v7

    move-object v1, v8

    .line 814
    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v1    # "inputStream":Ljava/io/FileInputStream;
    .local v7, "e":Ljava/lang/UnsupportedOperationException;
    :goto_9
    throw v7

    .line 815
    .end local v1    # "inputStream":Ljava/io/FileInputStream;
    .end local v7    # "e":Ljava/lang/UnsupportedOperationException;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    :catch_5
    move-exception v7

    move-object v1, v8

    .line 816
    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v1    # "inputStream":Ljava/io/FileInputStream;
    .local v7, "e":Ljava/io/FileNotFoundException;
    :goto_a
    const/4 v9, 0x0

    .line 817
    const-string/jumbo v2, "ThumbnailDocumentParser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception in XLSX: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v7}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 822
    if-eqz v1, :cond_4

    .line 824
    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 829
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    :cond_4
    :goto_b
    iget-object v2, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    goto :goto_4

    .line 825
    .restart local v7    # "e":Ljava/io/FileNotFoundException;
    :catch_6
    move-exception v7

    .line 826
    .local v7, "e":Ljava/io/IOException;
    const-string/jumbo v2, "ThumbnailDocumentParser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "IOException : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 818
    .end local v1    # "inputStream":Ljava/io/FileInputStream;
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    :catch_7
    move-exception v7

    move-object v1, v8

    .line 819
    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v1    # "inputStream":Ljava/io/FileInputStream;
    .local v7, "e":Ljava/lang/Exception;
    :goto_c
    const/4 v9, 0x0

    .line 820
    :try_start_9
    const-string/jumbo v2, "ThumbnailDocumentParser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception in XLSX: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 822
    if-eqz v1, :cond_5

    .line 824
    :try_start_a
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    .line 829
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_d
    iget-object v2, p0, Lcom/samsung/index/parser/ThumbnailDocumentParser;->folderName:Ljava/io/File;

    invoke-direct {p0, v2}, Lcom/samsung/index/parser/ThumbnailDocumentParser;->deleteTempFiles(Ljava/io/File;)Z

    goto/16 :goto_4

    .line 825
    .restart local v7    # "e":Ljava/lang/Exception;
    :catch_8
    move-exception v7

    .line 826
    .local v7, "e":Ljava/io/IOException;
    const-string/jumbo v2, "ThumbnailDocumentParser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "IOException : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 825
    .end local v7    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v7

    .line 826
    .restart local v7    # "e":Ljava/io/IOException;
    const-string/jumbo v3, "ThumbnailDocumentParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "IOException : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 822
    .end local v1    # "inputStream":Ljava/io/FileInputStream;
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v2

    move-object v1, v8

    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v1    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .line 818
    .restart local v10    # "tempInputStream":Ljava/io/FileInputStream;
    :catch_a
    move-exception v7

    goto :goto_c

    .line 815
    :catch_b
    move-exception v7

    goto/16 :goto_a

    .line 812
    :catch_c
    move-exception v7

    goto/16 :goto_9

    .line 809
    :catch_d
    move-exception v7

    goto/16 :goto_8

    .line 806
    :catch_e
    move-exception v7

    goto/16 :goto_5

    .end local v1    # "inputStream":Ljava/io/FileInputStream;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    :cond_6
    move-object v1, v8

    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v1    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_2
.end method
