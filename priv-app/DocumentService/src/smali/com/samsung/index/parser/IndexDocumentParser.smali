.class public Lcom/samsung/index/parser/IndexDocumentParser;
.super Ljava/lang/Object;
.source "IndexDocumentParser.java"


# static fields
.field public static final MAX_CHAR_COUNT:I = 0x100000

.field private static final TAG:Ljava/lang/String; = "IndexDocumentParser"

.field public static final TEXT_PARSER_LIMIT:I = 0x14


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private deleteZipDirectory(Ljava/io/File;)V
    .locals 8
    .param p1, "dataDir"    # Ljava/io/File;

    .prologue
    .line 971
    const/4 v2, 0x0

    .line 972
    .local v2, "files":[Ljava/io/File;
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v5

    if-nez v5, :cond_0

    .line 973
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 978
    :goto_0
    if-nez v2, :cond_1

    .line 1006
    :goto_1
    return-void

    .line 975
    :cond_0
    const/4 v5, 0x1

    new-array v3, v5, [Ljava/io/File;

    const/4 v5, 0x0

    aput-object p1, v3, v5

    .end local v2    # "files":[Ljava/io/File;
    .local v3, "files":[Ljava/io/File;
    move-object v2, v3

    .end local v3    # "files":[Ljava/io/File;
    .restart local v2    # "files":[Ljava/io/File;
    goto :goto_0

    .line 981
    :cond_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    array-length v5, v2

    if-ge v4, v5, :cond_3

    .line 982
    aget-object v1, v2, v4

    .line 983
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 984
    invoke-direct {p0, v1}, Lcom/samsung/index/parser/IndexDocumentParser;->deleteZipDirectory(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 986
    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 981
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 987
    :catch_0
    move-exception v0

    .line 988
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string/jumbo v5, "IndexDocumentParser"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 1003
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "file":Ljava/io/File;
    .end local v4    # "i":I
    :catch_1
    move-exception v0

    .line 1004
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string/jumbo v5, "IndexDocumentParser"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 992
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v4    # "i":I
    :cond_2
    :try_start_3
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    .line 993
    :catch_2
    move-exception v0

    .line 994
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_4
    const-string/jumbo v5, "IndexDocumentParser"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_3

    .line 999
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "file":Ljava/io/File;
    :cond_3
    :try_start_5
    invoke-virtual {p1}, Ljava/io/File;->delete()Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_1

    .line 1000
    :catch_3
    move-exception v0

    .line 1001
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_6
    const-string/jumbo v5, "IndexDocumentParser"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_1
.end method

.method private iterateZipDirectory(Ljava/io/File;Lcom/samsung/index/ITextContentObs;Landroid/content/Context;)V
    .locals 10
    .param p1, "dataDir"    # Ljava/io/File;
    .param p2, "indexWriter"    # Lcom/samsung/index/ITextContentObs;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 925
    const/4 v0, 0x0

    .line 926
    .local v0, "contentString":Ljava/lang/String;
    const/4 v4, 0x0

    .line 927
    .local v4, "files":[Ljava/io/File;
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v7

    if-nez v7, :cond_1

    .line 928
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 933
    :goto_0
    if-nez v4, :cond_2

    .line 961
    :cond_0
    :goto_1
    return-void

    .line 930
    :cond_1
    const/4 v7, 0x1

    new-array v5, v7, [Ljava/io/File;

    const/4 v7, 0x0

    aput-object p1, v5, v7

    .end local v4    # "files":[Ljava/io/File;
    .local v5, "files":[Ljava/io/File;
    move-object v4, v5

    .end local v5    # "files":[Ljava/io/File;
    .restart local v4    # "files":[Ljava/io/File;
    goto :goto_0

    .line 936
    :cond_2
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    array-length v7, v4

    if-ge v6, v7, :cond_0

    .line 937
    aget-object v2, v4, v6

    .line 938
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 939
    invoke-direct {p0, v2, p2, p3}, Lcom/samsung/index/parser/IndexDocumentParser;->iterateZipDirectory(Ljava/io/File;Lcom/samsung/index/ITextContentObs;Landroid/content/Context;)V

    .line 936
    :cond_3
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 941
    :cond_4
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 942
    .local v3, "fileName":Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/index/Utils;->isSupported(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v7

    if-eqz v7, :cond_3

    .line 944
    :try_start_1
    invoke-virtual {p0, v2, p3, p2}, Lcom/samsung/index/parser/IndexDocumentParser;->parse(Ljava/io/File;Landroid/content/Context;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;

    move-result-object v0

    .line 945
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_3

    .line 947
    invoke-interface {p2, v0}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 948
    const/4 v0, 0x0

    goto :goto_3

    .line 950
    :catch_0
    move-exception v1

    .line 951
    .local v1, "e":Ljava/lang/UnsupportedOperationException;
    :try_start_2
    const-string/jumbo v7, "IndexDocumentParser"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "UnsupportedOperationException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/UnsupportedOperationException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 958
    .end local v1    # "e":Ljava/lang/UnsupportedOperationException;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fileName":Ljava/lang/String;
    .end local v6    # "i":I
    :catch_1
    move-exception v1

    .line 959
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v7, "IndexDocumentParser"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 952
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "fileName":Ljava/lang/String;
    .restart local v6    # "i":I
    :catch_2
    move-exception v1

    .line 953
    .local v1, "e":Ljava/io/IOException;
    :try_start_3
    const-string/jumbo v7, "IndexDocumentParser"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_3
.end method

.method private processRTF(Ljava/io/InputStream;Lorg/apache/tika/sax/ToTextContentHandler;Lorg/apache/tika/metadata/Metadata;)Ljava/lang/String;
    .locals 3
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "toTextContentHandler"    # Lorg/apache/tika/sax/ToTextContentHandler;
    .param p3, "metadata"    # Lorg/apache/tika/metadata/Metadata;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 830
    new-instance v1, Lorg/apache/tika/parser/rtf/RTFParser;

    invoke-direct {v1}, Lorg/apache/tika/parser/rtf/RTFParser;-><init>()V

    .line 831
    .local v1, "p":Lorg/apache/tika/parser/Parser;
    new-instance v0, Lorg/apache/tika/sax/BodyContentHandler;

    invoke-direct {v0, p2}, Lorg/apache/tika/sax/BodyContentHandler;-><init>(Lorg/apache/tika/sax/ToTextContentHandler;)V

    .line 832
    .local v0, "handler":Lorg/xml/sax/ContentHandler;
    const/4 v2, 0x0

    invoke-interface {v1, p1, v0, p3, v2}, Lorg/apache/tika/parser/Parser;->parse(Ljava/io/InputStream;Lorg/xml/sax/ContentHandler;Lorg/apache/tika/metadata/Metadata;Lorg/apache/tika/parser/ParseContext;)V

    .line 833
    invoke-virtual {p2}, Lorg/apache/tika/sax/ToTextContentHandler;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public parse(Ljava/io/File;Landroid/content/Context;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    .locals 6
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "indexWriter"    # Lcom/samsung/index/ITextContentObs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 74
    const/4 v0, 0x0

    .line 76
    .local v0, "contents":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    .line 77
    .local v2, "fileName":Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/index/Utils;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 80
    .local v1, "extension":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 81
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v4, "File does not have proper extension"

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 85
    :cond_0
    const-string/jumbo v3, ".txt"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, ".xml"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 87
    :cond_1
    invoke-virtual {p0, p1, p3}, Lcom/samsung/index/parser/IndexDocumentParser;->readTXT(Ljava/io/File;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;

    move-result-object v0

    .line 208
    :goto_0
    return-object v0

    .line 93
    :cond_2
    const-string/jumbo v3, ".docx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string/jumbo v3, ".docm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string/jumbo v3, ".dotm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string/jumbo v3, ".dotx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 97
    :cond_3
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, p3}, Lcom/samsung/index/parser/IndexDocumentParser;->readDOCX(Ljava/lang/String;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 99
    :cond_4
    const-string/jumbo v3, ".xlsx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v3, ".xlsm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v3, ".xltm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v3, ".xltx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 103
    :cond_5
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, p3}, Lcom/samsung/index/parser/IndexDocumentParser;->readXLSX(Ljava/lang/String;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 105
    :cond_6
    const-string/jumbo v3, ".pptx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".potm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".potx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".ppam"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".ppsm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".ppsx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".pptm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".sldm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, ".sldx"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 114
    :cond_7
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, p3}, Lcom/samsung/index/parser/IndexDocumentParser;->readPPTX(Ljava/lang/String;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 116
    :cond_8
    const-string/jumbo v3, ".doc"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, ".dot"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 118
    :cond_9
    invoke-virtual {p0, p1, p3}, Lcom/samsung/index/parser/IndexDocumentParser;->readDOC(Ljava/io/File;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 120
    :cond_a
    const-string/jumbo v3, ".ppt"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string/jumbo v3, ".pot"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string/jumbo v3, ".pps"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 123
    :cond_b
    invoke-virtual {p0, p1, p3}, Lcom/samsung/index/parser/IndexDocumentParser;->readPPT(Ljava/io/File;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 125
    :cond_c
    const-string/jumbo v3, ".xls"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_d

    const-string/jumbo v3, ".xlm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_d

    const-string/jumbo v3, ".xlt"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 128
    :cond_d
    invoke-virtual {p0, p1, p3}, Lcom/samsung/index/parser/IndexDocumentParser;->readXLS(Ljava/io/File;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 130
    :cond_e
    const-string/jumbo v3, ".pdf"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 131
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, p3}, Lcom/samsung/index/parser/IndexDocumentParser;->readPDF(Ljava/lang/String;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 133
    :cond_f
    const-string/jumbo v3, ".rtf"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 134
    invoke-virtual {p0, p1, p3}, Lcom/samsung/index/parser/IndexDocumentParser;->readRTF(Ljava/io/File;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 137
    :cond_10
    const-string/jumbo v3, ".snb"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 138
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, p3}, Lcom/samsung/index/parser/IndexDocumentParser;->readSNB(Ljava/lang/String;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 198
    :cond_11
    const-string/jumbo v3, ".zip"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 199
    invoke-virtual {p0, p1, p3, p2}, Lcom/samsung/index/parser/IndexDocumentParser;->readZIP(Ljava/io/File;Lcom/samsung/index/ITextContentObs;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 200
    :cond_12
    const-string/jumbo v3, ".csv"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 201
    invoke-virtual {p0, p1, p3}, Lcom/samsung/index/parser/IndexDocumentParser;->readCSV(Ljava/io/File;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 202
    :cond_13
    const-string/jumbo v3, ".asc"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 203
    invoke-virtual {p0, p1, p3}, Lcom/samsung/index/parser/IndexDocumentParser;->readTXT(Ljava/io/File;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 205
    :cond_14
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " is not supported"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public readCSV(Ljava/io/File;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    .locals 21
    .param p1, "file"    # Ljava/io/File;
    .param p2, "indexWriter"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    .line 1011
    const/16 v2, 0x400

    .line 1013
    .local v2, "MAX_CHARS_TO_READ":I
    new-instance v16, Ljava/lang/StringBuilder;

    const/16 v17, 0x400

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1014
    .local v16, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .line 1015
    .local v4, "br":Ljava/io/BufferedReader;
    const/16 v3, 0x2c

    .line 1016
    .local v3, "SPLIT_BY_CHAR":C
    const/4 v10, 0x0

    .line 1018
    .local v10, "fis":Ljava/io/FileInputStream;
    const/4 v9, 0x1

    .line 1021
    .local v9, "encryptionCheck":Z
    :try_start_0
    new-instance v11, Ljava/io/FileInputStream;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1022
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .local v11, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v12, Ljava/io/InputStreamReader;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/index/Utils;->getCharsetFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v12, v11, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 1026
    .local v12, "fr":Ljava/io/InputStreamReader;
    new-instance v5, Ljava/io/BufferedReader;

    invoke-direct {v5, v12}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1028
    .end local v4    # "br":Ljava/io/BufferedReader;
    .local v5, "br":Ljava/io/BufferedReader;
    :try_start_2
    const-string/jumbo v14, ""

    .line 1030
    .local v14, "lastWord":Ljava/lang/String;
    const/16 v17, 0x400

    move/from16 v0, v17

    new-array v7, v0, [C

    .line 1031
    .local v7, "dataArr":[C
    const/4 v6, 0x0

    .line 1032
    .local v6, "currentLen":I
    const/4 v13, 0x0

    .line 1035
    .local v13, "iter":I
    :cond_0
    const/16 v17, 0x0

    const/16 v18, 0x400

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v7, v0, v1}, Ljava/io/BufferedReader;->read([CII)I

    move-result v6

    if-lez v6, :cond_7

    .line 1038
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    add-int v17, v17, v6

    const/high16 v18, 0x100000

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_2

    .line 1039
    const/4 v15, 0x0

    .line 1040
    .local v15, "spaceIndex":I
    const/16 v17, 0x20

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v15

    .line 1041
    if-lez v15, :cond_1

    .line 1042
    add-int/lit8 v17, v15, 0x1

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 1043
    add-int/lit8 v17, v15, 0x1

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1045
    :cond_1
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 1046
    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1047
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1051
    .end local v15    # "spaceIndex":I
    :cond_2
    const/4 v13, 0x0

    :goto_0
    if-ge v13, v6, :cond_4

    .line 1052
    const/16 v17, 0x2c

    aget-char v18, v7, v13

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 1053
    const/16 v17, 0x20

    aput-char v17, v7, v13

    .line 1051
    :cond_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 1055
    :cond_4
    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v7, v1, v6}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 1057
    if-eqz v9, :cond_0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    const-string/jumbo v18, "<##"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 1060
    new-instance v17, Lcom/samsung/index/GeneralEncryptedDocException;

    const-string/jumbo v18, "Document is encrypted"

    invoke-direct/range {v17 .. v18}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v17
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1064
    .end local v6    # "currentLen":I
    .end local v7    # "dataArr":[C
    .end local v13    # "iter":I
    .end local v14    # "lastWord":Ljava/lang/String;
    :catch_0
    move-exception v8

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    move-object v4, v5

    .line 1065
    .end local v5    # "br":Ljava/io/BufferedReader;
    .end local v12    # "fr":Ljava/io/InputStreamReader;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    .local v8, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    const-string/jumbo v17, "IndexDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "FileNotFoundException: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1070
    if-eqz v10, :cond_5

    .line 1072
    :try_start_4
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1077
    .end local v8    # "e":Ljava/io/FileNotFoundException;
    :cond_5
    :goto_2
    if-eqz v4, :cond_6

    .line 1079
    :try_start_5
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 1085
    :cond_6
    :goto_3
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    return-object v17

    .line 1070
    .end local v4    # "br":Ljava/io/BufferedReader;
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "br":Ljava/io/BufferedReader;
    .restart local v6    # "currentLen":I
    .restart local v7    # "dataArr":[C
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fr":Ljava/io/InputStreamReader;
    .restart local v13    # "iter":I
    .restart local v14    # "lastWord":Ljava/lang/String;
    :cond_7
    if-eqz v11, :cond_8

    .line 1072
    :try_start_6
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 1077
    :cond_8
    :goto_4
    if-eqz v5, :cond_c

    .line 1079
    :try_start_7
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    move-object v4, v5

    .line 1082
    .end local v5    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    goto :goto_3

    .line 1073
    .end local v4    # "br":Ljava/io/BufferedReader;
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "br":Ljava/io/BufferedReader;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v8

    .line 1074
    .local v8, "e":Ljava/io/IOException;
    const-string/jumbo v17, "IndexDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "IOException: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 1080
    .end local v8    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v8

    .line 1081
    .restart local v8    # "e":Ljava/io/IOException;
    const-string/jumbo v17, "IndexDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "IOException: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    move-object v4, v5

    .line 1082
    .end local v5    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    goto :goto_3

    .line 1073
    .end local v6    # "currentLen":I
    .end local v7    # "dataArr":[C
    .end local v12    # "fr":Ljava/io/InputStreamReader;
    .end local v13    # "iter":I
    .end local v14    # "lastWord":Ljava/lang/String;
    .local v8, "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v8

    .line 1074
    .local v8, "e":Ljava/io/IOException;
    const-string/jumbo v17, "IndexDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "IOException: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1080
    .end local v8    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v8

    .line 1081
    .restart local v8    # "e":Ljava/io/IOException;
    const-string/jumbo v17, "IndexDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "IOException: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1066
    .end local v8    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v8

    .line 1067
    .restart local v8    # "e":Ljava/io/IOException;
    :goto_5
    :try_start_8
    const-string/jumbo v17, "IndexDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "IOException: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1070
    if-eqz v10, :cond_9

    .line 1072
    :try_start_9
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 1077
    :cond_9
    :goto_6
    if-eqz v4, :cond_6

    .line 1079
    :try_start_a
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto/16 :goto_3

    .line 1080
    :catch_6
    move-exception v8

    .line 1081
    const-string/jumbo v17, "IndexDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "IOException: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1073
    :catch_7
    move-exception v8

    .line 1074
    const-string/jumbo v17, "IndexDocumentParser"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "IOException: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 1070
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v17

    :goto_7
    if-eqz v10, :cond_a

    .line 1072
    :try_start_b
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    .line 1077
    :cond_a
    :goto_8
    if-eqz v4, :cond_b

    .line 1079
    :try_start_c
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 1082
    :cond_b
    :goto_9
    throw v17

    .line 1073
    :catch_8
    move-exception v8

    .line 1074
    .restart local v8    # "e":Ljava/io/IOException;
    const-string/jumbo v18, "IndexDocumentParser"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "IOException: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 1080
    .end local v8    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v8

    .line 1081
    .restart local v8    # "e":Ljava/io/IOException;
    const-string/jumbo v18, "IndexDocumentParser"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "IOException: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 1070
    .end local v8    # "e":Ljava/io/IOException;
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v17

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto :goto_7

    .end local v4    # "br":Ljava/io/BufferedReader;
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "br":Ljava/io/BufferedReader;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fr":Ljava/io/InputStreamReader;
    :catchall_2
    move-exception v17

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    move-object v4, v5

    .end local v5    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    goto :goto_7

    .line 1066
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .end local v12    # "fr":Ljava/io/InputStreamReader;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catch_a
    move-exception v8

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_5

    .end local v4    # "br":Ljava/io/BufferedReader;
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "br":Ljava/io/BufferedReader;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fr":Ljava/io/InputStreamReader;
    :catch_b
    move-exception v8

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    move-object v4, v5

    .end local v5    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_5

    .line 1064
    .end local v12    # "fr":Ljava/io/InputStreamReader;
    :catch_c
    move-exception v8

    goto/16 :goto_1

    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    :catch_d
    move-exception v8

    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_1

    .end local v4    # "br":Ljava/io/BufferedReader;
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "br":Ljava/io/BufferedReader;
    .restart local v6    # "currentLen":I
    .restart local v7    # "dataArr":[C
    .restart local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "fr":Ljava/io/InputStreamReader;
    .restart local v13    # "iter":I
    .restart local v14    # "lastWord":Ljava/lang/String;
    :cond_c
    move-object v10, v11

    .end local v11    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    move-object v4, v5

    .end local v5    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_3
.end method

.method public readDOC(Ljava/io/File;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    .locals 10
    .param p1, "file"    # Ljava/io/File;
    .param p2, "indexWriter"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    const/4 v9, 0x1

    .line 461
    :try_start_0
    new-instance v3, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v3, p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/File;)V

    .line 465
    .local v3, "fileSystem":Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    new-instance v0, Lorg/apache/index/poi/hwpf/HWPFDocument;

    invoke-direct {v0, p2, v3}, Lorg/apache/index/poi/hwpf/HWPFDocument;-><init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 466
    .local v0, "doc":Lorg/apache/index/poi/hwpf/HWPFDocument;
    invoke-virtual {v0}, Lorg/apache/index/poi/hwpf/HWPFDocument;->getText()Ljava/lang/String;

    move-result-object v5

    .line 470
    .local v5, "text":Ljava/lang/String;
    invoke-virtual {v3}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->closePOIFileHandler()V
    :try_end_0
    .catch Lorg/apache/index/poi/EncryptedDocumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/apache/index/poi/poifs/filesystem/OfficeXmlFileException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    .line 502
    .end local v0    # "doc":Lorg/apache/index/poi/hwpf/HWPFDocument;
    .end local v3    # "fileSystem":Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .end local v5    # "text":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 472
    :catch_0
    move-exception v1

    .line 474
    .local v1, "e":Lorg/apache/index/poi/EncryptedDocumentException;
    new-instance v6, Lcom/samsung/index/PasswordProtectedDocException;

    invoke-virtual {v1}, Lorg/apache/index/poi/EncryptedDocumentException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/samsung/index/PasswordProtectedDocException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 475
    .end local v1    # "e":Lorg/apache/index/poi/EncryptedDocumentException;
    :catch_1
    move-exception v1

    .line 476
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    .line 477
    .local v4, "msg":Ljava/lang/String;
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    if-eqz v4, :cond_0

    const-string/jumbo v6, "Invalid header signature"

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 480
    new-instance v6, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-direct {v6, v4}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 482
    :cond_0
    invoke-interface {p2, v9}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    .line 483
    const-string/jumbo v5, ""

    goto :goto_0

    .line 484
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "msg":Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 486
    .local v1, "e":Lcom/samsung/index/GeneralEncryptedDocException;
    throw v1

    .line 487
    .end local v1    # "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :catch_3
    move-exception v1

    .line 488
    .local v1, "e":Lorg/apache/index/poi/poifs/filesystem/OfficeXmlFileException;
    const/4 v6, 0x0

    invoke-interface {p2, v6}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    .line 489
    const-string/jumbo v5, ""

    .line 491
    .restart local v5    # "text":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6, p2}, Lcom/samsung/index/parser/IndexDocumentParser;->readDOCX(Ljava/lang/String;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v5

    goto :goto_0

    .line 492
    :catch_4
    move-exception v2

    .line 493
    .local v2, "e1":Ljava/lang/Exception;
    invoke-interface {p2, v9}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    .line 494
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception in reading DOCX: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 498
    .end local v1    # "e":Lorg/apache/index/poi/poifs/filesystem/OfficeXmlFileException;
    .end local v2    # "e1":Ljava/lang/Exception;
    .end local v5    # "text":Ljava/lang/String;
    :catch_5
    move-exception v1

    .line 500
    .local v1, "e":Ljava/lang/Exception;
    invoke-interface {p2, v9}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    .line 501
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception in reading DOC: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    const-string/jumbo v5, ""

    goto/16 :goto_0
.end method

.method public readDOCX(Ljava/lang/String;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    .locals 12
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "indexWriter"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    .line 517
    const-string/jumbo v3, ""

    .line 519
    .local v3, "text":Ljava/lang/String;
    const/4 v6, 0x0

    .line 526
    .local v6, "zipInput":Ljava/util/zip/ZipInputStream;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 528
    .local v1, "file":Ljava/io/File;
    :try_start_0
    invoke-static {v1}, Lcom/samsung/thumbnail/util/Utils;->isValid(Ljava/io/File;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 529
    const/4 v8, 0x1

    invoke-interface {p2, v8}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V
    :try_end_0
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 551
    if-eqz v6, :cond_0

    .line 553
    :try_start_1
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 554
    const/4 v6, 0x0

    :cond_0
    :goto_0
    move-object v4, v3

    .line 560
    .end local v3    # "text":Ljava/lang/String;
    .local v4, "text":Ljava/lang/String;
    :goto_1
    return-object v4

    .line 555
    .end local v4    # "text":Ljava/lang/String;
    .restart local v3    # "text":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 556
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v8, "IndexDocumentParser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "IOException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 533
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_2
    new-instance v7, Ljava/util/zip/ZipInputStream;

    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v7, v8}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 535
    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .local v7, "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_2
    :try_start_3
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v5

    .local v5, "zipEntry":Ljava/util/zip/ZipEntry;
    if-eqz v5, :cond_3

    .line 536
    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v2

    .line 538
    .local v2, "name":Ljava/lang/String;
    const-string/jumbo v8, "word/document.xml"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 539
    const-string/jumbo v8, ".docx"

    invoke-static {p2, v7, v8}, Lcom/samsung/index/parser/ooxml/OOXMLSaxParser;->parse(Lcom/samsung/index/ITextContentObs;Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v3

    .line 551
    .end local v2    # "name":Ljava/lang/String;
    :cond_3
    if-eqz v7, :cond_6

    .line 553
    :try_start_4
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 554
    const/4 v6, 0x0

    .end local v5    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_4
    :goto_2
    move-object v4, v3

    .line 560
    .end local v3    # "text":Ljava/lang/String;
    .restart local v4    # "text":Ljava/lang/String;
    goto :goto_1

    .line 555
    .end local v4    # "text":Ljava/lang/String;
    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v3    # "text":Ljava/lang/String;
    .restart local v5    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_1
    move-exception v0

    .line 556
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v8, "IndexDocumentParser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "IOException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    .line 557
    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto :goto_2

    .line 544
    .end local v0    # "e":Ljava/io/IOException;
    .end local v5    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_2
    move-exception v0

    .line 546
    .local v0, "e":Lorg/apache/poi/EncryptedDocumentException;
    :goto_3
    :try_start_5
    new-instance v8, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-virtual {v0}, Lorg/apache/poi/EncryptedDocumentException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 551
    .end local v0    # "e":Lorg/apache/poi/EncryptedDocumentException;
    :catchall_0
    move-exception v8

    :goto_4
    if-eqz v6, :cond_5

    .line 553
    :try_start_6
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 554
    const/4 v6, 0x0

    .line 557
    :cond_5
    :goto_5
    throw v8

    .line 547
    :catch_3
    move-exception v0

    .line 548
    .local v0, "e":Ljava/io/IOException;
    :goto_6
    :try_start_7
    const-string/jumbo v8, "IndexDocumentParser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "IOException: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    const-string/jumbo v3, ""
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 551
    if-eqz v6, :cond_4

    .line 553
    :try_start_8
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 554
    const/4 v6, 0x0

    goto :goto_2

    .line 555
    :catch_4
    move-exception v0

    .line 556
    const-string/jumbo v8, "IndexDocumentParser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "IOException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 555
    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 556
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v9, "IndexDocumentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "IOException : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 551
    .end local v0    # "e":Ljava/io/IOException;
    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catchall_1
    move-exception v8

    move-object v6, v7

    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto :goto_4

    .line 547
    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_6
    move-exception v0

    move-object v6, v7

    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto :goto_6

    .line 544
    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_7
    move-exception v0

    move-object v6, v7

    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto/16 :goto_3

    .end local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v5    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_6
    move-object v6, v7

    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v6    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto/16 :goto_2
.end method

.method public readPDF(Ljava/lang/String;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "indexWriter"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    const/high16 v9, 0x100000

    const/4 v10, 0x1

    .line 732
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 733
    .local v6, "text":Ljava/lang/StringBuilder;
    new-instance v1, Lorg/icepdf/index/core/pobjects/Document;

    invoke-direct {v1}, Lorg/icepdf/index/core/pobjects/Document;-><init>()V

    .line 735
    .local v1, "document":Lorg/icepdf/index/core/pobjects/Document;
    :try_start_0
    invoke-virtual {v1, p1}, Lorg/icepdf/index/core/pobjects/Document;->setFile(Ljava/lang/String;)V

    .line 736
    const/4 v4, 0x0

    .line 737
    .local v4, "pagNumber":I
    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/Document;->getNumberOfPages()I

    move-result v0

    .line 738
    .local v0, "count":I
    if-nez v0, :cond_1

    .line 739
    new-instance v7, Lorg/apache/poi/EncryptedDocumentException;

    const-string/jumbo v8, "Encrypted Document Exception"

    invoke-direct {v7, v8}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Lorg/icepdf/index/core/exceptions/PDFSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 764
    .end local v0    # "count":I
    .end local v4    # "pagNumber":I
    :catch_0
    move-exception v2

    .line 765
    .local v2, "e":Lorg/icepdf/index/core/exceptions/PDFSecurityException;
    :try_start_1
    new-instance v7, Lcom/samsung/index/PasswordProtectedDocException;

    invoke-virtual {v2}, Lorg/icepdf/index/core/exceptions/PDFSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/index/PasswordProtectedDocException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 772
    .end local v2    # "e":Lorg/icepdf/index/core/exceptions/PDFSecurityException;
    :catchall_0
    move-exception v7

    if-eqz v1, :cond_0

    .line 773
    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/Document;->dispose()V

    .line 774
    const/4 v1, 0x0

    :cond_0
    throw v7

    .line 742
    .restart local v0    # "count":I
    .restart local v4    # "pagNumber":I
    :cond_1
    :goto_0
    if-ge v4, v0, :cond_4

    .line 744
    :try_start_2
    invoke-virtual {v1, v4}, Lorg/icepdf/index/core/pobjects/Document;->getPageText(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 745
    .local v5, "pageText":Ljava/lang/StringBuilder;
    if-eqz v5, :cond_3

    .line 747
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    add-int/2addr v7, v8

    if-le v7, v9, :cond_2

    .line 748
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p2, v7}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 749
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 751
    :cond_2
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 752
    const/16 v7, 0x20

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 754
    :cond_3
    add-int/lit8 v4, v4, 0x1

    .line 757
    rem-int/lit8 v7, v4, 0x32

    if-nez v7, :cond_1

    .line 760
    const-string/jumbo v7, "readPDF"

    invoke-static {v7}, Lcom/samsung/index/indexservice/service/SystemInfo;->checkTemperatureLevel(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/icepdf/index/core/exceptions/PDFSecurityException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 766
    .end local v0    # "count":I
    .end local v4    # "pagNumber":I
    .end local v5    # "pageText":Ljava/lang/StringBuilder;
    :catch_1
    move-exception v2

    .line 767
    .local v2, "e":Lorg/apache/poi/EncryptedDocumentException;
    :try_start_3
    new-instance v7, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-virtual {v2}, Lorg/apache/poi/EncryptedDocumentException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 772
    .end local v2    # "e":Lorg/apache/poi/EncryptedDocumentException;
    .restart local v0    # "count":I
    .restart local v4    # "pagNumber":I
    :cond_4
    if-eqz v1, :cond_5

    .line 773
    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/Document;->dispose()V

    .line 774
    const/4 v1, 0x0

    .line 780
    .end local v0    # "count":I
    .end local v4    # "pagNumber":I
    :cond_5
    :goto_1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-nez v7, :cond_6

    .line 781
    invoke-interface {p2, v10}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    .line 783
    :cond_6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 768
    :catch_2
    move-exception v3

    .line 769
    .local v3, "ex":Ljava/lang/Exception;
    const/4 v7, 0x1

    :try_start_4
    invoke-interface {p2, v7}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    .line 770
    const-string/jumbo v7, "IndexDocumentParser"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "ERROR: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 772
    if-eqz v1, :cond_5

    .line 773
    invoke-virtual {v1}, Lorg/icepdf/index/core/pobjects/Document;->dispose()V

    .line 774
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public readPPT(Ljava/io/File;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    .locals 10
    .param p1, "file"    # Ljava/io/File;
    .param p2, "indexWriter"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    const/4 v9, 0x1

    .line 397
    :try_start_0
    new-instance v2, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v2, p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/File;)V

    .line 401
    .local v2, "fileSystem":Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    new-instance v4, Lorg/apache/index/poi/hslf/HSLFSlideShow;

    invoke-direct {v4, p2, v2}, Lorg/apache/index/poi/hslf/HSLFSlideShow;-><init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 402
    .local v4, "slideShow":Lorg/apache/index/poi/hslf/HSLFSlideShow;
    invoke-virtual {v4}, Lorg/apache/index/poi/hslf/HSLFSlideShow;->getText()Ljava/lang/String;

    move-result-object v5

    .line 406
    .local v5, "text":Ljava/lang/String;
    invoke-virtual {v2}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->closePOIFileHandler()V
    :try_end_0
    .catch Lorg/apache/index/poi/hslf/exceptions/EncryptedPowerPointFileException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/apache/index/poi/poifs/filesystem/OfficeXmlFileException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    .line 439
    .end local v2    # "fileSystem":Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .end local v4    # "slideShow":Lorg/apache/index/poi/hslf/HSLFSlideShow;
    .end local v5    # "text":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 409
    :catch_0
    move-exception v0

    .line 411
    .local v0, "e":Lorg/apache/index/poi/hslf/exceptions/EncryptedPowerPointFileException;
    new-instance v6, Lcom/samsung/index/PasswordProtectedDocException;

    invoke-virtual {v0}, Lorg/apache/index/poi/hslf/exceptions/EncryptedPowerPointFileException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/samsung/index/PasswordProtectedDocException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 412
    .end local v0    # "e":Lorg/apache/index/poi/hslf/exceptions/EncryptedPowerPointFileException;
    :catch_1
    move-exception v0

    .line 413
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 414
    .local v3, "msg":Ljava/lang/String;
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    if-eqz v3, :cond_0

    const-string/jumbo v6, "Invalid header signature"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 417
    new-instance v6, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-direct {v6, v3}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 419
    :cond_0
    invoke-interface {p2, v9}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    .line 420
    const-string/jumbo v5, ""

    goto :goto_0

    .line 421
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "msg":Ljava/lang/String;
    :catch_2
    move-exception v0

    .line 423
    .local v0, "e":Lcom/samsung/index/GeneralEncryptedDocException;
    throw v0

    .line 424
    .end local v0    # "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :catch_3
    move-exception v0

    .line 425
    .local v0, "e":Lorg/apache/index/poi/poifs/filesystem/OfficeXmlFileException;
    const/4 v6, 0x0

    invoke-interface {p2, v6}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    .line 426
    const-string/jumbo v5, ""

    .line 428
    .restart local v5    # "text":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6, p2}, Lcom/samsung/index/parser/IndexDocumentParser;->readPPTX(Ljava/lang/String;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v5

    goto :goto_0

    .line 429
    :catch_4
    move-exception v1

    .line 430
    .local v1, "e1":Ljava/lang/Exception;
    invoke-interface {p2, v9}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    .line 431
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception in reading PPTX: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 435
    .end local v0    # "e":Lorg/apache/index/poi/poifs/filesystem/OfficeXmlFileException;
    .end local v1    # "e1":Ljava/lang/Exception;
    .end local v5    # "text":Ljava/lang/String;
    :catch_5
    move-exception v0

    .line 437
    .local v0, "e":Ljava/lang/Exception;
    invoke-interface {p2, v9}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    .line 438
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception in reading PPT: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    const-string/jumbo v5, ""

    goto/16 :goto_0
.end method

.method public readPPTX(Ljava/lang/String;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    .locals 13
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "indexWriter"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    const/high16 v10, 0x100000

    .line 658
    const-string/jumbo v5, ""

    .line 660
    .local v5, "text":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 661
    .local v4, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .line 669
    .local v7, "zipInput":Ljava/util/zip/ZipInputStream;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 672
    .local v1, "file":Ljava/io/File;
    :try_start_0
    invoke-static {v1}, Lcom/samsung/thumbnail/util/Utils;->isValid(Ljava/io/File;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 673
    const/4 v9, 0x1

    invoke-interface {p2, v9}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V
    :try_end_0
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 705
    if-eqz v7, :cond_0

    .line 707
    :try_start_1
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 708
    const/4 v7, 0x0

    :cond_0
    :goto_0
    move-object v9, v5

    .line 714
    :goto_1
    return-object v9

    .line 709
    :catch_0
    move-exception v0

    .line 710
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v9, "IndexDocumentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "ERROR: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 677
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_2
    new-instance v8, Ljava/util/zip/ZipInputStream;

    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v8, v9}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 679
    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .local v8, "zipInput":Ljava/util/zip/ZipInputStream;
    :try_start_3
    new-instance v3, Lcom/samsung/index/parser/ooxml/OOXMLPullParser;

    invoke-direct {v3, p2}, Lcom/samsung/index/parser/ooxml/OOXMLPullParser;-><init>(Lcom/samsung/index/ITextContentObs;)V

    .line 681
    .local v3, "pullParser":Lcom/samsung/index/parser/ooxml/OOXMLPullParser;
    :goto_2
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v6

    .local v6, "zipEntry":Ljava/util/zip/ZipEntry;
    if-eqz v6, :cond_4

    .line 682
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v2

    .line 685
    .local v2, "name":Ljava/lang/String;
    const-string/jumbo v9, "ppt/slides/slide"

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 686
    invoke-virtual {v3, v8}, Lcom/samsung/index/parser/ooxml/OOXMLPullParser;->parse(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v5

    .line 688
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 689
    const-string/jumbo v9, ""

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 691
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    if-lt v9, v10, :cond_2

    .line 692
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {p2, v9}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 693
    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 696
    :cond_2
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->closeEntry()V
    :try_end_3
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 698
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "pullParser":Lcom/samsung/index/parser/ooxml/OOXMLPullParser;
    .end local v6    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_1
    move-exception v0

    move-object v7, v8

    .line 700
    .end local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    .local v0, "e":Lorg/apache/poi/EncryptedDocumentException;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    :goto_3
    :try_start_4
    new-instance v9, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-virtual {v0}, Lorg/apache/poi/EncryptedDocumentException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 705
    .end local v0    # "e":Lorg/apache/poi/EncryptedDocumentException;
    :catchall_0
    move-exception v9

    :goto_4
    if-eqz v7, :cond_3

    .line 707
    :try_start_5
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 708
    const/4 v7, 0x0

    .line 711
    :cond_3
    :goto_5
    throw v9

    .line 705
    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v3    # "pullParser":Lcom/samsung/index/parser/ooxml/OOXMLPullParser;
    .restart local v6    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_4
    if-eqz v8, :cond_6

    .line 707
    :try_start_6
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 708
    const/4 v7, 0x0

    .line 714
    .end local v3    # "pullParser":Lcom/samsung/index/parser/ooxml/OOXMLPullParser;
    .end local v6    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_5
    :goto_6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_1

    .line 709
    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v3    # "pullParser":Lcom/samsung/index/parser/ooxml/OOXMLPullParser;
    .restart local v6    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_2
    move-exception v0

    .line 710
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v9, "IndexDocumentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "ERROR: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v7, v8

    .line 711
    .end local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto :goto_6

    .line 701
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "pullParser":Lcom/samsung/index/parser/ooxml/OOXMLPullParser;
    .end local v6    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_3
    move-exception v0

    .line 702
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_7
    :try_start_7
    const-string/jumbo v9, "IndexDocumentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    const-string/jumbo v5, ""
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 705
    if-eqz v7, :cond_5

    .line 707
    :try_start_8
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 708
    const/4 v7, 0x0

    goto :goto_6

    .line 709
    :catch_4
    move-exception v0

    .line 710
    const-string/jumbo v9, "IndexDocumentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "ERROR: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 709
    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 710
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v10, "IndexDocumentParser"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "ERROR: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 705
    .end local v0    # "e":Ljava/io/IOException;
    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catchall_1
    move-exception v9

    move-object v7, v8

    .end local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto/16 :goto_4

    .line 701
    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_6
    move-exception v0

    move-object v7, v8

    .end local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto :goto_7

    .line 698
    :catch_7
    move-exception v0

    goto/16 :goto_3

    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v3    # "pullParser":Lcom/samsung/index/parser/ooxml/OOXMLPullParser;
    .restart local v6    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_6
    move-object v7, v8

    .end local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto/16 :goto_6
.end method

.method public readRTF(Ljava/io/File;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    .locals 10
    .param p1, "file"    # Ljava/io/File;
    .param p2, "indexWriter"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    .line 798
    const/4 v1, 0x0

    .line 799
    .local v1, "input":Ljava/io/InputStream;
    new-instance v2, Ljava/lang/StringBuffer;

    const/high16 v6, 0x100000

    invoke-direct {v2, v6}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 800
    .local v2, "luceneRTFStringBuffer":Ljava/lang/StringBuffer;
    new-instance v3, Lorg/apache/tika/metadata/Metadata;

    invoke-direct {v3}, Lorg/apache/tika/metadata/Metadata;-><init>()V

    .line 801
    .local v3, "metadata":Lorg/apache/tika/metadata/Metadata;
    new-instance v5, Lorg/apache/tika/sax/ToTextContentHandler;

    invoke-direct {v5, v2, p2}, Lorg/apache/tika/sax/ToTextContentHandler;-><init>(Ljava/lang/StringBuffer;Lcom/samsung/index/ITextContentObs;)V

    .line 803
    .local v5, "toTextContentHandler":Lorg/apache/tika/sax/ToTextContentHandler;
    const/4 v4, 0x0

    .line 805
    .local v4, "text":Ljava/lang/String;
    :try_start_0
    invoke-static {p1, v3}, Lorg/apache/tika/io/TikaInputStream;->get(Ljava/io/File;Lorg/apache/tika/metadata/Metadata;)Lorg/apache/tika/io/TikaInputStream;

    move-result-object v1

    .line 806
    invoke-direct {p0, v1, v5, v3}, Lcom/samsung/index/parser/IndexDocumentParser;->processRTF(Ljava/io/InputStream;Lorg/apache/tika/sax/ToTextContentHandler;Lorg/apache/tika/metadata/Metadata;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 816
    if-eqz v1, :cond_0

    .line 818
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 824
    :cond_0
    :goto_0
    return-object v4

    .line 819
    :catch_0
    move-exception v0

    .line 820
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 807
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 808
    .local v0, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "FileNotFoundException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 816
    if-eqz v1, :cond_0

    .line 818
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 819
    :catch_2
    move-exception v0

    .line 820
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 809
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 810
    .local v0, "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :try_start_4
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "GeneralEncryptedDocException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/samsung/index/GeneralEncryptedDocException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 816
    .end local v0    # "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :catchall_0
    move-exception v6

    if-eqz v1, :cond_1

    .line 818
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 821
    :cond_1
    :goto_1
    throw v6

    .line 812
    :catch_4
    move-exception v0

    .line 813
    .local v0, "e":Ljava/lang/Exception;
    const/4 v6, 0x1

    :try_start_6
    invoke-interface {p2, v6}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    .line 814
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 816
    if-eqz v1, :cond_0

    .line 818
    :try_start_7
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto/16 :goto_0

    .line 819
    :catch_5
    move-exception v0

    .line 820
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 819
    .end local v0    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v0

    .line 820
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v7, "IndexDocumentParser"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public readSNB(Ljava/lang/String;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "indexWriter"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    .line 850
    const-string/jumbo v2, ""

    .line 852
    .local v2, "text":Ljava/lang/String;
    const/4 v4, 0x0

    .line 855
    .local v4, "zipInput":Ljava/util/zip/ZipInputStream;
    :try_start_0
    new-instance v5, Ljava/util/zip/ZipInputStream;

    new-instance v6, Ljava/io/FileInputStream;

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v5, v6}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 858
    .end local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    .local v5, "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_0
    :try_start_1
    invoke-virtual {v5}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v3

    .local v3, "zipEntry":Ljava/util/zip/ZipEntry;
    if-eqz v3, :cond_1

    .line 859
    invoke-virtual {v3}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v1

    .line 860
    .local v1, "name":Ljava/lang/String;
    const-string/jumbo v6, "snote/snote.xml"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 861
    const-string/jumbo v6, ".snb"

    invoke-static {p2, v5, v6}, Lcom/samsung/index/parser/ooxml/OOXMLSaxParser;->parse(Lcom/samsung/index/ITextContentObs;Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 872
    .end local v1    # "name":Ljava/lang/String;
    :cond_1
    if-eqz v5, :cond_4

    .line 873
    :try_start_2
    invoke-virtual {v5}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 874
    const/4 v4, 0x0

    .line 881
    .end local v3    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_2
    :goto_0
    return-object v2

    .line 876
    .end local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v3    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v0

    .line 877
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "ERROR: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v5

    .line 879
    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto :goto_0

    .line 867
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_1
    move-exception v0

    .line 868
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 869
    const-string/jumbo v2, ""
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 872
    if-eqz v4, :cond_2

    .line 873
    :try_start_4
    invoke-virtual {v4}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 874
    const/4 v4, 0x0

    goto :goto_0

    .line 876
    :catch_2
    move-exception v0

    .line 877
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "ERROR: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 871
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 872
    :goto_2
    if-eqz v4, :cond_3

    .line 873
    :try_start_5
    invoke-virtual {v4}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 874
    const/4 v4, 0x0

    .line 878
    :cond_3
    :goto_3
    throw v6

    .line 876
    :catch_3
    move-exception v0

    .line 877
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v7, "IndexDocumentParser"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "ERROR: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 871
    .end local v0    # "e":Ljava/io/IOException;
    .end local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto :goto_2

    .line 867
    .end local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_4
    move-exception v0

    move-object v4, v5

    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto :goto_1

    .end local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v3    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_4
    move-object v4, v5

    .end local v5    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto/16 :goto_0
.end method

.method public readTXT(Ljava/io/File;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    .locals 19
    .param p1, "file"    # Ljava/io/File;
    .param p2, "indexWriter"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    .line 243
    new-instance v13, Ljava/lang/StringBuilder;

    const/high16 v15, 0x100000

    invoke-direct {v13, v15}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 244
    .local v13, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 246
    .local v2, "br":Ljava/io/BufferedReader;
    const/4 v7, 0x0

    .line 250
    .local v7, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v8, v15}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .local v8, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v9, Ljava/io/InputStreamReader;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/samsung/index/Utils;->getCharsetFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v9, v8, v15}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 255
    .local v9, "fr":Ljava/io/InputStreamReader;
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 257
    .end local v2    # "br":Ljava/io/BufferedReader;
    .local v3, "br":Ljava/io/BufferedReader;
    const/16 v1, 0x400

    .line 258
    .local v1, "MAX_CHARS_TO_READ":I
    const/16 v15, 0x400

    :try_start_2
    new-array v5, v15, [C

    .line 259
    .local v5, "dataArr":[C
    const/4 v4, 0x0

    .line 260
    .local v4, "currentLen":I
    const-string/jumbo v11, ""

    .line 261
    .local v11, "lastWord":Ljava/lang/String;
    const/4 v10, 0x1

    .line 262
    .local v10, "isFirstRead":Z
    const/4 v14, 0x0

    .line 263
    .local v14, "textParserLimit":I
    :cond_0
    const/4 v15, 0x0

    const/16 v16, 0x400

    move/from16 v0, v16

    invoke-virtual {v3, v5, v15, v0}, Ljava/io/BufferedReader;->read([CII)I

    move-result v4

    if-lez v4, :cond_5

    .line 264
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v15

    add-int/lit16 v15, v15, 0x400

    const/high16 v16, 0x100000

    move/from16 v0, v16

    if-le v15, v0, :cond_2

    .line 265
    const/4 v12, 0x0

    .line 266
    .local v12, "spaceIndex":I
    const/16 v15, 0x20

    invoke-static {v15}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v12

    .line 267
    if-lez v12, :cond_1

    .line 268
    add-int/lit8 v15, v12, 0x1

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 269
    add-int/lit8 v15, v12, 0x1

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 271
    :cond_1
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 272
    const/4 v15, 0x0

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 273
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    add-int/lit8 v14, v14, 0x1

    .line 277
    .end local v12    # "spaceIndex":I
    :cond_2
    if-eqz v10, :cond_4

    const/4 v15, 0x0

    aget-char v15, v5, v15

    const v16, 0xfeff

    move/from16 v0, v16

    if-eq v15, v0, :cond_3

    const/4 v15, 0x0

    aget-char v15, v5, v15

    const v16, 0xfffe

    move/from16 v0, v16

    if-ne v15, v0, :cond_4

    .line 279
    :cond_3
    const/4 v15, 0x0

    const/16 v16, 0x20

    aput-char v16, v5, v15

    .line 280
    const/4 v10, 0x0

    .line 282
    :cond_4
    const/4 v15, 0x0

    invoke-virtual {v13, v5, v15, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 283
    const/4 v4, 0x0

    .line 285
    const/16 v15, 0x14

    if-lt v14, v15, :cond_0

    .line 295
    :cond_5
    if-eqz v8, :cond_6

    .line 298
    :try_start_3
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 304
    :cond_6
    :goto_0
    if-eqz v3, :cond_c

    .line 306
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    move-object v2, v3

    .line 313
    .end local v1    # "MAX_CHARS_TO_READ":I
    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v4    # "currentLen":I
    .end local v5    # "dataArr":[C
    .end local v9    # "fr":Ljava/io/InputStreamReader;
    .end local v10    # "isFirstRead":Z
    .end local v11    # "lastWord":Ljava/lang/String;
    .end local v14    # "textParserLimit":I
    .restart local v2    # "br":Ljava/io/BufferedReader;
    :cond_7
    :goto_1
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    return-object v15

    .line 299
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "MAX_CHARS_TO_READ":I
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "currentLen":I
    .restart local v5    # "dataArr":[C
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fr":Ljava/io/InputStreamReader;
    .restart local v10    # "isFirstRead":Z
    .restart local v11    # "lastWord":Ljava/lang/String;
    .restart local v14    # "textParserLimit":I
    :catch_0
    move-exception v6

    .line 300
    .local v6, "e":Ljava/io/IOException;
    const-string/jumbo v15, "IndexDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 307
    .end local v6    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v6

    .line 308
    .restart local v6    # "e":Ljava/io/IOException;
    const-string/jumbo v15, "IndexDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    move-object v2, v3

    .line 309
    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    goto :goto_1

    .line 289
    .end local v1    # "MAX_CHARS_TO_READ":I
    .end local v4    # "currentLen":I
    .end local v5    # "dataArr":[C
    .end local v6    # "e":Ljava/io/IOException;
    .end local v9    # "fr":Ljava/io/InputStreamReader;
    .end local v10    # "isFirstRead":Z
    .end local v11    # "lastWord":Ljava/lang/String;
    .end local v14    # "textParserLimit":I
    :catch_2
    move-exception v6

    .line 290
    .local v6, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_5
    const-string/jumbo v15, "IndexDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "FileNotFoundException :"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 295
    if-eqz v7, :cond_8

    .line 298
    :try_start_6
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 304
    .end local v6    # "e":Ljava/io/FileNotFoundException;
    :cond_8
    :goto_3
    if-eqz v2, :cond_7

    .line 306
    :try_start_7
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 307
    :catch_3
    move-exception v6

    .line 308
    .local v6, "e":Ljava/io/IOException;
    const-string/jumbo v15, "IndexDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 299
    .local v6, "e":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v6

    .line 300
    .local v6, "e":Ljava/io/IOException;
    const-string/jumbo v15, "IndexDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 291
    .end local v6    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v6

    .line 292
    .restart local v6    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_8
    const-string/jumbo v15, "IndexDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 295
    if-eqz v7, :cond_9

    .line 298
    :try_start_9
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 304
    :cond_9
    :goto_5
    if-eqz v2, :cond_7

    .line 306
    :try_start_a
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto/16 :goto_1

    .line 307
    :catch_6
    move-exception v6

    .line 308
    const-string/jumbo v15, "IndexDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 299
    :catch_7
    move-exception v6

    .line 300
    const-string/jumbo v15, "IndexDocumentParser"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 295
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v15

    :goto_6
    if-eqz v7, :cond_a

    .line 298
    :try_start_b
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    .line 304
    :cond_a
    :goto_7
    if-eqz v2, :cond_b

    .line 306
    :try_start_c
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 309
    :cond_b
    :goto_8
    throw v15

    .line 299
    :catch_8
    move-exception v6

    .line 300
    .restart local v6    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "IndexDocumentParser"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 307
    .end local v6    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v6

    .line 308
    .restart local v6    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "IndexDocumentParser"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 295
    .end local v6    # "e":Ljava/io/IOException;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v15

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto :goto_6

    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "MAX_CHARS_TO_READ":I
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fr":Ljava/io/InputStreamReader;
    :catchall_2
    move-exception v15

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    goto :goto_6

    .line 291
    .end local v1    # "MAX_CHARS_TO_READ":I
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "fr":Ljava/io/InputStreamReader;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catch_a
    move-exception v6

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_4

    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "MAX_CHARS_TO_READ":I
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fr":Ljava/io/InputStreamReader;
    :catch_b
    move-exception v6

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_4

    .line 289
    .end local v1    # "MAX_CHARS_TO_READ":I
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "fr":Ljava/io/InputStreamReader;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catch_c
    move-exception v6

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "MAX_CHARS_TO_READ":I
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fr":Ljava/io/InputStreamReader;
    :catch_d
    move-exception v6

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "currentLen":I
    .restart local v5    # "dataArr":[C
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "isFirstRead":Z
    .restart local v11    # "lastWord":Ljava/lang/String;
    .restart local v14    # "textParserLimit":I
    :cond_c
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method

.method public readXLS(Ljava/io/File;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    .locals 10
    .param p1, "file"    # Ljava/io/File;
    .param p2, "indexWriter"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    const/4 v9, 0x1

    .line 328
    const-string/jumbo v4, ""

    .line 335
    .local v4, "text":Ljava/lang/String;
    :try_start_0
    new-instance v2, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;

    invoke-direct {v2, p1}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/File;)V

    .line 339
    .local v2, "fileSystem":Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    new-instance v6, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;

    invoke-direct {v6, p2, v2}, Lorg/apache/index/poi/hssf/usermodel/HSSFWorkbook;-><init>(Lcom/samsung/index/ITextContentObs;Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;)V

    .line 344
    invoke-virtual {v2}, Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;->closePOIFileHandler()V
    :try_end_0
    .catch Lorg/apache/index/poi/EncryptedDocumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/index/GeneralEncryptedDocException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/apache/index/poi/poifs/filesystem/OfficeXmlFileException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    move-object v5, v4

    .line 375
    .end local v2    # "fileSystem":Lorg/apache/index/poi/poifs/filesystem/POIFSFileSystem;
    .end local v4    # "text":Ljava/lang/String;
    .local v5, "text":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 347
    .end local v5    # "text":Ljava/lang/String;
    .restart local v4    # "text":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 349
    .local v0, "e":Lorg/apache/index/poi/EncryptedDocumentException;
    new-instance v6, Lcom/samsung/index/PasswordProtectedDocException;

    invoke-virtual {v0}, Lorg/apache/index/poi/EncryptedDocumentException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/samsung/index/PasswordProtectedDocException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 350
    .end local v0    # "e":Lorg/apache/index/poi/EncryptedDocumentException;
    :catch_1
    move-exception v0

    .line 352
    .local v0, "e":Lcom/samsung/index/GeneralEncryptedDocException;
    throw v0

    .line 353
    .end local v0    # "e":Lcom/samsung/index/GeneralEncryptedDocException;
    :catch_2
    move-exception v0

    .line 354
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 355
    .local v3, "msg":Ljava/lang/String;
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    if-eqz v3, :cond_0

    const-string/jumbo v6, "Invalid header signature"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 358
    new-instance v6, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-direct {v6, v3}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 360
    :cond_0
    invoke-interface {p2, v9}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    move-object v5, v4

    .line 361
    .end local v4    # "text":Ljava/lang/String;
    .restart local v5    # "text":Ljava/lang/String;
    goto :goto_0

    .line 362
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "msg":Ljava/lang/String;
    .end local v5    # "text":Ljava/lang/String;
    .restart local v4    # "text":Ljava/lang/String;
    :catch_3
    move-exception v0

    .line 363
    .local v0, "e":Lorg/apache/index/poi/poifs/filesystem/OfficeXmlFileException;
    const/4 v6, 0x0

    invoke-interface {p2, v6}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    .line 365
    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6, p2}, Lcom/samsung/index/parser/IndexDocumentParser;->readXLSX(Ljava/lang/String;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v4

    :goto_1
    move-object v5, v4

    .line 371
    .end local v4    # "text":Ljava/lang/String;
    .restart local v5    # "text":Ljava/lang/String;
    goto :goto_0

    .line 366
    .end local v5    # "text":Ljava/lang/String;
    .restart local v4    # "text":Ljava/lang/String;
    :catch_4
    move-exception v1

    .line 367
    .local v1, "e1":Ljava/lang/Exception;
    invoke-interface {p2, v9}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    .line 368
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception in reading XLSX: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 372
    .end local v0    # "e":Lorg/apache/index/poi/poifs/filesystem/OfficeXmlFileException;
    .end local v1    # "e1":Ljava/lang/Exception;
    :catch_5
    move-exception v0

    .line 373
    .local v0, "e":Ljava/lang/Exception;
    invoke-interface {p2, v9}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    .line 374
    const-string/jumbo v6, "IndexDocumentParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Exception in reading XLS: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v4

    .line 375
    .end local v4    # "text":Ljava/lang/String;
    .restart local v5    # "text":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method public readXLSX(Ljava/lang/String;Lcom/samsung/index/ITextContentObs;)Ljava/lang/String;
    .locals 13
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "indexWriter"    # Lcom/samsung/index/ITextContentObs;

    .prologue
    const/high16 v10, 0x100000

    .line 574
    const-string/jumbo v5, ""

    .line 577
    .local v5, "text":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 578
    .local v4, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .line 586
    .local v7, "zipInput":Ljava/util/zip/ZipInputStream;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 589
    .local v1, "file":Ljava/io/File;
    :try_start_0
    invoke-static {v1}, Lcom/samsung/thumbnail/util/Utils;->isValid(Ljava/io/File;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 590
    const/4 v9, 0x1

    invoke-interface {p2, v9}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V
    :try_end_0
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 635
    if-eqz v7, :cond_0

    .line 637
    :try_start_1
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 638
    const/4 v7, 0x0

    :cond_0
    :goto_0
    move-object v9, v5

    .line 644
    :goto_1
    return-object v9

    .line 639
    :catch_0
    move-exception v0

    .line 640
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v9, "IndexDocumentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "IOException: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 594
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_2
    new-instance v8, Ljava/util/zip/ZipInputStream;

    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v8, v9}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 596
    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .local v8, "zipInput":Ljava/util/zip/ZipInputStream;
    :try_start_3
    new-instance v3, Lcom/samsung/index/parser/ooxml/OOXMLPullParser;

    invoke-direct {v3, p2}, Lcom/samsung/index/parser/ooxml/OOXMLPullParser;-><init>(Lcom/samsung/index/ITextContentObs;)V

    .line 598
    .local v3, "pullParser":Lcom/samsung/index/parser/ooxml/OOXMLPullParser;
    :goto_2
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v6

    .local v6, "zipEntry":Ljava/util/zip/ZipEntry;
    if-eqz v6, :cond_7

    .line 599
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v2

    .line 603
    .local v2, "name":Ljava/lang/String;
    const-string/jumbo v9, "xl/sharedStrings.xml"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 604
    invoke-virtual {v3, v8}, Lcom/samsung/index/parser/ooxml/OOXMLPullParser;->parseExcelSST(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v5

    .line 605
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 620
    :cond_2
    :goto_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    if-lt v9, v10, :cond_3

    .line 622
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {p2, v9}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 623
    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 627
    :cond_3
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->closeEntry()V
    :try_end_3
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 629
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "pullParser":Lcom/samsung/index/parser/ooxml/OOXMLPullParser;
    .end local v6    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_1
    move-exception v0

    move-object v7, v8

    .line 631
    .end local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    .local v0, "e":Lorg/apache/poi/EncryptedDocumentException;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    :goto_4
    :try_start_4
    new-instance v9, Lcom/samsung/index/GeneralEncryptedDocException;

    invoke-virtual {v0}, Lorg/apache/poi/EncryptedDocumentException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/samsung/index/GeneralEncryptedDocException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 635
    .end local v0    # "e":Lorg/apache/poi/EncryptedDocumentException;
    :catchall_0
    move-exception v9

    :goto_5
    if-eqz v7, :cond_4

    .line 637
    :try_start_5
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 638
    const/4 v7, 0x0

    .line 641
    :cond_4
    :goto_6
    throw v9

    .line 615
    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v2    # "name":Ljava/lang/String;
    .restart local v3    # "pullParser":Lcom/samsung/index/parser/ooxml/OOXMLPullParser;
    .restart local v6    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_5
    :try_start_6
    const-string/jumbo v9, "xl/drawings/drawing"

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 616
    invoke-virtual {v3, v8}, Lcom/samsung/index/parser/ooxml/OOXMLPullParser;->parse(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v5

    .line 617
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_6
    .catch Lorg/apache/poi/EncryptedDocumentException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_3

    .line 632
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "pullParser":Lcom/samsung/index/parser/ooxml/OOXMLPullParser;
    .end local v6    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_2
    move-exception v0

    move-object v7, v8

    .line 633
    .end local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    .local v0, "e":Ljava/io/IOException;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    :goto_7
    :try_start_7
    const-string/jumbo v9, "IndexDocumentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "IOException: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 635
    if-eqz v7, :cond_6

    .line 637
    :try_start_8
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 638
    const/4 v7, 0x0

    .line 644
    .end local v0    # "e":Ljava/io/IOException;
    :cond_6
    :goto_8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_1

    .line 635
    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v3    # "pullParser":Lcom/samsung/index/parser/ooxml/OOXMLPullParser;
    .restart local v6    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_7
    if-eqz v8, :cond_8

    .line 637
    :try_start_9
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 638
    const/4 v7, 0x0

    .end local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto :goto_8

    .line 639
    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catch_3
    move-exception v0

    .line 640
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v9, "IndexDocumentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "IOException: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v7, v8

    .line 641
    .end local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto :goto_8

    .line 639
    .end local v3    # "pullParser":Lcom/samsung/index/parser/ooxml/OOXMLPullParser;
    .end local v6    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_4
    move-exception v0

    .line 640
    const-string/jumbo v9, "IndexDocumentParser"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "IOException: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 639
    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 640
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v10, "IndexDocumentParser"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "IOException: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 635
    .end local v0    # "e":Ljava/io/IOException;
    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    :catchall_1
    move-exception v9

    move-object v7, v8

    .end local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto/16 :goto_5

    .line 632
    :catch_6
    move-exception v0

    goto/16 :goto_7

    .line 629
    :catch_7
    move-exception v0

    goto/16 :goto_4

    .end local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v3    # "pullParser":Lcom/samsung/index/parser/ooxml/OOXMLPullParser;
    .restart local v6    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    :cond_8
    move-object v7, v8

    .end local v8    # "zipInput":Ljava/util/zip/ZipInputStream;
    .restart local v7    # "zipInput":Ljava/util/zip/ZipInputStream;
    goto :goto_8
.end method

.method public readZIP(Ljava/io/File;Lcom/samsung/index/ITextContentObs;Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p1, "file"    # Ljava/io/File;
    .param p2, "indexWriter"    # Lcom/samsung/index/ITextContentObs;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 896
    const-string/jumbo v3, "IndexDocumentParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Parsing zip file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    new-instance v2, Lcom/samsung/index/parser/util/ZipUtility;

    invoke-direct {v2, p1}, Lcom/samsung/index/parser/util/ZipUtility;-><init>(Ljava/io/File;)V

    .line 898
    .local v2, "zipUtility":Lcom/samsung/index/parser/util/ZipUtility;
    invoke-virtual {v2}, Lcom/samsung/index/parser/util/ZipUtility;->unzip()V

    .line 899
    new-instance v1, Ljava/io/File;

    iget-object v3, v2, Lcom/samsung/index/parser/util/ZipUtility;->mTempUnZipLocation:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 901
    .local v1, "fTemp":Ljava/io/File;
    invoke-direct {p0, v1, p2, p3}, Lcom/samsung/index/parser/IndexDocumentParser;->iterateZipDirectory(Ljava/io/File;Lcom/samsung/index/ITextContentObs;Landroid/content/Context;)V

    .line 905
    :try_start_0
    invoke-direct {p0, v1}, Lcom/samsung/index/parser/IndexDocumentParser;->deleteZipDirectory(Ljava/io/File;)V

    .line 906
    const-string/jumbo v3, "IndexDocumentParser"

    const-string/jumbo v4, "Deleted successfully "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 910
    :goto_0
    const-string/jumbo v3, ""

    return-object v3

    .line 907
    :catch_0
    move-exception v0

    .line 908
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, 0x1

    invoke-interface {p2, v3}, Lcom/samsung/index/ITextContentObs;->setCorrupted(Z)V

    goto :goto_0
.end method
