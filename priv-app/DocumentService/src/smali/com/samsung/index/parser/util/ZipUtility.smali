.class public Lcom/samsung/index/parser/util/ZipUtility;
.super Ljava/lang/Object;
.source "ZipUtility.java"


# static fields
.field public static final LUCENE_ZIP_TEMP_PATH:Ljava/lang/String; = "/lucene/zipdata"

.field private static final TAG:Ljava/lang/String; = "ZipUtility"


# instance fields
.field private final defaultTempDirectory:Ljava/lang/String;

.field public mTempUnZipLocation:Ljava/lang/String;

.field private final mZipFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 4
    .param p1, "zipFile"    # Ljava/io/File;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "/default"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/index/parser/util/ZipUtility;->defaultTempDirectory:Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lcom/samsung/index/parser/util/ZipUtility;->mZipFile:Ljava/io/File;

    .line 34
    invoke-virtual {p0}, Lcom/samsung/index/parser/util/ZipUtility;->createTempDir()V

    .line 35
    return-void
.end method

.method private createZipDirectory(Ljava/lang/String;)V
    .locals 5
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    .line 122
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/index/parser/util/ZipUtility;->mTempUnZipLocation:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 123
    .local v1, "f":Ljava/io/File;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_0

    .line 125
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "ZipUtility"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "ERROR: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public final createTempDir()V
    .locals 8

    .prologue
    .line 93
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "/lucene/zipdata"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 96
    .local v4, "sysTempDir":Ljava/io/File;
    const/4 v3, 0x0

    .line 97
    .local v3, "newTempDir":Ljava/io/File;
    const/16 v2, 0xa

    .line 98
    .local v2, "maxAttempts":I
    const/4 v0, 0x0

    .line 100
    .local v0, "attemptCount":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 101
    const/16 v5, 0xa

    if-le v0, v5, :cond_1

    .line 102
    const-string/jumbo v5, "ZipUtility"

    const-string/jumbo v6, "Not able to create directory"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_1
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 105
    .local v1, "dirName":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    .end local v3    # "newTempDir":Ljava/io/File;
    invoke-direct {v3, v4, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 106
    .restart local v3    # "newTempDir":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 108
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v5

    if-nez v5, :cond_2

    .line 109
    const-string/jumbo v5, "ZipUtility"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Failed to create temp dir named "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/index/parser/util/ZipUtility;->mTempUnZipLocation:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public unzip()V
    .locals 14

    .prologue
    .line 43
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    iget-object v10, p0, Lcom/samsung/index/parser/util/ZipUtility;->mZipFile:Ljava/io/File;

    invoke-direct {v6, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 48
    .local v6, "fin":Ljava/io/FileInputStream;
    new-instance v9, Ljava/util/zip/ZipInputStream;

    new-instance v10, Ljava/io/BufferedInputStream;

    invoke-direct {v10, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v9, v10}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 49
    .local v9, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v8, 0x0

    .line 50
    .local v8, "ze":Ljava/util/zip/ZipEntry;
    const/4 v3, 0x0

    .line 51
    .local v3, "dest":Ljava/io/BufferedOutputStream;
    const/16 v0, 0x800

    .local v0, "BUFFER":I
    move-object v4, v3

    .line 53
    .end local v3    # "dest":Ljava/io/BufferedOutputStream;
    .local v4, "dest":Ljava/io/BufferedOutputStream;
    :goto_0
    :try_start_1
    invoke-virtual {v9}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 55
    invoke-virtual {v8}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 56
    invoke-virtual {v8}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/samsung/index/parser/util/ZipUtility;->createZipDirectory(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 75
    :catch_0
    move-exception v5

    move-object v3, v4

    .line 76
    .end local v4    # "dest":Ljava/io/BufferedOutputStream;
    .restart local v3    # "dest":Ljava/io/BufferedOutputStream;
    .local v5, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    const-string/jumbo v10, "ZipUtility"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Decompress unzip"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 78
    if-eqz v9, :cond_0

    .line 80
    :try_start_3
    invoke-virtual {v9}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    .line 87
    .end local v0    # "BUFFER":I
    .end local v3    # "dest":Ljava/io/BufferedOutputStream;
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .end local v8    # "ze":Ljava/util/zip/ZipEntry;
    .end local v9    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_0
    :goto_2
    return-void

    .line 44
    :catch_1
    move-exception v5

    .line 45
    .local v5, "e":Ljava/io/FileNotFoundException;
    const-string/jumbo v10, "ZipUtility"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Exception :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 58
    .end local v5    # "e":Ljava/io/FileNotFoundException;
    .restart local v0    # "BUFFER":I
    .restart local v4    # "dest":Ljava/io/BufferedOutputStream;
    .restart local v6    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v9    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_1
    const/4 v1, 0x0

    .line 59
    .local v1, "count":I
    const/16 v10, 0x800

    :try_start_4
    new-array v2, v10, [B

    .line 60
    .local v2, "data":[B
    new-instance v7, Ljava/io/FileOutputStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/index/parser/util/ZipUtility;->mTempUnZipLocation:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 64
    .local v7, "fout":Ljava/io/FileOutputStream;
    :try_start_5
    new-instance v3, Ljava/io/BufferedOutputStream;

    const/16 v10, 0x800

    invoke-direct {v3, v7, v10}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 65
    .end local v4    # "dest":Ljava/io/BufferedOutputStream;
    .restart local v3    # "dest":Ljava/io/BufferedOutputStream;
    :goto_3
    const/4 v10, 0x0

    const/16 v11, 0x800

    :try_start_6
    invoke-virtual {v9, v2, v10, v11}, Ljava/util/zip/ZipInputStream;->read([BII)I

    move-result v1

    const/4 v10, -0x1

    if-eq v1, v10, :cond_2

    .line 66
    const/4 v10, 0x0

    invoke-virtual {v3, v2, v10, v1}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    .line 70
    :catchall_0
    move-exception v10

    :goto_4
    :try_start_7
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V

    throw v10
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 75
    :catch_2
    move-exception v5

    goto/16 :goto_1

    .line 68
    :cond_2
    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 70
    :try_start_9
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V

    .line 72
    invoke-virtual {v9}, Ljava/util/zip/ZipInputStream;->closeEntry()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    move-object v4, v3

    .line 73
    .end local v3    # "dest":Ljava/io/BufferedOutputStream;
    .restart local v4    # "dest":Ljava/io/BufferedOutputStream;
    goto/16 :goto_0

    .line 78
    .end local v1    # "count":I
    .end local v2    # "data":[B
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    :cond_3
    if-eqz v9, :cond_5

    .line 80
    :try_start_a
    invoke-virtual {v9}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    move-object v3, v4

    .line 83
    .end local v4    # "dest":Ljava/io/BufferedOutputStream;
    .restart local v3    # "dest":Ljava/io/BufferedOutputStream;
    goto :goto_2

    .line 81
    .end local v3    # "dest":Ljava/io/BufferedOutputStream;
    .restart local v4    # "dest":Ljava/io/BufferedOutputStream;
    :catch_3
    move-exception v5

    .line 82
    .local v5, "e":Ljava/lang/Exception;
    const-string/jumbo v10, "ZipUtility"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Exception: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v4

    .line 83
    .end local v4    # "dest":Ljava/io/BufferedOutputStream;
    .restart local v3    # "dest":Ljava/io/BufferedOutputStream;
    goto/16 :goto_2

    .line 81
    :catch_4
    move-exception v5

    .line 82
    const-string/jumbo v10, "ZipUtility"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Exception: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 78
    .end local v3    # "dest":Ljava/io/BufferedOutputStream;
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v4    # "dest":Ljava/io/BufferedOutputStream;
    :catchall_1
    move-exception v10

    move-object v3, v4

    .end local v4    # "dest":Ljava/io/BufferedOutputStream;
    .restart local v3    # "dest":Ljava/io/BufferedOutputStream;
    :goto_5
    if-eqz v9, :cond_4

    .line 80
    :try_start_b
    invoke-virtual {v9}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_5

    .line 83
    :cond_4
    :goto_6
    throw v10

    .line 81
    :catch_5
    move-exception v5

    .line 82
    .restart local v5    # "e":Ljava/lang/Exception;
    const-string/jumbo v11, "ZipUtility"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Exception: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 78
    .end local v5    # "e":Ljava/lang/Exception;
    :catchall_2
    move-exception v10

    goto :goto_5

    .line 70
    .end local v3    # "dest":Ljava/io/BufferedOutputStream;
    .restart local v1    # "count":I
    .restart local v2    # "data":[B
    .restart local v4    # "dest":Ljava/io/BufferedOutputStream;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    :catchall_3
    move-exception v10

    move-object v3, v4

    .end local v4    # "dest":Ljava/io/BufferedOutputStream;
    .restart local v3    # "dest":Ljava/io/BufferedOutputStream;
    goto/16 :goto_4

    .end local v1    # "count":I
    .end local v2    # "data":[B
    .end local v3    # "dest":Ljava/io/BufferedOutputStream;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v4    # "dest":Ljava/io/BufferedOutputStream;
    :cond_5
    move-object v3, v4

    .end local v4    # "dest":Ljava/io/BufferedOutputStream;
    .restart local v3    # "dest":Ljava/io/BufferedOutputStream;
    goto/16 :goto_2
.end method
