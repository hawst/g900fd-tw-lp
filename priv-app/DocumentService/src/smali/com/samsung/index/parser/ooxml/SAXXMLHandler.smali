.class public Lcom/samsung/index/parser/ooxml/SAXXMLHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "SAXXMLHandler.java"


# static fields
.field public static final BR_TAG:Ljava/lang/String; = "br"

.field private static final DOCX:I = 0x2

.field public static final END_PARA_RPR_TAG:Ljava/lang/String; = "endParaRPr"

.field private static final MAX_CHAR_COUNT:I = 0x100000

.field private static final PPTX:I = 0x0

.field public static final P_TAG:Ljava/lang/String; = "p"

.field public static final SI_TAG:Ljava/lang/String; = "si"

.field private static final SNB:I = 0x3

.field public static final TAB_TAG:Ljava/lang/String; = "tab"

.field public static final T_TAG:Ljava/lang/String; = "t"

.field private static final XLSX:I = 0x1


# instance fields
.field indexWriter:Lcom/samsung/index/ITextContentObs;

.field mIntfileType:I

.field strBuilder:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Lcom/samsung/index/ITextContentObs;Ljava/lang/String;)V
    .locals 2
    .param p1, "indexWriter"    # Lcom/samsung/index/ITextContentObs;
    .param p2, "fileType"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->mIntfileType:I

    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    const/high16 v1, 0x100000

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->strBuilder:Ljava/lang/StringBuilder;

    .line 45
    iput-object p1, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->indexWriter:Lcom/samsung/index/ITextContentObs;

    .line 47
    const-string/jumbo v0, ".pptx"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->mIntfileType:I

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    const-string/jumbo v0, ".xlsx"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 50
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->mIntfileType:I

    goto :goto_0

    .line 51
    :cond_2
    const-string/jumbo v0, ".docx"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 52
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->mIntfileType:I

    goto :goto_0

    .line 53
    :cond_3
    const-string/jumbo v0, ".snb"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->mIntfileType:I

    goto :goto_0
.end method


# virtual methods
.method public characters([CII)V
    .locals 5
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    const/high16 v4, 0x100000

    const/4 v3, 0x0

    .line 116
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    .line 118
    .local v0, "txt":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/2addr v1, v2

    if-lt v1, v4, :cond_1

    .line 119
    iget-object v1, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->indexWriter:Lcom/samsung/index/ITextContentObs;

    iget-object v2, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 120
    iget-object v1, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->indexWriter:Lcom/samsung/index/ITextContentObs;

    invoke-interface {v1, v0}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 121
    iget-object v1, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 127
    :goto_0
    iget-object v1, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lt v1, v4, :cond_0

    .line 133
    iget-object v1, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->indexWriter:Lcom/samsung/index/ITextContentObs;

    iget-object v2, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/index/ITextContentObs;->updateText(Ljava/lang/String;)V

    .line 134
    iget-object v1, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 136
    :cond_0
    return-void

    .line 124
    :cond_1
    iget-object v1, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x20

    .line 93
    iget v0, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->mIntfileType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 94
    const-string/jumbo v0, "p"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "br"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "tab"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 107
    :cond_1
    :goto_0
    return-void

    .line 98
    :cond_2
    iget v0, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->mIntfileType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 99
    const-string/jumbo v0, "si"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 102
    :cond_3
    iget v0, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->mIntfileType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 103
    const-string/jumbo v0, "t"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public getData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/index/parser/ooxml/SAXXMLHandler;->strBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
