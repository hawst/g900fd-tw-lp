.class public Lcom/samsung/index/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/index/Utils$Extension;
    }
.end annotation


# static fields
.field public static final ACTION_DOCUMENT_SERVICE_INDEX_FINISHED:Ljava/lang/String; = "action.documentservice.index.finished"

.field public static final ENCODING_TYPE_UTF_8:Ljava/lang/String; = "UTF-8"

.field public static final MAX_PARSER_THREAD:I = 0x2

.field public static final MIN_PARSER_THREAD:I = 0x1

.field public static final PACKAGE_NAME_MYFILES:Ljava/lang/String; = "com.sec.android.app.myfiles"

.field private static final STRING_FOR_DOT:Ljava/lang/String; = "."

.field private static final TAG:Ljava/lang/String; = "Utils"

.field public static final _TAG:Ljava/lang/String; = "DocumentService"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method public static checkPDFLOrPOI(Ljava/lang/String;)Z
    .locals 3
    .param p0, "extension"    # Ljava/lang/String;

    .prologue
    .line 192
    const/4 v0, 0x0

    .line 194
    .local v0, "returnVal":Z
    const-string/jumbo v1, ".pdf"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 195
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-le v1, v2, :cond_0

    .line 196
    const/4 v0, 0x1

    .line 200
    :cond_0
    :goto_0
    return v0

    .line 198
    :cond_1
    invoke-static {p0}, Lcom/samsung/index/Utils;->isPOIDoc(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static deleteDirectory(Ljava/io/File;)V
    .locals 4
    .param p0, "directory"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 289
    const-string/jumbo v1, "Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "forcefully trying to delete: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 304
    :cond_0
    return-void

    .line 294
    :cond_1
    invoke-static {p0}, Lcom/samsung/index/Utils;->isSymlink(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 295
    invoke-static {p0}, Lcom/samsung/index/Utils;->emptyDir(Ljava/io/File;)V

    .line 298
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unable to delete directory "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 300
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static deleteFile(Ljava/io/File;)V
    .locals 5
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 360
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 361
    invoke-static {p0}, Lcom/samsung/index/Utils;->deleteDirectory(Ljava/io/File;)V

    .line 373
    :cond_0
    return-void

    .line 363
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    .line 364
    .local v0, "isFilePresent":Z
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_0

    .line 365
    if-nez v0, :cond_2

    .line 366
    new-instance v2, Ljava/io/FileNotFoundException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "File does not exist: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 369
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to delete the file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 370
    .local v1, "message":Ljava/lang/String;
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static emptyDir(Ljava/io/File;)V
    .locals 11
    .param p0, "dir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 330
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    .line 331
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " does not exist"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 332
    .local v7, "message":Ljava/lang/String;
    new-instance v8, Ljava/lang/IllegalArgumentException;

    invoke-direct {v8, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 335
    .end local v7    # "message":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-nez v8, :cond_1

    .line 336
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " is not a directory"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 337
    .restart local v7    # "message":Ljava/lang/String;
    new-instance v8, Ljava/lang/IllegalArgumentException;

    invoke-direct {v8, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 340
    .end local v7    # "message":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 341
    .local v3, "files":[Ljava/io/File;
    if-nez v3, :cond_2

    .line 342
    new-instance v8, Ljava/io/IOException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "No Files found in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 345
    :cond_2
    const/4 v1, 0x0

    .line 346
    .local v1, "exception":Ljava/io/IOException;
    move-object v0, v3

    .local v0, "arr$":[Ljava/io/File;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_3

    aget-object v2, v0, v4

    .line 348
    .local v2, "file":Ljava/io/File;
    :try_start_0
    invoke-static {v2}, Lcom/samsung/index/Utils;->deleteFile(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 346
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 349
    :catch_0
    move-exception v5

    .line 350
    .local v5, "ioe":Ljava/io/IOException;
    move-object v1, v5

    goto :goto_1

    .line 354
    .end local v2    # "file":Ljava/io/File;
    .end local v5    # "ioe":Ljava/io/IOException;
    :cond_3
    if-eqz v1, :cond_4

    .line 355
    throw v1

    .line 357
    :cond_4
    return-void
.end method

.method public static getCharsetFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 19
    .param p0, "pathToFile"    # Ljava/lang/String;

    .prologue
    .line 232
    const-string/jumbo v1, "UTF-8"

    .line 234
    .local v1, "DEFAULT_CHARSET":Ljava/lang/String;
    const-string/jumbo v3, "UTF-8"

    .line 236
    .local v3, "detectedCharset":Ljava/lang/String;
    const/16 v15, 0x400

    new-array v14, v15, [B

    .line 237
    .local v14, "tempByteBuff":[B
    const/4 v7, 0x0

    .line 238
    .local v7, "fis":Ljava/io/FileInputStream;
    const/4 v4, 0x0

    .line 241
    .local v4, "detector":Lcom/ibm/icu/text/CharsetDetector;
    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .local v8, "fis":Ljava/io/FileInputStream;
    const/4 v10, 0x0

    .line 243
    .local v10, "length":I
    :try_start_1
    invoke-virtual {v8, v14}, Ljava/io/FileInputStream;->read([B)I

    move-result v10

    if-lez v10, :cond_1

    .line 246
    const/4 v9, 0x0

    .line 247
    .local v9, "index":I
    move v13, v10

    .local v13, "oldLength":I
    move v11, v10

    .line 248
    .end local v10    # "length":I
    .local v11, "length":I
    :goto_0
    const/16 v15, 0x64

    if-ge v11, v15, :cond_0

    .line 249
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "length":I
    .restart local v10    # "length":I
    aget-byte v15, v14, v9

    aput-byte v15, v14, v11

    .line 250
    rem-int/2addr v9, v13

    .line 251
    add-int/lit8 v9, v9, 0x1

    move v11, v10

    .end local v10    # "length":I
    .restart local v11    # "length":I
    goto :goto_0

    .line 253
    :cond_0
    new-instance v5, Lcom/ibm/icu/text/CharsetDetector;

    invoke-direct {v5}, Lcom/ibm/icu/text/CharsetDetector;-><init>()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 254
    .end local v4    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    .local v5, "detector":Lcom/ibm/icu/text/CharsetDetector;
    :try_start_2
    invoke-virtual {v5, v14}, Lcom/ibm/icu/text/CharsetDetector;->setText([B)Lcom/ibm/icu/text/CharsetDetector;

    .line 256
    invoke-virtual {v5}, Lcom/ibm/icu/text/CharsetDetector;->detect()Lcom/ibm/icu/text/CharsetMatch;

    move-result-object v12

    .line 258
    .local v12, "match":Lcom/ibm/icu/text/CharsetMatch;
    if-eqz v12, :cond_5

    .line 259
    invoke-virtual {v12}, Lcom/ibm/icu/text/CharsetMatch;->getConfidence()I

    move-result v2

    .line 260
    .local v2, "confidence":I
    const/16 v15, 0x19

    if-lt v2, v15, :cond_5

    .line 263
    invoke-virtual {v12}, Lcom/ibm/icu/text/CharsetMatch;->getName()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v3

    move v10, v11

    .end local v11    # "length":I
    .restart local v10    # "length":I
    move-object v4, v5

    .line 274
    .end local v2    # "confidence":I
    .end local v5    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    .end local v9    # "index":I
    .end local v12    # "match":Lcom/ibm/icu/text/CharsetMatch;
    .end local v13    # "oldLength":I
    .restart local v4    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    :cond_1
    :goto_1
    if-eqz v8, :cond_4

    .line 275
    :try_start_3
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 276
    const/4 v7, 0x0

    .line 283
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .end local v10    # "length":I
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :cond_2
    :goto_2
    return-object v3

    .line 278
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "length":I
    :catch_0
    move-exception v6

    .line 279
    .local v6, "e":Ljava/io/IOException;
    const-string/jumbo v15, "Utils"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "IOException in SubtitleParser: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v7, v8

    .line 281
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 268
    .end local v6    # "e":Ljava/io/IOException;
    .end local v10    # "length":I
    :catch_1
    move-exception v6

    .line 269
    .local v6, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_4
    const-string/jumbo v15, "Utils"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "FileNotFoundException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 274
    if-eqz v7, :cond_2

    .line 275
    :try_start_5
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 276
    const/4 v7, 0x0

    goto :goto_2

    .line 278
    :catch_2
    move-exception v6

    .line 279
    .local v6, "e":Ljava/io/IOException;
    const-string/jumbo v15, "Utils"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "IOException in SubtitleParser: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 270
    .end local v6    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v6

    .line 271
    .restart local v6    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_6
    const-string/jumbo v15, "Utils"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 274
    if-eqz v7, :cond_2

    .line 275
    :try_start_7
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 276
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 278
    :catch_4
    move-exception v6

    .line 279
    const-string/jumbo v15, "Utils"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "IOException in SubtitleParser: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 273
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v15

    .line 274
    :goto_5
    if-eqz v7, :cond_3

    .line 275
    :try_start_8
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 276
    const/4 v7, 0x0

    .line 280
    :cond_3
    :goto_6
    throw v15

    .line 278
    :catch_5
    move-exception v6

    .line 279
    .restart local v6    # "e":Ljava/io/IOException;
    const-string/jumbo v16, "Utils"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "IOException in SubtitleParser: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 273
    .end local v6    # "e":Ljava/io/IOException;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v15

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto :goto_5

    .end local v4    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "index":I
    .restart local v11    # "length":I
    .restart local v13    # "oldLength":I
    :catchall_2
    move-exception v15

    move-object v4, v5

    .end local v5    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    .restart local v4    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto :goto_5

    .line 270
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "index":I
    .end local v11    # "length":I
    .end local v13    # "oldLength":I
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catch_6
    move-exception v6

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .end local v4    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "index":I
    .restart local v11    # "length":I
    .restart local v13    # "oldLength":I
    :catch_7
    move-exception v6

    move-object v4, v5

    .end local v5    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    .restart local v4    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 268
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "index":I
    .end local v11    # "length":I
    .end local v13    # "oldLength":I
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catch_8
    move-exception v6

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_3

    .end local v4    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "index":I
    .restart local v11    # "length":I
    .restart local v13    # "oldLength":I
    :catch_9
    move-exception v6

    move-object v4, v5

    .end local v5    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    .restart local v4    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_3

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "index":I
    .end local v11    # "length":I
    .end local v13    # "oldLength":I
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "length":I
    :cond_4
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_2

    .end local v4    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v10    # "length":I
    .restart local v5    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "index":I
    .restart local v11    # "length":I
    .restart local v12    # "match":Lcom/ibm/icu/text/CharsetMatch;
    .restart local v13    # "oldLength":I
    :cond_5
    move v10, v11

    .end local v11    # "length":I
    .restart local v10    # "length":I
    move-object v4, v5

    .end local v5    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    .restart local v4    # "detector":Lcom/ibm/icu/text/CharsetDetector;
    goto/16 :goto_1
.end method

.method public static getExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 221
    const-string/jumbo v2, "."

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 222
    .local v1, "mid":I
    const/4 v0, 0x0

    .line 223
    .local v0, "ext":Ljava/lang/String;
    if-lez v1, :cond_0

    .line 224
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    .line 227
    :cond_0
    return-object v0
.end method

.method public static getUserCount(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 386
    const/4 v1, 0x1

    .line 387
    .local v1, "userCount":I
    const-string/jumbo v2, "user"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 389
    .local v0, "um":Landroid/os/UserManager;
    invoke-virtual {v0}, Landroid/os/UserManager;->getUserCount()I

    move-result v1

    .line 391
    return v1
.end method

.method public static isPOIDoc(Ljava/lang/String;)Z
    .locals 2
    .param p0, "extension"    # Ljava/lang/String;

    .prologue
    .line 204
    const/4 v0, 0x0

    .line 206
    .local v0, "returnVal":Z
    const-string/jumbo v1, ".doc"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, ".dot"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, ".ppt"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, ".pot"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, ".pps"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, ".xls"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, ".xlm"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, ".xlt"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 214
    :cond_0
    const/4 v0, 0x1

    .line 216
    :cond_1
    return v0
.end method

.method public static isSupported(Ljava/lang/String;)Z
    .locals 4
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 155
    const/4 v0, 0x0

    .line 156
    .local v0, "lastDotIndex":I
    const/4 v1, 0x0

    .line 158
    .local v1, "returnVal":Z
    const-string/jumbo v2, "."

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 160
    if-lez v0, :cond_0

    .line 161
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object p0

    .line 164
    sget-object v2, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    const/4 v1, 0x1

    .line 168
    :cond_0
    return v1
.end method

.method public static isSymlink(Ljava/io/File;)Z
    .locals 5
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 307
    if-nez p0, :cond_0

    .line 308
    new-instance v3, Ljava/lang/NullPointerException;

    const-string/jumbo v4, "File must not be null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 311
    :cond_0
    const/4 v1, 0x0

    .line 312
    .local v1, "fileInCanonicalDir":Ljava/io/File;
    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    .line 313
    .local v2, "parentFile":Ljava/io/File;
    if-nez v2, :cond_2

    .line 314
    move-object v1, p0

    .line 320
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 323
    :cond_1
    const/4 v3, 0x0

    .line 325
    :goto_1
    return v3

    .line 316
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v0

    .line 317
    .local v0, "canonicalDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    .end local v1    # "fileInCanonicalDir":Ljava/io/File;
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .restart local v1    # "fileInCanonicalDir":Ljava/io/File;
    goto :goto_0

    .line 325
    .end local v0    # "canonicalDir":Ljava/io/File;
    :cond_3
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public static isThumbnailSupported(Ljava/lang/String;)Z
    .locals 4
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 172
    const/4 v0, 0x0

    .line 173
    .local v0, "lastDotIndex":I
    const/4 v1, 0x0

    .line 175
    .local v1, "returnVal":Z
    const-string/jumbo v2, "."

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 177
    if-lez v0, :cond_0

    .line 178
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object p0

    .line 181
    const-string/jumbo v2, ".asc"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, ".csv"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, ".rtf"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 184
    const/4 v1, 0x1

    .line 188
    :cond_0
    return v1
.end method

.method public static startServiceFromProvider(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 401
    invoke-static {}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getInstance()Lcom/samsung/index/indexservice/service/DocumentServiceState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/index/indexservice/service/DocumentServiceState;->getCurrentState()Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    move-result-object v0

    .line 403
    .local v0, "curState":Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;
    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_NOT_STARTED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;->SERVICE_STOPPED:Lcom/samsung/index/indexservice/service/DocumentServiceState$EProcessState;

    if-ne v0, v1, :cond_1

    .line 405
    :cond_0
    invoke-static {p0}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->startServiceFromProvider(Landroid/content/Context;)V

    .line 407
    :cond_1
    return-void
.end method
