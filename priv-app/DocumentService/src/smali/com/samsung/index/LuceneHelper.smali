.class public Lcom/samsung/index/LuceneHelper;
.super Ljava/lang/Object;
.source "LuceneHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/index/LuceneHelper$1;,
        Lcom/samsung/index/LuceneHelper$ConcurrentMergeScedulerCustom;
    }
.end annotation


# static fields
.field private static final MERGE_FACTOR:I = 0xf

.field private static final TAG:Ljava/lang/String; = "LuceneHelper"

.field private static mIndexDir:Ljava/io/File;

.field private static mPath:Ljava/lang/String;


# instance fields
.field private mAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

.field private mConfig:Lorg/apache/lucene/index/IndexWriterConfig;

.field private mCsDirectory:Lorg/apache/lucene/store/Directory;

.field private mFsDirectory:Lorg/apache/lucene/store/Directory;

.field private mIndexReader:Lorg/apache/lucene/index/IndexReader;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/index/LuceneHelper;->mIndexReader:Lorg/apache/lucene/index/IndexReader;

    .line 165
    return-void
.end method

.method private restartIndexService(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 118
    if-nez p1, :cond_0

    .line 122
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-static {p1}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->processRestart(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public closeIndexReader()V
    .locals 4

    .prologue
    .line 285
    iget-object v1, p0, Lcom/samsung/index/LuceneHelper;->mIndexReader:Lorg/apache/lucene/index/IndexReader;

    if-eqz v1, :cond_0

    .line 287
    :try_start_0
    iget-object v1, p0, Lcom/samsung/index/LuceneHelper;->mIndexReader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->close()V

    .line 288
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/index/LuceneHelper;->mIndexReader:Lorg/apache/lucene/index/IndexReader;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 294
    :cond_0
    :goto_0
    return-void

    .line 289
    :catch_0
    move-exception v0

    .line 290
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v1, "DocumentService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error on closing IndexReader"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized deleteCurrentIndexPath()Z
    .locals 5

    .prologue
    .line 313
    monitor-enter p0

    const/4 v0, 0x1

    .line 314
    .local v0, "directoryDeleted":Z
    :try_start_0
    sget-object v2, Lcom/samsung/index/LuceneHelper;->mIndexDir:Ljava/io/File;

    if-eqz v2, :cond_1

    .line 317
    sget-object v2, Lcom/samsung/index/LuceneHelper;->mIndexDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/samsung/index/LuceneHelper;->mIndexDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 319
    :try_start_1
    sget-object v2, Lcom/samsung/index/LuceneHelper;->mIndexDir:Ljava/io/File;

    invoke-static {v2}, Lcom/samsung/index/Utils;->deleteDirectory(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 325
    :cond_0
    :goto_0
    :try_start_2
    sget-object v2, Lcom/samsung/index/LuceneHelper;->mIndexDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    const/4 v0, 0x1

    .line 328
    :cond_1
    :goto_1
    monitor-exit p0

    return v0

    .line 320
    :catch_0
    move-exception v1

    .line 321
    .local v1, "e":Ljava/io/IOException;
    :try_start_3
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Error on deleting index directory: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 313
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 325
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public encryptionForIndexes(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "algorithm"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 128
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 129
    const/16 v1, 0x10

    new-array v3, v1, [B

    .line 130
    .local v3, "salt":[B
    const/4 v0, 0x0

    .line 131
    .local v0, "enc":Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;
    new-instance v0, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;

    .end local v0    # "enc":Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;
    const/16 v4, 0x80

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;-><init>(Ljava/lang/String;Ljava/lang/String;[BIZ)V

    .line 133
    .restart local v0    # "enc":Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;
    new-instance v6, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;

    invoke-direct {v6, p2, v3, v5}, Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;-><init>(Ljava/lang/String;[BZ)V

    .line 135
    .local v6, "dec":Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;
    new-instance v8, Lorg/apache/lucene/store/transform/algorithm/StorePipeTransformer;

    new-instance v1, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;

    const/16 v2, 0x9

    const/4 v4, 0x1

    invoke-direct {v1, v2, v4}, Lorg/apache/lucene/store/transform/algorithm/compress/DeflateDataTransformer;-><init>(II)V

    invoke-direct {v8, v1, v0}, Lorg/apache/lucene/store/transform/algorithm/StorePipeTransformer;-><init>(Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;)V

    .line 139
    .local v8, "st":Lorg/apache/lucene/store/transform/algorithm/StorePipeTransformer;
    new-instance v7, Lorg/apache/lucene/store/transform/algorithm/ReadPipeTransformer;

    new-instance v1, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;

    invoke-direct {v1}, Lorg/apache/lucene/store/transform/algorithm/compress/InflateDataTransformer;-><init>()V

    invoke-direct {v7, v6, v1}, Lorg/apache/lucene/store/transform/algorithm/ReadPipeTransformer;-><init>(Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;)V

    .line 142
    .local v7, "rt":Lorg/apache/lucene/store/transform/algorithm/ReadPipeTransformer;
    new-instance v1, Lorg/apache/lucene/store/transform/TransformedDirectory;

    iget-object v2, p0, Lcom/samsung/index/LuceneHelper;->mFsDirectory:Lorg/apache/lucene/store/Directory;

    invoke-direct {v1, v2, v8, v7}, Lorg/apache/lucene/store/transform/TransformedDirectory;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/transform/algorithm/StoreDataTransformer;Lorg/apache/lucene/store/transform/algorithm/ReadDataTransformer;)V

    iput-object v1, p0, Lcom/samsung/index/LuceneHelper;->mCsDirectory:Lorg/apache/lucene/store/Directory;

    .line 144
    iget-object v1, p0, Lcom/samsung/index/LuceneHelper;->mFsDirectory:Lorg/apache/lucene/store/Directory;

    iput-object v1, p0, Lcom/samsung/index/LuceneHelper;->mCsDirectory:Lorg/apache/lucene/store/Directory;

    .line 146
    .end local v0    # "enc":Lorg/apache/lucene/store/transform/algorithm/security/DataEncryptor;
    .end local v3    # "salt":[B
    .end local v6    # "dec":Lorg/apache/lucene/store/transform/algorithm/security/DataDecryptor;
    .end local v7    # "rt":Lorg/apache/lucene/store/transform/algorithm/ReadPipeTransformer;
    .end local v8    # "st":Lorg/apache/lucene/store/transform/algorithm/StorePipeTransformer;
    :cond_0
    return-void
.end method

.method public getCurrentAnalyser()Lorg/apache/lucene/analysis/Analyzer;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/samsung/index/LuceneHelper;->mAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    return-object v0
.end method

.method public getCurrentConfig()Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/samsung/index/LuceneHelper;->mConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    return-object v0
.end method

.method public getCurrentDirectory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/samsung/index/LuceneHelper;->mCsDirectory:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method public getIndexReader()Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 278
    iget-object v0, p0, Lcom/samsung/index/LuceneHelper;->mIndexReader:Lorg/apache/lucene/index/IndexReader;

    if-nez v0, :cond_0

    .line 279
    invoke-virtual {p0}, Lcom/samsung/index/LuceneHelper;->getCurrentDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/index/IndexReader;->open(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/index/LuceneHelper;->mIndexReader:Lorg/apache/lucene/index/IndexReader;

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/samsung/index/LuceneHelper;->mIndexReader:Lorg/apache/lucene/index/IndexReader;

    return-object v0
.end method

.method public declared-synchronized getNewSyncIndexWriter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lorg/apache/lucene/index/IndexWriter;
    .locals 15
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "algorithm"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    .line 188
    monitor-enter p0

    const/4 v9, 0x0

    .line 190
    .local v9, "writer":Lorg/apache/lucene/index/IndexWriter;
    const/4 v2, 0x2

    .line 191
    .local v2, "MAX_RETRY_COUNT":I
    const/4 v8, 0x0

    .line 192
    .local v8, "retryCount":I
    const/4 v11, 0x0

    .line 193
    .local v11, "writerInitialized":Z
    const/4 v6, 0x0

    .local v6, "indexCorrupted":Z
    move-object v10, v9

    .line 198
    .end local v9    # "writer":Lorg/apache/lucene/index/IndexWriter;
    .local v10, "writer":Lorg/apache/lucene/index/IndexWriter;
    :goto_0
    :try_start_0
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/index/LuceneHelper;->initLuceneConfiguration(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v7

    .line 199
    .local v7, "isSuccess":Z
    if-nez v7, :cond_1

    .line 200
    if-eqz p4, :cond_0

    .line 201
    invoke-static/range {p4 .. p4}, Lcom/samsung/index/indexservice/service/BroadcastProcessorService;->stopDocumentService(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    move-object v9, v10

    .line 254
    .end local v10    # "writer":Lorg/apache/lucene/index/IndexWriter;
    :goto_1
    monitor-exit p0

    return-object v10

    .line 207
    .restart local v10    # "writer":Lorg/apache/lucene/index/IndexWriter;
    :cond_1
    :try_start_1
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/index/LuceneHelper;->encryptionForIndexes(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214
    :goto_2
    :try_start_2
    invoke-virtual {p0}, Lcom/samsung/index/LuceneHelper;->initWriterConfiguration()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 218
    :try_start_3
    new-instance v9, Lorg/apache/lucene/index/IndexWriter;

    iget-object v12, p0, Lcom/samsung/index/LuceneHelper;->mCsDirectory:Lorg/apache/lucene/store/Directory;

    iget-object v13, p0, Lcom/samsung/index/LuceneHelper;->mConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-direct {v9, v12, v13}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 220
    .end local v10    # "writer":Lorg/apache/lucene/index/IndexWriter;
    .restart local v9    # "writer":Lorg/apache/lucene/index/IndexWriter;
    const/4 v11, 0x1

    .line 242
    :goto_3
    if-nez v11, :cond_2

    add-int/lit8 v8, v8, 0x1

    const/4 v12, 0x2

    if-lt v8, v12, :cond_8

    .line 246
    :cond_2
    :goto_4
    if-nez v9, :cond_5

    .line 247
    const/4 v12, 0x2

    if-ge v8, v12, :cond_3

    if-eqz v6, :cond_4

    .line 248
    :cond_3
    :try_start_4
    invoke-virtual {p0}, Lcom/samsung/index/LuceneHelper;->deleteCurrentIndexPath()Z

    .line 251
    :cond_4
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/samsung/index/LuceneHelper;->restartIndexService(Landroid/content/Context;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_5
    move-object v10, v9

    .line 254
    .end local v9    # "writer":Lorg/apache/lucene/index/IndexWriter;
    .restart local v10    # "writer":Lorg/apache/lucene/index/IndexWriter;
    goto :goto_1

    .line 208
    :catch_0
    move-exception v5

    .line 209
    .local v5, "err":Ljava/security/GeneralSecurityException;
    :try_start_5
    const-string/jumbo v12, "LuceneHelper"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Encryption of index failed due to: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v5}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 188
    .end local v5    # "err":Ljava/security/GeneralSecurityException;
    .end local v7    # "isSuccess":Z
    :catchall_0
    move-exception v12

    move-object v9, v10

    .end local v10    # "writer":Lorg/apache/lucene/index/IndexWriter;
    .restart local v9    # "writer":Lorg/apache/lucene/index/IndexWriter;
    :goto_5
    monitor-exit p0

    throw v12

    .line 221
    .end local v9    # "writer":Lorg/apache/lucene/index/IndexWriter;
    .restart local v7    # "isSuccess":Z
    .restart local v10    # "writer":Lorg/apache/lucene/index/IndexWriter;
    :catch_1
    move-exception v3

    .line 222
    .local v3, "e":Ljava/lang/Exception;
    :try_start_6
    const-string/jumbo v12, "LuceneHelper"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Exception: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    instance-of v12, v3, Lorg/apache/lucene/store/LockObtainFailedException;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v12, :cond_6

    .line 226
    :try_start_7
    const-string/jumbo v12, "LuceneHelper"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "trying to unlock "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Lcom/samsung/index/LuceneHelper;->mCsDirectory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iget-object v12, p0, Lcom/samsung/index/LuceneHelper;->mCsDirectory:Lorg/apache/lucene/store/Directory;

    invoke-static {v12}, Lorg/apache/lucene/index/IndexWriter;->unlock(Lorg/apache/lucene/store/Directory;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_6
    move-object v9, v10

    .line 231
    .end local v10    # "writer":Lorg/apache/lucene/index/IndexWriter;
    .restart local v9    # "writer":Lorg/apache/lucene/index/IndexWriter;
    goto/16 :goto_3

    .line 228
    .end local v9    # "writer":Lorg/apache/lucene/index/IndexWriter;
    .restart local v10    # "writer":Lorg/apache/lucene/index/IndexWriter;
    :catch_2
    move-exception v4

    .line 229
    .local v4, "e1":Ljava/io/IOException;
    :try_start_8
    const-string/jumbo v12, "DocumentService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "unlock failed: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 232
    .end local v4    # "e1":Ljava/io/IOException;
    :cond_6
    instance-of v12, v3, Ljava/io/FileNotFoundException;

    if-nez v12, :cond_7

    instance-of v12, v3, Lorg/apache/lucene/index/CorruptIndexException;

    if-nez v12, :cond_7

    instance-of v12, v3, Ljava/lang/NumberFormatException;

    if-eqz v12, :cond_9

    .line 235
    :cond_7
    const-string/jumbo v12, "DocumentService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "exception: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 238
    const/4 v6, 0x1

    move-object v9, v10

    .line 239
    .end local v10    # "writer":Lorg/apache/lucene/index/IndexWriter;
    .restart local v9    # "writer":Lorg/apache/lucene/index/IndexWriter;
    goto/16 :goto_4

    .line 188
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v12

    goto/16 :goto_5

    :cond_8
    move-object v10, v9

    .end local v9    # "writer":Lorg/apache/lucene/index/IndexWriter;
    .restart local v10    # "writer":Lorg/apache/lucene/index/IndexWriter;
    goto/16 :goto_0

    .restart local v3    # "e":Ljava/lang/Exception;
    :cond_9
    move-object v9, v10

    .end local v10    # "writer":Lorg/apache/lucene/index/IndexWriter;
    .restart local v9    # "writer":Lorg/apache/lucene/index/IndexWriter;
    goto/16 :goto_3
.end method

.method public getNumberOfDocsIndexed()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 297
    const/4 v1, -0x1

    .line 299
    .local v1, "numberOfDocs":I
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/index/LuceneHelper;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexReader;->numDocs()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    move v2, v1

    .line 309
    .end local v1    # "numberOfDocs":I
    .local v2, "numberOfDocs":I
    :goto_0
    return v2

    .line 300
    .end local v2    # "numberOfDocs":I
    .restart local v1    # "numberOfDocs":I
    :catch_0
    move-exception v0

    .line 305
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Error on opening IndexReader, Message : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    .line 307
    .end local v1    # "numberOfDocs":I
    .restart local v2    # "numberOfDocs":I
    goto :goto_0
.end method

.method public initLuceneConfiguration(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 6
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 59
    new-instance v3, Lcom/samsung/index/AlphaNumericAnalyzer;

    invoke-direct {v3}, Lcom/samsung/index/AlphaNumericAnalyzer;-><init>()V

    iput-object v3, p0, Lcom/samsung/index/LuceneHelper;->mAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 61
    new-instance v3, Lorg/apache/lucene/index/IndexWriterConfig;

    sget-object v4, Lorg/apache/lucene/util/Version;->LUCENE_36:Lorg/apache/lucene/util/Version;

    iget-object v5, p0, Lcom/samsung/index/LuceneHelper;->mAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/index/IndexWriterConfig;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V

    iput-object v3, p0, Lcom/samsung/index/LuceneHelper;->mConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    .line 62
    iget-object v3, p0, Lcom/samsung/index/LuceneHelper;->mConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    sget-object v4, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE_OR_APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/IndexWriterConfig;->setOpenMode(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 64
    if-eqz p2, :cond_2

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p2, v3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 65
    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/index/LuceneHelper;->mPath:Ljava/lang/String;

    .line 69
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/index/LuceneHelper;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v3, Lcom/samsung/index/LuceneHelper;->mIndexDir:Ljava/io/File;

    .line 72
    sget-object v3, Lcom/samsung/index/LuceneHelper;->mIndexDir:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 75
    sget-object v3, Lcom/samsung/index/LuceneHelper;->mIndexDir:Ljava/io/File;

    invoke-static {v3}, Lorg/apache/lucene/store/FSDirectory;->open(Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/index/LuceneHelper;->mFsDirectory:Lorg/apache/lucene/store/Directory;

    .line 76
    iget-object v3, p0, Lcom/samsung/index/LuceneHelper;->mFsDirectory:Lorg/apache/lucene/store/Directory;

    iput-object v3, p0, Lcom/samsung/index/LuceneHelper;->mCsDirectory:Lorg/apache/lucene/store/Directory;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 77
    const/4 v1, -0x1

    .line 79
    .local v1, "numDocs":I
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/index/LuceneHelper;->getNumberOfDocsIndexed()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v1

    .line 83
    :goto_0
    if-gez v1, :cond_0

    .line 84
    :try_start_2
    invoke-virtual {p0}, Lcom/samsung/index/LuceneHelper;->deleteCurrentIndexPath()Z

    .line 86
    :cond_0
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/index/LuceneHelper;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v3, Lcom/samsung/index/LuceneHelper;->mIndexDir:Ljava/io/File;

    .line 87
    iget-object v3, p0, Lcom/samsung/index/LuceneHelper;->mCsDirectory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3}, Lorg/apache/lucene/store/Directory;->close()V

    .line 95
    .end local v1    # "numDocs":I
    :cond_1
    sget-object v3, Lcom/samsung/index/LuceneHelper;->mIndexDir:Ljava/io/File;

    invoke-static {v3}, Lorg/apache/lucene/store/FSDirectory;->open(Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/index/LuceneHelper;->mFsDirectory:Lorg/apache/lucene/store/Directory;

    .line 96
    iget-object v3, p0, Lcom/samsung/index/LuceneHelper;->mFsDirectory:Lorg/apache/lucene/store/Directory;

    iput-object v3, p0, Lcom/samsung/index/LuceneHelper;->mCsDirectory:Lorg/apache/lucene/store/Directory;

    .line 98
    iget-object v3, p0, Lcom/samsung/index/LuceneHelper;->mCsDirectory:Lorg/apache/lucene/store/Directory;

    new-instance v4, Lorg/apache/lucene/store/SimpleFSLockFactory;

    sget-object v5, Lcom/samsung/index/LuceneHelper;->mIndexDir:Ljava/io/File;

    invoke-direct {v4, v5}, Lorg/apache/lucene/store/SimpleFSLockFactory;-><init>(Ljava/io/File;)V

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/Directory;->setLockFactory(Lorg/apache/lucene/store/LockFactory;)V

    .line 112
    const/4 v2, 0x1

    :cond_2
    :goto_1
    return v2

    .line 80
    .restart local v1    # "numDocs":I
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v3, "LuceneHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 101
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "numDocs":I
    :catch_1
    move-exception v0

    .line 102
    .restart local v0    # "e":Ljava/io/IOException;
    const-string/jumbo v3, "LuceneHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Error on initializing index writer "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 107
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 108
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string/jumbo v3, "LuceneHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Error on initializing index writer "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/index/indexservice/Slog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public initWriterConfiguration()V
    .locals 4

    .prologue
    .line 153
    new-instance v0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;

    invoke-direct {v0}, Lorg/apache/lucene/index/LogByteSizeMergePolicy;-><init>()V

    .line 155
    .local v0, "merge":Lorg/apache/lucene/index/LogByteSizeMergePolicy;
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->setMergeFactor(I)V

    .line 156
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->setUseCompoundFile(Z)V

    .line 157
    iget-object v1, p0, Lcom/samsung/index/LuceneHelper;->mConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/index/IndexWriterConfig;->setMergePolicy(Lorg/apache/lucene/index/MergePolicy;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 159
    iget-object v1, p0, Lcom/samsung/index/LuceneHelper;->mConfig:Lorg/apache/lucene/index/IndexWriterConfig;

    new-instance v2, Lcom/samsung/index/LuceneHelper$ConcurrentMergeScedulerCustom;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/samsung/index/LuceneHelper$ConcurrentMergeScedulerCustom;-><init>(Lcom/samsung/index/LuceneHelper$1;)V

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexWriterConfig;->setMergeScheduler(Lorg/apache/lucene/index/MergeScheduler;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 160
    return-void
.end method
