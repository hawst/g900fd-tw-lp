.class public Lcom/samsung/index/Utils$Extension;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/index/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Extension"
.end annotation


# static fields
.field public static final ALLOWED_FILE_TYPES:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final ASC:Ljava/lang/String; = ".asc"

.field public static final CSV:Ljava/lang/String; = ".csv"

.field public static final DOC:Ljava/lang/String; = ".doc"

.field public static final DOCX:Ljava/lang/String; = ".docx"

.field public static final DOCX_DOCM:Ljava/lang/String; = ".docm"

.field public static final DOCX_DOTM:Ljava/lang/String; = ".dotm"

.field public static final DOCX_DOTX:Ljava/lang/String; = ".dotx"

.field public static final DOC_DOT:Ljava/lang/String; = ".dot"

.field public static final PDF:Ljava/lang/String; = ".pdf"

.field public static final PPT:Ljava/lang/String; = ".ppt"

.field public static final PPTX:Ljava/lang/String; = ".pptx"

.field public static final PPTX_POTM:Ljava/lang/String; = ".potm"

.field public static final PPTX_POTX:Ljava/lang/String; = ".potx"

.field public static final PPTX_PPAM:Ljava/lang/String; = ".ppam"

.field public static final PPTX_PPSM:Ljava/lang/String; = ".ppsm"

.field public static final PPTX_PPSX:Ljava/lang/String; = ".ppsx"

.field public static final PPTX_PPTM:Ljava/lang/String; = ".pptm"

.field public static final PPTX_SLDM:Ljava/lang/String; = ".sldm"

.field public static final PPTX_SLDX:Ljava/lang/String; = ".sldx"

.field public static final PPT_POT:Ljava/lang/String; = ".pot"

.field public static final PPT_PPS:Ljava/lang/String; = ".pps"

.field public static final RTF:Ljava/lang/String; = ".rtf"

.field public static final SNB:Ljava/lang/String; = ".snb"

.field public static final TXT:Ljava/lang/String; = ".txt"

.field public static final XLS:Ljava/lang/String; = ".xls"

.field public static final XLSX:Ljava/lang/String; = ".xlsx"

.field public static final XLSX_XLSM:Ljava/lang/String; = ".xlsm"

.field public static final XLSX_XLTM:Ljava/lang/String; = ".xltm"

.field public static final XLSX_XLTX:Ljava/lang/String; = ".xltx"

.field public static final XLS_XLM:Ljava/lang/String; = ".xlm"

.field public static final XLS_XLT:Ljava/lang/String; = ".xlt"

.field public static final XML:Ljava/lang/String; = ".xml"

.field public static final ZIP:Ljava/lang/String; = ".zip"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 105
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    .line 108
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".txt"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".doc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".docx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".pdf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".xls"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".xlsx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".ppt"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".pptx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".rtf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".dot"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".docm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".dotx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".dotm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".pot"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".pps"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".pptm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".potx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".potm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".ppam"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".ppsx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".ppsm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".sldx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".sldm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".xlt"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".xlm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".xlsm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".xltx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".xltm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".csv"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    sget-object v0, Lcom/samsung/index/Utils$Extension;->ALLOWED_FILE_TYPES:Ljava/util/ArrayList;

    const-string/jumbo v1, ".asc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
