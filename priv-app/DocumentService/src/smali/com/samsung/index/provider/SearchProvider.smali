.class public Lcom/samsung/index/provider/SearchProvider;
.super Landroid/content/ContentProvider;
.source "SearchProvider.java"


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.index.searchprovider"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field private static final FILES_API_PROJECTION:[Ljava/lang/String;

.field private static final GALAXY_SEARCH:I = 0x1

.field public static final GALAXY_SEARCH_TABLE:Ljava/lang/String; = "galaxy_search"

.field public static final HIGHLIGHT_CONTENT:Ljava/lang/String; = "highlight_content"

.field public static final PROJ_CONTENT:Ljava/lang/String; = "content_search"

.field private static final SEARCH:I = 0x0

.field public static final SEARCH_TABLE:Ljava/lang/String; = "search"

.field private static final SPACE_STRING:Ljava/lang/String; = " "

.field private static final TAG:Ljava/lang/String; = "SearchProvider"

.field public static isSearch:Z

.field private static sUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 38
    const-string/jumbo v0, "content://com.samsung.index.searchprovider/search"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/index/provider/SearchProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 43
    sput-boolean v3, Lcom/samsung/index/provider/SearchProvider;->isSearch:Z

    .line 49
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "_data"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string/jumbo v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "format"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "_size"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "date_modified"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "content_search"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "highlight_content"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/index/provider/SearchProvider;->FILES_API_PROJECTION:[Ljava/lang/String;

    .line 58
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/samsung/index/provider/SearchProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 59
    sget-object v0, Lcom/samsung/index/provider/SearchProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v1, "com.samsung.index.searchprovider"

    const-string/jumbo v2, "search"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 60
    sget-object v0, Lcom/samsung/index/provider/SearchProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v1, "com.samsung.index.searchprovider"

    const-string/jumbo v2, "galaxy_search"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 61
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Landroid/content/pm/PathPermission;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "readPermission"    # Ljava/lang/String;
    .param p3, "writePermission"    # Ljava/lang/String;
    .param p4, "pathPermissions"    # [Landroid/content/pm/PathPermission;

    .prologue
    .line 75
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 77
    return-void
.end method

.method private getCursorForGalaxyFinder([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/MatrixCursor;
    .locals 28
    .param p1, "keyArray"    # [Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "valueArray"    # [Ljava/lang/String;

    .prologue
    .line 254
    if-eqz p1, :cond_0

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v24, v0

    if-eqz v24, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_0

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v24, v0

    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_2

    .line 256
    :cond_0
    const/4 v5, 0x0

    .line 347
    :cond_1
    :goto_0
    return-object v5

    .line 258
    :cond_2
    const/4 v5, 0x0

    .line 259
    .local v5, "contentCursor":Landroid/database/MatrixCursor;
    const/4 v12, 0x0

    .line 260
    .local v12, "isExactmatch":Z
    const/16 v22, 0x1

    .line 263
    .local v22, "searchSubFolders":Z
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    .line 264
    .local v15, "keyValues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v13, -0x1

    .line 265
    .local v13, "j":I
    move-object/from16 v4, p1

    .local v4, "arr$":[Ljava/lang/String;
    array-length v0, v4

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_1
    move/from16 v0, v16

    if-ge v11, v0, :cond_4

    aget-object v14, v4, v11

    .line 266
    .local v14, "key":Ljava/lang/String;
    add-int/lit8 v13, v13, 0x1

    .line 267
    if-nez v14, :cond_3

    .line 265
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 270
    :cond_3
    aget-object v24, p3, v13

    move-object/from16 v0, v24

    invoke-virtual {v15, v14, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 276
    .end local v14    # "key":Ljava/lang/String;
    :cond_4
    new-instance v18, Lcom/samsung/index/QueryParser;

    invoke-direct/range {v18 .. v18}, Lcom/samsung/index/QueryParser;-><init>()V

    .line 277
    .local v18, "qParser":Lcom/samsung/index/QueryParser;
    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/index/QueryParser;->regexParser(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .line 280
    .local v19, "queryArray":[Ljava/lang/String;
    new-instance v23, Lcom/samsung/index/ContentSearch;

    const-string/jumbo v24, ".MyFilesContentSearch"

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/index/provider/SearchProvider;->getContext()Landroid/content/Context;

    move-result-object v25

    invoke-direct/range {v23 .. v25}, Lcom/samsung/index/ContentSearch;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 284
    .local v23, "searcher":Lcom/samsung/index/ContentSearch;
    if-eqz v23, :cond_5

    if-eqz v19, :cond_5

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v24, v0

    if-eqz v24, :cond_5

    invoke-virtual {v15}, Ljava/util/HashMap;->isEmpty()Z

    move-result v24

    if-eqz v24, :cond_6

    .line 286
    :cond_5
    const/4 v5, 0x0

    goto :goto_0

    .line 288
    :cond_6
    new-instance v21, Ljava/lang/StringBuilder;

    const/16 v24, 0x100

    move-object/from16 v0, v21

    move/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 289
    .local v21, "searchQueryBuilder":Ljava/lang/StringBuilder;
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v20, v0

    .line 292
    .local v20, "queryLen":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_3
    move/from16 v0, v20

    if-ge v10, v0, :cond_9

    .line 293
    rem-int/lit8 v24, v10, 0x2

    if-eqz v24, :cond_7

    .line 292
    :goto_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 295
    :cond_7
    rem-int/lit8 v24, v10, 0x2

    if-nez v24, :cond_8

    const/16 v24, 0x1

    move/from16 v0, v24

    if-le v10, v0, :cond_8

    .line 296
    const-string/jumbo v24, " "

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    :cond_8
    aget-object v24, v19, v10

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 304
    :cond_9
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v22

    invoke-virtual {v0, v1, v12, v2, v15}, Lcom/samsung/index/ContentSearch;->searchTextContentWithHighlighted(Ljava/lang/String;ZZLjava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v8

    .line 307
    .local v8, "foundFiles":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v8, :cond_a

    invoke-virtual {v8}, Ljava/util/HashMap;->size()I

    move-result v24

    if-lez v24, :cond_a

    .line 308
    new-instance v5, Landroid/database/MatrixCursor;

    .end local v5    # "contentCursor":Landroid/database/MatrixCursor;
    sget-object v24, Lcom/samsung/index/provider/SearchProvider;->FILES_API_PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-direct {v5, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 323
    .restart local v5    # "contentCursor":Landroid/database/MatrixCursor;
    :cond_a
    if-nez v5, :cond_b

    .line 325
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 328
    :cond_b
    invoke-virtual {v8}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_c
    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 329
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 330
    .local v17, "path":Ljava/lang/String;
    const-string/jumbo v9, ""

    .line 332
    .local v9, "highLightedSent":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, v17

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 334
    .local v7, "f":Ljava/io/File;
    if-eqz v7, :cond_c

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_c

    .line 337
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "highLightedSent":Ljava/lang/String;
    check-cast v9, Ljava/lang/String;

    .line 340
    .restart local v9    # "highLightedSent":Ljava/lang/String;
    const/16 v24, 0x8

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->hashCode()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    aput-object v17, v24, v25

    const/16 v25, 0x2

    const/16 v26, 0x2f

    move-object/from16 v0, v17

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v26

    move-object/from16 v0, v17

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x3

    const-string/jumbo v26, "0"

    aput-object v26, v24, v25

    const/16 v25, 0x4

    invoke-static {v7}, Lcom/samsung/index/provider/SearchProvider;->getTotalSize(Ljava/io/File;)J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x5

    invoke-virtual {v7}, Ljava/io/File;->lastModified()J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x6

    const-string/jumbo v26, "content_search"

    aput-object v26, v24, v25

    const/16 v25, 0x7

    aput-object v9, v24, v25

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_5
.end method

.method public static getTotalSize(Ljava/io/File;)J
    .locals 10
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 363
    const-wide/16 v6, 0x0

    .line 364
    .local v6, "totalSize":J
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 365
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 366
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 367
    .local v2, "fileList":[Ljava/io/File;
    if-eqz v2, :cond_2

    .line 368
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v1, v0, v3

    .line 369
    .local v1, "content":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 370
    invoke-static {v1}, Lcom/samsung/index/provider/SearchProvider;->getTotalSize(Ljava/io/File;)J

    move-result-wide v8

    add-long/2addr v6, v8

    .line 368
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 372
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v8

    add-long/2addr v6, v8

    goto :goto_1

    .line 376
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "content":Ljava/io/File;
    .end local v2    # "fileList":[Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v8

    .line 379
    :goto_2
    return-wide v8

    :cond_2
    move-wide v8, v6

    goto :goto_2
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # [Ljava/lang/String;

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;

    .prologue
    .line 99
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Landroid/content/ContentValues;

    .prologue
    .line 111
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 28
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # [Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # [Ljava/lang/String;
    .param p5, "arg4"    # Ljava/lang/String;

    .prologue
    .line 135
    const-string/jumbo v21, "SearchProvider"

    const-string/jumbo v24, "Search query received in content Search Provider"

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    sget-object v21, Lcom/samsung/index/provider/SearchProvider;->sUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v12

    .line 137
    .local v12, "id":I
    const/4 v6, 0x0

    .line 141
    .local v6, "contentCursor":Landroid/database/MatrixCursor;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/index/provider/SearchProvider;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/samsung/index/Utils;->startServiceFromProvider(Landroid/content/Context;)V

    .line 142
    const/16 v21, 0x1

    sput-boolean v21, Lcom/samsung/index/provider/SearchProvider;->isSearch:Z

    .line 144
    packed-switch v12, :pswitch_data_0

    :goto_0
    move-object/from16 v21, v6

    .line 238
    :goto_1
    return-object v21

    .line 147
    :pswitch_0
    move-object/from16 v18, p3

    .line 148
    .local v18, "query":Ljava/lang/String;
    const/4 v13, 0x0

    .line 149
    .local v13, "isExactmatch":Z
    const/16 v19, 0x1

    .line 150
    .local v19, "searchSubFolders":Z
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 152
    .local v5, "attrs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v14, -0x1

    .line 154
    .local v14, "j":I
    if-nez p2, :cond_0

    .line 155
    const/16 v21, 0x0

    goto :goto_1

    .line 158
    :cond_0
    move-object/from16 v4, p2

    .local v4, "arr$":[Ljava/lang/String;
    array-length v0, v4

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_2
    move/from16 v0, v16

    if-ge v11, v0, :cond_2

    aget-object v15, v4, v11

    .line 159
    .local v15, "key":Ljava/lang/String;
    add-int/lit8 v14, v14, 0x1

    .line 160
    if-nez v15, :cond_1

    .line 158
    :goto_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 163
    :cond_1
    aget-object v21, p4, v14

    move-object/from16 v0, v21

    invoke-virtual {v5, v15, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 168
    .end local v15    # "key":Ljava/lang/String;
    :cond_2
    if-eqz v18, :cond_3

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v21

    if-nez v21, :cond_4

    .line 171
    :cond_3
    const/16 v21, 0x0

    goto :goto_1

    .line 173
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    .line 174
    .local v22, "start":J
    new-instance v20, Lcom/samsung/index/ContentSearch;

    const-string/jumbo v21, ".MyFilesContentSearch"

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/index/provider/SearchProvider;->getContext()Landroid/content/Context;

    move-result-object v24

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/samsung/index/ContentSearch;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 180
    .local v20, "searcher":Lcom/samsung/index/ContentSearch;
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v13, v2, v5}, Lcom/samsung/index/ContentSearch;->searchTextContentWithHighlighted(Ljava/lang/String;ZZLjava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v9

    .line 184
    .local v9, "foundFiles":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v9, :cond_5

    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    move-result v21

    if-lez v21, :cond_5

    .line 186
    new-instance v6, Landroid/database/MatrixCursor;

    .end local v6    # "contentCursor":Landroid/database/MatrixCursor;
    sget-object v21, Lcom/samsung/index/provider/SearchProvider;->FILES_API_PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-direct {v6, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 188
    .restart local v6    # "contentCursor":Landroid/database/MatrixCursor;
    :cond_5
    if-nez v6, :cond_6

    .line 189
    const/16 v21, 0x0

    goto :goto_1

    .line 192
    :cond_6
    invoke-virtual {v9}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_8

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 193
    .local v7, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 195
    .local v17, "path":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    move-object/from16 v0, v17

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 197
    .local v8, "f":Ljava/io/File;
    if-eqz v8, :cond_7

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_7

    .line 200
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 203
    .local v10, "highLightedSent":Ljava/lang/String;
    const/16 v21, 0x8

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v24, 0x0

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->hashCode()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string/jumbo v26, ""

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    aput-object v25, v21, v24

    const/16 v24, 0x1

    aput-object v17, v21, v24

    const/16 v24, 0x2

    const/16 v25, 0x2f

    move-object/from16 v0, v17

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v25

    move-object/from16 v0, v17

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v21, v24

    const/16 v24, 0x3

    const-string/jumbo v25, "0"

    aput-object v25, v21, v24

    const/16 v24, 0x4

    invoke-static {v8}, Lcom/samsung/index/provider/SearchProvider;->getTotalSize(Ljava/io/File;)J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v21, v24

    const/16 v24, 0x5

    invoke-virtual {v8}, Ljava/io/File;->lastModified()J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v21, v24

    const/16 v24, 0x6

    const-string/jumbo v25, "content_search"

    aput-object v25, v21, v24

    const/16 v24, 0x7

    aput-object v10, v21, v24

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 223
    .end local v7    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v8    # "f":Ljava/io/File;
    .end local v10    # "highLightedSent":Ljava/lang/String;
    .end local v17    # "path":Ljava/lang/String;
    :cond_8
    const-string/jumbo v21, ""

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v25, "Time Taken to search in Content Search :"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    sub-long v26, v26, v22

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 230
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v5    # "attrs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v9    # "foundFiles":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v13    # "isExactmatch":Z
    .end local v14    # "j":I
    .end local v16    # "len$":I
    .end local v18    # "query":Ljava/lang/String;
    .end local v19    # "searchSubFolders":Z
    .end local v20    # "searcher":Lcom/samsung/index/ContentSearch;
    .end local v22    # "start":J
    :pswitch_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/index/provider/SearchProvider;->getCursorForGalaxyFinder([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/MatrixCursor;

    move-result-object v6

    .line 232
    goto/16 :goto_0

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Landroid/content/ContentValues;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # [Ljava/lang/String;

    .prologue
    .line 359
    const/4 v0, 0x0

    return v0
.end method
