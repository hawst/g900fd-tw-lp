.class public Lcom/samsung/index/ContentIndex;
.super Ljava/lang/Object;
.source "ContentIndex.java"


# static fields
.field public static final ENABLELOGS:Z = true

.field public static final ENABLEMEASUREMENTLOGS:Z = false

.field public static final ENABLEMEASUREMENTLOGS_TESTAPP:Z = true

.field public static final HIGHLIGHT_FIRSTLINETEXT:I = 0x2

.field public static final HIGHLIGHT_NOTEXT:I = 0x0

.field public static final HIGHLIGHT_RELEVANTTEXT:I = 0x1

.field public static final KEY_FILEPATH:Ljava/lang/String; = "filepath"

.field public static final KEY_HIGHLIGHT_CONTENT:Ljava/lang/String; = "hlcontents"

.field public static final KEY_TEXTCONTENT:Ljava/lang/String; = "contents"

.field public static mHighlightText:I


# instance fields
.field private mAppName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mEncryptionAlgorithm:Ljava/lang/String;

.field private mEncryptionPassword:Ljava/lang/String;

.field private mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

.field private mLuceneMgr:Lcom/samsung/index/LuceneHelper;

.field private mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    sput v0, Lcom/samsung/index/ContentIndex;->mHighlightText:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/index/ContentIndex;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "hightLightType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 91
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/index/ContentIndex;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "hightLightType"    # I
    .param p4, "algorithm"    # Ljava/lang/String;
    .param p5, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object v0, p0, Lcom/samsung/index/ContentIndex;->mEncryptionPassword:Ljava/lang/String;

    .line 67
    iput-object v0, p0, Lcom/samsung/index/ContentIndex;->mEncryptionAlgorithm:Ljava/lang/String;

    .line 68
    iput-object v0, p0, Lcom/samsung/index/ContentIndex;->mAppName:Ljava/lang/String;

    .line 70
    iput-object v0, p0, Lcom/samsung/index/ContentIndex;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    .line 71
    iput-object v0, p0, Lcom/samsung/index/ContentIndex;->mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    .line 129
    sput p3, Lcom/samsung/index/ContentIndex;->mHighlightText:I

    .line 130
    iput-object p1, p0, Lcom/samsung/index/ContentIndex;->mContext:Landroid/content/Context;

    .line 131
    iput-object p2, p0, Lcom/samsung/index/ContentIndex;->mAppName:Ljava/lang/String;

    .line 133
    new-instance v0, Lcom/samsung/index/LuceneHelper;

    invoke-direct {v0}, Lcom/samsung/index/LuceneHelper;-><init>()V

    iput-object v0, p0, Lcom/samsung/index/ContentIndex;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    .line 134
    iput-object p4, p0, Lcom/samsung/index/ContentIndex;->mEncryptionAlgorithm:Ljava/lang/String;

    .line 135
    iput-object p5, p0, Lcom/samsung/index/ContentIndex;->mEncryptionPassword:Ljava/lang/String;

    .line 137
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    invoke-virtual {v0, p2, p4, p5, p1}, Lcom/samsung/index/LuceneHelper;->getNewSyncIndexWriter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lorg/apache/lucene/index/IndexWriter;

    move-result-object v1

    .line 140
    .local v1, "writer":Lorg/apache/lucene/index/IndexWriter;
    new-instance v0, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    iget-object v2, p0, Lcom/samsung/index/ContentIndex;->mAppName:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/index/ContentIndex;->mEncryptionAlgorithm:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/index/ContentIndex;->mEncryptionPassword:Ljava/lang/String;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;-><init>(Lorg/apache/lucene/index/IndexWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    .line 143
    new-instance v0, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    iget-object v2, p0, Lcom/samsung/index/ContentIndex;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-direct {v0, v2, v3}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;-><init>(Landroid/content/Context;Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;)V

    iput-object v0, p0, Lcom/samsung/index/ContentIndex;->mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    .line 146
    return-void
.end method

.method private declared-synchronized closeNowNoMerge()V
    .locals 1

    .prologue
    .line 483
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v0}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->closeNoMerge()V

    .line 485
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    .line 487
    :cond_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    if-eqz v0, :cond_1

    .line 488
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    invoke-virtual {v0}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->closeNow()V

    .line 489
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/index/ContentIndex;->mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 490
    monitor-exit p0

    return-void

    .line 483
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized clearAllIndex()V
    .locals 1

    .prologue
    .line 493
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/samsung/index/ContentIndex;->closeNowNoMerge()V

    .line 494
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    invoke-virtual {v0}, Lcom/samsung/index/LuceneHelper;->deleteCurrentIndexPath()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 495
    monitor-exit p0

    return-void

    .line 493
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized closeNow()V
    .locals 1

    .prologue
    .line 431
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v0}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->close()V

    .line 433
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    .line 436
    :cond_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    if-eqz v0, :cond_1

    .line 437
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    invoke-virtual {v0}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->closeNow()V

    .line 438
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/index/ContentIndex;->mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 439
    monitor-exit p0

    return-void

    .line 431
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized closeReader()V
    .locals 1

    .prologue
    .line 191
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    invoke-virtual {v0}, Lcom/samsung/index/LuceneHelper;->closeIndexReader()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :cond_0
    monitor-exit p0

    return-void

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized closeWriter()V
    .locals 1

    .prologue
    .line 453
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v0}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->close()V

    .line 455
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 457
    :cond_0
    monitor-exit p0

    return-void

    .line 453
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized commitAfterDelete()V
    .locals 1

    .prologue
    .line 374
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v0}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->commitAfterDelete()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377
    :cond_0
    monitor-exit p0

    return-void

    .line 374
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized deleteIndex(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 369
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->deleteIndex(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371
    :cond_0
    monitor-exit p0

    return-void

    .line 369
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized deleteIndex(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "values"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 393
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->deleteIndex(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395
    :cond_0
    monitor-exit p0

    return-void

    .line 393
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized deleteIndex(Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/lucene/queryParser/ParseException;
        }
    .end annotation

    .prologue
    .line 415
    .local p1, "keyvalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v0, p1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->deleteIndex(Ljava/util/HashMap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 417
    :cond_0
    monitor-exit p0

    return-void

    .line 415
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized doCleanUpBeforeNew()V
    .locals 1

    .prologue
    .line 172
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v0}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->closeAndRestartIndexWriter()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    :cond_0
    monitor-exit p0

    return-void

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumDocsIndexed()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    invoke-virtual {v0}, Lcom/samsung/index/LuceneHelper;->getNumberOfDocsIndexed()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isRunning()Z
    .locals 4

    .prologue
    .line 153
    const-wide/16 v0, 0x0

    .line 154
    .local v0, "pendingDocs":J
    iget-object v2, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    if-eqz v2, :cond_0

    .line 155
    iget-object v2, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    invoke-virtual {v2}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->getPendingDocCount()J

    move-result-wide v0

    .line 158
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 159
    const/4 v2, 0x0

    .line 161
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public declared-synchronized setStopIndexing()V
    .locals 2

    .prologue
    .line 185
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mIndexWriter:Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/threads/AsyncIndexWriter;->setStopParserThread(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    :cond_0
    monitor-exit p0

    return-void

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized shutDownIndexing()V
    .locals 2

    .prologue
    .line 269
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->setInterrupt(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    :cond_0
    monitor-exit p0

    return-void

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized startIndexing(Ljava/io/File;Ljava/util/HashMap;Lcom/samsung/index/IFileIndexedNotifier;)Z
    .locals 2
    .param p1, "dataFileDir"    # Ljava/io/File;
    .param p3, "filenotifier"    # Lcom/samsung/index/IFileIndexedNotifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/samsung/index/IFileIndexedNotifier;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 244
    .local p2, "setkeyvalue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    monitor-enter p0

    const/4 v0, 0x0

    .line 248
    .local v0, "retVal":Z
    :try_start_0
    iget-object v1, p0, Lcom/samsung/index/ContentIndex;->mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    if-eqz v1, :cond_0

    .line 249
    iget-object v1, p0, Lcom/samsung/index/ContentIndex;->mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    invoke-virtual {v1, p1, p3, p2}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->startIndexing(Ljava/io/File;Lcom/samsung/index/IFileIndexedNotifier;Ljava/util/HashMap;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 255
    :cond_0
    monitor-exit p0

    return v0

    .line 244
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized switchIndexToTwoThread()V
    .locals 1

    .prologue
    .line 260
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/samsung/index/ContentIndex;->mThreadMgr:Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;

    invoke-virtual {v0}, Lcom/samsung/index/indexservice/threads/IndexParserThreadsManager;->switchIndexToTwoThread()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    :cond_0
    monitor-exit p0

    return-void

    .line 260
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public updateIndex(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "srcfilepath"    # Ljava/lang/String;
    .param p2, "dstFilePath"    # Ljava/lang/String;
    .param p3, "keyList"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 351
    return-void
.end method
