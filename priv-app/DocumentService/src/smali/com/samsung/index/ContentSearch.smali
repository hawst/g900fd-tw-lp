.class public Lcom/samsung/index/ContentSearch;
.super Ljava/lang/Object;
.source "ContentSearch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/index/ContentSearch$HitResult;
    }
.end annotation


# static fields
.field private static final CHAR_STAR:C = '*'

.field private static final FROM:I = 0x19

.field private static final KEY_PARENT:Ljava/lang/String; = "parent"

.field public static final MODIFIED_FROM_TIME:Ljava/lang/String; = "modifiedfromTime"

.field public static final MODIFIED_TO_TIME:Ljava/lang/String; = "modifiedtoTime"

.field private static final SPACE_STRING:Ljava/lang/String; = " "

.field private static final TAG:Ljava/lang/String; = "ContentSearch"

.field private static final TO:I = 0x64


# instance fields
.field private dateFrom:Ljava/lang/Integer;

.field private dateTo:Ljava/lang/Integer;

.field private mAppName:Ljava/lang/String;

.field private mEncryptionAlgorithm:Ljava/lang/String;

.field private mEncryptionPassword:Ljava/lang/String;

.field private mLuceneMgr:Lcom/samsung/index/LuceneHelper;

.field private mMaxHits:I

.field private mReader:Lorg/apache/lucene/index/IndexReader;

.field private mSearcher:Lorg/apache/lucene/search/IndexSearcher;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/16 v0, 0x12c

    iput v0, p0, Lcom/samsung/index/ContentSearch;->mMaxHits:I

    .line 63
    iput-object v1, p0, Lcom/samsung/index/ContentSearch;->mEncryptionPassword:Ljava/lang/String;

    .line 64
    iput-object v1, p0, Lcom/samsung/index/ContentSearch;->mEncryptionAlgorithm:Ljava/lang/String;

    .line 65
    iput-object v1, p0, Lcom/samsung/index/ContentSearch;->mAppName:Ljava/lang/String;

    .line 67
    iput-object v1, p0, Lcom/samsung/index/ContentSearch;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    .line 72
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/index/ContentSearch;->dateFrom:Ljava/lang/Integer;

    .line 73
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/index/ContentSearch;->dateTo:Ljava/lang/Integer;

    .line 87
    iput-object p1, p0, Lcom/samsung/index/ContentSearch;->mAppName:Ljava/lang/String;

    .line 89
    new-instance v0, Lcom/samsung/index/LuceneHelper;

    invoke-direct {v0}, Lcom/samsung/index/LuceneHelper;-><init>()V

    iput-object v0, p0, Lcom/samsung/index/ContentSearch;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    .line 90
    iget-object v0, p0, Lcom/samsung/index/ContentSearch;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    iget-object v1, p0, Lcom/samsung/index/ContentSearch;->mAppName:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/samsung/index/LuceneHelper;->initLuceneConfiguration(Ljava/lang/String;Landroid/content/Context;)Z

    .line 91
    iget-object v0, p0, Lcom/samsung/index/ContentSearch;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    invoke-virtual {v0}, Lcom/samsung/index/LuceneHelper;->initWriterConfiguration()V

    .line 92
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "algorithm"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    .line 114
    invoke-direct {p0, p1, p4}, Lcom/samsung/index/ContentSearch;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 117
    :try_start_0
    invoke-direct {p0, p2, p3}, Lcom/samsung/index/ContentSearch;->setEncryptionAlgorithmPassword(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Ljava/security/GeneralSecurityException;
    const-string/jumbo v1, "ContentSearch"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ERROR: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const-string/jumbo v1, "ContentSearch"

    const-string/jumbo v2, "Lucene index cannot be encrypted"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private addParentKey(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/search/BooleanQuery;Z)V
    .locals 11
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "mainBooleanQuery"    # Lorg/apache/lucene/search/BooleanQuery;
    .param p4, "isSearchSubFolders"    # Z

    .prologue
    .line 802
    const-string/jumbo v9, ":"

    invoke-virtual {p2, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 803
    .local v7, "values":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 804
    .local v6, "pathTerm":Lorg/apache/lucene/index/Term;
    const/4 v8, 0x0

    .line 805
    .local v8, "wcQueryForSubDir":Lorg/apache/lucene/search/WildcardQuery;
    const/4 v3, 0x0

    .line 806
    .local v3, "multiPhraseQuery":Lorg/apache/lucene/search/MultiPhraseQuery;
    if-eqz p4, :cond_2

    .line 807
    if-eqz v7, :cond_1

    array-length v9, v7

    const/4 v10, 0x1

    if-le v9, v10, :cond_1

    .line 808
    new-instance v5, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v5}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 809
    .local v5, "pathBooleanQuery":Lorg/apache/lucene/search/BooleanQuery;
    move-object v0, v7

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 810
    .local v4, "path":Ljava/lang/String;
    new-instance v6, Lorg/apache/lucene/index/Term;

    .end local v6    # "pathTerm":Lorg/apache/lucene/index/Term;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, v4}, Lcom/samsung/index/ContentSearch;->getSearchableParentPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0x2a

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, p1, v9}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    .restart local v6    # "pathTerm":Lorg/apache/lucene/index/Term;
    new-instance v8, Lorg/apache/lucene/search/WildcardQuery;

    .end local v8    # "wcQueryForSubDir":Lorg/apache/lucene/search/WildcardQuery;
    invoke-direct {v8, v6}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 813
    .restart local v8    # "wcQueryForSubDir":Lorg/apache/lucene/search/WildcardQuery;
    sget-object v9, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v5, v8, v9}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 809
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 815
    .end local v4    # "path":Ljava/lang/String;
    :cond_0
    sget-object v9, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p3, v5, v9}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 839
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v5    # "pathBooleanQuery":Lorg/apache/lucene/search/BooleanQuery;
    :goto_1
    return-void

    .line 817
    :cond_1
    new-instance v6, Lorg/apache/lucene/index/Term;

    .end local v6    # "pathTerm":Lorg/apache/lucene/index/Term;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p2}, Lcom/samsung/index/ContentSearch;->getSearchableParentPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0x2a

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, p1, v9}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    .restart local v6    # "pathTerm":Lorg/apache/lucene/index/Term;
    new-instance v8, Lorg/apache/lucene/search/WildcardQuery;

    .end local v8    # "wcQueryForSubDir":Lorg/apache/lucene/search/WildcardQuery;
    invoke-direct {v8, v6}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 820
    .restart local v8    # "wcQueryForSubDir":Lorg/apache/lucene/search/WildcardQuery;
    sget-object v9, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p3, v8, v9}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto :goto_1

    .line 823
    :cond_2
    if-eqz v7, :cond_4

    array-length v9, v7

    const/4 v10, 0x1

    if-le v9, v10, :cond_4

    .line 824
    new-instance v5, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v5}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 825
    .restart local v5    # "pathBooleanQuery":Lorg/apache/lucene/search/BooleanQuery;
    move-object v0, v7

    .restart local v0    # "arr$":[Ljava/lang/String;
    array-length v2, v0

    .restart local v2    # "len$":I
    const/4 v1, 0x0

    .restart local v1    # "i$":I
    :goto_2
    if-ge v1, v2, :cond_3

    aget-object v4, v0, v1

    .line 826
    .restart local v4    # "path":Ljava/lang/String;
    new-instance v3, Lorg/apache/lucene/search/MultiPhraseQuery;

    .end local v3    # "multiPhraseQuery":Lorg/apache/lucene/search/MultiPhraseQuery;
    invoke-direct {v3}, Lorg/apache/lucene/search/MultiPhraseQuery;-><init>()V

    .line 827
    .restart local v3    # "multiPhraseQuery":Lorg/apache/lucene/search/MultiPhraseQuery;
    new-instance v6, Lorg/apache/lucene/index/Term;

    .end local v6    # "pathTerm":Lorg/apache/lucene/index/Term;
    invoke-direct {p0, v4}, Lcom/samsung/index/ContentSearch;->getSearchableParentPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, p1, v9}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    .restart local v6    # "pathTerm":Lorg/apache/lucene/index/Term;
    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/MultiPhraseQuery;->add(Lorg/apache/lucene/index/Term;)V

    .line 829
    sget-object v9, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v5, v3, v9}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 825
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 831
    .end local v4    # "path":Ljava/lang/String;
    :cond_3
    sget-object v9, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p3, v5, v9}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto :goto_1

    .line 833
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v5    # "pathBooleanQuery":Lorg/apache/lucene/search/BooleanQuery;
    :cond_4
    new-instance v3, Lorg/apache/lucene/search/MultiPhraseQuery;

    .end local v3    # "multiPhraseQuery":Lorg/apache/lucene/search/MultiPhraseQuery;
    invoke-direct {v3}, Lorg/apache/lucene/search/MultiPhraseQuery;-><init>()V

    .line 834
    .restart local v3    # "multiPhraseQuery":Lorg/apache/lucene/search/MultiPhraseQuery;
    new-instance v6, Lorg/apache/lucene/index/Term;

    .end local v6    # "pathTerm":Lorg/apache/lucene/index/Term;
    invoke-direct {p0, p2}, Lcom/samsung/index/ContentSearch;->getSearchableParentPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, p1, v9}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    .restart local v6    # "pathTerm":Lorg/apache/lucene/index/Term;
    invoke-virtual {v3, v6}, Lorg/apache/lucene/search/MultiPhraseQuery;->add(Lorg/apache/lucene/index/Term;)V

    .line 836
    sget-object v9, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {p3, v3, v9}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto :goto_1
.end method

.method private checkWordBoundary(C)Z
    .locals 1
    .param p1, "charAt"    # C

    .prologue
    .line 441
    sparse-switch p1, :sswitch_data_0

    .line 451
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 449
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 441
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xb -> :sswitch_0
        0xc -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
    .end sparse-switch
.end method

.method private closeSearch()V
    .locals 4

    .prologue
    .line 143
    :try_start_0
    iget-object v1, p0, Lcom/samsung/index/ContentSearch;->mReader:Lorg/apache/lucene/index/IndexReader;

    if-eqz v1, :cond_0

    .line 144
    iget-object v1, p0, Lcom/samsung/index/ContentSearch;->mReader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->close()V

    .line 145
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/index/ContentSearch;->mReader:Lorg/apache/lucene/index/IndexReader;

    .line 148
    :cond_0
    iget-object v1, p0, Lcom/samsung/index/ContentSearch;->mSearcher:Lorg/apache/lucene/search/IndexSearcher;

    if-eqz v1, :cond_1

    .line 151
    iget-object v1, p0, Lcom/samsung/index/ContentSearch;->mSearcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v1}, Lorg/apache/lucene/search/IndexSearcher;->close()V

    .line 152
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/index/ContentSearch;->mSearcher:Lorg/apache/lucene/search/IndexSearcher;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :cond_1
    :goto_0
    return-void

    .line 155
    :catch_0
    move-exception v0

    .line 156
    .local v0, "e":Ljava/io/IOException;
    const-string/jumbo v1, "ContentSearch"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "ERROR: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getHighlightedText(Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;
    .locals 12
    .param p1, "contents"    # Ljava/lang/String;
    .param p2, "queryStr"    # Ljava/lang/String;
    .param p3, "pattern"    # Ljava/util/regex/Pattern;

    .prologue
    const/16 v11, 0x20

    .line 382
    const-string/jumbo v2, ""

    .line 383
    .local v2, "highLightResult":Ljava/lang/String;
    if-nez p1, :cond_0

    move-object v9, v2

    .line 431
    :goto_0
    return-object v9

    .line 386
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 387
    .local v1, "fileContent":Ljava/lang/String;
    const/4 v3, -0x1

    .line 388
    .local v3, "index":I
    invoke-virtual {p3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 389
    .local v6, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->find()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 390
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->start()I

    move-result v3

    .line 392
    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v7

    .line 394
    .local v7, "queryLength":I
    const/4 v9, -0x1

    if-eq v3, v9, :cond_5

    .line 395
    const/4 v8, 0x0

    .line 396
    .local v8, "start":I
    const/4 v0, 0x0

    .line 397
    .local v0, "end":I
    const/16 v9, 0x19

    if-le v3, v9, :cond_7

    .line 398
    add-int/lit8 v8, v3, -0x19

    .line 399
    add-int v9, v3, v7

    add-int/lit8 v0, v9, 0x64

    .line 404
    add-int/lit8 v5, v8, -0x1

    .line 405
    .local v5, "lineBreakCharIndex":I
    :goto_1
    if-gt v5, v3, :cond_2

    .line 406
    invoke-virtual {v1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-direct {p0, v9}, Lcom/samsung/index/ContentSearch;->checkWordBoundary(C)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 408
    move v8, v5

    .line 416
    :cond_2
    if-gt v0, v8, :cond_3

    .line 417
    move v8, v3

    .line 423
    .end local v5    # "lineBreakCharIndex":I
    :cond_3
    :goto_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    .line 424
    .local v4, "length":I
    if-le v0, v4, :cond_4

    .line 425
    move v0, v4

    .line 428
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 431
    .end local v0    # "end":I
    .end local v4    # "length":I
    .end local v8    # "start":I
    :cond_5
    const-string/jumbo v9, "(\u000c|\r|\n|\t|\r\n|\u000b)"

    const-string/jumbo v10, " "

    invoke-virtual {v2, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 411
    .restart local v0    # "end":I
    .restart local v5    # "lineBreakCharIndex":I
    .restart local v8    # "start":I
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 421
    .end local v5    # "lineBreakCharIndex":I
    :cond_7
    add-int v9, v3, v7

    add-int/lit8 v9, v9, 0x64

    rsub-int/lit8 v10, v3, 0x19

    add-int v0, v9, v10

    goto :goto_2
.end method

.method private getHitDocResults(Ljava/lang/String;ZZLjava/util/HashMap;)Lcom/samsung/index/ContentSearch$HitResult;
    .locals 33
    .param p1, "queryStr"    # Ljava/lang/String;
    .param p2, "isExactMatch"    # Z
    .param p3, "searchSubFolders"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/samsung/index/ContentSearch$HitResult;"
        }
    .end annotation

    .prologue
    .line 546
    .local p4, "keyvalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v10, 0x1

    .line 547
    .local v10, "j":I
    const/16 v23, 0x0

    .line 548
    .local v23, "topDocs":Lorg/apache/lucene/search/TopDocs;
    const/16 v24, 0x0

    .line 550
    .local v24, "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    new-instance v12, Lorg/apache/lucene/search/MultiPhraseQuery;

    invoke-direct {v12}, Lorg/apache/lucene/search/MultiPhraseQuery;-><init>()V

    .line 551
    .local v12, "mpquery":Lorg/apache/lucene/search/MultiPhraseQuery;
    const-string/jumbo v28, " "

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v27

    .line 552
    .local v27, "words":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 554
    .local v7, "inexTerm":Lorg/apache/lucene/index/Term;
    const/4 v14, 0x0

    .line 555
    .local v14, "q":Lorg/apache/lucene/search/Query;
    const/4 v15, 0x0

    .line 568
    .local v15, "query":Lorg/apache/lucene/search/Query;
    move/from16 v9, p2

    .line 570
    .local v9, "isExactmatch":Z
    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v28, v0

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_0

    .line 571
    const/4 v9, 0x1

    .line 575
    :cond_0
    if-nez v9, :cond_14

    .line 576
    :try_start_0
    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v28, v0

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_f

    .line 577
    move-object/from16 v3, v27

    .local v3, "arr$":[Ljava/lang/String;
    array-length v11, v3
    :try_end_0
    .catch Lorg/apache/lucene/queryParser/ParseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .local v11, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    move-object v8, v7

    .end local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    .local v8, "inexTerm":Lorg/apache/lucene/index/Term;
    :goto_0
    if-ge v6, v11, :cond_a

    :try_start_1
    aget-object v26, v3, v6

    .line 578
    .local v26, "word":Ljava/lang/String;
    const/16 v28, 0x1

    move/from16 v0, v28

    if-ne v10, v0, :cond_4

    .line 579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->mReader:Lorg/apache/lucene/index/IndexReader;

    move-object/from16 v28, v0

    new-instance v29, Lorg/apache/lucene/index/Term;

    const-string/jumbo v30, "contents"

    invoke-direct/range {v29 .. v30}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v28 .. v29}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v19

    .line 581
    .local v19, "te":Lorg/apache/lucene/index/TermEnum;
    new-instance v21, Ljava/util/LinkedList;

    invoke-direct/range {v21 .. v21}, Ljava/util/LinkedList;-><init>()V

    .line 583
    .local v21, "termList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    :cond_1
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v18

    .line 584
    .local v18, "t":Lorg/apache/lucene/index/Term;
    const-string/jumbo v28, "contents"

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_2

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_2

    .line 586
    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 589
    :cond_2
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/TermEnum;->next()Z

    move-result v28

    if-nez v28, :cond_1

    .line 590
    const/16 v22, 0x0

    .line 591
    .local v22, "terms":[Lorg/apache/lucene/index/Term;
    const/16 v28, 0x0

    move/from16 v0, v28

    new-array v0, v0, [Lorg/apache/lucene/index/Term;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v22

    .end local v22    # "terms":[Lorg/apache/lucene/index/Term;
    check-cast v22, [Lorg/apache/lucene/index/Term;

    .line 592
    .restart local v22    # "terms":[Lorg/apache/lucene/index/Term;
    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v28, v0

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-lt v0, v1, :cond_3

    .line 593
    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;)V

    .line 598
    :goto_1
    add-int/lit8 v10, v10, 0x1

    move-object v7, v8

    .line 577
    .end local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    .end local v18    # "t":Lorg/apache/lucene/index/Term;
    .end local v19    # "te":Lorg/apache/lucene/index/TermEnum;
    .end local v21    # "termList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    .end local v22    # "terms":[Lorg/apache/lucene/index/Term;
    .restart local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    :goto_2
    add-int/lit8 v6, v6, 0x1

    move-object v8, v7

    .end local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    goto :goto_0

    .line 595
    .restart local v18    # "t":Lorg/apache/lucene/index/Term;
    .restart local v19    # "te":Lorg/apache/lucene/index/TermEnum;
    .restart local v21    # "termList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    .restart local v22    # "terms":[Lorg/apache/lucene/index/Term;
    :cond_3
    new-instance v28, Lorg/apache/lucene/index/Term;

    const-string/jumbo v29, "contents"

    const-string/jumbo v30, "&&^%&%$"

    invoke-direct/range {v28 .. v30}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v28

    invoke-virtual {v12, v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->add(Lorg/apache/lucene/index/Term;)V
    :try_end_1
    .catch Lorg/apache/lucene/queryParser/ParseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 745
    .end local v18    # "t":Lorg/apache/lucene/index/Term;
    .end local v19    # "te":Lorg/apache/lucene/index/TermEnum;
    .end local v21    # "termList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    .end local v22    # "terms":[Lorg/apache/lucene/index/Term;
    .end local v26    # "word":Ljava/lang/String;
    :catch_0
    move-exception v5

    move-object v7, v8

    .line 746
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    .end local v11    # "len$":I
    .local v5, "e":Lorg/apache/lucene/queryParser/ParseException;
    .restart local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    :goto_3
    const-string/jumbo v28, "ContentSearch"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v30, "ERROR: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual {v5}, Lorg/apache/lucene/queryParser/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 751
    .end local v5    # "e":Lorg/apache/lucene/queryParser/ParseException;
    :goto_4
    new-instance v17, Lcom/samsung/index/ContentSearch$HitResult;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/index/ContentSearch$HitResult;-><init>()V

    .line 754
    .local v17, "result":Lcom/samsung/index/ContentSearch$HitResult;
    move-object/from16 v0, v23

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/samsung/index/ContentSearch$HitResult;->topDocs:Lorg/apache/lucene/search/TopDocs;

    .line 756
    return-object v17

    .line 599
    .end local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    .end local v17    # "result":Lcom/samsung/index/ContentSearch$HitResult;
    .restart local v3    # "arr$":[Ljava/lang/String;
    .restart local v6    # "i$":I
    .restart local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v11    # "len$":I
    .restart local v26    # "word":Ljava/lang/String;
    :cond_4
    :try_start_2
    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v10, v0, :cond_5

    .line 600
    new-instance v7, Lorg/apache/lucene/index/Term;

    const-string/jumbo v28, "contents"

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-direct {v7, v0, v1}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/apache/lucene/queryParser/ParseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 602
    .end local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    :try_start_3
    invoke-virtual {v12, v7}, Lorg/apache/lucene/search/MultiPhraseQuery;->add(Lorg/apache/lucene/index/Term;)V
    :try_end_3
    .catch Lorg/apache/lucene/queryParser/ParseException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 603
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 605
    .end local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    :cond_5
    :try_start_4
    const-string/jumbo v28, "ContentSearch"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v30, "Found  word.toString(); "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->mReader:Lorg/apache/lucene/index/IndexReader;

    move-object/from16 v28, v0

    new-instance v29, Lorg/apache/lucene/index/Term;

    const-string/jumbo v30, "contents"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v28 .. v29}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v19

    .line 609
    .restart local v19    # "te":Lorg/apache/lucene/index/TermEnum;
    new-instance v21, Ljava/util/LinkedList;

    invoke-direct/range {v21 .. v21}, Ljava/util/LinkedList;-><init>()V

    .line 611
    .restart local v21    # "termList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    :cond_6
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v18

    .line 612
    .restart local v18    # "t":Lorg/apache/lucene/index/Term;
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v28

    const-string/jumbo v29, "contents"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v28

    if-nez v28, :cond_8

    .line 620
    :cond_7
    :goto_5
    const/16 v22, 0x0

    .line 621
    .restart local v22    # "terms":[Lorg/apache/lucene/index/Term;
    const/16 v28, 0x0

    move/from16 v0, v28

    new-array v0, v0, [Lorg/apache/lucene/index/Term;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v22

    .end local v22    # "terms":[Lorg/apache/lucene/index/Term;
    check-cast v22, [Lorg/apache/lucene/index/Term;

    .line 622
    .restart local v22    # "terms":[Lorg/apache/lucene/index/Term;
    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v28, v0

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-lt v0, v1, :cond_9

    .line 623
    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;)V

    .line 628
    :goto_6
    add-int/lit8 v10, v10, 0x1

    move-object v7, v8

    .end local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    goto/16 :goto_2

    .line 618
    .end local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    .end local v22    # "terms":[Lorg/apache/lucene/index/Term;
    .restart local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    :cond_8
    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 619
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/TermEnum;->next()Z

    move-result v28

    if-nez v28, :cond_6

    goto :goto_5

    .line 625
    .restart local v22    # "terms":[Lorg/apache/lucene/index/Term;
    :cond_9
    new-instance v28, Lorg/apache/lucene/index/Term;

    const-string/jumbo v29, "contents"

    const-string/jumbo v30, "&&^%&%$"

    invoke-direct/range {v28 .. v30}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v28

    invoke-virtual {v12, v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->add(Lorg/apache/lucene/index/Term;)V
    :try_end_4
    .catch Lorg/apache/lucene/queryParser/ParseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_6

    .line 747
    .end local v18    # "t":Lorg/apache/lucene/index/Term;
    .end local v19    # "te":Lorg/apache/lucene/index/TermEnum;
    .end local v21    # "termList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    .end local v22    # "terms":[Lorg/apache/lucene/index/Term;
    .end local v26    # "word":Ljava/lang/String;
    :catch_1
    move-exception v5

    move-object v7, v8

    .line 748
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    .end local v11    # "len$":I
    .local v5, "e":Ljava/io/IOException;
    .restart local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    :goto_7
    const-string/jumbo v28, "ContentSearch"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v30, "ERROR: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 633
    .end local v5    # "e":Ljava/io/IOException;
    .end local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v3    # "arr$":[Ljava/lang/String;
    .restart local v6    # "i$":I
    .restart local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v11    # "len$":I
    :cond_a
    if-eqz p4, :cond_e

    :try_start_5
    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v28

    if-nez v28, :cond_e

    .line 635
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/samsung/index/ContentSearch;->getQueryForAdvancedSearch(Ljava/util/HashMap;Z)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v4

    .line 641
    .local v4, "bq":Lorg/apache/lucene/search/BooleanQuery;
    if-eqz v12, :cond_b

    .line 642
    sget-object v28, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v28

    invoke-virtual {v4, v12, v0}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 644
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateFrom:Ljava/lang/Integer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    if-eqz v28, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateTo:Ljava/lang/Integer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    if-eqz v28, :cond_c

    .line 645
    const-string/jumbo v28, "modifiedTime"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateFrom:Ljava/lang/Integer;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateTo:Ljava/lang/Integer;

    move-object/from16 v30, v0

    const/16 v31, 0x1

    const/16 v32, 0x1

    invoke-static/range {v28 .. v32}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v14

    .line 648
    const/16 v28, 0x0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/index/ContentSearch;->dateFrom:Ljava/lang/Integer;

    .line 649
    const/16 v28, 0x0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/index/ContentSearch;->dateTo:Ljava/lang/Integer;

    .line 651
    :cond_c
    if-eqz v14, :cond_d

    .line 652
    sget-object v28, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v28

    invoke-virtual {v4, v14, v0}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V
    :try_end_5
    .catch Lorg/apache/lucene/queryParser/ParseException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 654
    :cond_d
    move-object v15, v4

    move-object v7, v8

    .line 743
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v4    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    .end local v6    # "i$":I
    .end local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    .end local v11    # "len$":I
    .restart local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    :goto_8
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->mSearcher:Lorg/apache/lucene/search/IndexSearcher;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/index/ContentSearch;->mMaxHits:I

    move/from16 v29, v0

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v0, v15, v1}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Query;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v23

    goto/16 :goto_4

    .line 656
    .end local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v3    # "arr$":[Ljava/lang/String;
    .restart local v6    # "i$":I
    .restart local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v11    # "len$":I
    :cond_e
    move-object v15, v12

    move-object v7, v8

    .end local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    goto :goto_8

    .line 660
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v11    # "len$":I
    :cond_f
    new-instance v20, Lorg/apache/lucene/index/Term;

    const-string/jumbo v28, "contents"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const/16 v30, 0x2a

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v20

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    .local v20, "term":Lorg/apache/lucene/index/Term;
    new-instance v25, Lorg/apache/lucene/search/WildcardQuery;

    move-object/from16 v0, v25

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/WildcardQuery;-><init>(Lorg/apache/lucene/index/Term;)V
    :try_end_6
    .catch Lorg/apache/lucene/queryParser/ParseException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 663
    .end local v24    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    .local v25, "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    if-eqz p4, :cond_13

    :try_start_7
    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v28

    if-nez v28, :cond_13

    .line 664
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/samsung/index/ContentSearch;->getQueryForAdvancedSearch(Ljava/util/HashMap;Z)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v4

    .line 670
    .restart local v4    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    if-eqz v25, :cond_10

    .line 671
    sget-object v28, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    invoke-virtual {v4, v0, v1}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 673
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateFrom:Ljava/lang/Integer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    if-eqz v28, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateTo:Ljava/lang/Integer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    if-eqz v28, :cond_11

    .line 674
    const-string/jumbo v28, "modifiedTime"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateFrom:Ljava/lang/Integer;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateTo:Ljava/lang/Integer;

    move-object/from16 v30, v0

    const/16 v31, 0x1

    const/16 v32, 0x1

    invoke-static/range {v28 .. v32}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v14

    .line 678
    :cond_11
    if-eqz v14, :cond_12

    .line 679
    sget-object v28, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v28

    invoke-virtual {v4, v14, v0}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V
    :try_end_7
    .catch Lorg/apache/lucene/queryParser/ParseException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 681
    :cond_12
    move-object v15, v4

    .end local v4    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    :goto_9
    move-object/from16 v24, v25

    .line 685
    .end local v25    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    .restart local v24    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    goto/16 :goto_8

    .line 683
    .end local v24    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    .restart local v25    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    :cond_13
    move-object/from16 v15, v25

    goto :goto_9

    .line 687
    .end local v20    # "term":Lorg/apache/lucene/index/Term;
    .end local v25    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    .restart local v24    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    :cond_14
    :try_start_8
    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v28, v0

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_1a

    .line 688
    move-object/from16 v3, v27

    .restart local v3    # "arr$":[Ljava/lang/String;
    array-length v11, v3
    :try_end_8
    .catch Lorg/apache/lucene/queryParser/ParseException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    .restart local v11    # "len$":I
    const/4 v6, 0x0

    .restart local v6    # "i$":I
    move-object v8, v7

    .end local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    :goto_a
    if-ge v6, v11, :cond_15

    :try_start_9
    aget-object v26, v3, v6

    .line 689
    .restart local v26    # "word":Ljava/lang/String;
    new-instance v7, Lorg/apache/lucene/index/Term;

    const-string/jumbo v28, "contents"

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-direct {v7, v0, v1}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Lorg/apache/lucene/queryParser/ParseException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    .line 690
    .end local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    :try_start_a
    invoke-virtual {v12, v7}, Lorg/apache/lucene/search/MultiPhraseQuery;->add(Lorg/apache/lucene/index/Term;)V
    :try_end_a
    .catch Lorg/apache/lucene/queryParser/ParseException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    .line 688
    add-int/lit8 v6, v6, 0x1

    move-object v8, v7

    .end local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    goto :goto_a

    .line 692
    .end local v26    # "word":Ljava/lang/String;
    :cond_15
    if-eqz p4, :cond_19

    :try_start_b
    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v28

    if-nez v28, :cond_19

    .line 693
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/samsung/index/ContentSearch;->getQueryForAdvancedSearch(Ljava/util/HashMap;Z)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v4

    .line 696
    .restart local v4    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    if-eqz v12, :cond_16

    .line 697
    sget-object v28, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v28

    invoke-virtual {v4, v12, v0}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 699
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateFrom:Ljava/lang/Integer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    if-eqz v28, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateTo:Ljava/lang/Integer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    if-eqz v28, :cond_17

    .line 700
    const-string/jumbo v28, "modifiedTime"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateFrom:Ljava/lang/Integer;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateTo:Ljava/lang/Integer;

    move-object/from16 v30, v0

    const/16 v31, 0x1

    const/16 v32, 0x1

    invoke-static/range {v28 .. v32}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v14

    .line 703
    const/16 v28, 0x0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/index/ContentSearch;->dateFrom:Ljava/lang/Integer;

    .line 704
    const/16 v28, 0x0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/index/ContentSearch;->dateTo:Ljava/lang/Integer;

    .line 706
    :cond_17
    if-eqz v14, :cond_18

    .line 707
    sget-object v28, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v28

    invoke-virtual {v4, v14, v0}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V
    :try_end_b
    .catch Lorg/apache/lucene/queryParser/ParseException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1

    .line 709
    :cond_18
    move-object v15, v4

    move-object v7, v8

    .line 710
    .end local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    goto/16 :goto_8

    .line 711
    .end local v4    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    .end local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    :cond_19
    move-object v15, v12

    move-object v7, v8

    .end local v8    # "inexTerm":Lorg/apache/lucene/index/Term;
    .restart local v7    # "inexTerm":Lorg/apache/lucene/index/Term;
    goto/16 :goto_8

    .line 714
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v11    # "len$":I
    :cond_1a
    :try_start_c
    new-instance v13, Lorg/apache/lucene/queryParser/QueryParser;

    sget-object v28, Lorg/apache/lucene/util/Version;->LUCENE_36:Lorg/apache/lucene/util/Version;

    const-string/jumbo v29, "contents"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/index/LuceneHelper;->getCurrentAnalyser()Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-direct {v13, v0, v1, v2}, Lorg/apache/lucene/queryParser/QueryParser;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 717
    .local v13, "parser":Lorg/apache/lucene/queryParser/QueryParser;
    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lorg/apache/lucene/queryParser/QueryParser;->parse(Ljava/lang/String;)Lorg/apache/lucene/search/Query;

    move-result-object v16

    .line 718
    .local v16, "query1":Lorg/apache/lucene/search/Query;
    if-eqz p4, :cond_1e

    invoke-virtual/range {p4 .. p4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v28

    if-nez v28, :cond_1e

    .line 719
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/samsung/index/ContentSearch;->getQueryForAdvancedSearch(Ljava/util/HashMap;Z)Lorg/apache/lucene/search/BooleanQuery;

    move-result-object v4

    .line 722
    .restart local v4    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    if-eqz v16, :cond_1b

    .line 723
    sget-object v28, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v16

    move-object/from16 v1, v28

    invoke-virtual {v4, v0, v1}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 725
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateFrom:Ljava/lang/Integer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    if-eqz v28, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateTo:Ljava/lang/Integer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    if-eqz v28, :cond_1c

    .line 726
    const-string/jumbo v28, "modifiedTime"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateFrom:Ljava/lang/Integer;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->dateTo:Ljava/lang/Integer;

    move-object/from16 v30, v0

    const/16 v31, 0x1

    const/16 v32, 0x1

    invoke-static/range {v28 .. v32}, Lorg/apache/lucene/search/NumericRangeQuery;->newIntRange(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;ZZ)Lorg/apache/lucene/search/NumericRangeQuery;

    move-result-object v14

    .line 729
    const/16 v28, 0x0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/index/ContentSearch;->dateFrom:Ljava/lang/Integer;

    .line 730
    const/16 v28, 0x0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/index/ContentSearch;->dateTo:Ljava/lang/Integer;

    .line 732
    :cond_1c
    if-eqz v14, :cond_1d

    .line 733
    sget-object v28, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    move-object/from16 v0, v28

    invoke-virtual {v4, v14, v0}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V
    :try_end_c
    .catch Lorg/apache/lucene/queryParser/ParseException; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2

    .line 735
    :cond_1d
    move-object v15, v4

    .line 736
    goto/16 :goto_8

    .line 737
    .end local v4    # "bq":Lorg/apache/lucene/search/BooleanQuery;
    :cond_1e
    move-object/from16 v15, v16

    goto/16 :goto_8

    .line 747
    .end local v13    # "parser":Lorg/apache/lucene/queryParser/QueryParser;
    .end local v16    # "query1":Lorg/apache/lucene/search/Query;
    :catch_2
    move-exception v5

    goto/16 :goto_7

    .end local v24    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    .restart local v20    # "term":Lorg/apache/lucene/index/Term;
    .restart local v25    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    :catch_3
    move-exception v5

    move-object/from16 v24, v25

    .end local v25    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    .restart local v24    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    goto/16 :goto_7

    .line 745
    .end local v20    # "term":Lorg/apache/lucene/index/Term;
    :catch_4
    move-exception v5

    goto/16 :goto_3

    .end local v24    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    .restart local v20    # "term":Lorg/apache/lucene/index/Term;
    .restart local v25    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    :catch_5
    move-exception v5

    move-object/from16 v24, v25

    .end local v25    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    .restart local v24    # "wcquery":Lorg/apache/lucene/search/WildcardQuery;
    goto/16 :goto_3
.end method

.method private getQueryForAdvancedSearch(Ljava/util/HashMap;Z)Lorg/apache/lucene/search/BooleanQuery;
    .locals 8
    .param p2, "searchSubFolders"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)",
            "Lorg/apache/lucene/search/BooleanQuery;"
        }
    .end annotation

    .prologue
    .line 761
    .local p1, "keyValues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanQuery;-><init>()V

    .line 763
    .local v0, "bq":Lorg/apache/lucene/search/BooleanQuery;
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 765
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 766
    .local v3, "inex_term1":Lorg/apache/lucene/index/Term;
    if-eqz p2, :cond_0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string/jumbo v6, "parent"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 767
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const/4 v7, 0x1

    invoke-direct {p0, v5, v6, v0, v7}, Lcom/samsung/index/ContentSearch;->addParentKey(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/search/BooleanQuery;Z)V

    goto :goto_0

    .line 768
    :cond_0
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string/jumbo v6, "parent"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 769
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const/4 v7, 0x0

    invoke-direct {p0, v5, v6, v0, v7}, Lcom/samsung/index/ContentSearch;->addParentKey(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/search/BooleanQuery;Z)V

    goto :goto_0

    .line 770
    :cond_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string/jumbo v6, "modifiedfromTime"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 771
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/index/ContentSearch;->dateFrom:Ljava/lang/Integer;

    goto :goto_0

    .line 772
    :cond_2
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string/jumbo v6, "modifiedtoTime"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 773
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/index/ContentSearch;->dateTo:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 776
    :cond_3
    new-instance v4, Lorg/apache/lucene/search/MultiPhraseQuery;

    invoke-direct {v4}, Lorg/apache/lucene/search/MultiPhraseQuery;-><init>()V

    .line 777
    .local v4, "multiPhraseQuery":Lorg/apache/lucene/search/MultiPhraseQuery;
    new-instance v3, Lorg/apache/lucene/index/Term;

    .end local v3    # "inex_term1":Lorg/apache/lucene/index/Term;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {v3, v5, v6}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    .restart local v3    # "inex_term1":Lorg/apache/lucene/index/Term;
    invoke-virtual {v4, v3}, Lorg/apache/lucene/search/MultiPhraseQuery;->add(Lorg/apache/lucene/index/Term;)V

    .line 779
    sget-object v5, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    goto/16 :goto_0

    .line 786
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "inex_term1":Lorg/apache/lucene/index/Term;
    .end local v4    # "multiPhraseQuery":Lorg/apache/lucene/search/MultiPhraseQuery;
    :cond_4
    return-object v0
.end method

.method private getSearchableParentPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 850
    const-string/jumbo v0, "/"

    .line 851
    .local v0, "FOLDER_SEPERATOR":Ljava/lang/String;
    const-string/jumbo v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 852
    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 854
    :cond_0
    return-object p1
.end method

.method private initSearch()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/index/ContentSearch;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    invoke-virtual {v0}, Lcom/samsung/index/LuceneHelper;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/index/ContentSearch;->mReader:Lorg/apache/lucene/index/IndexReader;

    .line 138
    new-instance v0, Lorg/apache/lucene/search/IndexSearcher;

    iget-object v1, p0, Lcom/samsung/index/ContentSearch;->mReader:Lorg/apache/lucene/index/IndexReader;

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    iput-object v0, p0, Lcom/samsung/index/ContentSearch;->mSearcher:Lorg/apache/lucene/search/IndexSearcher;

    .line 139
    return-void
.end method

.method private setEncryptionAlgorithmPassword(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "algorithm"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 129
    iput-object p1, p0, Lcom/samsung/index/ContentSearch;->mEncryptionAlgorithm:Ljava/lang/String;

    .line 130
    iput-object p2, p0, Lcom/samsung/index/ContentSearch;->mEncryptionPassword:Ljava/lang/String;

    .line 132
    iget-object v0, p0, Lcom/samsung/index/ContentSearch;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    iget-object v1, p0, Lcom/samsung/index/ContentSearch;->mEncryptionAlgorithm:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/index/ContentSearch;->mEncryptionPassword:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/index/LuceneHelper;->encryptionForIndexes(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    return-void
.end method


# virtual methods
.method public searchTextContent(Ljava/lang/String;ZZLjava/util/HashMap;)Ljava/util/ArrayList;
    .locals 18
    .param p1, "queryString"    # Ljava/lang/String;
    .param p2, "isExactmatch"    # Z
    .param p3, "searchSubFolders"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    .local p4, "keyvalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 190
    .local v7, "foundFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/index/ContentSearch;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    invoke-virtual {v15}, Lcom/samsung/index/LuceneHelper;->getCurrentDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v15

    if-nez v15, :cond_1

    .line 264
    :cond_0
    :goto_0
    return-object v7

    .line 192
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    .line 194
    .local v10, "queryStr":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v15

    if-eqz v15, :cond_0

    .line 199
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/ContentSearch;->initSearch()V

    .line 200
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move-object/from16 v3, p4

    invoke-direct {v0, v10, v1, v2, v3}, Lcom/samsung/index/ContentSearch;->getHitDocResults(Ljava/lang/String;ZZLjava/util/HashMap;)Lcom/samsung/index/ContentSearch$HitResult;

    move-result-object v11

    .line 203
    .local v11, "resultHit":Lcom/samsung/index/ContentSearch$HitResult;
    iget-object v14, v11, Lcom/samsung/index/ContentSearch$HitResult;->topDocs:Lorg/apache/lucene/search/TopDocs;

    .line 205
    .local v14, "topDocs":Lorg/apache/lucene/search/TopDocs;
    iget-object v8, v14, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .line 208
    .local v8, "hits":[Lorg/apache/lucene/search/ScoreDoc;
    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    .line 210
    .local v13, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string/jumbo v15, "ContentSearch"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Found "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    array-length v0, v8

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string/jumbo v17, " results."

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    array-length v15, v8

    if-ge v9, v15, :cond_3

    .line 212
    aget-object v15, v8, v9

    iget v5, v15, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    .line 213
    .local v5, "docId":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/index/ContentSearch;->mSearcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v15, v5}, Lorg/apache/lucene/search/IndexSearcher;->doc(I)Lorg/apache/lucene/document/Document;

    move-result-object v4

    .line 214
    .local v4, "d":Lorg/apache/lucene/document/Document;
    const-string/jumbo v15, "filepath"

    invoke-virtual {v4, v15}, Lorg/apache/lucene/document/Document;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 215
    .local v12, "searchParam":Ljava/lang/String;
    if-eqz v12, :cond_2

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v15

    if-eqz v15, :cond_2

    .line 218
    invoke-interface {v13, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 219
    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 261
    .end local v4    # "d":Lorg/apache/lucene/document/Document;
    .end local v5    # "docId":I
    .end local v12    # "searchParam":Ljava/lang/String;
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/ContentSearch;->closeSearch()V

    goto :goto_0

    .line 225
    .end local v8    # "hits":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v9    # "i":I
    .end local v11    # "resultHit":Lcom/samsung/index/ContentSearch$HitResult;
    .end local v13    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v14    # "topDocs":Lorg/apache/lucene/search/TopDocs;
    :catch_0
    move-exception v6

    .line 226
    .local v6, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v15, "ContentSearch"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "Exception in reading: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 261
    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/ContentSearch;->closeSearch()V

    goto/16 :goto_0

    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v15

    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/ContentSearch;->closeSearch()V

    throw v15
.end method

.method public searchTextContentWithHighlighted(Ljava/lang/String;ZZLjava/util/HashMap;)Ljava/util/HashMap;
    .locals 26
    .param p1, "queryStr"    # Ljava/lang/String;
    .param p2, "isExactmatch"    # Z
    .param p3, "searchSubFolders"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    .local p4, "keyvalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 272
    .local v9, "foundFiles":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->mLuceneMgr:Lcom/samsung/index/LuceneHelper;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/index/LuceneHelper;->getCurrentDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v21

    if-nez v21, :cond_1

    .line 368
    :cond_0
    :goto_0
    return-object v9

    .line 274
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 276
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v21

    if-eqz v21, :cond_0

    .line 281
    :try_start_0
    const-string/jumbo v21, "ContentSearch"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v25, "Perfom document content search  for : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string/jumbo v25, " "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/ContentSearch;->initSearch()V

    .line 284
    invoke-direct/range {p0 .. p4}, Lcom/samsung/index/ContentSearch;->getHitDocResults(Ljava/lang/String;ZZLjava/util/HashMap;)Lcom/samsung/index/ContentSearch$HitResult;

    move-result-object v15

    .line 287
    .local v15, "resultHit":Lcom/samsung/index/ContentSearch$HitResult;
    iget-object v0, v15, Lcom/samsung/index/ContentSearch$HitResult;->topDocs:Lorg/apache/lucene/search/TopDocs;

    move-object/from16 v20, v0

    .line 289
    .local v20, "topDocs":Lorg/apache/lucene/search/TopDocs;
    move-object/from16 v0, v20

    iget-object v10, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .line 292
    .local v10, "hits":[Lorg/apache/lucene/search/ScoreDoc;
    new-instance v17, Ljava/util/HashSet;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashSet;-><init>()V

    .line 293
    .local v17, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-wide/16 v22, 0x0

    .line 295
    .local v22, "total":J
    const-string/jumbo v21, "ContentSearch"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v25, "Found "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    array-length v0, v10

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string/jumbo v25, " results."

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "[\u000c|\r|\n|\t|\r\n|\u000b|\' \']"

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v14

    .line 298
    .local v14, "pattern":Ljava/util/regex/Pattern;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    array-length v0, v10

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v11, v0, :cond_5

    .line 299
    aget-object v21, v10, v11

    move-object/from16 v0, v21

    iget v7, v0, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    .line 300
    .local v7, "docId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/index/ContentSearch;->mSearcher:Lorg/apache/lucene/search/IndexSearcher;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Lorg/apache/lucene/search/IndexSearcher;->doc(I)Lorg/apache/lucene/document/Document;

    move-result-object v6

    .line 301
    .local v6, "d":Lorg/apache/lucene/document/Document;
    const-string/jumbo v21, "filepath"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Lorg/apache/lucene/document/Document;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 302
    .local v16, "search_param":Ljava/lang/String;
    if-eqz v16, :cond_2

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v21

    if-eqz v21, :cond_2

    .line 304
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_3

    .line 298
    :cond_2
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 307
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 309
    .local v18, "startTime":J
    const-string/jumbo v21, "hlcontents"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Lorg/apache/lucene/document/Document;->getBinaryValue(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 311
    .local v4, "Content":[B
    const/4 v5, 0x0

    .line 313
    .local v5, "contents":Ljava/lang/String;
    if-eqz v4, :cond_4

    .line 315
    :try_start_1
    invoke-static {v4}, Lorg/apache/lucene/document/CompressionTools;->decompressString([B)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 322
    :cond_4
    :goto_3
    :try_start_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v5, v1, v14}, Lcom/samsung/index/ContentSearch;->getHighlightedText(Ljava/lang/String;Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 326
    .local v12, "last":J
    sub-long v24, v12, v18

    add-long v22, v22, v24

    goto :goto_2

    .line 317
    .end local v12    # "last":J
    :catch_0
    move-exception v8

    .line 318
    .local v8, "e":Ljava/lang/Exception;
    const-string/jumbo v21, "ContentSearch"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v25, "Exception: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 330
    .end local v4    # "Content":[B
    .end local v5    # "contents":Ljava/lang/String;
    .end local v6    # "d":Lorg/apache/lucene/document/Document;
    .end local v7    # "docId":I
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v10    # "hits":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v11    # "i":I
    .end local v14    # "pattern":Ljava/util/regex/Pattern;
    .end local v15    # "resultHit":Lcom/samsung/index/ContentSearch$HitResult;
    .end local v16    # "search_param":Ljava/lang/String;
    .end local v17    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v18    # "startTime":J
    .end local v20    # "topDocs":Lorg/apache/lucene/search/TopDocs;
    .end local v22    # "total":J
    :catch_1
    move-exception v8

    .line 331
    .restart local v8    # "e":Ljava/lang/Exception;
    :try_start_3
    const-string/jumbo v21, "ContentSearch"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v25, "Exception in reading: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 366
    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/ContentSearch;->closeSearch()V

    goto/16 :goto_0

    .line 329
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v10    # "hits":[Lorg/apache/lucene/search/ScoreDoc;
    .restart local v11    # "i":I
    .restart local v14    # "pattern":Ljava/util/regex/Pattern;
    .restart local v15    # "resultHit":Lcom/samsung/index/ContentSearch$HitResult;
    .restart local v17    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v20    # "topDocs":Lorg/apache/lucene/search/TopDocs;
    .restart local v22    # "total":J
    :cond_5
    :try_start_4
    const-string/jumbo v21, "ContentSearch"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v25, "Lucene Total time for Highlighted Content : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 366
    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/ContentSearch;->closeSearch()V

    goto/16 :goto_0

    .end local v10    # "hits":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v11    # "i":I
    .end local v14    # "pattern":Ljava/util/regex/Pattern;
    .end local v15    # "resultHit":Lcom/samsung/index/ContentSearch$HitResult;
    .end local v17    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v20    # "topDocs":Lorg/apache/lucene/search/TopDocs;
    .end local v22    # "total":J
    :catchall_0
    move-exception v21

    invoke-direct/range {p0 .. p0}, Lcom/samsung/index/ContentSearch;->closeSearch()V

    throw v21
.end method

.method public setMaxHit(I)V
    .locals 0
    .param p1, "maxHits"    # I

    .prologue
    .line 166
    iput p1, p0, Lcom/samsung/index/ContentSearch;->mMaxHits:I

    .line 167
    return-void
.end method
