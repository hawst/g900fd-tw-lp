.class Lcom/samsung/index/LuceneHelper$ConcurrentMergeScedulerCustom;
.super Lorg/apache/lucene/index/ConcurrentMergeScheduler;
.source "LuceneHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/index/LuceneHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConcurrentMergeScedulerCustom"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/index/LuceneHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/index/LuceneHelper$1;

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/samsung/index/LuceneHelper$ConcurrentMergeScedulerCustom;-><init>()V

    return-void
.end method


# virtual methods
.method protected doMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 3
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .prologue
    .line 171
    :try_start_0
    const-string/jumbo v1, "LuceneHelper"

    const-string/jumbo v2, "Lucene doMerge is started"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    invoke-super {p0, p1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->doMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 173
    const-string/jumbo v1, "LuceneHelper"

    const-string/jumbo v2, "Lucene doMerge is ended"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :goto_0
    return-void

    .line 174
    :catch_0
    move-exception v0

    .line 175
    .local v0, "e":Ljava/io/IOException;
    instance-of v1, v0, Lorg/apache/lucene/index/CorruptIndexException;

    if-eqz v1, :cond_0

    .line 176
    const-string/jumbo v1, "LuceneHelper"

    const-string/jumbo v2, "CorruptIndexException on merge"

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 178
    :cond_0
    const-string/jumbo v1, "LuceneHelper"

    const-string/jumbo v2, "IOException on merge"

    invoke-static {v1, v2}, Lcom/samsung/index/indexservice/Slog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
