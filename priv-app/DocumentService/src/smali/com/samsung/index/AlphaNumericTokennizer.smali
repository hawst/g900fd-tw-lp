.class public final Lcom/samsung/index/AlphaNumericTokennizer;
.super Lorg/apache/lucene/analysis/CharTokenizer;
.source "AlphaNumericTokennizer.java"


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 0
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Ljava/io/Reader;)V

    .line 12
    return-void
.end method


# virtual methods
.method public isTokenChar(C)Z
    .locals 1
    .param p1, "c"    # C

    .prologue
    .line 19
    invoke-static {p1}, Ljava/lang/Character;->isDefined(C)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
