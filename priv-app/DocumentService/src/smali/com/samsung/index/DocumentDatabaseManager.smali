.class public Lcom/samsung/index/DocumentDatabaseManager;
.super Ljava/lang/Object;
.source "DocumentDatabaseManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/index/DocumentDatabaseManager$LocalDatabaseHelper;
    }
.end annotation


# static fields
.field public static final CORRUPTED:I = 0x3

.field public static final DATABASE_NAME:Ljava/lang/String; = "docservice.db"

.field public static final DATABASE_VERSION:I = 0x2

.field public static final DELETE_BOTH:I = 0x1

.field public static final DELETE_DEFAULT:I = 0x0

.field public static final DELETE_INDEX:I = 0x2

.field public static final DELETE_STATUS:Ljava/lang/String; = "delete_status"

.field public static final DOC_ACCOUNT_ID:Ljava/lang/String; = "account_id"

.field public static final DOC_SERVICE_TABLE:Ljava/lang/String; = "document_service"

.field public static final DOC_SERVICE_TABLE_CREATE:Ljava/lang/String; = "CREATE TABLE document_service (file_path VARCHAR PRIMARY KEY  NOT NULL UNIQUE COLLATE NOCASE, last_indexed_time INTEGER , last_modified_file_time INTEGER, account_id VARCHAR, index_status INTEGER default -1, thumbnail_status INTEGER default -1, thumbnail_data VARCHAR ,delete_status INTEGER default 0 );"

.field public static final ENCRYPTED_DOC:I = 0x2

.field public static final FILE_PATH:Ljava/lang/String; = "file_path"

.field private static final FOLDER_SEPERATOR:Ljava/lang/String; = "/"

.field public static final INDEX_STATUS:Ljava/lang/String; = "index_status"

.field public static final LAST_INDEXED_TIME:Ljava/lang/String; = "last_indexed_time"

.field public static final LAST_MODIFIED_FILE_TIME:Ljava/lang/String; = "last_modified_file_time"

.field public static final NOT_PROCESSED:I = -0x1

.field public static final NOT_SUPPORTED:I = 0x4

.field private static final NULL_VAL:Ljava/lang/String; = "null"

.field public static final PWD_PROTECTED:I = 0x1

.field public static final SUCCESSFULLY_PROCESSED:I = 0x0

.field private static final TAG:Ljava/lang/String; = "DocumentDatabaseManager"

.field public static final THUMBNAIL_DATA:Ljava/lang/String; = "thumbnail_data"

.field public static final THUMBNAIL_STATUS:Ljava/lang/String; = "thumbnail_status"

.field public static final UNKNOWN:I = -0x2

.field private static sInstance:Lcom/samsung/index/DocumentDatabaseManager;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDatabaseOpenHelper:Lcom/samsung/index/DocumentDatabaseManager$LocalDatabaseHelper;

.field private mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "app"    # Landroid/content/Context;

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/index/DocumentDatabaseManager;->mContext:Landroid/content/Context;

    .line 117
    new-instance v0, Lcom/samsung/index/DocumentDatabaseManager$LocalDatabaseHelper;

    iget-object v1, p0, Lcom/samsung/index/DocumentDatabaseManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/index/DocumentDatabaseManager$LocalDatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/index/DocumentDatabaseManager;->mDatabaseOpenHelper:Lcom/samsung/index/DocumentDatabaseManager$LocalDatabaseHelper;

    .line 118
    iget-object v0, p0, Lcom/samsung/index/DocumentDatabaseManager;->mDatabaseOpenHelper:Lcom/samsung/index/DocumentDatabaseManager$LocalDatabaseHelper;

    invoke-virtual {v0}, Lcom/samsung/index/DocumentDatabaseManager$LocalDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 119
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/index/DocumentDatabaseManager;
    .locals 2
    .param p0, "app"    # Landroid/content/Context;

    .prologue
    .line 124
    const-class v1, Lcom/samsung/index/DocumentDatabaseManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/index/DocumentDatabaseManager;->sInstance:Lcom/samsung/index/DocumentDatabaseManager;

    if-nez v0, :cond_0

    if-eqz p0, :cond_0

    .line 125
    new-instance v0, Lcom/samsung/index/DocumentDatabaseManager;

    invoke-direct {v0, p0}, Lcom/samsung/index/DocumentDatabaseManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/index/DocumentDatabaseManager;->sInstance:Lcom/samsung/index/DocumentDatabaseManager;

    .line 127
    :cond_0
    sget-object v0, Lcom/samsung/index/DocumentDatabaseManager;->sInstance:Lcom/samsung/index/DocumentDatabaseManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 498
    iget-object v0, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 500
    iput-object v1, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 501
    const-class v1, Lcom/samsung/index/DocumentDatabaseManager;

    monitor-enter v1

    .line 502
    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/samsung/index/DocumentDatabaseManager;->sInstance:Lcom/samsung/index/DocumentDatabaseManager;

    .line 503
    monitor-exit v1

    .line 505
    :cond_0
    return-void

    .line 503
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized deleteAll()V
    .locals 5

    .prologue
    .line 449
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 451
    .local v1, "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_1
    const-string/jumbo v2, "document_service"

    const-string/jumbo v3, "index_status=0 OR thumbnail_status=0"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457
    :goto_0
    monitor-exit p0

    return-void

    .line 454
    :catch_0
    move-exception v0

    .line 455
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string/jumbo v2, "DocumentDatabaseManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 449
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized deleteFileEntry(Ljava/lang/String;)I
    .locals 7
    .param p1, "filePathStr"    # Ljava/lang/String;

    .prologue
    .line 485
    monitor-enter p0

    :try_start_0
    const-string/jumbo v4, "\'"

    const-string/jumbo v5, "\'\'"

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 486
    .local v3, "strFilePath":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 487
    .local v1, "database":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, -0x1

    .line 489
    .local v0, "count":I
    :try_start_1
    const-string/jumbo v4, "document_service"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "file_path =\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 494
    :goto_0
    monitor-exit p0

    return v0

    .line 491
    :catch_0
    move-exception v2

    .line 492
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    const-string/jumbo v4, "DocumentDatabaseManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 485
    .end local v0    # "count":I
    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "strFilePath":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized deleteIndexedRecordsInFolder(Ljava/lang/String;)I
    .locals 7
    .param p1, "folderPathStr"    # Ljava/lang/String;

    .prologue
    .line 526
    monitor-enter p0

    const/4 v3, 0x0

    .line 527
    .local v3, "strFolderPath":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 528
    :try_start_0
    const-string/jumbo v4, "\'"

    const-string/jumbo v5, "\'\'"

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 529
    :cond_0
    if-nez v3, :cond_1

    .line 530
    const/4 v0, 0x0

    .line 541
    :goto_0
    monitor-exit p0

    return v0

    .line 532
    :cond_1
    const/4 v0, -0x1

    .line 534
    .local v0, "count":I
    :try_start_1
    iget-object v1, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 535
    .local v1, "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string/jumbo v4, "document_service"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "file_path LIKE \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "%\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 537
    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v2

    .line 538
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    const-string/jumbo v4, "DocumentDatabaseManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 539
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    :try_start_3
    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 526
    .end local v0    # "count":I
    :catchall_1
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized deleteStorageEntry(Ljava/lang/String;)I
    .locals 7
    .param p1, "storageId"    # Ljava/lang/String;

    .prologue
    .line 466
    monitor-enter p0

    :try_start_0
    const-string/jumbo v4, "\'"

    const-string/jumbo v5, "\'\'"

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 467
    .local v3, "storageID":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 468
    .local v1, "database":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, -0x1

    .line 470
    .local v0, "count":I
    :try_start_1
    const-string/jumbo v4, "document_service"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "account_id=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 475
    :goto_0
    monitor-exit p0

    return v0

    .line 472
    :catch_0
    move-exception v2

    .line 473
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    const-string/jumbo v4, "DocumentDatabaseManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 466
    .end local v0    # "count":I
    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "storageID":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 510
    invoke-virtual {p0}, Lcom/samsung/index/DocumentDatabaseManager;->close()V

    .line 512
    const-class v1, Lcom/samsung/index/DocumentDatabaseManager;

    monitor-enter v1

    .line 513
    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/samsung/index/DocumentDatabaseManager;->sInstance:Lcom/samsung/index/DocumentDatabaseManager;

    .line 514
    monitor-exit v1

    .line 516
    return-void

    .line 514
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAllThumbnailPath()Landroid/database/Cursor;
    .locals 10

    .prologue
    .line 418
    iget-object v0, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 419
    .local v0, "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 421
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x2

    :try_start_0
    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v3, "file_path"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string/jumbo v3, "thumbnail_data"

    aput-object v3, v2, v1

    .line 422
    .local v2, "strArrColumns":[Ljava/lang/String;
    const-string/jumbo v1, "document_service"

    const-string/jumbo v3, "thumbnail_status = 0"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 428
    .end local v2    # "strArrColumns":[Ljava/lang/String;
    :goto_0
    return-object v8

    .line 425
    :catch_0
    move-exception v9

    .line 426
    .local v9, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "DocumentDatabaseManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception in fetching the DB :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getCursorForAllIndexedFilesInPath(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 720
    const/4 v11, 0x0

    .line 721
    .local v11, "cursor":Landroid/database/Cursor;
    const-string/jumbo v8, "file_path ASC"

    .line 722
    .local v8, "ORDERBY":Ljava/lang/String;
    const-string/jumbo v9, "file_path LIKE ?"

    .line 723
    .local v9, "STORAGE_PATH_INCLUDER":Ljava/lang/String;
    const-string/jumbo v10, "%"

    .line 725
    .local v10, "WILD_CARD":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    .line 726
    .local v4, "storage":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "%"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    .line 729
    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 730
    .local v0, "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v3, "file_path"

    aput-object v3, v2, v1

    .line 732
    .local v2, "strArrColumns":[Ljava/lang/String;
    const-string/jumbo v1, "document_service"

    const-string/jumbo v3, "file_path LIKE ?"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string/jumbo v7, "file_path ASC"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 740
    .end local v0    # "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "strArrColumns":[Ljava/lang/String;
    :goto_0
    return-object v11

    .line 735
    :catch_0
    move-exception v12

    .line 736
    .local v12, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v1, "DocumentDatabaseManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 737
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    throw v1
.end method

.method public getDeletedFileList()Landroid/database/Cursor;
    .locals 10

    .prologue
    .line 432
    iget-object v0, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 433
    .local v0, "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 435
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x2

    :try_start_0
    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v3, "file_path"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string/jumbo v3, "thumbnail_data"

    aput-object v3, v2, v1

    .line 436
    .local v2, "strArrColumns":[Ljava/lang/String;
    const-string/jumbo v1, "document_service"

    const-string/jumbo v3, "delete_status = 1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 442
    .end local v2    # "strArrColumns":[Ljava/lang/String;
    :goto_0
    return-object v8

    .line 439
    :catch_0
    move-exception v9

    .line 440
    .local v9, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "DocumentDatabaseManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception in fetching the DB :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getDocumentsEntryWithTime(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13
    .param p1, "storagePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 672
    const/4 v11, 0x0

    .line 673
    .local v11, "cursor":Landroid/database/Cursor;
    const-string/jumbo v8, "file_path ASC"

    .line 674
    .local v8, "ORDERBY":Ljava/lang/String;
    const-string/jumbo v9, "file_path LIKE ? AND delete_status <> ?"

    .line 675
    .local v9, "STORAGE_PATH":Ljava/lang/String;
    const-string/jumbo v10, "%"

    .line 677
    .local v10, "WILD_CARD":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    .line 678
    .local v4, "selectionArgs":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "%"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    .line 679
    const-string/jumbo v1, "1"

    aput-object v1, v4, v6

    .line 682
    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 683
    .local v0, "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x5

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v3, "file_path"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string/jumbo v3, "last_modified_file_time"

    aput-object v3, v2, v1

    const/4 v1, 0x2

    const-string/jumbo v3, "index_status"

    aput-object v3, v2, v1

    const/4 v1, 0x3

    const-string/jumbo v3, "thumbnail_status"

    aput-object v3, v2, v1

    const/4 v1, 0x4

    const-string/jumbo v3, "delete_status"

    aput-object v3, v2, v1

    .line 686
    .local v2, "strArrColumns":[Ljava/lang/String;
    const-string/jumbo v1, "document_service"

    const-string/jumbo v3, "file_path LIKE ? AND delete_status <> ?"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string/jumbo v7, "file_path ASC"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 694
    .end local v0    # "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "strArrColumns":[Ljava/lang/String;
    :goto_0
    return-object v11

    .line 689
    :catch_0
    move-exception v12

    .line 690
    .local v12, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v1, "DocumentDatabaseManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception DB query: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 691
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    throw v1
.end method

.method public getIndexedSubFolders(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 18
    .param p1, "pathtoFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 600
    const-string/jumbo v2, "\'"

    const-string/jumbo v4, "\'\'"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 601
    .local v14, "pathToFile":Ljava/lang/String;
    const/4 v9, 0x0

    .line 603
    .local v9, "cursor":Landroid/database/Cursor;
    new-instance v17, Ljava/util/ArrayList;

    const/16 v2, 0x10

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 606
    .local v17, "subFoldersIndexed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 608
    .local v1, "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v4, "file_path"

    aput-object v4, v3, v2

    .line 609
    .local v3, "strArrColumns":[Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    const/16 v2, 0x64

    invoke-direct {v15, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 611
    .local v15, "querybuilder":Ljava/lang/StringBuilder;
    const-string/jumbo v2, "file_path"

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, " LIKE "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v4, 0x27

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "/%/%"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v4, 0x27

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 625
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v13

    .line 627
    .local v13, "parentLength":I
    const-string/jumbo v2, "document_service"

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 630
    if-eqz v9, :cond_2

    .line 631
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 633
    :cond_0
    const-string/jumbo v2, "file_path"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 636
    .local v11, "filePath":Ljava/lang/String;
    add-int/lit8 v2, v13, 0x1

    invoke-virtual {v11, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    .line 638
    .local v16, "subDirORFile":Ljava/lang/String;
    const-string/jumbo v2, "/"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 640
    const/4 v2, 0x0

    const-string/jumbo v4, "/"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v4, v13

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v11, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 646
    .local v12, "folderPath":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 647
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 649
    .end local v12    # "folderPath":Ljava/lang/String;
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 658
    .end local v11    # "filePath":Ljava/lang/String;
    .end local v16    # "subDirORFile":Ljava/lang/String;
    :cond_2
    if-eqz v9, :cond_3

    .line 659
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 662
    .end local v1    # "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    .end local v3    # "strArrColumns":[Ljava/lang/String;
    .end local v13    # "parentLength":I
    .end local v15    # "querybuilder":Ljava/lang/StringBuilder;
    :cond_3
    :goto_0
    return-object v17

    .line 655
    :catch_0
    move-exception v10

    .line 656
    .local v10, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v2, "DocumentDatabaseManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 658
    if-eqz v9, :cond_3

    .line 659
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 658
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v9, :cond_4

    .line 659
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2
.end method

.method public getLastIndexedFileModifiedTime(Ljava/lang/String;)J
    .locals 18
    .param p1, "storagePath"    # Ljava/lang/String;

    .prologue
    .line 745
    const/4 v13, 0x0

    .line 746
    .local v13, "cursor":Landroid/database/Cursor;
    const-string/jumbo v10, "file_path ASC"

    .line 747
    .local v10, "ORDERBY":Ljava/lang/String;
    const-string/jumbo v11, "file_path LIKE ?"

    .line 748
    .local v11, "STORAGE_PATH_INCLUDER":Ljava/lang/String;
    const-string/jumbo v12, "%"

    .line 749
    .local v12, "WILD_CARD":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v5, "MAX(last_modified_file_time)"

    aput-object v5, v4, v3

    .line 751
    .local v4, "strArrColumns":[Ljava/lang/String;
    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/String;

    .line 752
    .local v6, "storage":[Ljava/lang/String;
    const/4 v3, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, "%"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v6, v3

    .line 754
    const-wide/16 v16, -0x1

    .line 757
    .local v16, "lastIndexedTime":J
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 758
    .local v2, "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    const-string/jumbo v3, "document_service"

    const-string/jumbo v5, "file_path LIKE ?"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string/jumbo v9, "file_path ASC"

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 761
    if-eqz v13, :cond_0

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 762
    const/4 v3, 0x0

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v16

    .line 769
    :cond_0
    if-eqz v13, :cond_1

    .line 770
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 775
    .end local v2    # "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    :cond_1
    :goto_0
    return-wide v16

    .line 764
    :catch_0
    move-exception v14

    .line 765
    .local v14, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v3, "DocumentDatabaseManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "ERROR while getting the last modified time: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v14}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 769
    if-eqz v13, :cond_1

    .line 770
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 769
    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v13, :cond_2

    .line 770
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v3
.end method

.method public getLastMidifiedFileTime(Ljava/lang/String;)J
    .locals 11
    .param p1, "filePathStr"    # Ljava/lang/String;

    .prologue
    .line 340
    const-string/jumbo v1, "\'"

    const-string/jumbo v3, "\'\'"

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 341
    .local v10, "filePath":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 342
    .local v0, "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 344
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x1

    :try_start_0
    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v3, "last_modified_file_time"

    aput-object v3, v2, v1

    .line 345
    .local v2, "strArrColumns":[Ljava/lang/String;
    const-string/jumbo v1, "document_service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "file_path = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 349
    if-eqz v8, :cond_1

    .line 350
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 351
    const-string/jumbo v1, "last_modified_file_time"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    .line 359
    if-eqz v8, :cond_0

    .line 360
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 363
    .end local v2    # "strArrColumns":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-wide v4

    .line 359
    .restart local v2    # "strArrColumns":[Ljava/lang/String;
    :cond_1
    if-eqz v8, :cond_2

    .line 360
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 363
    .end local v2    # "strArrColumns":[Ljava/lang/String;
    :cond_2
    :goto_1
    const-wide/16 v4, 0x0

    goto :goto_0

    .line 355
    :catch_0
    move-exception v9

    .line 356
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v1, "DocumentDatabaseManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception in getLastMidifiedFileTime :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 359
    if-eqz v8, :cond_2

    .line 360
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 359
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_3

    .line 360
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method public getLastModifiedFileTimes(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 19
    .param p1, "pathtoFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 547
    const-string/jumbo v3, "\'"

    const-string/jumbo v5, "\'\'"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 548
    .local v15, "pathToFile":Ljava/lang/String;
    const/4 v10, 0x0

    .line 550
    .local v10, "cursor":Landroid/database/Cursor;
    const/4 v13, 0x0

    .line 553
    .local v13, "fileToModifiedTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 555
    .local v2, "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v5, "file_path"

    aput-object v5, v4, v3

    const/4 v3, 0x1

    const-string/jumbo v5, "last_modified_file_time"

    aput-object v5, v4, v3

    .line 556
    .local v4, "strArrColumns":[Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    const/16 v3, 0x64

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 559
    .local v18, "querybuilder":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "file_path"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, " LIKE "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v5, 0x27

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "/%"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v5, 0x27

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 562
    const-string/jumbo v3, " AND "

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "file_path"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, " NOT LIKE "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v5, 0x27

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "/%/%"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v5, 0x27

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 566
    const-string/jumbo v3, "document_service"

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 569
    if-eqz v10, :cond_2

    .line 570
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572
    .end local v13    # "fileToModifiedTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .local v14, "fileToModifiedTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 574
    :cond_0
    const-string/jumbo v3, "file_path"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 576
    .local v12, "filePath":Ljava/lang/String;
    const-string/jumbo v3, "last_modified_file_time"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 581
    .local v16, "modifiedTime":J
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v14, v12, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 582
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    if-nez v3, :cond_0

    .end local v12    # "filePath":Ljava/lang/String;
    .end local v16    # "modifiedTime":J
    :cond_1
    move-object v13, v14

    .line 590
    .end local v14    # "fileToModifiedTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .restart local v13    # "fileToModifiedTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    :cond_2
    if-eqz v10, :cond_3

    .line 591
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 594
    .end local v2    # "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "strArrColumns":[Ljava/lang/String;
    .end local v18    # "querybuilder":Ljava/lang/StringBuilder;
    :cond_3
    :goto_0
    return-object v13

    .line 587
    :catch_0
    move-exception v11

    .line 588
    .local v11, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    const-string/jumbo v3, "DocumentDatabaseManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 590
    if-eqz v10, :cond_3

    .line 591
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 590
    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    :goto_2
    if-eqz v10, :cond_4

    .line 591
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v3

    .line 590
    .end local v13    # "fileToModifiedTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .restart local v2    # "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v4    # "strArrColumns":[Ljava/lang/String;
    .restart local v14    # "fileToModifiedTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .restart local v18    # "querybuilder":Ljava/lang/StringBuilder;
    :catchall_1
    move-exception v3

    move-object v13, v14

    .end local v14    # "fileToModifiedTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .restart local v13    # "fileToModifiedTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    goto :goto_2

    .line 587
    .end local v13    # "fileToModifiedTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .restart local v14    # "fileToModifiedTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    :catch_1
    move-exception v11

    move-object v13, v14

    .end local v14    # "fileToModifiedTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .restart local v13    # "fileToModifiedTimeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    goto :goto_1
.end method

.method public getProcessedDocCursor()Landroid/database/Cursor;
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 698
    const/4 v10, 0x0

    .line 699
    .local v10, "cursor":Landroid/database/Cursor;
    const-string/jumbo v8, "file_path ASC"

    .line 700
    .local v8, "ORDERBY":Ljava/lang/String;
    const-string/jumbo v9, "thumbnail_data = ?"

    .line 702
    .local v9, "STORAGE_PATH_INCLUDER":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    .line 703
    .local v4, "storage":[Ljava/lang/String;
    const-string/jumbo v1, "0"

    aput-object v1, v4, v3

    .line 706
    :try_start_0
    iget-object v0, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 707
    .local v0, "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v3, "file_path"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string/jumbo v3, "last_modified_file_time"

    aput-object v3, v2, v1

    .line 709
    .local v2, "strArrColumns":[Ljava/lang/String;
    const-string/jumbo v1, "document_service"

    const-string/jumbo v3, "thumbnail_data = ?"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string/jumbo v7, "file_path ASC"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 715
    .end local v0    # "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "strArrColumns":[Ljava/lang/String;
    :goto_0
    return-object v10

    .line 712
    :catch_0
    move-exception v11

    .line 713
    .local v11, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "DocumentDatabaseManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getThumbnailPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "filePathStr"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    .line 375
    const-string/jumbo v1, "\'"

    const-string/jumbo v3, "\'\'"

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 376
    .local v10, "filePath":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 378
    .local v0, "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 382
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_0

    .line 383
    :try_start_0
    iget-object v1, p0, Lcom/samsung/index/DocumentDatabaseManager;->mDatabaseOpenHelper:Lcom/samsung/index/DocumentDatabaseManager$LocalDatabaseHelper;

    if-eqz v1, :cond_2

    .line 384
    iget-object v1, p0, Lcom/samsung/index/DocumentDatabaseManager;->mDatabaseOpenHelper:Lcom/samsung/index/DocumentDatabaseManager$LocalDatabaseHelper;

    invoke-virtual {v1}, Lcom/samsung/index/DocumentDatabaseManager$LocalDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 385
    iget-object v0, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 391
    :cond_0
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v3, "thumbnail_data"

    aput-object v3, v2, v1

    .line 392
    .local v2, "strArrColumns":[Ljava/lang/String;
    const-string/jumbo v1, "document_service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "file_path = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 396
    if-eqz v8, :cond_4

    .line 397
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 398
    const-string/jumbo v1, "thumbnail_data"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 405
    if-eqz v8, :cond_1

    .line 406
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 409
    .end local v2    # "strArrColumns":[Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v1

    .line 405
    :cond_2
    if-eqz v8, :cond_3

    .line 406
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v1, v11

    goto :goto_0

    .line 405
    .restart local v2    # "strArrColumns":[Ljava/lang/String;
    :cond_4
    if-eqz v8, :cond_5

    .line 406
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .end local v2    # "strArrColumns":[Ljava/lang/String;
    :cond_5
    :goto_1
    move-object v1, v11

    .line 409
    goto :goto_0

    .line 402
    :catch_0
    move-exception v9

    .line 403
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v1, "DocumentDatabaseManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception in getThumbnailPath :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 405
    if-eqz v8, :cond_5

    .line 406
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 405
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_6

    .line 406
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v1
.end method

.method public handleIndexFolderDeletion()J
    .locals 8

    .prologue
    .line 137
    iget-object v3, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 138
    .local v3, "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 140
    const-string/jumbo v5, "UPDATE document_service SET index_status = ? WHERE index_status = ?"

    invoke-virtual {v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v4

    .line 143
    .local v4, "stmtSelectRec":Landroid/database/sqlite/SQLiteStatement;
    const/4 v5, 0x1

    const-wide/16 v6, -0x1

    invoke-virtual {v4, v5, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 144
    const/4 v5, 0x2

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 146
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v5

    int-to-long v0, v5

    .line 148
    .local v0, "countRec":J
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 150
    :try_start_0
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 157
    :goto_0
    return-wide v0

    .line 151
    :catch_0
    move-exception v2

    .line 152
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v5, "DocumentDatabaseManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception in endTransaction: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->close()V

    goto :goto_0

    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v5
.end method

.method public declared-synchronized markForDelete(Ljava/lang/String;I)V
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "deleteStatus"    # I

    .prologue
    .line 172
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    .local v2, "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    .line 176
    .local v3, "stmtUpdate":Landroid/database/sqlite/SQLiteStatement;
    :try_start_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 177
    packed-switch p2, :pswitch_data_0

    .line 194
    const-string/jumbo v4, "UPDATE document_service SET delete_status = ? WHERE file_path = ?"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    .line 198
    const/4 v4, 0x1

    int-to-long v6, p2

    invoke-virtual {v3, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 199
    const/4 v4, 0x2

    invoke-virtual {v3, v4, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 203
    :goto_0
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    .line 204
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 210
    if-eqz v3, :cond_0

    .line 211
    :try_start_2
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 214
    :cond_0
    :try_start_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 219
    :goto_1
    monitor-exit p0

    return-void

    .line 179
    :pswitch_0
    :try_start_4
    const-string/jumbo v4, "UPDATE document_service SET last_modified_file_time = ?, index_status = ?, thumbnail_status = ?, thumbnail_data = ?, delete_status = ? WHERE file_path = ?"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    .line 185
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 186
    .local v1, "file":Ljava/io/File;
    const/4 v4, 0x1

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-virtual {v3, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 187
    const/4 v4, 0x2

    const-wide/16 v6, -0x1

    invoke-virtual {v3, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 188
    const/4 v4, 0x3

    const-wide/16 v6, -0x1

    invoke-virtual {v3, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 189
    const/4 v4, 0x4

    const-string/jumbo v5, "null"

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 190
    const/4 v4, 0x5

    int-to-long v6, p2

    invoke-virtual {v3, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 191
    const/4 v4, 0x6

    invoke-virtual {v3, v4, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 205
    .end local v1    # "file":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/Exception;
    :try_start_5
    const-string/jumbo v4, "DocumentDatabaseManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception in updating delete flag in DB: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 210
    if-eqz v3, :cond_1

    .line 211
    :try_start_6
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 214
    :cond_1
    :try_start_7
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 215
    :catch_1
    move-exception v0

    .line 216
    :try_start_8
    const-string/jumbo v4, "DocumentDatabaseManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception in closing DB: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    .line 172
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    .end local v3    # "stmtUpdate":Landroid/database/sqlite/SQLiteStatement;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 215
    .restart local v2    # "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v3    # "stmtUpdate":Landroid/database/sqlite/SQLiteStatement;
    :catch_2
    move-exception v0

    .line 216
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_9
    const-string/jumbo v4, "DocumentDatabaseManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception in closing DB: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 210
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v4

    if-eqz v3, :cond_2

    .line 211
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 214
    :cond_2
    :try_start_a
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 217
    :goto_2
    :try_start_b
    throw v4

    .line 215
    :catch_3
    move-exception v0

    .line 216
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string/jumbo v5, "DocumentDatabaseManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception in closing DB: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_2

    .line 177
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized updateEntryInDB(Ljava/lang/String;IZI)V
    .locals 6
    .param p1, "strFilePath"    # Ljava/lang/String;
    .param p2, "indexStatus"    # I
    .param p3, "isThumbnail"    # Z
    .param p4, "thumbnailStatus"    # I

    .prologue
    .line 162
    monitor-enter p0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/index/DocumentDatabaseManager;->updateEntryInDB(Ljava/lang/String;IZILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    monitor-exit p0

    return-void

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized updateEntryInDB(Ljava/lang/String;IZILjava/lang/String;)V
    .locals 18
    .param p1, "strFilePath"    # Ljava/lang/String;
    .param p2, "indexStatus"    # I
    .param p3, "isThumbnail"    # Z
    .param p4, "thumbnailStatus"    # I
    .param p5, "thumnailPath"    # Ljava/lang/String;

    .prologue
    .line 229
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/index/DocumentDatabaseManager;->mObjSqliteD:Landroid/database/sqlite/SQLiteDatabase;

    .line 232
    .local v7, "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v11, 0x0

    .line 233
    .local v11, "stmtSelectRec":Landroid/database/sqlite/SQLiteStatement;
    const/4 v10, 0x0

    .line 234
    .local v10, "stmtInsert":Landroid/database/sqlite/SQLiteStatement;
    const/4 v12, 0x0

    .line 236
    .local v12, "stmtUpdate":Landroid/database/sqlite/SQLiteStatement;
    if-nez p5, :cond_0

    .line 237
    const-string/jumbo p5, "null"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    :cond_0
    :try_start_1
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 243
    const-string/jumbo v13, "SELECT COUNT(*) from document_service WHERE file_path = ?"

    invoke-virtual {v7, v13}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v11

    .line 248
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v11, v13, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 250
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J

    move-result-wide v4

    .line 252
    .local v4, "countRec":J
    const-wide/16 v14, 0x0

    cmp-long v13, v4, v14

    if-eqz v13, :cond_9

    .line 253
    if-eqz p3, :cond_5

    .line 254
    const-string/jumbo v13, "UPDATE document_service SET last_indexed_time = ? , thumbnail_status = ? , thumbnail_data = ? WHERE file_path = ?"

    invoke-virtual {v7, v13}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v12

    .line 259
    if-eqz p4, :cond_1

    .line 260
    const-string/jumbo p5, "null"

    .line 262
    :cond_1
    const/4 v13, 0x2

    move/from16 v0, p4

    int-to-long v14, v0

    invoke-virtual {v12, v13, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 263
    const/4 v13, 0x3

    move-object/from16 v0, p5

    invoke-virtual {v12, v13, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 264
    const/4 v13, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v12, v13, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 274
    :goto_0
    const/4 v13, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    div-long v14, v14, v16

    invoke-virtual {v12, v13, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 275
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    .line 310
    :goto_1
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 315
    if-eqz v11, :cond_2

    .line 316
    :try_start_2
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 318
    :cond_2
    if-eqz v10, :cond_3

    .line 319
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 321
    :cond_3
    if-eqz v12, :cond_4

    .line 322
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteStatement;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 325
    :cond_4
    :try_start_3
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 331
    .end local v4    # "countRec":J
    :goto_2
    monitor-exit p0

    return-void

    .line 266
    .restart local v4    # "countRec":J
    :cond_5
    :try_start_4
    const-string/jumbo v13, "UPDATE document_service SET last_indexed_time = ? , index_status = ? , delete_status = ? WHERE file_path = ?"

    invoke-virtual {v7, v13}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v12

    .line 270
    const/4 v13, 0x2

    move/from16 v0, p2

    int-to-long v14, v0

    invoke-virtual {v12, v13, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 271
    const/4 v13, 0x3

    const-wide/16 v14, 0x0

    invoke-virtual {v12, v13, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 272
    const/4 v13, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v12, v13, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 311
    .end local v4    # "countRec":J
    :catch_0
    move-exception v3

    .line 312
    .local v3, "e":Ljava/lang/Exception;
    :try_start_5
    const-string/jumbo v13, "DocumentDatabaseManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Exception in inserting entry in DB: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 315
    if-eqz v11, :cond_6

    .line 316
    :try_start_6
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 318
    :cond_6
    if-eqz v10, :cond_7

    .line 319
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 321
    :cond_7
    if-eqz v12, :cond_8

    .line 322
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteStatement;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 325
    :cond_8
    :try_start_7
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2

    .line 326
    :catch_1
    move-exception v3

    .line 327
    :try_start_8
    const-string/jumbo v13, "DocumentDatabaseManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Exception in closing DB: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2

    .line 229
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v7    # "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    .end local v10    # "stmtInsert":Landroid/database/sqlite/SQLiteStatement;
    .end local v11    # "stmtSelectRec":Landroid/database/sqlite/SQLiteStatement;
    .end local v12    # "stmtUpdate":Landroid/database/sqlite/SQLiteStatement;
    :catchall_0
    move-exception v13

    monitor-exit p0

    throw v13

    .line 279
    .restart local v4    # "countRec":J
    .restart local v7    # "objSqliteDB":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v10    # "stmtInsert":Landroid/database/sqlite/SQLiteStatement;
    .restart local v11    # "stmtSelectRec":Landroid/database/sqlite/SQLiteStatement;
    .restart local v12    # "stmtUpdate":Landroid/database/sqlite/SQLiteStatement;
    :cond_9
    :try_start_9
    const-string/jumbo v13, "INSERT INTO document_service VALUES(?,?,?,?,?,?,?,?)"

    invoke-virtual {v7, v13}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v10

    .line 282
    const-string/jumbo v2, "sdCard"

    .line 283
    .local v2, "accountId":Ljava/lang/String;
    sget-object v13, Lcom/samsung/index/indexservice/Const;->EXT_SD_PATH:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_f

    .line 284
    const-string/jumbo v2, "extSdCard"

    .line 288
    :cond_a
    :goto_3
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v10, v13, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 289
    const/4 v13, 0x2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    div-long v14, v14, v16

    invoke-virtual {v10, v13, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 291
    new-instance v6, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 292
    .local v6, "file":Ljava/io/File;
    const/4 v13, 0x3

    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    div-long v14, v14, v16

    invoke-virtual {v10, v13, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 294
    const/4 v13, 0x4

    invoke-virtual {v10, v13, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 295
    if-eqz p3, :cond_10

    .line 296
    const/4 v13, 0x5

    const-wide/16 v14, -0x1

    invoke-virtual {v10, v13, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 297
    const/4 v13, 0x6

    move/from16 v0, p4

    int-to-long v14, v0

    invoke-virtual {v10, v13, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 298
    if-eqz p4, :cond_b

    .line 299
    const-string/jumbo p5, "null"

    .line 305
    :cond_b
    :goto_4
    const/4 v13, 0x7

    move-object/from16 v0, p5

    invoke-virtual {v10, v13, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 306
    const/16 v13, 0x8

    const-wide/16 v14, 0x0

    invoke-virtual {v10, v13, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 307
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v8

    .line 308
    .local v8, "id":J
    const-string/jumbo v13, "DocumentDatabaseManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Inserted Id for indexed file :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_1

    .line 315
    .end local v2    # "accountId":Ljava/lang/String;
    .end local v4    # "countRec":J
    .end local v6    # "file":Ljava/io/File;
    .end local v8    # "id":J
    :catchall_1
    move-exception v13

    if-eqz v11, :cond_c

    .line 316
    :try_start_a
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 318
    :cond_c
    if-eqz v10, :cond_d

    .line 319
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 321
    :cond_d
    if-eqz v12, :cond_e

    .line 322
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteStatement;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 325
    :cond_e
    :try_start_b
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 328
    :goto_5
    :try_start_c
    throw v13
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 286
    .restart local v2    # "accountId":Ljava/lang/String;
    .restart local v4    # "countRec":J
    :cond_f
    :try_start_d
    sget-object v13, Lcom/samsung/index/indexservice/Const;->PRIVATE_PATH:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 287
    const-string/jumbo v2, "private"

    goto/16 :goto_3

    .line 302
    .restart local v6    # "file":Ljava/io/File;
    :cond_10
    const/4 v13, 0x5

    move/from16 v0, p2

    int-to-long v14, v0

    invoke-virtual {v10, v13, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 303
    const/4 v13, 0x6

    const-wide/16 v14, -0x1

    invoke-virtual {v10, v13, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto :goto_4

    .line 326
    .end local v2    # "accountId":Ljava/lang/String;
    .end local v6    # "file":Ljava/io/File;
    :catch_2
    move-exception v3

    .line 327
    .restart local v3    # "e":Ljava/lang/Exception;
    :try_start_e
    const-string/jumbo v13, "DocumentDatabaseManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "Exception in closing DB: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 326
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "countRec":J
    :catch_3
    move-exception v3

    .line 327
    .restart local v3    # "e":Ljava/lang/Exception;
    const-string/jumbo v14, "DocumentDatabaseManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "Exception in closing DB: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_5
.end method
