.class public final Lcom/samsung/index/AlphaNumericAnalyzer;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "AlphaNumericAnalyzer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;-><init>()V

    return-void
.end method


# virtual methods
.method public tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 3
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 15
    new-instance v0, Lorg/apache/lucene/analysis/LowerCaseFilter;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    new-instance v2, Lcom/samsung/index/AlphaNumericTokennizer;

    invoke-direct {v2, p2}, Lcom/samsung/index/AlphaNumericTokennizer;-><init>(Ljava/io/Reader;)V

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/analysis/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    return-object v0
.end method
