.class public final Lorg/apache/lucene/analysis/standard/StandardAnalyzer;
.super Lorg/apache/lucene/analysis/StopwordAnalyzerBase;
.source "StandardAnalyzer.java"


# static fields
.field public static final DEFAULT_MAX_TOKEN_LENGTH:I = 0xff

.field public static final STOP_WORDS_SET:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private maxTokenLength:I

.field private final replaceInvalidAcronym:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lorg/apache/lucene/analysis/StopAnalyzer;->ENGLISH_STOP_WORDS_SET:Ljava/util/Set;

    sput-object v0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->STOP_WORDS_SET:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 83
    sget-object v0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->STOP_WORDS_SET:Ljava/util/Set;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V

    .line 84
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/File;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 95
    sget-object v0, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    invoke-static {p2, v0}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/io/File;Ljava/nio/charset/Charset;)Ljava/io/Reader;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/apache/lucene/analysis/WordlistLoader;->getWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/CharArraySet;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V

    .line 97
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-static {p2, p1}, Lorg/apache/lucene/analysis/WordlistLoader;->getWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/CharArraySet;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V

    .line 106
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/Set",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p2, "stopWords":Ljava/util/Set;, "Ljava/util/Set<*>;"
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/StopwordAnalyzerBase;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V

    .line 56
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->maxTokenLength:I

    .line 74
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_24:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->replaceInvalidAcronym:Z

    .line 75
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/analysis/standard/StandardAnalyzer;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/analysis/standard/StandardAnalyzer;

    .prologue
    .line 51
    iget v0, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->maxTokenLength:I

    return v0
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 127
    new-instance v0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, p2}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 128
    .local v0, "src":Lorg/apache/lucene/analysis/standard/StandardTokenizer;
    iget v3, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->maxTokenLength:I

    invoke-virtual {v0, v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->setMaxTokenLength(I)V

    .line 129
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->replaceInvalidAcronym:Z

    invoke-virtual {v0, v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->setReplaceInvalidAcronym(Z)V

    .line 130
    new-instance v1, Lorg/apache/lucene/analysis/standard/StandardFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v1, v3, v0}, Lorg/apache/lucene/analysis/standard/StandardFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 131
    .local v1, "tok":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v2, Lorg/apache/lucene/analysis/LowerCaseFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, v1}, Lorg/apache/lucene/analysis/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 132
    .end local v1    # "tok":Lorg/apache/lucene/analysis/TokenStream;
    .local v2, "tok":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v4, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->stopwords:Lorg/apache/lucene/analysis/CharArraySet;

    invoke-direct {v1, v3, v2, v4}, Lorg/apache/lucene/analysis/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;)V

    .line 133
    .end local v2    # "tok":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "tok":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v3, Lorg/apache/lucene/analysis/standard/StandardAnalyzer$1;

    invoke-direct {v3, p0, v0, v1, v0}, Lorg/apache/lucene/analysis/standard/StandardAnalyzer$1;-><init>(Lorg/apache/lucene/analysis/standard/StandardAnalyzer;Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/standard/StandardTokenizer;)V

    return-object v3
.end method

.method public getMaxTokenLength()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->maxTokenLength:I

    return v0
.end method

.method public setMaxTokenLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 115
    iput p1, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->maxTokenLength:I

    .line 116
    return-void
.end method
