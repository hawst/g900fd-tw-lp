.class public abstract Lorg/apache/lucene/analysis/FilteringTokenFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "FilteringTokenFilter.java"


# instance fields
.field private enablePositionIncrements:Z

.field private first:Z

.field private final posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;


# direct methods
.method public constructor <init>(ZLorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "enablePositionIncrements"    # Z
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 38
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 33
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/FilteringTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->first:Z

    .line 39
    iput-boolean p1, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->enablePositionIncrements:Z

    .line 40
    return-void
.end method


# virtual methods
.method protected abstract accept()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getEnablePositionIncrements()Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->enablePositionIncrements:Z

    return v0
.end method

.method public final incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 47
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->enablePositionIncrements:Z

    if-eqz v3, :cond_2

    .line 48
    const/4 v0, 0x0

    .line 49
    .local v0, "skippedPositions":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 50
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/FilteringTokenFilter;->accept()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 51
    if-eqz v0, :cond_0

    .line 52
    iget-object v2, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iget-object v3, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v3

    add-int/2addr v3, v0

    invoke-interface {v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 73
    .end local v0    # "skippedPositions":I
    :cond_0
    :goto_1
    return v1

    .line 56
    .restart local v0    # "skippedPositions":I
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_0

    .line 59
    .end local v0    # "skippedPositions":I
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 60
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/FilteringTokenFilter;->accept()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 61
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->first:Z

    if-eqz v3, :cond_0

    .line 63
    iget-object v3, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v3

    if-nez v3, :cond_3

    .line 64
    iget-object v3, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3, v1}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 66
    :cond_3
    iput-boolean v2, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->first:Z

    goto :goto_1

    :cond_4
    move v1, v2

    .line 73
    goto :goto_1
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->first:Z

    .line 80
    return-void
.end method

.method public setEnablePositionIncrements(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 106
    iput-boolean p1, p0, Lorg/apache/lucene/analysis/FilteringTokenFilter;->enablePositionIncrements:Z

    .line 107
    return-void
.end method
