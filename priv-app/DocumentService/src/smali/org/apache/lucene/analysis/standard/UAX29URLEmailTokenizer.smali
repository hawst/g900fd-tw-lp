.class public final Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "UAX29URLEmailTokenizer.java"


# static fields
.field public static final ALPHANUM:I = 0x0

.field public static final EMAIL:I = 0x8

.field public static final EMAIL_TYPE:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final HANGUL:I = 0x6

.field public static final HANGUL_TYPE:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final HIRAGANA:I = 0x4

.field public static final HIRAGANA_TYPE:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final IDEOGRAPHIC:I = 0x3

.field public static final IDEOGRAPHIC_TYPE:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final KATAKANA:I = 0x5

.field public static final KATAKANA_TYPE:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NUM:I = 0x1

.field public static final NUMERIC_TYPE:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SOUTHEAST_ASIAN:I = 0x2

.field public static final SOUTH_EAST_ASIAN_TYPE:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TOKEN_TYPES:[Ljava/lang/String;

.field public static final URL:I = 0x7

.field public static final URL_TYPE:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WORD_TYPE:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private maxTokenLength:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private final scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x6

    const/4 v4, 0x0

    .line 78
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v1, v1, v5

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    aput-object v1, v0, v7

    sget-object v1, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/16 v3, 0xb

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/16 v3, 0xc

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    sget-object v1, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    aput-object v1, v0, v5

    const/4 v1, 0x7

    const-string/jumbo v2, "<URL>"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "<EMAIL>"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    .line 93
    sget-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v0, v0, v4

    sput-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->WORD_TYPE:Ljava/lang/String;

    .line 98
    sget-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v0, v0, v6

    sput-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->NUMERIC_TYPE:Ljava/lang/String;

    .line 103
    sget-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/4 v1, 0x7

    aget-object v0, v0, v1

    sput-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->URL_TYPE:Ljava/lang/String;

    .line 108
    sget-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/16 v1, 0x8

    aget-object v0, v0, v1

    sput-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->EMAIL_TYPE:Ljava/lang/String;

    .line 120
    sget-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v0, v0, v7

    sput-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->SOUTH_EAST_ASIAN_TYPE:Ljava/lang/String;

    .line 124
    sget-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v0, v0, v8

    sput-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->IDEOGRAPHIC_TYPE:Ljava/lang/String;

    .line 128
    sget-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    sput-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->HIRAGANA_TYPE:Ljava/lang/String;

    .line 132
    sget-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    sput-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->KATAKANA_TYPE:Ljava/lang/String;

    .line 136
    sget-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v0, v0, v5

    sput-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->HANGUL_TYPE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 160
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 161
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1, "input"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 154
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 155
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 1
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 172
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 173
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V
    .locals 1
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p2, "input"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 166
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V

    .line 167
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Ljava/io/Reader;

    .prologue
    .line 182
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 138
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->maxTokenLength:I

    .line 214
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 215
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 216
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 217
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 183
    invoke-static {p1, p2}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->getScannerFor(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    .line 184
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p3, "input"    # Ljava/io/Reader;

    .prologue
    .line 198
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 138
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->maxTokenLength:I

    .line 214
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 215
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 216
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 217
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 199
    invoke-static {p1, p3}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->getScannerFor(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    .line 200
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p3, "input"    # Ljava/io/Reader;

    .prologue
    .line 190
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V

    .line 138
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->maxTokenLength:I

    .line 214
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 215
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 216
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 217
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 191
    invoke-static {p1, p3}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->getScannerFor(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    .line 192
    return-void
.end method

.method private static getScannerFor(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;
    .locals 1
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    .line 203
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_36:Lorg/apache/lucene/util/Version;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    new-instance v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizerImpl;

    invoke-direct {v0, p1}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizerImpl;-><init>(Ljava/io/Reader;)V

    .line 208
    :goto_0
    return-object v0

    .line 205
    :cond_0
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_34:Lorg/apache/lucene/util/Version;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206
    new-instance v0, Lorg/apache/lucene/analysis/standard/std34/UAX29URLEmailTokenizerImpl34;

    invoke-direct {v0, p1}, Lorg/apache/lucene/analysis/standard/std34/UAX29URLEmailTokenizerImpl34;-><init>(Ljava/io/Reader;)V

    goto :goto_0

    .line 208
    :cond_1
    new-instance v0, Lorg/apache/lucene/analysis/standard/std31/UAX29URLEmailTokenizerImpl31;

    invoke-direct {v0, p1}, Lorg/apache/lucene/analysis/standard/std31/UAX29URLEmailTokenizerImpl31;-><init>(Ljava/io/Reader;)V

    goto :goto_0
.end method


# virtual methods
.method public final end()V
    .locals 3

    .prologue
    .line 248
    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yychar()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yylength()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->correctOffset(I)I

    move-result v0

    .line 249
    .local v0, "finalOffset":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1, v0, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 250
    return-void
.end method

.method public getMaxTokenLength()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->maxTokenLength:I

    return v0
.end method

.method public final incrementToken()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->clearAttributes()V

    .line 222
    const/4 v0, 0x1

    .line 225
    .local v0, "posIncr":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->getNextToken()I

    move-result v2

    .line 227
    .local v2, "tokenType":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 228
    const/4 v3, 0x0

    .line 237
    :goto_1
    return v3

    .line 231
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yylength()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->maxTokenLength:I

    if-gt v3, v4, :cond_1

    .line 232
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3, v0}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 233
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    iget-object v4, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->getText(Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;)V

    .line 234
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yychar()I

    move-result v1

    .line 235
    .local v1, "start":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->correctOffset(I)I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {p0, v5}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->correctOffset(I)I

    move-result v5

    invoke-interface {v3, v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 236
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    sget-object v4, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 237
    const/4 v3, 0x1

    goto :goto_1

    .line 241
    .end local v1    # "start":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 242
    goto :goto_0
.end method

.method public reset(Ljava/io/Reader;)V
    .locals 1
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 254
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/Tokenizer;->reset(Ljava/io/Reader;)V

    .line 255
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v0, p1}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yyreset(Ljava/io/Reader;)V

    .line 256
    return-void
.end method

.method public setMaxTokenLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 143
    iput p1, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->maxTokenLength:I

    .line 144
    return-void
.end method
