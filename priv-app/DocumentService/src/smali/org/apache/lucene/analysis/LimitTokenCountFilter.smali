.class public final Lorg/apache/lucene/analysis/LimitTokenCountFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "LimitTokenCountFilter.java"


# instance fields
.field private final maxTokenCount:I

.field private tokenCount:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;I)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "maxTokenCount"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/LimitTokenCountFilter;->tokenCount:I

    .line 36
    iput p2, p0, Lorg/apache/lucene/analysis/LimitTokenCountFilter;->maxTokenCount:I

    .line 37
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget v0, p0, Lorg/apache/lucene/analysis/LimitTokenCountFilter;->tokenCount:I

    iget v1, p0, Lorg/apache/lucene/analysis/LimitTokenCountFilter;->maxTokenCount:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/LimitTokenCountFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    iget v0, p0, Lorg/apache/lucene/analysis/LimitTokenCountFilter;->tokenCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/LimitTokenCountFilter;->tokenCount:I

    .line 43
    const/4 v0, 0x1

    .line 45
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/LimitTokenCountFilter;->tokenCount:I

    .line 52
    return-void
.end method
