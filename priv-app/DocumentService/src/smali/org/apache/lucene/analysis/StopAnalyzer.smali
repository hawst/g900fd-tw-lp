.class public final Lorg/apache/lucene/analysis/StopAnalyzer;
.super Lorg/apache/lucene/analysis/StopwordAnalyzerBase;
.source "StopAnalyzer.java"


# static fields
.field public static final ENGLISH_STOP_WORDS_SET:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 49
    const/16 v2, 0x21

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "a"

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string/jumbo v4, "an"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "and"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "are"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "as"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string/jumbo v4, "at"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "be"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "but"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "by"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "for"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "if"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "in"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "into"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "is"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string/jumbo v4, "it"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string/jumbo v4, "no"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string/jumbo v4, "not"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    const-string/jumbo v4, "of"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string/jumbo v4, "on"

    aput-object v4, v2, v3

    const/16 v3, 0x13

    const-string/jumbo v4, "or"

    aput-object v4, v2, v3

    const/16 v3, 0x14

    const-string/jumbo v4, "such"

    aput-object v4, v2, v3

    const/16 v3, 0x15

    const-string/jumbo v4, "that"

    aput-object v4, v2, v3

    const/16 v3, 0x16

    const-string/jumbo v4, "the"

    aput-object v4, v2, v3

    const/16 v3, 0x17

    const-string/jumbo v4, "their"

    aput-object v4, v2, v3

    const/16 v3, 0x18

    const-string/jumbo v4, "then"

    aput-object v4, v2, v3

    const/16 v3, 0x19

    const-string/jumbo v4, "there"

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    const-string/jumbo v4, "these"

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    const-string/jumbo v4, "they"

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    const-string/jumbo v4, "this"

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    const-string/jumbo v4, "to"

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    const-string/jumbo v4, "was"

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    const-string/jumbo v4, "will"

    aput-object v4, v2, v3

    const/16 v3, 0x20

    const-string/jumbo v4, "with"

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 56
    .local v1, "stopWords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lorg/apache/lucene/analysis/CharArraySet;

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v0, v2, v3, v5}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    .line 58
    .local v0, "stopSet":Lorg/apache/lucene/analysis/CharArraySet;
    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->addAll(Ljava/util/Collection;)Z

    .line 59
    invoke-static {v0}, Lorg/apache/lucene/analysis/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/CharArraySet;)Lorg/apache/lucene/analysis/CharArraySet;

    move-result-object v2

    sput-object v2, Lorg/apache/lucene/analysis/StopAnalyzer;->ENGLISH_STOP_WORDS_SET:Ljava/util/Set;

    .line 60
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 67
    sget-object v0, Lorg/apache/lucene/analysis/StopAnalyzer;->ENGLISH_STOP_WORDS_SET:Ljava/util/Set;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/StopAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/File;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwordsFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    sget-object v0, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    invoke-static {p2, v0}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/io/File;Ljava/nio/charset/Charset;)Ljava/io/Reader;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/apache/lucene/analysis/WordlistLoader;->getWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/CharArraySet;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/StopAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V

    .line 84
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-static {p2, p1}, Lorg/apache/lucene/analysis/WordlistLoader;->getWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/CharArraySet;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/StopAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V

    .line 92
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/Set",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p2, "stopWords":Ljava/util/Set;, "Ljava/util/Set<*>;"
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/StopwordAnalyzerBase;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V

    .line 75
    return-void
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 106
    new-instance v0, Lorg/apache/lucene/analysis/LowerCaseTokenizer;

    iget-object v1, p0, Lorg/apache/lucene/analysis/StopAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v1, p2}, Lorg/apache/lucene/analysis/LowerCaseTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 107
    .local v0, "source":Lorg/apache/lucene/analysis/Tokenizer;
    new-instance v1, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;

    new-instance v2, Lorg/apache/lucene/analysis/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/StopAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v4, p0, Lorg/apache/lucene/analysis/StopAnalyzer;->stopwords:Lorg/apache/lucene/analysis/CharArraySet;

    invoke-direct {v2, v3, v0, v4}, Lorg/apache/lucene/analysis/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;)V

    invoke-direct {v1, v0, v2}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-object v1
.end method
