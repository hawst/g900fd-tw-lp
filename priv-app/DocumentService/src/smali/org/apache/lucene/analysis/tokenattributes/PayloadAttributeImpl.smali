.class public Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "PayloadAttributeImpl.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;


# instance fields
.field private payload:Lorg/apache/lucene/index/Payload;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Payload;)V
    .locals 0
    .param p1, "payload"    # Lorg/apache/lucene/index/Payload;

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    .line 40
    iput-object p1, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    .line 41
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    .line 60
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 64
    invoke-super {p0}, Lorg/apache/lucene/util/AttributeImpl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;

    .line 65
    .local v0, "clone":Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    if-eqz v1, :cond_0

    .line 66
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Payload;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/Payload;

    iput-object v1, v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    .line 68
    :cond_0
    return-object v0
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 2
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 96
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    .line 97
    .local v0, "t":Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->setPayload(Lorg/apache/lucene/index/Payload;)V

    .line 98
    return-void

    .line 97
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Payload;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/Payload;

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 73
    if-ne p1, p0, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v1

    .line 77
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 78
    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;

    .line 79
    .local v0, "o":Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;
    iget-object v3, v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    if-nez v3, :cond_4

    .line 80
    :cond_2
    iget-object v3, v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    if-nez v3, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 83
    :cond_4
    iget-object v1, v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    iget-object v2, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/Payload;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .end local v0    # "o":Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;
    :cond_5
    move v1, v2

    .line 86
    goto :goto_0
.end method

.method public getPayload()Lorg/apache/lucene/index/Payload;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Payload;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public setPayload(Lorg/apache/lucene/index/Payload;)V
    .locals 0
    .param p1, "payload"    # Lorg/apache/lucene/index/Payload;

    .prologue
    .line 54
    iput-object p1, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/index/Payload;

    .line 55
    return-void
.end method
