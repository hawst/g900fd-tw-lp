.class public abstract Lorg/apache/lucene/analysis/BaseCharFilter;
.super Lorg/apache/lucene/analysis/CharFilter;
.source "BaseCharFilter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private diffs:[I

.field private offsets:[I

.field private size:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lorg/apache/lucene/analysis/BaseCharFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/BaseCharFilter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/CharStream;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/CharStream;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/CharFilter;-><init>(Lorg/apache/lucene/analysis/CharStream;)V

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->size:I

    .line 71
    return-void
.end method


# virtual methods
.method protected addOffCorrectMap(II)V
    .locals 4
    .param p1, "off"    # I
    .param p2, "cumulativeDiff"    # I

    .prologue
    const/16 v1, 0x40

    .line 122
    iget-object v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    if-nez v0, :cond_1

    .line 123
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    .line 124
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->diffs:[I

    .line 132
    :cond_0
    :goto_0
    sget-boolean v0, Lorg/apache/lucene/analysis/BaseCharFilter;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->size:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    iget v1, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->size:I

    aget v0, v0, v1

    if-ge p1, v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Offset #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->size:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") is less than the last recorded offset "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    iget v3, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->size:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->diffs:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 125
    :cond_1
    iget v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->size:I

    iget-object v1, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 126
    iget-object v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    invoke-static {v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([I)[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    .line 127
    iget-object v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->diffs:[I

    invoke-static {v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([I)[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->diffs:[I

    goto/16 :goto_0

    .line 134
    :cond_2
    iget v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->size:I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    iget v1, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->size:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    if-eq p1, v0, :cond_4

    .line 135
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    iget v1, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->size:I

    aput p1, v0, v1

    .line 136
    iget-object v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->diffs:[I

    iget v1, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->size:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->size:I

    aput p2, v0, v1

    .line 140
    :goto_1
    return-void

    .line 138
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->diffs:[I

    iget v1, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->size:I

    add-int/lit8 v1, v1, -0x1

    aput p2, v0, v1

    goto :goto_1
.end method

.method protected correct(I)I
    .locals 5
    .param p1, "currentOff"    # I

    .prologue
    .line 76
    iget-object v3, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-ge p1, v3, :cond_1

    .line 100
    .end local p1    # "currentOff":I
    :cond_0
    :goto_0
    return p1

    .line 80
    .restart local p1    # "currentOff":I
    :cond_1
    iget v3, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->size:I

    add-int/lit8 v0, v3, -0x1

    .line 81
    .local v0, "hi":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    aget v3, v3, v0

    if-lt p1, v3, :cond_2

    .line 82
    iget-object v3, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->diffs:[I

    aget v3, v3, v0

    add-int/2addr p1, v3

    goto :goto_0

    .line 84
    :cond_2
    const/4 v1, 0x0

    .line 85
    .local v1, "lo":I
    const/4 v2, -0x1

    .line 87
    .local v2, "mid":I
    :goto_1
    if-lt v0, v1, :cond_5

    .line 88
    add-int v3, v1, v0

    ushr-int/lit8 v2, v3, 0x1

    .line 89
    iget-object v3, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    aget v3, v3, v2

    if-ge p1, v3, :cond_3

    .line 90
    add-int/lit8 v0, v2, -0x1

    goto :goto_1

    .line 91
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    aget v3, v3, v2

    if-le p1, v3, :cond_4

    .line 92
    add-int/lit8 v1, v2, 0x1

    goto :goto_1

    .line 94
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->diffs:[I

    aget v3, v3, v2

    add-int/2addr p1, v3

    goto :goto_0

    .line 97
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    aget v3, v3, v2

    if-ge p1, v3, :cond_6

    .line 98
    if-eqz v2, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->diffs:[I

    add-int/lit8 v4, v2, -0x1

    aget v3, v3, v4

    add-int/2addr p1, v3

    goto :goto_0

    .line 100
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->diffs:[I

    aget v3, v3, v2

    add-int/2addr p1, v3

    goto :goto_0
.end method

.method protected getLastCumulativeDiff()I
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->offsets:[I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->diffs:[I

    iget v1, p0, Lorg/apache/lucene/analysis/BaseCharFilter;->size:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_0
.end method
