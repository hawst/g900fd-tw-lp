.class final Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;
.super Ljava/lang/Object;
.source "CharArrayMap.java"

# interfaces
.implements Ljava/util/Map$Entry;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/CharArrayMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MapEntry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/Object;",
        "TV;>;"
    }
.end annotation


# instance fields
.field private final allowModify:Z

.field private final pos:I

.field final synthetic this$0:Lorg/apache/lucene/analysis/CharArrayMap;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/analysis/CharArrayMap;IZ)V
    .locals 0
    .param p2, "pos"    # I
    .param p3, "allowModify"    # Z

    .prologue
    .line 465
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.MapEntry;"
    iput-object p1, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 466
    iput p2, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->pos:I

    .line 467
    iput-boolean p3, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->allowModify:Z

    .line 468
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/analysis/CharArrayMap;IZLorg/apache/lucene/analysis/CharArrayMap$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/analysis/CharArrayMap;
    .param p2, "x1"    # I
    .param p3, "x2"    # Z
    .param p4, "x3"    # Lorg/apache/lucene/analysis/CharArrayMap$1;

    .prologue
    .line 461
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.MapEntry;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;IZ)V

    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 473
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.MapEntry;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v0, v0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    iget v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->pos:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, [C->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 477
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.MapEntry;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v0, v0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    iget v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->pos:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)TV;"
        }
    .end annotation

    .prologue
    .line 481
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.MapEntry;"
    .local p1, "value":Ljava/lang/Object;, "TV;"
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->allowModify:Z

    if-nez v1, :cond_0

    .line 482
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v1

    .line 483
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v1, v1, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    iget v2, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->pos:I

    aget-object v0, v1, v2

    .line 484
    .local v0, "old":Ljava/lang/Object;, "TV;"
    iget-object v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v1, v1, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    iget v2, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->pos:I

    aput-object p1, v1, v2

    .line 485
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 490
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.MapEntry;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v1, v1, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    iget v2, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->pos:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v0, v0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    iget v2, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->pos:I

    aget-object v0, v0, v2

    iget-object v2, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    if-ne v0, v2, :cond_0

    const-string/jumbo v0, "(this Map)"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v0, v0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    iget v2, p0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;->pos:I

    aget-object v0, v0, v2

    goto :goto_0
.end method
