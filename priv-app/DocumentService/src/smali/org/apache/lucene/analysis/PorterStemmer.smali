.class Lorg/apache/lucene/analysis/PorterStemmer;
.super Ljava/lang/Object;
.source "PorterStemmer.java"


# static fields
.field private static final INITIAL_SIZE:I = 0x32


# instance fields
.field private b:[C

.field private dirty:Z

.field private i:I

.field private j:I

.field private k:I

.field private k0:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->dirty:Z

    .line 72
    const/16 v0, 0x32

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    .line 73
    iput v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->i:I

    .line 74
    return-void
.end method

.method private final cons(I)Z
    .locals 3
    .param p1, "i"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 117
    iget-object v2, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    aget-char v2, v2, p1

    sparse-switch v2, :sswitch_data_0

    .line 123
    :cond_0
    :goto_0
    return v0

    :sswitch_0
    move v0, v1

    .line 119
    goto :goto_0

    .line 121
    :sswitch_1
    iget v2, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k0:I

    if-eq p1, v2, :cond_0

    add-int/lit8 v2, p1, -0x1

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/PorterStemmer;->cons(I)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 117
    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x69 -> :sswitch_0
        0x6f -> :sswitch_0
        0x75 -> :sswitch_0
        0x79 -> :sswitch_1
    .end sparse-switch
.end method

.method private final cvc(I)Z
    .locals 3
    .param p1, "i"    # I

    .prologue
    const/4 v1, 0x0

    .line 200
    iget v2, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k0:I

    add-int/lit8 v2, v2, 0x2

    if-lt p1, v2, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/PorterStemmer;->cons(I)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v2, p1, -0x1

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/PorterStemmer;->cons(I)Z

    move-result v2

    if-nez v2, :cond_0

    add-int/lit8 v2, p1, -0x2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/PorterStemmer;->cons(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 206
    :cond_0
    :goto_0
    return v1

    .line 203
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    aget-char v0, v2, p1

    .line 204
    .local v0, "ch":I
    const/16 v2, 0x77

    if-eq v0, v2, :cond_0

    const/16 v2, 0x78

    if-eq v0, v2, :cond_0

    const/16 v2, 0x79

    if-eq v0, v2, :cond_0

    .line 206
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private final doublec(I)Z
    .locals 4
    .param p1, "j"    # I

    .prologue
    const/4 v0, 0x0

    .line 183
    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k0:I

    add-int/lit8 v1, v1, 0x1

    if-ge p1, v1, :cond_1

    .line 187
    :cond_0
    :goto_0
    return v0

    .line 185
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    aget-char v1, v1, p1

    iget-object v2, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    add-int/lit8 v3, p1, -0x1

    aget-char v2, v2, v3

    if-ne v1, v2, :cond_0

    .line 187
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/PorterStemmer;->cons(I)Z

    move-result v0

    goto :goto_0
.end method

.method private final ends(Ljava/lang/String;)Z
    .locals 6
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 210
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 211
    .local v1, "l":I
    iget v4, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    sub-int/2addr v4, v1

    add-int/lit8 v2, v4, 0x1

    .line 212
    .local v2, "o":I
    iget v4, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k0:I

    if-ge v2, v4, :cond_1

    .line 218
    :cond_0
    :goto_0
    return v3

    .line 214
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 215
    iget-object v4, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    add-int v5, v2, v0

    aget-char v4, v4, v5

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_0

    .line 214
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 217
    :cond_2
    iget v3, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    sub-int/2addr v3, v1

    iput v3, p0, Lorg/apache/lucene/analysis/PorterStemmer;->j:I

    .line 218
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private final m()I
    .locals 4

    .prologue
    .line 139
    const/4 v1, 0x0

    .line 140
    .local v1, "n":I
    iget v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k0:I

    .line 142
    .local v0, "i":I
    :goto_0
    iget v3, p0, Lorg/apache/lucene/analysis/PorterStemmer;->j:I

    if-le v0, v3, :cond_0

    move v2, v1

    .line 161
    .end local v1    # "n":I
    .local v2, "n":I
    :goto_1
    return v2

    .line 144
    .end local v2    # "n":I
    .restart local v1    # "n":I
    :cond_0
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->cons(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 148
    add-int/lit8 v0, v0, 0x1

    .line 151
    :goto_2
    iget v3, p0, Lorg/apache/lucene/analysis/PorterStemmer;->j:I

    if-le v0, v3, :cond_2

    move v2, v1

    .line 152
    .end local v1    # "n":I
    .restart local v2    # "n":I
    goto :goto_1

    .line 146
    .end local v2    # "n":I
    .restart local v1    # "n":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 153
    :cond_2
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->cons(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 157
    add-int/lit8 v0, v0, 0x1

    .line 158
    add-int/lit8 v1, v1, 0x1

    .line 160
    :goto_3
    iget v3, p0, Lorg/apache/lucene/analysis/PorterStemmer;->j:I

    if-le v0, v3, :cond_4

    move v2, v1

    .line 161
    .end local v1    # "n":I
    .restart local v2    # "n":I
    goto :goto_1

    .line 155
    .end local v2    # "n":I
    .restart local v1    # "n":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 162
    :cond_4
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->cons(I)Z

    move-result v3

    if-nez v3, :cond_5

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 164
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method public static main([Ljava/lang/String;)V
    .locals 12
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    .line 500
    new-instance v8, Lorg/apache/lucene/analysis/PorterStemmer;

    invoke-direct {v8}, Lorg/apache/lucene/analysis/PorterStemmer;-><init>()V

    .line 502
    .local v8, "s":Lorg/apache/lucene/analysis/PorterStemmer;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v9, p0

    if-ge v4, v9, :cond_4

    .line 504
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    aget-object v9, p0, v4

    invoke-direct {v5, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 505
    .local v5, "in":Ljava/io/InputStream;
    const/16 v9, 0x400

    new-array v0, v9, [B

    .line 508
    .local v0, "buffer":[B
    invoke-virtual {v5, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 509
    .local v1, "bufferLen":I
    const/4 v6, 0x0

    .line 510
    .local v6, "offset":I
    invoke-virtual {v8}, Lorg/apache/lucene/analysis/PorterStemmer;->reset()V

    move v7, v6

    .line 513
    .end local v6    # "offset":I
    .local v7, "offset":I
    :goto_1
    if-ge v7, v1, :cond_0

    .line 514
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "offset":I
    .restart local v6    # "offset":I
    aget-byte v2, v0, v7

    .line 524
    .local v2, "ch":I
    :goto_2
    int-to-char v9, v2

    invoke-static {v9}, Ljava/lang/Character;->isLetter(C)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 525
    int-to-char v9, v2

    invoke-static {v9}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v9

    invoke-virtual {v8, v9}, Lorg/apache/lucene/analysis/PorterStemmer;->add(C)V

    move v7, v6

    .end local v6    # "offset":I
    .restart local v7    # "offset":I
    goto :goto_1

    .line 516
    .end local v2    # "ch":I
    :cond_0
    invoke-virtual {v5, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 517
    const/4 v6, 0x0

    .line 518
    .end local v7    # "offset":I
    .restart local v6    # "offset":I
    if-gez v1, :cond_1

    .line 519
    const/4 v2, -0x1

    .restart local v2    # "ch":I
    goto :goto_2

    .line 521
    .end local v2    # "ch":I
    :cond_1
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "offset":I
    .restart local v7    # "offset":I
    aget-byte v2, v0, v6

    .restart local v2    # "ch":I
    move v6, v7

    .end local v7    # "offset":I
    .restart local v6    # "offset":I
    goto :goto_2

    .line 528
    :cond_2
    invoke-virtual {v8}, Lorg/apache/lucene/analysis/PorterStemmer;->stem()Z

    .line 529
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v8}, Lorg/apache/lucene/analysis/PorterStemmer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 530
    invoke-virtual {v8}, Lorg/apache/lucene/analysis/PorterStemmer;->reset()V

    .line 531
    if-gez v2, :cond_3

    .line 539
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 502
    .end local v0    # "buffer":[B
    .end local v1    # "bufferLen":I
    .end local v2    # "ch":I
    .end local v5    # "in":Ljava/io/InputStream;
    .end local v6    # "offset":I
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 534
    .restart local v0    # "buffer":[B
    .restart local v1    # "bufferLen":I
    .restart local v2    # "ch":I
    .restart local v5    # "in":Ljava/io/InputStream;
    .restart local v6    # "offset":I
    :cond_3
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    int-to-char v10, v2

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->print(C)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v7, v6

    .end local v6    # "offset":I
    .restart local v7    # "offset":I
    goto :goto_1

    .line 541
    .end local v0    # "buffer":[B
    .end local v1    # "bufferLen":I
    .end local v2    # "ch":I
    .end local v5    # "in":Ljava/io/InputStream;
    .end local v7    # "offset":I
    :catch_0
    move-exception v3

    .line 542
    .local v3, "e":Ljava/io/IOException;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "error reading "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    aget-object v11, p0, v4

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_3

    .line 545
    .end local v3    # "e":Ljava/io/IOException;
    :cond_4
    return-void
.end method

.method private final step1()V
    .locals 5

    .prologue
    const/16 v4, 0x73

    .line 260
    iget-object v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    iget v2, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    aget-char v1, v1, v2

    if-ne v1, v4, :cond_0

    .line 261
    const-string/jumbo v1, "sses"

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    add-int/lit8 v1, v1, -0x2

    iput v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    .line 265
    :cond_0
    :goto_0
    const-string/jumbo v1, "eed"

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 266
    invoke-direct {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->m()I

    move-result v1

    if-lez v1, :cond_1

    .line 267
    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    .line 282
    :cond_1
    :goto_1
    return-void

    .line 262
    :cond_2
    const-string/jumbo v1, "ies"

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "i"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->setto(Ljava/lang/String;)V

    goto :goto_0

    .line 263
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    iget v2, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    add-int/lit8 v2, v2, -0x1

    aget-char v1, v1, v2

    if-eq v1, v4, :cond_0

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    goto :goto_0

    .line 269
    :cond_4
    const-string/jumbo v1, "ed"

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "ing"

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_5
    invoke-direct {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->vowelinstem()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 270
    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    .line 271
    const-string/jumbo v1, "at"

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string/jumbo v1, "ate"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->setto(Ljava/lang/String;)V

    goto :goto_1

    .line 272
    :cond_6
    const-string/jumbo v1, "bl"

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string/jumbo v1, "ble"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->setto(Ljava/lang/String;)V

    goto :goto_1

    .line 273
    :cond_7
    const-string/jumbo v1, "iz"

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string/jumbo v1, "ize"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->setto(Ljava/lang/String;)V

    goto :goto_1

    .line 274
    :cond_8
    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->doublec(I)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 275
    iget-object v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    iget v2, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    add-int/lit8 v3, v2, -0x1

    iput v3, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    aget-char v0, v1, v2

    .line 276
    .local v0, "ch":I
    const/16 v1, 0x6c

    if-eq v0, v1, :cond_9

    if-eq v0, v4, :cond_9

    const/16 v1, 0x7a

    if-ne v0, v1, :cond_1

    .line 277
    :cond_9
    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    goto/16 :goto_1

    .line 279
    .end local v0    # "ch":I
    :cond_a
    invoke-direct {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->m()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->cvc(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 280
    const-string/jumbo v1, "e"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->setto(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private final step2()V
    .locals 3

    .prologue
    .line 287
    const-string/jumbo v0, "y"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->vowelinstem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    const/16 v2, 0x69

    aput-char v2, v0, v1

    .line 289
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->dirty:Z

    .line 291
    :cond_0
    return-void
.end method

.method private final step3()V
    .locals 2

    .prologue
    .line 298
    iget v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k0:I

    if-ne v0, v1, :cond_1

    .line 337
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    aget-char v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 301
    :sswitch_0
    const-string/jumbo v0, "ational"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "ate"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto :goto_0

    .line 302
    :cond_2
    const-string/jumbo v0, "tional"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "tion"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto :goto_0

    .line 305
    :sswitch_1
    const-string/jumbo v0, "enci"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "ence"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto :goto_0

    .line 306
    :cond_3
    const-string/jumbo v0, "anci"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ance"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto :goto_0

    .line 309
    :sswitch_2
    const-string/jumbo v0, "izer"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ize"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto :goto_0

    .line 312
    :sswitch_3
    const-string/jumbo v0, "bli"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "ble"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto :goto_0

    .line 313
    :cond_4
    const-string/jumbo v0, "alli"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "al"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto :goto_0

    .line 314
    :cond_5
    const-string/jumbo v0, "entli"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "ent"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 315
    :cond_6
    const-string/jumbo v0, "eli"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string/jumbo v0, "e"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 316
    :cond_7
    const-string/jumbo v0, "ousli"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ous"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 319
    :sswitch_4
    const-string/jumbo v0, "ization"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "ize"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 320
    :cond_8
    const-string/jumbo v0, "ation"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string/jumbo v0, "ate"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 321
    :cond_9
    const-string/jumbo v0, "ator"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ate"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 324
    :sswitch_5
    const-string/jumbo v0, "alism"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string/jumbo v0, "al"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 325
    :cond_a
    const-string/jumbo v0, "iveness"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string/jumbo v0, "ive"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 326
    :cond_b
    const-string/jumbo v0, "fulness"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string/jumbo v0, "ful"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 327
    :cond_c
    const-string/jumbo v0, "ousness"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ous"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 330
    :sswitch_6
    const-string/jumbo v0, "aliti"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string/jumbo v0, "al"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 331
    :cond_d
    const-string/jumbo v0, "iviti"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string/jumbo v0, "ive"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 332
    :cond_e
    const-string/jumbo v0, "biliti"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ble"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 335
    :sswitch_7
    const-string/jumbo v0, "logi"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "log"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 299
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x63 -> :sswitch_1
        0x65 -> :sswitch_2
        0x67 -> :sswitch_7
        0x6c -> :sswitch_3
        0x6f -> :sswitch_4
        0x73 -> :sswitch_5
        0x74 -> :sswitch_6
    .end sparse-switch
.end method

.method private final step4()V
    .locals 2

    .prologue
    .line 342
    iget-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    aget-char v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 344
    :sswitch_0
    const-string/jumbo v0, "icate"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "ic"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto :goto_0

    .line 345
    :cond_1
    const-string/jumbo v0, "ative"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto :goto_0

    .line 346
    :cond_2
    const-string/jumbo v0, "alize"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "al"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto :goto_0

    .line 349
    :sswitch_1
    const-string/jumbo v0, "iciti"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ic"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto :goto_0

    .line 352
    :sswitch_2
    const-string/jumbo v0, "ical"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "ic"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto :goto_0

    .line 353
    :cond_3
    const-string/jumbo v0, "ful"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto :goto_0

    .line 356
    :sswitch_3
    const-string/jumbo v0, "ness"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->r(Ljava/lang/String;)V

    goto :goto_0

    .line 342
    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_0
        0x69 -> :sswitch_1
        0x6c -> :sswitch_2
        0x73 -> :sswitch_3
    .end sparse-switch
.end method

.method private final step5()V
    .locals 2

    .prologue
    .line 364
    iget v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k0:I

    if-ne v0, v1, :cond_1

    .line 414
    :cond_0
    :goto_0
    return-void

    .line 365
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    aget-char v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 367
    :pswitch_1
    const-string/jumbo v0, "al"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    :cond_2
    :goto_1
    invoke-direct {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->m()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 413
    iget v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->j:I

    iput v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    goto :goto_0

    .line 370
    :pswitch_2
    const-string/jumbo v0, "ance"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 371
    const-string/jumbo v0, "ence"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 374
    :pswitch_3
    const-string/jumbo v0, "er"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 376
    :pswitch_4
    const-string/jumbo v0, "ic"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 378
    :pswitch_5
    const-string/jumbo v0, "able"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 379
    const-string/jumbo v0, "ible"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 381
    :pswitch_6
    const-string/jumbo v0, "ant"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 382
    const-string/jumbo v0, "ement"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 383
    const-string/jumbo v0, "ment"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 385
    const-string/jumbo v0, "ent"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 388
    :pswitch_7
    const-string/jumbo v0, "ion"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->j:I

    if-ltz v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->j:I

    aget-char v0, v0, v1

    const/16 v1, 0x73

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->j:I

    aget-char v0, v0, v1

    const/16 v1, 0x74

    if-eq v0, v1, :cond_2

    .line 390
    :cond_3
    const-string/jumbo v0, "ou"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 394
    :pswitch_8
    const-string/jumbo v0, "ism"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 397
    :pswitch_9
    const-string/jumbo v0, "ate"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 398
    const-string/jumbo v0, "iti"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 401
    :pswitch_a
    const-string/jumbo v0, "ous"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 404
    :pswitch_b
    const-string/jumbo v0, "ive"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 407
    :pswitch_c
    const-string/jumbo v0, "ize"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->ends(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 365
    nop

    :pswitch_data_0
    .packed-switch 0x61
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
    .end packed-switch
.end method

.method private final step6()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 419
    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    iput v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->j:I

    .line 420
    iget-object v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    iget v2, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    aget-char v1, v1, v2

    const/16 v2, 0x65

    if-ne v1, v2, :cond_1

    .line 421
    invoke-direct {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->m()I

    move-result v0

    .line 422
    .local v0, "a":I
    if-gt v0, v3, :cond_0

    if-ne v0, v3, :cond_1

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->cvc(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 423
    :cond_0
    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    .line 425
    .end local v0    # "a":I
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    iget v2, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    aget-char v1, v1, v2

    const/16 v2, 0x6c

    if-ne v1, v2, :cond_2

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->doublec(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->m()I

    move-result v1

    if-le v1, v3, :cond_2

    .line 426
    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    .line 427
    :cond_2
    return-void
.end method

.method private final vowelinstem()Z
    .locals 2

    .prologue
    .line 174
    iget v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k0:I

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->j:I

    if-gt v0, v1, :cond_1

    .line 175
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->cons(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 176
    const/4 v1, 0x1

    .line 177
    :goto_1
    return v1

    .line 174
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public add(C)V
    .locals 3
    .param p1, "ch"    # C

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    array-length v0, v0

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->i:I

    if-gt v0, v1, :cond_0

    .line 89
    iget-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->i:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->grow([CI)[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    .line 91
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/PorterStemmer;->i:I

    aput-char p1, v0, v1

    .line 92
    return-void
.end method

.method public getResultBuffer()[C
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    return-object v0
.end method

.method public getResultLength()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->i:I

    return v0
.end method

.method r(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 235
    invoke-direct {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->m()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/PorterStemmer;->setto(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 81
    iput v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->i:I

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->dirty:Z

    return-void
.end method

.method setto(Ljava/lang/String;)V
    .locals 6
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 225
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 226
    .local v1, "l":I
    iget v3, p0, Lorg/apache/lucene/analysis/PorterStemmer;->j:I

    add-int/lit8 v2, v3, 0x1

    .line 227
    .local v2, "o":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 228
    iget-object v3, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    add-int v4, v2, v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    aput-char v5, v3, v4

    .line 227
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 229
    :cond_0
    iget v3, p0, Lorg/apache/lucene/analysis/PorterStemmer;->j:I

    add-int/2addr v3, v1

    iput v3, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    .line 230
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/apache/lucene/analysis/PorterStemmer;->dirty:Z

    .line 231
    return-void
.end method

.method public stem(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 434
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->stem([CI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 435
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->toString()Ljava/lang/String;

    move-result-object p1

    .line 437
    .end local p1    # "s":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method public stem()Z
    .locals 1

    .prologue
    .line 478
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->stem(I)Z

    move-result v0

    return v0
.end method

.method public stem(I)Z
    .locals 2
    .param p1, "i0"    # I

    .prologue
    .line 482
    iget v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    .line 483
    iput p1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k0:I

    .line 484
    iget v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k0:I

    add-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_0

    .line 485
    invoke-direct {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->step1()V

    invoke-direct {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->step2()V

    invoke-direct {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->step3()V

    invoke-direct {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->step4()V

    invoke-direct {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->step5()V

    invoke-direct {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->step6()V

    .line 489
    :cond_0
    iget v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->i:I

    iget v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_1

    .line 490
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->dirty:Z

    .line 491
    :cond_1
    iget v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->i:I

    .line 492
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->dirty:Z

    return v0
.end method

.method public stem([C)Z
    .locals 1
    .param p1, "word"    # [C

    .prologue
    .line 445
    array-length v0, p1

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/PorterStemmer;->stem([CI)Z

    move-result v0

    return v0
.end method

.method public stem([CI)Z
    .locals 1
    .param p1, "word"    # [C
    .param p2, "wordLen"    # I

    .prologue
    .line 469
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/lucene/analysis/PorterStemmer;->stem([CII)Z

    move-result v0

    return v0
.end method

.method public stem([CII)Z
    .locals 2
    .param p1, "wordBuffer"    # [C
    .param p2, "offset"    # I
    .param p3, "wordLen"    # I

    .prologue
    const/4 v1, 0x0

    .line 454
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/PorterStemmer;->reset()V

    .line 455
    iget-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    array-length v0, v0

    if-ge v0, p3, :cond_0

    .line 456
    const/4 v0, 0x2

    invoke-static {p3, v0}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    .line 458
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 459
    iput p3, p0, Lorg/apache/lucene/analysis/PorterStemmer;->i:I

    .line 460
    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/PorterStemmer;->stem(I)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 100
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/PorterStemmer;->b:[C

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/analysis/PorterStemmer;->i:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method
