.class public abstract Lorg/apache/lucene/analysis/ReusableAnalyzerBase;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "ReusableAnalyzerBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;-><init>()V

    .line 110
    return-void
.end method


# virtual methods
.method protected abstract createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;
.end method

.method protected initReader(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 0
    .param p1, "reader"    # Ljava/io/Reader;

    .prologue
    .line 99
    return-object p1
.end method

.method public final reusableTokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase;->getPreviousTokenStream()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;

    .line 71
    .local v1, "streamChain":Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;
    invoke-virtual {p0, p2}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase;->initReader(Ljava/io/Reader;)Ljava/io/Reader;

    move-result-object v0

    .line 72
    .local v0, "r":Ljava/io/Reader;
    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;->reset(Ljava/io/Reader;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 73
    :cond_0
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase;->createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;

    move-result-object v1

    .line 74
    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase;->setPreviousTokenStream(Ljava/lang/Object;)V

    .line 76
    :cond_1
    invoke-virtual {v1}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;->getTokenStream()Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v2

    return-object v2
.end method

.method public final tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 92
    invoke-virtual {p0, p2}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase;->initReader(Ljava/io/Reader;)Ljava/io/Reader;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase;->createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;->getTokenStream()Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v0

    return-object v0
.end method
