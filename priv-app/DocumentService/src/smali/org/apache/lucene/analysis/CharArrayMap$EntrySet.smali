.class public final Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;
.super Ljava/util/AbstractSet;
.source "CharArrayMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/CharArrayMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "EntrySet"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/Object;",
        "TV;>;>;"
    }
.end annotation


# instance fields
.field private final allowModify:Z

.field final synthetic this$0:Lorg/apache/lucene/analysis/CharArrayMap;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/analysis/CharArrayMap;Z)V
    .locals 0
    .param p2, "allowModify"    # Z

    .prologue
    .line 500
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntrySet;"
    iput-object p1, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 501
    iput-boolean p2, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;->allowModify:Z

    .line 502
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/analysis/CharArrayMap;ZLorg/apache/lucene/analysis/CharArrayMap$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/analysis/CharArrayMap;
    .param p2, "x1"    # Z
    .param p3, "x2"    # Lorg/apache/lucene/analysis/CharArrayMap$1;

    .prologue
    .line 497
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntrySet;"
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;Z)V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 533
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntrySet;"
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;->allowModify:Z

    if-nez v0, :cond_0

    .line 534
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 535
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/CharArrayMap;->clear()V

    .line 536
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntrySet;"
    const/4 v4, 0x0

    .line 512
    instance-of v5, p1, Ljava/util/Map$Entry;

    if-nez v5, :cond_1

    .line 518
    :cond_0
    :goto_0
    return v4

    :cond_1
    move-object v0, p1

    .line 514
    check-cast v0, Ljava/util/Map$Entry;

    .line 515
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;TV;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 516
    .local v1, "key":Ljava/lang/Object;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    .line 517
    .local v3, "val":Ljava/lang/Object;
    iget-object v5, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-virtual {v5, v1}, Lorg/apache/lucene/analysis/CharArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 518
    .local v2, "v":Ljava/lang/Object;
    if-nez v2, :cond_2

    if-nez v3, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto :goto_0
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 497
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntrySet;"
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;->iterator()Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/analysis/CharArrayMap",
            "<TV;>.EntryIterator;"
        }
    .end annotation

    .prologue
    .line 506
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntrySet;"
    new-instance v0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;

    iget-object v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-boolean v2, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;->allowModify:Z

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;ZLorg/apache/lucene/analysis/CharArrayMap$1;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 523
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntrySet;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 528
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntrySet;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    # getter for: Lorg/apache/lucene/analysis/CharArrayMap;->count:I
    invoke-static {v0}, Lorg/apache/lucene/analysis/CharArrayMap;->access$300(Lorg/apache/lucene/analysis/CharArrayMap;)I

    move-result v0

    return v0
.end method
