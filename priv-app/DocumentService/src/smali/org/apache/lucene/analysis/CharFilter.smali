.class public abstract Lorg/apache/lucene/analysis/CharFilter;
.super Lorg/apache/lucene/analysis/CharStream;
.source "CharFilter.java"


# instance fields
.field protected input:Lorg/apache/lucene/analysis/CharStream;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/analysis/CharStream;)V
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/analysis/CharStream;

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/apache/lucene/analysis/CharStream;-><init>()V

    .line 33
    iput-object p1, p0, Lorg/apache/lucene/analysis/CharFilter;->input:Lorg/apache/lucene/analysis/CharStream;

    .line 34
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharFilter;->input:Lorg/apache/lucene/analysis/CharStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/CharStream;->close()V

    .line 58
    return-void
.end method

.method protected correct(I)I
    .locals 0
    .param p1, "currentOff"    # I

    .prologue
    .line 43
    return p1
.end method

.method public final correctOffset(I)I
    .locals 2
    .param p1, "currentOff"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharFilter;->input:Lorg/apache/lucene/analysis/CharStream;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/CharFilter;->correct(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/CharStream;->correctOffset(I)I

    move-result v0

    return v0
.end method

.method public mark(I)V
    .locals 1
    .param p1, "readAheadLimit"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharFilter;->input:Lorg/apache/lucene/analysis/CharStream;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/CharStream;->mark(I)V

    .line 73
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharFilter;->input:Lorg/apache/lucene/analysis/CharStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/CharStream;->markSupported()Z

    move-result v0

    return v0
.end method

.method public read([CII)I
    .locals 1
    .param p1, "cbuf"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharFilter;->input:Lorg/apache/lucene/analysis/CharStream;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/analysis/CharStream;->read([CII)I

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharFilter;->input:Lorg/apache/lucene/analysis/CharStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/CharStream;->reset()V

    .line 78
    return-void
.end method
