.class public final Lorg/apache/lucene/analysis/LimitTokenCountAnalyzer;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "LimitTokenCountAnalyzer.java"


# instance fields
.field private final delegate:Lorg/apache/lucene/analysis/Analyzer;

.field private final maxTokenCount:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;I)V
    .locals 0
    .param p1, "delegate"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p2, "maxTokenCount"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/apache/lucene/analysis/LimitTokenCountAnalyzer;->delegate:Lorg/apache/lucene/analysis/Analyzer;

    .line 38
    iput p2, p0, Lorg/apache/lucene/analysis/LimitTokenCountAnalyzer;->maxTokenCount:I

    .line 39
    return-void
.end method


# virtual methods
.method public getOffsetGap(Lorg/apache/lucene/document/Fieldable;)I
    .locals 1
    .param p1, "field"    # Lorg/apache/lucene/document/Fieldable;

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/lucene/analysis/LimitTokenCountAnalyzer;->delegate:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/Analyzer;->getOffsetGap(Lorg/apache/lucene/document/Fieldable;)I

    move-result v0

    return v0
.end method

.method public getPositionIncrementGap(Ljava/lang/String;)I
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/analysis/LimitTokenCountAnalyzer;->delegate:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/Analyzer;->getPositionIncrementGap(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public reusableTokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lorg/apache/lucene/analysis/LimitTokenCountFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/LimitTokenCountAnalyzer;->delegate:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/analysis/Analyzer;->reusableTokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/analysis/LimitTokenCountAnalyzer;->maxTokenCount:I

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/analysis/LimitTokenCountFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;I)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "LimitTokenCountAnalyzer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/analysis/LimitTokenCountAnalyzer;->delegate:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", maxTokenCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/analysis/LimitTokenCountAnalyzer;->maxTokenCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 43
    new-instance v0, Lorg/apache/lucene/analysis/LimitTokenCountFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/LimitTokenCountAnalyzer;->delegate:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/analysis/LimitTokenCountAnalyzer;->maxTokenCount:I

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/analysis/LimitTokenCountFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;I)V

    return-object v0
.end method
