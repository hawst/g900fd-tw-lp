.class public final Lorg/apache/lucene/analysis/NumericTokenStream;
.super Lorg/apache/lucene/analysis/TokenStream;
.source "NumericTokenStream.java"


# static fields
.field public static final TOKEN_TYPE_FULL_PREC:Ljava/lang/String; = "fullPrecNumeric"

.field public static final TOKEN_TYPE_LOWER_PREC:Ljava/lang/String; = "lowerPrecNumeric"


# instance fields
.field private final posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private final precisionStep:I

.field private shift:I

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

.field private valSize:I

.field private value:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/NumericTokenStream;-><init>(I)V

    .line 100
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "precisionStep"    # I

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-direct {p0}, Lorg/apache/lucene/analysis/TokenStream;-><init>()V

    .line 244
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/NumericTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 245
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/NumericTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 246
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/NumericTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 248
    iput v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    iput v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    .line 251
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->value:J

    .line 109
    iput p1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    .line 110
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "precisionStep must be >=1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_0
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;I)V
    .locals 2
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "precisionStep"    # I

    .prologue
    const/4 v1, 0x0

    .line 135
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V

    .line 244
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/NumericTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 245
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/NumericTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 246
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/NumericTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 248
    iput v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    iput v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    .line 251
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->value:J

    .line 136
    iput p2, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    .line 137
    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    .line 138
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "precisionStep must be >=1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :cond_0
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource;I)V
    .locals 2
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p2, "precisionStep"    # I

    .prologue
    const/4 v1, 0x0

    .line 121
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource;)V

    .line 244
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/NumericTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 245
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/NumericTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 246
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/NumericTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 248
    iput v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    iput v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    .line 251
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->value:J

    .line 122
    iput p2, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    .line 123
    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    .line 124
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "precisionStep must be >=1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    return-void
.end method


# virtual methods
.method public getPrecisionStep()I
    .locals 1

    .prologue
    .line 240
    iget v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    return v0
.end method

.method public incrementToken()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 202
    iget v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    if-nez v1, :cond_0

    .line 203
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "call set???Value() before usage"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 204
    :cond_0
    iget v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    iget v4, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    if-lt v1, v4, :cond_1

    .line 228
    :goto_0
    return v3

    .line 207
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/NumericTokenStream;->clearAttributes()V

    .line 209
    iget v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    sparse-switch v1, :sswitch_data_0

    .line 222
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "valSize must be 32 or 64"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 211
    :sswitch_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    const/16 v4, 0xb

    invoke-interface {v1, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    move-result-object v0

    .line 212
    .local v0, "buffer":[C
    iget-object v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-wide v4, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->value:J

    iget v6, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    invoke-static {v4, v5, v6, v0}, Lorg/apache/lucene/util/NumericUtils;->longToPrefixCoded(JI[C)I

    move-result v4

    invoke-interface {v1, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 225
    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iget v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    if-nez v1, :cond_2

    const-string/jumbo v1, "fullPrecNumeric"

    :goto_2
    invoke-interface {v4, v1}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 226
    iget-object v4, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iget v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    invoke-interface {v4, v1}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 227
    iget v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    iget v3, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    add-int/2addr v1, v3

    iput v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    move v3, v2

    .line 228
    goto :goto_0

    .line 216
    .end local v0    # "buffer":[C
    :sswitch_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    const/4 v4, 0x6

    invoke-interface {v1, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    move-result-object v0

    .line 217
    .restart local v0    # "buffer":[C
    iget-object v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-wide v4, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->value:J

    long-to-int v4, v4

    iget v5, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    invoke-static {v4, v5, v0}, Lorg/apache/lucene/util/NumericUtils;->intToPrefixCoded(II[C)I

    move-result v4

    invoke-interface {v1, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    goto :goto_1

    .line 225
    :cond_2
    const-string/jumbo v1, "lowerPrecNumeric"

    goto :goto_2

    :cond_3
    move v1, v3

    .line 226
    goto :goto_3

    .line 209
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_1
        0x40 -> :sswitch_0
    .end sparse-switch
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 195
    iget v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    if-nez v0, :cond_0

    .line 196
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "call set???Value() before usage"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    .line 198
    return-void
.end method

.method public setDoubleValue(D)Lorg/apache/lucene/analysis/NumericTokenStream;
    .locals 3
    .param p1, "value"    # D

    .prologue
    .line 174
    invoke-static {p1, p2}, Lorg/apache/lucene/util/NumericUtils;->doubleToSortableLong(D)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->value:J

    .line 175
    const/16 v0, 0x40

    iput v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    .line 176
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    .line 177
    return-object p0
.end method

.method public setFloatValue(F)Lorg/apache/lucene/analysis/NumericTokenStream;
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 187
    invoke-static {p1}, Lorg/apache/lucene/util/NumericUtils;->floatToSortableInt(F)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->value:J

    .line 188
    const/16 v0, 0x20

    iput v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    .line 189
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    .line 190
    return-object p0
.end method

.method public setIntValue(I)Lorg/apache/lucene/analysis/NumericTokenStream;
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 161
    int-to-long v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->value:J

    .line 162
    const/16 v0, 0x20

    iput v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    .line 163
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    .line 164
    return-object p0
.end method

.method public setLongValue(J)Lorg/apache/lucene/analysis/NumericTokenStream;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 148
    iput-wide p1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->value:J

    .line 149
    const/16 v0, 0x40

    iput v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    .line 150
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->shift:I

    .line 151
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 233
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "(numeric,valSize="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 234
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, ",precisionStep="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 235
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
