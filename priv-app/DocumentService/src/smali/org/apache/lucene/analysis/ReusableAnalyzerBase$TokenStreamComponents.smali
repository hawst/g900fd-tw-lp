.class public Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;
.super Ljava/lang/Object;
.source "ReusableAnalyzerBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/ReusableAnalyzerBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TokenStreamComponents"
.end annotation


# instance fields
.field protected final sink:Lorg/apache/lucene/analysis/TokenStream;

.field protected final source:Lorg/apache/lucene/analysis/Tokenizer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/Tokenizer;)V
    .locals 0
    .param p1, "source"    # Lorg/apache/lucene/analysis/Tokenizer;

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    iput-object p1, p0, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;->source:Lorg/apache/lucene/analysis/Tokenizer;

    .line 136
    iput-object p1, p0, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;->sink:Lorg/apache/lucene/analysis/TokenStream;

    .line 137
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 0
    .param p1, "source"    # Lorg/apache/lucene/analysis/Tokenizer;
    .param p2, "result"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p1, p0, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;->source:Lorg/apache/lucene/analysis/Tokenizer;

    .line 125
    iput-object p2, p0, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;->sink:Lorg/apache/lucene/analysis/TokenStream;

    .line 126
    return-void
.end method


# virtual methods
.method protected getTokenStream()Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;->sink:Lorg/apache/lucene/analysis/TokenStream;

    return-object v0
.end method

.method protected reset(Ljava/io/Reader;)Z
    .locals 1
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;->source:Lorg/apache/lucene/analysis/Tokenizer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/Tokenizer;->reset(Ljava/io/Reader;)V

    .line 155
    const/4 v0, 0x1

    return v0
.end method
