.class public Lorg/apache/lucene/analysis/MappingCharFilter;
.super Lorg/apache/lucene/analysis/BaseCharFilter;
.source "MappingCharFilter.java"


# instance fields
.field private buffer:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field private charPointer:I

.field private nextCharCounter:I

.field private final normMap:Lorg/apache/lucene/analysis/NormalizeCharMap;

.field private replacement:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/NormalizeCharMap;Ljava/io/Reader;)V
    .locals 1
    .param p1, "normMap"    # Lorg/apache/lucene/analysis/NormalizeCharMap;
    .param p2, "in"    # Ljava/io/Reader;

    .prologue
    .line 46
    invoke-static {p2}, Lorg/apache/lucene/analysis/CharReader;->get(Ljava/io/Reader;)Lorg/apache/lucene/analysis/CharStream;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/BaseCharFilter;-><init>(Lorg/apache/lucene/analysis/CharStream;)V

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->normMap:Lorg/apache/lucene/analysis/NormalizeCharMap;

    .line 48
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/NormalizeCharMap;Lorg/apache/lucene/analysis/CharStream;)V
    .locals 0
    .param p1, "normMap"    # Lorg/apache/lucene/analysis/NormalizeCharMap;
    .param p2, "in"    # Lorg/apache/lucene/analysis/CharStream;

    .prologue
    .line 40
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/BaseCharFilter;-><init>(Lorg/apache/lucene/analysis/CharStream;)V

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->normMap:Lorg/apache/lucene/analysis/NormalizeCharMap;

    .line 42
    return-void
.end method

.method private match(Lorg/apache/lucene/analysis/NormalizeCharMap;)Lorg/apache/lucene/analysis/NormalizeCharMap;
    .locals 5
    .param p1, "map"    # Lorg/apache/lucene/analysis/NormalizeCharMap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    const/4 v1, 0x0

    .line 102
    .local v1, "result":Lorg/apache/lucene/analysis/NormalizeCharMap;
    iget-object v3, p1, Lorg/apache/lucene/analysis/NormalizeCharMap;->submap:Ljava/util/Map;

    if-eqz v3, :cond_1

    .line 103
    invoke-direct {p0}, Lorg/apache/lucene/analysis/MappingCharFilter;->nextChar()I

    move-result v0

    .line 104
    .local v0, "chr":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    .line 105
    iget-object v3, p1, Lorg/apache/lucene/analysis/NormalizeCharMap;->submap:Ljava/util/Map;

    int-to-char v4, v0

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/analysis/NormalizeCharMap;

    .line 106
    .local v2, "subMap":Lorg/apache/lucene/analysis/NormalizeCharMap;
    if-eqz v2, :cond_0

    .line 107
    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/MappingCharFilter;->match(Lorg/apache/lucene/analysis/NormalizeCharMap;)Lorg/apache/lucene/analysis/NormalizeCharMap;

    move-result-object v1

    .line 109
    :cond_0
    if-nez v1, :cond_1

    .line 110
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/MappingCharFilter;->pushChar(I)V

    .line 114
    .end local v0    # "chr":I
    .end local v2    # "subMap":Lorg/apache/lucene/analysis/NormalizeCharMap;
    :cond_1
    if-nez v1, :cond_2

    iget-object v3, p1, Lorg/apache/lucene/analysis/NormalizeCharMap;->normStr:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 115
    move-object v1, p1

    .line 117
    :cond_2
    return-object v1
.end method

.method private nextChar()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    iget v0, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->nextCharCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->nextCharCounter:I

    .line 80
    iget-object v0, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->buffer:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->buffer:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->buffer:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 83
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->input:Lorg/apache/lucene/analysis/CharStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/CharStream;->read()I

    move-result v0

    goto :goto_0
.end method

.method private pushChar(I)V
    .locals 2
    .param p1, "c"    # I

    .prologue
    .line 87
    iget v0, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->nextCharCounter:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->nextCharCounter:I

    .line 88
    iget-object v0, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->buffer:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->buffer:Ljava/util/LinkedList;

    .line 90
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->buffer:Ljava/util/LinkedList;

    int-to-char v1, p1

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 91
    return-void
.end method

.method private pushLastChar(I)V
    .locals 2
    .param p1, "c"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->buffer:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    .line 95
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->buffer:Ljava/util/LinkedList;

    .line 97
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->buffer:Ljava/util/LinkedList;

    int-to-char v1, p1

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 98
    return-void
.end method


# virtual methods
.method public read()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    .line 53
    :cond_0
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->replacement:Ljava/lang/String;

    if-eqz v5, :cond_2

    iget v5, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->charPointer:I

    iget-object v7, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->replacement:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v5, v7, :cond_2

    .line 54
    iget-object v5, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->replacement:Ljava/lang/String;

    iget v6, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->charPointer:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->charPointer:I

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 63
    :cond_1
    :goto_1
    return v0

    .line 57
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/analysis/MappingCharFilter;->nextChar()I

    move-result v0

    .line 58
    .local v0, "firstChar":I
    if-ne v0, v6, :cond_3

    move v0, v6

    goto :goto_1

    .line 59
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->normMap:Lorg/apache/lucene/analysis/NormalizeCharMap;

    iget-object v5, v5, Lorg/apache/lucene/analysis/NormalizeCharMap;->submap:Ljava/util/Map;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->normMap:Lorg/apache/lucene/analysis/NormalizeCharMap;

    iget-object v5, v5, Lorg/apache/lucene/analysis/NormalizeCharMap;->submap:Ljava/util/Map;

    int-to-char v7, v0

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/analysis/NormalizeCharMap;

    move-object v2, v5

    .line 61
    .local v2, "nm":Lorg/apache/lucene/analysis/NormalizeCharMap;
    :goto_2
    if-eqz v2, :cond_1

    .line 62
    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/MappingCharFilter;->match(Lorg/apache/lucene/analysis/NormalizeCharMap;)Lorg/apache/lucene/analysis/NormalizeCharMap;

    move-result-object v4

    .line 63
    .local v4, "result":Lorg/apache/lucene/analysis/NormalizeCharMap;
    if-eqz v4, :cond_1

    .line 64
    iget-object v5, v4, Lorg/apache/lucene/analysis/NormalizeCharMap;->normStr:Ljava/lang/String;

    iput-object v5, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->replacement:Ljava/lang/String;

    .line 65
    const/4 v5, 0x0

    iput v5, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->charPointer:I

    .line 66
    iget v5, v4, Lorg/apache/lucene/analysis/NormalizeCharMap;->diff:I

    if-eqz v5, :cond_0

    .line 67
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/MappingCharFilter;->getLastCumulativeDiff()I

    move-result v3

    .line 68
    .local v3, "prevCumulativeDiff":I
    iget v5, v4, Lorg/apache/lucene/analysis/NormalizeCharMap;->diff:I

    if-gez v5, :cond_5

    .line 69
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    iget v5, v4, Lorg/apache/lucene/analysis/NormalizeCharMap;->diff:I

    neg-int v5, v5

    if-ge v1, v5, :cond_0

    .line 70
    iget v5, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->nextCharCounter:I

    add-int/2addr v5, v1

    sub-int/2addr v5, v3

    add-int/lit8 v7, v3, -0x1

    sub-int/2addr v7, v1

    invoke-virtual {p0, v5, v7}, Lorg/apache/lucene/analysis/MappingCharFilter;->addOffCorrectMap(II)V

    .line 69
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 59
    .end local v1    # "i":I
    .end local v2    # "nm":Lorg/apache/lucene/analysis/NormalizeCharMap;
    .end local v3    # "prevCumulativeDiff":I
    .end local v4    # "result":Lorg/apache/lucene/analysis/NormalizeCharMap;
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 72
    .restart local v2    # "nm":Lorg/apache/lucene/analysis/NormalizeCharMap;
    .restart local v3    # "prevCumulativeDiff":I
    .restart local v4    # "result":Lorg/apache/lucene/analysis/NormalizeCharMap;
    :cond_5
    iget v5, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->nextCharCounter:I

    iget v7, v4, Lorg/apache/lucene/analysis/NormalizeCharMap;->diff:I

    sub-int/2addr v5, v7

    sub-int/2addr v5, v3

    iget v7, v4, Lorg/apache/lucene/analysis/NormalizeCharMap;->diff:I

    add-int/2addr v7, v3

    invoke-virtual {p0, v5, v7}, Lorg/apache/lucene/analysis/MappingCharFilter;->addOffCorrectMap(II)V

    goto :goto_0
.end method

.method public read([CII)I
    .locals 7
    .param p1, "cbuf"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 122
    new-array v3, p3, [C

    .line 123
    .local v3, "tmp":[C
    iget-object v5, p0, Lorg/apache/lucene/analysis/MappingCharFilter;->input:Lorg/apache/lucene/analysis/CharStream;

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6, p3}, Lorg/apache/lucene/analysis/CharStream;->read([CII)I

    move-result v2

    .line 124
    .local v2, "l":I
    if-eq v2, v4, :cond_0

    .line 125
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 126
    aget-char v5, v3, v1

    invoke-direct {p0, v5}, Lorg/apache/lucene/analysis/MappingCharFilter;->pushLastChar(I)V

    .line 125
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 128
    .end local v1    # "i":I
    :cond_0
    const/4 v2, 0x0

    .line 129
    move v1, p2

    .restart local v1    # "i":I
    :goto_1
    add-int v5, p2, p3

    if-ge v1, v5, :cond_1

    .line 130
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/MappingCharFilter;->read()I

    move-result v0

    .line 131
    .local v0, "c":I
    if-ne v0, v4, :cond_3

    .line 135
    .end local v0    # "c":I
    :cond_1
    if-nez v2, :cond_2

    move v2, v4

    .end local v2    # "l":I
    :cond_2
    return v2

    .line 132
    .restart local v0    # "c":I
    .restart local v2    # "l":I
    :cond_3
    int-to-char v5, v0

    aput-char v5, p1, v1

    .line 133
    add-int/lit8 v2, v2, 0x1

    .line 129
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
