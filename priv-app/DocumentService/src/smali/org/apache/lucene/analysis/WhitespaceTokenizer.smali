.class public final Lorg/apache/lucene/analysis/WhitespaceTokenizer;
.super Lorg/apache/lucene/analysis/CharTokenizer;
.source "WhitespaceTokenizer.java"


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 0
    .param p1, "in"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Ljava/io/Reader;)V

    .line 90
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 0
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "in"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 113
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V
    .locals 0
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p2, "in"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V

    .line 101
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Ljava/io/Reader;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p3, "in"    # Ljava/io/Reader;

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p3, "in"    # Ljava/io/Reader;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V

    .line 63
    return-void
.end method


# virtual methods
.method protected isTokenChar(I)Z
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 119
    invoke-static {p1}, Ljava/lang/Character;->isWhitespace(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
