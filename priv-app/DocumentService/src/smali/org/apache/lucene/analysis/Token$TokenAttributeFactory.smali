.class public final Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;
.super Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
.source "Token.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/Token;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TokenAttributeFactory"
.end annotation


# instance fields
.field private final delegate:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V
    .locals 0
    .param p1, "delegate"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    .prologue
    .line 622
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;-><init>()V

    .line 623
    iput-object p1, p0, Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;->delegate:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    .line 624
    return-void
.end method


# virtual methods
.method public createAttributeInstance(Ljava/lang/Class;)Lorg/apache/lucene/util/AttributeImpl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;)",
            "Lorg/apache/lucene/util/AttributeImpl;"
        }
    .end annotation

    .prologue
    .line 628
    .local p1, "attClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;"
    const-class v0, Lorg/apache/lucene/analysis/Token;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/lucene/analysis/Token;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/Token;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;->delegate:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->createAttributeInstance(Ljava/lang/Class;)Lorg/apache/lucene/util/AttributeImpl;

    move-result-object v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 634
    if-ne p0, p1, :cond_0

    const/4 v1, 0x1

    .line 639
    :goto_0
    return v1

    .line 635
    :cond_0
    instance-of v1, p1, Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 636
    check-cast v0, Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;

    .line 637
    .local v0, "af":Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;->delegate:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    iget-object v2, v0, Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;->delegate:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 639
    .end local v0    # "af":Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 644
    iget-object v0, p0, Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;->delegate:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0xa45aa31

    xor-int/2addr v0, v1

    return v0
.end method
