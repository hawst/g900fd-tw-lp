.class public Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "CharTermAttributeImpl.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
.implements Lorg/apache/lucene/analysis/tokenattributes/TermAttribute;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static MIN_BUFFER_SIZE:I


# instance fields
.field private termBuffer:[C

.field private termLength:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->$assertionsDisabled:Z

    .line 32
    const/16 v0, 0xa

    sput v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->MIN_BUFFER_SIZE:I

    return-void

    .line 31
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    .line 34
    sget v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->MIN_BUFFER_SIZE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    return-void
.end method

.method private appendNull()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 4

    .prologue
    const/16 v3, 0x6c

    .line 222
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->resizeBuffer(I)[C

    .line 223
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    const/16 v2, 0x6e

    aput-char v2, v0, v1

    .line 224
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    const/16 v2, 0x75

    aput-char v2, v0, v1

    .line 225
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    aput-char v3, v0, v1

    .line 226
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    aput-char v3, v0, v1

    .line 227
    return-object p0
.end method

.method private growTermBuffer(I)V
    .locals 1
    .param p1, "newSize"    # I

    .prologue
    .line 97
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    array-length v0, v0

    if-ge v0, p1, :cond_0

    .line 100
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    .line 102
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic append(C)Ljava/lang/Appendable;
    .locals 1
    .param p1, "x0"    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->append(C)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
    .locals 1
    .param p1, "x0"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->append(Ljava/lang/CharSequence;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
    .locals 1
    .param p1, "x0"    # Ljava/lang/CharSequence;
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v0

    return-object v0
.end method

.method public final append(C)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 3
    .param p1, "c"    # C

    .prologue
    .line 188
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->resizeBuffer(I)[C

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    aput-char p1, v0, v1

    .line 189
    return-object p0
.end method

.method public final append(Ljava/lang/CharSequence;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 2
    .param p1, "csq"    # Ljava/lang/CharSequence;

    .prologue
    .line 146
    if-nez p1, :cond_0

    .line 147
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->appendNull()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v0

    .line 148
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v0

    goto :goto_0
.end method

.method public final append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 8
    .param p1, "csq"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 152
    if-nez p1, :cond_0

    .line 153
    const-string/jumbo p1, "null"

    .line 154
    :cond_0
    sub-int v2, p3, p2

    .local v2, "len":I
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 155
    .local v1, "csqlen":I
    if-ltz v2, :cond_1

    if-gt p2, v1, :cond_1

    if-le p3, v1, :cond_2

    .line 156
    :cond_1
    new-instance v4, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v4}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v4

    .line 157
    :cond_2
    if-nez v2, :cond_3

    .line 183
    .end local p1    # "csq":Ljava/lang/CharSequence;
    :goto_0
    return-object p0

    .line 159
    .restart local p1    # "csq":Ljava/lang/CharSequence;
    :cond_3
    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v4, v2

    invoke-virtual {p0, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->resizeBuffer(I)[C

    .line 160
    const/4 v4, 0x4

    if-le v2, v4, :cond_b

    .line 161
    instance-of v4, p1, Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 162
    check-cast p1, Ljava/lang/String;

    .end local p1    # "csq":Ljava/lang/CharSequence;
    iget-object v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v5, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-virtual {p1, p2, p3, v4, v5}, Ljava/lang/String;->getChars(II[CI)V

    .line 178
    :goto_1
    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v4, v2

    iput v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    goto :goto_0

    .line 163
    .restart local p1    # "csq":Ljava/lang/CharSequence;
    :cond_4
    instance-of v4, p1, Ljava/lang/StringBuilder;

    if-eqz v4, :cond_5

    .line 164
    check-cast p1, Ljava/lang/StringBuilder;

    .end local p1    # "csq":Ljava/lang/CharSequence;
    iget-object v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v5, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-virtual {p1, p2, p3, v4, v5}, Ljava/lang/StringBuilder;->getChars(II[CI)V

    goto :goto_1

    .line 165
    .restart local p1    # "csq":Ljava/lang/CharSequence;
    :cond_5
    instance-of v4, p1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    if-eqz v4, :cond_6

    .line 166
    check-cast p1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .end local p1    # "csq":Ljava/lang/CharSequence;
    invoke-interface {p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v6, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-static {v4, p2, v5, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    .line 167
    .restart local p1    # "csq":Ljava/lang/CharSequence;
    :cond_6
    instance-of v4, p1, Ljava/nio/CharBuffer;

    if-eqz v4, :cond_7

    move-object v4, p1

    check-cast v4, Ljava/nio/CharBuffer;

    invoke-virtual {v4}, Ljava/nio/CharBuffer;->hasArray()Z

    move-result v4

    if-eqz v4, :cond_7

    move-object v0, p1

    .line 168
    check-cast v0, Ljava/nio/CharBuffer;

    .line 169
    .local v0, "cb":Ljava/nio/CharBuffer;
    invoke-virtual {v0}, Ljava/nio/CharBuffer;->array()[C

    move-result-object v4

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->arrayOffset()I

    move-result v5

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->position()I

    move-result v6

    add-int/2addr v5, v6

    add-int/2addr v5, p2

    iget-object v6, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v7, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-static {v4, v5, v6, v7, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    .line 170
    .end local v0    # "cb":Ljava/nio/CharBuffer;
    :cond_7
    instance-of v4, p1, Ljava/lang/StringBuffer;

    if-eqz v4, :cond_a

    .line 171
    check-cast p1, Ljava/lang/StringBuffer;

    .end local p1    # "csq":Ljava/lang/CharSequence;
    iget-object v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v5, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-virtual {p1, p2, p3, v4, v5}, Ljava/lang/StringBuffer;->getChars(II[CI)V

    goto :goto_1

    .line 173
    .end local p2    # "start":I
    .local v3, "start":I
    .restart local p1    # "csq":Ljava/lang/CharSequence;
    :goto_2
    if-ge v3, p3, :cond_8

    .line 174
    iget-object v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v5, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 p2, v3, 0x1

    .end local v3    # "start":I
    .restart local p2    # "start":I
    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    aput-char v6, v4, v5

    move v3, p2

    .end local p2    # "start":I
    .restart local v3    # "start":I
    goto :goto_2

    :cond_8
    move p2, v3

    .line 176
    .end local v3    # "start":I
    .restart local p2    # "start":I
    goto/16 :goto_0

    .line 181
    .end local p2    # "start":I
    .restart local v3    # "start":I
    :goto_3
    if-ge v3, p3, :cond_9

    .line 182
    iget-object v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v5, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 p2, v3, 0x1

    .end local v3    # "start":I
    .restart local p2    # "start":I
    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    aput-char v6, v4, v5

    move v3, p2

    .end local p2    # "start":I
    .restart local v3    # "start":I
    goto :goto_3

    :cond_9
    move p2, v3

    .line 183
    .end local v3    # "start":I
    .restart local p2    # "start":I
    goto/16 :goto_0

    :cond_a
    move v3, p2

    .end local p2    # "start":I
    .restart local v3    # "start":I
    goto :goto_2

    .end local v3    # "start":I
    .restart local p2    # "start":I
    :cond_b
    move v3, p2

    .end local p2    # "start":I
    .restart local v3    # "start":I
    goto :goto_3
.end method

.method public final append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 195
    if-nez p1, :cond_0

    .line 196
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->appendNull()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object p0

    .line 200
    .end local p0    # "this":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    :goto_0
    return-object p0

    .line 197
    .restart local p0    # "this":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 198
    .local v0, "len":I
    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v2, v0

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->resizeBuffer(I)[C

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-virtual {p1, v1, v0, v2, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 199
    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    goto :goto_0
.end method

.method public final append(Ljava/lang/StringBuilder;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 4
    .param p1, "s"    # Ljava/lang/StringBuilder;

    .prologue
    .line 204
    if-nez p1, :cond_0

    .line 205
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->appendNull()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object p0

    .line 209
    .end local p0    # "this":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    :goto_0
    return-object p0

    .line 206
    .restart local p0    # "this":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 207
    .local v0, "len":I
    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v2, v0

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->resizeBuffer(I)[C

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-virtual {p1, v1, v0, v2, v3}, Ljava/lang/StringBuilder;->getChars(II[CI)V

    .line 208
    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    goto :goto_0
.end method

.method public final append(Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 5
    .param p1, "ta"    # Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .prologue
    .line 213
    if-nez p1, :cond_0

    .line 214
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->appendNull()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object p0

    .line 218
    .end local p0    # "this":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    :goto_0
    return-object p0

    .line 215
    .restart local p0    # "this":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    :cond_0
    invoke-interface {p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v0

    .line 216
    .local v0, "len":I
    invoke-interface {p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v1

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v3, v0

    invoke-virtual {p0, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->resizeBuffer(I)[C

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-static {v1, v2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 217
    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    goto :goto_0
.end method

.method public final buffer()[C
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    return-object v0
.end method

.method public final charAt(I)C
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 132
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    if-lt p1, v0, :cond_0

    .line 133
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 134
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    aget-char v0, v0, p1

    return v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    .line 242
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 246
    invoke-super {p0}, Lorg/apache/lucene/util/AttributeImpl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;

    .line 248
    .local v0, "t":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    new-array v1, v1, [C

    iput-object v1, v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    .line 249
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget-object v2, v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 250
    return-object v0
.end method

.method public final copyBuffer([CII)V
    .locals 2
    .param p1, "buffer"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 44
    invoke-direct {p0, p3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->growTermBuffer(I)V

    .line 45
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 46
    iput p3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    .line 47
    return-void
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 4
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    const/4 v3, 0x0

    .line 296
    instance-of v1, p1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 297
    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 298
    .local v0, "t":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-interface {v0, v1, v3, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 303
    .end local v0    # "t":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 300
    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TermAttribute;

    .line 301
    .local v0, "t":Lorg/apache/lucene/analysis/tokenattributes/TermAttribute;
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-interface {v0, v1, v3, v2}, Lorg/apache/lucene/analysis/tokenattributes/TermAttribute;->setTermBuffer([CII)V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 255
    if-ne p1, p0, :cond_1

    .line 271
    :cond_0
    :goto_0
    return v2

    .line 259
    :cond_1
    instance-of v4, p1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;

    if-eqz v4, :cond_4

    move-object v1, p1

    .line 260
    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;

    .line 261
    .local v1, "o":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    iget v5, v1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    if-eq v4, v5, :cond_2

    move v2, v3

    .line 262
    goto :goto_0

    .line 263
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    if-ge v0, v4, :cond_0

    .line 264
    iget-object v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    aget-char v4, v4, v0

    iget-object v5, v1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    aget-char v5, v5, v0

    if-eq v4, v5, :cond_3

    move v2, v3

    .line 265
    goto :goto_0

    .line 263
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "o":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    :cond_4
    move v2, v3

    .line 271
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 234
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    .line 235
    .local v0, "code":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    const/4 v3, 0x0

    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/util/ArrayUtil;->hashCode([CII)I

    move-result v2

    add-int v0, v1, v2

    .line 236
    return v0
.end method

.method public final length()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    return v0
.end method

.method public reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V
    .locals 3
    .param p1, "reflector"    # Lorg/apache/lucene/util/AttributeReflector;

    .prologue
    .line 291
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    const-string/jumbo v1, "term"

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 292
    return-void
.end method

.method public final resizeBuffer(I)[C
    .locals 4
    .param p1, "newSize"    # I

    .prologue
    const/4 v3, 0x0

    .line 81
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    array-length v1, v1

    if-ge v1, p1, :cond_0

    .line 84
    const/4 v1, 0x2

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [C

    .line 85
    .local v0, "newCharBuffer":[C
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget-object v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    iput-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    .line 88
    .end local v0    # "newCharBuffer":[C
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    return-object v1
.end method

.method public resizeTermBuffer(I)[C
    .locals 1
    .param p1, "newSize"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 93
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->resizeBuffer(I)[C

    move-result-object v0

    return-object v0
.end method

.method public final setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    .line 118
    return-object p0
.end method

.method public final setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 3
    .param p1, "length"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    array-length v0, v0

    if-le p1, v0, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "length "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " exceeds the size of the termBuffer ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_0
    iput p1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    .line 113
    return-object p0
.end method

.method public setTermBuffer(Ljava/lang/String;)V
    .locals 3
    .param p1, "buffer"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 56
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 57
    .local v0, "length":I
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->growTermBuffer(I)V

    .line 58
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    invoke-virtual {p1, v2, v0, v1, v2}, Ljava/lang/String;->getChars(II[CI)V

    .line 59
    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    .line 60
    return-void
.end method

.method public setTermBuffer(Ljava/lang/String;II)V
    .locals 3
    .param p1, "buffer"    # Ljava/lang/String;
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 64
    sget-boolean v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le p2, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 65
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    add-int v0, p2, p3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 66
    :cond_1
    invoke-direct {p0, p3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->growTermBuffer(I)V

    .line 67
    add-int v0, p2, p3

    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    const/4 v2, 0x0

    invoke-virtual {p1, p2, v0, v1, v2}, Ljava/lang/String;->getChars(II[CI)V

    .line 68
    iput p3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    .line 69
    return-void
.end method

.method public setTermBuffer([CII)V
    .locals 0
    .param p1, "buffer"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->copyBuffer([CII)V

    .line 52
    return-void
.end method

.method public setTermLength(I)V
    .locals 0
    .param p1, "length"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 124
    return-void
.end method

.method public final subSequence(II)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 138
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    if-gt p1, v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    if-le p2, v0, :cond_1

    .line 139
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 140
    :cond_1
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    sub-int v2, p2, p1

    invoke-direct {v0, v1, p1, v2}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method public term()Ljava/lang/String;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 40
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method public termBuffer()[C
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    return-object v0
.end method

.method public termLength()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 106
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 286
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method
