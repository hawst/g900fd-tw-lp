.class public Lorg/apache/lucene/analysis/Token;
.super Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;
.source "Token.java"

# interfaces
.implements Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;
.implements Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
.implements Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;
.implements Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
.implements Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;
    }
.end annotation


# static fields
.field public static final TOKEN_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;


# instance fields
.field private endOffset:I

.field private flags:I

.field private payload:Lorg/apache/lucene/index/Payload;

.field private positionIncrement:I

.field private startOffset:I

.field private type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 609
    new-instance v0, Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;

    sget-object v1, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->DEFAULT_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/Token$TokenAttributeFactory;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V

    sput-object v0, Lorg/apache/lucene/analysis/Token;->TOKEN_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;-><init>()V

    .line 128
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 131
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 135
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 141
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;-><init>()V

    .line 128
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 131
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 142
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 143
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 144
    return-void
.end method

.method public constructor <init>(III)V
    .locals 1
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "flags"    # I

    .prologue
    .line 164
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;-><init>()V

    .line 128
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 131
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 165
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 166
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 167
    iput p3, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 168
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;)V
    .locals 1
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "typ"    # Ljava/lang/String;

    .prologue
    .line 151
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;-><init>()V

    .line 128
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 131
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 152
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 153
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 154
    iput-object p3, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 155
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 179
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;-><init>()V

    .line 128
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 131
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 180
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/Token;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 181
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 182
    iput p3, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 183
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;III)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "flags"    # I

    .prologue
    .line 211
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;-><init>()V

    .line 128
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 131
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 212
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/Token;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 213
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 214
    iput p3, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 215
    iput p4, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 216
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "typ"    # Ljava/lang/String;

    .prologue
    .line 194
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;-><init>()V

    .line 128
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 131
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 195
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/Token;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 196
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 197
    iput p3, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 198
    iput-object p4, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 199
    return-void
.end method

.method public constructor <init>([CIIII)V
    .locals 1
    .param p1, "startTermBuffer"    # [C
    .param p2, "termBufferOffset"    # I
    .param p3, "termBufferLength"    # I
    .param p4, "start"    # I
    .param p5, "end"    # I

    .prologue
    .line 228
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;-><init>()V

    .line 128
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 131
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 229
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/Token;->copyBuffer([CII)V

    .line 230
    iput p4, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 231
    iput p5, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 232
    return-void
.end method

.method private clearNoTermBuffer()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 429
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    .line 430
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 431
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 432
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    iput v1, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 433
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 434
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 361
    invoke-super {p0}, Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;->clear()V

    .line 362
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    .line 363
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 364
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 365
    iput v1, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    iput v1, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 366
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 367
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 371
    invoke-super {p0}, Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/Token;

    .line 373
    .local v0, "t":Lorg/apache/lucene/analysis/Token;
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    if-eqz v1, :cond_0

    .line 374
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Payload;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/Payload;

    iput-object v1, v0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    .line 376
    :cond_0
    return-object v0
.end method

.method public clone([CIIII)Lorg/apache/lucene/analysis/Token;
    .locals 6
    .param p1, "newTermBuffer"    # [C
    .param p2, "newTermOffset"    # I
    .param p3, "newTermLength"    # I
    .param p4, "newStartOffset"    # I
    .param p5, "newEndOffset"    # I

    .prologue
    .line 385
    new-instance v0, Lorg/apache/lucene/analysis/Token;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/Token;-><init>([CIIII)V

    .line 386
    .local v0, "t":Lorg/apache/lucene/analysis/Token;
    iget v1, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    iput v1, v0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 387
    iget v1, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    iput v1, v0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 388
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    iput-object v1, v0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 389
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    if-eqz v1, :cond_0

    .line 390
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Payload;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/Payload;

    iput-object v1, v0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    .line 391
    :cond_0
    return-object v0
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 4
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 576
    instance-of v1, p1, Lorg/apache/lucene/analysis/Token;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 577
    check-cast v0, Lorg/apache/lucene/analysis/Token;

    .line 578
    .local v0, "to":Lorg/apache/lucene/analysis/Token;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/analysis/Token;->reinit(Lorg/apache/lucene/analysis/Token;)V

    .line 580
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    if-eqz v1, :cond_0

    .line 581
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Payload;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/Payload;

    iput-object v1, v0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    .line 591
    .end local v0    # "to":Lorg/apache/lucene/analysis/Token;
    .end local p1    # "target":Lorg/apache/lucene/util/AttributeImpl;
    :cond_0
    :goto_0
    return-void

    .line 584
    .restart local p1    # "target":Lorg/apache/lucene/util/AttributeImpl;
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;->copyTo(Lorg/apache/lucene/util/AttributeImpl;)V

    move-object v1, p1

    .line 585
    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    iget v3, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    invoke-interface {v1, v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    move-object v1, p1

    .line 586
    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    invoke-interface {v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    move-object v1, p1

    .line 587
    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    iget-object v2, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    invoke-interface {v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->setPayload(Lorg/apache/lucene/index/Payload;)V

    move-object v1, p1

    .line 588
    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    invoke-interface {v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;->setFlags(I)V

    .line 589
    check-cast p1, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .end local p1    # "target":Lorg/apache/lucene/util/AttributeImpl;
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    invoke-interface {p1, v1}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    goto :goto_0

    .line 587
    .restart local p1    # "target":Lorg/apache/lucene/util/AttributeImpl;
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Payload;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/Payload;

    goto :goto_1
.end method

.method public final endOffset()I
    .locals 1

    .prologue
    .line 294
    iget v0, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 396
    if-ne p1, p0, :cond_1

    .line 410
    :cond_0
    :goto_0
    return v1

    .line 399
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/analysis/Token;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 400
    check-cast v0, Lorg/apache/lucene/analysis/Token;

    .line 401
    .local v0, "other":Lorg/apache/lucene/analysis/Token;
    iget v3, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    iget v4, v0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    if-ne v3, v4, :cond_2

    iget v3, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    iget v4, v0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    if-ne v3, v4, :cond_2

    iget v3, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    iget v4, v0, Lorg/apache/lucene/analysis/Token;->flags:I

    if-ne v3, v4, :cond_2

    iget v3, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    iget v4, v0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    if-nez v3, :cond_3

    iget-object v3, v0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    if-nez v3, :cond_4

    iget-object v3, v0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    if-nez v3, :cond_2

    :goto_2
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    iget-object v4, v0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Payload;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .end local v0    # "other":Lorg/apache/lucene/analysis/Token;
    :cond_5
    move v1, v2

    .line 410
    goto :goto_0
.end method

.method public getFlags()I
    .locals 1

    .prologue
    .line 332
    iget v0, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    return v0
.end method

.method public getPayload()Lorg/apache/lucene/index/Payload;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    return-object v0
.end method

.method public getPositionIncrement()I
    .locals 1

    .prologue
    .line 271
    iget v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 415
    invoke-super {p0}, Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;->hashCode()I

    move-result v0

    .line 416
    .local v0, "code":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    add-int v0, v1, v2

    .line 417
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    add-int v0, v1, v2

    .line 418
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    add-int v0, v1, v2

    .line 419
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    add-int v0, v1, v2

    .line 420
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 421
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 422
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    if-eqz v1, :cond_1

    .line 423
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Payload;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 424
    :cond_1
    return v0
.end method

.method public reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V
    .locals 3
    .param p1, "reflector"    # Lorg/apache/lucene/util/AttributeReflector;

    .prologue
    .line 595
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/tokenattributes/TermAttributeImpl;->reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V

    .line 596
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    const-string/jumbo v1, "startOffset"

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 597
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    const-string/jumbo v1, "endOffset"

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 598
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    const-string/jumbo v1, "positionIncrement"

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 599
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    const-string/jumbo v1, "payload"

    iget-object v2, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 600
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    const-string/jumbo v1, "flags"

    iget v2, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 601
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    const-string/jumbo v1, "type"

    iget-object v2, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 602
    return-void
.end method

.method public reinit(Ljava/lang/String;II)Lorg/apache/lucene/analysis/Token;
    .locals 1
    .param p1, "newTerm"    # Ljava/lang/String;
    .param p2, "newStartOffset"    # I
    .param p3, "newEndOffset"    # I

    .prologue
    .line 505
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Token;->clear()V

    .line 506
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/Token;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 507
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 508
    iput p3, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 509
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 510
    return-object p0
.end method

.method public reinit(Ljava/lang/String;IIII)Lorg/apache/lucene/analysis/Token;
    .locals 1
    .param p1, "newTerm"    # Ljava/lang/String;
    .param p2, "newTermOffset"    # I
    .param p3, "newTermLength"    # I
    .param p4, "newStartOffset"    # I
    .param p5, "newEndOffset"    # I

    .prologue
    .line 520
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Token;->clear()V

    .line 521
    add-int v0, p2, p3

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/Token;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 522
    iput p4, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 523
    iput p5, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 524
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 525
    return-object p0
.end method

.method public reinit(Ljava/lang/String;IIIILjava/lang/String;)Lorg/apache/lucene/analysis/Token;
    .locals 1
    .param p1, "newTerm"    # Ljava/lang/String;
    .param p2, "newTermOffset"    # I
    .param p3, "newTermLength"    # I
    .param p4, "newStartOffset"    # I
    .param p5, "newEndOffset"    # I
    .param p6, "newType"    # Ljava/lang/String;

    .prologue
    .line 490
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Token;->clear()V

    .line 491
    add-int v0, p2, p3

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/Token;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 492
    iput p4, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 493
    iput p5, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 494
    iput-object p6, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 495
    return-object p0
.end method

.method public reinit(Ljava/lang/String;IILjava/lang/String;)Lorg/apache/lucene/analysis/Token;
    .locals 0
    .param p1, "newTerm"    # Ljava/lang/String;
    .param p2, "newStartOffset"    # I
    .param p3, "newEndOffset"    # I
    .param p4, "newType"    # Ljava/lang/String;

    .prologue
    .line 475
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Token;->clear()V

    .line 476
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/Token;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 477
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 478
    iput p3, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 479
    iput-object p4, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 480
    return-object p0
.end method

.method public reinit([CIIII)Lorg/apache/lucene/analysis/Token;
    .locals 1
    .param p1, "newTermBuffer"    # [C
    .param p2, "newTermOffset"    # I
    .param p3, "newTermLength"    # I
    .param p4, "newStartOffset"    # I
    .param p5, "newEndOffset"    # I

    .prologue
    .line 460
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Token;->clearNoTermBuffer()V

    .line 461
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/Token;->copyBuffer([CII)V

    .line 462
    iput p4, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 463
    iput p5, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 464
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 465
    return-object p0
.end method

.method public reinit([CIIIILjava/lang/String;)Lorg/apache/lucene/analysis/Token;
    .locals 1
    .param p1, "newTermBuffer"    # [C
    .param p2, "newTermOffset"    # I
    .param p3, "newTermLength"    # I
    .param p4, "newStartOffset"    # I
    .param p5, "newEndOffset"    # I
    .param p6, "newType"    # Ljava/lang/String;

    .prologue
    .line 443
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Token;->clearNoTermBuffer()V

    .line 444
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/Token;->copyBuffer([CII)V

    .line 445
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    .line 446
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 447
    iput p4, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 448
    iput p5, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 449
    iput-object p6, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 450
    return-object p0
.end method

.method public reinit(Lorg/apache/lucene/analysis/Token;)V
    .locals 3
    .param p1, "prototype"    # Lorg/apache/lucene/analysis/Token;

    .prologue
    .line 533
    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->buffer()[C

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->length()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/lucene/analysis/Token;->copyBuffer([CII)V

    .line 534
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 535
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->flags:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 536
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->startOffset:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 537
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->endOffset:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 538
    iget-object v0, p1, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 539
    iget-object v0, p1, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    .line 540
    return-void
.end method

.method public reinit(Lorg/apache/lucene/analysis/Token;Ljava/lang/String;)V
    .locals 1
    .param p1, "prototype"    # Lorg/apache/lucene/analysis/Token;
    .param p2, "newTerm"    # Ljava/lang/String;

    .prologue
    .line 548
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Token;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v0

    invoke-interface {v0, p2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 549
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 550
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->flags:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 551
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->startOffset:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 552
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->endOffset:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 553
    iget-object v0, p1, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 554
    iget-object v0, p1, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    .line 555
    return-void
.end method

.method public reinit(Lorg/apache/lucene/analysis/Token;[CII)V
    .locals 1
    .param p1, "prototype"    # Lorg/apache/lucene/analysis/Token;
    .param p2, "newTermBuffer"    # [C
    .param p3, "offset"    # I
    .param p4, "length"    # I

    .prologue
    .line 565
    invoke-virtual {p0, p2, p3, p4}, Lorg/apache/lucene/analysis/Token;->copyBuffer([CII)V

    .line 566
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 567
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->flags:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 568
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->startOffset:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 569
    iget v0, p1, Lorg/apache/lucene/analysis/Token;->endOffset:I

    iput v0, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 570
    iget-object v0, p1, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 571
    iget-object v0, p1, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    iput-object v0, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    .line 572
    return-void
.end method

.method public setEndOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 300
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 301
    return-void
.end method

.method public setFlags(I)V
    .locals 0
    .param p1, "flags"    # I

    .prologue
    .line 339
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->flags:I

    .line 340
    return-void
.end method

.method public setOffset(II)V
    .locals 0
    .param p1, "startOffset"    # I
    .param p2, "endOffset"    # I

    .prologue
    .line 306
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 307
    iput p2, p0, Lorg/apache/lucene/analysis/Token;->endOffset:I

    .line 308
    return-void
.end method

.method public setPayload(Lorg/apache/lucene/index/Payload;)V
    .locals 0
    .param p1, "payload"    # Lorg/apache/lucene/index/Payload;

    .prologue
    .line 353
    iput-object p1, p0, Lorg/apache/lucene/analysis/Token;->payload:Lorg/apache/lucene/index/Payload;

    .line 354
    return-void
.end method

.method public setPositionIncrement(I)V
    .locals 3
    .param p1, "positionIncrement"    # I

    .prologue
    .line 261
    if-gez p1, :cond_0

    .line 262
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Increment must be zero or greater: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 264
    :cond_0
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->positionIncrement:I

    .line 265
    return-void
.end method

.method public setStartOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 287
    iput p1, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    .line 288
    return-void
.end method

.method public final setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 318
    iput-object p1, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    .line 319
    return-void
.end method

.method public final startOffset()I
    .locals 1

    .prologue
    .line 281
    iget v0, p0, Lorg/apache/lucene/analysis/Token;->startOffset:I

    return v0
.end method

.method public final type()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lorg/apache/lucene/analysis/Token;->type:Ljava/lang/String;

    return-object v0
.end method
