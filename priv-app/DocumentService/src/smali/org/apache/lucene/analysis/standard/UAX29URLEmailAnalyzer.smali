.class public final Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;
.super Lorg/apache/lucene/analysis/StopwordAnalyzerBase;
.source "UAX29URLEmailAnalyzer.java"


# static fields
.field public static final DEFAULT_MAX_TOKEN_LENGTH:I = 0xff

.field public static final STOP_WORDS_SET:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private maxTokenLength:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lorg/apache/lucene/analysis/StopAnalyzer;->ENGLISH_STOP_WORDS_SET:Ljava/util/Set;

    sput-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;->STOP_WORDS_SET:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 65
    sget-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;->STOP_WORDS_SET:Ljava/util/Set;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    invoke-static {p2, p1}, Lorg/apache/lucene/analysis/WordlistLoader;->getWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/CharArraySet;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/Set",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p2, "stopWords":Ljava/util/Set;, "Ljava/util/Set<*>;"
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/StopwordAnalyzerBase;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V

    .line 45
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;->maxTokenLength:I

    .line 57
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;

    .prologue
    .line 40
    iget v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;->maxTokenLength:I

    return v0
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 96
    new-instance v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, p2}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 97
    .local v0, "src":Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;
    iget v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;->maxTokenLength:I

    invoke-virtual {v0, v3}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->setMaxTokenLength(I)V

    .line 98
    new-instance v1, Lorg/apache/lucene/analysis/standard/StandardFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v1, v3, v0}, Lorg/apache/lucene/analysis/standard/StandardFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 99
    .local v1, "tok":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v2, Lorg/apache/lucene/analysis/LowerCaseFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, v1}, Lorg/apache/lucene/analysis/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 100
    .end local v1    # "tok":Lorg/apache/lucene/analysis/TokenStream;
    .local v2, "tok":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v4, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;->stopwords:Lorg/apache/lucene/analysis/CharArraySet;

    invoke-direct {v1, v3, v2, v4}, Lorg/apache/lucene/analysis/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;)V

    .line 101
    .end local v2    # "tok":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "tok":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v3, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer$1;

    invoke-direct {v3, p0, v0, v1, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer$1;-><init>(Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;)V

    return-object v3
.end method

.method public getMaxTokenLength()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;->maxTokenLength:I

    return v0
.end method

.method public setMaxTokenLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 84
    iput p1, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailAnalyzer;->maxTokenLength:I

    .line 85
    return-void
.end method
