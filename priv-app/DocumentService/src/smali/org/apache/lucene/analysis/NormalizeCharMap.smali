.class public Lorg/apache/lucene/analysis/NormalizeCharMap;
.super Ljava/lang/Object;
.source "NormalizeCharMap.java"


# instance fields
.field diff:I

.field normStr:Ljava/lang/String;

.field submap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Character;",
            "Lorg/apache/lucene/analysis/NormalizeCharMap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "singleMatch"    # Ljava/lang/String;
    .param p2, "replacement"    # Ljava/lang/String;

    .prologue
    .line 42
    move-object v1, p0

    .line 43
    .local v1, "currMap":Lorg/apache/lucene/analysis/NormalizeCharMap;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 44
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 45
    .local v0, "c":C
    iget-object v4, v1, Lorg/apache/lucene/analysis/NormalizeCharMap;->submap:Ljava/util/Map;

    if-nez v4, :cond_0

    .line 46
    new-instance v4, Ljava/util/HashMap;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/util/HashMap;-><init>(I)V

    iput-object v4, v1, Lorg/apache/lucene/analysis/NormalizeCharMap;->submap:Ljava/util/Map;

    .line 48
    :cond_0
    iget-object v4, v1, Lorg/apache/lucene/analysis/NormalizeCharMap;->submap:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/analysis/NormalizeCharMap;

    .line 49
    .local v3, "map":Lorg/apache/lucene/analysis/NormalizeCharMap;
    if-nez v3, :cond_1

    .line 50
    new-instance v3, Lorg/apache/lucene/analysis/NormalizeCharMap;

    .end local v3    # "map":Lorg/apache/lucene/analysis/NormalizeCharMap;
    invoke-direct {v3}, Lorg/apache/lucene/analysis/NormalizeCharMap;-><init>()V

    .line 51
    .restart local v3    # "map":Lorg/apache/lucene/analysis/NormalizeCharMap;
    iget-object v4, v1, Lorg/apache/lucene/analysis/NormalizeCharMap;->submap:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    :cond_1
    move-object v1, v3

    .line 43
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 55
    .end local v0    # "c":C
    .end local v3    # "map":Lorg/apache/lucene/analysis/NormalizeCharMap;
    :cond_2
    iget-object v4, v1, Lorg/apache/lucene/analysis/NormalizeCharMap;->normStr:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 56
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "MappingCharFilter: there is already a mapping for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 58
    :cond_3
    iput-object p2, v1, Lorg/apache/lucene/analysis/NormalizeCharMap;->normStr:Ljava/lang/String;

    .line 59
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, v1, Lorg/apache/lucene/analysis/NormalizeCharMap;->diff:I

    .line 60
    return-void
.end method
