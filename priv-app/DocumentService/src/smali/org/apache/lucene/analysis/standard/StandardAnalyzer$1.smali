.class Lorg/apache/lucene/analysis/standard/StandardAnalyzer$1;
.super Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;
.source "StandardAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/analysis/standard/StandardAnalyzer;

.field final synthetic val$src:Lorg/apache/lucene/analysis/standard/StandardTokenizer;


# direct methods
.method constructor <init>(Lorg/apache/lucene/analysis/standard/StandardAnalyzer;Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/standard/StandardTokenizer;)V
    .locals 0
    .param p2, "x0"    # Lorg/apache/lucene/analysis/Tokenizer;
    .param p3, "x1"    # Lorg/apache/lucene/analysis/TokenStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    iput-object p1, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer$1;->this$0:Lorg/apache/lucene/analysis/standard/StandardAnalyzer;

    iput-object p4, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer$1;->val$src:Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-void
.end method


# virtual methods
.method protected reset(Ljava/io/Reader;)Z
    .locals 2
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer$1;->val$src:Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer$1;->this$0:Lorg/apache/lucene/analysis/standard/StandardAnalyzer;

    # getter for: Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->maxTokenLength:I
    invoke-static {v1}, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->access$000(Lorg/apache/lucene/analysis/standard/StandardAnalyzer;)I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->setMaxTokenLength(I)V

    .line 137
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;->reset(Ljava/io/Reader;)Z

    move-result v0

    return v0
.end method
