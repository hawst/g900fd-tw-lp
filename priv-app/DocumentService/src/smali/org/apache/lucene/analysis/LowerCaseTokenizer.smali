.class public final Lorg/apache/lucene/analysis/LowerCaseTokenizer;
.super Lorg/apache/lucene/analysis/LetterTokenizer;
.source "LowerCaseTokenizer.java"


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1, "in"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 98
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/analysis/LetterTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 99
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 1
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "in"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 121
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/lucene/analysis/LetterTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 122
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V
    .locals 1
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p2, "in"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 109
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/lucene/analysis/LetterTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V

    .line 110
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Ljava/io/Reader;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/LetterTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p3, "in"    # Ljava/io/Reader;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/LetterTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 88
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p3, "in"    # Ljava/io/Reader;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/LetterTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V

    .line 73
    return-void
.end method


# virtual methods
.method protected normalize(I)I
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 128
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v0

    return v0
.end method
