.class public final Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;
.super Lorg/apache/lucene/analysis/TokenStream;
.source "TeeSinkTokenFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/TeeSinkTokenFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SinkTokenStream"
.end annotation


# instance fields
.field private final cachedStates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/util/AttributeSource$State;",
            ">;"
        }
    .end annotation
.end field

.field private filter:Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;

.field private finalState:Lorg/apache/lucene/util/AttributeSource$State;

.field private it:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/util/AttributeSource$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;)V
    .locals 1
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p2, "filter"    # Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;

    .prologue
    .line 193
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource;)V

    .line 187
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->cachedStates:Ljava/util/List;

    .line 189
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->it:Ljava/util/Iterator;

    .line 194
    iput-object p2, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->filter:Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;

    .line 195
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;Lorg/apache/lucene/analysis/TeeSinkTokenFilter$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/util/AttributeSource;
    .param p2, "x1"    # Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;
    .param p3, "x2"    # Lorg/apache/lucene/analysis/TeeSinkTokenFilter$1;

    .prologue
    .line 186
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;)V

    return-void
.end method

.method private accept(Lorg/apache/lucene/util/AttributeSource;)Z
    .locals 1
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;

    .prologue
    .line 198
    iget-object v0, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->filter:Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;->accept(Lorg/apache/lucene/util/AttributeSource;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;Lorg/apache/lucene/util/AttributeSource;)Z
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;
    .param p1, "x1"    # Lorg/apache/lucene/util/AttributeSource;

    .prologue
    .line 186
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->accept(Lorg/apache/lucene/util/AttributeSource;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;Lorg/apache/lucene/util/AttributeSource$State;)V
    .locals 0
    .param p0, "x0"    # Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;
    .param p1, "x1"    # Lorg/apache/lucene/util/AttributeSource$State;

    .prologue
    .line 186
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->addState(Lorg/apache/lucene/util/AttributeSource$State;)V

    return-void
.end method

.method static synthetic access$300(Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;Lorg/apache/lucene/util/AttributeSource$State;)V
    .locals 0
    .param p0, "x0"    # Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;
    .param p1, "x1"    # Lorg/apache/lucene/util/AttributeSource$State;

    .prologue
    .line 186
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->setFinalState(Lorg/apache/lucene/util/AttributeSource$State;)V

    return-void
.end method

.method private addState(Lorg/apache/lucene/util/AttributeSource$State;)V
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/util/AttributeSource$State;

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->it:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    .line 203
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The tee must be consumed before sinks are consumed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->cachedStates:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    return-void
.end method

.method private setFinalState(Lorg/apache/lucene/util/AttributeSource$State;)V
    .locals 0
    .param p1, "finalState"    # Lorg/apache/lucene/util/AttributeSource$State;

    .prologue
    .line 209
    iput-object p1, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->finalState:Lorg/apache/lucene/util/AttributeSource$State;

    .line 210
    return-void
.end method


# virtual methods
.method public final end()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 230
    iget-object v0, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->finalState:Lorg/apache/lucene/util/AttributeSource$State;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->finalState:Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 233
    :cond_0
    return-void
.end method

.method public final incrementToken()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 215
    iget-object v1, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->it:Ljava/util/Iterator;

    if-nez v1, :cond_0

    .line 216
    iget-object v1, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->cachedStates:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->it:Ljava/util/Iterator;

    .line 219
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->it:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 220
    const/4 v1, 0x0

    .line 225
    :goto_0
    return v1

    .line 223
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->it:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/AttributeSource$State;

    .line 224
    .local v0, "state":Lorg/apache/lucene/util/AttributeSource$State;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 225
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final reset()V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->cachedStates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->it:Ljava/util/Iterator;

    .line 238
    return-void
.end method
