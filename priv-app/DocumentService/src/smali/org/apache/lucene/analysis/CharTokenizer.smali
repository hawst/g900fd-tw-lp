.class public abstract Lorg/apache/lucene/analysis/CharTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "CharTokenizer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final IO_BUFFER_SIZE:I = 0x1000

.field private static final MAX_WORD_LEN:I = 0xff

.field private static final isTokenCharMethod:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/analysis/CharTokenizer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final normalizeMethod:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/analysis/CharTokenizer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private bufferIndex:I

.field private final charUtils:Lorg/apache/lucene/util/CharacterUtils;

.field private dataLen:I

.field private finalOffset:I

.field private final ioBuffer:Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

.field private offset:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final useOldAPI:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 68
    const-class v0, Lorg/apache/lucene/analysis/CharTokenizer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/CharTokenizer;->$assertionsDisabled:Z

    .line 174
    new-instance v0, Lorg/apache/lucene/util/VirtualMethod;

    const-class v3, Lorg/apache/lucene/analysis/CharTokenizer;

    const-string/jumbo v4, "isTokenChar"

    new-array v5, v1, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    aput-object v6, v5, v2

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lorg/apache/lucene/analysis/CharTokenizer;->isTokenCharMethod:Lorg/apache/lucene/util/VirtualMethod;

    .line 181
    new-instance v0, Lorg/apache/lucene/util/VirtualMethod;

    const-class v3, Lorg/apache/lucene/analysis/CharTokenizer;

    const-string/jumbo v4, "normalize"

    new-array v1, v1, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    aput-object v5, v1, v2

    invoke-direct {v0, v3, v4, v1}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lorg/apache/lucene/analysis/CharTokenizer;->normalizeMethod:Lorg/apache/lucene/util/VirtualMethod;

    return-void

    :cond_0
    move v0, v2

    .line 68
    goto :goto_0
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1, "input"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 127
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 128
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 1
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 151
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 152
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V
    .locals 1
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p2, "input"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 139
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V

    .line 140
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Ljava/io/Reader;

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 154
    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offset:I

    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->dataLen:I

    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->finalOffset:I

    .line 158
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/CharTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 159
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/CharTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 162
    const/16 v0, 0x1000

    invoke-static {v0}, Lorg/apache/lucene/util/CharacterUtils;->newCharacterBuffer(I)Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->ioBuffer:Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    .line 80
    invoke-static {p1}, Lorg/apache/lucene/util/CharacterUtils;->getInstance(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/util/CharacterUtils;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    .line 81
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/CharTokenizer;->useOldAPI(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->useOldAPI:Z

    .line 83
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p3, "input"    # Ljava/io/Reader;

    .prologue
    const/4 v0, 0x0

    .line 114
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 154
    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offset:I

    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->dataLen:I

    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->finalOffset:I

    .line 158
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/CharTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 159
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/CharTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 162
    const/16 v0, 0x1000

    invoke-static {v0}, Lorg/apache/lucene/util/CharacterUtils;->newCharacterBuffer(I)Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->ioBuffer:Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    .line 115
    invoke-static {p1}, Lorg/apache/lucene/util/CharacterUtils;->getInstance(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/util/CharacterUtils;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    .line 116
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/CharTokenizer;->useOldAPI(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->useOldAPI:Z

    .line 117
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p3, "input"    # Ljava/io/Reader;

    .prologue
    const/4 v0, 0x0

    .line 97
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V

    .line 154
    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offset:I

    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->dataLen:I

    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->finalOffset:I

    .line 158
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/CharTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 159
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/CharTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 162
    const/16 v0, 0x1000

    invoke-static {v0}, Lorg/apache/lucene/util/CharacterUtils;->newCharacterBuffer(I)Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->ioBuffer:Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    .line 98
    invoke-static {p1}, Lorg/apache/lucene/util/CharacterUtils;->getInstance(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/util/CharacterUtils;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    .line 99
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/CharTokenizer;->useOldAPI(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->useOldAPI:Z

    .line 100
    return-void
.end method

.method private incrementTokenOld()Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v9, -0x1

    .line 321
    const/4 v2, 0x0

    .line 322
    .local v2, "length":I
    const/4 v5, -0x1

    .line 323
    .local v5, "start":I
    iget-object v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v7}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 324
    .local v0, "buffer":[C
    iget-object v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->ioBuffer:Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    invoke-virtual {v7}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->getBuffer()[C

    move-result-object v4

    .line 327
    .local v4, "oldIoBuffer":[C
    :cond_0
    :goto_0
    iget v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    iget v8, p0, Lorg/apache/lucene/analysis/CharTokenizer;->dataLen:I

    if-lt v7, v8, :cond_3

    .line 328
    iget v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offset:I

    iget v8, p0, Lorg/apache/lucene/analysis/CharTokenizer;->dataLen:I

    add-int/2addr v7, v8

    iput v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offset:I

    .line 329
    iget-object v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->input:Ljava/io/Reader;

    invoke-virtual {v7, v4}, Ljava/io/Reader;->read([C)I

    move-result v7

    iput v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->dataLen:I

    .line 330
    iget v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->dataLen:I

    if-ne v7, v9, :cond_2

    .line 331
    iput v6, p0, Lorg/apache/lucene/analysis/CharTokenizer;->dataLen:I

    .line 332
    if-lez v2, :cond_1

    .line 362
    :goto_1
    iget-object v6, p0, Lorg/apache/lucene/analysis/CharTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 363
    sget-boolean v6, Lorg/apache/lucene/analysis/CharTokenizer;->$assertionsDisabled:Z

    if-nez v6, :cond_8

    if-ne v5, v9, :cond_8

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 335
    :cond_1
    iget v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offset:I

    invoke-virtual {p0, v7}, Lorg/apache/lucene/analysis/CharTokenizer;->correctOffset(I)I

    move-result v7

    iput v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->finalOffset:I

    .line 365
    :goto_2
    return v6

    .line 339
    :cond_2
    iput v6, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    .line 342
    :cond_3
    iget v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    aget-char v1, v4, v7

    .line 344
    .local v1, "c":C
    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/CharTokenizer;->isTokenChar(C)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 346
    if-nez v2, :cond_6

    .line 347
    sget-boolean v7, Lorg/apache/lucene/analysis/CharTokenizer;->$assertionsDisabled:Z

    if-nez v7, :cond_4

    if-eq v5, v9, :cond_4

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 348
    :cond_4
    iget v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offset:I

    iget v8, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    add-int/2addr v7, v8

    add-int/lit8 v5, v7, -0x1

    .line 353
    :cond_5
    :goto_3
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "length":I
    .local v3, "length":I
    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/CharTokenizer;->normalize(C)C

    move-result v7

    aput-char v7, v0, v2

    .line 355
    const/16 v7, 0xff

    if-ne v3, v7, :cond_9

    move v2, v3

    .line 356
    .end local v3    # "length":I
    .restart local v2    # "length":I
    goto :goto_1

    .line 349
    :cond_6
    array-length v7, v0

    if-ne v2, v7, :cond_5

    .line 350
    iget-object v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    add-int/lit8 v8, v2, 0x1

    invoke-interface {v7, v8}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    move-result-object v0

    goto :goto_3

    .line 358
    :cond_7
    if-lez v2, :cond_0

    goto :goto_1

    .line 364
    .end local v1    # "c":C
    :cond_8
    iget-object v6, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v5}, Lorg/apache/lucene/analysis/CharTokenizer;->correctOffset(I)I

    move-result v7

    add-int v8, v5, v2

    invoke-virtual {p0, v8}, Lorg/apache/lucene/analysis/CharTokenizer;->correctOffset(I)I

    move-result v8

    invoke-interface {v6, v7, v8}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 365
    const/4 v6, 0x1

    goto :goto_2

    .end local v2    # "length":I
    .restart local v1    # "c":C
    .restart local v3    # "length":I
    :cond_9
    move v2, v3

    .end local v3    # "length":I
    .restart local v2    # "length":I
    goto/16 :goto_0
.end method

.method private useOldAPI(Lorg/apache/lucene/util/Version;)Z
    .locals 3
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 391
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 392
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/analysis/CharTokenizer;>;"
    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lorg/apache/lucene/analysis/CharTokenizer;->isTokenCharMethod:Lorg/apache/lucene/util/VirtualMethod;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/util/VirtualMethod;->isOverriddenAsOf(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lorg/apache/lucene/analysis/CharTokenizer;->normalizeMethod:Lorg/apache/lucene/util/VirtualMethod;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/util/VirtualMethod;->isOverriddenAsOf(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 394
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "For matchVersion >= LUCENE_31, CharTokenizer subclasses must not override isTokenChar(char) or normalize(char)."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 396
    :cond_1
    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final end()V
    .locals 3

    .prologue
    .line 373
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v1, p0, Lorg/apache/lucene/analysis/CharTokenizer;->finalOffset:I

    iget v2, p0, Lorg/apache/lucene/analysis/CharTokenizer;->finalOffset:I

    invoke-interface {v0, v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 374
    return-void
.end method

.method public final incrementToken()Z
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, -0x1

    const/4 v6, 0x0

    .line 264
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CharTokenizer;->clearAttributes()V

    .line 265
    iget-boolean v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->useOldAPI:Z

    if-eqz v7, :cond_0

    .line 266
    invoke-direct {p0}, Lorg/apache/lucene/analysis/CharTokenizer;->incrementTokenOld()Z

    move-result v6

    .line 310
    :goto_0
    return v6

    .line 267
    :cond_0
    const/4 v4, 0x0

    .line 268
    .local v4, "length":I
    const/4 v5, -0x1

    .line 269
    .local v5, "start":I
    const/4 v3, -0x1

    .line 270
    .local v3, "end":I
    iget-object v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v7}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 272
    .local v0, "buffer":[C
    :cond_1
    iget v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    iget v8, p0, Lorg/apache/lucene/analysis/CharTokenizer;->dataLen:I

    if-lt v7, v8, :cond_4

    .line 273
    iget v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offset:I

    iget v8, p0, Lorg/apache/lucene/analysis/CharTokenizer;->dataLen:I

    add-int/2addr v7, v8

    iput v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offset:I

    .line 274
    iget-object v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    iget-object v8, p0, Lorg/apache/lucene/analysis/CharTokenizer;->ioBuffer:Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    iget-object v9, p0, Lorg/apache/lucene/analysis/CharTokenizer;->input:Ljava/io/Reader;

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/util/CharacterUtils;->fill(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;Ljava/io/Reader;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 275
    iput v6, p0, Lorg/apache/lucene/analysis/CharTokenizer;->dataLen:I

    .line 276
    if-lez v4, :cond_2

    .line 307
    :goto_1
    iget-object v6, p0, Lorg/apache/lucene/analysis/CharTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 308
    sget-boolean v6, Lorg/apache/lucene/analysis/CharTokenizer;->$assertionsDisabled:Z

    if-nez v6, :cond_9

    if-ne v5, v10, :cond_9

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 279
    :cond_2
    iget v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offset:I

    invoke-virtual {p0, v7}, Lorg/apache/lucene/analysis/CharTokenizer;->correctOffset(I)I

    move-result v7

    iput v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->finalOffset:I

    goto :goto_0

    .line 283
    :cond_3
    iget-object v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->ioBuffer:Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    invoke-virtual {v7}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->getLength()I

    move-result v7

    iput v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->dataLen:I

    .line 284
    iput v6, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    .line 287
    :cond_4
    iget-object v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    iget-object v8, p0, Lorg/apache/lucene/analysis/CharTokenizer;->ioBuffer:Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    invoke-virtual {v8}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->getBuffer()[C

    move-result-object v8

    iget v9, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/util/CharacterUtils;->codePointAt([CI)I

    move-result v1

    .line 288
    .local v1, "c":I
    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    .line 289
    .local v2, "charCount":I
    iget v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    add-int/2addr v7, v2

    iput v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    .line 291
    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/CharTokenizer;->isTokenChar(I)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 292
    if-nez v4, :cond_7

    .line 293
    sget-boolean v7, Lorg/apache/lucene/analysis/CharTokenizer;->$assertionsDisabled:Z

    if-nez v7, :cond_5

    if-eq v5, v10, :cond_5

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 294
    :cond_5
    iget v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offset:I

    iget v8, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    add-int/2addr v7, v8

    sub-int v5, v7, v2

    .line 295
    move v3, v5

    .line 299
    :cond_6
    :goto_2
    add-int/2addr v3, v2

    .line 300
    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/CharTokenizer;->normalize(I)I

    move-result v7

    invoke-static {v7, v0, v4}, Ljava/lang/Character;->toChars(I[CI)I

    move-result v7

    add-int/2addr v4, v7

    .line 301
    const/16 v7, 0xff

    if-lt v4, v7, :cond_1

    goto :goto_1

    .line 296
    :cond_7
    array-length v7, v0

    add-int/lit8 v7, v7, -0x1

    if-lt v4, v7, :cond_6

    .line 297
    iget-object v7, p0, Lorg/apache/lucene/analysis/CharTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    add-int/lit8 v8, v4, 0x2

    invoke-interface {v7, v8}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    move-result-object v0

    goto :goto_2

    .line 303
    :cond_8
    if-lez v4, :cond_1

    goto :goto_1

    .line 309
    .end local v1    # "c":I
    .end local v2    # "charCount":I
    :cond_9
    iget-object v6, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v5}, Lorg/apache/lucene/analysis/CharTokenizer;->correctOffset(I)I

    move-result v7

    invoke-virtual {p0, v3}, Lorg/apache/lucene/analysis/CharTokenizer;->correctOffset(I)I

    move-result v8

    iput v8, p0, Lorg/apache/lucene/analysis/CharTokenizer;->finalOffset:I

    invoke-interface {v6, v7, v8}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 310
    const/4 v6, 0x1

    goto/16 :goto_0
.end method

.method protected isTokenChar(C)Z
    .locals 1
    .param p1, "c"    # C
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 201
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/CharTokenizer;->isTokenChar(I)Z

    move-result v0

    return v0
.end method

.method protected isTokenChar(I)Z
    .locals 2
    .param p1, "c"    # I

    .prologue
    .line 240
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "since LUCENE_31 subclasses of CharTokenizer must implement isTokenChar(int)"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected normalize(C)C
    .locals 1
    .param p1, "c"    # C
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 220
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/CharTokenizer;->normalize(I)I

    move-result v0

    int-to-char v0, v0

    return v0
.end method

.method protected normalize(I)I
    .locals 0
    .param p1, "c"    # I

    .prologue
    .line 259
    return p1
.end method

.method public reset(Ljava/io/Reader;)V
    .locals 1
    .param p1, "input"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 378
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/Tokenizer;->reset(Ljava/io/Reader;)V

    .line 379
    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->bufferIndex:I

    .line 380
    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->offset:I

    .line 381
    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->dataLen:I

    .line 382
    iput v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->finalOffset:I

    .line 383
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharTokenizer;->ioBuffer:Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->reset()V

    .line 384
    return-void
.end method
