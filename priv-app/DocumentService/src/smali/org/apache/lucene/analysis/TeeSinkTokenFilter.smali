.class public final Lorg/apache/lucene/analysis/TeeSinkTokenFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "TeeSinkTokenFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;,
        Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;
    }
.end annotation


# static fields
.field private static final ACCEPT_ALL_FILTER:Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;


# instance fields
.field private final sinks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 241
    new-instance v0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$1;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->ACCEPT_ALL_FILTER:Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 76
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->sinks:Ljava/util/List;

    .line 83
    return-void
.end method


# virtual methods
.method public addSinkTokenStream(Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;)V
    .locals 3
    .param p1, "sink"    # Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;

    .prologue
    .line 110
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->getAttributeFactory()Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->getAttributeFactory()Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "The supplied sink is not compatible to this tee"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 114
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->cloneAttributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/util/AttributeSource;->getAttributeImplsIterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/util/AttributeImpl;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 115
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->addAttributeImpl(Lorg/apache/lucene/util/AttributeImpl;)V

    goto :goto_0

    .line 117
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->sinks:Ljava/util/List;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    return-void
.end method

.method public consumeAllTokens()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->incrementToken()Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    return-void
.end method

.method public final end()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 154
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->end()V

    .line 155
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    .line 156
    .local v0, "finalState":Lorg/apache/lucene/util/AttributeSource$State;
    iget-object v4, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->sinks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 157
    .local v2, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;

    .line 158
    .local v3, "sink":Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;
    if-eqz v3, :cond_0

    .line 159
    # invokes: Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->setFinalState(Lorg/apache/lucene/util/AttributeSource$State;)V
    invoke-static {v3, v0}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->access$300(Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;Lorg/apache/lucene/util/AttributeSource$State;)V

    goto :goto_0

    .line 162
    .end local v2    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;>;"
    .end local v3    # "sink":Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;
    :cond_1
    return-void
.end method

.method public incrementToken()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    iget-object v4, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v4}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 134
    const/4 v3, 0x0

    .line 135
    .local v3, "state":Lorg/apache/lucene/util/AttributeSource$State;
    iget-object v4, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->sinks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 136
    .local v1, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;

    .line 137
    .local v2, "sink":Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;
    if-eqz v2, :cond_0

    .line 138
    # invokes: Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->accept(Lorg/apache/lucene/util/AttributeSource;)Z
    invoke-static {v2, p0}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->access$100(Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;Lorg/apache/lucene/util/AttributeSource;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 139
    if-nez v3, :cond_1

    .line 140
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v3

    .line 142
    :cond_1
    # invokes: Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->addState(Lorg/apache/lucene/util/AttributeSource$State;)V
    invoke-static {v2, v3}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;->access$200(Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;Lorg/apache/lucene/util/AttributeSource$State;)V

    goto :goto_0

    .line 146
    .end local v1    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;>;"
    .end local v2    # "sink":Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;
    :cond_2
    const/4 v4, 0x1

    .line 149
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v3    # "state":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_1
    return v4

    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public newSinkTokenStream()Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->ACCEPT_ALL_FILTER:Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->newSinkTokenStream(Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;)Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;

    move-result-object v0

    return-object v0
.end method

.method public newSinkTokenStream(Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;)Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;
    .locals 3
    .param p1, "filter"    # Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;

    .prologue
    .line 98
    new-instance v0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->cloneAttributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkFilter;Lorg/apache/lucene/analysis/TeeSinkTokenFilter$1;)V

    .line 99
    .local v0, "sink":Lorg/apache/lucene/analysis/TeeSinkTokenFilter$SinkTokenStream;
    iget-object v1, p0, Lorg/apache/lucene/analysis/TeeSinkTokenFilter;->sinks:Ljava/util/List;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    return-object v0
.end method
