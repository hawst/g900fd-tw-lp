.class public final Lorg/apache/lucene/analysis/LengthFilter;
.super Lorg/apache/lucene/analysis/FilteringTokenFilter;
.source "LengthFilter.java"


# instance fields
.field private final max:I

.field private final min:I

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;II)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "min"    # I
    .param p3, "max"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Lorg/apache/lucene/analysis/LengthFilter;-><init>(ZLorg/apache/lucene/analysis/TokenStream;II)V

    .line 55
    return-void
.end method

.method public constructor <init>(ZLorg/apache/lucene/analysis/TokenStream;II)V
    .locals 1
    .param p1, "enablePositionIncrements"    # Z
    .param p2, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "min"    # I
    .param p4, "max"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/FilteringTokenFilter;-><init>(ZLorg/apache/lucene/analysis/TokenStream;)V

    .line 35
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/LengthFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/LengthFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 43
    iput p3, p0, Lorg/apache/lucene/analysis/LengthFilter;->min:I

    .line 44
    iput p4, p0, Lorg/apache/lucene/analysis/LengthFilter;->max:I

    .line 45
    return-void
.end method


# virtual methods
.method public accept()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v1, p0, Lorg/apache/lucene/analysis/LengthFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v0

    .line 60
    .local v0, "len":I
    iget v1, p0, Lorg/apache/lucene/analysis/LengthFilter;->min:I

    if-lt v0, v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/analysis/LengthFilter;->max:I

    if-gt v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
