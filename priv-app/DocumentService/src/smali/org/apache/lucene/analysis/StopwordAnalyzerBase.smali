.class public abstract Lorg/apache/lucene/analysis/StopwordAnalyzerBase;
.super Lorg/apache/lucene/analysis/ReusableAnalyzerBase;
.source "StopwordAnalyzerBase.java"


# instance fields
.field protected final matchVersion:Lorg/apache/lucene/util/Version;

.field protected final stopwords:Lorg/apache/lucene/analysis/CharArraySet;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "version"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/StopwordAnalyzerBase;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V

    .line 78
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/util/Version;Ljava/util/Set;)V
    .locals 1
    .param p1, "version"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/Set",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p2, "stopwords":Ljava/util/Set;, "Ljava/util/Set<*>;"
    invoke-direct {p0}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase;-><init>()V

    .line 64
    iput-object p1, p0, Lorg/apache/lucene/analysis/StopwordAnalyzerBase;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 66
    if-nez p2, :cond_0

    sget-object v0, Lorg/apache/lucene/analysis/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/CharArraySet;

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/analysis/StopwordAnalyzerBase;->stopwords:Lorg/apache/lucene/analysis/CharArraySet;

    .line 68
    return-void

    .line 66
    :cond_0
    invoke-static {p1, p2}, Lorg/apache/lucene/analysis/CharArraySet;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/CharArraySet;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/analysis/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/CharArraySet;)Lorg/apache/lucene/analysis/CharArraySet;

    move-result-object v0

    goto :goto_0
.end method

.method protected static loadStopwordSet(Ljava/io/File;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/CharArraySet;
    .locals 4
    .param p0, "stopwords"    # Ljava/io/File;
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 126
    const/4 v0, 0x0

    .line 128
    .local v0, "reader":Ljava/io/Reader;
    :try_start_0
    sget-object v1, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    invoke-static {p0, v1}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/io/File;Ljava/nio/charset/Charset;)Ljava/io/Reader;

    move-result-object v0

    .line 129
    invoke-static {v0, p1}, Lorg/apache/lucene/analysis/WordlistLoader;->getWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/CharArraySet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 131
    new-array v2, v2, [Ljava/io/Closeable;

    aput-object v0, v2, v3

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    return-object v1

    :catchall_0
    move-exception v1

    new-array v2, v2, [Ljava/io/Closeable;

    aput-object v0, v2, v3

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    throw v1
.end method

.method protected static loadStopwordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/CharArraySet;
    .locals 3
    .param p0, "stopwords"    # Ljava/io/Reader;
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 151
    :try_start_0
    invoke-static {p0, p1}, Lorg/apache/lucene/analysis/WordlistLoader;->getWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/CharArraySet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 153
    new-array v1, v1, [Ljava/io/Closeable;

    aput-object p0, v1, v2

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v1, [Ljava/io/Closeable;

    aput-object p0, v1, v2

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    throw v0
.end method

.method protected static loadStopwordSet(ZLjava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/analysis/CharArraySet;
    .locals 6
    .param p0, "ignoreCase"    # Z
    .param p2, "resource"    # Ljava/lang/String;
    .param p3, "comment"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/analysis/ReusableAnalyzerBase;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lorg/apache/lucene/analysis/CharArraySet;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "aClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/analysis/ReusableAnalyzerBase;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 101
    const/4 v0, 0x0

    .line 103
    .local v0, "reader":Ljava/io/Reader;
    :try_start_0
    invoke-virtual {p1, p2}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v1, v2}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/io/InputStream;Ljava/nio/charset/Charset;)Ljava/io/Reader;

    move-result-object v0

    .line 104
    new-instance v1, Lorg/apache/lucene/analysis/CharArraySet;

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/16 v3, 0x10

    invoke-direct {v1, v2, v3, p0}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    invoke-static {v0, p3, v1}, Lorg/apache/lucene/analysis/WordlistLoader;->getWordSet(Ljava/io/Reader;Ljava/lang/String;Lorg/apache/lucene/analysis/CharArraySet;)Lorg/apache/lucene/analysis/CharArraySet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 106
    new-array v2, v5, [Ljava/io/Closeable;

    aput-object v0, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    return-object v1

    :catchall_0
    move-exception v1

    new-array v2, v5, [Ljava/io/Closeable;

    aput-object v0, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    throw v1
.end method


# virtual methods
.method public getStopwordSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/analysis/StopwordAnalyzerBase;->stopwords:Lorg/apache/lucene/analysis/CharArraySet;

    return-object v0
.end method
