.class public Lorg/apache/lucene/analysis/CharArraySet;
.super Ljava/util/AbstractSet;
.source "CharArraySet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/CharArraySet$1;,
        Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final EMPTY_SET:Lorg/apache/lucene/analysis/CharArraySet;

.field private static final PLACEHOLDER:Ljava/lang/Object;


# instance fields
.field private final map:Lorg/apache/lucene/analysis/CharArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/analysis/CharArrayMap",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Lorg/apache/lucene/analysis/CharArraySet;

    invoke-static {}, Lorg/apache/lucene/analysis/CharArrayMap;->emptyMap()Lorg/apache/lucene/analysis/CharArrayMap;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;)V

    sput-object v0, Lorg/apache/lucene/analysis/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/CharArraySet;

    .line 58
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/apache/lucene/analysis/CharArraySet;->PLACEHOLDER:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 1
    .param p1, "startSize"    # I
    .param p2, "ignoreCase"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 107
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    .line 108
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;Z)V
    .locals 2
    .param p2, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;Z)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 122
    .local p1, "c":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {p0, v0, v1, p2}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    .line 123
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/CharArraySet;->addAll(Ljava/util/Collection;)Z

    .line 124
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/analysis/CharArrayMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/analysis/CharArrayMap",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, "map":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<Ljava/lang/Object;>;"
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 128
    iput-object p1, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    .line 129
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;IZ)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "startSize"    # I
    .param p3, "ignoreCase"    # Z

    .prologue
    .line 75
    new-instance v0, Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-direct {v0, p1, p2, p3}, Lorg/apache/lucene/analysis/CharArrayMap;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p3, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/Collection",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p2, "c":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {p0, p1, v0, p3}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    .line 92
    invoke-virtual {p0, p2}, Lorg/apache/lucene/analysis/CharArraySet;->addAll(Ljava/util/Collection;)Z

    .line 93
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/analysis/CharArraySet;)Lorg/apache/lucene/analysis/CharArrayMap;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/analysis/CharArraySet;

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    return-object v0
.end method

.method public static copy(Ljava/util/Set;)Lorg/apache/lucene/analysis/CharArraySet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<*>;)",
            "Lorg/apache/lucene/analysis/CharArraySet;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 214
    .local p0, "set":Ljava/util/Set;, "Ljava/util/Set<*>;"
    sget-object v0, Lorg/apache/lucene/analysis/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/CharArraySet;

    if-ne p0, v0, :cond_0

    .line 215
    sget-object v0, Lorg/apache/lucene/analysis/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/CharArraySet;

    .line 216
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-static {v0, p0}, Lorg/apache/lucene/analysis/CharArraySet;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/CharArraySet;

    move-result-object v0

    goto :goto_0
.end method

.method public static copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/CharArraySet;
    .locals 4
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/Set",
            "<*>;)",
            "Lorg/apache/lucene/analysis/CharArraySet;"
        }
    .end annotation

    .prologue
    .line 241
    .local p1, "set":Ljava/util/Set;, "Ljava/util/Set<*>;"
    sget-object v1, Lorg/apache/lucene/analysis/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/CharArraySet;

    if-ne p1, v1, :cond_0

    .line 242
    sget-object v1, Lorg/apache/lucene/analysis/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/CharArraySet;

    .line 247
    :goto_0
    return-object v1

    .line 243
    :cond_0
    instance-of v1, p1, Lorg/apache/lucene/analysis/CharArraySet;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 244
    check-cast v0, Lorg/apache/lucene/analysis/CharArraySet;

    .line 245
    .local v0, "source":Lorg/apache/lucene/analysis/CharArraySet;
    new-instance v1, Lorg/apache/lucene/analysis/CharArraySet;

    iget-object v2, v0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v2, v2, Lorg/apache/lucene/analysis/CharArrayMap;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v3, v0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-static {v2, v3}, Lorg/apache/lucene/analysis/CharArrayMap;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Map;)Lorg/apache/lucene/analysis/CharArrayMap;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;)V

    goto :goto_0

    .line 247
    .end local v0    # "source":Lorg/apache/lucene/analysis/CharArraySet;
    :cond_1
    new-instance v1, Lorg/apache/lucene/analysis/CharArraySet;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    goto :goto_0
.end method

.method public static unmodifiableSet(Lorg/apache/lucene/analysis/CharArraySet;)Lorg/apache/lucene/analysis/CharArraySet;
    .locals 2
    .param p0, "set"    # Lorg/apache/lucene/analysis/CharArraySet;

    .prologue
    .line 192
    if-nez p0, :cond_0

    .line 193
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Given set is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_0
    sget-object v0, Lorg/apache/lucene/analysis/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/CharArraySet;

    if-ne p0, v0, :cond_2

    .line 195
    sget-object p0, Lorg/apache/lucene/analysis/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/CharArraySet;

    .line 198
    .end local p0    # "set":Lorg/apache/lucene/analysis/CharArraySet;
    :cond_1
    :goto_0
    return-object p0

    .line 196
    .restart local p0    # "set":Lorg/apache/lucene/analysis/CharArraySet;
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    instance-of v0, v0, Lorg/apache/lucene/analysis/CharArrayMap$UnmodifiableCharArrayMap;

    if-nez v0, :cond_1

    .line 198
    new-instance v0, Lorg/apache/lucene/analysis/CharArraySet;

    iget-object v1, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-static {v1}, Lorg/apache/lucene/analysis/CharArrayMap;->unmodifiableMap(Lorg/apache/lucene/analysis/CharArrayMap;)Lorg/apache/lucene/analysis/CharArrayMap;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public add(Ljava/lang/CharSequence;)Z
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 160
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    sget-object v1, Lorg/apache/lucene/analysis/CharArraySet;->PLACEHOLDER:Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/analysis/CharArrayMap;->put(Ljava/lang/CharSequence;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public add(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 155
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    sget-object v1, Lorg/apache/lucene/analysis/CharArraySet;->PLACEHOLDER:Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/analysis/CharArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public add(Ljava/lang/String;)Z
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 165
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    sget-object v1, Lorg/apache/lucene/analysis/CharArraySet;->PLACEHOLDER:Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/analysis/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public add([C)Z
    .locals 2
    .param p1, "text"    # [C

    .prologue
    .line 173
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    sget-object v1, Lorg/apache/lucene/analysis/CharArraySet;->PLACEHOLDER:Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/analysis/CharArrayMap;->put([CLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/CharArrayMap;->clear()V

    .line 135
    return-void
.end method

.method public contains(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/CharArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public contains([CII)Z
    .locals 1
    .param p1, "text"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/analysis/CharArrayMap;->containsKey([CII)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 310
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v0, v0, Lorg/apache/lucene/analysis/CharArrayMap;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/CharArrayMap;->originalKeySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CharArraySet;->stringIterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/CharArrayMap;->size()I

    move-result v0

    return v0
.end method

.method public stringIterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 295
    new-instance v0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;-><init>(Lorg/apache/lucene/analysis/CharArraySet;Lorg/apache/lucene/analysis/CharArraySet$1;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 316
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 317
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CharArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 318
    .local v1, "item":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    :cond_0
    instance-of v3, v1, [C

    if-eqz v3, :cond_1

    .line 320
    check-cast v1, [C

    .end local v1    # "item":Ljava/lang/Object;
    check-cast v1, [C

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 322
    .restart local v1    # "item":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 325
    .end local v1    # "item":Ljava/lang/Object;
    :cond_2
    const/16 v3, 0x5d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
