.class public abstract Lorg/apache/lucene/analysis/Analyzer;
.super Ljava/lang/Object;
.source "Analyzer.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private tokenStreams:Lorg/apache/lucene/util/CloseableThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/CloseableThreadLocal",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/Analyzer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-direct {v0}, Lorg/apache/lucene/util/CloseableThreadLocal;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/Analyzer;->tokenStreams:Lorg/apache/lucene/util/CloseableThreadLocal;

    .line 45
    sget-boolean v0, Lorg/apache/lucene/analysis/Analyzer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;->assertFinal()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 46
    :cond_0
    return-void
.end method

.method private assertFinal()Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 50
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 51
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v4

    if-nez v4, :cond_1

    .line 62
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    :goto_0
    return v2

    .line 59
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    sget-boolean v4, Lorg/apache/lucene/analysis/Analyzer;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    invoke-virtual {v0}, Ljava/lang/Class;->isAnonymousClass()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v4

    and-int/lit8 v4, v4, 0x12

    if-nez v4, :cond_0

    const-string/jumbo v4, "tokenStream"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-class v7, Ljava/io/Reader;

    aput-object v7, v5, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v4

    invoke-static {v4}, Ljava/lang/reflect/Modifier;->isFinal(I)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string/jumbo v4, "reusableTokenStream"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-class v7, Ljava/io/Reader;

    aput-object v7, v5, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v4

    invoke-static {v4}, Ljava/lang/reflect/Modifier;->isFinal(I)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_2
    new-instance v2, Ljava/lang/AssertionError;

    const-string/jumbo v4, "Analyzer implementation classes or at least their tokenStream() and reusableTokenStream() implementations must be final"

    invoke-direct {v2, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_0
    move-exception v1

    .local v1, "nsme":Ljava/lang/NoSuchMethodException;
    move v2, v3

    .line 62
    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/lucene/analysis/Analyzer;->tokenStreams:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v0}, Lorg/apache/lucene/util/CloseableThreadLocal;->close()V

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/Analyzer;->tokenStreams:Lorg/apache/lucene/util/CloseableThreadLocal;

    .line 154
    return-void
.end method

.method public getOffsetGap(Lorg/apache/lucene/document/Fieldable;)I
    .locals 1
    .param p1, "field"    # Lorg/apache/lucene/document/Fieldable;

    .prologue
    .line 144
    invoke-interface {p1}, Lorg/apache/lucene/document/Fieldable;->isTokenized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const/4 v0, 0x1

    .line 147
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPositionIncrementGap(Ljava/lang/String;)I
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 129
    const/4 v0, 0x0

    return v0
.end method

.method protected getPreviousTokenStream()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 90
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/Analyzer;->tokenStreams:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v1}, Lorg/apache/lucene/util/CloseableThreadLocal;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "npe":Ljava/lang/NullPointerException;
    iget-object v1, p0, Lorg/apache/lucene/analysis/Analyzer;->tokenStreams:Lorg/apache/lucene/util/CloseableThreadLocal;

    if-nez v1, :cond_0

    .line 93
    new-instance v1, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string/jumbo v2, "this Analyzer is closed"

    invoke-direct {v1, v2}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 95
    :cond_0
    throw v0
.end method

.method public reusableTokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v0

    return-object v0
.end method

.method protected setPreviousTokenStream(Ljava/lang/Object;)V
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 105
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/Analyzer;->tokenStreams:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/CloseableThreadLocal;->set(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "npe":Ljava/lang/NullPointerException;
    iget-object v1, p0, Lorg/apache/lucene/analysis/Analyzer;->tokenStreams:Lorg/apache/lucene/util/CloseableThreadLocal;

    if-nez v1, :cond_0

    .line 108
    new-instance v1, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string/jumbo v2, "this Analyzer is closed"

    invoke-direct {v1, v2}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 110
    :cond_0
    throw v0
.end method

.method public abstract tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;
.end method
