.class public final Lorg/apache/lucene/analysis/CachingTokenFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "CachingTokenFilter.java"


# instance fields
.field private cache:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/util/AttributeSource$State;",
            ">;"
        }
    .end annotation
.end field

.field private finalState:Lorg/apache/lucene/util/AttributeSource$State;

.field private iterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/util/AttributeSource$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 37
    iput-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->cache:Ljava/util/List;

    .line 38
    iput-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->iterator:Ljava/util/Iterator;

    .line 43
    return-void
.end method

.method private fillCache()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->cache:Ljava/util/List;

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 82
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 83
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->finalState:Lorg/apache/lucene/util/AttributeSource$State;

    .line 84
    return-void
.end method


# virtual methods
.method public final end()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->finalState:Lorg/apache/lucene/util/AttributeSource$State;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->finalState:Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 68
    :cond_0
    return-void
.end method

.method public final incrementToken()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->cache:Ljava/util/List;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->cache:Ljava/util/List;

    .line 50
    invoke-direct {p0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->fillCache()V

    .line 51
    iget-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->cache:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->iterator:Ljava/util/Iterator;

    .line 54
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 56
    const/4 v0, 0x0

    .line 60
    :goto_0
    return v0

    .line 59
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/CachingTokenFilter;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 60
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->cache:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->cache:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/CachingTokenFilter;->iterator:Ljava/util/Iterator;

    .line 75
    :cond_0
    return-void
.end method
