.class public Lorg/apache/lucene/analysis/LetterTokenizer;
.super Lorg/apache/lucene/analysis/CharTokenizer;
.source "LetterTokenizer.java"


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1, "in"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 97
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 1
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "in"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 119
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 120
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V
    .locals 1
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p2, "in"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 107
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V

    .line 108
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Ljava/io/Reader;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p3, "in"    # Ljava/io/Reader;

    .prologue
    .line 85
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p3, "in"    # Ljava/io/Reader;

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V

    .line 71
    return-void
.end method


# virtual methods
.method protected isTokenChar(I)Z
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 126
    invoke-static {p1}, Ljava/lang/Character;->isLetter(I)Z

    move-result v0

    return v0
.end method
