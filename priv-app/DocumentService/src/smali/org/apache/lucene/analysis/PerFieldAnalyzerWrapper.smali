.class public final Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "PerFieldAnalyzerWrapper.java"


# instance fields
.field private final analyzerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ">;"
        }
    .end annotation
.end field

.field private final defaultAnalyzer:Lorg/apache/lucene/analysis/Analyzer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p1, "defaultAnalyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;-><init>(Lorg/apache/lucene/analysis/Analyzer;Ljava/util/Map;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;Ljava/util/Map;)V
    .locals 1
    .param p1, "defaultAnalyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/analysis/Analyzer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p2, "fieldAnalyzers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;>;"
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;-><init>()V

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;->analyzerMap:Ljava/util/Map;

    .line 76
    iput-object p1, p0, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;->defaultAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 77
    if-eqz p2, :cond_0

    .line 78
    iget-object v0, p0, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;->analyzerMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 80
    :cond_0
    return-void
.end method


# virtual methods
.method public addAnalyzer(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;->analyzerMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    return-void
.end method

.method public getOffsetGap(Lorg/apache/lucene/document/Fieldable;)I
    .locals 3
    .param p1, "field"    # Lorg/apache/lucene/document/Fieldable;

    .prologue
    .line 127
    iget-object v1, p0, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;->analyzerMap:Ljava/util/Map;

    invoke-interface {p1}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/Analyzer;

    .line 128
    .local v0, "analyzer":Lorg/apache/lucene/analysis/Analyzer;
    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;->defaultAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 130
    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/Analyzer;->getOffsetGap(Lorg/apache/lucene/document/Fieldable;)I

    move-result v1

    return v1
.end method

.method public getPositionIncrementGap(Ljava/lang/String;)I
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 118
    iget-object v1, p0, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;->analyzerMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/Analyzer;

    .line 119
    .local v0, "analyzer":Lorg/apache/lucene/analysis/Analyzer;
    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;->defaultAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 121
    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/Analyzer;->getPositionIncrementGap(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public reusableTokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    iget-object v1, p0, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;->analyzerMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/Analyzer;

    .line 109
    .local v0, "analyzer":Lorg/apache/lucene/analysis/Analyzer;
    if-nez v0, :cond_0

    .line 110
    iget-object v0, p0, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;->defaultAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 112
    :cond_0
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/analysis/Analyzer;->reusableTokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v1

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PerFieldAnalyzerWrapper("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;->analyzerMap:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", default="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;->defaultAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 98
    iget-object v1, p0, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;->analyzerMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/Analyzer;

    .line 99
    .local v0, "analyzer":Lorg/apache/lucene/analysis/Analyzer;
    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lorg/apache/lucene/analysis/PerFieldAnalyzerWrapper;->defaultAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 103
    :cond_0
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v1

    return-object v1
.end method
