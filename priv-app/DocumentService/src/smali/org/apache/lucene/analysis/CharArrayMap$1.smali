.class Lorg/apache/lucene/analysis/CharArrayMap$1;
.super Lorg/apache/lucene/analysis/CharArraySet;
.source "CharArrayMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/analysis/CharArrayMap;->keySet()Lorg/apache/lucene/analysis/CharArraySet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/analysis/CharArrayMap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/analysis/CharArrayMap;Lorg/apache/lucene/analysis/CharArrayMap;)V
    .locals 0

    .prologue
    .line 396
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$1;, "Lorg/apache/lucene/analysis/CharArrayMap.1;"
    .local p2, "x0":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<Ljava/lang/Object;>;"
    iput-object p1, p0, Lorg/apache/lucene/analysis/CharArrayMap$1;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;)V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 389
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$1;, "Lorg/apache/lucene/analysis/CharArrayMap.1;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public add(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 385
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$1;, "Lorg/apache/lucene/analysis/CharArrayMap.1;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public add(Ljava/lang/String;)Z
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 393
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$1;, "Lorg/apache/lucene/analysis/CharArrayMap.1;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public add([C)Z
    .locals 1
    .param p1, "text"    # [C

    .prologue
    .line 397
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$1;, "Lorg/apache/lucene/analysis/CharArrayMap.1;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
