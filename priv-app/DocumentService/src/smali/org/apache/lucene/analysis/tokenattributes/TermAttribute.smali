.class public interface abstract Lorg/apache/lucene/analysis/tokenattributes/TermAttribute;
.super Ljava/lang/Object;
.source "TermAttribute.java"

# interfaces
.implements Lorg/apache/lucene/util/Attribute;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# virtual methods
.method public abstract resizeTermBuffer(I)[C
.end method

.method public abstract setTermBuffer(Ljava/lang/String;)V
.end method

.method public abstract setTermBuffer(Ljava/lang/String;II)V
.end method

.method public abstract setTermBuffer([CII)V
.end method

.method public abstract setTermLength(I)V
.end method

.method public abstract term()Ljava/lang/String;
.end method

.method public abstract termBuffer()[C
.end method

.method public abstract termLength()I
.end method
