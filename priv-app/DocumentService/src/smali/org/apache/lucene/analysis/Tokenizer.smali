.class public abstract Lorg/apache/lucene/analysis/Tokenizer;
.super Lorg/apache/lucene/analysis/TokenStream;
.source "Tokenizer.java"


# instance fields
.field protected input:Ljava/io/Reader;


# direct methods
.method protected constructor <init>()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/analysis/TokenStream;-><init>()V

    return-void
.end method

.method protected constructor <init>(Ljava/io/Reader;)V
    .locals 0
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/analysis/TokenStream;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    .line 46
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V
    .locals 0
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V

    .line 54
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 0
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V

    .line 59
    iput-object p2, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    .line 60
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/util/AttributeSource;)V
    .locals 0
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource;)V

    .line 68
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V
    .locals 0
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p2, "input"    # Ljava/io/Reader;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource;)V

    .line 73
    iput-object p2, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    .line 74
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    .line 85
    :cond_0
    return-void
.end method

.method protected final correctOffset(I)I
    .locals 1
    .param p1, "currentOff"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    instance-of v0, v0, Lorg/apache/lucene/analysis/CharStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    check-cast v0, Lorg/apache/lucene/analysis/CharStream;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/CharStream;->correctOffset(I)I

    move-result p1

    .end local p1    # "currentOff":I
    :cond_0
    return p1
.end method

.method public reset(Ljava/io/Reader;)V
    .locals 0
    .param p1, "input"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    iput-object p1, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    .line 102
    return-void
.end method
