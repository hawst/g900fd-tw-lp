.class public Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;
.super Ljava/lang/Object;
.source "CharArraySet.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/CharArraySet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CharArraySetIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field next:[C

.field pos:I

.field final synthetic this$0:Lorg/apache/lucene/analysis/CharArraySet;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/analysis/CharArraySet;)V
    .locals 1

    .prologue
    .line 258
    iput-object p1, p0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->this$0:Lorg/apache/lucene/analysis/CharArraySet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->pos:I

    .line 259
    invoke-direct {p0}, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->goNext()V

    .line 260
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/analysis/CharArraySet;Lorg/apache/lucene/analysis/CharArraySet$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/analysis/CharArraySet;
    .param p2, "x1"    # Lorg/apache/lucene/analysis/CharArraySet$1;

    .prologue
    .line 255
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;-><init>(Lorg/apache/lucene/analysis/CharArraySet;)V

    return-void
.end method

.method private goNext()V
    .locals 2

    .prologue
    .line 263
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->next:[C

    .line 264
    iget v0, p0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->pos:I

    .line 265
    :goto_0
    iget v0, p0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->pos:I

    iget-object v1, p0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->this$0:Lorg/apache/lucene/analysis/CharArraySet;

    # getter for: Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;
    invoke-static {v1}, Lorg/apache/lucene/analysis/CharArraySet;->access$000(Lorg/apache/lucene/analysis/CharArraySet;)Lorg/apache/lucene/analysis/CharArrayMap;

    move-result-object v1

    iget-object v1, v1, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->this$0:Lorg/apache/lucene/analysis/CharArraySet;

    # getter for: Lorg/apache/lucene/analysis/CharArraySet;->map:Lorg/apache/lucene/analysis/CharArrayMap;
    invoke-static {v0}, Lorg/apache/lucene/analysis/CharArraySet;->access$000(Lorg/apache/lucene/analysis/CharArraySet;)Lorg/apache/lucene/analysis/CharArrayMap;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    iget v1, p0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->pos:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->next:[C

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->pos:I

    goto :goto_0

    .line 266
    :cond_0
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->next:[C

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 255
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->next()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public next()Ljava/lang/String;
    .locals 2

    .prologue
    .line 282
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->nextCharArray()[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method public nextCharArray()[C
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->next:[C

    .line 275
    .local v0, "ret":[C
    invoke-direct {p0}, Lorg/apache/lucene/analysis/CharArraySet$CharArraySetIterator;->goNext()V

    .line 276
    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 286
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
