.class public interface abstract Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
.super Ljava/lang/Object;
.source "CharTermAttribute.java"

# interfaces
.implements Ljava/lang/Appendable;
.implements Ljava/lang/CharSequence;
.implements Lorg/apache/lucene/util/Attribute;


# virtual methods
.method public abstract append(C)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
.end method

.method public abstract append(Ljava/lang/CharSequence;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
.end method

.method public abstract append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
.end method

.method public abstract append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
.end method

.method public abstract append(Ljava/lang/StringBuilder;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
.end method

.method public abstract append(Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
.end method

.method public abstract buffer()[C
.end method

.method public abstract copyBuffer([CII)V
.end method

.method public abstract resizeBuffer(I)[C
.end method

.method public abstract setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
.end method

.method public abstract setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
.end method
