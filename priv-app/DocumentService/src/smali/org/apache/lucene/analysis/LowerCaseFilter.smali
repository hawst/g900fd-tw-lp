.class public final Lorg/apache/lucene/analysis/LowerCaseFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "LowerCaseFilter.java"


# instance fields
.field private final charUtils:Lorg/apache/lucene/util/CharacterUtils;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 55
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/analysis/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 46
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 37
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/LowerCaseFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/LowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 47
    invoke-static {p1}, Lorg/apache/lucene/util/CharacterUtils;->getInstance(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/util/CharacterUtils;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/LowerCaseFilter;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    .line 48
    return-void
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v3, p0, Lorg/apache/lucene/analysis/LowerCaseFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 61
    iget-object v3, p0, Lorg/apache/lucene/analysis/LowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 62
    .local v0, "buffer":[C
    iget-object v3, p0, Lorg/apache/lucene/analysis/LowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v2

    .line 63
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 64
    iget-object v3, p0, Lorg/apache/lucene/analysis/LowerCaseFilter;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    invoke-virtual {v3, v0, v1}, Lorg/apache/lucene/util/CharacterUtils;->codePointAt([CI)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v3

    invoke-static {v3, v0, v1}, Ljava/lang/Character;->toChars(I[CI)I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_0

    .line 68
    :cond_0
    const/4 v3, 0x1

    .line 70
    .end local v0    # "buffer":[C
    .end local v1    # "i":I
    .end local v2    # "length":I
    :goto_1
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method
