.class public final Lorg/apache/lucene/analysis/KeywordAnalyzer;
.super Lorg/apache/lucene/analysis/ReusableAnalyzerBase;
.source "KeywordAnalyzer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase;-><init>()V

    .line 28
    return-void
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 32
    new-instance v0, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;

    new-instance v1, Lorg/apache/lucene/analysis/KeywordTokenizer;

    invoke-direct {v1, p2}, Lorg/apache/lucene/analysis/KeywordTokenizer;-><init>(Ljava/io/Reader;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;)V

    return-object v0
.end method
