.class public Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "OffsetAttributeImpl.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;


# instance fields
.field private endOffset:I

.field private startOffset:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->startOffset:I

    .line 61
    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->endOffset:I

    .line 62
    return-void
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 3
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 87
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 88
    .local v0, "t":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->startOffset:I

    iget v2, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->endOffset:I

    invoke-interface {v0, v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 89
    return-void
.end method

.method public endOffset()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->endOffset:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    if-ne p1, p0, :cond_1

    .line 75
    :cond_0
    :goto_0
    return v1

    .line 70
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 71
    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;

    .line 72
    .local v0, "o":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;
    iget v3, v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->startOffset:I

    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->startOffset:I

    if-ne v3, v4, :cond_2

    iget v3, v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->endOffset:I

    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->endOffset:I

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "o":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;
    :cond_3
    move v1, v2

    .line 75
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 80
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->startOffset:I

    .line 81
    .local v0, "code":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->endOffset:I

    add-int v0, v1, v2

    .line 82
    return v0
.end method

.method public setOffset(II)V
    .locals 0
    .param p1, "startOffset"    # I
    .param p2, "endOffset"    # I

    .prologue
    .line 45
    iput p1, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->startOffset:I

    .line 46
    iput p2, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->endOffset:I

    .line 47
    return-void
.end method

.method public startOffset()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->startOffset:I

    return v0
.end method
