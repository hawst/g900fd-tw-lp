.class public final Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "ISOLatin1AccentFilter.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private output:[C

.field private outputPos:I

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 40
    const/16 v0, 0x100

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    .line 42
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 38
    return-void
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 46
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v5}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 47
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 48
    .local v0, "buffer":[C
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    .line 51
    .local v3, "length":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 52
    aget-char v1, v0, v2

    .line 53
    .local v1, "c":C
    const/16 v5, 0xc0

    if-lt v1, v5, :cond_2

    const v5, 0xfb06

    if-gt v1, v5, :cond_2

    .line 54
    invoke-virtual {p0, v0, v3}, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->removeAccents([CI)V

    .line 55
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    invoke-interface {v5, v6, v4, v7}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 59
    .end local v1    # "c":C
    :cond_0
    const/4 v4, 0x1

    .line 61
    .end local v0    # "buffer":[C
    .end local v2    # "i":I
    .end local v3    # "length":I
    :cond_1
    return v4

    .line 51
    .restart local v0    # "buffer":[C
    .restart local v1    # "c":C
    .restart local v2    # "i":I
    .restart local v3    # "length":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public final removeAccents([CI)V
    .locals 13
    .param p1, "input"    # [C
    .param p2, "length"    # I

    .prologue
    const/16 v12, 0x73

    const/16 v11, 0x69

    const/16 v10, 0x65

    const/16 v9, 0x45

    const/16 v8, 0x66

    .line 70
    mul-int/lit8 v2, p2, 0x2

    .line 72
    .local v2, "maxSizeNeeded":I
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    array-length v4, v5

    .line 73
    .local v4, "size":I
    :goto_0
    if-ge v4, v2, :cond_0

    .line 74
    mul-int/lit8 v4, v4, 0x2

    goto :goto_0

    .line 76
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    array-length v5, v5

    if-eq v4, v5, :cond_1

    .line 77
    new-array v5, v4, [C

    iput-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    .line 79
    :cond_1
    const/4 v5, 0x0

    iput v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    .line 81
    const/4 v3, 0x0

    .line 83
    .local v3, "pos":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, p2, :cond_4

    .line 84
    aget-char v0, p1, v3

    .line 88
    .local v0, "c":C
    const/16 v5, 0xc0

    if-lt v0, v5, :cond_2

    const v5, 0xfb06

    if-le v0, v5, :cond_3

    .line 89
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v0, v5, v6

    .line 83
    :goto_2
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 91
    :cond_3
    sparse-switch v0, :sswitch_data_0

    .line 254
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v0, v5, v6

    goto :goto_2

    .line 98
    :sswitch_0
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x41

    aput-char v7, v5, v6

    goto :goto_2

    .line 101
    :sswitch_1
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x41

    aput-char v7, v5, v6

    .line 102
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v9, v5, v6

    goto :goto_2

    .line 105
    :sswitch_2
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x43

    aput-char v7, v5, v6

    goto :goto_2

    .line 111
    :sswitch_3
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v9, v5, v6

    goto :goto_2

    .line 117
    :sswitch_4
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x49

    aput-char v7, v5, v6

    goto :goto_2

    .line 120
    :sswitch_5
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x49

    aput-char v7, v5, v6

    .line 121
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x4a

    aput-char v7, v5, v6

    goto :goto_2

    .line 124
    :sswitch_6
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x44

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 127
    :sswitch_7
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x4e

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 135
    :sswitch_8
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x4f

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 138
    :sswitch_9
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x4f

    aput-char v7, v5, v6

    .line 139
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v9, v5, v6

    goto/16 :goto_2

    .line 142
    :sswitch_a
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x54

    aput-char v7, v5, v6

    .line 143
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x48

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 149
    :sswitch_b
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x55

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 153
    :sswitch_c
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x59

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 161
    :sswitch_d
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x61

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 164
    :sswitch_e
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x61

    aput-char v7, v5, v6

    .line 165
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v10, v5, v6

    goto/16 :goto_2

    .line 168
    :sswitch_f
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x63

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 174
    :sswitch_10
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v10, v5, v6

    goto/16 :goto_2

    .line 180
    :sswitch_11
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v11, v5, v6

    goto/16 :goto_2

    .line 183
    :sswitch_12
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v11, v5, v6

    .line 184
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x6a

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 187
    :sswitch_13
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x64

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 190
    :sswitch_14
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x6e

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 198
    :sswitch_15
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x6f

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 201
    :sswitch_16
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x6f

    aput-char v7, v5, v6

    .line 202
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v10, v5, v6

    goto/16 :goto_2

    .line 205
    :sswitch_17
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v12, v5, v6

    .line 206
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v12, v5, v6

    goto/16 :goto_2

    .line 209
    :sswitch_18
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x74

    aput-char v7, v5, v6

    .line 210
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x68

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 216
    :sswitch_19
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x75

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 220
    :sswitch_1a
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x79

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 223
    :sswitch_1b
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v8, v5, v6

    .line 224
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v8, v5, v6

    goto/16 :goto_2

    .line 227
    :sswitch_1c
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v8, v5, v6

    .line 228
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v11, v5, v6

    goto/16 :goto_2

    .line 231
    :sswitch_1d
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v8, v5, v6

    .line 232
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x6c

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 246
    :sswitch_1e
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v8, v5, v6

    .line 247
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x74

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 250
    :sswitch_1f
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    aput-char v12, v5, v6

    .line 251
    iget-object v5, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->output:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/analysis/ISOLatin1AccentFilter;->outputPos:I

    const/16 v7, 0x74

    aput-char v7, v5, v6

    goto/16 :goto_2

    .line 259
    .end local v0    # "c":C
    :cond_4
    return-void

    .line 91
    nop

    :sswitch_data_0
    .sparse-switch
        0xc0 -> :sswitch_0
        0xc1 -> :sswitch_0
        0xc2 -> :sswitch_0
        0xc3 -> :sswitch_0
        0xc4 -> :sswitch_0
        0xc5 -> :sswitch_0
        0xc6 -> :sswitch_1
        0xc7 -> :sswitch_2
        0xc8 -> :sswitch_3
        0xc9 -> :sswitch_3
        0xca -> :sswitch_3
        0xcb -> :sswitch_3
        0xcc -> :sswitch_4
        0xcd -> :sswitch_4
        0xce -> :sswitch_4
        0xcf -> :sswitch_4
        0xd0 -> :sswitch_6
        0xd1 -> :sswitch_7
        0xd2 -> :sswitch_8
        0xd3 -> :sswitch_8
        0xd4 -> :sswitch_8
        0xd5 -> :sswitch_8
        0xd6 -> :sswitch_8
        0xd8 -> :sswitch_8
        0xd9 -> :sswitch_b
        0xda -> :sswitch_b
        0xdb -> :sswitch_b
        0xdc -> :sswitch_b
        0xdd -> :sswitch_c
        0xde -> :sswitch_a
        0xdf -> :sswitch_17
        0xe0 -> :sswitch_d
        0xe1 -> :sswitch_d
        0xe2 -> :sswitch_d
        0xe3 -> :sswitch_d
        0xe4 -> :sswitch_d
        0xe5 -> :sswitch_d
        0xe6 -> :sswitch_e
        0xe7 -> :sswitch_f
        0xe8 -> :sswitch_10
        0xe9 -> :sswitch_10
        0xea -> :sswitch_10
        0xeb -> :sswitch_10
        0xec -> :sswitch_11
        0xed -> :sswitch_11
        0xee -> :sswitch_11
        0xef -> :sswitch_11
        0xf0 -> :sswitch_13
        0xf1 -> :sswitch_14
        0xf2 -> :sswitch_15
        0xf3 -> :sswitch_15
        0xf4 -> :sswitch_15
        0xf5 -> :sswitch_15
        0xf6 -> :sswitch_15
        0xf8 -> :sswitch_15
        0xf9 -> :sswitch_19
        0xfa -> :sswitch_19
        0xfb -> :sswitch_19
        0xfc -> :sswitch_19
        0xfd -> :sswitch_1a
        0xfe -> :sswitch_18
        0xff -> :sswitch_1a
        0x132 -> :sswitch_5
        0x133 -> :sswitch_12
        0x152 -> :sswitch_9
        0x153 -> :sswitch_16
        0x178 -> :sswitch_c
        0xfb00 -> :sswitch_1b
        0xfb01 -> :sswitch_1c
        0xfb02 -> :sswitch_1d
        0xfb05 -> :sswitch_1e
        0xfb06 -> :sswitch_1f
    .end sparse-switch
.end method
