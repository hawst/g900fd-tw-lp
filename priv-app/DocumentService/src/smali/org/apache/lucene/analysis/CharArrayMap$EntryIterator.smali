.class public Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;
.super Ljava/lang/Object;
.source "CharArrayMap.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/CharArrayMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EntryIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/Object;",
        "TV;>;>;"
    }
.end annotation


# instance fields
.field private final allowModify:Z

.field private lastPos:I

.field private pos:I

.field final synthetic this$0:Lorg/apache/lucene/analysis/CharArrayMap;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/analysis/CharArrayMap;Z)V
    .locals 1
    .param p2, "allowModify"    # Z

    .prologue
    .line 410
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntryIterator;"
    iput-object p1, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 406
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->pos:I

    .line 411
    iput-boolean p2, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->allowModify:Z

    .line 412
    invoke-direct {p0}, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->goNext()V

    .line 413
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/analysis/CharArrayMap;ZLorg/apache/lucene/analysis/CharArrayMap$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/analysis/CharArrayMap;
    .param p2, "x1"    # Z
    .param p3, "x2"    # Lorg/apache/lucene/analysis/CharArrayMap$1;

    .prologue
    .line 405
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntryIterator;"
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;Z)V

    return-void
.end method

.method private goNext()V
    .locals 2

    .prologue
    .line 416
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntryIterator;"
    iget v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->pos:I

    iput v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->lastPos:I

    .line 417
    iget v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->pos:I

    .line 418
    :goto_0
    iget v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->pos:I

    iget-object v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v1, v1, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v0, v0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    iget v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->pos:I

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->pos:I

    goto :goto_0

    .line 419
    :cond_0
    return-void
.end method


# virtual methods
.method public currentValue()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 438
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntryIterator;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v0, v0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    iget v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->lastPos:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 422
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntryIterator;"
    iget v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->pos:I

    iget-object v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v1, v1, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 405
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntryIterator;"
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->next()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public next()Ljava/util/Map$Entry;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Object;",
            "TV;>;"
        }
    .end annotation

    .prologue
    .line 452
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntryIterator;"
    invoke-direct {p0}, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->goNext()V

    .line 453
    new-instance v0, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;

    iget-object v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget v2, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->lastPos:I

    iget-boolean v3, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->allowModify:Z

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;IZLorg/apache/lucene/analysis/CharArrayMap$1;)V

    return-object v0
.end method

.method public nextKey()[C
    .locals 2

    .prologue
    .line 427
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntryIterator;"
    invoke-direct {p0}, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->goNext()V

    .line 428
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v0, v0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    iget v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->lastPos:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public nextKeyString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 433
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntryIterator;"
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->nextKey()[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 457
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntryIterator;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)TV;"
        }
    .end annotation

    .prologue
    .line 443
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>.EntryIterator;"
    .local p1, "value":Ljava/lang/Object;, "TV;"
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->allowModify:Z

    if-nez v1, :cond_0

    .line 444
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v1

    .line 445
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v1, v1, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    iget v2, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->lastPos:I

    aget-object v0, v1, v2

    .line 446
    .local v0, "old":Ljava/lang/Object;, "TV;"
    iget-object v1, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->this$0:Lorg/apache/lucene/analysis/CharArrayMap;

    iget-object v1, v1, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    iget v2, p0, Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;->lastPos:I

    aput-object p1, v1, v2

    .line 447
    return-object v0
.end method
