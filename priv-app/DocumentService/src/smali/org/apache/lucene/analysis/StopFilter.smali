.class public final Lorg/apache/lucene/analysis/StopFilter;
.super Lorg/apache/lucene/analysis/FilteringTokenFilter;
.source "StopFilter.java"


# instance fields
.field private final stopWords:Lorg/apache/lucene/analysis/CharArraySet;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Lorg/apache/lucene/analysis/TokenStream;",
            "Ljava/util/Set",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 136
    .local p3, "stopWords":Ljava/util/Set;, "Ljava/util/Set<*>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/analysis/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;Z)V

    .line 137
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;Z)V
    .locals 6
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p4, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Lorg/apache/lucene/analysis/TokenStream;",
            "Ljava/util/Set",
            "<*>;Z)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 95
    .local p3, "stopWords":Ljava/util/Set;, "Ljava/util/Set<*>;"
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_29:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/StopFilter;-><init>(Lorg/apache/lucene/util/Version;ZLorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;Z)V

    .line 96
    return-void
.end method

.method private constructor <init>(Lorg/apache/lucene/util/Version;ZLorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;Z)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "enablePositionIncrements"    # Z
    .param p3, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p5, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Z",
            "Lorg/apache/lucene/analysis/TokenStream;",
            "Ljava/util/Set",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p4, "stopWords":Ljava/util/Set;, "Ljava/util/Set<*>;"
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/FilteringTokenFilter;-><init>(ZLorg/apache/lucene/analysis/TokenStream;)V

    .line 44
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/StopFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/StopFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 103
    instance-of v0, p4, Lorg/apache/lucene/analysis/CharArraySet;

    if-eqz v0, :cond_0

    check-cast p4, Lorg/apache/lucene/analysis/CharArraySet;

    .end local p4    # "stopWords":Ljava/util/Set;, "Ljava/util/Set<*>;"
    :goto_0
    iput-object p4, p0, Lorg/apache/lucene/analysis/StopFilter;->stopWords:Lorg/apache/lucene/analysis/CharArraySet;

    .line 104
    return-void

    .line 103
    .restart local p4    # "stopWords":Ljava/util/Set;, "Ljava/util/Set<*>;"
    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/CharArraySet;

    invoke-direct {v0, p1, p4, p5}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    move-object p4, v0

    goto :goto_0
.end method

.method public constructor <init>(ZLorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;)V
    .locals 6
    .param p1, "enablePositionIncrements"    # Z
    .param p2, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lorg/apache/lucene/analysis/TokenStream;",
            "Ljava/util/Set",
            "<*>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 118
    .local p3, "stopWords":Ljava/util/Set;, "Ljava/util/Set<*>;"
    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    const/4 v5, 0x0

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/StopFilter;-><init>(Lorg/apache/lucene/util/Version;ZLorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;Z)V

    .line 119
    return-void
.end method

.method public constructor <init>(ZLorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;Z)V
    .locals 6
    .param p1, "enablePositionIncrements"    # Z
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p4, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lorg/apache/lucene/analysis/TokenStream;",
            "Ljava/util/Set",
            "<*>;Z)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 66
    .local p3, "stopWords":Ljava/util/Set;, "Ljava/util/Set<*>;"
    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/StopFilter;-><init>(Lorg/apache/lucene/util/Version;ZLorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;Z)V

    .line 67
    return-void
.end method

.method public static getEnablePositionIncrementsVersionDefault(Lorg/apache/lucene/util/Version;)Z
    .locals 1
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 265
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_29:Lorg/apache/lucene/util/Version;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    return v0
.end method

.method public static final makeStopSet(Ljava/util/List;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 179
    .local p0, "stopWords":Ljava/util/List;, "Ljava/util/List<*>;"
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lorg/apache/lucene/analysis/StopFilter;->makeStopSet(Lorg/apache/lucene/util/Version;Ljava/util/List;Z)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final makeStopSet(Ljava/util/List;Z)Ljava/util/Set;
    .locals 1
    .param p1, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;Z)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 231
    .local p0, "stopWords":Ljava/util/List;, "Ljava/util/List<*>;"
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-static {v0, p0, p1}, Lorg/apache/lucene/analysis/StopFilter;->makeStopSet(Lorg/apache/lucene/util/Version;Ljava/util/List;Z)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final makeStopSet(Lorg/apache/lucene/util/Version;Ljava/util/List;)Ljava/util/Set;
    .locals 1
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/List",
            "<*>;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 194
    .local p1, "stopWords":Ljava/util/List;, "Ljava/util/List<*>;"
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/lucene/analysis/StopFilter;->makeStopSet(Lorg/apache/lucene/util/Version;Ljava/util/List;Z)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final makeStopSet(Lorg/apache/lucene/util/Version;Ljava/util/List;Z)Ljava/util/Set;
    .locals 2
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/List",
            "<*>;Z)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    .local p1, "stopWords":Ljava/util/List;, "Ljava/util/List<*>;"
    new-instance v0, Lorg/apache/lucene/analysis/CharArraySet;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, p0, v1, p2}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    .line 243
    .local v0, "stopSet":Lorg/apache/lucene/analysis/CharArraySet;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/CharArraySet;->addAll(Ljava/util/Collection;)Z

    .line 244
    return-object v0
.end method

.method public static final varargs makeStopSet(Lorg/apache/lucene/util/Version;[Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "stopWords"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/lucene/analysis/StopFilter;->makeStopSet(Lorg/apache/lucene/util/Version;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final makeStopSet(Lorg/apache/lucene/util/Version;[Ljava/lang/String;Z)Ljava/util/Set;
    .locals 2
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "stopWords"    # [Ljava/lang/String;
    .param p2, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "[",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    new-instance v0, Lorg/apache/lucene/analysis/CharArraySet;

    array-length v1, p1

    invoke-direct {v0, p0, v1, p2}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    .line 218
    .local v0, "stopSet":Lorg/apache/lucene/analysis/CharArraySet;
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/CharArraySet;->addAll(Ljava/util/Collection;)Z

    .line 219
    return-object v0
.end method

.method public static final varargs makeStopSet([Ljava/lang/String;)Ljava/util/Set;
    .locals 2
    .param p0, "stopWords"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 150
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lorg/apache/lucene/analysis/StopFilter;->makeStopSet(Lorg/apache/lucene/util/Version;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static final makeStopSet([Ljava/lang/String;Z)Ljava/util/Set;
    .locals 1
    .param p0, "stopWords"    # [Ljava/lang/String;
    .param p1, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 206
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-static {v0, p0, p1}, Lorg/apache/lucene/analysis/StopFilter;->makeStopSet(Lorg/apache/lucene/util/Version;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected accept()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 252
    iget-object v1, p0, Lorg/apache/lucene/analysis/StopFilter;->stopWords:Lorg/apache/lucene/analysis/CharArraySet;

    iget-object v2, p0, Lorg/apache/lucene/analysis/StopFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/StopFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    invoke-virtual {v1, v2, v0, v3}, Lorg/apache/lucene/analysis/CharArraySet;->contains([CII)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
