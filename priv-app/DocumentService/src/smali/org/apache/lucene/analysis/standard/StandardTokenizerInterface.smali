.class public interface abstract Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;
.super Ljava/lang/Object;
.source "StandardTokenizerInterface.java"


# static fields
.field public static final YYEOF:I = -0x1


# virtual methods
.method public abstract getNextToken()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getText(Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;)V
.end method

.method public abstract yychar()I
.end method

.method public abstract yylength()I
.end method

.method public abstract yyreset(Ljava/io/Reader;)V
.end method
