.class public Lorg/apache/lucene/analysis/CharArrayMap;
.super Ljava/util/AbstractMap;
.source "CharArrayMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/CharArrayMap$EmptyCharArrayMap;,
        Lorg/apache/lucene/analysis/CharArrayMap$UnmodifiableCharArrayMap;,
        Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;,
        Lorg/apache/lucene/analysis/CharArrayMap$MapEntry;,
        Lorg/apache/lucene/analysis/CharArrayMap$EntryIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<",
        "Ljava/lang/Object;",
        "TV;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final EMPTY_MAP:Lorg/apache/lucene/analysis/CharArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/analysis/CharArrayMap",
            "<*>;"
        }
    .end annotation
.end field

.field private static final INIT_SIZE:I = 0x8


# instance fields
.field private final charUtils:Lorg/apache/lucene/util/CharacterUtils;

.field private count:I

.field private entrySet:Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/analysis/CharArrayMap",
            "<TV;>.EntrySet;"
        }
    .end annotation
.end field

.field private ignoreCase:Z

.field private keySet:Lorg/apache/lucene/analysis/CharArraySet;

.field keys:[[C

.field final matchVersion:Lorg/apache/lucene/util/Version;

.field values:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TV;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/CharArrayMap;->$assertionsDisabled:Z

    .line 52
    new-instance v0, Lorg/apache/lucene/analysis/CharArrayMap$EmptyCharArrayMap;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/CharArrayMap$EmptyCharArrayMap;-><init>()V

    sput-object v0, Lorg/apache/lucene/analysis/CharArrayMap;->EMPTY_MAP:Lorg/apache/lucene/analysis/CharArrayMap;

    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/analysis/CharArrayMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/analysis/CharArrayMap",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    .local p1, "toCopy":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    const/4 v0, 0x0

    .line 104
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 356
    iput-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->entrySet:Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;

    .line 357
    iput-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keySet:Lorg/apache/lucene/analysis/CharArraySet;

    .line 105
    iget-object v0, p1, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    .line 106
    iget-object v0, p1, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    .line 107
    iget-boolean v0, p1, Lorg/apache/lucene/analysis/CharArrayMap;->ignoreCase:Z

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->ignoreCase:Z

    .line 108
    iget v0, p1, Lorg/apache/lucene/analysis/CharArrayMap;->count:I

    iput v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->count:I

    .line 109
    iget-object v0, p1, Lorg/apache/lucene/analysis/CharArrayMap;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    .line 110
    iget-object v0, p1, Lorg/apache/lucene/analysis/CharArrayMap;->matchVersion:Lorg/apache/lucene/util/Version;

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 111
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/analysis/CharArrayMap;Lorg/apache/lucene/analysis/CharArrayMap$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/analysis/CharArrayMap;
    .param p2, "x1"    # Lorg/apache/lucene/analysis/CharArrayMap$1;

    .prologue
    .line 50
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/CharArrayMap;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;IZ)V
    .locals 2
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "startSize"    # I
    .param p3, "ignoreCase"    # Z

    .prologue
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 356
    iput-object v1, p0, Lorg/apache/lucene/analysis/CharArrayMap;->entrySet:Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;

    .line 357
    iput-object v1, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keySet:Lorg/apache/lucene/analysis/CharArraySet;

    .line 76
    iput-boolean p3, p0, Lorg/apache/lucene/analysis/CharArrayMap;->ignoreCase:Z

    .line 77
    const/16 v0, 0x8

    .line 78
    .local v0, "size":I
    :goto_0
    shr-int/lit8 v1, p2, 0x2

    add-int/2addr v1, p2

    if-le v1, v0, :cond_0

    .line 79
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_0
    new-array v1, v0, [[C

    iput-object v1, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    .line 81
    new-array v1, v0, [Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    iput-object v1, p0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    .line 82
    invoke-static {p1}, Lorg/apache/lucene/util/CharacterUtils;->getInstance(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/util/CharacterUtils;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/CharArrayMap;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    .line 83
    iput-object p1, p0, Lorg/apache/lucene/analysis/CharArrayMap;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 84
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/util/Map;Z)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p3, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/Map",
            "<*+TV;>;Z)V"
        }
    .end annotation

    .prologue
    .line 99
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    .local p2, "c":Ljava/util/Map;, "Ljava/util/Map<*+TV;>;"
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {p0, p1, v0, p3}, Lorg/apache/lucene/analysis/CharArrayMap;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    .line 100
    invoke-virtual {p0, p2}, Lorg/apache/lucene/analysis/CharArrayMap;->putAll(Ljava/util/Map;)V

    .line 101
    return-void
.end method

.method static synthetic access$300(Lorg/apache/lucene/analysis/CharArrayMap;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/analysis/CharArrayMap;

    .prologue
    .line 50
    iget v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->count:I

    return v0
.end method

.method public static copy(Lorg/apache/lucene/util/Version;Ljava/util/Map;)Lorg/apache/lucene/analysis/CharArrayMap;
    .locals 7
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/Map",
            "<*+TV;>;)",
            "Lorg/apache/lucene/analysis/CharArrayMap",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<*+TV;>;"
    const/4 v6, 0x0

    .line 582
    sget-object v4, Lorg/apache/lucene/analysis/CharArrayMap;->EMPTY_MAP:Lorg/apache/lucene/analysis/CharArrayMap;

    if-ne p1, v4, :cond_0

    .line 583
    invoke-static {}, Lorg/apache/lucene/analysis/CharArrayMap;->emptyMap()Lorg/apache/lucene/analysis/CharArrayMap;

    move-result-object v2

    .line 597
    :goto_0
    return-object v2

    .line 584
    :cond_0
    instance-of v4, p1, Lorg/apache/lucene/analysis/CharArrayMap;

    if-eqz v4, :cond_1

    move-object v1, p1

    .line 585
    check-cast v1, Lorg/apache/lucene/analysis/CharArrayMap;

    .line 588
    .local v1, "m":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    iget-object v4, v1, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    array-length v4, v4

    new-array v0, v4, [[C

    .line 589
    .local v0, "keys":[[C
    iget-object v4, v1, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    array-length v5, v0

    invoke-static {v4, v6, v0, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 590
    iget-object v4, v1, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    array-length v4, v4

    new-array v3, v4, [Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    .line 591
    .local v3, "values":[Ljava/lang/Object;, "[TV;"
    iget-object v4, v1, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    array-length v5, v3

    invoke-static {v4, v6, v3, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 592
    new-instance v2, Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-direct {v2, v1}, Lorg/apache/lucene/analysis/CharArrayMap;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;)V

    .line 593
    .end local v1    # "m":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    .local v2, "m":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    iput-object v0, v2, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    .line 594
    iput-object v3, v2, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    goto :goto_0

    .line 597
    .end local v0    # "keys":[[C
    .end local v2    # "m":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    .end local v3    # "values":[Ljava/lang/Object;, "[TV;"
    :cond_1
    new-instance v2, Lorg/apache/lucene/analysis/CharArrayMap;

    invoke-direct {v2, p0, p1, v6}, Lorg/apache/lucene/analysis/CharArrayMap;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Map;Z)V

    goto :goto_0
.end method

.method public static emptyMap()Lorg/apache/lucene/analysis/CharArrayMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">()",
            "Lorg/apache/lucene/analysis/CharArrayMap",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 603
    sget-object v0, Lorg/apache/lucene/analysis/CharArrayMap;->EMPTY_MAP:Lorg/apache/lucene/analysis/CharArrayMap;

    return-object v0
.end method

.method private equals(Ljava/lang/CharSequence;[C)Z
    .locals 6
    .param p1, "text1"    # Ljava/lang/CharSequence;
    .param p2, "text2"    # [C

    .prologue
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    const/4 v3, 0x0

    .line 279
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 280
    .local v2, "len":I
    array-length v4, p2

    if-eq v2, v4, :cond_1

    .line 295
    :cond_0
    :goto_0
    return v3

    .line 282
    :cond_1
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->ignoreCase:Z

    if-eqz v4, :cond_2

    .line 283
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_3

    .line 284
    iget-object v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    invoke-virtual {v4, p1, v1}, Lorg/apache/lucene/util/CharacterUtils;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 285
    .local v0, "codePointAt":I
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/CharArrayMap;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    invoke-virtual {v5, p2, v1}, Lorg/apache/lucene/util/CharacterUtils;->codePointAt([CI)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 287
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 288
    goto :goto_1

    .line 290
    .end local v0    # "codePointAt":I
    .end local v1    # "i":I
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    if-ge v1, v2, :cond_3

    .line 291
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    aget-char v5, p2, v1

    if-ne v4, v5, :cond_0

    .line 290
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 295
    :cond_3
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private equals([CII[C)Z
    .locals 6
    .param p1, "text1"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I
    .param p4, "text2"    # [C

    .prologue
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    const/4 v3, 0x0

    .line 259
    array-length v4, p4

    if-eq p3, v4, :cond_1

    .line 275
    :cond_0
    :goto_0
    return v3

    .line 261
    :cond_1
    add-int v2, p2, p3

    .line 262
    .local v2, "limit":I
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->ignoreCase:Z

    if-eqz v4, :cond_2

    .line 263
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, p3, :cond_3

    .line 264
    iget-object v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    add-int v5, p2, v1

    invoke-virtual {v4, p1, v5, v2}, Lorg/apache/lucene/util/CharacterUtils;->codePointAt([CII)I

    move-result v0

    .line 265
    .local v0, "codePointAt":I
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/CharArrayMap;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    invoke-virtual {v5, p4, v1}, Lorg/apache/lucene/util/CharacterUtils;->codePointAt([CI)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 267
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 268
    goto :goto_1

    .line 270
    .end local v0    # "codePointAt":I
    .end local v1    # "i":I
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    if-ge v1, p3, :cond_3

    .line 271
    add-int v4, p2, v1

    aget-char v4, p1, v4

    aget-char v5, p4, v1

    if-ne v4, v5, :cond_0

    .line 270
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 275
    :cond_3
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private getHashCode(Ljava/lang/CharSequence;)I
    .locals 6
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 318
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    if-nez p1, :cond_0

    .line 319
    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    .line 320
    :cond_0
    const/4 v0, 0x0

    .line 321
    .local v0, "code":I
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 322
    .local v3, "len":I
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->ignoreCase:Z

    if-eqz v4, :cond_1

    .line 323
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 324
    iget-object v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    invoke-virtual {v4, p1, v2}, Lorg/apache/lucene/util/CharacterUtils;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v1

    .line 325
    .local v1, "codePointAt":I
    mul-int/lit8 v4, v0, 0x1f

    invoke-static {v1}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v5

    add-int v0, v4, v5

    .line 326
    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v2, v4

    .line 327
    goto :goto_0

    .line 329
    .end local v1    # "codePointAt":I
    .end local v2    # "i":I
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    if-ge v2, v3, :cond_2

    .line 330
    mul-int/lit8 v4, v0, 0x1f

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    add-int v0, v4, v5

    .line 329
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 333
    :cond_2
    return v0
.end method

.method private getHashCode([CII)I
    .locals 6
    .param p1, "text"    # [C
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 299
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    if-nez p1, :cond_0

    .line 300
    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    .line 301
    :cond_0
    const/4 v0, 0x0

    .line 302
    .local v0, "code":I
    add-int v3, p2, p3

    .line 303
    .local v3, "stop":I
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->ignoreCase:Z

    if-eqz v4, :cond_1

    .line 304
    move v2, p2

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 305
    iget-object v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    invoke-virtual {v4, p1, v2, v3}, Lorg/apache/lucene/util/CharacterUtils;->codePointAt([CII)I

    move-result v1

    .line 306
    .local v1, "codePointAt":I
    mul-int/lit8 v4, v0, 0x1f

    invoke-static {v1}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v5

    add-int v0, v4, v5

    .line 307
    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v2, v4

    .line 308
    goto :goto_0

    .line 310
    .end local v1    # "codePointAt":I
    .end local v2    # "i":I
    :cond_1
    move v2, p2

    .restart local v2    # "i":I
    :goto_1
    if-ge v2, v3, :cond_2

    .line 311
    mul-int/lit8 v4, v0, 0x1f

    aget-char v5, p1, v2

    add-int v0, v4, v5

    .line 310
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 314
    :cond_2
    return v0
.end method

.method private getSlot(Ljava/lang/CharSequence;)I
    .locals 5
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 178
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/CharArrayMap;->getHashCode(Ljava/lang/CharSequence;)I

    move-result v0

    .line 179
    .local v0, "code":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    and-int v2, v0, v4

    .line 180
    .local v2, "pos":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    aget-object v3, v4, v2

    .line 181
    .local v3, "text2":[C
    if-eqz v3, :cond_1

    invoke-direct {p0, p1, v3}, Lorg/apache/lucene/analysis/CharArrayMap;->equals(Ljava/lang/CharSequence;[C)Z

    move-result v4

    if-nez v4, :cond_1

    .line 182
    shr-int/lit8 v4, v0, 0x8

    add-int/2addr v4, v0

    or-int/lit8 v1, v4, 0x1

    .line 184
    .local v1, "inc":I
    :cond_0
    add-int/2addr v0, v1

    .line 185
    iget-object v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    and-int v2, v0, v4

    .line 186
    iget-object v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    aget-object v3, v4, v2

    .line 187
    if-eqz v3, :cond_1

    invoke-direct {p0, p1, v3}, Lorg/apache/lucene/analysis/CharArrayMap;->equals(Ljava/lang/CharSequence;[C)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 189
    .end local v1    # "inc":I
    :cond_1
    return v2
.end method

.method private getSlot([CII)I
    .locals 5
    .param p1, "text"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 162
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/CharArrayMap;->getHashCode([CII)I

    move-result v0

    .line 163
    .local v0, "code":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    and-int v2, v0, v4

    .line 164
    .local v2, "pos":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    aget-object v3, v4, v2

    .line 165
    .local v3, "text2":[C
    if-eqz v3, :cond_1

    invoke-direct {p0, p1, p2, p3, v3}, Lorg/apache/lucene/analysis/CharArrayMap;->equals([CII[C)Z

    move-result v4

    if-nez v4, :cond_1

    .line 166
    shr-int/lit8 v4, v0, 0x8

    add-int/2addr v4, v0

    or-int/lit8 v1, v4, 0x1

    .line 168
    .local v1, "inc":I
    :cond_0
    add-int/2addr v0, v1

    .line 169
    iget-object v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    and-int v2, v0, v4

    .line 170
    iget-object v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    aget-object v3, v4, v2

    .line 171
    if-eqz v3, :cond_1

    invoke-direct {p0, p1, p2, p3, v3}, Lorg/apache/lucene/analysis/CharArrayMap;->equals([CII[C)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 173
    .end local v1    # "inc":I
    :cond_1
    return v2
.end method

.method private rehash()V
    .locals 8

    .prologue
    .line 240
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    sget-boolean v6, Lorg/apache/lucene/analysis/CharArrayMap;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    array-length v6, v6

    iget-object v7, p0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    array-length v7, v7

    if-eq v6, v7, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 241
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    array-length v6, v6

    mul-int/lit8 v1, v6, 0x2

    .line 242
    .local v1, "newSize":I
    iget-object v2, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    .line 243
    .local v2, "oldkeys":[[C
    iget-object v3, p0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    .line 244
    .local v3, "oldvalues":[Ljava/lang/Object;, "[TV;"
    new-array v6, v1, [[C

    iput-object v6, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    .line 245
    new-array v6, v1, [Ljava/lang/Object;

    check-cast v6, [Ljava/lang/Object;

    iput-object v6, p0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    .line 247
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, v2

    if-ge v0, v6, :cond_2

    .line 248
    aget-object v5, v2, v0

    .line 249
    .local v5, "text":[C
    if-eqz v5, :cond_1

    .line 251
    const/4 v6, 0x0

    array-length v7, v5

    invoke-direct {p0, v5, v6, v7}, Lorg/apache/lucene/analysis/CharArrayMap;->getSlot([CII)I

    move-result v4

    .line 252
    .local v4, "slot":I
    iget-object v6, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    aput-object v5, v6, v4

    .line 253
    iget-object v6, p0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    aget-object v7, v3, v0

    aput-object v7, v6, v4

    .line 247
    .end local v4    # "slot":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 256
    .end local v5    # "text":[C
    :cond_2
    return-void
.end method

.method public static unmodifiableMap(Lorg/apache/lucene/analysis/CharArrayMap;)Lorg/apache/lucene/analysis/CharArrayMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/analysis/CharArrayMap",
            "<TV;>;)",
            "Lorg/apache/lucene/analysis/CharArrayMap",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 550
    .local p0, "map":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    if-nez p0, :cond_0

    .line 551
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Given map is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 552
    :cond_0
    invoke-static {}, Lorg/apache/lucene/analysis/CharArrayMap;->emptyMap()Lorg/apache/lucene/analysis/CharArrayMap;

    move-result-object v0

    if-eq p0, v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CharArrayMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 553
    :cond_1
    invoke-static {}, Lorg/apache/lucene/analysis/CharArrayMap;->emptyMap()Lorg/apache/lucene/analysis/CharArrayMap;

    move-result-object p0

    .line 556
    .end local p0    # "map":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    :cond_2
    :goto_0
    return-object p0

    .line 554
    .restart local p0    # "map":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    :cond_3
    instance-of v0, p0, Lorg/apache/lucene/analysis/CharArrayMap$UnmodifiableCharArrayMap;

    if-nez v0, :cond_2

    .line 556
    new-instance v0, Lorg/apache/lucene/analysis/CharArrayMap$UnmodifiableCharArrayMap;

    invoke-direct {v0, p0}, Lorg/apache/lucene/analysis/CharArrayMap$UnmodifiableCharArrayMap;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    const/4 v1, 0x0

    .line 116
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->count:I

    .line 117
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 118
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 119
    return-void
.end method

.method public containsKey(Ljava/lang/CharSequence;)Z
    .locals 2
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 129
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/CharArrayMap;->getSlot(Ljava/lang/CharSequence;)I

    move-result v1

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 134
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    instance-of v1, p1, [C

    if-eqz v1, :cond_0

    .line 135
    check-cast p1, [C

    .end local p1    # "o":Ljava/lang/Object;
    move-object v0, p1

    check-cast v0, [C

    .line 136
    .local v0, "text":[C
    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/lucene/analysis/CharArrayMap;->containsKey([CII)Z

    move-result v1

    .line 138
    .end local v0    # "text":[C
    :goto_0
    return v1

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v1

    goto :goto_0
.end method

.method public containsKey([CII)Z
    .locals 2
    .param p1, "text"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 124
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/CharArrayMap;->getSlot([CII)I

    move-result v1

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method createEntrySet()Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/analysis/CharArrayMap",
            "<TV;>.EntrySet;"
        }
    .end annotation

    .prologue
    .line 360
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    new-instance v0, Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;ZLorg/apache/lucene/analysis/CharArrayMap$1;)V

    return-object v0
.end method

.method public bridge synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 50
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CharArrayMap;->entrySet()Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;

    move-result-object v0

    return-object v0
.end method

.method public final entrySet()Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/analysis/CharArrayMap",
            "<TV;>.EntrySet;"
        }
    .end annotation

    .prologue
    .line 365
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->entrySet:Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;

    if-nez v0, :cond_0

    .line 366
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CharArrayMap;->createEntrySet()Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->entrySet:Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;

    .line 368
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->entrySet:Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;

    return-object v0
.end method

.method public get(Ljava/lang/CharSequence;)Ljava/lang/Object;
    .locals 2
    .param p1, "cs"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 149
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/CharArrayMap;->getSlot(Ljava/lang/CharSequence;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 154
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    instance-of v1, p1, [C

    if-eqz v1, :cond_0

    .line 155
    check-cast p1, [C

    .end local p1    # "o":Ljava/lang/Object;
    move-object v0, p1

    check-cast v0, [C

    .line 156
    .local v0, "text":[C
    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/lucene/analysis/CharArrayMap;->get([CII)Ljava/lang/Object;

    move-result-object v1

    .line 158
    .end local v0    # "text":[C
    :goto_0
    return-object v1

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/CharArrayMap;->get(Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public get([CII)Ljava/lang/Object;
    .locals 2
    .param p1, "text"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([CII)TV;"
        }
    .end annotation

    .prologue
    .line 144
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/CharArrayMap;->getSlot([CII)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public bridge synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 50
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CharArrayMap;->keySet()Lorg/apache/lucene/analysis/CharArraySet;

    move-result-object v0

    return-object v0
.end method

.method public final keySet()Lorg/apache/lucene/analysis/CharArraySet;
    .locals 1

    .prologue
    .line 380
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keySet:Lorg/apache/lucene/analysis/CharArraySet;

    if-nez v0, :cond_0

    .line 382
    new-instance v0, Lorg/apache/lucene/analysis/CharArrayMap$1;

    invoke-direct {v0, p0, p0}, Lorg/apache/lucene/analysis/CharArrayMap$1;-><init>(Lorg/apache/lucene/analysis/CharArrayMap;Lorg/apache/lucene/analysis/CharArrayMap;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keySet:Lorg/apache/lucene/analysis/CharArraySet;

    .line 401
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keySet:Lorg/apache/lucene/analysis/CharArraySet;

    return-object v0
.end method

.method final originalKeySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 373
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    invoke-super {p0}, Ljava/util/AbstractMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/CharSequence;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "TV;)TV;"
        }
    .end annotation

    .prologue
    .line 194
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/lucene/analysis/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "TV;)TV;"
        }
    .end annotation

    .prologue
    .line 199
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    instance-of v0, p1, [C

    if-eqz v0, :cond_0

    .line 200
    check-cast p1, [C

    .end local p1    # "o":Ljava/lang/Object;
    check-cast p1, [C

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/analysis/CharArrayMap;->put([CLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 202
    :goto_0
    return-object v0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/lucene/analysis/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TV;)TV;"
        }
    .end annotation

    .prologue
    .line 207
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/lucene/analysis/CharArrayMap;->put([CLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public put([CLjava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "text"    # [C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([CTV;)TV;"
        }
    .end annotation

    .prologue
    .line 215
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/CharArrayMap;->ignoreCase:Z

    if-eqz v3, :cond_0

    .line 216
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 217
    iget-object v3, p0, Lorg/apache/lucene/analysis/CharArrayMap;->charUtils:Lorg/apache/lucene/util/CharacterUtils;

    invoke-virtual {v3, p1, v0}, Lorg/apache/lucene/util/CharacterUtils;->codePointAt([CI)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v3

    invoke-static {v3, p1, v0}, Ljava/lang/Character;->toChars(I[CI)I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_0

    .line 221
    .end local v0    # "i":I
    :cond_0
    const/4 v3, 0x0

    array-length v4, p1

    invoke-direct {p0, p1, v3, v4}, Lorg/apache/lucene/analysis/CharArrayMap;->getSlot([CII)I

    move-result v2

    .line 222
    .local v2, "slot":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    aget-object v3, v3, v2

    if-eqz v3, :cond_1

    .line 223
    iget-object v3, p0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    aget-object v1, v3, v2

    .line 224
    .local v1, "oldValue":Ljava/lang/Object;, "TV;"
    iget-object v3, p0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    aput-object p2, v3, v2

    .line 235
    .end local v1    # "oldValue":Ljava/lang/Object;, "TV;"
    :goto_1
    return-object v1

    .line 227
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    aput-object p1, v3, v2

    .line 228
    iget-object v3, p0, Lorg/apache/lucene/analysis/CharArrayMap;->values:[Ljava/lang/Object;

    aput-object p2, v3, v2

    .line 229
    iget v3, p0, Lorg/apache/lucene/analysis/CharArrayMap;->count:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/analysis/CharArrayMap;->count:I

    .line 231
    iget v3, p0, Lorg/apache/lucene/analysis/CharArrayMap;->count:I

    iget v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->count:I

    shr-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget-object v4, p0, Lorg/apache/lucene/analysis/CharArrayMap;->keys:[[C

    array-length v4, v4

    if-le v3, v4, :cond_2

    .line 232
    invoke-direct {p0}, Lorg/apache/lucene/analysis/CharArrayMap;->rehash()V

    .line 235
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 338
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 343
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    iget v0, p0, Lorg/apache/lucene/analysis/CharArrayMap;->count:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 348
    .local p0, "this":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<TV;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "{"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 349
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/CharArrayMap;->entrySet()Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/CharArrayMap$EntrySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 350
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;TV;>;"
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 353
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;TV;>;"
    :cond_1
    const/16 v3, 0x7d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
