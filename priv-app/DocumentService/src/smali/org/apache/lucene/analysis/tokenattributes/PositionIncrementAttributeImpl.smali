.class public Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "PositionIncrementAttributeImpl.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;


# instance fields
.field private positionIncrement:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    .line 50
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttributeImpl;->positionIncrement:I

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttributeImpl;->positionIncrement:I

    .line 74
    return-void
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 2
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 97
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 98
    .local v0, "t":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttributeImpl;->positionIncrement:I

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 99
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78
    if-ne p1, p0, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v1

    .line 82
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttributeImpl;

    if-eqz v3, :cond_2

    move-object v0, p1

    .line 83
    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttributeImpl;

    .line 84
    .local v0, "_other":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttributeImpl;
    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttributeImpl;->positionIncrement:I

    iget v4, v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttributeImpl;->positionIncrement:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    .end local v0    # "_other":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttributeImpl;
    :cond_2
    move v1, v2

    .line 87
    goto :goto_0
.end method

.method public getPositionIncrement()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttributeImpl;->positionIncrement:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttributeImpl;->positionIncrement:I

    return v0
.end method

.method public setPositionIncrement(I)V
    .locals 3
    .param p1, "positionIncrement"    # I

    .prologue
    .line 57
    if-gez p1, :cond_0

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Increment must be zero or greater: got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    iput p1, p0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttributeImpl;->positionIncrement:I

    .line 62
    return-void
.end method
