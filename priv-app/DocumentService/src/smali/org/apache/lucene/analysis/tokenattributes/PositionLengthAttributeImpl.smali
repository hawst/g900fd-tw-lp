.class public Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "PositionLengthAttributeImpl.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;


# instance fields
.field private positionLength:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    .line 25
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttributeImpl;->positionLength:I

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttributeImpl;->positionLength:I

    .line 48
    return-void
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 2
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 71
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    .line 72
    .local v0, "t":Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;
    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttributeImpl;->positionLength:I

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;->setPositionLength(I)V

    .line 73
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    if-ne p1, p0, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v1

    .line 56
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttributeImpl;

    if-eqz v3, :cond_2

    move-object v0, p1

    .line 57
    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttributeImpl;

    .line 58
    .local v0, "_other":Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttributeImpl;
    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttributeImpl;->positionLength:I

    iget v4, v0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttributeImpl;->positionLength:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    .end local v0    # "_other":Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttributeImpl;
    :cond_2
    move v1, v2

    .line 61
    goto :goto_0
.end method

.method public getPositionLength()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttributeImpl;->positionLength:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttributeImpl;->positionLength:I

    return v0
.end method

.method public setPositionLength(I)V
    .locals 3
    .param p1, "positionLength"    # I

    .prologue
    .line 31
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 32
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Position length must be 1 or greater: got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_0
    iput p1, p0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttributeImpl;->positionLength:I

    .line 36
    return-void
.end method
