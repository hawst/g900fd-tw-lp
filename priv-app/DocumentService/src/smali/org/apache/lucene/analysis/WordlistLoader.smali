.class public Lorg/apache/lucene/analysis/WordlistLoader;
.super Ljava/lang/Object;
.source "WordlistLoader.java"


# static fields
.field private static final INITITAL_CAPACITY:I = 0x10


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getBufferedReader(Ljava/io/Reader;)Ljava/io/BufferedReader;
    .locals 1
    .param p0, "reader"    # Ljava/io/Reader;

    .prologue
    .line 198
    instance-of v0, p0, Ljava/io/BufferedReader;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/io/BufferedReader;

    .end local p0    # "reader":Ljava/io/Reader;
    :goto_0
    return-object p0

    .restart local p0    # "reader":Ljava/io/Reader;
    :cond_0
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, p0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static getSnowballWordSet(Ljava/io/Reader;Lorg/apache/lucene/analysis/CharArraySet;)Lorg/apache/lucene/analysis/CharArraySet;
    .locals 8
    .param p0, "reader"    # Ljava/io/Reader;
    .param p1, "result"    # Lorg/apache/lucene/analysis/CharArraySet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 137
    const/4 v0, 0x0

    .line 139
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    invoke-static {p0}, Lorg/apache/lucene/analysis/WordlistLoader;->getBufferedReader(Ljava/io/Reader;)Ljava/io/BufferedReader;

    move-result-object v0

    .line 140
    const/4 v3, 0x0

    .line 141
    .local v3, "line":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 142
    const/16 v5, 0x7c

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 143
    .local v1, "comment":I
    if-ltz v1, :cond_1

    const/4 v5, 0x0

    invoke-virtual {v3, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 144
    :cond_1
    const-string/jumbo v5, "\\s+"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 145
    .local v4, "words":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, v4

    if-ge v2, v5, :cond_0

    .line 146
    aget-object v5, v4, v2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    aget-object v5, v4, v2

    invoke-virtual {p1, v5}, Lorg/apache/lucene/analysis/CharArraySet;->add(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 149
    .end local v1    # "comment":I
    .end local v2    # "i":I
    .end local v4    # "words":[Ljava/lang/String;
    :cond_3
    new-array v5, v6, [Ljava/io/Closeable;

    aput-object v0, v5, v7

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 151
    return-object p1

    .line 149
    .end local v3    # "line":Ljava/lang/String;
    :catchall_0
    move-exception v5

    new-array v6, v6, [Ljava/io/Closeable;

    aput-object v0, v6, v7

    invoke-static {v6}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    throw v5
.end method

.method public static getSnowballWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/CharArraySet;
    .locals 3
    .param p0, "reader"    # Ljava/io/Reader;
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    new-instance v0, Lorg/apache/lucene/analysis/CharArraySet;

    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    invoke-static {p0, v0}, Lorg/apache/lucene/analysis/WordlistLoader;->getSnowballWordSet(Ljava/io/Reader;Lorg/apache/lucene/analysis/CharArraySet;)Lorg/apache/lucene/analysis/CharArraySet;

    move-result-object v0

    return-object v0
.end method

.method public static getStemDict(Ljava/io/Reader;Lorg/apache/lucene/analysis/CharArrayMap;)Lorg/apache/lucene/analysis/CharArrayMap;
    .locals 7
    .param p0, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/Reader;",
            "Lorg/apache/lucene/analysis/CharArrayMap",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/lucene/analysis/CharArrayMap",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "result":Lorg/apache/lucene/analysis/CharArrayMap;, "Lorg/apache/lucene/analysis/CharArrayMap<Ljava/lang/String;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 183
    const/4 v0, 0x0

    .line 185
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    invoke-static {p0}, Lorg/apache/lucene/analysis/WordlistLoader;->getBufferedReader(Ljava/io/Reader;)Ljava/io/BufferedReader;

    move-result-object v0

    .line 187
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .local v1, "line":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 188
    const-string/jumbo v3, "\t"

    const/4 v4, 0x2

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    .line 189
    .local v2, "wordstem":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v2, v3

    const/4 v4, 0x1

    aget-object v4, v2, v4

    invoke-virtual {p1, v3, v4}, Lorg/apache/lucene/analysis/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 192
    .end local v1    # "line":Ljava/lang/String;
    .end local v2    # "wordstem":[Ljava/lang/String;
    :catchall_0
    move-exception v3

    new-array v4, v6, [Ljava/io/Closeable;

    aput-object v0, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    throw v3

    .restart local v1    # "line":Ljava/lang/String;
    :cond_0
    new-array v3, v6, [Ljava/io/Closeable;

    aput-object v0, v3, v5

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 194
    return-object p1
.end method

.method public static getWordSet(Ljava/io/Reader;Ljava/lang/String;Lorg/apache/lucene/analysis/CharArraySet;)Lorg/apache/lucene/analysis/CharArraySet;
    .locals 5
    .param p0, "reader"    # Ljava/io/Reader;
    .param p1, "comment"    # Ljava/lang/String;
    .param p2, "result"    # Lorg/apache/lucene/analysis/CharArraySet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 103
    const/4 v0, 0x0

    .line 105
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    invoke-static {p0}, Lorg/apache/lucene/analysis/WordlistLoader;->getBufferedReader(Ljava/io/Reader;)Ljava/io/BufferedReader;

    move-result-object v0

    .line 106
    const/4 v1, 0x0

    .line 107
    .local v1, "word":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 108
    invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 109
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lorg/apache/lucene/analysis/CharArraySet;->add(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 114
    .end local v1    # "word":Ljava/lang/String;
    :catchall_0
    move-exception v2

    new-array v3, v3, [Ljava/io/Closeable;

    aput-object v0, v3, v4

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    throw v2

    .restart local v1    # "word":Ljava/lang/String;
    :cond_1
    new-array v2, v3, [Ljava/io/Closeable;

    aput-object v0, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 116
    return-object p2
.end method

.method public static getWordSet(Ljava/io/Reader;Ljava/lang/String;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/CharArraySet;
    .locals 3
    .param p0, "reader"    # Ljava/io/Reader;
    .param p1, "comment"    # Ljava/lang/String;
    .param p2, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    new-instance v0, Lorg/apache/lucene/analysis/CharArraySet;

    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-direct {v0, p2, v1, v2}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    invoke-static {p0, p1, v0}, Lorg/apache/lucene/analysis/WordlistLoader;->getWordSet(Ljava/io/Reader;Ljava/lang/String;Lorg/apache/lucene/analysis/CharArraySet;)Lorg/apache/lucene/analysis/CharArraySet;

    move-result-object v0

    return-object v0
.end method

.method public static getWordSet(Ljava/io/Reader;Lorg/apache/lucene/analysis/CharArraySet;)Lorg/apache/lucene/analysis/CharArraySet;
    .locals 5
    .param p0, "reader"    # Ljava/io/Reader;
    .param p1, "result"    # Lorg/apache/lucene/analysis/CharArraySet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 48
    const/4 v0, 0x0

    .line 50
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    invoke-static {p0}, Lorg/apache/lucene/analysis/WordlistLoader;->getBufferedReader(Ljava/io/Reader;)Ljava/io/BufferedReader;

    move-result-object v0

    .line 51
    const/4 v1, 0x0

    .line 52
    .local v1, "word":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 53
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/lucene/analysis/CharArraySet;->add(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 57
    .end local v1    # "word":Ljava/lang/String;
    :catchall_0
    move-exception v2

    new-array v3, v3, [Ljava/io/Closeable;

    aput-object v0, v3, v4

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    throw v2

    .restart local v1    # "word":Ljava/lang/String;
    :cond_0
    new-array v2, v3, [Ljava/io/Closeable;

    aput-object v0, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 59
    return-object p1
.end method

.method public static getWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/CharArraySet;
    .locals 3
    .param p0, "reader"    # Ljava/io/Reader;
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    new-instance v0, Lorg/apache/lucene/analysis/CharArraySet;

    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/lucene/analysis/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    invoke-static {p0, v0}, Lorg/apache/lucene/analysis/WordlistLoader;->getWordSet(Ljava/io/Reader;Lorg/apache/lucene/analysis/CharArraySet;)Lorg/apache/lucene/analysis/CharArraySet;

    move-result-object v0

    return-object v0
.end method
