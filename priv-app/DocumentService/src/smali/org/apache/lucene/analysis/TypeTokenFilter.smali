.class public final Lorg/apache/lucene/analysis/TypeTokenFilter;
.super Lorg/apache/lucene/analysis/FilteringTokenFilter;
.source "TypeTokenFilter.java"


# instance fields
.field private final stopTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

.field private final useWhiteList:Z


# direct methods
.method public constructor <init>(ZLorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;)V
    .locals 1
    .param p1, "enablePositionIncrements"    # Z
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lorg/apache/lucene/analysis/TokenStream;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p3, "stopTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/analysis/TypeTokenFilter;-><init>(ZLorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;Z)V

    .line 42
    return-void
.end method

.method public constructor <init>(ZLorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;Z)V
    .locals 1
    .param p1, "enablePositionIncrements"    # Z
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p4, "useWhiteList"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lorg/apache/lucene/analysis/TokenStream;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p3, "stopTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/FilteringTokenFilter;-><init>(ZLorg/apache/lucene/analysis/TokenStream;)V

    .line 31
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/TypeTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/TypeTokenFilter;->typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 36
    iput-object p3, p0, Lorg/apache/lucene/analysis/TypeTokenFilter;->stopTypes:Ljava/util/Set;

    .line 37
    iput-boolean p4, p0, Lorg/apache/lucene/analysis/TypeTokenFilter;->useWhiteList:Z

    .line 38
    return-void
.end method


# virtual methods
.method protected accept()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/TypeTokenFilter;->useWhiteList:Z

    iget-object v1, p0, Lorg/apache/lucene/analysis/TypeTokenFilter;->stopTypes:Ljava/util/Set;

    iget-object v2, p0, Lorg/apache/lucene/analysis/TypeTokenFilter;->typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->type()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
