.class public final Lorg/apache/lucene/analysis/CharReader;
.super Lorg/apache/lucene/analysis/CharStream;
.source "CharReader.java"


# instance fields
.field private final input:Ljava/io/Reader;


# direct methods
.method private constructor <init>(Ljava/io/Reader;)V
    .locals 0
    .param p1, "in"    # Ljava/io/Reader;

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/analysis/CharStream;-><init>()V

    .line 39
    iput-object p1, p0, Lorg/apache/lucene/analysis/CharReader;->input:Ljava/io/Reader;

    .line 40
    return-void
.end method

.method public static get(Ljava/io/Reader;)Lorg/apache/lucene/analysis/CharStream;
    .locals 1
    .param p0, "input"    # Ljava/io/Reader;

    .prologue
    .line 34
    instance-of v0, p0, Lorg/apache/lucene/analysis/CharStream;

    if-eqz v0, :cond_0

    check-cast p0, Lorg/apache/lucene/analysis/CharStream;

    .end local p0    # "input":Ljava/io/Reader;
    :goto_0
    return-object p0

    .restart local p0    # "input":Ljava/io/Reader;
    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/CharReader;

    invoke-direct {v0, p0}, Lorg/apache/lucene/analysis/CharReader;-><init>(Ljava/io/Reader;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharReader;->input:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    .line 50
    return-void
.end method

.method public correctOffset(I)I
    .locals 0
    .param p1, "currentOff"    # I

    .prologue
    .line 44
    return p1
.end method

.method public mark(I)V
    .locals 1
    .param p1, "readAheadLimit"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharReader;->input:Ljava/io/Reader;

    invoke-virtual {v0, p1}, Ljava/io/Reader;->mark(I)V

    .line 65
    return-void
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharReader;->input:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->markSupported()Z

    move-result v0

    return v0
.end method

.method public read([CII)I
    .locals 1
    .param p1, "cbuf"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharReader;->input:Ljava/io/Reader;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/Reader;->read([CII)I

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharReader;->input:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->reset()V

    .line 70
    return-void
.end method
