.class Lorg/apache/lucene/analysis/standard/ClassicAnalyzer$1;
.super Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;
.source "ClassicAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;

.field final synthetic val$src:Lorg/apache/lucene/analysis/standard/ClassicTokenizer;


# direct methods
.method constructor <init>(Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/standard/ClassicTokenizer;)V
    .locals 0
    .param p2, "x0"    # Lorg/apache/lucene/analysis/Tokenizer;
    .param p3, "x1"    # Lorg/apache/lucene/analysis/TokenStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 139
    iput-object p1, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer$1;->this$0:Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;

    iput-object p4, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer$1;->val$src:Lorg/apache/lucene/analysis/standard/ClassicTokenizer;

    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-void
.end method


# virtual methods
.method protected reset(Ljava/io/Reader;)Z
    .locals 2
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer$1;->val$src:Lorg/apache/lucene/analysis/standard/ClassicTokenizer;

    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer$1;->this$0:Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;

    # getter for: Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->maxTokenLength:I
    invoke-static {v1}, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->access$000(Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;)I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->setMaxTokenLength(I)V

    .line 141
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;->reset(Ljava/io/Reader;)Z

    move-result v0

    return v0
.end method
