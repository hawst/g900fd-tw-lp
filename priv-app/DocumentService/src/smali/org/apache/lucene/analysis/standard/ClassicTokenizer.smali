.class public final Lorg/apache/lucene/analysis/standard/ClassicTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "ClassicTokenizer.java"


# static fields
.field public static final ACRONYM:I = 0x2

.field public static final ACRONYM_DEP:I = 0x8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ALPHANUM:I = 0x0

.field public static final APOSTROPHE:I = 0x1

.field public static final CJ:I = 0x7

.field public static final COMPANY:I = 0x3

.field public static final EMAIL:I = 0x4

.field public static final HOST:I = 0x5

.field public static final NUM:I = 0x6

.field public static final TOKEN_TYPES:[Ljava/lang/String;


# instance fields
.field private maxTokenLength:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private replaceInvalidAcronym:Z

.field private scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 81
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "<ALPHANUM>"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "<APOSTROPHE>"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "<ACRONYM>"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "<COMPANY>"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "<EMAIL>"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "<HOST>"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "<NUM>"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "<CJ>"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "<ACRONYM_DEP>"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Ljava/io/Reader;

    .prologue
    .line 117
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 95
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->maxTokenLength:I

    .line 149
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 150
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 151
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 152
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 118
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->init(Lorg/apache/lucene/util/Version;)V

    .line 119
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p3, "input"    # Ljava/io/Reader;

    .prologue
    .line 133
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 95
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->maxTokenLength:I

    .line 149
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 150
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 151
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 152
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 134
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->init(Lorg/apache/lucene/util/Version;)V

    .line 135
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p3, "input"    # Ljava/io/Reader;

    .prologue
    .line 125
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource;Ljava/io/Reader;)V

    .line 95
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->maxTokenLength:I

    .line 149
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 150
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 151
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 152
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 126
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->init(Lorg/apache/lucene/util/Version;)V

    .line 127
    return-void
.end method

.method private final init(Lorg/apache/lucene/util/Version;)V
    .locals 2
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 138
    new-instance v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;

    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->input:Ljava/io/Reader;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    .line 140
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_24:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->replaceInvalidAcronym:Z

    .line 145
    :goto_0
    return-void

    .line 143
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->replaceInvalidAcronym:Z

    goto :goto_0
.end method


# virtual methods
.method public final end()V
    .locals 3

    .prologue
    .line 200
    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yychar()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yylength()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->correctOffset(I)I

    move-result v0

    .line 201
    .local v0, "finalOffset":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1, v0, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 202
    return-void
.end method

.method public getMaxTokenLength()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->maxTokenLength:I

    return v0
.end method

.method public final incrementToken()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->clearAttributes()V

    .line 162
    const/4 v0, 0x1

    .line 165
    .local v0, "posIncr":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->getNextToken()I

    move-result v2

    .line 167
    .local v2, "tokenType":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 168
    const/4 v3, 0x0

    .line 189
    :goto_1
    return v3

    .line 171
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yylength()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->maxTokenLength:I

    if-gt v3, v4, :cond_3

    .line 172
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3, v0}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 173
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    iget-object v4, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->getText(Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;)V

    .line 174
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yychar()I

    move-result v1

    .line 175
    .local v1, "start":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->correctOffset(I)I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {p0, v5}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->correctOffset(I)I

    move-result v5

    invoke-interface {v3, v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 179
    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    .line 180
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->replaceInvalidAcronym:Z

    if-eqz v3, :cond_1

    .line 181
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    sget-object v4, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/4 v5, 0x5

    aget-object v4, v4, v5

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 182
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v4, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 189
    :goto_2
    const/4 v3, 0x1

    goto :goto_1

    .line 184
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    sget-object v4, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    goto :goto_2

    .line 187
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    sget-object v4, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    goto :goto_2

    .line 193
    .end local v1    # "start":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 194
    goto :goto_0
.end method

.method public isReplaceInvalidAcronym()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 219
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->replaceInvalidAcronym:Z

    return v0
.end method

.method public reset(Ljava/io/Reader;)V
    .locals 1
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 206
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/Tokenizer;->reset(Ljava/io/Reader;)V

    .line 207
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v0, p1}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yyreset(Ljava/io/Reader;)V

    .line 208
    return-void
.end method

.method public setMaxTokenLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 100
    iput p1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->maxTokenLength:I

    .line 101
    return-void
.end method

.method public setReplaceInvalidAcronym(Z)V
    .locals 0
    .param p1, "replaceInvalidAcronym"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 231
    iput-boolean p1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->replaceInvalidAcronym:Z

    .line 232
    return-void
.end method
