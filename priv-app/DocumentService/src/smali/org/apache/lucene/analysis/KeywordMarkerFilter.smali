.class public final Lorg/apache/lucene/analysis/KeywordMarkerFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "KeywordMarkerFilter.java"


# instance fields
.field private final keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

.field private final keywordSet:Lorg/apache/lucene/analysis/CharArraySet;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/analysis/TokenStream;",
            "Ljava/util/Set",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p2, "keywordSet":Ljava/util/Set;, "Ljava/util/Set<*>;"
    instance-of v0, p2, Lorg/apache/lucene/analysis/CharArraySet;

    if-eqz v0, :cond_0

    check-cast p2, Lorg/apache/lucene/analysis/CharArraySet;

    .end local p2    # "keywordSet":Ljava/util/Set;, "Ljava/util/Set<*>;"
    :goto_0
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/KeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/CharArraySet;)V

    .line 69
    return-void

    .line 67
    .restart local p2    # "keywordSet":Ljava/util/Set;, "Ljava/util/Set<*>;"
    :cond_0
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-static {v0, p2}, Lorg/apache/lucene/analysis/CharArraySet;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/CharArraySet;

    move-result-object p2

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/CharArraySet;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "keywordSet"    # Lorg/apache/lucene/analysis/CharArraySet;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 36
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/KeywordMarkerFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/KeywordMarkerFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 37
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/KeywordMarkerFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/KeywordMarkerFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 53
    iput-object p2, p0, Lorg/apache/lucene/analysis/KeywordMarkerFilter;->keywordSet:Lorg/apache/lucene/analysis/CharArraySet;

    .line 54
    return-void
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 73
    iget-object v2, p0, Lorg/apache/lucene/analysis/KeywordMarkerFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 74
    iget-object v2, p0, Lorg/apache/lucene/analysis/KeywordMarkerFilter;->keywordSet:Lorg/apache/lucene/analysis/CharArraySet;

    iget-object v3, p0, Lorg/apache/lucene/analysis/KeywordMarkerFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/analysis/KeywordMarkerFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v4

    invoke-virtual {v2, v3, v1, v4}, Lorg/apache/lucene/analysis/CharArraySet;->contains([CII)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lorg/apache/lucene/analysis/KeywordMarkerFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v1, v0}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->setKeyword(Z)V

    .line 79
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
