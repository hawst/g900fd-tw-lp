.class public final Lorg/apache/lucene/analysis/SimpleAnalyzer;
.super Lorg/apache/lucene/analysis/ReusableAnalyzerBase;
.source "SimpleAnalyzer.java"


# instance fields
.field private final matchVersion:Lorg/apache/lucene/util/Version;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 53
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/SimpleAnalyzer;-><init>(Lorg/apache/lucene/util/Version;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/apache/lucene/analysis/SimpleAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 46
    return-void
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 58
    new-instance v0, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;

    new-instance v1, Lorg/apache/lucene/analysis/LowerCaseTokenizer;

    iget-object v2, p0, Lorg/apache/lucene/analysis/SimpleAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v1, v2, p2}, Lorg/apache/lucene/analysis/LowerCaseTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/ReusableAnalyzerBase$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;)V

    return-object v0
.end method
