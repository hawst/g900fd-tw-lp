.class final Lorg/apache/lucene/util/fst/FST$ForwardBytesReader;
.super Lorg/apache/lucene/util/fst/FST$BytesReader;
.source "FST.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/FST;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ForwardBytesReader"
.end annotation


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "bytes"    # [B
    .param p2, "pos"    # I

    .prologue
    .line 1280
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;-><init>([BI)V

    .line 1281
    return-void
.end method


# virtual methods
.method public readByte()B
    .locals 3

    .prologue
    .line 1285
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST$ForwardBytesReader;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/util/fst/FST$ForwardBytesReader;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/util/fst/FST$ForwardBytesReader;->pos:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public readBytes([BII)V
    .locals 2
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 1290
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST$ForwardBytesReader;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/util/fst/FST$ForwardBytesReader;->pos:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1291
    iget v0, p0, Lorg/apache/lucene/util/fst/FST$ForwardBytesReader;->pos:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$ForwardBytesReader;->pos:I

    .line 1292
    return-void
.end method

.method public skip(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 1295
    iget v0, p0, Lorg/apache/lucene/util/fst/FST$ForwardBytesReader;->pos:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$ForwardBytesReader;->pos:I

    .line 1296
    return-void
.end method

.method public skip(II)V
    .locals 1
    .param p1, "base"    # I
    .param p2, "count"    # I

    .prologue
    .line 1299
    add-int v0, p1, p2

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$ForwardBytesReader;->pos:I

    .line 1300
    return-void
.end method
