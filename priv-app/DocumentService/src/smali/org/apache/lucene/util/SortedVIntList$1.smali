.class Lorg/apache/lucene/util/SortedVIntList$1;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "SortedVIntList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/SortedVIntList;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field bytePos:I

.field doc:I

.field lastInt:I

.field final synthetic this$0:Lorg/apache/lucene/util/SortedVIntList;


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/SortedVIntList;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 215
    iput-object p1, p0, Lorg/apache/lucene/util/SortedVIntList$1;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 184
    iput v0, p0, Lorg/apache/lucene/util/SortedVIntList$1;->bytePos:I

    .line 185
    iput v0, p0, Lorg/apache/lucene/util/SortedVIntList$1;->lastInt:I

    .line 186
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/SortedVIntList$1;->doc:I

    return-void
.end method

.method private advance()V
    .locals 5

    .prologue
    .line 190
    iget-object v2, p0, Lorg/apache/lucene/util/SortedVIntList$1;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    # getter for: Lorg/apache/lucene/util/SortedVIntList;->bytes:[B
    invoke-static {v2}, Lorg/apache/lucene/util/SortedVIntList;->access$200(Lorg/apache/lucene/util/SortedVIntList;)[B

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/util/SortedVIntList$1;->bytePos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/util/SortedVIntList$1;->bytePos:I

    aget-byte v0, v2, v3

    .line 191
    .local v0, "b":B
    iget v2, p0, Lorg/apache/lucene/util/SortedVIntList$1;->lastInt:I

    and-int/lit8 v3, v0, 0x7f

    add-int/2addr v2, v3

    iput v2, p0, Lorg/apache/lucene/util/SortedVIntList$1;->lastInt:I

    .line 192
    const/4 v1, 0x7

    .local v1, "s":I
    :goto_0
    and-int/lit8 v2, v0, -0x80

    if-eqz v2, :cond_0

    .line 193
    iget-object v2, p0, Lorg/apache/lucene/util/SortedVIntList$1;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    # getter for: Lorg/apache/lucene/util/SortedVIntList;->bytes:[B
    invoke-static {v2}, Lorg/apache/lucene/util/SortedVIntList;->access$200(Lorg/apache/lucene/util/SortedVIntList;)[B

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/util/SortedVIntList$1;->bytePos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/util/SortedVIntList$1;->bytePos:I

    aget-byte v0, v2, v3

    .line 194
    iget v2, p0, Lorg/apache/lucene/util/SortedVIntList$1;->lastInt:I

    and-int/lit8 v3, v0, 0x7f

    shl-int/2addr v3, v1

    add-int/2addr v2, v3

    iput v2, p0, Lorg/apache/lucene/util/SortedVIntList$1;->lastInt:I

    .line 192
    add-int/lit8 v1, v1, 0x7

    goto :goto_0

    .line 196
    :cond_0
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I

    .prologue
    .line 216
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/SortedVIntList$1;->bytePos:I

    iget-object v1, p0, Lorg/apache/lucene/util/SortedVIntList$1;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    # getter for: Lorg/apache/lucene/util/SortedVIntList;->lastBytePos:I
    invoke-static {v1}, Lorg/apache/lucene/util/SortedVIntList;->access$100(Lorg/apache/lucene/util/SortedVIntList;)I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 217
    invoke-direct {p0}, Lorg/apache/lucene/util/SortedVIntList$1;->advance()V

    .line 218
    iget v0, p0, Lorg/apache/lucene/util/SortedVIntList$1;->lastInt:I

    if-lt v0, p1, :cond_0

    .line 219
    iget v0, p0, Lorg/apache/lucene/util/SortedVIntList$1;->lastInt:I

    iput v0, p0, Lorg/apache/lucene/util/SortedVIntList$1;->doc:I

    .line 222
    :goto_0
    return v0

    :cond_1
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/util/SortedVIntList$1;->doc:I

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lorg/apache/lucene/util/SortedVIntList$1;->doc:I

    return v0
.end method

.method public nextDoc()I
    .locals 2

    .prologue
    .line 205
    iget v0, p0, Lorg/apache/lucene/util/SortedVIntList$1;->bytePos:I

    iget-object v1, p0, Lorg/apache/lucene/util/SortedVIntList$1;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    # getter for: Lorg/apache/lucene/util/SortedVIntList;->lastBytePos:I
    invoke-static {v1}, Lorg/apache/lucene/util/SortedVIntList;->access$100(Lorg/apache/lucene/util/SortedVIntList;)I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 206
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/util/SortedVIntList$1;->doc:I

    .line 211
    :goto_0
    iget v0, p0, Lorg/apache/lucene/util/SortedVIntList$1;->doc:I

    return v0

    .line 208
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/util/SortedVIntList$1;->advance()V

    .line 209
    iget v0, p0, Lorg/apache/lucene/util/SortedVIntList$1;->lastInt:I

    iput v0, p0, Lorg/apache/lucene/util/SortedVIntList$1;->doc:I

    goto :goto_0
.end method
