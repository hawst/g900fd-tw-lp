.class public final Lorg/apache/lucene/util/TwoPhaseCommitTool$TwoPhaseCommitWrapper;
.super Ljava/lang/Object;
.source "TwoPhaseCommitTool.java"

# interfaces
.implements Lorg/apache/lucene/util/TwoPhaseCommit;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/TwoPhaseCommitTool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TwoPhaseCommitWrapper"
.end annotation


# instance fields
.field private final commitData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final tpc:Lorg/apache/lucene/util/TwoPhaseCommit;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/TwoPhaseCommit;Ljava/util/Map;)V
    .locals 0
    .param p1, "tpc"    # Lorg/apache/lucene/util/TwoPhaseCommit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/TwoPhaseCommit;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p2, "commitData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lorg/apache/lucene/util/TwoPhaseCommitTool$TwoPhaseCommitWrapper;->tpc:Lorg/apache/lucene/util/TwoPhaseCommit;

    .line 44
    iput-object p2, p0, Lorg/apache/lucene/util/TwoPhaseCommitTool$TwoPhaseCommitWrapper;->commitData:Ljava/util/Map;

    .line 45
    return-void
.end method


# virtual methods
.method public commit()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/util/TwoPhaseCommitTool$TwoPhaseCommitWrapper;->commitData:Ljava/util/Map;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/TwoPhaseCommitTool$TwoPhaseCommitWrapper;->commit(Ljava/util/Map;)V

    .line 57
    return-void
.end method

.method public commit(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    .local p1, "commitData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/TwoPhaseCommitTool$TwoPhaseCommitWrapper;->tpc:Lorg/apache/lucene/util/TwoPhaseCommit;

    iget-object v1, p0, Lorg/apache/lucene/util/TwoPhaseCommitTool$TwoPhaseCommitWrapper;->commitData:Ljava/util/Map;

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/TwoPhaseCommit;->commit(Ljava/util/Map;)V

    .line 61
    return-void
.end method

.method public prepareCommit()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/lucene/util/TwoPhaseCommitTool$TwoPhaseCommitWrapper;->commitData:Ljava/util/Map;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/TwoPhaseCommitTool$TwoPhaseCommitWrapper;->prepareCommit(Ljava/util/Map;)V

    .line 49
    return-void
.end method

.method public prepareCommit(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "commitData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/TwoPhaseCommitTool$TwoPhaseCommitWrapper;->tpc:Lorg/apache/lucene/util/TwoPhaseCommit;

    iget-object v1, p0, Lorg/apache/lucene/util/TwoPhaseCommitTool$TwoPhaseCommitWrapper;->commitData:Ljava/util/Map;

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/TwoPhaseCommit;->prepareCommit(Ljava/util/Map;)V

    .line 53
    return-void
.end method

.method public rollback()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/util/TwoPhaseCommitTool$TwoPhaseCommitWrapper;->tpc:Lorg/apache/lucene/util/TwoPhaseCommit;

    invoke-interface {v0}, Lorg/apache/lucene/util/TwoPhaseCommit;->rollback()V

    .line 65
    return-void
.end method
