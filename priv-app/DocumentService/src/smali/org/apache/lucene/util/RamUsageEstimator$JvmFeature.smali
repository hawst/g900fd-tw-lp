.class public final enum Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;
.super Ljava/lang/Enum;
.source "RamUsageEstimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/RamUsageEstimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "JvmFeature"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

.field public static final enum ARRAY_HEADER_SIZE:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

.field public static final enum FIELD_OFFSETS:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

.field public static final enum OBJECT_ALIGNMENT:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

.field public static final enum OBJECT_REFERENCE_SIZE:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;


# instance fields
.field public final description:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 52
    new-instance v0, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    const-string/jumbo v1, "OBJECT_REFERENCE_SIZE"

    const-string/jumbo v2, "Object reference size estimated using array index scale"

    invoke-direct {v0, v1, v3, v2}, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->OBJECT_REFERENCE_SIZE:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    .line 53
    new-instance v0, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    const-string/jumbo v1, "ARRAY_HEADER_SIZE"

    const-string/jumbo v2, "Array header size estimated using array based offset"

    invoke-direct {v0, v1, v4, v2}, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->ARRAY_HEADER_SIZE:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    .line 54
    new-instance v0, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    const-string/jumbo v1, "FIELD_OFFSETS"

    const-string/jumbo v2, "Shallow instance size based on field offsets"

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->FIELD_OFFSETS:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    .line 55
    new-instance v0, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    const-string/jumbo v1, "OBJECT_ALIGNMENT"

    const-string/jumbo v2, "Object alignment retrieved from HotSpotDiagnostic MX bean"

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->OBJECT_ALIGNMENT:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    .line 51
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    sget-object v1, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->OBJECT_REFERENCE_SIZE:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->ARRAY_HEADER_SIZE:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->FIELD_OFFSETS:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->OBJECT_ALIGNMENT:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    aput-object v1, v0, v6

    sput-object v0, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->$VALUES:[Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "description"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 60
    iput-object p3, p0, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->description:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    const-class v0, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    return-object v0
.end method

.method public static final values()[Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->$VALUES:[Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    invoke-virtual {v0}, [Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
