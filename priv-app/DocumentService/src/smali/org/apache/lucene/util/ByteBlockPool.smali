.class public final Lorg/apache/lucene/util/ByteBlockPool;
.super Ljava/lang/Object;
.source "ByteBlockPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;,
        Lorg/apache/lucene/util/ByteBlockPool$DirectAllocator;,
        Lorg/apache/lucene/util/ByteBlockPool$Allocator;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final BYTE_BLOCK_MASK:I = 0x7fff

.field public static final BYTE_BLOCK_SHIFT:I = 0xf

.field public static final BYTE_BLOCK_SIZE:I = 0x8000

.field public static final FIRST_LEVEL_SIZE:I

.field public static final levelSizeArray:[I

.field public static final nextLevelArray:[I


# instance fields
.field private final allocator:Lorg/apache/lucene/util/ByteBlockPool$Allocator;

.field public buffer:[B

.field bufferUpto:I

.field public buffers:[[B

.field public byteOffset:I

.field public byteUpto:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    const/4 v1, 0x0

    .line 47
    const-class v0, Lorg/apache/lucene/util/ByteBlockPool;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/ByteBlockPool;->$assertionsDisabled:Z

    .line 198
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/util/ByteBlockPool;->nextLevelArray:[I

    .line 199
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/lucene/util/ByteBlockPool;->levelSizeArray:[I

    .line 200
    sget-object v0, Lorg/apache/lucene/util/ByteBlockPool;->levelSizeArray:[I

    aget v0, v0, v1

    sput v0, Lorg/apache/lucene/util/ByteBlockPool;->FIRST_LEVEL_SIZE:I

    return-void

    :cond_0
    move v0, v1

    .line 47
    goto :goto_0

    .line 198
    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0x9
    .end array-data

    .line 199
    :array_1
    .array-data 4
        0x5
        0xe
        0x14
        0x1e
        0x28
        0x28
        0x50
        0x50
        0x78
        0xc8
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/lucene/util/ByteBlockPool$Allocator;)V
    .locals 1
    .param p1, "allocator"    # Lorg/apache/lucene/util/ByteBlockPool$Allocator;

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    const/16 v0, 0xa

    new-array v0, v0, [[B

    iput-object v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    .line 120
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    .line 121
    const v0, 0x8000

    iput v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 124
    const/16 v0, -0x8000

    iput v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    .line 129
    iput-object p1, p0, Lorg/apache/lucene/util/ByteBlockPool;->allocator:Lorg/apache/lucene/util/ByteBlockPool$Allocator;

    .line 130
    return-void
.end method


# virtual methods
.method public allocSlice([BI)I
    .locals 8
    .param p1, "slice"    # [B
    .param p2, "upto"    # I

    .prologue
    .line 204
    aget-byte v5, p1, p2

    and-int/lit8 v0, v5, 0xf

    .line 205
    .local v0, "level":I
    sget-object v5, Lorg/apache/lucene/util/ByteBlockPool;->nextLevelArray:[I

    aget v1, v5, v0

    .line 206
    .local v1, "newLevel":I
    sget-object v5, Lorg/apache/lucene/util/ByteBlockPool;->levelSizeArray:[I

    aget v2, v5, v1

    .line 209
    .local v2, "newSize":I
    iget v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    const v6, 0x8000

    sub-int/2addr v6, v2

    if-le v5, v6, :cond_0

    .line 210
    invoke-virtual {p0}, Lorg/apache/lucene/util/ByteBlockPool;->nextBuffer()V

    .line 212
    :cond_0
    iget v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 213
    .local v3, "newUpto":I
    iget v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    add-int v4, v3, v5

    .line 214
    .local v4, "offset":I
    iget v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/2addr v5, v2

    iput v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 218
    iget-object v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    add-int/lit8 v6, p2, -0x3

    aget-byte v6, p1, v6

    aput-byte v6, v5, v3

    .line 219
    iget-object v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    add-int/lit8 v6, v3, 0x1

    add-int/lit8 v7, p2, -0x2

    aget-byte v7, p1, v7

    aput-byte v7, v5, v6

    .line 220
    iget-object v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    add-int/lit8 v6, v3, 0x2

    add-int/lit8 v7, p2, -0x1

    aget-byte v7, p1, v7

    aput-byte v7, v5, v6

    .line 223
    add-int/lit8 v5, p2, -0x3

    ushr-int/lit8 v6, v4, 0x18

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 224
    add-int/lit8 v5, p2, -0x2

    ushr-int/lit8 v6, v4, 0x10

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 225
    add-int/lit8 v5, p2, -0x1

    ushr-int/lit8 v6, v4, 0x8

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 226
    int-to-byte v5, v4

    aput-byte v5, p1, p2

    .line 229
    iget-object v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    iget v6, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/lit8 v6, v6, -0x1

    or-int/lit8 v7, v1, 0x10

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 231
    add-int/lit8 v5, v3, 0x3

    return v5
.end method

.method public final copy(Lorg/apache/lucene/util/BytesRef;)V
    .locals 7
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 257
    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 258
    .local v1, "length":I
    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 259
    .local v2, "offset":I
    iget v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/2addr v4, v1

    add-int/lit16 v3, v4, -0x8000

    .line 261
    .local v3, "overflow":I
    :goto_0
    if-gtz v3, :cond_0

    .line 262
    iget-object v4, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    iget v6, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    invoke-static {v4, v2, v5, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 263
    iget v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/2addr v4, v1

    iput v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 274
    return-void

    .line 266
    :cond_0
    sub-int v0, v1, v3

    .line 267
    .local v0, "bytesToCopy":I
    iget-object v4, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    iget v6, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    invoke-static {v4, v2, v5, v6, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 268
    add-int/2addr v2, v0

    .line 269
    sub-int/2addr v1, v0

    .line 270
    invoke-virtual {p0}, Lorg/apache/lucene/util/ByteBlockPool;->nextBuffer()V

    .line 271
    add-int/lit16 v3, v3, -0x8000

    .line 273
    goto :goto_0
.end method

.method public final copyFrom(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;
    .locals 11
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v10, 0x0

    .line 280
    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 281
    .local v3, "length":I
    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 282
    .local v4, "offset":I
    iput v10, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 283
    invoke-virtual {p1, v3}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 284
    shr-int/lit8 v1, v4, 0xf

    .line 285
    .local v1, "bufferIndex":I
    iget-object v7, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    aget-object v0, v7, v1

    .line 286
    .local v0, "buffer":[B
    and-int/lit16 v6, v4, 0x7fff

    .line 287
    .local v6, "pos":I
    add-int v7, v6, v3

    add-int/lit16 v5, v7, -0x8000

    .line 289
    .local v5, "overflow":I
    :goto_0
    if-gtz v5, :cond_0

    .line 290
    iget-object v7, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v8, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v9, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v0, v6, v7, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 291
    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 292
    iput v10, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 304
    return-object p1

    .line 295
    :cond_0
    sub-int v2, v3, v5

    .line 296
    .local v2, "bytesToCopy":I
    iget-object v7, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v8, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    invoke-static {v0, v6, v7, v8, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 297
    const/4 v6, 0x0

    .line 298
    iget v7, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v7, v2

    iput v7, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 299
    iget v7, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v7, v2

    iput v7, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 300
    iget-object v7, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v7, v1

    .line 301
    add-int/lit16 v5, v5, -0x8000

    .line 303
    goto :goto_0
.end method

.method public dropBuffersAndReset()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 133
    iget v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    if-eq v0, v4, :cond_0

    .line 135
    iget-object v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->allocator:Lorg/apache/lucene/util/ByteBlockPool$Allocator;

    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/util/ByteBlockPool$Allocator;->recycleByteBlocks([[BII)V

    .line 138
    iput v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    .line 139
    const v0, 0x8000

    iput v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 140
    const/16 v0, -0x8000

    iput v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    .line 141
    const/16 v0, 0xa

    new-array v0, v0, [[B

    iput-object v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    .line 144
    :cond_0
    return-void
.end method

.method public newSlice(I)I
    .locals 4
    .param p1, "size"    # I

    .prologue
    .line 184
    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    const v2, 0x8000

    sub-int/2addr v2, p1

    if-le v1, v2, :cond_0

    .line 185
    invoke-virtual {p0}, Lorg/apache/lucene/util/ByteBlockPool;->nextBuffer()V

    .line 186
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 187
    .local v0, "upto":I
    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/2addr v1, p1

    iput v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 188
    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x10

    aput-byte v3, v1, v2

    .line 189
    return v0
.end method

.method public nextBuffer()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 170
    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 171
    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    sget v2, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [[B

    .line 173
    .local v0, "newBuffers":[[B
    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    iget-object v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    array-length v2, v2

    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 174
    iput-object v0, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    .line 176
    .end local v0    # "newBuffers":[[B
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    iget v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->allocator:Lorg/apache/lucene/util/ByteBlockPool$Allocator;

    invoke-virtual {v3}, Lorg/apache/lucene/util/ByteBlockPool$Allocator;->getByteBlock()[B

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    .line 177
    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    .line 179
    iput v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 180
    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    const v2, 0x8000

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    .line 181
    return-void
.end method

.method public reset()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 147
    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 150
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    if-ge v0, v1, :cond_0

    .line 152
    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    aget-object v1, v1, v0

    invoke-static {v1, v5}, Ljava/util/Arrays;->fill([BB)V

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    iget v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    aget-object v1, v1, v2

    iget v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    invoke-static {v1, v5, v2, v5}, Ljava/util/Arrays;->fill([BIIB)V

    .line 157
    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    if-lez v1, :cond_1

    .line 159
    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->allocator:Lorg/apache/lucene/util/ByteBlockPool$Allocator;

    iget-object v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    const/4 v3, 0x1

    iget v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/util/ByteBlockPool$Allocator;->recycleByteBlocks([[BII)V

    .line 162
    :cond_1
    iput v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->bufferUpto:I

    .line 163
    iput v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 164
    iput v5, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    .line 165
    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    aget-object v1, v1, v5

    iput-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    .line 167
    .end local v0    # "i":I
    :cond_2
    return-void
.end method

.method public final setBytesRef(Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/util/BytesRef;
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "textStart"    # I

    .prologue
    .line 237
    iget-object v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    shr-int/lit8 v3, p2, 0xf

    aget-object v0, v2, v3

    iput-object v0, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 238
    .local v0, "bytes":[B
    and-int/lit16 v1, p2, 0x7fff

    .line 239
    .local v1, "pos":I
    aget-byte v2, v0, v1

    and-int/lit16 v2, v2, 0x80

    if-nez v2, :cond_0

    .line 241
    aget-byte v2, v0, v1

    iput v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 242
    add-int/lit8 v2, v1, 0x1

    iput v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 248
    :goto_0
    sget-boolean v2, Lorg/apache/lucene/util/ByteBlockPool;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-gez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 245
    :cond_0
    aget-byte v2, v0, v1

    and-int/lit8 v2, v2, 0x7f

    add-int/lit8 v3, v1, 0x1

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x7

    add-int/2addr v2, v3

    iput v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 246
    add-int/lit8 v2, v1, 0x2

    iput v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    goto :goto_0

    .line 249
    :cond_1
    return-object p1
.end method

.method public final writePool(Lorg/apache/lucene/store/DataOutput;)V
    .locals 5
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 311
    iget v2, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    .line 312
    .local v2, "bytesOffset":I
    const/4 v0, 0x0

    .local v0, "block":I
    move v1, v0

    .line 313
    .end local v0    # "block":I
    .local v1, "block":I
    :goto_0
    if-lez v2, :cond_0

    .line 314
    iget-object v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "block":I
    .restart local v0    # "block":I
    aget-object v3, v3, v1

    const v4, 0x8000

    invoke-virtual {p1, v3, v4}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BI)V

    .line 315
    add-int/lit16 v2, v2, -0x8000

    move v1, v0

    .end local v0    # "block":I
    .restart local v1    # "block":I
    goto :goto_0

    .line 317
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    aget-object v3, v3, v1

    iget v4, p0, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    invoke-virtual {p1, v3, v4}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BI)V

    .line 318
    return-void
.end method
