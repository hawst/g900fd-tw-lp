.class public Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;
.super Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;
.source "BytesRefHash.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/BytesRefHash;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TrackingDirectBytesStartArray"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private bytesStart:[I

.field protected final bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

.field protected final initSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 540
    const-class v0, Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ILjava/util/concurrent/atomic/AtomicLong;)V
    .locals 0
    .param p1, "initSize"    # I
    .param p2, "bytesUsed"    # Ljava/util/concurrent/atomic/AtomicLong;

    .prologue
    .line 545
    invoke-direct {p0}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;-><init>()V

    .line 546
    iput p1, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->initSize:I

    .line 547
    iput-object p2, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    .line 548
    return-void
.end method


# virtual methods
.method public bytesUsed()Ljava/util/concurrent/atomic/AtomicLong;
    .locals 1

    .prologue
    .line 577
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    return-object v0
.end method

.method public clear()[I
    .locals 4

    .prologue
    .line 552
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesStart:[I

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesStart:[I

    array-length v1, v1

    neg-int v1, v1

    mul-int/lit8 v1, v1, 0x4

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 555
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesStart:[I

    return-object v0
.end method

.method public grow()[I
    .locals 4

    .prologue
    .line 560
    sget-boolean v1, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesStart:[I

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 561
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesStart:[I

    array-length v0, v1

    .line 562
    .local v0, "oldSize":I
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesStart:[I

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesStart:[I

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesStart:[I

    .line 563
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesStart:[I

    array-length v2, v2

    sub-int/2addr v2, v0

    mul-int/lit8 v2, v2, 0x4

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 564
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesStart:[I

    return-object v1
.end method

.method public init()[I
    .locals 4

    .prologue
    .line 569
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->initSize:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesStart:[I

    .line 571
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesStart:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 572
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;->bytesStart:[I

    return-object v0
.end method
