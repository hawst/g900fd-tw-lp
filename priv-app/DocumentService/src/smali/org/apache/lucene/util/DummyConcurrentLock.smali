.class public final Lorg/apache/lucene/util/DummyConcurrentLock;
.super Ljava/lang/Object;
.source "DummyConcurrentLock.java"

# interfaces
.implements Ljava/util/concurrent/locks/Lock;


# static fields
.field public static final INSTANCE:Lorg/apache/lucene/util/DummyConcurrentLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lorg/apache/lucene/util/DummyConcurrentLock;

    invoke-direct {v0}, Lorg/apache/lucene/util/DummyConcurrentLock;-><init>()V

    sput-object v0, Lorg/apache/lucene/util/DummyConcurrentLock;->INSTANCE:Lorg/apache/lucene/util/DummyConcurrentLock;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public lock()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method public lockInterruptibly()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method public newCondition()Ljava/util/concurrent/locks/Condition;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public tryLock()Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    return v0
.end method

.method public tryLock(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1
    .param p1, "time"    # J
    .param p3, "unit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method

.method public unlock()V
    .locals 0

    .prologue
    .line 46
    return-void
.end method
