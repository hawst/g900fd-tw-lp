.class public final Lorg/apache/lucene/util/fst/FST;
.super Ljava/lang/Object;
.source "FST.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/fst/FST$NodeQueue;,
        Lorg/apache/lucene/util/fst/FST$NodeAndInCount;,
        Lorg/apache/lucene/util/fst/FST$ArcAndState;,
        Lorg/apache/lucene/util/fst/FST$ForwardBytesReader;,
        Lorg/apache/lucene/util/fst/FST$ReverseBytesReader;,
        Lorg/apache/lucene/util/fst/FST$BytesReader;,
        Lorg/apache/lucene/util/fst/FST$BytesWriter;,
        Lorg/apache/lucene/util/fst/FST$Arc;,
        Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ARCS_AS_FIXED_ARRAY:B = 0x20t

.field static final BIT_ARC_HAS_FINAL_OUTPUT:I = 0x20

.field static final BIT_ARC_HAS_OUTPUT:I = 0x10

.field static final BIT_FINAL_ARC:I = 0x1

.field static final BIT_LAST_ARC:I = 0x2

.field static final BIT_STOP_NODE:I = 0x8

.field private static final BIT_TARGET_DELTA:I = 0x40

.field static final BIT_TARGET_NEXT:I = 0x4

.field public static final END_LABEL:I = -0x1

.field private static final FILE_FORMAT_NAME:Ljava/lang/String; = "FST"

.field private static final FINAL_END_NODE:I = -0x1

.field static final FIXED_ARRAY_NUM_ARCS_DEEP:I = 0xa

.field static final FIXED_ARRAY_NUM_ARCS_SHALLOW:I = 0x5

.field static final FIXED_ARRAY_SHALLOW_DISTANCE:I = 0x3

.field private static final NON_FINAL_END_NODE:I = 0x0

.field private static final VERSION_CURRENT:I = 0x3

.field private static final VERSION_INT_NUM_BYTES_PER_ARC:I = 0x1

.field private static final VERSION_PACKED:I = 0x3

.field private static final VERSION_SHORT_BYTE2_LABELS:I = 0x2

.field private static final VERSION_START:I


# instance fields
.field private final NO_OUTPUT:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private allowArrayArcs:Z

.field public arcCount:I

.field public arcWithOutputCount:I

.field byteUpto:I

.field bytes:[B

.field private bytesPerArc:[I

.field private cachedRootArcs:[Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation
.end field

.field emptyOutput:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private emptyOutputBytes:[B

.field private inCounts:[I

.field public final inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

.field private lastFrozenNode:I

.field private nodeAddress:[I

.field public nodeCount:I

.field private final nodeRefToAddress:[I

.field public final outputs:Lorg/apache/lucene/util/fst/Outputs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final packed:Z

.field private startNode:I

.field private final writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>.BytesWriter;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-class v0, Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/fst/Outputs;)V
    .locals 9
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/DataInput;",
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p2, "outputs":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    new-array v4, v6, [I

    iput-object v4, p0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    .line 141
    iput v6, p0, Lorg/apache/lucene/util/fst/FST;->byteUpto:I

    .line 143
    const/4 v4, -0x1

    iput v4, p0, Lorg/apache/lucene/util/fst/FST;->startNode:I

    .line 161
    iput-boolean v5, p0, Lorg/apache/lucene/util/fst/FST;->allowArrayArcs:Z

    .line 286
    iput-object p2, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    .line 287
    iput-object v7, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    .line 290
    const-string/jumbo v4, "FST"

    invoke-static {p1, v4, v8, v8}, Lorg/apache/lucene/util/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 291
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v4

    if-ne v4, v5, :cond_0

    move v4, v5

    :goto_0
    iput-boolean v4, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    .line 292
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v4

    if-ne v4, v5, :cond_2

    .line 294
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v2

    .line 296
    .local v2, "numBytes":I
    new-array v4, v2, [B

    iput-object v4, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    .line 297
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    invoke-virtual {p1, v4, v6, v2}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 298
    iget-boolean v4, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v4, :cond_1

    .line 299
    invoke-virtual {p0, v6}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v4

    invoke-virtual {p2, v4}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    .line 306
    .end local v2    # "numBytes":I
    :goto_1
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v3

    .line 307
    .local v3, "t":B
    packed-switch v3, :pswitch_data_0

    .line 318
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "invalid input type "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .end local v3    # "t":B
    :cond_0
    move v4, v6

    .line 291
    goto :goto_0

    .line 301
    .restart local v2    # "numBytes":I
    :cond_1
    add-int/lit8 v4, v2, -0x1

    invoke-virtual {p0, v4}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v4

    invoke-virtual {p2, v4}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    goto :goto_1

    .line 304
    .end local v2    # "numBytes":I
    :cond_2
    iput-object v7, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    goto :goto_1

    .line 309
    .restart local v3    # "t":B
    :pswitch_0
    sget-object v4, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    iput-object v4, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    .line 320
    :goto_2
    iget-boolean v4, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v4, :cond_3

    .line 321
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v1

    .line 322
    .local v1, "nodeRefCount":I
    new-array v4, v1, [I

    iput-object v4, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:[I

    .line 323
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_3
    if-ge v0, v1, :cond_4

    .line 324
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:[I

    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v5

    aput v5, v4, v0

    .line 323
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 312
    .end local v0    # "idx":I
    .end local v1    # "nodeRefCount":I
    :pswitch_1
    sget-object v4, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE2:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    iput-object v4, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    goto :goto_2

    .line 315
    :pswitch_2
    sget-object v4, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE4:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    iput-object v4, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    goto :goto_2

    .line 327
    :cond_3
    iput-object v7, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:[I

    .line 329
    :cond_4
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/util/fst/FST;->startNode:I

    .line 330
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    .line 331
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/util/fst/FST;->arcCount:I

    .line 332
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:I

    .line 334
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v4

    new-array v4, v4, [B

    iput-object v4, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    .line 335
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    array-length v5, v5

    invoke-virtual {p1, v4, v6, v5}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 336
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    .line 338
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FST;->cacheRootArcs()V

    .line 339
    return-void

    .line 307
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method constructor <init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;Lorg/apache/lucene/util/fst/Outputs;Z)V
    .locals 4
    .param p1, "inputType"    # Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;
    .param p3, "willPackFST"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;",
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TT;>;Z)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p2, "outputs":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    const/16 v3, 0x8

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    new-array v0, v2, [I

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    .line 141
    iput v2, p0, Lorg/apache/lucene/util/fst/FST;->byteUpto:I

    .line 143
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/fst/FST;->startNode:I

    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/util/fst/FST;->allowArrayArcs:Z

    .line 265
    iput-object p1, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    .line 266
    iput-object p2, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    .line 267
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    .line 268
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    .line 269
    if-eqz p3, :cond_0

    .line 270
    new-array v0, v3, [I

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    .line 271
    new-array v0, v3, [I

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->inCounts:[I

    .line 277
    :goto_0
    new-instance v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/fst/FST$BytesWriter;-><init>(Lorg/apache/lucene/util/fst/FST;)V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    .line 279
    iput-object v1, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    .line 280
    iput-boolean v2, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    .line 281
    iput-object v1, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:[I

    .line 282
    return-void

    .line 273
    :cond_0
    iput-object v1, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    .line 274
    iput-object v1, p0, Lorg/apache/lucene/util/fst/FST;->inCounts:[I

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;[ILorg/apache/lucene/util/fst/Outputs;)V
    .locals 3
    .param p1, "inputType"    # Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;
    .param p2, "nodeRefToAddress"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;",
            "[I",
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p3, "outputs":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    .line 141
    iput v1, p0, Lorg/apache/lucene/util/fst/FST;->byteUpto:I

    .line 143
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/fst/FST;->startNode:I

    .line 161
    iput-boolean v2, p0, Lorg/apache/lucene/util/fst/FST;->allowArrayArcs:Z

    .line 1423
    iput-boolean v2, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    .line 1424
    iput-object p1, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    .line 1425
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    .line 1426
    iput-object p2, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:[I

    .line 1427
    iput-object p3, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    .line 1428
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    .line 1429
    new-instance v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/fst/FST$BytesWriter;-><init>(Lorg/apache/lucene/util/fst/FST;)V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    .line 1430
    return-void
.end method

.method static synthetic access$000(II)Z
    .locals 1
    .param p0, "x0"    # I
    .param p1, "x1"    # I

    .prologue
    .line 69
    invoke-static {p0, p1}, Lorg/apache/lucene/util/fst/FST;->flag(II)Z

    move-result v0

    return v0
.end method

.method private cacheRootArcs()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 385
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/16 v2, 0x80

    new-array v2, v2, [Lorg/apache/lucene/util/fst/FST$Arc;

    check-cast v2, [Lorg/apache/lucene/util/fst/FST$Arc;

    iput-object v2, p0, Lorg/apache/lucene/util/fst/FST;->cachedRootArcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    .line 386
    new-instance v0, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    .line 387
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 388
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    .line 389
    .local v1, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    invoke-static {v0}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 390
    iget v2, v0, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    invoke-virtual {p0, v2, v0, v1}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 392
    :goto_0
    sget-boolean v2, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget v2, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 393
    :cond_0
    iget v2, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    iget-object v3, p0, Lorg/apache/lucene/util/fst/FST;->cachedRootArcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 394
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->cachedRootArcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    iget v3, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    new-instance v4, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v4}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    invoke-virtual {v4, v0}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v4

    aput-object v4, v2, v3

    .line 398
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 404
    :cond_1
    return-void

    .line 401
    :cond_2
    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_0
.end method

.method private static final flag(II)Z
    .locals 1
    .param p0, "flags"    # I
    .param p1, "bit"    # I

    .prologue
    .line 248
    and-int v0, p0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getNodeAddress(I)I
    .locals 1
    .param p1, "node"    # I

    .prologue
    .line 373
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    aget p1, v0, p1

    .line 378
    .end local p1    # "node":I
    :cond_0
    return p1
.end method

.method public static read(Ljava/io/File;Lorg/apache/lucene/util/fst/Outputs;)Lorg/apache/lucene/util/fst/FST;
    .locals 6
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/File;",
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TT;>;)",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "outputs":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TT;>;"
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 506
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 507
    .local v1, "is":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 509
    .local v2, "success":Z
    :try_start_0
    new-instance v0, Lorg/apache/lucene/util/fst/FST;

    new-instance v3, Lorg/apache/lucene/store/InputStreamDataInput;

    invoke-direct {v3, v1}, Lorg/apache/lucene/store/InputStreamDataInput;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v3, p1}, Lorg/apache/lucene/util/fst/FST;-><init>(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/fst/Outputs;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 510
    .local v0, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/4 v2, 0x1

    .line 513
    if-eqz v2, :cond_1

    .line 514
    new-array v3, v4, [Ljava/io/Closeable;

    aput-object v1, v3, v5

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 511
    :goto_0
    return-object v0

    .line 513
    .end local v0    # "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_0

    .line 514
    new-array v4, v4, [Ljava/io/Closeable;

    aput-object v1, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 513
    :goto_1
    throw v3

    .line 516
    :cond_0
    new-array v4, v4, [Ljava/io/Closeable;

    aput-object v1, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1

    .restart local v0    # "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    :cond_1
    new-array v3, v4, [Ljava/io/Closeable;

    aput-object v1, v3, v5

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0
.end method

.method private seekToNextNode(Lorg/apache/lucene/util/fst/FST$BytesReader;)V
    .locals 2
    .param p1, "in"    # Lorg/apache/lucene/util/fst/FST$BytesReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1125
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v0

    .line 1126
    .local v0, "flags":I
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    .line 1128
    const/16 v1, 0x10

    invoke-static {v0, v1}, Lorg/apache/lucene/util/fst/FST;->flag(II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1129
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    .line 1132
    :cond_1
    const/16 v1, 0x20

    invoke-static {v0, v1}, Lorg/apache/lucene/util/fst/FST;->flag(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1133
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    .line 1136
    :cond_2
    const/16 v1, 0x8

    invoke-static {v0, v1}, Lorg/apache/lucene/util/fst/FST;->flag(II)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lorg/apache/lucene/util/fst/FST;->flag(II)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1137
    iget-boolean v1, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v1, :cond_4

    .line 1138
    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    .line 1144
    :cond_3
    :goto_0
    const/4 v1, 0x2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/fst/FST;->flag(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1145
    return-void

    .line 1140
    :cond_4
    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readInt()I

    goto :goto_0
.end method

.method private shouldExpand(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 1183
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "node":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    iget-boolean v0, p0, Lorg/apache/lucene/util/fst/FST;->allowArrayArcs:Z

    if-eqz v0, :cond_2

    iget v0, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->depth:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    iget v0, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    :cond_0
    iget v0, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    const/16 v1, 0xa

    if-lt v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 552
    .local p0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private writeLabel(I)V
    .locals 3
    .param p1, "v"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 522
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    sget-boolean v0, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "v="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 523
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v1, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    if-ne v0, v1, :cond_2

    .line 524
    sget-boolean v0, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    const/16 v0, 0xff

    if-le p1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "v="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 525
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeByte(B)V

    .line 533
    :goto_0
    return-void

    .line 526
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v1, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE2:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    if-ne v0, v1, :cond_4

    .line 527
    sget-boolean v0, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    const v0, 0xffff

    if-le p1, v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "v="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 528
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    int-to-short v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeShort(S)V

    goto :goto_0

    .line 531
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeVInt(I)V

    goto :goto_0
.end method


# virtual methods
.method addNode(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)I
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode",
            "<TT;>;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 559
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "nodeIn":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    move/from16 v25, v0

    if-nez v25, :cond_1

    .line 560
    move-object/from16 v0, p1

    iget-boolean v0, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->isFinal:Z

    move/from16 v25, v0

    if-eqz v25, :cond_0

    .line 561
    const/16 v17, -0x1

    .line 728
    :goto_0
    return v17

    .line 563
    :cond_0
    const/16 v17, 0x0

    goto :goto_0

    .line 567
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    move/from16 v22, v0

    .line 570
    .local v22, "startAddress":I
    invoke-direct/range {p0 .. p1}, Lorg/apache/lucene/util/fst/FST;->shouldExpand(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)Z

    move-result v8

    .line 572
    .local v8, "doFixedArray":Z
    if-eqz v8, :cond_a

    .line 573
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_2

    .line 574
    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    move/from16 v25, v0

    const/16 v26, 0x1

    invoke-static/range {v25 .. v26}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v25

    move/from16 v0, v25

    new-array v0, v0, [I

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    .line 577
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v25, v0

    const/16 v26, 0x20

    invoke-virtual/range {v25 .. v26}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeByte(B)V

    .line 578
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v25, v0

    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeVInt(I)V

    .line 582
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeInt(I)V

    .line 583
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v10, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    .line 589
    .local v10, "fixedArrayStart":I
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->arcCount:I

    move/from16 v25, v0

    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    move/from16 v26, v0

    add-int v25, v25, v26

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/util/fst/FST;->arcCount:I

    .line 591
    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    move/from16 v25, v0

    add-int/lit8 v12, v25, -0x1

    .line 593
    .local v12, "lastArc":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v13, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    .line 594
    .local v13, "lastArcStart":I
    const/16 v16, 0x0

    .line 595
    .local v16, "maxBytesPerArc":I
    const/4 v5, 0x0

    .local v5, "arcIdx":I
    :goto_2
    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    move/from16 v25, v0

    move/from16 v0, v25

    if-ge v5, v0, :cond_11

    .line 596
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    move-object/from16 v25, v0

    aget-object v4, v25, v5

    .line 597
    .local v4, "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    iget-object v0, v4, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    move-object/from16 v23, v0

    check-cast v23, Lorg/apache/lucene/util/fst/Builder$CompiledNode;

    .line 598
    .local v23, "target":Lorg/apache/lucene/util/fst/Builder$CompiledNode;
    const/4 v11, 0x0

    .line 600
    .local v11, "flags":I
    if-ne v5, v12, :cond_3

    .line 601
    add-int/lit8 v11, v11, 0x2

    .line 604
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->lastFrozenNode:I

    move/from16 v25, v0

    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_4

    if-nez v8, :cond_4

    .line 608
    add-int/lit8 v11, v11, 0x4

    .line 611
    :cond_4
    iget-boolean v0, v4, Lorg/apache/lucene/util/fst/Builder$Arc;->isFinal:Z

    move/from16 v25, v0

    if-eqz v25, :cond_b

    .line 612
    add-int/lit8 v11, v11, 0x1

    .line 613
    iget-object v0, v4, Lorg/apache/lucene/util/fst/Builder$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_5

    .line 614
    add-int/lit8 v11, v11, 0x20

    .line 620
    :cond_5
    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:I

    move/from16 v25, v0

    if-lez v25, :cond_c

    const/16 v24, 0x1

    .line 622
    .local v24, "targetHasArcs":Z
    :goto_3
    if-nez v24, :cond_d

    .line 623
    add-int/lit8 v11, v11, 0x8

    .line 628
    :cond_6
    :goto_4
    iget-object v0, v4, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_7

    .line 629
    add-int/lit8 v11, v11, 0x10

    .line 632
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v25, v0

    int-to-byte v0, v11

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeByte(B)V

    .line 633
    iget v0, v4, Lorg/apache/lucene/util/fst/Builder$Arc;->label:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/fst/FST;->writeLabel(I)V

    .line 637
    iget-object v0, v4, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_8

    .line 638
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v25, v0

    iget-object v0, v4, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v27, v0

    invoke-virtual/range {v25 .. v27}, Lorg/apache/lucene/util/fst/Outputs;->write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V

    .line 640
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:I

    .line 643
    :cond_8
    iget-object v0, v4, Lorg/apache/lucene/util/fst/Builder$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_9

    .line 645
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v25, v0

    iget-object v0, v4, Lorg/apache/lucene/util/fst/Builder$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v27, v0

    invoke-virtual/range {v25 .. v27}, Lorg/apache/lucene/util/fst/Outputs;->write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V

    .line 648
    :cond_9
    if-eqz v24, :cond_f

    and-int/lit8 v25, v11, 0x4

    if-nez v25, :cond_f

    .line 649
    sget-boolean v25, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v25, :cond_e

    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:I

    move/from16 v25, v0

    if-gtz v25, :cond_e

    new-instance v25, Ljava/lang/AssertionError;

    invoke-direct/range {v25 .. v25}, Ljava/lang/AssertionError;-><init>()V

    throw v25

    .line 586
    .end local v4    # "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    .end local v5    # "arcIdx":I
    .end local v10    # "fixedArrayStart":I
    .end local v11    # "flags":I
    .end local v12    # "lastArc":I
    .end local v13    # "lastArcStart":I
    .end local v16    # "maxBytesPerArc":I
    .end local v23    # "target":Lorg/apache/lucene/util/fst/Builder$CompiledNode;
    .end local v24    # "targetHasArcs":Z
    :cond_a
    const/4 v10, 0x0

    .restart local v10    # "fixedArrayStart":I
    goto/16 :goto_1

    .line 617
    .restart local v4    # "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    .restart local v5    # "arcIdx":I
    .restart local v11    # "flags":I
    .restart local v12    # "lastArc":I
    .restart local v13    # "lastArcStart":I
    .restart local v16    # "maxBytesPerArc":I
    .restart local v23    # "target":Lorg/apache/lucene/util/fst/Builder$CompiledNode;
    :cond_b
    sget-boolean v25, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v25, :cond_5

    iget-object v0, v4, Lorg/apache/lucene/util/fst/Builder$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_5

    new-instance v25, Ljava/lang/AssertionError;

    invoke-direct/range {v25 .. v25}, Ljava/lang/AssertionError;-><init>()V

    throw v25

    .line 620
    :cond_c
    const/16 v24, 0x0

    goto/16 :goto_3

    .line 624
    .restart local v24    # "targetHasArcs":Z
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:[I

    move-object/from16 v25, v0

    if-eqz v25, :cond_6

    .line 625
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:[I

    move-object/from16 v25, v0

    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:I

    move/from16 v26, v0

    aget v27, v25, v26

    add-int/lit8 v27, v27, 0x1

    aput v27, v25, v26

    goto/16 :goto_4

    .line 651
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v25, v0

    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeInt(I)V

    .line 657
    :cond_f
    if-eqz v8, :cond_10

    .line 658
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    move/from16 v26, v0

    sub-int v26, v26, v13

    aput v26, v25, v5

    .line 659
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v13, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    .line 660
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    move-object/from16 v25, v0

    aget v25, v25, v5

    move/from16 v0, v16

    move/from16 v1, v25

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 595
    :cond_10
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 670
    .end local v4    # "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    .end local v11    # "flags":I
    .end local v23    # "target":Lorg/apache/lucene/util/fst/Builder$CompiledNode;
    .end local v24    # "targetHasArcs":Z
    :cond_11
    if-eqz v8, :cond_15

    .line 672
    sget-boolean v25, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v25, :cond_12

    if-gtz v16, :cond_12

    new-instance v25, Ljava/lang/AssertionError;

    invoke-direct/range {v25 .. v25}, Ljava/lang/AssertionError;-><init>()V

    throw v25

    .line 675
    :cond_12
    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    move/from16 v25, v0

    mul-int v25, v25, v16

    add-int v20, v10, v25

    .line 676
    .local v20, "sizeNeeded":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, v20

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    .line 678
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    move-object/from16 v25, v0

    add-int/lit8 v26, v10, -0x4

    shr-int/lit8 v27, v16, 0x18

    move/from16 v0, v27

    int-to-byte v0, v0

    move/from16 v27, v0

    aput-byte v27, v25, v26

    .line 679
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    move-object/from16 v25, v0

    add-int/lit8 v26, v10, -0x3

    shr-int/lit8 v27, v16, 0x10

    move/from16 v0, v27

    int-to-byte v0, v0

    move/from16 v27, v0

    aput-byte v27, v25, v26

    .line 680
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    move-object/from16 v25, v0

    add-int/lit8 v26, v10, -0x2

    shr-int/lit8 v27, v16, 0x8

    move/from16 v0, v27

    int-to-byte v0, v0

    move/from16 v27, v0

    aput-byte v27, v25, v26

    .line 681
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    move-object/from16 v25, v0

    add-int/lit8 v26, v10, -0x1

    move/from16 v0, v16

    int-to-byte v0, v0

    move/from16 v27, v0

    aput-byte v27, v25, v26

    .line 684
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    move/from16 v21, v0

    .line 685
    .local v21, "srcPos":I
    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    move/from16 v25, v0

    mul-int v25, v25, v16

    add-int v7, v10, v25

    .line 686
    .local v7, "destPos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iput v7, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    .line 687
    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    move/from16 v25, v0

    add-int/lit8 v5, v25, -0x1

    :goto_5
    if-ltz v5, :cond_15

    .line 689
    sub-int v7, v7, v16

    .line 690
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    move-object/from16 v25, v0

    aget v25, v25, v5

    sub-int v21, v21, v25

    .line 691
    move/from16 v0, v21

    if-eq v0, v7, :cond_14

    .line 692
    sget-boolean v25, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v25, :cond_13

    move/from16 v0, v21

    if-gt v7, v0, :cond_13

    new-instance v25, Ljava/lang/AssertionError;

    invoke-direct/range {v25 .. v25}, Ljava/lang/AssertionError;-><init>()V

    throw v25

    .line 693
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytesPerArc:[I

    move-object/from16 v27, v0

    aget v27, v27, v5

    move-object/from16 v0, v25

    move/from16 v1, v21

    move-object/from16 v2, v26

    move/from16 v3, v27

    invoke-static {v0, v1, v2, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 687
    :cond_14
    add-int/lit8 v5, v5, -0x1

    goto :goto_5

    .line 701
    .end local v7    # "destPos":I
    .end local v20    # "sizeNeeded":I
    .end local v21    # "srcPos":I
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    move/from16 v25, v0

    add-int/lit8 v9, v25, -0x1

    .line 703
    .local v9, "endAddress":I
    move/from16 v14, v22

    .line 704
    .local v14, "left":I
    move/from16 v18, v9

    .local v18, "right":I
    move/from16 v19, v18

    .end local v18    # "right":I
    .local v19, "right":I
    move v15, v14

    .line 705
    .end local v14    # "left":I
    .local v15, "left":I
    :goto_6
    move/from16 v0, v19

    if-ge v15, v0, :cond_16

    .line 706
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    move-object/from16 v25, v0

    aget-byte v6, v25, v15

    .line 707
    .local v6, "b":B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    move-object/from16 v25, v0

    add-int/lit8 v14, v15, 0x1

    .end local v15    # "left":I
    .restart local v14    # "left":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    move-object/from16 v26, v0

    aget-byte v26, v26, v19

    aput-byte v26, v25, v15

    .line 708
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    move-object/from16 v25, v0

    add-int/lit8 v18, v19, -0x1

    .end local v19    # "right":I
    .restart local v18    # "right":I
    aput-byte v6, v25, v19

    move/from16 v19, v18

    .end local v18    # "right":I
    .restart local v19    # "right":I
    move v15, v14

    .line 709
    .end local v14    # "left":I
    .restart local v15    # "left":I
    goto :goto_6

    .line 712
    .end local v6    # "b":B
    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    .line 714
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    move-object/from16 v25, v0

    if-eqz v25, :cond_18

    .line 716
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_17

    .line 717
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lorg/apache/lucene/util/ArrayUtil;->grow([I)[I

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    .line 718
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:[I

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lorg/apache/lucene/util/ArrayUtil;->grow([I)[I

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/util/fst/FST;->inCounts:[I

    .line 720
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    move/from16 v26, v0

    aput v9, v25, v26

    .line 722
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    move/from16 v17, v0

    .line 726
    .local v17, "node":I
    :goto_7
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/util/fst/FST;->lastFrozenNode:I

    goto/16 :goto_0

    .line 724
    .end local v17    # "node":I
    :cond_18
    move/from16 v17, v9

    .restart local v17    # "node":I
    goto :goto_7
.end method

.method public findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 9
    .param p1, "labelToMatch"    # I
    .param p4, "in"    # Lorg/apache/lucene/util/fst/FST$BytesReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p2, "follow":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p3, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    const/4 v8, -0x1

    const/4 v5, 0x0

    .line 1027
    sget-boolean v6, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lorg/apache/lucene/util/fst/FST;->cachedRootArcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 1028
    :cond_0
    sget-boolean v6, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    iget-object v6, p4, Lorg/apache/lucene/util/fst/FST$BytesReader;->bytes:[B

    iget-object v7, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    if-eq v6, v7, :cond_1

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 1030
    :cond_1
    if-ne p1, v8, :cond_5

    .line 1031
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1032
    iget v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    if-gtz v6, :cond_3

    .line 1033
    const/4 v6, 0x2

    iput-byte v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 1040
    :goto_0
    iget-object v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    iput-object v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .line 1041
    iput v8, p3, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    .line 1114
    .end local p3    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_2
    :goto_1
    return-object p3

    .line 1035
    .restart local p3    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_3
    const/4 v6, 0x0

    iput-byte v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 1037
    iget v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    iput v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    .line 1038
    iget v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    iput v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->node:I

    goto :goto_0

    :cond_4
    move-object p3, v5

    .line 1044
    goto :goto_1

    .line 1049
    :cond_5
    iget v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    iget v7, p0, Lorg/apache/lucene/util/fst/FST;->startNode:I

    if-ne v6, v7, :cond_7

    iget-object v6, p0, Lorg/apache/lucene/util/fst/FST;->cachedRootArcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v6, v6

    if-ge p1, v6, :cond_7

    .line 1050
    iget-object v6, p0, Lorg/apache/lucene/util/fst/FST;->cachedRootArcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    aget-object v5, v6, p1

    .line 1051
    .local v5, "result":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    if-nez v5, :cond_6

    move-object p3, v5

    .line 1052
    goto :goto_1

    .line 1054
    :cond_6
    invoke-virtual {p3, v5}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_1

    .line 1059
    .end local v5    # "result":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_7
    invoke-static {p2}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v6

    if-nez v6, :cond_8

    move-object p3, v5

    .line 1060
    goto :goto_1

    .line 1063
    :cond_8
    iget v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    invoke-direct {p0, v6}, Lorg/apache/lucene/util/fst/FST;->getNodeAddress(I)I

    move-result v6

    iput v6, p4, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    .line 1065
    iget v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    iput v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->node:I

    .line 1069
    invoke-virtual {p4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v6

    const/16 v7, 0x20

    if-ne v6, v7, :cond_d

    .line 1071
    invoke-virtual {p4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    move-result v6

    iput v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    .line 1072
    iget-boolean v6, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v6, :cond_9

    .line 1073
    invoke-virtual {p4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    move-result v6

    iput v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    .line 1077
    :goto_2
    iget v6, p4, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    iput v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:I

    .line 1078
    const/4 v2, 0x0

    .line 1079
    .local v2, "low":I
    iget v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    add-int/lit8 v1, v6, -0x1

    .line 1080
    .local v1, "high":I
    :goto_3
    if-gt v2, v1, :cond_c

    .line 1082
    add-int v6, v2, v1

    ushr-int/lit8 v3, v6, 0x1

    .line 1083
    .local v3, "mid":I
    iget v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:I

    iget v7, p3, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    mul-int/2addr v7, v3

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {p4, v6, v7}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skip(II)V

    .line 1084
    invoke-virtual {p0, p4}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    move-result v4

    .line 1085
    .local v4, "midLabel":I
    sub-int v0, v4, p1

    .line 1086
    .local v0, "cmp":I
    if-gez v0, :cond_a

    .line 1087
    add-int/lit8 v2, v3, 0x1

    goto :goto_3

    .line 1075
    .end local v0    # "cmp":I
    .end local v1    # "high":I
    .end local v2    # "low":I
    .end local v3    # "mid":I
    .end local v4    # "midLabel":I
    :cond_9
    invoke-virtual {p4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readInt()I

    move-result v6

    iput v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    goto :goto_2

    .line 1088
    .restart local v0    # "cmp":I
    .restart local v1    # "high":I
    .restart local v2    # "low":I
    .restart local v3    # "mid":I
    .restart local v4    # "midLabel":I
    :cond_a
    if-lez v0, :cond_b

    .line 1089
    add-int/lit8 v1, v3, -0x1

    goto :goto_3

    .line 1091
    :cond_b
    add-int/lit8 v6, v3, -0x1

    iput v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 1093
    invoke-virtual {p0, p3, p4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object p3

    goto/16 :goto_1

    .end local v0    # "cmp":I
    .end local v3    # "mid":I
    .end local v4    # "midLabel":I
    :cond_c
    move-object p3, v5

    .line 1097
    goto/16 :goto_1

    .line 1101
    .end local v1    # "high":I
    .end local v2    # "low":I
    :cond_d
    iget v6, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    invoke-virtual {p0, v6, p3, p4}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 1108
    :goto_4
    iget v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-eq v6, p1, :cond_2

    .line 1111
    iget v6, p3, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-le v6, p1, :cond_e

    move-object p3, v5

    .line 1112
    goto/16 :goto_1

    .line 1113
    :cond_e
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v6

    if-eqz v6, :cond_f

    move-object p3, v5

    .line 1114
    goto/16 :goto_1

    .line 1116
    :cond_f
    invoke-virtual {p0, p3, p4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_4
.end method

.method finish(I)V
    .locals 4
    .param p1, "startNode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 358
    if-ne p1, v2, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 359
    const/4 p1, 0x0

    .line 361
    :cond_0
    iget v1, p0, Lorg/apache/lucene/util/fst/FST;->startNode:I

    if-eq v1, v2, :cond_1

    .line 362
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "already finished"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 364
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    iget v1, v1, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    new-array v0, v1, [B

    .line 365
    .local v0, "finalBytes":[B
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    iget v2, v2, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 366
    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    .line 367
    iput p1, p0, Lorg/apache/lucene/util/fst/FST;->startNode:I

    .line 369
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FST;->cacheRootArcs()V

    .line 370
    return-void
.end method

.method public getArcCount()I
    .locals 1

    .prologue
    .line 1156
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/fst/FST;->arcCount:I

    return v0
.end method

.method public getArcWithOutputCount()I
    .locals 1

    .prologue
    .line 1160
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:I

    return v0
.end method

.method public final getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 1226
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget-boolean v0, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v0, :cond_0

    .line 1227
    new-instance v0, Lorg/apache/lucene/util/fst/FST$ForwardBytesReader;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    invoke-direct {v0, v1, p1}, Lorg/apache/lucene/util/fst/FST$ForwardBytesReader;-><init>([BI)V

    .line 1229
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/util/fst/FST$ReverseBytesReader;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    invoke-direct {v0, v1, p1}, Lorg/apache/lucene/util/fst/FST$ReverseBytesReader;-><init>([BI)V

    goto :goto_0
.end method

.method public getEmptyOutput()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 407
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    return-object v0
.end method

.method public getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;)",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 735
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 736
    const/4 v0, 0x3

    iput-byte v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 737
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    iput-object v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    .line 742
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    iput-object v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .line 746
    iget v0, p0, Lorg/apache/lucene/util/fst/FST;->startNode:I

    iput v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    .line 747
    return-object p1

    .line 739
    :cond_0
    const/4 v0, 0x2

    iput-byte v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 740
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    iput-object v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    goto :goto_0
.end method

.method public getInputType()Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;
    .locals 1

    .prologue
    .line 342
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    return-object v0
.end method

.method public getNodeCount()I
    .locals 1

    .prologue
    .line 1152
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method isExpandedTarget(Lorg/apache/lucene/util/fst/FST$Arc;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "follow":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    const/4 v1, 0x0

    .line 883
    invoke-static {p1}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 887
    :cond_0
    :goto_0
    return v1

    .line 886
    :cond_1
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    invoke-direct {p0, v2}, Lorg/apache/lucene/util/fst/FST;->getNodeAddress(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v0

    .line 887
    .local v0, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public pack(II)Lorg/apache/lucene/util/fst/FST;
    .locals 47
    .param p1, "minInCountDeref"    # I
    .param p2, "maxDerefNodes"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1448
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    move-object/from16 v43, v0

    if-nez v43, :cond_0

    .line 1449
    new-instance v43, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v44, "this FST was not built with willPackFST=true"

    invoke-direct/range {v43 .. v44}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v43

    .line 1452
    :cond_0
    new-instance v10, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v10}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    .line 1454
    .local v10, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    const/16 v43, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v36

    .line 1456
    .local v36, "r":Lorg/apache/lucene/util/fst/FST$BytesReader;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:[I

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    array-length v0, v0

    move/from16 v43, v0

    move/from16 v0, p2

    move/from16 v1, v43

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v39

    .line 1459
    .local v39, "topN":I
    new-instance v35, Lorg/apache/lucene/util/fst/FST$NodeQueue;

    move-object/from16 v0, v35

    move/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/fst/FST$NodeQueue;-><init>(I)V

    .line 1462
    .local v35, "q":Lorg/apache/lucene/util/fst/FST$NodeQueue;
    const/4 v13, 0x0

    .line 1463
    .local v13, "bottom":Lorg/apache/lucene/util/fst/FST$NodeAndInCount;
    const/16 v31, 0x0

    .local v31, "node":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:[I

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    array-length v0, v0

    move/from16 v43, v0

    move/from16 v0, v31

    move/from16 v1, v43

    if-ge v0, v1, :cond_3

    .line 1464
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:[I

    move-object/from16 v43, v0

    aget v43, v43, v31

    move/from16 v0, v43

    move/from16 v1, p1

    if-lt v0, v1, :cond_1

    .line 1465
    if-nez v13, :cond_2

    .line 1466
    new-instance v43, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:[I

    move-object/from16 v44, v0

    aget v44, v44, v31

    move-object/from16 v0, v43

    move/from16 v1, v31

    move/from16 v2, v44

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;-><init>(II)V

    move-object/from16 v0, v35

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1467
    invoke-virtual/range {v35 .. v35}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->size()I

    move-result v43

    move/from16 v0, v43

    move/from16 v1, v39

    if-ne v0, v1, :cond_1

    .line 1468
    invoke-virtual/range {v35 .. v35}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->top()Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "bottom":Lorg/apache/lucene/util/fst/FST$NodeAndInCount;
    check-cast v13, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;

    .line 1463
    .restart local v13    # "bottom":Lorg/apache/lucene/util/fst/FST$NodeAndInCount;
    :cond_1
    :goto_1
    add-int/lit8 v31, v31, 0x1

    goto :goto_0

    .line 1470
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:[I

    move-object/from16 v43, v0

    aget v43, v43, v31

    iget v0, v13, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->count:I

    move/from16 v44, v0

    move/from16 v0, v43

    move/from16 v1, v44

    if-le v0, v1, :cond_1

    .line 1471
    new-instance v43, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inCounts:[I

    move-object/from16 v44, v0

    aget v44, v44, v31

    move-object/from16 v0, v43

    move/from16 v1, v31

    move/from16 v2, v44

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;-><init>(II)V

    move-object/from16 v0, v35

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1477
    :cond_3
    const/16 v43, 0x0

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/util/fst/FST;->inCounts:[I

    .line 1479
    new-instance v40, Ljava/util/HashMap;

    invoke-direct/range {v40 .. v40}, Ljava/util/HashMap;-><init>()V

    .line 1480
    .local v40, "topNodeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-virtual/range {v35 .. v35}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->size()I

    move-result v43

    add-int/lit8 v20, v43, -0x1

    .local v20, "downTo":I
    :goto_2
    if-ltz v20, :cond_4

    .line 1481
    invoke-virtual/range {v35 .. v35}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->pop()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;

    .line 1482
    .local v27, "n":Lorg/apache/lucene/util/fst/FST$NodeAndInCount;
    move-object/from16 v0, v27

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->node:I

    move/from16 v43, v0

    invoke-static/range {v43 .. v43}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v43

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v44

    move-object/from16 v0, v40

    move-object/from16 v1, v43

    move-object/from16 v2, v44

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1480
    add-int/lit8 v20, v20, -0x1

    goto :goto_2

    .line 1489
    .end local v27    # "n":Lorg/apache/lucene/util/fst/FST$NodeAndInCount;
    :cond_4
    invoke-interface/range {v40 .. v40}, Ljava/util/Map;->size()I

    move-result v43

    move/from16 v0, v43

    new-array v0, v0, [I

    move-object/from16 v33, v0

    .line 1491
    .local v33, "nodeRefToAddressIn":[I
    new-instance v24, Lorg/apache/lucene/util/fst/FST;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v44, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v43

    move-object/from16 v2, v33

    move-object/from16 v3, v44

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/fst/FST;-><init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;[ILorg/apache/lucene/util/fst/Outputs;)V

    .line 1493
    .local v24, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    move-object/from16 v0, v24

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    move-object/from16 v42, v0

    .line 1495
    .local v42, "writer":Lorg/apache/lucene/util/fst/FST$BytesWriter;, "Lorg/apache/lucene/util/fst/FST<TT;>.BytesWriter;"
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    move/from16 v43, v0

    add-int/lit8 v43, v43, 0x1

    move/from16 v0, v43

    new-array v0, v0, [I

    move-object/from16 v29, v0

    .line 1498
    .local v29, "newNodeAddress":[I
    const/16 v31, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    move/from16 v43, v0

    move/from16 v0, v31

    move/from16 v1, v43

    if-gt v0, v1, :cond_5

    .line 1499
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    move-object/from16 v43, v0

    move-object/from16 v0, v43

    array-length v0, v0

    move/from16 v43, v0

    add-int/lit8 v43, v43, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    move-object/from16 v44, v0

    aget v44, v44, v31

    sub-int v43, v43, v44

    aput v43, v29, v31

    .line 1498
    add-int/lit8 v31, v31, 0x1

    goto :goto_3

    .line 1511
    :cond_5
    const/4 v15, 0x0

    .line 1514
    .local v15, "changed":Z
    const/16 v28, 0x0

    .line 1516
    .local v28, "negDelta":Z
    const/16 v43, 0x0

    move/from16 v0, v43

    move-object/from16 v1, v42

    iput v0, v1, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    .line 1518
    const/16 v43, 0x0

    invoke-virtual/range {v42 .. v43}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeByte(B)V

    .line 1520
    const/16 v43, 0x0

    move/from16 v0, v43

    move-object/from16 v1, v24

    iput v0, v1, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:I

    .line 1521
    const/16 v43, 0x0

    move/from16 v0, v43

    move-object/from16 v1, v24

    iput v0, v1, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    .line 1522
    const/16 v43, 0x0

    move/from16 v0, v43

    move-object/from16 v1, v24

    iput v0, v1, Lorg/apache/lucene/util/fst/FST;->arcCount:I

    .line 1524
    const/16 v30, 0x0

    .local v30, "nextCount":I
    move/from16 v38, v30

    .local v38, "topCount":I
    move/from16 v18, v30

    .local v18, "deltaCount":I
    move/from16 v5, v30

    .line 1526
    .local v5, "absCount":I
    const/16 v16, 0x0

    .line 1528
    .local v16, "changedCount":I
    const/4 v8, 0x0

    .line 1535
    .local v8, "addressError":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    move/from16 v31, v0

    :goto_4
    const/16 v43, 0x1

    move/from16 v0, v31

    move/from16 v1, v43

    if-lt v0, v1, :cond_1f

    .line 1536
    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    move/from16 v43, v0

    add-int/lit8 v43, v43, 0x1

    move/from16 v0, v43

    move-object/from16 v1, v24

    iput v0, v1, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    .line 1537
    move-object/from16 v0, v42

    iget v7, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    .line 1539
    .local v7, "address":I
    aget v43, v29, v31

    move/from16 v0, v43

    if-eq v7, v0, :cond_6

    .line 1540
    aget v43, v29, v31

    sub-int v8, v7, v43

    .line 1542
    const/4 v15, 0x1

    .line 1543
    aput v7, v29, v31

    .line 1544
    add-int/lit8 v16, v16, 0x1

    .line 1547
    :cond_6
    const/16 v32, 0x0

    .line 1548
    .local v32, "nodeArcCount":I
    const/4 v14, 0x0

    .line 1550
    .local v14, "bytesPerArc":I
    const/16 v37, 0x0

    .line 1553
    .local v37, "retry":Z
    const/4 v9, 0x0

    .line 1560
    .local v9, "anyNegDelta":Z
    :goto_5
    move-object/from16 v0, p0

    move/from16 v1, v31

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v10, v2}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 1562
    iget v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    move/from16 v43, v0

    if-eqz v43, :cond_16

    const/16 v41, 0x1

    .line 1563
    .local v41, "useArcArray":Z
    :goto_6
    if-eqz v41, :cond_8

    .line 1565
    if-nez v14, :cond_7

    .line 1566
    iget v14, v10, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    .line 1568
    :cond_7
    const/16 v43, 0x20

    invoke-virtual/range {v42 .. v43}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeByte(B)V

    .line 1569
    iget v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    move/from16 v43, v0

    invoke-virtual/range {v42 .. v43}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeVInt(I)V

    .line 1570
    move-object/from16 v0, v42

    invoke-virtual {v0, v14}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeVInt(I)V

    .line 1574
    :cond_8
    const/16 v26, 0x0

    .line 1579
    .local v26, "maxBytesPerArc":I
    :goto_7
    move-object/from16 v0, v42

    iget v12, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    .line 1580
    .local v12, "arcStartPos":I
    add-int/lit8 v32, v32, 0x1

    .line 1582
    const/16 v23, 0x0

    .line 1584
    .local v23, "flags":B
    invoke-virtual {v10}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v43

    if-eqz v43, :cond_9

    .line 1585
    const/16 v43, 0x2

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v23, v0

    .line 1592
    :cond_9
    if-nez v41, :cond_a

    const/16 v43, 0x1

    move/from16 v0, v31

    move/from16 v1, v43

    if-eq v0, v1, :cond_a

    iget v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v43, v0

    add-int/lit8 v44, v31, -0x1

    move/from16 v0, v43

    move/from16 v1, v44

    if-ne v0, v1, :cond_a

    .line 1593
    add-int/lit8 v43, v23, 0x4

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v23, v0

    .line 1594
    if-nez v37, :cond_a

    .line 1595
    add-int/lit8 v30, v30, 0x1

    .line 1598
    :cond_a
    invoke-virtual {v10}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v43

    if-eqz v43, :cond_17

    .line 1599
    add-int/lit8 v43, v23, 0x1

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v23, v0

    .line 1600
    iget-object v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v44, v0

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_b

    .line 1601
    add-int/lit8 v43, v23, 0x20

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v23, v0

    .line 1606
    :cond_b
    invoke-static {v10}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v43

    if-nez v43, :cond_c

    .line 1607
    add-int/lit8 v43, v23, 0x8

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v23, v0

    .line 1610
    :cond_c
    iget-object v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v44, v0

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_d

    .line 1611
    add-int/lit8 v43, v23, 0x10

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v23, v0

    .line 1616
    :cond_d
    invoke-static {v10}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v43

    if-eqz v43, :cond_18

    and-int/lit8 v43, v23, 0x4

    if-nez v43, :cond_18

    const/16 v19, 0x1

    .line 1617
    .local v19, "doWriteTarget":Z
    :goto_8
    if-eqz v19, :cond_1a

    .line 1619
    iget v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v43, v0

    invoke-static/range {v43 .. v43}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v43

    move-object/from16 v0, v40

    move-object/from16 v1, v43

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/Integer;

    .line 1620
    .local v34, "ptr":Ljava/lang/Integer;
    if-eqz v34, :cond_19

    .line 1621
    invoke-virtual/range {v34 .. v34}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 1626
    .local v6, "absPtr":I
    :goto_9
    iget v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v43, v0

    aget v43, v29, v43

    add-int v43, v43, v8

    move-object/from16 v0, v42

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    move/from16 v44, v0

    sub-int v43, v43, v44

    add-int/lit8 v17, v43, -0x2

    .line 1627
    .local v17, "delta":I
    if-gez v17, :cond_e

    .line 1629
    const/4 v9, 0x1

    .line 1630
    const/16 v17, 0x0

    .line 1633
    :cond_e
    move/from16 v0, v17

    if-ge v0, v6, :cond_f

    .line 1634
    or-int/lit8 v43, v23, 0x40

    move/from16 v0, v43

    int-to-byte v0, v0

    move/from16 v23, v0

    .line 1641
    .end local v17    # "delta":I
    :cond_f
    :goto_a
    move-object/from16 v0, v42

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeByte(B)V

    .line 1642
    iget v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move/from16 v43, v0

    move-object/from16 v0, v24

    move/from16 v1, v43

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/fst/FST;->writeLabel(I)V

    .line 1644
    iget-object v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v44, v0

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_10

    .line 1645
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v43, v0

    iget-object v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v44, v0

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    move-object/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/Outputs;->write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V

    .line 1646
    if-nez v37, :cond_10

    .line 1647
    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:I

    move/from16 v43, v0

    add-int/lit8 v43, v43, 0x1

    move/from16 v0, v43

    move-object/from16 v1, v24

    iput v0, v1, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:I

    .line 1650
    :cond_10
    iget-object v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v44, v0

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_11

    .line 1651
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v43, v0

    iget-object v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v44, v0

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    move-object/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/Outputs;->write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V

    .line 1654
    :cond_11
    if-eqz v19, :cond_13

    .line 1656
    iget v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v43, v0

    aget v43, v29, v43

    add-int v43, v43, v8

    move-object/from16 v0, v42

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    move/from16 v44, v0

    sub-int v17, v43, v44

    .line 1657
    .restart local v17    # "delta":I
    if-gez v17, :cond_12

    .line 1658
    const/4 v9, 0x1

    .line 1660
    const/16 v17, 0x0

    .line 1663
    :cond_12
    const/16 v43, 0x40

    move/from16 v0, v23

    move/from16 v1, v43

    invoke-static {v0, v1}, Lorg/apache/lucene/util/fst/FST;->flag(II)Z

    move-result v43

    if-eqz v43, :cond_1b

    .line 1665
    move-object/from16 v0, v42

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeVInt(I)V

    .line 1666
    if-nez v37, :cond_13

    .line 1667
    add-int/lit8 v18, v18, 0x1

    .line 1688
    .end local v17    # "delta":I
    :cond_13
    :goto_b
    if-eqz v41, :cond_14

    .line 1689
    move-object/from16 v0, v42

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    move/from16 v43, v0

    sub-int v11, v43, v12

    .line 1691
    .local v11, "arcBytes":I
    move/from16 v0, v26

    invoke-static {v0, v11}, Ljava/lang/Math;->max(II)I

    move-result v26

    .line 1699
    add-int v43, v12, v14

    invoke-virtual/range {v42 .. v43}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->setPosWrite(I)V

    .line 1702
    .end local v11    # "arcBytes":I
    :cond_14
    invoke-virtual {v10}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v43

    if-eqz v43, :cond_1d

    .line 1709
    if-eqz v41, :cond_15

    .line 1710
    move/from16 v0, v26

    if-eq v0, v14, :cond_15

    if-eqz v37, :cond_1e

    move/from16 v0, v26

    if-gt v0, v14, :cond_1e

    .line 1729
    :cond_15
    or-int v28, v28, v9

    .line 1731
    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->arcCount:I

    move/from16 v43, v0

    add-int v43, v43, v32

    move/from16 v0, v43

    move-object/from16 v1, v24

    iput v0, v1, Lorg/apache/lucene/util/fst/FST;->arcCount:I

    .line 1535
    add-int/lit8 v31, v31, -0x1

    goto/16 :goto_4

    .line 1562
    .end local v6    # "absPtr":I
    .end local v12    # "arcStartPos":I
    .end local v19    # "doWriteTarget":Z
    .end local v23    # "flags":B
    .end local v26    # "maxBytesPerArc":I
    .end local v34    # "ptr":Ljava/lang/Integer;
    .end local v41    # "useArcArray":Z
    :cond_16
    const/16 v41, 0x0

    goto/16 :goto_6

    .line 1604
    .restart local v12    # "arcStartPos":I
    .restart local v23    # "flags":B
    .restart local v26    # "maxBytesPerArc":I
    .restart local v41    # "useArcArray":Z
    :cond_17
    sget-boolean v43, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v43, :cond_b

    iget-object v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->NO_OUTPUT:Ljava/lang/Object;

    move-object/from16 v44, v0

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    if-eq v0, v1, :cond_b

    new-instance v43, Ljava/lang/AssertionError;

    invoke-direct/range {v43 .. v43}, Ljava/lang/AssertionError;-><init>()V

    throw v43

    .line 1616
    :cond_18
    const/16 v19, 0x0

    goto/16 :goto_8

    .line 1623
    .restart local v19    # "doWriteTarget":Z
    .restart local v34    # "ptr":Ljava/lang/Integer;
    :cond_19
    invoke-interface/range {v40 .. v40}, Ljava/util/Map;->size()I

    move-result v43

    iget v0, v10, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v44, v0

    aget v44, v29, v44

    add-int v43, v43, v44

    add-int v6, v43, v8

    .restart local v6    # "absPtr":I
    goto/16 :goto_9

    .line 1637
    .end local v6    # "absPtr":I
    .end local v34    # "ptr":Ljava/lang/Integer;
    :cond_1a
    const/16 v34, 0x0

    .line 1638
    .restart local v34    # "ptr":Ljava/lang/Integer;
    const/4 v6, 0x0

    .restart local v6    # "absPtr":I
    goto/16 :goto_a

    .line 1677
    .restart local v17    # "delta":I
    :cond_1b
    move-object/from16 v0, v42

    invoke-virtual {v0, v6}, Lorg/apache/lucene/util/fst/FST$BytesWriter;->writeVInt(I)V

    .line 1678
    if-nez v37, :cond_13

    .line 1679
    invoke-interface/range {v40 .. v40}, Ljava/util/Map;->size()I

    move-result v43

    move/from16 v0, v43

    if-lt v6, v0, :cond_1c

    .line 1680
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_b

    .line 1682
    :cond_1c
    add-int/lit8 v38, v38, 0x1

    goto/16 :goto_b

    .line 1706
    .end local v17    # "delta":I
    :cond_1d
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-virtual {v0, v10, v1}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto/16 :goto_7

    .line 1723
    :cond_1e
    move/from16 v14, v26

    .line 1724
    move-object/from16 v0, v42

    iput v7, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    .line 1725
    const/16 v32, 0x0

    .line 1726
    const/16 v37, 0x1

    .line 1727
    const/4 v9, 0x0

    .line 1728
    goto/16 :goto_5

    .line 1734
    .end local v6    # "absPtr":I
    .end local v7    # "address":I
    .end local v9    # "anyNegDelta":Z
    .end local v12    # "arcStartPos":I
    .end local v14    # "bytesPerArc":I
    .end local v19    # "doWriteTarget":Z
    .end local v23    # "flags":B
    .end local v26    # "maxBytesPerArc":I
    .end local v32    # "nodeArcCount":I
    .end local v34    # "ptr":Ljava/lang/Integer;
    .end local v37    # "retry":Z
    .end local v41    # "useArcArray":Z
    :cond_1f
    if-nez v15, :cond_5

    .line 1739
    sget-boolean v43, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v43, :cond_20

    if-eqz v28, :cond_20

    new-instance v43, Ljava/lang/AssertionError;

    invoke-direct/range {v43 .. v43}, Ljava/lang/AssertionError;-><init>()V

    throw v43

    .line 1747
    :cond_20
    invoke-interface/range {v40 .. v40}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v43

    invoke-interface/range {v43 .. v43}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v25

    .local v25, "i$":Ljava/util/Iterator;
    :goto_c
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v43

    if-eqz v43, :cond_21

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/util/Map$Entry;

    .line 1748
    .local v21, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface/range {v21 .. v21}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Ljava/lang/Integer;

    invoke-virtual/range {v43 .. v43}, Ljava/lang/Integer;->intValue()I

    move-result v44

    invoke-interface/range {v21 .. v21}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Ljava/lang/Integer;

    invoke-virtual/range {v43 .. v43}, Ljava/lang/Integer;->intValue()I

    move-result v43

    aget v43, v29, v43

    aput v43, v33, v44

    goto :goto_c

    .line 1751
    .end local v21    # "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_21
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->startNode:I

    move/from16 v43, v0

    aget v43, v29, v43

    move/from16 v0, v43

    move-object/from16 v1, v24

    iput v0, v1, Lorg/apache/lucene/util/fst/FST;->startNode:I

    .line 1754
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    move-object/from16 v43, v0

    if-eqz v43, :cond_22

    .line 1755
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    move-object/from16 v43, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST;->setEmptyOutput(Ljava/lang/Object;)V

    .line 1758
    :cond_22
    sget-boolean v43, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v43, :cond_23

    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    move/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    move/from16 v44, v0

    move/from16 v0, v43

    move/from16 v1, v44

    if-eq v0, v1, :cond_23

    new-instance v43, Ljava/lang/AssertionError;

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v45, "fst.nodeCount="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    move/from16 v45, v0

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " nodeCount="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    move/from16 v45, v0

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    invoke-direct/range {v43 .. v44}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v43

    .line 1759
    :cond_23
    sget-boolean v43, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v43, :cond_24

    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->arcCount:I

    move/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->arcCount:I

    move/from16 v44, v0

    move/from16 v0, v43

    move/from16 v1, v44

    if-eq v0, v1, :cond_24

    new-instance v43, Ljava/lang/AssertionError;

    invoke-direct/range {v43 .. v43}, Ljava/lang/AssertionError;-><init>()V

    throw v43

    .line 1760
    :cond_24
    sget-boolean v43, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v43, :cond_25

    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:I

    move/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:I

    move/from16 v44, v0

    move/from16 v0, v43

    move/from16 v1, v44

    if-eq v0, v1, :cond_25

    new-instance v43, Ljava/lang/AssertionError;

    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v45, "fst.arcWithOutputCount="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:I

    move/from16 v45, v0

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " arcWithOutputCount="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:I

    move/from16 v45, v0

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    invoke-direct/range {v43 .. v44}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v43

    .line 1762
    :cond_25
    move-object/from16 v0, v42

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    move/from16 v43, v0

    move/from16 v0, v43

    new-array v0, v0, [B

    move-object/from16 v22, v0

    .line 1764
    .local v22, "finalBytes":[B
    move-object/from16 v0, v24

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    move-object/from16 v43, v0

    const/16 v44, 0x0

    const/16 v45, 0x0

    move-object/from16 v0, v42

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    move/from16 v46, v0

    move-object/from16 v0, v43

    move/from16 v1, v44

    move-object/from16 v2, v22

    move/from16 v3, v45

    move/from16 v4, v46

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1765
    move-object/from16 v0, v22

    move-object/from16 v1, v24

    iput-object v0, v1, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    .line 1766
    invoke-direct/range {v24 .. v24}, Lorg/apache/lucene/util/fst/FST;->cacheRootArcs()V

    .line 1771
    return-object v24
.end method

.method public readFirstRealTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 3
    .param p1, "node"    # I
    .param p3, "in"    # Lorg/apache/lucene/util/fst/FST$BytesReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 847
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p2, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    sget-boolean v1, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p3, Lorg/apache/lucene/util/fst/FST$BytesReader;->bytes:[B

    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 848
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/FST;->getNodeAddress(I)I

    move-result v0

    .line 849
    .local v0, "address":I
    iput v0, p3, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    .line 853
    iput p1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->node:I

    .line 855
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_2

    .line 858
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    move-result v1

    iput v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    .line 859
    iget-boolean v1, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v1, :cond_1

    .line 860
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    move-result v1

    iput v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    .line 864
    :goto_0
    const/4 v1, -0x1

    iput v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 865
    iget v1, p3, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    iput v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:I

    iput v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    .line 873
    :goto_1
    invoke-virtual {p0, p2, p3}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v1

    return-object v1

    .line 862
    :cond_1
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readInt()I

    move-result v1

    iput v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    goto :goto_0

    .line 869
    :cond_2
    iput v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    .line 870
    const/4 v1, 0x0

    iput v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    goto :goto_1
.end method

.method public readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;)",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "follow":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p2, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    const/4 v1, -0x1

    .line 826
    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 828
    iput v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    .line 829
    iget-object v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    iput-object v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .line 830
    const/4 v0, 0x1

    iput-byte v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 831
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    if-gtz v0, :cond_0

    .line 832
    iget-byte v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    or-int/lit8 v0, v0, 0x2

    int-to-byte v0, v0

    iput-byte v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 838
    :goto_0
    iput v1, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    .line 842
    .end local p2    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :goto_1
    return-object p2

    .line 834
    .restart local p2    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_0
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    iput v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->node:I

    .line 836
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    iput v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    goto :goto_0

    .line 842
    :cond_1
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    invoke-virtual {p0, v0, p2, v1}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object p2

    goto :goto_1
.end method

.method readLabel(Lorg/apache/lucene/store/DataInput;)I
    .locals 3
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 537
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v2, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    if-ne v1, v2, :cond_0

    .line 539
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v1

    and-int/lit16 v0, v1, 0xff

    .line 546
    .local v0, "v":I
    :goto_0
    return v0

    .line 540
    .end local v0    # "v":I
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v2, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE2:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    if-ne v1, v2, :cond_1

    .line 542
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readShort()S

    move-result v1

    const v2, 0xffff

    and-int v0, v1, v2

    .restart local v0    # "v":I
    goto :goto_0

    .line 544
    .end local v0    # "v":I
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v0

    .restart local v0    # "v":I
    goto :goto_0
.end method

.method public readLastTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;)",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "follow":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p2, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    const/16 v5, 0x20

    const/4 v4, 0x4

    const/4 v3, -0x1

    .line 758
    invoke-static {p1}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 760
    sget-boolean v2, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 761
    :cond_0
    iput v3, p2, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    .line 762
    iput v3, p2, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    .line 763
    iget-object v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    iput-object v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .line 764
    const/4 v2, 0x2

    iput-byte v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 812
    :cond_1
    return-object p2

    .line 767
    :cond_2
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    invoke-direct {p0, v2}, Lorg/apache/lucene/util/fst/FST;->getNodeAddress(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    .line 768
    .local v1, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    iput v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->node:I

    .line 769
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v0

    .line 770
    .local v0, "b":B
    if-ne v0, v5, :cond_4

    .line 772
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    move-result v2

    iput v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    .line 773
    iget-boolean v2, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v2, :cond_3

    .line 774
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    move-result v2

    iput v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    .line 779
    :goto_0
    iget v2, v1, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    iput v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:I

    .line 780
    iget v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    add-int/lit8 v2, v2, -0x2

    iput v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 810
    :goto_1
    invoke-virtual {p0, p2, v1}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 811
    sget-boolean v2, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 776
    :cond_3
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readInt()I

    move-result v2

    iput v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    goto :goto_0

    .line 782
    :cond_4
    iput-byte v0, p2, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 784
    const/4 v2, 0x0

    iput v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    .line 786
    :goto_2
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v2

    if-nez v2, :cond_a

    .line 788
    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    .line 789
    const/16 v2, 0x10

    invoke-virtual {p2, v2}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 790
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    .line 792
    :cond_5
    invoke-virtual {p2, v5}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 793
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    .line 795
    :cond_6
    const/16 v2, 0x8

    invoke-virtual {p2, v2}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 804
    :cond_7
    :goto_3
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v2

    iput-byte v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    goto :goto_2

    .line 796
    :cond_8
    invoke-virtual {p2, v4}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v2

    if-nez v2, :cond_7

    .line 798
    iget-boolean v2, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v2, :cond_9

    .line 799
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    goto :goto_3

    .line 801
    :cond_9
    invoke-virtual {v1, v4}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skip(I)V

    goto :goto_3

    .line 807
    :cond_a
    invoke-virtual {v1, v3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skip(I)V

    .line 808
    iget v2, v1, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    iput v2, p2, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    goto :goto_1
.end method

.method public readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;)",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    const/4 v2, 0x0

    .line 893
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 895
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    if-gtz v0, :cond_0

    .line 896
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "cannot readNextArc when arc.isLast()=true"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898
    :cond_0
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    invoke-virtual {p0, v0, p1, v1}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 900
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    goto :goto_0
.end method

.method public readNextArcLabel(Lorg/apache/lucene/util/fst/FST$Arc;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 907
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    sget-boolean v2, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 910
    :cond_0
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    .line 912
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    invoke-direct {p0, v2}, Lorg/apache/lucene/util/fst/FST;->getNodeAddress(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    .line 913
    .local v1, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    iget v3, v1, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    aget-byte v0, v2, v3

    .line 914
    .local v0, "b":B
    const/16 v2, 0x20

    if-ne v0, v2, :cond_1

    .line 916
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skip(I)V

    .line 917
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    .line 918
    iget-boolean v2, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v2, :cond_2

    .line 919
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    .line 937
    .end local v0    # "b":B
    :cond_1
    :goto_0
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    .line 938
    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    move-result v2

    return v2

    .line 921
    .restart local v0    # "b":B
    :cond_2
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readInt()I

    goto :goto_0

    .line 925
    .end local v0    # "b":B
    .end local v1    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    :cond_3
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v2, :cond_4

    .line 928
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:I

    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    .line 929
    .restart local v1    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p1, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    mul-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skip(I)V

    goto :goto_0

    .line 933
    .end local v1    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    :cond_4
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    .restart local v1    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    goto :goto_0
.end method

.method public readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 5
    .param p2, "in"    # Lorg/apache/lucene/util/fst/FST$BytesReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 944
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    sget-boolean v2, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-object v2, p2, Lorg/apache/lucene/util/fst/FST$BytesReader;->bytes:[B

    iget-object v3, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    if-eq v2, v3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 950
    :cond_0
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v2, :cond_3

    .line 952
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 953
    sget-boolean v2, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    iget v3, p1, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    if-lt v2, v3, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 954
    :cond_1
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:I

    iget v3, p1, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    iget v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    mul-int/2addr v3, v4

    invoke-virtual {p2, v2, v3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skip(II)V

    .line 959
    :goto_0
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v2

    iput-byte v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 960
    invoke-virtual {p0, p2}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    move-result v2

    iput v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    .line 962
    const/16 v2, 0x10

    invoke-virtual {p1, v2}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 963
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v2, p2}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .line 968
    :goto_1
    const/16 v2, 0x20

    invoke-virtual {p1, v2}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 969
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v2, p2}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    .line 974
    :goto_2
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 975
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 976
    const/4 v2, -0x1

    iput v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    .line 980
    :goto_3
    iget v2, p2, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    iput v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    .line 1021
    :cond_2
    :goto_4
    return-object p1

    .line 957
    :cond_3
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    iput v2, p2, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    goto :goto_0

    .line 965
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    goto :goto_1

    .line 971
    :cond_5
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    goto :goto_2

    .line 978
    :cond_6
    const/4 v2, 0x0

    iput v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    goto :goto_3

    .line 981
    :cond_7
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 982
    iget v2, p2, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    iput v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    .line 985
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    if-nez v2, :cond_a

    .line 986
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v2

    if-nez v2, :cond_8

    .line 987
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-nez v2, :cond_9

    .line 989
    invoke-direct {p0, p2}, Lorg/apache/lucene/util/fst/FST;->seekToNextNode(Lorg/apache/lucene/util/fst/FST$BytesReader;)V

    .line 994
    :cond_8
    :goto_5
    iget v2, p2, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    iput v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    goto :goto_4

    .line 991
    :cond_9
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:I

    iget v3, p1, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    iget v4, p1, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    mul-int/2addr v3, v4

    invoke-virtual {p2, v2, v3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skip(II)V

    goto :goto_5

    .line 996
    :cond_a
    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->node:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    .line 997
    sget-boolean v2, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    if-gtz v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1000
    :cond_b
    iget-boolean v2, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v2, :cond_e

    .line 1001
    iget v1, p2, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    .line 1002
    .local v1, "pos":I
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readVInt()I

    move-result v0

    .line 1003
    .local v0, "code":I
    const/16 v2, 0x40

    invoke-virtual {p1, v2}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1005
    add-int v2, v1, v0

    iput v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    .line 1019
    .end local v0    # "code":I
    .end local v1    # "pos":I
    :goto_6
    iget v2, p2, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    iput v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    goto :goto_4

    .line 1007
    .restart local v0    # "code":I
    .restart local v1    # "pos":I
    :cond_c
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:[I

    array-length v2, v2

    if-ge v0, v2, :cond_d

    .line 1009
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:[I

    aget v2, v2, v0

    iput v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    goto :goto_6

    .line 1013
    :cond_d
    iput v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    goto :goto_6

    .line 1017
    .end local v0    # "code":I
    .end local v1    # "pos":I
    :cond_e
    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readInt()I

    move-result v2

    iput v2, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    goto :goto_6
.end method

.method public save(Ljava/io/File;)V
    .locals 5
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 488
    const/4 v1, 0x0

    .line 489
    .local v1, "success":Z
    new-instance v0, Ljava/io/BufferedOutputStream;

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 491
    .local v0, "os":Ljava/io/OutputStream;
    :try_start_0
    new-instance v2, Lorg/apache/lucene/store/OutputStreamDataOutput;

    invoke-direct {v2, v0}, Lorg/apache/lucene/store/OutputStreamDataOutput;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/fst/FST;->save(Lorg/apache/lucene/store/DataOutput;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 492
    const/4 v1, 0x1

    .line 494
    if-eqz v1, :cond_1

    .line 495
    new-array v2, v3, [Ljava/io/Closeable;

    aput-object v0, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 500
    :goto_0
    return-void

    .line 494
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_0

    .line 495
    new-array v3, v3, [Ljava/io/Closeable;

    aput-object v0, v3, v4

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 494
    :goto_1
    throw v2

    .line 497
    :cond_0
    new-array v3, v3, [Ljava/io/Closeable;

    aput-object v0, v3, v4

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1

    :cond_1
    new-array v2, v3, [Ljava/io/Closeable;

    aput-object v0, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0
.end method

.method public save(Lorg/apache/lucene/store/DataOutput;)V
    .locals 6
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 439
    iget v2, p0, Lorg/apache/lucene/util/fst/FST;->startNode:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 440
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "call finish first"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 442
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    if-eqz v2, :cond_1

    .line 443
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "cannot save an FST pre-packed FST; it must first be packed"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 445
    :cond_1
    const-string/jumbo v2, "FST"

    const/4 v3, 0x3

    invoke-static {p1, v2, v3}, Lorg/apache/lucene/util/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)Lorg/apache/lucene/store/DataOutput;

    .line 446
    iget-boolean v2, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v2, :cond_2

    .line 447
    invoke-virtual {p1, v5}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 453
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    if-eqz v2, :cond_3

    .line 454
    invoke-virtual {p1, v5}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 455
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutputBytes:[B

    array-length v2, v2

    invoke-virtual {p1, v2}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 456
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutputBytes:[B

    iget-object v3, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutputBytes:[B

    array-length v3, v3

    invoke-virtual {p1, v2, v4, v3}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BII)V

    .line 461
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v3, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    if-ne v2, v3, :cond_4

    .line 462
    const/4 v1, 0x0

    .line 468
    .local v1, "t":B
    :goto_2
    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 469
    iget-boolean v2, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v2, :cond_7

    .line 470
    sget-boolean v2, Lorg/apache/lucene/util/fst/FST;->$assertionsDisabled:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:[I

    if-nez v2, :cond_6

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 449
    .end local v1    # "t":B
    :cond_2
    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    goto :goto_0

    .line 458
    :cond_3
    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    goto :goto_1

    .line 463
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v3, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE2:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    if-ne v2, v3, :cond_5

    .line 464
    const/4 v1, 0x1

    .restart local v1    # "t":B
    goto :goto_2

    .line 466
    .end local v1    # "t":B
    :cond_5
    const/4 v1, 0x2

    .restart local v1    # "t":B
    goto :goto_2

    .line 471
    :cond_6
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:[I

    array-length v2, v2

    invoke-virtual {p1, v2}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 472
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_3
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:[I

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 473
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:[I

    aget v2, v2, v0

    invoke-virtual {p1, v2}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 472
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 476
    .end local v0    # "idx":I
    :cond_7
    iget v2, p0, Lorg/apache/lucene/util/fst/FST;->startNode:I

    invoke-virtual {p1, v2}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 477
    iget v2, p0, Lorg/apache/lucene/util/fst/FST;->nodeCount:I

    invoke-virtual {p1, v2}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 478
    iget v2, p0, Lorg/apache/lucene/util/fst/FST;->arcCount:I

    invoke-virtual {p1, v2}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 479
    iget v2, p0, Lorg/apache/lucene/util/fst/FST;->arcWithOutputCount:I

    invoke-virtual {p1, v2}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 480
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    array-length v2, v2

    invoke-virtual {p1, v2}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 481
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    iget-object v3, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    array-length v3, v3

    invoke-virtual {p1, v2, v4, v3}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BII)V

    .line 482
    return-void
.end method

.method public setAllowArrayArcs(Z)V
    .locals 0
    .param p1, "v"    # Z

    .prologue
    .line 1164
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iput-boolean p1, p0, Lorg/apache/lucene/util/fst/FST;->allowArrayArcs:Z

    .line 1165
    return-void
.end method

.method setEmptyOutput(Ljava/lang/Object;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 411
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "v":Ljava/lang/Object;, "TT;"
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    if-eqz v4, :cond_0

    .line 412
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    invoke-virtual {v4, v5, p1}, Lorg/apache/lucene/util/fst/Outputs;->merge(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    .line 419
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    iget v1, v4, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    .line 420
    .local v1, "posSave":I
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    iget-object v6, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/fst/Outputs;->write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V

    .line 421
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    iget v4, v4, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    sub-int/2addr v4, v1

    new-array v4, v4, [B

    iput-object v4, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutputBytes:[B

    .line 423
    iget-boolean v4, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-nez v4, :cond_1

    .line 425
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    iget v4, v4, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    sub-int/2addr v4, v1

    div-int/lit8 v2, v4, 0x2

    .line 426
    .local v2, "stopAt":I
    const/4 v3, 0x0

    .line 427
    .local v3, "upto":I
    :goto_1
    if-ge v3, v2, :cond_1

    .line 428
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    add-int v5, v1, v3

    aget-byte v0, v4, v5

    .line 429
    .local v0, "b":B
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    add-int v5, v1, v3

    iget-object v6, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    iget-object v7, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    iget v7, v7, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    sub-int/2addr v7, v3

    add-int/lit8 v7, v7, -0x1

    aget-byte v6, v6, v7

    aput-byte v6, v4, v5

    .line 430
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    iget v5, v5, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    sub-int/2addr v5, v3

    add-int/lit8 v5, v5, -0x1

    aput-byte v0, v4, v5

    .line 431
    add-int/lit8 v3, v3, 0x1

    .line 432
    goto :goto_1

    .line 414
    .end local v0    # "b":B
    .end local v1    # "posSave":I
    .end local v2    # "stopAt":I
    .end local v3    # "upto":I
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutput:Ljava/lang/Object;

    goto :goto_0

    .line 434
    .restart local v1    # "posSave":I
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->emptyOutputBytes:[B

    const/4 v6, 0x0

    iget-object v7, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    iget v7, v7, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    sub-int/2addr v7, v1

    invoke-static {v4, v1, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 435
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->writer:Lorg/apache/lucene/util/fst/FST$BytesWriter;

    iput v1, v4, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    .line 436
    return-void
.end method

.method public sizeInBytes()I
    .locals 2

    .prologue
    .line 347
    .local p0, "this":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    array-length v0, v1

    .line 348
    .local v0, "size":I
    iget-boolean v1, p0, Lorg/apache/lucene/util/fst/FST;->packed:Z

    if-eqz v1, :cond_1

    .line 349
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->nodeRefToAddress:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 354
    :cond_0
    :goto_0
    return v0

    .line 350
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    if-eqz v1, :cond_0

    .line 351
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->nodeAddress:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 352
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST;->inCounts:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    goto :goto_0
.end method
