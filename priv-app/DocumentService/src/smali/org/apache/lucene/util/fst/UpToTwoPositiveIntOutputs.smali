.class public final Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;
.super Lorg/apache/lucene/util/fst/Outputs;
.source "UpToTwoPositiveIntOutputs.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/fst/Outputs",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NO_OUTPUT:Ljava/lang/Long;

.field private static final singletonNoShare:Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;

.field private static final singletonShare:Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;


# instance fields
.field private final doShare:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 40
    const-class v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    .line 75
    new-instance v0, Ljava/lang/Long;

    const-wide/16 v4, 0x0

    invoke-direct {v0, v4, v5}, Ljava/lang/Long;-><init>(J)V

    sput-object v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    .line 79
    new-instance v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;-><init>(Z)V

    sput-object v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->singletonShare:Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;

    .line 80
    new-instance v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;

    invoke-direct {v0, v2}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;-><init>(Z)V

    sput-object v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->singletonNoShare:Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;

    return-void

    :cond_0
    move v0, v2

    .line 40
    goto :goto_0
.end method

.method private constructor <init>(Z)V
    .locals 0
    .param p1, "doShare"    # Z

    .prologue
    .line 82
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/Outputs;-><init>()V

    .line 83
    iput-boolean p1, p0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->doShare:Z

    .line 84
    return-void
.end method

.method public static getSingleton(Z)Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;
    .locals 1
    .param p0, "doShare"    # Z

    .prologue
    .line 87
    if-eqz p0, :cond_0

    sget-object v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->singletonShare:Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->singletonNoShare:Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;

    goto :goto_0
.end method

.method private valid(Ljava/lang/Long;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Long;

    .prologue
    .line 192
    sget-boolean v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 193
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    instance-of v0, p1, Ljava/lang/Long;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 194
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    sget-object v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    if-eq p1, v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 195
    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method private valid(Ljava/lang/Object;Z)Z
    .locals 1
    .param p1, "_o"    # Ljava/lang/Object;
    .param p2, "allowDouble"    # Z

    .prologue
    .line 200
    if-nez p2, :cond_1

    .line 201
    sget-boolean v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Long;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 202
    :cond_0
    check-cast p1, Ljava/lang/Long;

    .end local p1    # "_o":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->valid(Ljava/lang/Long;)Z

    move-result v0

    .line 206
    :goto_0
    return v0

    .line 203
    .restart local p1    # "_o":Ljava/lang/Object;
    :cond_1
    instance-of v0, p1, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;

    if-eqz v0, :cond_2

    .line 204
    const/4 v0, 0x1

    goto :goto_0

    .line 206
    :cond_2
    check-cast p1, Ljava/lang/Long;

    .end local p1    # "_o":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->valid(Ljava/lang/Long;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "_prefix"    # Ljava/lang/Object;
    .param p2, "_output"    # Ljava/lang/Object;

    .prologue
    .line 140
    sget-boolean v4, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    const/4 v4, 0x0

    invoke-direct {p0, p1, v4}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->valid(Ljava/lang/Object;Z)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 141
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    const/4 v4, 0x1

    invoke-direct {p0, p2, v4}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->valid(Ljava/lang/Object;Z)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    :cond_1
    move-object v1, p1

    .line 142
    check-cast v1, Ljava/lang/Long;

    .line 143
    .local v1, "prefix":Ljava/lang/Long;
    instance-of v4, p2, Ljava/lang/Long;

    if-eqz v4, :cond_4

    move-object v0, p2

    .line 144
    check-cast v0, Ljava/lang/Long;

    .line 145
    .local v0, "output":Ljava/lang/Long;
    sget-object v4, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    if-ne v1, v4, :cond_2

    .line 155
    .end local v0    # "output":Ljava/lang/Long;
    .end local v1    # "prefix":Ljava/lang/Long;
    :goto_0
    return-object v0

    .line 147
    .restart local v0    # "output":Ljava/lang/Long;
    .restart local v1    # "prefix":Ljava/lang/Long;
    :cond_2
    sget-object v4, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    if-ne v0, v4, :cond_3

    move-object v0, v1

    .line 148
    goto :goto_0

    .line 150
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .end local v0    # "output":Ljava/lang/Long;
    :cond_4
    move-object v0, p2

    .line 153
    check-cast v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;

    .line 154
    .local v0, "output":Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 155
    .local v2, "v":J
    new-instance v1, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;

    .end local v1    # "prefix":Ljava/lang/Long;
    iget-wide v4, v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->first:J

    add-long/2addr v4, v2

    iget-wide v6, v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->second:J

    add-long/2addr v6, v2

    invoke-direct {v1, v4, v5, v6, v7}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;-><init>(JJ)V

    move-object v0, v1

    goto :goto_0
.end method

.method public common(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Long;
    .locals 6
    .param p1, "_output1"    # Ljava/lang/Object;
    .param p2, "_output2"    # Ljava/lang/Object;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 104
    sget-boolean v2, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-direct {p0, p1, v3}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->valid(Ljava/lang/Object;Z)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 105
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    invoke-direct {p0, p2, v3}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->valid(Ljava/lang/Object;Z)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_1
    move-object v0, p1

    .line 106
    check-cast v0, Ljava/lang/Long;

    .local v0, "output1":Ljava/lang/Long;
    move-object v1, p2

    .line 107
    check-cast v1, Ljava/lang/Long;

    .line 108
    .local v1, "output2":Ljava/lang/Long;
    sget-object v2, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    if-eq v0, v2, :cond_2

    sget-object v2, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    if-ne v1, v2, :cond_4

    .line 109
    :cond_2
    sget-object v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    .line 117
    .end local v0    # "output1":Ljava/lang/Long;
    :cond_3
    :goto_0
    return-object v0

    .line 110
    .restart local v0    # "output1":Ljava/lang/Long;
    :cond_4
    iget-boolean v2, p0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->doShare:Z

    if-eqz v2, :cond_7

    .line 111
    sget-boolean v2, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v2, :cond_5

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-gtz v2, :cond_5

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 112
    :cond_5
    sget-boolean v2, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v2, :cond_6

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-gtz v2, :cond_6

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 113
    :cond_6
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 114
    :cond_7
    invoke-virtual {v0, v1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 117
    sget-object v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    goto :goto_0
.end method

.method public bridge synthetic common(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->common(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public get(J)Ljava/lang/Long;
    .locals 3
    .param p1, "v"    # J

    .prologue
    .line 91
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 92
    sget-object v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    .line 94
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public get(JJ)Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;
    .locals 1
    .param p1, "first"    # J
    .param p3, "second"    # J

    .prologue
    .line 99
    new-instance v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;

    invoke-direct {v0, p1, p2, p3, p4}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;-><init>(JJ)V

    return-object v0
.end method

.method public getNoOutput()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 212
    sget-object v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    return-object v0
.end method

.method public merge(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "first"    # Ljava/lang/Object;
    .param p2, "second"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 222
    sget-boolean v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1, v1}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->valid(Ljava/lang/Object;Z)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 223
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p2, v1}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->valid(Ljava/lang/Object;Z)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 224
    :cond_1
    new-instance v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;

    check-cast p1, Ljava/lang/Long;

    .end local p1    # "first":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    check-cast p2, Ljava/lang/Long;

    .end local p2    # "second":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;-><init>(JJ)V

    return-object v0
.end method

.method public outputToString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "output"    # Ljava/lang/Object;

    .prologue
    .line 217
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;
    .locals 14
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x1

    .line 174
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v0

    .line 175
    .local v0, "code":J
    const-wide/16 v8, 0x1

    and-long/2addr v8, v0

    cmp-long v8, v8, v12

    if-nez v8, :cond_1

    .line 177
    ushr-long v6, v0, v10

    .line 178
    .local v6, "v":J
    cmp-long v8, v6, v12

    if-nez v8, :cond_0

    .line 179
    sget-object v8, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    .line 187
    .end local v6    # "v":J
    :goto_0
    return-object v8

    .line 181
    .restart local v6    # "v":J
    :cond_0
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    goto :goto_0

    .line 185
    .end local v6    # "v":J
    :cond_1
    ushr-long v2, v0, v10

    .line 186
    .local v2, "first":J
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v4

    .line 187
    .local v4, "second":J
    new-instance v8, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;

    invoke-direct {v8, v2, v3, v4, v5}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;-><init>(JJ)V

    goto :goto_0
.end method

.method public subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Long;
    .locals 6
    .param p1, "_output"    # Ljava/lang/Object;
    .param p2, "_inc"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 123
    sget-boolean v2, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-direct {p0, p1, v3}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->valid(Ljava/lang/Object;Z)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 124
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    invoke-direct {p0, p2, v3}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->valid(Ljava/lang/Object;Z)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_1
    move-object v1, p1

    .line 125
    check-cast v1, Ljava/lang/Long;

    .local v1, "output":Ljava/lang/Long;
    move-object v0, p2

    .line 126
    check-cast v0, Ljava/lang/Long;

    .line 127
    .local v0, "inc":Ljava/lang/Long;
    sget-boolean v2, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 129
    :cond_2
    sget-object v2, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    if-ne v0, v2, :cond_3

    .line 134
    .end local v1    # "output":Ljava/lang/Long;
    :goto_0
    return-object v1

    .line 131
    .restart local v1    # "output":Ljava/lang/Long;
    :cond_3
    invoke-virtual {v1, v0}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 132
    sget-object v1, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->NO_OUTPUT:Ljava/lang/Long;

    goto :goto_0

    .line 134
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0
.end method

.method public bridge synthetic subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V
    .locals 6
    .param p1, "_output"    # Ljava/lang/Object;
    .param p2, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 161
    sget-boolean v1, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0, p1, v4}, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;->valid(Ljava/lang/Object;Z)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 162
    :cond_0
    instance-of v1, p1, Ljava/lang/Long;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 163
    check-cast v0, Ljava/lang/Long;

    .line 164
    .local v0, "output":Ljava/lang/Long;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    shl-long/2addr v2, v4

    invoke-virtual {p2, v2, v3}, Lorg/apache/lucene/store/DataOutput;->writeVLong(J)V

    .line 170
    .end local v0    # "output":Ljava/lang/Long;
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 166
    check-cast v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;

    .line 167
    .local v0, "output":Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;
    iget-wide v2, v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->first:J

    shl-long/2addr v2, v4

    const-wide/16 v4, 0x1

    or-long/2addr v2, v4

    invoke-virtual {p2, v2, v3}, Lorg/apache/lucene/store/DataOutput;->writeVLong(J)V

    .line 168
    iget-wide v2, v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->second:J

    invoke-virtual {p2, v2, v3}, Lorg/apache/lucene/store/DataOutput;->writeVLong(J)V

    goto :goto_0
.end method
