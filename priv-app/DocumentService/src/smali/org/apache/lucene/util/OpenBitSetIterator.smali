.class public Lorg/apache/lucene/util/OpenBitSetIterator;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "OpenBitSetIterator.java"


# static fields
.field protected static final bitlist:[I


# instance fields
.field final arr:[J

.field private curDocId:I

.field private i:I

.field private indexArray:I

.field private word:J

.field private wordShift:I

.field final words:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/16 v0, 0x100

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/util/OpenBitSetIterator;->bitlist:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x21
        0x3
        0x31
        0x32
        0x321
        0x4
        0x41
        0x42
        0x421
        0x43
        0x431
        0x432
        0x4321
        0x5
        0x51
        0x52
        0x521
        0x53
        0x531
        0x532
        0x5321
        0x54
        0x541
        0x542
        0x5421
        0x543
        0x5431
        0x5432
        0x54321
        0x6
        0x61
        0x62
        0x621
        0x63
        0x631
        0x632
        0x6321
        0x64
        0x641
        0x642
        0x6421
        0x643
        0x6431
        0x6432
        0x64321
        0x65
        0x651
        0x652
        0x6521
        0x653
        0x6531
        0x6532
        0x65321
        0x654
        0x6541
        0x6542
        0x65421
        0x6543
        0x65431
        0x65432
        0x654321
        0x7
        0x71
        0x72
        0x721
        0x73
        0x731
        0x732
        0x7321
        0x74
        0x741
        0x742
        0x7421
        0x743
        0x7431
        0x7432
        0x74321
        0x75
        0x751
        0x752
        0x7521
        0x753
        0x7531
        0x7532
        0x75321
        0x754
        0x7541
        0x7542
        0x75421
        0x7543
        0x75431
        0x75432
        0x754321
        0x76
        0x761
        0x762
        0x7621
        0x763
        0x7631
        0x7632
        0x76321
        0x764
        0x7641
        0x7642
        0x76421
        0x7643
        0x76431
        0x76432
        0x764321
        0x765
        0x7651
        0x7652
        0x76521
        0x7653
        0x76531
        0x76532
        0x765321
        0x7654
        0x76541
        0x76542
        0x765421
        0x76543
        0x765431
        0x765432
        0x7654321
        0x8
        0x81
        0x82
        0x821
        0x83
        0x831
        0x832
        0x8321
        0x84
        0x841
        0x842
        0x8421
        0x843
        0x8431
        0x8432
        0x84321
        0x85
        0x851
        0x852
        0x8521
        0x853
        0x8531
        0x8532
        0x85321
        0x854
        0x8541
        0x8542
        0x85421
        0x8543
        0x85431
        0x85432
        0x854321
        0x86
        0x861
        0x862
        0x8621
        0x863
        0x8631
        0x8632
        0x86321
        0x864
        0x8641
        0x8642
        0x86421
        0x8643
        0x86431
        0x86432
        0x864321
        0x865
        0x8651
        0x8652
        0x86521
        0x8653
        0x86531
        0x86532
        0x865321
        0x8654
        0x86541
        0x86542
        0x865421
        0x86543
        0x865431
        0x865432
        0x8654321
        0x87
        0x871
        0x872
        0x8721
        0x873
        0x8731
        0x8732
        0x87321
        0x874
        0x8741
        0x8742
        0x87421
        0x8743
        0x87431
        0x87432
        0x874321
        0x875
        0x8751
        0x8752
        0x87521
        0x8753
        0x87531
        0x87532
        0x875321
        0x8754
        0x87541
        0x87542
        0x875421
        0x87543
        0x875431
        0x875432
        0x8754321
        0x876
        0x8761
        0x8762
        0x87621
        0x8763
        0x87631
        0x87632
        0x876321
        0x8764
        0x87641
        0x87642
        0x876421
        0x87643
        0x876431
        0x876432
        0x8764321
        0x8765
        0x87651
        0x87652
        0x876521
        0x87653
        0x876531
        0x876532
        0x8765321
        0x87654
        0x876541
        0x876542
        0x8765421
        0x876543
        0x8765431
        0x8765432
        -0x789abcdf
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 2
    .param p1, "obs"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 91
    invoke-virtual {p1}, Lorg/apache/lucene/util/OpenBitSet;->getBits()[J

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/lucene/util/OpenBitSet;->getNumWords()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/OpenBitSetIterator;-><init>([JI)V

    .line 92
    return-void
.end method

.method public constructor <init>([JI)V
    .locals 1
    .param p1, "bits"    # [J
    .param p2, "numWords"    # I

    .prologue
    const/4 v0, -0x1

    .line 94
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 84
    iput v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    .line 88
    iput v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->curDocId:I

    .line 95
    iput-object p1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->arr:[J

    .line 96
    iput p2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    .line 97
    return-void
.end method

.method private shift()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 101
    iget-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    long-to-int v0, v0

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    add-int/lit8 v0, v0, 0x20

    iput v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    iget-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    const/16 v2, 0x20

    ushr-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 102
    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    const-wide/32 v2, 0xffff

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    add-int/lit8 v0, v0, 0x10

    iput v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    iget-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    const/16 v2, 0x10

    ushr-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 103
    :cond_1
    iget-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    const-wide/16 v2, 0xff

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    iget v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    iget-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    const/16 v2, 0x8

    ushr-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 104
    :cond_2
    sget-object v0, Lorg/apache/lucene/util/OpenBitSetIterator;->bitlist:[I

    iget-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    long-to-int v1, v2

    and-int/lit16 v1, v1, 0xff

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    .line 105
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 8
    .param p1, "target"    # I

    .prologue
    const v1, 0x7fffffff

    const-wide/16 v6, 0x0

    .line 159
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    .line 160
    shr-int/lit8 v2, p1, 0x6

    iput v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    .line 161
    iget v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    iget v3, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    if-lt v2, v3, :cond_0

    .line 162
    iput-wide v6, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 163
    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->curDocId:I

    .line 185
    :goto_0
    return v1

    .line 165
    :cond_0
    and-int/lit8 v2, p1, 0x3f

    iput v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    .line 166
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->arr:[J

    iget v3, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    aget-wide v2, v2, v3

    iget v4, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    ushr-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 167
    iget-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_2

    .line 168
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    .line 179
    :goto_1
    invoke-direct {p0}, Lorg/apache/lucene/util/OpenBitSetIterator;->shift()V

    .line 181
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    and-int/lit8 v1, v1, 0xf

    iget v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    add-int v0, v1, v2

    .line 182
    .local v0, "bitIndex":I
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    ushr-int/lit8 v1, v1, 0x4

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    .line 185
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    shl-int/lit8 v1, v1, 0x6

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->curDocId:I

    goto :goto_0

    .line 174
    .end local v0    # "bitIndex":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->arr:[J

    iget v3, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    aget-wide v2, v2, v3

    iput-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 170
    :cond_2
    iget-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_3

    .line 171
    iget v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    iget v3, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    if-lt v2, v3, :cond_1

    .line 172
    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->curDocId:I

    goto :goto_0

    .line 176
    :cond_3
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    goto :goto_1
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->curDocId:I

    return v0
.end method

.method public nextDoc()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 131
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    if-nez v1, :cond_3

    .line 132
    iget-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 133
    iget-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    const/16 v1, 0x8

    ushr-long/2addr v2, v1

    iput-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 134
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    add-int/lit8 v1, v1, 0x8

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    .line 137
    :cond_0
    :goto_0
    iget-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 138
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    iget v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    if-lt v1, v2, :cond_1

    .line 139
    const v1, 0x7fffffff

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->curDocId:I

    .line 154
    :goto_1
    return v1

    .line 141
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->arr:[J

    iget v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    aget-wide v2, v1, v2

    iput-wide v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->word:J

    .line 142
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    goto :goto_0

    .line 147
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/util/OpenBitSetIterator;->shift()V

    .line 150
    :cond_3
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    and-int/lit8 v1, v1, 0xf

    iget v2, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->wordShift:I

    add-int v0, v1, v2

    .line 151
    .local v0, "bitIndex":I
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    ushr-int/lit8 v1, v1, 0x4

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->indexArray:I

    .line 154
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->i:I

    shl-int/lit8 v1, v1, 0x6

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSetIterator;->curDocId:I

    goto :goto_1
.end method
