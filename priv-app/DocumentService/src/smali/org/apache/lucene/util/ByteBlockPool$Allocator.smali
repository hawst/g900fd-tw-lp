.class public abstract Lorg/apache/lucene/util/ByteBlockPool$Allocator;
.super Ljava/lang/Object;
.source "ByteBlockPool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/ByteBlockPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Allocator"
.end annotation


# instance fields
.field protected final blockSize:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "blockSize"    # I

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput p1, p0, Lorg/apache/lucene/util/ByteBlockPool$Allocator;->blockSize:I

    .line 59
    return-void
.end method


# virtual methods
.method public getByteBlock()[B
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lorg/apache/lucene/util/ByteBlockPool$Allocator;->blockSize:I

    new-array v0, v0, [B

    return-object v0
.end method

.method public recycleByteBlocks(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "blocks":Ljava/util/List;, "Ljava/util/List<[B>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [[B

    invoke-interface {p1, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[B

    .line 65
    .local v0, "b":[[B
    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/lucene/util/ByteBlockPool$Allocator;->recycleByteBlocks([[BII)V

    .line 66
    return-void
.end method

.method public abstract recycleByteBlocks([[BII)V
.end method
