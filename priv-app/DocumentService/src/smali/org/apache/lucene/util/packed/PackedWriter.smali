.class Lorg/apache/lucene/util/packed/PackedWriter;
.super Lorg/apache/lucene/util/packed/PackedInts$Writer;
.source "PackedWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final masks:[J

.field private pending:J

.field private pendingBitPos:I

.field private written:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/apache/lucene/util/packed/PackedWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/PackedWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataOutput;II)V
    .locals 6
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p2, "valueCount"    # I
    .param p3, "bitsPerValue"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/util/packed/PackedInts$Writer;-><init>(Lorg/apache/lucene/store/DataOutput;II)V

    .line 39
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->written:I

    .line 45
    const/16 v1, 0x40

    iput v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pendingBitPos:I

    .line 46
    add-int/lit8 v1, p3, -0x1

    new-array v1, v1, [J

    iput-object v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->masks:[J

    .line 48
    const-wide/16 v2, 0x1

    .line 49
    .local v2, "v":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    add-int/lit8 v1, p3, -0x1

    if-ge v0, v1, :cond_0

    .line 50
    const-wide/16 v4, 0x2

    mul-long/2addr v2, v4

    .line 51
    iget-object v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->masks:[J

    const-wide/16 v4, 0x1

    sub-long v4, v2, v4

    aput-wide v4, v1, v0

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_0
    return-void
.end method


# virtual methods
.method public add(J)V
    .locals 7
    .param p1, "v"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 61
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->bitsPerValue:I

    invoke-static {v0}, Lorg/apache/lucene/util/packed/PackedInts;->maxValue(I)J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "v="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " maxValue="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/packed/PackedWriter;->bitsPerValue:I

    invoke-static {v2}, Lorg/apache/lucene/util/packed/PackedInts;->maxValue(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 62
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    cmp-long v0, p1, v4

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 66
    :cond_1
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pendingBitPos:I

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->bitsPerValue:I

    if-lt v0, v1, :cond_3

    .line 70
    iget-wide v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pending:J

    iget v2, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pendingBitPos:I

    iget v3, p0, Lorg/apache/lucene/util/packed/PackedWriter;->bitsPerValue:I

    sub-int/2addr v2, v3

    shl-long v2, p1, v2

    or-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pending:J

    .line 71
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pendingBitPos:I

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->bitsPerValue:I

    if-ne v0, v1, :cond_2

    .line 73
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->out:Lorg/apache/lucene/store/DataOutput;

    iget-wide v2, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pending:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/DataOutput;->writeLong(J)V

    .line 74
    iput-wide v4, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pending:J

    .line 75
    const/16 v0, 0x40

    iput v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pendingBitPos:I

    .line 95
    :goto_0
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->written:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->written:I

    .line 96
    return-void

    .line 77
    :cond_2
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pendingBitPos:I

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->bitsPerValue:I

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pendingBitPos:I

    goto :goto_0

    .line 84
    :cond_3
    iget-wide v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pending:J

    iget v2, p0, Lorg/apache/lucene/util/packed/PackedWriter;->bitsPerValue:I

    iget v3, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pendingBitPos:I

    sub-int/2addr v2, v3

    shr-long v2, p1, v2

    iget-object v4, p0, Lorg/apache/lucene/util/packed/PackedWriter;->masks:[J

    iget v5, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pendingBitPos:I

    add-int/lit8 v5, v5, -0x1

    aget-wide v4, v4, v5

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pending:J

    .line 88
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->out:Lorg/apache/lucene/store/DataOutput;

    iget-wide v2, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pending:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/DataOutput;->writeLong(J)V

    .line 91
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->bitsPerValue:I

    rsub-int/lit8 v0, v0, 0x40

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pendingBitPos:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pendingBitPos:I

    .line 93
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pendingBitPos:I

    shl-long v0, p1, v0

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pending:J

    goto :goto_0
.end method

.method public finish()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    :goto_0
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->written:I

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->valueCount:I

    if-ge v0, v1, :cond_0

    .line 101
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/packed/PackedWriter;->add(J)V

    goto :goto_0

    .line 104
    :cond_0
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pendingBitPos:I

    const/16 v1, 0x40

    if-eq v0, v1, :cond_1

    .line 105
    iget-object v0, p0, Lorg/apache/lucene/util/packed/PackedWriter;->out:Lorg/apache/lucene/store/DataOutput;

    iget-wide v2, p0, Lorg/apache/lucene/util/packed/PackedWriter;->pending:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/DataOutput;->writeLong(J)V

    .line 107
    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PackedWriter(written "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->written:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->valueCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " with "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/packed/PackedWriter;->bitsPerValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " bits/value)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
