.class public abstract Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;
.super Ljava/lang/Object;
.source "NumericUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/NumericUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "IntRangeBuilder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addRange(III)V
    .locals 2
    .param p1, "min"    # I
    .param p2, "max"    # I
    .param p3, "shift"    # I

    .prologue
    .line 468
    invoke-static {p1, p3}, Lorg/apache/lucene/util/NumericUtils;->intToPrefixCoded(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3}, Lorg/apache/lucene/util/NumericUtils;->intToPrefixCoded(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;->addRange(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    return-void
.end method

.method public addRange(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "minPrefixCoded"    # Ljava/lang/String;
    .param p2, "maxPrefixCoded"    # Ljava/lang/String;

    .prologue
    .line 460
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
