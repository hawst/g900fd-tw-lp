.class public final Lorg/apache/lucene/util/fst/BytesRefFSTEnum;
.super Lorg/apache/lucene/util/fst/FSTEnum;
.source "BytesRefFSTEnum.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/lucene/util/fst/FSTEnum",
        "<TT;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final current:Lorg/apache/lucene/util/BytesRef;

.field private final result:Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput",
            "<TT;>;"
        }
    .end annotation
.end field

.field private target:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/fst/FST;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<TT;>;"
    .local p1, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/FSTEnum;-><init>(Lorg/apache/lucene/util/fst/FST;)V

    .line 31
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->current:Lorg/apache/lucene/util/BytesRef;

    .line 32
    new-instance v0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->result:Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    .line 46
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->result:Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->current:Lorg/apache/lucene/util/BytesRef;

    iput-object v1, v0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->input:Lorg/apache/lucene/util/BytesRef;

    .line 47
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->current:Lorg/apache/lucene/util/BytesRef;

    const/4 v1, 0x1

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 48
    return-void
.end method

.method private setResult()Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 117
    .local p0, "this":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->upto:I

    if-nez v0, :cond_0

    .line 118
    const/4 v0, 0x0

    .line 122
    :goto_0
    return-object v0

    .line 120
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->current:Lorg/apache/lucene/util/BytesRef;

    iget v1, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->upto:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 121
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->result:Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->output:[Ljava/lang/Object;

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->upto:I

    aget-object v1, v1, v2

    iput-object v1, v0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->output:Ljava/lang/Object;

    .line 122
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->result:Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    goto :goto_0
.end method


# virtual methods
.method public current()Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "this":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->result:Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    return-object v0
.end method

.method protected getCurrentLabel()I
    .locals 2

    .prologue
    .line 103
    .local p0, "this":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->current:Lorg/apache/lucene/util/BytesRef;

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->upto:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method protected getTargetLabel()I
    .locals 3

    .prologue
    .line 93
    .local p0, "this":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->upto:I

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->target:Lorg/apache/lucene/util/BytesRef;

    iget v1, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ne v0, v1, :cond_0

    .line 94
    const/4 v0, -0x1

    .line 96
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->target:Lorg/apache/lucene/util/BytesRef;

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v1, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->target:Lorg/apache/lucene/util/BytesRef;

    iget v1, v1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v2, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->upto:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method protected grow()V
    .locals 2

    .prologue
    .line 113
    .local p0, "this":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->current:Lorg/apache/lucene/util/BytesRef;

    iget v1, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->upto:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 114
    return-void
.end method

.method public next()Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    .local p0, "this":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->doNext()V

    .line 57
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->setResult()Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v0

    return-object v0
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;
    .locals 1
    .param p1, "target"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/BytesRef;",
            ")",
            "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "this":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<TT;>;"
    iput-object p1, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->target:Lorg/apache/lucene/util/BytesRef;

    .line 63
    iget v0, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->targetLength:I

    .line 64
    invoke-super {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->doSeekCeil()V

    .line 65
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->setResult()Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v0

    return-object v0
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;
    .locals 2
    .param p1, "target"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/BytesRef;",
            ")",
            "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    .local p0, "this":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<TT;>;"
    iput-object p1, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->target:Lorg/apache/lucene/util/BytesRef;

    .line 82
    iget v0, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->targetLength:I

    .line 83
    invoke-super {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->doSeekExact()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    sget-boolean v0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->upto:I

    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 85
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->setResult()Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v0

    .line 87
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public seekFloor(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;
    .locals 1
    .param p1, "target"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/BytesRef;",
            ")",
            "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "this":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<TT;>;"
    iput-object p1, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->target:Lorg/apache/lucene/util/BytesRef;

    .line 71
    iget v0, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->targetLength:I

    .line 72
    invoke-super {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->doSeekFloor()V

    .line 73
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->setResult()Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v0

    return-object v0
.end method

.method protected setCurrentLabel(I)V
    .locals 3
    .param p1, "label"    # I

    .prologue
    .line 108
    .local p0, "this":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->current:Lorg/apache/lucene/util/BytesRef;

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->upto:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 109
    return-void
.end method
