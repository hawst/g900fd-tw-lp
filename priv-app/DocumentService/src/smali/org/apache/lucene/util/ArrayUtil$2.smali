.class Lorg/apache/lucene/util/ArrayUtil$2;
.super Lorg/apache/lucene/util/SorterTemplate;
.source "ArrayUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Comparable;)Lorg/apache/lucene/util/SorterTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private pivot:Ljava/lang/Comparable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field final synthetic val$a:[Ljava/lang/Comparable;


# direct methods
.method constructor <init>([Ljava/lang/Comparable;)V
    .locals 0

    .prologue
    .line 613
    iput-object p1, p0, Lorg/apache/lucene/util/ArrayUtil$2;->val$a:[Ljava/lang/Comparable;

    invoke-direct {p0}, Lorg/apache/lucene/util/SorterTemplate;-><init>()V

    return-void
.end method


# virtual methods
.method protected compare(II)I
    .locals 2
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 600
    iget-object v0, p0, Lorg/apache/lucene/util/ArrayUtil$2;->val$a:[Ljava/lang/Comparable;

    aget-object v0, v0, p1

    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$2;->val$a:[Ljava/lang/Comparable;

    aget-object v1, v1, p2

    invoke-interface {v0, v1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected comparePivot(I)I
    .locals 2
    .param p1, "j"    # I

    .prologue
    .line 610
    iget-object v0, p0, Lorg/apache/lucene/util/ArrayUtil$2;->pivot:Ljava/lang/Comparable;

    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$2;->val$a:[Ljava/lang/Comparable;

    aget-object v1, v1, p1

    invoke-interface {v0, v1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected setPivot(I)V
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 605
    iget-object v0, p0, Lorg/apache/lucene/util/ArrayUtil$2;->val$a:[Ljava/lang/Comparable;

    aget-object v0, v0, p1

    iput-object v0, p0, Lorg/apache/lucene/util/ArrayUtil$2;->pivot:Ljava/lang/Comparable;

    .line 606
    return-void
.end method

.method protected swap(II)V
    .locals 3
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 593
    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$2;->val$a:[Ljava/lang/Comparable;

    aget-object v0, v1, p1

    .line 594
    .local v0, "o":Ljava/lang/Comparable;, "TT;"
    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$2;->val$a:[Ljava/lang/Comparable;

    iget-object v2, p0, Lorg/apache/lucene/util/ArrayUtil$2;->val$a:[Ljava/lang/Comparable;

    aget-object v2, v2, p2

    aput-object v2, v1, p1

    .line 595
    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$2;->val$a:[Ljava/lang/Comparable;

    aput-object v0, v1, p2

    .line 596
    return-void
.end method
