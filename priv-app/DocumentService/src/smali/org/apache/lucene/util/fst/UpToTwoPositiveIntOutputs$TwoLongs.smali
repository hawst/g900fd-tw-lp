.class public final Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;
.super Ljava/lang/Object;
.source "UpToTwoPositiveIntOutputs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TwoLongs"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public final first:J

.field public final second:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(JJ)V
    .locals 5
    .param p1, "first"    # J
    .param p3, "second"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-wide p1, p0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->first:J

    .line 49
    iput-wide p3, p0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->second:J

    .line 50
    sget-boolean v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    cmp-long v0, p1, v2

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 51
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    cmp-long v0, p3, v2

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 52
    :cond_1
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "_other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 61
    instance-of v2, p1, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 62
    check-cast v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;

    .line 63
    .local v0, "other":Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;
    iget-wide v2, p0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->first:J

    iget-wide v4, v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->first:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->second:J

    iget-wide v4, v0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->second:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 65
    .end local v0    # "other":Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 71
    iget-wide v0, p0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->first:J

    iget-wide v2, p0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->first:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    iget-wide v2, p0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->second:J

    iget-wide v4, p0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->second:J

    shr-long/2addr v4, v6

    xor-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "TwoLongs:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->first:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/util/fst/UpToTwoPositiveIntOutputs$TwoLongs;->second:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
