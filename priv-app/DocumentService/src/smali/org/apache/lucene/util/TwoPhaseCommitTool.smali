.class public final Lorg/apache/lucene/util/TwoPhaseCommitTool;
.super Ljava/lang/Object;
.source "TwoPhaseCommitTool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/TwoPhaseCommitTool$CommitFailException;,
        Lorg/apache/lucene/util/TwoPhaseCommitTool$PrepareCommitFailException;,
        Lorg/apache/lucene/util/TwoPhaseCommitTool$TwoPhaseCommitWrapper;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    return-void
.end method

.method public static varargs execute([Lorg/apache/lucene/util/TwoPhaseCommit;)V
    .locals 4
    .param p0, "objects"    # [Lorg/apache/lucene/util/TwoPhaseCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/util/TwoPhaseCommitTool$PrepareCommitFailException;,
            Lorg/apache/lucene/util/TwoPhaseCommitTool$CommitFailException;
        }
    .end annotation

    .prologue
    .line 130
    const/4 v2, 0x0

    .line 133
    .local v2, "tpc":Lorg/apache/lucene/util/TwoPhaseCommit;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    array-length v3, p0

    if-ge v0, v3, :cond_1

    .line 134
    aget-object v2, p0, v0

    .line 135
    if-eqz v2, :cond_0

    .line 136
    invoke-interface {v2}, Lorg/apache/lucene/util/TwoPhaseCommit;->prepareCommit()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    :catch_0
    move-exception v1

    .line 142
    .local v1, "t":Ljava/lang/Throwable;
    invoke-static {p0}, Lorg/apache/lucene/util/TwoPhaseCommitTool;->rollback([Lorg/apache/lucene/util/TwoPhaseCommit;)V

    .line 143
    new-instance v3, Lorg/apache/lucene/util/TwoPhaseCommitTool$PrepareCommitFailException;

    invoke-direct {v3, v1, v2}, Lorg/apache/lucene/util/TwoPhaseCommitTool$PrepareCommitFailException;-><init>(Ljava/lang/Throwable;Lorg/apache/lucene/util/TwoPhaseCommit;)V

    throw v3

    .line 148
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_1
    const/4 v0, 0x0

    :goto_1
    :try_start_1
    array-length v3, p0

    if-ge v0, v3, :cond_3

    .line 149
    aget-object v2, p0, v0

    .line 150
    if-eqz v2, :cond_2

    .line 151
    invoke-interface {v2}, Lorg/apache/lucene/util/TwoPhaseCommit;->commit()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 148
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 154
    :catch_1
    move-exception v1

    .line 157
    .restart local v1    # "t":Ljava/lang/Throwable;
    invoke-static {p0}, Lorg/apache/lucene/util/TwoPhaseCommitTool;->rollback([Lorg/apache/lucene/util/TwoPhaseCommit;)V

    .line 158
    new-instance v3, Lorg/apache/lucene/util/TwoPhaseCommitTool$CommitFailException;

    invoke-direct {v3, v1, v2}, Lorg/apache/lucene/util/TwoPhaseCommitTool$CommitFailException;-><init>(Ljava/lang/Throwable;Lorg/apache/lucene/util/TwoPhaseCommit;)V

    throw v3

    .line 160
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_3
    return-void
.end method

.method private static varargs rollback([Lorg/apache/lucene/util/TwoPhaseCommit;)V
    .locals 5
    .param p0, "objects"    # [Lorg/apache/lucene/util/TwoPhaseCommit;

    .prologue
    .line 96
    move-object v0, p0

    .local v0, "arr$":[Lorg/apache/lucene/util/TwoPhaseCommit;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 99
    .local v3, "tpc":Lorg/apache/lucene/util/TwoPhaseCommit;
    if-eqz v3, :cond_0

    .line 101
    :try_start_0
    invoke-interface {v3}, Lorg/apache/lucene/util/TwoPhaseCommit;->rollback()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 102
    :catch_0
    move-exception v4

    goto :goto_1

    .line 105
    .end local v3    # "tpc":Lorg/apache/lucene/util/TwoPhaseCommit;
    :cond_1
    return-void
.end method
