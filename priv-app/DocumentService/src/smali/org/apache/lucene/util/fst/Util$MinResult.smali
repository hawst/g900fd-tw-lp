.class public final Lorg/apache/lucene/util/fst/Util$MinResult;
.super Ljava/lang/Object;
.source "Util.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/Util;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MinResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/util/fst/Util$MinResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field final comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final input:Lorg/apache/lucene/util/IntsRef;

.field public final output:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;Ljava/util/Comparator;)V
    .locals 0
    .param p1, "input"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/IntsRef;",
            "TT;",
            "Ljava/util/Comparator",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 528
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$MinResult;, "Lorg/apache/lucene/util/fst/Util$MinResult<TT;>;"
    .local p2, "output":Ljava/lang/Object;, "TT;"
    .local p3, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529
    iput-object p1, p0, Lorg/apache/lucene/util/fst/Util$MinResult;->input:Lorg/apache/lucene/util/IntsRef;

    .line 530
    iput-object p2, p0, Lorg/apache/lucene/util/fst/Util$MinResult;->output:Ljava/lang/Object;

    .line 531
    iput-object p3, p0, Lorg/apache/lucene/util/fst/Util$MinResult;->comparator:Ljava/util/Comparator;

    .line 532
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 524
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$MinResult;, "Lorg/apache/lucene/util/fst/Util$MinResult<TT;>;"
    check-cast p1, Lorg/apache/lucene/util/fst/Util$MinResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/fst/Util$MinResult;->compareTo(Lorg/apache/lucene/util/fst/Util$MinResult;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/util/fst/Util$MinResult;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Util$MinResult",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 536
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$MinResult;, "Lorg/apache/lucene/util/fst/Util$MinResult<TT;>;"
    .local p1, "other":Lorg/apache/lucene/util/fst/Util$MinResult;, "Lorg/apache/lucene/util/fst/Util$MinResult<TT;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Util$MinResult;->comparator:Ljava/util/Comparator;

    iget-object v2, p0, Lorg/apache/lucene/util/fst/Util$MinResult;->output:Ljava/lang/Object;

    iget-object v3, p1, Lorg/apache/lucene/util/fst/Util$MinResult;->output:Ljava/lang/Object;

    invoke-interface {v1, v2, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 537
    .local v0, "cmp":I
    if-nez v0, :cond_0

    .line 538
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Util$MinResult;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v2, p1, Lorg/apache/lucene/util/fst/Util$MinResult;->input:Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/IntsRef;->compareTo(Lorg/apache/lucene/util/IntsRef;)I

    move-result v0

    .line 540
    .end local v0    # "cmp":I
    :cond_0
    return v0
.end method
