.class public final Lorg/apache/lucene/util/NumericUtils;
.super Ljava/lang/Object;
.source "NumericUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;,
        Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;
    }
.end annotation


# static fields
.field public static final BUF_SIZE_INT:I = 0x6

.field public static final BUF_SIZE_LONG:I = 0xb

.field public static final PRECISION_STEP_DEFAULT:I = 0x4

.field public static final SHIFT_START_INT:C = '`'

.field public static final SHIFT_START_LONG:C = ' '


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addRange(Ljava/lang/Object;IJJI)V
    .locals 8
    .param p0, "builder"    # Ljava/lang/Object;
    .param p1, "valSize"    # I
    .param p2, "minBound"    # J
    .param p4, "maxBound"    # J
    .param p6, "shift"    # I

    .prologue
    const-wide/16 v2, 0x1

    .line 406
    shl-long v0, v2, p6

    sub-long/2addr v0, v2

    or-long/2addr p4, v0

    .line 408
    sparse-switch p1, :sswitch_data_0

    .line 417
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "valSize must be 32 or 64."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    move-object v1, p0

    .line 410
    check-cast v1, Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;

    move-wide v2, p2

    move-wide v4, p4

    move v6, p6

    invoke-virtual/range {v1 .. v6}, Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;->addRange(JJI)V

    .line 419
    .end local p0    # "builder":Ljava/lang/Object;
    :goto_0
    return-void

    .line 413
    .restart local p0    # "builder":Ljava/lang/Object;
    :sswitch_1
    check-cast p0, Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;

    .end local p0    # "builder":Ljava/lang/Object;
    long-to-int v0, p2

    long-to-int v1, p4

    invoke-virtual {p0, v0, v1, p6}, Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;->addRange(III)V

    goto :goto_0

    .line 408
    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_1
        0x40 -> :sswitch_0
    .end sparse-switch
.end method

.method public static doubleToPrefixCoded(D)Ljava/lang/String;
    .locals 2
    .param p0, "val"    # D

    .prologue
    .line 268
    invoke-static {p0, p1}, Lorg/apache/lucene/util/NumericUtils;->doubleToSortableLong(D)J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/apache/lucene/util/NumericUtils;->longToPrefixCoded(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static doubleToSortableLong(D)J
    .locals 4
    .param p0, "val"    # D

    .prologue
    .line 258
    invoke-static {p0, p1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    .line 259
    .local v0, "f":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    const-wide v2, 0x7fffffffffffffffL

    xor-long/2addr v0, v2

    .line 260
    :cond_0
    return-wide v0
.end method

.method public static floatToPrefixCoded(F)Ljava/lang/String;
    .locals 1
    .param p0, "val"    # F

    .prologue
    .line 308
    invoke-static {p0}, Lorg/apache/lucene/util/NumericUtils;->floatToSortableInt(F)I

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/util/NumericUtils;->intToPrefixCoded(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static floatToSortableInt(F)I
    .locals 2
    .param p0, "val"    # F

    .prologue
    .line 298
    invoke-static {p0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 299
    .local v0, "f":I
    if-gez v0, :cond_0

    const v1, 0x7fffffff

    xor-int/2addr v0, v1

    .line 300
    :cond_0
    return v0
.end method

.method public static intToPrefixCoded(II[C)I
    .locals 6
    .param p0, "val"    # I
    .param p1, "shift"    # I
    .param p2, "buffer"    # [C

    .prologue
    .line 156
    const/16 v4, 0x1f

    if-gt p1, v4, :cond_0

    if-gez p1, :cond_1

    .line 157
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "Illegal shift value, must be 0..31"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 158
    :cond_1
    rsub-int/lit8 v4, p1, 0x1f

    div-int/lit8 v4, v4, 0x7

    add-int/lit8 v1, v4, 0x1

    .local v1, "nChars":I
    add-int/lit8 v0, v1, 0x1

    .line 159
    .local v0, "len":I
    const/4 v4, 0x0

    add-int/lit8 v5, p1, 0x60

    int-to-char v5, v5

    aput-char v5, p2, v4

    .line 160
    const/high16 v4, -0x80000000

    xor-int v3, p0, v4

    .line 161
    .local v3, "sortableBits":I
    ushr-int/2addr v3, p1

    move v2, v1

    .line 162
    .end local v1    # "nChars":I
    .local v2, "nChars":I
    :goto_0
    const/4 v4, 0x1

    if-lt v2, v4, :cond_2

    .line 166
    add-int/lit8 v1, v2, -0x1

    .end local v2    # "nChars":I
    .restart local v1    # "nChars":I
    and-int/lit8 v4, v3, 0x7f

    int-to-char v4, v4

    aput-char v4, p2, v2

    .line 167
    ushr-int/lit8 v3, v3, 0x7

    move v2, v1

    .end local v1    # "nChars":I
    .restart local v2    # "nChars":I
    goto :goto_0

    .line 169
    :cond_2
    return v0
.end method

.method public static intToPrefixCoded(I)Ljava/lang/String;
    .locals 1
    .param p0, "val"    # I

    .prologue
    .line 191
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/lucene/util/NumericUtils;->intToPrefixCoded(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static intToPrefixCoded(II)Ljava/lang/String;
    .locals 4
    .param p0, "val"    # I
    .param p1, "shift"    # I

    .prologue
    .line 179
    const/4 v2, 0x6

    new-array v0, v2, [C

    .line 180
    .local v0, "buffer":[C
    invoke-static {p0, p1, v0}, Lorg/apache/lucene/util/NumericUtils;->intToPrefixCoded(II[C)I

    move-result v1

    .line 181
    .local v1, "len":I
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3, v1}, Ljava/lang/String;-><init>([CII)V

    return-object v2
.end method

.method public static longToPrefixCoded(JI[C)I
    .locals 8
    .param p0, "val"    # J
    .param p2, "shift"    # I
    .param p3, "buffer"    # [C

    .prologue
    .line 108
    const/16 v3, 0x3f

    if-gt p2, v3, :cond_0

    if-gez p2, :cond_1

    .line 109
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v6, "Illegal shift value, must be 0..63"

    invoke-direct {v3, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 110
    :cond_1
    rsub-int/lit8 v3, p2, 0x3f

    div-int/lit8 v3, v3, 0x7

    add-int/lit8 v1, v3, 0x1

    .local v1, "nChars":I
    add-int/lit8 v0, v1, 0x1

    .line 111
    .local v0, "len":I
    const/4 v3, 0x0

    add-int/lit8 v6, p2, 0x20

    int-to-char v6, v6

    aput-char v6, p3, v3

    .line 112
    const-wide/high16 v6, -0x8000000000000000L

    xor-long v4, p0, v6

    .line 113
    .local v4, "sortableBits":J
    ushr-long/2addr v4, p2

    move v2, v1

    .line 114
    .end local v1    # "nChars":I
    .local v2, "nChars":I
    :goto_0
    const/4 v3, 0x1

    if-lt v2, v3, :cond_2

    .line 118
    add-int/lit8 v1, v2, -0x1

    .end local v2    # "nChars":I
    .restart local v1    # "nChars":I
    const-wide/16 v6, 0x7f

    and-long/2addr v6, v4

    long-to-int v3, v6

    int-to-char v3, v3

    aput-char v3, p3, v2

    .line 119
    const/4 v3, 0x7

    ushr-long/2addr v4, v3

    move v2, v1

    .end local v1    # "nChars":I
    .restart local v2    # "nChars":I
    goto :goto_0

    .line 121
    :cond_2
    return v0
.end method

.method public static longToPrefixCoded(J)Ljava/lang/String;
    .locals 2
    .param p0, "val"    # J

    .prologue
    .line 143
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/lucene/util/NumericUtils;->longToPrefixCoded(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static longToPrefixCoded(JI)Ljava/lang/String;
    .locals 4
    .param p0, "val"    # J
    .param p2, "shift"    # I

    .prologue
    .line 131
    const/16 v2, 0xb

    new-array v0, v2, [C

    .line 132
    .local v0, "buffer":[C
    invoke-static {p0, p1, p2, v0}, Lorg/apache/lucene/util/NumericUtils;->longToPrefixCoded(JI[C)I

    move-result v1

    .line 133
    .local v1, "len":I
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3, v1}, Ljava/lang/String;-><init>([CII)V

    return-object v2
.end method

.method public static prefixCodedToDouble(Ljava/lang/String;)D
    .locals 2
    .param p0, "val"    # Ljava/lang/String;

    .prologue
    .line 285
    invoke-static {p0}, Lorg/apache/lucene/util/NumericUtils;->prefixCodedToLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/apache/lucene/util/NumericUtils;->sortableLongToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public static prefixCodedToFloat(Ljava/lang/String;)F
    .locals 1
    .param p0, "val"    # Ljava/lang/String;

    .prologue
    .line 325
    invoke-static {p0}, Lorg/apache/lucene/util/NumericUtils;->prefixCodedToInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/util/NumericUtils;->sortableIntToFloat(I)F

    move-result v0

    return v0
.end method

.method public static prefixCodedToInt(Ljava/lang/String;)I
    .locals 8
    .param p0, "prefixCoded"    # Ljava/lang/String;

    .prologue
    .line 230
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    add-int/lit8 v3, v5, -0x60

    .line 231
    .local v3, "shift":I
    const/16 v5, 0x1f

    if-gt v3, v5, :cond_0

    if-gez v3, :cond_1

    .line 232
    :cond_0
    new-instance v5, Ljava/lang/NumberFormatException;

    const-string/jumbo v6, "Invalid shift value in prefixCoded string (is encoded value really an INT?)"

    invoke-direct {v5, v6}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 233
    :cond_1
    const/4 v4, 0x0

    .line 234
    .local v4, "sortableBits":I
    const/4 v1, 0x1

    .local v1, "i":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .local v2, "len":I
    :goto_0
    if-ge v1, v2, :cond_3

    .line 235
    shl-int/lit8 v4, v4, 0x7

    .line 236
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 237
    .local v0, "ch":C
    const/16 v5, 0x7f

    if-le v0, v5, :cond_2

    .line 238
    new-instance v5, Ljava/lang/NumberFormatException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Invalid prefixCoded numerical value representation (char "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " at position "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " is invalid)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 243
    :cond_2
    or-int/2addr v4, v0

    .line 234
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 245
    .end local v0    # "ch":C
    :cond_3
    shl-int v5, v4, v3

    const/high16 v6, -0x80000000

    xor-int/2addr v5, v6

    return v5
.end method

.method public static prefixCodedToLong(Ljava/lang/String;)J
    .locals 10
    .param p0, "prefixCoded"    # Ljava/lang/String;

    .prologue
    .line 203
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    add-int/lit8 v3, v6, -0x20

    .line 204
    .local v3, "shift":I
    const/16 v6, 0x3f

    if-gt v3, v6, :cond_0

    if-gez v3, :cond_1

    .line 205
    :cond_0
    new-instance v6, Ljava/lang/NumberFormatException;

    const-string/jumbo v7, "Invalid shift value in prefixCoded string (is encoded value really a LONG?)"

    invoke-direct {v6, v7}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 206
    :cond_1
    const-wide/16 v4, 0x0

    .line 207
    .local v4, "sortableBits":J
    const/4 v1, 0x1

    .local v1, "i":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .local v2, "len":I
    :goto_0
    if-ge v1, v2, :cond_3

    .line 208
    const/4 v6, 0x7

    shl-long/2addr v4, v6

    .line 209
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 210
    .local v0, "ch":C
    const/16 v6, 0x7f

    if-le v0, v6, :cond_2

    .line 211
    new-instance v6, Ljava/lang/NumberFormatException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Invalid prefixCoded numerical value representation (char "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " at position "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " is invalid)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 216
    :cond_2
    int-to-long v6, v0

    or-long/2addr v4, v6

    .line 207
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 218
    .end local v0    # "ch":C
    :cond_3
    shl-long v6, v4, v3

    const-wide/high16 v8, -0x8000000000000000L

    xor-long/2addr v6, v8

    return-wide v6
.end method

.method public static sortableIntToFloat(I)F
    .locals 1
    .param p0, "val"    # I

    .prologue
    .line 316
    if-gez p0, :cond_0

    const v0, 0x7fffffff

    xor-int/2addr p0, v0

    .line 317
    :cond_0
    invoke-static {p0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method

.method public static sortableLongToDouble(J)D
    .locals 2
    .param p0, "val"    # J

    .prologue
    .line 276
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    xor-long/2addr p0, v0

    .line 277
    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public static splitIntRange(Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;III)V
    .locals 8
    .param p0, "builder"    # Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;
    .param p1, "precisionStep"    # I
    .param p2, "minBound"    # I
    .param p3, "maxBound"    # I

    .prologue
    .line 353
    const/16 v2, 0x20

    int-to-long v4, p2

    int-to-long v6, p3

    move-object v1, p0

    move v3, p1

    invoke-static/range {v1 .. v7}, Lorg/apache/lucene/util/NumericUtils;->splitRange(Ljava/lang/Object;IIJJ)V

    .line 354
    return-void
.end method

.method public static splitLongRange(Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;IJJ)V
    .locals 8
    .param p0, "builder"    # Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;
    .param p1, "precisionStep"    # I
    .param p2, "minBound"    # J
    .param p4, "maxBound"    # J

    .prologue
    .line 339
    const/16 v2, 0x40

    move-object v1, p0

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-static/range {v1 .. v7}, Lorg/apache/lucene/util/NumericUtils;->splitRange(Ljava/lang/Object;IIJJ)V

    .line 340
    return-void
.end method

.method private static splitRange(Ljava/lang/Object;IIJJ)V
    .locals 21
    .param p0, "builder"    # Ljava/lang/Object;
    .param p1, "valSize"    # I
    .param p2, "precisionStep"    # I
    .param p3, "minBound"    # J
    .param p5, "maxBound"    # J

    .prologue
    .line 361
    const/4 v2, 0x1

    move/from16 v0, p2

    if-ge v0, v2, :cond_0

    .line 362
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "precisionStep must be >=1"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 363
    :cond_0
    cmp-long v2, p3, p5

    if-lez v2, :cond_1

    .line 394
    :goto_0
    return-void

    .line 364
    :cond_1
    const/4 v8, 0x0

    .line 366
    .local v8, "shift":I
    :goto_1
    const-wide/16 v2, 0x1

    add-int v4, v8, p2

    shl-long v10, v2, v4

    .line 367
    .local v10, "diff":J
    const-wide/16 v2, 0x1

    shl-long v2, v2, p2

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    shl-long v14, v2, v8

    .line 369
    .local v14, "mask":J
    and-long v2, p3, v14

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    const/4 v9, 0x1

    .line 370
    .local v9, "hasLower":Z
    :goto_2
    and-long v2, p5, v14

    cmp-long v2, v2, v14

    if-eqz v2, :cond_4

    const/4 v12, 0x1

    .line 372
    .local v12, "hasUpper":Z
    :goto_3
    if-eqz v9, :cond_5

    add-long v2, p3, v10

    :goto_4
    const-wide/16 v4, -0x1

    xor-long/2addr v4, v14

    and-long v18, v2, v4

    .line 373
    .local v18, "nextMinBound":J
    if-eqz v12, :cond_6

    sub-long v2, p5, v10

    :goto_5
    const-wide/16 v4, -0x1

    xor-long/2addr v4, v14

    and-long v16, v2, v4

    .line 375
    .local v16, "nextMaxBound":J
    cmp-long v2, v18, p3

    if-gez v2, :cond_7

    const/4 v13, 0x1

    .line 376
    .local v13, "lowerWrapped":Z
    :goto_6
    cmp-long v2, v16, p5

    if-lez v2, :cond_8

    const/16 v20, 0x1

    .line 378
    .local v20, "upperWrapped":Z
    :goto_7
    add-int v2, v8, p2

    move/from16 v0, p1

    if-ge v2, v0, :cond_2

    cmp-long v2, v18, v16

    if-gtz v2, :cond_2

    if-nez v13, :cond_2

    if-eqz v20, :cond_9

    :cond_2
    move-object/from16 v2, p0

    move/from16 v3, p1

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    .line 380
    invoke-static/range {v2 .. v8}, Lorg/apache/lucene/util/NumericUtils;->addRange(Ljava/lang/Object;IJJI)V

    goto :goto_0

    .line 369
    .end local v9    # "hasLower":Z
    .end local v12    # "hasUpper":Z
    .end local v13    # "lowerWrapped":Z
    .end local v16    # "nextMaxBound":J
    .end local v18    # "nextMinBound":J
    .end local v20    # "upperWrapped":Z
    :cond_3
    const/4 v9, 0x0

    goto :goto_2

    .line 370
    .restart local v9    # "hasLower":Z
    :cond_4
    const/4 v12, 0x0

    goto :goto_3

    .restart local v12    # "hasUpper":Z
    :cond_5
    move-wide/from16 v2, p3

    .line 372
    goto :goto_4

    .restart local v18    # "nextMinBound":J
    :cond_6
    move-wide/from16 v2, p5

    .line 373
    goto :goto_5

    .line 375
    .restart local v16    # "nextMaxBound":J
    :cond_7
    const/4 v13, 0x0

    goto :goto_6

    .line 376
    .restart local v13    # "lowerWrapped":Z
    :cond_8
    const/16 v20, 0x0

    goto :goto_7

    .line 385
    .restart local v20    # "upperWrapped":Z
    :cond_9
    if-eqz v9, :cond_a

    .line 386
    or-long v6, p3, v14

    move-object/from16 v2, p0

    move/from16 v3, p1

    move-wide/from16 v4, p3

    invoke-static/range {v2 .. v8}, Lorg/apache/lucene/util/NumericUtils;->addRange(Ljava/lang/Object;IJJI)V

    .line 387
    :cond_a
    if-eqz v12, :cond_b

    .line 388
    const-wide/16 v2, -0x1

    xor-long/2addr v2, v14

    and-long v4, p5, v2

    move-object/from16 v2, p0

    move/from16 v3, p1

    move-wide/from16 v6, p5

    invoke-static/range {v2 .. v8}, Lorg/apache/lucene/util/NumericUtils;->addRange(Ljava/lang/Object;IJJI)V

    .line 391
    :cond_b
    move-wide/from16 p3, v18

    .line 392
    move-wide/from16 p5, v16

    .line 364
    add-int v8, v8, p2

    goto/16 :goto_1
.end method
