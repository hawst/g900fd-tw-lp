.class public final Lorg/apache/lucene/util/RollingCharBuffer;
.super Ljava/lang/Object;
.source "RollingCharBuffer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private buffer:[C

.field private count:I

.field private end:Z

.field private nextPos:I

.field private nextWrite:I

.field private reader:Ljava/io/Reader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/util/RollingCharBuffer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/RollingCharBuffer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/16 v0, 0x20

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    return-void
.end method

.method private getIndex(I)I
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 108
    iget v1, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextWrite:I

    iget v2, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextPos:I

    sub-int/2addr v2, p1

    sub-int v0, v1, v2

    .line 109
    .local v0, "index":I
    if-gez v0, :cond_0

    .line 111
    iget-object v1, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    array-length v1, v1

    add-int/2addr v0, v1

    .line 112
    sget-boolean v1, Lorg/apache/lucene/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 114
    :cond_0
    return v0
.end method

.method private inBounds(I)Z
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 104
    if-ltz p1, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextPos:I

    if-ge p1, v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextPos:I

    iget v1, p0, Lorg/apache/lucene/util/RollingCharBuffer;->count:I

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public freeBefore(I)V
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 141
    sget-boolean v1, Lorg/apache/lucene/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 142
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget v1, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextPos:I

    if-le p1, v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 143
    :cond_1
    iget v1, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextPos:I

    sub-int v0, v1, p1

    .line 144
    .local v0, "newCount":I
    sget-boolean v1, Lorg/apache/lucene/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget v1, p0, Lorg/apache/lucene/util/RollingCharBuffer;->count:I

    if-le v0, v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "newCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " count="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->count:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 145
    :cond_2
    sget-boolean v1, Lorg/apache/lucene/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    array-length v1, v1

    if-le v0, v1, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "newCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " buf.length="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 146
    :cond_3
    iput v0, p0, Lorg/apache/lucene/util/RollingCharBuffer;->count:I

    .line 147
    return-void
.end method

.method public get(I)I
    .locals 8
    .param p1, "pos"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v3, -0x1

    .line 65
    iget v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextPos:I

    if-ne p1, v4, :cond_4

    .line 66
    iget-boolean v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->end:Z

    if-eqz v4, :cond_0

    move v0, v3

    .line 98
    :goto_0
    return v0

    .line 69
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->reader:Ljava/io/Reader;

    invoke-virtual {v4}, Ljava/io/Reader;->read()I

    move-result v0

    .line 70
    .local v0, "ch":I
    if-ne v0, v3, :cond_1

    .line 71
    const/4 v4, 0x1

    iput-boolean v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->end:Z

    move v0, v3

    .line 72
    goto :goto_0

    .line 74
    :cond_1
    iget v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->count:I

    iget-object v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    array-length v4, v4

    if-ne v3, v4, :cond_2

    .line 76
    iget v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->count:I

    add-int/lit8 v3, v3, 0x1

    const/4 v4, 0x2

    invoke-static {v3, v4}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v3

    new-array v2, v3, [C

    .line 78
    .local v2, "newBuffer":[C
    iget-object v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    iget v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextWrite:I

    iget-object v5, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    array-length v5, v5

    iget v6, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextWrite:I

    sub-int/2addr v5, v6

    invoke-static {v3, v4, v2, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 79
    iget-object v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    iget-object v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    array-length v4, v4

    iget v5, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextWrite:I

    sub-int/2addr v4, v5

    iget v5, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextWrite:I

    invoke-static {v3, v7, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 80
    iget-object v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    array-length v3, v3

    iput v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextWrite:I

    .line 81
    iput-object v2, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    .line 83
    .end local v2    # "newBuffer":[C
    :cond_2
    iget v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextWrite:I

    iget-object v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    array-length v4, v4

    if-ne v3, v4, :cond_3

    .line 84
    iput v7, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextWrite:I

    .line 86
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    iget v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextWrite:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextWrite:I

    int-to-char v5, v0

    aput-char v5, v3, v4

    .line 87
    iget v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->count:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->count:I

    .line 88
    iget v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextPos:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextPos:I

    goto :goto_0

    .line 92
    .end local v0    # "ch":I
    :cond_4
    sget-boolean v3, Lorg/apache/lucene/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v3, :cond_5

    iget v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextPos:I

    if-lt p1, v3, :cond_5

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 95
    :cond_5
    sget-boolean v3, Lorg/apache/lucene/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v3, :cond_6

    iget v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextPos:I

    sub-int/2addr v3, p1

    iget v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->count:I

    if-le v3, v4, :cond_6

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "nextPos="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextPos:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " pos="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " count="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/util/RollingCharBuffer;->count:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 97
    :cond_6
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/RollingCharBuffer;->getIndex(I)I

    move-result v1

    .line 98
    .local v1, "index":I
    iget-object v3, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    aget-char v0, v3, v1

    goto/16 :goto_0
.end method

.method public get(II)[C
    .locals 8
    .param p1, "posStart"    # I
    .param p2, "length"    # I

    .prologue
    const/4 v7, 0x0

    .line 118
    sget-boolean v4, Lorg/apache/lucene/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-gtz p2, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 119
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    invoke-direct {p0, p1}, Lorg/apache/lucene/util/RollingCharBuffer;->inBounds(I)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "posStart="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " length="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 122
    :cond_1
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/RollingCharBuffer;->getIndex(I)I

    move-result v3

    .line 123
    .local v3, "startIndex":I
    add-int v4, p1, p2

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/RollingCharBuffer;->getIndex(I)I

    move-result v0

    .line 126
    .local v0, "endIndex":I
    new-array v2, p2, [C

    .line 127
    .local v2, "result":[C
    if-lt v0, v3, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    array-length v4, v4

    if-ge p2, v4, :cond_2

    .line 128
    iget-object v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    sub-int v5, v0, v3

    invoke-static {v4, v3, v2, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 135
    :goto_0
    return-object v2

    .line 131
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    array-length v4, v4

    sub-int v1, v4, v3

    .line 132
    .local v1, "part1":I
    iget-object v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    invoke-static {v4, v3, v2, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 133
    iget-object v4, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    iget-object v5, p0, Lorg/apache/lucene/util/RollingCharBuffer;->buffer:[C

    array-length v5, v5

    sub-int/2addr v5, v3

    sub-int v6, p2, v1

    invoke-static {v4, v7, v2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public reset(Ljava/io/Reader;)V
    .locals 1
    .param p1, "reader"    # Ljava/io/Reader;

    .prologue
    const/4 v0, 0x0

    .line 51
    iput-object p1, p0, Lorg/apache/lucene/util/RollingCharBuffer;->reader:Ljava/io/Reader;

    .line 52
    iput v0, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextPos:I

    .line 53
    iput v0, p0, Lorg/apache/lucene/util/RollingCharBuffer;->nextWrite:I

    .line 54
    iput v0, p0, Lorg/apache/lucene/util/RollingCharBuffer;->count:I

    .line 55
    iput-boolean v0, p0, Lorg/apache/lucene/util/RollingCharBuffer;->end:Z

    .line 56
    return-void
.end method
