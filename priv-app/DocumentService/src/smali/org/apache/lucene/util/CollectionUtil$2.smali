.class Lorg/apache/lucene/util/CollectionUtil$2;
.super Lorg/apache/lucene/util/SorterTemplate;
.source "CollectionUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;)Lorg/apache/lucene/util/SorterTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private pivot:Ljava/lang/Comparable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field final synthetic val$list:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lorg/apache/lucene/util/CollectionUtil$2;->val$list:Ljava/util/List;

    invoke-direct {p0}, Lorg/apache/lucene/util/SorterTemplate;-><init>()V

    return-void
.end method


# virtual methods
.method protected compare(II)I
    .locals 2
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$2;->val$list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    iget-object v1, p0, Lorg/apache/lucene/util/CollectionUtil$2;->val$list:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected comparePivot(I)I
    .locals 2
    .param p1, "j"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$2;->pivot:Ljava/lang/Comparable;

    iget-object v1, p0, Lorg/apache/lucene/util/CollectionUtil$2;->val$list:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected setPivot(I)V
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$2;->val$list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    iput-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$2;->pivot:Ljava/lang/Comparable;

    .line 85
    return-void
.end method

.method protected swap(II)V
    .locals 1
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$2;->val$list:Ljava/util/List;

    invoke-static {v0, p1, p2}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 75
    return-void
.end method
