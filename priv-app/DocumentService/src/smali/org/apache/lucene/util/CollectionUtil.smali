.class public final Lorg/apache/lucene/util/CollectionUtil;
.super Ljava/lang/Object;
.source "CollectionUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getSorter(Ljava/util/List;)Lorg/apache/lucene/util/SorterTemplate;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>(",
            "Ljava/util/List",
            "<TT;>;)",
            "Lorg/apache/lucene/util/SorterTemplate;"
        }
    .end annotation

    .prologue
    .line 69
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "CollectionUtil can only sort random access lists in-place."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/CollectionUtil$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/CollectionUtil$2;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method private static getSorter(Ljava/util/List;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/Comparator",
            "<-TT;>;)",
            "Lorg/apache/lucene/util/SorterTemplate;"
        }
    .end annotation

    .prologue
    .line 40
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "CollectionUtil can only sort random access lists in-place."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/CollectionUtil$1;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/util/CollectionUtil$1;-><init>(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method

.method public static insertionSort(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 167
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 168
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 170
    :goto_0
    return-void

    .line 169
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->insertionSort(II)V

    goto :goto_0
.end method

.method public static insertionSort(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 155
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 156
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 158
    :goto_0
    return-void

    .line 157
    :cond_0
    invoke-static {p0, p1}, Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->insertionSort(II)V

    goto :goto_0
.end method

.method public static mergeSort(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 141
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 142
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 144
    :goto_0
    return-void

    .line 143
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->mergeSort(II)V

    goto :goto_0
.end method

.method public static mergeSort(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 129
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 130
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 132
    :goto_0
    return-void

    .line 131
    :cond_0
    invoke-static {p0, p1}, Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->mergeSort(II)V

    goto :goto_0
.end method

.method public static quickSort(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 116
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 118
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->quickSort(II)V

    goto :goto_0
.end method

.method public static quickSort(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 103
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 104
    .local v0, "size":I
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 106
    :goto_0
    return-void

    .line 105
    :cond_0
    invoke-static {p0, p1}, Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/SorterTemplate;->quickSort(II)V

    goto :goto_0
.end method
