.class Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUTF16Comparator;
.super Ljava/lang/Object;
.source "BytesRef.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/BytesRef;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UTF8SortedAsUTF16Comparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/util/BytesRef;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/BytesRef$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/util/BytesRef$1;

    .prologue
    .line 296
    invoke-direct {p0}, Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUTF16Comparator;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 296
    check-cast p1, Lorg/apache/lucene/util/BytesRef;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/util/BytesRef;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUTF16Comparator;->compare(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)I
    .locals 12
    .param p1, "a"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "b"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/16 v11, 0xee

    .line 302
    iget-object v1, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 303
    .local v1, "aBytes":[B
    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 304
    .local v3, "aUpto":I
    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 305
    .local v6, "bBytes":[B
    iget v7, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 308
    .local v7, "bUpto":I
    iget v9, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v10, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v9, v10, :cond_2

    .line 309
    iget v9, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v2, v3, v9

    .local v2, "aStop":I
    move v8, v7

    .end local v7    # "bUpto":I
    .local v8, "bUpto":I
    move v4, v3

    .line 314
    .end local v3    # "aUpto":I
    .local v4, "aUpto":I
    :goto_0
    if-ge v4, v2, :cond_4

    .line 315
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "aUpto":I
    .restart local v3    # "aUpto":I
    aget-byte v9, v1, v4

    and-int/lit16 v0, v9, 0xff

    .line 316
    .local v0, "aByte":I
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "bUpto":I
    .restart local v7    # "bUpto":I
    aget-byte v9, v6, v8

    and-int/lit16 v5, v9, 0xff

    .line 318
    .local v5, "bByte":I
    if-eq v0, v5, :cond_3

    .line 330
    if-lt v0, v11, :cond_1

    if-lt v5, v11, :cond_1

    .line 331
    and-int/lit16 v9, v0, 0xfe

    if-ne v9, v11, :cond_0

    .line 332
    add-int/lit8 v0, v0, 0xe

    .line 334
    :cond_0
    and-int/lit16 v9, v5, 0xfe

    if-ne v9, v11, :cond_1

    .line 335
    add-int/lit8 v5, v5, 0xe

    .line 338
    :cond_1
    sub-int v9, v0, v5

    .line 343
    .end local v0    # "aByte":I
    .end local v5    # "bByte":I
    :goto_1
    return v9

    .line 311
    .end local v2    # "aStop":I
    :cond_2
    iget v9, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v2, v3, v9

    .restart local v2    # "aStop":I
    move v8, v7

    .end local v7    # "bUpto":I
    .restart local v8    # "bUpto":I
    move v4, v3

    .end local v3    # "aUpto":I
    .restart local v4    # "aUpto":I
    goto :goto_0

    .end local v4    # "aUpto":I
    .end local v8    # "bUpto":I
    .restart local v0    # "aByte":I
    .restart local v3    # "aUpto":I
    .restart local v5    # "bByte":I
    .restart local v7    # "bUpto":I
    :cond_3
    move v8, v7

    .end local v7    # "bUpto":I
    .restart local v8    # "bUpto":I
    move v4, v3

    .line 340
    .end local v3    # "aUpto":I
    .restart local v4    # "aUpto":I
    goto :goto_0

    .line 343
    .end local v0    # "aByte":I
    .end local v5    # "bByte":I
    :cond_4
    iget v9, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v10, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v9, v10

    move v7, v8

    .end local v8    # "bUpto":I
    .restart local v7    # "bUpto":I
    move v3, v4

    .end local v4    # "aUpto":I
    .restart local v3    # "aUpto":I
    goto :goto_1
.end method
