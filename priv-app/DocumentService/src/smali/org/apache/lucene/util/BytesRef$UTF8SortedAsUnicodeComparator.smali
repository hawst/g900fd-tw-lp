.class Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUnicodeComparator;
.super Ljava/lang/Object;
.source "BytesRef.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/BytesRef;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UTF8SortedAsUnicodeComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/util/BytesRef;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/BytesRef$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/util/BytesRef$1;

    .prologue
    .line 264
    invoke-direct {p0}, Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUnicodeComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 264
    check-cast p1, Lorg/apache/lucene/util/BytesRef;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/util/BytesRef;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUnicodeComparator;->compare(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)I
    .locals 12
    .param p1, "a"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "b"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 269
    iget-object v1, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 270
    .local v1, "aBytes":[B
    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 271
    .local v3, "aUpto":I
    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 272
    .local v6, "bBytes":[B
    iget v7, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 274
    .local v7, "bUpto":I
    iget v10, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v11, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v10

    add-int v2, v3, v10

    .local v2, "aStop":I
    move v8, v7

    .end local v7    # "bUpto":I
    .local v8, "bUpto":I
    move v4, v3

    .line 275
    .end local v3    # "aUpto":I
    .local v4, "aUpto":I
    :goto_0
    if-ge v4, v2, :cond_1

    .line 276
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "aUpto":I
    .restart local v3    # "aUpto":I
    aget-byte v10, v1, v4

    and-int/lit16 v0, v10, 0xff

    .line 277
    .local v0, "aByte":I
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "bUpto":I
    .restart local v7    # "bUpto":I
    aget-byte v10, v6, v8

    and-int/lit16 v5, v10, 0xff

    .line 279
    .local v5, "bByte":I
    sub-int v9, v0, v5

    .line 280
    .local v9, "diff":I
    if-eqz v9, :cond_0

    .line 286
    .end local v0    # "aByte":I
    .end local v5    # "bByte":I
    .end local v9    # "diff":I
    :goto_1
    return v9

    .restart local v0    # "aByte":I
    .restart local v5    # "bByte":I
    .restart local v9    # "diff":I
    :cond_0
    move v8, v7

    .end local v7    # "bUpto":I
    .restart local v8    # "bUpto":I
    move v4, v3

    .line 283
    .end local v3    # "aUpto":I
    .restart local v4    # "aUpto":I
    goto :goto_0

    .line 286
    .end local v0    # "aByte":I
    .end local v5    # "bByte":I
    .end local v9    # "diff":I
    :cond_1
    iget v10, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v11, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int v9, v10, v11

    move v7, v8

    .end local v8    # "bUpto":I
    .restart local v7    # "bUpto":I
    move v3, v4

    .end local v4    # "aUpto":I
    .restart local v3    # "aUpto":I
    goto :goto_1
.end method
