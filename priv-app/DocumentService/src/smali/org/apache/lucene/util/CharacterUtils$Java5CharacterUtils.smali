.class final Lorg/apache/lucene/util/CharacterUtils$Java5CharacterUtils;
.super Lorg/apache/lucene/util/CharacterUtils;
.source "CharacterUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/CharacterUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Java5CharacterUtils"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 158
    const-class v0, Lorg/apache/lucene/util/CharacterUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/CharacterUtils$Java5CharacterUtils;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Lorg/apache/lucene/util/CharacterUtils;-><init>()V

    .line 160
    return-void
.end method


# virtual methods
.method public codePointAt(Ljava/lang/CharSequence;I)I
    .locals 1
    .param p1, "seq"    # Ljava/lang/CharSequence;
    .param p2, "offset"    # I

    .prologue
    .line 169
    invoke-static {p1, p2}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    return v0
.end method

.method public codePointAt([CI)I
    .locals 1
    .param p1, "chars"    # [C
    .param p2, "offset"    # I

    .prologue
    .line 164
    invoke-static {p1, p2}, Ljava/lang/Character;->codePointAt([CI)I

    move-result v0

    return v0
.end method

.method public codePointAt([CII)I
    .locals 1
    .param p1, "chars"    # [C
    .param p2, "offset"    # I
    .param p3, "limit"    # I

    .prologue
    .line 174
    invoke-static {p1, p2, p3}, Ljava/lang/Character;->codePointAt([CII)I

    move-result v0

    return v0
.end method

.method public fill(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;Ljava/io/Reader;)Z
    .locals 8
    .param p1, "buffer"    # Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;
    .param p2, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 179
    # getter for: Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->buffer:[C
    invoke-static {p1}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->access$000(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;)[C

    move-result-object v0

    .line 180
    .local v0, "charBuffer":[C
    # setter for: Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->offset:I
    invoke-static {p1, v5}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->access$102(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;I)I

    .line 184
    iget-char v6, p1, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->lastTrailingHighSurrogate:C

    if-eqz v6, :cond_1

    .line 185
    iget-char v6, p1, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->lastTrailingHighSurrogate:C

    aput-char v6, v0, v5

    .line 186
    const/4 v1, 0x1

    .line 191
    .local v1, "offset":I
    :goto_0
    array-length v6, v0

    sub-int/2addr v6, v1

    invoke-virtual {p2, v0, v1, v6}, Ljava/io/Reader;->read([CII)I

    move-result v2

    .line 194
    .local v2, "read":I
    if-ne v2, v7, :cond_3

    .line 195
    # setter for: Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1, v1}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->access$202(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;I)I

    .line 196
    iput-char v5, p1, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->lastTrailingHighSurrogate:C

    .line 197
    if-eqz v1, :cond_2

    .line 226
    :cond_0
    :goto_1
    return v4

    .line 188
    .end local v1    # "offset":I
    .end local v2    # "read":I
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "offset":I
    goto :goto_0

    .restart local v2    # "read":I
    :cond_2
    move v4, v5

    .line 197
    goto :goto_1

    .line 199
    :cond_3
    sget-boolean v6, Lorg/apache/lucene/util/CharacterUtils$Java5CharacterUtils;->$assertionsDisabled:Z

    if-nez v6, :cond_4

    if-gtz v2, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 200
    :cond_4
    add-int v6, v2, v1

    # setter for: Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1, v6}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->access$202(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;I)I

    .line 204
    # getter for: Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->access$200(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;)I

    move-result v6

    if-ne v6, v4, :cond_6

    # getter for: Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->access$200(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    aget-char v6, v0, v6

    invoke-static {v6}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 206
    array-length v6, v0

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {p2, v0, v4, v6}, Ljava/io/Reader;->read([CII)I

    move-result v3

    .line 209
    .local v3, "read2":I
    if-eq v3, v7, :cond_0

    .line 214
    sget-boolean v6, Lorg/apache/lucene/util/CharacterUtils$Java5CharacterUtils;->$assertionsDisabled:Z

    if-nez v6, :cond_5

    if-gtz v3, :cond_5

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 216
    :cond_5
    # += operator for: Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1, v3}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->access$212(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;I)I

    .line 219
    .end local v3    # "read2":I
    :cond_6
    # getter for: Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->access$200(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;)I

    move-result v6

    if-le v6, v4, :cond_7

    # getter for: Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->access$200(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    aget-char v6, v0, v6

    invoke-static {v6}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 221
    # --operator for: Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->access$206(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;)I

    move-result v5

    aget-char v5, v0, v5

    iput-char v5, p1, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->lastTrailingHighSurrogate:C

    goto :goto_1

    .line 223
    :cond_7
    iput-char v5, p1, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->lastTrailingHighSurrogate:C

    goto :goto_1
.end method
