.class public abstract Lorg/apache/lucene/util/CharacterUtils;
.super Ljava/lang/Object;
.source "CharacterUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;,
        Lorg/apache/lucene/util/CharacterUtils$Java4CharacterUtils;,
        Lorg/apache/lucene/util/CharacterUtils$Java5CharacterUtils;
    }
.end annotation


# static fields
.field private static final JAVA_4:Lorg/apache/lucene/util/CharacterUtils$Java4CharacterUtils;

.field private static final JAVA_5:Lorg/apache/lucene/util/CharacterUtils$Java5CharacterUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lorg/apache/lucene/util/CharacterUtils$Java4CharacterUtils;

    invoke-direct {v0}, Lorg/apache/lucene/util/CharacterUtils$Java4CharacterUtils;-><init>()V

    sput-object v0, Lorg/apache/lucene/util/CharacterUtils;->JAVA_4:Lorg/apache/lucene/util/CharacterUtils$Java4CharacterUtils;

    .line 34
    new-instance v0, Lorg/apache/lucene/util/CharacterUtils$Java5CharacterUtils;

    invoke-direct {v0}, Lorg/apache/lucene/util/CharacterUtils$Java5CharacterUtils;-><init>()V

    sput-object v0, Lorg/apache/lucene/util/CharacterUtils;->JAVA_5:Lorg/apache/lucene/util/CharacterUtils$Java5CharacterUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267
    return-void
.end method

.method public static getInstance(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/util/CharacterUtils;
    .locals 1
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 46
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/lucene/util/CharacterUtils;->JAVA_5:Lorg/apache/lucene/util/CharacterUtils$Java5CharacterUtils;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/lucene/util/CharacterUtils;->JAVA_4:Lorg/apache/lucene/util/CharacterUtils$Java4CharacterUtils;

    goto :goto_0
.end method

.method public static newCharacterBuffer(I)Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;
    .locals 3
    .param p0, "bufferSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 124
    const/4 v0, 0x2

    if-ge p0, v0, :cond_0

    .line 125
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "buffersize must be >= 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    new-array v1, p0, [C

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;-><init>([CII)V

    return-object v0
.end method


# virtual methods
.method public abstract codePointAt(Ljava/lang/CharSequence;I)I
.end method

.method public abstract codePointAt([CI)I
.end method

.method public abstract codePointAt([CII)I
.end method

.method public abstract fill(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;Ljava/io/Reader;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
