.class final Lorg/apache/lucene/util/fst/NodeHash;
.super Ljava/lang/Object;
.source "NodeHash.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private count:I

.field private final fst:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mask:I

.field private final scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation
.end field

.field private table:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lorg/apache/lucene/util/fst/NodeHash;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/NodeHash;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/fst/FST;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    .local p1, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 32
    const/16 v0, 0x10

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:[I

    .line 33
    const/16 v0, 0xf

    iput v0, p0, Lorg/apache/lucene/util/fst/NodeHash;->mask:I

    .line 34
    iput-object p1, p0, Lorg/apache/lucene/util/fst/NodeHash;->fst:Lorg/apache/lucene/util/fst/FST;

    .line 35
    return-void
.end method

.method private addNew(I)V
    .locals 4
    .param p1, "address"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/NodeHash;->hash(I)I

    move-result v2

    iget v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->mask:I

    and-int v1, v2, v3

    .line 144
    .local v1, "pos":I
    const/4 v0, 0x0

    .line 146
    .local v0, "c":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:[I

    aget v2, v2, v1

    if-nez v2, :cond_0

    .line 147
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:[I

    aput p1, v2, v1

    .line 154
    return-void

    .line 152
    :cond_0
    add-int/lit8 v0, v0, 0x1

    add-int v2, v1, v0

    iget v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->mask:I

    and-int v1, v2, v3

    goto :goto_0
.end method

.method private hash(I)I
    .locals 5
    .param p1, "node"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    const/16 v0, 0x1f

    .line 90
    .local v0, "PRIME":I
    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->fst:Lorg/apache/lucene/util/fst/FST;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v2

    .line 92
    .local v2, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    const/4 v1, 0x0

    .line 93
    .local v1, "h":I
    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v3, p1, v4, v2}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 96
    :goto_0
    mul-int/lit8 v3, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v4, v4, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    add-int v1, v3, v4

    .line 97
    mul-int/lit8 v3, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v4, v4, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    add-int v1, v3, v4

    .line 98
    mul-int/lit8 v3, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v4, v4, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    add-int v1, v3, v4

    .line 99
    mul-int/lit8 v3, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v4, v4, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    add-int v1, v3, v4

    .line 100
    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v3}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 101
    add-int/lit8 v1, v1, 0x11

    .line 103
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v3}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 109
    const v3, 0x7fffffff

    and-int/2addr v3, v1

    return v3

    .line 106
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v3, v4, v2}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_0
.end method

.method private hash(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 68
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    .local p1, "node":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    const/16 v0, 0x1f

    .line 70
    .local v0, "PRIME":I
    const/4 v3, 0x0

    .line 72
    .local v3, "h":I
    const/4 v2, 0x0

    .local v2, "arcIdx":I
    :goto_0
    iget v4, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-ge v2, v4, :cond_1

    .line 73
    iget-object v4, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    aget-object v1, v4, v2

    .line 75
    .local v1, "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    mul-int/lit8 v4, v3, 0x1f

    iget v5, v1, Lorg/apache/lucene/util/fst/Builder$Arc;->label:I

    add-int v3, v4, v5

    .line 76
    mul-int/lit8 v5, v3, 0x1f

    iget-object v4, v1, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    check-cast v4, Lorg/apache/lucene/util/fst/Builder$CompiledNode;

    iget v4, v4, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:I

    add-int v3, v5, v4

    .line 77
    mul-int/lit8 v4, v3, 0x1f

    iget-object v5, v1, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v5

    add-int v3, v4, v5

    .line 78
    mul-int/lit8 v4, v3, 0x1f

    iget-object v5, v1, Lorg/apache/lucene/util/fst/Builder$Arc;->nextFinalOutput:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v5

    add-int v3, v4, v5

    .line 79
    iget-boolean v4, v1, Lorg/apache/lucene/util/fst/Builder$Arc;->isFinal:Z

    if-eqz v4, :cond_0

    .line 80
    add-int/lit8 v3, v3, 0x11

    .line 72
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 84
    .end local v1    # "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    :cond_1
    const v4, 0x7fffffff

    and-int/2addr v4, v3

    return v4
.end method

.method private nodesEqual(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;ILorg/apache/lucene/util/fst/FST$BytesReader;)Z
    .locals 5
    .param p2, "address"    # I
    .param p3, "in"    # Lorg/apache/lucene/util/fst/FST$BytesReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode",
            "<TT;>;I",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    .local p1, "node":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    const/4 v3, 0x0

    .line 38
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v2, p2, v4, p3}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 39
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v2, v2, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v2, :cond_0

    iget v2, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v4, v4, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    if-eq v2, v4, :cond_0

    move v2, v3

    .line 62
    :goto_0
    return v2

    .line 42
    :cond_0
    const/4 v1, 0x0

    .local v1, "arcUpto":I
    :goto_1
    iget v2, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-ge v1, v2, :cond_5

    .line 43
    iget-object v2, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    aget-object v0, v2, v1

    .line 44
    .local v0, "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    iget v2, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->label:I

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v4, v4, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-ne v2, v4, :cond_1

    iget-object v2, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->output:Ljava/lang/Object;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v4, v4, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    check-cast v2, Lorg/apache/lucene/util/fst/Builder$CompiledNode;

    iget v2, v2, Lorg/apache/lucene/util/fst/Builder$CompiledNode;->node:I

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v4, v4, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    if-ne v2, v4, :cond_1

    iget-object v2, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->nextFinalOutput:Ljava/lang/Object;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v4, v4, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, v0, Lorg/apache/lucene/util/fst/Builder$Arc;->isFinal:Z

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v4}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v4

    if-eq v2, v4, :cond_2

    :cond_1
    move v2, v3

    .line 49
    goto :goto_0

    .line 52
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 53
    iget v2, p1, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_3

    .line 54
    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    move v2, v3

    .line 56
    goto :goto_0

    .line 59
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/NodeHash;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v2, v4, p3}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 42
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "arc":Lorg/apache/lucene/util/fst/Builder$Arc;, "Lorg/apache/lucene/util/fst/Builder$Arc<TT;>;"
    :cond_5
    move v2, v3

    .line 62
    goto :goto_0
.end method

.method private rehash()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 157
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:[I

    .line 158
    .local v2, "oldTable":[I
    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:[I

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x2

    new-array v3, v3, [I

    iput-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:[I

    .line 159
    iget-object v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:[I

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/lucene/util/fst/NodeHash;->mask:I

    .line 160
    const/4 v1, 0x0

    .local v1, "idx":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 161
    aget v0, v2, v1

    .line 162
    .local v0, "address":I
    if-eqz v0, :cond_0

    .line 163
    invoke-direct {p0, v0}, Lorg/apache/lucene/util/fst/NodeHash;->addNew(I)V

    .line 160
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 166
    .end local v0    # "address":I
    :cond_1
    return-void
.end method


# virtual methods
.method public add(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode",
            "<TT;>;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    .local p1, "nodeIn":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<TT;>;"
    iget-object v6, p0, Lorg/apache/lucene/util/fst/NodeHash;->fst:Lorg/apache/lucene/util/fst/FST;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v2

    .line 115
    .local v2, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/NodeHash;->hash(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)I

    move-result v1

    .line 116
    .local v1, "h":I
    iget v6, p0, Lorg/apache/lucene/util/fst/NodeHash;->mask:I

    and-int v4, v1, v6

    .line 117
    .local v4, "pos":I
    const/4 v0, 0x0

    .line 119
    .local v0, "c":I
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:[I

    aget v5, v6, v4

    .line 120
    .local v5, "v":I
    if-nez v5, :cond_2

    .line 122
    iget-object v6, p0, Lorg/apache/lucene/util/fst/NodeHash;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v6, p1}, Lorg/apache/lucene/util/fst/FST;->addNode(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;)I

    move-result v3

    .line 124
    .local v3, "node":I
    sget-boolean v6, Lorg/apache/lucene/util/fst/NodeHash;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    invoke-direct {p0, v3}, Lorg/apache/lucene/util/fst/NodeHash;->hash(I)I

    move-result v6

    if-eq v6, v1, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "frozenHash="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-direct {p0, v3}, Lorg/apache/lucene/util/fst/NodeHash;->hash(I)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " vs h="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6

    .line 125
    :cond_0
    iget v6, p0, Lorg/apache/lucene/util/fst/NodeHash;->count:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/lucene/util/fst/NodeHash;->count:I

    .line 126
    iget-object v6, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:[I

    aput v3, v6, v4

    .line 127
    iget-object v6, p0, Lorg/apache/lucene/util/fst/NodeHash;->table:[I

    array-length v6, v6

    iget v7, p0, Lorg/apache/lucene/util/fst/NodeHash;->count:I

    mul-int/lit8 v7, v7, 0x2

    if-ge v6, v7, :cond_1

    .line 128
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/NodeHash;->rehash()V

    .line 133
    .end local v3    # "node":I
    :cond_1
    :goto_1
    return v3

    .line 131
    :cond_2
    invoke-direct {p0, p1, v5, v2}, Lorg/apache/lucene/util/fst/NodeHash;->nodesEqual(Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;ILorg/apache/lucene/util/fst/FST$BytesReader;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v3, v5

    .line 133
    goto :goto_1

    .line 137
    :cond_3
    add-int/lit8 v0, v0, 0x1

    add-int v6, v4, v0

    iget v7, p0, Lorg/apache/lucene/util/fst/NodeHash;->mask:I

    and-int v4, v6, v7

    .line 138
    goto :goto_0
.end method

.method public count()I
    .locals 1

    .prologue
    .line 169
    .local p0, "this":Lorg/apache/lucene/util/fst/NodeHash;, "Lorg/apache/lucene/util/fst/NodeHash<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/fst/NodeHash;->count:I

    return v0
.end method
