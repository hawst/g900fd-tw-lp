.class public final Lorg/apache/lucene/util/AttributeSource$State;
.super Ljava/lang/Object;
.source "AttributeSource.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/AttributeSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "State"
.end annotation


# instance fields
.field attribute:Lorg/apache/lucene/util/AttributeImpl;

.field next:Lorg/apache/lucene/util/AttributeSource$State;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 113
    new-instance v0, Lorg/apache/lucene/util/AttributeSource$State;

    invoke-direct {v0}, Lorg/apache/lucene/util/AttributeSource$State;-><init>()V

    .line 114
    .local v0, "clone":Lorg/apache/lucene/util/AttributeSource$State;
    iget-object v1, p0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v1}, Lorg/apache/lucene/util/AttributeImpl;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/AttributeImpl;

    iput-object v1, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    .line 116
    iget-object v1, p0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    if-eqz v1, :cond_0

    .line 117
    iget-object v1, p0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {v1}, Lorg/apache/lucene/util/AttributeSource$State;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/AttributeSource$State;

    iput-object v1, v0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    .line 120
    :cond_0
    return-object v0
.end method
