.class public Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;
.super Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;
.source "BytesRefHash.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/BytesRefHash;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DirectBytesStartArray"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private bytesStart:[I

.field private final bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

.field protected final initSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 584
    const-class v0, Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 4
    .param p1, "initSize"    # I

    .prologue
    .line 593
    invoke-direct {p0}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;-><init>()V

    .line 594
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    .line 595
    iput p1, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->initSize:I

    .line 596
    return-void
.end method


# virtual methods
.method public bytesUsed()Ljava/util/concurrent/atomic/AtomicLong;
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    return-object v0
.end method

.method public clear()[I
    .locals 1

    .prologue
    .line 600
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesStart:[I

    return-object v0
.end method

.method public grow()[I
    .locals 2

    .prologue
    .line 605
    sget-boolean v0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesStart:[I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 606
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesStart:[I

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesStart:[I

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesStart:[I

    return-object v0
.end method

.method public init()[I
    .locals 2

    .prologue
    .line 611
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->initSize:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;->bytesStart:[I

    return-object v0
.end method
