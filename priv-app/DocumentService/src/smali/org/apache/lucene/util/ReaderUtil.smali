.class public final Lorg/apache/lucene/util/ReaderUtil;
.super Ljava/lang/Object;
.source "ReaderUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/ReaderUtil$Gather;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static gatherSubReaders(Ljava/util/List;Lorg/apache/lucene/index/IndexReader;)V
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;",
            "Lorg/apache/lucene/index/IndexReader;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "allSubReaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexReader;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->getSequentialSubReaders()[Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    .line 47
    .local v1, "subReaders":[Lorg/apache/lucene/index/IndexReader;
    if-nez v1, :cond_1

    .line 49
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    :cond_0
    return-void

    .line 51
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 52
    aget-object v2, v1, v0

    invoke-static {p0, v2}, Lorg/apache/lucene/util/ReaderUtil;->gatherSubReaders(Ljava/util/List;Lorg/apache/lucene/index/IndexReader;)V

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static getIndexedFields(Lorg/apache/lucene/index/IndexReader;)Ljava/util/Collection;
    .locals 4
    .param p0, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/IndexReader;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 124
    .local v1, "fields":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-static {p0}, Lorg/apache/lucene/util/ReaderUtil;->getMergedFieldInfos(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    .line 125
    .local v0, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    iget-boolean v3, v0, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    if-eqz v3, :cond_0

    .line 126
    iget-object v3, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 129
    .end local v0    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    :cond_1
    return-object v1
.end method

.method public static getMergedFieldInfos(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/FieldInfos;
    .locals 5
    .param p0, "reader"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 135
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v3, "subReaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexReader;>;"
    invoke-static {v3, p0}, Lorg/apache/lucene/util/ReaderUtil;->gatherSubReaders(Ljava/util/List;Lorg/apache/lucene/index/IndexReader;)V

    .line 137
    new-instance v0, Lorg/apache/lucene/index/FieldInfos;

    invoke-direct {v0}, Lorg/apache/lucene/index/FieldInfos;-><init>()V

    .line 138
    .local v0, "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/IndexReader;

    .line 139
    .local v2, "subReader":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/FieldInfos;->add(Lorg/apache/lucene/index/FieldInfos;)V

    goto :goto_0

    .line 141
    .end local v2    # "subReader":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    return-object v0
.end method

.method public static subIndex(I[I)I
    .locals 6
    .param p0, "n"    # I
    .param p1, "docStarts"    # [I

    .prologue
    .line 102
    array-length v4, p1

    .line 103
    .local v4, "size":I
    const/4 v1, 0x0

    .line 104
    .local v1, "lo":I
    add-int/lit8 v0, v4, -0x1

    .line 105
    .local v0, "hi":I
    :goto_0
    if-lt v0, v1, :cond_2

    .line 106
    add-int v5, v1, v0

    ushr-int/lit8 v2, v5, 0x1

    .line 107
    .local v2, "mid":I
    aget v3, p1, v2

    .line 108
    .local v3, "midValue":I
    if-ge p0, v3, :cond_0

    .line 109
    add-int/lit8 v0, v2, -0x1

    goto :goto_0

    .line 110
    :cond_0
    if-le p0, v3, :cond_1

    .line 111
    add-int/lit8 v1, v2, 0x1

    goto :goto_0

    .line 113
    :cond_1
    :goto_1
    add-int/lit8 v5, v2, 0x1

    if-ge v5, v4, :cond_3

    add-int/lit8 v5, v2, 0x1

    aget v5, p1, v5

    if-ne v5, v3, :cond_3

    .line 114
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v2    # "mid":I
    .end local v3    # "midValue":I
    :cond_2
    move v2, v0

    .line 119
    :cond_3
    return v2
.end method
