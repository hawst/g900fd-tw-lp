.class Lorg/apache/lucene/util/packed/Direct8;
.super Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;
.source "Direct8.java"

# interfaces
.implements Lorg/apache/lucene/util/packed/PackedInts$Mutable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final BITS_PER_VALUE:I = 0x8


# instance fields
.field private values:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/util/packed/Direct8;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/Direct8;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "valueCount"    # I

    .prologue
    .line 37
    const/16 v0, 0x8

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 38
    new-array v0, p1, [B

    iput-object v0, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    .line 39
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataInput;I)V
    .locals 5
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p2, "valueCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    const/16 v4, 0x8

    invoke-direct {p0, p2, v4}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 44
    new-array v3, p2, [B

    .line 45
    .local v3, "values":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 46
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v4

    aput-byte v4, v3, v0

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    rem-int/lit8 v1, p2, 0x8

    .line 49
    .local v1, "mod":I
    if-eqz v1, :cond_1

    .line 50
    rsub-int/lit8 v2, v1, 0x8

    .line 52
    .local v2, "pad":I
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    .line 53
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readByte()B

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 57
    .end local v2    # "pad":I
    :cond_1
    iput-object v3, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    .line 58
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "values"    # [B

    .prologue
    .line 68
    array-length v0, p1

    const/16 v1, 0x8

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 69
    iput-object p1, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    .line 70
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 87
    return-void
.end method

.method public get(I)J
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 73
    sget-boolean v0, Lorg/apache/lucene/util/packed/Direct8;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/Direct8;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 74
    :cond_1
    const-wide/16 v0, 0xff

    iget-object v2, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    aget-byte v2, v2, p1

    int-to-long v2, v2

    and-long/2addr v0, v2

    return-wide v0
.end method

.method public getArray()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    return-object v0
.end method

.method public hasArray()Z
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    return v0
.end method

.method public ramBytesUsed()J
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    invoke-static {v0}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf([B)J

    move-result-wide v0

    return-wide v0
.end method

.method public set(IJ)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct8;->values:[B

    const-wide/16 v2, 0xff

    and-long/2addr v2, p2

    long-to-int v1, v2

    int-to-byte v1, v1

    aput-byte v1, v0, p1

    .line 79
    return-void
.end method
