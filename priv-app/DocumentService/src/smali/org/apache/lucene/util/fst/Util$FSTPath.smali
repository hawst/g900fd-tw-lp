.class Lorg/apache/lucene/util/fst/Util$FSTPath;
.super Ljava/lang/Object;
.source "Util.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/Util;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FSTPath"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/util/fst/Util$FSTPath",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public arc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation
.end field

.field final comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<TT;>;"
        }
    .end annotation
.end field

.field public cost:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final input:Lorg/apache/lucene/util/IntsRef;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lorg/apache/lucene/util/fst/FST$Arc;Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Ljava/util/Comparator",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 239
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    .local p1, "cost":Ljava/lang/Object;, "TT;"
    .local p2, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p3, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    .line 240
    new-instance v0, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    invoke-virtual {v0, p2}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 241
    iput-object p1, p0, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    .line 242
    iput-object p3, p0, Lorg/apache/lucene/util/fst/Util$FSTPath;->comparator:Ljava/util/Comparator;

    .line 243
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 233
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    check-cast p1, Lorg/apache/lucene/util/fst/Util$FSTPath;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/fst/Util$FSTPath;->compareTo(Lorg/apache/lucene/util/fst/Util$FSTPath;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/util/fst/Util$FSTPath;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Util$FSTPath",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 252
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    .local p1, "other":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Util$FSTPath;->comparator:Ljava/util/Comparator;

    iget-object v2, p0, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    iget-object v3, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    invoke-interface {v1, v2, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 253
    .local v0, "cmp":I
    if-nez v0, :cond_0

    .line 254
    iget-object v1, p0, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v2, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/IntsRef;->compareTo(Lorg/apache/lucene/util/IntsRef;)I

    move-result v0

    .line 256
    .end local v0    # "cmp":I
    :cond_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 247
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "input="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " cost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
