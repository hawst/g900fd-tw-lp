.class public final Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;
.super Ljava/lang/Object;
.source "CharacterUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/CharacterUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CharacterBuffer"
.end annotation


# instance fields
.field private final buffer:[C

.field lastTrailingHighSurrogate:C

.field private length:I

.field private offset:I


# direct methods
.method constructor <init>([CII)V
    .locals 0
    .param p1, "buffer"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 277
    iput-object p1, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->buffer:[C

    .line 278
    iput p2, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->offset:I

    .line 279
    iput p3, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I

    .line 280
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;)[C
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    .prologue
    .line 267
    iget-object v0, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->buffer:[C

    return-object v0
.end method

.method static synthetic access$102(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;I)I
    .locals 0
    .param p0, "x0"    # Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;
    .param p1, "x1"    # I

    .prologue
    .line 267
    iput p1, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->offset:I

    return p1
.end method

.method static synthetic access$200(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    .prologue
    .line 267
    iget v0, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I

    return v0
.end method

.method static synthetic access$202(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;I)I
    .locals 0
    .param p0, "x0"    # Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;
    .param p1, "x1"    # I

    .prologue
    .line 267
    iput p1, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I

    return p1
.end method

.method static synthetic access$206(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;

    .prologue
    .line 267
    iget v0, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I

    return v0
.end method

.method static synthetic access$212(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;I)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;
    .param p1, "x1"    # I

    .prologue
    .line 267
    iget v0, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I

    return v0
.end method


# virtual methods
.method public getBuffer()[C
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->buffer:[C

    return-object v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 307
    iget v0, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I

    return v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 297
    iget v0, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->offset:I

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 315
    iput v0, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->offset:I

    .line 316
    iput v0, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I

    .line 317
    iput-char v0, p0, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->lastTrailingHighSurrogate:C

    .line 318
    return-void
.end method
