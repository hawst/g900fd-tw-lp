.class final Lorg/apache/lucene/util/fst/FST$ReverseBytesReader;
.super Lorg/apache/lucene/util/fst/FST$BytesReader;
.source "FST.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/FST;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ReverseBytesReader"
.end annotation


# direct methods
.method public constructor <init>([BI)V
    .locals 0
    .param p1, "bytes"    # [B
    .param p2, "pos"    # I

    .prologue
    .line 1251
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/fst/FST$BytesReader;-><init>([BI)V

    .line 1252
    return-void
.end method


# virtual methods
.method public readByte()B
    .locals 3

    .prologue
    .line 1256
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST$ReverseBytesReader;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/util/fst/FST$ReverseBytesReader;->pos:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lorg/apache/lucene/util/fst/FST$ReverseBytesReader;->pos:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public readBytes([BII)V
    .locals 5
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 1261
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_0

    .line 1262
    add-int v1, p2, v0

    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST$ReverseBytesReader;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/util/fst/FST$ReverseBytesReader;->pos:I

    add-int/lit8 v4, v3, -0x1

    iput v4, p0, Lorg/apache/lucene/util/fst/FST$ReverseBytesReader;->pos:I

    aget-byte v2, v2, v3

    aput-byte v2, p1, v1

    .line 1261
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1264
    :cond_0
    return-void
.end method

.method public skip(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 1267
    iget v0, p0, Lorg/apache/lucene/util/fst/FST$ReverseBytesReader;->pos:I

    sub-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$ReverseBytesReader;->pos:I

    .line 1268
    return-void
.end method

.method public skip(II)V
    .locals 1
    .param p1, "base"    # I
    .param p2, "count"    # I

    .prologue
    .line 1271
    sub-int v0, p1, p2

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$ReverseBytesReader;->pos:I

    .line 1272
    return-void
.end method
