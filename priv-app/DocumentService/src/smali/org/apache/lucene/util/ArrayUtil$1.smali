.class Lorg/apache/lucene/util/ArrayUtil$1;
.super Lorg/apache/lucene/util/SorterTemplate;
.source "ArrayUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Object;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private pivot:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field final synthetic val$a:[Ljava/lang/Object;

.field final synthetic val$comp:Ljava/util/Comparator;


# direct methods
.method constructor <init>([Ljava/lang/Object;Ljava/util/Comparator;)V
    .locals 0

    .prologue
    .line 584
    iput-object p1, p0, Lorg/apache/lucene/util/ArrayUtil$1;->val$a:[Ljava/lang/Object;

    iput-object p2, p0, Lorg/apache/lucene/util/ArrayUtil$1;->val$comp:Ljava/util/Comparator;

    invoke-direct {p0}, Lorg/apache/lucene/util/SorterTemplate;-><init>()V

    return-void
.end method


# virtual methods
.method protected compare(II)I
    .locals 3
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 571
    iget-object v0, p0, Lorg/apache/lucene/util/ArrayUtil$1;->val$comp:Ljava/util/Comparator;

    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$1;->val$a:[Ljava/lang/Object;

    aget-object v1, v1, p1

    iget-object v2, p0, Lorg/apache/lucene/util/ArrayUtil$1;->val$a:[Ljava/lang/Object;

    aget-object v2, v2, p2

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected comparePivot(I)I
    .locals 3
    .param p1, "j"    # I

    .prologue
    .line 581
    iget-object v0, p0, Lorg/apache/lucene/util/ArrayUtil$1;->val$comp:Ljava/util/Comparator;

    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$1;->pivot:Ljava/lang/Object;

    iget-object v2, p0, Lorg/apache/lucene/util/ArrayUtil$1;->val$a:[Ljava/lang/Object;

    aget-object v2, v2, p1

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected setPivot(I)V
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 576
    iget-object v0, p0, Lorg/apache/lucene/util/ArrayUtil$1;->val$a:[Ljava/lang/Object;

    aget-object v0, v0, p1

    iput-object v0, p0, Lorg/apache/lucene/util/ArrayUtil$1;->pivot:Ljava/lang/Object;

    .line 577
    return-void
.end method

.method protected swap(II)V
    .locals 3
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 564
    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$1;->val$a:[Ljava/lang/Object;

    aget-object v0, v1, p1

    .line 565
    .local v0, "o":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$1;->val$a:[Ljava/lang/Object;

    iget-object v2, p0, Lorg/apache/lucene/util/ArrayUtil$1;->val$a:[Ljava/lang/Object;

    aget-object v2, v2, p2

    aput-object v2, v1, p1

    .line 566
    iget-object v1, p0, Lorg/apache/lucene/util/ArrayUtil$1;->val$a:[Ljava/lang/Object;

    aput-object v0, v1, p2

    .line 567
    return-void
.end method
