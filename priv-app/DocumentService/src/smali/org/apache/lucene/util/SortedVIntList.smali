.class public Lorg/apache/lucene/util/SortedVIntList;
.super Lorg/apache/lucene/search/DocIdSet;
.source "SortedVIntList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;
    }
.end annotation


# static fields
.field static final BITS2VINTLIST_SIZE:I = 0x8

.field private static final BIT_SHIFT:I = 0x7

.field private static final VB1:I = 0x7f


# instance fields
.field private final MAX_BYTES_PER_INT:I

.field private bytes:[B

.field private lastBytePos:I

.field private size:I


# direct methods
.method public constructor <init>(Ljava/util/BitSet;)V
    .locals 3
    .param p1, "bits"    # Ljava/util/BitSet;

    .prologue
    .line 77
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 156
    const/4 v2, 0x5

    iput v2, p0, Lorg/apache/lucene/util/SortedVIntList;->MAX_BYTES_PER_INT:I

    .line 78
    new-instance v0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;-><init>(Lorg/apache/lucene/util/SortedVIntList;)V

    .line 79
    .local v0, "builder":Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v1

    .line 80
    .local v1, "nextInt":I
    :goto_0
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 81
    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->addInt(I)V

    .line 82
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v1

    goto :goto_0

    .line 84
    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->done()V

    .line 85
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 3
    .param p1, "docIdSetIterator"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 156
    const/4 v2, 0x5

    iput v2, p0, Lorg/apache/lucene/util/SortedVIntList;->MAX_BYTES_PER_INT:I

    .line 95
    new-instance v0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;-><init>(Lorg/apache/lucene/util/SortedVIntList;)V

    .line 97
    .local v0, "builder":Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v1

    .local v1, "doc":I
    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 98
    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->addInt(I)V

    goto :goto_0

    .line 100
    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->done()V

    .line 101
    return-void
.end method

.method public varargs constructor <init>([I)V
    .locals 1
    .param p1, "sortedInts"    # [I

    .prologue
    .line 57
    array-length v0, p1

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/SortedVIntList;-><init>([II)V

    .line 58
    return-void
.end method

.method public constructor <init>([II)V
    .locals 3
    .param p1, "sortedInts"    # [I
    .param p2, "inputSize"    # I

    .prologue
    .line 65
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 156
    const/4 v2, 0x5

    iput v2, p0, Lorg/apache/lucene/util/SortedVIntList;->MAX_BYTES_PER_INT:I

    .line 66
    new-instance v0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;-><init>(Lorg/apache/lucene/util/SortedVIntList;)V

    .line 67
    .local v0, "builder":Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_0

    .line 68
    aget v2, p1, v1

    invoke-virtual {v0, v2}, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->addInt(I)V

    .line 67
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 70
    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->done()V

    .line 71
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/util/SortedVIntList;)V
    .locals 0
    .param p0, "x0"    # Lorg/apache/lucene/util/SortedVIntList;

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/util/SortedVIntList;->initBytes()V

    return-void
.end method

.method static synthetic access$100(Lorg/apache/lucene/util/SortedVIntList;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/util/SortedVIntList;

    .prologue
    .line 40
    iget v0, p0, Lorg/apache/lucene/util/SortedVIntList;->lastBytePos:I

    return v0
.end method

.method static synthetic access$108(Lorg/apache/lucene/util/SortedVIntList;)I
    .locals 2
    .param p0, "x0"    # Lorg/apache/lucene/util/SortedVIntList;

    .prologue
    .line 40
    iget v0, p0, Lorg/apache/lucene/util/SortedVIntList;->lastBytePos:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/apache/lucene/util/SortedVIntList;->lastBytePos:I

    return v0
.end method

.method static synthetic access$200(Lorg/apache/lucene/util/SortedVIntList;)[B
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/util/SortedVIntList;

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/lucene/util/SortedVIntList;->bytes:[B

    return-object v0
.end method

.method static synthetic access$300(Lorg/apache/lucene/util/SortedVIntList;I)V
    .locals 0
    .param p0, "x0"    # Lorg/apache/lucene/util/SortedVIntList;
    .param p1, "x1"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/SortedVIntList;->resizeBytes(I)V

    return-void
.end method

.method static synthetic access$408(Lorg/apache/lucene/util/SortedVIntList;)I
    .locals 2
    .param p0, "x0"    # Lorg/apache/lucene/util/SortedVIntList;

    .prologue
    .line 40
    iget v0, p0, Lorg/apache/lucene/util/SortedVIntList;->size:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/apache/lucene/util/SortedVIntList;->size:I

    return v0
.end method

.method private initBytes()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 141
    iput v1, p0, Lorg/apache/lucene/util/SortedVIntList;->size:I

    .line 142
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/SortedVIntList;->bytes:[B

    .line 143
    iput v1, p0, Lorg/apache/lucene/util/SortedVIntList;->lastBytePos:I

    .line 144
    return-void
.end method

.method private resizeBytes(I)V
    .locals 4
    .param p1, "newSize"    # I

    .prologue
    const/4 v3, 0x0

    .line 147
    iget-object v1, p0, Lorg/apache/lucene/util/SortedVIntList;->bytes:[B

    array-length v1, v1

    if-eq p1, v1, :cond_0

    .line 148
    new-array v0, p1, [B

    .line 149
    .local v0, "newBytes":[B
    iget-object v1, p0, Lorg/apache/lucene/util/SortedVIntList;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/util/SortedVIntList;->lastBytePos:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 150
    iput-object v0, p0, Lorg/apache/lucene/util/SortedVIntList;->bytes:[B

    .line 152
    .end local v0    # "newBytes":[B
    :cond_0
    return-void
.end method


# virtual methods
.method public getByteSize()I
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lorg/apache/lucene/util/SortedVIntList;->bytes:[B

    array-length v0, v0

    return v0
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x1

    return v0
.end method

.method public iterator()Lorg/apache/lucene/search/DocIdSetIterator;
    .locals 1

    .prologue
    .line 183
    new-instance v0, Lorg/apache/lucene/util/SortedVIntList$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/SortedVIntList$1;-><init>(Lorg/apache/lucene/util/SortedVIntList;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lorg/apache/lucene/util/SortedVIntList;->size:I

    return v0
.end method
