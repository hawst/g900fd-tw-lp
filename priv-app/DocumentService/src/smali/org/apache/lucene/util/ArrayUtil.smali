.class public final Lorg/apache/lucene/util/ArrayUtil;
.super Ljava/lang/Object;
.source "ArrayUtil.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/apache/lucene/util/ArrayUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static equals([CI[CII)Z
    .locals 4
    .param p0, "left"    # [C
    .param p1, "offsetLeft"    # I
    .param p2, "right"    # [C
    .param p3, "offsetRight"    # I
    .param p4, "length"    # I

    .prologue
    const/4 v1, 0x0

    .line 507
    add-int v2, p1, p4

    array-length v3, p0

    if-gt v2, v3, :cond_0

    add-int v2, p3, p4

    array-length v3, p2

    if-gt v2, v3, :cond_0

    .line 508
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p4, :cond_2

    .line 509
    add-int v2, p1, v0

    aget-char v2, p0, v2

    add-int v3, p3, v0

    aget-char v3, p2, v3

    if-eq v2, v3, :cond_1

    .line 516
    .end local v0    # "i":I
    :cond_0
    :goto_1
    return v1

    .line 508
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 514
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public static equals([II[III)Z
    .locals 4
    .param p0, "left"    # [I
    .param p1, "offsetLeft"    # I
    .param p2, "right"    # [I
    .param p3, "offsetRight"    # I
    .param p4, "length"    # I

    .prologue
    const/4 v1, 0x0

    .line 533
    add-int v2, p1, p4

    array-length v3, p0

    if-gt v2, v3, :cond_0

    add-int v2, p3, p4

    array-length v3, p2

    if-gt v2, v3, :cond_0

    .line 534
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p4, :cond_2

    .line 535
    add-int v2, p1, v0

    aget v2, p0, v2

    add-int v3, p3, v0

    aget v3, p2, v3

    if-eq v2, v3, :cond_1

    .line 542
    .end local v0    # "i":I
    :cond_0
    :goto_1
    return v1

    .line 534
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 540
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public static getShrinkSize(III)I
    .locals 2
    .param p0, "currentSize"    # I
    .param p1, "targetSize"    # I
    .param p2, "bytesPerElement"    # I

    .prologue
    .line 228
    invoke-static {p1, p2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    .line 232
    .local v0, "newSize":I
    div-int/lit8 v1, p0, 0x2

    if-ge v0, v1, :cond_0

    .line 235
    .end local v0    # "newSize":I
    :goto_0
    return v0

    .restart local v0    # "newSize":I
    :cond_0
    move v0, p0

    goto :goto_0
.end method

.method private static getSorter([Ljava/lang/Comparable;)Lorg/apache/lucene/util/SorterTemplate;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;)",
            "Lorg/apache/lucene/util/SorterTemplate;"
        }
    .end annotation

    .prologue
    .line 590
    .local p0, "a":[Ljava/lang/Comparable;, "[TT;"
    new-instance v0, Lorg/apache/lucene/util/ArrayUtil$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/ArrayUtil$2;-><init>([Ljava/lang/Comparable;)V

    return-object v0
.end method

.method private static getSorter([Ljava/lang/Object;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Ljava/util/Comparator",
            "<-TT;>;)",
            "Lorg/apache/lucene/util/SorterTemplate;"
        }
    .end annotation

    .prologue
    .line 561
    .local p0, "a":[Ljava/lang/Object;, "[TT;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    new-instance v0, Lorg/apache/lucene/util/ArrayUtil$1;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/util/ArrayUtil$1;-><init>([Ljava/lang/Object;Ljava/util/Comparator;)V

    return-object v0
.end method

.method public static grow([B)[B
    .locals 1
    .param p0, "array"    # [B

    .prologue
    .line 352
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v0

    return-object v0
.end method

.method public static grow([BI)[B
    .locals 4
    .param p0, "array"    # [B
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 342
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "size must be positive (got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 343
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 344
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [B

    .line 345
    .local v0, "newArray":[B
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 348
    .end local v0    # "newArray":[B
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([C)[C
    .locals 1
    .param p0, "array"    # [C

    .prologue
    .line 402
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([CI)[C

    move-result-object v0

    return-object v0
.end method

.method public static grow([CI)[C
    .locals 4
    .param p0, "array"    # [C
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 392
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "size must be positive (got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 393
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 394
    const/4 v1, 0x2

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [C

    .line 395
    .local v0, "newArray":[C
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 398
    .end local v0    # "newArray":[C
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([D)[D
    .locals 1
    .param p0, "array"    # [D

    .prologue
    .line 277
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([DI)[D

    move-result-object v0

    return-object v0
.end method

.method public static grow([DI)[D
    .locals 4
    .param p0, "array"    # [D
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 267
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "size must be positive (got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 268
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 269
    const/16 v1, 0x8

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [D

    .line 270
    .local v0, "newArray":[D
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 273
    .end local v0    # "newArray":[D
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([F)[F
    .locals 1
    .param p0, "array"    # [F

    .prologue
    .line 263
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([FI)[F

    move-result-object v0

    return-object v0
.end method

.method public static grow([FI)[F
    .locals 4
    .param p0, "array"    # [F
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 253
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "size must be positive (got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 254
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 255
    const/4 v1, 0x4

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [F

    .line 256
    .local v0, "newArray":[F
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 259
    .end local v0    # "newArray":[F
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([I)[I
    .locals 1
    .param p0, "array"    # [I

    .prologue
    .line 302
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v0

    return-object v0
.end method

.method public static grow([II)[I
    .locals 4
    .param p0, "array"    # [I
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 292
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "size must be positive (got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 293
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 294
    const/4 v1, 0x4

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [I

    .line 295
    .local v0, "newArray":[I
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 298
    .end local v0    # "newArray":[I
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([J)[J
    .locals 1
    .param p0, "array"    # [J

    .prologue
    .line 327
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([JI)[J

    move-result-object v0

    return-object v0
.end method

.method public static grow([JI)[J
    .locals 4
    .param p0, "array"    # [J
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 317
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "size must be positive (got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 318
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 319
    const/16 v1, 0x8

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [J

    .line 320
    .local v0, "newArray":[J
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 323
    .end local v0    # "newArray":[J
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([S)[S
    .locals 1
    .param p0, "array"    # [S

    .prologue
    .line 249
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([SI)[S

    move-result-object v0

    return-object v0
.end method

.method public static grow([SI)[S
    .locals 4
    .param p0, "array"    # [S
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 239
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "size must be positive (got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 240
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 241
    const/4 v1, 0x2

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [S

    .line 242
    .local v0, "newArray":[S
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 245
    .end local v0    # "newArray":[S
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([Z)[Z
    .locals 1
    .param p0, "array"    # [Z

    .prologue
    .line 377
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([ZI)[Z

    move-result-object v0

    return-object v0
.end method

.method public static grow([ZI)[Z
    .locals 4
    .param p0, "array"    # [Z
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 367
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "size must be positive (got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 368
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 369
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [Z

    .line 370
    .local v0, "newArray":[Z
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 373
    .end local v0    # "newArray":[Z
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([[F)[[F
    .locals 1
    .param p0, "array"    # [[F

    .prologue
    .line 455
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([[FI)[[F

    move-result-object v0

    return-object v0
.end method

.method public static grow([[FI)[[F
    .locals 4
    .param p0, "array"    # [[F
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 444
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "size must be positive (got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 445
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 446
    sget v1, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [[F

    .line 447
    .local v0, "newArray":[[F
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 450
    .end local v0    # "newArray":[[F
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static grow([[I)[[I
    .locals 1
    .param p0, "array"    # [[I

    .prologue
    .line 428
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([[II)[[I

    move-result-object v0

    return-object v0
.end method

.method public static grow([[II)[[I
    .locals 4
    .param p0, "array"    # [[I
    .param p1, "minSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 417
    sget-boolean v1, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "size must be positive (got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "): likely integer overflow?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 418
    :cond_0
    array-length v1, p0

    if-ge v1, p1, :cond_1

    .line 419
    sget v1, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [[I

    .line 420
    .local v0, "newArray":[[I
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 423
    .end local v0    # "newArray":[[I
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static hashCode([BII)I
    .locals 4
    .param p0, "array"    # [B
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 486
    const/4 v0, 0x0

    .line 487
    .local v0, "code":I
    add-int/lit8 v1, p2, -0x1

    .local v1, "i":I
    :goto_0
    if-lt v1, p1, :cond_0

    .line 488
    mul-int/lit8 v2, v0, 0x1f

    aget-byte v3, p0, v1

    add-int v0, v2, v3

    .line 487
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 489
    :cond_0
    return v0
.end method

.method public static hashCode([CII)I
    .locals 4
    .param p0, "array"    # [C
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 475
    const/4 v0, 0x0

    .line 476
    .local v0, "code":I
    add-int/lit8 v1, p2, -0x1

    .local v1, "i":I
    :goto_0
    if-lt v1, p1, :cond_0

    .line 477
    mul-int/lit8 v2, v0, 0x1f

    aget-char v3, p0, v1

    add-int v0, v2, v3

    .line 476
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 478
    :cond_0
    return v0
.end method

.method public static insertionSort([Ljava/lang/Comparable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;)V"
        }
    .end annotation

    .prologue
    .line 735
    .local p0, "a":[Ljava/lang/Comparable;, "[TT;"
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->insertionSort([Ljava/lang/Comparable;II)V

    .line 736
    return-void
.end method

.method public static insertionSort([Ljava/lang/Comparable;II)V
    .locals 2
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;II)V"
        }
    .end annotation

    .prologue
    .line 726
    .local p0, "a":[Ljava/lang/Comparable;, "[TT;"
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 728
    :goto_0
    return-void

    .line 727
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Comparable;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->insertionSort(II)V

    goto :goto_0
.end method

.method public static insertionSort([Ljava/lang/Object;IILjava/util/Comparator;)V
    .locals 2
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;II",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 707
    .local p0, "a":[Ljava/lang/Object;, "[TT;"
    .local p3, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 709
    :goto_0
    return-void

    .line 708
    :cond_0
    invoke-static {p0, p3}, Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Object;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->insertionSort(II)V

    goto :goto_0
.end method

.method public static insertionSort([Ljava/lang/Object;Ljava/util/Comparator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 716
    .local p0, "a":[Ljava/lang/Object;, "[TT;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1, p1}, Lorg/apache/lucene/util/ArrayUtil;->insertionSort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 717
    return-void
.end method

.method public static mergeSort([Ljava/lang/Comparable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;)V"
        }
    .end annotation

    .prologue
    .line 695
    .local p0, "a":[Ljava/lang/Comparable;, "[TT;"
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Comparable;II)V

    .line 696
    return-void
.end method

.method public static mergeSort([Ljava/lang/Comparable;II)V
    .locals 2
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;II)V"
        }
    .end annotation

    .prologue
    .line 686
    .local p0, "a":[Ljava/lang/Comparable;, "[TT;"
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 688
    :goto_0
    return-void

    .line 687
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Comparable;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->mergeSort(II)V

    goto :goto_0
.end method

.method public static mergeSort([Ljava/lang/Object;IILjava/util/Comparator;)V
    .locals 2
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;II",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 666
    .local p0, "a":[Ljava/lang/Object;, "[TT;"
    .local p3, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 669
    :goto_0
    return-void

    .line 668
    :cond_0
    invoke-static {p0, p3}, Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Object;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->mergeSort(II)V

    goto :goto_0
.end method

.method public static mergeSort([Ljava/lang/Object;Ljava/util/Comparator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 676
    .local p0, "a":[Ljava/lang/Object;, "[TT;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1, p1}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 677
    return-void
.end method

.method public static oversize(II)I
    .locals 5
    .param p0, "minTargetSize"    # I
    .param p1, "bytesPerElement"    # I

    .prologue
    const v4, 0x7ffffffe

    const v3, 0x7ffffffc

    .line 160
    if-gez p0, :cond_0

    .line 162
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "invalid array size "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 165
    :cond_0
    if-nez p0, :cond_1

    .line 167
    const/4 v1, 0x0

    .line 222
    :goto_0
    return v1

    .line 173
    :cond_1
    shr-int/lit8 v0, p0, 0x3

    .line 175
    .local v0, "extra":I
    const/4 v2, 0x3

    if-ge v0, v2, :cond_2

    .line 179
    const/4 v0, 0x3

    .line 182
    :cond_2
    add-int v1, p0, v0

    .line 185
    .local v1, "newSize":I
    add-int/lit8 v2, v1, 0x7

    if-gez v2, :cond_3

    .line 187
    const v1, 0x7fffffff

    goto :goto_0

    .line 190
    :cond_3
    sget-boolean v2, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v2, :cond_4

    .line 192
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 201
    :pswitch_1
    add-int/lit8 v2, v1, 0x7

    const v3, 0x7ffffff8

    and-int v1, v2, v3

    goto :goto_0

    .line 195
    :pswitch_2
    add-int/lit8 v2, v1, 0x1

    and-int v1, v2, v4

    goto :goto_0

    .line 198
    :pswitch_3
    add-int/lit8 v2, v1, 0x3

    and-int v1, v2, v3

    goto :goto_0

    .line 210
    :cond_4
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 216
    :pswitch_4
    add-int/lit8 v2, v1, 0x3

    and-int v1, v2, v3

    goto :goto_0

    .line 213
    :pswitch_5
    add-int/lit8 v2, v1, 0x1

    and-int v1, v2, v4

    goto :goto_0

    .line 192
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 210
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static parse([CIIIZ)I
    .locals 7
    .param p0, "chars"    # [C
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .param p3, "radix"    # I
    .param p4, "negative"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .prologue
    .line 106
    const/high16 v5, -0x80000000

    div-int v2, v5, p3

    .line 107
    .local v2, "max":I
    const/4 v4, 0x0

    .line 108
    .local v4, "result":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_3

    .line 109
    add-int v5, v1, p1

    aget-char v5, p0, v5

    invoke-static {v5, p3}, Ljava/lang/Character;->digit(CI)I

    move-result v0

    .line 110
    .local v0, "digit":I
    const/4 v5, -0x1

    if-ne v0, v5, :cond_0

    .line 111
    new-instance v5, Ljava/lang/NumberFormatException;

    const-string/jumbo v6, "Unable to parse"

    invoke-direct {v5, v6}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 113
    :cond_0
    if-le v2, v4, :cond_1

    .line 114
    new-instance v5, Ljava/lang/NumberFormatException;

    const-string/jumbo v6, "Unable to parse"

    invoke-direct {v5, v6}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 116
    :cond_1
    mul-int v5, v4, p3

    sub-int v3, v5, v0

    .line 117
    .local v3, "next":I
    if-le v3, v4, :cond_2

    .line 118
    new-instance v5, Ljava/lang/NumberFormatException;

    const-string/jumbo v6, "Unable to parse"

    invoke-direct {v5, v6}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 120
    :cond_2
    move v4, v3

    .line 108
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 125
    .end local v0    # "digit":I
    .end local v3    # "next":I
    :cond_3
    if-nez p4, :cond_4

    .line 126
    neg-int v4, v4

    .line 127
    if-gez v4, :cond_4

    .line 128
    new-instance v5, Ljava/lang/NumberFormatException;

    const-string/jumbo v6, "Unable to parse"

    invoke-direct {v5, v6}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 131
    :cond_4
    return v4
.end method

.method public static parseInt([C)I
    .locals 3
    .param p0, "chars"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .prologue
    .line 56
    const/4 v0, 0x0

    array-length v1, p0

    const/16 v2, 0xa

    invoke-static {p0, v0, v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CIII)I

    move-result v0

    return v0
.end method

.method public static parseInt([CII)I
    .locals 1
    .param p0, "chars"    # [C
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .prologue
    .line 68
    const/16 v0, 0xa

    invoke-static {p0, p1, p2, v0}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CIII)I

    move-result v0

    return v0
.end method

.method public static parseInt([CIII)I
    .locals 5
    .param p0, "chars"    # [C
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .param p3, "radix"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 84
    if-eqz p0, :cond_0

    const/4 v3, 0x2

    if-lt p3, v3, :cond_0

    const/16 v3, 0x24

    if-le p3, v3, :cond_1

    .line 86
    :cond_0
    new-instance v2, Ljava/lang/NumberFormatException;

    invoke-direct {v2}, Ljava/lang/NumberFormatException;-><init>()V

    throw v2

    .line 88
    :cond_1
    const/4 v0, 0x0

    .line 89
    .local v0, "i":I
    if-nez p2, :cond_2

    .line 90
    new-instance v2, Ljava/lang/NumberFormatException;

    const-string/jumbo v3, "chars length is 0"

    invoke-direct {v2, v3}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 92
    :cond_2
    add-int v3, p1, v0

    aget-char v3, p0, v3

    const/16 v4, 0x2d

    if-ne v3, v4, :cond_3

    move v1, v2

    .line 93
    .local v1, "negative":Z
    :goto_0
    if-eqz v1, :cond_4

    add-int/lit8 v0, v0, 0x1

    if-ne v0, p2, :cond_4

    .line 94
    new-instance v2, Ljava/lang/NumberFormatException;

    const-string/jumbo v3, "can\'t convert to an int"

    invoke-direct {v2, v3}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 92
    .end local v1    # "negative":Z
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 96
    .restart local v1    # "negative":Z
    :cond_4
    if-ne v1, v2, :cond_5

    .line 97
    add-int/lit8 p1, p1, 0x1

    .line 98
    add-int/lit8 p2, p2, -0x1

    .line 100
    :cond_5
    invoke-static {p0, p1, p2, p3, v1}, Lorg/apache/lucene/util/ArrayUtil;->parse([CIIIZ)I

    move-result v2

    return v2
.end method

.method public static quickSort([Ljava/lang/Comparable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;)V"
        }
    .end annotation

    .prologue
    .line 654
    .local p0, "a":[Ljava/lang/Comparable;, "[TT;"
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->quickSort([Ljava/lang/Comparable;II)V

    .line 655
    return-void
.end method

.method public static quickSort([Ljava/lang/Comparable;II)V
    .locals 2
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>([TT;II)V"
        }
    .end annotation

    .prologue
    .line 645
    .local p0, "a":[Ljava/lang/Comparable;, "[TT;"
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 647
    :goto_0
    return-void

    .line 646
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Comparable;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->quickSort(II)V

    goto :goto_0
.end method

.method public static quickSort([Ljava/lang/Object;IILjava/util/Comparator;)V
    .locals 2
    .param p1, "fromIndex"    # I
    .param p2, "toIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;II",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 626
    .local p0, "a":[Ljava/lang/Object;, "[TT;"
    .local p3, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    sub-int v0, p2, p1

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 628
    :goto_0
    return-void

    .line 627
    :cond_0
    invoke-static {p0, p3}, Lorg/apache/lucene/util/ArrayUtil;->getSorter([Ljava/lang/Object;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/util/SorterTemplate;->quickSort(II)V

    goto :goto_0
.end method

.method public static quickSort([Ljava/lang/Object;Ljava/util/Comparator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 635
    .local p0, "a":[Ljava/lang/Object;, "[TT;"
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1, p1}, Lorg/apache/lucene/util/ArrayUtil;->quickSort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 636
    return-void
.end method

.method public static shrink([BI)[B
    .locals 5
    .param p0, "array"    # [B
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 356
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "size must be positive (got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 357
    :cond_0
    array-length v2, p0

    const/4 v3, 0x1

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 358
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 359
    new-array v0, v1, [B

    .line 360
    .local v0, "newArray":[B
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 363
    .end local v0    # "newArray":[B
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static shrink([CI)[C
    .locals 5
    .param p0, "array"    # [C
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 406
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "size must be positive (got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 407
    :cond_0
    array-length v2, p0

    const/4 v3, 0x2

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 408
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 409
    new-array v0, v1, [C

    .line 410
    .local v0, "newArray":[C
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 413
    .end local v0    # "newArray":[C
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static shrink([II)[I
    .locals 5
    .param p0, "array"    # [I
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 306
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "size must be positive (got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 307
    :cond_0
    array-length v2, p0

    const/4 v3, 0x4

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 308
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 309
    new-array v0, v1, [I

    .line 310
    .local v0, "newArray":[I
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 313
    .end local v0    # "newArray":[I
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static shrink([JI)[J
    .locals 5
    .param p0, "array"    # [J
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 331
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "size must be positive (got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 332
    :cond_0
    array-length v2, p0

    const/16 v3, 0x8

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 333
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 334
    new-array v0, v1, [J

    .line 335
    .local v0, "newArray":[J
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 338
    .end local v0    # "newArray":[J
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static shrink([SI)[S
    .locals 5
    .param p0, "array"    # [S
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 281
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "size must be positive (got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 282
    :cond_0
    array-length v2, p0

    const/4 v3, 0x2

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 283
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 284
    new-array v0, v1, [S

    .line 285
    .local v0, "newArray":[S
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 288
    .end local v0    # "newArray":[S
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static shrink([ZI)[Z
    .locals 5
    .param p0, "array"    # [Z
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 381
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "size must be positive (got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 382
    :cond_0
    array-length v2, p0

    const/4 v3, 0x1

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 383
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 384
    new-array v0, v1, [Z

    .line 385
    .local v0, "newArray":[Z
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 388
    .end local v0    # "newArray":[Z
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static shrink([[FI)[[F
    .locals 5
    .param p0, "array"    # [[F
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 459
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "size must be positive (got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 460
    :cond_0
    array-length v2, p0

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 461
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 462
    new-array v0, v1, [[F

    .line 463
    .local v0, "newArray":[[F
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 466
    .end local v0    # "newArray":[[F
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static shrink([[II)[[I
    .locals 5
    .param p0, "array"    # [[I
    .param p1, "targetSize"    # I

    .prologue
    const/4 v4, 0x0

    .line 432
    sget-boolean v2, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "size must be positive (got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "): likely integer overflow?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 433
    :cond_0
    array-length v2, p0

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, p1, v3}, Lorg/apache/lucene/util/ArrayUtil;->getShrinkSize(III)I

    move-result v1

    .line 434
    .local v1, "newSize":I
    array-length v2, p0

    if-eq v1, v2, :cond_1

    .line 435
    new-array v0, v1, [[I

    .line 436
    .local v0, "newArray":[[I
    invoke-static {p0, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 439
    .end local v0    # "newArray":[[I
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static toIntArray(Ljava/util/Collection;)[I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)[I"
        }
    .end annotation

    .prologue
    .line 547
    .local p0, "ints":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v5

    new-array v1, v5, [I

    .line 548
    .local v1, "result":[I
    const/4 v2, 0x0

    .line 549
    .local v2, "upto":I
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 550
    .local v4, "v":I
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "upto":I
    .local v3, "upto":I
    aput v4, v1, v2

    move v2, v3

    .end local v3    # "upto":I
    .restart local v2    # "upto":I
    goto :goto_0

    .line 554
    .end local v4    # "v":I
    :cond_0
    sget-boolean v5, Lorg/apache/lucene/util/ArrayUtil;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    array-length v5, v1

    if-eq v2, v5, :cond_1

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 556
    :cond_1
    return-object v1
.end method
