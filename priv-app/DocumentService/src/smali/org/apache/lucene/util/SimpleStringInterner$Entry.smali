.class Lorg/apache/lucene/util/SimpleStringInterner$Entry;
.super Ljava/lang/Object;
.source "SimpleStringInterner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/SimpleStringInterner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Entry"
.end annotation


# instance fields
.field private final hash:I

.field private next:Lorg/apache/lucene/util/SimpleStringInterner$Entry;

.field private final str:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;ILorg/apache/lucene/util/SimpleStringInterner$Entry;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "hash"    # I
    .param p3, "next"    # Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lorg/apache/lucene/util/SimpleStringInterner$Entry;->str:Ljava/lang/String;

    .line 35
    iput p2, p0, Lorg/apache/lucene/util/SimpleStringInterner$Entry;->hash:I

    .line 36
    iput-object p3, p0, Lorg/apache/lucene/util/SimpleStringInterner$Entry;->next:Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    .line 37
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILorg/apache/lucene/util/SimpleStringInterner$Entry;Lorg/apache/lucene/util/SimpleStringInterner$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lorg/apache/lucene/util/SimpleStringInterner$Entry;
    .param p4, "x3"    # Lorg/apache/lucene/util/SimpleStringInterner$1;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/util/SimpleStringInterner$Entry;-><init>(Ljava/lang/String;ILorg/apache/lucene/util/SimpleStringInterner$Entry;)V

    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/util/SimpleStringInterner$Entry;)Lorg/apache/lucene/util/SimpleStringInterner$Entry;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    .prologue
    .line 29
    iget-object v0, p0, Lorg/apache/lucene/util/SimpleStringInterner$Entry;->next:Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    return-object v0
.end method

.method static synthetic access$002(Lorg/apache/lucene/util/SimpleStringInterner$Entry;Lorg/apache/lucene/util/SimpleStringInterner$Entry;)Lorg/apache/lucene/util/SimpleStringInterner$Entry;
    .locals 0
    .param p0, "x0"    # Lorg/apache/lucene/util/SimpleStringInterner$Entry;
    .param p1, "x1"    # Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    .prologue
    .line 29
    iput-object p1, p0, Lorg/apache/lucene/util/SimpleStringInterner$Entry;->next:Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    return-object p1
.end method

.method static synthetic access$100(Lorg/apache/lucene/util/SimpleStringInterner$Entry;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    .prologue
    .line 29
    iget v0, p0, Lorg/apache/lucene/util/SimpleStringInterner$Entry;->hash:I

    return v0
.end method

.method static synthetic access$200(Lorg/apache/lucene/util/SimpleStringInterner$Entry;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    .prologue
    .line 29
    iget-object v0, p0, Lorg/apache/lucene/util/SimpleStringInterner$Entry;->str:Ljava/lang/String;

    return-object v0
.end method
