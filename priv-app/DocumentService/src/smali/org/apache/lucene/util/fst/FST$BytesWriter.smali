.class Lorg/apache/lucene/util/fst/FST$BytesWriter;
.super Lorg/apache/lucene/store/DataOutput;
.source "FST.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/FST;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BytesWriter"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field posWrite:I

.field final synthetic this$0:Lorg/apache/lucene/util/fst/FST;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1189
    const-class v0, Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/fst/FST;)V
    .locals 1

    .prologue
    .line 1192
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$BytesWriter;, "Lorg/apache/lucene/util/fst/FST<TT;>.BytesWriter;"
    iput-object p1, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->this$0:Lorg/apache/lucene/util/fst/FST;

    invoke-direct {p0}, Lorg/apache/lucene/store/DataOutput;-><init>()V

    .line 1195
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    .line 1196
    return-void
.end method


# virtual methods
.method public setPosWrite(I)V
    .locals 2
    .param p1, "posWrite"    # I

    .prologue
    .line 1209
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$BytesWriter;, "Lorg/apache/lucene/util/fst/FST<TT;>.BytesWriter;"
    iput p1, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    .line 1210
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->this$0:Lorg/apache/lucene/util/fst/FST;

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    array-length v0, v0

    if-ge v0, p1, :cond_0

    .line 1211
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->this$0:Lorg/apache/lucene/util/fst/FST;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->this$0:Lorg/apache/lucene/util/fst/FST;

    iget-object v1, v1, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    invoke-static {v1, p1}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    .line 1213
    :cond_0
    return-void
.end method

.method public writeByte(B)V
    .locals 3
    .param p1, "b"    # B

    .prologue
    .line 1200
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$BytesWriter;, "Lorg/apache/lucene/util/fst/FST<TT;>.BytesWriter;"
    sget-boolean v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->this$0:Lorg/apache/lucene/util/fst/FST;

    iget-object v1, v1, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1201
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->this$0:Lorg/apache/lucene/util/fst/FST;

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    array-length v0, v0

    iget v1, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    if-ne v0, v1, :cond_1

    .line 1202
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->this$0:Lorg/apache/lucene/util/fst/FST;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->this$0:Lorg/apache/lucene/util/fst/FST;

    iget-object v1, v1, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    invoke-static {v1}, Lorg/apache/lucene/util/ArrayUtil;->grow([B)[B

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    .line 1204
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget v0, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->this$0:Lorg/apache/lucene/util/fst/FST;

    iget-object v1, v1, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    array-length v1, v1

    if-lt v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "posWrite="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " bytes.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->this$0:Lorg/apache/lucene/util/fst/FST;

    iget-object v2, v2, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1205
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->this$0:Lorg/apache/lucene/util/fst/FST;

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    aput-byte p1, v0, v1

    .line 1206
    return-void
.end method

.method public writeBytes([BII)V
    .locals 3
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 1217
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$BytesWriter;, "Lorg/apache/lucene/util/fst/FST<TT;>.BytesWriter;"
    iget v1, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    add-int v0, v1, p3

    .line 1218
    .local v0, "size":I
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->this$0:Lorg/apache/lucene/util/fst/FST;

    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->this$0:Lorg/apache/lucene/util/fst/FST;

    iget-object v2, v2, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    invoke-static {v2, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v2

    iput-object v2, v1, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    .line 1219
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->this$0:Lorg/apache/lucene/util/fst/FST;

    iget-object v1, v1, Lorg/apache/lucene/util/fst/FST;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    invoke-static {p1, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1220
    iget v1, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    add-int/2addr v1, p3

    iput v1, p0, Lorg/apache/lucene/util/fst/FST$BytesWriter;->posWrite:I

    .line 1221
    return-void
.end method
