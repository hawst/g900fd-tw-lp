.class public final Lorg/apache/lucene/util/BitUtil;
.super Ljava/lang/Object;
.source "BitUtil.java"


# static fields
.field public static final nlzTable:[B

.field public static final ntzTable:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x100

    .line 689
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/util/BitUtil;->ntzTable:[B

    .line 782
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/lucene/util/BitUtil;->nlzTable:[B

    return-void

    .line 689
    nop

    :array_0
    .array-data 1
        0x8t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x4t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x5t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x4t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x6t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x4t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x5t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x4t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x7t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x4t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x5t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x4t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x6t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x4t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x5t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x4t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x1t
        0x0t
    .end array-data

    .line 782
    :array_1
    .array-data 1
        0x8t
        0x7t
        0x6t
        0x6t
        0x5t
        0x5t
        0x5t
        0x5t
        0x4t
        0x4t
        0x4t
        0x4t
        0x4t
        0x4t
        0x4t
        0x4t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x3t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x2t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isPowerOfTwo(I)Z
    .locals 1
    .param p0, "v"    # I

    .prologue
    .line 806
    add-int/lit8 v0, p0, -0x1

    and-int/2addr v0, p0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPowerOfTwo(J)Z
    .locals 4
    .param p0, "v"    # J

    .prologue
    .line 811
    const-wide/16 v0, 0x1

    sub-long v0, p0, v0

    and-long/2addr v0, p0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static nextHighestPowerOfTwo(I)I
    .locals 1
    .param p0, "v"    # I

    .prologue
    .line 816
    add-int/lit8 p0, p0, -0x1

    .line 817
    shr-int/lit8 v0, p0, 0x1

    or-int/2addr p0, v0

    .line 818
    shr-int/lit8 v0, p0, 0x2

    or-int/2addr p0, v0

    .line 819
    shr-int/lit8 v0, p0, 0x4

    or-int/2addr p0, v0

    .line 820
    shr-int/lit8 v0, p0, 0x8

    or-int/2addr p0, v0

    .line 821
    shr-int/lit8 v0, p0, 0x10

    or-int/2addr p0, v0

    .line 822
    add-int/lit8 p0, p0, 0x1

    .line 823
    return p0
.end method

.method public static nextHighestPowerOfTwo(J)J
    .locals 4
    .param p0, "v"    # J

    .prologue
    const-wide/16 v2, 0x1

    .line 828
    sub-long/2addr p0, v2

    .line 829
    const/4 v0, 0x1

    shr-long v0, p0, v0

    or-long/2addr p0, v0

    .line 830
    const/4 v0, 0x2

    shr-long v0, p0, v0

    or-long/2addr p0, v0

    .line 831
    const/4 v0, 0x4

    shr-long v0, p0, v0

    or-long/2addr p0, v0

    .line 832
    const/16 v0, 0x8

    shr-long v0, p0, v0

    or-long/2addr p0, v0

    .line 833
    const/16 v0, 0x10

    shr-long v0, p0, v0

    or-long/2addr p0, v0

    .line 834
    const/16 v0, 0x20

    shr-long v0, p0, v0

    or-long/2addr p0, v0

    .line 835
    add-long/2addr p0, v2

    .line 836
    return-wide p0
.end method

.method public static nlz(J)I
    .locals 4
    .param p0, "x"    # J

    .prologue
    .line 787
    const/4 v0, 0x0

    .line 789
    .local v0, "n":I
    const/16 v2, 0x20

    ushr-long v2, p0, v2

    long-to-int v1, v2

    .line 790
    .local v1, "y":I
    if-nez v1, :cond_0

    add-int/lit8 v0, v0, 0x20

    long-to-int v1, p0

    .line 791
    :cond_0
    const/high16 v2, -0x10000

    and-int/2addr v2, v1

    if-nez v2, :cond_1

    add-int/lit8 v0, v0, 0x10

    shl-int/lit8 v1, v1, 0x10

    .line 792
    :cond_1
    const/high16 v2, -0x1000000

    and-int/2addr v2, v1

    if-nez v2, :cond_2

    add-int/lit8 v0, v0, 0x8

    shl-int/lit8 v1, v1, 0x8

    .line 793
    :cond_2
    sget-object v2, Lorg/apache/lucene/util/BitUtil;->nlzTable:[B

    ushr-int/lit8 v3, v1, 0x18

    aget-byte v2, v2, v3

    add-int/2addr v2, v0

    return v2
.end method

.method public static ntz(I)I
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 738
    and-int/lit16 v0, p0, 0xff

    .line 739
    .local v0, "lowByte":I
    if-eqz v0, :cond_0

    sget-object v1, Lorg/apache/lucene/util/BitUtil;->ntzTable:[B

    aget-byte v1, v1, v0

    .line 746
    :goto_0
    return v1

    .line 740
    :cond_0
    ushr-int/lit8 v1, p0, 0x8

    and-int/lit16 v0, v1, 0xff

    .line 741
    if-eqz v0, :cond_1

    sget-object v1, Lorg/apache/lucene/util/BitUtil;->ntzTable:[B

    aget-byte v1, v1, v0

    add-int/lit8 v1, v1, 0x8

    goto :goto_0

    .line 742
    :cond_1
    ushr-int/lit8 v1, p0, 0x10

    and-int/lit16 v0, v1, 0xff

    .line 743
    if-eqz v0, :cond_2

    sget-object v1, Lorg/apache/lucene/util/BitUtil;->ntzTable:[B

    aget-byte v1, v1, v0

    add-int/lit8 v1, v1, 0x10

    goto :goto_0

    .line 746
    :cond_2
    sget-object v1, Lorg/apache/lucene/util/BitUtil;->ntzTable:[B

    ushr-int/lit8 v2, p0, 0x18

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x18

    goto :goto_0
.end method

.method public static ntz(J)I
    .locals 6
    .param p0, "val"    # J

    .prologue
    .line 705
    long-to-int v1, p0

    .line 706
    .local v1, "lower":I
    and-int/lit16 v0, v1, 0xff

    .line 707
    .local v0, "lowByte":I
    if-eqz v0, :cond_0

    sget-object v3, Lorg/apache/lucene/util/BitUtil;->ntzTable:[B

    aget-byte v3, v3, v0

    .line 728
    :goto_0
    return v3

    .line 709
    :cond_0
    if-eqz v1, :cond_3

    .line 710
    ushr-int/lit8 v3, v1, 0x8

    and-int/lit16 v0, v3, 0xff

    .line 711
    if-eqz v0, :cond_1

    sget-object v3, Lorg/apache/lucene/util/BitUtil;->ntzTable:[B

    aget-byte v3, v3, v0

    add-int/lit8 v3, v3, 0x8

    goto :goto_0

    .line 712
    :cond_1
    ushr-int/lit8 v3, v1, 0x10

    and-int/lit16 v0, v3, 0xff

    .line 713
    if-eqz v0, :cond_2

    sget-object v3, Lorg/apache/lucene/util/BitUtil;->ntzTable:[B

    aget-byte v3, v3, v0

    add-int/lit8 v3, v3, 0x10

    goto :goto_0

    .line 716
    :cond_2
    sget-object v3, Lorg/apache/lucene/util/BitUtil;->ntzTable:[B

    ushr-int/lit8 v4, v1, 0x18

    aget-byte v3, v3, v4

    add-int/lit8 v3, v3, 0x18

    goto :goto_0

    .line 719
    :cond_3
    const/16 v3, 0x20

    shr-long v4, p0, v3

    long-to-int v2, v4

    .line 720
    .local v2, "upper":I
    and-int/lit16 v0, v2, 0xff

    .line 721
    if-eqz v0, :cond_4

    sget-object v3, Lorg/apache/lucene/util/BitUtil;->ntzTable:[B

    aget-byte v3, v3, v0

    add-int/lit8 v3, v3, 0x20

    goto :goto_0

    .line 722
    :cond_4
    ushr-int/lit8 v3, v2, 0x8

    and-int/lit16 v0, v3, 0xff

    .line 723
    if-eqz v0, :cond_5

    sget-object v3, Lorg/apache/lucene/util/BitUtil;->ntzTable:[B

    aget-byte v3, v3, v0

    add-int/lit8 v3, v3, 0x28

    goto :goto_0

    .line 724
    :cond_5
    ushr-int/lit8 v3, v2, 0x10

    and-int/lit16 v0, v3, 0xff

    .line 725
    if-eqz v0, :cond_6

    sget-object v3, Lorg/apache/lucene/util/BitUtil;->ntzTable:[B

    aget-byte v3, v3, v0

    add-int/lit8 v3, v3, 0x30

    goto :goto_0

    .line 728
    :cond_6
    sget-object v3, Lorg/apache/lucene/util/BitUtil;->ntzTable:[B

    ushr-int/lit8 v4, v2, 0x18

    aget-byte v3, v3, v4

    add-int/lit8 v3, v3, 0x38

    goto :goto_0
.end method

.method public static ntz2(J)I
    .locals 4
    .param p0, "x"    # J

    .prologue
    .line 754
    const/4 v0, 0x0

    .line 755
    .local v0, "n":I
    long-to-int v1, p0

    .line 756
    .local v1, "y":I
    if-nez v1, :cond_0

    add-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    ushr-long v2, p0, v2

    long-to-int v1, v2

    .line 757
    :cond_0
    const v2, 0xffff

    and-int/2addr v2, v1

    if-nez v2, :cond_1

    add-int/lit8 v0, v0, 0x10

    ushr-int/lit8 v1, v1, 0x10

    .line 758
    :cond_1
    and-int/lit16 v2, v1, 0xff

    if-nez v2, :cond_2

    add-int/lit8 v0, v0, 0x8

    ushr-int/lit8 v1, v1, 0x8

    .line 759
    :cond_2
    sget-object v2, Lorg/apache/lucene/util/BitUtil;->ntzTable:[B

    and-int/lit16 v3, v1, 0xff

    aget-byte v2, v2, v3

    add-int/2addr v2, v0

    return v2
.end method

.method public static ntz3(J)I
    .locals 4
    .param p0, "x"    # J

    .prologue
    .line 769
    const/4 v0, 0x1

    .line 772
    .local v0, "n":I
    long-to-int v1, p0

    .line 773
    .local v1, "y":I
    if-nez v1, :cond_0

    add-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    ushr-long v2, p0, v2

    long-to-int v1, v2

    .line 774
    :cond_0
    const v2, 0xffff

    and-int/2addr v2, v1

    if-nez v2, :cond_1

    add-int/lit8 v0, v0, 0x10

    ushr-int/lit8 v1, v1, 0x10

    .line 775
    :cond_1
    and-int/lit16 v2, v1, 0xff

    if-nez v2, :cond_2

    add-int/lit8 v0, v0, 0x8

    ushr-int/lit8 v1, v1, 0x8

    .line 776
    :cond_2
    and-int/lit8 v2, v1, 0xf

    if-nez v2, :cond_3

    add-int/lit8 v0, v0, 0x4

    ushr-int/lit8 v1, v1, 0x4

    .line 777
    :cond_3
    and-int/lit8 v2, v1, 0x3

    if-nez v2, :cond_4

    add-int/lit8 v0, v0, 0x2

    ushr-int/lit8 v1, v1, 0x2

    .line 778
    :cond_4
    and-int/lit8 v2, v1, 0x1

    sub-int v2, v0, v2

    return v2
.end method

.method public static pop(J)I
    .locals 6
    .param p0, "x"    # J

    .prologue
    const-wide v4, 0x3333333333333333L    # 4.667261458395856E-62

    .line 43
    const/4 v0, 0x1

    ushr-long v0, p0, v0

    const-wide v2, 0x5555555555555555L    # 1.1945305291614955E103

    and-long/2addr v0, v2

    sub-long/2addr p0, v0

    .line 44
    and-long v0, p0, v4

    const/4 v2, 0x2

    ushr-long v2, p0, v2

    and-long/2addr v2, v4

    add-long p0, v0, v2

    .line 45
    const/4 v0, 0x4

    ushr-long v0, p0, v0

    add-long/2addr v0, p0

    const-wide v2, 0xf0f0f0f0f0f0f0fL    # 3.815736827118017E-236

    and-long p0, v0, v2

    .line 46
    const/16 v0, 0x8

    ushr-long v0, p0, v0

    add-long/2addr p0, v0

    .line 47
    const/16 v0, 0x10

    ushr-long v0, p0, v0

    add-long/2addr p0, v0

    .line 48
    const/16 v0, 0x20

    ushr-long v0, p0, v0

    add-long/2addr p0, v0

    .line 49
    long-to-int v0, p0

    and-int/lit8 v0, v0, 0x7f

    return v0
.end method

.method public static pop_andnot([J[JII)J
    .locals 36
    .param p0, "A"    # [J
    .param p1, "B"    # [J
    .param p2, "wordOffset"    # I
    .param p3, "numWords"    # I

    .prologue
    .line 439
    add-int v15, p2, p3

    .line 440
    .local v15, "n":I
    const-wide/16 v18, 0x0

    .local v18, "tot":J
    const-wide/16 v20, 0x0

    .line 441
    .local v20, "tot8":J
    const-wide/16 v16, 0x0

    .local v16, "ones":J
    const-wide/16 v22, 0x0

    .local v22, "twos":J
    const-wide/16 v8, 0x0

    .line 444
    .local v8, "fours":J
    move/from16 v14, p2

    .local v14, "i":I
    :goto_0
    add-int/lit8 v30, v15, -0x8

    move/from16 v0, v30

    if-gt v14, v0, :cond_0

    .line 455
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v2, v30, v32

    .local v2, "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x1

    aget-wide v32, p1, v32

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v4, v30, v32

    .line 456
    .local v4, "c":J
    xor-long v28, v16, v2

    .line 457
    .local v28, "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 458
    .local v24, "twosA":J
    xor-long v16, v28, v4

    .line 462
    add-int/lit8 v30, v14, 0x2

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x2

    aget-wide v32, p1, v32

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v2, v30, v32

    add-int/lit8 v30, v14, 0x3

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x3

    aget-wide v32, p1, v32

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v4, v30, v32

    .line 463
    xor-long v28, v16, v2

    .line 464
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 465
    .local v26, "twosB":J
    xor-long v16, v28, v4

    .line 469
    xor-long v28, v22, v24

    .line 470
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v10, v30, v32

    .line 471
    .local v10, "foursA":J
    xor-long v22, v28, v26

    .line 475
    add-int/lit8 v30, v14, 0x4

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x4

    aget-wide v32, p1, v32

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v2, v30, v32

    add-int/lit8 v30, v14, 0x5

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x5

    aget-wide v32, p1, v32

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v4, v30, v32

    .line 476
    xor-long v28, v16, v2

    .line 477
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 478
    xor-long v16, v28, v4

    .line 482
    add-int/lit8 v30, v14, 0x6

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x6

    aget-wide v32, p1, v32

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v2, v30, v32

    add-int/lit8 v30, v14, 0x7

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x7

    aget-wide v32, p1, v32

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v4, v30, v32

    .line 483
    xor-long v28, v16, v2

    .line 484
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 485
    xor-long v16, v28, v4

    .line 489
    xor-long v28, v22, v24

    .line 490
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v12, v30, v32

    .line 491
    .local v12, "foursB":J
    xor-long v22, v28, v26

    .line 496
    xor-long v28, v8, v10

    .line 497
    and-long v30, v8, v10

    and-long v32, v28, v12

    or-long v6, v30, v32

    .line 498
    .local v6, "eights":J
    xor-long v8, v28, v12

    .line 500
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 444
    add-int/lit8 v14, v14, 0x8

    goto/16 :goto_0

    .line 504
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v12    # "foursB":J
    .end local v24    # "twosA":J
    .end local v26    # "twosB":J
    .end local v28    # "u":J
    :cond_0
    add-int/lit8 v30, v15, -0x4

    move/from16 v0, v30

    if-gt v14, v0, :cond_1

    .line 507
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v2, v30, v32

    .restart local v2    # "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x1

    aget-wide v32, p1, v32

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v4, v30, v32

    .line 508
    .restart local v4    # "c":J
    xor-long v28, v16, v2

    .line 509
    .restart local v28    # "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 510
    .restart local v24    # "twosA":J
    xor-long v16, v28, v4

    .line 513
    add-int/lit8 v30, v14, 0x2

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x2

    aget-wide v32, p1, v32

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v2, v30, v32

    add-int/lit8 v30, v14, 0x3

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x3

    aget-wide v32, p1, v32

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v4, v30, v32

    .line 514
    xor-long v28, v16, v2

    .line 515
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 516
    .restart local v26    # "twosB":J
    xor-long v16, v28, v4

    .line 519
    xor-long v28, v22, v24

    .line 520
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v10, v30, v32

    .line 521
    .restart local v10    # "foursA":J
    xor-long v22, v28, v26

    .line 523
    and-long v6, v8, v10

    .line 524
    .restart local v6    # "eights":J
    xor-long/2addr v8, v10

    .line 526
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 527
    add-int/lit8 v14, v14, 0x4

    .line 530
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v24    # "twosA":J
    .end local v26    # "twosB":J
    .end local v28    # "u":J
    :cond_1
    add-int/lit8 v30, v15, -0x2

    move/from16 v0, v30

    if-gt v14, v0, :cond_2

    .line 531
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v2, v30, v32

    .restart local v2    # "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x1

    aget-wide v32, p1, v32

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v4, v30, v32

    .line 532
    .restart local v4    # "c":J
    xor-long v28, v16, v2

    .line 533
    .restart local v28    # "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 534
    .restart local v24    # "twosA":J
    xor-long v16, v28, v4

    .line 536
    and-long v10, v22, v24

    .line 537
    .restart local v10    # "foursA":J
    xor-long v22, v22, v24

    .line 539
    and-long v6, v8, v10

    .line 540
    .restart local v6    # "eights":J
    xor-long/2addr v8, v10

    .line 542
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 543
    add-int/lit8 v14, v14, 0x2

    .line 546
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v24    # "twosA":J
    .end local v28    # "u":J
    :cond_2
    if-ge v14, v15, :cond_3

    .line 547
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    const-wide/16 v34, -0x1

    xor-long v32, v32, v34

    and-long v30, v30, v32

    invoke-static/range {v30 .. v31}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v18, v18, v30

    .line 550
    :cond_3
    invoke-static {v8, v9}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    shl-int/lit8 v30, v30, 0x2

    invoke-static/range {v22 .. v23}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v31

    shl-int/lit8 v31, v31, 0x1

    add-int v30, v30, v31

    invoke-static/range {v16 .. v17}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v31

    add-int v30, v30, v31

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    const/16 v32, 0x3

    shl-long v32, v20, v32

    add-long v30, v30, v32

    add-long v18, v18, v30

    .line 555
    return-wide v18
.end method

.method public static pop_array([JII)J
    .locals 34
    .param p0, "A"    # [J
    .param p1, "wordOffset"    # I
    .param p2, "numWords"    # I

    .prologue
    .line 68
    add-int v15, p1, p2

    .line 69
    .local v15, "n":I
    const-wide/16 v18, 0x0

    .local v18, "tot":J
    const-wide/16 v20, 0x0

    .line 70
    .local v20, "tot8":J
    const-wide/16 v16, 0x0

    .local v16, "ones":J
    const-wide/16 v22, 0x0

    .local v22, "twos":J
    const-wide/16 v8, 0x0

    .line 73
    .local v8, "fours":J
    move/from16 v14, p1

    .local v14, "i":I
    :goto_0
    add-int/lit8 v30, v15, -0x8

    move/from16 v0, v30

    if-gt v14, v0, :cond_0

    .line 84
    aget-wide v2, p0, v14

    .local v2, "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v4, p0, v30

    .line 85
    .local v4, "c":J
    xor-long v28, v16, v2

    .line 86
    .local v28, "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 87
    .local v24, "twosA":J
    xor-long v16, v28, v4

    .line 91
    add-int/lit8 v30, v14, 0x2

    aget-wide v2, p0, v30

    add-int/lit8 v30, v14, 0x3

    aget-wide v4, p0, v30

    .line 92
    xor-long v28, v16, v2

    .line 93
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 94
    .local v26, "twosB":J
    xor-long v16, v28, v4

    .line 98
    xor-long v28, v22, v24

    .line 99
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v10, v30, v32

    .line 100
    .local v10, "foursA":J
    xor-long v22, v28, v26

    .line 104
    add-int/lit8 v30, v14, 0x4

    aget-wide v2, p0, v30

    add-int/lit8 v30, v14, 0x5

    aget-wide v4, p0, v30

    .line 105
    xor-long v28, v16, v2

    .line 106
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 107
    xor-long v16, v28, v4

    .line 111
    add-int/lit8 v30, v14, 0x6

    aget-wide v2, p0, v30

    add-int/lit8 v30, v14, 0x7

    aget-wide v4, p0, v30

    .line 112
    xor-long v28, v16, v2

    .line 113
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 114
    xor-long v16, v28, v4

    .line 118
    xor-long v28, v22, v24

    .line 119
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v12, v30, v32

    .line 120
    .local v12, "foursB":J
    xor-long v22, v28, v26

    .line 125
    xor-long v28, v8, v10

    .line 126
    and-long v30, v8, v10

    and-long v32, v28, v12

    or-long v6, v30, v32

    .line 127
    .local v6, "eights":J
    xor-long v8, v28, v12

    .line 129
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 73
    add-int/lit8 v14, v14, 0x8

    goto :goto_0

    .line 138
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v12    # "foursB":J
    .end local v24    # "twosA":J
    .end local v26    # "twosB":J
    .end local v28    # "u":J
    :cond_0
    add-int/lit8 v30, v15, -0x4

    move/from16 v0, v30

    if-gt v14, v0, :cond_1

    .line 141
    aget-wide v2, p0, v14

    .restart local v2    # "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v4, p0, v30

    .line 142
    .restart local v4    # "c":J
    xor-long v28, v16, v2

    .line 143
    .restart local v28    # "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 144
    .restart local v24    # "twosA":J
    xor-long v16, v28, v4

    .line 147
    add-int/lit8 v30, v14, 0x2

    aget-wide v2, p0, v30

    add-int/lit8 v30, v14, 0x3

    aget-wide v4, p0, v30

    .line 148
    xor-long v28, v16, v2

    .line 149
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 150
    .restart local v26    # "twosB":J
    xor-long v16, v28, v4

    .line 153
    xor-long v28, v22, v24

    .line 154
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v10, v30, v32

    .line 155
    .restart local v10    # "foursA":J
    xor-long v22, v28, v26

    .line 157
    and-long v6, v8, v10

    .line 158
    .restart local v6    # "eights":J
    xor-long/2addr v8, v10

    .line 160
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 161
    add-int/lit8 v14, v14, 0x4

    .line 164
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v24    # "twosA":J
    .end local v26    # "twosB":J
    .end local v28    # "u":J
    :cond_1
    add-int/lit8 v30, v15, -0x2

    move/from16 v0, v30

    if-gt v14, v0, :cond_2

    .line 165
    aget-wide v2, p0, v14

    .restart local v2    # "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v4, p0, v30

    .line 166
    .restart local v4    # "c":J
    xor-long v28, v16, v2

    .line 167
    .restart local v28    # "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 168
    .restart local v24    # "twosA":J
    xor-long v16, v28, v4

    .line 170
    and-long v10, v22, v24

    .line 171
    .restart local v10    # "foursA":J
    xor-long v22, v22, v24

    .line 173
    and-long v6, v8, v10

    .line 174
    .restart local v6    # "eights":J
    xor-long/2addr v8, v10

    .line 176
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 177
    add-int/lit8 v14, v14, 0x2

    .line 180
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v24    # "twosA":J
    .end local v28    # "u":J
    :cond_2
    if-ge v14, v15, :cond_3

    .line 181
    aget-wide v30, p0, v14

    invoke-static/range {v30 .. v31}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v18, v18, v30

    .line 184
    :cond_3
    invoke-static {v8, v9}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    shl-int/lit8 v30, v30, 0x2

    invoke-static/range {v22 .. v23}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v31

    shl-int/lit8 v31, v31, 0x1

    add-int v30, v30, v31

    invoke-static/range {v16 .. v17}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v31

    add-int v30, v30, v31

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    const/16 v32, 0x3

    shl-long v32, v20, v32

    add-long v30, v30, v32

    add-long v18, v18, v30

    .line 189
    return-wide v18
.end method

.method public static pop_intersect([J[JII)J
    .locals 34
    .param p0, "A"    # [J
    .param p1, "B"    # [J
    .param p2, "wordOffset"    # I
    .param p3, "numWords"    # I

    .prologue
    .line 197
    add-int v15, p2, p3

    .line 198
    .local v15, "n":I
    const-wide/16 v18, 0x0

    .local v18, "tot":J
    const-wide/16 v20, 0x0

    .line 199
    .local v20, "tot8":J
    const-wide/16 v16, 0x0

    .local v16, "ones":J
    const-wide/16 v22, 0x0

    .local v22, "twos":J
    const-wide/16 v8, 0x0

    .line 202
    .local v8, "fours":J
    move/from16 v14, p2

    .local v14, "i":I
    :goto_0
    add-int/lit8 v30, v15, -0x8

    move/from16 v0, v30

    if-gt v14, v0, :cond_0

    .line 207
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    and-long v2, v30, v32

    .local v2, "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x1

    aget-wide v32, p1, v32

    and-long v4, v30, v32

    .line 208
    .local v4, "c":J
    xor-long v28, v16, v2

    .line 209
    .local v28, "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 210
    .local v24, "twosA":J
    xor-long v16, v28, v4

    .line 214
    add-int/lit8 v30, v14, 0x2

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x2

    aget-wide v32, p1, v32

    and-long v2, v30, v32

    add-int/lit8 v30, v14, 0x3

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x3

    aget-wide v32, p1, v32

    and-long v4, v30, v32

    .line 215
    xor-long v28, v16, v2

    .line 216
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 217
    .local v26, "twosB":J
    xor-long v16, v28, v4

    .line 221
    xor-long v28, v22, v24

    .line 222
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v10, v30, v32

    .line 223
    .local v10, "foursA":J
    xor-long v22, v28, v26

    .line 227
    add-int/lit8 v30, v14, 0x4

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x4

    aget-wide v32, p1, v32

    and-long v2, v30, v32

    add-int/lit8 v30, v14, 0x5

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x5

    aget-wide v32, p1, v32

    and-long v4, v30, v32

    .line 228
    xor-long v28, v16, v2

    .line 229
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 230
    xor-long v16, v28, v4

    .line 234
    add-int/lit8 v30, v14, 0x6

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x6

    aget-wide v32, p1, v32

    and-long v2, v30, v32

    add-int/lit8 v30, v14, 0x7

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x7

    aget-wide v32, p1, v32

    and-long v4, v30, v32

    .line 235
    xor-long v28, v16, v2

    .line 236
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 237
    xor-long v16, v28, v4

    .line 241
    xor-long v28, v22, v24

    .line 242
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v12, v30, v32

    .line 243
    .local v12, "foursB":J
    xor-long v22, v28, v26

    .line 248
    xor-long v28, v8, v10

    .line 249
    and-long v30, v8, v10

    and-long v32, v28, v12

    or-long v6, v30, v32

    .line 250
    .local v6, "eights":J
    xor-long v8, v28, v12

    .line 252
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 202
    add-int/lit8 v14, v14, 0x8

    goto/16 :goto_0

    .line 256
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v12    # "foursB":J
    .end local v24    # "twosA":J
    .end local v26    # "twosB":J
    .end local v28    # "u":J
    :cond_0
    add-int/lit8 v30, v15, -0x4

    move/from16 v0, v30

    if-gt v14, v0, :cond_1

    .line 259
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    and-long v2, v30, v32

    .restart local v2    # "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x1

    aget-wide v32, p1, v32

    and-long v4, v30, v32

    .line 260
    .restart local v4    # "c":J
    xor-long v28, v16, v2

    .line 261
    .restart local v28    # "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 262
    .restart local v24    # "twosA":J
    xor-long v16, v28, v4

    .line 265
    add-int/lit8 v30, v14, 0x2

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x2

    aget-wide v32, p1, v32

    and-long v2, v30, v32

    add-int/lit8 v30, v14, 0x3

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x3

    aget-wide v32, p1, v32

    and-long v4, v30, v32

    .line 266
    xor-long v28, v16, v2

    .line 267
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 268
    .restart local v26    # "twosB":J
    xor-long v16, v28, v4

    .line 271
    xor-long v28, v22, v24

    .line 272
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v10, v30, v32

    .line 273
    .restart local v10    # "foursA":J
    xor-long v22, v28, v26

    .line 275
    and-long v6, v8, v10

    .line 276
    .restart local v6    # "eights":J
    xor-long/2addr v8, v10

    .line 278
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 279
    add-int/lit8 v14, v14, 0x4

    .line 282
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v24    # "twosA":J
    .end local v26    # "twosB":J
    .end local v28    # "u":J
    :cond_1
    add-int/lit8 v30, v15, -0x2

    move/from16 v0, v30

    if-gt v14, v0, :cond_2

    .line 283
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    and-long v2, v30, v32

    .restart local v2    # "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x1

    aget-wide v32, p1, v32

    and-long v4, v30, v32

    .line 284
    .restart local v4    # "c":J
    xor-long v28, v16, v2

    .line 285
    .restart local v28    # "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 286
    .restart local v24    # "twosA":J
    xor-long v16, v28, v4

    .line 288
    and-long v10, v22, v24

    .line 289
    .restart local v10    # "foursA":J
    xor-long v22, v22, v24

    .line 291
    and-long v6, v8, v10

    .line 292
    .restart local v6    # "eights":J
    xor-long/2addr v8, v10

    .line 294
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 295
    add-int/lit8 v14, v14, 0x2

    .line 298
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v24    # "twosA":J
    .end local v28    # "u":J
    :cond_2
    if-ge v14, v15, :cond_3

    .line 299
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    and-long v30, v30, v32

    invoke-static/range {v30 .. v31}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v18, v18, v30

    .line 302
    :cond_3
    invoke-static {v8, v9}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    shl-int/lit8 v30, v30, 0x2

    invoke-static/range {v22 .. v23}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v31

    shl-int/lit8 v31, v31, 0x1

    add-int v30, v30, v31

    invoke-static/range {v16 .. v17}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v31

    add-int v30, v30, v31

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    const/16 v32, 0x3

    shl-long v32, v20, v32

    add-long v30, v30, v32

    add-long v18, v18, v30

    .line 307
    return-wide v18
.end method

.method public static pop_union([J[JII)J
    .locals 34
    .param p0, "A"    # [J
    .param p1, "B"    # [J
    .param p2, "wordOffset"    # I
    .param p3, "numWords"    # I

    .prologue
    .line 315
    add-int v15, p2, p3

    .line 316
    .local v15, "n":I
    const-wide/16 v18, 0x0

    .local v18, "tot":J
    const-wide/16 v20, 0x0

    .line 317
    .local v20, "tot8":J
    const-wide/16 v16, 0x0

    .local v16, "ones":J
    const-wide/16 v22, 0x0

    .local v22, "twos":J
    const-wide/16 v8, 0x0

    .line 320
    .local v8, "fours":J
    move/from16 v14, p2

    .local v14, "i":I
    :goto_0
    add-int/lit8 v30, v15, -0x8

    move/from16 v0, v30

    if-gt v14, v0, :cond_0

    .line 331
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    or-long v2, v30, v32

    .local v2, "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x1

    aget-wide v32, p1, v32

    or-long v4, v30, v32

    .line 332
    .local v4, "c":J
    xor-long v28, v16, v2

    .line 333
    .local v28, "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 334
    .local v24, "twosA":J
    xor-long v16, v28, v4

    .line 338
    add-int/lit8 v30, v14, 0x2

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x2

    aget-wide v32, p1, v32

    or-long v2, v30, v32

    add-int/lit8 v30, v14, 0x3

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x3

    aget-wide v32, p1, v32

    or-long v4, v30, v32

    .line 339
    xor-long v28, v16, v2

    .line 340
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 341
    .local v26, "twosB":J
    xor-long v16, v28, v4

    .line 345
    xor-long v28, v22, v24

    .line 346
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v10, v30, v32

    .line 347
    .local v10, "foursA":J
    xor-long v22, v28, v26

    .line 351
    add-int/lit8 v30, v14, 0x4

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x4

    aget-wide v32, p1, v32

    or-long v2, v30, v32

    add-int/lit8 v30, v14, 0x5

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x5

    aget-wide v32, p1, v32

    or-long v4, v30, v32

    .line 352
    xor-long v28, v16, v2

    .line 353
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 354
    xor-long v16, v28, v4

    .line 358
    add-int/lit8 v30, v14, 0x6

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x6

    aget-wide v32, p1, v32

    or-long v2, v30, v32

    add-int/lit8 v30, v14, 0x7

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x7

    aget-wide v32, p1, v32

    or-long v4, v30, v32

    .line 359
    xor-long v28, v16, v2

    .line 360
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 361
    xor-long v16, v28, v4

    .line 365
    xor-long v28, v22, v24

    .line 366
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v12, v30, v32

    .line 367
    .local v12, "foursB":J
    xor-long v22, v28, v26

    .line 372
    xor-long v28, v8, v10

    .line 373
    and-long v30, v8, v10

    and-long v32, v28, v12

    or-long v6, v30, v32

    .line 374
    .local v6, "eights":J
    xor-long v8, v28, v12

    .line 376
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 320
    add-int/lit8 v14, v14, 0x8

    goto/16 :goto_0

    .line 380
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v12    # "foursB":J
    .end local v24    # "twosA":J
    .end local v26    # "twosB":J
    .end local v28    # "u":J
    :cond_0
    add-int/lit8 v30, v15, -0x4

    move/from16 v0, v30

    if-gt v14, v0, :cond_1

    .line 383
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    or-long v2, v30, v32

    .restart local v2    # "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x1

    aget-wide v32, p1, v32

    or-long v4, v30, v32

    .line 384
    .restart local v4    # "c":J
    xor-long v28, v16, v2

    .line 385
    .restart local v28    # "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 386
    .restart local v24    # "twosA":J
    xor-long v16, v28, v4

    .line 389
    add-int/lit8 v30, v14, 0x2

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x2

    aget-wide v32, p1, v32

    or-long v2, v30, v32

    add-int/lit8 v30, v14, 0x3

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x3

    aget-wide v32, p1, v32

    or-long v4, v30, v32

    .line 390
    xor-long v28, v16, v2

    .line 391
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 392
    .restart local v26    # "twosB":J
    xor-long v16, v28, v4

    .line 395
    xor-long v28, v22, v24

    .line 396
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v10, v30, v32

    .line 397
    .restart local v10    # "foursA":J
    xor-long v22, v28, v26

    .line 399
    and-long v6, v8, v10

    .line 400
    .restart local v6    # "eights":J
    xor-long/2addr v8, v10

    .line 402
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 403
    add-int/lit8 v14, v14, 0x4

    .line 406
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v24    # "twosA":J
    .end local v26    # "twosB":J
    .end local v28    # "u":J
    :cond_1
    add-int/lit8 v30, v15, -0x2

    move/from16 v0, v30

    if-gt v14, v0, :cond_2

    .line 407
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    or-long v2, v30, v32

    .restart local v2    # "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x1

    aget-wide v32, p1, v32

    or-long v4, v30, v32

    .line 408
    .restart local v4    # "c":J
    xor-long v28, v16, v2

    .line 409
    .restart local v28    # "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 410
    .restart local v24    # "twosA":J
    xor-long v16, v28, v4

    .line 412
    and-long v10, v22, v24

    .line 413
    .restart local v10    # "foursA":J
    xor-long v22, v22, v24

    .line 415
    and-long v6, v8, v10

    .line 416
    .restart local v6    # "eights":J
    xor-long/2addr v8, v10

    .line 418
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 419
    add-int/lit8 v14, v14, 0x2

    .line 422
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v24    # "twosA":J
    .end local v28    # "u":J
    :cond_2
    if-ge v14, v15, :cond_3

    .line 423
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    or-long v30, v30, v32

    invoke-static/range {v30 .. v31}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v18, v18, v30

    .line 426
    :cond_3
    invoke-static {v8, v9}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    shl-int/lit8 v30, v30, 0x2

    invoke-static/range {v22 .. v23}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v31

    shl-int/lit8 v31, v31, 0x1

    add-int v30, v30, v31

    invoke-static/range {v16 .. v17}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v31

    add-int v30, v30, v31

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    const/16 v32, 0x3

    shl-long v32, v20, v32

    add-long v30, v30, v32

    add-long v18, v18, v30

    .line 431
    return-wide v18
.end method

.method public static pop_xor([J[JII)J
    .locals 34
    .param p0, "A"    # [J
    .param p1, "B"    # [J
    .param p2, "wordOffset"    # I
    .param p3, "numWords"    # I

    .prologue
    .line 559
    add-int v15, p2, p3

    .line 560
    .local v15, "n":I
    const-wide/16 v18, 0x0

    .local v18, "tot":J
    const-wide/16 v20, 0x0

    .line 561
    .local v20, "tot8":J
    const-wide/16 v16, 0x0

    .local v16, "ones":J
    const-wide/16 v22, 0x0

    .local v22, "twos":J
    const-wide/16 v8, 0x0

    .line 564
    .local v8, "fours":J
    move/from16 v14, p2

    .local v14, "i":I
    :goto_0
    add-int/lit8 v30, v15, -0x8

    move/from16 v0, v30

    if-gt v14, v0, :cond_0

    .line 575
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    xor-long v2, v30, v32

    .local v2, "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x1

    aget-wide v32, p1, v32

    xor-long v4, v30, v32

    .line 576
    .local v4, "c":J
    xor-long v28, v16, v2

    .line 577
    .local v28, "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 578
    .local v24, "twosA":J
    xor-long v16, v28, v4

    .line 582
    add-int/lit8 v30, v14, 0x2

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x2

    aget-wide v32, p1, v32

    xor-long v2, v30, v32

    add-int/lit8 v30, v14, 0x3

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x3

    aget-wide v32, p1, v32

    xor-long v4, v30, v32

    .line 583
    xor-long v28, v16, v2

    .line 584
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 585
    .local v26, "twosB":J
    xor-long v16, v28, v4

    .line 589
    xor-long v28, v22, v24

    .line 590
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v10, v30, v32

    .line 591
    .local v10, "foursA":J
    xor-long v22, v28, v26

    .line 595
    add-int/lit8 v30, v14, 0x4

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x4

    aget-wide v32, p1, v32

    xor-long v2, v30, v32

    add-int/lit8 v30, v14, 0x5

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x5

    aget-wide v32, p1, v32

    xor-long v4, v30, v32

    .line 596
    xor-long v28, v16, v2

    .line 597
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 598
    xor-long v16, v28, v4

    .line 602
    add-int/lit8 v30, v14, 0x6

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x6

    aget-wide v32, p1, v32

    xor-long v2, v30, v32

    add-int/lit8 v30, v14, 0x7

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x7

    aget-wide v32, p1, v32

    xor-long v4, v30, v32

    .line 603
    xor-long v28, v16, v2

    .line 604
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 605
    xor-long v16, v28, v4

    .line 609
    xor-long v28, v22, v24

    .line 610
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v12, v30, v32

    .line 611
    .local v12, "foursB":J
    xor-long v22, v28, v26

    .line 616
    xor-long v28, v8, v10

    .line 617
    and-long v30, v8, v10

    and-long v32, v28, v12

    or-long v6, v30, v32

    .line 618
    .local v6, "eights":J
    xor-long v8, v28, v12

    .line 620
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 564
    add-int/lit8 v14, v14, 0x8

    goto/16 :goto_0

    .line 624
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v12    # "foursB":J
    .end local v24    # "twosA":J
    .end local v26    # "twosB":J
    .end local v28    # "u":J
    :cond_0
    add-int/lit8 v30, v15, -0x4

    move/from16 v0, v30

    if-gt v14, v0, :cond_1

    .line 627
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    xor-long v2, v30, v32

    .restart local v2    # "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x1

    aget-wide v32, p1, v32

    xor-long v4, v30, v32

    .line 628
    .restart local v4    # "c":J
    xor-long v28, v16, v2

    .line 629
    .restart local v28    # "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 630
    .restart local v24    # "twosA":J
    xor-long v16, v28, v4

    .line 633
    add-int/lit8 v30, v14, 0x2

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x2

    aget-wide v32, p1, v32

    xor-long v2, v30, v32

    add-int/lit8 v30, v14, 0x3

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x3

    aget-wide v32, p1, v32

    xor-long v4, v30, v32

    .line 634
    xor-long v28, v16, v2

    .line 635
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v26, v30, v32

    .line 636
    .restart local v26    # "twosB":J
    xor-long v16, v28, v4

    .line 639
    xor-long v28, v22, v24

    .line 640
    and-long v30, v22, v24

    and-long v32, v28, v26

    or-long v10, v30, v32

    .line 641
    .restart local v10    # "foursA":J
    xor-long v22, v28, v26

    .line 643
    and-long v6, v8, v10

    .line 644
    .restart local v6    # "eights":J
    xor-long/2addr v8, v10

    .line 646
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 647
    add-int/lit8 v14, v14, 0x4

    .line 650
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v24    # "twosA":J
    .end local v26    # "twosB":J
    .end local v28    # "u":J
    :cond_1
    add-int/lit8 v30, v15, -0x2

    move/from16 v0, v30

    if-gt v14, v0, :cond_2

    .line 651
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    xor-long v2, v30, v32

    .restart local v2    # "b":J
    add-int/lit8 v30, v14, 0x1

    aget-wide v30, p0, v30

    add-int/lit8 v32, v14, 0x1

    aget-wide v32, p1, v32

    xor-long v4, v30, v32

    .line 652
    .restart local v4    # "c":J
    xor-long v28, v16, v2

    .line 653
    .restart local v28    # "u":J
    and-long v30, v16, v2

    and-long v32, v28, v4

    or-long v24, v30, v32

    .line 654
    .restart local v24    # "twosA":J
    xor-long v16, v28, v4

    .line 656
    and-long v10, v22, v24

    .line 657
    .restart local v10    # "foursA":J
    xor-long v22, v22, v24

    .line 659
    and-long v6, v8, v10

    .line 660
    .restart local v6    # "eights":J
    xor-long/2addr v8, v10

    .line 662
    invoke-static {v6, v7}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 663
    add-int/lit8 v14, v14, 0x2

    .line 666
    .end local v2    # "b":J
    .end local v4    # "c":J
    .end local v6    # "eights":J
    .end local v10    # "foursA":J
    .end local v24    # "twosA":J
    .end local v28    # "u":J
    :cond_2
    if-ge v14, v15, :cond_3

    .line 667
    aget-wide v30, p0, v14

    aget-wide v32, p1, v14

    xor-long v30, v30, v32

    invoke-static/range {v30 .. v31}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v18, v18, v30

    .line 670
    :cond_3
    invoke-static {v8, v9}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v30

    shl-int/lit8 v30, v30, 0x2

    invoke-static/range {v22 .. v23}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v31

    shl-int/lit8 v31, v31, 0x1

    add-int v30, v30, v31

    invoke-static/range {v16 .. v17}, Lorg/apache/lucene/util/BitUtil;->pop(J)I

    move-result v31

    add-int v30, v30, v31

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    const/16 v32, 0x3

    shl-long v32, v20, v32

    add-long v30, v30, v32

    add-long v18, v18, v30

    .line 675
    return-wide v18
.end method
