.class public abstract Lorg/apache/lucene/util/packed/PackedInts$Writer;
.super Ljava/lang/Object;
.source "PackedInts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Writer"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final bitsPerValue:I

.field protected final out:Lorg/apache/lucene/store/DataOutput;

.field protected final valueCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 143
    const-class v0, Lorg/apache/lucene/util/packed/PackedInts;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lorg/apache/lucene/store/DataOutput;II)V
    .locals 2
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p2, "valueCount"    # I
    .param p3, "bitsPerValue"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const/16 v0, 0x40

    if-le p3, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 152
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->out:Lorg/apache/lucene/store/DataOutput;

    .line 153
    iput p2, p0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->valueCount:I

    .line 154
    iput p3, p0, Lorg/apache/lucene/util/packed/PackedInts$Writer;->bitsPerValue:I

    .line 155
    const-string/jumbo v0, "PackedInts"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lorg/apache/lucene/util/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)Lorg/apache/lucene/store/DataOutput;

    .line 156
    invoke-virtual {p1, p3}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 157
    invoke-virtual {p1, p2}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 158
    return-void
.end method


# virtual methods
.method public abstract add(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract finish()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
