.class public abstract Lorg/apache/lucene/util/StringHelper;
.super Ljava/lang/Object;
.source "StringHelper.java"


# static fields
.field public static interner:Lorg/apache/lucene/util/StringInterner;

.field private static versionComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    new-instance v0, Lorg/apache/lucene/util/SimpleStringInterner;

    const/16 v1, 0x400

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/SimpleStringInterner;-><init>(II)V

    sput-object v0, Lorg/apache/lucene/util/StringHelper;->interner:Lorg/apache/lucene/util/StringInterner;

    .line 69
    new-instance v0, Lorg/apache/lucene/util/StringHelper$1;

    invoke-direct {v0}, Lorg/apache/lucene/util/StringHelper$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/util/StringHelper;->versionComparator:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    return-void
.end method

.method public static final bytesDifference([BI[BI)I
    .locals 4
    .param p0, "bytes1"    # [B
    .param p1, "len1"    # I
    .param p2, "bytes2"    # [B
    .param p3, "len2"    # I

    .prologue
    .line 51
    if-ge p1, p3, :cond_0

    move v1, p1

    .line 52
    .local v1, "len":I
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 53
    aget-byte v2, p0, v0

    aget-byte v3, p2, v0

    if-eq v2, v3, :cond_1

    .line 55
    .end local v0    # "i":I
    :goto_2
    return v0

    .end local v1    # "len":I
    :cond_0
    move v1, p3

    .line 51
    goto :goto_0

    .line 52
    .restart local v0    # "i":I
    .restart local v1    # "len":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 55
    goto :goto_2
.end method

.method public static getVersionComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    sget-object v0, Lorg/apache/lucene/util/StringHelper;->versionComparator:Ljava/util/Comparator;

    return-object v0
.end method

.method public static intern(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 39
    sget-object v0, Lorg/apache/lucene/util/StringHelper;->interner:Lorg/apache/lucene/util/StringInterner;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/StringInterner;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
