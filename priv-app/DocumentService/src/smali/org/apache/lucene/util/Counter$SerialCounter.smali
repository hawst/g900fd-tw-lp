.class final Lorg/apache/lucene/util/Counter$SerialCounter;
.super Lorg/apache/lucene/util/Counter;
.source "Counter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/Counter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SerialCounter"
.end annotation


# instance fields
.field private count:J


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Lorg/apache/lucene/util/Counter;-><init>()V

    .line 66
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/util/Counter$SerialCounter;->count:J

    .line 76
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/Counter$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/util/Counter$1;

    .prologue
    .line 65
    invoke-direct {p0}, Lorg/apache/lucene/util/Counter$SerialCounter;-><init>()V

    return-void
.end method


# virtual methods
.method public addAndGet(J)J
    .locals 3
    .param p1, "delta"    # J

    .prologue
    .line 70
    iget-wide v0, p0, Lorg/apache/lucene/util/Counter$SerialCounter;->count:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/util/Counter$SerialCounter;->count:J

    return-wide v0
.end method

.method public get()J
    .locals 2

    .prologue
    .line 75
    iget-wide v0, p0, Lorg/apache/lucene/util/Counter$SerialCounter;->count:J

    return-wide v0
.end method
