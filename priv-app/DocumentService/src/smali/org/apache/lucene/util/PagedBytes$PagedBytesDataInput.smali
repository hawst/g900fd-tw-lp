.class public final Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;
.super Lorg/apache/lucene/store/DataInput;
.source "PagedBytes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/PagedBytes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "PagedBytesDataInput"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private currentBlock:[B

.field private currentBlockIndex:I

.field private currentBlockUpto:I

.field final synthetic this$0:Lorg/apache/lucene/util/PagedBytes;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 396
    const-class v0, Lorg/apache/lucene/util/PagedBytes;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/util/PagedBytes;)V
    .locals 2

    .prologue
    .line 401
    iput-object p1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    invoke-direct {p0}, Lorg/apache/lucene/store/DataInput;-><init>()V

    .line 402
    # getter for: Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;
    invoke-static {p1}, Lorg/apache/lucene/util/PagedBytes;->access$000(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlock:[B

    .line 403
    return-void
.end method

.method private nextBlock()V
    .locals 2

    .prologue
    .line 458
    iget v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockIndex:I

    .line 459
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockUpto:I

    .line 460
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$000(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlock:[B

    .line 461
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 407
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    invoke-virtual {v1}, Lorg/apache/lucene/util/PagedBytes;->getDataInput()Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    move-result-object v0

    .line 408
    .local v0, "clone":Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;
    invoke-virtual {p0}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->getPosition()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->setPosition(J)V

    .line 409
    return-object v0
.end method

.method public getPosition()J
    .locals 2

    .prologue
    .line 414
    iget v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockIndex:I

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v1}, Lorg/apache/lucene/util/PagedBytes;->access$400(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    mul-int/2addr v0, v1

    iget v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockUpto:I

    add-int/2addr v0, v1

    int-to-long v0, v0

    return-wide v0
.end method

.method public readByte()B
    .locals 3

    .prologue
    .line 427
    iget v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockUpto:I

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v1}, Lorg/apache/lucene/util/PagedBytes;->access$400(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 428
    invoke-direct {p0}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->nextBlock()V

    .line 430
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlock:[B

    iget v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockUpto:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockUpto:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public readBytes([BII)V
    .locals 5
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 435
    sget-boolean v3, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    array-length v3, p1

    add-int v4, p2, p3

    if-ge v3, v4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 436
    :cond_0
    add-int v2, p2, p3

    .line 438
    .local v2, "offsetEnd":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$400(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockUpto:I

    sub-int v0, v3, v4

    .line 439
    .local v0, "blockLeft":I
    sub-int v1, v2, p2

    .line 440
    .local v1, "left":I
    if-ge v0, v1, :cond_1

    .line 441
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlock:[B

    iget v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockUpto:I

    invoke-static {v3, v4, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 444
    invoke-direct {p0}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->nextBlock()V

    .line 445
    add-int/2addr p2, v0

    goto :goto_0

    .line 448
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlock:[B

    iget v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockUpto:I

    invoke-static {v3, v4, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 451
    iget v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockUpto:I

    add-int/2addr v3, v1

    iput v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockUpto:I

    .line 455
    return-void
.end method

.method public setPosition(J)V
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 420
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockBits:I
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$200(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v0

    shr-long v0, p1, v0

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockIndex:I

    .line 421
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$000(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlock:[B

    .line 422
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockMask:I
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$300(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v0

    int-to-long v0, v0

    and-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->currentBlockUpto:I

    .line 423
    return-void
.end method
