.class public Lorg/apache/lucene/util/CloseableThreadLocal;
.super Ljava/lang/Object;
.source "CloseableThreadLocal.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Closeable;"
    }
.end annotation


# static fields
.field private static PURGE_MULTIPLIER:I


# instance fields
.field private final countUntilPurge:Ljava/util/concurrent/atomic/AtomicInteger;

.field private hardRefs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Thread;",
            "TT;>;"
        }
    .end annotation
.end field

.field private t:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const/16 v0, 0x14

    sput v0, Lorg/apache/lucene/util/CloseableThreadLocal;->PURGE_MULTIPLIER:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 56
    .local p0, "this":Lorg/apache/lucene/util/CloseableThreadLocal;, "Lorg/apache/lucene/util/CloseableThreadLocal<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->t:Ljava/lang/ThreadLocal;

    .line 62
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->hardRefs:Ljava/util/Map;

    .line 71
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    sget v1, Lorg/apache/lucene/util/CloseableThreadLocal;->PURGE_MULTIPLIER:I

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->countUntilPurge:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method private maybePurge()V
    .locals 1

    .prologue
    .line 104
    .local p0, "this":Lorg/apache/lucene/util/CloseableThreadLocal;, "Lorg/apache/lucene/util/CloseableThreadLocal<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->countUntilPurge:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndDecrement()I

    move-result v0

    if-nez v0, :cond_0

    .line 105
    invoke-direct {p0}, Lorg/apache/lucene/util/CloseableThreadLocal;->purge()V

    .line 107
    :cond_0
    return-void
.end method

.method private purge()V
    .locals 7

    .prologue
    .line 111
    .local p0, "this":Lorg/apache/lucene/util/CloseableThreadLocal;, "Lorg/apache/lucene/util/CloseableThreadLocal<TT;>;"
    iget-object v5, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->hardRefs:Ljava/util/Map;

    monitor-enter v5

    .line 112
    const/4 v2, 0x0

    .line 113
    .local v2, "stillAliveCount":I
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->hardRefs:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Thread;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 114
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Thread;

    .line 115
    .local v3, "t":Ljava/lang/Thread;
    invoke-virtual {v3}, Ljava/lang/Thread;->isAlive()Z

    move-result v4

    if-nez v4, :cond_0

    .line 116
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 128
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Thread;>;"
    .end local v3    # "t":Ljava/lang/Thread;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 118
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Thread;>;"
    .restart local v3    # "t":Ljava/lang/Thread;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 121
    .end local v3    # "t":Ljava/lang/Thread;
    :cond_1
    add-int/lit8 v4, v2, 0x1

    :try_start_1
    sget v6, Lorg/apache/lucene/util/CloseableThreadLocal;->PURGE_MULTIPLIER:I

    mul-int v1, v4, v6

    .line 122
    .local v1, "nextCount":I
    if-gtz v1, :cond_2

    .line 124
    const v1, 0xf4240

    .line 127
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->countUntilPurge:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 128
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 129
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/CloseableThreadLocal;, "Lorg/apache/lucene/util/CloseableThreadLocal<TT;>;"
    const/4 v1, 0x0

    .line 135
    iput-object v1, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->hardRefs:Ljava/util/Map;

    .line 138
    iget-object v0, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->t:Ljava/lang/ThreadLocal;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->t:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    .line 141
    :cond_0
    iput-object v1, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->t:Ljava/lang/ThreadLocal;

    .line 142
    return-void
.end method

.method public get()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 78
    .local p0, "this":Lorg/apache/lucene/util/CloseableThreadLocal;, "Lorg/apache/lucene/util/CloseableThreadLocal<TT;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->t:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 79
    .local v1, "weakRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<TT;>;"
    if-nez v1, :cond_1

    .line 80
    invoke-virtual {p0}, Lorg/apache/lucene/util/CloseableThreadLocal;->initialValue()Ljava/lang/Object;

    move-result-object v0

    .line 81
    .local v0, "iv":Ljava/lang/Object;, "TT;"
    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/CloseableThreadLocal;->set(Ljava/lang/Object;)V

    .line 89
    .end local v0    # "iv":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object v0

    .line 85
    .restart local v0    # "iv":Ljava/lang/Object;, "TT;"
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 88
    .end local v0    # "iv":Ljava/lang/Object;, "TT;"
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/util/CloseableThreadLocal;->maybePurge()V

    .line 89
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method protected initialValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "this":Lorg/apache/lucene/util/CloseableThreadLocal;, "Lorg/apache/lucene/util/CloseableThreadLocal<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public set(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 95
    .local p0, "this":Lorg/apache/lucene/util/CloseableThreadLocal;, "Lorg/apache/lucene/util/CloseableThreadLocal<TT;>;"
    .local p1, "object":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->t:Ljava/lang/ThreadLocal;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 97
    iget-object v1, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->hardRefs:Ljava/util/Map;

    monitor-enter v1

    .line 98
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/util/CloseableThreadLocal;->hardRefs:Ljava/util/Map;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    invoke-direct {p0}, Lorg/apache/lucene/util/CloseableThreadLocal;->maybePurge()V

    .line 100
    monitor-exit v1

    .line 101
    return-void

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
