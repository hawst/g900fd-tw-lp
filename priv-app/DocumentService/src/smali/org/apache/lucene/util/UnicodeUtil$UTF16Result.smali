.class public final Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;
.super Ljava/lang/Object;
.source "UnicodeUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/UnicodeUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UTF16Result"
.end annotation


# instance fields
.field public length:I

.field public offsets:[I

.field public result:[C


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    new-array v0, v1, [C

    iput-object v0, p0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    .line 140
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->offsets:[I

    return-void
.end method


# virtual methods
.method public copyText(Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;)V
    .locals 4
    .param p1, "other"    # Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    .prologue
    const/4 v3, 0x0

    .line 151
    iget v0, p1, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->setLength(I)V

    .line 152
    iget-object v0, p1, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    iget-object v1, p0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    iget v2, p0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 153
    return-void
.end method

.method public setLength(I)V
    .locals 1
    .param p1, "newLength"    # I

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    array-length v0, v0

    if-ge v0, p1, :cond_0

    .line 145
    iget-object v0, p0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    invoke-static {v0, p1}, Lorg/apache/lucene/util/ArrayUtil;->grow([CI)[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    .line 147
    :cond_0
    iput p1, p0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    .line 148
    return-void
.end method
