.class public Lorg/apache/lucene/util/fst/PairOutputs;
.super Lorg/apache/lucene/util/fst/Outputs;
.source "PairOutputs.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/fst/PairOutputs$1;,
        Lorg/apache/lucene/util/fst/PairOutputs$Pair;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "B:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/lucene/util/fst/Outputs",
        "<",
        "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
        "<TA;TB;>;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final NO_OUTPUT:Lorg/apache/lucene/util/fst/PairOutputs$Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;"
        }
    .end annotation
.end field

.field private final outputs1:Lorg/apache/lucene/util/fst/Outputs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TA;>;"
        }
    .end annotation
.end field

.field private final outputs2:Lorg/apache/lucene/util/fst/Outputs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TB;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/util/fst/PairOutputs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/PairOutputs;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/fst/Outputs;Lorg/apache/lucene/util/fst/Outputs;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TA;>;",
            "Lorg/apache/lucene/util/fst/Outputs",
            "<TB;>;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    .local p1, "outputs1":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TA;>;"
    .local p2, "outputs2":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<TB;>;"
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/Outputs;-><init>()V

    .line 67
    iput-object p1, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs1:Lorg/apache/lucene/util/fst/Outputs;

    .line 68
    iput-object p2, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs2:Lorg/apache/lucene/util/fst/Outputs;

    .line 69
    new-instance v0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p2}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/fst/PairOutputs$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/lucene/util/fst/PairOutputs$1;)V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/PairOutputs;->NO_OUTPUT:Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .line 70
    return-void
.end method

.method private valid(Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    .local p1, "pair":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<TA;TB;>;"
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 92
    iget-object v4, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    iget-object v5, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs1:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 93
    .local v0, "noOutput1":Z
    iget-object v4, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    iget-object v5, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs2:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 95
    .local v1, "noOutput2":Z
    if-eqz v0, :cond_1

    iget-object v4, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    iget-object v5, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs1:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v5

    if-eq v4, v5, :cond_1

    .line 110
    :cond_0
    :goto_0
    return v2

    .line 99
    :cond_1
    if-eqz v1, :cond_2

    iget-object v4, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    iget-object v5, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs2:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v5

    if-ne v4, v5, :cond_0

    .line 103
    :cond_2
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 104
    iget-object v4, p0, Lorg/apache/lucene/util/fst/PairOutputs;->NO_OUTPUT:Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    if-ne p1, v4, :cond_0

    move v2, v3

    .line 107
    goto :goto_0

    :cond_3
    move v2, v3

    .line 110
    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 31
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    check-cast p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/PairOutputs;->add(Lorg/apache/lucene/util/fst/PairOutputs$Pair;Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    move-result-object v0

    return-object v0
.end method

.method public add(Lorg/apache/lucene/util/fst/PairOutputs$Pair;Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;)",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;"
        }
    .end annotation

    .prologue
    .line 132
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    .local p1, "prefix":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<TA;TB;>;"
    .local p2, "output":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<TA;TB;>;"
    sget-boolean v0, Lorg/apache/lucene/util/fst/PairOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/PairOutputs;->valid(Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 133
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/PairOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p2}, Lorg/apache/lucene/util/fst/PairOutputs;->valid(Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 134
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs1:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v1, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    iget-object v2, p2, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs2:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v2, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    iget-object v3, p2, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/fst/PairOutputs;->newPair(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic common(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 31
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    check-cast p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/PairOutputs;->common(Lorg/apache/lucene/util/fst/PairOutputs$Pair;Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    move-result-object v0

    return-object v0
.end method

.method public common(Lorg/apache/lucene/util/fst/PairOutputs$Pair;Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;)",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;"
        }
    .end annotation

    .prologue
    .line 116
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    .local p1, "pair1":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<TA;TB;>;"
    .local p2, "pair2":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<TA;TB;>;"
    sget-boolean v0, Lorg/apache/lucene/util/fst/PairOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/PairOutputs;->valid(Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 117
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/PairOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p2}, Lorg/apache/lucene/util/fst/PairOutputs;->valid(Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 118
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs1:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v1, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    iget-object v2, p2, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/Outputs;->common(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs2:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v2, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    iget-object v3, p2, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/fst/Outputs;->common(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/fst/PairOutputs;->newPair(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getNoOutput()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/PairOutputs;->getNoOutput()Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    move-result-object v0

    return-object v0
.end method

.method public getNoOutput()Lorg/apache/lucene/util/fst/PairOutputs$Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;"
        }
    .end annotation

    .prologue
    .line 154
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/PairOutputs;->NO_OUTPUT:Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    return-object v0
.end method

.method public newPair(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;TB;)",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    .local p1, "a":Ljava/lang/Object;, "TA;"
    .local p2, "b":Ljava/lang/Object;, "TB;"
    iget-object v1, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs1:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs1:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object p1

    .line 77
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs2:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 78
    iget-object v1, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs2:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object p2

    .line 81
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs1:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs2:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v1

    if-ne p2, v1, :cond_3

    .line 82
    iget-object v0, p0, Lorg/apache/lucene/util/fst/PairOutputs;->NO_OUTPUT:Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .line 86
    :cond_2
    return-object v0

    .line 84
    :cond_3
    new-instance v0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lorg/apache/lucene/util/fst/PairOutputs$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/lucene/util/fst/PairOutputs$1;)V

    .line 85
    .local v0, "p":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<TA;TB;>;"
    sget-boolean v1, Lorg/apache/lucene/util/fst/PairOutputs;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/fst/PairOutputs;->valid(Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
.end method

.method public bridge synthetic outputToString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 31
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    check-cast p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/fst/PairOutputs;->outputToString(Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public outputToString(Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 159
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    .local p1, "output":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<TA;TB;>;"
    sget-boolean v0, Lorg/apache/lucene/util/fst/PairOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/PairOutputs;->valid(Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 160
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "<pair:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs1:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v2, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/fst/Outputs;->outputToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs2:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v2, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/fst/Outputs;->outputToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/fst/PairOutputs;->read(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    move-result-object v0

    return-object v0
.end method

.method public read(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;
    .locals 3
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/DataInput;",
            ")",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs1:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    move-result-object v0

    .line 148
    .local v0, "output1":Ljava/lang/Object;, "TA;"
    iget-object v2, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs2:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    move-result-object v1

    .line 149
    .local v1, "output2":Ljava/lang/Object;, "TB;"
    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/fst/PairOutputs;->newPair(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    move-result-object v2

    return-object v2
.end method

.method public bridge synthetic subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 31
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    check-cast p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/PairOutputs;->subtract(Lorg/apache/lucene/util/fst/PairOutputs$Pair;Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    move-result-object v0

    return-object v0
.end method

.method public subtract(Lorg/apache/lucene/util/fst/PairOutputs$Pair;Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;)",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;"
        }
    .end annotation

    .prologue
    .line 124
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    .local p1, "output":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<TA;TB;>;"
    .local p2, "inc":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<TA;TB;>;"
    sget-boolean v0, Lorg/apache/lucene/util/fst/PairOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/PairOutputs;->valid(Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 125
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/fst/PairOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p2}, Lorg/apache/lucene/util/fst/PairOutputs;->valid(Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 126
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs1:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v1, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    iget-object v2, p2, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/Outputs;->subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs2:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v2, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    iget-object v3, p2, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/fst/Outputs;->subtract(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/fst/PairOutputs;->newPair(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 165
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PairOutputs<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs1:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs2:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    check-cast p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/PairOutputs;->write(Lorg/apache/lucene/util/fst/PairOutputs$Pair;Lorg/apache/lucene/store/DataOutput;)V

    return-void
.end method

.method public write(Lorg/apache/lucene/util/fst/PairOutputs$Pair;Lorg/apache/lucene/store/DataOutput;)V
    .locals 2
    .param p2, "writer"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<TA;TB;>;",
            "Lorg/apache/lucene/store/DataOutput;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<TA;TB;>;"
    .local p1, "output":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<TA;TB;>;"
    sget-boolean v0, Lorg/apache/lucene/util/fst/PairOutputs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/lucene/util/fst/PairOutputs;->valid(Lorg/apache/lucene/util/fst/PairOutputs$Pair;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 141
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs1:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v1, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    invoke-virtual {v0, v1, p2}, Lorg/apache/lucene/util/fst/Outputs;->write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V

    .line 142
    iget-object v0, p0, Lorg/apache/lucene/util/fst/PairOutputs;->outputs2:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v1, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    invoke-virtual {v0, v1, p2}, Lorg/apache/lucene/util/fst/Outputs;->write(Ljava/lang/Object;Lorg/apache/lucene/store/DataOutput;)V

    .line 143
    return-void
.end method
