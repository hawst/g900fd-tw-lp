.class public Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;
.super Lorg/apache/lucene/util/ByteBlockPool$Allocator;
.source "ByteBlockPool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/ByteBlockPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DirectTrackingAllocator"
.end annotation


# instance fields
.field private final bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>(ILjava/util/concurrent/atomic/AtomicLong;)V
    .locals 0
    .param p1, "blockSize"    # I
    .param p2, "bytesUsed"    # Ljava/util/concurrent/atomic/AtomicLong;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/ByteBlockPool$Allocator;-><init>(I)V

    .line 100
    iput-object p2, p0, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    .line 101
    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/atomic/AtomicLong;)V
    .locals 1
    .param p1, "bytesUsed"    # Ljava/util/concurrent/atomic/AtomicLong;

    .prologue
    .line 95
    const v0, 0x8000

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;-><init>(ILjava/util/concurrent/atomic/AtomicLong;)V

    .line 96
    return-void
.end method


# virtual methods
.method public getByteBlock()[B
    .locals 4

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    iget v1, p0, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;->blockSize:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 105
    iget v0, p0, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;->blockSize:I

    new-array v0, v0, [B

    return-object v0
.end method

.method public recycleByteBlocks([[BII)V
    .locals 4
    .param p1, "blocks"    # [[B
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 110
    iget-object v1, p0, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    sub-int v2, p3, p2

    iget v3, p0, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;->blockSize:I

    mul-int/2addr v2, v3

    neg-int v2, v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 111
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_0

    .line 112
    const/4 v1, 0x0

    aput-object v1, p1, v0

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    :cond_0
    return-void
.end method
