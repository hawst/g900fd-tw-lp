.class public Lorg/apache/lucene/util/SimpleStringInterner;
.super Lorg/apache/lucene/util/StringInterner;
.source "SimpleStringInterner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/SimpleStringInterner$1;,
        Lorg/apache/lucene/util/SimpleStringInterner$Entry;
    }
.end annotation


# instance fields
.field private final cache:[Lorg/apache/lucene/util/SimpleStringInterner$Entry;

.field private final maxChainLength:I


# direct methods
.method public constructor <init>(II)V
    .locals 2
    .param p1, "tableSize"    # I
    .param p2, "maxChainLength"    # I

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/apache/lucene/util/StringInterner;-><init>()V

    .line 48
    const/4 v0, 0x1

    invoke-static {p1}, Lorg/apache/lucene/util/BitUtil;->nextHighestPowerOfTwo(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array v0, v0, [Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    iput-object v0, p0, Lorg/apache/lucene/util/SimpleStringInterner;->cache:[Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    .line 49
    const/4 v0, 0x2

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/util/SimpleStringInterner;->maxChainLength:I

    .line 50
    return-void
.end method


# virtual methods
.method public intern(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 54
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    .line 57
    .local v3, "h":I
    iget-object v6, p0, Lorg/apache/lucene/util/SimpleStringInterner;->cache:[Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    and-int v5, v3, v6

    .line 59
    .local v5, "slot":I
    iget-object v6, p0, Lorg/apache/lucene/util/SimpleStringInterner;->cache:[Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    aget-object v2, v6, v5

    .line 60
    .local v2, "first":Lorg/apache/lucene/util/SimpleStringInterner$Entry;
    const/4 v4, 0x0

    .line 62
    .local v4, "nextToLast":Lorg/apache/lucene/util/SimpleStringInterner$Entry;
    const/4 v0, 0x0

    .line 64
    .local v0, "chainLength":I
    move-object v1, v2

    .local v1, "e":Lorg/apache/lucene/util/SimpleStringInterner$Entry;
    :goto_0
    if-eqz v1, :cond_3

    .line 65
    # getter for: Lorg/apache/lucene/util/SimpleStringInterner$Entry;->hash:I
    invoke-static {v1}, Lorg/apache/lucene/util/SimpleStringInterner$Entry;->access$100(Lorg/apache/lucene/util/SimpleStringInterner$Entry;)I

    move-result v6

    if-ne v6, v3, :cond_1

    # getter for: Lorg/apache/lucene/util/SimpleStringInterner$Entry;->str:Ljava/lang/String;
    invoke-static {v1}, Lorg/apache/lucene/util/SimpleStringInterner$Entry;->access$200(Lorg/apache/lucene/util/SimpleStringInterner$Entry;)Ljava/lang/String;

    move-result-object v6

    if-eq v6, p1, :cond_0

    # getter for: Lorg/apache/lucene/util/SimpleStringInterner$Entry;->str:Ljava/lang/String;
    invoke-static {v1}, Lorg/apache/lucene/util/SimpleStringInterner$Entry;->access$200(Lorg/apache/lucene/util/SimpleStringInterner$Entry;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1

    .line 67
    :cond_0
    # getter for: Lorg/apache/lucene/util/SimpleStringInterner$Entry;->str:Ljava/lang/String;
    invoke-static {v1}, Lorg/apache/lucene/util/SimpleStringInterner$Entry;->access$200(Lorg/apache/lucene/util/SimpleStringInterner$Entry;)Ljava/lang/String;

    move-result-object v6

    .line 83
    :goto_1
    return-object v6

    .line 70
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 71
    # getter for: Lorg/apache/lucene/util/SimpleStringInterner$Entry;->next:Lorg/apache/lucene/util/SimpleStringInterner$Entry;
    invoke-static {v1}, Lorg/apache/lucene/util/SimpleStringInterner$Entry;->access$000(Lorg/apache/lucene/util/SimpleStringInterner$Entry;)Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 72
    move-object v4, v1

    .line 64
    :cond_2
    # getter for: Lorg/apache/lucene/util/SimpleStringInterner$Entry;->next:Lorg/apache/lucene/util/SimpleStringInterner$Entry;
    invoke-static {v1}, Lorg/apache/lucene/util/SimpleStringInterner$Entry;->access$000(Lorg/apache/lucene/util/SimpleStringInterner$Entry;)Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    move-result-object v1

    goto :goto_0

    .line 77
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object p1

    .line 78
    iget-object v6, p0, Lorg/apache/lucene/util/SimpleStringInterner;->cache:[Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    new-instance v7, Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    invoke-direct {v7, p1, v3, v2, v8}, Lorg/apache/lucene/util/SimpleStringInterner$Entry;-><init>(Ljava/lang/String;ILorg/apache/lucene/util/SimpleStringInterner$Entry;Lorg/apache/lucene/util/SimpleStringInterner$1;)V

    aput-object v7, v6, v5

    .line 79
    iget v6, p0, Lorg/apache/lucene/util/SimpleStringInterner;->maxChainLength:I

    if-lt v0, v6, :cond_4

    .line 81
    # setter for: Lorg/apache/lucene/util/SimpleStringInterner$Entry;->next:Lorg/apache/lucene/util/SimpleStringInterner$Entry;
    invoke-static {v4, v8}, Lorg/apache/lucene/util/SimpleStringInterner$Entry;->access$002(Lorg/apache/lucene/util/SimpleStringInterner$Entry;Lorg/apache/lucene/util/SimpleStringInterner$Entry;)Lorg/apache/lucene/util/SimpleStringInterner$Entry;

    :cond_4
    move-object v6, p1

    .line 83
    goto :goto_1
.end method
