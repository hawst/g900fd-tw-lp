.class public abstract Lorg/apache/lucene/util/AttributeImpl;
.super Ljava/lang/Object;
.source "AttributeImpl.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/util/Attribute;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final toStringMethod:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/util/AttributeImpl;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field protected enableBackwards:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 35
    const-class v0, Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/AttributeImpl;->$assertionsDisabled:Z

    .line 94
    new-instance v0, Lorg/apache/lucene/util/VirtualMethod;

    const-class v2, Lorg/apache/lucene/util/AttributeImpl;

    const-string/jumbo v3, "toString"

    new-array v1, v1, [Ljava/lang/Class;

    invoke-direct {v0, v2, v3, v1}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lorg/apache/lucene/util/AttributeImpl;->toStringMethod:Lorg/apache/lucene/util/VirtualMethod;

    return-void

    :cond_0
    move v0, v1

    .line 35
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/util/AttributeImpl;->enableBackwards:Z

    return-void
.end method

.method private assertExternalClass(Ljava/lang/Class;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/AttributeImpl;",
            ">;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 112
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/AttributeImpl;>;"
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "name":Ljava/lang/String;
    const-string/jumbo v1, "org.apache.lucene."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "org.apache.solr."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const-string/jumbo v1, "org.apache.lucene.util.TestAttributeSource$TestAttributeImpl"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract clear()V
.end method

.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 194
    const/4 v0, 0x0

    .line 196
    .local v0, "clone":Ljava/lang/Object;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 200
    return-object v0

    .line 197
    :catch_0
    move-exception v1

    .line 198
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public abstract copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
.end method

.method public final reflectAsString(Z)Ljava/lang/String;
    .locals 2
    .param p1, "prependAttClass"    # Z

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .local v0, "buffer":Ljava/lang/StringBuilder;
    new-instance v1, Lorg/apache/lucene/util/AttributeImpl$1;

    invoke-direct {v1, p0, v0, p1}, Lorg/apache/lucene/util/AttributeImpl$1;-><init>(Lorg/apache/lucene/util/AttributeImpl;Ljava/lang/StringBuilder;Z)V

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/AttributeImpl;->reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V

    .line 87
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V
    .locals 16
    .param p1, "reflector"    # Lorg/apache/lucene/util/AttributeReflector;

    .prologue
    .line 142
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 143
    .local v2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/AttributeImpl;>;"
    invoke-static {v2}, Lorg/apache/lucene/util/AttributeSource;->getAttributeInterfaces(Ljava/lang/Class;)Ljava/util/LinkedList;

    move-result-object v9

    .line 144
    .local v9, "interfaces":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;>;>;"
    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    move-result v13

    const/4 v14, 0x1

    if-eq v13, v14, :cond_0

    .line 145
    new-instance v13, Ljava/lang/UnsupportedOperationException;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " implements more than one Attribute interface, the default reflectWith() implementation cannot handle this."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 148
    :cond_0
    invoke-virtual {v9}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/ref/WeakReference;

    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Class;

    .line 151
    .local v8, "interf":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;"
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/lucene/util/AttributeImpl;->enableBackwards:Z

    if-eqz v13, :cond_3

    sget-object v13, Lorg/apache/lucene/util/AttributeImpl;->toStringMethod:Lorg/apache/lucene/util/VirtualMethod;

    invoke-virtual {v13, v2}, Lorg/apache/lucene/util/VirtualMethod;->isOverriddenAsOf(Ljava/lang/Class;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 152
    sget-boolean v13, Lorg/apache/lucene/util/AttributeImpl;->$assertionsDisabled:Z

    if-nez v13, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lorg/apache/lucene/util/AttributeImpl;->assertExternalClass(Ljava/lang/Class;)Z

    move-result v13

    if-nez v13, :cond_1

    new-instance v13, Ljava/lang/AssertionError;

    const-string/jumbo v14, "no Lucene/Solr classes should fallback to toString() parsing"

    invoke-direct {v13, v14}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v13

    .line 154
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/util/AttributeImpl;->toString()Ljava/lang/String;

    move-result-object v13

    const-string/jumbo v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/String;
    array-length v10, v1

    .local v10, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v10, :cond_5

    aget-object v11, v1, v7

    .line 155
    .local v11, "part":Ljava/lang/String;
    const/16 v13, 0x3d

    invoke-virtual {v11, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v12

    .line 156
    .local v12, "pos":I
    if-gez v12, :cond_2

    .line 157
    new-instance v13, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v14, "The backwards compatibility layer to support reflectWith() on old AtributeImpls expects the toString() implementation to return a correct format as specified for method reflectAsString(false)"

    invoke-direct {v13, v14}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 160
    :cond_2
    const/4 v13, 0x0

    invoke-virtual {v11, v13, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    add-int/lit8 v14, v12, 0x1

    invoke-virtual {v11, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-interface {v0, v8, v13, v14}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 154
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 166
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v7    # "i$":I
    .end local v10    # "len$":I
    .end local v11    # "part":Ljava/lang/String;
    .end local v12    # "pos":I
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v5

    .line 168
    .local v5, "fields":[Ljava/lang/reflect/Field;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    :try_start_0
    array-length v13, v5

    if-ge v6, v13, :cond_5

    .line 169
    aget-object v4, v5, v6

    .line 170
    .local v4, "f":Ljava/lang/reflect/Field;
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v13

    invoke-static {v13}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 168
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 171
    :cond_4
    const/4 v13, 0x1

    invoke-virtual {v4, v13}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 172
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-interface {v0, v8, v13, v14}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 174
    .end local v4    # "f":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v3

    .line 177
    .local v3, "e":Ljava/lang/IllegalAccessException;
    new-instance v13, Ljava/lang/RuntimeException;

    invoke-direct {v13, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v13

    .line 179
    .end local v3    # "e":Ljava/lang/IllegalAccessException;
    .end local v5    # "fields":[Ljava/lang/reflect/Field;
    .end local v6    # "i":I
    :cond_5
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/AttributeImpl;->reflectAsString(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
