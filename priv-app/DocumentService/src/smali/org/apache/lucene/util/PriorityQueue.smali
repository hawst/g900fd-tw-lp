.class public abstract Lorg/apache/lucene/util/PriorityQueue;
.super Ljava/lang/Object;
.source "PriorityQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private heap:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private maxSize:I

.field private size:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final downHeap()V
    .locals 6

    .prologue
    .line 232
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    const/4 v0, 0x1

    .line 233
    .local v0, "i":I
    iget-object v4, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v3, v4, v0

    .line 234
    .local v3, "node":Ljava/lang/Object;, "TT;"
    shl-int/lit8 v1, v0, 0x1

    .line 235
    .local v1, "j":I
    add-int/lit8 v2, v1, 0x1

    .line 236
    .local v2, "k":I
    iget v4, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    if-gt v2, v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v4, v4, v2

    iget-object v5, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v5, v5, v1

    invoke-virtual {p0, v4, v5}, Lorg/apache/lucene/util/PriorityQueue;->lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 237
    move v1, v2

    .line 239
    :cond_0
    :goto_0
    iget v4, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    if-gt v1, v4, :cond_1

    iget-object v4, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v4, v4, v1

    invoke-virtual {p0, v4, v3}, Lorg/apache/lucene/util/PriorityQueue;->lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 240
    iget-object v4, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    iget-object v5, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v5, v5, v1

    aput-object v5, v4, v0

    .line 241
    move v0, v1

    .line 242
    shl-int/lit8 v1, v0, 0x1

    .line 243
    add-int/lit8 v2, v1, 0x1

    .line 244
    iget v4, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    if-gt v2, v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v4, v4, v2

    iget-object v5, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v5, v5, v1

    invoke-virtual {p0, v4, v5}, Lorg/apache/lucene/util/PriorityQueue;->lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 245
    move v1, v2

    goto :goto_0

    .line 248
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aput-object v3, v4, v0

    .line 249
    return-void
.end method

.method private final upHeap()V
    .locals 5

    .prologue
    .line 220
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    .line 221
    .local v0, "i":I
    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v2, v3, v0

    .line 222
    .local v2, "node":Ljava/lang/Object;, "TT;"
    ushr-int/lit8 v1, v0, 0x1

    .line 223
    .local v1, "j":I
    :goto_0
    if-lez v1, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v3, v3, v1

    invoke-virtual {p0, v2, v3}, Lorg/apache/lucene/util/PriorityQueue;->lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 224
    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    iget-object v4, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v4, v4, v1

    aput-object v4, v3, v0

    .line 225
    move v0, v1

    .line 226
    ushr-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 228
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aput-object v2, v3, v0

    .line 229
    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 130
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    .local p1, "element":Ljava/lang/Object;, "TT;"
    iget v0, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    iget v1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    aput-object p1, v0, v1

    .line 132
    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;->upHeap()V

    .line 133
    iget-object v0, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final clear()V
    .locals 3

    .prologue
    .line 213
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    if-gt v0, v1, :cond_0

    .line 214
    iget-object v1, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 213
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 216
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    .line 217
    return-void
.end method

.method protected final getHeapArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 255
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    return-object v0
.end method

.method protected getSentinelObject()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 80
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final initialize(I)V
    .locals 5
    .param p1, "maxSize"    # I

    .prologue
    .line 86
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    .line 88
    if-nez p1, :cond_0

    .line 90
    const/4 v0, 0x2

    .line 108
    .local v0, "heapSize":I
    :goto_0
    new-array v3, v0, [Ljava/lang/Object;

    check-cast v3, [Ljava/lang/Object;

    iput-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    .line 109
    iput p1, p0, Lorg/apache/lucene/util/PriorityQueue;->maxSize:I

    .line 112
    invoke-virtual {p0}, Lorg/apache/lucene/util/PriorityQueue;->getSentinelObject()Ljava/lang/Object;

    move-result-object v2

    .line 113
    .local v2, "sentinel":Ljava/lang/Object;, "TT;"
    if-eqz v2, :cond_3

    .line 114
    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    const/4 v4, 0x1

    aput-object v2, v3, v4

    .line 115
    const/4 v1, 0x2

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 116
    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    invoke-virtual {p0}, Lorg/apache/lucene/util/PriorityQueue;->getSentinelObject()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v1

    .line 115
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 92
    .end local v0    # "heapSize":I
    .end local v1    # "i":I
    .end local v2    # "sentinel":Ljava/lang/Object;, "TT;"
    :cond_0
    const v3, 0x7fffffff

    if-ne p1, v3, :cond_1

    .line 101
    const v0, 0x7fffffff

    .restart local v0    # "heapSize":I
    goto :goto_0

    .line 105
    .end local v0    # "heapSize":I
    :cond_1
    add-int/lit8 v0, p1, 0x1

    .restart local v0    # "heapSize":I
    goto :goto_0

    .line 118
    .restart local v1    # "i":I
    .restart local v2    # "sentinel":Ljava/lang/Object;, "TT;"
    :cond_2
    iput p1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    .line 120
    .end local v1    # "i":I
    :cond_3
    return-void
.end method

.method public insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    .local p1, "element":Ljava/lang/Object;, "TT;"
    const/4 v3, 0x1

    .line 147
    iget v1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    iget v2, p0, Lorg/apache/lucene/util/PriorityQueue;->maxSize:I

    if-ge v1, v2, :cond_0

    .line 148
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/PriorityQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    const/4 v0, 0x0

    .line 156
    :goto_0
    return-object v0

    .line 150
    :cond_0
    iget v1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v1, v1, v3

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/util/PriorityQueue;->lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 151
    iget-object v1, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v0, v1, v3

    .line 152
    .local v0, "ret":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aput-object p1, v1, v3

    .line 153
    invoke-virtual {p0}, Lorg/apache/lucene/util/PriorityQueue;->updateTop()Ljava/lang/Object;

    goto :goto_0

    .end local v0    # "ret":Ljava/lang/Object;, "TT;"
    :cond_1
    move-object v0, p1

    .line 156
    goto :goto_0
.end method

.method protected abstract lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)Z"
        }
    .end annotation
.end method

.method public final pop()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 171
    iget v2, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    if-lez v2, :cond_0

    .line 172
    iget-object v2, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    aget-object v0, v2, v5

    .line 173
    .local v0, "result":Ljava/lang/Object;, "TT;"
    iget-object v2, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    iget-object v3, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    iget v4, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    aget-object v3, v3, v4

    aput-object v3, v2, v5

    .line 174
    iget-object v2, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    iget v3, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    aput-object v1, v2, v3

    .line 175
    iget v1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    .line 176
    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;->downHeap()V

    .line 179
    .end local v0    # "result":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 208
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    iget v0, p0, Lorg/apache/lucene/util/PriorityQueue;->size:I

    return v0
.end method

.method public final top()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 165
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final updateTop()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 202
    .local p0, "this":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<TT;>;"
    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;->downHeap()V

    .line 203
    iget-object v0, p0, Lorg/apache/lucene/util/PriorityQueue;->heap:[Ljava/lang/Object;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method
