.class Lorg/apache/lucene/util/packed/Direct32;
.super Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;
.source "Direct32.java"

# interfaces
.implements Lorg/apache/lucene/util/packed/PackedInts$Mutable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final BITS_PER_VALUE:I = 0x20


# instance fields
.field private values:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/util/packed/Direct32;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/Direct32;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "valueCount"    # I

    .prologue
    .line 37
    const/16 v0, 0x20

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 38
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/util/packed/Direct32;->values:[I

    .line 39
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataInput;I)V
    .locals 4
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p2, "valueCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    const/16 v3, 0x20

    invoke-direct {p0, p2, v3}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 43
    new-array v2, p2, [I

    .line 44
    .local v2, "values":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 45
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readInt()I

    move-result v3

    aput v3, v2, v0

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 47
    :cond_0
    rem-int/lit8 v1, p2, 0x2

    .line 48
    .local v1, "mod":I
    if-eqz v1, :cond_1

    .line 49
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readInt()I

    .line 52
    :cond_1
    iput-object v2, p0, Lorg/apache/lucene/util/packed/Direct32;->values:[I

    .line 53
    return-void
.end method

.method public constructor <init>([I)V
    .locals 2
    .param p1, "values"    # [I

    .prologue
    .line 63
    array-length v0, p1

    const/16 v1, 0x20

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 64
    iput-object p1, p0, Lorg/apache/lucene/util/packed/Direct32;->values:[I

    .line 65
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct32;->values:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 82
    return-void
.end method

.method public get(I)J
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 68
    sget-boolean v0, Lorg/apache/lucene/util/packed/Direct32;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/Direct32;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 69
    :cond_1
    const-wide v0, 0xffffffffL

    iget-object v2, p0, Lorg/apache/lucene/util/packed/Direct32;->values:[I

    aget v2, v2, p1

    int-to-long v2, v2

    and-long/2addr v0, v2

    return-wide v0
.end method

.method public bridge synthetic getArray()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/Direct32;->getArray()[I

    move-result-object v0

    return-object v0
.end method

.method public getArray()[I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct32;->values:[I

    return-object v0
.end method

.method public hasArray()Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x1

    return v0
.end method

.method public ramBytesUsed()J
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct32;->values:[I

    invoke-static {v0}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf([I)J

    move-result-wide v0

    return-wide v0
.end method

.method public set(IJ)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct32;->values:[I

    const-wide/16 v2, -0x1

    and-long/2addr v2, p2

    long-to-int v1, v2

    aput v1, v0, p1

    .line 74
    return-void
.end method
