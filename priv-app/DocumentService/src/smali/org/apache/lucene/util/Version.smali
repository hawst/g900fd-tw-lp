.class public final enum Lorg/apache/lucene/util/Version;
.super Ljava/lang/Enum;
.source "Version.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/util/Version;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/lucene/util/Version;

.field public static final enum LUCENE_20:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_21:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_22:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_23:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_24:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_29:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum LUCENE_30:Lorg/apache/lucene/util/Version;

.field public static final enum LUCENE_31:Lorg/apache/lucene/util/Version;

.field public static final enum LUCENE_32:Lorg/apache/lucene/util/Version;

.field public static final enum LUCENE_33:Lorg/apache/lucene/util/Version;

.field public static final enum LUCENE_34:Lorg/apache/lucene/util/Version;

.field public static final enum LUCENE_35:Lorg/apache/lucene/util/Version;

.field public static final enum LUCENE_36:Lorg/apache/lucene/util/Version;

.field public static final enum LUCENE_CURRENT:Lorg/apache/lucene/util/Version;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 38
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string/jumbo v1, "LUCENE_20"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_20:Lorg/apache/lucene/util/Version;

    .line 44
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string/jumbo v1, "LUCENE_21"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_21:Lorg/apache/lucene/util/Version;

    .line 50
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string/jumbo v1, "LUCENE_22"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_22:Lorg/apache/lucene/util/Version;

    .line 56
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string/jumbo v1, "LUCENE_23"

    invoke-direct {v0, v1, v6}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_23:Lorg/apache/lucene/util/Version;

    .line 62
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string/jumbo v1, "LUCENE_24"

    invoke-direct {v0, v1, v7}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_24:Lorg/apache/lucene/util/Version;

    .line 68
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string/jumbo v1, "LUCENE_29"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_29:Lorg/apache/lucene/util/Version;

    .line 72
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string/jumbo v1, "LUCENE_30"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    .line 75
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string/jumbo v1, "LUCENE_31"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    .line 78
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string/jumbo v1, "LUCENE_32"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_32:Lorg/apache/lucene/util/Version;

    .line 81
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string/jumbo v1, "LUCENE_33"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_33:Lorg/apache/lucene/util/Version;

    .line 84
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string/jumbo v1, "LUCENE_34"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_34:Lorg/apache/lucene/util/Version;

    .line 87
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string/jumbo v1, "LUCENE_35"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_35:Lorg/apache/lucene/util/Version;

    .line 95
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string/jumbo v1, "LUCENE_36"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_36:Lorg/apache/lucene/util/Version;

    .line 115
    new-instance v0, Lorg/apache/lucene/util/Version;

    const-string/jumbo v1, "LUCENE_CURRENT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/Version;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    .line 33
    const/16 v0, 0xe

    new-array v0, v0, [Lorg/apache/lucene/util/Version;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_20:Lorg/apache/lucene/util/Version;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_21:Lorg/apache/lucene/util/Version;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_22:Lorg/apache/lucene/util/Version;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_23:Lorg/apache/lucene/util/Version;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_24:Lorg/apache/lucene/util/Version;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_29:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_30:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_32:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_33:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_34:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_35:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_36:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/util/Version;->$VALUES:[Lorg/apache/lucene/util/Version;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/util/Version;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-class v0, Lorg/apache/lucene/util/Version;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/Version;

    return-object v0
.end method

.method public static final values()[Lorg/apache/lucene/util/Version;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lorg/apache/lucene/util/Version;->$VALUES:[Lorg/apache/lucene/util/Version;

    invoke-virtual {v0}, [Lorg/apache/lucene/util/Version;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/util/Version;

    return-object v0
.end method


# virtual methods
.method public onOrAfter(Lorg/apache/lucene/util/Version;)Z
    .locals 1
    .param p1, "other"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 119
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/Version;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
