.class public final Lorg/apache/lucene/util/FieldCacheSanityChecker;
.super Ljava/lang/Object;
.source "FieldCacheSanityChecker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;,
        Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;,
        Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    }
.end annotation


# instance fields
.field private estimateRam:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method

.method public static checkSanity(Lorg/apache/lucene/search/FieldCache;)[Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;
    .locals 1
    .param p0, "cache"    # Lorg/apache/lucene/search/FieldCache;

    .prologue
    .line 73
    invoke-interface {p0}, Lorg/apache/lucene/search/FieldCache;->getCacheEntries()[Lorg/apache/lucene/search/FieldCache$CacheEntry;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/util/FieldCacheSanityChecker;->checkSanity([Lorg/apache/lucene/search/FieldCache$CacheEntry;)[Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    move-result-object v0

    return-object v0
.end method

.method public static varargs checkSanity([Lorg/apache/lucene/search/FieldCache$CacheEntry;)[Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;
    .locals 2
    .param p0, "cacheEntries"    # [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .prologue
    .line 82
    new-instance v0, Lorg/apache/lucene/util/FieldCacheSanityChecker;

    invoke-direct {v0}, Lorg/apache/lucene/util/FieldCacheSanityChecker;-><init>()V

    .line 83
    .local v0, "sanityChecker":Lorg/apache/lucene/util/FieldCacheSanityChecker;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/FieldCacheSanityChecker;->setRamUsageEstimator(Z)V

    .line 84
    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/FieldCacheSanityChecker;->check([Lorg/apache/lucene/search/FieldCache$CacheEntry;)[Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    move-result-object v1

    return-object v1
.end method

.method private checkSubreaders(Lorg/apache/lucene/util/MapOfSets;Lorg/apache/lucene/util/MapOfSets;)Ljava/util/Collection;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/MapOfSets",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/search/FieldCache$CacheEntry;",
            ">;",
            "Lorg/apache/lucene/util/MapOfSets",
            "<",
            "Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    .local p1, "valIdToItems":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Ljava/lang/Integer;Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    .local p2, "readerFieldToValIds":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/lang/Integer;>;"
    new-instance v10, Ljava/util/ArrayList;

    const/16 v22, 0x17

    move/from16 v0, v22

    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 205
    .local v10, "insanity":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;>;"
    new-instance v3, Ljava/util/HashMap;

    const/16 v22, 0x11

    move/from16 v0, v22

    invoke-direct {v3, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 206
    .local v3, "badChildren":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;>;"
    new-instance v5, Lorg/apache/lucene/util/MapOfSets;

    invoke-direct {v5, v3}, Lorg/apache/lucene/util/MapOfSets;-><init>(Ljava/util/Map;)V

    .line 208
    .local v5, "badKids":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/util/MapOfSets;->getMap()Ljava/util/Map;

    move-result-object v21

    .line 209
    .local v21, "viToItemSets":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Set<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;>;"
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/util/MapOfSets;->getMap()Ljava/util/Map;

    move-result-object v18

    .line 211
    .local v18, "rfToValIdSets":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/util/Set<Ljava/lang/Integer;>;>;"
    new-instance v19, Ljava/util/HashSet;

    const/16 v22, 0x11

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 213
    .local v19, "seen":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v16

    .line 214
    .local v16, "readerFields":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;

    .line 216
    .local v17, "rf":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_0

    .line 218
    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->readerKey:Ljava/lang/Object;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/FieldCacheSanityChecker;->getAllDescendantReaderKeys(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v13

    .line 219
    .local v13, "kids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    .line 220
    .local v12, "kidKey":Ljava/lang/Object;
    new-instance v11, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->fieldName:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v11, v12, v0}, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    .local v11, "kid":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    invoke-interface {v3, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 225
    move-object/from16 v0, v17

    invoke-virtual {v5, v0, v11}, Lorg/apache/lucene/util/MapOfSets;->put(Ljava/lang/Object;Ljava/lang/Object;)I

    .line 226
    invoke-interface {v3, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/util/Collection;

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v5, v0, v1}, Lorg/apache/lucene/util/MapOfSets;->putAll(Ljava/lang/Object;Ljava/util/Collection;)I

    .line 227
    invoke-interface {v3, v11}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    :cond_1
    :goto_2
    move-object/from16 v0, v19

    invoke-interface {v0, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 229
    :cond_2
    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_1

    .line 231
    move-object/from16 v0, v17

    invoke-virtual {v5, v0, v11}, Lorg/apache/lucene/util/MapOfSets;->put(Ljava/lang/Object;Ljava/lang/Object;)I

    goto :goto_2

    .line 235
    .end local v11    # "kid":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    .end local v12    # "kidKey":Ljava/lang/Object;
    :cond_3
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 239
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v13    # "kids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    .end local v17    # "rf":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    :cond_4
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;

    .line 240
    .local v15, "parent":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    invoke-interface {v3, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Set;

    .line 242
    .local v14, "kids":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v14}, Ljava/util/Set;->size()I

    move-result v22

    mul-int/lit8 v22, v22, 0x2

    move/from16 v0, v22

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 246
    .local v4, "badEntries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    move-object/from16 v0, v18

    invoke-interface {v0, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/util/Set;

    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    .line 247
    .local v20, "value":Ljava/lang/Integer;
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/util/Collection;

    move-object/from16 v0, v22

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    .line 252
    .end local v20    # "value":Ljava/lang/Integer;
    :cond_5
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;

    .line 253
    .restart local v11    # "kid":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/util/Set;

    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    .line 254
    .restart local v20    # "value":Ljava/lang/Integer;
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/util/Collection;

    move-object/from16 v0, v22

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    .line 258
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v11    # "kid":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    .end local v20    # "value":Ljava/lang/Integer;
    :cond_7
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .line 259
    .local v6, "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    invoke-interface {v4, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    check-cast v6, [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .line 261
    .restart local v6    # "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    new-instance v22, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    sget-object v23, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;->SUBREADER:Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v25, "Found caches for descendants of "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v15}, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2, v6}, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;-><init>(Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;Ljava/lang/String;[Lorg/apache/lucene/search/FieldCache$CacheEntry;)V

    move-object/from16 v0, v22

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 267
    .end local v4    # "badEntries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    .end local v6    # "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    .end local v14    # "kids":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    .end local v15    # "parent":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    :cond_8
    return-object v10
.end method

.method private checkValueMismatch(Lorg/apache/lucene/util/MapOfSets;Lorg/apache/lucene/util/MapOfSets;Ljava/util/Set;)Ljava/util/Collection;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/MapOfSets",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/search/FieldCache$CacheEntry;",
            ">;",
            "Lorg/apache/lucene/util/MapOfSets",
            "<",
            "Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    .local p1, "valIdToItems":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Ljava/lang/Integer;Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    .local p2, "readerFieldToValIds":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/lang/Integer;>;"
    .local p3, "valMismatchKeys":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface/range {p3 .. p3}, Ljava/util/Set;->size()I

    move-result v11

    mul-int/lit8 v11, v11, 0x3

    invoke-direct {v6, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 168
    .local v6, "insanity":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/Set;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_2

    .line 171
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/util/MapOfSets;->getMap()Ljava/util/Map;

    move-result-object v8

    .line 172
    .local v8, "rfMap":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/util/Set<Ljava/lang/Integer;>;>;"
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/util/MapOfSets;->getMap()Ljava/util/Map;

    move-result-object v9

    .line 173
    .local v9, "valMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Set<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;

    .line 174
    .local v7, "rf":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface/range {p3 .. p3}, Ljava/util/Set;->size()I

    move-result v11

    mul-int/lit8 v11, v11, 0x2

    invoke-direct {v0, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 175
    .local v0, "badEntries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    invoke-interface {v8, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Set;

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    .line 176
    .local v10, "value":Ljava/lang/Integer;
    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Set;

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .line 177
    .local v2, "cacheEntry":Lorg/apache/lucene/search/FieldCache$CacheEntry;
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 181
    .end local v2    # "cacheEntry":Lorg/apache/lucene/search/FieldCache$CacheEntry;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v10    # "value":Ljava/lang/Integer;
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    new-array v1, v11, [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .line 182
    .local v1, "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    check-cast v1, [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .line 184
    .restart local v1    # "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    new-instance v11, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    sget-object v12, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;->VALUEMISMATCH:Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Multiple distinct value objects for "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v7}, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v12, v13, v1}, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;-><init>(Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;Ljava/lang/String;[Lorg/apache/lucene/search/FieldCache$CacheEntry;)V

    invoke-interface {v6, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 189
    .end local v0    # "badEntries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    .end local v1    # "badness":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    .end local v7    # "rf":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    .end local v8    # "rfMap":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/util/Set<Ljava/lang/Integer;>;>;"
    .end local v9    # "valMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Set<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;>;"
    :cond_2
    return-object v6
.end method

.method private getAllDescendantReaderKeys(Ljava/lang/Object;)Ljava/util/List;
    .locals 7
    .param p1, "seed"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277
    new-instance v0, Ljava/util/ArrayList;

    const/16 v5, 0x11

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 278
    .local v0, "all":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 280
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 281
    .local v3, "obj":Ljava/lang/Object;
    instance-of v5, v3, Lorg/apache/lucene/index/IndexReader;

    if-eqz v5, :cond_0

    .line 282
    check-cast v3, Lorg/apache/lucene/index/IndexReader;

    .end local v3    # "obj":Ljava/lang/Object;
    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexReader;->getSequentialSubReaders()[Lorg/apache/lucene/index/IndexReader;

    move-result-object v4

    .line 283
    .local v4, "subs":[Lorg/apache/lucene/index/IndexReader;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    if-eqz v4, :cond_0

    array-length v5, v4

    if-ge v2, v5, :cond_0

    .line 284
    aget-object v5, v4, v2

    invoke-virtual {v5}, Lorg/apache/lucene/index/IndexReader;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 279
    .end local v2    # "j":I
    .end local v4    # "subs":[Lorg/apache/lucene/index/IndexReader;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 290
    :cond_1
    const/4 v5, 0x1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {v0, v5, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    return-object v5
.end method


# virtual methods
.method public varargs check([Lorg/apache/lucene/search/FieldCache$CacheEntry;)[Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;
    .locals 11
    .param p1, "cacheEntries"    # [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    .prologue
    const/16 v10, 0x11

    .line 96
    if-eqz p1, :cond_0

    array-length v9, p1

    if-nez v9, :cond_1

    .line 97
    :cond_0
    const/4 v9, 0x0

    new-array v9, v9, [Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    .line 152
    :goto_0
    return-object v9

    .line 99
    :cond_1
    iget-boolean v9, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker;->estimateRam:Z

    if-eqz v9, :cond_2

    .line 100
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v9, p1

    if-ge v0, v9, :cond_2

    .line 101
    aget-object v9, p1, v0

    invoke-virtual {v9}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->estimateSize()V

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 109
    .end local v0    # "i":I
    :cond_2
    new-instance v7, Lorg/apache/lucene/util/MapOfSets;

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9, v10}, Ljava/util/HashMap;-><init>(I)V

    invoke-direct {v7, v9}, Lorg/apache/lucene/util/MapOfSets;-><init>(Ljava/util/Map;)V

    .line 111
    .local v7, "valIdToItems":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Ljava/lang/Integer;Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    new-instance v3, Lorg/apache/lucene/util/MapOfSets;

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9, v10}, Ljava/util/HashMap;-><init>(I)V

    invoke-direct {v3, v9}, Lorg/apache/lucene/util/MapOfSets;-><init>(Ljava/util/Map;)V

    .line 115
    .local v3, "readerFieldToValIds":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;Ljava/lang/Integer;>;"
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 118
    .local v8, "valMismatchKeys":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;>;"
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    array-length v9, p1

    if-ge v0, v9, :cond_5

    .line 119
    aget-object v2, p1, v0

    .line 120
    .local v2, "item":Lorg/apache/lucene/search/FieldCache$CacheEntry;
    invoke-virtual {v2}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getValue()Ljava/lang/Object;

    move-result-object v5

    .line 125
    .local v5, "val":Ljava/lang/Object;
    instance-of v9, v5, Lorg/apache/lucene/util/Bits;

    if-eqz v9, :cond_4

    .line 118
    :cond_3
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 129
    :cond_4
    instance-of v9, v5, Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;

    if-nez v9, :cond_3

    .line 132
    new-instance v4, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;

    invoke-virtual {v2}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getReaderKey()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v2}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getFieldName()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v4, v9, v10}, Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    .local v4, "rf":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    invoke-static {v5}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 138
    .local v6, "valId":Ljava/lang/Integer;
    invoke-virtual {v7, v6, v2}, Lorg/apache/lucene/util/MapOfSets;->put(Ljava/lang/Object;Ljava/lang/Object;)I

    .line 139
    const/4 v9, 0x1

    invoke-virtual {v3, v4, v6}, Lorg/apache/lucene/util/MapOfSets;->put(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v10

    if-ge v9, v10, :cond_3

    .line 140
    invoke-interface {v8, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 144
    .end local v2    # "item":Lorg/apache/lucene/search/FieldCache$CacheEntry;
    .end local v4    # "rf":Lorg/apache/lucene/util/FieldCacheSanityChecker$ReaderField;
    .end local v5    # "val":Ljava/lang/Object;
    .end local v6    # "valId":Ljava/lang/Integer;
    :cond_5
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v9

    mul-int/lit8 v9, v9, 0x3

    invoke-direct {v1, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 146
    .local v1, "insanity":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;>;"
    invoke-direct {p0, v7, v3, v8}, Lorg/apache/lucene/util/FieldCacheSanityChecker;->checkValueMismatch(Lorg/apache/lucene/util/MapOfSets;Lorg/apache/lucene/util/MapOfSets;Ljava/util/Set;)Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v1, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 149
    invoke-direct {p0, v7, v3}, Lorg/apache/lucene/util/FieldCacheSanityChecker;->checkSubreaders(Lorg/apache/lucene/util/MapOfSets;Lorg/apache/lucene/util/MapOfSets;)Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v1, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 152
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    invoke-interface {v1, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    goto/16 :goto_0
.end method

.method public setRamUsageEstimator(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker;->estimateRam:Z

    .line 65
    return-void
.end method
