.class public abstract Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;
.super Ljava/lang/Object;
.source "PackedInts.java"

# interfaces
.implements Lorg/apache/lucene/util/packed/PackedInts$Reader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/packed/PackedInts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ReaderImpl"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final bitsPerValue:I

.field protected final valueCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    const-class v0, Lorg/apache/lucene/util/packed/PackedInts;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(II)V
    .locals 3
    .param p1, "valueCount"    # I
    .param p2, "bitsPerValue"    # I

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput p2, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->bitsPerValue:I

    .line 115
    sget-boolean v0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-lez p2, :cond_0

    const/16 v0, 0x40

    if-le p2, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "bitsPerValue="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 116
    :cond_1
    iput p1, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->valueCount:I

    .line 117
    return-void
.end method


# virtual methods
.method public getArray()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBitsPerValue()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->bitsPerValue:I

    return v0
.end method

.method public getMaxValue()J
    .locals 2

    .prologue
    .line 128
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->bitsPerValue:I

    invoke-static {v0}, Lorg/apache/lucene/util/packed/PackedInts;->maxValue(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public hasArray()Z
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;->valueCount:I

    return v0
.end method
