.class public final Lorg/apache/lucene/util/VirtualMethod;
.super Ljava/lang/Object;
.source "VirtualMethod.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final singletonSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final baseClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TC;>;"
        }
    .end annotation
.end field

.field private final cache:Lorg/apache/lucene/util/WeakIdentityMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/WeakIdentityMap",
            "<",
            "Ljava/lang/Class",
            "<+TC;>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final method:Ljava/lang/String;

.field private final parameters:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/util/VirtualMethod;->singletonSet:Ljava/util/Set;

    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V
    .locals 4
    .param p2, "method"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TC;>;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "this":Lorg/apache/lucene/util/VirtualMethod;, "Lorg/apache/lucene/util/VirtualMethod<TC;>;"
    .local p1, "baseClass":Ljava/lang/Class;, "Ljava/lang/Class<TC;>;"
    .local p3, "parameters":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-static {}, Lorg/apache/lucene/util/WeakIdentityMap;->newConcurrentHashMap()Lorg/apache/lucene/util/WeakIdentityMap;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/util/VirtualMethod;->cache:Lorg/apache/lucene/util/WeakIdentityMap;

    .line 75
    iput-object p1, p0, Lorg/apache/lucene/util/VirtualMethod;->baseClass:Ljava/lang/Class;

    .line 76
    iput-object p2, p0, Lorg/apache/lucene/util/VirtualMethod;->method:Ljava/lang/String;

    .line 77
    iput-object p3, p0, Lorg/apache/lucene/util/VirtualMethod;->parameters:[Ljava/lang/Class;

    .line 79
    :try_start_0
    sget-object v1, Lorg/apache/lucene/util/VirtualMethod;->singletonSet:Ljava/util/Set;

    invoke-virtual {p1, p2, p3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 80
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v2, "VirtualMethod instances must be singletons and therefore assigned to static final members in the same class, they use as baseClass ctor param."

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :catch_0
    move-exception v0

    .line 85
    .local v0, "nsme":Ljava/lang/NoSuchMethodException;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " has no such method: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 87
    .end local v0    # "nsme":Ljava/lang/NoSuchMethodException;
    :cond_0
    return-void
.end method

.method public static compareImplementationDistance(Ljava/lang/Class;Lorg/apache/lucene/util/VirtualMethod;Lorg/apache/lucene/util/VirtualMethod;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TC;>;",
            "Lorg/apache/lucene/util/VirtualMethod",
            "<TC;>;",
            "Lorg/apache/lucene/util/VirtualMethod",
            "<TC;>;)I"
        }
    .end annotation

    .prologue
    .line 146
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+TC;>;"
    .local p1, "m1":Lorg/apache/lucene/util/VirtualMethod;, "Lorg/apache/lucene/util/VirtualMethod<TC;>;"
    .local p2, "m2":Lorg/apache/lucene/util/VirtualMethod;, "Lorg/apache/lucene/util/VirtualMethod<TC;>;"
    invoke-virtual {p1, p0}, Lorg/apache/lucene/util/VirtualMethod;->getImplementationDistance(Ljava/lang/Class;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, p0}, Lorg/apache/lucene/util/VirtualMethod;->getImplementationDistance(Ljava/lang/Class;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method private reflectImplementationDistance(Ljava/lang/Class;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TC;>;)I"
        }
    .end annotation

    .prologue
    .line 115
    .local p0, "this":Lorg/apache/lucene/util/VirtualMethod;, "Lorg/apache/lucene/util/VirtualMethod<TC;>;"
    .local p1, "subclazz":Ljava/lang/Class;, "Ljava/lang/Class<+TC;>;"
    iget-object v3, p0, Lorg/apache/lucene/util/VirtualMethod;->baseClass:Ljava/lang/Class;

    invoke-virtual {v3, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 116
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " is not a subclass of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/util/VirtualMethod;->baseClass:Ljava/lang/Class;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 117
    :cond_0
    const/4 v2, 0x0

    .line 118
    .local v2, "overridden":Z
    const/4 v1, 0x0

    .line 119
    .local v1, "distance":I
    move-object v0, p1

    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/util/VirtualMethod;->baseClass:Ljava/lang/Class;

    if-eq v0, v3, :cond_3

    if-eqz v0, :cond_3

    .line 121
    if-nez v2, :cond_1

    .line 123
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/util/VirtualMethod;->method:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/lucene/util/VirtualMethod;->parameters:[Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    const/4 v2, 0x1

    .line 130
    :cond_1
    :goto_1
    if-eqz v2, :cond_2

    add-int/lit8 v1, v1, 0x1

    .line 119
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 132
    :cond_3
    return v1

    .line 125
    :catch_0
    move-exception v3

    goto :goto_1
.end method


# virtual methods
.method public getImplementationDistance(Ljava/lang/Class;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TC;>;)I"
        }
    .end annotation

    .prologue
    .line 95
    .local p0, "this":Lorg/apache/lucene/util/VirtualMethod;, "Lorg/apache/lucene/util/VirtualMethod<TC;>;"
    .local p1, "subclazz":Ljava/lang/Class;, "Ljava/lang/Class<+TC;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/VirtualMethod;->cache:Lorg/apache/lucene/util/WeakIdentityMap;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/WeakIdentityMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 96
    .local v0, "distance":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 98
    iget-object v1, p0, Lorg/apache/lucene/util/VirtualMethod;->cache:Lorg/apache/lucene/util/WeakIdentityMap;

    invoke-direct {p0, p1}, Lorg/apache/lucene/util/VirtualMethod;->reflectImplementationDistance(Ljava/lang/Class;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lorg/apache/lucene/util/WeakIdentityMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1
.end method

.method public isOverriddenAsOf(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TC;>;)Z"
        }
    .end annotation

    .prologue
    .line 111
    .local p0, "this":Lorg/apache/lucene/util/VirtualMethod;, "Lorg/apache/lucene/util/VirtualMethod<TC;>;"
    .local p1, "subclazz":Ljava/lang/Class;, "Ljava/lang/Class<+TC;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/VirtualMethod;->getImplementationDistance(Ljava/lang/Class;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
