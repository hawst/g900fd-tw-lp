.class public abstract Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
.super Ljava/lang/Object;
.source "AttributeSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/AttributeSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AttributeFactory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/AttributeSource$AttributeFactory$DefaultAttributeFactory;
    }
.end annotation


# static fields
.field public static final DEFAULT_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lorg/apache/lucene/util/AttributeSource$AttributeFactory$DefaultAttributeFactory;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/AttributeSource$AttributeFactory$DefaultAttributeFactory;-><init>(Lorg/apache/lucene/util/AttributeSource$1;)V

    sput-object v0, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->DEFAULT_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method


# virtual methods
.method public abstract createAttributeInstance(Ljava/lang/Class;)Lorg/apache/lucene/util/AttributeImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;)",
            "Lorg/apache/lucene/util/AttributeImpl;"
        }
    .end annotation
.end method
