.class public final Lorg/apache/lucene/util/RamUsageEstimator;
.super Ljava/lang/Object;
.source "RamUsageEstimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;,
        Lorg/apache/lucene/util/RamUsageEstimator$DummyTwoLongObject;,
        Lorg/apache/lucene/util/RamUsageEstimator$DummyOneFieldObject;,
        Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;,
        Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;
    }
.end annotation


# static fields
.field public static final JVM_INFO_STRING:Ljava/lang/String;

.field public static final NUM_BYTES_ARRAY_HEADER:I

.field public static final NUM_BYTES_BOOLEAN:I = 0x1

.field public static final NUM_BYTES_BYTE:I = 0x1

.field public static final NUM_BYTES_CHAR:I = 0x2

.field public static final NUM_BYTES_DOUBLE:I = 0x8

.field public static final NUM_BYTES_FLOAT:I = 0x4

.field public static final NUM_BYTES_INT:I = 0x4

.field public static final NUM_BYTES_LONG:I = 0x8

.field public static final NUM_BYTES_OBJECT_ALIGNMENT:I

.field public static final NUM_BYTES_OBJECT_HEADER:I

.field public static final NUM_BYTES_OBJECT_REF:I

.field public static final NUM_BYTES_SHORT:I = 0x2

.field public static final ONE_GB:J = 0x40000000L

.field public static final ONE_KB:J = 0x400L

.field public static final ONE_MB:J = 0x100000L

.field private static final objectFieldOffsetMethod:Ljava/lang/reflect/Method;

.field private static final primitiveSizes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final supportedFeatures:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;",
            ">;"
        }
    .end annotation
.end field

.field private static final theUnsafe:Ljava/lang/Object;


# instance fields
.field private final checkInterned:Z


# direct methods
.method static constructor <clinit>()V
    .locals 27

    .prologue
    .line 116
    new-instance v23, Ljava/util/IdentityHashMap;

    invoke-direct/range {v23 .. v23}, Ljava/util/IdentityHashMap;-><init>()V

    sput-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    .line 117
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v24, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/16 v25, 0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-interface/range {v23 .. v25}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v24, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const/16 v25, 0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-interface/range {v23 .. v25}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v24, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const/16 v25, 0x2

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-interface/range {v23 .. v25}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v24, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const/16 v25, 0x2

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-interface/range {v23 .. v25}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v24, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/16 v25, 0x4

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-interface/range {v23 .. v25}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v24, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const/16 v25, 0x4

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-interface/range {v23 .. v25}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v24, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    const/16 v25, 0x8

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-interface/range {v23 .. v25}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    sget-object v24, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/16 v25, 0x8

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-interface/range {v23 .. v25}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-boolean v23, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v23, :cond_1

    const/16 v17, 0x8

    .line 149
    .local v17, "referenceSize":I
    :goto_0
    sget-boolean v23, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v23, :cond_2

    const/16 v14, 0x10

    .line 152
    .local v14, "objectHeader":I
    :goto_1
    sget-boolean v23, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v23, :cond_3

    const/16 v4, 0x18

    .line 154
    .local v4, "arrayHeader":I
    :goto_2
    const-class v23, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    invoke-static/range {v23 .. v23}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v23

    sput-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    .line 156
    const/16 v20, 0x0

    .line 157
    .local v20, "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/16 v19, 0x0

    .line 159
    .local v19, "tempTheUnsafe":Ljava/lang/Object;
    :try_start_0
    const-string/jumbo v23, "sun.misc.Unsafe"

    invoke-static/range {v23 .. v23}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v20

    .line 160
    const-string/jumbo v23, "theUnsafe"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v21

    .line 161
    .local v21, "unsafeField":Ljava/lang/reflect/Field;
    const/16 v23, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 162
    const/16 v23, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v19

    .line 166
    .end local v19    # "tempTheUnsafe":Ljava/lang/Object;
    .end local v21    # "unsafeField":Ljava/lang/reflect/Field;
    :goto_3
    sput-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->theUnsafe:Ljava/lang/Object;

    .line 170
    :try_start_1
    const-string/jumbo v23, "arrayIndexScale"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-class v26, Ljava/lang/Class;

    aput-object v26, v24, v25

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 171
    .local v5, "arrayIndexScaleM":Ljava/lang/reflect/Method;
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->theUnsafe:Ljava/lang/Object;

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-class v26, [Ljava/lang/Object;

    aput-object v26, v24, v25

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v5, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Number;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->intValue()I

    move-result v17

    .line 172
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    sget-object v24, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->OBJECT_REFERENCE_SIZE:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    invoke-virtual/range {v23 .. v24}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 179
    .end local v5    # "arrayIndexScaleM":Ljava/lang/reflect/Method;
    :goto_4
    sget-boolean v23, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v23, :cond_4

    add-int/lit8 v14, v17, 0x8

    .line 180
    :goto_5
    sget-boolean v23, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v23, :cond_5

    mul-int/lit8 v23, v17, 0x2

    add-int/lit8 v4, v23, 0x8

    .line 187
    :goto_6
    const/16 v18, 0x0

    .line 189
    .local v18, "tempObjectFieldOffsetMethod":Ljava/lang/reflect/Method;
    :try_start_2
    const-string/jumbo v23, "objectFieldOffset"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-class v26, Ljava/lang/reflect/Field;

    aput-object v26, v24, v25

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v13

    .line 190
    .local v13, "objectFieldOffsetM":Ljava/lang/reflect/Method;
    const-class v23, Lorg/apache/lucene/util/RamUsageEstimator$DummyTwoLongObject;

    const-string/jumbo v24, "dummy1"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v8

    .line 191
    .local v8, "dummy1Field":Ljava/lang/reflect/Field;
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->theUnsafe:Ljava/lang/Object;

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v8, v24, v25

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Number;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->intValue()I

    move-result v15

    .line 192
    .local v15, "ofs1":I
    const-class v23, Lorg/apache/lucene/util/RamUsageEstimator$DummyTwoLongObject;

    const-string/jumbo v24, "dummy2"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v9

    .line 193
    .local v9, "dummy2Field":Ljava/lang/reflect/Field;
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->theUnsafe:Ljava/lang/Object;

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v9, v24, v25

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Number;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->intValue()I

    move-result v16

    .line 194
    .local v16, "ofs2":I
    sub-int v23, v16, v15

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(I)I

    move-result v23

    const/16 v24, 0x8

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_0

    .line 195
    const-class v23, Lorg/apache/lucene/util/RamUsageEstimator$DummyOneFieldObject;

    const-string/jumbo v24, "base"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 196
    .local v6, "baseField":Ljava/lang/reflect/Field;
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->theUnsafe:Ljava/lang/Object;

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v6, v24, v25

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Number;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->intValue()I

    move-result v14

    .line 197
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    sget-object v24, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->FIELD_OFFSETS:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    invoke-virtual/range {v23 .. v24}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 198
    move-object/from16 v18, v13

    .line 203
    .end local v6    # "baseField":Ljava/lang/reflect/Field;
    .end local v8    # "dummy1Field":Ljava/lang/reflect/Field;
    .end local v9    # "dummy2Field":Ljava/lang/reflect/Field;
    .end local v13    # "objectFieldOffsetM":Ljava/lang/reflect/Method;
    .end local v15    # "ofs1":I
    .end local v16    # "ofs2":I
    :cond_0
    :goto_7
    sput-object v18, Lorg/apache/lucene/util/RamUsageEstimator;->objectFieldOffsetMethod:Ljava/lang/reflect/Method;

    .line 208
    :try_start_3
    const-string/jumbo v23, "arrayBaseOffset"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-class v26, Ljava/lang/Class;

    aput-object v26, v24, v25

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 210
    .local v3, "arrayBaseOffsetM":Ljava/lang/reflect/Method;
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->theUnsafe:Ljava/lang/Object;

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-class v26, [B

    aput-object v26, v24, v25

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v3, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Number;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Number;->intValue()I

    move-result v4

    .line 211
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    sget-object v24, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->ARRAY_HEADER_SIZE:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    invoke-virtual/range {v23 .. v24}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 216
    .end local v3    # "arrayBaseOffsetM":Ljava/lang/reflect/Method;
    :goto_8
    sput v17, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    .line 217
    sput v14, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_HEADER:I

    .line 218
    sput v4, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    .line 222
    const/16 v12, 0x8

    .line 224
    .local v12, "objectAlignment":I
    :try_start_4
    const-string/jumbo v23, "com.sun.management.HotSpotDiagnosticMXBean"

    invoke-static/range {v23 .. v23}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    .line 225
    .local v7, "beanClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {}, Ljava/lang/management/ManagementFactory;->getPlatformMBeanServer()Ljavax/management/MBeanServer;

    move-result-object v23

    const-string/jumbo v24, "com.sun.management:type=HotSpotDiagnostic"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-static {v0, v1, v7}, Ljava/lang/management/ManagementFactory;->newPlatformMXBeanProxy(Ljavax/management/MBeanServerConnection;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    .line 230
    .local v11, "hotSpotBean":Ljava/lang/Object;
    const-string/jumbo v23, "getVMOption"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-class v26, Ljava/lang/String;

    aput-object v26, v24, v25

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10

    .line 231
    .local v10, "getVMOptionMethod":Ljava/lang/reflect/Method;
    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const-string/jumbo v25, "ObjectAlignmentInBytes"

    aput-object v25, v23, v24

    move-object/from16 v0, v23

    invoke-virtual {v10, v11, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    .line 232
    .local v22, "vmOption":Ljava/lang/Object;
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v23

    const-string/jumbo v24, "getValue"

    const/16 v25, 0x0

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v25, v0

    invoke-virtual/range {v23 .. v25}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v23

    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 235
    sget-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    sget-object v24, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->OBJECT_ALIGNMENT:Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    invoke-virtual/range {v23 .. v24}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 240
    .end local v7    # "beanClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v10    # "getVMOptionMethod":Ljava/lang/reflect/Method;
    .end local v11    # "hotSpotBean":Ljava/lang/Object;
    .end local v22    # "vmOption":Ljava/lang/Object;
    :goto_9
    sput v12, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_ALIGNMENT:I

    .line 242
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "[JVM: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Lorg/apache/lucene/util/Constants;->JVM_NAME:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Lorg/apache/lucene/util/Constants;->JVM_VERSION:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Lorg/apache/lucene/util/Constants;->JVM_VENDOR:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Lorg/apache/lucene/util/Constants;->JAVA_VENDOR:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Lorg/apache/lucene/util/Constants;->JAVA_VERSION:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, "]"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    sput-object v23, Lorg/apache/lucene/util/RamUsageEstimator;->JVM_INFO_STRING:Ljava/lang/String;

    .line 245
    return-void

    .line 148
    .end local v4    # "arrayHeader":I
    .end local v12    # "objectAlignment":I
    .end local v14    # "objectHeader":I
    .end local v17    # "referenceSize":I
    .end local v18    # "tempObjectFieldOffsetMethod":Ljava/lang/reflect/Method;
    .end local v20    # "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    const/16 v17, 0x4

    goto/16 :goto_0

    .line 149
    .restart local v17    # "referenceSize":I
    :cond_2
    const/16 v14, 0x8

    goto/16 :goto_1

    .line 152
    .restart local v14    # "objectHeader":I
    :cond_3
    const/16 v4, 0xc

    goto/16 :goto_2

    .line 179
    .restart local v4    # "arrayHeader":I
    .restart local v20    # "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_4
    const/16 v14, 0x8

    goto/16 :goto_5

    .line 180
    :cond_5
    const/16 v4, 0xc

    goto/16 :goto_6

    .line 236
    .restart local v12    # "objectAlignment":I
    .restart local v18    # "tempObjectFieldOffsetMethod":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v23

    goto :goto_9

    .line 212
    .end local v12    # "objectAlignment":I
    :catch_1
    move-exception v23

    goto/16 :goto_8

    .line 200
    :catch_2
    move-exception v23

    goto/16 :goto_7

    .line 173
    .end local v18    # "tempObjectFieldOffsetMethod":Ljava/lang/reflect/Method;
    :catch_3
    move-exception v23

    goto/16 :goto_4

    .line 163
    .restart local v19    # "tempTheUnsafe":Ljava/lang/Object;
    :catch_4
    move-exception v23

    goto/16 :goto_3
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 855
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/RamUsageEstimator;-><init>(Z)V

    .line 856
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0
    .param p1, "checkInterned"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 868
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 869
    iput-boolean p1, p0, Lorg/apache/lucene/util/RamUsageEstimator;->checkInterned:Z

    .line 870
    return-void
.end method

.method private static adjustForField(JLjava/lang/reflect/Field;)J
    .locals 12
    .param p0, "sizeSoFar"    # J
    .param p2, "f"    # Ljava/lang/reflect/Field;

    .prologue
    .line 533
    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v6

    .line 534
    .local v6, "type":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v6}, Ljava/lang/Class;->isPrimitive()Z

    move-result v7

    if-eqz v7, :cond_0

    sget-object v7, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    invoke-interface {v7, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 535
    .local v2, "fsize":I
    :goto_0
    sget-object v7, Lorg/apache/lucene/util/RamUsageEstimator;->objectFieldOffsetMethod:Ljava/lang/reflect/Method;

    if-eqz v7, :cond_3

    .line 537
    :try_start_0
    sget-object v7, Lorg/apache/lucene/util/RamUsageEstimator;->objectFieldOffsetMethod:Ljava/lang/reflect/Method;

    sget-object v8, Lorg/apache/lucene/util/RamUsageEstimator;->theUnsafe:Ljava/lang/Object;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p2, v9, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->longValue()J

    move-result-wide v8

    int-to-long v10, v2

    add-long v4, v8, v10

    .line 539
    .local v4, "offsetPlusSize":J
    invoke-static {p0, p1, v4, v5}, Ljava/lang/Math;->max(JJ)J
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v8

    .line 556
    .end local v4    # "offsetPlusSize":J
    :goto_1
    return-wide v8

    .line 534
    .end local v2    # "fsize":I
    :cond_0
    sget v2, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    goto :goto_0

    .line 540
    .restart local v2    # "fsize":I
    :catch_0
    move-exception v1

    .line 541
    .local v1, "ex":Ljava/lang/IllegalAccessException;
    new-instance v7, Ljava/lang/RuntimeException;

    const-string/jumbo v8, "Access problem with sun.misc.Unsafe"

    invoke-direct {v7, v8, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 542
    .end local v1    # "ex":Ljava/lang/IllegalAccessException;
    :catch_1
    move-exception v3

    .line 543
    .local v3, "ite":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v3}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 544
    .local v0, "cause":Ljava/lang/Throwable;
    instance-of v7, v0, Ljava/lang/RuntimeException;

    if-eqz v7, :cond_1

    .line 545
    check-cast v0, Ljava/lang/RuntimeException;

    .end local v0    # "cause":Ljava/lang/Throwable;
    throw v0

    .line 546
    .restart local v0    # "cause":Ljava/lang/Throwable;
    :cond_1
    instance-of v7, v0, Ljava/lang/Error;

    if-eqz v7, :cond_2

    .line 547
    check-cast v0, Ljava/lang/Error;

    .end local v0    # "cause":Ljava/lang/Throwable;
    throw v0

    .line 550
    .restart local v0    # "cause":Ljava/lang/Throwable;
    :cond_2
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Call to Unsafe\'s objectFieldOffset() throwed checked Exception when accessing field "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "#"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 556
    .end local v0    # "cause":Ljava/lang/Throwable;
    .end local v3    # "ite":Ljava/lang/reflect/InvocationTargetException;
    :cond_3
    int-to-long v8, v2

    add-long/2addr v8, p0

    goto :goto_1
.end method

.method public static alignObjectSize(J)J
    .locals 4
    .param p0, "size"    # J

    .prologue
    .line 288
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_ALIGNMENT:I

    int-to-long v0, v0

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    add-long/2addr p0, v0

    .line 289
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_ALIGNMENT:I

    int-to-long v0, v0

    rem-long v0, p0, v0

    sub-long v0, p0, v0

    return-wide v0
.end method

.method private static createCacheEntry(Ljava/lang/Class;)Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;"
        }
    .end annotation

    .prologue
    .line 503
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    sget v10, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_HEADER:I

    int-to-long v8, v10

    .line 504
    .local v8, "shallowInstanceSize":J
    new-instance v7, Ljava/util/ArrayList;

    const/16 v10, 0x20

    invoke-direct {v7, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 505
    .local v7, "referenceFields":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/reflect/Field;>;"
    move-object v1, p0

    .local v1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    if-eqz v1, :cond_2

    .line 506
    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v4

    .line 507
    .local v4, "fields":[Ljava/lang/reflect/Field;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/reflect/Field;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_1

    aget-object v3, v0, v5

    .line 508
    .local v3, "f":Ljava/lang/reflect/Field;
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v10

    invoke-static {v10}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v10

    if-nez v10, :cond_0

    .line 509
    invoke-static {v8, v9, v3}, Lorg/apache/lucene/util/RamUsageEstimator;->adjustForField(JLjava/lang/reflect/Field;)J

    move-result-wide v8

    .line 511
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->isPrimitive()Z

    move-result v10

    if-nez v10, :cond_0

    .line 512
    const/4 v10, 0x1

    invoke-virtual {v3, v10}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 513
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 507
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 505
    .end local v3    # "f":Ljava/lang/reflect/Field;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    goto :goto_0

    .line 519
    .end local v0    # "arr$":[Ljava/lang/reflect/Field;
    .end local v4    # "fields":[Ljava/lang/reflect/Field;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :cond_2
    new-instance v2, Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;

    invoke-static {v8, v9}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v12

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v10, v10, [Ljava/lang/reflect/Field;

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/reflect/Field;

    invoke-direct {v2, v12, v13, v10}, Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;-><init>(J[Ljava/lang/reflect/Field;)V

    .line 522
    .local v2, "cachedInfo":Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;
    return-object v2
.end method

.method public static getSupportedFeatures()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;",
            ">;"
        }
    .end annotation

    .prologue
    .line 569
    sget-object v0, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public static getUnsupportedFeatures()Ljava/util/EnumSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;",
            ">;"
        }
    .end annotation

    .prologue
    .line 562
    const-class v1, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    invoke-static {v1}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 563
    .local v0, "unsupported":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;>;"
    sget-object v1, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->removeAll(Ljava/util/Collection;)Z

    .line 564
    return-object v0
.end method

.method public static humanReadableUnits(J)Ljava/lang/String;
    .locals 4
    .param p0, "bytes"    # J

    .prologue
    .line 576
    new-instance v0, Ljava/text/DecimalFormat;

    const-string/jumbo v1, "0.#"

    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    invoke-static {p0, p1, v0}, Lorg/apache/lucene/util/RamUsageEstimator;->humanReadableUnits(JLjava/text/DecimalFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static humanReadableUnits(JLjava/text/DecimalFormat;)Ljava/lang/String;
    .locals 4
    .param p0, "bytes"    # J
    .param p2, "df"    # Ljava/text/DecimalFormat;

    .prologue
    const-wide/16 v2, 0x0

    .line 584
    const-wide/32 v0, 0x40000000

    div-long v0, p0, v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 585
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    long-to-float v1, p0

    const/high16 v2, 0x4e800000

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {p2, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " GB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 591
    :goto_0
    return-object v0

    .line 586
    :cond_0
    const-wide/32 v0, 0x100000

    div-long v0, p0, v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 587
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    long-to-float v1, p0

    const/high16 v2, 0x49800000    # 1048576.0f

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {p2, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " MB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 588
    :cond_1
    const-wide/16 v0, 0x400

    div-long v0, p0, v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 589
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    long-to-float v1, p0

    const/high16 v2, 0x44800000    # 1024.0f

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {p2, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " KB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 591
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " bytes"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static humanSizeOf(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;

    .prologue
    .line 601
    invoke-static {p0}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf(Ljava/lang/Object;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->humanReadableUnits(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isSupportedJVM()Z
    .locals 2

    .prologue
    .line 281
    sget-object v0, Lorg/apache/lucene/util/RamUsageEstimator;->supportedFeatures:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->size()I

    move-result v0

    invoke-static {}, Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;->values()[Lorg/apache/lucene/util/RamUsageEstimator$JvmFeature;

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static measureObjectSize(Ljava/lang/Object;Z)J
    .locals 26
    .param p0, "root"    # Ljava/lang/Object;
    .param p1, "checkInterned"    # Z

    .prologue
    .line 415
    new-instance v15, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;

    invoke-direct {v15}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;-><init>()V

    .line 417
    .local v15, "seen":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<Ljava/lang/Object;>;"
    new-instance v4, Ljava/util/IdentityHashMap;

    invoke-direct {v4}, Ljava/util/IdentityHashMap;-><init>()V

    .line 419
    .local v4, "classCache":Ljava/util/IdentityHashMap;, "Ljava/util/IdentityHashMap<Ljava/lang/Class<*>;Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 420
    .local v18, "stack":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 422
    const-wide/16 v20, 0x0

    .line 423
    .local v20, "totalSize":J
    :cond_0
    :goto_0
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_9

    .line 424
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v13

    .line 426
    .local v13, "ob":Ljava/lang/Object;
    if-eqz v13, :cond_0

    invoke-virtual {v15, v13}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_0

    .line 429
    invoke-virtual {v15, v13}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->add(Ljava/lang/Object;)Z

    .line 432
    if-eqz p1, :cond_1

    instance-of v0, v13, Ljava/lang/String;

    move/from16 v19, v0

    if-eqz v19, :cond_1

    move-object/from16 v19, v13

    check-cast v19, Ljava/lang/String;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    if-eq v13, v0, :cond_0

    .line 437
    :cond_1
    invoke-virtual {v13}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    .line 438
    .local v14, "obClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v14}, Ljava/lang/Class;->isArray()Z

    move-result v19

    if-eqz v19, :cond_5

    .line 443
    sget v19, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 444
    .local v16, "size":J
    invoke-static {v13}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v10

    .line 445
    .local v10, "len":I
    if-lez v10, :cond_2

    .line 446
    invoke-virtual {v14}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v5

    .line 447
    .local v5, "componentClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v5}, Ljava/lang/Class;->isPrimitive()Z

    move-result v19

    if-eqz v19, :cond_3

    .line 448
    int-to-long v0, v10

    move-wide/from16 v22, v0

    sget-object v19, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v24, v0

    mul-long v22, v22, v24

    add-long v16, v16, v22

    .line 461
    .end local v5    # "componentClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_2
    invoke-static/range {v16 .. v17}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v22

    add-long v20, v20, v22

    .line 462
    goto :goto_0

    .line 450
    .restart local v5    # "componentClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_3
    sget v19, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v22, v0

    int-to-long v0, v10

    move-wide/from16 v24, v0

    mul-long v22, v22, v24

    add-long v16, v16, v22

    .line 453
    move v8, v10

    .local v8, "i":I
    :cond_4
    :goto_1
    add-int/lit8 v8, v8, -0x1

    if-ltz v8, :cond_2

    .line 454
    invoke-static {v13, v8}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v12

    .line 455
    .local v12, "o":Ljava/lang/Object;
    if-eqz v12, :cond_4

    invoke-virtual {v15, v12}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_4

    .line 456
    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 468
    .end local v5    # "componentClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v8    # "i":I
    .end local v10    # "len":I
    .end local v12    # "o":Ljava/lang/Object;
    .end local v16    # "size":J
    :cond_5
    :try_start_0
    invoke-virtual {v4, v14}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;

    .line 469
    .local v3, "cachedInfo":Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;
    if-nez v3, :cond_6

    .line 470
    invoke-static {v14}, Lorg/apache/lucene/util/RamUsageEstimator;->createCacheEntry(Ljava/lang/Class;)Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;

    move-result-object v3

    invoke-virtual {v4, v14, v3}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    :cond_6
    iget-object v2, v3, Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;->referenceFields:[Ljava/lang/reflect/Field;

    .local v2, "arr$":[Ljava/lang/reflect/Field;
    array-length v11, v2

    .local v11, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_2
    if-ge v9, v11, :cond_8

    aget-object v7, v2, v9

    .line 475
    .local v7, "f":Ljava/lang/reflect/Field;
    invoke-virtual {v7, v13}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    .line 476
    .restart local v12    # "o":Ljava/lang/Object;
    if-eqz v12, :cond_7

    invoke-virtual {v15, v12}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_7

    .line 477
    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 473
    :cond_7
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 481
    .end local v7    # "f":Ljava/lang/reflect/Field;
    .end local v12    # "o":Ljava/lang/Object;
    :cond_8
    iget-wide v0, v3, Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;->alignedShallowInstanceSize:J

    move-wide/from16 v22, v0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    add-long v20, v20, v22

    goto/16 :goto_0

    .line 482
    .end local v2    # "arr$":[Ljava/lang/reflect/Field;
    .end local v3    # "cachedInfo":Lorg/apache/lucene/util/RamUsageEstimator$ClassCache;
    .end local v9    # "i$":I
    .end local v11    # "len$":I
    :catch_0
    move-exception v6

    .line 484
    .local v6, "e":Ljava/lang/IllegalAccessException;
    new-instance v19, Ljava/lang/RuntimeException;

    const-string/jumbo v22, "Reflective field access failed?"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v19

    .line 490
    .end local v6    # "e":Ljava/lang/IllegalAccessException;
    .end local v13    # "ob":Ljava/lang/Object;
    .end local v14    # "obClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_9
    invoke-virtual {v15}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->clear()V

    .line 491
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->clear()V

    .line 492
    invoke-virtual {v4}, Ljava/util/IdentityHashMap;->clear()V

    .line 494
    return-wide v20
.end method

.method public static shallowSizeOf(Ljava/lang/Object;)J
    .locals 4
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 353
    if-nez p0, :cond_0

    const-wide/16 v2, 0x0

    .line 358
    :goto_0
    return-wide v2

    .line 354
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 355
    .local v0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 356
    invoke-static {p0}, Lorg/apache/lucene/util/RamUsageEstimator;->shallowSizeOfArray(Ljava/lang/Object;)J

    move-result-wide v2

    goto :goto_0

    .line 358
    :cond_1
    invoke-static {v0}, Lorg/apache/lucene/util/RamUsageEstimator;->shallowSizeOfInstance(Ljava/lang/Class;)J

    move-result-wide v2

    goto :goto_0
.end method

.method private static shallowSizeOfArray(Ljava/lang/Object;)J
    .locals 8
    .param p0, "array"    # Ljava/lang/Object;

    .prologue
    .line 394
    sget v4, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v2, v4

    .line 395
    .local v2, "size":J
    invoke-static {p0}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    .line 396
    .local v1, "len":I
    if-lez v1, :cond_0

    .line 397
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    .line 398
    .local v0, "arrayElementClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 399
    int-to-long v6, v1

    sget-object v4, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    .line 404
    .end local v0    # "arrayElementClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    :goto_0
    invoke-static {v2, v3}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v4

    return-wide v4

    .line 401
    .restart local v0    # "arrayElementClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    sget v4, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    int-to-long v4, v4

    int-to-long v6, v1

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    goto :goto_0
.end method

.method public static shallowSizeOfInstance(Ljava/lang/Class;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)J"
        }
    .end annotation

    .prologue
    .line 371
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Ljava/lang/Class;->isArray()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 372
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v8, "This method does not work with array classes."

    invoke-direct {v5, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 373
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 374
    sget-object v5, Lorg/apache/lucene/util/RamUsageEstimator;->primitiveSizes:Ljava/util/Map;

    invoke-interface {v5, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-long v8, v5

    .line 387
    :goto_0
    return-wide v8

    .line 376
    :cond_1
    sget v5, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_HEADER:I

    int-to-long v6, v5

    .line 379
    .local v6, "size":J
    :goto_1
    if-eqz p0, :cond_4

    .line 380
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    .line 381
    .local v2, "fields":[Ljava/lang/reflect/Field;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/reflect/Field;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_2
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    .line 382
    .local v1, "f":Ljava/lang/reflect/Field;
    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v5

    invoke-static {v5}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v5

    if-nez v5, :cond_2

    .line 383
    invoke-static {v6, v7, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->adjustForField(JLjava/lang/reflect/Field;)J

    move-result-wide v6

    .line 381
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 379
    .end local v1    # "f":Ljava/lang/reflect/Field;
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object p0

    goto :goto_1

    .line 387
    .end local v0    # "arr$":[Ljava/lang/reflect/Field;
    .end local v2    # "fields":[Ljava/lang/reflect/Field;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_4
    invoke-static {v6, v7}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v8

    goto :goto_0
.end method

.method public static sizeOf(Ljava/lang/Object;)J
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 342
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/lucene/util/RamUsageEstimator;->measureObjectSize(Ljava/lang/Object;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([B)J
    .locals 4
    .param p0, "arr"    # [B

    .prologue
    .line 294
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    array-length v2, p0

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([C)J
    .locals 6
    .param p0, "arr"    # [C

    .prologue
    .line 304
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    const-wide/16 v2, 0x2

    array-length v4, p0

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([D)J
    .locals 6
    .param p0, "arr"    # [D

    .prologue
    .line 329
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    const-wide/16 v2, 0x8

    array-length v4, p0

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([F)J
    .locals 6
    .param p0, "arr"    # [F

    .prologue
    .line 319
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    const-wide/16 v2, 0x4

    array-length v4, p0

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([I)J
    .locals 6
    .param p0, "arr"    # [I

    .prologue
    .line 314
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    const-wide/16 v2, 0x4

    array-length v4, p0

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([J)J
    .locals 6
    .param p0, "arr"    # [J

    .prologue
    .line 324
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    const-wide/16 v2, 0x8

    array-length v4, p0

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([S)J
    .locals 6
    .param p0, "arr"    # [S

    .prologue
    .line 309
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    const-wide/16 v2, 0x2

    array-length v4, p0

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static sizeOf([Z)J
    .locals 4
    .param p0, "arr"    # [Z

    .prologue
    .line 299
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_ARRAY_HEADER:I

    int-to-long v0, v0

    array-length v2, p0

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator;->alignObjectSize(J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public estimateRamUsage(Ljava/lang/Object;)J
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 879
    iget-boolean v0, p0, Lorg/apache/lucene/util/RamUsageEstimator;->checkInterned:Z

    invoke-static {p1, v0}, Lorg/apache/lucene/util/RamUsageEstimator;->measureObjectSize(Ljava/lang/Object;Z)J

    move-result-wide v0

    return-wide v0
.end method
