.class public Lorg/apache/lucene/util/AttributeSource;
.super Ljava/lang/Object;
.source "AttributeSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/AttributeSource$State;,
        Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final knownImplClasses:Lorg/apache/lucene/util/WeakIdentityMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/WeakIdentityMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/AttributeImpl;",
            ">;",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;>;>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final attributeImpls:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/AttributeImpl;",
            ">;",
            "Lorg/apache/lucene/util/AttributeImpl;",
            ">;"
        }
    .end annotation
.end field

.field private final attributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;",
            "Lorg/apache/lucene/util/AttributeImpl;",
            ">;"
        }
    .end annotation
.end field

.field private final currentState:[Lorg/apache/lucene/util/AttributeSource$State;

.field private final factory:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lorg/apache/lucene/util/AttributeSource;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/AttributeSource;->$assertionsDisabled:Z

    .line 208
    invoke-static {}, Lorg/apache/lucene/util/WeakIdentityMap;->newConcurrentHashMap()Lorg/apache/lucene/util/WeakIdentityMap;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/util/AttributeSource;->knownImplClasses:Lorg/apache/lucene/util/WeakIdentityMap;

    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->DEFAULT_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/AttributeSource;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V

    .line 137
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V
    .locals 1
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    .line 157
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    .line 158
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/lucene/util/AttributeSource$State;

    iput-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->currentState:[Lorg/apache/lucene/util/AttributeSource$State;

    .line 159
    iput-object p1, p0, Lorg/apache/lucene/util/AttributeSource;->factory:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    .line 160
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource;)V
    .locals 2
    .param p1, "input"    # Lorg/apache/lucene/util/AttributeSource;

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    if-nez p1, :cond_0

    .line 144
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "input AttributeSource must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    iput-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    .line 147
    iget-object v0, p1, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    iput-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    .line 148
    iget-object v0, p1, Lorg/apache/lucene/util/AttributeSource;->currentState:[Lorg/apache/lucene/util/AttributeSource$State;

    iput-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->currentState:[Lorg/apache/lucene/util/AttributeSource$State;

    .line 149
    iget-object v0, p1, Lorg/apache/lucene/util/AttributeSource;->factory:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    iput-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->factory:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    .line 150
    return-void
.end method

.method static getAttributeInterfaces(Ljava/lang/Class;)Ljava/util/LinkedList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/AttributeImpl;",
            ">;)",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 212
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/AttributeImpl;>;"
    sget-object v6, Lorg/apache/lucene/util/AttributeSource;->knownImplClasses:Lorg/apache/lucene/util/WeakIdentityMap;

    invoke-virtual {v6, p0}, Lorg/apache/lucene/util/WeakIdentityMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/LinkedList;

    .line 213
    .local v3, "foundInterfaces":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;>;>;"
    if-nez v3, :cond_3

    .line 215
    new-instance v3, Ljava/util/LinkedList;

    .end local v3    # "foundInterfaces":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;>;>;"
    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 218
    .restart local v3    # "foundInterfaces":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;>;>;"
    move-object v0, p0

    .line 220
    .local v0, "actClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/Class;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v2, v1, v4

    .line 221
    .local v2, "curInterface":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v6, Lorg/apache/lucene/util/Attribute;

    if-eq v2, v6, :cond_1

    const-class v6, Lorg/apache/lucene/util/Attribute;

    invoke-virtual {v6, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 222
    new-instance v6, Ljava/lang/ref/WeakReference;

    const-class v7, Lorg/apache/lucene/util/Attribute;

    invoke-virtual {v2, v7}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 220
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 225
    .end local v2    # "curInterface":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 226
    if-nez v0, :cond_0

    .line 227
    sget-object v6, Lorg/apache/lucene/util/AttributeSource;->knownImplClasses:Lorg/apache/lucene/util/WeakIdentityMap;

    invoke-virtual {v6, p0, v3}, Lorg/apache/lucene/util/WeakIdentityMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    .end local v0    # "actClazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "arr$":[Ljava/lang/Class;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_3
    return-object v3
.end method

.method private getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 314
    iget-object v4, p0, Lorg/apache/lucene/util/AttributeSource;->currentState:[Lorg/apache/lucene/util/AttributeSource$State;

    aget-object v2, v4, v5

    .line 315
    .local v2, "s":Lorg/apache/lucene/util/AttributeSource$State;
    if-nez v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/AttributeSource;->hasAttributes()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move-object v3, v2

    .line 326
    .end local v2    # "s":Lorg/apache/lucene/util/AttributeSource$State;
    .local v3, "s":Ljava/lang/Object;
    :goto_0
    return-object v3

    .line 318
    .end local v3    # "s":Ljava/lang/Object;
    .restart local v2    # "s":Lorg/apache/lucene/util/AttributeSource$State;
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/util/AttributeSource;->currentState:[Lorg/apache/lucene/util/AttributeSource$State;

    new-instance v2, Lorg/apache/lucene/util/AttributeSource$State;

    .end local v2    # "s":Lorg/apache/lucene/util/AttributeSource$State;
    invoke-direct {v2}, Lorg/apache/lucene/util/AttributeSource$State;-><init>()V

    aput-object v2, v4, v5

    .restart local v2    # "s":Lorg/apache/lucene/util/AttributeSource$State;
    move-object v0, v2

    .line 319
    .local v0, "c":Lorg/apache/lucene/util/AttributeSource$State;
    iget-object v4, p0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 320
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/util/AttributeImpl;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/AttributeImpl;

    iput-object v4, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    .line 321
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 322
    new-instance v4, Lorg/apache/lucene/util/AttributeSource$State;

    invoke-direct {v4}, Lorg/apache/lucene/util/AttributeSource$State;-><init>()V

    iput-object v4, v0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    .line 323
    iget-object v0, v0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    .line 324
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/AttributeImpl;

    iput-object v4, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    goto :goto_1

    :cond_2
    move-object v3, v2

    .line 326
    .restart local v3    # "s":Ljava/lang/Object;
    goto :goto_0
.end method


# virtual methods
.method public addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Lorg/apache/lucene/util/Attribute;",
            ">(",
            "Ljava/lang/Class",
            "<TA;>;)TA;"
        }
    .end annotation

    .prologue
    .line 268
    .local p1, "attClass":Ljava/lang/Class;, "Ljava/lang/Class<TA;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/AttributeImpl;

    .line 269
    .local v0, "attImpl":Lorg/apache/lucene/util/AttributeImpl;
    if-nez v0, :cond_2

    .line 270
    invoke-virtual {p1}, Ljava/lang/Class;->isInterface()Z

    move-result v1

    if-eqz v1, :cond_0

    const-class v1, Lorg/apache/lucene/util/Attribute;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 271
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "addAttribute() only accepts an interface that extends Attribute, but "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " does not fulfil this contract."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 276
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/AttributeSource;->factory:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->createAttributeInstance(Ljava/lang/Class;)Lorg/apache/lucene/util/AttributeImpl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/AttributeSource;->addAttributeImpl(Lorg/apache/lucene/util/AttributeImpl;)V

    .line 278
    :cond_2
    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/Attribute;

    return-object v1
.end method

.method public addAttributeImpl(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 8
    .param p1, "att"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 241
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 242
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/AttributeImpl;>;"
    iget-object v5, p0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 259
    :cond_0
    return-void

    .line 243
    :cond_1
    invoke-static {v0}, Lorg/apache/lucene/util/AttributeSource;->getAttributeInterfaces(Ljava/lang/Class;)Ljava/util/LinkedList;

    move-result-object v3

    .line 247
    .local v3, "foundInterfaces":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/ref/WeakReference<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;>;>;"
    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 248
    .local v2, "curInterfaceRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 250
    .local v1, "curInterface":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;"
    sget-boolean v5, Lorg/apache/lucene/util/AttributeSource;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    if-nez v1, :cond_3

    new-instance v5, Ljava/lang/AssertionError;

    const-string/jumbo v6, "We have a strong reference on the class holding the interfaces, so they should never get evicted"

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 252
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 254
    iget-object v5, p0, Lorg/apache/lucene/util/AttributeSource;->currentState:[Lorg/apache/lucene/util/AttributeSource$State;

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput-object v7, v5, v6

    .line 255
    iget-object v5, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v5, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    iget-object v5, p0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    invoke-interface {v5, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public captureState()Lorg/apache/lucene/util/AttributeSource$State;
    .locals 2

    .prologue
    .line 344
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    .line 345
    .local v0, "state":Lorg/apache/lucene/util/AttributeSource$State;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/util/AttributeSource$State;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_0
.end method

.method public clearAttributes()V
    .locals 2

    .prologue
    .line 334
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    .local v0, "state":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_0
    if-eqz v0, :cond_0

    .line 335
    iget-object v1, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v1}, Lorg/apache/lucene/util/AttributeImpl;->clear()V

    .line 334
    iget-object v0, v0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_0

    .line 337
    :cond_0
    return-void
.end method

.method public cloneAttributes()Lorg/apache/lucene/util/AttributeSource;
    .locals 8

    .prologue
    .line 496
    new-instance v0, Lorg/apache/lucene/util/AttributeSource;

    iget-object v4, p0, Lorg/apache/lucene/util/AttributeSource;->factory:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-direct {v0, v4}, Lorg/apache/lucene/util/AttributeSource;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V

    .line 498
    .local v0, "clone":Lorg/apache/lucene/util/AttributeSource;
    invoke-virtual {p0}, Lorg/apache/lucene/util/AttributeSource;->hasAttributes()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 500
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v3

    .local v3, "state":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_0
    if-eqz v3, :cond_0

    .line 501
    iget-object v5, v0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    iget-object v4, v3, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    iget-object v4, v3, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v4}, Lorg/apache/lucene/util/AttributeImpl;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/AttributeImpl;

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    iget-object v3, v3, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_0

    .line 505
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 506
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;Lorg/apache/lucene/util/AttributeImpl;>;"
    iget-object v5, v0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, v0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 510
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;Lorg/apache/lucene/util/AttributeImpl;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "state":Lorg/apache/lucene/util/AttributeSource$State;
    :cond_1
    return-object v0
.end method

.method public final copyTo(Lorg/apache/lucene/util/AttributeSource;)V
    .locals 5
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeSource;

    .prologue
    .line 522
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    .local v0, "state":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_0
    if-eqz v0, :cond_1

    .line 523
    iget-object v2, p1, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    iget-object v3, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/AttributeImpl;

    .line 524
    .local v1, "targetImpl":Lorg/apache/lucene/util/AttributeImpl;
    if-nez v1, :cond_0

    .line 525
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "This AttributeSource contains AttributeImpl of type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " that is not in the target"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 528
    :cond_0
    iget-object v2, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/util/AttributeImpl;->copyTo(Lorg/apache/lucene/util/AttributeImpl;)V

    .line 522
    iget-object v0, v0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_0

    .line 530
    .end local v1    # "targetImpl":Lorg/apache/lucene/util/AttributeImpl;
    :cond_1
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 388
    if-ne p1, p0, :cond_1

    move v4, v3

    .line 419
    :cond_0
    :goto_0
    return v4

    .line 392
    :cond_1
    instance-of v5, p1, Lorg/apache/lucene/util/AttributeSource;

    if-eqz v5, :cond_0

    move-object v0, p1

    .line 393
    check-cast v0, Lorg/apache/lucene/util/AttributeSource;

    .line 395
    .local v0, "other":Lorg/apache/lucene/util/AttributeSource;
    invoke-virtual {p0}, Lorg/apache/lucene/util/AttributeSource;->hasAttributes()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 396
    invoke-virtual {v0}, Lorg/apache/lucene/util/AttributeSource;->hasAttributes()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 400
    iget-object v5, p0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    iget-object v6, v0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v6

    if-ne v5, v6, :cond_0

    .line 405
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v2

    .line 406
    .local v2, "thisState":Lorg/apache/lucene/util/AttributeSource$State;
    invoke-direct {v0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v1

    .line 407
    .local v1, "otherState":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    .line 408
    iget-object v5, v1, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    iget-object v6, v2, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    if-ne v5, v6, :cond_0

    iget-object v5, v1, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    iget-object v6, v2, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 411
    iget-object v2, v2, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    .line 412
    iget-object v1, v1, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_1

    :cond_2
    move v4, v3

    .line 414
    goto :goto_0

    .line 416
    .end local v1    # "otherState":Lorg/apache/lucene/util/AttributeSource$State;
    .end local v2    # "thisState":Lorg/apache/lucene/util/AttributeSource$State;
    :cond_3
    invoke-virtual {v0}, Lorg/apache/lucene/util/AttributeSource;->hasAttributes()Z

    move-result v5

    if-nez v5, :cond_4

    :goto_2
    move v4, v3

    goto :goto_0

    :cond_4
    move v3, v4

    goto :goto_2
.end method

.method public getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Lorg/apache/lucene/util/Attribute;",
            ">(",
            "Ljava/lang/Class",
            "<TA;>;)TA;"
        }
    .end annotation

    .prologue
    .line 306
    .local p1, "attClass":Ljava/lang/Class;, "Ljava/lang/Class<TA;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/AttributeImpl;

    .line 307
    .local v0, "attImpl":Lorg/apache/lucene/util/AttributeImpl;
    if-nez v0, :cond_0

    .line 308
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "This AttributeSource does not have the attribute \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\'."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 310
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/Attribute;

    return-object v1
.end method

.method public getAttributeClassesIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getAttributeFactory()Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->factory:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    return-object v0
.end method

.method public getAttributeImplsIterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/util/AttributeImpl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    .line 182
    .local v0, "initState":Lorg/apache/lucene/util/AttributeSource$State;
    if-eqz v0, :cond_0

    .line 183
    new-instance v1, Lorg/apache/lucene/util/AttributeSource$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/util/AttributeSource$1;-><init>(Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 203
    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    goto :goto_0
.end method

.method public hasAttribute(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 291
    .local p1, "attClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hasAttributes()Z
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lorg/apache/lucene/util/AttributeSource;->attributes:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 379
    const/4 v0, 0x0

    .line 380
    .local v0, "code":I
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v1

    .local v1, "state":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_0
    if-eqz v1, :cond_0

    .line 381
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, v1, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    add-int v0, v2, v3

    .line 380
    iget-object v1, v1, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_0

    .line 383
    :cond_0
    return v0
.end method

.method public final reflectAsString(Z)Ljava/lang/String;
    .locals 2
    .param p1, "prependAttClass"    # Z

    .prologue
    .line 458
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 459
    .local v0, "buffer":Ljava/lang/StringBuilder;
    new-instance v1, Lorg/apache/lucene/util/AttributeSource$2;

    invoke-direct {v1, p0, v0, p1}, Lorg/apache/lucene/util/AttributeSource$2;-><init>(Lorg/apache/lucene/util/AttributeSource;Ljava/lang/StringBuilder;Z)V

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/AttributeSource;->reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V

    .line 470
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public final reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V
    .locals 2
    .param p1, "reflector"    # Lorg/apache/lucene/util/AttributeReflector;

    .prologue
    .line 483
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    .local v0, "state":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_0
    if-eqz v0, :cond_0

    .line 484
    iget-object v1, v0, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/AttributeImpl;->reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V

    .line 483
    iget-object v0, v0, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_0

    .line 486
    :cond_0
    return-void
.end method

.method public restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V
    .locals 4
    .param p1, "state"    # Lorg/apache/lucene/util/AttributeSource$State;

    .prologue
    .line 364
    if-nez p1, :cond_0

    .line 375
    :goto_0
    return-void

    .line 367
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/AttributeSource;->attributeImpls:Ljava/util/Map;

    iget-object v2, p1, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/AttributeImpl;

    .line 368
    .local v0, "targetImpl":Lorg/apache/lucene/util/AttributeImpl;
    if-nez v0, :cond_1

    .line 369
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "State contains AttributeImpl of type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " that is not in in this AttributeSource"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 372
    :cond_1
    iget-object v1, p1, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/util/AttributeImpl;->copyTo(Lorg/apache/lucene/util/AttributeImpl;)V

    .line 373
    iget-object p1, p1, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    .line 374
    if-nez p1, :cond_0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 436
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x28

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 437
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/util/AttributeSource;->hasAttributes()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 438
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;->getCurrentState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v1

    .local v1, "state":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_0
    if-eqz v1, :cond_1

    .line 439
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 440
    :cond_0
    iget-object v2, v1, Lorg/apache/lucene/util/AttributeSource$State;->attribute:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v2}, Lorg/apache/lucene/util/AttributeImpl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 438
    iget-object v1, v1, Lorg/apache/lucene/util/AttributeSource$State;->next:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_0

    .line 443
    .end local v1    # "state":Lorg/apache/lucene/util/AttributeSource$State;
    :cond_1
    const/16 v2, 0x29

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
