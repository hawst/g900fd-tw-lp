.class public abstract Lorg/apache/lucene/util/Counter;
.super Ljava/lang/Object;
.source "Counter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/Counter$1;,
        Lorg/apache/lucene/util/Counter$AtomicCounter;,
        Lorg/apache/lucene/util/Counter$SerialCounter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    return-void
.end method

.method public static newCounter()Lorg/apache/lucene/util/Counter;
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-static {v0}, Lorg/apache/lucene/util/Counter;->newCounter(Z)Lorg/apache/lucene/util/Counter;

    move-result-object v0

    return-object v0
.end method

.method public static newCounter(Z)Lorg/apache/lucene/util/Counter;
    .locals 2
    .param p0, "threadSafe"    # Z

    .prologue
    const/4 v1, 0x0

    .line 62
    if-eqz p0, :cond_0

    new-instance v0, Lorg/apache/lucene/util/Counter$AtomicCounter;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/Counter$AtomicCounter;-><init>(Lorg/apache/lucene/util/Counter$1;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/util/Counter$SerialCounter;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/Counter$SerialCounter;-><init>(Lorg/apache/lucene/util/Counter$1;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract addAndGet(J)J
.end method

.method public abstract get()J
.end method
