.class public interface abstract Lorg/apache/lucene/util/Bits;
.super Ljava/lang/Object;
.source "Bits.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/Bits$MatchNoBits;,
        Lorg/apache/lucene/util/Bits$MatchAllBits;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lorg/apache/lucene/util/Bits;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/lucene/util/Bits;

    sput-object v0, Lorg/apache/lucene/util/Bits;->EMPTY_ARRAY:[Lorg/apache/lucene/util/Bits;

    return-void
.end method


# virtual methods
.method public abstract get(I)Z
.end method

.method public abstract length()I
.end method
