.class final Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;
.super Ljava/lang/Object;
.source "RamUsageEstimator.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/RamUsageEstimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "IdentityHashSet"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<KType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<TKType;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT_LOAD_FACTOR:F = 0.75f

.field public static final MIN_CAPACITY:I = 0x4


# instance fields
.field public assigned:I

.field public keys:[Ljava/lang/Object;

.field public final loadFactor:F

.field private resizeThreshold:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 609
    const-class v0, Lorg/apache/lucene/util/RamUsageEstimator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 646
    .local p0, "this":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<TKType;>;"
    const/16 v0, 0x10

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;-><init>(IF)V

    .line 647
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "initialCapacity"    # I

    .prologue
    .line 654
    .local p0, "this":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<TKType;>;"
    const/high16 v0, 0x3f400000    # 0.75f

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;-><init>(IF)V

    .line 655
    return-void
.end method

.method public constructor <init>(IF)V
    .locals 2
    .param p1, "initialCapacity"    # I
    .param p2, "loadFactor"    # F

    .prologue
    .line 660
    .local p0, "this":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<TKType;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 661
    const/4 v0, 0x4

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 664
    sget-boolean v0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-gtz p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Initial capacity must be between (0, 2147483647]."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 665
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    const/4 v0, 0x0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p2, v0

    if-ltz v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Load factor must be between (0, 1)."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 666
    :cond_2
    iput p2, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->loadFactor:F

    .line 667
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->roundCapacity(I)I

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->allocateBuffers(I)V

    .line 668
    return-void
.end method

.method private allocateBuffers(I)V
    .locals 2
    .param p1, "capacity"    # I

    .prologue
    .line 760
    .local p0, "this":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<TKType;>;"
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    .line 761
    int-to-float v0, p1

    const/high16 v1, 0x3f400000    # 0.75f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->resizeThreshold:I

    .line 762
    return-void
.end method

.method private expandAndRehash()V
    .locals 7

    .prologue
    .line 731
    .local p0, "this":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<TKType;>;"
    iget-object v3, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    .line 733
    .local v3, "oldKeys":[Ljava/lang/Object;
    sget-boolean v5, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    iget v5, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->assigned:I

    iget v6, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->resizeThreshold:I

    if-ge v5, v6, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 734
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    array-length v5, v5

    invoke-virtual {p0, v5}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->nextCapacity(I)I

    move-result v5

    invoke-direct {p0, v5}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->allocateBuffers(I)V

    .line 739
    iget-object v5, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    array-length v5, v5

    add-int/lit8 v2, v5, -0x1

    .line 740
    .local v2, "mask":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, v3

    if-ge v0, v5, :cond_3

    .line 741
    aget-object v1, v3, v0

    .line 742
    .local v1, "key":Ljava/lang/Object;
    if-eqz v1, :cond_2

    .line 743
    invoke-static {v1}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->rehash(Ljava/lang/Object;)I

    move-result v5

    and-int v4, v5, v2

    .line 744
    .local v4, "slot":I
    :goto_1
    iget-object v5, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    aget-object v5, v5, v4

    if-eqz v5, :cond_1

    .line 745
    add-int/lit8 v5, v4, 0x1

    and-int v4, v5, v2

    goto :goto_1

    .line 747
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    aput-object v1, v5, v4

    .line 740
    .end local v4    # "slot":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 750
    .end local v1    # "key":Ljava/lang/Object;
    :cond_3
    const/4 v5, 0x0

    invoke-static {v3, v5}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 751
    return-void
.end method

.method private static rehash(Ljava/lang/Object;)I
    .locals 2
    .param p0, "o"    # Ljava/lang/Object;

    .prologue
    .line 717
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    .line 718
    .local v0, "k":I
    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    .line 719
    const v1, -0x7a143595

    mul-int/2addr v0, v1

    .line 720
    ushr-int/lit8 v1, v0, 0xd

    xor-int/2addr v0, v1

    .line 721
    const v1, -0x3d4d51cb

    mul-int/2addr v0, v1

    .line 722
    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    .line 723
    return v0
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKType;)Z"
        }
    .end annotation

    .prologue
    .line 674
    .local p0, "this":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<TKType;>;"
    .local p1, "e":Ljava/lang/Object;, "TKType;"
    sget-boolean v3, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-nez p1, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    const-string/jumbo v4, "Null keys not allowed."

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 676
    :cond_0
    iget v3, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->assigned:I

    iget v4, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->resizeThreshold:I

    if-lt v3, v4, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->expandAndRehash()V

    .line 678
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    array-length v3, v3

    add-int/lit8 v1, v3, -0x1

    .line 679
    .local v1, "mask":I
    invoke-static {p1}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->rehash(Ljava/lang/Object;)I

    move-result v3

    and-int v2, v3, v1

    .line 681
    .local v2, "slot":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    aget-object v0, v3, v2

    .local v0, "existing":Ljava/lang/Object;
    if-eqz v0, :cond_3

    .line 682
    if-ne p1, v0, :cond_2

    .line 683
    const/4 v3, 0x0

    .line 689
    :goto_1
    return v3

    .line 685
    :cond_2
    add-int/lit8 v3, v2, 0x1

    and-int v2, v3, v1

    goto :goto_0

    .line 687
    :cond_3
    iget v3, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->assigned:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->assigned:I

    .line 688
    iget-object v3, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    aput-object p1, v3, v2

    .line 689
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 792
    .local p0, "this":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<TKType;>;"
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->assigned:I

    .line 793
    iget-object v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 794
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKType;)Z"
        }
    .end annotation

    .prologue
    .line 696
    .local p0, "this":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<TKType;>;"
    .local p1, "e":Ljava/lang/Object;, "TKType;"
    iget-object v3, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    array-length v3, v3

    add-int/lit8 v1, v3, -0x1

    .line 697
    .local v1, "mask":I
    invoke-static {p1}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->rehash(Ljava/lang/Object;)I

    move-result v3

    and-int v2, v3, v1

    .line 699
    .local v2, "slot":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->keys:[Ljava/lang/Object;

    aget-object v0, v3, v2

    .local v0, "existing":Ljava/lang/Object;
    if-eqz v0, :cond_1

    .line 700
    if-ne p1, v0, :cond_0

    .line 701
    const/4 v3, 0x1

    .line 705
    :goto_1
    return v3

    .line 703
    :cond_0
    add-int/lit8 v3, v2, 0x1

    and-int v2, v3, v1

    goto :goto_0

    .line 705
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 801
    .local p0, "this":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<TKType;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TKType;>;"
        }
    .end annotation

    .prologue
    .line 806
    .local p0, "this":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<TKType;>;"
    new-instance v0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet$1;-><init>(Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;)V

    return-object v0
.end method

.method protected nextCapacity(I)I
    .locals 2
    .param p1, "current"    # I

    .prologue
    .line 768
    .local p0, "this":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<TKType;>;"
    sget-boolean v0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-lez p1, :cond_0

    int-to-long v0, p1

    invoke-static {v0, v1}, Ljava/lang/Long;->bitCount(J)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Capacity must be a power of two."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 770
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    shl-int/lit8 v0, p1, 0x1

    if-gtz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Maximum capacity exceeded (1073741824)."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 772
    :cond_2
    const/4 v0, 0x2

    if-ge p1, v0, :cond_3

    const/4 p1, 0x2

    .line 773
    :cond_3
    shl-int/lit8 v0, p1, 0x1

    return v0
.end method

.method protected roundCapacity(I)I
    .locals 1
    .param p1, "requestedCapacity"    # I

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<TKType;>;"
    const/high16 v0, 0x40000000    # 2.0f

    .line 781
    if-le p1, v0, :cond_1

    .line 788
    :cond_0
    return v0

    .line 783
    :cond_1
    const/4 v0, 0x4

    .line 784
    .local v0, "capacity":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 785
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 797
    .local p0, "this":Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;, "Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet<TKType;>;"
    iget v0, p0, Lorg/apache/lucene/util/RamUsageEstimator$IdentityHashSet;->assigned:I

    return v0
.end method
