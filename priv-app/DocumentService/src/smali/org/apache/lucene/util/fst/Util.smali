.class public final Lorg/apache/lucene/util/fst/Util;
.super Ljava/lang/Object;
.source "Util.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/fst/Util$MinResult;,
        Lorg/apache/lucene/util/fst/Util$TopNSearcher;,
        Lorg/apache/lucene/util/fst/Util$FSTPath;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lorg/apache/lucene/util/fst/Util;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/Util;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method private static emitDotState(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "out"    # Ljava/io/Writer;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "shape"    # Ljava/lang/String;
    .param p3, "color"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 774
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "shape="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p3, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "color="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p4, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "label=\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 780
    return-void

    .line 774
    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0

    :cond_1
    const-string/jumbo v0, ""

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "label=\"\""

    goto :goto_2
.end method

.method public static get(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/BytesRef;)Ljava/lang/Object;
    .locals 7
    .param p1, "input"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;",
            "Lorg/apache/lucene/util/BytesRef;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/4 v4, 0x0

    .line 63
    sget-boolean v5, Lorg/apache/lucene/util/fst/Util;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->inputType:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    sget-object v6, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    if-eq v5, v6, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 65
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    .line 68
    .local v1, "fstReader":Lorg/apache/lucene/util/fst/FST$BytesReader;
    new-instance v5, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v5}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    invoke-virtual {p0, v5}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 71
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v3

    .line 72
    .local v3, "output":Ljava/lang/Object;, "TT;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v5, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v2, v5, :cond_3

    .line 73
    iget-object v5, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v6, v2

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    invoke-virtual {p0, v5, v0, v0, v1}, Lorg/apache/lucene/util/fst/FST;->findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v5

    if-nez v5, :cond_2

    .line 82
    :cond_1
    :goto_1
    return-object v4

    .line 76
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v6, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v5, v3, v6}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 72
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 79
    :cond_3
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 80
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    invoke-virtual {v4, v3, v5}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    goto :goto_1
.end method

.method public static get(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/IntsRef;)Ljava/lang/Object;
    .locals 7
    .param p1, "input"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;",
            "Lorg/apache/lucene/util/IntsRef;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/4 v4, 0x0

    .line 38
    new-instance v5, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v5}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    invoke-virtual {p0, v5}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 40
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    .line 43
    .local v1, "fstReader":Lorg/apache/lucene/util/fst/FST$BytesReader;
    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v3

    .line 44
    .local v3, "output":Ljava/lang/Object;, "TT;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v5, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    if-ge v2, v5, :cond_2

    .line 45
    iget-object v5, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v6, p1, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int/2addr v6, v2

    aget v5, v5, v6

    invoke-virtual {p0, v5, v0, v0, v1}, Lorg/apache/lucene/util/fst/FST;->findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v5

    if-nez v5, :cond_1

    .line 54
    :cond_0
    :goto_1
    return-object v4

    .line 48
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v6, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v5, v3, v6}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 44
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 51
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 52
    iget-object v4, p0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    invoke-virtual {v4, v3, v5}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    goto :goto_1
.end method

.method public static getByOutput(Lorg/apache/lucene/util/fst/FST;J)Lorg/apache/lucene/util/IntsRef;
    .locals 25
    .param p1, "targetOutput"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Ljava/lang/Long;",
            ">;J)",
            "Lorg/apache/lucene/util/IntsRef;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    .local p0, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v10

    .line 105
    .local v10, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    new-instance v22, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct/range {v22 .. v22}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v2

    .line 107
    .local v2, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    new-instance v19, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct/range {v19 .. v19}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    .line 109
    .local v19, "scratchArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    new-instance v18, Lorg/apache/lucene/util/IntsRef;

    invoke-direct/range {v18 .. v18}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    .line 111
    .local v18, "result":Lorg/apache/lucene/util/IntsRef;
    iget-object v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 112
    .local v16, "output":J
    const/16 v20, 0x0

    .line 118
    .local v20, "upto":I
    :goto_0
    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v22

    if-eqz v22, :cond_1

    .line 119
    iget-object v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    add-long v6, v16, v22

    .line 121
    .local v6, "finalOutput":J
    cmp-long v22, v6, p1

    if-nez v22, :cond_0

    .line 122
    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 228
    .end local v6    # "finalOutput":J
    .end local v18    # "result":Lorg/apache/lucene/util/IntsRef;
    :goto_1
    return-object v18

    .line 125
    .restart local v6    # "finalOutput":J
    .restart local v18    # "result":Lorg/apache/lucene/util/IntsRef;
    :cond_0
    cmp-long v22, v6, p1

    if-lez v22, :cond_1

    .line 127
    const/16 v18, 0x0

    goto :goto_1

    .line 131
    .end local v6    # "finalOutput":J
    :cond_1
    invoke-static {v2}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v22

    if-eqz v22, :cond_e

    .line 133
    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    .line 134
    add-int/lit8 v22, v20, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 137
    :cond_2
    iget v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1, v2, v10}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 139
    iget v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    move/from16 v22, v0

    if-eqz v22, :cond_9

    .line 141
    const/4 v11, 0x0

    .line 142
    .local v11, "low":I
    iget v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    move/from16 v22, v0

    add-int/lit8 v9, v22, -0x1

    .line 143
    .local v9, "high":I
    const/4 v12, 0x0

    .line 145
    .local v12, "mid":I
    const/4 v3, 0x0

    .line 146
    .local v3, "exact":Z
    :goto_2
    if-gt v11, v9, :cond_3

    .line 147
    add-int v22, v11, v9

    ushr-int/lit8 v12, v22, 0x1

    .line 148
    iget v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:I

    move/from16 v22, v0

    move/from16 v0, v22

    iput v0, v10, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    .line 149
    iget v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    move/from16 v22, v0

    mul-int v22, v22, v12

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skip(I)V

    .line 150
    invoke-virtual {v10}, Lorg/apache/lucene/util/fst/FST$BytesReader;->readByte()B

    move-result v8

    .line 151
    .local v8, "flags":B
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    .line 153
    and-int/lit8 v22, v8, 0x10

    if-eqz v22, :cond_4

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Lorg/apache/lucene/util/fst/Outputs;->read(Lorg/apache/lucene/store/DataInput;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 155
    .local v4, "arcOutput":J
    add-long v14, v16, v4

    .line 160
    .end local v4    # "arcOutput":J
    .local v14, "minArcOutput":J
    :goto_3
    cmp-long v22, v14, p1

    if-nez v22, :cond_5

    .line 161
    const/4 v3, 0x1

    .line 170
    .end local v8    # "flags":B
    .end local v14    # "minArcOutput":J
    :cond_3
    const/16 v22, -0x1

    move/from16 v0, v22

    if-ne v9, v0, :cond_7

    .line 171
    const/16 v18, 0x0

    goto/16 :goto_1

    .line 157
    .restart local v8    # "flags":B
    :cond_4
    move-wide/from16 v14, v16

    .restart local v14    # "minArcOutput":J
    goto :goto_3

    .line 163
    :cond_5
    cmp-long v22, v14, p1

    if-gez v22, :cond_6

    .line 164
    add-int/lit8 v11, v12, 0x1

    goto :goto_2

    .line 166
    :cond_6
    add-int/lit8 v9, v12, -0x1

    goto :goto_2

    .line 172
    .end local v8    # "flags":B
    .end local v14    # "minArcOutput":J
    :cond_7
    if-eqz v3, :cond_8

    .line 173
    add-int/lit8 v22, v12, -0x1

    move/from16 v0, v22

    iput v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 178
    :goto_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v10}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 179
    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v22, v0

    add-int/lit8 v21, v20, 0x1

    .end local v20    # "upto":I
    .local v21, "upto":I
    iget v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move/from16 v23, v0

    aput v23, v22, v20

    .line 180
    iget-object v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    add-long v16, v16, v22

    move/from16 v20, v21

    .line 182
    .end local v21    # "upto":I
    .restart local v20    # "upto":I
    goto/16 :goto_0

    .line 175
    :cond_8
    add-int/lit8 v22, v11, -0x2

    move/from16 v0, v22

    iput v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    goto :goto_4

    .line 184
    .end local v3    # "exact":Z
    .end local v9    # "high":I
    .end local v11    # "low":I
    .end local v12    # "mid":I
    :cond_9
    const/4 v13, 0x0

    .line 191
    .local v13, "prevArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    :goto_5
    iget-object v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    add-long v14, v16, v22

    .line 193
    .restart local v14    # "minArcOutput":J
    cmp-long v22, v14, p1

    if-nez v22, :cond_a

    .line 196
    move-wide/from16 v16, v14

    .line 197
    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v22, v0

    add-int/lit8 v21, v20, 0x1

    .end local v20    # "upto":I
    .restart local v21    # "upto":I
    iget v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move/from16 v23, v0

    aput v23, v22, v20

    move/from16 v20, v21

    .line 198
    .end local v21    # "upto":I
    .restart local v20    # "upto":I
    goto/16 :goto_0

    .line 199
    :cond_a
    cmp-long v22, v14, p1

    if-lez v22, :cond_c

    .line 200
    if-nez v13, :cond_b

    .line 202
    const/16 v18, 0x0

    goto/16 :goto_1

    .line 205
    :cond_b
    invoke-virtual {v2, v13}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 206
    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v22, v0

    add-int/lit8 v21, v20, 0x1

    .end local v20    # "upto":I
    .restart local v21    # "upto":I
    iget v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move/from16 v23, v0

    aput v23, v22, v20

    .line 207
    iget-object v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    add-long v16, v16, v22

    move/from16 v20, v21

    .line 209
    .end local v21    # "upto":I
    .restart local v20    # "upto":I
    goto/16 :goto_0

    .line 211
    :cond_c
    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v22

    if-eqz v22, :cond_d

    .line 213
    move-wide/from16 v16, v14

    .line 215
    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v22, v0

    add-int/lit8 v21, v20, 0x1

    .end local v20    # "upto":I
    .restart local v21    # "upto":I
    iget v0, v2, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move/from16 v23, v0

    aput v23, v22, v20

    move/from16 v20, v21

    .line 216
    .end local v21    # "upto":I
    .restart local v20    # "upto":I
    goto/16 :goto_0

    .line 219
    :cond_d
    move-object/from16 v13, v19

    .line 220
    invoke-virtual {v13, v2}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 222
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v10}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_5

    .line 228
    .end local v13    # "prevArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    .end local v14    # "minArcOutput":J
    :cond_e
    const/16 v18, 0x0

    goto/16 :goto_1
.end method

.method private static printableLabel(I)Ljava/lang/String;
    .locals 2
    .param p0, "label"    # I

    .prologue
    .line 786
    const/16 v0, 0x20

    if-lt p0, v0, :cond_0

    const/16 v0, 0x7d

    if-gt p0, v0, :cond_0

    .line 787
    int-to-char v0, p0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    .line 789
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static shortestPaths(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/fst/FST$Arc;Ljava/util/Comparator;I)[Lorg/apache/lucene/util/fst/Util$MinResult;
    .locals 1
    .param p3, "topN"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;",
            "Ljava/util/Comparator",
            "<TT;>;I)[",
            "Lorg/apache/lucene/util/fst/Util$MinResult",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 553
    .local p0, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p1, "fromNode":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p2, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<TT;>;"
    new-instance v0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;

    invoke-direct {v0, p0, p1, p3, p2}, Lorg/apache/lucene/util/fst/Util$TopNSearcher;-><init>(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/fst/FST$Arc;ILjava/util/Comparator;)V

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->search()[Lorg/apache/lucene/util/fst/Util$MinResult;

    move-result-object v0

    return-object v0
.end method

.method public static toBytesRef(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;
    .locals 4
    .param p0, "input"    # Lorg/apache/lucene/util/IntsRef;
    .param p1, "scratch"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 843
    iget v1, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-virtual {p1, v1}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 844
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    if-ge v0, v1, :cond_0

    .line 845
    iget-object v1, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v2, p0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget v3, p0, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int/2addr v3, v0

    aget v2, v2, v3

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 844
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 847
    :cond_0
    iget v1, p0, Lorg/apache/lucene/util/IntsRef;->length:I

    iput v1, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 848
    return-object p1
.end method

.method public static toDot(Lorg/apache/lucene/util/fst/FST;Ljava/io/Writer;ZZ)V
    .locals 30
    .param p1, "out"    # Ljava/io/Writer;
    .param p2, "sameRank"    # Z
    .param p3, "labelStates"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;",
            "Ljava/io/Writer;",
            "ZZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 588
    .local p0, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const-string/jumbo v8, "blue"

    .line 592
    .local v8, "expandedNodeColor":Ljava/lang/String;
    new-instance v26, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct/range {v26 .. v26}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v21

    .line 595
    .local v21, "startArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 598
    .local v25, "thisLevelQueue":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/fst/FST$Arc<TT;>;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 599
    .local v15, "nextLevelQueue":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/fst/FST$Arc<TT;>;>;"
    move-object/from16 v0, v21

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 603
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 606
    .local v19, "sameLevelStates":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v20, Ljava/util/BitSet;

    invoke-direct/range {v20 .. v20}, Ljava/util/BitSet;-><init>()V

    .line 607
    .local v20, "seen":Ljava/util/BitSet;
    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v26, v0

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 610
    const-string/jumbo v24, "circle"

    .line 611
    .local v24, "stateShape":Ljava/lang/String;
    const-string/jumbo v11, "doublecircle"

    .line 614
    .local v11, "finalStateShape":Ljava/lang/String;
    const-string/jumbo v26, "digraph FST {\n"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 615
    const-string/jumbo v26, "  rankdir = LR; splines=true; concentrate=true; ordering=out; ranksep=2.5; \n"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 617
    if-nez p3, :cond_0

    .line 618
    const-string/jumbo v26, "  node [shape=circle, width=.2, height=.2, style=filled]\n"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 621
    :cond_0
    const-string/jumbo v26, "initial"

    const-string/jumbo v27, "point"

    const-string/jumbo v28, "white"

    const-string/jumbo v29, ""

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    move-object/from16 v3, v28

    move-object/from16 v4, v29

    invoke-static {v0, v1, v2, v3, v4}, Lorg/apache/lucene/util/fst/Util;->emitDotState(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v5

    .line 629
    .local v5, "NO_OUTPUT":Ljava/lang/Object;, "TT;"
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST;->isExpandedTarget(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 630
    const-string/jumbo v23, "blue"

    .line 637
    .local v23, "stateColor":Ljava/lang/String;
    :goto_0
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v26

    if-eqz v26, :cond_6

    .line 638
    const/4 v13, 0x1

    .line 639
    .local v13, "isFinal":Z
    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    if-ne v0, v5, :cond_5

    const/4 v10, 0x0

    .line 645
    :goto_1
    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v26, v0

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v28

    if-eqz v13, :cond_7

    const-string/jumbo v26, "doublecircle"

    move-object/from16 v27, v26

    :goto_2
    if-nez v10, :cond_8

    const-string/jumbo v26, ""

    :goto_3
    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v27

    move-object/from16 v3, v23

    move-object/from16 v4, v26

    invoke-static {v0, v1, v2, v3, v4}, Lorg/apache/lucene/util/fst/Util;->emitDotState(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "  initial -> "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, "\n"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 650
    const/4 v14, 0x0

    .line 652
    .local v14, "level":I
    const/16 v26, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v18

    .line 654
    .local v18, "r":Lorg/apache/lucene/util/fst/FST$BytesReader;
    :goto_4
    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_12

    .line 657
    move-object/from16 v0, v25

    invoke-interface {v0, v15}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 658
    invoke-interface {v15}, Ljava/util/List;->clear()V

    .line 660
    add-int/lit8 v14, v14, 0x1

    .line 661
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "\n  // Transitions and states at level: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, "\n"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 662
    :cond_1
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_f

    .line 663
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v26

    add-int/lit8 v26, v26, -0x1

    invoke-interface/range {v25 .. v26}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/util/fst/FST$Arc;

    .line 665
    .local v6, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-static {v6}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v26

    if-eqz v26, :cond_1

    .line 668
    iget v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v16, v0

    .line 670
    .local v16, "node":I
    iget v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    move/from16 v1, v26

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v6, v2}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 678
    :goto_5
    iget v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v26, v0

    if-ltz v26, :cond_2

    iget v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v26, v0

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v26

    if-nez v26, :cond_2

    .line 692
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lorg/apache/lucene/util/fst/FST;->isExpandedTarget(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v26

    if-eqz v26, :cond_9

    .line 693
    const-string/jumbo v23, "blue"

    .line 699
    :goto_6
    iget-object v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v26, v0

    if-eqz v26, :cond_a

    iget-object v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    if-eq v0, v5, :cond_a

    .line 700
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v26, v0

    iget-object v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lorg/apache/lucene/util/fst/Outputs;->outputToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 705
    .local v9, "finalOutput":Ljava/lang/String;
    :goto_7
    iget v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v26, v0

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v26

    const-string/jumbo v27, "circle"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    move-object/from16 v3, v23

    invoke-static {v0, v1, v2, v3, v9}, Lorg/apache/lucene/util/fst/Util;->emitDotState(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    iget v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v26, v0

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 709
    new-instance v26, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct/range {v26 .. v26}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 710
    iget v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v26, v0

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 714
    .end local v9    # "finalOutput":Ljava/lang/String;
    :cond_2
    iget-object v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    if-eq v0, v5, :cond_b

    .line 715
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "/"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v27, v0

    iget-object v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Lorg/apache/lucene/util/fst/Outputs;->outputToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 720
    .local v17, "outs":Ljava/lang/String;
    :goto_8
    invoke-static {v6}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v26

    if-nez v26, :cond_3

    invoke-virtual {v6}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v26

    if-eqz v26, :cond_3

    iget-object v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    if-eq v0, v5, :cond_3

    .line 727
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, "/["

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v27, v0

    iget-object v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Lorg/apache/lucene/util/fst/Outputs;->outputToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, "]"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 731
    :cond_3
    const/16 v26, 0x4

    move/from16 v0, v26

    invoke-virtual {v6, v0}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v26

    if-eqz v26, :cond_c

    .line 732
    const-string/jumbo v7, "red"

    .line 737
    .local v7, "arcColor":Ljava/lang/String;
    :goto_9
    sget-boolean v26, Lorg/apache/lucene/util/fst/Util;->$assertionsDisabled:Z

    if-nez v26, :cond_d

    iget v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move/from16 v26, v0

    const/16 v27, -0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_d

    new-instance v26, Ljava/lang/AssertionError;

    invoke-direct/range {v26 .. v26}, Ljava/lang/AssertionError;-><init>()V

    throw v26

    .line 632
    .end local v6    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .end local v7    # "arcColor":Ljava/lang/String;
    .end local v13    # "isFinal":Z
    .end local v14    # "level":I
    .end local v16    # "node":I
    .end local v17    # "outs":Ljava/lang/String;
    .end local v18    # "r":Lorg/apache/lucene/util/fst/FST$BytesReader;
    .end local v23    # "stateColor":Ljava/lang/String;
    :cond_4
    const/16 v23, 0x0

    .restart local v23    # "stateColor":Ljava/lang/String;
    goto/16 :goto_0

    .line 639
    .restart local v13    # "isFinal":Z
    :cond_5
    move-object/from16 v0, v21

    iget-object v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    goto/16 :goto_1

    .line 641
    .end local v13    # "isFinal":Z
    :cond_6
    const/4 v13, 0x0

    .line 642
    .restart local v13    # "isFinal":Z
    const/4 v10, 0x0

    .local v10, "finalOutput":Ljava/lang/Object;, "TT;"
    goto/16 :goto_1

    .line 645
    .end local v10    # "finalOutput":Ljava/lang/Object;, "TT;"
    :cond_7
    const-string/jumbo v26, "circle"

    move-object/from16 v27, v26

    goto/16 :goto_2

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Lorg/apache/lucene/util/fst/Outputs;->outputToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_3

    .line 695
    .restart local v6    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .restart local v14    # "level":I
    .restart local v16    # "node":I
    .restart local v18    # "r":Lorg/apache/lucene/util/fst/FST$BytesReader;
    :cond_9
    const/16 v23, 0x0

    goto/16 :goto_6

    .line 702
    :cond_a
    const-string/jumbo v9, ""

    .restart local v9    # "finalOutput":Ljava/lang/String;
    goto/16 :goto_7

    .line 717
    .end local v9    # "finalOutput":Ljava/lang/String;
    :cond_b
    const-string/jumbo v17, ""

    .restart local v17    # "outs":Ljava/lang/String;
    goto/16 :goto_8

    .line 734
    :cond_c
    const-string/jumbo v7, "black"

    .restart local v7    # "arcColor":Ljava/lang/String;
    goto :goto_9

    .line 738
    :cond_d
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "  "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, " -> "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    iget v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, " [label=\""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    iget v0, v6, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lorg/apache/lucene/util/fst/Util;->printableLabel(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, "\""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v6}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v26

    if-eqz v26, :cond_e

    const-string/jumbo v26, " style=\"bold\""

    :goto_a
    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, " color=\""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, "\"]\n"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 741
    invoke-virtual {v6}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v26

    if-nez v26, :cond_1

    .line 745
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v6, v1}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto/16 :goto_5

    .line 738
    :cond_e
    const-string/jumbo v26, ""

    goto :goto_a

    .line 751
    .end local v6    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .end local v7    # "arcColor":Ljava/lang/String;
    .end local v16    # "node":I
    .end local v17    # "outs":Ljava/lang/String;
    :cond_f
    if-eqz p2, :cond_11

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-le v0, v1, :cond_11

    .line 752
    const-string/jumbo v26, "  {rank=same; "

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 753
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_b
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_10

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 754
    .local v22, "state":I
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, "; "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_b

    .line 756
    .end local v22    # "state":I
    :cond_10
    const-string/jumbo v26, " }\n"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 758
    .end local v12    # "i$":Ljava/util/Iterator;
    :cond_11
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->clear()V

    goto/16 :goto_4

    .line 762
    :cond_12
    const-string/jumbo v26, "  -1 [style=filled, color=black, shape=doublecircle, label=\"\"]\n\n"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 763
    const-string/jumbo v26, "  {rank=sink; -1 }\n"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 765
    const-string/jumbo v26, "}\n"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 766
    invoke-virtual/range {p1 .. p1}, Ljava/io/Writer;->flush()V

    .line 767
    return-void
.end method

.method public static toIntsRef(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;
    .locals 4
    .param p0, "input"    # Lorg/apache/lucene/util/BytesRef;
    .param p1, "scratch"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 832
    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p1, v1}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 833
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v0, v1, :cond_0

    .line 834
    iget-object v1, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v3, v0

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    aput v2, v1, v0

    .line 833
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 836
    :cond_0
    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v1, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 837
    return-object p1
.end method

.method public static toUTF32(Ljava/lang/CharSequence;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;
    .locals 5
    .param p0, "s"    # Ljava/lang/CharSequence;
    .param p1, "scratch"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 797
    const/4 v0, 0x0

    .line 798
    .local v0, "charIdx":I
    const/4 v2, 0x0

    .line 799
    .local v2, "intIdx":I
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 800
    .local v1, "charLimit":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 801
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p1, v4}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 802
    invoke-static {p0, v0}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v3

    .line 803
    .local v3, "utf32":I
    iget-object v4, p1, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aput v3, v4, v2

    .line 804
    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 805
    add-int/lit8 v2, v2, 0x1

    .line 806
    goto :goto_0

    .line 807
    .end local v3    # "utf32":I
    :cond_0
    iput v2, p1, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 808
    return-object p1
.end method

.method public static toUTF32([CIILorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;
    .locals 5
    .param p0, "s"    # [C
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "scratch"    # Lorg/apache/lucene/util/IntsRef;

    .prologue
    .line 815
    move v0, p1

    .line 816
    .local v0, "charIdx":I
    const/4 v2, 0x0

    .line 817
    .local v2, "intIdx":I
    add-int v1, p1, p2

    .line 818
    .local v1, "charLimit":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 819
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p3, v4}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 820
    invoke-static {p0, v0}, Ljava/lang/Character;->codePointAt([CI)I

    move-result v3

    .line 821
    .local v3, "utf32":I
    iget-object v4, p3, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aput v3, v4, v2

    .line 822
    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 823
    add-int/lit8 v2, v2, 0x1

    .line 824
    goto :goto_0

    .line 825
    .end local v3    # "utf32":I
    :cond_0
    iput v2, p3, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 826
    return-object p3
.end method
