.class public final Lorg/apache/lucene/util/CodecUtil;
.super Ljava/lang/Object;
.source "CodecUtil.java"


# static fields
.field private static final CODEC_MAGIC:I = 0x3fd76c17


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I
    .locals 7
    .param p0, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p1, "codec"    # Ljava/lang/String;
    .param p2, "minVersion"    # I
    .param p3, "maxVersion"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v6, 0x3fd76c17

    .line 62
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readInt()I

    move-result v1

    .line 63
    .local v1, "actualHeader":I
    if-eq v1, v6, :cond_0

    .line 64
    new-instance v3, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "codec header mismatch: actual header="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " vs expected header="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " (resource: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 67
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readString()Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "actualCodec":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 69
    new-instance v3, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "codec mismatch: actual codec="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " vs expected codec="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " (resource: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 72
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readInt()I

    move-result v2

    .line 73
    .local v2, "actualVersion":I
    if-ge v2, p2, :cond_2

    .line 74
    new-instance v3, Lorg/apache/lucene/index/IndexFormatTooOldException;

    invoke-direct {v3, p0, v2, p2, p3}, Lorg/apache/lucene/index/IndexFormatTooOldException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v3

    .line 76
    :cond_2
    if-le v2, p3, :cond_3

    .line 77
    new-instance v3, Lorg/apache/lucene/index/IndexFormatTooNewException;

    invoke-direct {v3, p0, v2, p2, p3}, Lorg/apache/lucene/index/IndexFormatTooNewException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v3

    .line 80
    :cond_3
    return v2
.end method

.method public static headerLength(Ljava/lang/String;)I
    .locals 1
    .param p0, "codec"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x9

    return v0
.end method

.method public static writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)Lorg/apache/lucene/store/DataOutput;
    .locals 4
    .param p0, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p1, "codec"    # Ljava/lang/String;
    .param p2, "version"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, p1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    .line 44
    .local v0, "bytes":Lorg/apache/lucene/util/BytesRef;
    iget v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    const/16 v2, 0x80

    if-lt v1, v2, :cond_1

    .line 45
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "codec must be simple ASCII, less than 128 characters in length [got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 47
    :cond_1
    const v1, 0x3fd76c17

    invoke-virtual {p0, v1}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 48
    invoke-virtual {p0, p1}, Lorg/apache/lucene/store/DataOutput;->writeString(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0, p2}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 51
    return-object p0
.end method
