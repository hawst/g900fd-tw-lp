.class Lorg/apache/lucene/util/fst/Util$TopNSearcher;
.super Ljava/lang/Object;
.source "Util.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/Util;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TopNSearcher"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field bottom:Lorg/apache/lucene/util/fst/Util$FSTPath;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/Util$FSTPath",
            "<TT;>;"
        }
    .end annotation
.end field

.field final comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final fromNode:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final fst:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;"
        }
    .end annotation
.end field

.field queue:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lorg/apache/lucene/util/fst/Util$FSTPath",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final topN:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 261
    const-class v0, Lorg/apache/lucene/util/fst/Util;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/fst/FST$Arc;ILjava/util/Comparator;)V
    .locals 1
    .param p3, "topN"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;I",
            "Ljava/util/Comparator",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$TopNSearcher;, "Lorg/apache/lucene/util/fst/Util$TopNSearcher<TT;>;"
    .local p1, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    .local p2, "fromNode":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p4, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<TT;>;"
    const/4 v0, 0x0

    .line 274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270
    iput-object v0, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->bottom:Lorg/apache/lucene/util/fst/Util$FSTPath;

    .line 272
    iput-object v0, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    .line 275
    iput-object p1, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    .line 276
    iput p3, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->topN:I

    .line 277
    iput-object p2, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fromNode:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 278
    iput-object p4, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->comparator:Ljava/util/Comparator;

    .line 279
    return-void
.end method

.method private addIfCompetitive(Lorg/apache/lucene/util/fst/Util$FSTPath;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Util$FSTPath",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$TopNSearcher;, "Lorg/apache/lucene/util/fst/Util$TopNSearcher<TT;>;"
    .local p1, "path":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    const/4 v9, 0x0

    .line 284
    sget-boolean v5, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    if-nez v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 286
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v5, v5, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    iget-object v7, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v7, v7, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 289
    .local v2, "cost":Ljava/lang/Object;, "TT;"
    iget-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->bottom:Lorg/apache/lucene/util/fst/Util$FSTPath;

    if-eqz v5, :cond_4

    .line 290
    iget-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->comparator:Ljava/util/Comparator;

    iget-object v6, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->bottom:Lorg/apache/lucene/util/fst/Util$FSTPath;

    iget-object v6, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    invoke-interface {v5, v2, v6}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 291
    .local v1, "comp":I
    if-lez v1, :cond_2

    .line 335
    .end local v1    # "comp":I
    :cond_1
    :goto_0
    return-void

    .line 294
    .restart local v1    # "comp":I
    :cond_2
    if-nez v1, :cond_4

    .line 296
    iget-object v5, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v6, v6, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 297
    iget-object v5, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v5, v5, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v7, v6, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v8, v7, 0x1

    iput v8, v6, Lorg/apache/lucene/util/IntsRef;->length:I

    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v6, v6, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    aput v6, v5, v7

    .line 298
    iget-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->bottom:Lorg/apache/lucene/util/fst/Util$FSTPath;

    iget-object v5, v5, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/IntsRef;->compareTo(Lorg/apache/lucene/util/IntsRef;)I

    move-result v0

    .line 299
    .local v0, "cmp":I
    iget-object v5, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v6, v5, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v6, v6, -0x1

    iput v6, v5, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 300
    sget-boolean v5, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    if-nez v0, :cond_3

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 301
    :cond_3
    if-ltz v0, :cond_1

    .line 311
    .end local v0    # "cmp":I
    .end local v1    # "comp":I
    :cond_4
    new-instance v3, Lorg/apache/lucene/util/fst/Util$FSTPath;

    iget-object v5, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v6, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->comparator:Ljava/util/Comparator;

    invoke-direct {v3, v2, v5, v6}, Lorg/apache/lucene/util/fst/Util$FSTPath;-><init>(Ljava/lang/Object;Lorg/apache/lucene/util/fst/FST$Arc;Ljava/util/Comparator;)V

    .line 313
    .local v3, "newPath":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    iget-object v5, v3, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v6, v6, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 314
    iget-object v5, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v5, v5, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v6, v3, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v6, v6, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v7, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v7, v7, Lorg/apache/lucene/util/IntsRef;->length:I

    invoke-static {v5, v9, v6, v9, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 315
    iget-object v5, v3, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v5, v5, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v6, v6, Lorg/apache/lucene/util/IntsRef;->length:I

    iget-object v7, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v7, v7, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    aput v7, v5, v6

    .line 316
    iget-object v5, v3, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v6, p1, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v6, v6, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v5, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 319
    iget-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v5, v3}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 320
    iget-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->bottom:Lorg/apache/lucene/util/fst/Util$FSTPath;

    if-eqz v5, :cond_7

    .line 323
    sget-boolean v5, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->$assertionsDisabled:Z

    if-nez v5, :cond_5

    iget-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v5}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_5

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 324
    :cond_5
    iget-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v5}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/fst/Util$FSTPath;

    .line 325
    .local v4, "removed":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    iget-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v5, v4}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 327
    sget-boolean v5, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->$assertionsDisabled:Z

    if-nez v5, :cond_6

    iget-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->bottom:Lorg/apache/lucene/util/fst/Util$FSTPath;

    if-eq v4, v5, :cond_6

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 328
    :cond_6
    iget-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v5}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/util/fst/Util$FSTPath;

    iput-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->bottom:Lorg/apache/lucene/util/fst/Util$FSTPath;

    goto/16 :goto_0

    .line 330
    .end local v4    # "removed":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    :cond_7
    iget-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v5}, Ljava/util/TreeSet;->size()I

    move-result v5

    iget v6, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->topN:I

    if-ne v5, v6, :cond_1

    .line 332
    iget-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v5}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/util/fst/Util$FSTPath;

    iput-object v5, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->bottom:Lorg/apache/lucene/util/fst/Util$FSTPath;

    goto/16 :goto_0
.end method


# virtual methods
.method public search()[Lorg/apache/lucene/util/fst/Util$MinResult;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[",
            "Lorg/apache/lucene/util/fst/Util$MinResult",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 339
    .local p0, "this":Lorg/apache/lucene/util/fst/Util$TopNSearcher;, "Lorg/apache/lucene/util/fst/Util$TopNSearcher<TT;>;"
    new-instance v8, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v8}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    .line 341
    .local v8, "scratchArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 343
    .local v7, "results":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/fst/Util$MinResult<TT;>;>;"
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v9, v9, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v9}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v0

    .line 353
    .local v0, "NO_OUTPUT":Ljava/lang/Object;, "TT;"
    :goto_0
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    iget v10, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->topN:I

    if-ge v9, v10, :cond_0

    .line 358
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    if-nez v9, :cond_c

    .line 360
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    if-eqz v9, :cond_1

    .line 516
    :cond_0
    :goto_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    new-array v2, v9, [Lorg/apache/lucene/util/fst/Util$MinResult;

    check-cast v2, [Lorg/apache/lucene/util/fst/Util$MinResult;

    .line 518
    .local v2, "arr":[Lorg/apache/lucene/util/fst/Util$MinResult;, "[Lorg/apache/lucene/util/fst/Util$MinResult<TT;>;"
    invoke-interface {v7, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lorg/apache/lucene/util/fst/Util$MinResult;

    return-object v9

    .line 366
    .end local v2    # "arr":[Lorg/apache/lucene/util/fst/Util$MinResult;, "[Lorg/apache/lucene/util/fst/Util$MinResult<TT;>;"
    :cond_1
    iget v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->topN:I

    const/4 v10, 0x1

    if-le v9, v10, :cond_2

    .line 367
    new-instance v9, Ljava/util/TreeSet;

    invoke-direct {v9}, Ljava/util/TreeSet;-><init>()V

    iput-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    .line 370
    :cond_2
    const/4 v5, 0x0

    .line 371
    .local v5, "minArcCost":Ljava/lang/Object;, "TT;"
    const/4 v4, 0x0

    .line 373
    .local v4, "minArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    new-instance v6, Lorg/apache/lucene/util/fst/Util$FSTPath;

    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fromNode:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v10, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->comparator:Ljava/util/Comparator;

    invoke-direct {v6, v0, v9, v10}, Lorg/apache/lucene/util/fst/Util$FSTPath;-><init>(Ljava/lang/Object;Lorg/apache/lucene/util/fst/FST$Arc;Ljava/util/Comparator;)V

    .line 374
    .local v6, "path":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v10, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fromNode:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v11, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v9, v10, v11}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 378
    .end local v5    # "minArcCost":Ljava/lang/Object;, "TT;"
    :goto_2
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v1, v9, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .line 380
    .local v1, "arcScore":Ljava/lang/Object;, "TT;"
    if-eqz v5, :cond_3

    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->comparator:Ljava/util/Comparator;

    invoke-interface {v9, v1, v5}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v9

    if-gez v9, :cond_4

    .line 381
    :cond_3
    move-object v5, v1

    .line 382
    .restart local v5    # "minArcCost":Ljava/lang/Object;, "TT;"
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v8, v9}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v4

    .line 385
    .end local v5    # "minArcCost":Ljava/lang/Object;, "TT;"
    :cond_4
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    if-eqz v9, :cond_5

    .line 386
    invoke-direct {p0, v6}, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->addIfCompetitive(Lorg/apache/lucene/util/fst/Util$FSTPath;)V

    .line 388
    :cond_5
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v9}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 394
    sget-boolean v9, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->$assertionsDisabled:Z

    if-nez v9, :cond_7

    if-nez v4, :cond_7

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 391
    :cond_6
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v10, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v9, v10}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_2

    .line 396
    :cond_7
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    if-eqz v9, :cond_b

    .line 401
    sget-boolean v9, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->$assertionsDisabled:Z

    if-nez v9, :cond_8

    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v9}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_8

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 402
    :cond_8
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v9}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "path":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    check-cast v6, Lorg/apache/lucene/util/fst/Util$FSTPath;

    .line 403
    .restart local v6    # "path":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v9, v6}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 406
    sget-boolean v9, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->$assertionsDisabled:Z

    if-nez v9, :cond_9

    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v9, v9, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    iget v10, v4, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-eq v9, v10, :cond_9

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 407
    :cond_9
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->bottom:Lorg/apache/lucene/util/fst/Util$FSTPath;

    if-eqz v9, :cond_a

    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v9}, Ljava/util/TreeSet;->size()I

    move-result v9

    iget v10, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->topN:I

    add-int/lit8 v10, v10, -0x1

    if-ne v9, v10, :cond_a

    .line 408
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v9}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/util/fst/Util$FSTPath;

    iput-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->bottom:Lorg/apache/lucene/util/fst/Util$FSTPath;

    .line 440
    .end local v1    # "arcScore":Ljava/lang/Object;, "TT;"
    .end local v4    # "minArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_a
    :goto_3
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v9, v9, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_e

    .line 443
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v10, v9, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v10, v10, -0x1

    iput v10, v9, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 444
    new-instance v9, Lorg/apache/lucene/util/fst/Util$MinResult;

    iget-object v10, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v11, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    iget-object v12, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->comparator:Ljava/util/Comparator;

    invoke-direct {v9, v10, v11, v12}, Lorg/apache/lucene/util/fst/Util$MinResult;-><init>(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;Ljava/util/Comparator;)V

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 412
    .restart local v1    # "arcScore":Ljava/lang/Object;, "TT;"
    .restart local v4    # "minArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_b
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v9, v4}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 413
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 414
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v9, v9, Lorg/apache/lucene/util/IntsRef;->ints:[I

    const/4 v10, 0x0

    iget v11, v4, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    aput v11, v9, v10

    .line 415
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    const/4 v10, 0x1

    iput v10, v9, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 416
    iget-object v9, v4, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    iput-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    goto :goto_3

    .line 430
    .end local v1    # "arcScore":Ljava/lang/Object;, "TT;"
    .end local v4    # "minArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .end local v6    # "path":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    :cond_c
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v9}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_d

    .line 432
    const/4 v6, 0x0

    .line 433
    .restart local v6    # "path":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    goto/16 :goto_1

    .line 435
    .end local v6    # "path":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    :cond_d
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v9}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/util/fst/Util$FSTPath;

    .line 436
    .restart local v6    # "path":Lorg/apache/lucene/util/fst/Util$FSTPath;, "Lorg/apache/lucene/util/fst/Util$FSTPath<TT;>;"
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    invoke-virtual {v9, v6}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    goto :goto_3

    .line 448
    :cond_e
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    iget v10, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->topN:I

    add-int/lit8 v10, v10, -0x1

    if-ne v9, v10, :cond_f

    .line 450
    const/4 v9, 0x0

    iput-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    .line 465
    :cond_f
    :goto_4
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v10, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v11, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v9, v10, v11}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 468
    const/4 v3, 0x0

    .line 473
    .local v3, "foundZero":Z
    :goto_5
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->comparator:Ljava/util/Comparator;

    iget-object v10, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v10, v10, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-interface {v9, v0, v10}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v9

    if-nez v9, :cond_14

    .line 474
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    if-nez v9, :cond_11

    .line 475
    const/4 v3, 0x1

    .line 492
    :cond_10
    sget-boolean v9, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->$assertionsDisabled:Z

    if-nez v9, :cond_15

    if-nez v3, :cond_15

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 477
    :cond_11
    if-nez v3, :cond_13

    .line 478
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v8, v9}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 479
    const/4 v3, 0x1

    .line 486
    :cond_12
    :goto_6
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v9}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v9

    if-nez v9, :cond_10

    .line 489
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v10, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v9, v10}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_5

    .line 481
    :cond_13
    invoke-direct {p0, v6}, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->addIfCompetitive(Lorg/apache/lucene/util/fst/Util$FSTPath;)V

    goto :goto_6

    .line 483
    :cond_14
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    if-eqz v9, :cond_12

    .line 484
    invoke-direct {p0, v6}, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->addIfCompetitive(Lorg/apache/lucene/util/fst/Util$FSTPath;)V

    goto :goto_6

    .line 494
    :cond_15
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->queue:Ljava/util/TreeSet;

    if-eqz v9, :cond_16

    .line 499
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v9, v8}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 502
    :cond_16
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v9, v9, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_17

    .line 505
    new-instance v9, Lorg/apache/lucene/util/fst/Util$MinResult;

    iget-object v10, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v11, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v11, v11, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v12, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    iget-object v13, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v13, v13, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v11, v12, v13}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    iget-object v12, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->comparator:Ljava/util/Comparator;

    invoke-direct {v9, v10, v11, v12}, Lorg/apache/lucene/util/fst/Util$MinResult;-><init>(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;Ljava/util/Comparator;)V

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 508
    :cond_17
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v10, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v10, v10, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v9, v10}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 509
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget-object v9, v9, Lorg/apache/lucene/util/IntsRef;->ints:[I

    iget-object v10, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v10, v10, Lorg/apache/lucene/util/IntsRef;->length:I

    iget-object v11, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v11, v11, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    aput v11, v9, v10

    .line 510
    iget-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->input:Lorg/apache/lucene/util/IntsRef;

    iget v10, v9, Lorg/apache/lucene/util/IntsRef;->length:I

    add-int/lit8 v10, v10, 0x1

    iput v10, v9, Lorg/apache/lucene/util/IntsRef;->length:I

    .line 511
    iget-object v9, p0, Lorg/apache/lucene/util/fst/Util$TopNSearcher;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v9, v9, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v10, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    iget-object v11, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v11, v11, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v9, v10, v11}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    iput-object v9, v6, Lorg/apache/lucene/util/fst/Util$FSTPath;->cost:Ljava/lang/Object;

    goto/16 :goto_4
.end method
