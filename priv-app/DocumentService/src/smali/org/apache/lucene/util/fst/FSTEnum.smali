.class abstract Lorg/apache/lucene/util/fst/FSTEnum;
.super Ljava/lang/Object;
.source "FSTEnum.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected final NO_OUTPUT:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected arcs:[Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected final fst:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected output:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field protected final scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected targetLength:I

.field protected upto:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/util/fst/FSTEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lorg/apache/lucene/util/fst/FST;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    .local p1, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<TT;>;"
    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-array v0, v1, [Lorg/apache/lucene/util/fst/FST$Arc;

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    .line 35
    new-array v0, v1, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    .line 38
    new-instance v0, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    .line 48
    iget-object v0, p1, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->NO_OUTPUT:Ljava/lang/Object;

    .line 49
    invoke-direct {p0, v2}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 50
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->NO_OUTPUT:Ljava/lang/Object;

    aput-object v1, v0, v2

    .line 51
    return-void
.end method

.method private getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 2
    .param p1, "idx"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 522
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 523
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    new-instance v1, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v1}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    aput-object v1, v0, p1

    .line 525
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    aget-object v0, v0, p1

    return-object v0
.end method

.method private incr()V
    .locals 5

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    const/4 v4, 0x0

    .line 462
    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 463
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->grow()V

    .line 464
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v2, v2

    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-gt v2, v3, :cond_0

    .line 465
    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v2, v2, 0x1

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v0, v2, [Lorg/apache/lucene/util/fst/FST$Arc;

    .line 467
    .local v0, "newArcs":[Lorg/apache/lucene/util/fst/FST$Arc;, "[Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v3, v3

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 468
    iput-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    .line 470
    .end local v0    # "newArcs":[Lorg/apache/lucene/util/fst/FST$Arc;, "[Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    array-length v2, v2

    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-gt v2, v3, :cond_1

    .line 471
    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v2, v2, 0x1

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v1, v2, [Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    .line 473
    .local v1, "newOutput":[Ljava/lang/Object;, "[TT;"
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget-object v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 474
    iput-object v1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    .line 476
    .end local v1    # "newOutput":[Ljava/lang/Object;, "[TT;"
    :cond_1
    return-void
.end method

.method private pushFirst()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 482
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    aget-object v0, v2, v3

    .line 483
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    sget-boolean v2, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    if-nez v0, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 492
    :cond_0
    iget v2, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/fst/FSTEnum;->setCurrentLabel(I)V

    .line 493
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->incr()V

    .line 495
    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v2}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v1

    .line 496
    .local v1, "nextArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v2, v0, v1}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 497
    move-object v0, v1

    .line 486
    .end local v1    # "nextArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget-object v4, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v4, v4, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v6, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v6, v6, -0x1

    aget-object v5, v5, v6

    iget-object v6, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    .line 487
    iget v2, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 499
    return-void
.end method

.method private pushLast()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 505
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    aget-object v0, v1, v2

    .line 506
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    sget-boolean v1, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 515
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->incr()V

    .line 517
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v2}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/apache/lucene/util/fst/FST;->readLastTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 509
    :cond_1
    iget v1, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FSTEnum;->setCurrentLabel(I)V

    .line 510
    iget-object v1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget-object v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v3, v3, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v4, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v5, v5, -0x1

    aget-object v4, v4, v5

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    .line 511
    iget v1, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 519
    return-void
.end method


# virtual methods
.method protected doNext()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    const/4 v2, 0x1

    .line 92
    iget v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-nez v0, :cond_0

    .line 94
    iput v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 95
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v1

    invoke-direct {p0, v2}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 109
    :goto_0
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushFirst()V

    .line 110
    :goto_1
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    iget v1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    iget v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 101
    iget v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-nez v0, :cond_0

    goto :goto_1

    .line 106
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v1, p0, Lorg/apache/lucene/util/fst/FSTEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_0
.end method

.method protected doSeekCeil()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->rewindPrefix()V

    .line 132
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 133
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v9

    .line 141
    .local v9, "targetLabel":I
    :goto_0
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v10, :cond_d

    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v11, -0x1

    if-eq v10, v11, :cond_d

    .line 146
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v4

    .line 147
    .local v4, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    iget v5, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 148
    .local v5, "low":I
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    add-int/lit8 v3, v10, -0x1

    .line 149
    .local v3, "high":I
    const/4 v6, 0x0

    .line 151
    .local v6, "mid":I
    const/4 v2, 0x0

    .line 152
    .local v2, "found":Z
    :goto_1
    if-gt v5, v3, :cond_2

    .line 153
    add-int v10, v5, v3

    ushr-int/lit8 v6, v10, 0x1

    .line 154
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:I

    iput v10, v4, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    .line 155
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    mul-int/2addr v10, v6

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v4, v10}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skip(I)V

    .line 156
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v10, v4}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    move-result v7

    .line 157
    .local v7, "midLabel":I
    sub-int v1, v7, v9

    .line 159
    .local v1, "cmp":I
    if-gez v1, :cond_0

    .line 160
    add-int/lit8 v5, v6, 0x1

    goto :goto_1

    .line 161
    :cond_0
    if-lez v1, :cond_1

    .line 162
    add-int/lit8 v3, v6, -0x1

    goto :goto_1

    .line 164
    :cond_1
    const/4 v2, 0x1

    .line 171
    .end local v1    # "cmp":I
    .end local v7    # "midLabel":I
    :cond_2
    if-eqz v2, :cond_7

    .line 173
    add-int/lit8 v10, v6, -0x1

    iput v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 174
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v10, v0, v4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 175
    sget-boolean v10, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v10, :cond_3

    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    if-eq v10, v6, :cond_3

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 176
    :cond_3
    sget-boolean v10, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v10, :cond_4

    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-eq v10, v9, :cond_4

    new-instance v10, Ljava/lang/AssertionError;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "arc.label="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " vs targetLabel="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " mid="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v10

    .line 177
    :cond_4
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget-object v12, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v12, v12, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v13, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v14, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v14, v14, -0x1

    aget-object v13, v13, v14

    iget-object v14, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v12, v13, v14}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    .line 178
    const/4 v10, -0x1

    if-ne v9, v10, :cond_6

    .line 242
    .end local v2    # "found":Z
    .end local v3    # "high":I
    .end local v4    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    .end local v5    # "low":I
    .end local v6    # "mid":I
    :cond_5
    :goto_2
    return-void

    .line 181
    .restart local v2    # "found":Z
    .restart local v3    # "high":I
    .restart local v4    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    .restart local v5    # "low":I
    .restart local v6    # "mid":I
    :cond_6
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->setCurrentLabel(I)V

    .line 182
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->incr()V

    .line 183
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v11}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v11

    invoke-virtual {v10, v0, v11}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 184
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v9

    .line 185
    goto/16 :goto_0

    .line 186
    :cond_7
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    if-ne v5, v10, :cond_a

    .line 188
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    add-int/lit8 v10, v10, -0x2

    iput v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 189
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v10, v0, v4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 190
    sget-boolean v10, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v10, :cond_8

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v10

    if-nez v10, :cond_8

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 193
    :cond_8
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v10, v10, -0x1

    iput v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 195
    :goto_3
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-eqz v10, :cond_5

    .line 198
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v8

    .line 200
    .local v8, "prevArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-virtual {v8}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v10

    if-nez v10, :cond_9

    .line 201
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v10, v8}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 202
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushFirst()V

    goto :goto_2

    .line 205
    :cond_9
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v10, v10, -0x1

    iput v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    goto :goto_3

    .line 208
    .end local v8    # "prevArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_a
    if-le v5, v3, :cond_b

    .end local v5    # "low":I
    :goto_4
    add-int/lit8 v10, v5, -0x1

    iput v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 209
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v10, v0, v4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 210
    sget-boolean v10, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v10, :cond_c

    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-gt v10, v9, :cond_c

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .restart local v5    # "low":I
    :cond_b
    move v5, v3

    .line 208
    goto :goto_4

    .line 211
    .end local v5    # "low":I
    :cond_c
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushFirst()V

    goto :goto_2

    .line 216
    .end local v2    # "found":Z
    .end local v3    # "high":I
    .end local v4    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    .end local v6    # "mid":I
    :cond_d
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-ne v10, v9, :cond_e

    .line 218
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget-object v12, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v12, v12, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v13, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v14, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v14, v14, -0x1

    aget-object v13, v13, v14

    iget-object v14, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v12, v13, v14}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    .line 219
    const/4 v10, -0x1

    if-eq v9, v10, :cond_5

    .line 222
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->setCurrentLabel(I)V

    .line 223
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->incr()V

    .line 224
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v11}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v11

    invoke-virtual {v10, v0, v11}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 225
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v9

    goto/16 :goto_0

    .line 226
    :cond_e
    iget v10, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-le v10, v9, :cond_f

    .line 227
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushFirst()V

    goto/16 :goto_2

    .line 229
    :cond_f
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v10

    if-eqz v10, :cond_11

    .line 232
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v10, v10, -0x1

    iput v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 234
    :goto_5
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-eqz v10, :cond_5

    .line 237
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v8

    .line 239
    .restart local v8    # "prevArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-virtual {v8}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v10

    if-nez v10, :cond_10

    .line 240
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v10, v8}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 241
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushFirst()V

    goto/16 :goto_2

    .line 244
    :cond_10
    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v10, v10, -0x1

    iput v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    goto :goto_5

    .line 249
    .end local v8    # "prevArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_11
    iget-object v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v10, v0}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto/16 :goto_0
.end method

.method protected doSeekExact()Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    const/4 v4, 0x0

    .line 429
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->rewindPrefix()V

    .line 432
    iget v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v5, v5, -0x1

    invoke-direct {p0, v5}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 433
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v3

    .line 435
    .local v3, "targetLabel":I
    iget-object v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v5, v4}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    .line 439
    .local v1, "fstReader":Lorg/apache/lucene/util/fst/FST$BytesReader;
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v6, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v6}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v6

    invoke-virtual {v5, v3, v0, v6, v1}, Lorg/apache/lucene/util/fst/FST;->findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v2

    .line 440
    .local v2, "nextArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    if-nez v2, :cond_0

    .line 444
    iget-object v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v6, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v6}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 452
    :goto_1
    return v4

    .line 449
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v6, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget-object v7, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v7, v7, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v8, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v9, v9, -0x1

    aget-object v8, v8, v9

    iget-object v9, v2, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v5, v6

    .line 450
    const/4 v5, -0x1

    if-ne v3, v5, :cond_1

    .line 452
    const/4 v4, 0x1

    goto :goto_1

    .line 454
    :cond_1
    invoke-virtual {p0, v3}, Lorg/apache/lucene/util/fst/FSTEnum;->setCurrentLabel(I)V

    .line 455
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->incr()V

    .line 456
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v3

    .line 457
    move-object v0, v2

    .line 458
    goto :goto_0
.end method

.method protected doSeekFloor()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    const/4 v14, -0x1

    .line 268
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->rewindPrefix()V

    .line 272
    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v9}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 273
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v8

    .line 281
    .local v8, "targetLabel":I
    :goto_0
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v9, :cond_e

    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-eq v9, v14, :cond_e

    .line 285
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lorg/apache/lucene/util/fst/FST;->getBytesReader(I)Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v4

    .line 286
    .local v4, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    iget v5, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 287
    .local v5, "low":I
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    add-int/lit8 v3, v9, -0x1

    .line 288
    .local v3, "high":I
    const/4 v6, 0x0

    .line 290
    .local v6, "mid":I
    const/4 v2, 0x0

    .line 291
    .local v2, "found":Z
    :goto_1
    if-gt v5, v3, :cond_2

    .line 292
    add-int v9, v5, v3

    ushr-int/lit8 v6, v9, 0x1

    .line 293
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:I

    iput v9, v4, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    .line 294
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    mul-int/2addr v9, v6

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v4, v9}, Lorg/apache/lucene/util/fst/FST$BytesReader;->skip(I)V

    .line 295
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v4}, Lorg/apache/lucene/util/fst/FST;->readLabel(Lorg/apache/lucene/store/DataInput;)I

    move-result v7

    .line 296
    .local v7, "midLabel":I
    sub-int v1, v7, v8

    .line 298
    .local v1, "cmp":I
    if-gez v1, :cond_0

    .line 299
    add-int/lit8 v5, v6, 0x1

    goto :goto_1

    .line 300
    :cond_0
    if-lez v1, :cond_1

    .line 301
    add-int/lit8 v3, v6, -0x1

    goto :goto_1

    .line 303
    :cond_1
    const/4 v2, 0x1

    .line 310
    .end local v1    # "cmp":I
    .end local v7    # "midLabel":I
    :cond_2
    if-eqz v2, :cond_7

    .line 313
    add-int/lit8 v9, v6, -0x1

    iput v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 314
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v0, v4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 315
    sget-boolean v9, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v9, :cond_3

    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    if-eq v9, v6, :cond_3

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 316
    :cond_3
    sget-boolean v9, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v9, :cond_4

    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-eq v9, v8, :cond_4

    new-instance v9, Ljava/lang/AssertionError;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "arc.label="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " vs targetLabel="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " mid="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v9

    .line 317
    :cond_4
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget-object v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v11, v11, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v12, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v13, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v13, v13, -0x1

    aget-object v12, v12, v13

    iget-object v13, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v11, v12, v13}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v9, v10

    .line 318
    if-ne v8, v14, :cond_6

    .line 411
    .end local v2    # "found":Z
    .end local v3    # "high":I
    .end local v4    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    .end local v5    # "low":I
    .end local v6    # "mid":I
    :cond_5
    :goto_2
    return-void

    .line 321
    .restart local v2    # "found":Z
    .restart local v3    # "high":I
    .restart local v4    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    .restart local v5    # "low":I
    .restart local v6    # "mid":I
    :cond_6
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {p0, v9}, Lorg/apache/lucene/util/fst/FSTEnum;->setCurrentLabel(I)V

    .line 322
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->incr()V

    .line 323
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v10

    invoke-virtual {v9, v0, v10}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 324
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v8

    .line 325
    goto/16 :goto_0

    .line 326
    :cond_7
    if-ne v3, v14, :cond_a

    .line 336
    :goto_3
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v10, v10, -0x1

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v10

    invoke-virtual {v9, v10, v0}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 337
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-ge v9, v8, :cond_9

    .line 340
    :goto_4
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v9

    if-nez v9, :cond_8

    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v0}, Lorg/apache/lucene/util/fst/FST;->readNextArcLabel(Lorg/apache/lucene/util/fst/FST$Arc;)I

    move-result v9

    if-ge v9, v8, :cond_8

    .line 341
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v0}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_4

    .line 343
    :cond_8
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushLast()V

    goto :goto_2

    .line 346
    :cond_9
    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 347
    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-eqz v9, :cond_5

    .line 350
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v8

    .line 351
    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v9}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    goto :goto_3

    .line 355
    :cond_a
    if-le v5, v3, :cond_b

    .end local v3    # "high":I
    :goto_5
    add-int/lit8 v9, v3, -0x1

    iput v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 357
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v0, v4}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 358
    sget-boolean v9, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v9, :cond_c

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v9

    if-nez v9, :cond_c

    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v0}, Lorg/apache/lucene/util/fst/FST;->readNextArcLabel(Lorg/apache/lucene/util/fst/FST$Arc;)I

    move-result v9

    if-gt v9, v8, :cond_c

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .restart local v3    # "high":I
    :cond_b
    move v3, v5

    .line 355
    goto :goto_5

    .line 359
    .end local v3    # "high":I
    :cond_c
    sget-boolean v9, Lorg/apache/lucene/util/fst/FSTEnum;->$assertionsDisabled:Z

    if-nez v9, :cond_d

    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-lt v9, v8, :cond_d

    new-instance v9, Ljava/lang/AssertionError;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "arc.label="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " vs targetLabel="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v9

    .line 360
    :cond_d
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushLast()V

    goto/16 :goto_2

    .line 365
    .end local v2    # "found":Z
    .end local v4    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;
    .end local v5    # "low":I
    .end local v6    # "mid":I
    :cond_e
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-ne v9, v8, :cond_f

    .line 367
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget-object v11, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v11, v11, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v12, p0, Lorg/apache/lucene/util/fst/FSTEnum;->output:[Ljava/lang/Object;

    iget v13, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v13, v13, -0x1

    aget-object v12, v12, v13

    iget-object v13, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v11, v12, v13}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v9, v10

    .line 368
    if-eq v8, v14, :cond_5

    .line 371
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {p0, v9}, Lorg/apache/lucene/util/fst/FSTEnum;->setCurrentLabel(I)V

    .line 372
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->incr()V

    .line 373
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v10

    invoke-virtual {v9, v0, v10}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 374
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v8

    goto/16 :goto_0

    .line 375
    :cond_f
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-le v9, v8, :cond_12

    .line 383
    :goto_6
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v10, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v10, v10, -0x1

    invoke-direct {p0, v10}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v10

    invoke-virtual {v9, v10, v0}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 384
    iget v9, v0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    if-ge v9, v8, :cond_11

    .line 387
    :goto_7
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v9

    if-nez v9, :cond_10

    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v0}, Lorg/apache/lucene/util/fst/FST;->readNextArcLabel(Lorg/apache/lucene/util/fst/FST$Arc;)I

    move-result v9

    if-ge v9, v8, :cond_10

    .line 388
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v0}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_7

    .line 390
    :cond_10
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushLast()V

    goto/16 :goto_2

    .line 393
    :cond_11
    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 394
    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-eqz v9, :cond_5

    .line 397
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v8

    .line 398
    iget v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v9}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    goto :goto_6

    .line 400
    :cond_12
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v9

    if-nez v9, :cond_14

    .line 402
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v0}, Lorg/apache/lucene/util/fst/FST;->readNextArcLabel(Lorg/apache/lucene/util/fst/FST$Arc;)I

    move-result v9

    if-le v9, v8, :cond_13

    .line 403
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushLast()V

    goto/16 :goto_2

    .line 407
    :cond_13
    iget-object v9, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v9, v0}, Lorg/apache/lucene/util/fst/FST;->readNextArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto/16 :goto_0

    .line 410
    :cond_14
    invoke-direct {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->pushLast()V

    goto/16 :goto_2
.end method

.method protected abstract getCurrentLabel()I
.end method

.method protected abstract getTargetLabel()I
.end method

.method protected abstract grow()V
.end method

.method protected final rewindPrefix()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/FSTEnum;, "Lorg/apache/lucene/util/fst/FSTEnum<TT;>;"
    const/4 v5, 0x1

    .line 62
    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-nez v3, :cond_1

    .line 64
    iput v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 65
    iget-object v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v4

    invoke-direct {p0, v5}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    iget v2, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 71
    .local v2, "currentLimit":I
    iput v5, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    .line 72
    :goto_1
    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    if-ge v3, v2, :cond_0

    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    iget v4, p0, Lorg/apache/lucene/util/fst/FSTEnum;->targetLength:I

    add-int/lit8 v4, v4, 0x1

    if-gt v3, v4, :cond_0

    .line 73
    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getCurrentLabel()I

    move-result v3

    invoke-virtual {p0}, Lorg/apache/lucene/util/fst/FSTEnum;->getTargetLabel()I

    move-result v4

    sub-int v1, v3, v4

    .line 74
    .local v1, "cmp":I
    if-ltz v1, :cond_0

    .line 78
    if-lez v1, :cond_2

    .line 80
    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    invoke-direct {p0, v3}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 81
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    iget-object v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget v4, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v4, v4, -0x1

    invoke-direct {p0, v4}, Lorg/apache/lucene/util/fst/FSTEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lorg/apache/lucene/util/fst/FST;->readFirstTargetArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    goto :goto_0

    .line 85
    .end local v0    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    :cond_2
    iget v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/util/fst/FSTEnum;->upto:I

    goto :goto_1
.end method

.method protected abstract setCurrentLabel(I)V
.end method
