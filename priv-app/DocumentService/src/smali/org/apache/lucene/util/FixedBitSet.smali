.class public final Lorg/apache/lucene/util/FixedBitSet;
.super Lorg/apache/lucene/search/DocIdSet;
.source "FixedBitSet.java"

# interfaces
.implements Lorg/apache/lucene/util/Bits;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final bits:[J

.field private numBits:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "numBits"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 53
    iput p1, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    .line 54
    invoke-static {p1}, Lorg/apache/lucene/util/FixedBitSet;->bits2words(I)I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    .line 55
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/FixedBitSet;)V
    .locals 4
    .param p1, "other"    # Lorg/apache/lucene/util/FixedBitSet;

    .prologue
    const/4 v3, 0x0

    .line 58
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 59
    iget-object v0, p1, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    array-length v0, v0

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    .line 60
    iget-object v0, p1, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget-object v1, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget-object v2, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 61
    iget v0, p1, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    iput v0, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    .line 62
    return-void
.end method

.method private and([JI)V
    .locals 6
    .param p1, "otherArr"    # [J
    .param p2, "otherLen"    # I

    .prologue
    .line 242
    iget-object v1, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    .line 243
    .local v1, "thisArr":[J
    array-length v2, v1

    invoke-static {v2, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 244
    .local v0, "pos":I
    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    .line 245
    aget-wide v2, v1, v0

    aget-wide v4, p1, v0

    and-long/2addr v2, v4

    aput-wide v2, v1, v0

    goto :goto_0

    .line 247
    :cond_0
    array-length v2, v1

    if-le v2, p2, :cond_1

    .line 248
    array-length v2, v1

    const-wide/16 v4, 0x0

    invoke-static {v1, p2, v2, v4, v5}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 250
    :cond_1
    return-void
.end method

.method private andNot([JI)V
    .locals 8
    .param p1, "otherArr"    # [J
    .param p2, "otherLen"    # I

    .prologue
    .line 275
    iget-object v1, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    .line 276
    .local v1, "thisArr":[J
    array-length v2, v1

    invoke-static {v2, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 277
    .local v0, "pos":I
    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    .line 278
    aget-wide v2, v1, v0

    aget-wide v4, p1, v0

    const-wide/16 v6, -0x1

    xor-long/2addr v4, v6

    and-long/2addr v2, v4

    aput-wide v2, v1, v0

    goto :goto_0

    .line 280
    :cond_0
    return-void
.end method

.method public static bits2words(I)I
    .locals 2
    .param p0, "numBits"    # I

    .prologue
    .line 45
    ushr-int/lit8 v0, p0, 0x6

    .line 46
    .local v0, "numLong":I
    and-int/lit8 v1, p0, 0x3f

    if-eqz v1, :cond_0

    .line 47
    add-int/lit8 v0, v0, 0x1

    .line 49
    :cond_0
    return v0
.end method

.method private or([JI)V
    .locals 6
    .param p1, "otherArr"    # [J
    .param p2, "otherLen"    # I

    .prologue
    .line 206
    iget-object v1, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    .line 207
    .local v1, "thisArr":[J
    array-length v2, v1

    invoke-static {v2, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 208
    .local v0, "pos":I
    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    .line 209
    aget-wide v2, v1, v0

    aget-wide v4, p1, v0

    or-long/2addr v2, v4

    aput-wide v2, v1, v0

    goto :goto_0

    .line 211
    :cond_0
    return-void
.end method


# virtual methods
.method public and(Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 5
    .param p1, "iter"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 216
    instance-of v4, p1, Lorg/apache/lucene/util/OpenBitSetIterator;

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v4

    if-ne v4, v3, :cond_1

    move-object v2, p1

    .line 217
    check-cast v2, Lorg/apache/lucene/util/OpenBitSetIterator;

    .line 218
    .local v2, "obs":Lorg/apache/lucene/util/OpenBitSetIterator;
    iget-object v3, v2, Lorg/apache/lucene/util/OpenBitSetIterator;->arr:[J

    iget v4, v2, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    invoke-direct {p0, v3, v4}, Lorg/apache/lucene/util/FixedBitSet;->and([JI)V

    .line 221
    iget v3, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/OpenBitSetIterator;->advance(I)I

    .line 234
    .end local v2    # "obs":Lorg/apache/lucene/util/OpenBitSetIterator;
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-eqz v4, :cond_0

    .line 224
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v0

    .line 225
    .local v0, "bitSetDoc":I
    :goto_1
    if-eq v0, v3, :cond_3

    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v1

    .local v1, "disiDoc":I
    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-ge v1, v4, :cond_3

    .line 226
    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->clear(II)V

    .line 227
    add-int/lit8 v1, v1, 0x1

    .line 228
    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-ge v1, v4, :cond_2

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/FixedBitSet;->nextSetBit(I)I

    move-result v0

    :goto_2
    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_2

    .line 230
    .end local v1    # "disiDoc":I
    :cond_3
    if-eq v0, v3, :cond_0

    .line 231
    iget v3, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    invoke-virtual {p0, v0, v3}, Lorg/apache/lucene/util/FixedBitSet;->clear(II)V

    goto :goto_0
.end method

.method public and(Lorg/apache/lucene/util/FixedBitSet;)V
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/util/FixedBitSet;

    .prologue
    .line 238
    iget-object v0, p1, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget-object v1, p1, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    array-length v1, v1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->and([JI)V

    .line 239
    return-void
.end method

.method public andNot(Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 4
    .param p1, "iter"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 255
    instance-of v2, p1, Lorg/apache/lucene/util/OpenBitSetIterator;

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    move-object v1, p1

    .line 256
    check-cast v1, Lorg/apache/lucene/util/OpenBitSetIterator;

    .line 257
    .local v1, "obs":Lorg/apache/lucene/util/OpenBitSetIterator;
    iget-object v2, v1, Lorg/apache/lucene/util/OpenBitSetIterator;->arr:[J

    iget v3, v1, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/util/FixedBitSet;->andNot([JI)V

    .line 260
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/OpenBitSetIterator;->advance(I)I

    .line 267
    .end local v1    # "obs":Lorg/apache/lucene/util/OpenBitSetIterator;
    :cond_0
    return-void

    .line 263
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    .local v0, "doc":I
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-ge v0, v2, :cond_0

    .line 264
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/FixedBitSet;->clear(I)V

    goto :goto_0
.end method

.method public andNot(Lorg/apache/lucene/util/FixedBitSet;)V
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/util/FixedBitSet;

    .prologue
    .line 271
    iget-object v0, p1, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget-object v1, p1, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    array-length v1, v1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->andNot([JI)V

    .line 272
    return-void
.end method

.method public cardinality()I
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/BitUtil;->pop_array([JII)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public clear(I)V
    .locals 10
    .param p1, "index"    # I

    .prologue
    .line 120
    sget-boolean v4, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 121
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 122
    .local v1, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 123
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 124
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v6, v4, v1

    const-wide/16 v8, -0x1

    xor-long/2addr v8, v2

    and-long/2addr v6, v8

    aput-wide v6, v4, v1

    .line 125
    return-void
.end method

.method public clear(II)V
    .locals 12
    .param p1, "startIndex"    # I
    .param p2, "endIndex"    # I

    .prologue
    const-wide/16 v8, -0x1

    .line 359
    sget-boolean v6, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    if-ltz p1, :cond_0

    iget v6, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v6, :cond_1

    :cond_0
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 360
    :cond_1
    sget-boolean v6, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v6, :cond_3

    if-ltz p2, :cond_2

    iget v6, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-le p2, v6, :cond_3

    :cond_2
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 361
    :cond_3
    if-gt p2, p1, :cond_4

    .line 383
    :goto_0
    return-void

    .line 365
    :cond_4
    shr-int/lit8 v1, p1, 0x6

    .line 366
    .local v1, "startWord":I
    add-int/lit8 v6, p2, -0x1

    shr-int/lit8 v0, v6, 0x6

    .line 368
    .local v0, "endWord":I
    shl-long v4, v8, p1

    .line 369
    .local v4, "startmask":J
    neg-int v6, p2

    ushr-long v2, v8, v6

    .line 372
    .local v2, "endmask":J
    xor-long/2addr v4, v8

    .line 373
    xor-long/2addr v2, v8

    .line 375
    if-ne v1, v0, :cond_5

    .line 376
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v6, v1

    or-long v10, v4, v2

    and-long/2addr v8, v10

    aput-wide v8, v6, v1

    goto :goto_0

    .line 380
    :cond_5
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v6, v1

    and-long/2addr v8, v4

    aput-wide v8, v6, v1

    .line 381
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    add-int/lit8 v7, v1, 0x1

    const-wide/16 v8, 0x0

    invoke-static {v6, v7, v0, v8, v9}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 382
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v6, v0

    and-long/2addr v8, v2

    aput-wide v8, v6, v0

    goto :goto_0
.end method

.method public clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 387
    new-instance v0, Lorg/apache/lucene/util/FixedBitSet;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/FixedBitSet;-><init>(Lorg/apache/lucene/util/FixedBitSet;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 393
    if-ne p0, p1, :cond_1

    .line 394
    const/4 v1, 0x1

    .line 403
    :cond_0
    :goto_0
    return v1

    .line 396
    :cond_1
    instance-of v2, p1, Lorg/apache/lucene/util/FixedBitSet;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 399
    check-cast v0, Lorg/apache/lucene/util/FixedBitSet;

    .line 400
    .local v0, "other":Lorg/apache/lucene/util/FixedBitSet;
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    invoke-virtual {v0}, Lorg/apache/lucene/util/FixedBitSet;->length()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 403
    iget-object v1, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget-object v2, v0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v1

    goto :goto_0
.end method

.method public flip(II)V
    .locals 12
    .param p1, "startIndex"    # I
    .param p2, "endIndex"    # I

    .prologue
    const-wide/16 v10, -0x1

    .line 292
    sget-boolean v5, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    if-ltz p1, :cond_0

    iget v5, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 293
    :cond_1
    sget-boolean v5, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    if-ltz p2, :cond_2

    iget v5, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-le p2, v5, :cond_3

    :cond_2
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 294
    :cond_3
    if-gt p2, p1, :cond_4

    .line 323
    :goto_0
    return-void

    .line 298
    :cond_4
    shr-int/lit8 v4, p1, 0x6

    .line 299
    .local v4, "startWord":I
    add-int/lit8 v5, p2, -0x1

    shr-int/lit8 v0, v5, 0x6

    .line 308
    .local v0, "endWord":I
    shl-long v6, v10, p1

    .line 309
    .local v6, "startmask":J
    neg-int v5, p2

    ushr-long v2, v10, v5

    .line 311
    .local v2, "endmask":J
    if-ne v4, v0, :cond_5

    .line 312
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v5, v4

    and-long v10, v6, v2

    xor-long/2addr v8, v10

    aput-wide v8, v5, v4

    goto :goto_0

    .line 316
    :cond_5
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v5, v4

    xor-long/2addr v8, v6

    aput-wide v8, v5, v4

    .line 318
    add-int/lit8 v1, v4, 0x1

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_6

    .line 319
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget-object v8, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v8, v1

    xor-long/2addr v8, v10

    aput-wide v8, v5, v1

    .line 318
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 322
    :cond_6
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v5, v0

    xor-long/2addr v8, v2

    aput-wide v8, v5, v0

    goto :goto_0
.end method

.method public get(I)Z
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 92
    sget-boolean v4, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "index="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 93
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 96
    .local v1, "i":I
    and-int/lit8 v0, p1, 0x3f

    .line 97
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 98
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v4, v4, v1

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public getAndClear(I)Z
    .locals 10
    .param p1, "index"    # I

    .prologue
    .line 128
    sget-boolean v5, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    if-ltz p1, :cond_0

    iget v5, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 129
    :cond_1
    shr-int/lit8 v4, p1, 0x6

    .line 130
    .local v4, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 131
    .local v0, "bit":I
    const-wide/16 v6, 0x1

    shl-long v2, v6, v0

    .line 132
    .local v2, "bitmask":J
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v6, v5, v4

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    const/4 v1, 0x1

    .line 133
    .local v1, "val":Z
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v6, v5, v4

    const-wide/16 v8, -0x1

    xor-long/2addr v8, v2

    and-long/2addr v6, v8

    aput-wide v6, v5, v4

    .line 134
    return v1

    .line 132
    .end local v1    # "val":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAndSet(I)Z
    .locals 10
    .param p1, "index"    # I

    .prologue
    .line 110
    sget-boolean v5, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    if-ltz p1, :cond_0

    iget v5, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 111
    :cond_1
    shr-int/lit8 v4, p1, 0x6

    .line 112
    .local v4, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 113
    .local v0, "bit":I
    const-wide/16 v6, 0x1

    shl-long v2, v6, v0

    .line 114
    .local v2, "bitmask":J
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v6, v5, v4

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    const/4 v1, 0x1

    .line 115
    .local v1, "val":Z
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v6, v5, v4

    or-long/2addr v6, v2

    aput-wide v6, v5, v4

    .line 116
    return v1

    .line 114
    .end local v1    # "val":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBits()[J
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    return-object v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 408
    const-wide/16 v0, 0x0

    .line 409
    .local v0, "h":J
    iget-object v3, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    array-length v2, v3

    .local v2, "i":I
    :goto_0
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_0

    .line 410
    iget-object v3, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v4, v3, v2

    xor-long/2addr v0, v4

    .line 411
    const/4 v3, 0x1

    shl-long v4, v0, v3

    const/16 v3, 0x3f

    ushr-long v6, v0, v3

    or-long v0, v4, v6

    goto :goto_0

    .line 415
    :cond_0
    const/16 v3, 0x20

    shr-long v4, v0, v3

    xor-long/2addr v4, v0

    long-to-int v3, v4

    const v4, -0x6789edcc

    add-int/2addr v3, v4

    return v3
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    return v0
.end method

.method public iterator()Lorg/apache/lucene/search/DocIdSetIterator;
    .locals 3

    .prologue
    .line 66
    new-instance v0, Lorg/apache/lucene/util/OpenBitSetIterator;

    iget-object v1, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget-object v2, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    array-length v2, v2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/OpenBitSetIterator;-><init>([JI)V

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    return v0
.end method

.method public nextSetBit(I)I
    .locals 8
    .param p1, "index"    # I

    .prologue
    const-wide/16 v6, 0x0

    .line 141
    sget-boolean v4, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 142
    :cond_1
    shr-int/lit8 v0, p1, 0x6

    .line 143
    .local v0, "i":I
    and-int/lit8 v1, p1, 0x3f

    .line 144
    .local v1, "subIndex":I
    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v4, v4, v0

    shr-long v2, v4, v1

    .line 146
    .local v2, "word":J
    cmp-long v4, v2, v6

    if-eqz v4, :cond_2

    .line 147
    shl-int/lit8 v4, v0, 0x6

    add-int/2addr v4, v1

    invoke-static {v2, v3}, Lorg/apache/lucene/util/BitUtil;->ntz(J)I

    move-result v5

    add-int/2addr v4, v5

    .line 157
    :goto_0
    return v4

    .line 150
    :cond_2
    add-int/lit8 v0, v0, 0x1

    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    array-length v4, v4

    if-ge v0, v4, :cond_3

    .line 151
    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v2, v4, v0

    .line 152
    cmp-long v4, v2, v6

    if-eqz v4, :cond_2

    .line 153
    shl-int/lit8 v4, v0, 0x6

    invoke-static {v2, v3}, Lorg/apache/lucene/util/BitUtil;->ntz(J)I

    move-result v5

    add-int/2addr v4, v5

    goto :goto_0

    .line 157
    :cond_3
    const/4 v4, -0x1

    goto :goto_0
.end method

.method public or(Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 4
    .param p1, "iter"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    instance-of v2, p1, Lorg/apache/lucene/util/OpenBitSetIterator;

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    move-object v1, p1

    .line 187
    check-cast v1, Lorg/apache/lucene/util/OpenBitSetIterator;

    .line 188
    .local v1, "obs":Lorg/apache/lucene/util/OpenBitSetIterator;
    iget-object v2, v1, Lorg/apache/lucene/util/OpenBitSetIterator;->arr:[J

    iget v3, v1, Lorg/apache/lucene/util/OpenBitSetIterator;->words:I

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/util/FixedBitSet;->or([JI)V

    .line 191
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/OpenBitSetIterator;->advance(I)I

    .line 198
    .end local v1    # "obs":Lorg/apache/lucene/util/OpenBitSetIterator;
    :cond_0
    return-void

    .line 194
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    .local v0, "doc":I
    iget v2, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-ge v0, v2, :cond_0

    .line 195
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    goto :goto_0
.end method

.method public or(Lorg/apache/lucene/util/FixedBitSet;)V
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/util/FixedBitSet;

    .prologue
    .line 202
    iget-object v0, p1, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    iget-object v1, p1, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    array-length v1, v1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/FixedBitSet;->or([JI)V

    .line 203
    return-void
.end method

.method public prevSetBit(I)I
    .locals 10
    .param p1, "index"    # I

    .prologue
    const-wide/16 v8, 0x0

    .line 164
    sget-boolean v4, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "index="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " numBits="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 165
    :cond_1
    shr-int/lit8 v0, p1, 0x6

    .line 166
    .local v0, "i":I
    and-int/lit8 v1, p1, 0x3f

    .line 167
    .local v1, "subIndex":I
    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v4, v4, v0

    rsub-int/lit8 v6, v1, 0x3f

    shl-long v2, v4, v6

    .line 169
    .local v2, "word":J
    cmp-long v4, v2, v8

    if-eqz v4, :cond_2

    .line 170
    shl-int/lit8 v4, v0, 0x6

    add-int/2addr v4, v1

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v5

    sub-int/2addr v4, v5

    .line 180
    :goto_0
    return v4

    .line 173
    :cond_2
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_3

    .line 174
    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v2, v4, v0

    .line 175
    cmp-long v4, v2, v8

    if-eqz v4, :cond_2

    .line 176
    shl-int/lit8 v4, v0, 0x6

    add-int/lit8 v4, v4, 0x3f

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v5

    sub-int/2addr v4, v5

    goto :goto_0

    .line 180
    :cond_3
    const/4 v4, -0x1

    goto :goto_0
.end method

.method public set(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 102
    sget-boolean v4, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    iget v4, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 103
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 104
    .local v1, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 105
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 106
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v6, v4, v1

    or-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 107
    return-void
.end method

.method public set(II)V
    .locals 12
    .param p1, "startIndex"    # I
    .param p2, "endIndex"    # I

    .prologue
    const-wide/16 v10, -0x1

    .line 331
    sget-boolean v6, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    if-ltz p1, :cond_0

    iget v6, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-lt p1, v6, :cond_1

    :cond_0
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 332
    :cond_1
    sget-boolean v6, Lorg/apache/lucene/util/FixedBitSet;->$assertionsDisabled:Z

    if-nez v6, :cond_3

    if-ltz p2, :cond_2

    iget v6, p0, Lorg/apache/lucene/util/FixedBitSet;->numBits:I

    if-le p2, v6, :cond_3

    :cond_2
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 333
    :cond_3
    if-gt p2, p1, :cond_4

    .line 351
    :goto_0
    return-void

    .line 337
    :cond_4
    shr-int/lit8 v1, p1, 0x6

    .line 338
    .local v1, "startWord":I
    add-int/lit8 v6, p2, -0x1

    shr-int/lit8 v0, v6, 0x6

    .line 340
    .local v0, "endWord":I
    shl-long v4, v10, p1

    .line 341
    .local v4, "startmask":J
    neg-int v6, p2

    ushr-long v2, v10, v6

    .line 343
    .local v2, "endmask":J
    if-ne v1, v0, :cond_5

    .line 344
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v6, v1

    and-long v10, v4, v2

    or-long/2addr v8, v10

    aput-wide v8, v6, v1

    goto :goto_0

    .line 348
    :cond_5
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v6, v1

    or-long/2addr v8, v4

    aput-wide v8, v6, v1

    .line 349
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    add-int/lit8 v7, v1, 0x1

    invoke-static {v6, v7, v0, v10, v11}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 350
    iget-object v6, p0, Lorg/apache/lucene/util/FixedBitSet;->bits:[J

    aget-wide v8, v6, v0

    or-long/2addr v8, v2

    aput-wide v8, v6, v0

    goto :goto_0
.end method
