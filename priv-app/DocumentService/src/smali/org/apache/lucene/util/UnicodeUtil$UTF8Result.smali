.class public final Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;
.super Ljava/lang/Object;
.source "UnicodeUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/UnicodeUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UTF8Result"
.end annotation


# instance fields
.field public length:I

.field public result:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    const/16 v0, 0xa

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    return-void
.end method


# virtual methods
.method public setLength(I)V
    .locals 1
    .param p1, "newLength"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    array-length v0, v0

    if-ge v0, p1, :cond_0

    .line 127
    iget-object v0, p0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    invoke-static {v0, p1}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    .line 129
    :cond_0
    iput p1, p0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->length:I

    .line 130
    return-void
.end method
