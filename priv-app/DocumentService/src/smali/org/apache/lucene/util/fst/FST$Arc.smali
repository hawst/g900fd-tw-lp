.class public final Lorg/apache/lucene/util/fst/FST$Arc;
.super Ljava/lang/Object;
.source "FST.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/FST;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Arc"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field arcIdx:I

.field bytesPerArc:I

.field flags:B

.field public label:I

.field nextArc:I

.field public nextFinalOutput:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field node:I

.field numArcs:I

.field public output:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field posArcsStart:I

.field public target:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 166
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;)",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 191
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    .local p1, "other":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->node:I

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->node:I

    .line 192
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    .line 193
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    .line 194
    iget-byte v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    iput-byte v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    .line 195
    iget-object v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .line 196
    iget-object v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    iput-object v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    .line 197
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->nextArc:I

    .line 198
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    .line 199
    iget v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v0, :cond_0

    .line 200
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:I

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->posArcsStart:I

    .line 201
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    .line 202
    iget v0, p1, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    iput v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    .line 204
    :cond_0
    return-object p0
.end method

.method flag(I)Z
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 208
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    iget-byte v0, p0, Lorg/apache/lucene/util/fst/FST$Arc;->flags:B

    # invokes: Lorg/apache/lucene/util/fst/FST;->flag(II)Z
    invoke-static {v0, p1}, Lorg/apache/lucene/util/fst/FST;->access$000(II)Z

    move-result v0

    return v0
.end method

.method public isFinal()Z
    .locals 1

    .prologue
    .line 216
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v0

    return v0
.end method

.method public isLast()Z
    .locals 1

    .prologue
    .line 212
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 221
    .local p0, "this":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    .local v0, "b":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "node="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/fst/FST$Arc;->node:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " target="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/fst/FST$Arc;->target:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " label="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226
    const-string/jumbo v1, " last"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 229
    const-string/jumbo v1, " final"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    :cond_1
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 232
    const-string/jumbo v1, " targetNext"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    :cond_2
    const/16 v1, 0x10

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 235
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " output="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    :cond_3
    const/16 v1, 0x20

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/fst/FST$Arc;->flag(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 238
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " nextFinalOutput="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    :cond_4
    iget v1, p0, Lorg/apache/lucene/util/fst/FST$Arc;->bytesPerArc:I

    if-eqz v1, :cond_5

    .line 241
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " arcArray(idx="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/fst/FST$Arc;->arcIdx:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/fst/FST$Arc;->numArcs:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
