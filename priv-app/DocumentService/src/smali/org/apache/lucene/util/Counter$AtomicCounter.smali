.class final Lorg/apache/lucene/util/Counter$AtomicCounter;
.super Lorg/apache/lucene/util/Counter;
.source "Counter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/Counter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AtomicCounter"
.end annotation


# instance fields
.field private final count:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lorg/apache/lucene/util/Counter;-><init>()V

    .line 80
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/Counter$AtomicCounter;->count:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/Counter$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/util/Counter$1;

    .prologue
    .line 79
    invoke-direct {p0}, Lorg/apache/lucene/util/Counter$AtomicCounter;-><init>()V

    return-void
.end method


# virtual methods
.method public addAndGet(J)J
    .locals 3
    .param p1, "delta"    # J

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/util/Counter$AtomicCounter;->count:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public get()J
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/util/Counter$AtomicCounter;->count:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method
