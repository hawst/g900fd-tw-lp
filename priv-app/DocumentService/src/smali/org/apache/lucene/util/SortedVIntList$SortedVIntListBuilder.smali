.class Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;
.super Ljava/lang/Object;
.source "SortedVIntList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/SortedVIntList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SortedVIntListBuilder"
.end annotation


# instance fields
.field private lastInt:I

.field final synthetic this$0:Lorg/apache/lucene/util/SortedVIntList;


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/SortedVIntList;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 107
    iput-object p1, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput v0, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->lastInt:I

    .line 108
    # invokes: Lorg/apache/lucene/util/SortedVIntList;->initBytes()V
    invoke-static {p1}, Lorg/apache/lucene/util/SortedVIntList;->access$000(Lorg/apache/lucene/util/SortedVIntList;)V

    .line 109
    iput v0, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->lastInt:I

    .line 110
    return-void
.end method


# virtual methods
.method addInt(I)V
    .locals 4
    .param p1, "nextInt"    # I

    .prologue
    .line 113
    iget v1, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->lastInt:I

    sub-int v0, p1, v1

    .line 114
    .local v0, "diff":I
    if-gez v0, :cond_0

    .line 115
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Input not sorted or first element negative."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 119
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    # getter for: Lorg/apache/lucene/util/SortedVIntList;->lastBytePos:I
    invoke-static {v1}, Lorg/apache/lucene/util/SortedVIntList;->access$100(Lorg/apache/lucene/util/SortedVIntList;)I

    move-result v1

    add-int/lit8 v1, v1, 0x5

    iget-object v2, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    # getter for: Lorg/apache/lucene/util/SortedVIntList;->bytes:[B
    invoke-static {v2}, Lorg/apache/lucene/util/SortedVIntList;->access$200(Lorg/apache/lucene/util/SortedVIntList;)[B

    move-result-object v2

    array-length v2, v2

    if-le v1, v2, :cond_1

    .line 121
    iget-object v1, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    iget-object v2, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    # getter for: Lorg/apache/lucene/util/SortedVIntList;->lastBytePos:I
    invoke-static {v2}, Lorg/apache/lucene/util/SortedVIntList;->access$100(Lorg/apache/lucene/util/SortedVIntList;)I

    move-result v2

    add-int/lit8 v2, v2, 0x5

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    # invokes: Lorg/apache/lucene/util/SortedVIntList;->resizeBytes(I)V
    invoke-static {v1, v2}, Lorg/apache/lucene/util/SortedVIntList;->access$300(Lorg/apache/lucene/util/SortedVIntList;I)V

    .line 125
    :cond_1
    :goto_0
    and-int/lit8 v1, v0, -0x80

    if-eqz v1, :cond_2

    .line 126
    iget-object v1, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    # getter for: Lorg/apache/lucene/util/SortedVIntList;->bytes:[B
    invoke-static {v1}, Lorg/apache/lucene/util/SortedVIntList;->access$200(Lorg/apache/lucene/util/SortedVIntList;)[B

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    # operator++ for: Lorg/apache/lucene/util/SortedVIntList;->lastBytePos:I
    invoke-static {v2}, Lorg/apache/lucene/util/SortedVIntList;->access$108(Lorg/apache/lucene/util/SortedVIntList;)I

    move-result v2

    and-int/lit8 v3, v0, 0x7f

    or-int/lit8 v3, v3, -0x80

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 127
    ushr-int/lit8 v0, v0, 0x7

    goto :goto_0

    .line 129
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    # getter for: Lorg/apache/lucene/util/SortedVIntList;->bytes:[B
    invoke-static {v1}, Lorg/apache/lucene/util/SortedVIntList;->access$200(Lorg/apache/lucene/util/SortedVIntList;)[B

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    # operator++ for: Lorg/apache/lucene/util/SortedVIntList;->lastBytePos:I
    invoke-static {v2}, Lorg/apache/lucene/util/SortedVIntList;->access$108(Lorg/apache/lucene/util/SortedVIntList;)I

    move-result v2

    int-to-byte v3, v0

    aput-byte v3, v1, v2

    .line 130
    iget-object v1, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    # operator++ for: Lorg/apache/lucene/util/SortedVIntList;->size:I
    invoke-static {v1}, Lorg/apache/lucene/util/SortedVIntList;->access$408(Lorg/apache/lucene/util/SortedVIntList;)I

    .line 131
    iput p1, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->lastInt:I

    .line 132
    return-void
.end method

.method done()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    iget-object v1, p0, Lorg/apache/lucene/util/SortedVIntList$SortedVIntListBuilder;->this$0:Lorg/apache/lucene/util/SortedVIntList;

    # getter for: Lorg/apache/lucene/util/SortedVIntList;->lastBytePos:I
    invoke-static {v1}, Lorg/apache/lucene/util/SortedVIntList;->access$100(Lorg/apache/lucene/util/SortedVIntList;)I

    move-result v1

    # invokes: Lorg/apache/lucene/util/SortedVIntList;->resizeBytes(I)V
    invoke-static {v0, v1}, Lorg/apache/lucene/util/SortedVIntList;->access$300(Lorg/apache/lucene/util/SortedVIntList;I)V

    .line 136
    return-void
.end method
