.class public Lorg/apache/lucene/util/StringInterner;
.super Ljava/lang/Object;
.source "StringInterner.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public intern(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public intern([CII)Ljava/lang/String;
    .locals 1
    .param p1, "arr"    # [C
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 35
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/StringInterner;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
