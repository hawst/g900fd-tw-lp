.class public Lorg/apache/lucene/util/MapOfSets;
.super Ljava/lang/Object;
.source "MapOfSets.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final theMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Set",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Set",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "this":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<TK;TV;>;"
    .local p1, "m":Ljava/util/Map;, "Ljava/util/Map<TK;Ljava/util/Set<TV;>;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lorg/apache/lucene/util/MapOfSets;->theMap:Ljava/util/Map;

    .line 39
    return-void
.end method


# virtual methods
.method public getMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Set",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<TK;TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/MapOfSets;->theMap:Ljava/util/Map;

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)I"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "val":Ljava/lang/Object;, "TV;"
    iget-object v1, p0, Lorg/apache/lucene/util/MapOfSets;->theMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    iget-object v1, p0, Lorg/apache/lucene/util/MapOfSets;->theMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 61
    .local v0, "theSet":Ljava/util/Set;, "Ljava/util/Set<TV;>;"
    :goto_0
    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 62
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    return v1

    .line 58
    .end local v0    # "theSet":Ljava/util/Set;, "Ljava/util/Set<TV;>;"
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x17

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 59
    .restart local v0    # "theSet":Ljava/util/Set;, "Ljava/util/Set<TV;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/MapOfSets;->theMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public putAll(Ljava/lang/Object;Ljava/util/Collection;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/Collection",
            "<+TV;>;)I"
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "this":Lorg/apache/lucene/util/MapOfSets;, "Lorg/apache/lucene/util/MapOfSets<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "vals":Ljava/util/Collection;, "Ljava/util/Collection<+TV;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/MapOfSets;->theMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    iget-object v1, p0, Lorg/apache/lucene/util/MapOfSets;->theMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 78
    .local v0, "theSet":Ljava/util/Set;, "Ljava/util/Set<TV;>;"
    :goto_0
    invoke-interface {v0, p2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 79
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    return v1

    .line 75
    .end local v0    # "theSet":Ljava/util/Set;, "Ljava/util/Set<TV;>;"
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x17

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 76
    .restart local v0    # "theSet":Ljava/util/Set;, "Ljava/util/Set<TV;>;"
    iget-object v1, p0, Lorg/apache/lucene/util/MapOfSets;->theMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
