.class Lorg/apache/lucene/util/CollectionUtil$1;
.super Lorg/apache/lucene/util/SorterTemplate;
.source "CollectionUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/CollectionUtil;->getSorter(Ljava/util/List;Ljava/util/Comparator;)Lorg/apache/lucene/util/SorterTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private pivot:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field final synthetic val$comp:Ljava/util/Comparator;

.field final synthetic val$list:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lorg/apache/lucene/util/CollectionUtil$1;->val$list:Ljava/util/List;

    iput-object p2, p0, Lorg/apache/lucene/util/CollectionUtil$1;->val$comp:Ljava/util/Comparator;

    invoke-direct {p0}, Lorg/apache/lucene/util/SorterTemplate;-><init>()V

    return-void
.end method


# virtual methods
.method protected compare(II)I
    .locals 3
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$1;->val$comp:Ljava/util/Comparator;

    iget-object v1, p0, Lorg/apache/lucene/util/CollectionUtil$1;->val$list:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/CollectionUtil$1;->val$list:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected comparePivot(I)I
    .locals 3
    .param p1, "j"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$1;->val$comp:Ljava/util/Comparator;

    iget-object v1, p0, Lorg/apache/lucene/util/CollectionUtil$1;->pivot:Ljava/lang/Object;

    iget-object v2, p0, Lorg/apache/lucene/util/CollectionUtil$1;->val$list:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected setPivot(I)V
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$1;->val$list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$1;->pivot:Ljava/lang/Object;

    .line 56
    return-void
.end method

.method protected swap(II)V
    .locals 1
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 45
    iget-object v0, p0, Lorg/apache/lucene/util/CollectionUtil$1;->val$list:Ljava/util/List;

    invoke-static {v0, p1, p2}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 46
    return-void
.end method
