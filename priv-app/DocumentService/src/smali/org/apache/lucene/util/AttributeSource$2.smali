.class Lorg/apache/lucene/util/AttributeSource$2;
.super Ljava/lang/Object;
.source "AttributeSource.java"

# interfaces
.implements Lorg/apache/lucene/util/AttributeReflector;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/AttributeSource;->reflectAsString(Z)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/util/AttributeSource;

.field final synthetic val$buffer:Ljava/lang/StringBuilder;

.field final synthetic val$prependAttClass:Z


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/AttributeSource;Ljava/lang/StringBuilder;Z)V
    .locals 0

    .prologue
    .line 460
    iput-object p1, p0, Lorg/apache/lucene/util/AttributeSource$2;->this$0:Lorg/apache/lucene/util/AttributeSource;

    iput-object p2, p0, Lorg/apache/lucene/util/AttributeSource$2;->val$buffer:Ljava/lang/StringBuilder;

    iput-boolean p3, p0, Lorg/apache/lucene/util/AttributeSource$2;->val$prependAttClass:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/util/Attribute;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 461
    .local p1, "attClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/util/Attribute;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/AttributeSource$2;->val$buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 462
    iget-object v0, p0, Lorg/apache/lucene/util/AttributeSource$2;->val$buffer:Ljava/lang/StringBuilder;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 464
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/util/AttributeSource$2;->val$prependAttClass:Z

    if-eqz v0, :cond_1

    .line 465
    iget-object v0, p0, Lorg/apache/lucene/util/AttributeSource$2;->val$buffer:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 467
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/AttributeSource$2;->val$buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    if-nez p3, :cond_2

    const-string/jumbo p3, "null"

    .end local p3    # "value":Ljava/lang/Object;
    :cond_2
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 468
    return-void
.end method
