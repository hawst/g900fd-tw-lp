.class public final Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;
.super Ljava/lang/Object;
.source "FieldCacheSanityChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/FieldCacheSanityChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InsanityType"
.end annotation


# static fields
.field public static final EXPECTED:Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

.field public static final SUBREADER:Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

.field public static final VALUEMISMATCH:Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;


# instance fields
.field private final label:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 400
    new-instance v0, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

    const-string/jumbo v1, "SUBREADER"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;->SUBREADER:Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

    .line 418
    new-instance v0, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

    const-string/jumbo v1, "VALUEMISMATCH"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;->VALUEMISMATCH:Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

    .line 426
    new-instance v0, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

    const-string/jumbo v1, "EXPECTED"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;->EXPECTED:Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391
    iput-object p1, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;->label:Ljava/lang/String;

    .line 392
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lorg/apache/lucene/util/FieldCacheSanityChecker$InsanityType;->label:Ljava/lang/String;

    return-object v0
.end method
