.class public final Lorg/apache/lucene/util/RecyclingByteBlockAllocator;
.super Lorg/apache/lucene/util/ByteBlockPool$Allocator;
.source "RecyclingByteBlockAllocator.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT_BUFFERED_BLOCKS:I = 0x40


# instance fields
.field private final bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

.field private freeBlocks:I

.field private freeByteBlocks:[[B

.field private final maxBufferedBlocks:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 79
    const v0, 0x8000

    const/16 v1, 0x40

    new-instance v2, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;-><init>(IILjava/util/concurrent/atomic/AtomicLong;)V

    .line 80
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "blockSize"    # I
    .param p2, "maxBufferedBlocks"    # I

    .prologue
    .line 68
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;-><init>(IILjava/util/concurrent/atomic/AtomicLong;)V

    .line 69
    return-void
.end method

.method public constructor <init>(IILjava/util/concurrent/atomic/AtomicLong;)V
    .locals 1
    .param p1, "blockSize"    # I
    .param p2, "maxBufferedBlocks"    # I
    .param p3, "bytesUsed"    # Ljava/util/concurrent/atomic/AtomicLong;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/ByteBlockPool$Allocator;-><init>(I)V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    .line 53
    const/16 v0, 0xa

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-array v0, v0, [[B

    iput-object v0, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    .line 54
    iput p2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->maxBufferedBlocks:I

    .line 55
    iput-object p3, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    .line 56
    return-void
.end method


# virtual methods
.method public bytesUsed()J
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized freeBlocks(I)I
    .locals 6
    .param p1, "num"    # I

    .prologue
    .line 144
    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 147
    :cond_0
    :try_start_1
    iget v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    if-le p1, v2, :cond_1

    .line 148
    const/4 v1, 0x0

    .line 149
    .local v1, "stop":I
    iget v0, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    .line 154
    .local v0, "count":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    if-le v2, v1, :cond_2

    .line 155
    iget-object v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    iget v3, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    const/4 v4, 0x0

    aput-object v4, v2, v3

    goto :goto_0

    .line 151
    .end local v0    # "count":I
    .end local v1    # "stop":I
    :cond_1
    iget v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    sub-int v1, v2, p1

    .line 152
    .restart local v1    # "stop":I
    move v0, p1

    .restart local v0    # "count":I
    goto :goto_0

    .line 157
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    neg-int v3, v0

    iget v4, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->blockSize:I

    mul-int/2addr v3, v4

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 158
    sget-boolean v2, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    :cond_3
    monitor-exit p0

    return v0
.end method

.method public declared-synchronized getByteBlock()[B
    .locals 4

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    if-nez v1, :cond_0

    .line 85
    iget-object v1, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    iget v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->blockSize:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 86
    iget v1, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->blockSize:I

    new-array v0, v1, [B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    :goto_0
    monitor-exit p0

    return-object v0

    .line 88
    :cond_0
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    iget v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    aget-object v0, v1, v2

    .line 89
    .local v0, "b":[B
    iget-object v1, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    iget v2, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    const/4 v3, 0x0

    aput-object v3, v1, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 84
    .end local v0    # "b":[B
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public maxBufferedBlocks()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->maxBufferedBlocks:I

    return v0
.end method

.method public declared-synchronized numBufferedBlocks()I
    .locals 1

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized recycleByteBlocks([[BII)V
    .locals 10
    .param p1, "blocks"    # [[B
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    iget v5, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->maxBufferedBlocks:I

    iget v6, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    sub-int/2addr v5, v6

    sub-int v6, p3, p2

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 96
    .local v2, "numBlocks":I
    iget v5, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    add-int v3, v5, v2

    .line 97
    .local v3, "size":I
    iget-object v5, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    array-length v5, v5

    if-lt v3, v5, :cond_0

    .line 98
    sget v5, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v3, v5}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v5

    new-array v1, v5, [[B

    .line 100
    .local v1, "newBlocks":[[B
    iget-object v5, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget v8, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    invoke-static {v5, v6, v1, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 101
    iput-object v1, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    .line 103
    .end local v1    # "newBlocks":[[B
    :cond_0
    add-int v4, p2, v2

    .line 104
    .local v4, "stop":I
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_1

    .line 105
    iget-object v5, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeByteBlocks:[[B

    iget v6, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->freeBlocks:I

    aget-object v7, p1, v0

    aput-object v7, v5, v6

    .line 106
    const/4 v5, 0x0

    aput-object v5, p1, v0

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_1
    move v0, v4

    :goto_1
    if-ge v0, p3, :cond_2

    .line 109
    const/4 v5, 0x0

    aput-object v5, p1, v0

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 111
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    sub-int v6, p3, v4

    neg-int v6, v6

    iget v7, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->blockSize:I

    mul-int/2addr v6, v7

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 112
    sget-boolean v5, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    iget-object v5, p0, Lorg/apache/lucene/util/RecyclingByteBlockAllocator;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gez v5, :cond_3

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    .end local v0    # "i":I
    .end local v2    # "numBlocks":I
    .end local v3    # "size":I
    .end local v4    # "stop":I
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 113
    .restart local v0    # "i":I
    .restart local v2    # "numBlocks":I
    .restart local v3    # "size":I
    .restart local v4    # "stop":I
    :cond_3
    monitor-exit p0

    return-void
.end method
