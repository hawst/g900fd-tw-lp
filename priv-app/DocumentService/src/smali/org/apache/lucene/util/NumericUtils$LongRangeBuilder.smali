.class public abstract Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;
.super Ljava/lang/Object;
.source "NumericUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/NumericUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "LongRangeBuilder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addRange(JJI)V
    .locals 3
    .param p1, "min"    # J
    .param p3, "max"    # J
    .param p5, "shift"    # I

    .prologue
    .line 442
    invoke-static {p1, p2, p5}, Lorg/apache/lucene/util/NumericUtils;->longToPrefixCoded(JI)Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, p4, p5}, Lorg/apache/lucene/util/NumericUtils;->longToPrefixCoded(JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;->addRange(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    return-void
.end method

.method public addRange(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "minPrefixCoded"    # Ljava/lang/String;
    .param p2, "maxPrefixCoded"    # Ljava/lang/String;

    .prologue
    .line 434
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
