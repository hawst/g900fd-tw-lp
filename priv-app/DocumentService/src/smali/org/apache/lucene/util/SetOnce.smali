.class public final Lorg/apache/lucene/util/SetOnce;
.super Ljava/lang/Object;
.source "SetOnce.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/SetOnce$AlreadySetException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private volatile obj:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final set:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 47
    .local p0, "this":Lorg/apache/lucene/util/SetOnce;, "Lorg/apache/lucene/util/SetOnce<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/SetOnce;->obj:Ljava/lang/Object;

    .line 48
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lorg/apache/lucene/util/SetOnce;->set:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 49
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p0, "this":Lorg/apache/lucene/util/SetOnce;, "Lorg/apache/lucene/util/SetOnce<TT;>;"
    .local p1, "obj":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/SetOnce;->obj:Ljava/lang/Object;

    .line 60
    iput-object p1, p0, Lorg/apache/lucene/util/SetOnce;->obj:Ljava/lang/Object;

    .line 61
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lorg/apache/lucene/util/SetOnce;->set:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 62
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 75
    .local p0, "this":Lorg/apache/lucene/util/SetOnce;, "Lorg/apache/lucene/util/SetOnce<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/SetOnce;->obj:Ljava/lang/Object;

    return-object v0
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p0, "this":Lorg/apache/lucene/util/SetOnce;, "Lorg/apache/lucene/util/SetOnce<TT;>;"
    .local p1, "obj":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lorg/apache/lucene/util/SetOnce;->set:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iput-object p1, p0, Lorg/apache/lucene/util/SetOnce;->obj:Ljava/lang/Object;

    .line 71
    return-void

    .line 69
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/SetOnce$AlreadySetException;

    invoke-direct {v0}, Lorg/apache/lucene/util/SetOnce$AlreadySetException;-><init>()V

    throw v0
.end method
