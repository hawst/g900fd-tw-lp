.class Lorg/apache/lucene/util/packed/Direct64;
.super Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;
.source "Direct64.java"

# interfaces
.implements Lorg/apache/lucene/util/packed/PackedInts$Mutable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final BITS_PER_VALUE:I = 0x40


# instance fields
.field private values:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/util/packed/Direct64;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/Direct64;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "valueCount"    # I

    .prologue
    .line 37
    const/16 v0, 0x40

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 38
    new-array v0, p1, [J

    iput-object v0, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    .line 39
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataInput;I)V
    .locals 4
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p2, "valueCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    const/16 v2, 0x40

    invoke-direct {p0, p2, v2}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 43
    new-array v1, p2, [J

    .line 44
    .local v1, "values":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 45
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readLong()J

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    iput-object v1, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    .line 49
    return-void
.end method

.method public constructor <init>([J)V
    .locals 2
    .param p1, "values"    # [J

    .prologue
    .line 59
    array-length v0, p1

    const/16 v1, 0x40

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 60
    iput-object p1, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    .line 61
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 4

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 78
    return-void
.end method

.method public get(I)J
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 64
    sget-boolean v0, Lorg/apache/lucene/util/packed/Direct64;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/Direct64;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 65
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public bridge synthetic getArray()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/Direct64;->getArray()[J

    move-result-object v0

    return-object v0
.end method

.method public getArray()[J
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    return-object v0
.end method

.method public hasArray()Z
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x1

    return v0
.end method

.method public ramBytesUsed()J
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    invoke-static {v0}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf([J)J

    move-result-wide v0

    return-wide v0
.end method

.method public set(IJ)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Direct64;->values:[J

    aput-wide p2, v0, p1

    .line 70
    return-void
.end method
