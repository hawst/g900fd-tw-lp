.class Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
.super Ljava/lang/Object;
.source "IndexableBinaryStringTools.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/IndexableBinaryStringTools;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CodingCase"
.end annotation


# instance fields
.field advanceBytes:I

.field finalMask:S

.field finalShift:I

.field initialShift:I

.field middleMask:S

.field middleShift:I

.field numBytes:I


# direct methods
.method constructor <init>(II)V
    .locals 1
    .param p1, "initialShift"    # I
    .param p2, "finalShift"    # I

    .prologue
    const/4 v0, 0x2

    .line 386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374
    iput v0, p0, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->advanceBytes:I

    .line 387
    iput v0, p0, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->numBytes:I

    .line 388
    iput p1, p0, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    .line 389
    iput p2, p0, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalShift:I

    .line 390
    const/16 v0, 0xff

    ushr-int/2addr v0, p2

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalMask:S

    .line 391
    if-eqz p2, :cond_0

    .line 392
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->advanceBytes:I

    .line 394
    :cond_0
    return-void
.end method

.method constructor <init>(III)V
    .locals 2
    .param p1, "initialShift"    # I
    .param p2, "middleShift"    # I
    .param p3, "finalShift"    # I

    .prologue
    const/16 v1, 0xff

    .line 377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374
    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->advanceBytes:I

    .line 378
    const/4 v0, 0x3

    iput v0, p0, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->numBytes:I

    .line 379
    iput p1, p0, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    .line 380
    iput p2, p0, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->middleShift:I

    .line 381
    iput p3, p0, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalShift:I

    .line 382
    ushr-int v0, v1, p3

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalMask:S

    .line 383
    shl-int v0, v1, p2

    int-to-short v0, v0

    iput-short v0, p0, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->middleMask:S

    .line 384
    return-void
.end method
