.class public final Lorg/apache/lucene/util/CharsRef;
.super Ljava/lang/Object;
.source "CharsRef.java"

# interfaces
.implements Ljava/lang/CharSequence;
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/CharsRef$1;,
        Lorg/apache/lucene/util/CharsRef$UTF16SortedAsUTF8Comparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/util/CharsRef;",
        ">;",
        "Ljava/lang/CharSequence;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final EMPTY_CHARS:[C

.field private static final utf16SortedAsUTF8SortOrder:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/CharsRef;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public chars:[C

.field public length:I

.field public offset:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    const-class v0, Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/CharsRef;->$assertionsDisabled:Z

    .line 29
    new-array v0, v1, [C

    sput-object v0, Lorg/apache/lucene/util/CharsRef;->EMPTY_CHARS:[C

    .line 204
    new-instance v0, Lorg/apache/lucene/util/CharsRef$UTF16SortedAsUTF8Comparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/CharsRef$UTF16SortedAsUTF8Comparator;-><init>(Lorg/apache/lucene/util/CharsRef$1;)V

    sput-object v0, Lorg/apache/lucene/util/CharsRef;->utf16SortedAsUTF8SortOrder:Ljava/util/Comparator;

    return-void

    :cond_0
    move v0, v1

    .line 28
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    sget-object v0, Lorg/apache/lucene/util/CharsRef;->EMPTY_CHARS:[C

    invoke-direct {p0, v0, v1, v1}, Lorg/apache/lucene/util/CharsRef;-><init>([CII)V

    .line 39
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-array v0, p1, [C

    iput-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 47
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 68
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    array-length v0, v0

    iput v0, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    .line 69
    return-void
.end method

.method public constructor <init>([CII)V
    .locals 2
    .param p1, "chars"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    sget-boolean v0, Lorg/apache/lucene/util/CharsRef;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 55
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/CharsRef;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    array-length v0, p1

    add-int v1, p2, p3

    if-ge v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 56
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 57
    iput p2, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 58
    iput p3, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    .line 59
    return-void
.end method

.method public static deepCopyOf(Lorg/apache/lucene/util/CharsRef;)Lorg/apache/lucene/util/CharsRef;
    .locals 1
    .param p0, "other"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    .line 264
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    .line 265
    .local v0, "clone":Lorg/apache/lucene/util/CharsRef;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/CharsRef;->copyChars(Lorg/apache/lucene/util/CharsRef;)V

    .line 266
    return-object v0
.end method

.method public static getUTF16SortedAsUTF8Comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/CharsRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 207
    sget-object v0, Lorg/apache/lucene/util/CharsRef;->utf16SortedAsUTF8SortOrder:Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method public append([CII)V
    .locals 4
    .param p1, "otherChars"    # [C
    .param p2, "otherOffset"    # I
    .param p3, "otherLength"    # I

    .prologue
    .line 180
    iget v1, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int v0, v1, p3

    .line 181
    .local v0, "newLength":I
    iget v1, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    add-int/2addr v1, v0

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/CharsRef;->grow(I)V

    .line 182
    iget-object v1, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v3, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int/2addr v2, v3

    invoke-static {p1, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 184
    iget v1, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int/2addr v1, p3

    iput v1, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    .line 185
    return-void
.end method

.method public charAt(I)C
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 197
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v1, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    add-int/2addr v1, p1

    aget-char v0, v0, v1

    return v0
.end method

.method public charsEquals(Lorg/apache/lucene/util/CharsRef;)Z
    .locals 7
    .param p1, "other"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    const/4 v4, 0x0

    .line 99
    iget v5, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    iget v6, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    if-ne v5, v6, :cond_0

    .line 100
    iget v2, p1, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 101
    .local v2, "otherUpto":I
    iget-object v1, p1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 102
    .local v1, "otherChars":[C
    iget v5, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v6, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int v0, v5, v6

    .line 103
    .local v0, "end":I
    iget v3, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    .local v3, "upto":I
    :goto_0
    if-ge v3, v0, :cond_2

    .line 104
    iget-object v5, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    aget-char v5, v5, v3

    aget-char v6, v1, v2

    if-eq v5, v6, :cond_1

    .line 110
    .end local v0    # "end":I
    .end local v1    # "otherChars":[C
    .end local v2    # "otherUpto":I
    .end local v3    # "upto":I
    :cond_0
    :goto_1
    return v4

    .line 103
    .restart local v0    # "end":I
    .restart local v1    # "otherChars":[C
    .restart local v2    # "otherUpto":I
    .restart local v3    # "upto":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 108
    :cond_2
    const/4 v4, 0x1

    goto :goto_1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p0}, Lorg/apache/lucene/util/CharsRef;->clone()Lorg/apache/lucene/util/CharsRef;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/util/CharsRef;
    .locals 4

    .prologue
    .line 73
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    iget-object v1, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v3, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/CharsRef;-><init>([CII)V

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 28
    check-cast p1, Lorg/apache/lucene/util/CharsRef;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/CharsRef;->compareTo(Lorg/apache/lucene/util/CharsRef;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/util/CharsRef;)I
    .locals 11
    .param p1, "other"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    .line 116
    if-ne p0, p1, :cond_0

    .line 117
    const/4 v9, 0x0

    .line 137
    :goto_0
    return v9

    .line 119
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 120
    .local v0, "aChars":[C
    iget v3, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 121
    .local v3, "aUpto":I
    iget-object v5, p1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 122
    .local v5, "bChars":[C
    iget v7, p1, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 124
    .local v7, "bUpto":I
    iget v9, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    iget v10, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    add-int v2, v3, v9

    .local v2, "aStop":I
    move v8, v7

    .end local v7    # "bUpto":I
    .local v8, "bUpto":I
    move v4, v3

    .line 126
    .end local v3    # "aUpto":I
    .local v4, "aUpto":I
    :goto_1
    if-ge v4, v2, :cond_3

    .line 127
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "aUpto":I
    .restart local v3    # "aUpto":I
    aget-char v1, v0, v4

    .line 128
    .local v1, "aInt":I
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "bUpto":I
    .restart local v7    # "bUpto":I
    aget-char v6, v5, v8

    .line 129
    .local v6, "bInt":I
    if-le v1, v6, :cond_1

    .line 130
    const/4 v9, 0x1

    goto :goto_0

    .line 131
    :cond_1
    if-ge v1, v6, :cond_2

    .line 132
    const/4 v9, -0x1

    goto :goto_0

    :cond_2
    move v8, v7

    .end local v7    # "bUpto":I
    .restart local v8    # "bUpto":I
    move v4, v3

    .line 134
    .end local v3    # "aUpto":I
    .restart local v4    # "aUpto":I
    goto :goto_1

    .line 137
    .end local v1    # "aInt":I
    .end local v6    # "bInt":I
    :cond_3
    iget v9, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    iget v10, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    sub-int/2addr v9, v10

    goto :goto_0
.end method

.method public copyChars(Lorg/apache/lucene/util/CharsRef;)V
    .locals 5
    .param p1, "other"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    const/4 v4, 0x0

    .line 149
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    if-nez v0, :cond_0

    .line 150
    iget v0, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 154
    :goto_0
    iget-object v0, p1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v1, p1, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget-object v2, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v3, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v0, v1, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155
    iget v0, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    iput v0, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    .line 156
    iput v4, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 157
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v1, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->grow([CI)[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    goto :goto_0
.end method

.method public copyChars([CII)V
    .locals 2
    .param p1, "otherChars"    # [C
    .param p2, "otherOffset"    # I
    .param p3, "otherLength"    # I

    .prologue
    const/4 v1, 0x0

    .line 169
    invoke-virtual {p0, p3}, Lorg/apache/lucene/util/CharsRef;->grow(I)V

    .line 170
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 172
    iput v1, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 173
    iput p3, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    .line 174
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 89
    if-nez p1, :cond_1

    .line 95
    .end local p1    # "other":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 92
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Lorg/apache/lucene/util/CharsRef;

    if-eqz v1, :cond_0

    .line 93
    check-cast p1, Lorg/apache/lucene/util/CharsRef;

    .end local p1    # "other":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/CharsRef;->charsEquals(Lorg/apache/lucene/util/CharsRef;)Z

    move-result v0

    goto :goto_0
.end method

.method public grow(I)V
    .locals 1
    .param p1, "newLength"    # I

    .prologue
    .line 160
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    array-length v0, v0

    if-ge v0, p1, :cond_0

    .line 161
    iget-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    invoke-static {v0, p1}, Lorg/apache/lucene/util/ArrayUtil;->grow([CI)[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 163
    :cond_0
    return-void
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 78
    const/16 v2, 0x1f

    .line 79
    .local v2, "prime":I
    const/4 v3, 0x0

    .line 80
    .local v3, "result":I
    iget v4, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v5, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int v0, v4, v5

    .line 81
    .local v0, "end":I
    iget v1, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 82
    mul-int/lit8 v4, v3, 0x1f

    iget-object v5, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    aget-char v5, v5, v1

    add-int v3, v4, v5

    .line 81
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    :cond_0
    return v3
.end method

.method public length()I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    return v0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 201
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    iget-object v1, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    add-int/2addr v2, p1

    iget v3, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    add-int/2addr v3, p2

    add-int/lit8 v3, v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/CharsRef;-><init>([CII)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 189
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v2, p0, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v3, p0, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method
