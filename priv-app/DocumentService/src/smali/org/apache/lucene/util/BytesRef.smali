.class public final Lorg/apache/lucene/util/BytesRef;
.super Ljava/lang/Object;
.source "BytesRef.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/BytesRef$1;,
        Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUTF16Comparator;,
        Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUnicodeComparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/util/BytesRef;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final EMPTY_BYTES:[B

.field private static final utf8SortedAsUTF16SortOrder:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private static final utf8SortedAsUnicodeSortOrder:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public bytes:[B

.field public length:I

.field public offset:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 27
    const-class v0, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/BytesRef;->$assertionsDisabled:Z

    .line 29
    new-array v0, v1, [B

    sput-object v0, Lorg/apache/lucene/util/BytesRef;->EMPTY_BYTES:[B

    .line 258
    new-instance v0, Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUnicodeComparator;

    invoke-direct {v0, v2}, Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUnicodeComparator;-><init>(Lorg/apache/lucene/util/BytesRef$1;)V

    sput-object v0, Lorg/apache/lucene/util/BytesRef;->utf8SortedAsUnicodeSortOrder:Ljava/util/Comparator;

    .line 290
    new-instance v0, Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUTF16Comparator;

    invoke-direct {v0, v2}, Lorg/apache/lucene/util/BytesRef$UTF8SortedAsUTF16Comparator;-><init>(Lorg/apache/lucene/util/BytesRef$1;)V

    sput-object v0, Lorg/apache/lucene/util/BytesRef;->utf8SortedAsUTF16SortOrder:Ljava/util/Comparator;

    return-void

    :cond_0
    move v0, v1

    .line 27
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lorg/apache/lucene/util/BytesRef;->EMPTY_BYTES:[B

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    .line 43
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-array v0, p1, [B

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 67
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 77
    invoke-direct {p0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 78
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/BytesRef;->copyChars(Ljava/lang/CharSequence;)V

    .line 79
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "bytes"    # [B

    .prologue
    .line 58
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>([BII)V

    .line 59
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 1
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    sget-boolean v0, Lorg/apache/lucene/util/BytesRef;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 50
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 51
    iput p2, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 52
    iput p3, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 53
    return-void
.end method

.method public static deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .param p0, "other"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 355
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 356
    .local v0, "copy":Lorg/apache/lucene/util/BytesRef;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 357
    return-object v0
.end method

.method public static getUTF8SortedAsUTF16Comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293
    sget-object v0, Lorg/apache/lucene/util/BytesRef;->utf8SortedAsUTF16SortOrder:Ljava/util/Comparator;

    return-object v0
.end method

.method public static getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261
    sget-object v0, Lorg/apache/lucene/util/BytesRef;->utf8SortedAsUnicodeSortOrder:Ljava/util/Comparator;

    return-object v0
.end method

.method private sliceEquals(Lorg/apache/lucene/util/BytesRef;I)Z
    .locals 8
    .param p1, "other"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "pos"    # I

    .prologue
    const/4 v5, 0x0

    .line 132
    if-ltz p2, :cond_0

    iget v6, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v6, p2

    iget v7, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v6, v7, :cond_1

    .line 145
    :cond_0
    :goto_0
    return v5

    .line 135
    :cond_1
    iget v6, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int v0, v6, p2

    .line 136
    .local v0, "i":I
    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 137
    .local v2, "j":I
    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v7, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v4, v6, v7

    .local v4, "k":I
    move v3, v2

    .end local v2    # "j":I
    .local v3, "j":I
    move v1, v0

    .line 139
    .end local v0    # "i":I
    .local v1, "i":I
    :goto_1
    if-ge v3, v4, :cond_2

    .line 140
    iget-object v6, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aget-byte v6, v6, v1

    iget-object v7, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "j":I
    .restart local v2    # "j":I
    aget-byte v7, v7, v3

    if-ne v6, v7, :cond_0

    move v3, v2

    .end local v2    # "j":I
    .restart local v3    # "j":I
    move v1, v0

    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_1

    .line 145
    :cond_2
    const/4 v5, 0x1

    goto :goto_0
.end method


# virtual methods
.method public append(Lorg/apache/lucene/util/BytesRef;)V
    .locals 7
    .param p1, "other"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v5, 0x0

    .line 236
    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v1, v2, v3

    .line 237
    .local v1, "newLen":I
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v2, v2

    if-ge v2, v1, :cond_0

    .line 238
    new-array v0, v1, [B

    .line 239
    .local v0, "newBytes":[B
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v4, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v2, v3, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 240
    iput v5, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 241
    iput-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 243
    .end local v0    # "newBytes":[B
    :cond_0
    iget-object v2, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v6, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v5, v6

    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 244
    iput v1, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 245
    return-void
.end method

.method public bytesEquals(Lorg/apache/lucene/util/BytesRef;)Z
    .locals 7
    .param p1, "other"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v4, 0x0

    .line 110
    sget-boolean v5, Lorg/apache/lucene/util/BytesRef;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    if-nez p1, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 111
    :cond_0
    iget v5, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ne v5, v6, :cond_1

    .line 112
    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 113
    .local v2, "otherUpto":I
    iget-object v1, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 114
    .local v1, "otherBytes":[B
    iget v5, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v6, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v0, v5, v6

    .line 115
    .local v0, "end":I
    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .local v3, "upto":I
    :goto_0
    if-ge v3, v0, :cond_3

    .line 116
    iget-object v5, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v5, v5, v3

    aget-byte v6, v1, v2

    if-eq v5, v6, :cond_2

    .line 122
    .end local v0    # "end":I
    .end local v1    # "otherBytes":[B
    .end local v2    # "otherUpto":I
    .end local v3    # "upto":I
    :cond_1
    :goto_1
    return v4

    .line 115
    .restart local v0    # "end":I
    .restart local v1    # "otherBytes":[B
    .restart local v2    # "otherUpto":I
    .restart local v3    # "upto":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 120
    :cond_3
    const/4 v4, 0x1

    goto :goto_1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-virtual {p0}, Lorg/apache/lucene/util/BytesRef;->clone()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/util/BytesRef;
    .locals 4

    .prologue
    .line 128
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/BytesRef;-><init>([BII)V

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 27
    check-cast p1, Lorg/apache/lucene/util/BytesRef;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/util/BytesRef;)I
    .locals 1
    .param p1, "other"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 255
    sget-object v0, Lorg/apache/lucene/util/BytesRef;->utf8SortedAsUnicodeSortOrder:Ljava/util/Comparator;

    invoke-interface {v0, p0, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public copyBytes(Lorg/apache/lucene/util/BytesRef;)V
    .locals 5
    .param p1, "other"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 221
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v0, v0

    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v0, v1, :cond_0

    .line 222
    iget v0, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 223
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 225
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 226
    iget v0, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v0, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 227
    return-void
.end method

.method public copyChars(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-static {p1, v0, v1, p0}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8(Ljava/lang/CharSequence;IILorg/apache/lucene/util/BytesRef;)V

    .line 90
    return-void
.end method

.method public copyChars([CII)V
    .locals 0
    .param p1, "text"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 99
    invoke-static {p1, p2, p3, p0}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8([CIILorg/apache/lucene/util/BytesRef;)V

    .line 100
    return-void
.end method

.method public endsWith(Lorg/apache/lucene/util/BytesRef;)Z
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 153
    iget v0, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v0, v1

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/BytesRef;->sliceEquals(Lorg/apache/lucene/util/BytesRef;I)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 177
    if-nez p1, :cond_1

    .line 183
    .end local p1    # "other":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 180
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Lorg/apache/lucene/util/BytesRef;

    if-eqz v1, :cond_0

    .line 181
    check-cast p1, Lorg/apache/lucene/util/BytesRef;

    .end local p1    # "other":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/BytesRef;->bytesEquals(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    goto :goto_0
.end method

.method public grow(I)V
    .locals 1
    .param p1, "newLength"    # I

    .prologue
    .line 250
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-static {v0, p1}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 251
    return-void
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 167
    const/4 v1, 0x0

    .line 168
    .local v1, "hash":I
    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v4, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v0, v3, v4

    .line 169
    .local v0, "end":I
    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 170
    mul-int/lit8 v3, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v4, v4, v2

    add-int v1, v3, v4

    .line 169
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 172
    :cond_0
    return v1
.end method

.method public startsWith(Lorg/apache/lucene/util/BytesRef;)Z
    .locals 1
    .param p1, "other"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 149
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/BytesRef;->sliceEquals(Lorg/apache/lucene/util/BytesRef;I)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 201
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 202
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/16 v3, 0x5b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 203
    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v4, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int v0, v3, v4

    .line 204
    .local v0, "end":I
    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 205
    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    if-le v1, v3, :cond_0

    .line 206
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 208
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v3, v3, v1

    and-int/lit16 v3, v3, 0xff

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 210
    :cond_1
    const/16 v3, 0x5d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 211
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public utf8ToString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 190
    :try_start_0
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v4, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    const-string/jumbo v5, "UTF-8"

    invoke-direct {v1, v2, v3, v4, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 191
    :catch_0
    move-exception v0

    .line 194
    .local v0, "uee":Ljava/io/UnsupportedEncodingException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
