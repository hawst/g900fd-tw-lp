.class Lorg/apache/lucene/util/packed/Packed32;
.super Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;
.source "Packed32.java"

# interfaces
.implements Lorg/apache/lucene/util/packed/PackedInts$Mutable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final BLOCK_BITS:I = 0x5

.field static final BLOCK_SIZE:I = 0x20

.field private static final ENTRY_SIZE:I = 0x21

.field private static final FAC_BITPOS:I = 0x3

.field private static final MASKS:[[I

.field static final MOD_MASK:I = 0x1f

.field private static final SHIFTS:[[I

.field private static final WRITE_MASKS:[[I


# instance fields
.field private blocks:[I

.field private maxPos:I

.field private readMasks:[I

.field private shifts:[I

.field private writeMasks:[I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/16 v13, 0x63

    const/16 v12, 0x21

    const/16 v11, 0x20

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 35
    const-class v7, Lorg/apache/lucene/util/packed/Packed32;

    invoke-virtual {v7}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v7

    if-nez v7, :cond_0

    const/4 v7, 0x1

    :goto_0
    sput-boolean v7, Lorg/apache/lucene/util/packed/Packed32;->$assertionsDisabled:Z

    .line 53
    filled-new-array {v12, v13}, [I

    move-result-object v7

    sget-object v10, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v10, v7}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [[I

    sput-object v7, Lorg/apache/lucene/util/packed/Packed32;->SHIFTS:[[I

    .line 55
    filled-new-array {v12, v12}, [I

    move-result-object v7

    sget-object v10, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v10, v7}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [[I

    sput-object v7, Lorg/apache/lucene/util/packed/Packed32;->MASKS:[[I

    .line 58
    const/4 v4, 0x1

    .local v4, "elementBits":I
    :goto_1
    if-gt v4, v11, :cond_3

    .line 59
    const/4 v1, 0x0

    .local v1, "bitPos":I
    :goto_2
    if-ge v1, v11, :cond_2

    .line 60
    sget-object v7, Lorg/apache/lucene/util/packed/Packed32;->SHIFTS:[[I

    aget-object v3, v7, v4

    .line 61
    .local v3, "currentShifts":[I
    mul-int/lit8 v0, v1, 0x3

    .line 62
    .local v0, "base":I
    aput v1, v3, v0

    .line 63
    add-int/lit8 v7, v0, 0x1

    rsub-int/lit8 v10, v4, 0x20

    aput v10, v3, v7

    .line 64
    rsub-int/lit8 v7, v4, 0x20

    if-gt v1, v7, :cond_1

    .line 65
    add-int/lit8 v7, v0, 0x2

    aput v8, v3, v7

    .line 66
    sget-object v7, Lorg/apache/lucene/util/packed/Packed32;->MASKS:[[I

    aget-object v7, v7, v4

    aput v8, v7, v1

    .line 59
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v0    # "base":I
    .end local v1    # "bitPos":I
    .end local v3    # "currentShifts":[I
    .end local v4    # "elementBits":I
    :cond_0
    move v7, v8

    .line 35
    goto :goto_0

    .line 68
    .restart local v0    # "base":I
    .restart local v1    # "bitPos":I
    .restart local v3    # "currentShifts":[I
    .restart local v4    # "elementBits":I
    :cond_1
    rsub-int/lit8 v7, v1, 0x20

    sub-int v6, v4, v7

    .line 69
    .local v6, "rBits":I
    add-int/lit8 v7, v0, 0x2

    rsub-int/lit8 v10, v6, 0x20

    aput v10, v3, v7

    .line 70
    sget-object v7, Lorg/apache/lucene/util/packed/Packed32;->MASKS:[[I

    aget-object v7, v7, v4

    shl-int v10, v9, v6

    xor-int/lit8 v10, v10, -0x1

    aput v10, v7, v1

    goto :goto_3

    .line 58
    .end local v0    # "base":I
    .end local v3    # "currentShifts":[I
    .end local v6    # "rBits":I
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 79
    .end local v1    # "bitPos":I
    :cond_3
    filled-new-array {v12, v13}, [I

    move-result-object v7

    sget-object v10, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v10, v7}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [[I

    sput-object v7, Lorg/apache/lucene/util/packed/Packed32;->WRITE_MASKS:[[I

    .line 82
    const/4 v4, 0x1

    :goto_4
    if-gt v4, v11, :cond_7

    .line 83
    shl-int v7, v9, v4

    xor-int/lit8 v5, v7, -0x1

    .line 84
    .local v5, "elementPosMask":I
    sget-object v7, Lorg/apache/lucene/util/packed/Packed32;->SHIFTS:[[I

    aget-object v3, v7, v4

    .line 85
    .restart local v3    # "currentShifts":[I
    sget-object v7, Lorg/apache/lucene/util/packed/Packed32;->WRITE_MASKS:[[I

    aget-object v2, v7, v4

    .line 86
    .local v2, "currentMasks":[I
    const/4 v1, 0x0

    .restart local v1    # "bitPos":I
    :goto_5
    if-ge v1, v11, :cond_6

    .line 87
    mul-int/lit8 v0, v1, 0x3

    .line 88
    .restart local v0    # "base":I
    add-int/lit8 v7, v0, 0x1

    aget v7, v3, v7

    shl-int v7, v5, v7

    aget v10, v3, v0

    ushr-int/2addr v7, v10

    xor-int/lit8 v7, v7, -0x1

    aput v7, v2, v0

    .line 91
    rsub-int/lit8 v7, v4, 0x20

    if-gt v1, v7, :cond_4

    .line 92
    add-int/lit8 v7, v0, 0x1

    aput v9, v2, v7

    .line 93
    add-int/lit8 v7, v0, 0x2

    aput v8, v2, v7

    .line 86
    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 95
    :cond_4
    add-int/lit8 v7, v0, 0x1

    add-int/lit8 v10, v0, 0x2

    aget v10, v3, v10

    shl-int v10, v5, v10

    xor-int/lit8 v10, v10, -0x1

    aput v10, v2, v7

    .line 97
    add-int/lit8 v10, v0, 0x2

    add-int/lit8 v7, v0, 0x2

    aget v7, v3, v7

    if-nez v7, :cond_5

    move v7, v8

    :goto_7
    aput v7, v2, v10

    goto :goto_6

    :cond_5
    move v7, v9

    goto :goto_7

    .line 82
    .end local v0    # "base":I
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 101
    .end local v1    # "bitPos":I
    .end local v2    # "currentMasks":[I
    .end local v3    # "currentShifts":[I
    .end local v5    # "elementPosMask":I
    :cond_7
    return-void
.end method

.method public constructor <init>(II)V
    .locals 4
    .param p1, "valueCount"    # I
    .param p2, "bitsPerValue"    # I

    .prologue
    .line 120
    int-to-long v0, p1

    int-to-long v2, p2

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x20

    div-long/2addr v0, v2

    const-wide/16 v2, 0x2

    add-long/2addr v0, v2

    long-to-int v0, v0

    new-array v0, v0, [I

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/lucene/util/packed/Packed32;-><init>([III)V

    .line 122
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataInput;II)V
    .locals 4
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p2, "valueCount"    # I
    .param p3, "bitsPerValue"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 135
    invoke-static {p3, p2}, Lorg/apache/lucene/util/packed/Packed32;->size(II)I

    move-result v1

    .line 136
    .local v1, "size":I
    add-int/lit8 v2, v1, 0x1

    new-array v2, v2, [I

    iput-object v2, p0, Lorg/apache/lucene/util/packed/Packed32;->blocks:[I

    .line 138
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 139
    iget-object v2, p0, Lorg/apache/lucene/util/packed/Packed32;->blocks:[I

    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readInt()I

    move-result v3

    aput v3, v2, v0

    .line 138
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 141
    :cond_0
    rem-int/lit8 v2, v1, 0x2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 142
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readInt()I

    .line 144
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/util/packed/Packed32;->updateCached()V

    .line 145
    return-void
.end method

.method public constructor <init>([III)V
    .locals 5
    .param p1, "blocks"    # [I
    .param p2, "valueCount"    # I
    .param p3, "bitsPerValue"    # I

    .prologue
    .line 165
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 166
    const/16 v0, 0x1f

    if-le p3, v0, :cond_0

    .line 167
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "This array only supports values of 31 bits or less. The required number of bits was %d. The Packed64 implementation allows values with more than 31 bits"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/util/packed/Packed32;->blocks:[I

    .line 174
    invoke-direct {p0}, Lorg/apache/lucene/util/packed/Packed32;->updateCached()V

    .line 175
    return-void
.end method

.method private static size(II)I
    .locals 8
    .param p0, "bitsPerValue"    # I
    .param p1, "valueCount"    # I

    .prologue
    const-wide/16 v6, 0x20

    .line 148
    int-to-long v2, p1

    int-to-long v4, p0

    mul-long v0, v2, v4

    .line 149
    .local v0, "totBitCount":J
    div-long v4, v0, v6

    rem-long v2, v0, v6

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    int-to-long v2, v2

    add-long/2addr v2, v4

    long-to-int v2, v2

    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private updateCached()V
    .locals 4

    .prologue
    .line 178
    sget-object v0, Lorg/apache/lucene/util/packed/Packed32;->MASKS:[[I

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed32;->bitsPerValue:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/util/packed/Packed32;->readMasks:[I

    .line 179
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Packed32;->blocks:[I

    array-length v0, v0

    int-to-long v0, v0

    const-wide/16 v2, 0x20

    mul-long/2addr v0, v2

    iget v2, p0, Lorg/apache/lucene/util/packed/Packed32;->bitsPerValue:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    const-wide/16 v2, 0x2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/util/packed/Packed32;->maxPos:I

    .line 180
    sget-object v0, Lorg/apache/lucene/util/packed/Packed32;->SHIFTS:[[I

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed32;->bitsPerValue:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/util/packed/Packed32;->shifts:[I

    .line 181
    sget-object v0, Lorg/apache/lucene/util/packed/Packed32;->WRITE_MASKS:[[I

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed32;->bitsPerValue:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/util/packed/Packed32;->writeMasks:[I

    .line 182
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Packed32;->blocks:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 216
    return-void
.end method

.method public get(I)J
    .locals 10
    .param p1, "index"    # I

    .prologue
    .line 189
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed32;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/Packed32;->size()I

    move-result v3

    if-lt p1, v3, :cond_1

    :cond_0
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 190
    :cond_1
    int-to-long v6, p1

    iget v3, p0, Lorg/apache/lucene/util/packed/Packed32;->bitsPerValue:I

    int-to-long v8, v3

    mul-long v4, v6, v8

    .line 191
    .local v4, "majorBitPos":J
    const/4 v3, 0x5

    ushr-long v6, v4, v3

    long-to-int v2, v6

    .line 192
    .local v2, "elementPos":I
    const-wide/16 v6, 0x1f

    and-long/2addr v6, v4

    long-to-int v1, v6

    .line 194
    .local v1, "bitPos":I
    mul-int/lit8 v0, v1, 0x3

    .line 196
    .local v0, "base":I
    iget-object v3, p0, Lorg/apache/lucene/util/packed/Packed32;->blocks:[I

    aget v3, v3, v2

    iget-object v6, p0, Lorg/apache/lucene/util/packed/Packed32;->shifts:[I

    aget v6, v6, v0

    shl-int/2addr v3, v6

    iget-object v6, p0, Lorg/apache/lucene/util/packed/Packed32;->shifts:[I

    add-int/lit8 v7, v0, 0x1

    aget v6, v6, v7

    ushr-int/2addr v3, v6

    iget-object v6, p0, Lorg/apache/lucene/util/packed/Packed32;->blocks:[I

    add-int/lit8 v7, v2, 0x1

    aget v6, v6, v7

    iget-object v7, p0, Lorg/apache/lucene/util/packed/Packed32;->shifts:[I

    add-int/lit8 v8, v0, 0x2

    aget v7, v7, v8

    ushr-int/2addr v6, v7

    iget-object v7, p0, Lorg/apache/lucene/util/packed/Packed32;->readMasks:[I

    aget v7, v7, v1

    and-int/2addr v6, v7

    or-int/2addr v3, v6

    int-to-long v6, v3

    return-wide v6
.end method

.method public ramBytesUsed()J
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Packed32;->blocks:[I

    invoke-static {v0}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf([I)J

    move-result-wide v0

    return-wide v0
.end method

.method public set(IJ)V
    .locals 12
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 201
    long-to-int v3, p2

    .line 202
    .local v3, "intValue":I
    int-to-long v6, p1

    iget v8, p0, Lorg/apache/lucene/util/packed/Packed32;->bitsPerValue:I

    int-to-long v8, v8

    mul-long v4, v6, v8

    .line 203
    .local v4, "majorBitPos":J
    const/4 v6, 0x5

    ushr-long v6, v4, v6

    long-to-int v2, v6

    .line 204
    .local v2, "elementPos":I
    const-wide/16 v6, 0x1f

    and-long/2addr v6, v4

    long-to-int v1, v6

    .line 205
    .local v1, "bitPos":I
    mul-int/lit8 v0, v1, 0x3

    .line 207
    .local v0, "base":I
    iget-object v6, p0, Lorg/apache/lucene/util/packed/Packed32;->blocks:[I

    iget-object v7, p0, Lorg/apache/lucene/util/packed/Packed32;->blocks:[I

    aget v7, v7, v2

    iget-object v8, p0, Lorg/apache/lucene/util/packed/Packed32;->writeMasks:[I

    aget v8, v8, v0

    and-int/2addr v7, v8

    iget-object v8, p0, Lorg/apache/lucene/util/packed/Packed32;->shifts:[I

    add-int/lit8 v9, v0, 0x1

    aget v8, v8, v9

    shl-int v8, v3, v8

    iget-object v9, p0, Lorg/apache/lucene/util/packed/Packed32;->shifts:[I

    aget v9, v9, v0

    ushr-int/2addr v8, v9

    or-int/2addr v7, v8

    aput v7, v6, v2

    .line 209
    iget-object v6, p0, Lorg/apache/lucene/util/packed/Packed32;->blocks:[I

    add-int/lit8 v7, v2, 0x1

    iget-object v8, p0, Lorg/apache/lucene/util/packed/Packed32;->blocks:[I

    add-int/lit8 v9, v2, 0x1

    aget v8, v8, v9

    iget-object v9, p0, Lorg/apache/lucene/util/packed/Packed32;->writeMasks:[I

    add-int/lit8 v10, v0, 0x1

    aget v9, v9, v10

    and-int/2addr v8, v9

    iget-object v9, p0, Lorg/apache/lucene/util/packed/Packed32;->shifts:[I

    add-int/lit8 v10, v0, 0x2

    aget v9, v9, v10

    shl-int v9, v3, v9

    iget-object v10, p0, Lorg/apache/lucene/util/packed/Packed32;->writeMasks:[I

    add-int/lit8 v11, v0, 0x2

    aget v10, v10, v11

    and-int/2addr v9, v10

    or-int/2addr v8, v9

    aput v8, v6, v7

    .line 212
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Packed32(bitsPerValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed32;->bitsPerValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", maxPos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed32;->maxPos:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", elements.length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed32;->blocks:[I

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
