.class Lorg/apache/lucene/util/BytesRefHash$1;
.super Lorg/apache/lucene/util/SorterTemplate;
.source "BytesRefHash.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/util/BytesRefHash;->sort(Ljava/util/Comparator;)[I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final pivot:Lorg/apache/lucene/util/BytesRef;

.field private final scratch1:Lorg/apache/lucene/util/BytesRef;

.field private final scratch2:Lorg/apache/lucene/util/BytesRef;

.field final synthetic this$0:Lorg/apache/lucene/util/BytesRefHash;

.field final synthetic val$comp:Ljava/util/Comparator;

.field final synthetic val$compact:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 194
    const-class v0, Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/BytesRefHash$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/util/BytesRefHash;[ILjava/util/Comparator;)V
    .locals 1

    .prologue
    .line 194
    iput-object p1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iput-object p2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    iput-object p3, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$comp:Ljava/util/Comparator;

    invoke-direct {p0}, Lorg/apache/lucene/util/SorterTemplate;-><init>()V

    .line 193
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$1;->pivot:Lorg/apache/lucene/util/BytesRef;

    .line 194
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$1;->scratch1:Lorg/apache/lucene/util/BytesRef;

    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash$1;->scratch2:Lorg/apache/lucene/util/BytesRef;

    return-void
.end method


# virtual methods
.method protected compare(II)I
    .locals 7
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 172
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    aget v0, v2, p1

    .local v0, "ord1":I
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    aget v1, v2, p2

    .line 173
    .local v1, "ord2":I
    sget-boolean v2, Lorg/apache/lucene/util/BytesRefHash$1;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v2, v2, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v2, v2

    if-le v2, v0, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v2, v2, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v2, v2

    if-gt v2, v1, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 174
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$comp:Ljava/util/Comparator;

    iget-object v3, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash$1;->scratch1:Lorg/apache/lucene/util/BytesRef;

    iget-object v5, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v5, v5, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v5, v5, v0

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/util/ByteBlockPool;->setBytesRef(Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/util/BytesRef;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v5, p0, Lorg/apache/lucene/util/BytesRefHash$1;->scratch2:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v6, v6, v1

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/ByteBlockPool;->setBytesRef(Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    return v2
.end method

.method protected comparePivot(I)I
    .locals 6
    .param p1, "j"    # I

    .prologue
    .line 187
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    aget v0, v1, p1

    .line 188
    .local v0, "ord":I
    sget-boolean v1, Lorg/apache/lucene/util/BytesRefHash$1;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v1, v1

    if-gt v1, v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 189
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$comp:Ljava/util/Comparator;

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->pivot:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash$1;->scratch2:Lorg/apache/lucene/util/BytesRef;

    iget-object v5, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v5, v5, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v5, v5, v0

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/util/ByteBlockPool;->setBytesRef(Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/util/BytesRef;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    return v1
.end method

.method protected setPivot(I)V
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 180
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    aget v0, v1, p1

    .line 181
    .local v0, "ord":I
    sget-boolean v1, Lorg/apache/lucene/util/BytesRefHash$1;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v1, v1

    if-gt v1, v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 182
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->pivot:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, p0, Lorg/apache/lucene/util/BytesRefHash$1;->this$0:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v3, v3, v0

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/ByteBlockPool;->setBytesRef(Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/util/BytesRef;

    .line 183
    return-void
.end method

.method protected swap(II)V
    .locals 3
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 165
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    aget v0, v1, p1

    .line 166
    .local v0, "o":I
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    aget v2, v2, p2

    aput v2, v1, p1

    .line 167
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash$1;->val$compact:[I

    aput v0, v1, p2

    .line 168
    return-void
.end method
