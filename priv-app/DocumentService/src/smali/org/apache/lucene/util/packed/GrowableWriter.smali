.class public Lorg/apache/lucene/util/packed/GrowableWriter;
.super Ljava/lang/Object;
.source "GrowableWriter.java"

# interfaces
.implements Lorg/apache/lucene/util/packed/PackedInts$Mutable;


# instance fields
.field private current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

.field private currentMaxValue:J

.field private final roundFixedSize:Z


# direct methods
.method public constructor <init>(IIZ)V
    .locals 2
    .param p1, "startBitsPerValue"    # I
    .param p2, "valueCount"    # I
    .param p3, "roundFixedSize"    # Z

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-boolean p3, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->roundFixedSize:Z

    .line 35
    invoke-direct {p0, p1}, Lorg/apache/lucene/util/packed/GrowableWriter;->getSize(I)I

    move-result v0

    invoke-static {p2, v0}, Lorg/apache/lucene/util/packed/PackedInts;->getMutable(II)Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    .line 36
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->getBitsPerValue()I

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/util/packed/PackedInts;->maxValue(I)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->currentMaxValue:J

    .line 37
    return-void
.end method

.method private final getSize(I)I
    .locals 1
    .param p1, "bpv"    # I

    .prologue
    .line 40
    iget-boolean v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->roundFixedSize:Z

    if-eqz v0, :cond_0

    .line 41
    invoke-static {p1}, Lorg/apache/lucene/util/packed/PackedInts;->getNextFixedSize(I)I

    move-result p1

    .line 43
    .end local p1    # "bpv":I
    :cond_0
    return p1
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->clear()V

    .line 93
    return-void
.end method

.method public get(I)J
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->get(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getArray()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->getArray()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getBitsPerValue()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->getBitsPerValue()I

    move-result v0

    return v0
.end method

.method public getMutable()Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    return-object v0
.end method

.method public hasArray()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->hasArray()Z

    move-result v0

    return v0
.end method

.method public resize(I)Lorg/apache/lucene/util/packed/GrowableWriter;
    .locals 6
    .param p1, "newSize"    # I

    .prologue
    .line 96
    new-instance v2, Lorg/apache/lucene/util/packed/GrowableWriter;

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/GrowableWriter;->getBitsPerValue()I

    move-result v3

    iget-boolean v4, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->roundFixedSize:Z

    invoke-direct {v2, v3, p1, v4}, Lorg/apache/lucene/util/packed/GrowableWriter;-><init>(IIZ)V

    .line 97
    .local v2, "next":Lorg/apache/lucene/util/packed/GrowableWriter;
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v3

    invoke-static {v3, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 98
    .local v1, "limit":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 99
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/packed/GrowableWriter;->get(I)J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Lorg/apache/lucene/util/packed/GrowableWriter;->set(IJ)V

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_0
    return-object v2
.end method

.method public set(IJ)V
    .locals 8
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 74
    iget-wide v4, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->currentMaxValue:J

    cmp-long v4, p2, v4

    if-ltz v4, :cond_2

    .line 75
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/GrowableWriter;->getBitsPerValue()I

    move-result v0

    .line 76
    .local v0, "bpv":I
    :goto_0
    iget-wide v4, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->currentMaxValue:J

    cmp-long v4, v4, p2

    if-gtz v4, :cond_0

    iget-wide v4, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->currentMaxValue:J

    const-wide v6, 0x7fffffffffffffffL

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 77
    add-int/lit8 v0, v0, 0x1

    .line 78
    iget-wide v4, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->currentMaxValue:J

    const-wide/16 v6, 0x2

    mul-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->currentMaxValue:J

    goto :goto_0

    .line 80
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/GrowableWriter;->size()I

    move-result v3

    .line 81
    .local v3, "valueCount":I
    invoke-direct {p0, v0}, Lorg/apache/lucene/util/packed/GrowableWriter;->getSize(I)I

    move-result v4

    invoke-static {v3, v4}, Lorg/apache/lucene/util/packed/PackedInts;->getMutable(II)Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v2

    .line 82
    .local v2, "next":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_1

    .line 83
    iget-object v4, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v4, v1}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->get(I)J

    move-result-wide v4

    invoke-interface {v2, v1, v4, v5}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->set(IJ)V

    .line 82
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 85
    :cond_1
    iput-object v2, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    .line 86
    iget-object v4, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v4}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->getBitsPerValue()I

    move-result v4

    invoke-static {v4}, Lorg/apache/lucene/util/packed/PackedInts;->maxValue(I)J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->currentMaxValue:J

    .line 88
    .end local v0    # "bpv":I
    .end local v1    # "i":I
    .end local v2    # "next":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    .end local v3    # "valueCount":I
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v4, p1, p2, p3}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->set(IJ)V

    .line 89
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/util/packed/GrowableWriter;->current:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->size()I

    move-result v0

    return v0
.end method
