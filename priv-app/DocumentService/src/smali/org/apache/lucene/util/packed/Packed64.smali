.class Lorg/apache/lucene/util/packed/Packed64;
.super Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;
.source "Packed64.java"

# interfaces
.implements Lorg/apache/lucene/util/packed/PackedInts$Mutable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final BLOCK_BITS:I = 0x6

.field static final BLOCK_SIZE:I = 0x40

.field private static final ENTRY_SIZE:I = 0x41

.field private static final FAC_BITPOS:I = 0x3

.field private static final MASKS:[[J

.field static final MOD_MASK:I = 0x3f

.field private static final SHIFTS:[[I

.field private static final WRITE_MASKS:[[J


# instance fields
.field private blocks:[J

.field private maxPos:I

.field private readMasks:[J

.field private shifts:[I

.field private writeMasks:[J


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    .line 35
    const-class v8, Lorg/apache/lucene/util/packed/Packed64;

    invoke-virtual {v8}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v8

    if-nez v8, :cond_0

    const/4 v8, 0x1

    :goto_0
    sput-boolean v8, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    .line 53
    const/16 v8, 0x41

    const/16 v9, 0xc3

    filled-new-array {v8, v9}, [I

    move-result-object v8

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v9, v8}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [[I

    sput-object v8, Lorg/apache/lucene/util/packed/Packed64;->SHIFTS:[[I

    .line 56
    const/16 v8, 0x41

    const/16 v9, 0x41

    filled-new-array {v8, v9}, [I

    move-result-object v8

    sget-object v9, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v9, v8}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [[J

    sput-object v8, Lorg/apache/lucene/util/packed/Packed64;->MASKS:[[J

    .line 59
    const/4 v4, 0x1

    .local v4, "elementBits":I
    :goto_1
    const/16 v8, 0x40

    if-gt v4, v8, :cond_3

    .line 60
    const/4 v1, 0x0

    .local v1, "bitPos":I
    :goto_2
    const/16 v8, 0x40

    if-ge v1, v8, :cond_2

    .line 61
    sget-object v8, Lorg/apache/lucene/util/packed/Packed64;->SHIFTS:[[I

    aget-object v3, v8, v4

    .line 62
    .local v3, "currentShifts":[I
    mul-int/lit8 v0, v1, 0x3

    .line 63
    .local v0, "base":I
    aput v1, v3, v0

    .line 64
    add-int/lit8 v8, v0, 0x1

    rsub-int/lit8 v9, v4, 0x40

    aput v9, v3, v8

    .line 65
    rsub-int/lit8 v8, v4, 0x40

    if-gt v1, v8, :cond_1

    .line 66
    add-int/lit8 v8, v0, 0x2

    const/4 v9, 0x0

    aput v9, v3, v8

    .line 67
    sget-object v8, Lorg/apache/lucene/util/packed/Packed64;->MASKS:[[J

    aget-object v8, v8, v4

    const-wide/16 v10, 0x0

    aput-wide v10, v8, v1

    .line 60
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 35
    .end local v0    # "base":I
    .end local v1    # "bitPos":I
    .end local v3    # "currentShifts":[I
    .end local v4    # "elementBits":I
    :cond_0
    const/4 v8, 0x0

    goto :goto_0

    .line 69
    .restart local v0    # "base":I
    .restart local v1    # "bitPos":I
    .restart local v3    # "currentShifts":[I
    .restart local v4    # "elementBits":I
    :cond_1
    rsub-int/lit8 v8, v1, 0x40

    sub-int v5, v4, v8

    .line 70
    .local v5, "rBits":I
    add-int/lit8 v8, v0, 0x2

    rsub-int/lit8 v9, v5, 0x40

    aput v9, v3, v8

    .line 71
    sget-object v8, Lorg/apache/lucene/util/packed/Packed64;->MASKS:[[J

    aget-object v8, v8, v4

    const-wide/16 v10, -0x1

    shl-long/2addr v10, v5

    const-wide/16 v12, -0x1

    xor-long/2addr v10, v12

    aput-wide v10, v8, v1

    goto :goto_3

    .line 59
    .end local v0    # "base":I
    .end local v3    # "currentShifts":[I
    .end local v5    # "rBits":I
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 80
    .end local v1    # "bitPos":I
    :cond_3
    const/16 v8, 0x41

    const/16 v9, 0xc3

    filled-new-array {v8, v9}, [I

    move-result-object v8

    sget-object v9, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v9, v8}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [[J

    sput-object v8, Lorg/apache/lucene/util/packed/Packed64;->WRITE_MASKS:[[J

    .line 83
    const/4 v4, 0x1

    :goto_4
    const/16 v8, 0x40

    if-gt v4, v8, :cond_7

    .line 84
    const-wide/16 v8, -0x1

    shl-long/2addr v8, v4

    const-wide/16 v10, -0x1

    xor-long v6, v8, v10

    .line 85
    .local v6, "elementPosMask":J
    sget-object v8, Lorg/apache/lucene/util/packed/Packed64;->SHIFTS:[[I

    aget-object v3, v8, v4

    .line 86
    .restart local v3    # "currentShifts":[I
    sget-object v8, Lorg/apache/lucene/util/packed/Packed64;->WRITE_MASKS:[[J

    aget-object v2, v8, v4

    .line 87
    .local v2, "currentMasks":[J
    const/4 v1, 0x0

    .restart local v1    # "bitPos":I
    :goto_5
    const/16 v8, 0x40

    if-ge v1, v8, :cond_6

    .line 88
    mul-int/lit8 v0, v1, 0x3

    .line 89
    .restart local v0    # "base":I
    add-int/lit8 v8, v0, 0x1

    aget v8, v3, v8

    shl-long v8, v6, v8

    aget v10, v3, v0

    ushr-long/2addr v8, v10

    const-wide/16 v10, -0x1

    xor-long/2addr v8, v10

    aput-wide v8, v2, v0

    .line 92
    rsub-int/lit8 v8, v4, 0x40

    if-gt v1, v8, :cond_4

    .line 93
    add-int/lit8 v8, v0, 0x1

    const-wide/16 v10, -0x1

    aput-wide v10, v2, v8

    .line 94
    add-int/lit8 v8, v0, 0x2

    const-wide/16 v10, 0x0

    aput-wide v10, v2, v8

    .line 87
    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 96
    :cond_4
    add-int/lit8 v8, v0, 0x1

    add-int/lit8 v9, v0, 0x2

    aget v9, v3, v9

    shl-long v10, v6, v9

    const-wide/16 v12, -0x1

    xor-long/2addr v10, v12

    aput-wide v10, v2, v8

    .line 98
    add-int/lit8 v10, v0, 0x2

    add-int/lit8 v8, v0, 0x2

    aget v8, v3, v8

    if-nez v8, :cond_5

    const-wide/16 v8, 0x0

    :goto_7
    aput-wide v8, v2, v10

    goto :goto_6

    :cond_5
    const-wide/16 v8, -0x1

    goto :goto_7

    .line 83
    .end local v0    # "base":I
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 102
    .end local v1    # "bitPos":I
    .end local v2    # "currentMasks":[J
    .end local v3    # "currentShifts":[I
    .end local v6    # "elementPosMask":J
    :cond_7
    return-void
.end method

.method public constructor <init>(II)V
    .locals 4
    .param p1, "valueCount"    # I
    .param p2, "bitsPerValue"    # I

    .prologue
    .line 122
    int-to-long v0, p1

    int-to-long v2, p2

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x40

    div-long/2addr v0, v2

    const-wide/16 v2, 0x2

    add-long/2addr v0, v2

    long-to-int v0, v0

    new-array v0, v0, [J

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/lucene/util/packed/Packed64;-><init>([JII)V

    .line 124
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/DataInput;II)V
    .locals 6
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p2, "valueCount"    # I
    .param p3, "bitsPerValue"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 154
    invoke-static {p2, p3}, Lorg/apache/lucene/util/packed/Packed64;->size(II)I

    move-result v1

    .line 155
    .local v1, "size":I
    add-int/lit8 v2, v1, 0x1

    new-array v2, v2, [J

    iput-object v2, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    .line 157
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 158
    iget-object v2, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readLong()J

    move-result-wide v4

    aput-wide v4, v2, v0

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 160
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/util/packed/Packed64;->updateCached()V

    .line 161
    return-void
.end method

.method public constructor <init>([JII)V
    .locals 0
    .param p1, "blocks"    # [J
    .param p2, "valueCount"    # I
    .param p3, "bitsPerValue"    # I

    .prologue
    .line 138
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;-><init>(II)V

    .line 139
    iput-object p1, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    .line 140
    invoke-direct {p0}, Lorg/apache/lucene/util/packed/Packed64;->updateCached()V

    .line 141
    return-void
.end method

.method private static size(II)I
    .locals 8
    .param p0, "valueCount"    # I
    .param p1, "bitsPerValue"    # I

    .prologue
    const-wide/16 v6, 0x40

    .line 164
    int-to-long v2, p0

    int-to-long v4, p1

    mul-long v0, v2, v4

    .line 165
    .local v0, "totBitCount":J
    div-long v4, v0, v6

    rem-long v2, v0, v6

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    int-to-long v2, v2

    add-long/2addr v2, v4

    long-to-int v2, v2

    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private updateCached()V
    .locals 4

    .prologue
    .line 169
    sget-object v0, Lorg/apache/lucene/util/packed/Packed64;->MASKS:[[J

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/util/packed/Packed64;->readMasks:[J

    .line 170
    sget-object v0, Lorg/apache/lucene/util/packed/Packed64;->SHIFTS:[[I

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/util/packed/Packed64;->shifts:[I

    .line 171
    sget-object v0, Lorg/apache/lucene/util/packed/Packed64;->WRITE_MASKS:[[J

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/util/packed/Packed64;->writeMasks:[J

    .line 172
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    array-length v0, v0

    int-to-long v0, v0

    const-wide/16 v2, 0x40

    mul-long/2addr v0, v2

    iget v2, p0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    const-wide/16 v2, 0x2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/util/packed/Packed64;->maxPos:I

    .line 173
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 4

    .prologue
    .line 215
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 216
    return-void
.end method

.method public get(I)J
    .locals 12
    .param p1, "index"    # I

    .prologue
    .line 180
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/Packed64;->size()I

    move-result v3

    if-lt p1, v3, :cond_1

    :cond_0
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 181
    :cond_1
    int-to-long v6, p1

    iget v3, p0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    int-to-long v8, v3

    mul-long v4, v6, v8

    .line 182
    .local v4, "majorBitPos":J
    const/4 v3, 0x6

    ushr-long v6, v4, v3

    long-to-int v2, v6

    .line 183
    .local v2, "elementPos":I
    const-wide/16 v6, 0x3f

    and-long/2addr v6, v4

    long-to-int v1, v6

    .line 185
    .local v1, "bitPos":I
    mul-int/lit8 v0, v1, 0x3

    .line 186
    .local v0, "base":I
    sget-boolean v3, Lorg/apache/lucene/util/packed/Packed64;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    array-length v3, v3

    if-lt v2, v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "elementPos: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "; blocks.len: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    array-length v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 187
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    aget-wide v6, v3, v2

    iget-object v3, p0, Lorg/apache/lucene/util/packed/Packed64;->shifts:[I

    aget v3, v3, v0

    shl-long/2addr v6, v3

    iget-object v3, p0, Lorg/apache/lucene/util/packed/Packed64;->shifts:[I

    add-int/lit8 v8, v0, 0x1

    aget v3, v3, v8

    ushr-long/2addr v6, v3

    iget-object v3, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    add-int/lit8 v8, v2, 0x1

    aget-wide v8, v3, v8

    iget-object v3, p0, Lorg/apache/lucene/util/packed/Packed64;->shifts:[I

    add-int/lit8 v10, v0, 0x2

    aget v3, v3, v10

    ushr-long/2addr v8, v3

    iget-object v3, p0, Lorg/apache/lucene/util/packed/Packed64;->readMasks:[J

    aget-wide v10, v3, v1

    and-long/2addr v8, v10

    or-long/2addr v6, v8

    return-wide v6
.end method

.method public ramBytesUsed()J
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    invoke-static {v0}, Lorg/apache/lucene/util/RamUsageEstimator;->sizeOf([J)J

    move-result-wide v0

    return-wide v0
.end method

.method public set(IJ)V
    .locals 14
    .param p1, "index"    # I
    .param p2, "value"    # J

    .prologue
    .line 192
    int-to-long v6, p1

    iget v3, p0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    int-to-long v8, v3

    mul-long v4, v6, v8

    .line 193
    .local v4, "majorBitPos":J
    const/4 v3, 0x6

    ushr-long v6, v4, v3

    long-to-int v2, v6

    .line 194
    .local v2, "elementPos":I
    const-wide/16 v6, 0x3f

    and-long/2addr v6, v4

    long-to-int v1, v6

    .line 195
    .local v1, "bitPos":I
    mul-int/lit8 v0, v1, 0x3

    .line 197
    .local v0, "base":I
    iget-object v3, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    iget-object v6, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    aget-wide v6, v6, v2

    iget-object v8, p0, Lorg/apache/lucene/util/packed/Packed64;->writeMasks:[J

    aget-wide v8, v8, v0

    and-long/2addr v6, v8

    iget-object v8, p0, Lorg/apache/lucene/util/packed/Packed64;->shifts:[I

    add-int/lit8 v9, v0, 0x1

    aget v8, v8, v9

    shl-long v8, p2, v8

    iget-object v10, p0, Lorg/apache/lucene/util/packed/Packed64;->shifts:[I

    aget v10, v10, v0

    ushr-long/2addr v8, v10

    or-long/2addr v6, v8

    aput-wide v6, v3, v2

    .line 199
    iget-object v3, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    add-int/lit8 v6, v2, 0x1

    iget-object v7, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    add-int/lit8 v8, v2, 0x1

    aget-wide v8, v7, v8

    iget-object v7, p0, Lorg/apache/lucene/util/packed/Packed64;->writeMasks:[J

    add-int/lit8 v10, v0, 0x1

    aget-wide v10, v7, v10

    and-long/2addr v8, v10

    iget-object v7, p0, Lorg/apache/lucene/util/packed/Packed64;->shifts:[I

    add-int/lit8 v10, v0, 0x2

    aget v7, v7, v10

    shl-long v10, p2, v7

    iget-object v7, p0, Lorg/apache/lucene/util/packed/Packed64;->writeMasks:[J

    add-int/lit8 v12, v0, 0x2

    aget-wide v12, v7, v12

    and-long/2addr v10, v12

    or-long/2addr v8, v10

    aput-wide v8, v3, v6

    .line 201
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Packed64(bitsPerValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed64;->bitsPerValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/util/packed/Packed64;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", maxPos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/util/packed/Packed64;->maxPos:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", elements.length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/packed/Packed64;->blocks:[J

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
