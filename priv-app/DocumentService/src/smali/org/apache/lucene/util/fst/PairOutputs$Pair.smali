.class public Lorg/apache/lucene/util/fst/PairOutputs$Pair;
.super Ljava/lang/Object;
.source "PairOutputs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/PairOutputs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Pair"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "B:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final output1:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TA;"
        }
    .end annotation
.end field

.field public final output2:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TB;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;TB;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<TA;TB;>;"
    .local p1, "output1":Ljava/lang/Object;, "TA;"
    .local p2, "output2":Ljava/lang/Object;, "TB;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    .line 45
    iput-object p2, p0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    .line 46
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Lorg/apache/lucene/util/fst/PairOutputs$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;
    .param p3, "x2"    # Lorg/apache/lucene/util/fst/PairOutputs$1;

    .prologue
    .line 38
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<TA;TB;>;"
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/util/fst/PairOutputs$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<TA;TB;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 50
    if-ne p1, p0, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v1

    .line 52
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 53
    check-cast v0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .line 54
    .local v0, "pair":Lorg/apache/lucene/util/fst/PairOutputs$Pair;
    iget-object v3, p0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    iget-object v4, v0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    iget-object v4, v0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "pair":Lorg/apache/lucene/util/fst/PairOutputs$Pair;
    :cond_3
    move v1, v2

    .line 56
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 62
    .local p0, "this":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<TA;TB;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
