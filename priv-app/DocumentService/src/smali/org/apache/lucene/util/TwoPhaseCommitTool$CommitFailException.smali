.class public Lorg/apache/lucene/util/TwoPhaseCommitTool$CommitFailException;
.super Ljava/io/IOException;
.source "TwoPhaseCommitTool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/TwoPhaseCommitTool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CommitFailException"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Throwable;Lorg/apache/lucene/util/TwoPhaseCommit;)V
    .locals 2
    .param p1, "cause"    # Ljava/lang/Throwable;
    .param p2, "obj"    # Lorg/apache/lucene/util/TwoPhaseCommit;

    .prologue
    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "commit() failed on "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 89
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/TwoPhaseCommitTool$CommitFailException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 90
    return-void
.end method
