.class public abstract Lorg/apache/lucene/util/fst/FST$BytesReader;
.super Lorg/apache/lucene/store/DataInput;
.source "FST.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/FST;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BytesReader"
.end annotation


# instance fields
.field protected final bytes:[B

.field protected pos:I


# direct methods
.method protected constructor <init>([BI)V
    .locals 0
    .param p1, "bytes"    # [B
    .param p2, "pos"    # I

    .prologue
    .line 1240
    invoke-direct {p0}, Lorg/apache/lucene/store/DataInput;-><init>()V

    .line 1241
    iput-object p1, p0, Lorg/apache/lucene/util/fst/FST$BytesReader;->bytes:[B

    .line 1242
    iput p2, p0, Lorg/apache/lucene/util/fst/FST$BytesReader;->pos:I

    .line 1243
    return-void
.end method


# virtual methods
.method abstract skip(I)V
.end method

.method abstract skip(II)V
.end method
