.class public abstract Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;
.super Ljava/lang/Object;
.source "BytesRefHash.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/BytesRefHash;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BytesStartArray"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bytesUsed()Ljava/util/concurrent/atomic/AtomicLong;
.end method

.method public abstract clear()[I
.end method

.method public abstract grow()[I
.end method

.method public abstract init()[I
.end method
