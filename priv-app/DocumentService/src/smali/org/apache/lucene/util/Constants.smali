.class public final Lorg/apache/lucene/util/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final JAVA_1_1:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final JAVA_1_2:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final JAVA_1_3:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final JAVA_VENDOR:Ljava/lang/String;

.field public static final JAVA_VERSION:Ljava/lang/String;

.field public static final JRE_IS_64BIT:Z

.field public static final JRE_IS_MINIMUM_JAVA6:Z

.field public static final JRE_IS_MINIMUM_JAVA7:Z

.field public static final JVM_NAME:Ljava/lang/String;

.field public static final JVM_VENDOR:Ljava/lang/String;

.field public static final JVM_VERSION:Ljava/lang/String;

.field public static final LINUX:Z

.field public static final LUCENE_MAIN_VERSION:Ljava/lang/String;

.field public static final LUCENE_VERSION:Ljava/lang/String;

.field public static final MAC_OS_X:Z

.field public static final OS_ARCH:Ljava/lang/String;

.field public static final OS_NAME:Ljava/lang/String;

.field public static final OS_VERSION:Ljava/lang/String;

.field public static final SUN_OS:Z

.field public static final WINDOWS:Z


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    .line 31
    const-string/jumbo v12, "java.vm.vendor"

    invoke-static {v12}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lorg/apache/lucene/util/Constants;->JVM_VENDOR:Ljava/lang/String;

    .line 32
    const-string/jumbo v12, "java.vm.version"

    invoke-static {v12}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lorg/apache/lucene/util/Constants;->JVM_VERSION:Ljava/lang/String;

    .line 33
    const-string/jumbo v12, "java.vm.name"

    invoke-static {v12}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lorg/apache/lucene/util/Constants;->JVM_NAME:Ljava/lang/String;

    .line 36
    const-string/jumbo v12, "java.version"

    invoke-static {v12}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lorg/apache/lucene/util/Constants;->JAVA_VERSION:Ljava/lang/String;

    .line 41
    sget-object v12, Lorg/apache/lucene/util/Constants;->JAVA_VERSION:Ljava/lang/String;

    const-string/jumbo v13, "1.1."

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    sput-boolean v12, Lorg/apache/lucene/util/Constants;->JAVA_1_1:Z

    .line 45
    sget-object v12, Lorg/apache/lucene/util/Constants;->JAVA_VERSION:Ljava/lang/String;

    const-string/jumbo v13, "1.2."

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    sput-boolean v12, Lorg/apache/lucene/util/Constants;->JAVA_1_2:Z

    .line 49
    sget-object v12, Lorg/apache/lucene/util/Constants;->JAVA_VERSION:Ljava/lang/String;

    const-string/jumbo v13, "1.3."

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    sput-boolean v12, Lorg/apache/lucene/util/Constants;->JAVA_1_3:Z

    .line 52
    const-string/jumbo v12, "os.name"

    invoke-static {v12}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lorg/apache/lucene/util/Constants;->OS_NAME:Ljava/lang/String;

    .line 54
    sget-object v12, Lorg/apache/lucene/util/Constants;->OS_NAME:Ljava/lang/String;

    const-string/jumbo v13, "Linux"

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    sput-boolean v12, Lorg/apache/lucene/util/Constants;->LINUX:Z

    .line 56
    sget-object v12, Lorg/apache/lucene/util/Constants;->OS_NAME:Ljava/lang/String;

    const-string/jumbo v13, "Windows"

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    sput-boolean v12, Lorg/apache/lucene/util/Constants;->WINDOWS:Z

    .line 58
    sget-object v12, Lorg/apache/lucene/util/Constants;->OS_NAME:Ljava/lang/String;

    const-string/jumbo v13, "SunOS"

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    sput-boolean v12, Lorg/apache/lucene/util/Constants;->SUN_OS:Z

    .line 60
    sget-object v12, Lorg/apache/lucene/util/Constants;->OS_NAME:Ljava/lang/String;

    const-string/jumbo v13, "Mac OS X"

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    sput-boolean v12, Lorg/apache/lucene/util/Constants;->MAC_OS_X:Z

    .line 62
    const-string/jumbo v12, "os.arch"

    invoke-static {v12}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lorg/apache/lucene/util/Constants;->OS_ARCH:Ljava/lang/String;

    .line 63
    const-string/jumbo v12, "os.version"

    invoke-static {v12}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lorg/apache/lucene/util/Constants;->OS_VERSION:Ljava/lang/String;

    .line 64
    const-string/jumbo v12, "java.vendor"

    invoke-static {v12}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lorg/apache/lucene/util/Constants;->JAVA_VENDOR:Ljava/lang/String;

    .line 73
    const/4 v2, 0x0

    .line 75
    .local v2, "is64Bit":Z
    :try_start_0
    const-string/jumbo v12, "sun.misc.Unsafe"

    invoke-static {v12}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    .line 76
    .local v6, "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v12, "theUnsafe"

    invoke-virtual {v6, v12}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v7

    .line 77
    .local v7, "unsafeField":Ljava/lang/reflect/Field;
    const/4 v12, 0x1

    invoke-virtual {v7, v12}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 78
    const/4 v12, 0x0

    invoke-virtual {v7, v12}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 79
    .local v5, "unsafe":Ljava/lang/Object;
    const-string/jumbo v12, "addressSize"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Class;

    invoke-virtual {v6, v12, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v12

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-virtual {v12, v5, v13}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Number;

    invoke-virtual {v12}, Ljava/lang/Number;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 82
    .local v0, "addressSize":I
    const/16 v12, 0x8

    if-lt v0, v12, :cond_1

    const/4 v2, 0x1

    .line 95
    .end local v0    # "addressSize":I
    .end local v5    # "unsafe":Ljava/lang/Object;
    .end local v6    # "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v7    # "unsafeField":Ljava/lang/reflect/Field;
    :goto_0
    sput-boolean v2, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    .line 98
    const/4 v9, 0x1

    .line 100
    .local v9, "v6":Z
    :try_start_1
    const-class v12, Ljava/lang/String;

    const-string/jumbo v13, "isEmpty"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Class;

    invoke-virtual {v12, v13, v14}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    .line 104
    :goto_1
    sput-boolean v9, Lorg/apache/lucene/util/Constants;->JRE_IS_MINIMUM_JAVA6:Z

    .line 107
    const/4 v10, 0x1

    .line 109
    .local v10, "v7":Z
    :try_start_2
    const-class v12, Ljava/lang/Throwable;

    const-string/jumbo v13, "getSuppressed"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Class;

    invoke-virtual {v12, v13, v14}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_2

    .line 113
    :goto_2
    sput-boolean v10, Lorg/apache/lucene/util/Constants;->JRE_IS_MINIMUM_JAVA7:Z

    .line 125
    const-string/jumbo v12, "3.6"

    invoke-static {v12}, Lorg/apache/lucene/util/Constants;->ident(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    .line 129
    invoke-static {}, Lorg/apache/lucene/LucenePackage;->get()Ljava/lang/Package;

    move-result-object v4

    .line 130
    .local v4, "pkg":Ljava/lang/Package;
    if-nez v4, :cond_5

    const/4 v8, 0x0

    .line 131
    .local v8, "v":Ljava/lang/String;
    :goto_3
    if-nez v8, :cond_6

    .line 132
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "-SNAPSHOT"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 136
    :cond_0
    :goto_4
    invoke-static {v8}, Lorg/apache/lucene/util/Constants;->ident(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lorg/apache/lucene/util/Constants;->LUCENE_VERSION:Ljava/lang/String;

    .line 137
    return-void

    .line 82
    .end local v4    # "pkg":Ljava/lang/Package;
    .end local v8    # "v":Ljava/lang/String;
    .end local v9    # "v6":Z
    .end local v10    # "v7":Z
    .restart local v0    # "addressSize":I
    .restart local v5    # "unsafe":Ljava/lang/Object;
    .restart local v6    # "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v7    # "unsafeField":Ljava/lang/reflect/Field;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 83
    .end local v0    # "addressSize":I
    .end local v5    # "unsafe":Ljava/lang/Object;
    .end local v6    # "unsafeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v7    # "unsafeField":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v1

    .line 84
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v12, "sun.arch.data.model"

    invoke-static {v12}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 85
    .local v11, "x":Ljava/lang/String;
    if-eqz v11, :cond_3

    .line 86
    const-string/jumbo v12, "64"

    invoke-virtual {v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v12

    const/4 v13, -0x1

    if-eq v12, v13, :cond_2

    const/4 v2, 0x1

    :goto_5
    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_5

    .line 88
    :cond_3
    sget-object v12, Lorg/apache/lucene/util/Constants;->OS_ARCH:Ljava/lang/String;

    if-eqz v12, :cond_4

    sget-object v12, Lorg/apache/lucene/util/Constants;->OS_ARCH:Ljava/lang/String;

    const-string/jumbo v13, "64"

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v12

    const/4 v13, -0x1

    if-eq v12, v13, :cond_4

    .line 89
    const/4 v2, 0x1

    goto :goto_0

    .line 91
    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 101
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v11    # "x":Ljava/lang/String;
    .restart local v9    # "v6":Z
    :catch_1
    move-exception v3

    .line 102
    .local v3, "nsme":Ljava/lang/NoSuchMethodException;
    const/4 v9, 0x0

    goto :goto_1

    .line 110
    .end local v3    # "nsme":Ljava/lang/NoSuchMethodException;
    .restart local v10    # "v7":Z
    :catch_2
    move-exception v3

    .line 111
    .restart local v3    # "nsme":Ljava/lang/NoSuchMethodException;
    const/4 v10, 0x0

    goto :goto_2

    .line 130
    .end local v3    # "nsme":Ljava/lang/NoSuchMethodException;
    .restart local v4    # "pkg":Ljava/lang/Package;
    :cond_5
    invoke-virtual {v4}, Ljava/lang/Package;->getImplementationVersion()Ljava/lang/String;

    move-result-object v8

    goto :goto_3

    .line 133
    .restart local v8    # "v":Ljava/lang/String;
    :cond_6
    sget-object v12, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    invoke-virtual {v8, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 134
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, "-SNAPSHOT "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_4
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static ident(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 119
    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
