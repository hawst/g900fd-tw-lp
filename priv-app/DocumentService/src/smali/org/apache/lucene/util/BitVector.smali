.class public final Lorg/apache/lucene/util/BitVector;
.super Ljava/lang/Object;
.source "BitVector.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/util/Bits;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final BYTE_COUNTS:[B

.field private static CODEC:Ljava/lang/String; = null

.field private static final VERSION_CURRENT:I = 0x0

.field private static final VERSION_PRE:I = -0x1

.field private static final VERSION_START:I


# instance fields
.field private bits:[B

.field private count:I

.field private size:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/BitVector;->$assertionsDisabled:Z

    .line 153
    const/16 v0, 0x100

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/util/BitVector;->BYTE_COUNTS:[B

    .line 172
    const-string/jumbo v0, "BitVector"

    sput-object v0, Lorg/apache/lucene/util/BitVector;->CODEC:Ljava/lang/String;

    return-void

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 153
    :array_0
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x2t
        0x1t
        0x2t
        0x2t
        0x3t
        0x1t
        0x2t
        0x2t
        0x3t
        0x2t
        0x3t
        0x3t
        0x4t
        0x1t
        0x2t
        0x2t
        0x3t
        0x2t
        0x3t
        0x3t
        0x4t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x1t
        0x2t
        0x2t
        0x3t
        0x2t
        0x3t
        0x3t
        0x4t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x1t
        0x2t
        0x2t
        0x3t
        0x2t
        0x3t
        0x3t
        0x4t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x4t
        0x5t
        0x5t
        0x6t
        0x5t
        0x6t
        0x6t
        0x7t
        0x1t
        0x2t
        0x2t
        0x3t
        0x2t
        0x3t
        0x3t
        0x4t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x4t
        0x5t
        0x5t
        0x6t
        0x5t
        0x6t
        0x6t
        0x7t
        0x2t
        0x3t
        0x3t
        0x4t
        0x3t
        0x4t
        0x4t
        0x5t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x4t
        0x5t
        0x5t
        0x6t
        0x5t
        0x6t
        0x6t
        0x7t
        0x3t
        0x4t
        0x4t
        0x5t
        0x4t
        0x5t
        0x5t
        0x6t
        0x4t
        0x5t
        0x5t
        0x6t
        0x5t
        0x6t
        0x6t
        0x7t
        0x4t
        0x5t
        0x5t
        0x6t
        0x5t
        0x6t
        0x6t
        0x7t
        0x5t
        0x6t
        0x6t
        0x7t
        0x6t
        0x7t
        0x7t
        0x8t
    .end array-data
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput p1, p0, Lorg/apache/lucene/util/BitVector;->size:I

    .line 46
    iget v0, p0, Lorg/apache/lucene/util/BitVector;->size:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/BitVector;->getNumBytes(I)I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/BitVector;->count:I

    .line 48
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    .locals 6
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266
    invoke-virtual {p1, p2}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 269
    .local v1, "input":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    .line 271
    .local v0, "firstInt":I
    const/4 v3, -0x2

    if-ne v0, v3, :cond_0

    .line 273
    sget-object v3, Lorg/apache/lucene/util/BitVector;->CODEC:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v3, v4, v5}, Lorg/apache/lucene/util/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    move-result v2

    .line 274
    .local v2, "version":I
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/util/BitVector;->size:I

    .line 279
    :goto_0
    iget v3, p0, Lorg/apache/lucene/util/BitVector;->size:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 280
    invoke-direct {p0, v1}, Lorg/apache/lucene/util/BitVector;->readDgaps(Lorg/apache/lucene/store/IndexInput;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    :goto_1
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 287
    return-void

    .line 276
    .end local v2    # "version":I
    :cond_0
    const/4 v2, -0x1

    .line 277
    .restart local v2    # "version":I
    :try_start_1
    iput v0, p0, Lorg/apache/lucene/util/BitVector;->size:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 285
    .end local v0    # "firstInt":I
    .end local v2    # "version":I
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    throw v3

    .line 282
    .restart local v0    # "firstInt":I
    .restart local v2    # "version":I
    :cond_1
    :try_start_2
    invoke-direct {p0, v1}, Lorg/apache/lucene/util/BitVector;->readBits(Lorg/apache/lucene/store/IndexInput;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method constructor <init>([BI)V
    .locals 1
    .param p1, "bits"    # [B
    .param p2, "size"    # I

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    .line 52
    iput p2, p0, Lorg/apache/lucene/util/BitVector;->size:I

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/BitVector;->count:I

    .line 54
    return-void
.end method

.method private getNumBytes(I)I
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 57
    ushr-int/lit8 v0, p1, 0x3

    .line 58
    .local v0, "bytesLength":I
    and-int/lit8 v1, p1, 0x7

    if-eqz v1, :cond_0

    .line 59
    add-int/lit8 v0, v0, 0x1

    .line 61
    :cond_0
    return v0
.end method

.method private isSparse()Z
    .locals 14

    .prologue
    const/4 v8, 0x1

    .line 229
    invoke-virtual {p0}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v5

    .line 230
    .local v5, "setCount":I
    if-nez v5, :cond_1

    .line 259
    :cond_0
    :goto_0
    return v8

    .line 234
    :cond_1
    iget-object v9, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    array-length v9, v9

    div-int v0, v9, v5

    .line 238
    .local v0, "avgGapLength":I
    const/16 v9, 0x80

    if-gt v0, v9, :cond_2

    .line 239
    const/4 v4, 0x1

    .line 252
    .local v4, "expectedDGapBytes":I
    :goto_1
    add-int/lit8 v1, v4, 0x1

    .line 255
    .local v1, "bytesPerSetBit":I
    mul-int/lit8 v9, v1, 0x8

    invoke-virtual {p0}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v10

    mul-int/2addr v9, v10

    add-int/lit8 v9, v9, 0x20

    int-to-long v2, v9

    .line 258
    .local v2, "expectedBits":J
    const-wide/16 v6, 0xa

    .line 259
    .local v6, "factor":J
    const-wide/16 v10, 0xa

    mul-long/2addr v10, v2

    invoke-virtual {p0}, Lorg/apache/lucene/util/BitVector;->size()I

    move-result v9

    int-to-long v12, v9

    cmp-long v9, v10, v12

    if-ltz v9, :cond_0

    const/4 v8, 0x0

    goto :goto_0

    .line 240
    .end local v1    # "bytesPerSetBit":I
    .end local v2    # "expectedBits":J
    .end local v4    # "expectedDGapBytes":I
    .end local v6    # "factor":J
    :cond_2
    const/16 v9, 0x4000

    if-gt v0, v9, :cond_3

    .line 241
    const/4 v4, 0x2

    .restart local v4    # "expectedDGapBytes":I
    goto :goto_1

    .line 242
    .end local v4    # "expectedDGapBytes":I
    :cond_3
    const/high16 v9, 0x200000

    if-gt v0, v9, :cond_4

    .line 243
    const/4 v4, 0x3

    .restart local v4    # "expectedDGapBytes":I
    goto :goto_1

    .line 244
    .end local v4    # "expectedDGapBytes":I
    :cond_4
    const/high16 v9, 0x10000000

    if-gt v0, v9, :cond_5

    .line 245
    const/4 v4, 0x4

    .restart local v4    # "expectedDGapBytes":I
    goto :goto_1

    .line 247
    .end local v4    # "expectedDGapBytes":I
    :cond_5
    const/4 v4, 0x5

    .restart local v4    # "expectedDGapBytes":I
    goto :goto_1
.end method

.method private readBits(Lorg/apache/lucene/store/IndexInput;)V
    .locals 3
    .param p1, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 291
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/util/BitVector;->count:I

    .line 292
    iget v0, p0, Lorg/apache/lucene/util/BitVector;->size:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/BitVector;->getNumBytes(I)I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    .line 293
    iget-object v0, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    array-length v2, v2

    invoke-virtual {p1, v0, v1, v2}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 294
    return-void
.end method

.method private readDgaps(Lorg/apache/lucene/store/IndexInput;)V
    .locals 4
    .param p1, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 298
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/util/BitVector;->size:I

    .line 299
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/util/BitVector;->count:I

    .line 300
    iget v2, p0, Lorg/apache/lucene/util/BitVector;->size:I

    shr-int/lit8 v2, v2, 0x3

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    .line 301
    const/4 v0, 0x0

    .line 302
    .local v0, "last":I
    invoke-virtual {p0}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v1

    .line 303
    .local v1, "n":I
    :goto_0
    if-lez v1, :cond_0

    .line 304
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    add-int/2addr v0, v2

    .line 305
    iget-object v2, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v3

    aput-byte v3, v2, v0

    .line 306
    sget-object v2, Lorg/apache/lucene/util/BitVector;->BYTE_COUNTS:[B

    iget-object v3, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    aget-byte v3, v3, v0

    and-int/lit16 v3, v3, 0xff

    aget-byte v2, v2, v3

    sub-int/2addr v1, v2

    goto :goto_0

    .line 308
    :cond_0
    return-void
.end method

.method private writeBits(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 2
    .param p1, "output"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 203
    invoke-virtual {p0}, Lorg/apache/lucene/util/BitVector;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 204
    invoke-virtual {p0}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 205
    iget-object v0, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    iget-object v1, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    array-length v1, v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V

    .line 206
    return-void
.end method

.method private writeDgaps(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 6
    .param p1, "output"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 210
    const/4 v4, -0x1

    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 211
    invoke-virtual {p0}, Lorg/apache/lucene/util/BitVector;->size()I

    move-result v4

    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 212
    invoke-virtual {p0}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v4

    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 213
    const/4 v1, 0x0

    .line 214
    .local v1, "last":I
    invoke-virtual {p0}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v3

    .line 215
    .local v3, "n":I
    iget-object v4, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    array-length v2, v4

    .line 216
    .local v2, "m":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    if-lez v3, :cond_1

    .line 217
    iget-object v4, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    aget-byte v4, v4, v0

    if-eqz v4, :cond_0

    .line 218
    sub-int v4, v0, v1

    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 219
    iget-object v4, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    aget-byte v4, v4, v0

    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 220
    move v1, v0

    .line 221
    sget-object v4, Lorg/apache/lucene/util/BitVector;->BYTE_COUNTS:[B

    iget-object v5, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    aget-byte v5, v5, v0

    and-int/lit16 v5, v5, 0xff

    aget-byte v4, v4, v5

    sub-int/2addr v3, v4

    .line 216
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 224
    :cond_1
    return-void
.end method


# virtual methods
.method public final clear(I)V
    .locals 5
    .param p1, "bit"    # I

    .prologue
    .line 103
    iget v0, p0, Lorg/apache/lucene/util/BitVector;->size:I

    if-lt p1, v0, :cond_0

    .line 104
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 106
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    shr-int/lit8 v1, p1, 0x3

    aget-byte v2, v0, v1

    const/4 v3, 0x1

    and-int/lit8 v4, p1, 0x7

    shl-int/2addr v3, v4

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 107
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/BitVector;->count:I

    .line 108
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 66
    iget-object v2, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    array-length v2, v2

    new-array v1, v2, [B

    .line 67
    .local v1, "copyBits":[B
    iget-object v2, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    iget-object v3, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 68
    new-instance v0, Lorg/apache/lucene/util/BitVector;

    iget v2, p0, Lorg/apache/lucene/util/BitVector;->size:I

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/BitVector;-><init>([BI)V

    .line 69
    .local v0, "clone":Lorg/apache/lucene/util/BitVector;
    iget v2, p0, Lorg/apache/lucene/util/BitVector;->count:I

    iput v2, v0, Lorg/apache/lucene/util/BitVector;->count:I

    .line 70
    return-object v0
.end method

.method public final count()I
    .locals 5

    .prologue
    .line 134
    iget v3, p0, Lorg/apache/lucene/util/BitVector;->count:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 135
    const/4 v0, 0x0

    .line 136
    .local v0, "c":I
    iget-object v3, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    array-length v1, v3

    .line 137
    .local v1, "end":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 138
    sget-object v3, Lorg/apache/lucene/util/BitVector;->BYTE_COUNTS:[B

    iget-object v4, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    aget-byte v4, v4, v2

    and-int/lit16 v4, v4, 0xff

    aget-byte v3, v3, v4

    add-int/2addr v0, v3

    .line 137
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 139
    :cond_0
    iput v0, p0, Lorg/apache/lucene/util/BitVector;->count:I

    .line 141
    .end local v0    # "c":I
    .end local v1    # "end":I
    .end local v2    # "i":I
    :cond_1
    iget v3, p0, Lorg/apache/lucene/util/BitVector;->count:I

    return v3
.end method

.method public final get(I)Z
    .locals 3
    .param p1, "bit"    # I

    .prologue
    const/4 v0, 0x1

    .line 113
    sget-boolean v1, Lorg/apache/lucene/util/BitVector;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-ltz p1, :cond_0

    iget v1, p0, Lorg/apache/lucene/util/BitVector;->size:I

    if-lt p1, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "bit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is out of bounds 0.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/BitVector;->size:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 114
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    shr-int/lit8 v2, p1, 0x3

    aget-byte v1, v1, v2

    and-int/lit8 v2, p1, 0x7

    shl-int v2, v0, v2

    and-int/2addr v1, v2

    if-eqz v1, :cond_2

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getAndSet(I)Z
    .locals 6
    .param p1, "bit"    # I

    .prologue
    const/4 v3, 0x1

    .line 85
    iget v4, p0, Lorg/apache/lucene/util/BitVector;->size:I

    if-lt p1, v4, :cond_0

    .line 86
    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "bit="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/util/BitVector;->size:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 88
    :cond_0
    shr-int/lit8 v1, p1, 0x3

    .line 89
    .local v1, "pos":I
    iget-object v4, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    aget-byte v2, v4, v1

    .line 90
    .local v2, "v":I
    and-int/lit8 v4, p1, 0x7

    shl-int v0, v3, v4

    .line 91
    .local v0, "flag":I
    and-int v4, v0, v2

    if-eqz v4, :cond_1

    .line 97
    :goto_0
    return v3

    .line 94
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    or-int v4, v2, v0

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    .line 95
    iget v3, p0, Lorg/apache/lucene/util/BitVector;->count:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 96
    iget v3, p0, Lorg/apache/lucene/util/BitVector;->count:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/util/BitVector;->count:I

    .line 97
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final getRecomputedCount()I
    .locals 5

    .prologue
    .line 146
    const/4 v0, 0x0

    .line 147
    .local v0, "c":I
    iget-object v3, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    array-length v1, v3

    .line 148
    .local v1, "end":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 149
    sget-object v3, Lorg/apache/lucene/util/BitVector;->BYTE_COUNTS:[B

    iget-object v4, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    aget-byte v4, v4, v2

    and-int/lit16 v4, v4, 0xff

    aget-byte v3, v3, v4

    add-int/2addr v0, v3

    .line 148
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 150
    :cond_0
    return v0
.end method

.method public final length()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lorg/apache/lucene/util/BitVector;->size:I

    return v0
.end method

.method public final set(I)V
    .locals 5
    .param p1, "bit"    # I

    .prologue
    .line 75
    iget v0, p0, Lorg/apache/lucene/util/BitVector;->size:I

    if-lt p1, v0, :cond_0

    .line 76
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "bit="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/util/BitVector;->size:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/BitVector;->bits:[B

    shr-int/lit8 v1, p1, 0x3

    aget-byte v2, v0, v1

    const/4 v3, 0x1

    and-int/lit8 v4, p1, 0x7

    shl-int/2addr v3, v4

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 79
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/BitVector;->count:I

    .line 80
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lorg/apache/lucene/util/BitVector;->size:I

    return v0
.end method

.method public final write(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    .locals 3
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    invoke-virtual {p1, p2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    .line 189
    .local v0, "output":Lorg/apache/lucene/store/IndexOutput;
    const/4 v1, -0x2

    :try_start_0
    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 190
    sget-object v1, Lorg/apache/lucene/util/BitVector;->CODEC:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)Lorg/apache/lucene/store/DataOutput;

    .line 191
    invoke-direct {p0}, Lorg/apache/lucene/util/BitVector;->isSparse()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    invoke-direct {p0, v0}, Lorg/apache/lucene/util/BitVector;->writeDgaps(Lorg/apache/lucene/store/IndexOutput;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    :goto_0
    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 199
    return-void

    .line 194
    :cond_0
    :try_start_1
    invoke-direct {p0, v0}, Lorg/apache/lucene/util/BitVector;->writeBits(Lorg/apache/lucene/store/IndexOutput;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 197
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->close()V

    throw v1
.end method
