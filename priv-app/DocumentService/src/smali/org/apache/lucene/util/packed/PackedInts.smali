.class public Lorg/apache/lucene/util/packed/PackedInts;
.super Ljava/lang/Object;
.source "PackedInts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/packed/PackedInts$Writer;,
        Lorg/apache/lucene/util/packed/PackedInts$ReaderImpl;,
        Lorg/apache/lucene/util/packed/PackedInts$Mutable;,
        Lorg/apache/lucene/util/packed/PackedInts$Reader;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final CODEC_NAME:Ljava/lang/String; = "PackedInts"

.field private static final VERSION_CURRENT:I

.field private static final VERSION_START:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lorg/apache/lucene/util/packed/PackedInts;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/packed/PackedInts;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    return-void
.end method

.method public static bitsRequired(J)I
    .locals 6
    .param p0, "maxValue"    # J

    .prologue
    .line 253
    const-wide v0, 0x3fffffffffffffffL    # 1.9999999999999998

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    .line 254
    const/16 v0, 0x3f

    .line 258
    :goto_0
    return v0

    .line 255
    :cond_0
    const-wide v0, 0x1fffffffffffffffL

    cmp-long v0, p0, v0

    if-lez v0, :cond_1

    .line 256
    const/16 v0, 0x3e

    goto :goto_0

    .line 258
    :cond_1
    const/4 v0, 0x1

    const-wide/16 v2, 0x1

    add-long/2addr v2, p0

    long-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public static getMutable(II)Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    .locals 1
    .param p0, "valueCount"    # I
    .param p1, "bitsPerValue"    # I

    .prologue
    .line 210
    sparse-switch p1, :sswitch_data_0

    .line 220
    sget-boolean v0, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-nez v0, :cond_0

    const/16 v0, 0x20

    if-lt p1, v0, :cond_1

    .line 221
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/packed/Packed64;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/util/packed/Packed64;-><init>(II)V

    .line 223
    :goto_0
    return-object v0

    .line 212
    :sswitch_0
    new-instance v0, Lorg/apache/lucene/util/packed/Direct8;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Direct8;-><init>(I)V

    goto :goto_0

    .line 214
    :sswitch_1
    new-instance v0, Lorg/apache/lucene/util/packed/Direct16;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Direct16;-><init>(I)V

    goto :goto_0

    .line 216
    :sswitch_2
    new-instance v0, Lorg/apache/lucene/util/packed/Direct32;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Direct32;-><init>(I)V

    goto :goto_0

    .line 218
    :sswitch_3
    new-instance v0, Lorg/apache/lucene/util/packed/Direct64;

    invoke-direct {v0, p0}, Lorg/apache/lucene/util/packed/Direct64;-><init>(I)V

    goto :goto_0

    .line 223
    :cond_1
    new-instance v0, Lorg/apache/lucene/util/packed/Packed32;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/util/packed/Packed32;-><init>(II)V

    goto :goto_0

    .line 210
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x10 -> :sswitch_1
        0x20 -> :sswitch_2
        0x40 -> :sswitch_3
    .end sparse-switch
.end method

.method public static getNextFixedSize(I)I
    .locals 3
    .param p0, "bitsPerValue"    # I

    .prologue
    const/16 v2, 0x20

    const/16 v1, 0x10

    const/16 v0, 0x8

    .line 274
    if-gt p0, v0, :cond_0

    .line 281
    :goto_0
    return v0

    .line 276
    :cond_0
    if-gt p0, v1, :cond_1

    move v0, v1

    .line 277
    goto :goto_0

    .line 278
    :cond_1
    if-gt p0, v2, :cond_2

    move v0, v2

    .line 279
    goto :goto_0

    .line 281
    :cond_2
    const/16 v0, 0x40

    goto :goto_0
.end method

.method public static getReader(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .locals 5
    .param p0, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 173
    const-string/jumbo v2, "PackedInts"

    invoke-static {p0, v2, v3, v3}, Lorg/apache/lucene/util/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 174
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v0

    .line 175
    .local v0, "bitsPerValue":I
    sget-boolean v2, Lorg/apache/lucene/util/packed/PackedInts;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    if-lez v0, :cond_0

    const/16 v2, 0x40

    if-le v0, v2, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "bitsPerValue="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 176
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v1

    .line 178
    .local v1, "valueCount":I
    sparse-switch v0, :sswitch_data_0

    .line 188
    sget-boolean v2, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-nez v2, :cond_2

    const/16 v2, 0x20

    if-lt v0, v2, :cond_3

    .line 189
    :cond_2
    new-instance v2, Lorg/apache/lucene/util/packed/Packed64;

    invoke-direct {v2, p0, v1, v0}, Lorg/apache/lucene/util/packed/Packed64;-><init>(Lorg/apache/lucene/store/DataInput;II)V

    .line 191
    :goto_0
    return-object v2

    .line 180
    :sswitch_0
    new-instance v2, Lorg/apache/lucene/util/packed/Direct8;

    invoke-direct {v2, p0, v1}, Lorg/apache/lucene/util/packed/Direct8;-><init>(Lorg/apache/lucene/store/DataInput;I)V

    goto :goto_0

    .line 182
    :sswitch_1
    new-instance v2, Lorg/apache/lucene/util/packed/Direct16;

    invoke-direct {v2, p0, v1}, Lorg/apache/lucene/util/packed/Direct16;-><init>(Lorg/apache/lucene/store/DataInput;I)V

    goto :goto_0

    .line 184
    :sswitch_2
    new-instance v2, Lorg/apache/lucene/util/packed/Direct32;

    invoke-direct {v2, p0, v1}, Lorg/apache/lucene/util/packed/Direct32;-><init>(Lorg/apache/lucene/store/DataInput;I)V

    goto :goto_0

    .line 186
    :sswitch_3
    new-instance v2, Lorg/apache/lucene/util/packed/Direct64;

    invoke-direct {v2, p0, v1}, Lorg/apache/lucene/util/packed/Direct64;-><init>(Lorg/apache/lucene/store/DataInput;I)V

    goto :goto_0

    .line 191
    :cond_3
    new-instance v2, Lorg/apache/lucene/util/packed/Packed32;

    invoke-direct {v2, p0, v1, v0}, Lorg/apache/lucene/util/packed/Packed32;-><init>(Lorg/apache/lucene/store/DataInput;II)V

    goto :goto_0

    .line 178
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x10 -> :sswitch_1
        0x20 -> :sswitch_2
        0x40 -> :sswitch_3
    .end sparse-switch
.end method

.method public static getRoundedFixedSize(I)I
    .locals 1
    .param p0, "bitsPerValue"    # I

    .prologue
    .line 287
    const/16 v0, 0x3a

    if-gt p0, v0, :cond_0

    const/16 v0, 0x20

    if-ge p0, v0, :cond_1

    const/16 v0, 0x1d

    if-le p0, v0, :cond_1

    .line 288
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/util/packed/PackedInts;->getNextFixedSize(I)I

    move-result p0

    .line 290
    .end local p0    # "bitsPerValue":I
    :cond_1
    return p0
.end method

.method public static getWriter(Lorg/apache/lucene/store/DataOutput;II)Lorg/apache/lucene/util/packed/PackedInts$Writer;
    .locals 1
    .param p0, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p1, "valueCount"    # I
    .param p2, "bitsPerValue"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    new-instance v0, Lorg/apache/lucene/util/packed/PackedWriter;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/lucene/util/packed/PackedWriter;-><init>(Lorg/apache/lucene/store/DataOutput;II)V

    return-object v0
.end method

.method public static maxValue(I)J
    .locals 4
    .param p0, "bitsPerValue"    # I

    .prologue
    const-wide/16 v2, -0x1

    .line 269
    const/16 v0, 0x40

    if-ne p0, v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    :goto_0
    return-wide v0

    :cond_0
    shl-long v0, v2, p0

    xor-long/2addr v0, v2

    goto :goto_0
.end method
