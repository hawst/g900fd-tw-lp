.class public final Lorg/apache/lucene/util/BytesRefHash;
.super Ljava/lang/Object;
.source "BytesRefHash.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;,
        Lorg/apache/lucene/util/BytesRefHash$TrackingDirectBytesStartArray;,
        Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;,
        Lorg/apache/lucene/util/BytesRefHash$MaxBytesLengthExceededException;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT_CAPACITY:I = 0x10


# instance fields
.field bytesStart:[I

.field private final bytesStartArray:Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;

.field private bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

.field private count:I

.field private hashHalfSize:I

.field private hashMask:I

.field private hashSize:I

.field private lastCount:I

.field private ords:[I

.field final pool:Lorg/apache/lucene/util/ByteBlockPool;

.field private final scratch1:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lorg/apache/lucene/util/ByteBlockPool;

    new-instance v1, Lorg/apache/lucene/util/ByteBlockPool$DirectAllocator;

    invoke-direct {v1}, Lorg/apache/lucene/util/ByteBlockPool$DirectAllocator;-><init>()V

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/ByteBlockPool;-><init>(Lorg/apache/lucene/util/ByteBlockPool$Allocator;)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/BytesRefHash;-><init>(Lorg/apache/lucene/util/ByteBlockPool;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/ByteBlockPool;)V
    .locals 2
    .param p1, "pool"    # Lorg/apache/lucene/util/ByteBlockPool;

    .prologue
    const/16 v1, 0x10

    .line 77
    new-instance v0, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRefHash$DirectBytesStartArray;-><init>(I)V

    invoke-direct {p0, p1, v1, v0}, Lorg/apache/lucene/util/BytesRefHash;-><init>(Lorg/apache/lucene/util/ByteBlockPool;ILorg/apache/lucene/util/BytesRefHash$BytesStartArray;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/ByteBlockPool;ILorg/apache/lucene/util/BytesRefHash$BytesStartArray;)V
    .locals 4
    .param p1, "pool"    # Lorg/apache/lucene/util/ByteBlockPool;
    .param p2, "capacity"    # I
    .param p3, "bytesStartArray"    # Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;

    .prologue
    const/4 v1, -0x1

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->scratch1:Lorg/apache/lucene/util/BytesRef;

    .line 60
    iput v1, p0, Lorg/apache/lucene/util/BytesRefHash;->lastCount:I

    .line 85
    iput p2, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    .line 86
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    shr-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashHalfSize:I

    .line 87
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashMask:I

    .line 88
    iput-object p1, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    .line 89
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    .line 90
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 91
    iput-object p3, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStartArray:Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;

    .line 92
    invoke-virtual {p3}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;->init()[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    .line 93
    invoke-virtual {p3}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;->bytesUsed()Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    .line 94
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    iget v1, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    mul-int/lit8 v1, v1, 0x4

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 95
    return-void

    .line 93
    :cond_0
    invoke-virtual {p3}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;->bytesUsed()Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v0

    goto :goto_0
.end method

.method private equals(ILorg/apache/lucene/util/BytesRef;)Z
    .locals 3
    .param p1, "ord"    # I
    .param p2, "b"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash;->scratch1:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v2, v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/ByteBlockPool;->setBytesRef(Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/apache/lucene/util/BytesRef;->bytesEquals(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    return v0
.end method

.method private rehash(IZ)V
    .locals 22
    .param p1, "newSize"    # I
    .param p2, "hashOnData"    # Z

    .prologue
    .line 409
    add-int/lit8 v13, p1, -0x1

    .line 410
    .local v13, "newMask":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRefHash;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v18, v0

    mul-int/lit8 v19, p1, 0x4

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 411
    move/from16 v0, p1

    new-array v12, v0, [I

    .line 412
    .local v12, "newHash":[I
    const/16 v18, -0x1

    move/from16 v0, v18

    invoke-static {v12, v0}, Ljava/util/Arrays;->fill([II)V

    .line 413
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v9, v0, :cond_7

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    move-object/from16 v18, v0

    aget v6, v18, v9

    .line 415
    .local v6, "e0":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v6, v0, :cond_6

    .line 417
    if-eqz p2, :cond_1

    .line 418
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    move-object/from16 v18, v0

    aget v14, v18, v6

    .line 419
    .local v14, "off":I
    and-int/lit16 v0, v14, 0x7fff

    move/from16 v17, v0

    .line 420
    .local v17, "start":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    move-object/from16 v18, v0

    shr-int/lit8 v19, v14, 0xf

    aget-object v4, v18, v19

    .line 421
    .local v4, "bytes":[B
    const/4 v5, 0x0

    .line 424
    .local v5, "code":I
    aget-byte v18, v4, v17

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0x80

    move/from16 v18, v0

    if-nez v18, :cond_0

    .line 426
    aget-byte v11, v4, v17

    .line 427
    .local v11, "len":I
    add-int/lit8 v15, v17, 0x1

    .line 433
    .local v15, "pos":I
    :goto_1
    add-int v7, v15, v11

    .local v7, "endPos":I
    move/from16 v16, v15

    .line 434
    .end local v15    # "pos":I
    .local v16, "pos":I
    :goto_2
    move/from16 v0, v16

    if-ge v0, v7, :cond_2

    .line 435
    mul-int/lit8 v18, v5, 0x1f

    add-int/lit8 v15, v16, 0x1

    .end local v16    # "pos":I
    .restart local v15    # "pos":I
    aget-byte v19, v4, v16

    add-int v5, v18, v19

    move/from16 v16, v15

    .end local v15    # "pos":I
    .restart local v16    # "pos":I
    goto :goto_2

    .line 429
    .end local v7    # "endPos":I
    .end local v11    # "len":I
    .end local v16    # "pos":I
    :cond_0
    aget-byte v18, v4, v17

    and-int/lit8 v18, v18, 0x7f

    add-int/lit8 v19, v17, 0x1

    aget-byte v19, v4, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    shl-int/lit8 v19, v19, 0x7

    add-int v11, v18, v19

    .line 430
    .restart local v11    # "len":I
    add-int/lit8 v15, v17, 0x2

    .restart local v15    # "pos":I
    goto :goto_1

    .line 438
    .end local v4    # "bytes":[B
    .end local v5    # "code":I
    .end local v11    # "len":I
    .end local v14    # "off":I
    .end local v15    # "pos":I
    .end local v17    # "start":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    move-object/from16 v18, v0

    aget v5, v18, v6

    .line 441
    .restart local v5    # "code":I
    :cond_2
    and-int v8, v5, v13

    .line 442
    .local v8, "hashPos":I
    sget-boolean v18, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v18, :cond_3

    if-gez v8, :cond_3

    new-instance v18, Ljava/lang/AssertionError;

    invoke-direct/range {v18 .. v18}, Ljava/lang/AssertionError;-><init>()V

    throw v18

    .line 443
    :cond_3
    aget v18, v12, v8

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_5

    .line 444
    shr-int/lit8 v18, v5, 0x8

    add-int v18, v18, v5

    or-int/lit8 v10, v18, 0x1

    .line 446
    .local v10, "inc":I
    :cond_4
    add-int/2addr v5, v10

    .line 447
    and-int v8, v5, v13

    .line 448
    aget v18, v12, v8

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 450
    .end local v10    # "inc":I
    :cond_5
    aput v6, v12, v8

    .line 413
    .end local v5    # "code":I
    .end local v8    # "hashPos":I
    :cond_6
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 454
    .end local v6    # "e0":I
    :cond_7
    move-object/from16 v0, p0

    iput v13, v0, Lorg/apache/lucene/util/BytesRefHash;->hashMask:I

    .line 455
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRefHash;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    neg-int v0, v0

    move/from16 v19, v0

    mul-int/lit8 v19, v19, 0x4

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 456
    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    .line 457
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    .line 458
    div-int/lit8 v18, p1, 0x2

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/util/BytesRefHash;->hashHalfSize:I

    .line 459
    return-void
.end method

.method private shrink(I)Z
    .locals 4
    .param p1, "targetSize"    # I

    .prologue
    .line 206
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    .line 207
    .local v0, "newSize":I
    :goto_0
    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    div-int/lit8 v1, v0, 0x4

    if-le v1, p1, :cond_0

    .line 208
    div-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 210
    :cond_0
    iget v1, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    if-eq v0, v1, :cond_1

    .line 211
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    iget v2, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    sub-int/2addr v2, v0

    neg-int v2, v2

    mul-int/lit8 v2, v2, 0x4

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 213
    iput v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    .line 214
    iget v1, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    .line 215
    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    const/4 v2, -0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    .line 216
    div-int/lit8 v1, v0, 0x2

    iput v1, p0, Lorg/apache/lucene/util/BytesRefHash;->hashHalfSize:I

    .line 217
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lorg/apache/lucene/util/BytesRefHash;->hashMask:I

    .line 218
    const/4 v1, 0x1

    .line 220
    :goto_1
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public add(Lorg/apache/lucene/util/BytesRef;)I
    .locals 1
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 270
    invoke-virtual {p1}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/util/BytesRefHash;->add(Lorg/apache/lucene/util/BytesRef;I)I

    move-result v0

    return v0
.end method

.method public add(Lorg/apache/lucene/util/BytesRef;I)I
    .locals 11
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "code"    # I

    .prologue
    const v8, 0x8000

    const/4 v10, -0x1

    .line 301
    sget-boolean v7, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v7, :cond_0

    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    if-nez v7, :cond_0

    new-instance v7, Ljava/lang/AssertionError;

    const-string/jumbo v8, "Bytesstart is null - not initialized"

    invoke-direct {v7, v8}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v7

    .line 302
    :cond_0
    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 304
    .local v6, "length":I
    iget v7, p0, Lorg/apache/lucene/util/BytesRefHash;->hashMask:I

    and-int v3, p2, v7

    .line 305
    .local v3, "hashPos":I
    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    aget v2, v7, v3

    .line 306
    .local v2, "e":I
    if-eq v2, v10, :cond_2

    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/util/BytesRefHash;->equals(ILorg/apache/lucene/util/BytesRef;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 309
    shr-int/lit8 v7, p2, 0x8

    add-int/2addr v7, p2

    or-int/lit8 v4, v7, 0x1

    .line 311
    .local v4, "inc":I
    :cond_1
    add-int/2addr p2, v4

    .line 312
    iget v7, p0, Lorg/apache/lucene/util/BytesRefHash;->hashMask:I

    and-int v3, p2, v7

    .line 313
    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    aget v2, v7, v3

    .line 314
    if-eq v2, v10, :cond_2

    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/util/BytesRefHash;->equals(ILorg/apache/lucene/util/BytesRef;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 317
    .end local v4    # "inc":I
    :cond_2
    if-ne v2, v10, :cond_a

    .line 319
    iget v7, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v5, v7, 0x2

    .line 320
    .local v5, "len2":I
    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v7, v7, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/2addr v7, v5

    if-le v7, v8, :cond_4

    .line 321
    if-le v5, v8, :cond_3

    .line 322
    new-instance v7, Lorg/apache/lucene/util/BytesRefHash$MaxBytesLengthExceededException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "bytes can be at most 32766 in length; got "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lorg/apache/lucene/util/BytesRefHash$MaxBytesLengthExceededException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 325
    :cond_3
    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    invoke-virtual {v7}, Lorg/apache/lucene/util/ByteBlockPool;->nextBuffer()V

    .line 327
    :cond_4
    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v0, v7, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    .line 328
    .local v0, "buffer":[B
    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v1, v7, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 329
    .local v1, "bufferUpto":I
    iget v7, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iget-object v8, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v8, v8

    if-lt v7, v8, :cond_5

    .line 330
    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStartArray:Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;

    invoke-virtual {v7}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;->grow()[I

    move-result-object v7

    iput-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    .line 332
    sget-boolean v7, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v7, :cond_5

    iget v7, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iget-object v8, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v8, v8

    add-int/lit8 v8, v8, 0x1

    if-lt v7, v8, :cond_5

    new-instance v7, Ljava/lang/AssertionError;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "count: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " len: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v9, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v7

    .line 334
    :cond_5
    iget v2, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    .end local v2    # "e":I
    add-int/lit8 v7, v2, 0x1

    iput v7, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    .line 336
    .restart local v2    # "e":I
    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    iget-object v8, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v8, v8, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    add-int/2addr v8, v1

    aput v8, v7, v2

    .line 342
    const/16 v7, 0x80

    if-ge v6, v7, :cond_7

    .line 344
    int-to-byte v7, v6

    aput-byte v7, v0, v1

    .line 345
    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v8, v7, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/lit8 v9, v6, 0x1

    add-int/2addr v8, v9

    iput v8, v7, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 346
    sget-boolean v7, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v7, :cond_6

    if-gez v6, :cond_6

    new-instance v7, Ljava/lang/AssertionError;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Length must be positive: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v7

    .line 347
    :cond_6
    iget-object v7, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v8, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/lit8 v9, v1, 0x1

    invoke-static {v7, v8, v0, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 357
    :goto_0
    sget-boolean v7, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v7, :cond_8

    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    aget v7, v7, v3

    if-eq v7, v10, :cond_8

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 351
    :cond_7
    and-int/lit8 v7, v6, 0x7f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v0, v1

    .line 352
    add-int/lit8 v7, v1, 0x1

    shr-int/lit8 v8, v6, 0x7

    and-int/lit16 v8, v8, 0xff

    int-to-byte v8, v8

    aput-byte v8, v0, v7

    .line 353
    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v8, v7, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    add-int/lit8 v9, v6, 0x2

    add-int/2addr v8, v9

    iput v8, v7, Lorg/apache/lucene/util/ByteBlockPool;->byteUpto:I

    .line 354
    iget-object v7, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v8, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/lit8 v9, v1, 0x2

    invoke-static {v7, v8, v0, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 358
    :cond_8
    iget-object v7, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    aput v2, v7, v3

    .line 360
    iget v7, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iget v8, p0, Lorg/apache/lucene/util/BytesRefHash;->hashHalfSize:I

    if-ne v7, v8, :cond_9

    .line 361
    iget v7, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    mul-int/lit8 v7, v7, 0x2

    const/4 v8, 0x1

    invoke-direct {p0, v7, v8}, Lorg/apache/lucene/util/BytesRefHash;->rehash(IZ)V

    :cond_9
    move v7, v2

    .line 365
    .end local v0    # "buffer":[B
    .end local v1    # "bufferUpto":I
    .end local v5    # "len2":I
    :goto_1
    return v7

    :cond_a
    add-int/lit8 v7, v2, 0x1

    neg-int v7, v7

    goto :goto_1
.end method

.method public addByPoolOffset(I)I
    .locals 7
    .param p1, "offset"    # I

    .prologue
    const/4 v6, -0x1

    .line 369
    sget-boolean v4, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    const-string/jumbo v5, "Bytesstart is null - not initialized"

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 371
    :cond_0
    move v0, p1

    .line 372
    .local v0, "code":I
    iget v4, p0, Lorg/apache/lucene/util/BytesRefHash;->hashMask:I

    and-int v2, p1, v4

    .line 373
    .local v2, "hashPos":I
    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    aget v1, v4, v2

    .line 374
    .local v1, "e":I
    if-eq v1, v6, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v4, v4, v1

    if-eq v4, p1, :cond_2

    .line 377
    shr-int/lit8 v4, v0, 0x8

    add-int/2addr v4, v0

    or-int/lit8 v3, v4, 0x1

    .line 379
    .local v3, "inc":I
    :cond_1
    add-int/2addr v0, v3

    .line 380
    iget v4, p0, Lorg/apache/lucene/util/BytesRefHash;->hashMask:I

    and-int v2, v0, v4

    .line 381
    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    aget v1, v4, v2

    .line 382
    if-eq v1, v6, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v4, v4, v1

    if-ne v4, p1, :cond_1

    .line 384
    .end local v3    # "inc":I
    :cond_2
    if-ne v1, v6, :cond_6

    .line 386
    iget v4, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iget-object v5, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v5, v5

    if-lt v4, v5, :cond_3

    .line 387
    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStartArray:Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;

    invoke-virtual {v4}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;->grow()[I

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    .line 389
    sget-boolean v4, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    iget v4, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iget-object v5, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v5, v5

    add-int/lit8 v5, v5, 0x1

    if-lt v4, v5, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "count: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " len: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 391
    :cond_3
    iget v1, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    .end local v1    # "e":I
    add-int/lit8 v4, v1, 0x1

    iput v4, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    .line 392
    .restart local v1    # "e":I
    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aput p1, v4, v1

    .line 393
    sget-boolean v4, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    aget v4, v4, v2

    if-eq v4, v6, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 394
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    aput v1, v4, v2

    .line 396
    iget v4, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iget v5, p0, Lorg/apache/lucene/util/BytesRefHash;->hashHalfSize:I

    if-ne v4, v5, :cond_5

    .line 397
    iget v4, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    mul-int/lit8 v4, v4, 0x2

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lorg/apache/lucene/util/BytesRefHash;->rehash(IZ)V

    :cond_5
    move v4, v1

    .line 401
    :goto_0
    return v4

    :cond_6
    add-int/lit8 v4, v1, 0x1

    neg-int v4, v4

    goto :goto_0
.end method

.method public byteStart(I)I
    .locals 2
    .param p1, "ord"    # I

    .prologue
    .line 487
    sget-boolean v0, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Bytesstart is null - not initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 488
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-ltz p1, :cond_1

    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    if-lt p1, v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(I)V

    throw v0

    .line 489
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v0, v0, p1

    return v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 242
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/BytesRefHash;->clear(Z)V

    .line 243
    return-void
.end method

.method public clear(Z)V
    .locals 2
    .param p1, "resetPool"    # Z

    .prologue
    const/4 v1, -0x1

    .line 228
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iput v0, p0, Lorg/apache/lucene/util/BytesRefHash;->lastCount:I

    .line 229
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    .line 230
    if-eqz p1, :cond_0

    .line 231
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    invoke-virtual {v0}, Lorg/apache/lucene/util/ByteBlockPool;->dropBuffersAndReset()V

    .line 233
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStartArray:Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;

    invoke-virtual {v0}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;->clear()[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    .line 234
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->lastCount:I

    if-eq v0, v1, :cond_1

    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->lastCount:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/util/BytesRefHash;->shrink(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239
    :goto_0
    return-void

    .line 238
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    goto :goto_0
.end method

.method public close()V
    .locals 4

    .prologue
    .line 249
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/BytesRefHash;->clear(Z)V

    .line 250
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    .line 251
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    iget v1, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    neg-int v1, v1

    mul-int/lit8 v1, v1, 0x4

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 253
    return-void
.end method

.method public compact()[I
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 133
    sget-boolean v2, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    const-string/jumbo v3, "Bytesstart is null - not initialized"

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 134
    :cond_0
    const/4 v1, 0x0

    .line 135
    .local v1, "upto":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    if-ge v0, v2, :cond_3

    .line 136
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    aget v2, v2, v0

    if-eq v2, v4, :cond_2

    .line 137
    if-ge v1, v0, :cond_1

    .line 138
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    iget-object v3, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    aget v3, v3, v0

    aput v3, v2, v1

    .line 139
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    aput v4, v2, v0

    .line 141
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 135
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 145
    :cond_3
    sget-boolean v2, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    iget v2, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    if-eq v1, v2, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 146
    :cond_4
    iget v2, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    iput v2, p0, Lorg/apache/lucene/util/BytesRefHash;->lastCount:I

    .line 147
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    return-object v2
.end method

.method public get(ILorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;
    .locals 3
    .param p1, "ord"    # I
    .param p2, "ref"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 119
    sget-boolean v0, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "bytesStart is null - not initialized"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 120
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/BytesRefHash;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ord exceeds byteStart len: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 121
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v1, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    aget v1, v1, p1

    invoke-virtual {v0, p2, v1}, Lorg/apache/lucene/util/ByteBlockPool;->setBytesRef(Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public reinit()V
    .locals 4

    .prologue
    .line 467
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    if-nez v0, :cond_0

    .line 468
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStartArray:Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;

    invoke-virtual {v0}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;->init()[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesStart:[I

    .line 471
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    if-nez v0, :cond_1

    .line 472
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->ords:[I

    .line 473
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRefHash;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    iget v1, p0, Lorg/apache/lucene/util/BytesRefHash;->hashSize:I

    mul-int/lit8 v1, v1, 0x4

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 475
    :cond_1
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    return v0
.end method

.method public sort(Ljava/util/Comparator;)[I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;)[I"
        }
    .end annotation

    .prologue
    .line 161
    .local p1, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/BytesRefHash;->compact()[I

    move-result-object v0

    .line 162
    .local v0, "compact":[I
    new-instance v1, Lorg/apache/lucene/util/BytesRefHash$1;

    invoke-direct {v1, p0, v0, p1}, Lorg/apache/lucene/util/BytesRefHash$1;-><init>(Lorg/apache/lucene/util/BytesRefHash;[ILjava/util/Comparator;)V

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/util/BytesRefHash;->count:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/BytesRefHash$1;->quickSort(II)V

    .line 196
    return-object v0
.end method
