.class public Lorg/apache/lucene/util/ScorerDocQueue;
.super Ljava/lang/Object;
.source "ScorerDocQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;
    }
.end annotation


# instance fields
.field private final heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

.field private final maxSize:I

.field private size:I

.field private topHSD:Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;


# direct methods
.method public constructor <init>(I)V
    .locals 3
    .param p1, "maxSize"    # I

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    .line 58
    add-int/lit8 v0, p1, 0x1

    .line 59
    .local v0, "heapSize":I
    new-array v1, v0, [Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iput-object v1, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    .line 60
    iput p1, p0, Lorg/apache/lucene/util/ScorerDocQueue;->maxSize:I

    .line 61
    iget-object v1, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iput-object v1, p0, Lorg/apache/lucene/util/ScorerDocQueue;->topHSD:Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    .line 62
    return-void
.end method

.method private checkAdjustElsePop(Z)Z
    .locals 4
    .param p1, "cond"    # Z

    .prologue
    .line 128
    if-eqz p1, :cond_0

    .line 129
    iget-object v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->topHSD:Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget-object v1, p0, Lorg/apache/lucene/util/ScorerDocQueue;->topHSD:Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget-object v1, v1, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    iput v1, v0, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->doc:I

    .line 135
    :goto_0
    invoke-direct {p0}, Lorg/apache/lucene/util/ScorerDocQueue;->downHeap()V

    .line 136
    return p1

    .line 131
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget v3, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    .line 132
    iget-object v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget v1, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 133
    iget v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    goto :goto_0
.end method

.method private final downHeap()V
    .locals 6

    .prologue
    .line 200
    const/4 v0, 0x1

    .line 201
    .local v0, "i":I
    iget-object v4, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    aget-object v3, v4, v0

    .line 202
    .local v3, "node":Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;
    shl-int/lit8 v1, v0, 0x1

    .line 203
    .local v1, "j":I
    add-int/lit8 v2, v1, 0x1

    .line 204
    .local v2, "k":I
    iget v4, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    if-gt v2, v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    aget-object v4, v4, v2

    iget v4, v4, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->doc:I

    iget-object v5, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    aget-object v5, v5, v1

    iget v5, v5, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->doc:I

    if-ge v4, v5, :cond_0

    .line 205
    move v1, v2

    .line 207
    :cond_0
    :goto_0
    iget v4, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    if-gt v1, v4, :cond_1

    iget-object v4, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    aget-object v4, v4, v1

    iget v4, v4, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->doc:I

    iget v5, v3, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->doc:I

    if-ge v4, v5, :cond_1

    .line 208
    iget-object v4, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget-object v5, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    aget-object v5, v5, v1

    aput-object v5, v4, v0

    .line 209
    move v0, v1

    .line 210
    shl-int/lit8 v1, v0, 0x1

    .line 211
    add-int/lit8 v2, v1, 0x1

    .line 212
    iget v4, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    if-gt v2, v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    aget-object v4, v4, v2

    iget v4, v4, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->doc:I

    iget-object v5, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    aget-object v5, v5, v1

    iget v5, v5, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->doc:I

    if-ge v4, v5, :cond_0

    .line 213
    move v1, v2

    goto :goto_0

    .line 216
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    aput-object v3, v4, v0

    .line 217
    iget-object v4, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iput-object v4, p0, Lorg/apache/lucene/util/ScorerDocQueue;->topHSD:Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    .line 218
    return-void
.end method

.method private final popNoResult()V
    .locals 4

    .prologue
    .line 154
    iget-object v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget v3, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    .line 155
    iget-object v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget v1, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 156
    iget v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    .line 157
    invoke-direct {p0}, Lorg/apache/lucene/util/ScorerDocQueue;->downHeap()V

    .line 158
    return-void
.end method

.method private final upHeap()V
    .locals 5

    .prologue
    .line 187
    iget v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    .line 188
    .local v0, "i":I
    iget-object v3, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    aget-object v2, v3, v0

    .line 189
    .local v2, "node":Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;
    ushr-int/lit8 v1, v0, 0x1

    .line 190
    .local v1, "j":I
    :goto_0
    if-lez v1, :cond_0

    iget v3, v2, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->doc:I

    iget-object v4, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    aget-object v4, v4, v1

    iget v4, v4, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->doc:I

    if-ge v3, v4, :cond_0

    .line 191
    iget-object v3, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget-object v4, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    aget-object v4, v4, v1

    aput-object v4, v3, v0

    .line 192
    move v0, v1

    .line 193
    ushr-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 195
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    aput-object v2, v3, v0

    .line 196
    iget-object v3, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    iput-object v3, p0, Lorg/apache/lucene/util/ScorerDocQueue;->topHSD:Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    .line 197
    return-void
.end method


# virtual methods
.method public final adjustTop()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->topHSD:Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    invoke-virtual {v0}, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->adjust()V

    .line 170
    invoke-direct {p0}, Lorg/apache/lucene/util/ScorerDocQueue;->downHeap()V

    .line 171
    return-void
.end method

.method public final clear()V
    .locals 3

    .prologue
    .line 180
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    if-gt v0, v1, :cond_0

    .line 181
    iget-object v1, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 180
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 183
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    .line 184
    return-void
.end method

.method public insert(Lorg/apache/lucene/search/Scorer;)Z
    .locals 4
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    const/4 v1, 0x1

    .line 82
    iget v2, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    iget v3, p0, Lorg/apache/lucene/util/ScorerDocQueue;->maxSize:I

    if-ge v2, v3, :cond_0

    .line 83
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/ScorerDocQueue;->put(Lorg/apache/lucene/search/Scorer;)V

    .line 92
    :goto_0
    return v1

    .line 86
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    .line 87
    .local v0, "docNr":I
    iget v2, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    if-lez v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/util/ScorerDocQueue;->topHSD:Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget v2, v2, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->doc:I

    if-lt v0, v2, :cond_1

    .line 88
    iget-object v2, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    new-instance v3, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    invoke-direct {v3, p0, p1, v0}, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;-><init>(Lorg/apache/lucene/util/ScorerDocQueue;Lorg/apache/lucene/search/Scorer;I)V

    aput-object v3, v2, v1

    .line 89
    invoke-direct {p0}, Lorg/apache/lucene/util/ScorerDocQueue;->downHeap()V

    goto :goto_0

    .line 92
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final pop()Lorg/apache/lucene/search/Scorer;
    .locals 2

    .prologue
    .line 145
    iget-object v1, p0, Lorg/apache/lucene/util/ScorerDocQueue;->topHSD:Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget-object v0, v1, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 146
    .local v0, "result":Lorg/apache/lucene/search/Scorer;
    invoke-direct {p0}, Lorg/apache/lucene/util/ScorerDocQueue;->popNoResult()V

    .line 147
    return-object v0
.end method

.method public final put(Lorg/apache/lucene/search/Scorer;)V
    .locals 3
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 70
    iget v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    .line 71
    iget-object v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->heap:[Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget v1, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    new-instance v2, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    invoke-direct {v2, p0, p1}, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;-><init>(Lorg/apache/lucene/util/ScorerDocQueue;Lorg/apache/lucene/search/Scorer;)V

    aput-object v2, v0, v1

    .line 72
    invoke-direct {p0}, Lorg/apache/lucene/util/ScorerDocQueue;->upHeap()V

    .line 73
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->size:I

    return v0
.end method

.method public final top()Lorg/apache/lucene/search/Scorer;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->topHSD:Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget-object v0, v0, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->scorer:Lorg/apache/lucene/search/Scorer;

    return-object v0
.end method

.method public final topDoc()I
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->topHSD:Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget v0, v0, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->doc:I

    return v0
.end method

.method public final topNextAndAdjustElsePop()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->topHSD:Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget-object v0, v0, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lorg/apache/lucene/util/ScorerDocQueue;->checkAdjustElsePop(Z)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final topScore()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->topHSD:Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget-object v0, v0, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    return v0
.end method

.method public final topSkipToAndAdjustElsePop(I)Z
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lorg/apache/lucene/util/ScorerDocQueue;->topHSD:Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;

    iget-object v0, v0, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lorg/apache/lucene/util/ScorerDocQueue;->checkAdjustElsePop(Z)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
