.class public final Lorg/apache/lucene/util/PagedBytes$Reader;
.super Ljava/lang/Object;
.source "PagedBytes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/PagedBytes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Reader"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final blockBits:I

.field private final blockEnds:[I

.field private final blockMask:I

.field private final blockSize:I

.field private final blocks:[[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lorg/apache/lucene/util/PagedBytes;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/PagedBytes$Reader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/PagedBytes;)V
    .locals 3
    .param p1, "pagedBytes"    # Lorg/apache/lucene/util/PagedBytes;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    # getter for: Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;
    invoke-static {p1}, Lorg/apache/lucene/util/PagedBytes;->access$000(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [[B

    iput-object v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    .line 61
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 62
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;
    invoke-static {p1}, Lorg/apache/lucene/util/PagedBytes;->access$000(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    aput-object v1, v2, v0

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    array-length v1, v1

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockEnds:[I

    .line 65
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockEnds:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 66
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockEnds:[I

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockEnd:Ljava/util/List;
    invoke-static {p1}, Lorg/apache/lucene/util/PagedBytes;->access$100(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, v2, v0

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 68
    :cond_1
    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockBits:I
    invoke-static {p1}, Lorg/apache/lucene/util/PagedBytes;->access$200(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockBits:I

    .line 69
    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockMask:I
    invoke-static {p1}, Lorg/apache/lucene/util/PagedBytes;->access$300(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockMask:I

    .line 70
    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {p1}, Lorg/apache/lucene/util/PagedBytes;->access$400(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    .line 71
    return-void
.end method


# virtual methods
.method public fill(Lorg/apache/lucene/util/BytesRef;J)Lorg/apache/lucene/util/BytesRef;
    .locals 6
    .param p1, "b"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "start"    # J

    .prologue
    .line 114
    iget v3, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockBits:I

    shr-long v4, p2, v3

    long-to-int v1, v4

    .line 115
    .local v1, "index":I
    iget v3, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockMask:I

    int-to-long v4, v3

    and-long/2addr v4, p2

    long-to-int v2, v4

    .line 116
    .local v2, "offset":I
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    aget-object v0, v3, v1

    iput-object v0, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 118
    .local v0, "block":[B
    aget-byte v3, v0, v2

    and-int/lit16 v3, v3, 0x80

    if-nez v3, :cond_1

    .line 119
    aget-byte v3, v0, v2

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 120
    add-int/lit8 v3, v2, 0x1

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 126
    :cond_0
    return-object p1

    .line 122
    :cond_1
    aget-byte v3, v0, v2

    and-int/lit8 v3, v3, 0x7f

    shl-int/lit8 v3, v3, 0x8

    add-int/lit8 v4, v2, 0x1

    aget-byte v4, v0, v4

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 123
    add-int/lit8 v3, v2, 0x2

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 124
    sget-boolean v3, Lorg/apache/lucene/util/PagedBytes$Reader;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-gtz v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
.end method

.method public fillAndGetIndex(Lorg/apache/lucene/util/BytesRef;J)I
    .locals 6
    .param p1, "b"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "start"    # J

    .prologue
    .line 140
    iget v3, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockBits:I

    shr-long v4, p2, v3

    long-to-int v1, v4

    .line 141
    .local v1, "index":I
    iget v3, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockMask:I

    int-to-long v4, v3

    and-long/2addr v4, p2

    long-to-int v2, v4

    .line 142
    .local v2, "offset":I
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    aget-object v0, v3, v1

    iput-object v0, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 144
    .local v0, "block":[B
    aget-byte v3, v0, v2

    and-int/lit16 v3, v3, 0x80

    if-nez v3, :cond_1

    .line 145
    aget-byte v3, v0, v2

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 146
    add-int/lit8 v3, v2, 0x1

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 152
    :cond_0
    return v1

    .line 148
    :cond_1
    aget-byte v3, v0, v2

    and-int/lit8 v3, v3, 0x7f

    shl-int/lit8 v3, v3, 0x8

    add-int/lit8 v4, v2, 0x1

    aget-byte v4, v0, v4

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 149
    add-int/lit8 v3, v2, 0x2

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 150
    sget-boolean v3, Lorg/apache/lucene/util/PagedBytes$Reader;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-gtz v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
.end method

.method public fillAndGetStart(Lorg/apache/lucene/util/BytesRef;J)J
    .locals 8
    .param p1, "b"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "start"    # J

    .prologue
    .line 170
    iget v3, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockBits:I

    shr-long v4, p2, v3

    long-to-int v1, v4

    .line 171
    .local v1, "index":I
    iget v3, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockMask:I

    int-to-long v4, v3

    and-long/2addr v4, p2

    long-to-int v2, v4

    .line 172
    .local v2, "offset":I
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    aget-object v0, v3, v1

    iput-object v0, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 174
    .local v0, "block":[B
    aget-byte v3, v0, v2

    and-int/lit16 v3, v3, 0x80

    if-nez v3, :cond_1

    .line 175
    aget-byte v3, v0, v2

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 176
    add-int/lit8 v3, v2, 0x1

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 177
    const-wide/16 v4, 0x1

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    add-long/2addr p2, v4

    .line 184
    :cond_0
    return-wide p2

    .line 179
    :cond_1
    aget-byte v3, v0, v2

    and-int/lit8 v3, v3, 0x7f

    shl-int/lit8 v3, v3, 0x8

    add-int/lit8 v4, v2, 0x1

    aget-byte v4, v0, v4

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 180
    add-int/lit8 v3, v2, 0x2

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 181
    const-wide/16 v4, 0x2

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    add-long/2addr p2, v4

    .line 182
    sget-boolean v3, Lorg/apache/lucene/util/PagedBytes$Reader;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-gtz v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
.end method

.method public fillSlice(Lorg/apache/lucene/util/BytesRef;JI)Lorg/apache/lucene/util/BytesRef;
    .locals 8
    .param p1, "b"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "start"    # J
    .param p4, "length"    # I

    .prologue
    const/4 v6, 0x0

    .line 83
    sget-boolean v2, Lorg/apache/lucene/util/PagedBytes$Reader;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez p4, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "length="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 84
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/util/PagedBytes$Reader;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    add-int/lit8 v2, v2, 0x1

    if-le p4, v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 85
    :cond_1
    iget v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockBits:I

    shr-long v2, p2, v2

    long-to-int v0, v2

    .line 86
    .local v0, "index":I
    iget v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockMask:I

    int-to-long v2, v2

    and-long/2addr v2, p2

    long-to-int v1, v2

    .line 87
    .local v1, "offset":I
    iput p4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 88
    iget v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    sub-int/2addr v2, v1

    if-lt v2, p4, :cond_2

    .line 90
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    aget-object v2, v2, v0

    iput-object v2, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 91
    iput v1, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 99
    :goto_0
    return-object p1

    .line 94
    :cond_2
    new-array v2, p4, [B

    iput-object v2, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 95
    iput v6, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 96
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    aget-object v2, v2, v0

    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    sub-int/2addr v4, v1

    invoke-static {v2, v1, v3, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 97
    iget-object v2, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    add-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    sub-int/2addr v4, v1

    iget v5, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    sub-int/2addr v5, v1

    sub-int v5, p4, v5

    invoke-static {v2, v6, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public fillSliceWithPrefix(Lorg/apache/lucene/util/BytesRef;J)Lorg/apache/lucene/util/BytesRef;
    .locals 10
    .param p1, "b"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "start"    # J

    .prologue
    const/4 v8, 0x0

    .line 200
    iget v4, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockBits:I

    shr-long v4, p2, v4

    long-to-int v1, v4

    .line 201
    .local v1, "index":I
    iget v4, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockMask:I

    int-to-long v4, v4

    and-long/2addr v4, p2

    long-to-int v3, v4

    .line 202
    .local v3, "offset":I
    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    aget-object v0, v4, v1

    .line 204
    .local v0, "block":[B
    aget-byte v4, v0, v3

    and-int/lit16 v4, v4, 0x80

    if-nez v4, :cond_1

    .line 205
    aget-byte v2, v0, v3

    .line 206
    .local v2, "length":I
    add-int/lit8 v3, v3, 0x1

    .line 212
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/util/PagedBytes$Reader;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    if-gez v2, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "length="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 208
    .end local v2    # "length":I
    :cond_1
    aget-byte v4, v0, v3

    and-int/lit8 v4, v4, 0x7f

    shl-int/lit8 v4, v4, 0x8

    add-int/lit8 v5, v3, 0x1

    aget-byte v5, v0, v5

    and-int/lit16 v5, v5, 0xff

    or-int v2, v4, v5

    .line 209
    .restart local v2    # "length":I
    add-int/lit8 v3, v3, 0x2

    .line 210
    sget-boolean v4, Lorg/apache/lucene/util/PagedBytes$Reader;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-gtz v2, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 213
    :cond_2
    iput v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 219
    iget v4, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    sub-int/2addr v4, v3

    if-lt v4, v2, :cond_3

    .line 221
    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 222
    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    aget-object v4, v4, v1

    iput-object v4, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 230
    :goto_0
    return-object p1

    .line 225
    :cond_3
    new-array v4, v2, [B

    iput-object v4, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 226
    iput v8, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 227
    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    aget-object v4, v4, v1

    iget-object v5, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    sub-int/2addr v6, v3

    invoke-static {v4, v3, v5, v8, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 228
    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    add-int/lit8 v5, v1, 0x1

    aget-object v4, v4, v5

    iget-object v5, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    sub-int/2addr v6, v3

    iget v7, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockSize:I

    sub-int/2addr v7, v3

    sub-int v7, v2, v7

    invoke-static {v4, v8, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public getBlockEnds()[I
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blockEnds:[I

    return-object v0
.end method

.method public getBlocks()[[B
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$Reader;->blocks:[[B

    return-object v0
.end method
