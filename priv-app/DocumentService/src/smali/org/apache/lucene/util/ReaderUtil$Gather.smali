.class public abstract Lorg/apache/lucene/util/ReaderUtil$Gather;
.super Ljava/lang/Object;
.source "ReaderUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/ReaderUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Gather"
.end annotation


# instance fields
.field private final topReader:Lorg/apache/lucene/index/IndexReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;)V
    .locals 0
    .param p1, "r"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lorg/apache/lucene/util/ReaderUtil$Gather;->topReader:Lorg/apache/lucene/index/IndexReader;

    .line 67
    return-void
.end method

.method private run(ILorg/apache/lucene/index/IndexReader;)I
    .locals 3
    .param p1, "base"    # I
    .param p2, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexReader;->getSequentialSubReaders()[Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    .line 79
    .local v1, "subReaders":[Lorg/apache/lucene/index/IndexReader;
    if-nez v1, :cond_1

    .line 81
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/ReaderUtil$Gather;->add(ILorg/apache/lucene/index/IndexReader;)V

    .line 82
    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v2

    add-int/2addr p1, v2

    .line 90
    :cond_0
    return p1

    .line 85
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 86
    aget-object v2, v1, v0

    invoke-direct {p0, p1, v2}, Lorg/apache/lucene/util/ReaderUtil$Gather;->run(ILorg/apache/lucene/index/IndexReader;)I

    move-result p1

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected abstract add(ILorg/apache/lucene/index/IndexReader;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public run()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/lucene/util/ReaderUtil$Gather;->topReader:Lorg/apache/lucene/index/IndexReader;

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/ReaderUtil$Gather;->run(ILorg/apache/lucene/index/IndexReader;)I

    move-result v0

    return v0
.end method

.method public run(I)I
    .locals 1
    .param p1, "docBase"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/util/ReaderUtil$Gather;->topReader:Lorg/apache/lucene/index/IndexReader;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/util/ReaderUtil$Gather;->run(ILorg/apache/lucene/index/IndexReader;)I

    move-result v0

    return v0
.end method
