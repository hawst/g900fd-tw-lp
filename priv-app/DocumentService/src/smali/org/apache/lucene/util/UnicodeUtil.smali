.class public final Lorg/apache/lucene/util/UnicodeUtil;
.super Ljava/lang/Object;
.source "UnicodeUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;,
        Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final HALF_BASE:I = 0x10000

.field private static final HALF_MASK:J = 0x3ffL

.field private static final HALF_SHIFT:J = 0xaL

.field private static final LEAD_SURROGATE_MIN_VALUE:I = 0xd800

.field private static final LEAD_SURROGATE_OFFSET_:I = 0xd7c0

.field private static final LEAD_SURROGATE_SHIFT_:I = 0xa

.field private static final SUPPLEMENTARY_MIN_VALUE:I = 0x10000

.field private static final SURROGATE_OFFSET:I = -0x35fdc00

.field private static final TRAIL_SURROGATE_MASK_:I = 0x3ff

.field private static final TRAIL_SURROGATE_MIN_VALUE:I = 0xdc00

.field private static final UNI_MAX_BMP:J = 0xffffL

.field public static final UNI_REPLACEMENT_CHAR:I = 0xfffd

.field public static final UNI_SUR_HIGH_END:I = 0xdbff

.field public static final UNI_SUR_HIGH_START:I = 0xd800

.field public static final UNI_SUR_LOW_END:I = 0xdfff

.field public static final UNI_SUR_LOW_START:I = 0xdc00


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const-class v0, Lorg/apache/lucene/util/UnicodeUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/UnicodeUtil;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static UTF16toUTF8(Ljava/lang/CharSequence;IILorg/apache/lucene/util/BytesRef;)V
    .locals 12
    .param p0, "s"    # Ljava/lang/CharSequence;
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const v11, 0xdfff

    const v10, 0xdc00

    .line 376
    add-int v1, p1, p2

    .line 378
    .local v1, "end":I
    iget-object v4, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 379
    .local v4, "out":[B
    const/4 v8, 0x0

    iput v8, p3, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 381
    mul-int/lit8 v3, p2, 0x4

    .line 382
    .local v3, "maxLen":I
    array-length v8, v4

    if-ge v8, v3, :cond_0

    .line 383
    new-array v4, v3, [B

    .end local v4    # "out":[B
    iput-object v4, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 385
    .restart local v4    # "out":[B
    :cond_0
    const/4 v5, 0x0

    .line 386
    .local v5, "upto":I
    move v2, p1

    .local v2, "i":I
    move v6, v5

    .end local v5    # "upto":I
    .local v6, "upto":I
    :goto_0
    if-ge v2, v1, :cond_6

    .line 387
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 389
    .local v0, "code":I
    const/16 v8, 0x80

    if-ge v0, v8, :cond_1

    .line 390
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    int-to-byte v8, v0

    aput-byte v8, v4, v6

    .line 386
    :goto_1
    add-int/lit8 v2, v2, 0x1

    move v6, v5

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    goto :goto_0

    .line 391
    :cond_1
    const/16 v8, 0x800

    if-ge v0, v8, :cond_2

    .line 392
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v8, v0, 0x6

    or-int/lit16 v8, v8, 0xc0

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    .line 393
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    and-int/lit8 v8, v0, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v5

    move v5, v6

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    goto :goto_1

    .line 394
    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    :cond_2
    const v8, 0xd800

    if-lt v0, v8, :cond_3

    if-le v0, v11, :cond_4

    .line 395
    :cond_3
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v8, v0, 0xc

    or-int/lit16 v8, v8, 0xe0

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    .line 396
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    shr-int/lit8 v8, v0, 0x6

    and-int/lit8 v8, v8, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v5

    .line 397
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    and-int/lit8 v8, v0, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    goto :goto_1

    .line 401
    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    :cond_4
    if-ge v0, v10, :cond_5

    add-int/lit8 v8, v1, -0x1

    if-ge v2, v8, :cond_5

    .line 402
    add-int/lit8 v8, v2, 0x1

    invoke-interface {p0, v8}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v7

    .line 404
    .local v7, "utf32":I
    if-lt v7, v10, :cond_5

    if-gt v7, v11, :cond_5

    .line 405
    shl-int/lit8 v8, v0, 0xa

    add-int/2addr v8, v7

    const v9, -0x35fdc00

    add-int v7, v8, v9

    .line 406
    add-int/lit8 v2, v2, 0x1

    .line 407
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v8, v7, 0x12

    or-int/lit16 v8, v8, 0xf0

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    .line 408
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    shr-int/lit8 v8, v7, 0xc

    and-int/lit8 v8, v8, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v5

    .line 409
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v8, v7, 0x6

    and-int/lit8 v8, v8, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    .line 410
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    and-int/lit8 v8, v7, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v5

    move v5, v6

    .line 411
    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    goto/16 :goto_1

    .line 416
    .end local v5    # "upto":I
    .end local v7    # "utf32":I
    .restart local v6    # "upto":I
    :cond_5
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    const/16 v8, -0x11

    aput-byte v8, v4, v6

    .line 417
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    const/16 v8, -0x41

    aput-byte v8, v4, v5

    .line 418
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    const/16 v8, -0x43

    aput-byte v8, v4, v6

    goto/16 :goto_1

    .line 422
    .end local v0    # "code":I
    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    :cond_6
    iput v6, p3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 423
    return-void
.end method

.method public static UTF16toUTF8(Ljava/lang/String;IILorg/apache/lucene/util/UnicodeUtil$UTF8Result;)V
    .locals 11
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "result"    # Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    .prologue
    const v10, 0xdfff

    const v9, 0xdc00

    .line 324
    add-int v1, p1, p2

    .line 326
    .local v1, "end":I
    iget-object v3, p3, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    .line 328
    .local v3, "out":[B
    const/4 v4, 0x0

    .line 329
    .local v4, "upto":I
    move v2, p1

    .local v2, "i":I
    move v5, v4

    .end local v4    # "upto":I
    .local v5, "upto":I
    :goto_0
    if-ge v2, v1, :cond_6

    .line 330
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 332
    .local v0, "code":I
    add-int/lit8 v7, v5, 0x4

    array-length v8, v3

    if-le v7, v8, :cond_0

    .line 333
    add-int/lit8 v7, v5, 0x4

    invoke-static {v3, v7}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v3

    .end local v3    # "out":[B
    iput-object v3, p3, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    .line 335
    .restart local v3    # "out":[B
    :cond_0
    const/16 v7, 0x80

    if-ge v0, v7, :cond_1

    .line 336
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    int-to-byte v7, v0

    aput-byte v7, v3, v5

    .line 329
    :goto_1
    add-int/lit8 v2, v2, 0x1

    move v5, v4

    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    goto :goto_0

    .line 337
    :cond_1
    const/16 v7, 0x800

    if-ge v0, v7, :cond_2

    .line 338
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    shr-int/lit8 v7, v0, 0x6

    or-int/lit16 v7, v7, 0xc0

    int-to-byte v7, v7

    aput-byte v7, v3, v5

    .line 339
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    and-int/lit8 v7, v0, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v3, v4

    move v4, v5

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    goto :goto_1

    .line 340
    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    :cond_2
    const v7, 0xd800

    if-lt v0, v7, :cond_3

    if-le v0, v10, :cond_4

    .line 341
    :cond_3
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    shr-int/lit8 v7, v0, 0xc

    or-int/lit16 v7, v7, 0xe0

    int-to-byte v7, v7

    aput-byte v7, v3, v5

    .line 342
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v7, v0, 0x6

    and-int/lit8 v7, v7, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v3, v4

    .line 343
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    and-int/lit8 v7, v0, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v3, v5

    goto :goto_1

    .line 347
    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    :cond_4
    if-ge v0, v9, :cond_5

    add-int/lit8 v7, v1, -0x1

    if-ge v2, v7, :cond_5

    .line 348
    add-int/lit8 v7, v2, 0x1

    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 350
    .local v6, "utf32":I
    if-lt v6, v9, :cond_5

    if-gt v6, v10, :cond_5

    .line 351
    const v7, 0xd7c0

    sub-int v7, v0, v7

    shl-int/lit8 v7, v7, 0xa

    and-int/lit16 v8, v6, 0x3ff

    add-int v6, v7, v8

    .line 352
    add-int/lit8 v2, v2, 0x1

    .line 353
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    shr-int/lit8 v7, v6, 0x12

    or-int/lit16 v7, v7, 0xf0

    int-to-byte v7, v7

    aput-byte v7, v3, v5

    .line 354
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v7, v6, 0xc

    and-int/lit8 v7, v7, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v3, v4

    .line 355
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    shr-int/lit8 v7, v6, 0x6

    and-int/lit8 v7, v7, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v3, v5

    .line 356
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    and-int/lit8 v7, v6, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v3, v4

    move v4, v5

    .line 357
    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    goto/16 :goto_1

    .line 362
    .end local v4    # "upto":I
    .end local v6    # "utf32":I
    .restart local v5    # "upto":I
    :cond_5
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    const/16 v7, -0x11

    aput-byte v7, v3, v5

    .line 363
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    const/16 v7, -0x41

    aput-byte v7, v3, v4

    .line 364
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    const/16 v7, -0x43

    aput-byte v7, v3, v5

    goto/16 :goto_1

    .line 368
    .end local v0    # "code":I
    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    :cond_6
    iput v5, p3, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->length:I

    .line 369
    return-void
.end method

.method public static UTF16toUTF8([CIILorg/apache/lucene/util/BytesRef;)V
    .locals 11
    .param p0, "source"    # [C
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 431
    const/4 v6, 0x0

    .line 432
    .local v6, "upto":I
    move v2, p1

    .line 433
    .local v2, "i":I
    add-int v1, p1, p2

    .line 434
    .local v1, "end":I
    iget-object v5, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 436
    .local v5, "out":[B
    mul-int/lit8 v4, p2, 0x4

    .line 437
    .local v4, "maxLen":I
    array-length v9, v5

    if-ge v9, v4, :cond_0

    .line 438
    new-array v5, v4, [B

    .end local v5    # "out":[B
    iput-object v5, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 439
    .restart local v5    # "out":[B
    :cond_0
    const/4 v9, 0x0

    iput v9, p3, Lorg/apache/lucene/util/BytesRef;->offset:I

    move v3, v2

    .end local v2    # "i":I
    .local v3, "i":I
    move v7, v6

    .line 441
    .end local v6    # "upto":I
    .local v7, "upto":I
    :goto_0
    if-ge v3, v1, :cond_6

    .line 443
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget-char v0, p0, v3

    .line 445
    .local v0, "code":I
    const/16 v9, 0x80

    if-ge v0, v9, :cond_1

    .line 446
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    int-to-byte v9, v0

    aput-byte v9, v5, v7

    :goto_1
    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    move v7, v6

    .line 476
    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    goto :goto_0

    .line 447
    .end local v3    # "i":I
    .restart local v2    # "i":I
    :cond_1
    const/16 v9, 0x800

    if-ge v0, v9, :cond_2

    .line 448
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    shr-int/lit8 v9, v0, 0x6

    or-int/lit16 v9, v9, 0xc0

    int-to-byte v9, v9

    aput-byte v9, v5, v7

    .line 449
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    and-int/lit8 v9, v0, 0x3f

    or-int/lit16 v9, v9, 0x80

    int-to-byte v9, v9

    aput-byte v9, v5, v6

    move v6, v7

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    goto :goto_1

    .line 450
    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    :cond_2
    const v9, 0xd800

    if-lt v0, v9, :cond_3

    const v9, 0xdfff

    if-le v0, v9, :cond_4

    .line 451
    :cond_3
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    shr-int/lit8 v9, v0, 0xc

    or-int/lit16 v9, v9, 0xe0

    int-to-byte v9, v9

    aput-byte v9, v5, v7

    .line 452
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    shr-int/lit8 v9, v0, 0x6

    and-int/lit8 v9, v9, 0x3f

    or-int/lit16 v9, v9, 0x80

    int-to-byte v9, v9

    aput-byte v9, v5, v6

    .line 453
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    and-int/lit8 v9, v0, 0x3f

    or-int/lit16 v9, v9, 0x80

    int-to-byte v9, v9

    aput-byte v9, v5, v7

    goto :goto_1

    .line 457
    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    :cond_4
    const v9, 0xdc00

    if-ge v0, v9, :cond_5

    if-ge v2, v1, :cond_5

    .line 458
    aget-char v8, p0, v2

    .line 460
    .local v8, "utf32":I
    const v9, 0xdc00

    if-lt v8, v9, :cond_5

    const v9, 0xdfff

    if-gt v8, v9, :cond_5

    .line 461
    shl-int/lit8 v9, v0, 0xa

    add-int/2addr v9, v8

    const v10, -0x35fdc00

    add-int v8, v9, v10

    .line 462
    add-int/lit8 v2, v2, 0x1

    .line 463
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    shr-int/lit8 v9, v8, 0x12

    or-int/lit16 v9, v9, 0xf0

    int-to-byte v9, v9

    aput-byte v9, v5, v7

    .line 464
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    shr-int/lit8 v9, v8, 0xc

    and-int/lit8 v9, v9, 0x3f

    or-int/lit16 v9, v9, 0x80

    int-to-byte v9, v9

    aput-byte v9, v5, v6

    .line 465
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    shr-int/lit8 v9, v8, 0x6

    and-int/lit8 v9, v9, 0x3f

    or-int/lit16 v9, v9, 0x80

    int-to-byte v9, v9

    aput-byte v9, v5, v7

    .line 466
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    and-int/lit8 v9, v8, 0x3f

    or-int/lit16 v9, v9, 0x80

    int-to-byte v9, v9

    aput-byte v9, v5, v6

    move v3, v2

    .line 467
    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto/16 :goto_0

    .line 472
    .end local v3    # "i":I
    .end local v8    # "utf32":I
    .restart local v2    # "i":I
    :cond_5
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    const/16 v9, -0x11

    aput-byte v9, v5, v7

    .line 473
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "upto":I
    .restart local v7    # "upto":I
    const/16 v9, -0x41

    aput-byte v9, v5, v6

    .line 474
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "upto":I
    .restart local v6    # "upto":I
    const/16 v9, -0x43

    aput-byte v9, v5, v7

    goto/16 :goto_1

    .line 478
    .end local v0    # "code":I
    .end local v2    # "i":I
    .end local v6    # "upto":I
    .restart local v3    # "i":I
    .restart local v7    # "upto":I
    :cond_6
    iput v7, p3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 479
    return-void
.end method

.method public static UTF16toUTF8([CIILorg/apache/lucene/util/UnicodeUtil$UTF8Result;)V
    .locals 12
    .param p0, "source"    # [C
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "result"    # Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    .prologue
    const v11, 0xdfff

    const v10, 0xdc00

    .line 272
    const/4 v5, 0x0

    .line 273
    .local v5, "upto":I
    move v2, p1

    .line 274
    .local v2, "i":I
    add-int v1, p1, p2

    .line 275
    .local v1, "end":I
    iget-object v4, p3, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    .local v4, "out":[B
    move v3, v2

    .end local v2    # "i":I
    .local v3, "i":I
    move v6, v5

    .line 277
    .end local v5    # "upto":I
    .local v6, "upto":I
    :goto_0
    if-ge v3, v1, :cond_6

    .line 279
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget-char v0, p0, v3

    .line 281
    .local v0, "code":I
    add-int/lit8 v8, v6, 0x4

    array-length v9, v4

    if-le v8, v9, :cond_0

    .line 282
    add-int/lit8 v8, v6, 0x4

    invoke-static {v4, v8}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v4

    .end local v4    # "out":[B
    iput-object v4, p3, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    .line 284
    .restart local v4    # "out":[B
    :cond_0
    const/16 v8, 0x80

    if-ge v0, v8, :cond_1

    .line 285
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    int-to-byte v8, v0

    aput-byte v8, v4, v6

    :goto_1
    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    move v6, v5

    .line 315
    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    goto :goto_0

    .line 286
    .end local v3    # "i":I
    .restart local v2    # "i":I
    :cond_1
    const/16 v8, 0x800

    if-ge v0, v8, :cond_2

    .line 287
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v8, v0, 0x6

    or-int/lit16 v8, v8, 0xc0

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    .line 288
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    and-int/lit8 v8, v0, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v5

    move v5, v6

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    goto :goto_1

    .line 289
    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    :cond_2
    const v8, 0xd800

    if-lt v0, v8, :cond_3

    if-le v0, v11, :cond_4

    .line 290
    :cond_3
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v8, v0, 0xc

    or-int/lit16 v8, v8, 0xe0

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    .line 291
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    shr-int/lit8 v8, v0, 0x6

    and-int/lit8 v8, v8, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v5

    .line 292
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    and-int/lit8 v8, v0, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    goto :goto_1

    .line 296
    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    :cond_4
    if-ge v0, v10, :cond_5

    if-ge v2, v1, :cond_5

    aget-char v8, p0, v2

    const v9, 0xffff

    if-eq v8, v9, :cond_5

    .line 297
    aget-char v7, p0, v2

    .line 299
    .local v7, "utf32":I
    if-lt v7, v10, :cond_5

    if-gt v7, v11, :cond_5

    .line 300
    const v8, 0xd7c0

    sub-int v8, v0, v8

    shl-int/lit8 v8, v8, 0xa

    and-int/lit16 v9, v7, 0x3ff

    add-int v7, v8, v9

    .line 301
    add-int/lit8 v2, v2, 0x1

    .line 302
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v8, v7, 0x12

    or-int/lit16 v8, v8, 0xf0

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    .line 303
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    shr-int/lit8 v8, v7, 0xc

    and-int/lit8 v8, v8, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v5

    .line 304
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v8, v7, 0x6

    and-int/lit8 v8, v8, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v6

    .line 305
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    and-int/lit8 v8, v7, 0x3f

    or-int/lit16 v8, v8, 0x80

    int-to-byte v8, v8

    aput-byte v8, v4, v5

    move v3, v2

    .line 306
    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto/16 :goto_0

    .line 311
    .end local v3    # "i":I
    .end local v7    # "utf32":I
    .restart local v2    # "i":I
    :cond_5
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    const/16 v8, -0x11

    aput-byte v8, v4, v6

    .line 312
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    const/16 v8, -0x41

    aput-byte v8, v4, v5

    .line 313
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    const/16 v8, -0x43

    aput-byte v8, v4, v6

    goto/16 :goto_1

    .line 317
    .end local v0    # "code":I
    .end local v2    # "i":I
    .end local v5    # "upto":I
    .restart local v3    # "i":I
    .restart local v6    # "upto":I
    :cond_6
    iput v6, p3, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->length:I

    .line 318
    return-void
.end method

.method public static UTF16toUTF8([CILorg/apache/lucene/util/UnicodeUtil$UTF8Result;)V
    .locals 12
    .param p0, "source"    # [C
    .param p1, "offset"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    .prologue
    const v11, 0xffff

    const v10, 0xdfff

    const v9, 0xdc00

    .line 217
    const/4 v4, 0x0

    .line 218
    .local v4, "upto":I
    move v1, p1

    .line 219
    .local v1, "i":I
    iget-object v3, p2, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    .line 223
    .local v3, "out":[B
    :goto_0
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    aget-char v0, p0, v1

    .line 225
    .local v0, "code":I
    add-int/lit8 v7, v4, 0x4

    array-length v8, v3

    if-le v7, v8, :cond_0

    .line 226
    add-int/lit8 v7, v4, 0x4

    invoke-static {v3, v7}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v3

    .end local v3    # "out":[B
    iput-object v3, p2, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    .line 228
    .restart local v3    # "out":[B
    :cond_0
    const/16 v7, 0x80

    if-ge v0, v7, :cond_1

    .line 229
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "upto":I
    .local v5, "upto":I
    int-to-byte v7, v0

    aput-byte v7, v3, v4

    move v4, v5

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    :goto_1
    move v1, v2

    .line 262
    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 230
    .end local v1    # "i":I
    .restart local v2    # "i":I
    :cond_1
    const/16 v7, 0x800

    if-ge v0, v7, :cond_2

    .line 231
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v7, v0, 0x6

    or-int/lit16 v7, v7, 0xc0

    int-to-byte v7, v7

    aput-byte v7, v3, v4

    .line 232
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    and-int/lit8 v7, v0, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v3, v5

    goto :goto_1

    .line 233
    :cond_2
    const v7, 0xd800

    if-lt v0, v7, :cond_3

    if-le v0, v10, :cond_5

    .line 234
    :cond_3
    if-ne v0, v11, :cond_4

    .line 264
    iput v4, p2, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->length:I

    .line 265
    return-void

    .line 237
    :cond_4
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v7, v0, 0xc

    or-int/lit16 v7, v7, 0xe0

    int-to-byte v7, v7

    aput-byte v7, v3, v4

    .line 238
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    shr-int/lit8 v7, v0, 0x6

    and-int/lit8 v7, v7, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v3, v5

    .line 239
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    and-int/lit8 v7, v0, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v3, v4

    move v4, v5

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    goto :goto_1

    .line 243
    :cond_5
    if-ge v0, v9, :cond_6

    aget-char v7, p0, v2

    if-eq v7, v11, :cond_6

    .line 244
    aget-char v6, p0, v2

    .line 246
    .local v6, "utf32":I
    if-lt v6, v9, :cond_6

    if-gt v6, v10, :cond_6

    .line 247
    const v7, 0xd7c0

    sub-int v7, v0, v7

    shl-int/lit8 v7, v7, 0xa

    and-int/lit16 v8, v6, 0x3ff

    add-int v6, v7, v8

    .line 248
    add-int/lit8 v1, v2, 0x1

    .line 249
    .end local v2    # "i":I
    .restart local v1    # "i":I
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v7, v6, 0x12

    or-int/lit16 v7, v7, 0xf0

    int-to-byte v7, v7

    aput-byte v7, v3, v4

    .line 250
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    shr-int/lit8 v7, v6, 0xc

    and-int/lit8 v7, v7, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v3, v5

    .line 251
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    shr-int/lit8 v7, v6, 0x6

    and-int/lit8 v7, v7, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v3, v4

    .line 252
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    and-int/lit8 v7, v6, 0x3f

    or-int/lit16 v7, v7, 0x80

    int-to-byte v7, v7

    aput-byte v7, v3, v5

    goto/16 :goto_0

    .line 258
    .end local v1    # "i":I
    .end local v6    # "utf32":I
    .restart local v2    # "i":I
    :cond_6
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    const/16 v7, -0x11

    aput-byte v7, v3, v4

    .line 259
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    const/16 v7, -0x41

    aput-byte v7, v3, v5

    .line 260
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "upto":I
    .restart local v5    # "upto":I
    const/16 v7, -0x43

    aput-byte v7, v3, v4

    move v4, v5

    .end local v5    # "upto":I
    .restart local v4    # "upto":I
    goto/16 :goto_1
.end method

.method public static UTF16toUTF8WithHash([CIILorg/apache/lucene/util/BytesRef;)I
    .locals 12
    .param p0, "source"    # [C
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 160
    const/4 v2, 0x0

    .line 161
    .local v2, "hash":I
    const/4 v7, 0x0

    .line 162
    .local v7, "upto":I
    move v3, p1

    .line 163
    .local v3, "i":I
    add-int v1, p1, p2

    .line 164
    .local v1, "end":I
    iget-object v6, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 166
    .local v6, "out":[B
    mul-int/lit8 v5, p2, 0x4

    .line 167
    .local v5, "maxLen":I
    array-length v10, v6

    if-ge v10, v5, :cond_0

    .line 168
    const/4 v10, 0x1

    invoke-static {v5, v10}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v10

    new-array v6, v10, [B

    .end local v6    # "out":[B
    iput-object v6, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 169
    .restart local v6    # "out":[B
    :cond_0
    const/4 v10, 0x0

    iput v10, p3, Lorg/apache/lucene/util/BytesRef;->offset:I

    move v4, v3

    .end local v3    # "i":I
    .local v4, "i":I
    move v8, v7

    .line 171
    .end local v7    # "upto":I
    .local v8, "upto":I
    :goto_0
    if-ge v4, v1, :cond_6

    .line 173
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "i":I
    .restart local v3    # "i":I
    aget-char v0, p0, v4

    .line 175
    .local v0, "code":I
    const/16 v10, 0x80

    if-ge v0, v10, :cond_1

    .line 176
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    int-to-byte v11, v0

    aput-byte v11, v6, v8

    add-int v2, v10, v11

    :goto_1
    move v4, v3

    .end local v3    # "i":I
    .restart local v4    # "i":I
    move v8, v7

    .line 206
    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    goto :goto_0

    .line 177
    .end local v4    # "i":I
    .restart local v3    # "i":I
    :cond_1
    const/16 v10, 0x800

    if-ge v0, v10, :cond_2

    .line 178
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    shr-int/lit8 v11, v0, 0x6

    or-int/lit16 v11, v11, 0xc0

    int-to-byte v11, v11

    aput-byte v11, v6, v8

    add-int v2, v10, v11

    .line 179
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    and-int/lit8 v11, v0, 0x3f

    or-int/lit16 v11, v11, 0x80

    int-to-byte v11, v11

    aput-byte v11, v6, v7

    add-int v2, v10, v11

    move v7, v8

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    goto :goto_1

    .line 180
    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    :cond_2
    const v10, 0xd800

    if-lt v0, v10, :cond_3

    const v10, 0xdfff

    if-le v0, v10, :cond_4

    .line 181
    :cond_3
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    shr-int/lit8 v11, v0, 0xc

    or-int/lit16 v11, v11, 0xe0

    int-to-byte v11, v11

    aput-byte v11, v6, v8

    add-int v2, v10, v11

    .line 182
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    shr-int/lit8 v11, v0, 0x6

    and-int/lit8 v11, v11, 0x3f

    or-int/lit16 v11, v11, 0x80

    int-to-byte v11, v11

    aput-byte v11, v6, v7

    add-int v2, v10, v11

    .line 183
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    and-int/lit8 v11, v0, 0x3f

    or-int/lit16 v11, v11, 0x80

    int-to-byte v11, v11

    aput-byte v11, v6, v8

    add-int v2, v10, v11

    goto :goto_1

    .line 187
    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    :cond_4
    const v10, 0xdc00

    if-ge v0, v10, :cond_5

    if-ge v3, v1, :cond_5

    .line 188
    aget-char v9, p0, v3

    .line 190
    .local v9, "utf32":I
    const v10, 0xdc00

    if-lt v9, v10, :cond_5

    const v10, 0xdfff

    if-gt v9, v10, :cond_5

    .line 191
    shl-int/lit8 v10, v0, 0xa

    add-int/2addr v10, v9

    const v11, -0x35fdc00

    add-int v9, v10, v11

    .line 192
    add-int/lit8 v3, v3, 0x1

    .line 193
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    shr-int/lit8 v11, v9, 0x12

    or-int/lit16 v11, v11, 0xf0

    int-to-byte v11, v11

    aput-byte v11, v6, v8

    add-int v2, v10, v11

    .line 194
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    shr-int/lit8 v11, v9, 0xc

    and-int/lit8 v11, v11, 0x3f

    or-int/lit16 v11, v11, 0x80

    int-to-byte v11, v11

    aput-byte v11, v6, v7

    add-int v2, v10, v11

    .line 195
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    shr-int/lit8 v11, v9, 0x6

    and-int/lit8 v11, v11, 0x3f

    or-int/lit16 v11, v11, 0x80

    int-to-byte v11, v11

    aput-byte v11, v6, v8

    add-int v2, v10, v11

    .line 196
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    and-int/lit8 v11, v9, 0x3f

    or-int/lit16 v11, v11, 0x80

    int-to-byte v11, v11

    aput-byte v11, v6, v7

    add-int v2, v10, v11

    move v4, v3

    .line 197
    .end local v3    # "i":I
    .restart local v4    # "i":I
    goto/16 :goto_0

    .line 202
    .end local v4    # "i":I
    .end local v9    # "utf32":I
    .restart local v3    # "i":I
    :cond_5
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    const/16 v11, -0x11

    aput-byte v11, v6, v8

    add-int/lit8 v2, v10, -0x11

    .line 203
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "upto":I
    .restart local v8    # "upto":I
    const/16 v11, -0x41

    aput-byte v11, v6, v7

    add-int/lit8 v2, v10, -0x41

    .line 204
    mul-int/lit8 v10, v2, 0x1f

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "upto":I
    .restart local v7    # "upto":I
    const/16 v11, -0x43

    aput-byte v11, v6, v8

    add-int/lit8 v2, v10, -0x43

    goto/16 :goto_1

    .line 208
    .end local v0    # "code":I
    .end local v3    # "i":I
    .end local v7    # "upto":I
    .restart local v4    # "i":I
    .restart local v8    # "upto":I
    :cond_6
    iput v8, p3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 209
    return v2
.end method

.method public static UTF8toUTF16(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/CharsRef;)V
    .locals 3
    .param p0, "bytesRef"    # Lorg/apache/lucene/util/BytesRef;
    .param p1, "chars"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    .line 750
    iget-object v0, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v2, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v0, v1, v2, p1}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 751
    return-void
.end method

.method public static UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V
    .locals 12
    .param p0, "utf8"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "chars"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    .line 716
    const/4 v6, 0x0

    iput v6, p3, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 717
    .local v6, "out_offset":I
    iget-object v8, p3, Lorg/apache/lucene/util/CharsRef;->chars:[C

    invoke-static {v8, p2}, Lorg/apache/lucene/util/ArrayUtil;->grow([CI)[C

    move-result-object v5

    iput-object v5, p3, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 718
    .local v5, "out":[C
    add-int v3, p1, p2

    .local v3, "limit":I
    move v7, v6

    .end local v6    # "out_offset":I
    .local v7, "out_offset":I
    move v4, p1

    .line 719
    .end local p1    # "offset":I
    .local v4, "offset":I
    :goto_0
    if-ge v4, v3, :cond_6

    .line 720
    add-int/lit8 p1, v4, 0x1

    .end local v4    # "offset":I
    .restart local p1    # "offset":I
    aget-byte v8, p0, v4

    and-int/lit16 v0, v8, 0xff

    .line 721
    .local v0, "b":I
    const/16 v8, 0xc0

    if-ge v0, v8, :cond_1

    .line 722
    sget-boolean v8, Lorg/apache/lucene/util/UnicodeUtil;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    const/16 v8, 0x80

    if-lt v0, v8, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 723
    :cond_0
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "out_offset":I
    .restart local v6    # "out_offset":I
    int-to-char v8, v0

    aput-char v8, v5, v7

    :goto_1
    move v7, v6

    .end local v6    # "out_offset":I
    .restart local v7    # "out_offset":I
    move v4, p1

    .line 741
    .end local p1    # "offset":I
    .restart local v4    # "offset":I
    goto :goto_0

    .line 724
    .end local v4    # "offset":I
    .restart local p1    # "offset":I
    :cond_1
    const/16 v8, 0xe0

    if-ge v0, v8, :cond_2

    .line 725
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "out_offset":I
    .restart local v6    # "out_offset":I
    and-int/lit8 v8, v0, 0x1f

    shl-int/lit8 v8, v8, 0x6

    add-int/lit8 v4, p1, 0x1

    .end local p1    # "offset":I
    .restart local v4    # "offset":I
    aget-byte v9, p0, p1

    and-int/lit8 v9, v9, 0x3f

    add-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v5, v7

    move p1, v4

    .end local v4    # "offset":I
    .restart local p1    # "offset":I
    goto :goto_1

    .line 726
    .end local v6    # "out_offset":I
    .restart local v7    # "out_offset":I
    :cond_2
    const/16 v8, 0xf0

    if-ge v0, v8, :cond_3

    .line 727
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "out_offset":I
    .restart local v6    # "out_offset":I
    and-int/lit8 v8, v0, 0xf

    shl-int/lit8 v8, v8, 0xc

    aget-byte v9, p0, p1

    and-int/lit8 v9, v9, 0x3f

    shl-int/lit8 v9, v9, 0x6

    add-int/2addr v8, v9

    add-int/lit8 v9, p1, 0x1

    aget-byte v9, p0, v9

    and-int/lit8 v9, v9, 0x3f

    add-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v5, v7

    .line 728
    add-int/lit8 p1, p1, 0x2

    goto :goto_1

    .line 730
    .end local v6    # "out_offset":I
    .restart local v7    # "out_offset":I
    :cond_3
    sget-boolean v8, Lorg/apache/lucene/util/UnicodeUtil;->$assertionsDisabled:Z

    if-nez v8, :cond_4

    const/16 v8, 0xf8

    if-lt v0, v8, :cond_4

    new-instance v8, Ljava/lang/AssertionError;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "b="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v8

    .line 731
    :cond_4
    and-int/lit8 v8, v0, 0x7

    shl-int/lit8 v8, v8, 0x12

    aget-byte v9, p0, p1

    and-int/lit8 v9, v9, 0x3f

    shl-int/lit8 v9, v9, 0xc

    add-int/2addr v8, v9

    add-int/lit8 v9, p1, 0x1

    aget-byte v9, p0, v9

    and-int/lit8 v9, v9, 0x3f

    shl-int/lit8 v9, v9, 0x6

    add-int/2addr v8, v9

    add-int/lit8 v9, p1, 0x2

    aget-byte v9, p0, v9

    and-int/lit8 v9, v9, 0x3f

    add-int v1, v8, v9

    .line 732
    .local v1, "ch":I
    add-int/lit8 p1, p1, 0x3

    .line 733
    int-to-long v8, v1

    const-wide/32 v10, 0xffff

    cmp-long v8, v8, v10

    if-gez v8, :cond_5

    .line 734
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "out_offset":I
    .restart local v6    # "out_offset":I
    int-to-char v8, v1

    aput-char v8, v5, v7

    goto/16 :goto_1

    .line 736
    .end local v6    # "out_offset":I
    .restart local v7    # "out_offset":I
    :cond_5
    const/high16 v8, 0x10000

    sub-int v2, v1, v8

    .line 737
    .local v2, "chHalf":I
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "out_offset":I
    .restart local v6    # "out_offset":I
    shr-int/lit8 v8, v2, 0xa

    const v9, 0xd800

    add-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v5, v7

    .line 738
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "out_offset":I
    .restart local v7    # "out_offset":I
    int-to-long v8, v2

    const-wide/16 v10, 0x3ff

    and-long/2addr v8, v10

    const-wide/32 v10, 0xdc00

    add-long/2addr v8, v10

    long-to-int v8, v8

    int-to-char v8, v8

    aput-char v8, v5, v6

    move v6, v7

    .end local v7    # "out_offset":I
    .restart local v6    # "out_offset":I
    goto/16 :goto_1

    .line 742
    .end local v0    # "b":I
    .end local v1    # "ch":I
    .end local v2    # "chHalf":I
    .end local v6    # "out_offset":I
    .end local p1    # "offset":I
    .restart local v4    # "offset":I
    .restart local v7    # "out_offset":I
    :cond_6
    iget v8, p3, Lorg/apache/lucene/util/CharsRef;->offset:I

    sub-int v8, v7, v8

    iput v8, p3, Lorg/apache/lucene/util/CharsRef;->length:I

    .line 743
    return-void
.end method

.method public static UTF8toUTF16([BIILorg/apache/lucene/util/UnicodeUtil$UTF16Result;)V
    .locals 16
    .param p0, "utf8"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "result"    # Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    .prologue
    .line 487
    add-int v5, p1, p2

    .line 488
    .local v5, "end":I
    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    .line 489
    .local v7, "out":[C
    move-object/from16 v0, p3

    iget-object v12, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->offsets:[I

    array-length v12, v12

    if-gt v12, v5, :cond_0

    .line 490
    move-object/from16 v0, p3

    iget-object v12, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->offsets:[I

    add-int/lit8 v13, v5, 0x1

    invoke-static {v12, v13}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v12

    move-object/from16 v0, p3

    iput-object v12, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->offsets:[I

    .line 492
    :cond_0
    move-object/from16 v0, p3

    iget-object v6, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->offsets:[I

    .line 496
    .local v6, "offsets":[I
    move/from16 v10, p1

    .line 497
    .local v10, "upto":I
    :goto_0
    aget v12, v6, v10

    const/4 v13, -0x1

    if-ne v12, v13, :cond_1

    .line 498
    add-int/lit8 v10, v10, -0x1

    goto :goto_0

    .line 500
    :cond_1
    aget v8, v6, v10

    .line 503
    .local v8, "outUpto":I
    add-int v12, v8, p2

    array-length v13, v7

    if-lt v12, v13, :cond_9

    .line 504
    add-int v12, v8, p2

    add-int/lit8 v12, v12, 0x1

    invoke-static {v7, v12}, Lorg/apache/lucene/util/ArrayUtil;->grow([CI)[C

    move-result-object v7

    .end local v7    # "out":[C
    move-object/from16 v0, p3

    iput-object v7, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    .restart local v7    # "out":[C
    move v9, v8

    .end local v8    # "outUpto":I
    .local v9, "outUpto":I
    move v11, v10

    .line 507
    .end local v10    # "upto":I
    .local v11, "upto":I
    :goto_1
    if-ge v11, v5, :cond_8

    .line 509
    aget-byte v12, p0, v11

    and-int/lit16 v2, v12, 0xff

    .line 512
    .local v2, "b":I
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "upto":I
    .restart local v10    # "upto":I
    aput v9, v6, v11

    .line 514
    const/16 v12, 0xc0

    if-ge v2, v12, :cond_3

    .line 515
    sget-boolean v12, Lorg/apache/lucene/util/UnicodeUtil;->$assertionsDisabled:Z

    if-nez v12, :cond_2

    const/16 v12, 0x80

    if-lt v2, v12, :cond_2

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 516
    :cond_2
    move v3, v2

    .line 532
    .local v3, "ch":I
    :goto_2
    int-to-long v12, v3

    const-wide/32 v14, 0xffff

    cmp-long v12, v12, v14

    if-gtz v12, :cond_7

    .line 534
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "outUpto":I
    .restart local v8    # "outUpto":I
    int-to-char v12, v3

    aput-char v12, v7, v9

    :goto_3
    move v9, v8

    .end local v8    # "outUpto":I
    .restart local v9    # "outUpto":I
    move v11, v10

    .line 541
    .end local v10    # "upto":I
    .restart local v11    # "upto":I
    goto :goto_1

    .line 517
    .end local v3    # "ch":I
    .end local v11    # "upto":I
    .restart local v10    # "upto":I
    :cond_3
    const/16 v12, 0xe0

    if-ge v2, v12, :cond_4

    .line 518
    and-int/lit8 v12, v2, 0x1f

    shl-int/lit8 v12, v12, 0x6

    aget-byte v13, p0, v10

    and-int/lit8 v13, v13, 0x3f

    add-int v3, v12, v13

    .line 519
    .restart local v3    # "ch":I
    add-int/lit8 v11, v10, 0x1

    .end local v10    # "upto":I
    .restart local v11    # "upto":I
    const/4 v12, -0x1

    aput v12, v6, v10

    move v10, v11

    .end local v11    # "upto":I
    .restart local v10    # "upto":I
    goto :goto_2

    .line 520
    .end local v3    # "ch":I
    :cond_4
    const/16 v12, 0xf0

    if-ge v2, v12, :cond_5

    .line 521
    and-int/lit8 v12, v2, 0xf

    shl-int/lit8 v12, v12, 0xc

    aget-byte v13, p0, v10

    and-int/lit8 v13, v13, 0x3f

    shl-int/lit8 v13, v13, 0x6

    add-int/2addr v12, v13

    add-int/lit8 v13, v10, 0x1

    aget-byte v13, p0, v13

    and-int/lit8 v13, v13, 0x3f

    add-int v3, v12, v13

    .line 522
    .restart local v3    # "ch":I
    add-int/lit8 v11, v10, 0x1

    .end local v10    # "upto":I
    .restart local v11    # "upto":I
    const/4 v12, -0x1

    aput v12, v6, v10

    .line 523
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "upto":I
    .restart local v10    # "upto":I
    const/4 v12, -0x1

    aput v12, v6, v11

    goto :goto_2

    .line 525
    .end local v3    # "ch":I
    :cond_5
    sget-boolean v12, Lorg/apache/lucene/util/UnicodeUtil;->$assertionsDisabled:Z

    if-nez v12, :cond_6

    const/16 v12, 0xf8

    if-lt v2, v12, :cond_6

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 526
    :cond_6
    and-int/lit8 v12, v2, 0x7

    shl-int/lit8 v12, v12, 0x12

    aget-byte v13, p0, v10

    and-int/lit8 v13, v13, 0x3f

    shl-int/lit8 v13, v13, 0xc

    add-int/2addr v12, v13

    add-int/lit8 v13, v10, 0x1

    aget-byte v13, p0, v13

    and-int/lit8 v13, v13, 0x3f

    shl-int/lit8 v13, v13, 0x6

    add-int/2addr v12, v13

    add-int/lit8 v13, v10, 0x2

    aget-byte v13, p0, v13

    and-int/lit8 v13, v13, 0x3f

    add-int v3, v12, v13

    .line 527
    .restart local v3    # "ch":I
    add-int/lit8 v11, v10, 0x1

    .end local v10    # "upto":I
    .restart local v11    # "upto":I
    const/4 v12, -0x1

    aput v12, v6, v10

    .line 528
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "upto":I
    .restart local v10    # "upto":I
    const/4 v12, -0x1

    aput v12, v6, v11

    .line 529
    add-int/lit8 v11, v10, 0x1

    .end local v10    # "upto":I
    .restart local v11    # "upto":I
    const/4 v12, -0x1

    aput v12, v6, v10

    move v10, v11

    .end local v11    # "upto":I
    .restart local v10    # "upto":I
    goto/16 :goto_2

    .line 537
    :cond_7
    const/high16 v12, 0x10000

    sub-int v4, v3, v12

    .line 538
    .local v4, "chHalf":I
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "outUpto":I
    .restart local v8    # "outUpto":I
    shr-int/lit8 v12, v4, 0xa

    const v13, 0xd800

    add-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v7, v9

    .line 539
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "outUpto":I
    .restart local v9    # "outUpto":I
    int-to-long v12, v4

    const-wide/16 v14, 0x3ff

    and-long/2addr v12, v14

    const-wide/32 v14, 0xdc00

    add-long/2addr v12, v14

    long-to-int v12, v12

    int-to-char v12, v12

    aput-char v12, v7, v8

    move v8, v9

    .end local v9    # "outUpto":I
    .restart local v8    # "outUpto":I
    goto/16 :goto_3

    .line 543
    .end local v2    # "b":I
    .end local v3    # "ch":I
    .end local v4    # "chHalf":I
    .end local v8    # "outUpto":I
    .end local v10    # "upto":I
    .restart local v9    # "outUpto":I
    .restart local v11    # "upto":I
    :cond_8
    aput v9, v6, v11

    .line 544
    move-object/from16 v0, p3

    iput v9, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    .line 545
    return-void

    .end local v9    # "outUpto":I
    .end local v11    # "upto":I
    .restart local v8    # "outUpto":I
    .restart local v10    # "upto":I
    :cond_9
    move v9, v8

    .end local v8    # "outUpto":I
    .restart local v9    # "outUpto":I
    move v11, v10

    .end local v10    # "upto":I
    .restart local v11    # "upto":I
    goto/16 :goto_1
.end method

.method public static newString([III)Ljava/lang/String;
    .locals 13
    .param p0, "codePoints"    # [I
    .param p1, "offset"    # I
    .param p2, "count"    # I

    .prologue
    const/4 v12, 0x0

    .line 673
    if-gez p2, :cond_0

    .line 674
    new-instance v8, Ljava/lang/IllegalArgumentException;

    invoke-direct {v8}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v8

    .line 676
    :cond_0
    new-array v0, p2, [C

    .line 677
    .local v0, "chars":[C
    const/4 v7, 0x0

    .line 678
    .local v7, "w":I
    move v5, p1

    .local v5, "r":I
    add-int v2, p1, p2

    .local v2, "e":I
    :goto_0
    if-ge v5, v2, :cond_4

    .line 679
    aget v1, p0, v5

    .line 680
    .local v1, "cp":I
    if-ltz v1, :cond_1

    const v8, 0x10ffff

    if-le v1, v8, :cond_2

    .line 681
    :cond_1
    new-instance v8, Ljava/lang/IllegalArgumentException;

    invoke-direct {v8}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v8

    .line 694
    :catch_0
    move-exception v3

    .line 695
    .local v3, "ex":Ljava/lang/IndexOutOfBoundsException;
    array-length v8, p0

    int-to-double v8, v8

    add-int/lit8 v10, v7, 0x2

    int-to-double v10, v10

    mul-double/2addr v8, v10

    sub-int v10, v5, p1

    add-int/lit8 v10, v10, 0x1

    int-to-double v10, v10

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v4, v8

    .line 697
    .local v4, "newlen":I
    new-array v6, v4, [C

    .line 698
    .local v6, "temp":[C
    invoke-static {v0, v12, v6, v12, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 699
    move-object v0, v6

    .line 685
    .end local v3    # "ex":Ljava/lang/IndexOutOfBoundsException;
    .end local v4    # "newlen":I
    .end local v6    # "temp":[C
    :cond_2
    const/high16 v8, 0x10000

    if-ge v1, v8, :cond_3

    .line 686
    int-to-char v8, v1

    :try_start_0
    aput-char v8, v0, v7

    .line 687
    add-int/lit8 v7, v7, 0x1

    .line 678
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 689
    :cond_3
    const v8, 0xd7c0

    shr-int/lit8 v9, v1, 0xa

    add-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v0, v7

    .line 690
    add-int/lit8 v8, v7, 0x1

    const v9, 0xdc00

    and-int/lit16 v10, v1, 0x3ff

    add-int/2addr v9, v10

    int-to-char v9, v9

    aput-char v9, v0, v8
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 691
    add-int/lit8 v7, v7, 0x2

    goto :goto_1

    .line 703
    .end local v1    # "cp":I
    :cond_4
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v0, v12, v7}, Ljava/lang/String;-><init>([CII)V

    return-object v8
.end method

.method public static validUTF16String(Ljava/lang/CharSequence;)Z
    .locals 8
    .param p0, "s"    # Ljava/lang/CharSequence;

    .prologue
    const v7, 0xdfff

    const v6, 0xdc00

    const/4 v4, 0x0

    .line 754
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 755
    .local v3, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_3

    .line 756
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 757
    .local v0, "ch":C
    const v5, 0xd800

    if-lt v0, v5, :cond_1

    const v5, 0xdbff

    if-gt v0, v5, :cond_1

    .line 758
    add-int/lit8 v5, v3, -0x1

    if-ge v1, v5, :cond_2

    .line 759
    add-int/lit8 v1, v1, 0x1

    .line 760
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 761
    .local v2, "nextCH":C
    if-lt v2, v6, :cond_2

    if-gt v2, v7, :cond_2

    .line 755
    .end local v2    # "nextCH":C
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 769
    :cond_1
    if-lt v0, v6, :cond_0

    if-gt v0, v7, :cond_0

    .line 774
    .end local v0    # "ch":C
    :cond_2
    :goto_1
    return v4

    :cond_3
    const/4 v4, 0x1

    goto :goto_1
.end method
