.class Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;
.super Ljava/lang/Object;
.source "ScorerDocQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/ScorerDocQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HeapedScorerDoc"
.end annotation


# instance fields
.field doc:I

.field scorer:Lorg/apache/lucene/search/Scorer;

.field final synthetic this$0:Lorg/apache/lucene/util/ScorerDocQueue;


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/ScorerDocQueue;Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p2, "s"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 42
    invoke-virtual {p2}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;-><init>(Lorg/apache/lucene/util/ScorerDocQueue;Lorg/apache/lucene/search/Scorer;I)V

    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/util/ScorerDocQueue;Lorg/apache/lucene/search/Scorer;I)V
    .locals 0
    .param p2, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .param p3, "doc"    # I

    .prologue
    .line 44
    iput-object p1, p0, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->this$0:Lorg/apache/lucene/util/ScorerDocQueue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p2, p0, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 46
    iput p3, p0, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->doc:I

    .line 47
    return-void
.end method


# virtual methods
.method adjust()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/util/ScorerDocQueue$HeapedScorerDoc;->doc:I

    return-void
.end method
