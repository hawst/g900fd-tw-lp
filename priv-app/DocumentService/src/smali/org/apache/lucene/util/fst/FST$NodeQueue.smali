.class Lorg/apache/lucene/util/fst/FST$NodeQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "FST.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/fst/FST;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NodeQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/util/fst/FST$NodeAndInCount;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1796
    const-class v0, Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/fst/FST$NodeQueue;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "topN"    # I

    .prologue
    .line 1797
    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;-><init>()V

    .line 1798
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->initialize(I)V

    .line 1799
    return-void
.end method


# virtual methods
.method public bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 1796
    check-cast p1, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/fst/FST$NodeQueue;->lessThan(Lorg/apache/lucene/util/fst/FST$NodeAndInCount;Lorg/apache/lucene/util/fst/FST$NodeAndInCount;)Z

    move-result v0

    return v0
.end method

.method public lessThan(Lorg/apache/lucene/util/fst/FST$NodeAndInCount;Lorg/apache/lucene/util/fst/FST$NodeAndInCount;)Z
    .locals 2
    .param p1, "a"    # Lorg/apache/lucene/util/fst/FST$NodeAndInCount;
    .param p2, "b"    # Lorg/apache/lucene/util/fst/FST$NodeAndInCount;

    .prologue
    .line 1803
    invoke-virtual {p1, p2}, Lorg/apache/lucene/util/fst/FST$NodeAndInCount;->compareTo(Lorg/apache/lucene/util/fst/FST$NodeAndInCount;)I

    move-result v0

    .line 1804
    .local v0, "cmp":I
    sget-boolean v1, Lorg/apache/lucene/util/fst/FST$NodeQueue;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1805
    :cond_0
    if-gez v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
