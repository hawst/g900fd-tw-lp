.class public final Lorg/apache/lucene/util/IOUtils;
.super Ljava/lang/Object;
.source "IOUtils.java"


# static fields
.field public static final CHARSET_UTF_8:Ljava/nio/charset/Charset;

.field private static final SUPPRESS_METHOD:Ljava/lang/reflect/Method;

.field public static final UTF_8:Ljava/lang/String; = "UTF-8"


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 48
    const-string/jumbo v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    sput-object v2, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    .line 223
    :try_start_0
    const-class v2, Ljava/lang/Throwable;

    const-string/jumbo v3, "addSuppressed"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/Throwable;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 227
    .local v1, "m":Ljava/lang/reflect/Method;
    :goto_0
    sput-object v1, Lorg/apache/lucene/util/IOUtils;->SUPPRESS_METHOD:Ljava/lang/reflect/Method;

    .line 228
    return-void

    .line 224
    .end local v1    # "m":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 225
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .restart local v1    # "m":Ljava/lang/reflect/Method;
    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static final addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    .locals 3
    .param p0, "exception"    # Ljava/lang/Throwable;
    .param p1, "suppressed"    # Ljava/lang/Throwable;

    .prologue
    .line 235
    sget-object v0, Lorg/apache/lucene/util/IOUtils;->SUPPRESS_METHOD:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 237
    :try_start_0
    sget-object v0, Lorg/apache/lucene/util/IOUtils;->SUPPRESS_METHOD:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 238
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static close(Ljava/lang/Iterable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/io/Closeable;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 163
    .local p0, "objects":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/io/Closeable;>;"
    const/4 v3, 0x0

    .line 165
    .local v3, "th":Ljava/lang/Throwable;
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/Closeable;

    .line 167
    .local v1, "object":Ljava/io/Closeable;
    if-eqz v1, :cond_0

    .line 168
    :try_start_0
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 170
    :catch_0
    move-exception v2

    .line 171
    .local v2, "t":Ljava/lang/Throwable;
    invoke-static {v3, v2}, Lorg/apache/lucene/util/IOUtils;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    .line 172
    if-nez v3, :cond_0

    .line 173
    move-object v3, v2

    goto :goto_0

    .line 178
    .end local v1    # "object":Ljava/io/Closeable;
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_1
    if-eqz v3, :cond_5

    .line 179
    instance-of v4, v3, Ljava/io/IOException;

    if-eqz v4, :cond_2

    check-cast v3, Ljava/io/IOException;

    .end local v3    # "th":Ljava/lang/Throwable;
    throw v3

    .line 180
    .restart local v3    # "th":Ljava/lang/Throwable;
    :cond_2
    instance-of v4, v3, Ljava/lang/RuntimeException;

    if-eqz v4, :cond_3

    check-cast v3, Ljava/lang/RuntimeException;

    .end local v3    # "th":Ljava/lang/Throwable;
    throw v3

    .line 181
    .restart local v3    # "th":Ljava/lang/Throwable;
    :cond_3
    instance-of v4, v3, Ljava/lang/Error;

    if-eqz v4, :cond_4

    check-cast v3, Ljava/lang/Error;

    .end local v3    # "th":Ljava/lang/Throwable;
    throw v3

    .line 182
    .restart local v3    # "th":Ljava/lang/Throwable;
    :cond_4
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 184
    :cond_5
    return-void
.end method

.method public static varargs close([Ljava/io/Closeable;)V
    .locals 7
    .param p0, "objects"    # [Ljava/io/Closeable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    const/4 v5, 0x0

    .line 138
    .local v5, "th":Ljava/lang/Throwable;
    move-object v0, p0

    .local v0, "arr$":[Ljava/io/Closeable;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 140
    .local v3, "object":Ljava/io/Closeable;
    if-eqz v3, :cond_0

    .line 141
    :try_start_0
    invoke-interface {v3}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 143
    :catch_0
    move-exception v4

    .line 144
    .local v4, "t":Ljava/lang/Throwable;
    invoke-static {v5, v4}, Lorg/apache/lucene/util/IOUtils;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    .line 145
    if-nez v5, :cond_0

    .line 146
    move-object v5, v4

    goto :goto_1

    .line 151
    .end local v3    # "object":Ljava/io/Closeable;
    .end local v4    # "t":Ljava/lang/Throwable;
    :cond_1
    if-eqz v5, :cond_5

    .line 152
    instance-of v6, v5, Ljava/io/IOException;

    if-eqz v6, :cond_2

    check-cast v5, Ljava/io/IOException;

    .end local v5    # "th":Ljava/lang/Throwable;
    throw v5

    .line 153
    .restart local v5    # "th":Ljava/lang/Throwable;
    :cond_2
    instance-of v6, v5, Ljava/lang/RuntimeException;

    if-eqz v6, :cond_3

    check-cast v5, Ljava/lang/RuntimeException;

    .end local v5    # "th":Ljava/lang/Throwable;
    throw v5

    .line 154
    .restart local v5    # "th":Ljava/lang/Throwable;
    :cond_3
    instance-of v6, v5, Ljava/lang/Error;

    if-eqz v6, :cond_4

    check-cast v5, Ljava/lang/Error;

    .end local v5    # "th":Ljava/lang/Throwable;
    throw v5

    .line 155
    .restart local v5    # "th":Ljava/lang/Throwable;
    :cond_4
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 157
    :cond_5
    return-void
.end method

.method public static closeWhileHandlingException(Ljava/lang/Exception;Ljava/lang/Iterable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Exception;",
            ">(TE;",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/io/Closeable;",
            ">;)V^TE;^",
            "Ljava/io/IOException;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    .local p0, "priorException":Ljava/lang/Exception;, "TE;"
    .local p1, "objects":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/io/Closeable;>;"
    const/4 v3, 0x0

    .line 102
    .local v3, "th":Ljava/lang/Throwable;
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/Closeable;

    .line 104
    .local v1, "object":Ljava/io/Closeable;
    if-eqz v1, :cond_0

    .line 105
    :try_start_0
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 107
    :catch_0
    move-exception v2

    .line 108
    .local v2, "t":Ljava/lang/Throwable;
    if-nez p0, :cond_1

    move-object v4, v3

    :goto_1
    invoke-static {v4, v2}, Lorg/apache/lucene/util/IOUtils;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    .line 109
    if-nez v3, :cond_0

    .line 110
    move-object v3, v2

    goto :goto_0

    :cond_1
    move-object v4, p0

    .line 108
    goto :goto_1

    .line 115
    .end local v1    # "object":Ljava/io/Closeable;
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_2
    if-eqz p0, :cond_3

    .line 116
    throw p0

    .line 117
    :cond_3
    if-eqz v3, :cond_7

    .line 118
    instance-of v4, v3, Ljava/io/IOException;

    if-eqz v4, :cond_4

    check-cast v3, Ljava/io/IOException;

    .end local v3    # "th":Ljava/lang/Throwable;
    throw v3

    .line 119
    .restart local v3    # "th":Ljava/lang/Throwable;
    :cond_4
    instance-of v4, v3, Ljava/lang/RuntimeException;

    if-eqz v4, :cond_5

    check-cast v3, Ljava/lang/RuntimeException;

    .end local v3    # "th":Ljava/lang/Throwable;
    throw v3

    .line 120
    .restart local v3    # "th":Ljava/lang/Throwable;
    :cond_5
    instance-of v4, v3, Ljava/lang/Error;

    if-eqz v4, :cond_6

    check-cast v3, Ljava/lang/Error;

    .end local v3    # "th":Ljava/lang/Throwable;
    throw v3

    .line 121
    .restart local v3    # "th":Ljava/lang/Throwable;
    :cond_6
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 123
    :cond_7
    return-void
.end method

.method public static varargs closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V
    .locals 7
    .param p1, "objects"    # [Ljava/io/Closeable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Exception;",
            ">(TE;[",
            "Ljava/io/Closeable;",
            ")V^TE;^",
            "Ljava/io/IOException;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "priorException":Ljava/lang/Exception;, "TE;"
    const/4 v5, 0x0

    .line 75
    .local v5, "th":Ljava/lang/Throwable;
    move-object v0, p1

    .local v0, "arr$":[Ljava/io/Closeable;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 77
    .local v3, "object":Ljava/io/Closeable;
    if-eqz v3, :cond_0

    .line 78
    :try_start_0
    invoke-interface {v3}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 80
    :catch_0
    move-exception v4

    .line 81
    .local v4, "t":Ljava/lang/Throwable;
    if-nez p0, :cond_1

    move-object v6, v5

    :goto_2
    invoke-static {v6, v4}, Lorg/apache/lucene/util/IOUtils;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    .line 82
    if-nez v5, :cond_0

    .line 83
    move-object v5, v4

    goto :goto_1

    :cond_1
    move-object v6, p0

    .line 81
    goto :goto_2

    .line 88
    .end local v3    # "object":Ljava/io/Closeable;
    .end local v4    # "t":Ljava/lang/Throwable;
    :cond_2
    if-eqz p0, :cond_3

    .line 89
    throw p0

    .line 90
    :cond_3
    if-eqz v5, :cond_7

    .line 91
    instance-of v6, v5, Ljava/io/IOException;

    if-eqz v6, :cond_4

    check-cast v5, Ljava/io/IOException;

    .end local v5    # "th":Ljava/lang/Throwable;
    throw v5

    .line 92
    .restart local v5    # "th":Ljava/lang/Throwable;
    :cond_4
    instance-of v6, v5, Ljava/lang/RuntimeException;

    if-eqz v6, :cond_5

    check-cast v5, Ljava/lang/RuntimeException;

    .end local v5    # "th":Ljava/lang/Throwable;
    throw v5

    .line 93
    .restart local v5    # "th":Ljava/lang/Throwable;
    :cond_5
    instance-of v6, v5, Ljava/lang/Error;

    if-eqz v6, :cond_6

    check-cast v5, Ljava/lang/Error;

    .end local v5    # "th":Ljava/lang/Throwable;
    throw v5

    .line 94
    .restart local v5    # "th":Ljava/lang/Throwable;
    :cond_6
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 96
    :cond_7
    return-void
.end method

.method public static closeWhileHandlingException(Ljava/lang/Iterable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/io/Closeable;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    .local p0, "objects":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/io/Closeable;>;"
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/Closeable;

    .line 210
    .local v1, "object":Ljava/io/Closeable;
    if-eqz v1, :cond_0

    .line 211
    :try_start_0
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 213
    :catch_0
    move-exception v2

    goto :goto_0

    .line 216
    .end local v1    # "object":Ljava/io/Closeable;
    :cond_1
    return-void
.end method

.method public static varargs closeWhileHandlingException([Ljava/io/Closeable;)V
    .locals 5
    .param p0, "objects"    # [Ljava/io/Closeable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    move-object v0, p0

    .local v0, "arr$":[Ljava/io/Closeable;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 196
    .local v3, "object":Ljava/io/Closeable;
    if-eqz v3, :cond_0

    .line 197
    :try_start_0
    invoke-interface {v3}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 199
    :catch_0
    move-exception v4

    goto :goto_1

    .line 202
    .end local v3    # "object":Ljava/io/Closeable;
    :cond_1
    return-void
.end method

.method public static getDecodingReader(Ljava/io/File;Ljava/nio/charset/Charset;)Ljava/io/Reader;
    .locals 7
    .param p0, "file"    # Ljava/io/File;
    .param p1, "charSet"    # Ljava/nio/charset/Charset;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 277
    const/4 v1, 0x0

    .line 278
    .local v1, "stream":Ljava/io/FileInputStream;
    const/4 v3, 0x0

    .line 280
    .local v3, "success":Z
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    .end local v1    # "stream":Ljava/io/FileInputStream;
    .local v2, "stream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-static {v2, p1}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/io/InputStream;Ljava/nio/charset/Charset;)Ljava/io/Reader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 282
    .local v0, "reader":Ljava/io/Reader;
    const/4 v3, 0x1

    .line 286
    if-nez v3, :cond_0

    .line 287
    new-array v4, v5, [Ljava/io/Closeable;

    aput-object v2, v4, v6

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 283
    :cond_0
    return-object v0

    .line 286
    .end local v0    # "reader":Ljava/io/Reader;
    .end local v2    # "stream":Ljava/io/FileInputStream;
    .restart local v1    # "stream":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v4

    :goto_0
    if-nez v3, :cond_1

    .line 287
    new-array v5, v5, [Ljava/io/Closeable;

    aput-object v1, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 286
    :cond_1
    throw v4

    .end local v1    # "stream":Ljava/io/FileInputStream;
    .restart local v2    # "stream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "stream":Ljava/io/FileInputStream;
    .restart local v1    # "stream":Ljava/io/FileInputStream;
    goto :goto_0
.end method

.method public static getDecodingReader(Ljava/io/InputStream;Ljava/nio/charset/Charset;)Ljava/io/Reader;
    .locals 3
    .param p0, "stream"    # Ljava/io/InputStream;
    .param p1, "charSet"    # Ljava/nio/charset/Charset;

    .prologue
    .line 258
    invoke-virtual {p1}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v1

    sget-object v2, Ljava/nio/charset/CodingErrorAction;->REPORT:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v1, v2}, Ljava/nio/charset/CharsetDecoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v1

    sget-object v2, Ljava/nio/charset/CodingErrorAction;->REPORT:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v1, v2}, Ljava/nio/charset/CharsetDecoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v0

    .line 261
    .local v0, "charSetDecoder":Ljava/nio/charset/CharsetDecoder;
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, p0, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/CharsetDecoder;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    return-object v1
.end method

.method public static getDecodingReader(Ljava/lang/Class;Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/io/Reader;
    .locals 6
    .param p1, "resource"    # Ljava/lang/String;
    .param p2, "charSet"    # Ljava/nio/charset/Charset;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/nio/charset/Charset;",
            ")",
            "Ljava/io/Reader;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 307
    const/4 v1, 0x0

    .line 308
    .local v1, "stream":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 310
    .local v2, "success":Z
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 312
    invoke-static {v1, p2}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/io/InputStream;Ljava/nio/charset/Charset;)Ljava/io/Reader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 313
    .local v0, "reader":Ljava/io/Reader;
    const/4 v2, 0x1

    .line 316
    if-nez v2, :cond_0

    .line 317
    new-array v3, v4, [Ljava/io/Closeable;

    aput-object v1, v3, v5

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 314
    :cond_0
    return-object v0

    .line 316
    .end local v0    # "reader":Ljava/io/Reader;
    :catchall_0
    move-exception v3

    if-nez v2, :cond_1

    .line 317
    new-array v4, v4, [Ljava/io/Closeable;

    aput-object v1, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 316
    :cond_1
    throw v3
.end method
