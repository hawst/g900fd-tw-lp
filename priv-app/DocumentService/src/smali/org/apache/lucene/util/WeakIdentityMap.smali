.class public final Lorg/apache/lucene/util/WeakIdentityMap;
.super Ljava/lang/Object;
.source "WeakIdentityMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final backingStore:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;",
            "TV;>;"
        }
    .end annotation
.end field

.field private final queue:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;",
            "TV;>;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    .local p1, "backingStore":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->queue:Ljava/lang/ref/ReferenceQueue;

    .line 62
    iput-object p1, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    .line 63
    return-void
.end method

.method public static final newConcurrentHashMap()Lorg/apache/lucene/util/WeakIdentityMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lorg/apache/lucene/util/WeakIdentityMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v0, Lorg/apache/lucene/util/WeakIdentityMap;

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/WeakIdentityMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public static final newHashMap()Lorg/apache/lucene/util/WeakIdentityMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lorg/apache/lucene/util/WeakIdentityMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Lorg/apache/lucene/util/WeakIdentityMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/WeakIdentityMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method private reap()V
    .locals 2

    .prologue
    .line 103
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/util/WeakIdentityMap;->queue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v1}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    .local v0, "zombie":Ljava/lang/ref/Reference;, "Ljava/lang/ref/Reference<*>;"
    if-eqz v0, :cond_0

    .line 104
    iget-object v1, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 106
    :cond_0
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 66
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 67
    invoke-direct {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->reap()V

    .line 68
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 71
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    invoke-direct {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->reap()V

    .line 72
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    new-instance v1, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 76
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    invoke-direct {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->reap()V

    .line 77
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    new-instance v1, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 86
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 81
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    invoke-direct {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->reap()V

    .line 82
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    new-instance v1, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;

    iget-object v2, p0, Lorg/apache/lucene/util/WeakIdentityMap;->queue:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1, p1, v2}, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 90
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    invoke-direct {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->reap()V

    .line 91
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    new-instance v1, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lorg/apache/lucene/util/WeakIdentityMap$IdentityWeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 95
    .local p0, "this":Lorg/apache/lucene/util/WeakIdentityMap;, "Lorg/apache/lucene/util/WeakIdentityMap<TK;TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    const/4 v0, 0x0

    .line 98
    :goto_0
    return v0

    .line 97
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/util/WeakIdentityMap;->reap()V

    .line 98
    iget-object v0, p0, Lorg/apache/lucene/util/WeakIdentityMap;->backingStore:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    goto :goto_0
.end method
