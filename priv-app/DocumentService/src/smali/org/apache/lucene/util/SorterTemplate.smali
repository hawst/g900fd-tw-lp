.class public abstract Lorg/apache/lucene/util/SorterTemplate;
.super Ljava/lang/Object;
.source "SorterTemplate.java"


# static fields
.field private static final MERGESORT_THRESHOLD:I = 0xc

.field private static final QUICKSORT_THRESHOLD:I = 0x7


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private lower(III)I
    .locals 4
    .param p1, "lo"    # I
    .param p2, "hi"    # I
    .param p3, "val"    # I

    .prologue
    .line 183
    sub-int v1, p2, p1

    .line 184
    .local v1, "len":I
    :goto_0
    if-lez v1, :cond_1

    .line 185
    ushr-int/lit8 v0, v1, 0x1

    .line 186
    .local v0, "half":I
    add-int v2, p1, v0

    .line 187
    .local v2, "mid":I
    invoke-virtual {p0, v2, p3}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v3

    if-gez v3, :cond_0

    .line 188
    add-int/lit8 p1, v2, 0x1

    .line 189
    sub-int v3, v1, v0

    add-int/lit8 v1, v3, -0x1

    goto :goto_0

    .line 191
    :cond_0
    move v1, v0

    goto :goto_0

    .line 194
    .end local v0    # "half":I
    .end local v2    # "mid":I
    :cond_1
    return p1
.end method

.method private merge(IIIII)V
    .locals 14
    .param p1, "lo"    # I
    .param p2, "pivot"    # I
    .param p3, "hi"    # I
    .param p4, "len1"    # I
    .param p5, "len2"    # I

    .prologue
    .line 138
    if-eqz p4, :cond_0

    if-nez p5, :cond_1

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    add-int v2, p4, p5

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 142
    move/from16 v0, p2

    invoke-virtual {p0, v0, p1}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v2

    if-gez v2, :cond_0

    .line 143
    move/from16 v0, p2

    invoke-virtual {p0, v0, p1}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    goto :goto_0

    .line 149
    :cond_2
    move/from16 v0, p4

    move/from16 v1, p5

    if-le v0, v1, :cond_3

    .line 150
    ushr-int/lit8 v6, p4, 0x1

    .line 151
    .local v6, "len11":I
    add-int v4, p1, v6

    .line 152
    .local v4, "first_cut":I
    move/from16 v0, p2

    move/from16 v1, p3

    invoke-direct {p0, v0, v1, v4}, Lorg/apache/lucene/util/SorterTemplate;->lower(III)I

    move-result v10

    .line 153
    .local v10, "second_cut":I
    sub-int v7, v10, p2

    .line 160
    .local v7, "len22":I
    :goto_1
    move/from16 v0, p2

    invoke-direct {p0, v4, v0, v10}, Lorg/apache/lucene/util/SorterTemplate;->rotate(III)V

    .line 161
    add-int v5, v4, v7

    .local v5, "new_mid":I
    move-object v2, p0

    move v3, p1

    .line 162
    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/util/SorterTemplate;->merge(IIIII)V

    .line 163
    sub-int v12, p4, v6

    sub-int v13, p5, v7

    move-object v8, p0

    move v9, v5

    move/from16 v11, p3

    invoke-direct/range {v8 .. v13}, Lorg/apache/lucene/util/SorterTemplate;->merge(IIIII)V

    goto :goto_0

    .line 155
    .end local v4    # "first_cut":I
    .end local v5    # "new_mid":I
    .end local v6    # "len11":I
    .end local v7    # "len22":I
    .end local v10    # "second_cut":I
    :cond_3
    ushr-int/lit8 v7, p5, 0x1

    .line 156
    .restart local v7    # "len22":I
    add-int v10, p2, v7

    .line 157
    .restart local v10    # "second_cut":I
    move/from16 v0, p2

    invoke-direct {p0, p1, v0, v10}, Lorg/apache/lucene/util/SorterTemplate;->upper(III)I

    move-result v4

    .line 158
    .restart local v4    # "first_cut":I
    sub-int v6, v4, p1

    .restart local v6    # "len11":I
    goto :goto_1
.end method

.method private quickSort(III)V
    .locals 5
    .param p1, "lo"    # I
    .param p2, "hi"    # I
    .param p3, "maxDepth"    # I

    .prologue
    .line 73
    sub-int v0, p2, p1

    .line 74
    .local v0, "diff":I
    const/4 v4, 0x7

    if-gt v0, v4, :cond_0

    .line 75
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/SorterTemplate;->insertionSort(II)V

    .line 119
    :goto_0
    return-void

    .line 80
    :cond_0
    add-int/lit8 p3, p3, -0x1

    if-nez p3, :cond_1

    .line 81
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/SorterTemplate;->mergeSort(II)V

    goto :goto_0

    .line 85
    :cond_1
    ushr-int/lit8 v4, v0, 0x1

    add-int v2, p1, v4

    .line 87
    .local v2, "mid":I
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v4

    if-lez v4, :cond_2

    .line 88
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    .line 91
    :cond_2
    invoke-virtual {p0, v2, p2}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v4

    if-lez v4, :cond_3

    .line 92
    invoke-virtual {p0, v2, p2}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    .line 93
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v4

    if-lez v4, :cond_3

    .line 94
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    .line 98
    :cond_3
    add-int/lit8 v1, p1, 0x1

    .line 99
    .local v1, "left":I
    add-int/lit8 v3, p2, -0x1

    .line 101
    .local v3, "right":I
    invoke-virtual {p0, v2}, Lorg/apache/lucene/util/SorterTemplate;->setPivot(I)V

    .line 103
    :goto_1
    invoke-virtual {p0, v3}, Lorg/apache/lucene/util/SorterTemplate;->comparePivot(I)I

    move-result v4

    if-gez v4, :cond_4

    .line 104
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 106
    :cond_4
    :goto_2
    if-ge v1, v3, :cond_5

    invoke-virtual {p0, v1}, Lorg/apache/lucene/util/SorterTemplate;->comparePivot(I)I

    move-result v4

    if-ltz v4, :cond_5

    .line 107
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 109
    :cond_5
    if-ge v1, v3, :cond_6

    .line 110
    invoke-virtual {p0, v1, v3}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    .line 111
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 117
    :cond_6
    invoke-direct {p0, p1, v1, p3}, Lorg/apache/lucene/util/SorterTemplate;->quickSort(III)V

    .line 118
    add-int/lit8 v4, v1, 0x1

    invoke-direct {p0, v4, p2, p3}, Lorg/apache/lucene/util/SorterTemplate;->quickSort(III)V

    goto :goto_0
.end method

.method private rotate(III)V
    .locals 4
    .param p1, "lo"    # I
    .param p2, "mid"    # I
    .param p3, "hi"    # I

    .prologue
    .line 167
    move v2, p1

    .line 168
    .local v2, "lot":I
    add-int/lit8 v0, p2, -0x1

    .local v0, "hit":I
    move v1, v0

    .end local v0    # "hit":I
    .local v1, "hit":I
    move v3, v2

    .line 169
    .end local v2    # "lot":I
    .local v3, "lot":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 170
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "lot":I
    .restart local v2    # "lot":I
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "hit":I
    .restart local v0    # "hit":I
    invoke-virtual {p0, v3, v1}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    move v1, v0

    .end local v0    # "hit":I
    .restart local v1    # "hit":I
    move v3, v2

    .end local v2    # "lot":I
    .restart local v3    # "lot":I
    goto :goto_0

    .line 172
    :cond_0
    move v2, p2

    .end local v3    # "lot":I
    .restart local v2    # "lot":I
    add-int/lit8 v0, p3, -0x1

    .end local v1    # "hit":I
    .restart local v0    # "hit":I
    move v1, v0

    .end local v0    # "hit":I
    .restart local v1    # "hit":I
    move v3, v2

    .line 173
    .end local v2    # "lot":I
    .restart local v3    # "lot":I
    :goto_1
    if-ge v3, v1, :cond_1

    .line 174
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "lot":I
    .restart local v2    # "lot":I
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "hit":I
    .restart local v0    # "hit":I
    invoke-virtual {p0, v3, v1}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    move v1, v0

    .end local v0    # "hit":I
    .restart local v1    # "hit":I
    move v3, v2

    .end local v2    # "lot":I
    .restart local v3    # "lot":I
    goto :goto_1

    .line 176
    :cond_1
    move v2, p1

    .end local v3    # "lot":I
    .restart local v2    # "lot":I
    add-int/lit8 v0, p3, -0x1

    .end local v1    # "hit":I
    .restart local v0    # "hit":I
    move v1, v0

    .end local v0    # "hit":I
    .restart local v1    # "hit":I
    move v3, v2

    .line 177
    .end local v2    # "lot":I
    .restart local v3    # "lot":I
    :goto_2
    if-ge v3, v1, :cond_2

    .line 178
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "lot":I
    .restart local v2    # "lot":I
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "hit":I
    .restart local v0    # "hit":I
    invoke-virtual {p0, v3, v1}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    move v1, v0

    .end local v0    # "hit":I
    .restart local v1    # "hit":I
    move v3, v2

    .end local v2    # "lot":I
    .restart local v3    # "lot":I
    goto :goto_2

    .line 180
    :cond_2
    return-void
.end method

.method private upper(III)I
    .locals 4
    .param p1, "lo"    # I
    .param p2, "hi"    # I
    .param p3, "val"    # I

    .prologue
    .line 198
    sub-int v1, p2, p1

    .line 199
    .local v1, "len":I
    :goto_0
    if-lez v1, :cond_1

    .line 200
    ushr-int/lit8 v0, v1, 0x1

    .line 201
    .local v0, "half":I
    add-int v2, p1, v0

    .line 202
    .local v2, "mid":I
    invoke-virtual {p0, p3, v2}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v3

    if-gez v3, :cond_0

    .line 203
    move v1, v0

    goto :goto_0

    .line 205
    :cond_0
    add-int/lit8 p1, v2, 0x1

    .line 206
    sub-int v3, v1, v0

    add-int/lit8 v1, v3, -0x1

    goto :goto_0

    .line 209
    .end local v0    # "half":I
    .end local v2    # "mid":I
    :cond_1
    return p1
.end method


# virtual methods
.method protected abstract compare(II)I
.end method

.method protected abstract comparePivot(I)I
.end method

.method public final insertionSort(II)V
    .locals 3
    .param p1, "lo"    # I
    .param p2, "hi"    # I

    .prologue
    .line 52
    add-int/lit8 v0, p1, 0x1

    .local v0, "i":I
    :goto_0
    if-gt v0, p2, :cond_1

    .line 53
    move v1, v0

    .local v1, "j":I
    :goto_1
    if-le v1, p1, :cond_0

    .line 54
    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p0, v2, v1}, Lorg/apache/lucene/util/SorterTemplate;->compare(II)I

    move-result v2

    if-lez v2, :cond_0

    .line 55
    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p0, v2, v1}, Lorg/apache/lucene/util/SorterTemplate;->swap(II)V

    .line 53
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 52
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    .end local v1    # "j":I
    :cond_1
    return-void
.end method

.method public final mergeSort(II)V
    .locals 7
    .param p1, "lo"    # I
    .param p2, "hi"    # I

    .prologue
    .line 124
    sub-int v6, p2, p1

    .line 125
    .local v6, "diff":I
    const/16 v0, 0xc

    if-gt v6, v0, :cond_0

    .line 126
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/SorterTemplate;->insertionSort(II)V

    .line 135
    :goto_0
    return-void

    .line 130
    :cond_0
    ushr-int/lit8 v0, v6, 0x1

    add-int v2, p1, v0

    .line 132
    .local v2, "mid":I
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/util/SorterTemplate;->mergeSort(II)V

    .line 133
    invoke-virtual {p0, v2, p2}, Lorg/apache/lucene/util/SorterTemplate;->mergeSort(II)V

    .line 134
    sub-int v4, v2, p1

    sub-int v5, p2, v2

    move-object v0, p0

    move v1, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/util/SorterTemplate;->merge(IIIII)V

    goto :goto_0
.end method

.method public final quickSort(II)V
    .locals 1
    .param p1, "lo"    # I
    .param p2, "hi"    # I

    .prologue
    .line 66
    if-gt p2, p1, :cond_0

    .line 69
    :goto_0
    return-void

    .line 68
    :cond_0
    sub-int v0, p2, p1

    invoke-static {v0}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x20

    shl-int/lit8 v0, v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/util/SorterTemplate;->quickSort(III)V

    goto :goto_0
.end method

.method protected abstract setPivot(I)V
.end method

.method protected abstract swap(II)V
.end method
