.class final Lorg/apache/lucene/util/CharacterUtils$Java4CharacterUtils;
.super Lorg/apache/lucene/util/CharacterUtils;
.source "CharacterUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/CharacterUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Java4CharacterUtils"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 231
    invoke-direct {p0}, Lorg/apache/lucene/util/CharacterUtils;-><init>()V

    .line 232
    return-void
.end method


# virtual methods
.method public codePointAt(Ljava/lang/CharSequence;I)I
    .locals 1
    .param p1, "seq"    # Ljava/lang/CharSequence;
    .param p2, "offset"    # I

    .prologue
    .line 241
    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    return v0
.end method

.method public codePointAt([CI)I
    .locals 1
    .param p1, "chars"    # [C
    .param p2, "offset"    # I

    .prologue
    .line 236
    aget-char v0, p1, p2

    return v0
.end method

.method public codePointAt([CII)I
    .locals 2
    .param p1, "chars"    # [C
    .param p2, "offset"    # I
    .param p3, "limit"    # I

    .prologue
    .line 246
    if-lt p2, p3, :cond_0

    .line 247
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string/jumbo v1, "offset must be less than limit"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :cond_0
    aget-char v0, p1, p2

    return v0
.end method

.method public fill(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;Ljava/io/Reader;)Z
    .locals 3
    .param p1, "buffer"    # Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;
    .param p2, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 253
    # setter for: Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->offset:I
    invoke-static {p1, v1}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->access$102(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;I)I

    .line 254
    # getter for: Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->buffer:[C
    invoke-static {p1}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->access$000(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;)[C

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/Reader;->read([C)I

    move-result v0

    .line 255
    .local v0, "read":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 258
    :goto_0
    return v1

    .line 257
    :cond_0
    # setter for: Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1, v0}, Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;->access$202(Lorg/apache/lucene/util/CharacterUtils$CharacterBuffer;I)I

    .line 258
    const/4 v1, 0x1

    goto :goto_0
.end method
