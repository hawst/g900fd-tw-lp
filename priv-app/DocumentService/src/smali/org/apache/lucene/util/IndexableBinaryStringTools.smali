.class public final Lorg/apache/lucene/util/IndexableBinaryStringTools;
.super Ljava/lang/Object;
.source "IndexableBinaryStringTools.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 56
    const-class v0, Lorg/apache/lucene/util/IndexableBinaryStringTools;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/IndexableBinaryStringTools;->$assertionsDisabled:Z

    .line 58
    const/16 v0, 0x8

    new-array v0, v0, [Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    new-instance v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/4 v4, 0x7

    invoke-direct {v3, v4, v1}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(II)V

    aput-object v3, v0, v2

    new-instance v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/16 v4, 0xe

    const/4 v5, 0x6

    invoke-direct {v3, v4, v5, v7}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(III)V

    aput-object v3, v0, v1

    new-instance v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/16 v4, 0xd

    const/4 v5, 0x5

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(III)V

    aput-object v3, v0, v7

    new-instance v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/16 v4, 0xc

    invoke-direct {v3, v4, v8, v8}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(III)V

    aput-object v3, v0, v6

    new-instance v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/16 v4, 0xb

    const/4 v5, 0x5

    invoke-direct {v3, v4, v6, v5}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(III)V

    aput-object v3, v0, v8

    const/4 v3, 0x5

    new-instance v4, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/16 v5, 0xa

    const/4 v6, 0x6

    invoke-direct {v4, v5, v7, v6}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(III)V

    aput-object v4, v0, v3

    const/4 v3, 0x6

    new-instance v4, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/16 v5, 0x9

    const/4 v6, 0x7

    invoke-direct {v4, v5, v1, v6}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(III)V

    aput-object v4, v0, v3

    const/4 v1, 0x7

    new-instance v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    const/16 v4, 0x8

    invoke-direct {v3, v4, v2}, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;-><init>(II)V

    aput-object v3, v0, v1

    sput-object v0, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    return-void

    :cond_0
    move v0, v2

    .line 56
    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decode(Ljava/nio/CharBuffer;)Ljava/nio/ByteBuffer;
    .locals 3
    .param p0, "input"    # Ljava/nio/CharBuffer;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 348
    invoke-static {p0}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->getDecodedLength(Ljava/nio/CharBuffer;)I

    move-result v2

    new-array v1, v2, [B

    .line 349
    .local v1, "outputArray":[B
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 350
    .local v0, "output":Ljava/nio/ByteBuffer;
    invoke-static {p0, v0}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->decode(Ljava/nio/CharBuffer;Ljava/nio/ByteBuffer;)V

    .line 351
    return-object v0
.end method

.method public static decode(Ljava/nio/CharBuffer;Ljava/nio/ByteBuffer;)V
    .locals 6
    .param p0, "input"    # Ljava/nio/CharBuffer;
    .param p1, "output"    # Ljava/nio/ByteBuffer;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 251
    invoke-virtual {p0}, Ljava/nio/CharBuffer;->hasArray()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    invoke-virtual {p0}, Ljava/nio/CharBuffer;->arrayOffset()I

    move-result v1

    .line 253
    .local v1, "inputOffset":I
    invoke-virtual {p0}, Ljava/nio/CharBuffer;->limit()I

    move-result v0

    sub-int v2, v0, v1

    .line 254
    .local v2, "inputLength":I
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v4

    .line 255
    .local v4, "outputOffset":I
    invoke-virtual {p0}, Ljava/nio/CharBuffer;->array()[C

    move-result-object v0

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->getDecodedLength([CII)I

    move-result v5

    .line 257
    .local v5, "outputLength":I
    add-int v0, v5, v4

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 258
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 259
    invoke-virtual {p0}, Ljava/nio/CharBuffer;->array()[C

    move-result-object v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->decode([CII[BII)V

    .line 264
    return-void

    .line 262
    .end local v1    # "inputOffset":I
    .end local v2    # "inputLength":I
    .end local v4    # "outputOffset":I
    .end local v5    # "outputLength":I
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Arguments must have backing arrays"

    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static decode([CII[BII)V
    .locals 12
    .param p0, "inputArray"    # [C
    .param p1, "inputOffset"    # I
    .param p2, "inputLength"    # I
    .param p3, "outputArray"    # [B
    .param p4, "outputOffset"    # I
    .param p5, "outputLength"    # I

    .prologue
    .line 281
    sget-boolean v9, Lorg/apache/lucene/util/IndexableBinaryStringTools;->$assertionsDisabled:Z

    if-nez v9, :cond_0

    invoke-static {p0, p1, p2}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->getDecodedLength([CII)I

    move-result v9

    move/from16 v0, p5

    if-eq v0, v9, :cond_0

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 283
    :cond_0
    add-int/lit8 v6, p2, -0x1

    .line 284
    .local v6, "numInputChars":I
    move/from16 v7, p5

    .line 286
    .local v7, "numOutputBytes":I
    if-lez v7, :cond_6

    .line 287
    const/4 v2, 0x0

    .line 288
    .local v2, "caseNum":I
    move/from16 v8, p4

    .line 289
    .local v8, "outputByteNum":I
    move v5, p1

    .line 292
    .local v5, "inputCharNum":I
    :goto_0
    add-int/lit8 v9, v6, -0x1

    if-ge v5, v9, :cond_4

    .line 293
    sget-object v9, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    aget-object v3, v9, v2

    .line 294
    .local v3, "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    aget-char v9, p0, v5

    int-to-short v4, v9

    .line 295
    .local v4, "inputChar":S
    const/4 v9, 0x2

    iget v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->numBytes:I

    if-ne v9, v10, :cond_3

    .line 296
    if-nez v2, :cond_2

    .line 297
    iget v9, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    ushr-int v9, v4, v9

    int-to-byte v9, v9

    aput-byte v9, p3, v8

    .line 301
    :goto_1
    add-int/lit8 v9, v8, 0x1

    iget-short v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalMask:S

    and-int/2addr v10, v4

    iget v11, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalShift:I

    shl-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, p3, v9

    .line 307
    :goto_2
    iget v9, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->advanceBytes:I

    add-int/2addr v8, v9

    .line 308
    add-int/lit8 v2, v2, 0x1

    sget-object v9, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    array-length v9, v9

    if-ne v2, v9, :cond_1

    .line 309
    const/4 v2, 0x0

    .line 292
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 299
    :cond_2
    aget-byte v9, p3, v8

    iget v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    ushr-int v10, v4, v10

    int-to-byte v10, v10

    add-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, p3, v8

    goto :goto_1

    .line 303
    :cond_3
    aget-byte v9, p3, v8

    iget v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    ushr-int v10, v4, v10

    int-to-byte v10, v10

    add-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, p3, v8

    .line 304
    add-int/lit8 v9, v8, 0x1

    iget-short v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->middleMask:S

    and-int/2addr v10, v4

    iget v11, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->middleShift:I

    ushr-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, p3, v9

    .line 305
    add-int/lit8 v9, v8, 0x2

    iget-short v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalMask:S

    and-int/2addr v10, v4

    iget v11, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalShift:I

    shl-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, p3, v9

    goto :goto_2

    .line 313
    .end local v3    # "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    .end local v4    # "inputChar":S
    :cond_4
    aget-char v9, p0, v5

    int-to-short v4, v9

    .line 314
    .restart local v4    # "inputChar":S
    sget-object v9, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    aget-object v3, v9, v2

    .line 315
    .restart local v3    # "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    if-nez v2, :cond_5

    .line 316
    const/4 v9, 0x0

    aput-byte v9, p3, v8

    .line 318
    :cond_5
    aget-byte v9, p3, v8

    iget v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    ushr-int v10, v4, v10

    int-to-byte v10, v10

    add-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, p3, v8

    .line 319
    sub-int v1, v7, v8

    .line 320
    .local v1, "bytesLeft":I
    const/4 v9, 0x1

    if-le v1, v9, :cond_6

    .line 321
    const/4 v9, 0x2

    iget v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->numBytes:I

    if-ne v9, v10, :cond_7

    .line 322
    add-int/lit8 v9, v8, 0x1

    iget-short v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalMask:S

    and-int/2addr v10, v4

    iget v11, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalShift:I

    ushr-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, p3, v9

    .line 331
    .end local v1    # "bytesLeft":I
    .end local v2    # "caseNum":I
    .end local v3    # "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    .end local v4    # "inputChar":S
    .end local v5    # "inputCharNum":I
    .end local v8    # "outputByteNum":I
    :cond_6
    :goto_3
    return-void

    .line 324
    .restart local v1    # "bytesLeft":I
    .restart local v2    # "caseNum":I
    .restart local v3    # "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    .restart local v4    # "inputChar":S
    .restart local v5    # "inputCharNum":I
    .restart local v8    # "outputByteNum":I
    :cond_7
    add-int/lit8 v9, v8, 0x1

    iget-short v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->middleMask:S

    and-int/2addr v10, v4

    iget v11, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->middleShift:I

    ushr-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, p3, v9

    .line 325
    const/4 v9, 0x2

    if-le v1, v9, :cond_6

    .line 326
    add-int/lit8 v9, v8, 0x2

    iget-short v10, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalMask:S

    and-int/2addr v10, v4

    iget v11, v3, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalShift:I

    shl-int/2addr v10, v11

    int-to-byte v10, v10

    aput-byte v10, p3, v9

    goto :goto_3
.end method

.method public static encode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;
    .locals 3
    .param p0, "input"    # Ljava/nio/ByteBuffer;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 367
    invoke-static {p0}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->getEncodedLength(Ljava/nio/ByteBuffer;)I

    move-result v2

    new-array v1, v2, [C

    .line 368
    .local v1, "outputArray":[C
    invoke-static {v1}, Ljava/nio/CharBuffer;->wrap([C)Ljava/nio/CharBuffer;

    move-result-object v0

    .line 369
    .local v0, "output":Ljava/nio/CharBuffer;
    invoke-static {p0, v0}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->encode(Ljava/nio/ByteBuffer;Ljava/nio/CharBuffer;)V

    .line 370
    return-object v0
.end method

.method public static encode(Ljava/nio/ByteBuffer;Ljava/nio/CharBuffer;)V
    .locals 6
    .param p0, "input"    # Ljava/nio/ByteBuffer;
    .param p1, "output"    # Ljava/nio/CharBuffer;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 167
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/nio/CharBuffer;->hasArray()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v1

    .line 169
    .local v1, "inputOffset":I
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    sub-int v2, v0, v1

    .line 170
    .local v2, "inputLength":I
    invoke-virtual {p1}, Ljava/nio/CharBuffer;->arrayOffset()I

    move-result v4

    .line 171
    .local v4, "outputOffset":I
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->getEncodedLength([BII)I

    move-result v5

    .line 173
    .local v5, "outputLength":I
    add-int v0, v5, v4

    invoke-virtual {p1, v0}, Ljava/nio/CharBuffer;->limit(I)Ljava/nio/Buffer;

    .line 174
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/nio/CharBuffer;->position(I)Ljava/nio/Buffer;

    .line 175
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-virtual {p1}, Ljava/nio/CharBuffer;->array()[C

    move-result-object v3

    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->encode([BII[CII)V

    .line 180
    return-void

    .line 178
    .end local v1    # "inputOffset":I
    .end local v2    # "inputLength":I
    .end local v4    # "outputOffset":I
    .end local v5    # "outputLength":I
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Arguments must have backing arrays"

    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static encode([BII[CII)V
    .locals 9
    .param p0, "inputArray"    # [B
    .param p1, "inputOffset"    # I
    .param p2, "inputLength"    # I
    .param p3, "outputArray"    # [C
    .param p4, "outputOffset"    # I
    .param p5, "outputLength"    # I

    .prologue
    const/4 v5, 0x1

    .line 196
    sget-boolean v6, Lorg/apache/lucene/util/IndexableBinaryStringTools;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    invoke-static {p0, p1, p2}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->getEncodedLength([BII)I

    move-result v6

    if-eq p5, v6, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 198
    :cond_0
    if-lez p2, :cond_4

    .line 199
    move v2, p1

    .line 200
    .local v2, "inputByteNum":I
    const/4 v0, 0x0

    .line 201
    .local v0, "caseNum":I
    move v3, p4

    .line 203
    .local v3, "outputCharNum":I
    :goto_0
    sget-object v6, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    aget-object v6, v6, v0

    iget v6, v6, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->numBytes:I

    add-int/2addr v6, v2

    if-gt v6, p2, :cond_3

    .line 204
    sget-object v6, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    aget-object v1, v6, v0

    .line 205
    .local v1, "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    const/4 v6, 0x2

    iget v7, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->numBytes:I

    if-ne v6, v7, :cond_2

    .line 206
    aget-byte v6, p0, v2

    and-int/lit16 v6, v6, 0xff

    iget v7, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    shl-int/2addr v6, v7

    add-int/lit8 v7, v2, 0x1

    aget-byte v7, p0, v7

    and-int/lit16 v7, v7, 0xff

    iget v8, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalShift:I

    ushr-int/2addr v7, v8

    iget-short v8, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalMask:S

    and-int/2addr v7, v8

    add-int/2addr v6, v7

    and-int/lit16 v6, v6, 0x7fff

    int-to-char v6, v6

    aput-char v6, p3, v3

    .line 213
    :goto_1
    iget v6, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->advanceBytes:I

    add-int/2addr v2, v6

    .line 214
    add-int/lit8 v0, v0, 0x1

    sget-object v6, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    array-length v6, v6

    if-ne v0, v6, :cond_1

    .line 215
    const/4 v0, 0x0

    .line 203
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 209
    :cond_2
    aget-byte v6, p0, v2

    and-int/lit16 v6, v6, 0xff

    iget v7, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    shl-int/2addr v6, v7

    add-int/lit8 v7, v2, 0x1

    aget-byte v7, p0, v7

    and-int/lit16 v7, v7, 0xff

    iget v8, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->middleShift:I

    shl-int/2addr v7, v8

    add-int/2addr v6, v7

    add-int/lit8 v7, v2, 0x2

    aget-byte v7, p0, v7

    and-int/lit16 v7, v7, 0xff

    iget v8, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalShift:I

    ushr-int/2addr v7, v8

    iget-short v8, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->finalMask:S

    and-int/2addr v7, v8

    add-int/2addr v6, v7

    and-int/lit16 v6, v6, 0x7fff

    int-to-char v6, v6

    aput-char v6, p3, v3

    goto :goto_1

    .line 219
    .end local v1    # "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    :cond_3
    sget-object v6, Lorg/apache/lucene/util/IndexableBinaryStringTools;->CODING_CASES:[Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;

    aget-object v1, v6, v0

    .line 221
    .restart local v1    # "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    add-int/lit8 v6, v2, 0x1

    if-ge v6, p2, :cond_5

    .line 222
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "outputCharNum":I
    .local v4, "outputCharNum":I
    aget-byte v6, p0, v2

    and-int/lit16 v6, v6, 0xff

    iget v7, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    shl-int/2addr v6, v7

    add-int/lit8 v7, v2, 0x1

    aget-byte v7, p0, v7

    and-int/lit16 v7, v7, 0xff

    iget v8, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->middleShift:I

    shl-int/2addr v7, v8

    add-int/2addr v6, v7

    and-int/lit16 v6, v6, 0x7fff

    int-to-char v6, v6

    aput-char v6, p3, v3

    .line 224
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "outputCharNum":I
    .restart local v3    # "outputCharNum":I
    aput-char v5, p3, v4

    .line 234
    .end local v0    # "caseNum":I
    .end local v1    # "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    .end local v2    # "inputByteNum":I
    .end local v3    # "outputCharNum":I
    :cond_4
    :goto_2
    return-void

    .line 225
    .restart local v0    # "caseNum":I
    .restart local v1    # "codingCase":Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;
    .restart local v2    # "inputByteNum":I
    .restart local v3    # "outputCharNum":I
    :cond_5
    if-ge v2, p2, :cond_7

    .line 226
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "outputCharNum":I
    .restart local v4    # "outputCharNum":I
    aget-byte v6, p0, v2

    and-int/lit16 v6, v6, 0xff

    iget v7, v1, Lorg/apache/lucene/util/IndexableBinaryStringTools$CodingCase;->initialShift:I

    shl-int/2addr v6, v7

    and-int/lit16 v6, v6, 0x7fff

    int-to-char v6, v6

    aput-char v6, p3, v3

    .line 228
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "outputCharNum":I
    .restart local v3    # "outputCharNum":I
    if-nez v0, :cond_6

    :goto_3
    aput-char v5, p3, v4

    goto :goto_2

    :cond_6
    const/4 v5, 0x0

    goto :goto_3

    .line 231
    :cond_7
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "outputCharNum":I
    .restart local v4    # "outputCharNum":I
    aput-char v5, p3, v3

    goto :goto_2
.end method

.method public static getDecodedLength(Ljava/nio/CharBuffer;)I
    .locals 4
    .param p0, "encoded"    # Ljava/nio/CharBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 124
    invoke-virtual {p0}, Ljava/nio/CharBuffer;->hasArray()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {p0}, Ljava/nio/CharBuffer;->array()[C

    move-result-object v0

    invoke-virtual {p0}, Ljava/nio/CharBuffer;->arrayOffset()I

    move-result v1

    invoke-virtual {p0}, Ljava/nio/CharBuffer;->limit()I

    move-result v2

    invoke-virtual {p0}, Ljava/nio/CharBuffer;->arrayOffset()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->getDecodedLength([CII)I

    move-result v0

    return v0

    .line 128
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "encoded argument must have a backing array"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getDecodedLength([CII)I
    .locals 10
    .param p0, "encoded"    # [C
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 141
    add-int/lit8 v0, p2, -0x1

    .line 142
    .local v0, "numChars":I
    if-gtz v0, :cond_0

    .line 143
    const/4 v1, 0x0

    .line 148
    :goto_0
    return v1

    .line 146
    :cond_0
    add-int v1, p1, p2

    add-int/lit8 v1, v1, -0x1

    aget-char v1, p0, v1

    int-to-long v4, v1

    .line 147
    .local v4, "numFullBytesInFinalChar":J
    add-int/lit8 v1, v0, -0x1

    int-to-long v2, v1

    .line 148
    .local v2, "numEncodedChars":J
    const-wide/16 v6, 0xf

    mul-long/2addr v6, v2

    const-wide/16 v8, 0x7

    add-long/2addr v6, v8

    const-wide/16 v8, 0x8

    div-long/2addr v6, v8

    add-long/2addr v6, v4

    long-to-int v1, v6

    goto :goto_0
.end method

.method public static getEncodedLength(Ljava/nio/ByteBuffer;)I
    .locals 4
    .param p0, "original"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v1

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/IndexableBinaryStringTools;->getEncodedLength([BII)I

    move-result v0

    return v0

    .line 92
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "original argument must have a backing array"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getEncodedLength([BII)I
    .locals 4
    .param p0, "inputArray"    # [B
    .param p1, "inputOffset"    # I
    .param p2, "inputLength"    # I

    .prologue
    .line 107
    const-wide/16 v0, 0x8

    int-to-long v2, p2

    mul-long/2addr v0, v2

    const-wide/16 v2, 0xe

    add-long/2addr v0, v2

    const-wide/16 v2, 0xf

    div-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
