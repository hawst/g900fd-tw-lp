.class public Lorg/apache/lucene/util/OpenBitSet;
.super Lorg/apache/lucene/search/DocIdSet;
.source "OpenBitSet.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/util/Bits;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected bits:[J

.field private numBits:J

.field protected wlen:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-class v0, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 96
    const-wide/16 v0, 0x40

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/util/OpenBitSet;-><init>(J)V

    .line 97
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "numBits"    # J

    .prologue
    .line 89
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 90
    iput-wide p1, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    .line 91
    invoke-static {p1, p2}, Lorg/apache/lucene/util/OpenBitSet;->bits2words(J)I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 92
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    array-length v0, v0

    iput v0, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    .line 93
    return-void
.end method

.method public constructor <init>([JI)V
    .locals 2
    .param p1, "bits"    # [J
    .param p2, "numWords"    # I

    .prologue
    .line 112
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 113
    iput-object p1, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 114
    iput p2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    .line 115
    iget v0, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    mul-int/lit8 v0, v0, 0x40

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    .line 116
    return-void
.end method

.method public static andNotCount(Lorg/apache/lucene/util/OpenBitSet;Lorg/apache/lucene/util/OpenBitSet;)J
    .locals 7
    .param p0, "a"    # Lorg/apache/lucene/util/OpenBitSet;
    .param p1, "b"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 598
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget-object v3, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    const/4 v4, 0x0

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v6, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Lorg/apache/lucene/util/BitUtil;->pop_andnot([J[JII)J

    move-result-wide v0

    .line 599
    .local v0, "tot":J
    iget v2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-le v2, v3, :cond_0

    .line 600
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/util/BitUtil;->pop_array([JII)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 602
    :cond_0
    return-wide v0
.end method

.method public static bits2words(J)I
    .locals 6
    .param p0, "numBits"    # J

    .prologue
    const-wide/16 v4, 0x1

    .line 861
    sub-long v0, p0, v4

    const/4 v2, 0x6

    ushr-long/2addr v0, v2

    add-long/2addr v0, v4

    long-to-int v0, v0

    return v0
.end method

.method public static intersectionCount(Lorg/apache/lucene/util/OpenBitSet;Lorg/apache/lucene/util/OpenBitSet;)J
    .locals 5
    .param p0, "a"    # Lorg/apache/lucene/util/OpenBitSet;
    .param p1, "b"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 577
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget-object v1, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/util/BitUtil;->pop_intersect([J[JII)J

    move-result-wide v0

    return-wide v0
.end method

.method public static unionCount(Lorg/apache/lucene/util/OpenBitSet;Lorg/apache/lucene/util/OpenBitSet;)J
    .locals 7
    .param p0, "a"    # Lorg/apache/lucene/util/OpenBitSet;
    .param p1, "b"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 584
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget-object v3, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    const/4 v4, 0x0

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v6, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Lorg/apache/lucene/util/BitUtil;->pop_union([J[JII)J

    move-result-wide v0

    .line 585
    .local v0, "tot":J
    iget v2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v2, v3, :cond_1

    .line 586
    iget-object v2, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget v3, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/util/BitUtil;->pop_array([JII)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 590
    :cond_0
    :goto_0
    return-wide v0

    .line 587
    :cond_1
    iget v2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-le v2, v3, :cond_0

    .line 588
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/util/BitUtil;->pop_array([JII)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public static xorCount(Lorg/apache/lucene/util/OpenBitSet;Lorg/apache/lucene/util/OpenBitSet;)J
    .locals 7
    .param p0, "a"    # Lorg/apache/lucene/util/OpenBitSet;
    .param p1, "b"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 609
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget-object v3, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    const/4 v4, 0x0

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v6, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Lorg/apache/lucene/util/BitUtil;->pop_xor([J[JII)J

    move-result-wide v0

    .line 610
    .local v0, "tot":J
    iget v2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v2, v3, :cond_1

    .line 611
    iget-object v2, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget v3, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/util/BitUtil;->pop_array([JII)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 615
    :cond_0
    :goto_0
    return-wide v0

    .line 612
    :cond_1
    iget v2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-le v2, v3, :cond_0

    .line 613
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget v3, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    sub-int/2addr v4, v5

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/util/BitUtil;->pop_array([JII)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0
.end method


# virtual methods
.method public and(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 0
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 808
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/OpenBitSet;->intersect(Lorg/apache/lucene/util/OpenBitSet;)V

    .line 809
    return-void
.end method

.method public andNot(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 0
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 818
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/OpenBitSet;->remove(Lorg/apache/lucene/util/OpenBitSet;)V

    .line 819
    return-void
.end method

.method public capacity()J
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x6

    int-to-long v0, v0

    return-wide v0
.end method

.method public cardinality()J
    .locals 3

    .prologue
    .line 570
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/BitUtil;->pop_array([JII)J

    move-result-wide v0

    return-wide v0
.end method

.method public clear(II)V
    .locals 12
    .param p1, "startIndex"    # I
    .param p2, "endIndex"    # I

    .prologue
    const-wide/16 v8, -0x1

    .line 358
    if-gt p2, p1, :cond_1

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 360
    :cond_1
    shr-int/lit8 v4, p1, 0x6

    .line 361
    .local v4, "startWord":I
    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v4, v5, :cond_0

    .line 365
    add-int/lit8 v5, p2, -0x1

    shr-int/lit8 v0, v5, 0x6

    .line 367
    .local v0, "endWord":I
    shl-long v6, v8, p1

    .line 368
    .local v6, "startmask":J
    neg-int v5, p2

    ushr-long v2, v8, v5

    .line 371
    .local v2, "endmask":J
    xor-long/2addr v6, v8

    .line 372
    xor-long/2addr v2, v8

    .line 374
    if-ne v4, v0, :cond_2

    .line 375
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v8, v5, v4

    or-long v10, v6, v2

    and-long/2addr v8, v10

    aput-wide v8, v5, v4

    goto :goto_0

    .line 379
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v8, v5, v4

    and-long/2addr v8, v6

    aput-wide v8, v5, v4

    .line 381
    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 382
    .local v1, "middle":I
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    add-int/lit8 v8, v4, 0x1

    const-wide/16 v10, 0x0

    invoke-static {v5, v8, v1, v10, v11}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 383
    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v0, v5, :cond_0

    .line 384
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v8, v5, v0

    and-long/2addr v8, v2

    aput-wide v8, v5, v0

    goto :goto_0
.end method

.method public clear(J)V
    .locals 11
    .param p1, "index"    # J

    .prologue
    .line 345
    const/4 v4, 0x6

    shr-long v4, p1, v4

    long-to-int v1, v4

    .line 346
    .local v1, "wordNum":I
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v1, v4, :cond_0

    .line 350
    :goto_0
    return-void

    .line 347
    :cond_0
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 348
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 349
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    const-wide/16 v8, -0x1

    xor-long/2addr v8, v2

    and-long/2addr v6, v8

    aput-wide v6, v4, v1

    goto :goto_0
.end method

.method public clear(JJ)V
    .locals 15
    .param p1, "startIndex"    # J
    .param p3, "endIndex"    # J

    .prologue
    .line 395
    cmp-long v7, p3, p1

    if-gtz v7, :cond_1

    .line 423
    :cond_0
    :goto_0
    return-void

    .line 397
    :cond_1
    const/4 v7, 0x6

    shr-long v10, p1, v7

    long-to-int v6, v10

    .line 398
    .local v6, "startWord":I
    iget v7, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v6, v7, :cond_0

    .line 402
    const-wide/16 v10, 0x1

    sub-long v10, p3, v10

    const/4 v7, 0x6

    shr-long/2addr v10, v7

    long-to-int v2, v10

    .line 404
    .local v2, "endWord":I
    const-wide/16 v10, -0x1

    move-wide/from16 v0, p1

    long-to-int v7, v0

    shl-long v8, v10, v7

    .line 405
    .local v8, "startmask":J
    const-wide/16 v10, -0x1

    move-wide/from16 v0, p3

    neg-long v12, v0

    long-to-int v7, v12

    ushr-long v4, v10, v7

    .line 408
    .local v4, "endmask":J
    const-wide/16 v10, -0x1

    xor-long/2addr v8, v10

    .line 409
    const-wide/16 v10, -0x1

    xor-long/2addr v4, v10

    .line 411
    if-ne v6, v2, :cond_2

    .line 412
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v7, v6

    or-long v12, v8, v4

    and-long/2addr v10, v12

    aput-wide v10, v7, v6

    goto :goto_0

    .line 416
    :cond_2
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v7, v6

    and-long/2addr v10, v8

    aput-wide v10, v7, v6

    .line 418
    iget v7, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v7, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 419
    .local v3, "middle":I
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    add-int/lit8 v10, v6, 0x1

    const-wide/16 v12, 0x0

    invoke-static {v7, v10, v3, v12, v13}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 420
    iget v7, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v2, v7, :cond_0

    .line 421
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v7, v2

    and-long/2addr v10, v4

    aput-wide v10, v7, v2

    goto :goto_0
.end method

.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 731
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/OpenBitSet;

    .line 732
    .local v1, "obs":Lorg/apache/lucene/util/OpenBitSet;
    iget-object v2, v1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    invoke-virtual {v2}, [J->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [J

    iput-object v2, v1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 733
    return-object v1

    .line 734
    .end local v1    # "obs":Lorg/apache/lucene/util/OpenBitSet;
    :catch_0
    move-exception v0

    .line 735
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public ensureCapacity(J)V
    .locals 1
    .param p1, "numBits"    # J

    .prologue
    .line 847
    invoke-static {p1, p2}, Lorg/apache/lucene/util/OpenBitSet;->bits2words(J)I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/OpenBitSet;->ensureCapacityWords(I)V

    .line 848
    return-void
.end method

.method public ensureCapacityWords(I)V
    .locals 1
    .param p1, "numWords"    # I

    .prologue
    .line 838
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    array-length v0, v0

    if-ge v0, p1, :cond_0

    .line 839
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    invoke-static {v0, p1}, Lorg/apache/lucene/util/ArrayUtil;->grow([JI)[J

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 841
    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 868
    if-ne p0, p1, :cond_1

    .line 888
    :cond_0
    :goto_0
    return v3

    .line 869
    :cond_1
    instance-of v5, p1, Lorg/apache/lucene/util/OpenBitSet;

    if-nez v5, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    move-object v1, p1

    .line 871
    check-cast v1, Lorg/apache/lucene/util/OpenBitSet;

    .line 873
    .local v1, "b":Lorg/apache/lucene/util/OpenBitSet;
    iget v5, v1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v6, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-le v5, v6, :cond_3

    .line 874
    move-object v0, v1

    .local v0, "a":Lorg/apache/lucene/util/OpenBitSet;
    move-object v1, p0

    .line 880
    :goto_1
    iget v5, v0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    add-int/lit8 v2, v5, -0x1

    .local v2, "i":I
    :goto_2
    iget v5, v1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v2, v5, :cond_5

    .line 881
    iget-object v5, v0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_4

    move v3, v4

    goto :goto_0

    .line 876
    .end local v0    # "a":Lorg/apache/lucene/util/OpenBitSet;
    .end local v2    # "i":I
    :cond_3
    move-object v0, p0

    .restart local v0    # "a":Lorg/apache/lucene/util/OpenBitSet;
    goto :goto_1

    .line 880
    .restart local v2    # "i":I
    :cond_4
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 884
    :cond_5
    iget v5, v1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    add-int/lit8 v2, v5, -0x1

    :goto_3
    if-ltz v2, :cond_0

    .line 885
    iget-object v5, v0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v2

    iget-object v5, v1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v8, v5, v2

    cmp-long v5, v6, v8

    if-eqz v5, :cond_6

    move v3, v4

    goto :goto_0

    .line 884
    :cond_6
    add-int/lit8 v2, v2, -0x1

    goto :goto_3
.end method

.method protected expandingWordNum(J)I
    .locals 7
    .param p1, "index"    # J

    .prologue
    const-wide/16 v4, 0x1

    .line 304
    const/4 v1, 0x6

    shr-long v2, p1, v1

    long-to-int v0, v2

    .line 305
    .local v0, "wordNum":I
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v0, v1, :cond_0

    .line 306
    add-long v2, p1, v4

    invoke-virtual {p0, v2, v3}, Lorg/apache/lucene/util/OpenBitSet;->ensureCapacity(J)V

    .line 307
    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    .line 309
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-wide v2, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    add-long/2addr v4, p1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 310
    :cond_1
    return v0
.end method

.method public fastClear(I)V
    .locals 10
    .param p1, "index"    # I

    .prologue
    .line 318
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    int-to-long v4, p1

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 319
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 320
    .local v1, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 321
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 322
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    const-wide/16 v8, -0x1

    xor-long/2addr v8, v2

    and-long/2addr v6, v8

    aput-wide v6, v4, v1

    .line 330
    return-void
.end method

.method public fastClear(J)V
    .locals 11
    .param p1, "index"    # J

    .prologue
    .line 336
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-ltz v4, :cond_0

    iget-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 337
    :cond_1
    const/4 v4, 0x6

    shr-long v4, p1, v4

    long-to-int v1, v4

    .line 338
    .local v1, "wordNum":I
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 339
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 340
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    const-wide/16 v8, -0x1

    xor-long/2addr v8, v2

    and-long/2addr v6, v8

    aput-wide v6, v4, v1

    .line 341
    return-void
.end method

.method public fastFlip(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 457
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    int-to-long v4, p1

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 458
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 459
    .local v1, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 460
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 461
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    xor-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 462
    return-void
.end method

.method public fastFlip(J)V
    .locals 9
    .param p1, "index"    # J

    .prologue
    .line 468
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-ltz v4, :cond_0

    iget-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 469
    :cond_1
    const/4 v4, 0x6

    shr-long v4, p1, v4

    long-to-int v1, v4

    .line 470
    .local v1, "wordNum":I
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 471
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 472
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    xor-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 473
    return-void
.end method

.method public fastGet(I)Z
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 178
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    int-to-long v4, p1

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 179
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 182
    .local v1, "i":I
    and-int/lit8 v0, p1, 0x3f

    .line 183
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 184
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v4, v4, v1

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public fastGet(J)Z
    .locals 9
    .param p1, "index"    # J

    .prologue
    const-wide/16 v6, 0x0

    .line 203
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    cmp-long v4, p1, v6

    if-ltz v4, :cond_0

    iget-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 204
    :cond_1
    const/4 v4, 0x6

    shr-long v4, p1, v4

    long-to-int v1, v4

    .line 205
    .local v1, "i":I
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 206
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 207
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v4, v4, v1

    and-long/2addr v4, v2

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public fastSet(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 256
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    int-to-long v4, p1

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 257
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 258
    .local v1, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 259
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 260
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    or-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 261
    return-void
.end method

.method public fastSet(J)V
    .locals 9
    .param p1, "index"    # J

    .prologue
    .line 267
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-ltz v4, :cond_0

    iget-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 268
    :cond_1
    const/4 v4, 0x6

    shr-long v4, p1, v4

    long-to-int v1, v4

    .line 269
    .local v1, "wordNum":I
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 270
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 271
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    or-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 272
    return-void
.end method

.method public flip(J)V
    .locals 9
    .param p1, "index"    # J

    .prologue
    .line 477
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/OpenBitSet;->expandingWordNum(J)I

    move-result v1

    .line 478
    .local v1, "wordNum":I
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 479
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 480
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    xor-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 481
    return-void
.end method

.method public flip(JJ)V
    .locals 15
    .param p1, "startIndex"    # J
    .param p3, "endIndex"    # J

    .prologue
    .line 513
    cmp-long v7, p3, p1

    if-gtz v7, :cond_0

    .line 542
    :goto_0
    return-void

    .line 514
    :cond_0
    const/4 v7, 0x6

    shr-long v10, p1, v7

    long-to-int v6, v10

    .line 518
    .local v6, "startWord":I
    const-wide/16 v10, 0x1

    sub-long v10, p3, v10

    invoke-virtual {p0, v10, v11}, Lorg/apache/lucene/util/OpenBitSet;->expandingWordNum(J)I

    move-result v2

    .line 527
    .local v2, "endWord":I
    const-wide/16 v10, -0x1

    move-wide/from16 v0, p1

    long-to-int v7, v0

    shl-long v8, v10, v7

    .line 528
    .local v8, "startmask":J
    const-wide/16 v10, -0x1

    move-wide/from16 v0, p3

    neg-long v12, v0

    long-to-int v7, v12

    ushr-long v4, v10, v7

    .line 530
    .local v4, "endmask":J
    if-ne v6, v2, :cond_1

    .line 531
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v7, v6

    and-long v12, v8, v4

    xor-long/2addr v10, v12

    aput-wide v10, v7, v6

    goto :goto_0

    .line 535
    :cond_1
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v7, v6

    xor-long/2addr v10, v8

    aput-wide v10, v7, v6

    .line 537
    add-int/lit8 v3, v6, 0x1

    .local v3, "i":I
    :goto_1
    if-ge v3, v2, :cond_2

    .line 538
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget-object v10, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v10, v3

    const-wide/16 v12, -0x1

    xor-long/2addr v10, v12

    aput-wide v10, v7, v3

    .line 537
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 541
    :cond_2
    iget-object v7, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v7, v2

    xor-long/2addr v10, v4

    aput-wide v10, v7, v2

    goto :goto_0
.end method

.method public flipAndGet(I)Z
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 487
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ltz p1, :cond_0

    int-to-long v4, p1

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 488
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 489
    .local v1, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 490
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 491
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    xor-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 492
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v4, v4, v1

    and-long/2addr v4, v2

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public flipAndGet(J)Z
    .locals 11
    .param p1, "index"    # J

    .prologue
    const-wide/16 v8, 0x0

    .line 499
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    cmp-long v4, p1, v8

    if-ltz v4, :cond_0

    iget-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_1

    :cond_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 500
    :cond_1
    const/4 v4, 0x6

    shr-long v4, p1, v4

    long-to-int v1, v4

    .line 501
    .local v1, "wordNum":I
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 502
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 503
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    xor-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 504
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v4, v4, v1

    and-long/2addr v4, v2

    cmp-long v4, v4, v8

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public get(I)Z
    .locals 10
    .param p1, "index"    # I

    .prologue
    const/4 v4, 0x0

    .line 163
    shr-int/lit8 v1, p1, 0x6

    .line 166
    .local v1, "i":I
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    array-length v5, v5

    if-lt v1, v5, :cond_1

    .line 170
    :cond_0
    :goto_0
    return v4

    .line 168
    :cond_1
    and-int/lit8 v0, p1, 0x3f

    .line 169
    .local v0, "bit":I
    const-wide/16 v6, 0x1

    shl-long v2, v6, v0

    .line 170
    .local v2, "bitmask":J
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v1

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    const/4 v4, 0x1

    goto :goto_0
.end method

.method public get(J)Z
    .locals 11
    .param p1, "index"    # J

    .prologue
    const/4 v4, 0x0

    .line 192
    const/4 v5, 0x6

    shr-long v6, p1, v5

    long-to-int v1, v6

    .line 193
    .local v1, "i":I
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    array-length v5, v5

    if-lt v1, v5, :cond_1

    .line 196
    :cond_0
    :goto_0
    return v4

    .line 194
    :cond_1
    long-to-int v5, p1

    and-int/lit8 v0, v5, 0x3f

    .line 195
    .local v0, "bit":I
    const-wide/16 v6, 0x1

    shl-long v2, v6, v0

    .line 196
    .local v2, "bitmask":J
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v1

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    const/4 v4, 0x1

    goto :goto_0
.end method

.method public getAndSet(I)Z
    .locals 10
    .param p1, "index"    # I

    .prologue
    .line 431
    sget-boolean v5, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    if-ltz p1, :cond_0

    int-to-long v6, p1

    iget-wide v8, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v5, v6, v8

    if-ltz v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 432
    :cond_1
    shr-int/lit8 v4, p1, 0x6

    .line 433
    .local v4, "wordNum":I
    and-int/lit8 v0, p1, 0x3f

    .line 434
    .local v0, "bit":I
    const-wide/16 v6, 0x1

    shl-long v2, v6, v0

    .line 435
    .local v2, "bitmask":J
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v4

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    const/4 v1, 0x1

    .line 436
    .local v1, "val":Z
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v4

    or-long/2addr v6, v2

    aput-wide v6, v5, v4

    .line 437
    return v1

    .line 435
    .end local v1    # "val":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAndSet(J)Z
    .locals 11
    .param p1, "index"    # J

    .prologue
    const-wide/16 v8, 0x0

    .line 444
    sget-boolean v5, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    cmp-long v5, p1, v8

    if-ltz v5, :cond_0

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v5, p1, v6

    if-ltz v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 445
    :cond_1
    const/4 v5, 0x6

    shr-long v6, p1, v5

    long-to-int v4, v6

    .line 446
    .local v4, "wordNum":I
    long-to-int v5, p1

    and-int/lit8 v0, v5, 0x3f

    .line 447
    .local v0, "bit":I
    const-wide/16 v6, 0x1

    shl-long v2, v6, v0

    .line 448
    .local v2, "bitmask":J
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v4

    and-long/2addr v6, v2

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    const/4 v1, 0x1

    .line 449
    .local v1, "val":Z
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v4

    or-long/2addr v6, v2

    aput-wide v6, v5, v4

    .line 450
    return v1

    .line 448
    .end local v1    # "val":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBit(I)I
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 227
    sget-boolean v2, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    if-ltz p1, :cond_0

    int-to-long v2, p1

    iget-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 228
    :cond_1
    shr-int/lit8 v1, p1, 0x6

    .line 229
    .local v1, "i":I
    and-int/lit8 v0, p1, 0x3f

    .line 230
    .local v0, "bit":I
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v2, v1

    ushr-long/2addr v2, v0

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1

    return v2
.end method

.method public getBits()[J
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    return-object v0
.end method

.method public getNumWords()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    return v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 896
    const-wide/16 v0, 0x0

    .line 897
    .local v0, "h":J
    iget-object v3, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    array-length v2, v3

    .local v2, "i":I
    :goto_0
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_0

    .line 898
    iget-object v3, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v4, v3, v2

    xor-long/2addr v0, v4

    .line 899
    const/4 v3, 0x1

    shl-long v4, v0, v3

    const/16 v3, 0x3f

    ushr-long v6, v0, v3

    or-long v0, v4, v6

    goto :goto_0

    .line 903
    :cond_0
    const/16 v3, 0x20

    shr-long v4, v0, v3

    xor-long/2addr v4, v0

    long-to-int v3, v4

    const v4, -0x6789edcc

    add-int/2addr v3, v4

    return v3
.end method

.method public intersect(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 8
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 741
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 742
    .local v0, "newLen":I
    iget-object v3, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 743
    .local v3, "thisArr":[J
    iget-object v1, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 745
    .local v1, "otherArr":[J
    move v2, v0

    .line 746
    .local v2, "pos":I
    :goto_0
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_0

    .line 747
    aget-wide v4, v3, v2

    aget-wide v6, v1, v2

    and-long/2addr v4, v6

    aput-wide v4, v3, v2

    goto :goto_0

    .line 749
    :cond_0
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-le v4, v0, :cond_1

    .line 751
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    const-wide/16 v6, 0x0

    invoke-static {v4, v0, v5, v6, v7}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 753
    :cond_1
    iput v0, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    .line 754
    return-void
.end method

.method public intersects(Lorg/apache/lucene/util/OpenBitSet;)Z
    .locals 8
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 823
    iget v3, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 824
    .local v1, "pos":I
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 825
    .local v2, "thisArr":[J
    iget-object v0, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 826
    .local v0, "otherArr":[J
    :cond_0
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_1

    .line 827
    aget-wide v4, v2, v1

    aget-wide v6, v0, v1

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    .line 829
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 4

    .prologue
    .line 145
    invoke-virtual {p0}, Lorg/apache/lucene/util/OpenBitSet;->cardinality()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Lorg/apache/lucene/search/DocIdSetIterator;
    .locals 3

    .prologue
    .line 120
    new-instance v0, Lorg/apache/lucene/util/OpenBitSetIterator;

    iget-object v1, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    iget v2, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/OpenBitSetIterator;-><init>([JI)V

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x6

    return v0
.end method

.method public nextSetBit(I)I
    .locals 10
    .param p1, "index"    # I

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, -0x1

    .line 623
    shr-int/lit8 v0, p1, 0x6

    .line 624
    .local v0, "i":I
    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v0, v5, :cond_1

    .line 637
    :cond_0
    :goto_0
    return v4

    .line 625
    :cond_1
    and-int/lit8 v1, p1, 0x3f

    .line 626
    .local v1, "subIndex":I
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v0

    shr-long v2, v6, v1

    .line 628
    .local v2, "word":J
    cmp-long v5, v2, v8

    if-eqz v5, :cond_2

    .line 629
    shl-int/lit8 v4, v0, 0x6

    add-int/2addr v4, v1

    invoke-static {v2, v3}, Lorg/apache/lucene/util/BitUtil;->ntz(J)I

    move-result v5

    add-int/2addr v4, v5

    goto :goto_0

    .line 632
    :cond_2
    add-int/lit8 v0, v0, 0x1

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v0, v5, :cond_0

    .line 633
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v5, v0

    .line 634
    cmp-long v5, v2, v8

    if-eqz v5, :cond_2

    shl-int/lit8 v4, v0, 0x6

    invoke-static {v2, v3}, Lorg/apache/lucene/util/BitUtil;->ntz(J)I

    move-result v5

    add-int/2addr v4, v5

    goto :goto_0
.end method

.method public nextSetBit(J)J
    .locals 13
    .param p1, "index"    # J

    .prologue
    const-wide/16 v10, 0x0

    const-wide/16 v4, -0x1

    const/4 v8, 0x6

    .line 644
    ushr-long v6, p1, v8

    long-to-int v0, v6

    .line 645
    .local v0, "i":I
    iget v6, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v0, v6, :cond_1

    .line 658
    :cond_0
    :goto_0
    return-wide v4

    .line 646
    :cond_1
    long-to-int v6, p1

    and-int/lit8 v1, v6, 0x3f

    .line 647
    .local v1, "subIndex":I
    iget-object v6, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v6, v0

    ushr-long v2, v6, v1

    .line 649
    .local v2, "word":J
    cmp-long v6, v2, v10

    if-eqz v6, :cond_2

    .line 650
    int-to-long v4, v0

    shl-long/2addr v4, v8

    invoke-static {v2, v3}, Lorg/apache/lucene/util/BitUtil;->ntz(J)I

    move-result v6

    add-int/2addr v6, v1

    int-to-long v6, v6

    add-long/2addr v4, v6

    goto :goto_0

    .line 653
    :cond_2
    add-int/lit8 v0, v0, 0x1

    iget v6, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v0, v6, :cond_0

    .line 654
    iget-object v6, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v6, v0

    .line 655
    cmp-long v6, v2, v10

    if-eqz v6, :cond_2

    int-to-long v4, v0

    shl-long/2addr v4, v8

    invoke-static {v2, v3}, Lorg/apache/lucene/util/BitUtil;->ntz(J)I

    move-result v6

    int-to-long v6, v6

    add-long/2addr v4, v6

    goto :goto_0
.end method

.method public or(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 0
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 813
    invoke-virtual {p0, p1}, Lorg/apache/lucene/util/OpenBitSet;->union(Lorg/apache/lucene/util/OpenBitSet;)V

    .line 814
    return-void
.end method

.method public prevSetBit(I)I
    .locals 10
    .param p1, "index"    # I

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, -0x1

    .line 667
    shr-int/lit8 v0, p1, 0x6

    .line 670
    .local v0, "i":I
    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v0, v5, :cond_2

    .line 671
    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    add-int/lit8 v0, v5, -0x1

    .line 672
    if-gez v0, :cond_1

    .line 692
    :cond_0
    :goto_0
    return v4

    .line 673
    :cond_1
    const/16 v1, 0x3f

    .line 674
    .local v1, "subIndex":I
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v5, v0

    .line 681
    .local v2, "word":J
    :goto_1
    cmp-long v5, v2, v8

    if-eqz v5, :cond_3

    .line 682
    shl-int/lit8 v4, v0, 0x6

    add-int/2addr v4, v1

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v5

    sub-int/2addr v4, v5

    goto :goto_0

    .line 676
    .end local v1    # "subIndex":I
    .end local v2    # "word":J
    :cond_2
    if-ltz v0, :cond_0

    .line 677
    and-int/lit8 v1, p1, 0x3f

    .line 678
    .restart local v1    # "subIndex":I
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v5, v0

    rsub-int/lit8 v5, v1, 0x3f

    shl-long v2, v6, v5

    .restart local v2    # "word":J
    goto :goto_1

    .line 685
    :cond_3
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    .line 686
    iget-object v5, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v5, v0

    .line 687
    cmp-long v5, v2, v8

    if-eqz v5, :cond_3

    .line 688
    shl-int/lit8 v4, v0, 0x6

    add-int/lit8 v4, v4, 0x3f

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v5

    sub-int/2addr v4, v5

    goto :goto_0
.end method

.method public prevSetBit(J)J
    .locals 13
    .param p1, "index"    # J

    .prologue
    const-wide/16 v10, 0x0

    const-wide/16 v4, -0x1

    const/4 v9, 0x6

    .line 700
    shr-long v6, p1, v9

    long-to-int v0, v6

    .line 703
    .local v0, "i":I
    iget v6, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-lt v0, v6, :cond_2

    .line 704
    iget v6, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    add-int/lit8 v0, v6, -0x1

    .line 705
    if-gez v0, :cond_1

    .line 725
    :cond_0
    :goto_0
    return-wide v4

    .line 706
    :cond_1
    const/16 v1, 0x3f

    .line 707
    .local v1, "subIndex":I
    iget-object v6, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v6, v0

    .line 714
    .local v2, "word":J
    :goto_1
    cmp-long v6, v2, v10

    if-eqz v6, :cond_3

    .line 715
    int-to-long v4, v0

    shl-long/2addr v4, v9

    int-to-long v6, v1

    add-long/2addr v4, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v6

    int-to-long v6, v6

    sub-long/2addr v4, v6

    goto :goto_0

    .line 709
    .end local v1    # "subIndex":I
    .end local v2    # "word":J
    :cond_2
    if-ltz v0, :cond_0

    .line 710
    long-to-int v6, p1

    and-int/lit8 v1, v6, 0x3f

    .line 711
    .restart local v1    # "subIndex":I
    iget-object v6, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v6, v0

    rsub-int/lit8 v8, v1, 0x3f

    shl-long v2, v6, v8

    .restart local v2    # "word":J
    goto :goto_1

    .line 718
    :cond_3
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    .line 719
    iget-object v6, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v6, v0

    .line 720
    cmp-long v6, v2, v10

    if-eqz v6, :cond_3

    .line 721
    int-to-long v4, v0

    shl-long/2addr v4, v9

    const-wide/16 v6, 0x3f

    add-long/2addr v4, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v6

    int-to-long v6, v6

    sub-long/2addr v4, v6

    goto :goto_0
.end method

.method public remove(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 10
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 777
    iget v3, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v4, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 778
    .local v0, "idx":I
    iget-object v2, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 779
    .local v2, "thisArr":[J
    iget-object v1, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 780
    .local v1, "otherArr":[J
    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    .line 781
    aget-wide v4, v2, v0

    aget-wide v6, v1, v0

    const-wide/16 v8, -0x1

    xor-long/2addr v6, v8

    and-long/2addr v4, v6

    aput-wide v4, v2, v0

    goto :goto_0

    .line 783
    :cond_0
    return-void
.end method

.method public set(J)V
    .locals 9
    .param p1, "index"    # J

    .prologue
    .line 245
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/util/OpenBitSet;->expandingWordNum(J)I

    move-result v1

    .line 246
    .local v1, "wordNum":I
    long-to-int v4, p1

    and-int/lit8 v0, v4, 0x3f

    .line 247
    .local v0, "bit":I
    const-wide/16 v4, 0x1

    shl-long v2, v4, v0

    .line 248
    .local v2, "bitmask":J
    iget-object v4, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v6, v4, v1

    or-long/2addr v6, v2

    aput-wide v6, v4, v1

    .line 249
    return-void
.end method

.method public set(JJ)V
    .locals 15
    .param p1, "startIndex"    # J
    .param p3, "endIndex"    # J

    .prologue
    .line 280
    cmp-long v8, p3, p1

    if-gtz v8, :cond_0

    .line 299
    :goto_0
    return-void

    .line 282
    :cond_0
    const/4 v8, 0x6

    shr-long v8, p1, v8

    long-to-int v3, v8

    .line 286
    .local v3, "startWord":I
    const-wide/16 v8, 0x1

    sub-long v8, p3, v8

    invoke-virtual {p0, v8, v9}, Lorg/apache/lucene/util/OpenBitSet;->expandingWordNum(J)I

    move-result v2

    .line 288
    .local v2, "endWord":I
    const-wide/16 v8, -0x1

    move-wide/from16 v0, p1

    long-to-int v10, v0

    shl-long v6, v8, v10

    .line 289
    .local v6, "startmask":J
    const-wide/16 v8, -0x1

    move-wide/from16 v0, p3

    neg-long v10, v0

    long-to-int v10, v10

    ushr-long v4, v8, v10

    .line 291
    .local v4, "endmask":J
    if-ne v3, v2, :cond_1

    .line 292
    iget-object v8, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v8, v3

    and-long v12, v6, v4

    or-long/2addr v10, v12

    aput-wide v10, v8, v3

    goto :goto_0

    .line 296
    :cond_1
    iget-object v8, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v8, v3

    or-long/2addr v10, v6

    aput-wide v10, v8, v3

    .line 297
    iget-object v8, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    add-int/lit8 v9, v3, 0x1

    const-wide/16 v10, -0x1

    invoke-static {v8, v9, v2, v10, v11}, Ljava/util/Arrays;->fill([JIIJ)V

    .line 298
    iget-object v8, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v10, v8, v2

    or-long/2addr v10, v4

    aput-wide v10, v8, v2

    goto :goto_0
.end method

.method public setBits([J)V
    .locals 0
    .param p1, "bits"    # [J

    .prologue
    .line 151
    iput-object p1, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    return-void
.end method

.method public setNumWords(I)V
    .locals 0
    .param p1, "nWords"    # I

    .prologue
    .line 157
    iput p1, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    return-void
.end method

.method public size()J
    .locals 2

    .prologue
    .line 137
    invoke-virtual {p0}, Lorg/apache/lucene/util/OpenBitSet;->capacity()J

    move-result-wide v0

    return-wide v0
.end method

.method public trimTrailingZeros()V
    .locals 6

    .prologue
    .line 854
    iget v1, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    add-int/lit8 v0, v1, -0x1

    .line 855
    .local v0, "idx":I
    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    aget-wide v2, v1, v0

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 856
    :cond_0
    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    .line 857
    return-void
.end method

.method public union(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 8
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 758
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 759
    .local v0, "newLen":I
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/OpenBitSet;->ensureCapacityWords(I)V

    .line 760
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget-wide v4, p1, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 762
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 763
    .local v3, "thisArr":[J
    iget-object v1, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 764
    .local v1, "otherArr":[J
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 765
    .local v2, "pos":I
    :goto_0
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_1

    .line 766
    aget-wide v4, v3, v2

    aget-wide v6, v1, v2

    or-long/2addr v4, v6

    aput-wide v4, v3, v2

    goto :goto_0

    .line 768
    :cond_1
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v4, v0, :cond_2

    .line 769
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v6, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    sub-int v6, v0, v6

    invoke-static {v1, v4, v3, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 771
    :cond_2
    iput v0, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    .line 772
    return-void
.end method

.method public xor(Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 8
    .param p1, "other"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 787
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 788
    .local v0, "newLen":I
    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/OpenBitSet;->ensureCapacityWords(I)V

    .line 789
    sget-boolean v4, Lorg/apache/lucene/util/OpenBitSet;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget-wide v4, p1, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    iget-wide v6, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/util/OpenBitSet;->numBits:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 791
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 792
    .local v3, "thisArr":[J
    iget-object v1, p1, Lorg/apache/lucene/util/OpenBitSet;->bits:[J

    .line 793
    .local v1, "otherArr":[J
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p1, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 794
    .local v2, "pos":I
    :goto_0
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_1

    .line 795
    aget-wide v4, v3, v2

    aget-wide v6, v1, v2

    xor-long/2addr v4, v6

    aput-wide v4, v3, v2

    goto :goto_0

    .line 797
    :cond_1
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    if-ge v4, v0, :cond_2

    .line 798
    iget v4, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v5, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    iget v6, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    sub-int v6, v0, v6

    invoke-static {v1, v4, v3, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 800
    :cond_2
    iput v0, p0, Lorg/apache/lucene/util/OpenBitSet;->wlen:I

    .line 801
    return-void
.end method
