.class public final Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;
.super Lorg/apache/lucene/store/DataOutput;
.source "PagedBytes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/util/PagedBytes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "PagedBytesDataOutput"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/util/PagedBytes;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 464
    const-class v0, Lorg/apache/lucene/util/PagedBytes;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/PagedBytes;)V
    .locals 0

    .prologue
    .line 464
    iput-object p1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    invoke-direct {p0}, Lorg/apache/lucene/store/DataOutput;-><init>()V

    return-void
.end method


# virtual methods
.method public getPosition()J
    .locals 2

    .prologue
    .line 516
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$600(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v0

    if-nez v0, :cond_0

    .line 517
    const-wide/16 v0, 0x0

    .line 519
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$000(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v1}, Lorg/apache/lucene/util/PagedBytes;->access$400(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    mul-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v1}, Lorg/apache/lucene/util/PagedBytes;->access$500(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    add-int/2addr v0, v1

    int-to-long v0, v0

    goto :goto_0
.end method

.method public writeByte(B)V
    .locals 2
    .param p1, "b"    # B

    .prologue
    .line 467
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$500(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v1}, Lorg/apache/lucene/util/PagedBytes;->access$400(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 468
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$600(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$000(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v1}, Lorg/apache/lucene/util/PagedBytes;->access$600(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 470
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockEnd:Ljava/util/List;
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$100(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v1}, Lorg/apache/lucene/util/PagedBytes;->access$500(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 472
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v1}, Lorg/apache/lucene/util/PagedBytes;->access$400(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    new-array v1, v1, [B

    # setter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v0, v1}, Lorg/apache/lucene/util/PagedBytes;->access$602(Lorg/apache/lucene/util/PagedBytes;[B)[B

    .line 473
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    const/4 v1, 0x0

    # setter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v0, v1}, Lorg/apache/lucene/util/PagedBytes;->access$502(Lorg/apache/lucene/util/PagedBytes;I)I

    .line 475
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v0}, Lorg/apache/lucene/util/PagedBytes;->access$600(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # operator++ for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v1}, Lorg/apache/lucene/util/PagedBytes;->access$508(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v1

    aput-byte p1, v0, v1

    .line 476
    return-void
.end method

.method public writeBytes([BII)V
    .locals 6
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 480
    sget-boolean v3, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    array-length v3, p1

    add-int v4, p2, p3

    if-ge v3, v4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 481
    :cond_0
    if-nez p3, :cond_1

    .line 512
    :goto_0
    return-void

    .line 485
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$500(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$400(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    if-ne v3, v4, :cond_3

    .line 486
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$600(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v3

    if-eqz v3, :cond_2

    .line 487
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$000(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$600(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 488
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockEnd:Ljava/util/List;
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$100(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$500(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 490
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$400(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    new-array v4, v4, [B

    # setter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v3, v4}, Lorg/apache/lucene/util/PagedBytes;->access$602(Lorg/apache/lucene/util/PagedBytes;[B)[B

    .line 491
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # setter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v3, v5}, Lorg/apache/lucene/util/PagedBytes;->access$502(Lorg/apache/lucene/util/PagedBytes;I)I

    .line 494
    :cond_3
    add-int v2, p2, p3

    .line 496
    .local v2, "offsetEnd":I
    :goto_1
    sub-int v1, v2, p2

    .line 497
    .local v1, "left":I
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$400(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$500(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    sub-int v0, v3, v4

    .line 498
    .local v0, "blockLeft":I
    if-ge v0, v1, :cond_4

    .line 499
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$600(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$500(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    invoke-static {p1, p2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 500
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blocks:Ljava/util/List;
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$000(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$600(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 501
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockEnd:Ljava/util/List;
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$100(Lorg/apache/lucene/util/PagedBytes;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$400(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 502
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->blockSize:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$400(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    new-array v4, v4, [B

    # setter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v3, v4}, Lorg/apache/lucene/util/PagedBytes;->access$602(Lorg/apache/lucene/util/PagedBytes;[B)[B

    .line 503
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # setter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v3, v5}, Lorg/apache/lucene/util/PagedBytes;->access$502(Lorg/apache/lucene/util/PagedBytes;I)I

    .line 504
    add-int/2addr p2, v0

    goto :goto_1

    .line 507
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->currentBlock:[B
    invoke-static {v3}, Lorg/apache/lucene/util/PagedBytes;->access$600(Lorg/apache/lucene/util/PagedBytes;)[B

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # getter for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v4}, Lorg/apache/lucene/util/PagedBytes;->access$500(Lorg/apache/lucene/util/PagedBytes;)I

    move-result v4

    invoke-static {p1, p2, v3, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 508
    iget-object v3, p0, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->this$0:Lorg/apache/lucene/util/PagedBytes;

    # += operator for: Lorg/apache/lucene/util/PagedBytes;->upto:I
    invoke-static {v3, v1}, Lorg/apache/lucene/util/PagedBytes;->access$512(Lorg/apache/lucene/util/PagedBytes;I)I

    goto/16 :goto_0
.end method
