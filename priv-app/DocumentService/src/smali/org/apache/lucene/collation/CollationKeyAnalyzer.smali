.class public final Lorg/apache/lucene/collation/CollationKeyAnalyzer;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "CollationKeyAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/collation/CollationKeyAnalyzer$1;,
        Lorg/apache/lucene/collation/CollationKeyAnalyzer$SavedStreams;
    }
.end annotation


# instance fields
.field private collator:Ljava/text/Collator;


# direct methods
.method public constructor <init>(Ljava/text/Collator;)V
    .locals 0
    .param p1, "collator"    # Ljava/text/Collator;

    .prologue
    .line 82
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;-><init>()V

    .line 83
    iput-object p1, p0, Lorg/apache/lucene/collation/CollationKeyAnalyzer;->collator:Ljava/text/Collator;

    .line 84
    return-void
.end method


# virtual methods
.method public reusableTokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 4
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    invoke-virtual {p0}, Lorg/apache/lucene/collation/CollationKeyAnalyzer;->getPreviousTokenStream()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/collation/CollationKeyAnalyzer$SavedStreams;

    .line 103
    .local v0, "streams":Lorg/apache/lucene/collation/CollationKeyAnalyzer$SavedStreams;
    if-nez v0, :cond_0

    .line 104
    new-instance v0, Lorg/apache/lucene/collation/CollationKeyAnalyzer$SavedStreams;

    .end local v0    # "streams":Lorg/apache/lucene/collation/CollationKeyAnalyzer$SavedStreams;
    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/collation/CollationKeyAnalyzer$SavedStreams;-><init>(Lorg/apache/lucene/collation/CollationKeyAnalyzer;Lorg/apache/lucene/collation/CollationKeyAnalyzer$1;)V

    .line 105
    .restart local v0    # "streams":Lorg/apache/lucene/collation/CollationKeyAnalyzer$SavedStreams;
    new-instance v1, Lorg/apache/lucene/analysis/KeywordTokenizer;

    invoke-direct {v1, p2}, Lorg/apache/lucene/analysis/KeywordTokenizer;-><init>(Ljava/io/Reader;)V

    iput-object v1, v0, Lorg/apache/lucene/collation/CollationKeyAnalyzer$SavedStreams;->source:Lorg/apache/lucene/analysis/Tokenizer;

    .line 106
    new-instance v1, Lorg/apache/lucene/collation/CollationKeyFilter;

    iget-object v2, v0, Lorg/apache/lucene/collation/CollationKeyAnalyzer$SavedStreams;->source:Lorg/apache/lucene/analysis/Tokenizer;

    iget-object v3, p0, Lorg/apache/lucene/collation/CollationKeyAnalyzer;->collator:Ljava/text/Collator;

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/collation/CollationKeyFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/text/Collator;)V

    iput-object v1, v0, Lorg/apache/lucene/collation/CollationKeyAnalyzer$SavedStreams;->result:Lorg/apache/lucene/analysis/TokenStream;

    .line 107
    invoke-virtual {p0, v0}, Lorg/apache/lucene/collation/CollationKeyAnalyzer;->setPreviousTokenStream(Ljava/lang/Object;)V

    .line 111
    :goto_0
    iget-object v1, v0, Lorg/apache/lucene/collation/CollationKeyAnalyzer$SavedStreams;->result:Lorg/apache/lucene/analysis/TokenStream;

    return-object v1

    .line 109
    :cond_0
    iget-object v1, v0, Lorg/apache/lucene/collation/CollationKeyAnalyzer$SavedStreams;->source:Lorg/apache/lucene/analysis/Tokenizer;

    invoke-virtual {v1, p2}, Lorg/apache/lucene/analysis/Tokenizer;->reset(Ljava/io/Reader;)V

    goto :goto_0
.end method

.method public tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 88
    new-instance v0, Lorg/apache/lucene/analysis/KeywordTokenizer;

    invoke-direct {v0, p2}, Lorg/apache/lucene/analysis/KeywordTokenizer;-><init>(Ljava/io/Reader;)V

    .line 89
    .local v0, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/collation/CollationKeyFilter;

    iget-object v2, p0, Lorg/apache/lucene/collation/CollationKeyAnalyzer;->collator:Ljava/text/Collator;

    invoke-direct {v1, v0, v2}, Lorg/apache/lucene/collation/CollationKeyFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/text/Collator;)V

    .line 90
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .local v1, "result":Lorg/apache/lucene/analysis/TokenStream;
    return-object v1
.end method
