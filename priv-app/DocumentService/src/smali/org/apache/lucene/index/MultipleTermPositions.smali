.class public Lorg/apache/lucene/index/MultipleTermPositions;
.super Ljava/lang/Object;
.source "MultipleTermPositions.java"

# interfaces
.implements Lorg/apache/lucene/index/TermPositions;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/MultipleTermPositions$1;,
        Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;,
        Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;
    }
.end annotation


# instance fields
.field private _doc:I

.field private _freq:I

.field private _posList:Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;

.field private _termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/Term;)V
    .locals 4
    .param p1, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "terms"    # [Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 104
    .local v1, "termPositions":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/TermPositions;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    .line 105
    aget-object v2, p2, v0

    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/IndexReader;->termPositions(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermPositions;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 107
    :cond_0
    new-instance v2, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    invoke-direct {v2, v1}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;-><init>(Ljava/util/List;)V

    iput-object v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    .line 108
    new-instance v2, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;-><init>(Lorg/apache/lucene/index/MultipleTermPositions$1;)V

    iput-object v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_posList:Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;

    .line 109
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 168
    iget-object v0, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->close()V

    goto :goto_0

    .line 169
    :cond_0
    return-void
.end method

.method public final doc()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_doc:I

    return v0
.end method

.method public final freq()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_freq:I

    return v0
.end method

.method public getPayload([BI)[B
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 209
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getPayloadLength()I
    .locals 1

    .prologue
    .line 201
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isPayloadAvailable()Z
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x0

    return v0
.end method

.method public final next()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    iget-object v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 113
    const/4 v2, 0x0

    .line 138
    :goto_0
    return v2

    .line 115
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_posList:Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->clear()V

    .line 116
    iget-object v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->peek()Lorg/apache/lucene/index/TermPositions;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_doc:I

    .line 120
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->peek()Lorg/apache/lucene/index/TermPositions;

    move-result-object v1

    .line 122
    .local v1, "tp":Lorg/apache/lucene/index/TermPositions;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->freq()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 124
    iget-object v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_posList:Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->nextPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->add(I)V

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 127
    :cond_2
    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->next()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 128
    iget-object v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->updateTop()Ljava/lang/Object;

    .line 133
    :goto_2
    iget-object v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->size()I

    move-result v2

    if-lez v2, :cond_3

    iget-object v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->peek()Lorg/apache/lucene/index/TermPositions;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v2

    iget v3, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_doc:I

    if-eq v2, v3, :cond_1

    .line 135
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_posList:Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->sort()V

    .line 136
    iget-object v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_posList:Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->size()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_freq:I

    .line 138
    const/4 v2, 0x1

    goto :goto_0

    .line 130
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->pop()Ljava/lang/Object;

    .line 131
    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->close()V

    goto :goto_2
.end method

.method public final nextPosition()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_posList:Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->next()I

    move-result v0

    return v0
.end method

.method public read([I[I)I
    .locals 1
    .param p1, "arg0"    # [I
    .param p2, "arg1"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public seek(Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "arg0"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public seek(Lorg/apache/lucene/index/TermEnum;)V
    .locals 1
    .param p1, "termEnum"    # Lorg/apache/lucene/index/TermEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final skipTo(I)Z
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->peek()Lorg/apache/lucene/index/TermPositions;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->peek()Lorg/apache/lucene/index/TermPositions;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v1

    if-le p1, v1, :cond_1

    .line 149
    iget-object v1, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    .line 150
    .local v0, "tp":Lorg/apache/lucene/index/TermPositions;
    invoke-interface {v0, p1}, Lorg/apache/lucene/index/TermPositions;->skipTo(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 151
    iget-object v1, p0, Lorg/apache/lucene/index/MultipleTermPositions;->_termPositionsQueue:Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 153
    :cond_0
    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->close()V

    goto :goto_0

    .line 155
    .end local v0    # "tp":Lorg/apache/lucene/index/TermPositions;
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultipleTermPositions;->next()Z

    move-result v1

    return v1
.end method
