.class abstract Lorg/apache/lucene/index/InvertedDocEndConsumer;
.super Ljava/lang/Object;
.source "InvertedDocEndConsumer.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract abort()V
.end method

.method abstract addThread(Lorg/apache/lucene/index/DocInverterPerThread;)Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;
.end method

.method abstract flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;",
            ">;>;",
            "Lorg/apache/lucene/index/SegmentWriteState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract setFieldInfos(Lorg/apache/lucene/index/FieldInfos;)V
.end method
