.class final Lorg/apache/lucene/index/ByteBlockPool;
.super Ljava/lang/Object;
.source "ByteBlockPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/ByteBlockPool$Allocator;
    }
.end annotation


# static fields
.field static final FIRST_LEVEL_SIZE:I

.field static final levelSizeArray:[I

.field static final nextLevelArray:[I


# instance fields
.field private final allocator:Lorg/apache/lucene/index/ByteBlockPool$Allocator;

.field public buffer:[B

.field bufferUpto:I

.field public buffers:[[B

.field public byteOffset:I

.field public byteUpto:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 115
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/index/ByteBlockPool;->nextLevelArray:[I

    .line 116
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/lucene/index/ByteBlockPool;->levelSizeArray:[I

    .line 117
    sget-object v0, Lorg/apache/lucene/index/ByteBlockPool;->levelSizeArray:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    sput v0, Lorg/apache/lucene/index/ByteBlockPool;->FIRST_LEVEL_SIZE:I

    return-void

    .line 115
    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0x9
    .end array-data

    .line 116
    :array_1
    .array-data 4
        0x5
        0xe
        0x14
        0x1e
        0x28
        0x28
        0x50
        0x50
        0x78
        0xc8
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/lucene/index/ByteBlockPool$Allocator;)V
    .locals 1
    .param p1, "allocator"    # Lorg/apache/lucene/index/ByteBlockPool$Allocator;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/16 v0, 0xa

    new-array v0, v0, [[B

    iput-object v0, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffers:[[B

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/ByteBlockPool;->bufferUpto:I

    .line 53
    const v0, 0x8000

    iput v0, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    .line 56
    const/16 v0, -0x8000

    iput v0, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteOffset:I

    .line 61
    iput-object p1, p0, Lorg/apache/lucene/index/ByteBlockPool;->allocator:Lorg/apache/lucene/index/ByteBlockPool$Allocator;

    .line 62
    return-void
.end method


# virtual methods
.method public allocSlice([BI)I
    .locals 8
    .param p1, "slice"    # [B
    .param p2, "upto"    # I

    .prologue
    .line 121
    aget-byte v5, p1, p2

    and-int/lit8 v0, v5, 0xf

    .line 122
    .local v0, "level":I
    sget-object v5, Lorg/apache/lucene/index/ByteBlockPool;->nextLevelArray:[I

    aget v1, v5, v0

    .line 123
    .local v1, "newLevel":I
    sget-object v5, Lorg/apache/lucene/index/ByteBlockPool;->levelSizeArray:[I

    aget v2, v5, v1

    .line 126
    .local v2, "newSize":I
    iget v5, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    const v6, 0x8000

    sub-int/2addr v6, v2

    if-le v5, v6, :cond_0

    .line 127
    invoke-virtual {p0}, Lorg/apache/lucene/index/ByteBlockPool;->nextBuffer()V

    .line 129
    :cond_0
    iget v3, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    .line 130
    .local v3, "newUpto":I
    iget v5, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteOffset:I

    add-int v4, v3, v5

    .line 131
    .local v4, "offset":I
    iget v5, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    add-int/2addr v5, v2

    iput v5, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    .line 135
    iget-object v5, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffer:[B

    add-int/lit8 v6, p2, -0x3

    aget-byte v6, p1, v6

    aput-byte v6, v5, v3

    .line 136
    iget-object v5, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffer:[B

    add-int/lit8 v6, v3, 0x1

    add-int/lit8 v7, p2, -0x2

    aget-byte v7, p1, v7

    aput-byte v7, v5, v6

    .line 137
    iget-object v5, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffer:[B

    add-int/lit8 v6, v3, 0x2

    add-int/lit8 v7, p2, -0x1

    aget-byte v7, p1, v7

    aput-byte v7, v5, v6

    .line 140
    add-int/lit8 v5, p2, -0x3

    ushr-int/lit8 v6, v4, 0x18

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 141
    add-int/lit8 v5, p2, -0x2

    ushr-int/lit8 v6, v4, 0x10

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 142
    add-int/lit8 v5, p2, -0x1

    ushr-int/lit8 v6, v4, 0x8

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 143
    int-to-byte v5, v4

    aput-byte v5, p1, p2

    .line 146
    iget-object v5, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffer:[B

    iget v6, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    add-int/lit8 v6, v6, -0x1

    or-int/lit8 v7, v1, 0x10

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 148
    add-int/lit8 v5, v3, 0x3

    return v5
.end method

.method public newSlice(I)I
    .locals 4
    .param p1, "size"    # I

    .prologue
    .line 102
    iget v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    const v2, 0x8000

    sub-int/2addr v2, p1

    if-le v1, v2, :cond_0

    .line 103
    invoke-virtual {p0}, Lorg/apache/lucene/index/ByteBlockPool;->nextBuffer()V

    .line 104
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    .line 105
    .local v0, "upto":I
    iget v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    add-int/2addr v1, p1

    iput v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    .line 106
    iget-object v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x10

    aput-byte v3, v1, v2

    .line 107
    return v0
.end method

.method public nextBuffer()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 88
    iget v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->bufferUpto:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffers:[[B

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 89
    iget-object v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffers:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    sget v2, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [[B

    .line 91
    .local v0, "newBuffers":[[B
    iget-object v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffers:[[B

    iget-object v2, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffers:[[B

    array-length v2, v2

    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 92
    iput-object v0, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffers:[[B

    .line 94
    .end local v0    # "newBuffers":[[B
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffers:[[B

    iget v2, p0, Lorg/apache/lucene/index/ByteBlockPool;->bufferUpto:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lorg/apache/lucene/index/ByteBlockPool;->allocator:Lorg/apache/lucene/index/ByteBlockPool$Allocator;

    invoke-virtual {v3}, Lorg/apache/lucene/index/ByteBlockPool$Allocator;->getByteBlock()[B

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v3, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffer:[B

    .line 95
    iget v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->bufferUpto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->bufferUpto:I

    .line 97
    iput v4, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    .line 98
    iget v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteOffset:I

    const v2, 0x8000

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteOffset:I

    .line 99
    return-void
.end method

.method public reset()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 65
    iget v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->bufferUpto:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 68
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->bufferUpto:I

    if-ge v0, v1, :cond_0

    .line 70
    iget-object v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffers:[[B

    aget-object v1, v1, v0

    invoke-static {v1, v5}, Ljava/util/Arrays;->fill([BB)V

    .line 68
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffers:[[B

    iget v2, p0, Lorg/apache/lucene/index/ByteBlockPool;->bufferUpto:I

    aget-object v1, v1, v2

    iget v2, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    invoke-static {v1, v5, v2, v5}, Ljava/util/Arrays;->fill([BIIB)V

    .line 75
    iget v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->bufferUpto:I

    if-lez v1, :cond_1

    .line 77
    iget-object v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->allocator:Lorg/apache/lucene/index/ByteBlockPool$Allocator;

    iget-object v2, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffers:[[B

    const/4 v3, 0x1

    iget v4, p0, Lorg/apache/lucene/index/ByteBlockPool;->bufferUpto:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/index/ByteBlockPool$Allocator;->recycleByteBlocks([[BII)V

    .line 80
    :cond_1
    iput v5, p0, Lorg/apache/lucene/index/ByteBlockPool;->bufferUpto:I

    .line 81
    iput v5, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    .line 82
    iput v5, p0, Lorg/apache/lucene/index/ByteBlockPool;->byteOffset:I

    .line 83
    iget-object v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffers:[[B

    aget-object v1, v1, v5

    iput-object v1, p0, Lorg/apache/lucene/index/ByteBlockPool;->buffer:[B

    .line 85
    .end local v0    # "i":I
    :cond_2
    return-void
.end method
