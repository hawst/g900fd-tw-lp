.class Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;
.super Lorg/apache/lucene/index/TermVectorMapper;
.source "TermVectorsReader.java"


# instance fields
.field private currentPosition:I

.field private field:Ljava/lang/String;

.field private offsets:[[Lorg/apache/lucene/index/TermVectorOffsetInfo;

.field private positions:[[I

.field private storingOffsets:Z

.field private storingPositions:Z

.field private termFreqs:[I

.field private terms:[Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 535
    invoke-direct {p0}, Lorg/apache/lucene/index/TermVectorMapper;-><init>()V

    return-void
.end method


# virtual methods
.method public map(Ljava/lang/String;I[Lorg/apache/lucene/index/TermVectorOffsetInfo;[I)V
    .locals 2
    .param p1, "term"    # Ljava/lang/String;
    .param p2, "frequency"    # I
    .param p3, "offsets"    # [Lorg/apache/lucene/index/TermVectorOffsetInfo;
    .param p4, "positions"    # [I

    .prologue
    .line 562
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->terms:[Ljava/lang/String;

    iget v1, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->currentPosition:I

    aput-object p1, v0, v1

    .line 563
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->termFreqs:[I

    iget v1, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->currentPosition:I

    aput p2, v0, v1

    .line 564
    iget-boolean v0, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->storingOffsets:Z

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->offsets:[[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    iget v1, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->currentPosition:I

    aput-object p3, v0, v1

    .line 568
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->storingPositions:Z

    if-eqz v0, :cond_1

    .line 570
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->positions:[[I

    iget v1, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->currentPosition:I

    aput-object p4, v0, v1

    .line 572
    :cond_1
    iget v0, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->currentPosition:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->currentPosition:I

    .line 573
    return-void
.end method

.method public materializeVector()Lorg/apache/lucene/index/TermFreqVector;
    .locals 6

    .prologue
    .line 580
    const/4 v0, 0x0

    .line 581
    .local v0, "tv":Lorg/apache/lucene/index/SegmentTermVector;
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->field:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->terms:[Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 582
    iget-boolean v1, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->storingPositions:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->storingOffsets:Z

    if-eqz v1, :cond_2

    .line 583
    :cond_0
    new-instance v0, Lorg/apache/lucene/index/SegmentTermPositionVector;

    .end local v0    # "tv":Lorg/apache/lucene/index/SegmentTermVector;
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->field:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->terms:[Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->termFreqs:[I

    iget-object v4, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->positions:[[I

    iget-object v5, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->offsets:[[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/SegmentTermPositionVector;-><init>(Ljava/lang/String;[Ljava/lang/String;[I[[I[[Lorg/apache/lucene/index/TermVectorOffsetInfo;)V

    .line 588
    .restart local v0    # "tv":Lorg/apache/lucene/index/SegmentTermVector;
    :cond_1
    :goto_0
    return-object v0

    .line 585
    :cond_2
    new-instance v0, Lorg/apache/lucene/index/SegmentTermVector;

    .end local v0    # "tv":Lorg/apache/lucene/index/SegmentTermVector;
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->field:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->terms:[Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->termFreqs:[I

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/index/SegmentTermVector;-><init>(Ljava/lang/String;[Ljava/lang/String;[I)V

    .restart local v0    # "tv":Lorg/apache/lucene/index/SegmentTermVector;
    goto :goto_0
.end method

.method public setExpectations(Ljava/lang/String;IZZ)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "numTerms"    # I
    .param p3, "storeOffsets"    # Z
    .param p4, "storePositions"    # Z

    .prologue
    .line 549
    iput-object p1, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->field:Ljava/lang/String;

    .line 550
    new-array v0, p2, [Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->terms:[Ljava/lang/String;

    .line 551
    new-array v0, p2, [I

    iput-object v0, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->termFreqs:[I

    .line 552
    iput-boolean p3, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->storingOffsets:Z

    .line 553
    iput-boolean p4, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->storingPositions:Z

    .line 554
    if-eqz p4, :cond_0

    .line 555
    new-array v0, p2, [[I

    iput-object v0, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->positions:[[I

    .line 556
    :cond_0
    if-eqz p3, :cond_1

    .line 557
    new-array v0, p2, [[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    iput-object v0, p0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->offsets:[[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    .line 558
    :cond_1
    return-void
.end method
