.class public Lorg/apache/lucene/index/FilterIndexReader$FilterTermPositions;
.super Lorg/apache/lucene/index/FilterIndexReader$FilterTermDocs;
.source "FilterIndexReader.java"

# interfaces
.implements Lorg/apache/lucene/index/TermPositions;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FilterIndexReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FilterTermPositions"
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/TermPositions;)V
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/index/TermPositions;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/FilterIndexReader$FilterTermDocs;-><init>(Lorg/apache/lucene/index/TermDocs;)V

    return-void
.end method


# virtual methods
.method public getPayload([BI)[B
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermPositions;->in:Lorg/apache/lucene/index/TermDocs;

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0, p1, p2}, Lorg/apache/lucene/index/TermPositions;->getPayload([BI)[B

    move-result-object v0

    return-object v0
.end method

.method public getPayloadLength()I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermPositions;->in:Lorg/apache/lucene/index/TermDocs;

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->getPayloadLength()I

    move-result v0

    return v0
.end method

.method public isPayloadAvailable()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermPositions;->in:Lorg/apache/lucene/index/TermDocs;

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->isPayloadAvailable()Z

    move-result v0

    return v0
.end method

.method public nextPosition()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermPositions;->in:Lorg/apache/lucene/index/TermDocs;

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->nextPosition()I

    move-result v0

    return v0
.end method
