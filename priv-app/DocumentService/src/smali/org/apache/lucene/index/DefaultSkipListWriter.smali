.class Lorg/apache/lucene/index/DefaultSkipListWriter;
.super Lorg/apache/lucene/index/MultiLevelSkipListWriter;
.source "DefaultSkipListWriter.java"


# instance fields
.field private curDoc:I

.field private curFreqPointer:J

.field private curPayloadLength:I

.field private curProxPointer:J

.field private curStorePayloads:Z

.field private freqOutput:Lorg/apache/lucene/store/IndexOutput;

.field private lastSkipDoc:[I

.field private lastSkipFreqPointer:[J

.field private lastSkipPayloadLength:[I

.field private lastSkipProxPointer:[J

.field private proxOutput:Lorg/apache/lucene/store/IndexOutput;


# direct methods
.method constructor <init>(IIILorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/IndexOutput;)V
    .locals 1
    .param p1, "skipInterval"    # I
    .param p2, "numberOfSkipLevels"    # I
    .param p3, "docCount"    # I
    .param p4, "freqOutput"    # Lorg/apache/lucene/store/IndexOutput;
    .param p5, "proxOutput"    # Lorg/apache/lucene/store/IndexOutput;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/index/MultiLevelSkipListWriter;-><init>(III)V

    .line 48
    iput-object p4, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->freqOutput:Lorg/apache/lucene/store/IndexOutput;

    .line 49
    iput-object p5, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->proxOutput:Lorg/apache/lucene/store/IndexOutput;

    .line 51
    new-array v0, p2, [I

    iput-object v0, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipDoc:[I

    .line 52
    new-array v0, p2, [I

    iput-object v0, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipPayloadLength:[I

    .line 53
    new-array v0, p2, [J

    iput-object v0, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipFreqPointer:[J

    .line 54
    new-array v0, p2, [J

    iput-object v0, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipProxPointer:[J

    .line 55
    return-void
.end method


# virtual methods
.method protected resetSkip()V
    .locals 4

    .prologue
    .line 79
    invoke-super {p0}, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->resetSkip()V

    .line 80
    iget-object v0, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipDoc:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 81
    iget-object v0, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipPayloadLength:[I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 82
    iget-object v0, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipFreqPointer:[J

    iget-object v1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->freqOutput:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 83
    iget-object v0, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->proxOutput:Lorg/apache/lucene/store/IndexOutput;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipProxPointer:[J

    iget-object v1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->proxOutput:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 85
    :cond_0
    return-void
.end method

.method setFreqOutput(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 0
    .param p1, "freqOutput"    # Lorg/apache/lucene/store/IndexOutput;

    .prologue
    .line 58
    iput-object p1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->freqOutput:Lorg/apache/lucene/store/IndexOutput;

    .line 59
    return-void
.end method

.method setProxOutput(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 0
    .param p1, "proxOutput"    # Lorg/apache/lucene/store/IndexOutput;

    .prologue
    .line 62
    iput-object p1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->proxOutput:Lorg/apache/lucene/store/IndexOutput;

    .line 63
    return-void
.end method

.method setSkipData(IZI)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "storePayloads"    # Z
    .param p3, "payloadLength"    # I

    .prologue
    .line 69
    iput p1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curDoc:I

    .line 70
    iput-boolean p2, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curStorePayloads:Z

    .line 71
    iput p3, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curPayloadLength:I

    .line 72
    iget-object v0, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->freqOutput:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curFreqPointer:J

    .line 73
    iget-object v0, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->proxOutput:Lorg/apache/lucene/store/IndexOutput;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->proxOutput:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curProxPointer:J

    .line 75
    :cond_0
    return-void
.end method

.method protected writeSkipData(ILorg/apache/lucene/store/IndexOutput;)V
    .locals 6
    .param p1, "level"    # I
    .param p2, "skipBuffer"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    iget-boolean v1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curStorePayloads:Z

    if-eqz v1, :cond_1

    .line 110
    iget v1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curDoc:I

    iget-object v2, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipDoc:[I

    aget v2, v2, p1

    sub-int v0, v1, v2

    .line 111
    .local v0, "delta":I
    iget v1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curPayloadLength:I

    iget-object v2, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipPayloadLength:[I

    aget v2, v2, p1

    if-ne v1, v2, :cond_0

    .line 114
    mul-int/lit8 v1, v0, 0x2

    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 126
    .end local v0    # "delta":I
    :goto_0
    iget-wide v2, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curFreqPointer:J

    iget-object v1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipFreqPointer:[J

    aget-wide v4, v1, p1

    sub-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 127
    iget-wide v2, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curProxPointer:J

    iget-object v1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipProxPointer:[J

    aget-wide v4, v1, p1

    sub-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 129
    iget-object v1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipDoc:[I

    iget v2, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curDoc:I

    aput v2, v1, p1

    .line 132
    iget-object v1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipFreqPointer:[J

    iget-wide v2, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curFreqPointer:J

    aput-wide v2, v1, p1

    .line 133
    iget-object v1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipProxPointer:[J

    iget-wide v2, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curProxPointer:J

    aput-wide v2, v1, p1

    .line 134
    return-void

    .line 118
    .restart local v0    # "delta":I
    :cond_0
    mul-int/lit8 v1, v0, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 119
    iget v1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curPayloadLength:I

    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 120
    iget-object v1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipPayloadLength:[I

    iget v2, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curPayloadLength:I

    aput v2, v1, p1

    goto :goto_0

    .line 124
    .end local v0    # "delta":I
    :cond_1
    iget v1, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->curDoc:I

    iget-object v2, p0, Lorg/apache/lucene/index/DefaultSkipListWriter;->lastSkipDoc:[I

    aget v2, v2, p1

    sub-int/2addr v1, v2

    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    goto :goto_0
.end method
