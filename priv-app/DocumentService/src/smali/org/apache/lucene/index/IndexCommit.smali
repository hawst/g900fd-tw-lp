.class public abstract Lorg/apache/lucene/index/IndexCommit;
.super Ljava/lang/Object;
.source "IndexCommit.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/index/IndexCommit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 44
    check-cast p1, Lorg/apache/lucene/index/IndexCommit;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexCommit;->compareTo(Lorg/apache/lucene/index/IndexCommit;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/index/IndexCommit;)I
    .locals 6
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;

    .prologue
    .line 127
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v4

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v5

    if-eq v4, v5, :cond_0

    .line 128
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v5, "cannot compare IndexCommits from different Directory instances"

    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 131
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getGeneration()J

    move-result-wide v2

    .line 132
    .local v2, "gen":J
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexCommit;->getGeneration()J

    move-result-wide v0

    .line 133
    .local v0, "comgen":J
    cmp-long v4, v2, v0

    if-gez v4, :cond_1

    .line 134
    const/4 v4, -0x1

    .line 138
    :goto_0
    return v4

    .line 135
    :cond_1
    cmp-long v4, v2, v0

    if-lez v4, :cond_2

    .line 136
    const/4 v4, 0x1

    goto :goto_0

    .line 138
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public abstract delete()V
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 84
    instance-of v2, p1, Lorg/apache/lucene/index/IndexCommit;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 85
    check-cast v0, Lorg/apache/lucene/index/IndexCommit;

    .line 86
    .local v0, "otherCommit":Lorg/apache/lucene/index/IndexCommit;
    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getVersion()J

    move-result-wide v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getVersion()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 88
    .end local v0    # "otherCommit":Lorg/apache/lucene/index/IndexCommit;
    :cond_0
    return v1
.end method

.method public abstract getDirectory()Lorg/apache/lucene/store/Directory;
.end method

.method public abstract getFileNames()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getGeneration()J
.end method

.method public abstract getSegmentCount()I
.end method

.method public abstract getSegmentsFileName()Ljava/lang/String;
.end method

.method public getTimestamp()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 118
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/Directory;->fileModified(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public abstract getUserData()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getVersion()J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 94
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getVersion()J

    move-result-wide v2

    add-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public abstract isDeleted()Z
.end method
