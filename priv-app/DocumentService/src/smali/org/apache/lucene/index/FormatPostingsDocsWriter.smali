.class final Lorg/apache/lucene/index/FormatPostingsDocsWriter;
.super Lorg/apache/lucene/index/FormatPostingsDocsConsumer;
.source "FormatPostingsDocsWriter.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field df:I

.field fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field freqStart:J

.field lastDocID:I

.field omitTermFreqAndPositions:Z

.field final out:Lorg/apache/lucene/store/IndexOutput;

.field final parent:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

.field final posWriter:Lorg/apache/lucene/index/FormatPostingsPositionsWriter;

.field final skipInterval:I

.field final skipListWriter:Lorg/apache/lucene/index/DefaultSkipListWriter;

.field storePayloads:Z

.field private final termInfo:Lorg/apache/lucene/index/TermInfo;

.field final totalNumDocs:I

.field final utf8:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/index/FormatPostingsTermsWriter;)V
    .locals 6
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "parent"    # Lorg/apache/lucene/index/FormatPostingsTermsWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 45
    invoke-direct {p0}, Lorg/apache/lucene/index/FormatPostingsDocsConsumer;-><init>()V

    .line 107
    new-instance v1, Lorg/apache/lucene/index/TermInfo;

    invoke-direct {v1}, Lorg/apache/lucene/index/TermInfo;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->termInfo:Lorg/apache/lucene/index/TermInfo;

    .line 108
    new-instance v1, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-direct {v1}, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->utf8:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    .line 46
    iput-object p2, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    .line 47
    iget-object v1, p2, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsFieldsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v2, p2, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsFieldsWriter;

    iget-object v2, v2, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->segment:Ljava/lang/String;

    const-string/jumbo v3, "frq"

    invoke-static {v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    .line 48
    const/4 v0, 0x0

    .line 50
    .local v0, "success":Z
    :try_start_0
    iget-object v1, p2, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsFieldsWriter;

    iget v1, v1, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->totalNumDocs:I

    iput v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->totalNumDocs:I

    .line 53
    iget-object v1, p2, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsFieldsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->termsOut:Lorg/apache/lucene/index/TermInfosWriter;

    iget v1, v1, Lorg/apache/lucene/index/TermInfosWriter;->skipInterval:I

    iput v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->skipInterval:I

    .line 54
    iget-object v1, p2, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsFieldsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->skipListWriter:Lorg/apache/lucene/index/DefaultSkipListWriter;

    iput-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->skipListWriter:Lorg/apache/lucene/index/DefaultSkipListWriter;

    .line 55
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->skipListWriter:Lorg/apache/lucene/index/DefaultSkipListWriter;

    iget-object v2, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/DefaultSkipListWriter;->setFreqOutput(Lorg/apache/lucene/store/IndexOutput;)V

    .line 57
    new-instance v1, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;

    invoke-direct {v1, p1, p0}, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;-><init>(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/index/FormatPostingsDocsWriter;)V

    iput-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->posWriter:Lorg/apache/lucene/index/FormatPostingsPositionsWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    const/4 v0, 0x1

    .line 60
    if-nez v0, :cond_0

    .line 61
    new-array v1, v5, [Ljava/io/Closeable;

    iget-object v2, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v1, v4

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 64
    :cond_0
    return-void

    .line 60
    :catchall_0
    move-exception v1

    if-nez v0, :cond_1

    .line 61
    new-array v2, v5, [Ljava/io/Closeable;

    iget-object v3, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 60
    :cond_1
    throw v1
.end method


# virtual methods
.method addDoc(II)Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;
    .locals 5
    .param p1, "docID"    # I
    .param p2, "termDocFreq"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    iget v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->lastDocID:I

    sub-int v0, p1, v1

    .line 83
    .local v0, "delta":I
    if-ltz p1, :cond_0

    iget v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->df:I

    if-lez v1, :cond_1

    if-gtz v0, :cond_1

    .line 84
    :cond_0
    new-instance v1, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "docs out of order ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " <= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->lastDocID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ) (out: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 86
    :cond_1
    iget v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->df:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->df:I

    iget v2, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->skipInterval:I

    rem-int/2addr v1, v2

    if-nez v1, :cond_2

    .line 88
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->skipListWriter:Lorg/apache/lucene/index/DefaultSkipListWriter;

    iget v2, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->lastDocID:I

    iget-boolean v3, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->storePayloads:Z

    iget-object v4, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->posWriter:Lorg/apache/lucene/index/FormatPostingsPositionsWriter;

    iget v4, v4, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->lastPayloadLength:I

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/index/DefaultSkipListWriter;->setSkipData(IZI)V

    .line 89
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->skipListWriter:Lorg/apache/lucene/index/DefaultSkipListWriter;

    iget v2, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->df:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/DefaultSkipListWriter;->bufferSkip(I)V

    .line 92
    :cond_2
    sget-boolean v1, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    iget v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->totalNumDocs:I

    if-lt p1, v1, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "docID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " totalNumDocs="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->totalNumDocs:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 94
    :cond_3
    iput p1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->lastDocID:I

    .line 95
    iget-boolean v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->omitTermFreqAndPositions:Z

    if-eqz v1, :cond_4

    .line 96
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 104
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->posWriter:Lorg/apache/lucene/index/FormatPostingsPositionsWriter;

    return-object v1

    .line 97
    :cond_4
    const/4 v1, 0x1

    if-ne v1, p2, :cond_5

    .line 98
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    shl-int/lit8 v2, v0, 0x1

    or-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    goto :goto_0

    .line 100
    :cond_5
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    shl-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 101
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, p2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    goto :goto_0
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->posWriter:Lorg/apache/lucene/index/FormatPostingsPositionsWriter;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 135
    return-void
.end method

.method finish()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 113
    iget-object v0, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->skipListWriter:Lorg/apache/lucene/index/DefaultSkipListWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DefaultSkipListWriter;->writeSkip(Lorg/apache/lucene/store/IndexOutput;)J

    move-result-wide v8

    .line 117
    .local v8, "skipPointer":J
    iget-object v0, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->termInfo:Lorg/apache/lucene/index/TermInfo;

    iget v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->df:I

    iget-object v2, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    iget-wide v2, v2, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->freqStart:J

    iget-object v4, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    iget-wide v4, v4, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->proxStart:J

    iget-object v6, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    iget-wide v6, v6, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->freqStart:J

    sub-long v6, v8, v6

    long-to-int v6, v6

    invoke-virtual/range {v0 .. v6}, Lorg/apache/lucene/index/TermInfo;->set(IJJI)V

    .line 120
    iget-object v0, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->currentTerm:[C

    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    iget v1, v1, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->currentTermStart:I

    iget-object v2, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->utf8:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8([CILorg/apache/lucene/util/UnicodeUtil$UTF8Result;)V

    .line 122
    iget v0, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->df:I

    if-lez v0, :cond_0

    .line 123
    iget-object v0, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->termsOut:Lorg/apache/lucene/index/TermInfosWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget v1, v1, Lorg/apache/lucene/index/FieldInfo;->number:I

    iget-object v2, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->utf8:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    iget-object v2, v2, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    iget-object v3, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->utf8:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    iget v3, v3, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->length:I

    iget-object v4, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->termInfo:Lorg/apache/lucene/index/TermInfo;

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/lucene/index/TermInfosWriter;->add(I[BILorg/apache/lucene/index/TermInfo;)V

    .line 129
    :cond_0
    iput v10, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->lastDocID:I

    .line 130
    iput v10, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->df:I

    .line 131
    return-void
.end method

.method setField(Lorg/apache/lucene/index/FieldInfo;)V
    .locals 2
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 67
    iput-object p1, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 68
    iget-object v0, p1, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->omitTermFreqAndPositions:Z

    .line 69
    iget-boolean v0, p1, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    iput-boolean v0, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->storePayloads:Z

    .line 70
    iget-object v0, p0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->posWriter:Lorg/apache/lucene/index/FormatPostingsPositionsWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->setField(Lorg/apache/lucene/index/FieldInfo;)V

    .line 71
    return-void

    .line 68
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
