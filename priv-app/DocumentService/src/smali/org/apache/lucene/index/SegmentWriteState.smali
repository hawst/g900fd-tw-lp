.class public Lorg/apache/lucene/index/SegmentWriteState;
.super Ljava/lang/Object;
.source "SegmentWriteState.java"


# instance fields
.field public deletedDocs:Lorg/apache/lucene/util/BitVector;

.field public final directory:Lorg/apache/lucene/store/Directory;

.field public final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field public hasVectors:Z

.field public final infoStream:Ljava/io/PrintStream;

.field public final maxSkipLevels:I

.field public final numDocs:I

.field public final segDeletes:Lorg/apache/lucene/index/BufferedDeletes;

.field public final segmentName:Ljava/lang/String;

.field public final skipInterval:I

.field public final termIndexInterval:I


# direct methods
.method public constructor <init>(Ljava/io/PrintStream;Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;IILorg/apache/lucene/index/BufferedDeletes;)V
    .locals 1
    .param p1, "infoStream"    # Ljava/io/PrintStream;
    .param p2, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p3, "segmentName"    # Ljava/lang/String;
    .param p4, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p5, "numDocs"    # I
    .param p6, "termIndexInterval"    # I
    .param p7, "segDeletes"    # Lorg/apache/lucene/index/BufferedDeletes;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const/16 v0, 0x10

    iput v0, p0, Lorg/apache/lucene/index/SegmentWriteState;->skipInterval:I

    .line 64
    const/16 v0, 0xa

    iput v0, p0, Lorg/apache/lucene/index/SegmentWriteState;->maxSkipLevels:I

    .line 68
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentWriteState;->infoStream:Ljava/io/PrintStream;

    .line 69
    iput-object p7, p0, Lorg/apache/lucene/index/SegmentWriteState;->segDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    .line 70
    iput-object p2, p0, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    .line 71
    iput-object p3, p0, Lorg/apache/lucene/index/SegmentWriteState;->segmentName:Ljava/lang/String;

    .line 72
    iput-object p4, p0, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 73
    iput p5, p0, Lorg/apache/lucene/index/SegmentWriteState;->numDocs:I

    .line 74
    iput p6, p0, Lorg/apache/lucene/index/SegmentWriteState;->termIndexInterval:I

    .line 75
    return-void
.end method
