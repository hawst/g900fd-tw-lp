.class public final Lorg/apache/lucene/index/Term;
.super Ljava/lang/Object;
.source "Term.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/index/Term;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field field:Ljava/lang/String;

.field text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "fld"    # Ljava/lang/String;

    .prologue
    .line 49
    const-string/jumbo v0, ""

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "fld"    # Ljava/lang/String;
    .param p2, "txt"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    .line 40
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "fld"    # Ljava/lang/String;
    .param p2, "txt"    # Ljava/lang/String;
    .param p3, "intern"    # Z

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    if-eqz p3, :cond_0

    invoke-static {p1}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .end local p1    # "fld":Ljava/lang/String;
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    .line 54
    iput-object p2, p0, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    .line 55
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 132
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 133
    iget-object v0, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    .line 134
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p1, Lorg/apache/lucene/index/Term;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v0

    return v0
.end method

.method public final compareTo(Lorg/apache/lucene/index/Term;)I
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    if-ne v0, v1, :cond_0

    .line 115
    iget-object v0, p0, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 117
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public createTerm(Ljava/lang/String;)Lorg/apache/lucene/index/Term;
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 74
    new-instance v0, Lorg/apache/lucene/index/Term;

    iget-object v1, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    if-ne p0, p1, :cond_1

    .line 96
    :cond_0
    :goto_0
    return v1

    .line 81
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 82
    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 84
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 85
    check-cast v0, Lorg/apache/lucene/index/Term;

    .line 86
    .local v0, "other":Lorg/apache/lucene/index/Term;
    iget-object v3, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 87
    iget-object v3, v0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 88
    goto :goto_0

    .line 89
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 90
    goto :goto_0

    .line 91
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 92
    iget-object v3, v0, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 93
    goto :goto_0

    .line 94
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 95
    goto :goto_0
.end method

.method public final field()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 101
    const/16 v0, 0x1f

    .line 102
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 103
    .local v1, "result":I
    iget-object v2, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 104
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    if-nez v4, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 105
    return v1

    .line 103
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 104
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method final set(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "fld"    # Ljava/lang/String;
    .param p2, "txt"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    .line 123
    iput-object p2, p0, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public final text()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
