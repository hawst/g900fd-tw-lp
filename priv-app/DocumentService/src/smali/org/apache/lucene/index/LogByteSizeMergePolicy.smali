.class public Lorg/apache/lucene/index/LogByteSizeMergePolicy;
.super Lorg/apache/lucene/index/LogMergePolicy;
.source "LogByteSizeMergePolicy.java"


# static fields
.field public static final DEFAULT_MAX_MERGE_MB:D = 2048.0

.field public static final DEFAULT_MAX_MERGE_MB_FOR_FORCED_MERGE:D = 9.223372036854776E18

.field public static final DEFAULT_MIN_MERGE_MB:D = 1.6


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/lucene/index/LogMergePolicy;-><init>()V

    .line 38
    const-wide/32 v0, 0x199999

    iput-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->minMergeSize:J

    .line 39
    const-wide v0, 0x80000000L

    iput-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->maxMergeSize:J

    .line 40
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->maxMergeSizeForForcedMerge:J

    .line 41
    return-void
.end method


# virtual methods
.method public getMaxMergeMB()D
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 68
    iget-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->maxMergeSize:J

    long-to-double v0, v0

    div-double/2addr v0, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public getMaxMergeMBForForcedMerge()D
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 99
    iget-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->maxMergeSizeForForcedMerge:J

    long-to-double v0, v0

    div-double/2addr v0, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public getMaxMergeMBForOptimize()D
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 91
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->getMaxMergeMBForForcedMerge()D

    move-result-wide v0

    return-wide v0
.end method

.method public getMinMergeMB()D
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 119
    iget-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->minMergeSize:J

    long-to-double v0, v0

    div-double/2addr v0, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public setMaxMergeMB(D)V
    .locals 5
    .param p1, "mb"    # D

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 60
    mul-double v0, p1, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->maxMergeSize:J

    .line 61
    return-void
.end method

.method public setMaxMergeMBForForcedMerge(D)V
    .locals 5
    .param p1, "mb"    # D

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 84
    mul-double v0, p1, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->maxMergeSizeForForcedMerge:J

    .line 85
    return-void
.end method

.method public setMaxMergeMBForOptimize(D)V
    .locals 1
    .param p1, "mb"    # D
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->setMaxMergeMBForForcedMerge(D)V

    .line 76
    return-void
.end method

.method public setMinMergeMB(D)V
    .locals 5
    .param p1, "mb"    # D

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 112
    mul-double v0, p1, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->minMergeSize:J

    .line 113
    return-void
.end method

.method protected size(Lorg/apache/lucene/index/SegmentInfo;)J
    .locals 2
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/LogByteSizeMergePolicy;->sizeBytes(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v0

    return-wide v0
.end method
