.class public final Lorg/apache/lucene/index/IndexFileNames;
.super Ljava/lang/Object;
.source "IndexFileNames.java"


# static fields
.field public static final COMPOUND_EXTENSIONS:[Ljava/lang/String;

.field public static final COMPOUND_FILE_EXTENSION:Ljava/lang/String; = "cfs"

.field public static final COMPOUND_FILE_STORE_EXTENSION:Ljava/lang/String; = "cfx"

.field public static final DELETABLE:Ljava/lang/String; = "deletable"

.field public static final DELETES_EXTENSION:Ljava/lang/String; = "del"

.field public static final FIELDS_EXTENSION:Ljava/lang/String; = "fdt"

.field public static final FIELDS_INDEX_EXTENSION:Ljava/lang/String; = "fdx"

.field public static final FIELD_INFOS_EXTENSION:Ljava/lang/String; = "fnm"

.field public static final FREQ_EXTENSION:Ljava/lang/String; = "frq"

.field public static final GEN_EXTENSION:Ljava/lang/String; = "gen"

.field public static final INDEX_EXTENSIONS:[Ljava/lang/String;

.field public static final INDEX_EXTENSIONS_IN_COMPOUND_FILE:[Ljava/lang/String;

.field public static final NON_STORE_INDEX_EXTENSIONS:[Ljava/lang/String;

.field public static final NORMS_EXTENSION:Ljava/lang/String; = "nrm"

.field public static final PLAIN_NORMS_EXTENSION:Ljava/lang/String; = "f"

.field public static final PROX_EXTENSION:Ljava/lang/String; = "prx"

.field public static final SEGMENTS:Ljava/lang/String; = "segments"

.field public static final SEGMENTS_GEN:Ljava/lang/String; = "segments.gen"

.field public static final SEPARATE_NORMS_EXTENSION:Ljava/lang/String; = "s"

.field public static final STORE_INDEX_EXTENSIONS:[Ljava/lang/String;

.field public static final TERMS_EXTENSION:Ljava/lang/String; = "tis"

.field public static final TERMS_INDEX_EXTENSION:Ljava/lang/String; = "tii"

.field public static final VECTORS_DOCUMENTS_EXTENSION:Ljava/lang/String; = "tvd"

.field public static final VECTORS_FIELDS_EXTENSION:Ljava/lang/String; = "tvf"

.field public static final VECTORS_INDEX_EXTENSION:Ljava/lang/String; = "tvx"

.field public static final VECTOR_EXTENSIONS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 104
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "cfs"

    aput-object v1, v0, v3

    const-string/jumbo v1, "fnm"

    aput-object v1, v0, v4

    const-string/jumbo v1, "fdx"

    aput-object v1, v0, v5

    const-string/jumbo v1, "fdt"

    aput-object v1, v0, v6

    const-string/jumbo v1, "tii"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "tis"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "frq"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "prx"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "del"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "tvx"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "tvd"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "tvf"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "gen"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "nrm"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "cfx"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/index/IndexFileNames;->INDEX_EXTENSIONS:[Ljava/lang/String;

    .line 124
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "fnm"

    aput-object v1, v0, v3

    const-string/jumbo v1, "fdx"

    aput-object v1, v0, v4

    const-string/jumbo v1, "fdt"

    aput-object v1, v0, v5

    const-string/jumbo v1, "tii"

    aput-object v1, v0, v6

    const-string/jumbo v1, "tis"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "frq"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "prx"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "tvx"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "tvd"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "tvf"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "nrm"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/index/IndexFileNames;->INDEX_EXTENSIONS_IN_COMPOUND_FILE:[Ljava/lang/String;

    .line 138
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "tvx"

    aput-object v1, v0, v3

    const-string/jumbo v1, "tvf"

    aput-object v1, v0, v4

    const-string/jumbo v1, "tvd"

    aput-object v1, v0, v5

    const-string/jumbo v1, "fdx"

    aput-object v1, v0, v6

    const-string/jumbo v1, "fdt"

    aput-object v1, v0, v7

    sput-object v0, Lorg/apache/lucene/index/IndexFileNames;->STORE_INDEX_EXTENSIONS:[Ljava/lang/String;

    .line 146
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "fnm"

    aput-object v1, v0, v3

    const-string/jumbo v1, "frq"

    aput-object v1, v0, v4

    const-string/jumbo v1, "prx"

    aput-object v1, v0, v5

    const-string/jumbo v1, "tis"

    aput-object v1, v0, v6

    const-string/jumbo v1, "tii"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "nrm"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/index/IndexFileNames;->NON_STORE_INDEX_EXTENSIONS:[Ljava/lang/String;

    .line 156
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "fnm"

    aput-object v1, v0, v3

    const-string/jumbo v1, "frq"

    aput-object v1, v0, v4

    const-string/jumbo v1, "prx"

    aput-object v1, v0, v5

    const-string/jumbo v1, "fdx"

    aput-object v1, v0, v6

    const-string/jumbo v1, "fdt"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "tii"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "tis"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/index/IndexFileNames;->COMPOUND_EXTENSIONS:[Ljava/lang/String;

    .line 167
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, "tvx"

    aput-object v1, v0, v3

    const-string/jumbo v1, "tvd"

    aput-object v1, v0, v4

    const-string/jumbo v1, "tvf"

    aput-object v1, v0, v5

    sput-object v0, Lorg/apache/lucene/index/IndexFileNames;->VECTOR_EXTENSIONS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;
    .locals 4
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "ext"    # Ljava/lang/String;
    .param p2, "gen"    # J

    .prologue
    .line 186
    const-wide/16 v2, -0x1

    cmp-long v1, p2, v2

    if-nez v1, :cond_0

    .line 187
    const/4 v1, 0x0

    .line 199
    :goto_0
    return-object v1

    .line 188
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-nez v1, :cond_1

    .line 189
    invoke-static {p0, p1}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 194
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x5f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x24

    invoke-static {p2, p3, v2}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 196
    .local v0, "res":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 197
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static final isDocStoreFile(Ljava/lang/String;)Z
    .locals 6
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 208
    const-string/jumbo v5, "cfx"

    invoke-virtual {p0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 214
    :cond_0
    :goto_0
    return v4

    .line 210
    :cond_1
    sget-object v0, Lorg/apache/lucene/index/IndexFileNames;->STORE_INDEX_EXTENSIONS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 211
    .local v1, "ext":Ljava/lang/String;
    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 210
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 214
    .end local v1    # "ext":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static isSeparateNormsFile(Ljava/lang/String;)Z
    .locals 3
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 271
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 272
    .local v1, "idx":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v2, 0x0

    .line 274
    :goto_0
    return v2

    .line 273
    :cond_0
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 274
    .local v0, "ext":Ljava/lang/String;
    const-string/jumbo v2, "s[0-9]+"

    invoke-static {v2, v0}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v2

    goto :goto_0
.end method

.method public static final matchesExtension(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "ext"    # Ljava/lang/String;

    .prologue
    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static final segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "segmentName"    # Ljava/lang/String;
    .param p1, "ext"    # Ljava/lang/String;

    .prologue
    .line 226
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 227
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 230
    .end local p0    # "segmentName":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method public static final stripSegmentName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v3, -0x1

    .line 255
    const/16 v1, 0x5f

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 256
    .local v0, "idx":I
    if-ne v0, v3, :cond_0

    .line 258
    const/16 v1, 0x2e

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 260
    :cond_0
    if-eq v0, v3, :cond_1

    .line 261
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 263
    :cond_1
    return-object p0
.end method
