.class final Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;
.super Lorg/apache/lucene/index/TermsHashConsumerPerThread;
.source "TermVectorsTermsWriterPerThread.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

.field final docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

.field lastVectorFieldName:Ljava/lang/String;

.field final termsHashPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

.field final termsWriter:Lorg/apache/lucene/index/TermVectorsTermsWriter;

.field final utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

.field final vectorSliceReader:Lorg/apache/lucene/index/ByteSliceReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/TermsHashPerThread;Lorg/apache/lucene/index/TermVectorsTermsWriter;)V
    .locals 3
    .param p1, "termsHashPerThread"    # Lorg/apache/lucene/index/TermsHashPerThread;
    .param p2, "termsWriter"    # Lorg/apache/lucene/index/TermVectorsTermsWriter;

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashConsumerPerThread;-><init>()V

    .line 37
    new-instance v0, Lorg/apache/lucene/index/ByteSliceReader;

    invoke-direct {v0}, Lorg/apache/lucene/index/ByteSliceReader;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->vectorSliceReader:Lorg/apache/lucene/index/ByteSliceReader;

    .line 39
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-direct {v2}, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-direct {v2}, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    .line 31
    iput-object p2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->termsWriter:Lorg/apache/lucene/index/TermVectorsTermsWriter;

    .line 32
    iput-object p1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->termsHashPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    .line 33
    iget-object v0, p1, Lorg/apache/lucene/index/TermsHashPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 34
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->abort()V

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    .line 71
    :cond_0
    return-void
.end method

.method public addField(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/TermsHashConsumerPerField;
    .locals 1
    .param p1, "termsHashPerField"    # Lorg/apache/lucene/index/TermsHashPerField;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 62
    new-instance v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;

    invoke-direct {v0, p1, p0, p2}, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;-><init>(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;Lorg/apache/lucene/index/FieldInfo;)V

    return-object v0
.end method

.method final clearLastVectorFieldName()Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->lastVectorFieldName:Ljava/lang/String;

    .line 76
    const/4 v0, 0x1

    return v0
.end method

.method public finishDocument()Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    .line 54
    return-object v0

    .line 56
    :catchall_0
    move-exception v0

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    throw v0
.end method

.method public startDocument()V
    .locals 2

    .prologue
    .line 44
    sget-boolean v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->clearLastVectorFieldName()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 45
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    if-eqz v0, :cond_1

    .line 46
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->reset()V

    .line 47
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iput v1, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->docID:I

    .line 49
    :cond_1
    return-void
.end method

.method final vectorFieldsInOrder(Lorg/apache/lucene/index/FieldInfo;)Z
    .locals 3
    .param p1, "fi"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    const/4 v0, 0x1

    .line 83
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->lastVectorFieldName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 84
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->lastVectorFieldName:Ljava/lang/String;

    iget-object v2, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-gez v1, :cond_0

    .line 88
    :goto_0
    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    :goto_1
    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->lastVectorFieldName:Ljava/lang/String;

    .line 86
    return v0

    .line 84
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 88
    :catchall_0
    move-exception v0

    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->lastVectorFieldName:Ljava/lang/String;

    throw v0

    :cond_1
    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    goto :goto_1
.end method
