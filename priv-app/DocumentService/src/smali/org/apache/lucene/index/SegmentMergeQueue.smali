.class final Lorg/apache/lucene/index/SegmentMergeQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "SegmentMergeQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/index/SegmentMergeInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;-><init>()V

    .line 25
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentMergeQueue;->initialize(I)V

    .line 26
    return-void
.end method


# virtual methods
.method final close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentMergeQueue;->top()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentMergeQueue;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentMergeInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentMergeInfo;->close()V

    goto :goto_0

    .line 40
    :cond_0
    return-void
.end method

.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 23
    check-cast p1, Lorg/apache/lucene/index/SegmentMergeInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/index/SegmentMergeInfo;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/SegmentMergeQueue;->lessThan(Lorg/apache/lucene/index/SegmentMergeInfo;Lorg/apache/lucene/index/SegmentMergeInfo;)Z

    move-result v0

    return v0
.end method

.method protected final lessThan(Lorg/apache/lucene/index/SegmentMergeInfo;Lorg/apache/lucene/index/SegmentMergeInfo;)Z
    .locals 5
    .param p1, "stiA"    # Lorg/apache/lucene/index/SegmentMergeInfo;
    .param p2, "stiB"    # Lorg/apache/lucene/index/SegmentMergeInfo;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 30
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentMergeInfo;->term:Lorg/apache/lucene/index/Term;

    iget-object v4, p2, Lorg/apache/lucene/index/SegmentMergeInfo;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v0

    .line 31
    .local v0, "comparison":I
    if-nez v0, :cond_2

    .line 32
    iget v3, p1, Lorg/apache/lucene/index/SegmentMergeInfo;->base:I

    iget v4, p2, Lorg/apache/lucene/index/SegmentMergeInfo;->base:I

    if-ge v3, v4, :cond_1

    .line 34
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 32
    goto :goto_0

    .line 34
    :cond_2
    if-ltz v0, :cond_0

    move v1, v2

    goto :goto_0
.end method
