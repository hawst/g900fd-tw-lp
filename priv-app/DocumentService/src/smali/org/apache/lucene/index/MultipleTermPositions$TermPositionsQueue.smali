.class final Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "MultipleTermPositions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MultipleTermPositions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TermPositionsQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/index/TermPositions;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/TermPositions;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "termPositions":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/TermPositions;>;"
    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;-><init>()V

    .line 37
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->initialize(I)V

    .line 39
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/TermPositions;

    .line 40
    .local v1, "tp":Lorg/apache/lucene/index/TermPositions;
    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->next()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 41
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 43
    .end local v1    # "tp":Lorg/apache/lucene/index/TermPositions;
    :cond_1
    return-void
.end method


# virtual methods
.method public bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 35
    check-cast p1, Lorg/apache/lucene/index/TermPositions;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/index/TermPositions;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->lessThan(Lorg/apache/lucene/index/TermPositions;Lorg/apache/lucene/index/TermPositions;)Z

    move-result v0

    return v0
.end method

.method public final lessThan(Lorg/apache/lucene/index/TermPositions;Lorg/apache/lucene/index/TermPositions;)Z
    .locals 2
    .param p1, "a"    # Lorg/apache/lucene/index/TermPositions;
    .param p2, "b"    # Lorg/apache/lucene/index/TermPositions;

    .prologue
    .line 51
    invoke-interface {p1}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v0

    invoke-interface {p2}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final peek()Lorg/apache/lucene/index/TermPositions;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultipleTermPositions$TermPositionsQueue;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    return-object v0
.end method
