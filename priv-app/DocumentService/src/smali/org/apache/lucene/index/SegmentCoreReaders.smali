.class final Lorg/apache/lucene/index/SegmentCoreReaders;
.super Ljava/lang/Object;
.source "SegmentCoreReaders.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final cfsDir:Lorg/apache/lucene/store/Directory;

.field cfsReader:Lorg/apache/lucene/index/CompoundFileReader;

.field private final coreClosedListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;",
            ">;"
        }
    .end annotation
.end field

.field final dir:Lorg/apache/lucene/store/Directory;

.field final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field fieldsReaderOrig:Lorg/apache/lucene/index/FieldsReader;

.field final freqStream:Lorg/apache/lucene/store/IndexInput;

.field private final owner:Lorg/apache/lucene/index/SegmentReader;

.field final proxStream:Lorg/apache/lucene/store/IndexInput;

.field final readBufferSize:I

.field private final ref:Ljava/util/concurrent/atomic/AtomicInteger;

.field final segment:Ljava/lang/String;

.field storeCFSReader:Lorg/apache/lucene/index/CompoundFileReader;

.field termVectorsReaderOrig:Lorg/apache/lucene/index/TermVectorsReader;

.field final termsIndexDivisor:I

.field volatile tis:Lorg/apache/lucene/index/TermInfosReader;

.field final tisNoIndex:Lorg/apache/lucene/index/TermInfosReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/SegmentReader;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;II)V
    .locals 8
    .param p1, "owner"    # Lorg/apache/lucene/index/SegmentReader;
    .param p2, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p3, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p4, "readBufferSize"    # I
    .param p5, "termsIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->ref:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 62
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->coreClosedListeners:Ljava/util/Set;

    .line 66
    iget-object v1, p3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->segment:Ljava/lang/String;

    .line 67
    iput p4, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->readBufferSize:I

    .line 68
    iput-object p2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dir:Lorg/apache/lucene/store/Directory;

    .line 70
    const/4 v7, 0x0

    .line 73
    .local v7, "success":Z
    move-object v6, p2

    .line 74
    .local v6, "dir0":Lorg/apache/lucene/store/Directory;
    :try_start_0
    invoke-virtual {p3}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    new-instance v1, Lorg/apache/lucene/index/CompoundFileReader;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->segment:Ljava/lang/String;

    const-string/jumbo v3, "cfs"

    invoke-static {v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p2, v2, p4}, Lorg/apache/lucene/index/CompoundFileReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;I)V

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsReader:Lorg/apache/lucene/index/CompoundFileReader;

    .line 76
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsReader:Lorg/apache/lucene/index/CompoundFileReader;

    .line 78
    :cond_0
    iput-object v6, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsDir:Lorg/apache/lucene/store/Directory;

    .line 80
    new-instance v1, Lorg/apache/lucene/index/FieldInfos;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsDir:Lorg/apache/lucene/store/Directory;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->segment:Ljava/lang/String;

    const-string/jumbo v4, "fnm"

    invoke-static {v3, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/index/FieldInfos;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 82
    iput p5, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->termsIndexDivisor:I

    .line 83
    new-instance v0, Lorg/apache/lucene/index/TermInfosReader;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsDir:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->segment:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/TermInfosReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;II)V

    .line 84
    .local v0, "reader":Lorg/apache/lucene/index/TermInfosReader;
    const/4 v1, -0x1

    if-ne p5, v1, :cond_2

    .line 85
    iput-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->tisNoIndex:Lorg/apache/lucene/index/TermInfosReader;

    .line 93
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsDir:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->segment:Ljava/lang/String;

    const-string/jumbo v3, "frq"

    invoke-static {v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->freqStream:Lorg/apache/lucene/store/IndexInput;

    .line 95
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfos;->hasProx()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 96
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsDir:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->segment:Ljava/lang/String;

    const-string/jumbo v3, "prx"

    invoke-static {v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->proxStream:Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    :goto_1
    const/4 v7, 0x1

    .line 102
    if-nez v7, :cond_1

    .line 103
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentCoreReaders;->decRef()V

    .line 111
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->owner:Lorg/apache/lucene/index/SegmentReader;

    .line 112
    return-void

    .line 87
    :cond_2
    :try_start_1
    iput-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->tis:Lorg/apache/lucene/index/TermInfosReader;

    .line 88
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->tisNoIndex:Lorg/apache/lucene/index/TermInfosReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 102
    .end local v0    # "reader":Lorg/apache/lucene/index/TermInfosReader;
    :catchall_0
    move-exception v1

    if-nez v7, :cond_3

    .line 103
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentCoreReaders;->decRef()V

    :cond_3
    throw v1

    .line 98
    .restart local v0    # "reader":Lorg/apache/lucene/index/TermInfosReader;
    :cond_4
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->proxStream:Lorg/apache/lucene/store/IndexInput;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private final notifyCoreClosedListeners()V
    .locals 4

    .prologue
    .line 180
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->coreClosedListeners:Ljava/util/Set;

    monitor-enter v3

    .line 181
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->coreClosedListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    .line 182
    .local v1, "listener":Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->owner:Lorg/apache/lucene/index/SegmentReader;

    invoke-interface {v1, v2}, Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;->onClose(Lorg/apache/lucene/index/SegmentReader;)V

    goto :goto_0

    .line 184
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 185
    return-void
.end method


# virtual methods
.method addCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    .prologue
    .line 188
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->coreClosedListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 189
    return-void
.end method

.method declared-synchronized decRef()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->ref:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    .line 171
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->tis:Lorg/apache/lucene/index/TermInfosReader;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->tisNoIndex:Lorg/apache/lucene/index/TermInfosReader;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->freqStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->proxStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->termVectorsReaderOrig:Lorg/apache/lucene/index/TermVectorsReader;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldsReaderOrig:Lorg/apache/lucene/index/FieldsReader;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsReader:Lorg/apache/lucene/index/CompoundFileReader;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->storeCFSReader:Lorg/apache/lucene/index/CompoundFileReader;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 173
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->tis:Lorg/apache/lucene/index/TermInfosReader;

    .line 175
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentCoreReaders;->notifyCoreClosedListeners()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    :cond_0
    monitor-exit p0

    return-void

    .line 170
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized getCFSReader()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 127
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsReader:Lorg/apache/lucene/index/CompoundFileReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized getFieldsReaderOrig()Lorg/apache/lucene/index/FieldsReader;
    .locals 1

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldsReaderOrig:Lorg/apache/lucene/index/FieldsReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized getTermVectorsReaderOrig()Lorg/apache/lucene/index/TermVectorsReader;
    .locals 1

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->termVectorsReaderOrig:Lorg/apache/lucene/index/TermVectorsReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method getTermsReader()Lorg/apache/lucene/index/TermInfosReader;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->tis:Lorg/apache/lucene/index/TermInfosReader;

    .line 132
    .local v0, "tis":Lorg/apache/lucene/index/TermInfosReader;
    if-eqz v0, :cond_0

    .line 135
    .end local v0    # "tis":Lorg/apache/lucene/index/TermInfosReader;
    :goto_0
    return-object v0

    .restart local v0    # "tis":Lorg/apache/lucene/index/TermInfosReader;
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->tisNoIndex:Lorg/apache/lucene/index/TermInfosReader;

    goto :goto_0
.end method

.method declared-synchronized incRef()V
    .locals 1

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->ref:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    monitor-exit p0

    return-void

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized loadTermsIndex(Lorg/apache/lucene/index/SegmentInfo;I)V
    .locals 6
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "termsIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->tis:Lorg/apache/lucene/index/TermInfosReader;

    if-nez v0, :cond_1

    .line 151
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 156
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsReader:Lorg/apache/lucene/index/CompoundFileReader;

    if-nez v0, :cond_0

    .line 157
    new-instance v0, Lorg/apache/lucene/index/CompoundFileReader;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->segment:Ljava/lang/String;

    const-string/jumbo v4, "cfs"

    invoke-static {v3, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->readBufferSize:I

    invoke-direct {v0, v2, v3, v4}, Lorg/apache/lucene/index/CompoundFileReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;I)V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsReader:Lorg/apache/lucene/index/CompoundFileReader;

    .line 159
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsReader:Lorg/apache/lucene/index/CompoundFileReader;

    .line 164
    .local v1, "dir0":Lorg/apache/lucene/store/Directory;
    :goto_0
    new-instance v0, Lorg/apache/lucene/index/TermInfosReader;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->segment:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget v4, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->readBufferSize:I

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/TermInfosReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;II)V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->tis:Lorg/apache/lucene/index/TermInfosReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    .end local v1    # "dir0":Lorg/apache/lucene/store/Directory;
    :cond_1
    monitor-exit p0

    return-void

    .line 161
    :cond_2
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dir:Lorg/apache/lucene/store/Directory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v1    # "dir0":Lorg/apache/lucene/store/Directory;
    goto :goto_0

    .line 149
    .end local v1    # "dir0":Lorg/apache/lucene/store/Directory;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized openDocStores(Lorg/apache/lucene/index/SegmentInfo;)V
    .locals 8
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    .line 197
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->segment:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 199
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldsReaderOrig:Lorg/apache/lucene/index/FieldsReader;

    if-nez v0, :cond_9

    .line 201
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreOffset()I

    move-result v0

    if-eq v0, v7, :cond_3

    .line 202
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreIsCompoundFile()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 203
    sget-boolean v0, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->storeCFSReader:Lorg/apache/lucene/index/CompoundFileReader;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 204
    :cond_1
    new-instance v0, Lorg/apache/lucene/index/CompoundFileReader;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreSegment()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "cfx"

    invoke-static {v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->readBufferSize:I

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/lucene/index/CompoundFileReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;I)V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->storeCFSReader:Lorg/apache/lucene/index/CompoundFileReader;

    .line 207
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->storeCFSReader:Lorg/apache/lucene/index/CompoundFileReader;

    .line 208
    .local v1, "storeDir":Lorg/apache/lucene/store/Directory;
    sget-boolean v0, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    if-nez v1, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 210
    .end local v1    # "storeDir":Lorg/apache/lucene/store/Directory;
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dir:Lorg/apache/lucene/store/Directory;

    .line 211
    .restart local v1    # "storeDir":Lorg/apache/lucene/store/Directory;
    sget-boolean v0, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    if-nez v1, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 213
    .end local v1    # "storeDir":Lorg/apache/lucene/store/Directory;
    :cond_3
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 217
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsReader:Lorg/apache/lucene/index/CompoundFileReader;

    if-nez v0, :cond_4

    .line 218
    new-instance v0, Lorg/apache/lucene/index/CompoundFileReader;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->segment:Ljava/lang/String;

    const-string/jumbo v5, "cfs"

    invoke-static {v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->readBufferSize:I

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/lucene/index/CompoundFileReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;I)V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsReader:Lorg/apache/lucene/index/CompoundFileReader;

    .line 220
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsReader:Lorg/apache/lucene/index/CompoundFileReader;

    .line 221
    .restart local v1    # "storeDir":Lorg/apache/lucene/store/Directory;
    sget-boolean v0, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    if-nez v1, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 223
    .end local v1    # "storeDir":Lorg/apache/lucene/store/Directory;
    :cond_5
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->dir:Lorg/apache/lucene/store/Directory;

    .line 224
    .restart local v1    # "storeDir":Lorg/apache/lucene/store/Directory;
    sget-boolean v0, Lorg/apache/lucene/index/SegmentCoreReaders;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    if-nez v1, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 228
    :cond_6
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreOffset()I

    move-result v0

    if-eq v0, v7, :cond_7

    .line 229
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreSegment()Ljava/lang/String;

    move-result-object v2

    .line 234
    .local v2, "storesSegment":Ljava/lang/String;
    :goto_0
    new-instance v0, Lorg/apache/lucene/index/FieldsReader;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget v4, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->readBufferSize:I

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreOffset()I

    move-result v5

    iget v6, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/FieldsReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;III)V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldsReaderOrig:Lorg/apache/lucene/index/FieldsReader;

    .line 238
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreOffset()I

    move-result v0

    if-ne v0, v7, :cond_8

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldsReaderOrig:Lorg/apache/lucene/index/FieldsReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldsReader;->size()I

    move-result v0

    iget v3, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-eq v0, v3, :cond_8

    .line 239
    new-instance v0, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "doc counts differ for segment "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->segment:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ": fieldsReader shows "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldsReaderOrig:Lorg/apache/lucene/index/FieldsReader;

    invoke-virtual {v4}, Lorg/apache/lucene/index/FieldsReader;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " but segmentInfo shows "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    .end local v2    # "storesSegment":Ljava/lang/String;
    :cond_7
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->segment:Ljava/lang/String;

    .restart local v2    # "storesSegment":Ljava/lang/String;
    goto :goto_0

    .line 242
    :cond_8
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getHasVectors()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 243
    new-instance v0, Lorg/apache/lucene/index/TermVectorsReader;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget v4, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->readBufferSize:I

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreOffset()I

    move-result v5

    iget v6, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/TermVectorsReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;III)V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->termVectorsReaderOrig:Lorg/apache/lucene/index/TermVectorsReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246
    .end local v1    # "storeDir":Lorg/apache/lucene/store/Directory;
    .end local v2    # "storesSegment":Ljava/lang/String;
    :cond_9
    monitor-exit p0

    return-void
.end method

.method removeCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    .prologue
    .line 192
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->coreClosedListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 193
    return-void
.end method

.method termsIndexIsLoaded()Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->tis:Lorg/apache/lucene/index/TermInfosReader;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "SegmentCoreReader(owner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentCoreReaders;->owner:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
