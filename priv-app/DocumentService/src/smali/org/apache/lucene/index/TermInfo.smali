.class Lorg/apache/lucene/index/TermInfo;
.super Ljava/lang/Object;
.source "TermInfo.java"


# instance fields
.field docFreq:I

.field freqPointer:J

.field proxPointer:J

.field skipOffset:I


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    .line 26
    iput-wide v2, p0, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    .line 27
    iput-wide v2, p0, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    .line 30
    return-void
.end method

.method constructor <init>(IJJ)V
    .locals 4
    .param p1, "df"    # I
    .param p2, "fp"    # J
    .param p4, "pp"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    .line 26
    iput-wide v2, p0, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    .line 27
    iput-wide v2, p0, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    .line 33
    iput p1, p0, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    .line 34
    iput-wide p2, p0, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    .line 35
    iput-wide p4, p0, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    .line 36
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/index/TermInfo;)V
    .locals 4
    .param p1, "ti"    # Lorg/apache/lucene/index/TermInfo;

    .prologue
    const-wide/16 v2, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    .line 26
    iput-wide v2, p0, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    .line 27
    iput-wide v2, p0, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    .line 39
    iget v0, p1, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    iput v0, p0, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    .line 40
    iget-wide v0, p1, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    iput-wide v0, p0, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    .line 41
    iget-wide v0, p1, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    iput-wide v0, p0, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    .line 42
    iget v0, p1, Lorg/apache/lucene/index/TermInfo;->skipOffset:I

    iput v0, p0, Lorg/apache/lucene/index/TermInfo;->skipOffset:I

    .line 43
    return-void
.end method


# virtual methods
.method final set(IJJI)V
    .locals 0
    .param p1, "docFreq"    # I
    .param p2, "freqPointer"    # J
    .param p4, "proxPointer"    # J
    .param p6, "skipOffset"    # I

    .prologue
    .line 47
    iput p1, p0, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    .line 48
    iput-wide p2, p0, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    .line 49
    iput-wide p4, p0, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    .line 50
    iput p6, p0, Lorg/apache/lucene/index/TermInfo;->skipOffset:I

    .line 51
    return-void
.end method

.method final set(Lorg/apache/lucene/index/TermInfo;)V
    .locals 2
    .param p1, "ti"    # Lorg/apache/lucene/index/TermInfo;

    .prologue
    .line 54
    iget v0, p1, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    iput v0, p0, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    .line 55
    iget-wide v0, p1, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    iput-wide v0, p0, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    .line 56
    iget-wide v0, p1, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    iput-wide v0, p0, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    .line 57
    iget v0, p1, Lorg/apache/lucene/index/TermInfo;->skipOffset:I

    iput v0, p0, Lorg/apache/lucene/index/TermInfo;->skipOffset:I

    .line 58
    return-void
.end method
