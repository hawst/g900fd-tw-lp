.class final Lorg/apache/lucene/index/FieldsWriter;
.super Ljava/lang/Object;
.source "FieldsWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/FieldsWriter$1;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final FIELD_IS_BINARY:I = 0x2

.field static final FIELD_IS_COMPRESSED:I = 0x4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field static final FIELD_IS_NUMERIC_DOUBLE:I = 0x20

.field static final FIELD_IS_NUMERIC_FLOAT:I = 0x18

.field static final FIELD_IS_NUMERIC_INT:I = 0x8

.field static final FIELD_IS_NUMERIC_LONG:I = 0x10

.field static final FIELD_IS_NUMERIC_MASK:I = 0x38

.field static final FIELD_IS_TOKENIZED:I = 0x1

.field static final FORMAT:I = 0x0

.field static final FORMAT_CURRENT:I = 0x3

.field static final FORMAT_LUCENE_3_0_NO_COMPRESSED_FIELDS:I = 0x2

.field static final FORMAT_LUCENE_3_2_NUMERIC_FIELDS:I = 0x3

.field static final FORMAT_VERSION_UTF8_LENGTH_IN_BYTES:I = 0x1

.field private static final _NUMERIC_BIT_SHIFT:I = 0x3


# instance fields
.field private directory:Lorg/apache/lucene/store/Directory;

.field private fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private fieldsStream:Lorg/apache/lucene/store/IndexOutput;

.field private indexStream:Lorg/apache/lucene/store/IndexOutput;

.field private segment:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/index/FieldsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FieldsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 3
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "fn"    # Lorg/apache/lucene/index/FieldInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lorg/apache/lucene/index/FieldsWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 78
    iput-object p2, p0, Lorg/apache/lucene/index/FieldsWriter;->segment:Ljava/lang/String;

    .line 79
    iput-object p3, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 81
    const/4 v0, 0x0

    .line 83
    .local v0, "success":Z
    :try_start_0
    const-string/jumbo v1, "fdt"

    invoke-static {p2, v1}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    .line 84
    const-string/jumbo v1, "fdx"

    invoke-static {p2, v1}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/FieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    .line 86
    iget-object v1, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 87
    iget-object v1, p0, Lorg/apache/lucene/index/FieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    const/4 v0, 0x1

    .line 91
    if-nez v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lorg/apache/lucene/index/FieldsWriter;->abort()V

    .line 95
    :cond_0
    return-void

    .line 91
    :catchall_0
    move-exception v1

    if-nez v0, :cond_1

    .line 92
    invoke-virtual {p0}, Lorg/apache/lucene/index/FieldsWriter;->abort()V

    .line 91
    :cond_1
    throw v1
.end method

.method constructor <init>(Lorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 1
    .param p1, "fdx"    # Lorg/apache/lucene/store/IndexOutput;
    .param p2, "fdt"    # Lorg/apache/lucene/store/IndexOutput;
    .param p3, "fn"    # Lorg/apache/lucene/index/FieldInfos;

    .prologue
    const/4 v0, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object v0, p0, Lorg/apache/lucene/index/FieldsWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 99
    iput-object v0, p0, Lorg/apache/lucene/index/FieldsWriter;->segment:Ljava/lang/String;

    .line 100
    iput-object p3, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 101
    iput-object p2, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    .line 102
    iput-object p1, p0, Lorg/apache/lucene/index/FieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    .line 103
    return-void
.end method


# virtual methods
.method abort()V
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsWriter;->directory:Lorg/apache/lucene/store/Directory;

    if-eqz v0, :cond_0

    .line 149
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/FieldsWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :goto_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsWriter;->segment:Ljava/lang/String;

    const-string/jumbo v2, "fdt"

    invoke-static {v1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 157
    :goto_1
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsWriter;->segment:Ljava/lang/String;

    const-string/jumbo v2, "fdx"

    invoke-static {v1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 161
    :cond_0
    :goto_2
    return-void

    .line 150
    :catch_0
    move-exception v0

    goto :goto_0

    .line 158
    :catch_1
    move-exception v0

    goto :goto_2

    .line 154
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method final addDocument(Lorg/apache/lucene/document/Document;)V
    .locals 8
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 233
    iget-object v4, p0, Lorg/apache/lucene/index/FieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    iget-object v5, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 235
    const/4 v3, 0x0

    .line 236
    .local v3, "storedCount":I
    invoke-virtual {p1}, Lorg/apache/lucene/document/Document;->getFields()Ljava/util/List;

    move-result-object v1

    .line 237
    .local v1, "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Fieldable;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Fieldable;

    .line 238
    .local v0, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->isStored()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 239
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 241
    .end local v0    # "field":Lorg/apache/lucene/document/Fieldable;
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v4, v3}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 245
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Fieldable;

    .line 246
    .restart local v0    # "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->isStored()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 247
    iget-object v4, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v4

    invoke-virtual {p0, v4, v0}, Lorg/apache/lucene/index/FieldsWriter;->writeField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/document/Fieldable;)V

    goto :goto_1

    .line 249
    .end local v0    # "field":Lorg/apache/lucene/document/Fieldable;
    :cond_3
    return-void
.end method

.method final addRawDocuments(Lorg/apache/lucene/store/IndexInput;[II)V
    .locals 8
    .param p1, "stream"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "lengths"    # [I
    .param p3, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 222
    iget-object v1, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    .line 223
    .local v2, "position":J
    move-wide v4, v2

    .line 224
    .local v4, "start":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_0

    .line 225
    iget-object v1, p0, Lorg/apache/lucene/index/FieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 226
    aget v1, p2, v0

    int-to-long v6, v1

    add-long/2addr v2, v6

    .line 224
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 228
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    sub-long v6, v2, v4

    invoke-virtual {v1, p1, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V

    .line 229
    sget-boolean v1, Lorg/apache/lucene/index/FieldsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v6

    cmp-long v1, v6, v2

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 230
    :cond_1
    return-void
.end method

.method close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 137
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsWriter;->directory:Lorg/apache/lucene/store/Directory;

    if-eqz v0, :cond_0

    .line 139
    const/4 v0, 0x2

    :try_start_0
    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/index/FieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    iput-object v3, p0, Lorg/apache/lucene/index/FieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    iput-object v3, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    .line 144
    :cond_0
    return-void

    .line 141
    :catchall_0
    move-exception v0

    iput-object v3, p0, Lorg/apache/lucene/index/FieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    iput-object v3, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    throw v0
.end method

.method finish(I)V
    .locals 8
    .param p1, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    const-wide/16 v2, 0x4

    int-to-long v4, p1

    const-wide/16 v6, 0x8

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 131
    iget-object v1, p0, Lorg/apache/lucene/index/FieldsWriter;->segment:Ljava/lang/String;

    const-string/jumbo v2, "fdx"

    invoke-static {v1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "fieldsIdxName":Ljava/lang/String;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "fdx size mismatch: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " docs vs "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/FieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " length in bytes of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " file exists?="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/FieldsWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 134
    .end local v0    # "fieldsIdxName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method flushDocument(ILorg/apache/lucene/store/RAMOutputStream;)V
    .locals 4
    .param p1, "numStoredFields"    # I
    .param p2, "buffer"    # Lorg/apache/lucene/store/RAMOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 115
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 116
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {p2, v0}, Lorg/apache/lucene/store/RAMOutputStream;->writeTo(Lorg/apache/lucene/store/IndexOutput;)V

    .line 117
    return-void
.end method

.method setFieldsStream(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 0
    .param p1, "stream"    # Lorg/apache/lucene/store/IndexOutput;

    .prologue
    .line 106
    iput-object p1, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    .line 107
    return-void
.end method

.method skipDocument()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 121
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 122
    return-void
.end method

.method final writeField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/document/Fieldable;)V
    .locals 10
    .param p1, "fi"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "field"    # Lorg/apache/lucene/document/Fieldable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    iget-object v6, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    iget v7, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v6, v7}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 165
    const/4 v0, 0x0

    .line 166
    .local v0, "bits":I
    invoke-interface {p2}, Lorg/apache/lucene/document/Fieldable;->isTokenized()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 167
    or-int/lit8 v0, v0, 0x1

    .line 168
    :cond_0
    invoke-interface {p2}, Lorg/apache/lucene/document/Fieldable;->isBinary()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 169
    or-int/lit8 v0, v0, 0x2

    .line 170
    :cond_1
    instance-of v6, p2, Lorg/apache/lucene/document/NumericField;

    if-eqz v6, :cond_2

    .line 171
    sget-object v7, Lorg/apache/lucene/index/FieldsWriter$1;->$SwitchMap$org$apache$lucene$document$NumericField$DataType:[I

    move-object v6, p2

    check-cast v6, Lorg/apache/lucene/document/NumericField;

    invoke-virtual {v6}, Lorg/apache/lucene/document/NumericField;->getDataType()Lorg/apache/lucene/document/NumericField$DataType;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/document/NumericField$DataType;->ordinal()I

    move-result v6

    aget v6, v7, v6

    packed-switch v6, :pswitch_data_0

    .line 181
    sget-boolean v6, Lorg/apache/lucene/index/FieldsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_2

    new-instance v6, Ljava/lang/AssertionError;

    const-string/jumbo v7, "Should never get here"

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6

    .line 173
    :pswitch_0
    or-int/lit8 v0, v0, 0x8

    .line 184
    :cond_2
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    int-to-byte v7, v0

    invoke-virtual {v6, v7}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 186
    invoke-interface {p2}, Lorg/apache/lucene/document/Fieldable;->isBinary()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 190
    invoke-interface {p2}, Lorg/apache/lucene/document/Fieldable;->getBinaryValue()[B

    move-result-object v1

    .line 191
    .local v1, "data":[B
    invoke-interface {p2}, Lorg/apache/lucene/document/Fieldable;->getBinaryLength()I

    move-result v2

    .line 192
    .local v2, "len":I
    invoke-interface {p2}, Lorg/apache/lucene/document/Fieldable;->getBinaryOffset()I

    move-result v5

    .line 194
    .local v5, "offset":I
    iget-object v6, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v6, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 195
    iget-object v6, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v6, v1, v5, v2}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 214
    .end local v1    # "data":[B
    .end local v2    # "len":I
    .end local v5    # "offset":I
    :cond_3
    :goto_1
    return-void

    .line 175
    :pswitch_1
    or-int/lit8 v0, v0, 0x10

    goto :goto_0

    .line 177
    :pswitch_2
    or-int/lit8 v0, v0, 0x18

    goto :goto_0

    .line 179
    :pswitch_3
    or-int/lit8 v0, v0, 0x20

    goto :goto_0

    .line 196
    :cond_4
    instance-of v6, p2, Lorg/apache/lucene/document/NumericField;

    if-eqz v6, :cond_5

    move-object v4, p2

    .line 197
    check-cast v4, Lorg/apache/lucene/document/NumericField;

    .line 198
    .local v4, "nf":Lorg/apache/lucene/document/NumericField;
    invoke-virtual {v4}, Lorg/apache/lucene/document/NumericField;->getNumericValue()Ljava/lang/Number;

    move-result-object v3

    .line 199
    .local v3, "n":Ljava/lang/Number;
    sget-object v6, Lorg/apache/lucene/index/FieldsWriter$1;->$SwitchMap$org$apache$lucene$document$NumericField$DataType:[I

    invoke-virtual {v4}, Lorg/apache/lucene/document/NumericField;->getDataType()Lorg/apache/lucene/document/NumericField$DataType;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/lucene/document/NumericField$DataType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    .line 209
    sget-boolean v6, Lorg/apache/lucene/index/FieldsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_3

    new-instance v6, Ljava/lang/AssertionError;

    const-string/jumbo v7, "Should never get here"

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6

    .line 201
    :pswitch_4
    iget-object v6, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    goto :goto_1

    .line 203
    :pswitch_5
    iget-object v6, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Ljava/lang/Number;->longValue()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    goto :goto_1

    .line 205
    :pswitch_6
    iget-object v6, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    goto :goto_1

    .line 207
    :pswitch_7
    iget-object v6, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    goto :goto_1

    .line 212
    .end local v3    # "n":Ljava/lang/Number;
    .end local v4    # "nf":Lorg/apache/lucene/document/NumericField;
    :cond_5
    iget-object v6, p0, Lorg/apache/lucene/index/FieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-interface {p2}, Lorg/apache/lucene/document/Fieldable;->stringValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/store/IndexOutput;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 171
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 199
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
