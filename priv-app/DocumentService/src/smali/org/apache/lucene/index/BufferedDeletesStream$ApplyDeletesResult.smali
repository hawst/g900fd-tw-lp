.class public Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;
.super Ljava/lang/Object;
.source "BufferedDeletesStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/BufferedDeletesStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ApplyDeletesResult"
.end annotation


# instance fields
.field public final allDeleted:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final anyDeletes:Z

.field public final gen:J


# direct methods
.method constructor <init>(ZJLjava/util/List;)V
    .locals 0
    .param p1, "anyDeletes"    # Z
    .param p2, "gen"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZJ",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p4, "allDeleted":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput-boolean p1, p0, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->anyDeletes:Z

    .line 128
    iput-wide p2, p0, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->gen:J

    .line 129
    iput-object p4, p0, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->allDeleted:Ljava/util/List;

    .line 130
    return-void
.end method
