.class final Lorg/apache/lucene/index/MergeDocIDRemapper;
.super Ljava/lang/Object;
.source "MergeDocIDRemapper.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field docMaps:[[I

.field docShift:I

.field maxDocID:I

.field minDocID:I

.field newStarts:[I

.field starts:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lorg/apache/lucene/index/MergeDocIDRemapper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/MergeDocIDRemapper;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/SegmentInfos;[[I[ILorg/apache/lucene/index/MergePolicy$OneMerge;I)V
    .locals 11
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "docMaps"    # [[I
    .param p3, "delCounts"    # [I
    .param p4, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .param p5, "mergedDocCount"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p2, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->docMaps:[[I

    .line 35
    iget-object v6, p4, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfo;

    .line 36
    .local v0, "firstSegment":Lorg/apache/lucene/index/SegmentInfo;
    const/4 v1, 0x0

    .line 38
    .local v1, "i":I
    :goto_0
    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v2

    .line 39
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v2, v0}, Lorg/apache/lucene/index/SegmentInfo;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 45
    const/4 v5, 0x0

    .line 46
    .local v5, "numDocs":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v6, p2

    if-ge v3, v6, :cond_2

    .line 47
    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v6

    iget v6, v6, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    add-int/2addr v5, v6

    .line 48
    sget-boolean v6, Lorg/apache/lucene/index/MergeDocIDRemapper;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v6

    iget-object v7, p4, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/SegmentInfo;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 41
    .end local v3    # "j":I
    .end local v5    # "numDocs":I
    :cond_0
    iget v6, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->minDocID:I

    iget v7, v2, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    add-int/2addr v6, v7

    iput v6, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->minDocID:I

    .line 42
    add-int/lit8 v1, v1, 0x1

    .line 43
    goto :goto_0

    .line 46
    .restart local v3    # "j":I
    .restart local v5    # "numDocs":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 50
    :cond_2
    iget v6, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->minDocID:I

    add-int/2addr v6, v5

    iput v6, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->maxDocID:I

    .line 52
    array-length v6, p2

    new-array v6, v6, [I

    iput-object v6, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->starts:[I

    .line 53
    array-length v6, p2

    new-array v6, v6, [I

    iput-object v6, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->newStarts:[I

    .line 55
    iget-object v6, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->starts:[I

    const/4 v7, 0x0

    iget v8, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->minDocID:I

    aput v8, v6, v7

    .line 56
    iget-object v6, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->newStarts:[I

    const/4 v7, 0x0

    iget v8, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->minDocID:I

    aput v8, v6, v7

    .line 57
    const/4 v1, 0x1

    :goto_2
    array-length v6, p2

    if-ge v1, v6, :cond_3

    .line 58
    iget-object v6, p4, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    add-int/lit8 v7, v1, -0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/SegmentInfo;

    iget v4, v6, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    .line 59
    .local v4, "lastDocCount":I
    iget-object v6, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->starts:[I

    iget-object v7, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->starts:[I

    add-int/lit8 v8, v1, -0x1

    aget v7, v7, v8

    add-int/2addr v7, v4

    aput v7, v6, v1

    .line 60
    iget-object v6, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->newStarts:[I

    iget-object v7, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->newStarts:[I

    add-int/lit8 v8, v1, -0x1

    aget v7, v7, v8

    add-int/2addr v7, v4

    add-int/lit8 v8, v1, -0x1

    aget v8, p3, v8

    sub-int/2addr v7, v8

    aput v7, v6, v1

    .line 57
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 62
    .end local v4    # "lastDocCount":I
    :cond_3
    sub-int v6, v5, p5

    iput v6, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->docShift:I

    .line 72
    sget-boolean v6, Lorg/apache/lucene/index/MergeDocIDRemapper;->$assertionsDisabled:Z

    if-nez v6, :cond_4

    iget v7, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->docShift:I

    iget v8, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->maxDocID:I

    iget-object v6, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->newStarts:[I

    array-length v9, p2

    add-int/lit8 v9, v9, -0x1

    aget v9, v6, v9

    iget-object v6, p4, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    array-length v10, p2

    add-int/lit8 v10, v10, -0x1

    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/SegmentInfo;

    iget v6, v6, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    add-int/2addr v6, v9

    array-length v9, p2

    add-int/lit8 v9, v9, -0x1

    aget v9, p3, v9

    sub-int/2addr v6, v9

    sub-int v6, v8, v6

    if-eq v7, v6, :cond_4

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 73
    :cond_4
    return-void
.end method


# virtual methods
.method public remap(I)I
    .locals 7
    .param p1, "oldDocID"    # I

    .prologue
    .line 76
    iget v4, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->minDocID:I

    if-ge p1, v4, :cond_0

    .line 107
    .end local p1    # "oldDocID":I
    :goto_0
    return p1

    .line 79
    .restart local p1    # "oldDocID":I
    :cond_0
    iget v4, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->maxDocID:I

    if-lt p1, v4, :cond_1

    .line 81
    iget v4, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->docShift:I

    sub-int/2addr p1, v4

    goto :goto_0

    .line 84
    :cond_1
    const/4 v1, 0x0

    .line 85
    .local v1, "lo":I
    iget-object v4, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->docMaps:[[I

    array-length v4, v4

    add-int/lit8 v0, v4, -0x1

    .line 87
    .local v0, "hi":I
    :goto_1
    if-lt v0, v1, :cond_6

    .line 88
    add-int v4, v1, v0

    ushr-int/lit8 v2, v4, 0x1

    .line 89
    .local v2, "mid":I
    iget-object v4, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->starts:[I

    aget v3, v4, v2

    .line 90
    .local v3, "midValue":I
    if-ge p1, v3, :cond_2

    .line 91
    add-int/lit8 v0, v2, -0x1

    goto :goto_1

    .line 92
    :cond_2
    if-le p1, v3, :cond_3

    .line 93
    add-int/lit8 v1, v2, 0x1

    goto :goto_1

    .line 95
    :cond_3
    :goto_2
    add-int/lit8 v4, v2, 0x1

    iget-object v5, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->docMaps:[[I

    array-length v5, v5

    if-ge v4, v5, :cond_4

    iget-object v4, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->starts:[I

    add-int/lit8 v5, v2, 0x1

    aget v4, v4, v5

    if-ne v4, v3, :cond_4

    .line 96
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 98
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->docMaps:[[I

    aget-object v4, v4, v2

    if-eqz v4, :cond_5

    .line 99
    iget-object v4, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->newStarts:[I

    aget v4, v4, v2

    iget-object v5, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->docMaps:[[I

    aget-object v5, v5, v2

    iget-object v6, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->starts:[I

    aget v6, v6, v2

    sub-int v6, p1, v6

    aget v5, v5, v6

    add-int p1, v4, v5

    goto :goto_0

    .line 101
    :cond_5
    iget-object v4, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->newStarts:[I

    aget v4, v4, v2

    add-int/2addr v4, p1

    iget-object v5, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->starts:[I

    aget v5, v5, v2

    sub-int p1, v4, v5

    goto :goto_0

    .line 104
    .end local v2    # "mid":I
    .end local v3    # "midValue":I
    :cond_6
    iget-object v4, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->docMaps:[[I

    aget-object v4, v4, v0

    if-eqz v4, :cond_7

    .line 105
    iget-object v4, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->newStarts:[I

    aget v4, v4, v0

    iget-object v5, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->docMaps:[[I

    aget-object v5, v5, v0

    iget-object v6, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->starts:[I

    aget v6, v6, v0

    sub-int v6, p1, v6

    aget v5, v5, v6

    add-int p1, v4, v5

    goto :goto_0

    .line 107
    :cond_7
    iget-object v4, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->newStarts:[I

    aget v4, v4, v0

    add-int/2addr v4, p1

    iget-object v5, p0, Lorg/apache/lucene/index/MergeDocIDRemapper;->starts:[I

    aget v5, v5, v0

    sub-int p1, v4, v5

    goto :goto_0
.end method
