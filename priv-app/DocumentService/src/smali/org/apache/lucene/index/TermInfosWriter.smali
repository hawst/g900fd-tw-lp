.class final Lorg/apache/lucene/index/TermInfosWriter;
.super Ljava/lang/Object;
.source "TermInfosWriter.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final FORMAT:I = -0x3

.field public static final FORMAT_CURRENT:I = -0x4

.field public static final FORMAT_VERSION_UTF8_LENGTH_IN_BYTES:I = -0x4


# instance fields
.field private fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field indexInterval:I

.field private isIndex:Z

.field private lastFieldNumber:I

.field private lastIndexPointer:J

.field private lastTermBytes:[B

.field private lastTermBytesLength:I

.field private lastTi:Lorg/apache/lucene/index/TermInfo;

.field maxSkipLevels:I

.field private other:Lorg/apache/lucene/index/TermInfosWriter;

.field private output:Lorg/apache/lucene/store/IndexOutput;

.field private size:J

.field skipInterval:I

.field utf16Result1:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

.field utf16Result2:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

.field private utf8Result:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/apache/lucene/index/TermInfosWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermInfosWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;I)V
    .locals 7
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "interval"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lorg/apache/lucene/index/TermInfo;

    invoke-direct {v0}, Lorg/apache/lucene/index/TermInfo;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTi:Lorg/apache/lucene/index/TermInfo;

    .line 61
    const/16 v0, 0x80

    iput v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->indexInterval:I

    .line 68
    const/16 v0, 0x10

    iput v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->skipInterval:I

    .line 73
    const/16 v0, 0xa

    iput v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->maxSkipLevels:I

    .line 77
    const/16 v0, 0xa

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytes:[B

    .line 78
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytesLength:I

    .line 79
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastFieldNumber:I

    .line 82
    new-instance v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-direct {v0}, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf8Result:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    .line 87
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/TermInfosWriter;->initialize(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;IZ)V

    .line 88
    const/4 v6, 0x0

    .line 90
    .local v6, "success":Z
    :try_start_0
    new-instance v0, Lorg/apache/lucene/index/TermInfosWriter;

    const/4 v5, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/TermInfosWriter;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;IZ)V

    iput-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->other:Lorg/apache/lucene/index/TermInfosWriter;

    .line 91
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->other:Lorg/apache/lucene/index/TermInfosWriter;

    iput-object p0, v0, Lorg/apache/lucene/index/TermInfosWriter;->other:Lorg/apache/lucene/index/TermInfosWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    const/4 v6, 0x1

    .line 94
    if-nez v6, :cond_0

    .line 95
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->other:Lorg/apache/lucene/index/TermInfosWriter;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 98
    :cond_0
    return-void

    .line 94
    :catchall_0
    move-exception v0

    if-nez v6, :cond_1

    .line 95
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/io/Closeable;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->other:Lorg/apache/lucene/index/TermInfosWriter;

    aput-object v3, v1, v2

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 94
    :cond_1
    throw v0
.end method

.method private constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;IZ)V
    .locals 2
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "interval"    # I
    .param p5, "isIndex"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v1, 0xa

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lorg/apache/lucene/index/TermInfo;

    invoke-direct {v0}, Lorg/apache/lucene/index/TermInfo;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTi:Lorg/apache/lucene/index/TermInfo;

    .line 61
    const/16 v0, 0x80

    iput v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->indexInterval:I

    .line 68
    const/16 v0, 0x10

    iput v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->skipInterval:I

    .line 73
    iput v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->maxSkipLevels:I

    .line 77
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytes:[B

    .line 78
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytesLength:I

    .line 79
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastFieldNumber:I

    .line 82
    new-instance v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-direct {v0}, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf8Result:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    .line 102
    invoke-direct/range {p0 .. p5}, Lorg/apache/lucene/index/TermInfosWriter;->initialize(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;IZ)V

    .line 103
    return-void
.end method

.method private compareToLastTerm(I[BI)I
    .locals 10
    .param p1, "fieldNumber"    # I
    .param p2, "termBytes"    # [B
    .param p3, "termBytesLength"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v5, -0x1

    .line 146
    iget v6, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastFieldNumber:I

    if-eq v6, p1, :cond_1

    .line 147
    iget-object v6, p0, Lorg/apache/lucene/index/TermInfosWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget v7, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastFieldNumber:I

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/FieldInfos;->fieldName(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/index/TermInfosWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v7, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldName(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 152
    .local v2, "cmp":I
    if-nez v2, :cond_0

    iget v6, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastFieldNumber:I

    if-eq v6, v5, :cond_1

    .line 175
    .end local v2    # "cmp":I
    :cond_0
    :goto_0
    return v2

    .line 156
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytes:[B

    iget v7, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytesLength:I

    iget-object v8, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf16Result1:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    invoke-static {v6, v9, v7, v8}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/UnicodeUtil$UTF16Result;)V

    .line 157
    iget-object v6, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf16Result2:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    invoke-static {p2, v9, p3, v6}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/UnicodeUtil$UTF16Result;)V

    .line 159
    iget-object v6, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf16Result1:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget v6, v6, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    iget-object v7, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf16Result2:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget v7, v7, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    if-ge v6, v7, :cond_2

    .line 160
    iget-object v6, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf16Result1:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget v4, v6, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    .line 164
    .local v4, "len":I
    :goto_1
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v4, :cond_4

    .line 165
    iget-object v6, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf16Result1:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget-object v6, v6, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    aget-char v0, v6, v3

    .line 166
    .local v0, "ch1":C
    iget-object v6, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf16Result2:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget-object v6, v6, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    aget-char v1, v6, v3

    .line 167
    .local v1, "ch2":C
    if-eq v0, v1, :cond_3

    .line 168
    sub-int v2, v0, v1

    goto :goto_0

    .line 162
    .end local v0    # "ch1":C
    .end local v1    # "ch2":C
    .end local v3    # "i":I
    .end local v4    # "len":I
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf16Result2:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget v4, v6, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    .restart local v4    # "len":I
    goto :goto_1

    .line 164
    .restart local v0    # "ch1":C
    .restart local v1    # "ch2":C
    .restart local v3    # "i":I
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 170
    .end local v0    # "ch1":C
    .end local v1    # "ch2":C
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf16Result1:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget v6, v6, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    if-nez v6, :cond_5

    iget v6, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastFieldNumber:I

    if-ne v6, v5, :cond_5

    move v2, v5

    .line 173
    goto :goto_0

    .line 175
    :cond_5
    iget-object v5, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf16Result1:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget v5, v5, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    iget-object v6, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf16Result2:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget v6, v6, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    sub-int v2, v5, v6

    goto :goto_0
.end method

.method private initUTF16Results()Z
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    invoke-direct {v0}, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf16Result1:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    .line 139
    new-instance v0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    invoke-direct {v0}, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf16Result2:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    .line 140
    const/4 v0, 0x1

    return v0
.end method

.method private initialize(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;IZ)V
    .locals 6
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "interval"    # I
    .param p5, "isi"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 107
    iput p4, p0, Lorg/apache/lucene/index/TermInfosWriter;->indexInterval:I

    .line 108
    iput-object p3, p0, Lorg/apache/lucene/index/TermInfosWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 109
    iput-boolean p5, p0, Lorg/apache/lucene/index/TermInfosWriter;->isIndex:Z

    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->isIndex:Z

    if-eqz v1, :cond_1

    const-string/jumbo v1, ".tii"

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    .line 111
    const/4 v0, 0x0

    .line 113
    .local v0, "success":Z
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    const/4 v2, -0x4

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 114
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 115
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    iget v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->indexInterval:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 116
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    iget v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->skipInterval:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 117
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    iget v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->maxSkipLevels:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 118
    sget-boolean v1, Lorg/apache/lucene/index/TermInfosWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    invoke-direct {p0}, Lorg/apache/lucene/index/TermInfosWriter;->initUTF16Results()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    :catchall_0
    move-exception v1

    if-nez v0, :cond_0

    .line 122
    new-array v2, v5, [Ljava/io/Closeable;

    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 121
    :cond_0
    throw v1

    .line 110
    .end local v0    # "success":Z
    :cond_1
    const-string/jumbo v1, ".tis"

    goto :goto_0

    .line 119
    .restart local v0    # "success":Z
    :cond_2
    const/4 v0, 0x1

    .line 121
    if-nez v0, :cond_3

    .line 122
    new-array v1, v5, [Ljava/io/Closeable;

    iget-object v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v1, v4

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 125
    :cond_3
    return-void
.end method

.method private writeTerm(I[BI)V
    .locals 5
    .param p1, "fieldNumber"    # I
    .param p2, "termBytes"    # [B
    .param p3, "termBytesLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    const/4 v2, 0x0

    .line 222
    .local v2, "start":I
    iget v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytesLength:I

    if-ge p3, v3, :cond_2

    move v1, p3

    .line 223
    .local v1, "limit":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 224
    aget-byte v3, p2, v2

    iget-object v4, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytes:[B

    aget-byte v4, v4, v2

    if-eq v3, v4, :cond_3

    .line 229
    :cond_0
    sub-int v0, p3, v2

    .line 230
    .local v0, "length":I
    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 231
    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 232
    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, p2, v2, v0}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 233
    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 234
    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytes:[B

    array-length v3, v3

    if-ge v3, p3, :cond_1

    .line 235
    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytes:[B

    invoke-static {v3, p3}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytes:[B

    .line 237
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytes:[B

    invoke-static {p2, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 238
    iput p3, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytesLength:I

    .line 239
    return-void

    .line 222
    .end local v0    # "length":I
    .end local v1    # "limit":I
    :cond_2
    iget v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytesLength:I

    goto :goto_0

    .line 226
    .restart local v1    # "limit":I
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method add(I[BILorg/apache/lucene/index/TermInfo;)V
    .locals 7
    .param p1, "fieldNumber"    # I
    .param p2, "termBytes"    # [B
    .param p3, "termBytesLength"    # I
    .param p4, "ti"    # Lorg/apache/lucene/index/TermInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 188
    sget-boolean v0, Lorg/apache/lucene/index/TermInfosWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/index/TermInfosWriter;->compareToLastTerm(I[BI)I

    move-result v0

    if-ltz v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->isIndex:Z

    if-eqz v0, :cond_0

    if-nez p3, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytesLength:I

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Terms are out of order: field="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " (number "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " lastField="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastFieldNumber:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/FieldInfos;->fieldName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " (number "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastFieldNumber:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " text="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/String;

    const-string/jumbo v3, "UTF-8"

    invoke-direct {v2, p2, v6, p3, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " lastText="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytes:[B

    iget v4, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytesLength:I

    const-string/jumbo v5, "UTF-8"

    invoke-direct {v2, v3, v6, v4, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 190
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/index/TermInfosWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-wide v0, p4, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    iget-object v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTi:Lorg/apache/lucene/index/TermInfo;

    iget-wide v2, v2, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "freqPointer out of order ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p4, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTi:Lorg/apache/lucene/index/TermInfo;

    iget-wide v2, v2, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 191
    :cond_2
    sget-boolean v0, Lorg/apache/lucene/index/TermInfosWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-wide v0, p4, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    iget-object v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTi:Lorg/apache/lucene/index/TermInfo;

    iget-wide v2, v2, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "proxPointer out of order ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p4, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTi:Lorg/apache/lucene/index/TermInfo;

    iget-wide v2, v2, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 193
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->isIndex:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->size:J

    iget v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->indexInterval:I

    int-to-long v2, v2

    rem-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 194
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->other:Lorg/apache/lucene/index/TermInfosWriter;

    iget v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastFieldNumber:I

    iget-object v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytes:[B

    iget v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTermBytesLength:I

    iget-object v4, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTi:Lorg/apache/lucene/index/TermInfo;

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/lucene/index/TermInfosWriter;->add(I[BILorg/apache/lucene/index/TermInfo;)V

    .line 196
    :cond_4
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/index/TermInfosWriter;->writeTerm(I[BI)V

    .line 198
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    iget v1, p4, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 199
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    iget-wide v2, p4, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTi:Lorg/apache/lucene/index/TermInfo;

    iget-wide v4, v1, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 200
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    iget-wide v2, p4, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTi:Lorg/apache/lucene/index/TermInfo;

    iget-wide v4, v1, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 202
    iget v0, p4, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    iget v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->skipInterval:I

    if-lt v0, v1, :cond_5

    .line 203
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    iget v1, p4, Lorg/apache/lucene/index/TermInfo;->skipOffset:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 206
    :cond_5
    iget-boolean v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->isIndex:Z

    if-eqz v0, :cond_6

    .line 207
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->other:Lorg/apache/lucene/index/TermInfosWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    iget-wide v4, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastIndexPointer:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 208
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->other:Lorg/apache/lucene/index/TermInfosWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastIndexPointer:J

    .line 211
    :cond_6
    iput p1, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastFieldNumber:I

    .line 212
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->lastTi:Lorg/apache/lucene/index/TermInfo;

    invoke-virtual {v0, p4}, Lorg/apache/lucene/index/TermInfo;->set(Lorg/apache/lucene/index/TermInfo;)V

    .line 213
    iget-wide v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->size:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->size:J

    .line 214
    return-void
.end method

.method add(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermInfo;)V
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "ti"    # Lorg/apache/lucene/index/TermInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p1, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p1, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf8Result:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8(Ljava/lang/String;IILorg/apache/lucene/util/UnicodeUtil$UTF8Result;)V

    .line 129
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget-object v1, p1, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfos;->fieldNumber(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf8Result:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    iget-object v1, v1, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    iget-object v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->utf8Result:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    iget v2, v2, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->length:I

    invoke-virtual {p0, v0, v1, v2, p2}, Lorg/apache/lucene/index/TermInfosWriter;->add(I[BILorg/apache/lucene/index/TermInfo;)V

    .line 130
    return-void
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 244
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->seek(J)V

    .line 245
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    iget-wide v2, p0, Lorg/apache/lucene/index/TermInfosWriter;->size:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 250
    iget-boolean v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->isIndex:Z

    if-nez v0, :cond_0

    .line 251
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosWriter;->other:Lorg/apache/lucene/index/TermInfosWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermInfosWriter;->close()V

    .line 256
    :cond_0
    return-void

    .line 247
    :catchall_0
    move-exception v0

    .line 248
    :try_start_2
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->output:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 250
    iget-boolean v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->isIndex:Z

    if-nez v1, :cond_1

    .line 251
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->other:Lorg/apache/lucene/index/TermInfosWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermInfosWriter;->close()V

    .line 247
    :cond_1
    throw v0

    .line 250
    :catchall_1
    move-exception v0

    iget-boolean v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->isIndex:Z

    if-nez v1, :cond_2

    .line 251
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->other:Lorg/apache/lucene/index/TermInfosWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermInfosWriter;->close()V

    .line 250
    :cond_2
    throw v0

    :catchall_2
    move-exception v0

    iget-boolean v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->isIndex:Z

    if-nez v1, :cond_3

    .line 251
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosWriter;->other:Lorg/apache/lucene/index/TermInfosWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermInfosWriter;->close()V

    .line 250
    :cond_3
    throw v0
.end method
