.class Lorg/apache/lucene/index/IndexWriter$ReaderPool;
.super Ljava/lang/Object;
.source "IndexWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/IndexWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ReaderPool"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final readerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            "Lorg/apache/lucene/index/SegmentReader;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 472
    const-class v0, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 1

    .prologue
    .line 472
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 474
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method declared-synchronized clear(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 478
    .local p1, "infos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    monitor-enter p0

    if-nez p1, :cond_0

    .line 479
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 480
    .local v0, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/SegmentReader;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SegmentReader;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 478
    .end local v0    # "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/SegmentReader;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 483
    :cond_0
    :try_start_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentInfo;

    .line 484
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfo;
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/SegmentReader;

    .line 485
    .local v3, "r":Lorg/apache/lucene/index/SegmentReader;
    if-eqz v3, :cond_1

    .line 486
    const/4 v4, 0x0

    iput-boolean v4, v3, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 490
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v3    # "r":Lorg/apache/lucene/index/SegmentReader;
    :cond_2
    monitor-exit p0

    return-void
.end method

.method declared-synchronized close()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 599
    monitor-enter p0

    :try_start_0
    sget-boolean v3, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    invoke-static {v3}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 601
    :cond_0
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 603
    .local v0, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/SegmentReader;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentReader;

    .line 604
    .local v2, "sr":Lorg/apache/lucene/index/SegmentReader;
    iget-boolean v3, v2, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    if-eqz v3, :cond_2

    .line 605
    sget-boolean v3, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentReader;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->infoIsLive(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 606
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/SegmentReader;->doCommit(Ljava/util/Map;)V

    .line 611
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;
    invoke-static {v3}, Lorg/apache/lucene/index/IndexWriter;->access$100(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/IndexFileDeleter;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v4, v4, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 618
    :cond_2
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentReader;->decRef()V

    goto :goto_0

    .line 621
    .end local v0    # "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/SegmentReader;>;"
    .end local v2    # "sr":Lorg/apache/lucene/index/SegmentReader;
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 622
    monitor-exit p0

    return-void
.end method

.method declared-synchronized commit(Lorg/apache/lucene/index/SegmentInfos;)V
    .locals 6
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 632
    monitor-enter p0

    :try_start_0
    sget-boolean v3, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    invoke-static {v3}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 634
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfo;

    .line 636
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfo;
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentReader;

    .line 637
    .local v2, "sr":Lorg/apache/lucene/index/SegmentReader;
    if-eqz v2, :cond_1

    iget-boolean v3, v2, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    if-eqz v3, :cond_1

    .line 638
    sget-boolean v3, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->infoIsLive(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 639
    :cond_2
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/SegmentReader;->doCommit(Ljava/util/Map;)V

    .line 643
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;
    invoke-static {v3}, Lorg/apache/lucene/index/IndexWriter;->access$100(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/IndexFileDeleter;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v4, v4, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 646
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v2    # "sr":Lorg/apache/lucene/index/SegmentReader;
    :cond_3
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized drop(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 568
    .local p1, "infos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfo;

    .line 569
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->drop(Lorg/apache/lucene/index/SegmentInfo;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 568
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 571
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized drop(Lorg/apache/lucene/index/SegmentInfo;)V
    .locals 2
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 574
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentReader;

    .line 575
    .local v0, "sr":Lorg/apache/lucene/index/SegmentReader;
    if-eqz v0, :cond_0

    .line 576
    const/4 v1, 0x0

    iput-boolean v1, v0, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    .line 577
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 578
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 580
    :cond_0
    monitor-exit p0

    return-void

    .line 574
    .end local v0    # "sr":Lorg/apache/lucene/index/SegmentReader;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized dropAll()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 583
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentReader;

    .line 584
    .local v1, "reader":Lorg/apache/lucene/index/SegmentReader;
    const/4 v2, 0x0

    iput-boolean v2, v1, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    .line 589
    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentReader;->decRef()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 583
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "reader":Lorg/apache/lucene/index/SegmentReader;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 591
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized get(Lorg/apache/lucene/index/SegmentInfo;Z)Lorg/apache/lucene/index/SegmentReader;
    .locals 2
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "doOpenStores"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 671
    monitor-enter p0

    const/16 v0, 0x400

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;
    invoke-static {v1}, Lorg/apache/lucene/index/IndexWriter;->access$200(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriterConfig;->getReaderTermsIndexDivisor()I

    move-result v1

    invoke-virtual {p0, p1, p2, v0, v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfo;ZII)Lorg/apache/lucene/index/SegmentReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get(Lorg/apache/lucene/index/SegmentInfo;ZII)Lorg/apache/lucene/index/SegmentReader;
    .locals 7
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "doOpenStores"    # Z
    .param p3, "readBufferSize"    # I
    .param p4, "termsIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 687
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->poolReaders:Z
    invoke-static {v0}, Lorg/apache/lucene/index/IndexWriter;->access$000(Lorg/apache/lucene/index/IndexWriter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 688
    const/16 p3, 0x400

    .line 691
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/SegmentReader;

    .line 692
    .local v6, "sr":Lorg/apache/lucene/index/SegmentReader;
    if-nez v6, :cond_3

    .line 696
    const/4 v0, 0x0

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    move-object v2, p1

    move v3, p3

    move v4, p2

    move v5, p4

    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/index/SegmentReader;->get(ZLorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;IZI)Lorg/apache/lucene/index/SegmentReader;

    move-result-object v6

    .line 698
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;
    invoke-static {v1}, Lorg/apache/lucene/index/IndexWriter;->access$300(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/store/Directory;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 700
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v0, p1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 718
    :cond_1
    :goto_0
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;
    invoke-static {v1}, Lorg/apache/lucene/index/IndexWriter;->access$300(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/store/Directory;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 720
    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentReader;->incRef()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 722
    :cond_2
    monitor-exit p0

    return-object v6

    .line 703
    :cond_3
    if-eqz p2, :cond_4

    .line 704
    :try_start_1
    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentReader;->openDocStores()V

    .line 706
    :cond_4
    const/4 v0, -0x1

    if-eq p4, v0, :cond_1

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentReader;->termsIndexLoaded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 713
    invoke-virtual {v6, p4}, Lorg/apache/lucene/index/SegmentReader;->loadTermsIndex(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 687
    .end local v6    # "sr":Lorg/apache/lucene/index/SegmentReader;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getIfExists(Lorg/apache/lucene/index/SegmentInfo;)Lorg/apache/lucene/index/SegmentReader;
    .locals 2
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 727
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentReader;

    .line 728
    .local v0, "sr":Lorg/apache/lucene/index/SegmentReader;
    if-eqz v0, :cond_0

    .line 729
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->incRef()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 731
    :cond_0
    monitor-exit p0

    return-object v0

    .line 727
    .end local v0    # "sr":Lorg/apache/lucene/index/SegmentReader;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getReadOnlyClone(Lorg/apache/lucene/index/SegmentInfo;ZI)Lorg/apache/lucene/index/SegmentReader;
    .locals 2
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "doOpenStores"    # Z
    .param p3, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 654
    monitor-enter p0

    const/16 v1, 0x400

    :try_start_0
    invoke-virtual {p0, p1, p2, v1, p3}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfo;ZII)Lorg/apache/lucene/index/SegmentReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 656
    .local v0, "sr":Lorg/apache/lucene/index/SegmentReader;
    const/4 v1, 0x1

    :try_start_1
    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentReader;->clone(Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 658
    :try_start_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->decRef()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 656
    monitor-exit p0

    return-object v1

    .line 658
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->decRef()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 654
    .end local v0    # "sr":Lorg/apache/lucene/index/SegmentReader;
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized infoIsLive(Lorg/apache/lucene/index/SegmentInfo;)Z
    .locals 4
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    .line 494
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/SegmentInfos;->indexOf(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v0

    .line 495
    .local v0, "idx":I
    sget-boolean v1, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "info="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " isn\'t in pool"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 494
    .end local v0    # "idx":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 496
    .restart local v0    # "idx":I
    :cond_0
    :try_start_1
    sget-boolean v1, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v1

    if-eq v1, p1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "info="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " doesn\'t match live info in segmentInfos"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 497
    :cond_1
    const/4 v1, 0x1

    monitor-exit p0

    return v1
.end method

.method public declared-synchronized mapToLive(Lorg/apache/lucene/index/SegmentInfo;)Lorg/apache/lucene/index/SegmentInfo;
    .locals 2
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    .line 501
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/SegmentInfos;->indexOf(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v0

    .line 502
    .local v0, "idx":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 503
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object p1

    .line 505
    :cond_0
    monitor-exit p0

    return-object p1

    .line 501
    .end local v0    # "idx":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized release(Lorg/apache/lucene/index/SegmentReader;)Z
    .locals 1
    .param p1, "sr"    # Lorg/apache/lucene/index/SegmentReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 518
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/SegmentReader;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized release(Lorg/apache/lucene/index/SegmentReader;Z)Z
    .locals 5
    .param p1, "sr"    # Lorg/apache/lucene/index/SegmentReader;
    .param p2, "drop"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 532
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentReader;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    .line 534
    .local v1, "pooled":Z
    sget-boolean v3, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-eqz v1, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentReader;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eq v3, p1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 532
    .end local v1    # "pooled":Z
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 538
    .restart local v1    # "pooled":Z
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentReader;->decRef()V

    .line 540
    if-eqz v1, :cond_4

    if-nez p2, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->poolReaders:Z
    invoke-static {v3}, Lorg/apache/lucene/index/IndexWriter;->access$000(Lorg/apache/lucene/index/IndexWriter;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentReader;->getRefCount()I

    move-result v3

    if-ne v3, v2, :cond_4

    .line 544
    :cond_1
    sget-boolean v3, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget-boolean v3, p1, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    invoke-static {v3}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 549
    :cond_2
    iget-boolean v3, p1, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    if-nez p2, :cond_3

    move v0, v2

    :cond_3
    and-int v2, v3, v0

    iput-boolean v2, p1, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    .line 551
    iget-boolean v0, p1, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    .line 555
    .local v0, "hasChanges":Z
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentReader;->close()V

    .line 559
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentReader;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 564
    .end local v0    # "hasChanges":Z
    :cond_4
    monitor-exit p0

    return v0
.end method
