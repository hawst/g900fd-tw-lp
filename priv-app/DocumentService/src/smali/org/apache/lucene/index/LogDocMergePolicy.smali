.class public Lorg/apache/lucene/index/LogDocMergePolicy;
.super Lorg/apache/lucene/index/LogMergePolicy;
.source "LogDocMergePolicy.java"


# static fields
.field public static final DEFAULT_MIN_MERGE_DOCS:I = 0x3e8


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    .line 31
    invoke-direct {p0}, Lorg/apache/lucene/index/LogMergePolicy;-><init>()V

    .line 32
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lorg/apache/lucene/index/LogDocMergePolicy;->minMergeSize:J

    .line 36
    iput-wide v2, p0, Lorg/apache/lucene/index/LogDocMergePolicy;->maxMergeSize:J

    .line 37
    iput-wide v2, p0, Lorg/apache/lucene/index/LogDocMergePolicy;->maxMergeSizeForForcedMerge:J

    .line 38
    return-void
.end method


# virtual methods
.method public getMinMergeDocs()I
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lorg/apache/lucene/index/LogDocMergePolicy;->minMergeSize:J

    long-to-int v0, v0

    return v0
.end method

.method public setMinMergeDocs(I)V
    .locals 2
    .param p1, "minMergeDocs"    # I

    .prologue
    .line 55
    int-to-long v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/index/LogDocMergePolicy;->minMergeSize:J

    .line 56
    return-void
.end method

.method protected size(Lorg/apache/lucene/index/SegmentInfo;)J
    .locals 2
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/LogDocMergePolicy;->sizeDocs(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v0

    return-wide v0
.end method
