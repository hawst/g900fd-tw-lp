.class Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;
.super Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
.source "DocFieldProcessorPerThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocFieldProcessorPerThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PerDoc"
.end annotation


# instance fields
.field one:Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

.field final synthetic this$0:Lorg/apache/lucene/index/DocFieldProcessorPerThread;

.field two:Lorg/apache/lucene/index/DocumentsWriter$DocWriter;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/DocFieldProcessorPerThread;)V
    .locals 0

    .prologue
    .line 330
    iput-object p1, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->this$0:Lorg/apache/lucene/index/DocFieldProcessorPerThread;

    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;-><init>()V

    return-void
.end method


# virtual methods
.method public abort()V
    .locals 2

    .prologue
    .line 357
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->one:Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->abort()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 359
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->two:Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->abort()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 362
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->this$0:Lorg/apache/lucene/index/DocFieldProcessorPerThread;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->freePerDoc(Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;)V

    .line 364
    return-void

    .line 359
    :catchall_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->two:Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->abort()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 362
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->this$0:Lorg/apache/lucene/index/DocFieldProcessorPerThread;

    invoke-virtual {v1, p0}, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->freePerDoc(Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;)V

    throw v0
.end method

.method public finish()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 344
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->one:Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->two:Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->finish()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 349
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->this$0:Lorg/apache/lucene/index/DocFieldProcessorPerThread;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->freePerDoc(Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;)V

    .line 351
    return-void

    .line 346
    :catchall_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->two:Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->finish()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 349
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->this$0:Lorg/apache/lucene/index/DocFieldProcessorPerThread;

    invoke-virtual {v1, p0}, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->freePerDoc(Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;)V

    throw v0
.end method

.method public sizeInBytes()J
    .locals 4

    .prologue
    .line 337
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->one:Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->sizeInBytes()J

    move-result-wide v0

    iget-object v2, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->two:Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->sizeInBytes()J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method
