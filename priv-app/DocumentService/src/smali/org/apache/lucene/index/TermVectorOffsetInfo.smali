.class public Lorg/apache/lucene/index/TermVectorOffsetInfo;
.super Ljava/lang/Object;
.source "TermVectorOffsetInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final transient EMPTY_OFFSET_INFO:[Lorg/apache/lucene/index/TermVectorOffsetInfo;


# instance fields
.field private endOffset:I

.field private startOffset:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/lucene/index/TermVectorOffsetInfo;

    sput-object v0, Lorg/apache/lucene/index/TermVectorOffsetInfo;->EMPTY_OFFSET_INFO:[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "startOffset"    # I
    .param p2, "endOffset"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput p2, p0, Lorg/apache/lucene/index/TermVectorOffsetInfo;->endOffset:I

    .line 40
    iput p1, p0, Lorg/apache/lucene/index/TermVectorOffsetInfo;->startOffset:I

    .line 41
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    if-ne p0, p1, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v1

    .line 76
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/index/TermVectorOffsetInfo;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 78
    check-cast v0, Lorg/apache/lucene/index/TermVectorOffsetInfo;

    .line 80
    .local v0, "termVectorOffsetInfo":Lorg/apache/lucene/index/TermVectorOffsetInfo;
    iget v3, p0, Lorg/apache/lucene/index/TermVectorOffsetInfo;->endOffset:I

    iget v4, v0, Lorg/apache/lucene/index/TermVectorOffsetInfo;->endOffset:I

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    .line 81
    :cond_3
    iget v3, p0, Lorg/apache/lucene/index/TermVectorOffsetInfo;->startOffset:I

    iget v4, v0, Lorg/apache/lucene/index/TermVectorOffsetInfo;->startOffset:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getEndOffset()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lorg/apache/lucene/index/TermVectorOffsetInfo;->endOffset:I

    return v0
.end method

.method public getStartOffset()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lorg/apache/lucene/index/TermVectorOffsetInfo;->startOffset:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 89
    iget v0, p0, Lorg/apache/lucene/index/TermVectorOffsetInfo;->startOffset:I

    .line 90
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1d

    iget v2, p0, Lorg/apache/lucene/index/TermVectorOffsetInfo;->endOffset:I

    add-int v0, v1, v2

    .line 91
    return v0
.end method

.method public setEndOffset(I)V
    .locals 0
    .param p1, "endOffset"    # I

    .prologue
    .line 52
    iput p1, p0, Lorg/apache/lucene/index/TermVectorOffsetInfo;->endOffset:I

    .line 53
    return-void
.end method

.method public setStartOffset(I)V
    .locals 0
    .param p1, "startOffset"    # I

    .prologue
    .line 65
    iput p1, p0, Lorg/apache/lucene/index/TermVectorOffsetInfo;->startOffset:I

    .line 66
    return-void
.end method
