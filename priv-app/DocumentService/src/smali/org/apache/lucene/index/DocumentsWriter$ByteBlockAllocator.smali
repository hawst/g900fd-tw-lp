.class Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;
.super Lorg/apache/lucene/index/ByteBlockPool$Allocator;
.source "DocumentsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ByteBlockAllocator"
.end annotation


# instance fields
.field final blockSize:I

.field freeByteBlocks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/lucene/index/DocumentsWriter;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/DocumentsWriter;I)V
    .locals 1
    .param p2, "blockSize"    # I

    .prologue
    .line 1096
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->this$0:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-direct {p0}, Lorg/apache/lucene/index/ByteBlockPool$Allocator;-><init>()V

    .line 1100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    .line 1097
    iput p2, p0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->blockSize:I

    .line 1098
    return-void
.end method


# virtual methods
.method getByteBlock()[B
    .locals 6

    .prologue
    .line 1105
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->this$0:Lorg/apache/lucene/index/DocumentsWriter;

    monitor-enter v3

    .line 1106
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 1108
    .local v1, "size":I
    if-nez v1, :cond_0

    .line 1109
    iget v2, p0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->blockSize:I

    new-array v0, v2, [B

    .line 1110
    .local v0, "b":[B
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->this$0:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    iget v4, p0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->blockSize:I

    int-to-long v4, v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 1113
    :goto_0
    monitor-exit v3

    return-object v0

    .line 1112
    .end local v0    # "b":[B
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .restart local v0    # "b":[B
    goto :goto_0

    .line 1114
    .end local v0    # "b":[B
    .end local v1    # "size":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method recycleByteBlocks(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 1131
    .local p1, "blocks":Ljava/util/List;, "Ljava/util/List<[B>;"
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->this$0:Lorg/apache/lucene/index/DocumentsWriter;

    monitor-enter v3

    .line 1132
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 1133
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 1134
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1135
    const/4 v2, 0x0

    invoke-interface {p1, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1133
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1137
    :cond_0
    monitor-exit v3

    .line 1138
    return-void

    .line 1137
    .end local v0    # "i":I
    .end local v1    # "size":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method recycleByteBlocks([[BII)V
    .locals 4
    .param p1, "blocks"    # [[B
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 1121
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->this$0:Lorg/apache/lucene/index/DocumentsWriter;

    monitor-enter v2

    .line 1122
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_0

    .line 1123
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    aget-object v3, p1, v0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1124
    const/4 v1, 0x0

    aput-object v1, p1, v0

    .line 1122
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1126
    :cond_0
    monitor-exit v2

    .line 1127
    return-void

    .line 1126
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
