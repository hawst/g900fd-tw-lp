.class final Lorg/apache/lucene/index/NormsWriterPerField;
.super Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;
.source "NormsWriterPerField.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/index/NormsWriterPerField;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field docIDs:[I

.field final docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

.field final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field final fieldState:Lorg/apache/lucene/index/FieldInvertState;

.field norms:[B

.field final perThread:Lorg/apache/lucene/index/NormsWriterPerThread;

.field upto:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/apache/lucene/index/NormsWriterPerField;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/NormsWriterPerField;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/NormsWriterPerThread;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 2
    .param p1, "docInverterPerField"    # Lorg/apache/lucene/index/DocInverterPerField;
    .param p2, "perThread"    # Lorg/apache/lucene/index/NormsWriterPerThread;
    .param p3, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    const/4 v1, 0x1

    .line 48
    invoke-direct {p0}, Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;-><init>()V

    .line 35
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/NormsWriterPerField;->docIDs:[I

    .line 36
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/lucene/index/NormsWriterPerField;->norms:[B

    .line 49
    iput-object p2, p0, Lorg/apache/lucene/index/NormsWriterPerField;->perThread:Lorg/apache/lucene/index/NormsWriterPerThread;

    .line 50
    iput-object p3, p0, Lorg/apache/lucene/index/NormsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 51
    iget-object v0, p2, Lorg/apache/lucene/index/NormsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/NormsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 52
    iget-object v0, p1, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iput-object v0, p0, Lorg/apache/lucene/index/NormsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    .line 53
    return-void
.end method


# virtual methods
.method abort()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    .line 58
    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 28
    check-cast p1, Lorg/apache/lucene/index/NormsWriterPerField;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/NormsWriterPerField;->compareTo(Lorg/apache/lucene/index/NormsWriterPerField;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/index/NormsWriterPerField;)I
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/index/NormsWriterPerField;

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/lucene/index/NormsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/index/NormsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method finish()V
    .locals 4

    .prologue
    .line 66
    iget-object v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-boolean v1, v1, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-boolean v1, v1, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    if-nez v1, :cond_4

    .line 67
    iget-object v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->docIDs:[I

    array-length v1, v1

    iget v2, p0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    if-gt v1, v2, :cond_1

    .line 68
    sget-boolean v1, Lorg/apache/lucene/index/NormsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->docIDs:[I

    array-length v1, v1

    iget v2, p0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 69
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->docIDs:[I

    iget v2, p0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->docIDs:[I

    .line 71
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->norms:[B

    array-length v1, v1

    iget v2, p0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    if-gt v1, v2, :cond_3

    .line 72
    sget-boolean v1, Lorg/apache/lucene/index/NormsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->norms:[B

    array-length v1, v1

    iget v2, p0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    if-eq v1, v2, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 73
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->norms:[B

    iget v2, p0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->norms:[B

    .line 75
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->similarity:Lorg/apache/lucene/search/Similarity;

    iget-object v2, p0, Lorg/apache/lucene/index/NormsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/index/NormsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/search/Similarity;->computeNorm(Ljava/lang/String;Lorg/apache/lucene/index/FieldInvertState;)F

    move-result v0

    .line 76
    .local v0, "norm":F
    iget-object v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->norms:[B

    iget v2, p0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    iget-object v3, p0, Lorg/apache/lucene/index/NormsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget-object v3, v3, Lorg/apache/lucene/index/DocumentsWriter$DocState;->similarity:Lorg/apache/lucene/search/Similarity;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/search/Similarity;->encodeNormValue(F)B

    move-result v3

    aput-byte v3, v1, v2

    .line 77
    iget-object v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->docIDs:[I

    iget v2, p0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    iget-object v3, p0, Lorg/apache/lucene/index/NormsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v3, v3, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    aput v3, v1, v2

    .line 78
    iget v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    .line 80
    .end local v0    # "norm":F
    :cond_4
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/index/NormsWriterPerField;->docIDs:[I

    iget v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->shrink([II)[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/NormsWriterPerField;->docIDs:[I

    .line 44
    iget-object v0, p0, Lorg/apache/lucene/index/NormsWriterPerField;->norms:[B

    iget v1, p0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->shrink([BI)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/NormsWriterPerField;->norms:[B

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    .line 46
    return-void
.end method
