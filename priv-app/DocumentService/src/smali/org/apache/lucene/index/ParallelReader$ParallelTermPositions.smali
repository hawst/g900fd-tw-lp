.class Lorg/apache/lucene/index/ParallelReader$ParallelTermPositions;
.super Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;
.source "ParallelReader.java"

# interfaces
.implements Lorg/apache/lucene/index/TermPositions;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/ParallelReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ParallelTermPositions"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/index/ParallelReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/ParallelReader;)V
    .locals 0

    .prologue
    .line 628
    iput-object p1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermPositions;->this$0:Lorg/apache/lucene/index/ParallelReader;

    invoke-direct {p0, p1}, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;-><init>(Lorg/apache/lucene/index/ParallelReader;)V

    return-void
.end method


# virtual methods
.method public getPayload([BI)[B
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 646
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermPositions;->termDocs:Lorg/apache/lucene/index/TermDocs;

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0, p1, p2}, Lorg/apache/lucene/index/TermPositions;->getPayload([BI)[B

    move-result-object v0

    return-object v0
.end method

.method public getPayloadLength()I
    .locals 1

    .prologue
    .line 642
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermPositions;->termDocs:Lorg/apache/lucene/index/TermDocs;

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->getPayloadLength()I

    move-result v0

    return v0
.end method

.method public isPayloadAvailable()Z
    .locals 1

    .prologue
    .line 652
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermPositions;->termDocs:Lorg/apache/lucene/index/TermDocs;

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->isPayloadAvailable()Z

    move-result v0

    return v0
.end method

.method public nextPosition()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 638
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermPositions;->termDocs:Lorg/apache/lucene/index/TermDocs;

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->nextPosition()I

    move-result v0

    return v0
.end method

.method public seek(Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 632
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermPositions;->this$0:Lorg/apache/lucene/index/ParallelReader;

    # getter for: Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;
    invoke-static {v1}, Lorg/apache/lucene/index/ParallelReader;->access$000(Lorg/apache/lucene/index/ParallelReader;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;

    .line 633
    .local v0, "reader":Lorg/apache/lucene/index/IndexReader;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->termPositions(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermPositions;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermPositions;->termDocs:Lorg/apache/lucene/index/TermDocs;

    .line 634
    return-void

    .line 633
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
