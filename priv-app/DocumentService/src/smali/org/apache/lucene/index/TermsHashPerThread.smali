.class final Lorg/apache/lucene/index/TermsHashPerThread;
.super Lorg/apache/lucene/index/InvertedDocConsumerPerThread;
.source "TermsHashPerThread.java"


# instance fields
.field final bytePool:Lorg/apache/lucene/index/ByteBlockPool;

.field final charPool:Lorg/apache/lucene/index/CharBlockPool;

.field final consumer:Lorg/apache/lucene/index/TermsHashConsumerPerThread;

.field final docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

.field final intPool:Lorg/apache/lucene/index/IntBlockPool;

.field final nextPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

.field final primary:Z

.field final termsHash:Lorg/apache/lucene/index/TermsHash;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocInverterPerThread;Lorg/apache/lucene/index/TermsHash;Lorg/apache/lucene/index/TermsHash;Lorg/apache/lucene/index/TermsHashPerThread;)V
    .locals 2
    .param p1, "docInverterPerThread"    # Lorg/apache/lucene/index/DocInverterPerThread;
    .param p2, "termsHash"    # Lorg/apache/lucene/index/TermsHash;
    .param p3, "nextTermsHash"    # Lorg/apache/lucene/index/TermsHash;
    .param p4, "primaryPerThread"    # Lorg/apache/lucene/index/TermsHashPerThread;

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/lucene/index/InvertedDocConsumerPerThread;-><init>()V

    .line 35
    iget-object v0, p1, Lorg/apache/lucene/index/DocInverterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 37
    iput-object p2, p0, Lorg/apache/lucene/index/TermsHashPerThread;->termsHash:Lorg/apache/lucene/index/TermsHash;

    .line 38
    iget-object v0, p2, Lorg/apache/lucene/index/TermsHash;->consumer:Lorg/apache/lucene/index/TermsHashConsumer;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/TermsHashConsumer;->addThread(Lorg/apache/lucene/index/TermsHashPerThread;)Lorg/apache/lucene/index/TermsHashConsumerPerThread;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerThread;

    .line 40
    if-eqz p3, :cond_0

    .line 42
    new-instance v0, Lorg/apache/lucene/index/CharBlockPool;

    iget-object v1, p2, Lorg/apache/lucene/index/TermsHash;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CharBlockPool;-><init>(Lorg/apache/lucene/index/DocumentsWriter;)V

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->primary:Z

    .line 49
    :goto_0
    new-instance v0, Lorg/apache/lucene/index/IntBlockPool;

    iget-object v1, p2, Lorg/apache/lucene/index/TermsHash;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/IntBlockPool;-><init>(Lorg/apache/lucene/index/DocumentsWriter;)V

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    .line 50
    new-instance v0, Lorg/apache/lucene/index/ByteBlockPool;

    iget-object v1, p2, Lorg/apache/lucene/index/TermsHash;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriter;->byteBlockAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/ByteBlockPool;-><init>(Lorg/apache/lucene/index/ByteBlockPool$Allocator;)V

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    .line 52
    if-eqz p3, :cond_1

    .line 53
    invoke-virtual {p3, p1, p0}, Lorg/apache/lucene/index/TermsHash;->addThread(Lorg/apache/lucene/index/DocInverterPerThread;Lorg/apache/lucene/index/TermsHashPerThread;)Lorg/apache/lucene/index/TermsHashPerThread;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->nextPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    .line 56
    :goto_1
    return-void

    .line 45
    :cond_0
    iget-object v0, p4, Lorg/apache/lucene/index/TermsHashPerThread;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->primary:Z

    goto :goto_0

    .line 55
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->nextPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized abort()V
    .locals 2

    .prologue
    .line 65
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/TermsHashPerThread;->reset(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 67
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerThread;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashConsumerPerThread;->abort()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->nextPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->nextPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashPerThread;->abort()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 73
    :cond_0
    monitor-exit p0

    return-void

    .line 69
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerThread;->nextPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    if-eqz v1, :cond_1

    .line 70
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerThread;->nextPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermsHashPerThread;->abort()V

    .line 69
    :cond_1
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 65
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method addField(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/InvertedDocConsumerPerField;
    .locals 2
    .param p1, "docInverterPerField"    # Lorg/apache/lucene/index/DocInverterPerField;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 60
    new-instance v0, Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerThread;->nextPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    invoke-direct {v0, p1, p0, v1, p2}, Lorg/apache/lucene/index/TermsHashPerField;-><init>(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/TermsHashPerThread;Lorg/apache/lucene/index/TermsHashPerThread;Lorg/apache/lucene/index/FieldInfo;)V

    return-object v0
.end method

.method public finishDocument()Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerThread;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerThread;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsHashConsumerPerThread;->finishDocument()Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    move-result-object v0

    .line 87
    .local v0, "doc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerThread;->nextPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    if-eqz v2, :cond_0

    .line 88
    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerThread;->nextPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    iget-object v2, v2, Lorg/apache/lucene/index/TermsHashPerThread;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerThread;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsHashConsumerPerThread;->finishDocument()Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    move-result-object v1

    .line 91
    .local v1, "doc2":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :goto_0
    if-nez v0, :cond_1

    .line 95
    .end local v1    # "doc2":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :goto_1
    return-object v1

    .line 90
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "doc2":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    goto :goto_0

    .line 94
    :cond_1
    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->setNext(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)V

    move-object v1, v0

    .line 95
    goto :goto_1
.end method

.method reset(Z)V
    .locals 1
    .param p1, "recyclePostings"    # Z

    .prologue
    .line 101
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IntBlockPool;->reset()V

    .line 102
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    invoke-virtual {v0}, Lorg/apache/lucene/index/ByteBlockPool;->reset()V

    .line 104
    iget-boolean v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->primary:Z

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    invoke-virtual {v0}, Lorg/apache/lucene/index/CharBlockPool;->reset()V

    .line 106
    :cond_0
    return-void
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerThread;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashConsumerPerThread;->startDocument()V

    .line 78
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->nextPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerThread;->nextPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerThread;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerThread;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashConsumerPerThread;->startDocument()V

    .line 80
    :cond_0
    return-void
.end method
