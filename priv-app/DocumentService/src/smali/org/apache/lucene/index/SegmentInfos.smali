.class public final Lorg/apache/lucene/index/SegmentInfos;
.super Ljava/lang/Object;
.source "SegmentInfos.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Cloneable;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/lucene/index/SegmentInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final CURRENT_FORMAT:I = -0xb

.field public static final FORMAT:I = -0x1

.field public static final FORMAT_3_1:I = -0xb

.field public static final FORMAT_CHECKSUM:I = -0x5

.field public static final FORMAT_DEL_COUNT:I = -0x6

.field public static final FORMAT_DIAGNOSTICS:I = -0x9

.field public static final FORMAT_HAS_PROX:I = -0x7

.field public static final FORMAT_HAS_VECTORS:I = -0xa

.field public static final FORMAT_LOCKLESS:I = -0x2

.field public static final FORMAT_MAXIMUM:I = -0xb

.field public static final FORMAT_MINIMUM:I = -0x1

.field public static final FORMAT_SHARED_DOC_STORE:I = -0x4

.field public static final FORMAT_SINGLE_NORM_FILE:I = -0x3

.field public static final FORMAT_USER_DATA:I = -0x8

.field private static defaultGenLookaheadCount:I

.field private static infoStream:Ljava/io/PrintStream;


# instance fields
.field private transient cachedUnmodifiableList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private transient cachedUnmodifiableSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation
.end field

.field public counter:I

.field private format:I

.field private generation:J

.field private lastGeneration:J

.field pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

.field private segmentSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private segments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private userData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field version:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SegmentInfos;->$assertionsDisabled:Z

    .line 133
    const/4 v0, 0x0

    sput-object v0, Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;

    .line 496
    const/16 v0, 0xa

    sput v0, Lorg/apache/lucene/index/SegmentInfos;->defaultGenLookaheadCount:I

    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    .line 113
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->version:J

    .line 115
    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    .line 116
    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    .line 120
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    .line 125
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    .line 545
    return-void
.end method

.method static synthetic access$000()Ljava/io/PrintStream;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-static {p0}, Lorg/apache/lucene/index/SegmentInfos;->message(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 49
    sget v0, Lorg/apache/lucene/index/SegmentInfos;->defaultGenLookaheadCount:I

    return v0
.end method

.method public static generationFromSegmentsFileName(Ljava/lang/String;)J
    .locals 3
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 223
    const-string/jumbo v0, "segments"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    const-wide/16 v0, 0x0

    .line 226
    :goto_0
    return-wide v0

    .line 225
    :cond_0
    const-string/jumbo v0, "segments"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 226
    const-string/jumbo v0, "segments"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x24

    invoke-static {v0, v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    goto :goto_0

    .line 229
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "fileName \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\" is not a segments file"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getDefaultGenLookahedCount()I
    .locals 1

    .prologue
    .line 516
    sget v0, Lorg/apache/lucene/index/SegmentInfos;->defaultGenLookaheadCount:I

    return v0
.end method

.method public static getInfoStream()Ljava/io/PrintStream;
    .locals 1

    .prologue
    .line 523
    sget-object v0, Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;

    return-object v0
.end method

.method public static getLastCommitGeneration(Lorg/apache/lucene/store/Directory;)J
    .locals 4
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/lucene/index/SegmentInfos;->getLastCommitGeneration([Ljava/lang/String;)J
    :try_end_0
    .catch Lorg/apache/lucene/store/NoSuchDirectoryException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 180
    :goto_0
    return-wide v2

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "nsde":Lorg/apache/lucene/store/NoSuchDirectoryException;
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public static getLastCommitGeneration([Ljava/lang/String;)J
    .locals 7
    .param p0, "files"    # [Ljava/lang/String;

    .prologue
    .line 154
    if-nez p0, :cond_1

    .line 155
    const-wide/16 v4, -0x1

    .line 167
    :cond_0
    return-wide v4

    .line 157
    :cond_1
    const-wide/16 v4, -0x1

    .line 158
    .local v4, "max":J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, p0

    if-ge v1, v6, :cond_0

    .line 159
    aget-object v0, p0, v1

    .line 160
    .local v0, "file":Ljava/lang/String;
    const-string/jumbo v6, "segments"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string/jumbo v6, "segments.gen"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 161
    invoke-static {v0}, Lorg/apache/lucene/index/SegmentInfos;->generationFromSegmentsFileName(Ljava/lang/String;)J

    move-result-wide v2

    .line 162
    .local v2, "gen":J
    cmp-long v6, v2, v4

    if-lez v6, :cond_2

    .line 163
    move-wide v4, v2

    .line 158
    .end local v2    # "gen":J
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getLastCommitSegmentsFileName(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;
    .locals 4
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    const-string/jumbo v0, "segments"

    const-string/jumbo v1, ""

    invoke-static {p0}, Lorg/apache/lucene/index/SegmentInfos;->getLastCommitGeneration(Lorg/apache/lucene/store/Directory;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLastCommitSegmentsFileName([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "files"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    const-string/jumbo v0, "segments"

    const-string/jumbo v1, ""

    invoke-static {p0}, Lorg/apache/lucene/index/SegmentInfos;->getLastCommitGeneration([Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static message(Ljava/lang/String;)V
    .locals 3
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    .line 533
    sget-object v0, Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "SIS ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 534
    return-void
.end method

.method public static readCurrentVersion(Lorg/apache/lucene/store/Directory;)J
    .locals 4
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 482
    new-instance v0, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v0}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 483
    .local v0, "sis":Lorg/apache/lucene/index/SegmentInfos;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V

    .line 484
    iget-wide v2, v0, Lorg/apache/lucene/index/SegmentInfos;->version:J

    return-wide v2
.end method

.method public static setDefaultGenLookaheadCount(I)V
    .locals 0
    .param p0, "count"    # I

    .prologue
    .line 508
    sput p0, Lorg/apache/lucene/index/SegmentInfos;->defaultGenLookaheadCount:I

    .line 509
    return-void
.end method

.method public static setInfoStream(Ljava/io/PrintStream;)V
    .locals 0
    .param p0, "infoStream"    # Ljava/io/PrintStream;

    .prologue
    .line 491
    sput-object p0, Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;

    .line 492
    return-void
.end method

.method private final write(Lorg/apache/lucene/store/Directory;)V
    .locals 14
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x1

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 375
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->getNextSegmentFileName()Ljava/lang/String;

    move-result-object v1

    .line 378
    .local v1, "segmentFileName":Ljava/lang/String;
    iget-wide v6, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_1

    .line 379
    iput-wide v12, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    .line 384
    :goto_0
    const/4 v2, 0x0

    .line 385
    .local v2, "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    const/4 v5, 0x0

    .line 388
    .local v5, "success":Z
    :try_start_0
    new-instance v3, Lorg/apache/lucene/store/ChecksumIndexOutput;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v6

    invoke-direct {v3, v6}, Lorg/apache/lucene/store/ChecksumIndexOutput;-><init>(Lorg/apache/lucene/store/IndexOutput;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 389
    .end local v2    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .local v3, "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    const/16 v6, -0xb

    :try_start_1
    invoke-virtual {v3, v6}, Lorg/apache/lucene/store/ChecksumIndexOutput;->writeInt(I)V

    .line 390
    iget-wide v6, p0, Lorg/apache/lucene/index/SegmentInfos;->version:J

    invoke-virtual {v3, v6, v7}, Lorg/apache/lucene/store/ChecksumIndexOutput;->writeLong(J)V

    .line 391
    iget v6, p0, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    invoke-virtual {v3, v6}, Lorg/apache/lucene/store/ChecksumIndexOutput;->writeInt(I)V

    .line 392
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v6

    invoke-virtual {v3, v6}, Lorg/apache/lucene/store/ChecksumIndexOutput;->writeInt(I)V

    .line 393
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SegmentInfo;

    .line 394
    .local v4, "si":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v4, v3}, Lorg/apache/lucene/index/SegmentInfo;->write(Lorg/apache/lucene/store/IndexOutput;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 401
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v4    # "si":Lorg/apache/lucene/index/SegmentInfo;
    :catchall_0
    move-exception v6

    move-object v2, v3

    .end local v3    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .restart local v2    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    :goto_2
    if-nez v5, :cond_0

    .line 404
    new-array v7, v11, [Ljava/io/Closeable;

    aput-object v2, v7, v10

    invoke-static {v7}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 408
    :try_start_2
    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 401
    :cond_0
    :goto_3
    throw v6

    .line 381
    .end local v2    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .end local v5    # "success":Z
    :cond_1
    iget-wide v6, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    add-long/2addr v6, v12

    iput-wide v6, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    goto :goto_0

    .line 396
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v3    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .restart local v5    # "success":Z
    :cond_2
    :try_start_3
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    invoke-virtual {v3, v6}, Lorg/apache/lucene/store/ChecksumIndexOutput;->writeStringStringMap(Ljava/util/Map;)V

    .line 397
    invoke-virtual {v3}, Lorg/apache/lucene/store/ChecksumIndexOutput;->prepareCommit()V

    .line 398
    iput-object v3, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 399
    const/4 v5, 0x1

    .line 401
    if-nez v5, :cond_3

    .line 404
    new-array v6, v11, [Ljava/io/Closeable;

    aput-object v3, v6, v10

    invoke-static {v6}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 408
    :try_start_4
    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    .line 414
    :cond_3
    :goto_4
    return-void

    .line 409
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v3    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .restart local v2    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    :catch_0
    move-exception v7

    goto :goto_3

    .end local v2    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v3    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    :catch_1
    move-exception v6

    goto :goto_4

    .line 401
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v3    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .restart local v2    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    :catchall_1
    move-exception v6

    goto :goto_2
.end method


# virtual methods
.method public add(Lorg/apache/lucene/index/SegmentInfo;)V
    .locals 2
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    .line 1074
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1075
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot add the same segment two times to this SegmentInfos instance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1077
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1078
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1079
    sget-boolean v0, Lorg/apache/lucene/index/SegmentInfos;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1080
    :cond_1
    return-void
.end method

.method public addAll(Ljava/lang/Iterable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1083
    .local p1, "sis":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/index/SegmentInfo;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfo;

    .line 1084
    .local v1, "si":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/SegmentInfos;->add(Lorg/apache/lucene/index/SegmentInfo;)V

    goto :goto_0

    .line 1086
    .end local v1    # "si":Lorg/apache/lucene/index/SegmentInfo;
    :cond_0
    return-void
.end method

.method applyMergeChanges(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V
    .locals 9
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .param p2, "dropSegment"    # Z

    .prologue
    .line 990
    new-instance v3, Ljava/util/HashSet;

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-direct {v3, v6}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 991
    .local v3, "mergedAway":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/SegmentInfo;>;"
    const/4 v2, 0x0

    .line 992
    .local v2, "inserted":Z
    const/4 v4, 0x0

    .line 993
    .local v4, "newSegIdx":I
    const/4 v5, 0x0

    .local v5, "segIdx":I
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    .local v0, "cnt":I
    :goto_0
    if-ge v5, v0, :cond_3

    .line 994
    sget-boolean v6, Lorg/apache/lucene/index/SegmentInfos;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    if-ge v5, v4, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 995
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfo;

    .line 996
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 997
    if-nez v2, :cond_1

    if-nez p2, :cond_1

    .line 998
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    iget-object v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-interface {v6, v5, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 999
    const/4 v2, 0x1

    .line 1000
    add-int/lit8 v4, v4, 0x1

    .line 993
    :cond_1
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1003
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v6, v4, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1004
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1013
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_3
    if-nez v2, :cond_4

    if-nez p2, :cond_4

    .line 1014
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    const/4 v7, 0x0

    iget-object v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-interface {v6, v7, v8}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1018
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    iget-object v7, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-interface {v6, v4, v7}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 1021
    if-nez p2, :cond_5

    .line 1022
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    iget-object v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1024
    :cond_5
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    invoke-interface {v6, v3}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 1026
    sget-boolean v6, Lorg/apache/lucene/index/SegmentInfos;->$assertionsDisabled:Z

    if-nez v6, :cond_6

    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v6

    iget-object v7, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-eq v6, v7, :cond_6

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 1027
    :cond_6
    return-void
.end method

.method public asList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1054
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->cachedUnmodifiableList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1055
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->cachedUnmodifiableList:Ljava/util/List;

    .line 1057
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->cachedUnmodifiableList:Ljava/util/List;

    return-object v0
.end method

.method public asSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1063
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->cachedUnmodifiableSet:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 1064
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->cachedUnmodifiableSet:Ljava/util/Set;

    .line 1066
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->cachedUnmodifiableSet:Ljava/util/Set;

    return-object v0
.end method

.method public changed()V
    .locals 4

    .prologue
    .line 985
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->version:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->version:J

    .line 986
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 1089
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1090
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1091
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 436
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/SegmentInfos;

    .line 438
    .local v3, "sis":Lorg/apache/lucene/index/SegmentInfos;
    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, v3, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    .line 439
    new-instance v4, Ljava/util/HashSet;

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/HashSet;-><init>(I)V

    iput-object v4, v3, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    .line 440
    const/4 v4, 0x0

    iput-object v4, v3, Lorg/apache/lucene/index/SegmentInfos;->cachedUnmodifiableList:Ljava/util/List;

    .line 441
    const/4 v4, 0x0

    iput-object v4, v3, Lorg/apache/lucene/index/SegmentInfos;->cachedUnmodifiableSet:Ljava/util/Set;

    .line 442
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentInfo;

    .line 444
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/SegmentInfos;->add(Lorg/apache/lucene/index/SegmentInfo;)V
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 448
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v3    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :catch_0
    move-exception v0

    .line 449
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v4, Ljava/lang/RuntimeException;

    const-string/jumbo v5, "should not happen"

    invoke-direct {v4, v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 446
    .end local v0    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v3    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_0
    :try_start_1
    new-instance v4, Ljava/util/HashMap;

    iget-object v5, p0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    invoke-direct {v4, v5}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v4, v3, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 447
    return-object v3
.end method

.method final commit(Lorg/apache/lucene/store/Directory;)V
    .locals 0
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 933
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->prepareCommit(Lorg/apache/lucene/store/Directory;)V

    .line 934
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->finishCommit(Lorg/apache/lucene/store/Directory;)V

    .line 935
    return-void
.end method

.method public contains(Lorg/apache/lucene/index/SegmentInfo;)Z
    .locals 1
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    .line 1106
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method createBackupSegmentInfos(Z)Ljava/util/List;
    .locals 4
    .param p1, "cloneChildren"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1030
    if-eqz p1, :cond_0

    .line 1031
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1032
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfo;

    .line 1033
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/SegmentInfo;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1037
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :cond_1
    return-object v2
.end method

.method public files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;
    .locals 6
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "includeSegmentsFile"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            "Z)",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 836
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 837
    .local v0, "files":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 838
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v3

    .line 839
    .local v3, "segmentFileName":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 844
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 847
    .end local v3    # "segmentFileName":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v4

    .line 848
    .local v4, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_2

    .line 849
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v2

    .line 850
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfo;
    iget-object v5, v2, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    if-ne v5, p1, :cond_1

    .line 851
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 848
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 854
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_2
    return-object v0
.end method

.method final finishCommit(Lorg/apache/lucene/store/Directory;)V
    .locals 8
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 858
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    if-nez v4, :cond_0

    .line 859
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string/jumbo v5, "prepareCommit was not called"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 860
    :cond_0
    const/4 v2, 0x0

    .line 862
    .local v2, "success":Z
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/ChecksumIndexOutput;->finishCommit()V

    .line 863
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/ChecksumIndexOutput;->close()V

    .line 864
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 865
    const/4 v2, 0x1

    .line 867
    if-nez v2, :cond_1

    .line 868
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->rollbackCommit(Lorg/apache/lucene/store/Directory;)V

    .line 881
    :cond_1
    const-string/jumbo v4, "segments"

    const-string/jumbo v5, ""

    iget-wide v6, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    invoke-static {v4, v5, v6, v7}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 884
    .local v0, "fileName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 886
    :try_start_1
    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v4

    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 887
    const/4 v2, 0x1

    .line 889
    if-nez v2, :cond_2

    .line 891
    :try_start_2
    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 898
    :cond_2
    :goto_0
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    iput-wide v4, p0, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    .line 901
    :try_start_3
    const-string/jumbo v4, "segments.gen"

    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v1

    .line 903
    .local v1, "genOutput":Lorg/apache/lucene/store/IndexOutput;
    const/4 v4, -0x2

    :try_start_4
    invoke-virtual {v1, v4}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 904
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 905
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 907
    :try_start_5
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 908
    const-string/jumbo v4, "segments.gen"

    invoke-static {v4}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v4

    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    .line 923
    .end local v1    # "genOutput":Lorg/apache/lucene/store/IndexOutput;
    :cond_3
    return-void

    .line 867
    .end local v0    # "fileName":Ljava/lang/String;
    :catchall_0
    move-exception v4

    if-nez v2, :cond_4

    .line 868
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->rollbackCommit(Lorg/apache/lucene/store/Directory;)V

    .line 867
    :cond_4
    throw v4

    .line 889
    .restart local v0    # "fileName":Ljava/lang/String;
    :catchall_1
    move-exception v4

    if-nez v2, :cond_5

    .line 891
    :try_start_6
    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    .line 889
    :cond_5
    :goto_1
    throw v4

    .line 907
    .restart local v1    # "genOutput":Lorg/apache/lucene/store/IndexOutput;
    :catchall_2
    move-exception v4

    :try_start_7
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 908
    const-string/jumbo v5, "segments.gen"

    invoke-static {v5}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v5

    invoke-virtual {p1, v5}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V

    .line 907
    throw v4
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0

    .line 910
    .end local v1    # "genOutput":Lorg/apache/lucene/store/IndexOutput;
    :catch_0
    move-exception v3

    .line 914
    .local v3, "t":Ljava/lang/Throwable;
    :try_start_8
    const-string/jumbo v4, "segments.gen"

    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    .line 919
    :goto_2
    instance-of v4, v3, Lorg/apache/lucene/util/ThreadInterruptedException;

    if-eqz v4, :cond_3

    .line 920
    check-cast v3, Lorg/apache/lucene/util/ThreadInterruptedException;

    .end local v3    # "t":Ljava/lang/Throwable;
    throw v3

    .line 892
    :catch_1
    move-exception v5

    goto :goto_1

    :catch_2
    move-exception v4

    goto :goto_0

    .line 915
    .restart local v3    # "t":Ljava/lang/Throwable;
    :catch_3
    move-exception v4

    goto :goto_2
.end method

.method public getFormat()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lorg/apache/lucene/index/SegmentInfos;->format:I

    return v0
.end method

.method public getGeneration()J
    .locals 2

    .prologue
    .line 460
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    return-wide v0
.end method

.method public getLastGeneration()J
    .locals 2

    .prologue
    .line 463
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    return-wide v0
.end method

.method public getNextSegmentFileName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 240
    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 241
    const-wide/16 v0, 0x1

    .line 245
    .local v0, "nextGeneration":J
    :goto_0
    const-string/jumbo v2, "segments"

    const-string/jumbo v3, ""

    invoke-static {v2, v3, v0, v1}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 243
    .end local v0    # "nextGeneration":J
    :cond_0
    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    const-wide/16 v4, 0x1

    add-long v0, v2, v4

    .restart local v0    # "nextGeneration":J
    goto :goto_0
.end method

.method public getSegmentsFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 213
    const-string/jumbo v0, "segments"

    const-string/jumbo v1, ""

    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 952
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 457
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->version:J

    return-wide v0
.end method

.method public indexOf(Lorg/apache/lucene/index/SegmentInfo;)I
    .locals 1
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    .line 1110
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1111
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1113
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public info(I)Lorg/apache/lucene/index/SegmentInfo;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfo;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1049
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->asList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method final prepareCommit(Lorg/apache/lucene/store/Directory;)V
    .locals 2
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 825
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    if-eqz v0, :cond_0

    .line 826
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "prepareCommit was already called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 827
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->write(Lorg/apache/lucene/store/Directory;)V

    .line 828
    return-void
.end method

.method public pruneDeletedSegments()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 418
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/SegmentInfo;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 419
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfo;

    .line 420
    .local v0, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I

    move-result v2

    iget v3, v0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-ne v2, v3, :cond_0

    .line 421
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 422
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 425
    .end local v0    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_1
    sget-boolean v2, Lorg/apache/lucene/index/SegmentInfos;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eq v2, v3, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 426
    :cond_2
    return-void
.end method

.method public range(II)Lorg/apache/lucene/index/SegmentInfos;
    .locals 2
    .param p1, "first"    # I
    .param p2, "last"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 779
    new-instance v0, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v0}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 780
    .local v0, "infos":Lorg/apache/lucene/index/SegmentInfos;
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v1, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->addAll(Ljava/lang/Iterable;)V

    .line 781
    return-object v0
.end method

.method public final read(Lorg/apache/lucene/store/Directory;)V
    .locals 2
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 357
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    .line 359
    new-instance v0, Lorg/apache/lucene/index/SegmentInfos$1;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/index/SegmentInfos$1;-><init>(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/store/Directory;)V

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos$1;->run()Ljava/lang/Object;

    .line 367
    return-void
.end method

.method public final read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    .locals 18
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 260
    const/4 v13, 0x0

    .line 263
    .local v13, "success":Z
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentInfos;->clear()V

    .line 265
    new-instance v10, Lorg/apache/lucene/store/ChecksumIndexInput;

    invoke-virtual/range {p1 .. p2}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v14

    invoke-direct {v10, v14}, Lorg/apache/lucene/store/ChecksumIndexInput;-><init>(Lorg/apache/lucene/store/IndexInput;)V

    .line 267
    .local v10, "input":Lorg/apache/lucene/store/ChecksumIndexInput;
    invoke-static/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->generationFromSegmentsFileName(Ljava/lang/String;)J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    .line 269
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    move-object/from16 v0, p0

    iput-wide v14, v0, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    .line 272
    :try_start_0
    invoke-virtual {v10}, Lorg/apache/lucene/store/ChecksumIndexInput;->readInt()I

    move-result v8

    .line 274
    .local v8, "format":I
    const/4 v14, -0x1

    if-le v8, v14, :cond_1

    .line 275
    new-instance v14, Lorg/apache/lucene/index/IndexFormatTooOldException;

    const/4 v15, -0x1

    const/16 v16, -0xb

    move/from16 v0, v16

    invoke-direct {v14, v10, v8, v15, v0}, Lorg/apache/lucene/index/IndexFormatTooOldException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 340
    .end local v8    # "format":I
    :catchall_0
    move-exception v14

    invoke-virtual {v10}, Lorg/apache/lucene/store/ChecksumIndexInput;->close()V

    .line 341
    if-nez v13, :cond_0

    .line 344
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentInfos;->clear()V

    .line 340
    :cond_0
    throw v14

    .line 278
    .restart local v8    # "format":I
    :cond_1
    const/16 v14, -0xb

    if-ge v8, v14, :cond_2

    .line 279
    :try_start_1
    new-instance v14, Lorg/apache/lucene/index/IndexFormatTooNewException;

    const/4 v15, -0x1

    const/16 v16, -0xb

    move/from16 v0, v16

    invoke-direct {v14, v10, v8, v15, v0}, Lorg/apache/lucene/index/IndexFormatTooNewException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v14

    .line 282
    :cond_2
    invoke-virtual {v10}, Lorg/apache/lucene/store/ChecksumIndexInput;->readLong()J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lorg/apache/lucene/index/SegmentInfos;->version:J

    .line 283
    invoke-virtual {v10}, Lorg/apache/lucene/store/ChecksumIndexInput;->readInt()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    .line 285
    invoke-virtual {v10}, Lorg/apache/lucene/store/ChecksumIndexInput;->readInt()I

    move-result v9

    .local v9, "i":I
    :goto_0
    if-lez v9, :cond_8

    .line 286
    new-instance v11, Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, p1

    invoke-direct {v11, v0, v8, v10}, Lorg/apache/lucene/index/SegmentInfo;-><init>(Lorg/apache/lucene/store/Directory;ILorg/apache/lucene/store/IndexInput;)V

    .line 287
    .local v11, "si":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v11}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v14

    if-nez v14, :cond_4

    .line 289
    move-object/from16 v6, p1

    .line 290
    .local v6, "dir":Lorg/apache/lucene/store/Directory;
    invoke-virtual {v11}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreOffset()I

    move-result v14

    const/4 v15, -0x1

    if-eq v14, v15, :cond_5

    .line 291
    invoke-virtual {v11}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreIsCompoundFile()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 292
    new-instance v7, Lorg/apache/lucene/index/CompoundFileReader;

    invoke-virtual {v11}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreSegment()Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v15, "cfx"

    invoke-static {v14, v15}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const/16 v15, 0x400

    invoke-direct {v7, v6, v14, v15}, Lorg/apache/lucene/index/CompoundFileReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v6    # "dir":Lorg/apache/lucene/store/Directory;
    .local v7, "dir":Lorg/apache/lucene/store/Directory;
    move-object v6, v7

    .line 302
    .end local v7    # "dir":Lorg/apache/lucene/store/Directory;
    .restart local v6    # "dir":Lorg/apache/lucene/store/Directory;
    :cond_3
    :goto_1
    :try_start_2
    invoke-virtual {v11}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreOffset()I

    move-result v14

    const/4 v15, -0x1

    if-eq v14, v15, :cond_6

    invoke-virtual {v11}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreSegment()Ljava/lang/String;

    move-result-object v12

    .line 303
    .local v12, "store":Ljava/lang/String;
    :goto_2
    invoke-static {v6, v12}, Lorg/apache/lucene/index/FieldsReader;->detectCodeVersion(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Lorg/apache/lucene/index/SegmentInfo;->setVersion(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 306
    move-object/from16 v0, p1

    if-eq v6, v0, :cond_4

    :try_start_3
    invoke-virtual {v6}, Lorg/apache/lucene/store/Directory;->close()V

    .line 309
    .end local v6    # "dir":Lorg/apache/lucene/store/Directory;
    .end local v12    # "store":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/SegmentInfos;->add(Lorg/apache/lucene/index/SegmentInfo;)V

    .line 285
    add-int/lit8 v9, v9, -0x1

    goto :goto_0

    .line 296
    .restart local v6    # "dir":Lorg/apache/lucene/store/Directory;
    :cond_5
    invoke-virtual {v11}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 297
    new-instance v7, Lorg/apache/lucene/index/CompoundFileReader;

    iget-object v14, v11, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string/jumbo v15, "cfs"

    invoke-static {v14, v15}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const/16 v15, 0x400

    invoke-direct {v7, v6, v14, v15}, Lorg/apache/lucene/index/CompoundFileReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v6    # "dir":Lorg/apache/lucene/store/Directory;
    .restart local v7    # "dir":Lorg/apache/lucene/store/Directory;
    move-object v6, v7

    .end local v7    # "dir":Lorg/apache/lucene/store/Directory;
    .restart local v6    # "dir":Lorg/apache/lucene/store/Directory;
    goto :goto_1

    .line 302
    :cond_6
    :try_start_4
    iget-object v12, v11, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 306
    :catchall_1
    move-exception v14

    move-object/from16 v0, p1

    if-eq v6, v0, :cond_7

    :try_start_5
    invoke-virtual {v6}, Lorg/apache/lucene/store/Directory;->close()V

    :cond_7
    throw v14

    .line 312
    .end local v6    # "dir":Lorg/apache/lucene/store/Directory;
    .end local v11    # "si":Lorg/apache/lucene/index/SegmentInfo;
    :cond_8
    if-ltz v8, :cond_9

    .line 313
    invoke-virtual {v10}, Lorg/apache/lucene/store/ChecksumIndexInput;->getFilePointer()J

    move-result-wide v14

    invoke-virtual {v10}, Lorg/apache/lucene/store/ChecksumIndexInput;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_a

    .line 314
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lorg/apache/lucene/index/SegmentInfos;->version:J

    .line 319
    :cond_9
    :goto_3
    const/4 v14, -0x8

    if-gt v8, v14, :cond_d

    .line 320
    const/16 v14, -0x9

    if-gt v8, v14, :cond_b

    .line 321
    invoke-virtual {v10}, Lorg/apache/lucene/store/ChecksumIndexInput;->readStringStringMap()Ljava/util/Map;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    .line 331
    :goto_4
    const/4 v14, -0x5

    if-gt v8, v14, :cond_e

    .line 332
    invoke-virtual {v10}, Lorg/apache/lucene/store/ChecksumIndexInput;->getChecksum()J

    move-result-wide v2

    .line 333
    .local v2, "checksumNow":J
    invoke-virtual {v10}, Lorg/apache/lucene/store/ChecksumIndexInput;->readLong()J

    move-result-wide v4

    .line 334
    .local v4, "checksumThen":J
    cmp-long v14, v2, v4

    if-eqz v14, :cond_e

    .line 335
    new-instance v14, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "checksum mismatch in segments file (resource: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, ")"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 316
    .end local v2    # "checksumNow":J
    .end local v4    # "checksumThen":J
    :cond_a
    invoke-virtual {v10}, Lorg/apache/lucene/store/ChecksumIndexInput;->readLong()J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lorg/apache/lucene/index/SegmentInfos;->version:J

    goto :goto_3

    .line 322
    :cond_b
    invoke-virtual {v10}, Lorg/apache/lucene/store/ChecksumIndexInput;->readByte()B

    move-result v14

    if-eqz v14, :cond_c

    .line 323
    const-string/jumbo v14, "userData"

    invoke-virtual {v10}, Lorg/apache/lucene/store/ChecksumIndexInput;->readString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    goto :goto_4

    .line 325
    :cond_c
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    goto :goto_4

    .line 328
    :cond_d
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4

    .line 337
    :cond_e
    const/4 v13, 0x1

    .line 340
    invoke-virtual {v10}, Lorg/apache/lucene/store/ChecksumIndexInput;->close()V

    .line 341
    if-nez v13, :cond_f

    .line 344
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentInfos;->clear()V

    .line 347
    :cond_f
    return-void
.end method

.method public remove(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1101
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1102
    sget-boolean v0, Lorg/apache/lucene/index/SegmentInfos;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segmentSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1103
    :cond_0
    return-void
.end method

.method public remove(Lorg/apache/lucene/index/SegmentInfo;)V
    .locals 1
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    .line 1094
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->indexOf(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v0

    .line 1095
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 1096
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/SegmentInfos;->remove(I)V

    .line 1098
    :cond_0
    return-void
.end method

.method replace(Lorg/apache/lucene/index/SegmentInfos;)V
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/index/SegmentInfos;

    .prologue
    .line 968
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->asList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/SegmentInfos;->rollbackSegmentInfos(Ljava/util/List;)V

    .line 969
    iget-wide v0, p1, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    .line 970
    return-void
.end method

.method final rollbackCommit(Lorg/apache/lucene/store/Directory;)V
    .locals 6
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 791
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    if-eqz v1, :cond_0

    .line 793
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/ChecksumIndexOutput;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 802
    :goto_0
    :try_start_1
    const-string/jumbo v1, "segments"

    const-string/jumbo v2, ""

    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    invoke-static {v1, v2, v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 805
    .local v0, "segmentFileName":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 810
    .end local v0    # "segmentFileName":Ljava/lang/String;
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    .line 812
    :cond_0
    return-void

    .line 806
    :catch_0
    move-exception v1

    goto :goto_1

    .line 794
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method rollbackSegmentInfos(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1042
    .local p1, "infos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->clear()V

    .line 1043
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->addAll(Ljava/lang/Iterable;)V

    .line 1044
    return-void
.end method

.method public setFormat(I)V
    .locals 0
    .param p1, "format"    # I

    .prologue
    .line 136
    iput p1, p0, Lorg/apache/lucene/index/SegmentInfos;->format:I

    .line 137
    return-void
.end method

.method setUserData(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 956
    .local p1, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 957
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    .line 961
    :goto_0
    return-void

    .line 959
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 1070
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public toString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;
    .locals 6
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 938
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 939
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 940
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v1

    .line 941
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 942
    if-lez v2, :cond_0

    .line 943
    const/16 v4, 0x20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 945
    :cond_0
    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v3

    .line 946
    .local v3, "info":Lorg/apache/lucene/index/SegmentInfo;
    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lorg/apache/lucene/index/SegmentInfo;->toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 941
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 948
    .end local v3    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public totalDocCount()I
    .locals 4

    .prologue
    .line 975
    const/4 v0, 0x0

    .line 976
    .local v0, "count":I
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentInfo;

    .line 977
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfo;
    iget v3, v2, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    add-int/2addr v0, v3

    goto :goto_0

    .line 979
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_0
    return v0
.end method

.method updateGeneration(Lorg/apache/lucene/index/SegmentInfos;)V
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/index/SegmentInfos;

    .prologue
    .line 786
    iget-wide v0, p1, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    .line 787
    iget-wide v0, p1, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    .line 788
    return-void
.end method
