.class public Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
.super Ljava/lang/Thread;
.source "ConcurrentMergeScheduler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/ConcurrentMergeScheduler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "MergeThread"
.end annotation


# instance fields
.field private volatile done:Z

.field runningMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

.field startMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

.field tWriter:Lorg/apache/lucene/index/IndexWriter;

.field final synthetic this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/ConcurrentMergeScheduler;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 0
    .param p2, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p3, "startMerge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 407
    iput-object p1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 408
    iput-object p2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->tWriter:Lorg/apache/lucene/index/IndexWriter;

    .line 409
    iput-object p3, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->startMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 410
    return-void
.end method


# virtual methods
.method public declared-synchronized getCurrentMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .locals 1

    .prologue
    .line 421
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->done:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 422
    const/4 v0, 0x0

    .line 426
    :goto_0
    monitor-exit p0

    return-object v0

    .line 423
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->runningMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

    if-eqz v0, :cond_1

    .line 424
    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->runningMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

    goto :goto_0

    .line 426
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->startMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 421
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getRunningMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .locals 1

    .prologue
    .line 417
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->runningMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 447
    iget-object v1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->startMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 451
    .local v1, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 452
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    const-string/jumbo v3, "  merge thread: start"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 455
    :cond_0
    :goto_0
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->setRunningMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 456
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->doMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 460
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->tWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexWriter;->getNextMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-result-object v1

    .line 461
    if-eqz v1, :cond_2

    .line 462
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->tWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/index/IndexWriter;->mergeInit(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 463
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->updateMergeThreads()V

    .line 464
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 465
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "  merge thread: do another merge "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    iget-object v4, v4, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 474
    :catch_0
    move-exception v0

    .line 477
    .local v0, "exc":Ljava/lang/Throwable;
    :try_start_1
    instance-of v2, v0, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;

    if-nez v2, :cond_1

    .line 478
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    # getter for: Lorg/apache/lucene/index/ConcurrentMergeScheduler;->suppressExceptions:Z
    invoke-static {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->access$000(Lorg/apache/lucene/index/ConcurrentMergeScheduler;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 481
    const/4 v2, 0x1

    sput-boolean v2, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->anyExceptions:Z

    .line 482
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->handleMergeException(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 486
    :cond_1
    iput-boolean v5, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->done:Z

    .line 487
    iget-object v3, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    monitor-enter v3

    .line 488
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->updateMergeThreads()V

    .line 489
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 490
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 492
    .end local v0    # "exc":Ljava/lang/Throwable;
    :goto_1
    return-void

    .line 471
    :cond_2
    :try_start_3
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 472
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    const-string/jumbo v3, "  merge thread: done"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 486
    :cond_3
    iput-boolean v5, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->done:Z

    .line 487
    iget-object v3, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    monitor-enter v3

    .line 488
    :try_start_4
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->updateMergeThreads()V

    .line 489
    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 490
    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    .line 486
    :catchall_1
    move-exception v2

    iput-boolean v5, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->done:Z

    .line 487
    iget-object v3, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    monitor-enter v3

    .line 488
    :try_start_5
    iget-object v4, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v4}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->updateMergeThreads()V

    .line 489
    iget-object v4, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->this$0:Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 490
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 486
    throw v2

    .line 490
    :catchall_2
    move-exception v2

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v2

    .restart local v0    # "exc":Ljava/lang/Throwable;
    :catchall_3
    move-exception v2

    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v2
.end method

.method public declared-synchronized setRunningMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 1
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .prologue
    .line 413
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->runningMerge:Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    monitor-exit p0

    return-void

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setThreadPriority(I)V
    .locals 1
    .param p1, "pri"    # I

    .prologue
    .line 432
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->setPriority(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 440
    :goto_0
    return-void

    .line 433
    :catch_0
    move-exception v0

    goto :goto_0

    .line 436
    :catch_1
    move-exception v0

    goto :goto_0
.end method
