.class final Lorg/apache/lucene/index/FreqProxFieldMergeState;
.super Ljava/lang/Object;
.source "FreqProxFieldMergeState.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final charPool:Lorg/apache/lucene/index/CharBlockPool;

.field currentTermID:I

.field docID:I

.field final field:Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

.field final freq:Lorg/apache/lucene/index/ByteSliceReader;

.field final numPostings:I

.field private postingUpto:I

.field final postings:Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

.field final prox:Lorg/apache/lucene/index/ByteSliceReader;

.field termFreq:I

.field final termIDs:[I

.field text:[C

.field textOffset:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/index/FreqProxFieldMergeState;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/FreqProxTermsWriterPerField;)V
    .locals 1
    .param p1, "field"    # Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->postingUpto:I

    .line 45
    new-instance v0, Lorg/apache/lucene/index/ByteSliceReader;

    invoke-direct {v0}, Lorg/apache/lucene/index/ByteSliceReader;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->freq:Lorg/apache/lucene/index/ByteSliceReader;

    .line 46
    new-instance v0, Lorg/apache/lucene/index/ByteSliceReader;

    invoke-direct {v0}, Lorg/apache/lucene/index/ByteSliceReader;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->prox:Lorg/apache/lucene/index/ByteSliceReader;

    .line 52
    iput-object p1, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->field:Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    .line 53
    iget-object v0, p1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->perThread:Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;->termsHashPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerThread;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    .line 54
    iget-object v0, p1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    iput v0, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->numPostings:I

    .line 55
    iget-object v0, p1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashPerField;->sortPostings()[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->termIDs:[I

    .line 56
    iget-object v0, p1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    check-cast v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->postings:Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    .line 57
    return-void
.end method


# virtual methods
.method public nextDoc()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x1

    .line 91
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->freq:Lorg/apache/lucene/index/ByteSliceReader;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ByteSliceReader;->eof()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 92
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->postings:Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    iget-object v2, v2, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    iget v3, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->currentTermID:I

    aget v2, v2, v3

    if-eq v2, v4, :cond_2

    .line 94
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->postings:Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    iget-object v2, v2, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    iget v3, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->currentTermID:I

    aget v2, v2, v3

    iput v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->docID:I

    .line 95
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->field:Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    iget-object v2, v2, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v2, v3, :cond_0

    .line 96
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->postings:Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    iget-object v2, v2, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->docFreqs:[I

    iget v3, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->currentTermID:I

    aget v2, v2, v3

    iput v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->termFreq:I

    .line 97
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->postings:Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    iget-object v2, v2, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    iget v3, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->currentTermID:I

    aput v4, v2, v3

    .line 117
    :cond_1
    :goto_0
    return v1

    .line 101
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 104
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->freq:Lorg/apache/lucene/index/ByteSliceReader;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ByteSliceReader;->readVInt()I

    move-result v0

    .line 105
    .local v0, "code":I
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->field:Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    iget-object v2, v2, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v2, v3, :cond_4

    .line 106
    iget v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->docID:I

    add-int/2addr v2, v0

    iput v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->docID:I

    .line 115
    :goto_1
    sget-boolean v2, Lorg/apache/lucene/index/FreqProxFieldMergeState;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->docID:I

    iget-object v3, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->postings:Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    iget-object v3, v3, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    iget v4, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->currentTermID:I

    aget v3, v3, v4

    if-ne v2, v3, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 108
    :cond_4
    iget v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->docID:I

    ushr-int/lit8 v3, v0, 0x1

    add-int/2addr v2, v3

    iput v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->docID:I

    .line 109
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_5

    .line 110
    iput v1, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->termFreq:I

    goto :goto_1

    .line 112
    :cond_5
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->freq:Lorg/apache/lucene/index/ByteSliceReader;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ByteSliceReader;->readVInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->termFreq:I

    goto :goto_1
.end method

.method nextTerm()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    iget v4, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->postingUpto:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->postingUpto:I

    .line 61
    iget v4, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->postingUpto:I

    iget v5, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->numPostings:I

    if-ne v4, v5, :cond_0

    .line 79
    :goto_0
    return v2

    .line 64
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->termIDs:[I

    iget v5, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->postingUpto:I

    aget v4, v4, v5

    iput v4, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->currentTermID:I

    .line 65
    iput v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->docID:I

    .line 67
    iget-object v4, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->postings:Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    iget-object v4, v4, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->textStarts:[I

    iget v5, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->currentTermID:I

    aget v1, v4, v5

    .line 68
    .local v1, "textStart":I
    iget-object v4, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    iget-object v4, v4, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    shr-int/lit8 v5, v1, 0xe

    aget-object v4, v4, v5

    iput-object v4, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->text:[C

    .line 69
    and-int/lit16 v4, v1, 0x3fff

    iput v4, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->textOffset:I

    .line 71
    iget-object v4, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->field:Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    iget-object v4, v4, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v5, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->freq:Lorg/apache/lucene/index/ByteSliceReader;

    iget v6, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->currentTermID:I

    invoke-virtual {v4, v5, v6, v2}, Lorg/apache/lucene/index/TermsHashPerField;->initReader(Lorg/apache/lucene/index/ByteSliceReader;II)V

    .line 72
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->field:Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    iget-object v2, v2, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v2, v4, :cond_1

    .line 73
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->field:Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    iget-object v2, v2, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v4, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->prox:Lorg/apache/lucene/index/ByteSliceReader;

    iget v5, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->currentTermID:I

    invoke-virtual {v2, v4, v5, v3}, Lorg/apache/lucene/index/TermsHashPerField;->initReader(Lorg/apache/lucene/index/ByteSliceReader;II)V

    .line 76
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/FreqProxFieldMergeState;->nextDoc()Z

    move-result v0

    .line 77
    .local v0, "result":Z
    sget-boolean v2, Lorg/apache/lucene/index/FreqProxFieldMergeState;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    if-nez v0, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_2
    move v2, v3

    .line 79
    goto :goto_0
.end method

.method public termText()Ljava/lang/String;
    .locals 5

    .prologue
    .line 83
    iget v0, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->textOffset:I

    .line 84
    .local v0, "upto":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->text:[C

    aget-char v1, v1, v0

    const v2, 0xffff

    if-eq v1, v2, :cond_0

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_0
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->text:[C

    iget v3, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->textOffset:I

    iget v4, p0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->textOffset:I

    sub-int v4, v0, v4

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([CII)V

    return-object v1
.end method
