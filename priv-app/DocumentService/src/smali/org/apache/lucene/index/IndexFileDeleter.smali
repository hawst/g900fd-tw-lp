.class final Lorg/apache/lucene/index/IndexFileDeleter;
.super Ljava/lang/Object;
.source "IndexFileDeleter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;,
        Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static VERBOSE_REF_COUNTS:Z


# instance fields
.field private commits:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;",
            ">;"
        }
    .end annotation
.end field

.field private commitsToDelete:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;",
            ">;"
        }
    .end annotation
.end field

.field private deletable:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private directory:Lorg/apache/lucene/store/Directory;

.field private infoStream:Ljava/io/PrintStream;

.field private lastFiles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private lastSegmentInfos:Lorg/apache/lucene/index/SegmentInfos;

.field private policy:Lorg/apache/lucene/index/IndexDeletionPolicy;

.field private refCounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/IndexFileDeleter$RefCount;",
            ">;"
        }
    .end annotation
.end field

.field final startingCommitDeleted:Z

.field private final writer:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    const-class v0, Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    .line 107
    sput-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->VERBOSE_REF_COUNTS:Z

    return-void

    :cond_0
    move v0, v1

    .line 72
    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/SegmentInfos;Ljava/io/PrintStream;Lorg/apache/lucene/index/IndexWriter;)V
    .locals 24
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "policy"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .param p3, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p4, "infoStream"    # Ljava/io/PrintStream;
    .param p5, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    .line 89
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    .line 93
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->lastFiles:Ljava/util/List;

    .line 96
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->commitsToDelete:Ljava/util/List;

    .line 139
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Ljava/io/PrintStream;

    .line 140
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 142
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v7

    .line 144
    .local v7, "currentSegmentsFile":Ljava/lang/String;
    if-eqz p4, :cond_0

    .line 145
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "init: current segments file is \""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "\"; deletionPolicy="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 148
    :cond_0
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->policy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    .line 149
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    .line 153
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v8

    .line 154
    .local v8, "currentGen":J
    invoke-static {}, Lorg/apache/lucene/index/IndexFileNameFilter;->getFilter()Lorg/apache/lucene/index/IndexFileNameFilter;

    move-result-object v14

    .line 156
    .local v14, "filter":Lorg/apache/lucene/index/IndexFileNameFilter;
    const/4 v6, 0x0

    .line 157
    .local v6, "currentCommitPoint":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    const/4 v13, 0x0

    .line 159
    .local v13, "files":[Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/lucene/store/NoSuchDirectoryException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 165
    :goto_0
    move-object v4, v13

    .local v4, "arr$":[Ljava/lang/String;
    array-length v0, v4

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    :goto_1
    move/from16 v0, v16

    if-ge v15, v0, :cond_7

    aget-object v12, v4, v15

    .line 167
    .local v12, "fileName":Ljava/lang/String;
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v14, v0, v12}, Lorg/apache/lucene/index/IndexFileNameFilter;->accept(Ljava/io/File;Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_4

    const-string/jumbo v19, "segments.gen"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_4

    .line 170
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lorg/apache/lucene/index/IndexFileDeleter;->getRefCount(Ljava/lang/String;)Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    .line 172
    const-string/jumbo v19, "segments"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 177
    if-eqz p4, :cond_1

    .line 178
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "init: load commit \""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 180
    :cond_1
    new-instance v18, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct/range {v18 .. v18}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 182
    .local v18, "sis":Lorg/apache/lucene/index/SegmentInfos;
    :try_start_1
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v12}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 205
    :goto_2
    if-eqz v18, :cond_4

    .line 206
    new-instance v5, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->commitsToDelete:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v5, v0, v1, v2}, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;-><init>(Ljava/util/Collection;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;)V

    .line 207
    .local v5, "commitPoint":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v20

    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v22

    cmp-long v19, v20, v22

    if-nez v19, :cond_2

    .line 208
    move-object v6, v5

    .line 210
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->incRef(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 213
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->lastSegmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v19, v0

    if-eqz v19, :cond_3

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->lastSegmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v22

    cmp-long v19, v20, v22

    if-lez v19, :cond_4

    .line 214
    :cond_3
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->lastSegmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    .line 165
    .end local v5    # "commitPoint":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    .end local v18    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_4
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_1

    .line 160
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v15    # "i$":I
    .end local v16    # "len$":I
    :catch_0
    move-exception v10

    .line 162
    .local v10, "e":Lorg/apache/lucene/store/NoSuchDirectoryException;
    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v13, v0, [Ljava/lang/String;

    goto/16 :goto_0

    .line 183
    .end local v10    # "e":Lorg/apache/lucene/store/NoSuchDirectoryException;
    .restart local v4    # "arr$":[Ljava/lang/String;
    .restart local v12    # "fileName":Ljava/lang/String;
    .restart local v15    # "i$":I
    .restart local v16    # "len$":I
    .restart local v18    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :catch_1
    move-exception v10

    .line 191
    .local v10, "e":Ljava/io/FileNotFoundException;
    if-eqz p4, :cond_5

    .line 192
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "init: hit FileNotFoundException when loading commit \""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "\"; skipping this commit point"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 194
    :cond_5
    const/16 v18, 0x0

    .line 204
    goto/16 :goto_2

    .line 195
    .end local v10    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v10

    .line 196
    .local v10, "e":Ljava/io/IOException;
    invoke-static {v12}, Lorg/apache/lucene/index/SegmentInfos;->generationFromSegmentsFileName(Ljava/lang/String;)J

    move-result-wide v20

    cmp-long v19, v20, v8

    if-gtz v19, :cond_6

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/apache/lucene/store/Directory;->fileLength(Ljava/lang/String;)J

    move-result-wide v20

    const-wide/16 v22, 0x0

    cmp-long v19, v20, v22

    if-lez v19, :cond_6

    .line 197
    throw v10

    .line 202
    :cond_6
    const/16 v18, 0x0

    goto/16 :goto_2

    .line 221
    .end local v10    # "e":Ljava/io/IOException;
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v18    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_7
    if-nez v6, :cond_9

    if-eqz v7, :cond_9

    .line 229
    new-instance v18, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct/range {v18 .. v18}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 231
    .restart local v18    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :try_start_2
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v7}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 235
    if-eqz p4, :cond_8

    .line 236
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "forced open of current segments file "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 238
    :cond_8
    new-instance v6, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;

    .end local v6    # "currentCommitPoint":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->commitsToDelete:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v6, v0, v1, v2}, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;-><init>(Ljava/util/Collection;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;)V

    .line 239
    .restart local v6    # "currentCommitPoint":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->incRef(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 244
    .end local v18    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lorg/apache/lucene/util/CollectionUtil;->mergeSort(Ljava/util/List;)V

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_a
    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_c

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Map$Entry;

    .line 250
    .local v11, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/IndexFileDeleter$RefCount;>;"
    invoke-interface {v11}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    .line 251
    .local v17, "rc":Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 252
    .restart local v12    # "fileName":Ljava/lang/String;
    move-object/from16 v0, v17

    iget v0, v0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    move/from16 v19, v0

    if-nez v19, :cond_a

    .line 253
    if-eqz p4, :cond_b

    .line 254
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "init: removing unreferenced file \""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, "\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 256
    :cond_b
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    goto :goto_3

    .line 232
    .end local v11    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/IndexFileDeleter$RefCount;>;"
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v17    # "rc":Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    .local v15, "i$":I
    .restart local v18    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :catch_3
    move-exception v10

    .line 233
    .restart local v10    # "e":Ljava/io/IOException;
    new-instance v19, Lorg/apache/lucene/index/CorruptIndexException;

    const-string/jumbo v20, "failed to locate current segments_N file"

    invoke-direct/range {v19 .. v20}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 262
    .end local v10    # "e":Ljava/io/IOException;
    .end local v18    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    .local v15, "i$":Ljava/util/Iterator;
    :cond_c
    if-eqz v7, :cond_d

    .line 263
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Lorg/apache/lucene/index/IndexDeletionPolicy;->onInit(Ljava/util/List;)V

    .line 268
    :cond_d
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 270
    if-nez v6, :cond_e

    const/16 v19, 0x0

    :goto_4
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/lucene/index/IndexFileDeleter;->startingCommitDeleted:Z

    .line 272
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteCommits()V

    .line 273
    return-void

    .line 270
    :cond_e
    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->isDeleted()Z

    move-result v19

    goto :goto_4
.end method

.method private deleteCommits()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 285
    iget-object v7, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commitsToDelete:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    .line 287
    .local v5, "size":I
    if-lez v5, :cond_6

    .line 291
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v5, :cond_2

    .line 292
    iget-object v7, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commitsToDelete:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;

    .line 293
    .local v0, "commit":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    iget-object v7, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Ljava/io/PrintStream;

    if-eqz v7, :cond_0

    .line 294
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "deleteCommits: now decRef commit \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 296
    :cond_0
    iget-object v7, v0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->files:Ljava/util/Collection;

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 297
    .local v1, "file":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/lang/String;)V

    goto :goto_1

    .line 291
    .end local v1    # "file":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 300
    .end local v0    # "commit":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v7, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commitsToDelete:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 303
    iget-object v7, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    .line 304
    const/4 v4, 0x0

    .line 305
    .local v4, "readFrom":I
    const/4 v6, 0x0

    .line 306
    .local v6, "writeTo":I
    :goto_2
    if-ge v4, v5, :cond_5

    .line 307
    iget-object v7, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;

    .line 308
    .restart local v0    # "commit":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    iget-boolean v7, v0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->deleted:Z

    if-nez v7, :cond_4

    .line 309
    if-eq v6, v4, :cond_3

    .line 310
    iget-object v7, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    iget-object v8, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v7, v6, v8}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 312
    :cond_3
    add-int/lit8 v6, v6, 0x1

    .line 314
    :cond_4
    add-int/lit8 v4, v4, 0x1

    .line 315
    goto :goto_2

    .line 317
    .end local v0    # "commit":Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
    :cond_5
    :goto_3
    if-le v5, v6, :cond_6

    .line 318
    iget-object v7, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    add-int/lit8 v8, v5, -0x1

    invoke-interface {v7, v8}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 319
    add-int/lit8 v5, v5, -0x1

    goto :goto_3

    .line 322
    .end local v2    # "i":I
    .end local v4    # "readFrom":I
    .end local v6    # "writeTo":I
    :cond_6
    return-void
.end method

.method private getRefCount(Ljava/lang/String;)Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 539
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 541
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 542
    new-instance v0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    invoke-direct {v0, p1}, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;-><init>(Ljava/lang/String;)V

    .line 543
    .local v0, "rc":Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 547
    :goto_0
    return-object v0

    .line 545
    .end local v0    # "rc":Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    .restart local v0    # "rc":Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    goto :goto_0
.end method

.method private locked()Z
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private message(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "IFD ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 121
    return-void
.end method


# virtual methods
.method public checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V
    .locals 6
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "isCommit"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 442
    sget-boolean v2, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 444
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_1

    .line 445
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "now checkpoint \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\" ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " segments "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; isCommit = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 450
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->deletePendingFiles()V

    .line 453
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/IndexFileDeleter;->incRef(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 455
    if-eqz p2, :cond_2

    .line 457
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    new-instance v3, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;

    iget-object v4, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commitsToDelete:Ljava/util/List;

    iget-object v5, p0, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-direct {v3, v4, v5, p1}, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;-><init>(Ljava/util/Collection;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 460
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->policy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    invoke-interface {v2, v3}, Lorg/apache/lucene/index/IndexDeletionPolicy;->onCommit(Ljava/util/List;)V

    .line 463
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteCommits()V

    .line 474
    :goto_0
    return-void

    .line 466
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->lastFiles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    .line 467
    .local v1, "lastFile":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    goto :goto_1

    .line 469
    .end local v1    # "lastFile":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->lastFiles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 472
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->lastFiles:Ljava/util/List;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 373
    sget-boolean v2, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 374
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->lastFiles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 375
    .local v1, "size":I
    if-lez v1, :cond_2

    .line 376
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 377
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->lastFiles:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 376
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 379
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->lastFiles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 382
    .end local v0    # "i":I
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->deletePendingFiles()V

    .line 383
    return-void
.end method

.method decRef(Ljava/lang/String;)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 509
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 510
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexFileDeleter;->getRefCount(Ljava/lang/String;)Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    move-result-object v0

    .line 511
    .local v0, "rc":Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_1

    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->VERBOSE_REF_COUNTS:Z

    if-eqz v1, :cond_1

    .line 512
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "  DecRef \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\": pre-decr count is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 514
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->DecRef()I

    move-result v1

    if-nez v1, :cond_2

    .line 517
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 518
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 520
    :cond_2
    return-void
.end method

.method decRef(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 502
    .local p1, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    sget-boolean v2, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 503
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 504
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/lang/String;)V

    goto :goto_0

    .line 506
    .end local v0    # "file":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method decRef(Lorg/apache/lucene/index/SegmentInfos;)V
    .locals 4
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 523
    sget-boolean v2, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 524
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 525
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/lang/String;)V

    goto :goto_0

    .line 527
    .end local v0    # "file":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method deleteFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 573
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 575
    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_1

    .line 576
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "delete \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 578
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 598
    :cond_2
    :goto_0
    return-void

    .line 579
    :catch_0
    move-exception v0

    .line 580
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 589
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_3

    .line 590
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unable to remove file \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "; Will re-try later."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 592
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->deletable:Ljava/util/List;

    if-nez v1, :cond_4

    .line 593
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->deletable:Ljava/util/List;

    .line 595
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->deletable:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method deleteFiles(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 551
    .local p1, "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-boolean v2, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 552
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 553
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    goto :goto_0

    .line 555
    .end local v0    # "file":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method deleteNewFiles(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 560
    .local p1, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    sget-boolean v2, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 561
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 562
    .local v0, "fileName":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 563
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_2

    .line 564
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "delete new file \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 566
    :cond_2
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    goto :goto_0

    .line 569
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method public deletePendingFiles()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 407
    sget-boolean v3, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 408
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/IndexFileDeleter;->deletable:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 409
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->deletable:Ljava/util/List;

    .line 410
    .local v1, "oldDeletable":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/index/IndexFileDeleter;->deletable:Ljava/util/List;

    .line 411
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 412
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 413
    iget-object v3, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_1

    .line 414
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "delete pending file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 416
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 412
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 419
    .end local v0    # "i":I
    .end local v1    # "oldDeletable":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "size":I
    :cond_2
    return-void
.end method

.method public exists(Ljava/lang/String;)Z
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 530
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 531
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 534
    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexFileDeleter;->getRefCount(Ljava/lang/String;)Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    move-result-object v1

    iget v1, v1, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    if-lez v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getLastSegmentInfos()Lorg/apache/lucene/index/SegmentInfos;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter;->lastSegmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    return-object v0
.end method

.method incRef(Ljava/lang/String;)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 493
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 494
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexFileDeleter;->getRefCount(Ljava/lang/String;)Lorg/apache/lucene/index/IndexFileDeleter$RefCount;

    move-result-object v0

    .line 495
    .local v0, "rc":Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_1

    sget-boolean v1, Lorg/apache/lucene/index/IndexFileDeleter;->VERBOSE_REF_COUNTS:Z

    if-eqz v1, :cond_1

    .line 496
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "  IncRef \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\": pre-incr count is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 498
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->IncRef()I

    .line 499
    return-void
.end method

.method incRef(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 486
    .local p1, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    sget-boolean v2, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 487
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 488
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->incRef(Ljava/lang/String;)V

    goto :goto_0

    .line 490
    .end local v0    # "file":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method incRef(Lorg/apache/lucene/index/SegmentInfos;Z)V
    .locals 3
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "isCommit"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 477
    sget-boolean v2, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 480
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p1, v2, p2}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 481
    .local v0, "fileName":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->incRef(Ljava/lang/String;)V

    goto :goto_0

    .line 483
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public refresh()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 366
    sget-boolean v0, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 367
    :cond_0
    iput-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->deletable:Ljava/util/List;

    .line 368
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V

    .line 369
    return-void
.end method

.method public refresh(Ljava/lang/String;)V
    .locals 8
    .param p1, "segmentName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 333
    sget-boolean v6, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 335
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v6}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v1

    .line 336
    .local v1, "files":[Ljava/lang/String;
    invoke-static {}, Lorg/apache/lucene/index/IndexFileNameFilter;->getFilter()Lorg/apache/lucene/index/IndexFileNameFilter;

    move-result-object v2

    .line 339
    .local v2, "filter":Lorg/apache/lucene/index/IndexFileNameFilter;
    if-eqz p1, :cond_4

    .line 340
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 341
    .local v4, "segmentPrefix1":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 347
    .local v5, "segmentPrefix2":Ljava/lang/String;
    :goto_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    array-length v6, v1

    if-ge v3, v6, :cond_5

    .line 348
    aget-object v0, v1, v3

    .line 349
    .local v0, "fileName":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-virtual {v2, v6, v0}, Lorg/apache/lucene/index/IndexFileNameFilter;->accept(Ljava/io/File;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    if-eqz p1, :cond_1

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->refCounts:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string/jumbo v6, "segments.gen"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 354
    iget-object v6, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Ljava/io/PrintStream;

    if-eqz v6, :cond_2

    .line 355
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "refresh [prefix="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "]: removing newly created unreferenced file \""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 357
    :cond_2
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 347
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 343
    .end local v0    # "fileName":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "segmentPrefix1":Ljava/lang/String;
    .end local v5    # "segmentPrefix2":Ljava/lang/String;
    :cond_4
    const/4 v4, 0x0

    .line 344
    .restart local v4    # "segmentPrefix1":Ljava/lang/String;
    const/4 v5, 0x0

    .restart local v5    # "segmentPrefix2":Ljava/lang/String;
    goto :goto_0

    .line 360
    .restart local v3    # "i":I
    :cond_5
    return-void
.end method

.method revisitPolicy()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 395
    sget-boolean v0, Lorg/apache/lucene/index/IndexFileDeleter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->locked()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 396
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_1

    .line 397
    const-string/jumbo v0, "now revisitPolicy"

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 400
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 401
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter;->policy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->commits:Ljava/util/List;

    invoke-interface {v0, v1}, Lorg/apache/lucene/index/IndexDeletionPolicy;->onCommit(Ljava/util/List;)V

    .line 402
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteCommits()V

    .line 404
    :cond_2
    return-void
.end method

.method setInfoStream(Ljava/io/PrintStream;)V
    .locals 2
    .param p1, "infoStream"    # Ljava/io/PrintStream;

    .prologue
    .line 113
    iput-object p1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->infoStream:Ljava/io/PrintStream;

    .line 114
    if-eqz p1, :cond_0

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setInfoStream deletionPolicy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter;->policy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->message(Ljava/lang/String;)V

    .line 117
    :cond_0
    return-void
.end method
