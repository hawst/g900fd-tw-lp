.class public abstract Lorg/apache/lucene/index/AbstractAllTermDocs;
.super Ljava/lang/Object;
.source "AbstractAllTermDocs.java"

# interfaces
.implements Lorg/apache/lucene/index/TermDocs;


# instance fields
.field protected doc:I

.field protected maxDoc:I


# direct methods
.method protected constructor <init>(I)V
    .locals 1
    .param p1, "maxDoc"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->doc:I

    .line 35
    iput p1, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->maxDoc:I

    .line 36
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    return-void
.end method

.method public doc()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method public abstract isDeleted(I)Z
.end method

.method public next()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    iget v0, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->doc:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/AbstractAllTermDocs;->skipTo(I)Z

    move-result v0

    return v0
.end method

.method public read([I[I)I
    .locals 4
    .param p1, "docs"    # [I
    .param p2, "freqs"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    array-length v1, p1

    .line 64
    .local v1, "length":I
    const/4 v0, 0x0

    .line 65
    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    iget v2, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->doc:I

    iget v3, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->maxDoc:I

    if-ge v2, v3, :cond_1

    .line 66
    iget v2, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->doc:I

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/AbstractAllTermDocs;->isDeleted(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 67
    iget v2, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->doc:I

    aput v2, p1, v0

    .line 68
    const/4 v2, 0x1

    aput v2, p2, v0

    .line 69
    add-int/lit8 v0, v0, 0x1

    .line 71
    :cond_0
    iget v2, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->doc:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->doc:I

    goto :goto_0

    .line 73
    :cond_1
    return v0
.end method

.method public seek(Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    if-nez p1, :cond_0

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->doc:I

    .line 44
    return-void

    .line 42
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public seek(Lorg/apache/lucene/index/TermEnum;)V
    .locals 1
    .param p1, "termEnum"    # Lorg/apache/lucene/index/TermEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public skipTo(I)Z
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    iput p1, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->doc:I

    .line 78
    :goto_0
    iget v0, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->doc:I

    iget v1, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->maxDoc:I

    if-ge v0, v1, :cond_1

    .line 79
    iget v0, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->doc:I

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/AbstractAllTermDocs;->isDeleted(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    const/4 v0, 0x1

    .line 84
    :goto_1
    return v0

    .line 82
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->doc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/AbstractAllTermDocs;->doc:I

    goto :goto_0

    .line 84
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
