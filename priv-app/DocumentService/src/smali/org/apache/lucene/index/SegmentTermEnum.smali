.class final Lorg/apache/lucene/index/SegmentTermEnum;
.super Lorg/apache/lucene/index/TermEnum;
.source "SegmentTermEnum.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private first:Z

.field private format:I

.field private formatM1SkipInterval:I

.field indexInterval:I

.field indexPointer:J

.field private input:Lorg/apache/lucene/store/IndexInput;

.field private isIndex:Z

.field maxSkipLevels:I

.field position:J

.field private prevBuffer:Lorg/apache/lucene/index/TermBuffer;

.field private scanBuffer:Lorg/apache/lucene/index/TermBuffer;

.field size:J

.field skipInterval:I

.field private termBuffer:Lorg/apache/lucene/index/TermBuffer;

.field private termInfo:Lorg/apache/lucene/index/TermInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lorg/apache/lucene/index/SegmentTermEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SegmentTermEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfos;Z)V
    .locals 9
    .param p1, "i"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p3, "isi"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v8, 0x7fffffff

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, -0x4

    .line 45
    invoke-direct {p0}, Lorg/apache/lucene/index/TermEnum;-><init>()V

    .line 27
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentTermEnum;->position:J

    .line 28
    iput-boolean v7, p0, Lorg/apache/lucene/index/SegmentTermEnum;->first:Z

    .line 30
    new-instance v1, Lorg/apache/lucene/index/TermBuffer;

    invoke-direct {v1}, Lorg/apache/lucene/index/TermBuffer;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/index/TermBuffer;

    .line 31
    new-instance v1, Lorg/apache/lucene/index/TermBuffer;

    invoke-direct {v1}, Lorg/apache/lucene/index/TermBuffer;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->prevBuffer:Lorg/apache/lucene/index/TermBuffer;

    .line 32
    new-instance v1, Lorg/apache/lucene/index/TermBuffer;

    invoke-direct {v1}, Lorg/apache/lucene/index/TermBuffer;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->scanBuffer:Lorg/apache/lucene/index/TermBuffer;

    .line 34
    new-instance v1, Lorg/apache/lucene/index/TermInfo;

    invoke-direct {v1}, Lorg/apache/lucene/index/TermInfo;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    .line 37
    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentTermEnum;->isIndex:Z

    .line 38
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentTermEnum;->indexPointer:J

    .line 46
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    .line 47
    iput-object p2, p0, Lorg/apache/lucene/index/SegmentTermEnum;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 48
    iput-boolean p3, p0, Lorg/apache/lucene/index/SegmentTermEnum;->isIndex:Z

    .line 49
    iput v7, p0, Lorg/apache/lucene/index/SegmentTermEnum;->maxSkipLevels:I

    .line 51
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    .line 52
    .local v0, "firstInt":I
    if-ltz v0, :cond_2

    .line 54
    iput v6, p0, Lorg/apache/lucene/index/SegmentTermEnum;->format:I

    .line 55
    int-to-long v2, v0

    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentTermEnum;->size:J

    .line 58
    const/16 v1, 0x80

    iput v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->indexInterval:I

    .line 59
    iput v8, p0, Lorg/apache/lucene/index/SegmentTermEnum;->skipInterval:I

    .line 89
    :cond_0
    iget v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->format:I

    if-le v1, v4, :cond_1

    .line 90
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/index/TermBuffer;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermBuffer;->setPreUTF8Strings()V

    .line 91
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->scanBuffer:Lorg/apache/lucene/index/TermBuffer;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermBuffer;->setPreUTF8Strings()V

    .line 92
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->prevBuffer:Lorg/apache/lucene/index/TermBuffer;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermBuffer;->setPreUTF8Strings()V

    .line 94
    :cond_1
    return-void

    .line 62
    :cond_2
    iput v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->format:I

    .line 65
    iget v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->format:I

    if-ge v1, v4, :cond_3

    .line 66
    new-instance v1, Lorg/apache/lucene/index/IndexFormatTooNewException;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    iget v3, p0, Lorg/apache/lucene/index/SegmentTermEnum;->format:I

    invoke-direct {v1, v2, v3, v5, v4}, Lorg/apache/lucene/index/IndexFormatTooNewException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v1

    .line 68
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentTermEnum;->size:J

    .line 70
    iget v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->format:I

    if-ne v1, v5, :cond_6

    .line 71
    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->isIndex:Z

    if-nez v1, :cond_4

    .line 72
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->indexInterval:I

    .line 73
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->formatM1SkipInterval:I

    .line 77
    :cond_4
    iput v8, p0, Lorg/apache/lucene/index/SegmentTermEnum;->skipInterval:I

    .line 86
    :cond_5
    :goto_0
    sget-boolean v1, Lorg/apache/lucene/index/SegmentTermEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_7

    iget v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->indexInterval:I

    if-gtz v1, :cond_7

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "indexInterval="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/index/SegmentTermEnum;->indexInterval:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " is negative; must be > 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 79
    :cond_6
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->indexInterval:I

    .line 80
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->skipInterval:I

    .line 81
    iget v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->format:I

    const/4 v2, -0x3

    if-gt v1, v2, :cond_5

    .line 83
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->maxSkipLevels:I

    goto :goto_0

    .line 87
    :cond_7
    sget-boolean v1, Lorg/apache/lucene/index/SegmentTermEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->skipInterval:I

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "skipInterval="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/index/SegmentTermEnum;->skipInterval:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " is negative; must be > 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method


# virtual methods
.method protected clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 98
    const/4 v1, 0x0

    .line 100
    .local v1, "clone":Lorg/apache/lucene/index/SegmentTermEnum;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lorg/apache/lucene/index/SegmentTermEnum;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/store/IndexInput;

    iput-object v2, v1, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    .line 104
    new-instance v2, Lorg/apache/lucene/index/TermInfo;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    invoke-direct {v2, v3}, Lorg/apache/lucene/index/TermInfo;-><init>(Lorg/apache/lucene/index/TermInfo;)V

    iput-object v2, v1, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    .line 106
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/index/TermBuffer;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermBuffer;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/TermBuffer;

    iput-object v2, v1, Lorg/apache/lucene/index/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/index/TermBuffer;

    .line 107
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermEnum;->prevBuffer:Lorg/apache/lucene/index/TermBuffer;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermBuffer;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/TermBuffer;

    iput-object v2, v1, Lorg/apache/lucene/index/SegmentTermEnum;->prevBuffer:Lorg/apache/lucene/index/TermBuffer;

    .line 108
    new-instance v2, Lorg/apache/lucene/index/TermBuffer;

    invoke-direct {v2}, Lorg/apache/lucene/index/TermBuffer;-><init>()V

    iput-object v2, v1, Lorg/apache/lucene/index/SegmentTermEnum;->scanBuffer:Lorg/apache/lucene/index/TermBuffer;

    .line 110
    return-object v1

    .line 101
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public final close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 226
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 227
    return-void
.end method

.method public final docFreq()I
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    iget v0, v0, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    return v0
.end method

.method final freqPointer()J
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    iget-wide v0, v0, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    return-wide v0
.end method

.method public final next()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x1

    .line 126
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->position:J

    add-long v2, v0, v4

    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentTermEnum;->position:J

    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentTermEnum;->size:J

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 127
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->prevBuffer:Lorg/apache/lucene/index/TermBuffer;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/index/TermBuffer;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/TermBuffer;->set(Lorg/apache/lucene/index/TermBuffer;)V

    .line 128
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/index/TermBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermBuffer;->reset()V

    .line 129
    const/4 v0, 0x0

    .line 156
    :goto_0
    return v0

    .line 132
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->prevBuffer:Lorg/apache/lucene/index/TermBuffer;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/index/TermBuffer;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/TermBuffer;->set(Lorg/apache/lucene/index/TermBuffer;)V

    .line 133
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/index/TermBuffer;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermEnum;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/TermBuffer;->read(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfos;)V

    .line 135
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    iput v1, v0, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    .line 136
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    iget-wide v2, v0, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, v0, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    .line 137
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    iget-wide v2, v0, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, v0, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    .line 139
    iget v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->format:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 142
    iget-boolean v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->isIndex:Z

    if-nez v0, :cond_1

    .line 143
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    iget v0, v0, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    iget v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->formatM1SkipInterval:I

    if-le v0, v1, :cond_1

    .line 144
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    iput v1, v0, Lorg/apache/lucene/index/TermInfo;->skipOffset:I

    .line 153
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->isIndex:Z

    if-eqz v0, :cond_2

    .line 154
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->indexPointer:J

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->indexPointer:J

    .line 156
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 149
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    iget v0, v0, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    iget v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->skipInterval:I

    if-lt v0, v1, :cond_1

    .line 150
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    iput v1, v0, Lorg/apache/lucene/index/TermInfo;->skipOffset:I

    goto :goto_1
.end method

.method final prev()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->prevBuffer:Lorg/apache/lucene/index/TermBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermBuffer;->toTerm()Lorg/apache/lucene/index/Term;

    move-result-object v0

    return-object v0
.end method

.method final proxPointer()J
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    iget-wide v0, v0, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    return-wide v0
.end method

.method final scanTo(Lorg/apache/lucene/index/Term;)I
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->scanBuffer:Lorg/apache/lucene/index/TermBuffer;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/TermBuffer;->set(Lorg/apache/lucene/index/Term;)V

    .line 167
    const/4 v0, 0x0

    .line 168
    .local v0, "count":I
    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->first:Z

    if-eqz v1, :cond_0

    .line 170
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentTermEnum;->next()Z

    .line 171
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->first:Z

    .line 172
    add-int/lit8 v0, v0, 0x1

    .line 174
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->scanBuffer:Lorg/apache/lucene/index/TermBuffer;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/index/TermBuffer;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/TermBuffer;->compareTo(Lorg/apache/lucene/index/TermBuffer;)I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentTermEnum;->next()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_1
    return v0
.end method

.method final seek(JJLorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermInfo;)V
    .locals 3
    .param p1, "pointer"    # J
    .param p3, "p"    # J
    .param p5, "t"    # Lorg/apache/lucene/index/Term;
    .param p6, "ti"    # Lorg/apache/lucene/index/TermInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 116
    iput-wide p3, p0, Lorg/apache/lucene/index/SegmentTermEnum;->position:J

    .line 117
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/index/TermBuffer;

    invoke-virtual {v0, p5}, Lorg/apache/lucene/index/TermBuffer;->set(Lorg/apache/lucene/index/Term;)V

    .line 118
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->prevBuffer:Lorg/apache/lucene/index/TermBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermBuffer;->reset()V

    .line 119
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    invoke-virtual {v0, p6}, Lorg/apache/lucene/index/TermInfo;->set(Lorg/apache/lucene/index/TermInfo;)V

    .line 120
    const-wide/16 v0, -0x1

    cmp-long v0, p3, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->first:Z

    .line 121
    return-void

    .line 120
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final term()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/index/TermBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermBuffer;->toTerm()Lorg/apache/lucene/index/Term;

    move-result-object v0

    return-object v0
.end method

.method final termInfo()Lorg/apache/lucene/index/TermInfo;
    .locals 2

    .prologue
    .line 195
    new-instance v0, Lorg/apache/lucene/index/TermInfo;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TermInfo;-><init>(Lorg/apache/lucene/index/TermInfo;)V

    return-object v0
.end method

.method final termInfo(Lorg/apache/lucene/index/TermInfo;)V
    .locals 1
    .param p1, "ti"    # Lorg/apache/lucene/index/TermInfo;

    .prologue
    .line 201
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo:Lorg/apache/lucene/index/TermInfo;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/TermInfo;->set(Lorg/apache/lucene/index/TermInfo;)V

    .line 202
    return-void
.end method
