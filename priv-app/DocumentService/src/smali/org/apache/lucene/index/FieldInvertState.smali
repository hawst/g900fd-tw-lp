.class public final Lorg/apache/lucene/index/FieldInvertState;
.super Ljava/lang/Object;
.source "FieldInvertState.java"


# instance fields
.field attributeSource:Lorg/apache/lucene/util/AttributeSource;

.field boost:F

.field length:I

.field maxTermFrequency:I

.field numOverlap:I

.field offset:I

.field position:I

.field uniqueTermCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method public constructor <init>(IIIIF)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "length"    # I
    .param p3, "numOverlap"    # I
    .param p4, "offset"    # I
    .param p5, "boost"    # F

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput p1, p0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    .line 43
    iput p2, p0, Lorg/apache/lucene/index/FieldInvertState;->length:I

    .line 44
    iput p3, p0, Lorg/apache/lucene/index/FieldInvertState;->numOverlap:I

    .line 45
    iput p4, p0, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    .line 46
    iput p5, p0, Lorg/apache/lucene/index/FieldInvertState;->boost:F

    .line 47
    return-void
.end method


# virtual methods
.method public getAttributeSource()Lorg/apache/lucene/util/AttributeSource;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    return-object v0
.end method

.method public getBoost()F
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lorg/apache/lucene/index/FieldInvertState;->boost:F

    return v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lorg/apache/lucene/index/FieldInvertState;->length:I

    return v0
.end method

.method public getMaxTermFrequency()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    return v0
.end method

.method public getNumOverlap()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lorg/apache/lucene/index/FieldInvertState;->numOverlap:I

    return v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    return v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    return v0
.end method

.method public getUniqueTermCount()I
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    return v0
.end method

.method reset(F)V
    .locals 1
    .param p1, "docBoost"    # F

    .prologue
    const/4 v0, 0x0

    .line 54
    iput v0, p0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    .line 55
    iput v0, p0, Lorg/apache/lucene/index/FieldInvertState;->length:I

    .line 56
    iput v0, p0, Lorg/apache/lucene/index/FieldInvertState;->numOverlap:I

    .line 57
    iput v0, p0, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    .line 58
    iput v0, p0, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    .line 59
    iput v0, p0, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    .line 60
    iput p1, p0, Lorg/apache/lucene/index/FieldInvertState;->boost:F

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    .line 62
    return-void
.end method

.method public setBoost(F)V
    .locals 0
    .param p1, "boost"    # F

    .prologue
    .line 115
    iput p1, p0, Lorg/apache/lucene/index/FieldInvertState;->boost:F

    .line 116
    return-void
.end method

.method public setLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 81
    iput p1, p0, Lorg/apache/lucene/index/FieldInvertState;->length:I

    .line 82
    return-void
.end method

.method public setNumOverlap(I)V
    .locals 0
    .param p1, "numOverlap"    # I

    .prologue
    .line 93
    iput p1, p0, Lorg/apache/lucene/index/FieldInvertState;->numOverlap:I

    .line 94
    return-void
.end method
