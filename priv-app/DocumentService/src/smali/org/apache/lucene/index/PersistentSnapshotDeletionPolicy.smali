.class public Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;
.super Lorg/apache/lucene/index/SnapshotDeletionPolicy;
.source "PersistentSnapshotDeletionPolicy.java"


# static fields
.field private static final SNAPSHOTS_ID:Ljava/lang/String; = "$SNAPSHOTS_DOC$"


# instance fields
.field private final writer:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;Lorg/apache/lucene/util/Version;)V
    .locals 6
    .param p1, "primary"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .param p2, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p3, "mode"    # Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    .param p4, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Lorg/apache/lucene/store/LockObtainFailedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 114
    invoke-direct {p0, p1, v5}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;-><init>(Lorg/apache/lucene/index/IndexDeletionPolicy;Ljava/util/Map;)V

    .line 117
    new-instance v3, Lorg/apache/lucene/index/IndexWriter;

    new-instance v4, Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-direct {v4, p4, v5}, Lorg/apache/lucene/index/IndexWriterConfig;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V

    invoke-virtual {v4, p3}, Lorg/apache/lucene/index/IndexWriterConfig;->setOpenMode(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v4

    invoke-direct {v3, p2, v4}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    iput-object v3, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 118
    sget-object v3, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    if-eq p3, v3, :cond_0

    .line 122
    iget-object v3, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriter;->commit()V

    .line 129
    :cond_0
    :try_start_0
    invoke-static {p2}, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->readSnapshotsInfo(Lorg/apache/lucene/store/Directory;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 130
    .local v1, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p0, v3, v4, v5}, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->registerSnapshotInfo(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/index/IndexCommit;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 132
    .end local v1    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Ljava/lang/RuntimeException;
    iget-object v3, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriter;->close()V

    .line 134
    throw v0

    .line 135
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v0

    .line 136
    .local v0, "e":Ljava/io/IOException;
    iget-object v3, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriter;->close()V

    .line 137
    throw v0

    .line 139
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method private persistSnapshotInfos(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "segment"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    iget-object v3, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriter;->deleteAll()V

    .line 191
    new-instance v0, Lorg/apache/lucene/document/Document;

    invoke-direct {v0}, Lorg/apache/lucene/document/Document;-><init>()V

    .line 192
    .local v0, "d":Lorg/apache/lucene/document/Document;
    new-instance v3, Lorg/apache/lucene/document/Field;

    const-string/jumbo v4, "$SNAPSHOTS_DOC$"

    const-string/jumbo v5, ""

    sget-object v6, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    sget-object v7, Lorg/apache/lucene/document/Field$Index;->NO:Lorg/apache/lucene/document/Field$Index;

    invoke-direct {v3, v4, v5, v6, v7}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;)V

    invoke-virtual {v0, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/document/Fieldable;)V

    .line 193
    invoke-super {p0}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->getSnapshots()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 194
    .local v1, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v5, Lorg/apache/lucene/document/Field;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    sget-object v6, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    sget-object v7, Lorg/apache/lucene/document/Field$Index;->NO:Lorg/apache/lucene/document/Field$Index;

    invoke-direct {v5, v3, v4, v6, v7}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;)V

    invoke-virtual {v0, v5}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/document/Fieldable;)V

    goto :goto_0

    .line 196
    .end local v1    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    if-eqz p1, :cond_1

    .line 197
    new-instance v3, Lorg/apache/lucene/document/Field;

    sget-object v4, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    sget-object v5, Lorg/apache/lucene/document/Field$Index;->NO:Lorg/apache/lucene/document/Field$Index;

    invoke-direct {v3, p1, p2, v4, v5}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;)V

    invoke-virtual {v0, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/document/Fieldable;)V

    .line 199
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/IndexWriter;->addDocument(Lorg/apache/lucene/document/Document;)V

    .line 200
    iget-object v3, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriter;->commit()V

    .line 201
    return-void
.end method

.method public static readSnapshotsInfo(Lorg/apache/lucene/store/Directory;)Ljava/util/Map;
    .locals 10
    .param p0, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 67
    invoke-static {p0, v7}, Lorg/apache/lucene/index/IndexReader;->open(Lorg/apache/lucene/store/Directory;Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v4

    .line 68
    .local v4, "r":Lorg/apache/lucene/index/IndexReader;
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 70
    .local v6, "snapshots":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v3

    .line 72
    .local v3, "numDocs":I
    if-ne v3, v7, :cond_1

    .line 73
    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v4, v7}, Lorg/apache/lucene/index/IndexReader;->document(I)Lorg/apache/lucene/document/Document;

    move-result-object v0

    .line 74
    .local v0, "doc":Lorg/apache/lucene/document/Document;
    const-string/jumbo v7, "$SNAPSHOTS_DOC$"

    invoke-virtual {v0, v7}, Lorg/apache/lucene/document/Document;->getField(Ljava/lang/String;)Lorg/apache/lucene/document/Field;

    move-result-object v5

    .line 75
    .local v5, "sid":Lorg/apache/lucene/document/Field;
    if-nez v5, :cond_0

    .line 76
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string/jumbo v8, "directory is not a valid snapshots store!"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    .end local v0    # "doc":Lorg/apache/lucene/document/Document;
    .end local v3    # "numDocs":I
    .end local v5    # "sid":Lorg/apache/lucene/document/Field;
    :catchall_0
    move-exception v7

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexReader;->close()V

    throw v7

    .line 78
    .restart local v0    # "doc":Lorg/apache/lucene/document/Document;
    .restart local v3    # "numDocs":I
    .restart local v5    # "sid":Lorg/apache/lucene/document/Field;
    :cond_0
    :try_start_1
    const-string/jumbo v7, "$SNAPSHOTS_DOC$"

    invoke-virtual {v0, v7}, Lorg/apache/lucene/document/Document;->removeField(Ljava/lang/String;)V

    .line 79
    invoke-virtual {v0}, Lorg/apache/lucene/document/Document;->getFields()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/document/Fieldable;

    .line 80
    .local v1, "f":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v1}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1}, Lorg/apache/lucene/document/Fieldable;->stringValue()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 82
    .end local v0    # "doc":Lorg/apache/lucene/document/Document;
    .end local v1    # "f":Lorg/apache/lucene/document/Fieldable;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "sid":Lorg/apache/lucene/document/Field;
    :cond_1
    if-eqz v3, :cond_2

    .line 83
    new-instance v7, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "should be at most 1 document in the snapshots directory: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    :cond_2
    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexReader;->close()V

    .line 89
    return-object v6
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->close()V

    .line 183
    return-void
.end method

.method public declared-synchronized onInit(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->onInit(Ljava/util/List;)V

    .line 149
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->persistSnapshotInfos(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    monitor-exit p0

    return-void

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized release(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->release(Ljava/lang/String;)V

    .line 177
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->persistSnapshotInfos(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    monitor-exit p0

    return-void

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized snapshot(Ljava/lang/String;)Lorg/apache/lucene/index/IndexCommit;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 160
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->checkSnapshotted(Ljava/lang/String;)V

    .line 161
    const-string/jumbo v0, "$SNAPSHOTS_DOC$"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is reserved and cannot be used as a snapshot id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 164
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/PersistentSnapshotDeletionPolicy;->persistSnapshotInfos(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-super {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->snapshot(Ljava/lang/String;)Lorg/apache/lucene/index/IndexCommit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method
