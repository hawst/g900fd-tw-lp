.class public Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;
.super Ljava/lang/Object;
.source "PositionBasedTermVectorMapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/PositionBasedTermVectorMapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TVPositionInfo"
.end annotation


# instance fields
.field private offsets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/TermVectorOffsetInfo;",
            ">;"
        }
    .end annotation
.end field

.field private position:I

.field private terms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(IZ)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "storeOffsets"    # Z

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput p1, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;->position:I

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;->terms:Ljava/util/List;

    .line 131
    if-eqz p2, :cond_0

    .line 132
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;->offsets:Ljava/util/List;

    .line 134
    :cond_0
    return-void
.end method


# virtual methods
.method addTerm(Ljava/lang/String;Lorg/apache/lucene/index/TermVectorOffsetInfo;)V
    .locals 1
    .param p1, "term"    # Ljava/lang/String;
    .param p2, "info"    # Lorg/apache/lucene/index/TermVectorOffsetInfo;

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;->terms:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    iget-object v0, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;->offsets:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;->offsets:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_0
    return-void
.end method

.method public getOffsets()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/TermVectorOffsetInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;->offsets:Ljava/util/List;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;->position:I

    return v0
.end method

.method public getTerms()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;->terms:Ljava/util/List;

    return-object v0
.end method
