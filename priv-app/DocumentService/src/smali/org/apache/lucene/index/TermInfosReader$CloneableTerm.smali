.class Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;
.super Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;
.source "TermInfosReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/TermInfosReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CloneableTerm"
.end annotation


# instance fields
.field private final term:Lorg/apache/lucene/index/Term;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "t"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 61
    invoke-direct {p0}, Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;-><init>()V

    .line 62
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;->term:Lorg/apache/lucene/index/Term;

    .line 63
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;

    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;->term:Lorg/apache/lucene/index/Term;

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;-><init>(Lorg/apache/lucene/index/Term;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "_other"    # Ljava/lang/Object;

    .prologue
    .line 72
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;

    .line 73
    .local v0, "other":Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;->term:Lorg/apache/lucene/index/Term;

    iget-object v2, v0, Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v0

    return v0
.end method
