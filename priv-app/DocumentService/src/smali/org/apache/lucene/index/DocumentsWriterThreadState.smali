.class final Lorg/apache/lucene/index/DocumentsWriterThreadState;
.super Ljava/lang/Object;
.source "DocumentsWriterThreadState.java"


# instance fields
.field final consumer:Lorg/apache/lucene/index/DocConsumerPerThread;

.field final docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

.field final docWriter:Lorg/apache/lucene/index/DocumentsWriter;

.field isIdle:Z

.field numThreads:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriter;)V
    .locals 2
    .param p1, "docWriter"    # Lorg/apache/lucene/index/DocumentsWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterThreadState;->isIdle:Z

    .line 29
    iput v0, p0, Lorg/apache/lucene/index/DocumentsWriterThreadState;->numThreads:I

    .line 36
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 37
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;

    invoke-direct {v0}, Lorg/apache/lucene/index/DocumentsWriter$DocState;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 38
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v1, p1, Lorg/apache/lucene/index/DocumentsWriter;->maxFieldLength:I

    iput v1, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->maxFieldLength:I

    .line 39
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget-object v1, p1, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    iput-object v1, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->infoStream:Ljava/io/PrintStream;

    .line 40
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget-object v1, p1, Lorg/apache/lucene/index/DocumentsWriter;->similarity:Lorg/apache/lucene/search/Similarity;

    iput-object v1, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 41
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput-object p1, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 42
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriter;->consumer:Lorg/apache/lucene/index/DocConsumer;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/DocConsumer;->addThread(Lorg/apache/lucene/index/DocumentsWriterThreadState;)Lorg/apache/lucene/index/DocConsumerPerThread;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterThreadState;->consumer:Lorg/apache/lucene/index/DocConsumerPerThread;

    .line 43
    return-void
.end method


# virtual methods
.method doAfterFlush()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/DocumentsWriterThreadState;->numThreads:I

    .line 47
    return-void
.end method
