.class final Lorg/apache/lucene/index/DocFieldProcessorPerField;
.super Ljava/lang/Object;
.source "DocFieldProcessorPerField.java"


# instance fields
.field final consumer:Lorg/apache/lucene/index/DocFieldConsumerPerField;

.field fieldCount:I

.field final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field fields:[Lorg/apache/lucene/document/Fieldable;

.field lastGen:I

.field next:Lorg/apache/lucene/index/DocFieldProcessorPerField;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocFieldProcessorPerThread;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 1
    .param p1, "perThread"    # Lorg/apache/lucene/index/DocFieldProcessorPerThread;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->lastGen:I

    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/lucene/document/Fieldable;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fields:[Lorg/apache/lucene/document/Fieldable;

    .line 38
    iget-object v0, p1, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->consumer:Lorg/apache/lucene/index/DocFieldConsumerPerThread;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/index/DocFieldConsumerPerThread;->addField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/DocFieldConsumerPerField;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->consumer:Lorg/apache/lucene/index/DocFieldConsumerPerField;

    .line 39
    iput-object p2, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 40
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->consumer:Lorg/apache/lucene/index/DocFieldConsumerPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocFieldConsumerPerField;->abort()V

    .line 44
    return-void
.end method
