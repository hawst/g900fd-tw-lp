.class public final Lorg/apache/lucene/index/FieldInfos;
.super Ljava/lang/Object;
.source "FieldInfos.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/lucene/index/FieldInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final CURRENT_FORMAT:I = -0x3

.field public static final FORMAT_OMIT_POSITIONS:I = -0x3

.field public static final FORMAT_PRE:I = -0x1

.field public static final FORMAT_START:I = -0x2

.field static final IS_INDEXED:B = 0x1t

.field static final OMIT_NORMS:B = 0x10t

.field static final OMIT_POSITIONS:B = -0x80t

.field static final OMIT_TERM_FREQ_AND_POSITIONS:B = 0x40t

.field static final STORE_PAYLOADS:B = 0x20t

.field static final STORE_TERMVECTOR:B = 0x2t


# instance fields
.field private final byName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/FieldInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final byNumber:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/index/FieldInfo;",
            ">;"
        }
    .end annotation
.end field

.field private format:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FieldInfos;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/FieldInfos;->byName:Ljava/util/HashMap;

    .line 61
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    .locals 6
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/ArrayList;

    .line 58
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lorg/apache/lucene/index/FieldInfos;->byName:Ljava/util/HashMap;

    .line 71
    invoke-virtual {p1, p2}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    .line 74
    .local v0, "input":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    invoke-direct {p0, v0, p2}, Lorg/apache/lucene/index/FieldInfos;->read(Lorg/apache/lucene/store/IndexInput;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :goto_0
    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 99
    return-void

    .line 75
    :catch_0
    move-exception v1

    .line 76
    .local v1, "ioe":Ljava/io/IOException;
    :try_start_1
    iget v3, p0, Lorg/apache/lucene/index/FieldInfos;->format:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 80
    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 81
    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->setModifiedUTF8StringsMode()V

    .line 82
    iget-object v3, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 83
    iget-object v3, p0, Lorg/apache/lucene/index/FieldInfos;->byName:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    :try_start_2
    invoke-direct {p0, v0, p2}, Lorg/apache/lucene/index/FieldInfos;->read(Lorg/apache/lucene/store/IndexInput;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 86
    :catch_1
    move-exception v2

    .line 88
    .local v2, "t":Ljava/lang/Throwable;
    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 97
    .end local v1    # "ioe":Ljava/io/IOException;
    .end local v2    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    throw v3

    .line 93
    .restart local v1    # "ioe":Ljava/io/IOException;
    :cond_0
    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method private addInternal(Ljava/lang/String;ZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)Lorg/apache/lucene/index/FieldInfo;
    .locals 8
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "isIndexed"    # Z
    .param p3, "storeTermVector"    # Z
    .param p4, "omitNorms"    # Z
    .param p5, "storePayloads"    # Z
    .param p6, "indexOptions"    # Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .prologue
    .line 213
    invoke-static {p1}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 214
    new-instance v0, Lorg/apache/lucene/index/FieldInfo;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move-object v1, p1

    move v2, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/index/FieldInfo;-><init>(Ljava/lang/String;ZIZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 216
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    iget-object v1, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    iget-object v1, p0, Lorg/apache/lucene/index/FieldInfos;->byName:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    return-object v0
.end method

.method private read(Lorg/apache/lucene/store/IndexInput;Ljava/lang/String;)V
    .locals 18
    .param p1, "input"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 314
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v10

    .line 316
    .local v10, "firstInt":I
    if-gez v10, :cond_0

    .line 318
    move-object/from16 v0, p0

    iput v10, v0, Lorg/apache/lucene/index/FieldInfos;->format:I

    .line 323
    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/index/FieldInfos;->format:I

    const/4 v13, -0x1

    if-eq v2, v13, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/index/FieldInfos;->format:I

    const/4 v13, -0x2

    if-eq v2, v13, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/index/FieldInfos;->format:I

    const/4 v13, -0x3

    if-eq v2, v13, :cond_1

    .line 324
    new-instance v2, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "unrecognized format "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/lucene/index/FieldInfos;->format:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " in file \""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "\""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v2, v13}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 320
    :cond_0
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/index/FieldInfos;->format:I

    goto :goto_0

    .line 328
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/index/FieldInfos;->format:I

    const/4 v13, -0x1

    if-ne v2, v13, :cond_3

    .line 329
    move v12, v10

    .line 334
    .local v12, "size":I
    :goto_1
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    if-ge v11, v12, :cond_b

    .line 335
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 336
    .local v3, "name":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v9

    .line 337
    .local v9, "bits":B
    and-int/lit8 v2, v9, 0x1

    if-eqz v2, :cond_4

    const/4 v4, 0x1

    .line 338
    .local v4, "isIndexed":Z
    :goto_3
    and-int/lit8 v2, v9, 0x2

    if-eqz v2, :cond_5

    const/4 v5, 0x1

    .line 339
    .local v5, "storeTermVector":Z
    :goto_4
    and-int/lit8 v2, v9, 0x10

    if-eqz v2, :cond_6

    const/4 v6, 0x1

    .line 340
    .local v6, "omitNorms":Z
    :goto_5
    and-int/lit8 v2, v9, 0x20

    if-eqz v2, :cond_7

    const/4 v7, 0x1

    .line 342
    .local v7, "storePayloads":Z
    :goto_6
    and-int/lit8 v2, v9, 0x40

    if-eqz v2, :cond_8

    .line 343
    sget-object v8, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 357
    .local v8, "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :goto_7
    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v8, v2, :cond_2

    .line 358
    const/4 v7, 0x0

    :cond_2
    move-object/from16 v2, p0

    .line 361
    invoke-direct/range {v2 .. v8}, Lorg/apache/lucene/index/FieldInfos;->addInternal(Ljava/lang/String;ZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)Lorg/apache/lucene/index/FieldInfo;

    .line 334
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 331
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "isIndexed":Z
    .end local v5    # "storeTermVector":Z
    .end local v6    # "omitNorms":Z
    .end local v7    # "storePayloads":Z
    .end local v8    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .end local v9    # "bits":B
    .end local v11    # "i":I
    .end local v12    # "size":I
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v12

    .restart local v12    # "size":I
    goto :goto_1

    .line 337
    .restart local v3    # "name":Ljava/lang/String;
    .restart local v9    # "bits":B
    .restart local v11    # "i":I
    :cond_4
    const/4 v4, 0x0

    goto :goto_3

    .line 338
    .restart local v4    # "isIndexed":Z
    :cond_5
    const/4 v5, 0x0

    goto :goto_4

    .line 339
    .restart local v5    # "storeTermVector":Z
    :cond_6
    const/4 v6, 0x0

    goto :goto_5

    .line 340
    .restart local v6    # "omitNorms":Z
    :cond_7
    const/4 v7, 0x0

    goto :goto_6

    .line 344
    .restart local v7    # "storePayloads":Z
    :cond_8
    and-int/lit8 v2, v9, -0x80

    if-eqz v2, :cond_a

    .line 345
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/index/FieldInfos;->format:I

    const/4 v13, -0x3

    if-gt v2, v13, :cond_9

    .line 346
    sget-object v8, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .restart local v8    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    goto :goto_7

    .line 348
    .end local v8    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_9
    new-instance v2, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Corrupt fieldinfos, OMIT_POSITIONS set but format="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/lucene/index/FieldInfos;->format:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " (resource: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v2, v13}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 351
    :cond_a
    sget-object v8, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .restart local v8    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    goto :goto_7

    .line 364
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "isIndexed":Z
    .end local v5    # "storeTermVector":Z
    .end local v6    # "omitNorms":Z
    .end local v7    # "storePayloads":Z
    .end local v8    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .end local v9    # "bits":B
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v16

    cmp-long v2, v14, v16

    if-eqz v2, :cond_c

    .line 365
    new-instance v2, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "did not read all bytes from file \""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "\": read "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " vs size "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " (resource: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v2, v13}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 367
    :cond_c
    return-void
.end method


# virtual methods
.method public declared-synchronized add(Ljava/lang/String;ZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)Lorg/apache/lucene/index/FieldInfo;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "isIndexed"    # Z
    .param p3, "storeTermVector"    # Z
    .param p4, "omitNorms"    # Z
    .param p5, "storePayloads"    # Z
    .param p6, "indexOptions"    # Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 196
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-nez v0, :cond_1

    .line 197
    invoke-direct/range {p0 .. p6}, Lorg/apache/lucene/index/FieldInfos;->addInternal(Ljava/lang/String;ZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)Lorg/apache/lucene/index/FieldInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 202
    .end local v0    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :cond_0
    monitor-exit p0

    return-object v0

    .restart local v0    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :cond_1
    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    .line 199
    :try_start_1
    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/index/FieldInfo;->update(ZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 201
    sget-boolean v1, Lorg/apache/lucene/index/FieldInfos;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v1, v2, :cond_0

    iget-boolean v1, v0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 195
    .end local v0    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized add(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/FieldInfo;
    .locals 7
    .param p1, "fi"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 206
    monitor-enter p0

    :try_start_0
    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-boolean v2, p1, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    iget-boolean v3, p1, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    iget-boolean v4, p1, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    iget-boolean v5, p1, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    iget-object v6, p1, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lorg/apache/lucene/index/FieldInfos;->add(Ljava/lang/String;ZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)Lorg/apache/lucene/index/FieldInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized add(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "isIndexed"    # Z

    .prologue
    .line 151
    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0, v1}, Lorg/apache/lucene/index/FieldInfos;->add(Ljava/lang/String;ZZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    monitor-exit p0

    return-void

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized add(Ljava/lang/String;ZZ)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "isIndexed"    # Z
    .param p3, "storeTermVector"    # Z

    .prologue
    .line 162
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/apache/lucene/index/FieldInfos;->add(Ljava/lang/String;ZZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    monitor-exit p0

    return-void

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized add(Ljava/lang/String;ZZZ)V
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "isIndexed"    # Z
    .param p3, "storeTermVector"    # Z
    .param p4, "omitNorms"    # Z

    .prologue
    .line 177
    monitor-enter p0

    const/4 v5, 0x0

    :try_start_0
    sget-object v6, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v6}, Lorg/apache/lucene/index/FieldInfos;->add(Ljava/lang/String;ZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)Lorg/apache/lucene/index/FieldInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    monitor-exit p0

    return-void

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized add(Lorg/apache/lucene/document/Document;)V
    .locals 10
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lorg/apache/lucene/document/Document;->getFields()Ljava/util/List;

    move-result-object v8

    .line 125
    .local v8, "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Fieldable;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/document/Fieldable;

    .line 126
    .local v7, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v7}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7}, Lorg/apache/lucene/document/Fieldable;->isIndexed()Z

    move-result v2

    invoke-interface {v7}, Lorg/apache/lucene/document/Fieldable;->isTermVectorStored()Z

    move-result v3

    invoke-interface {v7}, Lorg/apache/lucene/document/Fieldable;->getOmitNorms()Z

    move-result v4

    const/4 v5, 0x0

    invoke-interface {v7}, Lorg/apache/lucene/document/Fieldable;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lorg/apache/lucene/index/FieldInfos;->add(Ljava/lang/String;ZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)Lorg/apache/lucene/index/FieldInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124
    .end local v7    # "field":Lorg/apache/lucene/document/Fieldable;
    .end local v8    # "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Fieldable;>;"
    .end local v9    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 129
    .restart local v8    # "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Fieldable;>;"
    .restart local v9    # "i$":Ljava/util/Iterator;
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public add(Lorg/apache/lucene/index/FieldInfos;)V
    .locals 3
    .param p1, "other"    # Lorg/apache/lucene/index/FieldInfos;

    .prologue
    .line 102
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    .line 103
    .local v0, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/FieldInfos;->add(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/FieldInfo;

    goto :goto_0

    .line 105
    .end local v0    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    :cond_0
    return-void
.end method

.method public declared-synchronized clone()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 112
    monitor-enter p0

    :try_start_0
    new-instance v1, Lorg/apache/lucene/index/FieldInfos;

    invoke-direct {v1}, Lorg/apache/lucene/index/FieldInfos;-><init>()V

    .line 113
    .local v1, "fis":Lorg/apache/lucene/index/FieldInfos;
    iget-object v4, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 114
    .local v3, "numField":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 115
    iget-object v4, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/FieldInfo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    .line 116
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    iget-object v4, v1, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    iget-object v4, v1, Lorg/apache/lucene/index/FieldInfos;->byName:Ljava/util/HashMap;

    iget-object v5, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 119
    .end local v0    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :cond_0
    monitor-exit p0

    return-object v1

    .line 112
    .end local v1    # "fis":Lorg/apache/lucene/index/FieldInfos;
    .end local v2    # "i":I
    .end local v3    # "numField":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;
    .locals 1
    .param p1, "fieldNumber"    # I

    .prologue
    .line 255
    if-ltz p1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 233
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos;->byName:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    return-object v0
.end method

.method public fieldName(I)Ljava/lang/String;
    .locals 2
    .param p1, "fieldNumber"    # I

    .prologue
    .line 244
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 245
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const-string/jumbo v1, ""

    goto :goto_0
.end method

.method public fieldNumber(Ljava/lang/String;)I
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 228
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 229
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-eqz v0, :cond_0

    iget v1, v0, Lorg/apache/lucene/index/FieldInfo;->number:I

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public hasProx()Z
    .locals 5

    .prologue
    .line 133
    iget-object v3, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 134
    .local v2, "numFields":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 135
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 136
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    iget-boolean v3, v0, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    if-eqz v3, :cond_0

    iget-object v3, v0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v3, v4, :cond_0

    .line 137
    const/4 v3, 0x1

    .line 140
    .end local v0    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :goto_1
    return v3

    .line 134
    .restart local v0    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 140
    .end local v0    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public hasVectors()Z
    .locals 3

    .prologue
    .line 273
    const/4 v0, 0x0

    .line 274
    .local v0, "hasVectors":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 275
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v2

    iget-boolean v2, v2, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    if-eqz v2, :cond_1

    .line 276
    const/4 v0, 0x1

    .line 280
    :cond_0
    return v0

    .line 274
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/index/FieldInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 259
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public write(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    .locals 2
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 284
    invoke-virtual {p1, p2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    .line 286
    .local v0, "output":Lorg/apache/lucene/store/IndexOutput;
    :try_start_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/FieldInfos;->write(Lorg/apache/lucene/store/IndexOutput;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 290
    return-void

    .line 288
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->close()V

    throw v1
.end method

.method public write(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 5
    .param p1, "output"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 293
    const/4 v3, -0x3

    invoke-virtual {p1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 294
    invoke-virtual {p0}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v3

    invoke-virtual {p1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 295
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v3

    if-ge v2, v3, :cond_7

    .line 296
    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v1

    .line 297
    .local v1, "fi":Lorg/apache/lucene/index/FieldInfo;
    sget-boolean v3, Lorg/apache/lucene/index/FieldInfos;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, v1, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v3, v4, :cond_0

    iget-boolean v3, v1, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 298
    :cond_0
    const/4 v0, 0x0

    .line 299
    .local v0, "bits":B
    iget-boolean v3, v1, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    int-to-byte v0, v3

    .line 300
    :cond_1
    iget-boolean v3, v1, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    if-eqz v3, :cond_2

    or-int/lit8 v3, v0, 0x2

    int-to-byte v0, v3

    .line 301
    :cond_2
    iget-boolean v3, v1, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    if-eqz v3, :cond_3

    or-int/lit8 v3, v0, 0x10

    int-to-byte v0, v3

    .line 302
    :cond_3
    iget-boolean v3, v1, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    if-eqz v3, :cond_4

    or-int/lit8 v3, v0, 0x20

    int-to-byte v0, v3

    .line 303
    :cond_4
    iget-object v3, v1, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v3, v4, :cond_6

    .line 304
    or-int/lit8 v3, v0, 0x40

    int-to-byte v0, v3

    .line 308
    :cond_5
    :goto_1
    iget-object v3, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeString(Ljava/lang/String;)V

    .line 309
    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 295
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 305
    :cond_6
    iget-object v3, v1, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v3, v4, :cond_5

    .line 306
    or-int/lit8 v3, v0, -0x80

    int-to-byte v0, v3

    goto :goto_1

    .line 311
    .end local v0    # "bits":B
    .end local v1    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :cond_7
    return-void
.end method
