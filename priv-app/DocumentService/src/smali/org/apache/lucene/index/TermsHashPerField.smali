.class final Lorg/apache/lucene/index/TermsHashPerField;
.super Lorg/apache/lucene/index/InvertedDocConsumerPerField;
.source "TermsHashPerField.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final bytePool:Lorg/apache/lucene/index/ByteBlockPool;

.field final charPool:Lorg/apache/lucene/index/CharBlockPool;

.field final consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

.field private doCall:Z

.field private doNextCall:Z

.field final docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

.field final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field final fieldState:Lorg/apache/lucene/index/FieldInvertState;

.field final intPool:Lorg/apache/lucene/index/IntBlockPool;

.field intUptoStart:I

.field intUptos:[I

.field final nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

.field final numPostingInt:I

.field numPostings:I

.field final perThread:Lorg/apache/lucene/index/TermsHashPerThread;

.field postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

.field postingsCompacted:Z

.field private postingsHash:[I

.field private postingsHashHalfSize:I

.field private postingsHashMask:I

.field private postingsHashSize:I

.field final streamCount:I

.field termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/TermsHashPerThread;Lorg/apache/lucene/index/TermsHashPerThread;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 2
    .param p1, "docInverterPerField"    # Lorg/apache/lucene/index/DocInverterPerField;
    .param p2, "perThread"    # Lorg/apache/lucene/index/TermsHashPerThread;
    .param p3, "nextPerThread"    # Lorg/apache/lucene/index/TermsHashPerThread;
    .param p4, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 58
    invoke-direct {p0}, Lorg/apache/lucene/index/InvertedDocConsumerPerField;-><init>()V

    .line 51
    const/4 v0, 0x4

    iput v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashSize:I

    .line 52
    iget v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashSize:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashHalfSize:I

    .line 53
    iget v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashMask:I

    .line 59
    iput-object p2, p0, Lorg/apache/lucene/index/TermsHashPerField;->perThread:Lorg/apache/lucene/index/TermsHashPerThread;

    .line 60
    iget-object v0, p2, Lorg/apache/lucene/index/TermsHashPerThread;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    .line 61
    iget-object v0, p2, Lorg/apache/lucene/index/TermsHashPerThread;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    .line 62
    iget-object v0, p2, Lorg/apache/lucene/index/TermsHashPerThread;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    .line 63
    iget-object v0, p2, Lorg/apache/lucene/index/TermsHashPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 65
    iget v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashSize:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    .line 66
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 67
    iget v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashSize:I

    mul-int/lit8 v0, v0, 0x4

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/TermsHashPerField;->bytesUsed(J)V

    .line 69
    iget-object v0, p1, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    .line 70
    iget-object v0, p2, Lorg/apache/lucene/index/TermsHashPerThread;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerThread;

    invoke-virtual {v0, p0, p4}, Lorg/apache/lucene/index/TermsHashConsumerPerThread;->addField(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/TermsHashConsumerPerField;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    .line 71
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashPerField;->initPostingsArray()V

    .line 73
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->getStreamCount()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    .line 74
    iget v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostingInt:I

    .line 75
    iput-object p4, p0, Lorg/apache/lucene/index/TermsHashPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 76
    if-eqz p3, :cond_0

    .line 77
    invoke-virtual {p3, p1, p4}, Lorg/apache/lucene/index/TermsHashPerThread;->addField(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/TermsHashPerField;

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    goto :goto_0
.end method

.method private bytesUsed(J)V
    .locals 1
    .param p1, "size"    # J

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->perThread:Lorg/apache/lucene/index/TermsHashPerThread;

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerThread;->termsHash:Lorg/apache/lucene/index/TermsHash;

    iget-boolean v0, v0, Lorg/apache/lucene/index/TermsHash;->trackAllocations:Z

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->perThread:Lorg/apache/lucene/index/TermsHashPerThread;

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerThread;->termsHash:Lorg/apache/lucene/index/TermsHash;

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHash;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed(J)V

    .line 92
    :cond_0
    return-void
.end method

.method private compactPostings()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 152
    const/4 v1, 0x0

    .line 153
    .local v1, "upto":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashSize:I

    if-ge v0, v2, :cond_2

    .line 154
    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    aget v2, v2, v0

    if-eq v2, v4, :cond_1

    .line 155
    if-ge v1, v0, :cond_0

    .line 156
    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    aget v3, v3, v0

    aput v3, v2, v1

    .line 157
    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    aput v4, v2, v0

    .line 159
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 153
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 163
    :cond_2
    sget-boolean v2, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    iget v2, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    if-eq v1, v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "upto="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " numPostings="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 164
    :cond_3
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsCompacted:Z

    .line 165
    return-void
.end method

.method private final growParallelPostingsArray()V
    .locals 4

    .prologue
    .line 136
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget v0, v1, Lorg/apache/lucene/index/ParallelPostingsArray;->size:I

    .line 137
    .local v0, "oldSize":I
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    invoke-virtual {v1}, Lorg/apache/lucene/index/ParallelPostingsArray;->grow()Lorg/apache/lucene/index/ParallelPostingsArray;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    .line 138
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    invoke-virtual {v1}, Lorg/apache/lucene/index/ParallelPostingsArray;->bytesPerPosting()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget v2, v2, Lorg/apache/lucene/index/ParallelPostingsArray;->size:I

    sub-int/2addr v2, v0

    mul-int/2addr v1, v2

    int-to-long v2, v1

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/index/TermsHashPerField;->bytesUsed(J)V

    .line 139
    return-void
.end method

.method private initPostingsArray()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->createPostingsArray(I)Lorg/apache/lucene/index/ParallelPostingsArray;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    .line 84
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget v0, v0, Lorg/apache/lucene/index/ParallelPostingsArray;->size:I

    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    invoke-virtual {v1}, Lorg/apache/lucene/index/ParallelPostingsArray;->bytesPerPosting()I

    move-result v1

    mul-int/2addr v0, v1

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/TermsHashPerField;->bytesUsed(J)V

    .line 85
    return-void
.end method

.method private postingEquals(I[CI)Z
    .locals 7
    .param p1, "termID"    # I
    .param p2, "tokenText"    # [C
    .param p3, "tokenTextLen"    # I

    .prologue
    const/4 v4, 0x0

    .line 243
    iget-object v5, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v5, v5, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    aget v2, v5, p1

    .line 245
    .local v2, "textStart":I
    iget-object v5, p0, Lorg/apache/lucene/index/TermsHashPerField;->perThread:Lorg/apache/lucene/index/TermsHashPerThread;

    iget-object v5, v5, Lorg/apache/lucene/index/TermsHashPerThread;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    iget-object v5, v5, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    shr-int/lit8 v6, v2, 0xe

    aget-object v1, v5, v6

    .line 246
    .local v1, "text":[C
    sget-boolean v5, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    if-nez v1, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 247
    :cond_0
    and-int/lit16 v0, v2, 0x3fff

    .line 249
    .local v0, "pos":I
    const/4 v3, 0x0

    .line 250
    .local v3, "tokenPos":I
    :goto_0
    if-ge v3, p3, :cond_3

    .line 251
    aget-char v5, p2, v3

    aget-char v6, v1, v0

    if-eq v5, v6, :cond_2

    .line 253
    :cond_1
    :goto_1
    return v4

    .line 250
    :cond_2
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 253
    :cond_3
    const v5, 0xffff

    aget-char v6, v1, v0

    if-ne v5, v6, :cond_1

    const/4 v4, 0x1

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized abort()V
    .locals 1

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/TermsHashPerField;->reset()V

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashPerField;->abort()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    :cond_0
    monitor-exit p0

    return-void

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method add()V
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 358
    sget-boolean v19, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v19, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsCompacted:Z

    move/from16 v19, v0

    if-eqz v19, :cond_0

    new-instance v19, Ljava/lang/AssertionError;

    invoke-direct/range {v19 .. v19}, Ljava/lang/AssertionError;-><init>()V

    throw v19

    .line 364
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v16

    .line 365
    .local v16, "tokenText":[C
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v17

    .line 368
    .local v17, "tokenTextLen":I
    move/from16 v7, v17

    .line 369
    .local v7, "downto":I
    const/4 v6, 0x0

    .line 370
    .local v6, "code":I
    :goto_0
    if-lez v7, :cond_6

    .line 371
    add-int/lit8 v7, v7, -0x1

    aget-char v4, v16, v7

    .line 373
    .local v4, "ch":C
    const v19, 0xdc00

    move/from16 v0, v19

    if-lt v4, v0, :cond_4

    const v19, 0xdfff

    move/from16 v0, v19

    if-gt v4, v0, :cond_4

    .line 374
    if-nez v7, :cond_2

    .line 376
    const v4, 0xfffd

    aput-char v4, v16, v7

    .line 396
    :cond_1
    :goto_1
    mul-int/lit8 v19, v6, 0x1f

    add-int v6, v19, v4

    .line 397
    goto :goto_0

    .line 378
    :cond_2
    add-int/lit8 v19, v7, -0x1

    aget-char v5, v16, v19

    .line 379
    .local v5, "ch2":C
    const v19, 0xd800

    move/from16 v0, v19

    if-lt v5, v0, :cond_3

    const v19, 0xdbff

    move/from16 v0, v19

    if-gt v5, v0, :cond_3

    .line 382
    mul-int/lit8 v19, v6, 0x1f

    add-int v19, v19, v4

    mul-int/lit8 v19, v19, 0x1f

    add-int v6, v19, v5

    .line 383
    add-int/lit8 v7, v7, -0x1

    .line 384
    goto :goto_0

    .line 387
    :cond_3
    const v4, 0xfffd

    aput-char v4, v16, v7

    .line 389
    goto :goto_1

    .line 390
    .end local v5    # "ch2":C
    :cond_4
    const v19, 0xd800

    move/from16 v0, v19

    if-lt v4, v0, :cond_1

    const v19, 0xdbff

    move/from16 v0, v19

    if-le v4, v0, :cond_5

    const v19, 0xffff

    move/from16 v0, v19

    if-ne v4, v0, :cond_1

    .line 393
    :cond_5
    const v4, 0xfffd

    aput-char v4, v16, v7

    goto :goto_1

    .line 399
    .end local v4    # "ch":C
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashMask:I

    move/from16 v19, v0

    and-int v8, v6, v19

    .line 402
    .local v8, "hashPos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    move-object/from16 v19, v0

    aget v12, v19, v8

    .line 404
    .local v12, "termID":I
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v12, v0, :cond_8

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-direct {v0, v12, v1, v2}, Lorg/apache/lucene/index/TermsHashPerField;->postingEquals(I[CI)Z

    move-result v19

    if-nez v19, :cond_8

    .line 407
    shr-int/lit8 v19, v6, 0x8

    add-int v19, v19, v6

    or-int/lit8 v10, v19, 0x1

    .line 409
    .local v10, "inc":I
    :cond_7
    add-int/2addr v6, v10

    .line 410
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashMask:I

    move/from16 v19, v0

    and-int v8, v6, v19

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    move-object/from16 v19, v0

    aget v12, v19, v8

    .line 412
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v12, v0, :cond_8

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-direct {v0, v12, v1, v2}, Lorg/apache/lucene/index/TermsHashPerField;->postingEquals(I[CI)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 415
    .end local v10    # "inc":I
    :cond_8
    const/16 v19, -0x1

    move/from16 v0, v19

    if-ne v12, v0, :cond_14

    .line 419
    add-int/lit8 v14, v17, 0x1

    .line 420
    .local v14, "textLen1":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/index/CharBlockPool;->charUpto:I

    move/from16 v19, v0

    add-int v19, v19, v14

    const/16 v20, 0x4000

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_c

    .line 421
    const/16 v19, 0x4000

    move/from16 v0, v19

    if-le v14, v0, :cond_b

    .line 428
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->maxTermPrefix:Ljava/lang/String;

    move-object/from16 v19, v0

    if-nez v19, :cond_9

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/String;

    const/16 v21, 0x0

    const/16 v22, 0x1e

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->maxTermPrefix:Ljava/lang/String;

    .line 431
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->skippingLongTerm()V

    .line 490
    .end local v14    # "textLen1":I
    :cond_a
    :goto_2
    return-void

    .line 434
    .restart local v14    # "textLen1":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/CharBlockPool;->nextBuffer()V

    .line 438
    :cond_c
    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    .end local v12    # "termID":I
    add-int/lit8 v19, v12, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    .line 439
    .restart local v12    # "termID":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/index/ParallelPostingsArray;->size:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-lt v12, v0, :cond_d

    .line 440
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/TermsHashPerField;->growParallelPostingsArray()V

    .line 443
    :cond_d
    sget-boolean v19, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v19, :cond_e

    const/16 v19, -0x1

    move/from16 v0, v19

    if-ne v12, v0, :cond_e

    new-instance v19, Ljava/lang/AssertionError;

    invoke-direct/range {v19 .. v19}, Ljava/lang/AssertionError;-><init>()V

    throw v19

    .line 445
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v13, v0, Lorg/apache/lucene/index/CharBlockPool;->buffer:[C

    .line 446
    .local v13, "text":[C
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v15, v0, Lorg/apache/lucene/index/CharBlockPool;->charUpto:I

    .line 447
    .local v15, "textUpto":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/index/CharBlockPool;->charOffset:I

    move/from16 v20, v0

    add-int v20, v20, v15

    aput v20, v19, v12

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/index/CharBlockPool;->charUpto:I

    move/from16 v20, v0

    add-int v20, v20, v14

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Lorg/apache/lucene/index/CharBlockPool;->charUpto:I

    .line 449
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    move/from16 v2, v17

    invoke-static {v0, v1, v13, v15, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 450
    add-int v19, v15, v17

    const v20, 0xffff

    aput-char v20, v13, v19

    .line 452
    sget-boolean v19, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v19, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    move-object/from16 v19, v0

    aget v19, v19, v8

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_f

    new-instance v19, Ljava/lang/AssertionError;

    invoke-direct/range {v19 .. v19}, Ljava/lang/AssertionError;-><init>()V

    throw v19

    .line 453
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    move-object/from16 v19, v0

    aput v12, v19, v8

    .line 455
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashHalfSize:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_10

    .line 456
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashSize:I

    move/from16 v19, v0

    mul-int/lit8 v19, v19, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/TermsHashPerField;->rehashPostings(I)V

    .line 457
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    move/from16 v19, v0

    mul-int/lit8 v19, v19, 0x2

    mul-int/lit8 v19, v19, 0x4

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/TermsHashPerField;->bytesUsed(J)V

    .line 461
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->numPostingInt:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/index/IntBlockPool;->intUpto:I

    move/from16 v20, v0

    add-int v19, v19, v20

    const/16 v20, 0x2000

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_11

    .line 462
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/IntBlockPool;->nextBuffer()V

    .line 464
    :cond_11
    const v19, 0x8000

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->numPostingInt:I

    move/from16 v20, v0

    sget v21, Lorg/apache/lucene/index/ByteBlockPool;->FIRST_LEVEL_SIZE:I

    mul-int v20, v20, v21

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_12

    .line 465
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/ByteBlockPool;->nextBuffer()V

    .line 467
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/IntBlockPool;->buffer:[I

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    .line 468
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/index/IntBlockPool;->intUpto:I

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    .line 469
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/index/IntBlockPool;->intUpto:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    move/from16 v21, v0

    add-int v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Lorg/apache/lucene/index/IntBlockPool;->intUpto:I

    .line 471
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/ParallelPostingsArray;->intStarts:[I

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/index/IntBlockPool;->intOffset:I

    move/from16 v21, v0

    add-int v20, v20, v21

    aput v20, v19, v12

    .line 473
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v9, v0, :cond_13

    .line 474
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    move-object/from16 v19, v0

    sget v20, Lorg/apache/lucene/index/ByteBlockPool;->FIRST_LEVEL_SIZE:I

    invoke-virtual/range {v19 .. v20}, Lorg/apache/lucene/index/ByteBlockPool;->newSlice(I)I

    move-result v18

    .line 475
    .local v18, "upto":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    move/from16 v20, v0

    add-int v20, v20, v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/index/ByteBlockPool;->byteOffset:I

    move/from16 v21, v0

    add-int v21, v21, v18

    aput v21, v19, v20

    .line 473
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 477
    .end local v18    # "upto":I
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/ParallelPostingsArray;->byteStarts:[I

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    move/from16 v21, v0

    aget v20, v20, v21

    aput v20, v19, v12

    .line 479
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->newTerm(I)V

    .line 488
    .end local v9    # "i":I
    .end local v13    # "text":[C
    .end local v14    # "textLen1":I
    .end local v15    # "textUpto":I
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->doNextCall:Z

    move/from16 v19, v0

    if-eqz v19, :cond_a

    .line 489
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    move-object/from16 v20, v0

    aget v20, v20, v12

    invoke-virtual/range {v19 .. v20}, Lorg/apache/lucene/index/TermsHashPerField;->add(I)V

    goto/16 :goto_2

    .line 482
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/ParallelPostingsArray;->intStarts:[I

    move-object/from16 v19, v0

    aget v11, v19, v12

    .line 483
    .local v11, "intStart":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/IntBlockPool;->buffers:[[I

    move-object/from16 v19, v0

    shr-int/lit8 v20, v11, 0xd

    aget-object v19, v19, v20

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    .line 484
    and-int/lit16 v0, v11, 0x1fff

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    .line 485
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->addTerm(I)V

    goto :goto_4
.end method

.method public add(I)V
    .locals 10
    .param p1, "textStart"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, -0x1

    .line 284
    move v0, p1

    .line 286
    .local v0, "code":I
    iget v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashMask:I

    and-int v1, v0, v7

    .line 288
    .local v1, "hashPos":I
    sget-boolean v7, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v7, :cond_0

    iget-boolean v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsCompacted:Z

    if-eqz v7, :cond_0

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 291
    :cond_0
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    aget v5, v7, v1

    .line 293
    .local v5, "termID":I
    if-eq v5, v8, :cond_2

    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v7, v7, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    aget v7, v7, v5

    if-eq v7, p1, :cond_2

    .line 296
    shr-int/lit8 v7, v0, 0x8

    add-int/2addr v7, v0

    or-int/lit8 v3, v7, 0x1

    .line 298
    .local v3, "inc":I
    :cond_1
    add-int/2addr v0, v3

    .line 299
    iget v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashMask:I

    and-int v1, v0, v7

    .line 300
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    aget v5, v7, v1

    .line 301
    if-eq v5, v8, :cond_2

    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v7, v7, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    aget v7, v7, v5

    if-ne v7, p1, :cond_1

    .line 304
    .end local v3    # "inc":I
    :cond_2
    if-ne v5, v8, :cond_a

    .line 310
    iget v5, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    .end local v5    # "termID":I
    add-int/lit8 v7, v5, 0x1

    iput v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    .line 311
    .restart local v5    # "termID":I
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget v7, v7, Lorg/apache/lucene/index/ParallelPostingsArray;->size:I

    if-lt v5, v7, :cond_3

    .line 312
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashPerField;->growParallelPostingsArray()V

    .line 315
    :cond_3
    sget-boolean v7, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v7, :cond_4

    if-gez v5, :cond_4

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 317
    :cond_4
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v7, v7, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    aput p1, v7, v5

    .line 319
    sget-boolean v7, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v7, :cond_5

    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    aget v7, v7, v1

    if-eq v7, v8, :cond_5

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 320
    :cond_5
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    aput v5, v7, v1

    .line 322
    iget v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    iget v8, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashHalfSize:I

    if-ne v7, v8, :cond_6

    .line 323
    iget v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashSize:I

    mul-int/lit8 v7, v7, 0x2

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/TermsHashPerField;->rehashPostings(I)V

    .line 326
    :cond_6
    iget v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostingInt:I

    iget-object v8, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    iget v8, v8, Lorg/apache/lucene/index/IntBlockPool;->intUpto:I

    add-int/2addr v7, v8

    const/16 v8, 0x2000

    if-le v7, v8, :cond_7

    .line 327
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    invoke-virtual {v7}, Lorg/apache/lucene/index/IntBlockPool;->nextBuffer()V

    .line 329
    :cond_7
    const v7, 0x8000

    iget-object v8, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    iget v8, v8, Lorg/apache/lucene/index/ByteBlockPool;->byteUpto:I

    sub-int/2addr v7, v8

    iget v8, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostingInt:I

    sget v9, Lorg/apache/lucene/index/ByteBlockPool;->FIRST_LEVEL_SIZE:I

    mul-int/2addr v8, v9

    if-ge v7, v8, :cond_8

    .line 330
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    invoke-virtual {v7}, Lorg/apache/lucene/index/ByteBlockPool;->nextBuffer()V

    .line 332
    :cond_8
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    iget-object v7, v7, Lorg/apache/lucene/index/IntBlockPool;->buffer:[I

    iput-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    .line 333
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    iget v7, v7, Lorg/apache/lucene/index/IntBlockPool;->intUpto:I

    iput v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    .line 334
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    iget v8, v7, Lorg/apache/lucene/index/IntBlockPool;->intUpto:I

    iget v9, p0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    add-int/2addr v8, v9

    iput v8, v7, Lorg/apache/lucene/index/IntBlockPool;->intUpto:I

    .line 336
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v7, v7, Lorg/apache/lucene/index/ParallelPostingsArray;->intStarts:[I

    iget v8, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    iget-object v9, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    iget v9, v9, Lorg/apache/lucene/index/IntBlockPool;->intOffset:I

    add-int/2addr v8, v9

    aput v8, v7, v5

    .line 338
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    if-ge v2, v7, :cond_9

    .line 339
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    sget v8, Lorg/apache/lucene/index/ByteBlockPool;->FIRST_LEVEL_SIZE:I

    invoke-virtual {v7, v8}, Lorg/apache/lucene/index/ByteBlockPool;->newSlice(I)I

    move-result v6

    .line 340
    .local v6, "upto":I
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    iget v8, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    add-int/2addr v8, v2

    iget-object v9, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    iget v9, v9, Lorg/apache/lucene/index/ByteBlockPool;->byteOffset:I

    add-int/2addr v9, v6

    aput v9, v7, v8

    .line 338
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 342
    .end local v6    # "upto":I
    :cond_9
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v7, v7, Lorg/apache/lucene/index/ParallelPostingsArray;->byteStarts:[I

    iget-object v8, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    iget v9, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    aget v8, v8, v9

    aput v8, v7, v5

    .line 344
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v7, v5}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->newTerm(I)V

    .line 352
    .end local v2    # "i":I
    :goto_1
    return-void

    .line 347
    :cond_a
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v7, v7, Lorg/apache/lucene/index/ParallelPostingsArray;->intStarts:[I

    aget v4, v7, v5

    .line 348
    .local v4, "intStart":I
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    iget-object v7, v7, Lorg/apache/lucene/index/IntBlockPool;->buffers:[[I

    shr-int/lit8 v8, v4, 0xd

    aget-object v7, v7, v8

    iput-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    .line 349
    and-int/lit16 v7, v4, 0x1fff

    iput v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    .line 350
    iget-object v7, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v7, v5}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->addTerm(I)V

    goto :goto_1
.end method

.method finish()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 529
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 531
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    if-eqz v0, :cond_0

    .line 532
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashPerField;->finish()V

    .line 535
    :cond_0
    return-void

    .line 531
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    if-eqz v1, :cond_1

    .line 532
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermsHashPerField;->finish()V

    .line 531
    :cond_1
    throw v0
.end method

.method public initReader(Lorg/apache/lucene/index/ByteSliceReader;II)V
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/ByteSliceReader;
    .param p2, "termID"    # I
    .param p3, "stream"    # I

    .prologue
    .line 142
    sget-boolean v3, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    if-lt p3, v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 143
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v3, v3, Lorg/apache/lucene/index/ParallelPostingsArray;->intStarts:[I

    aget v0, v3, p2

    .line 144
    .local v0, "intStart":I
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->intPool:Lorg/apache/lucene/index/IntBlockPool;

    iget-object v3, v3, Lorg/apache/lucene/index/IntBlockPool;->buffers:[[I

    shr-int/lit8 v4, v0, 0xd

    aget-object v1, v3, v4

    .line 145
    .local v1, "ints":[I
    and-int/lit16 v2, v0, 0x1fff

    .line 146
    .local v2, "upto":I
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v4, v4, Lorg/apache/lucene/index/ParallelPostingsArray;->byteStarts:[I

    aget v4, v4, p2

    sget v5, Lorg/apache/lucene/index/ByteBlockPool;->FIRST_LEVEL_SIZE:I

    mul-int/2addr v5, p3

    add-int/2addr v4, v5

    add-int v5, v2, p3

    aget v5, v1, v5

    invoke-virtual {p1, v3, v4, v5}, Lorg/apache/lucene/index/ByteSliceReader;->init(Lorg/apache/lucene/index/ByteBlockPool;II)V

    .line 149
    return-void
.end method

.method rehashPostings(I)V
    .locals 14
    .param p1, "newSize"    # I

    .prologue
    const/4 v13, -0x1

    .line 541
    add-int/lit8 v5, p1, -0x1

    .line 543
    .local v5, "newMask":I
    new-array v4, p1, [I

    .line 544
    .local v4, "newHash":[I
    invoke-static {v4, v13}, Ljava/util/Arrays;->fill([II)V

    .line 545
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v11, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashSize:I

    if-ge v2, v11, :cond_7

    .line 546
    iget-object v11, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    aget v8, v11, v2

    .line 547
    .local v8, "termID":I
    if-eq v8, v13, :cond_6

    .line 549
    iget-object v11, p0, Lorg/apache/lucene/index/TermsHashPerField;->perThread:Lorg/apache/lucene/index/TermsHashPerThread;

    iget-boolean v11, v11, Lorg/apache/lucene/index/TermsHashPerThread;->primary:Z

    if-eqz v11, :cond_1

    .line 550
    iget-object v11, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v11, v11, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    aget v10, v11, v8

    .line 551
    .local v10, "textStart":I
    and-int/lit16 v7, v10, 0x3fff

    .line 552
    .local v7, "start":I
    iget-object v11, p0, Lorg/apache/lucene/index/TermsHashPerField;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    iget-object v11, v11, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    shr-int/lit8 v12, v10, 0xe

    aget-object v9, v11, v12

    .line 553
    .local v9, "text":[C
    move v6, v7

    .line 554
    .local v6, "pos":I
    :goto_1
    aget-char v11, v9, v6

    const v12, 0xffff

    if-eq v11, v12, :cond_0

    .line 555
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 556
    :cond_0
    const/4 v0, 0x0

    .line 557
    .local v0, "code":I
    :goto_2
    if-le v6, v7, :cond_2

    .line 558
    mul-int/lit8 v11, v0, 0x1f

    add-int/lit8 v6, v6, -0x1

    aget-char v12, v9, v6

    add-int v0, v11, v12

    goto :goto_2

    .line 560
    .end local v0    # "code":I
    .end local v6    # "pos":I
    .end local v7    # "start":I
    .end local v9    # "text":[C
    .end local v10    # "textStart":I
    :cond_1
    iget-object v11, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v11, v11, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    aget v0, v11, v8

    .line 562
    .restart local v0    # "code":I
    :cond_2
    and-int v1, v0, v5

    .line 563
    .local v1, "hashPos":I
    sget-boolean v11, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v11, :cond_3

    if-gez v1, :cond_3

    new-instance v11, Ljava/lang/AssertionError;

    invoke-direct {v11}, Ljava/lang/AssertionError;-><init>()V

    throw v11

    .line 564
    :cond_3
    aget v11, v4, v1

    if-eq v11, v13, :cond_5

    .line 565
    shr-int/lit8 v11, v0, 0x8

    add-int/2addr v11, v0

    or-int/lit8 v3, v11, 0x1

    .line 567
    .local v3, "inc":I
    :cond_4
    add-int/2addr v0, v3

    .line 568
    and-int v1, v0, v5

    .line 569
    aget v11, v4, v1

    if-ne v11, v13, :cond_4

    .line 571
    .end local v3    # "inc":I
    :cond_5
    aput v8, v4, v1

    .line 545
    .end local v0    # "code":I
    .end local v1    # "hashPos":I
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 575
    .end local v8    # "termID":I
    :cond_7
    iput v5, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashMask:I

    .line 576
    iput-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    .line 578
    iput p1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashSize:I

    .line 579
    shr-int/lit8 v11, p1, 0x1

    iput v11, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashHalfSize:I

    .line 580
    return-void
.end method

.method public reset()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 116
    iget-boolean v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsCompacted:Z

    if-nez v0, :cond_0

    .line 117
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashPerField;->compactPostings()V

    .line 118
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    array-length v1, v1

    if-le v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 119
    :cond_1
    iget v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    if-lez v0, :cond_2

    .line 120
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    iget v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    const/4 v2, -0x1

    invoke-static {v0, v3, v1, v2}, Ljava/util/Arrays;->fill([IIII)V

    .line 121
    iput v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    .line 123
    :cond_2
    iput-boolean v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsCompacted:Z

    .line 124
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    if-eqz v0, :cond_3

    .line 125
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashPerField;->reset()V

    .line 126
    :cond_3
    return-void
.end method

.method shrinkHash(I)V
    .locals 10
    .param p1, "targetSize"    # I

    .prologue
    const-wide/16 v8, 0x4

    const/4 v6, 0x4

    .line 95
    sget-boolean v1, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsCompacted:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 97
    :cond_0
    const/4 v0, 0x4

    .line 98
    .local v0, "newSize":I
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    array-length v1, v1

    if-eq v6, v1, :cond_1

    .line 99
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    array-length v1, v1

    int-to-long v2, v1

    .line 100
    .local v2, "previousSize":J
    new-array v1, v6, [I

    iput-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    .line 101
    sub-long v4, v8, v2

    mul-long/2addr v4, v8

    invoke-direct {p0, v4, v5}, Lorg/apache/lucene/index/TermsHashPerField;->bytesUsed(J)V

    .line 102
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    const/4 v4, -0x1

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([II)V

    .line 103
    iput v6, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashSize:I

    .line 104
    const/4 v1, 0x2

    iput v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashHalfSize:I

    .line 105
    const/4 v1, 0x3

    iput v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHashMask:I

    .line 109
    .end local v2    # "previousSize":J
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    if-eqz v1, :cond_2

    .line 110
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    invoke-virtual {v1}, Lorg/apache/lucene/index/ParallelPostingsArray;->bytesPerPosting()I

    move-result v1

    neg-int v1, v1

    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget v4, v4, Lorg/apache/lucene/index/ParallelPostingsArray;->size:I

    mul-int/2addr v1, v4

    int-to-long v4, v1

    invoke-direct {p0, v4, v5}, Lorg/apache/lucene/index/TermsHashPerField;->bytesUsed(J)V

    .line 111
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    .line 113
    :cond_2
    return-void
.end method

.method public sortPostings()[I
    .locals 4

    .prologue
    .line 169
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashPerField;->compactPostings()V

    .line 170
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsHash:[I

    .line 171
    .local v0, "postingsHash":[I
    new-instance v1, Lorg/apache/lucene/index/TermsHashPerField$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/index/TermsHashPerField$1;-><init>(Lorg/apache/lucene/index/TermsHashPerField;[I)V

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/index/TermsHashPerField$1;->quickSort(II)V

    .line 237
    return-object v0
.end method

.method start(Lorg/apache/lucene/document/Fieldable;)V
    .locals 2
    .param p1, "f"    # Lorg/apache/lucene/document/Fieldable;

    .prologue
    .line 261
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 262
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->start(Lorg/apache/lucene/document/Fieldable;)V

    .line 263
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/TermsHashPerField;->start(Lorg/apache/lucene/document/Fieldable;)V

    .line 266
    :cond_0
    return-void
.end method

.method start([Lorg/apache/lucene/document/Fieldable;I)Z
    .locals 1
    .param p1, "fields"    # [Lorg/apache/lucene/document/Fieldable;
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 270
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->start([Lorg/apache/lucene/document/Fieldable;I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->doCall:Z

    .line 271
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    if-nez v0, :cond_0

    .line 272
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashPerField;->initPostingsArray()V

    .line 275
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/TermsHashPerField;->start([Lorg/apache/lucene/document/Fieldable;I)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->doNextCall:Z

    .line 277
    :cond_1
    iget-boolean v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->doCall:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->doNextCall:Z

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method writeByte(IB)V
    .locals 6
    .param p1, "stream"    # I
    .param p2, "b"    # B

    .prologue
    .line 496
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    iget v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    add-int/2addr v4, p1

    aget v2, v3, v4

    .line 497
    .local v2, "upto":I
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    iget-object v3, v3, Lorg/apache/lucene/index/ByteBlockPool;->buffers:[[B

    shr-int/lit8 v4, v2, 0xf

    aget-object v0, v3, v4

    .line 498
    .local v0, "bytes":[B
    sget-boolean v3, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-nez v0, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 499
    :cond_0
    and-int/lit16 v1, v2, 0x7fff

    .line 500
    .local v1, "offset":I
    aget-byte v3, v0, v1

    if-eqz v3, :cond_1

    .line 502
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    invoke-virtual {v3, v0, v1}, Lorg/apache/lucene/index/ByteBlockPool;->allocSlice([BI)I

    move-result v1

    .line 503
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    iget-object v0, v3, Lorg/apache/lucene/index/ByteBlockPool;->buffer:[B

    .line 504
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    iget v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    add-int/2addr v4, p1

    iget-object v5, p0, Lorg/apache/lucene/index/TermsHashPerField;->bytePool:Lorg/apache/lucene/index/ByteBlockPool;

    iget v5, v5, Lorg/apache/lucene/index/ByteBlockPool;->byteOffset:I

    add-int/2addr v5, v1

    aput v5, v3, v4

    .line 506
    :cond_1
    aput-byte p2, v0, v1

    .line 507
    iget-object v3, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptos:[I

    iget v4, p0, Lorg/apache/lucene/index/TermsHashPerField;->intUptoStart:I

    add-int/2addr v4, p1

    aget v5, v3, v4

    add-int/lit8 v5, v5, 0x1

    aput v5, v3, v4

    .line 508
    return-void
.end method

.method public writeBytes(I[BII)V
    .locals 3
    .param p1, "stream"    # I
    .param p2, "b"    # [B
    .param p3, "offset"    # I
    .param p4, "len"    # I

    .prologue
    .line 512
    add-int v0, p3, p4

    .line 513
    .local v0, "end":I
    move v1, p3

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 514
    aget-byte v2, p2, v1

    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/index/TermsHashPerField;->writeByte(IB)V

    .line 513
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 515
    :cond_0
    return-void
.end method

.method writeVInt(II)V
    .locals 1
    .param p1, "stream"    # I
    .param p2, "i"    # I

    .prologue
    .line 518
    sget-boolean v0, Lorg/apache/lucene/index/TermsHashPerField;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/TermsHashPerField;->streamCount:I

    if-lt p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 519
    :cond_0
    :goto_0
    and-int/lit8 v0, p2, -0x80

    if-eqz v0, :cond_1

    .line 520
    and-int/lit8 v0, p2, 0x7f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/TermsHashPerField;->writeByte(IB)V

    .line 521
    ushr-int/lit8 p2, p2, 0x7

    goto :goto_0

    .line 523
    :cond_1
    int-to-byte v0, p2

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/TermsHashPerField;->writeByte(IB)V

    .line 524
    return-void
.end method
