.class public final Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;
.super Ljava/lang/Object;
.source "CheckIndex.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/CheckIndex$Status;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TermIndexStatus"
.end annotation


# instance fields
.field public error:Ljava/lang/Throwable;

.field public termCount:J

.field public totFreq:J

.field public totPos:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215
    iput-wide v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    .line 218
    iput-wide v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totFreq:J

    .line 221
    iput-wide v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totPos:J

    .line 224
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->error:Ljava/lang/Throwable;

    return-void
.end method
