.class abstract Lorg/apache/lucene/index/MultiLevelSkipListReader;
.super Ljava/lang/Object;
.source "MultiLevelSkipListReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/MultiLevelSkipListReader$SkipBuffer;
    }
.end annotation


# instance fields
.field private childPointer:[J

.field private docCount:I

.field private haveSkipped:Z

.field private inputIsBuffered:Z

.field private lastChildPointer:J

.field private lastDoc:I

.field private maxNumberOfSkipLevels:I

.field private numSkipped:[I

.field private numberOfLevelsToBuffer:I

.field private numberOfSkipLevels:I

.field private skipDoc:[I

.field private skipInterval:[I

.field private skipPointer:[J

.field private skipStream:[Lorg/apache/lucene/store/IndexInput;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/IndexInput;II)V
    .locals 4
    .param p1, "skipStream"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "maxSkipLevels"    # I
    .param p3, "skipInterval"    # I

    .prologue
    const/4 v2, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numberOfLevelsToBuffer:I

    .line 67
    new-array v1, p2, [Lorg/apache/lucene/store/IndexInput;

    iput-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    .line 68
    new-array v1, p2, [J

    iput-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipPointer:[J

    .line 69
    new-array v1, p2, [J

    iput-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->childPointer:[J

    .line 70
    new-array v1, p2, [I

    iput-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numSkipped:[I

    .line 71
    iput p2, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->maxNumberOfSkipLevels:I

    .line 72
    new-array v1, p2, [I

    iput-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipInterval:[I

    .line 73
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aput-object p1, v1, v2

    .line 74
    instance-of v1, p1, Lorg/apache/lucene/store/BufferedIndexInput;

    iput-boolean v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->inputIsBuffered:Z

    .line 75
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipInterval:[I

    aput p3, v1, v2

    .line 76
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 78
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipInterval:[I

    iget-object v2, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipInterval:[I

    add-int/lit8 v3, v0, -0x1

    aget v2, v2, v3

    mul-int/2addr v2, p3

    aput v2, v1, v0

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_0
    new-array v1, p2, [I

    iput-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipDoc:[I

    .line 81
    return-void
.end method

.method private loadNextSkip(I)Z
    .locals 6
    .param p1, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MultiLevelSkipListReader;->setLastSkipData(I)V

    .line 130
    iget-object v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numSkipped:[I

    aget v1, v0, p1

    iget-object v2, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipInterval:[I

    aget v2, v2, p1

    add-int/2addr v1, v2

    aput v1, v0, p1

    .line 132
    iget-object v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numSkipped:[I

    aget v0, v0, p1

    iget v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->docCount:I

    if-le v0, v1, :cond_1

    .line 134
    iget-object v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipDoc:[I

    const v1, 0x7fffffff

    aput v1, v0, p1

    .line 135
    iget v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numberOfSkipLevels:I

    if-le v0, p1, :cond_0

    iput p1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numberOfSkipLevels:I

    .line 136
    :cond_0
    const/4 v0, 0x0

    .line 147
    :goto_0
    return v0

    .line 140
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipDoc:[I

    aget v1, v0, p1

    iget-object v2, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v2, v2, p1

    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/index/MultiLevelSkipListReader;->readSkipData(ILorg/apache/lucene/store/IndexInput;)I

    move-result v2

    add-int/2addr v1, v2

    aput v1, v0, p1

    .line 142
    if-eqz p1, :cond_2

    .line 144
    iget-object v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->childPointer:[J

    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v2

    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipPointer:[J

    add-int/lit8 v4, p1, -0x1

    aget-wide v4, v1, v4

    add-long/2addr v2, v4

    aput-wide v2, v0, p1

    .line 147
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private loadSkipLevels()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 185
    iget v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->docCount:I

    if-nez v4, :cond_1

    move v4, v5

    :goto_0
    iput v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numberOfSkipLevels:I

    .line 186
    iget v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numberOfSkipLevels:I

    iget v6, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->maxNumberOfSkipLevels:I

    if-le v4, v6, :cond_0

    .line 187
    iget v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->maxNumberOfSkipLevels:I

    iput v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numberOfSkipLevels:I

    .line 190
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v4, v4, v5

    iget-object v6, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipPointer:[J

    aget-wide v6, v6, v5

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 192
    iget v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numberOfLevelsToBuffer:I

    .line 194
    .local v1, "toBuffer":I
    iget v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numberOfSkipLevels:I

    add-int/lit8 v0, v4, -0x1

    .local v0, "i":I
    :goto_1
    if-lez v0, :cond_4

    .line 196
    iget-object v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v2

    .line 199
    .local v2, "length":J
    iget-object v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipPointer:[J

    iget-object v6, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v6, v6, v5

    invoke-virtual {v6}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    aput-wide v6, v4, v0

    .line 200
    if-lez v1, :cond_2

    .line 202
    iget-object v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    new-instance v6, Lorg/apache/lucene/index/MultiLevelSkipListReader$SkipBuffer;

    iget-object v7, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v7, v7, v5

    long-to-int v8, v2

    invoke-direct {v6, v7, v8}, Lorg/apache/lucene/index/MultiLevelSkipListReader$SkipBuffer;-><init>(Lorg/apache/lucene/store/IndexInput;I)V

    aput-object v6, v4, v0

    .line 203
    add-int/lit8 v1, v1, -0x1

    .line 194
    :goto_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 185
    .end local v0    # "i":I
    .end local v1    # "toBuffer":I
    .end local v2    # "length":J
    :cond_1
    iget v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->docCount:I

    int-to-double v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    iget-object v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipInterval:[I

    aget v4, v4, v5

    int-to-double v8, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->log(D)D

    move-result-wide v8

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v4, v6

    goto :goto_0

    .line 206
    .restart local v0    # "i":I
    .restart local v1    # "toBuffer":I
    .restart local v2    # "length":J
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    iget-object v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/store/IndexInput;

    aput-object v4, v6, v0

    .line 207
    iget-boolean v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->inputIsBuffered:Z

    if-eqz v4, :cond_3

    const-wide/16 v6, 0x400

    cmp-long v4, v2, v6

    if-gez v4, :cond_3

    .line 208
    iget-object v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v4, v4, v0

    check-cast v4, Lorg/apache/lucene/store/BufferedIndexInput;

    long-to-int v6, v2

    invoke-virtual {v4, v6}, Lorg/apache/lucene/store/BufferedIndexInput;->setBufferSize(I)V

    .line 212
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v4, v4, v5

    iget-object v6, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v6, v6, v5

    invoke-virtual {v6}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    add-long/2addr v6, v2

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto :goto_2

    .line 217
    .end local v2    # "length":J
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipPointer:[J

    iget-object v6, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v6, v6, v5

    invoke-virtual {v6}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    aput-wide v6, v4, v5

    .line 218
    return-void
.end method


# virtual methods
.method close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 163
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 164
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 162
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 167
    :cond_1
    return-void
.end method

.method getDoc()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->lastDoc:I

    return v0
.end method

.method init(JI)V
    .locals 5
    .param p1, "skipPointer"    # J
    .param p3, "df"    # I

    .prologue
    const/4 v4, 0x0

    .line 171
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipPointer:[J

    aput-wide p1, v1, v4

    .line 172
    iput p3, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->docCount:I

    .line 173
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipDoc:[I

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([II)V

    .line 174
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numSkipped:[I

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([II)V

    .line 175
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->childPointer:[J

    const-wide/16 v2, 0x0

    invoke-static {v1, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 177
    iput-boolean v4, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->haveSkipped:Z

    .line 178
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numberOfSkipLevels:I

    if-ge v0, v1, :cond_0

    .line 179
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_0
    return-void
.end method

.method protected abstract readSkipData(ILorg/apache/lucene/store/IndexInput;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected seekChild(I)V
    .locals 6
    .param p1, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v0, v0, p1

    iget-wide v2, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->lastChildPointer:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 154
    iget-object v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numSkipped:[I

    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numSkipped:[I

    add-int/lit8 v2, p1, 0x1

    aget v1, v1, v2

    iget-object v2, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipInterval:[I

    add-int/lit8 v3, p1, 0x1

    aget v2, v2, v3

    sub-int/2addr v1, v2

    aput v1, v0, p1

    .line 155
    iget-object v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipDoc:[I

    iget v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->lastDoc:I

    aput v1, v0, p1

    .line 156
    if-lez p1, :cond_0

    .line 157
    iget-object v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->childPointer:[J

    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v2

    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipPointer:[J

    add-int/lit8 v4, p1, -0x1

    aget-wide v4, v1, v4

    add-long/2addr v2, v4

    aput-wide v2, v0, p1

    .line 159
    :cond_0
    return-void
.end method

.method protected setLastSkipData(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 230
    iget-object v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipDoc:[I

    aget v0, v0, p1

    iput v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->lastDoc:I

    .line 231
    iget-object v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->childPointer:[J

    aget-wide v0, v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->lastChildPointer:J

    .line 232
    return-void
.end method

.method skipTo(I)I
    .locals 7
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 95
    iget-boolean v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->haveSkipped:Z

    if-nez v1, :cond_0

    .line 97
    invoke-direct {p0}, Lorg/apache/lucene/index/MultiLevelSkipListReader;->loadSkipLevels()V

    .line 98
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->haveSkipped:Z

    .line 103
    :cond_0
    const/4 v0, 0x0

    .line 104
    .local v0, "level":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numberOfSkipLevels:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipDoc:[I

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    if-le p1, v1, :cond_1

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_1
    :goto_1
    if-ltz v0, :cond_4

    .line 109
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipDoc:[I

    aget v1, v1, v0

    if-le p1, v1, :cond_2

    .line 110
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/MultiLevelSkipListReader;->loadNextSkip(I)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_1

    .line 115
    :cond_2
    if-lez v0, :cond_3

    iget-wide v2, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->lastChildPointer:J

    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    add-int/lit8 v4, v0, -0x1

    aget-object v1, v1, v4

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    .line 116
    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/MultiLevelSkipListReader;->seekChild(I)V

    .line 118
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 122
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->numSkipped:[I

    aget v1, v1, v6

    iget-object v2, p0, Lorg/apache/lucene/index/MultiLevelSkipListReader;->skipInterval:[I

    aget v2, v2, v6

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    return v1
.end method
