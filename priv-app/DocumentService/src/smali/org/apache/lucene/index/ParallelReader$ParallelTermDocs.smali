.class Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;
.super Ljava/lang/Object;
.source "ParallelReader.java"

# interfaces
.implements Lorg/apache/lucene/index/TermDocs;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/ParallelReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ParallelTermDocs"
.end annotation


# instance fields
.field protected termDocs:Lorg/apache/lucene/index/TermDocs;

.field final synthetic this$0:Lorg/apache/lucene/index/ParallelReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/ParallelReader;)V
    .locals 0

    .prologue
    .line 577
    iput-object p1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->this$0:Lorg/apache/lucene/index/ParallelReader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/ParallelReader;Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 578
    iput-object p1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->this$0:Lorg/apache/lucene/index/ParallelReader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 579
    if-nez p2, :cond_1

    .line 580
    # getter for: Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;
    invoke-static {p1}, Lorg/apache/lucene/index/ParallelReader;->access$100(Lorg/apache/lucene/index/ParallelReader;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->termDocs:Lorg/apache/lucene/index/TermDocs;

    .line 583
    :goto_1
    return-void

    .line 580
    :cond_0
    # getter for: Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;
    invoke-static {p1}, Lorg/apache/lucene/index/ParallelReader;->access$100(Lorg/apache/lucene/index/ParallelReader;)Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    goto :goto_0

    .line 582
    :cond_1
    invoke-virtual {p0, p2}, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->seek(Lorg/apache/lucene/index/Term;)V

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 619
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->termDocs:Lorg/apache/lucene/index/TermDocs;

    if-eqz v0, :cond_0

    .line 620
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 621
    :cond_0
    return-void
.end method

.method public doc()I
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v0

    return v0
.end method

.method public freq()I
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->freq()I

    move-result v0

    return v0
.end method

.method public next()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 598
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->termDocs:Lorg/apache/lucene/index/TermDocs;

    if-nez v0, :cond_0

    .line 599
    const/4 v0, 0x0

    .line 601
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v0

    goto :goto_0
.end method

.method public read([I[I)I
    .locals 1
    .param p1, "docs"    # [I
    .param p2, "freqs"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 605
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->termDocs:Lorg/apache/lucene/index/TermDocs;

    if-nez v0, :cond_0

    .line 606
    const/4 v0, 0x0

    .line 608
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0, p1, p2}, Lorg/apache/lucene/index/TermDocs;->read([I[I)I

    move-result v0

    goto :goto_0
.end method

.method public seek(Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 589
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->this$0:Lorg/apache/lucene/index/ParallelReader;

    # getter for: Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;
    invoke-static {v1}, Lorg/apache/lucene/index/ParallelReader;->access$000(Lorg/apache/lucene/index/ParallelReader;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;

    .line 590
    .local v0, "reader":Lorg/apache/lucene/index/IndexReader;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->termDocs:Lorg/apache/lucene/index/TermDocs;

    .line 591
    return-void

    .line 590
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public seek(Lorg/apache/lucene/index/TermEnum;)V
    .locals 1
    .param p1, "termEnum"    # Lorg/apache/lucene/index/TermEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 594
    invoke-virtual {p1}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->seek(Lorg/apache/lucene/index/Term;)V

    .line 595
    return-void
.end method

.method public skipTo(I)Z
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 612
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->termDocs:Lorg/apache/lucene/index/TermDocs;

    if-nez v0, :cond_0

    .line 613
    const/4 v0, 0x0

    .line 615
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0, p1}, Lorg/apache/lucene/index/TermDocs;->skipTo(I)Z

    move-result v0

    goto :goto_0
.end method
