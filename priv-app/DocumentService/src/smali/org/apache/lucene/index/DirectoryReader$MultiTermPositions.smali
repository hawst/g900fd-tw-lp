.class Lorg/apache/lucene/index/DirectoryReader$MultiTermPositions;
.super Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;
.source "DirectoryReader.java"

# interfaces
.implements Lorg/apache/lucene/index/TermPositions;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DirectoryReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MultiTermPositions"
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[I)V
    .locals 0
    .param p1, "topReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "r"    # [Lorg/apache/lucene/index/IndexReader;
    .param p3, "s"    # [I

    .prologue
    .line 1307
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;-><init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[I)V

    .line 1308
    return-void
.end method


# virtual methods
.method public getPayload([BI)[B
    .locals 1
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1324
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermPositions;->current:Lorg/apache/lucene/index/TermDocs;

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0, p1, p2}, Lorg/apache/lucene/index/TermPositions;->getPayload([BI)[B

    move-result-object v0

    return-object v0
.end method

.method public getPayloadLength()I
    .locals 1

    .prologue
    .line 1320
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermPositions;->current:Lorg/apache/lucene/index/TermDocs;

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->getPayloadLength()I

    move-result v0

    return v0
.end method

.method public isPayloadAvailable()Z
    .locals 1

    .prologue
    .line 1330
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermPositions;->current:Lorg/apache/lucene/index/TermDocs;

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->isPayloadAvailable()Z

    move-result v0

    return v0
.end method

.method public nextPosition()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1316
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermPositions;->current:Lorg/apache/lucene/index/TermDocs;

    check-cast v0, Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->nextPosition()I

    move-result v0

    return v0
.end method

.method protected termDocs(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/TermDocs;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1312
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->termPositions()Lorg/apache/lucene/index/TermPositions;

    move-result-object v0

    return-object v0
.end method
