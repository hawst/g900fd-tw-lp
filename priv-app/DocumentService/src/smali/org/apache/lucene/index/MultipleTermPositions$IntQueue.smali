.class final Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;
.super Ljava/lang/Object;
.source "MultipleTermPositions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MultipleTermPositions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "IntQueue"
.end annotation


# instance fields
.field private _array:[I

.field private _arraySize:I

.field private _index:I

.field private _lastIndex:I


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/16 v0, 0x10

    iput v0, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_arraySize:I

    .line 57
    iput v1, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_index:I

    .line 58
    iput v1, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_lastIndex:I

    .line 59
    iget v0, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_arraySize:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_array:[I

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/index/MultipleTermPositions$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/index/MultipleTermPositions$1;

    .prologue
    .line 55
    invoke-direct {p0}, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;-><init>()V

    return-void
.end method

.method private growArray()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_array:[I

    iget v1, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_arraySize:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_array:[I

    .line 87
    iget-object v0, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_array:[I

    array-length v0, v0

    iput v0, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_arraySize:I

    .line 88
    return-void
.end method


# virtual methods
.method final add(I)V
    .locals 3
    .param p1, "i"    # I

    .prologue
    .line 62
    iget v0, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_lastIndex:I

    iget v1, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_arraySize:I

    if-ne v0, v1, :cond_0

    .line 63
    invoke-direct {p0}, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->growArray()V

    .line 65
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_array:[I

    iget v1, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_lastIndex:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_lastIndex:I

    aput p1, v0, v1

    .line 66
    return-void
.end method

.method final clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 77
    iput v0, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_index:I

    .line 78
    iput v0, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_lastIndex:I

    .line 79
    return-void
.end method

.method final next()I
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_array:[I

    iget v1, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_index:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_index:I

    aget v0, v0, v1

    return v0
.end method

.method final size()I
    .locals 2

    .prologue
    .line 82
    iget v0, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_lastIndex:I

    iget v1, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_index:I

    sub-int/2addr v0, v1

    return v0
.end method

.method final sort()V
    .locals 3

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_array:[I

    iget v1, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_index:I

    iget v2, p0, Lorg/apache/lucene/index/MultipleTermPositions$IntQueue;->_lastIndex:I

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->sort([III)V

    .line 74
    return-void
.end method
