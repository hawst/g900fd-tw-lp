.class final Lorg/apache/lucene/index/StoredFieldsWriter;
.super Ljava/lang/Object;
.source "StoredFieldsWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field allocCount:I

.field docFreeList:[Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

.field final docWriter:Lorg/apache/lucene/index/DocumentsWriter;

.field final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field fieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

.field freeCount:I

.field lastDocID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lorg/apache/lucene/index/StoredFieldsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/StoredFieldsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriter;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 1
    .param p1, "docWriter"    # Lorg/apache/lucene/index/DocumentsWriter;
    .param p2, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->docFreeList:[Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    .line 37
    iput-object p1, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 38
    iput-object p2, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 39
    return-void
.end method

.method private declared-synchronized initFieldsWriter()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lorg/apache/lucene/index/FieldsWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->getSegment()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/index/FieldsWriter;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;)V

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    .line 65
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->lastDocID:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    :cond_0
    monitor-exit p0

    return-void

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method declared-synchronized abort()V
    .locals 1

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldsWriter;->abort()V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    .line 91
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->lastDocID:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :cond_0
    monitor-exit p0

    return-void

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public addThread(Lorg/apache/lucene/index/DocumentsWriter$DocState;)Lorg/apache/lucene/index/StoredFieldsWriterPerThread;
    .locals 1
    .param p1, "docState"    # Lorg/apache/lucene/index/DocumentsWriter$DocState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;

    invoke-direct {v0, p1, p0}, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;-><init>(Lorg/apache/lucene/index/DocumentsWriter$DocState;Lorg/apache/lucene/index/StoredFieldsWriter;)V

    return-object v0
.end method

.method fill(I)V
    .locals 1
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    :goto_0
    iget v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->lastDocID:I

    if-ge v0, p1, :cond_0

    .line 100
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldsWriter;->skipDocument()V

    .line 101
    iget v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->lastDocID:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->lastDocID:I

    goto :goto_0

    .line 103
    :cond_0
    return-void
.end method

.method declared-synchronized finishDocument(Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;)V
    .locals 3
    .param p1, "perDoc"    # Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/StoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    const-string/jumbo v1, "StoredFieldsWriter.finishDocument start"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 107
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lorg/apache/lucene/index/StoredFieldsWriter;->initFieldsWriter()V

    .line 109
    iget v0, p1, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->docID:I

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/StoredFieldsWriter;->fill(I)V

    .line 112
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    iget v1, p1, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->numStoredFields:I

    iget-object v2, p1, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->fdt:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/FieldsWriter;->flushDocument(ILorg/apache/lucene/store/RAMOutputStream;)V

    .line 113
    iget v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->lastDocID:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->lastDocID:I

    .line 114
    invoke-virtual {p1}, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->reset()V

    .line 115
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/StoredFieldsWriter;->free(Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;)V

    .line 116
    sget-boolean v0, Lorg/apache/lucene/index/StoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    const-string/jumbo v1, "StoredFieldsWriter.finishDocument end"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 117
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized flush(Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->numDocs:I

    iget v1, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->lastDocID:I

    if-le v0, v1, :cond_0

    .line 47
    invoke-direct {p0}, Lorg/apache/lucene/index/StoredFieldsWriter;->initFieldsWriter()V

    .line 48
    iget v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->numDocs:I

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/StoredFieldsWriter;->fill(I)V

    .line 51
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldsWriter:Lorg/apache/lucene/index/FieldsWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_1

    .line 53
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    iget v1, p1, Lorg/apache/lucene/index/SegmentWriteState;->numDocs:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldsWriter;->finish(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldsWriter;->close()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->lastDocID:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 60
    :cond_1
    monitor-exit p0

    return-void

    .line 55
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldsWriter;->close()V

    .line 56
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    .line 57
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->lastDocID:I

    .line 55
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 46
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized free(Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;)V
    .locals 4
    .param p1, "perDoc"    # Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    .prologue
    const-wide/16 v2, 0x0

    .line 120
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/StoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->freeCount:I

    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->docFreeList:[Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 121
    :cond_0
    :try_start_1
    sget-boolean v0, Lorg/apache/lucene/index/StoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p1, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->numStoredFields:I

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 122
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/index/StoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p1, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->fdt:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMOutputStream;->length()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 123
    :cond_2
    sget-boolean v0, Lorg/apache/lucene/index/StoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p1, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->fdt:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMOutputStream;->getFilePointer()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 124
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->docFreeList:[Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    iget v1, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->freeCount:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->freeCount:I

    aput-object p1, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 125
    monitor-exit p0

    return-void
.end method

.method declared-synchronized getPerDoc()Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;
    .locals 2

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->freeCount:I

    if-nez v0, :cond_2

    .line 73
    iget v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->allocCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->allocCount:I

    .line 74
    iget v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->allocCount:I

    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->docFreeList:[Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    array-length v1, v1

    if-le v0, v1, :cond_1

    .line 78
    sget-boolean v0, Lorg/apache/lucene/index/StoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->allocCount:I

    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->docFreeList:[Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 79
    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->allocCount:I

    sget v1, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    new-array v0, v0, [Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->docFreeList:[Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    .line 81
    :cond_1
    new-instance v0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;-><init>(Lorg/apache/lucene/index/StoredFieldsWriter;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_2
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->docFreeList:[Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    iget v1, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->freeCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/index/StoredFieldsWriter;->freeCount:I

    aget-object v0, v0, v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
