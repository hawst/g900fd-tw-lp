.class public final Lorg/apache/lucene/index/NoDeletionPolicy;
.super Ljava/lang/Object;
.source "NoDeletionPolicy.java"

# interfaces
.implements Lorg/apache/lucene/index/IndexDeletionPolicy;


# static fields
.field public static final INSTANCE:Lorg/apache/lucene/index/IndexDeletionPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lorg/apache/lucene/index/NoDeletionPolicy;

    invoke-direct {v0}, Lorg/apache/lucene/index/NoDeletionPolicy;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/NoDeletionPolicy;->INSTANCE:Lorg/apache/lucene/index/IndexDeletionPolicy;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method


# virtual methods
.method public onCommit(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    return-void
.end method

.method public onInit(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    return-void
.end method
