.class public final Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;
.super Ljava/lang/Object;
.source "IndexWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/IndexWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MaxFieldLength"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final LIMITED:Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;

.field public static final UNLIMITED:Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;


# instance fields
.field private limit:I

.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 4664
    new-instance v0, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;

    const-string/jumbo v1, "UNLIMITED"

    const v2, 0x7fffffff

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;->UNLIMITED:Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;

    .line 4671
    new-instance v0, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;

    const-string/jumbo v1, "LIMITED"

    const/16 v2, 0x2710

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;->LIMITED:Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "limit"    # I

    .prologue
    .line 4650
    const-string/jumbo v0, "User-specified"

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;-><init>(Ljava/lang/String;I)V

    .line 4651
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "limit"    # I

    .prologue
    .line 4639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4640
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;->name:Ljava/lang/String;

    .line 4641
    iput p2, p0, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;->limit:I

    .line 4642
    return-void
.end method


# virtual methods
.method public getLimit()I
    .locals 1

    .prologue
    .line 4654
    iget v0, p0, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;->limit:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4660
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;->limit:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
