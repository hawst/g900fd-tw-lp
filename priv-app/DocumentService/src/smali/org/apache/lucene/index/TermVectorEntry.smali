.class public Lorg/apache/lucene/index/TermVectorEntry;
.super Ljava/lang/Object;
.source "TermVectorEntry.java"


# instance fields
.field private field:Ljava/lang/String;

.field private frequency:I

.field private offsets:[Lorg/apache/lucene/index/TermVectorOffsetInfo;

.field positions:[I

.field private term:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I[Lorg/apache/lucene/index/TermVectorOffsetInfo;[I)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "term"    # Ljava/lang/String;
    .param p3, "frequency"    # I
    .param p4, "offsets"    # [Lorg/apache/lucene/index/TermVectorOffsetInfo;
    .param p5, "positions"    # [I

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lorg/apache/lucene/index/TermVectorEntry;->field:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lorg/apache/lucene/index/TermVectorEntry;->term:Ljava/lang/String;

    .line 36
    iput p3, p0, Lorg/apache/lucene/index/TermVectorEntry;->frequency:I

    .line 37
    iput-object p4, p0, Lorg/apache/lucene/index/TermVectorEntry;->offsets:[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    .line 38
    iput-object p5, p0, Lorg/apache/lucene/index/TermVectorEntry;->positions:[I

    .line 39
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78
    if-ne p0, p1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v1

    .line 79
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 81
    check-cast v0, Lorg/apache/lucene/index/TermVectorEntry;

    .line 83
    .local v0, "that":Lorg/apache/lucene/index/TermVectorEntry;
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorEntry;->term:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorEntry;->term:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/index/TermVectorEntry;->term:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, v0, Lorg/apache/lucene/index/TermVectorEntry;->term:Ljava/lang/String;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorEntry;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getFrequency()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lorg/apache/lucene/index/TermVectorEntry;->frequency:I

    return v0
.end method

.method public getOffsets()[Lorg/apache/lucene/index/TermVectorOffsetInfo;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorEntry;->offsets:[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    return-object v0
.end method

.method public getPositions()[I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorEntry;->positions:[I

    return-object v0
.end method

.method public getTerm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorEntry;->term:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorEntry;->term:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorEntry;->term:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setFrequency(I)V
    .locals 0
    .param p1, "frequency"    # I

    .prologue
    .line 64
    iput p1, p0, Lorg/apache/lucene/index/TermVectorEntry;->frequency:I

    .line 65
    return-void
.end method

.method setOffsets([Lorg/apache/lucene/index/TermVectorOffsetInfo;)V
    .locals 0
    .param p1, "offsets"    # [Lorg/apache/lucene/index/TermVectorOffsetInfo;

    .prologue
    .line 68
    iput-object p1, p0, Lorg/apache/lucene/index/TermVectorEntry;->offsets:[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    .line 69
    return-void
.end method

.method setPositions([I)V
    .locals 0
    .param p1, "positions"    # [I

    .prologue
    .line 72
    iput-object p1, p0, Lorg/apache/lucene/index/TermVectorEntry;->positions:[I

    .line 73
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "TermVectorEntry{field=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorEntry;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", term=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorEntry;->term:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", frequency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/index/TermVectorEntry;->frequency:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
