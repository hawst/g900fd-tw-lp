.class Lorg/apache/lucene/index/SegmentTermDocs;
.super Ljava/lang/Object;
.source "SegmentTermDocs.java"

# interfaces
.implements Lorg/apache/lucene/index/TermDocs;


# instance fields
.field protected count:I

.field protected currentFieldStoresPayloads:Z

.field protected deletedDocs:Lorg/apache/lucene/util/BitVector;

.field protected df:I

.field doc:I

.field freq:I

.field private freqBasePointer:J

.field protected freqStream:Lorg/apache/lucene/store/IndexInput;

.field private haveSkipped:Z

.field protected indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

.field private maxSkipLevels:I

.field protected parent:Lorg/apache/lucene/index/SegmentReader;

.field private proxBasePointer:J

.field private skipInterval:I

.field private skipListReader:Lorg/apache/lucene/index/DefaultSkipListReader;

.field private skipPointer:J


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/index/SegmentReader;)V
    .locals 1
    .param p1, "parent"    # Lorg/apache/lucene/index/SegmentReader;

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/SegmentTermDocs;-><init>(Lorg/apache/lucene/index/SegmentReader;Z)V

    .line 63
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/index/SegmentReader;Z)V
    .locals 1
    .param p1, "parent"    # Lorg/apache/lucene/index/SegmentReader;
    .param p2, "raw"    # Z

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    .line 48
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->parent:Lorg/apache/lucene/index/SegmentReader;

    .line 49
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentCoreReaders;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/IndexInput;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    .line 50
    if-nez p2, :cond_0

    .line 51
    monitor-enter p1

    .line 52
    :try_start_0
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentTermDocs;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    .line 53
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :goto_0
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentCoreReaders;->getTermsReader()Lorg/apache/lucene/index/TermInfosReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermInfosReader;->getSkipInterval()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipInterval:I

    .line 58
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentCoreReaders;->getTermsReader()Lorg/apache/lucene/index/TermInfosReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermInfosReader;->getMaxSkipLevels()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/SegmentTermDocs;->maxSkipLevels:I

    .line 59
    return-void

    .line 53
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 55
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentTermDocs;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    goto :goto_0
.end method

.method private final readNoTf([I[II)I
    .locals 3
    .param p1, "docs"    # [I
    .param p2, "freqs"    # [I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    const/4 v0, 0x0

    .line 173
    .local v0, "i":I
    :cond_0
    :goto_0
    if-ge v0, p3, :cond_2

    iget v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->count:I

    iget v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->df:I

    if-ge v1, v2, :cond_2

    .line 175
    iget v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    .line 176
    iget v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->count:I

    .line 178
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    iget v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/BitVector;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 179
    :cond_1
    iget v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    aput v1, p1, v0

    .line 182
    const/4 v1, 0x1

    aput v1, p2, v0

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 186
    :cond_2
    return v0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 107
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/index/DefaultSkipListReader;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/index/DefaultSkipListReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DefaultSkipListReader;->close()V

    .line 109
    :cond_0
    return-void
.end method

.method public final doc()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    return v0
.end method

.method public final freq()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freq:I

    return v0
.end method

.method public next()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 119
    :goto_0
    iget v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->count:I

    iget v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->df:I

    if-ne v2, v3, :cond_1

    .line 120
    const/4 v1, 0x0

    .line 140
    :cond_0
    return v1

    .line 121
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 123
    .local v0, "docCode":I
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v2, v3, :cond_2

    .line 124
    iget v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    add-int/2addr v2, v0

    iput v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    .line 125
    iput v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freq:I

    .line 134
    :goto_1
    iget v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->count:I

    .line 136
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    iget v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/BitVector;->get(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 138
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentTermDocs;->skippingDoc()V

    goto :goto_0

    .line 127
    :cond_2
    iget v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    ushr-int/lit8 v3, v0, 0x1

    add-int/2addr v2, v3

    iput v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    .line 128
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_3

    .line 129
    iput v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freq:I

    goto :goto_1

    .line 131
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freq:I

    goto :goto_1
.end method

.method public read([I[I)I
    .locals 5
    .param p1, "docs"    # [I
    .param p2, "freqs"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    array-length v2, p1

    .line 147
    .local v2, "length":I
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v3, v4, :cond_1

    .line 148
    invoke-direct {p0, p1, p2, v2}, Lorg/apache/lucene/index/SegmentTermDocs;->readNoTf([I[II)I

    move-result v1

    .line 167
    :cond_0
    return v1

    .line 150
    :cond_1
    const/4 v1, 0x0

    .line 151
    .local v1, "i":I
    :cond_2
    :goto_0
    if-ge v1, v2, :cond_0

    iget v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->count:I

    iget v4, p0, Lorg/apache/lucene/index/SegmentTermDocs;->df:I

    if-ge v3, v4, :cond_0

    .line 153
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 154
    .local v0, "docCode":I
    iget v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    ushr-int/lit8 v4, v0, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    .line 155
    and-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_4

    .line 156
    const/4 v3, 0x1

    iput v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freq:I

    .line 159
    :goto_1
    iget v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->count:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->count:I

    .line 161
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    iget v4, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/BitVector;->get(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 162
    :cond_3
    iget v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    aput v3, p1, v1

    .line 163
    iget v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freq:I

    aput v3, p2, v1

    .line 164
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 158
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freq:I

    goto :goto_1
.end method

.method public seek(Lorg/apache/lucene/index/Term;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->parent:Lorg/apache/lucene/index/SegmentReader;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentCoreReaders;->getTermsReader()Lorg/apache/lucene/index/TermInfosReader;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/TermInfosReader;->get(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermInfo;

    move-result-object v0

    .line 67
    .local v0, "ti":Lorg/apache/lucene/index/TermInfo;
    invoke-virtual {p0, v0, p1}, Lorg/apache/lucene/index/SegmentTermDocs;->seek(Lorg/apache/lucene/index/TermInfo;Lorg/apache/lucene/index/Term;)V

    .line 68
    return-void
.end method

.method public seek(Lorg/apache/lucene/index/TermEnum;)V
    .locals 5
    .param p1, "termEnum"    # Lorg/apache/lucene/index/TermEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    instance-of v3, p1, Lorg/apache/lucene/index/SegmentTermEnum;

    if-eqz v3, :cond_0

    move-object v3, p1

    check-cast v3, Lorg/apache/lucene/index/SegmentTermEnum;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentTermEnum;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentTermDocs;->parent:Lorg/apache/lucene/index/SegmentReader;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    if-ne v3, v4, :cond_0

    move-object v0, p1

    .line 76
    check-cast v0, Lorg/apache/lucene/index/SegmentTermEnum;

    .line 77
    .local v0, "segmentTermEnum":Lorg/apache/lucene/index/SegmentTermEnum;
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v1

    .line 78
    .local v1, "term":Lorg/apache/lucene/index/Term;
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo()Lorg/apache/lucene/index/TermInfo;

    move-result-object v2

    .line 84
    .end local v0    # "segmentTermEnum":Lorg/apache/lucene/index/SegmentTermEnum;
    .local v2, "ti":Lorg/apache/lucene/index/TermInfo;
    :goto_0
    invoke-virtual {p0, v2, v1}, Lorg/apache/lucene/index/SegmentTermDocs;->seek(Lorg/apache/lucene/index/TermInfo;Lorg/apache/lucene/index/Term;)V

    .line 85
    return-void

    .line 80
    .end local v1    # "term":Lorg/apache/lucene/index/Term;
    .end local v2    # "ti":Lorg/apache/lucene/index/TermInfo;
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v1

    .line 81
    .restart local v1    # "term":Lorg/apache/lucene/index/Term;
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->parent:Lorg/apache/lucene/index/SegmentReader;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentCoreReaders;->getTermsReader()Lorg/apache/lucene/index/TermInfosReader;

    move-result-object v3

    invoke-virtual {v3, v1}, Lorg/apache/lucene/index/TermInfosReader;->get(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermInfo;

    move-result-object v2

    .restart local v2    # "ti":Lorg/apache/lucene/index/TermInfo;
    goto :goto_0
.end method

.method seek(Lorg/apache/lucene/index/TermInfo;Lorg/apache/lucene/index/Term;)V
    .locals 8
    .param p1, "ti"    # Lorg/apache/lucene/index/TermInfo;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 88
    iput v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->count:I

    .line 89
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->parent:Lorg/apache/lucene/index/SegmentReader;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget-object v3, p2, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 90
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    :goto_0
    iput-object v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 91
    if-eqz v0, :cond_1

    iget-boolean v1, v0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    :goto_1
    iput-boolean v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->currentFieldStoresPayloads:Z

    .line 92
    if-nez p1, :cond_2

    .line 93
    iput v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->df:I

    .line 103
    :goto_2
    return-void

    .line 90
    :cond_0
    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    goto :goto_0

    :cond_1
    move v1, v2

    .line 91
    goto :goto_1

    .line 95
    :cond_2
    iget v1, p1, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    iput v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->df:I

    .line 96
    iput v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    .line 97
    iget-wide v4, p1, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    iput-wide v4, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freqBasePointer:J

    .line 98
    iget-wide v4, p1, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    iput-wide v4, p0, Lorg/apache/lucene/index/SegmentTermDocs;->proxBasePointer:J

    .line 99
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freqBasePointer:J

    iget v1, p1, Lorg/apache/lucene/index/TermInfo;->skipOffset:I

    int-to-long v6, v1

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipPointer:J

    .line 100
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freqBasePointer:J

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 101
    iput-boolean v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->haveSkipped:Z

    goto :goto_2
.end method

.method protected skipProx(JI)V
    .locals 0
    .param p1, "proxPointer"    # J
    .param p3, "payloadLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    return-void
.end method

.method public skipTo(I)Z
    .locals 11
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    .line 195
    iget v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipInterval:I

    sub-int v1, p1, v1

    iget v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    if-lt v1, v2, :cond_2

    iget v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->df:I

    iget v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipInterval:I

    if-lt v1, v2, :cond_2

    .line 196
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/index/DefaultSkipListReader;

    if-nez v1, :cond_0

    .line 197
    new-instance v2, Lorg/apache/lucene/index/DefaultSkipListReader;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/store/IndexInput;

    iget v3, p0, Lorg/apache/lucene/index/SegmentTermDocs;->maxSkipLevels:I

    iget v4, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipInterval:I

    invoke-direct {v2, v1, v3, v4}, Lorg/apache/lucene/index/DefaultSkipListReader;-><init>(Lorg/apache/lucene/store/IndexInput;II)V

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/index/DefaultSkipListReader;

    .line 199
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->haveSkipped:Z

    if-nez v1, :cond_1

    .line 200
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/index/DefaultSkipListReader;

    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipPointer:J

    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freqBasePointer:J

    iget-wide v6, p0, Lorg/apache/lucene/index/SegmentTermDocs;->proxBasePointer:J

    iget v8, p0, Lorg/apache/lucene/index/SegmentTermDocs;->df:I

    iget-boolean v9, p0, Lorg/apache/lucene/index/SegmentTermDocs;->currentFieldStoresPayloads:Z

    invoke-virtual/range {v1 .. v9}, Lorg/apache/lucene/index/DefaultSkipListReader;->init(JJJIZ)V

    .line 201
    iput-boolean v10, p0, Lorg/apache/lucene/index/SegmentTermDocs;->haveSkipped:Z

    .line 204
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/index/DefaultSkipListReader;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/DefaultSkipListReader;->skipTo(I)I

    move-result v0

    .line 205
    .local v0, "newCount":I
    iget v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->count:I

    if-le v0, v1, :cond_2

    .line 206
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/index/DefaultSkipListReader;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DefaultSkipListReader;->getFreqPointer()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 207
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/index/DefaultSkipListReader;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DefaultSkipListReader;->getProxPointer()J

    move-result-wide v2

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/index/DefaultSkipListReader;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DefaultSkipListReader;->getPayloadLength()I

    move-result v1

    invoke-virtual {p0, v2, v3, v1}, Lorg/apache/lucene/index/SegmentTermDocs;->skipProx(JI)V

    .line 209
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/index/DefaultSkipListReader;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DefaultSkipListReader;->getDoc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    .line 210
    iput v0, p0, Lorg/apache/lucene/index/SegmentTermDocs;->count:I

    .line 216
    .end local v0    # "newCount":I
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentTermDocs;->next()Z

    move-result v1

    if-nez v1, :cond_3

    .line 217
    const/4 v1, 0x0

    .line 219
    :goto_0
    return v1

    .line 218
    :cond_3
    iget v1, p0, Lorg/apache/lucene/index/SegmentTermDocs;->doc:I

    if-gt p1, v1, :cond_2

    move v1, v10

    .line 219
    goto :goto_0
.end method

.method protected skippingDoc()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    return-void
.end method
