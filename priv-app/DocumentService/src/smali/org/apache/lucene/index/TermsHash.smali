.class final Lorg/apache/lucene/index/TermsHash;
.super Lorg/apache/lucene/index/InvertedDocConsumer;
.source "TermsHash.java"


# instance fields
.field final consumer:Lorg/apache/lucene/index/TermsHashConsumer;

.field final docWriter:Lorg/apache/lucene/index/DocumentsWriter;

.field final nextTermsHash:Lorg/apache/lucene/index/TermsHash;

.field trackAllocations:Z


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriter;ZLorg/apache/lucene/index/TermsHashConsumer;Lorg/apache/lucene/index/TermsHash;)V
    .locals 0
    .param p1, "docWriter"    # Lorg/apache/lucene/index/DocumentsWriter;
    .param p2, "trackAllocations"    # Z
    .param p3, "consumer"    # Lorg/apache/lucene/index/TermsHashConsumer;
    .param p4, "nextTermsHash"    # Lorg/apache/lucene/index/TermsHash;

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/lucene/index/InvertedDocConsumer;-><init>()V

    .line 44
    iput-object p1, p0, Lorg/apache/lucene/index/TermsHash;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 45
    iput-object p3, p0, Lorg/apache/lucene/index/TermsHash;->consumer:Lorg/apache/lucene/index/TermsHashConsumer;

    .line 46
    iput-object p4, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    .line 47
    iput-boolean p2, p0, Lorg/apache/lucene/index/TermsHash;->trackAllocations:Z

    .line 48
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 2

    .prologue
    .line 68
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->consumer:Lorg/apache/lucene/index/TermsHashConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashConsumer;->abort()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHash;->abort()V

    .line 74
    :cond_0
    return-void

    .line 70
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    if-eqz v1, :cond_1

    .line 71
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermsHash;->abort()V

    .line 70
    :cond_1
    throw v0
.end method

.method addThread(Lorg/apache/lucene/index/DocInverterPerThread;)Lorg/apache/lucene/index/InvertedDocConsumerPerThread;
    .locals 3
    .param p1, "docInverterPerThread"    # Lorg/apache/lucene/index/DocInverterPerThread;

    .prologue
    .line 52
    new-instance v0, Lorg/apache/lucene/index/TermsHashPerThread;

    iget-object v1, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    const/4 v2, 0x0

    invoke-direct {v0, p1, p0, v1, v2}, Lorg/apache/lucene/index/TermsHashPerThread;-><init>(Lorg/apache/lucene/index/DocInverterPerThread;Lorg/apache/lucene/index/TermsHash;Lorg/apache/lucene/index/TermsHash;Lorg/apache/lucene/index/TermsHashPerThread;)V

    return-object v0
.end method

.method addThread(Lorg/apache/lucene/index/DocInverterPerThread;Lorg/apache/lucene/index/TermsHashPerThread;)Lorg/apache/lucene/index/TermsHashPerThread;
    .locals 2
    .param p1, "docInverterPerThread"    # Lorg/apache/lucene/index/DocInverterPerThread;
    .param p2, "primaryPerThread"    # Lorg/apache/lucene/index/TermsHashPerThread;

    .prologue
    .line 56
    new-instance v0, Lorg/apache/lucene/index/TermsHashPerThread;

    iget-object v1, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    invoke-direct {v0, p1, p0, v1, p2}, Lorg/apache/lucene/index/TermsHashPerThread;-><init>(Lorg/apache/lucene/index/DocInverterPerThread;Lorg/apache/lucene/index/TermsHash;Lorg/apache/lucene/index/TermsHash;Lorg/apache/lucene/index/TermsHashPerThread;)V

    return-object v0
.end method

.method declared-synchronized flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 11
    .param p2, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/InvertedDocConsumerPerThread;",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/InvertedDocConsumerPerField;",
            ">;>;",
            "Lorg/apache/lucene/index/SegmentWriteState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "threadsAndFields":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/InvertedDocConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;>;"
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 81
    .local v1, "childThreadsAndFields":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/TermsHashConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;>;"
    iget-object v10, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    if-eqz v10, :cond_2

    .line 82
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 86
    .local v7, "nextThreadsAndFields":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/InvertedDocConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;>;"
    :goto_0
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 88
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/InvertedDocConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/index/TermsHashPerThread;

    .line 90
    .local v9, "perThread":Lorg/apache/lucene/index/TermsHashPerThread;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Collection;

    .line 92
    .local v3, "fields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 93
    .local v4, "fieldsIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 96
    .local v0, "childFields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;"
    iget-object v10, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    if-eqz v10, :cond_3

    .line 97
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 101
    .local v6, "nextChildFields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    :cond_1
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 102
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/index/TermsHashPerField;

    .line 103
    .local v8, "perField":Lorg/apache/lucene/index/TermsHashPerField;
    iget-object v10, v8, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-interface {v0, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 104
    iget-object v10, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    if-eqz v10, :cond_1

    .line 105
    iget-object v10, v8, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-interface {v6, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 78
    .end local v0    # "childFields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;"
    .end local v1    # "childThreadsAndFields":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/TermsHashConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;>;"
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/InvertedDocConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;>;"
    .end local v3    # "fields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    .end local v4    # "fieldsIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "nextChildFields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    .end local v7    # "nextThreadsAndFields":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/InvertedDocConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;>;"
    .end local v8    # "perField":Lorg/apache/lucene/index/TermsHashPerField;
    .end local v9    # "perThread":Lorg/apache/lucene/index/TermsHashPerThread;
    :catchall_0
    move-exception v10

    monitor-exit p0

    throw v10

    .line 84
    .restart local v1    # "childThreadsAndFields":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/TermsHashConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;>;"
    :cond_2
    const/4 v7, 0x0

    .restart local v7    # "nextThreadsAndFields":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/InvertedDocConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;>;"
    goto :goto_0

    .line 99
    .restart local v0    # "childFields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;"
    .restart local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/InvertedDocConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;>;"
    .restart local v3    # "fields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    .restart local v4    # "fieldsIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v9    # "perThread":Lorg/apache/lucene/index/TermsHashPerThread;
    :cond_3
    const/4 v6, 0x0

    .restart local v6    # "nextChildFields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    goto :goto_2

    .line 108
    :cond_4
    :try_start_1
    iget-object v10, v9, Lorg/apache/lucene/index/TermsHashPerThread;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerThread;

    invoke-interface {v1, v10, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    iget-object v10, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    if-eqz v10, :cond_0

    .line 110
    iget-object v10, v9, Lorg/apache/lucene/index/TermsHashPerThread;->nextPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    invoke-interface {v7, v10, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 113
    .end local v0    # "childFields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;"
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/InvertedDocConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;>;"
    .end local v3    # "fields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    .end local v4    # "fieldsIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    .end local v6    # "nextChildFields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    .end local v9    # "perThread":Lorg/apache/lucene/index/TermsHashPerThread;
    :cond_5
    iget-object v10, p0, Lorg/apache/lucene/index/TermsHash;->consumer:Lorg/apache/lucene/index/TermsHashConsumer;

    invoke-virtual {v10, v1, p2}, Lorg/apache/lucene/index/TermsHashConsumer;->flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 115
    iget-object v10, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    if-eqz v10, :cond_6

    .line 116
    iget-object v10, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    invoke-virtual {v10, v7, p2}, Lorg/apache/lucene/index/TermsHash;->flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 117
    :cond_6
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized freeRAM()Z
    .locals 1

    .prologue
    .line 121
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return v0
.end method

.method setFieldInfos(Lorg/apache/lucene/index/FieldInfos;)V
    .locals 1
    .param p1, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;

    .prologue
    .line 61
    iput-object p1, p0, Lorg/apache/lucene/index/TermsHash;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 62
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->consumer:Lorg/apache/lucene/index/TermsHashConsumer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/TermsHashConsumer;->setFieldInfos(Lorg/apache/lucene/index/FieldInfos;)V

    .line 63
    return-void
.end method
