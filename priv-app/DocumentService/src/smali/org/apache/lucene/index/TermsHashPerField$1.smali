.class Lorg/apache/lucene/index/TermsHashPerField$1;
.super Lorg/apache/lucene/util/SorterTemplate;
.source "TermsHashPerField.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/TermsHashPerField;->sortPostings()[I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private pivotBuf:[C

.field private pivotBufPos:I

.field private pivotTerm:I

.field final synthetic this$0:Lorg/apache/lucene/index/TermsHashPerField;

.field final synthetic val$postingsHash:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 217
    const-class v0, Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermsHashPerField$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/TermsHashPerField;[I)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->this$0:Lorg/apache/lucene/index/TermsHashPerField;

    iput-object p2, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->val$postingsHash:[I

    invoke-direct {p0}, Lorg/apache/lucene/util/SorterTemplate;-><init>()V

    return-void
.end method

.method private comparePostings([CI[CI)I
    .locals 6
    .param p1, "text1"    # [C
    .param p2, "pos1"    # I
    .param p3, "text2"    # [C
    .param p4, "pos2"    # I

    .prologue
    const v5, 0xffff

    .line 218
    sget-boolean v4, Lorg/apache/lucene/index/TermsHashPerField$1;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-ne p1, p3, :cond_1

    if-ne p2, p4, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .end local p2    # "pos1":I
    .end local p4    # "pos2":I
    .local v0, "c1":C
    .local v1, "c2":C
    .local v2, "pos1":I
    .local v3, "pos2":I
    :cond_0
    move p4, v3

    .end local v3    # "pos2":I
    .restart local p4    # "pos2":I
    move p2, v2

    .line 221
    .end local v0    # "c1":C
    .end local v1    # "c2":C
    .end local v2    # "pos1":I
    .restart local p2    # "pos1":I
    :cond_1
    add-int/lit8 v2, p2, 0x1

    .end local p2    # "pos1":I
    .restart local v2    # "pos1":I
    aget-char v0, p1, p2

    .line 222
    .restart local v0    # "c1":C
    add-int/lit8 v3, p4, 0x1

    .end local p4    # "pos2":I
    .restart local v3    # "pos2":I
    aget-char v1, p3, p4

    .line 223
    .restart local v1    # "c2":C
    if-eq v0, v1, :cond_4

    .line 224
    if-ne v5, v1, :cond_2

    .line 225
    const/4 v4, 0x1

    .line 229
    :goto_0
    return v4

    .line 226
    :cond_2
    if-ne v5, v0, :cond_3

    .line 227
    const/4 v4, -0x1

    goto :goto_0

    .line 229
    :cond_3
    sub-int v4, v0, v1

    goto :goto_0

    .line 233
    :cond_4
    sget-boolean v4, Lorg/apache/lucene/index/TermsHashPerField$1;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-ne v0, v5, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
.end method


# virtual methods
.method protected compare(II)I
    .locals 10
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 181
    iget-object v8, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->val$postingsHash:[I

    aget v2, v8, p1

    .local v2, "term1":I
    iget-object v8, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->val$postingsHash:[I

    aget v3, v8, p2

    .line 182
    .local v3, "term2":I
    if-ne v2, v3, :cond_0

    .line 183
    const/4 v8, 0x0

    .line 190
    :goto_0
    return v8

    .line 184
    :cond_0
    iget-object v8, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->this$0:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v8, v8, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v8, v8, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    aget v6, v8, v2

    .line 185
    .local v6, "textStart1":I
    iget-object v8, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->this$0:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v8, v8, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v8, v8, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    aget v7, v8, v3

    .line 186
    .local v7, "textStart2":I
    iget-object v8, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->this$0:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v8, v8, Lorg/apache/lucene/index/TermsHashPerField;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    iget-object v8, v8, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    shr-int/lit8 v9, v6, 0xe

    aget-object v4, v8, v9

    .line 187
    .local v4, "text1":[C
    and-int/lit16 v0, v6, 0x3fff

    .line 188
    .local v0, "pos1":I
    iget-object v8, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->this$0:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v8, v8, Lorg/apache/lucene/index/TermsHashPerField;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    iget-object v8, v8, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    shr-int/lit8 v9, v7, 0xe

    aget-object v5, v8, v9

    .line 189
    .local v5, "text2":[C
    and-int/lit16 v1, v7, 0x3fff

    .line 190
    .local v1, "pos2":I
    invoke-direct {p0, v4, v0, v5, v1}, Lorg/apache/lucene/index/TermsHashPerField$1;->comparePostings([CI[CI)I

    move-result v8

    goto :goto_0
.end method

.method protected comparePivot(I)I
    .locals 6
    .param p1, "j"    # I

    .prologue
    .line 203
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->val$postingsHash:[I

    aget v1, v4, p1

    .line 204
    .local v1, "term":I
    iget v4, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->pivotTerm:I

    if-ne v4, v1, :cond_0

    .line 205
    const/4 v4, 0x0

    .line 209
    :goto_0
    return v4

    .line 206
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->this$0:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v4, v4, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v4, v4, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    aget v3, v4, v1

    .line 207
    .local v3, "textStart":I
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->this$0:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v4, v4, Lorg/apache/lucene/index/TermsHashPerField;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    iget-object v4, v4, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    shr-int/lit8 v5, v3, 0xe

    aget-object v2, v4, v5

    .line 208
    .local v2, "text":[C
    and-int/lit16 v0, v3, 0x3fff

    .line 209
    .local v0, "pos":I
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->pivotBuf:[C

    iget v5, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->pivotBufPos:I

    invoke-direct {p0, v4, v5, v2, v0}, Lorg/apache/lucene/index/TermsHashPerField$1;->comparePostings([CI[CI)I

    move-result v4

    goto :goto_0
.end method

.method protected setPivot(I)V
    .locals 3
    .param p1, "i"    # I

    .prologue
    .line 195
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->val$postingsHash:[I

    aget v1, v1, p1

    iput v1, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->pivotTerm:I

    .line 196
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->this$0:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, v1, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v1, v1, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    iget v2, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->pivotTerm:I

    aget v0, v1, v2

    .line 197
    .local v0, "textStart":I
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->this$0:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, v1, Lorg/apache/lucene/index/TermsHashPerField;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    iget-object v1, v1, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    shr-int/lit8 v2, v0, 0xe

    aget-object v1, v1, v2

    iput-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->pivotBuf:[C

    .line 198
    and-int/lit16 v1, v0, 0x3fff

    iput v1, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->pivotBufPos:I

    .line 199
    return-void
.end method

.method protected swap(II)V
    .locals 3
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 174
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->val$postingsHash:[I

    aget v0, v1, p1

    .line 175
    .local v0, "o":I
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->val$postingsHash:[I

    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->val$postingsHash:[I

    aget v2, v2, p2

    aput v2, v1, p1

    .line 176
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField$1;->val$postingsHash:[I

    aput v0, v1, p2

    .line 177
    return-void
.end method
