.class final Lorg/apache/lucene/index/TermVectorsWriter;
.super Ljava/lang/Object;
.source "TermVectorsWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final directory:Lorg/apache/lucene/store/Directory;

.field private fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field final segment:Ljava/lang/String;

.field private tvd:Lorg/apache/lucene/store/IndexOutput;

.field private tvf:Lorg/apache/lucene/store/IndexOutput;

.field private tvx:Lorg/apache/lucene/store/IndexOutput;

.field final utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/apache/lucene/index/TermVectorsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermVectorsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 7
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    .line 32
    new-array v1, v6, [Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    new-instance v2, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-direct {v2}, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;-><init>()V

    aput-object v2, v1, v4

    new-instance v2, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-direct {v2}, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;-><init>()V

    aput-object v2, v1, v5

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsWriter;->utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    .line 40
    iput-object p2, p0, Lorg/apache/lucene/index/TermVectorsWriter;->segment:Ljava/lang/String;

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/index/TermVectorsWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 42
    const/4 v0, 0x0

    .line 45
    .local v0, "success":Z
    :try_start_0
    const-string/jumbo v1, "tvx"

    invoke-static {p2, v1}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    .line 46
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 47
    const-string/jumbo v1, "tvd"

    invoke-static {p2, v1}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    .line 48
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 49
    const-string/jumbo v1, "tvf"

    invoke-static {p2, v1}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    .line 50
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    const/4 v0, 0x1

    .line 53
    if-nez v0, :cond_0

    .line 54
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/io/Closeable;

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v1, v4

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v1, v5

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v1, v6

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 58
    :cond_0
    iput-object p3, p0, Lorg/apache/lucene/index/TermVectorsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 59
    return-void

    .line 53
    :catchall_0
    move-exception v1

    if-nez v0, :cond_1

    .line 54
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/io/Closeable;

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v2, v4

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v2, v5

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v2, v6

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 53
    :cond_1
    throw v1
.end method


# virtual methods
.method public final addAllDocVectors([Lorg/apache/lucene/index/TermFreqVector;)V
    .locals 36
    .param p1, "vectors"    # [Lorg/apache/lucene/index/TermFreqVector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v34

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 71
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v34

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 73
    if-eqz p1, :cond_d

    .line 74
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v19, v0

    .line 75
    .local v19, "numFields":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 77
    move/from16 v0, v19

    new-array v7, v0, [J

    .line 79
    .local v7, "fieldPointers":[J
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    move/from16 v0, v19

    if-ge v11, v0, :cond_c

    .line 80
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v32

    aput-wide v32, v7, v11

    .line 82
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v32, v0

    aget-object v33, p1, v11

    invoke-interface/range {v33 .. v33}, Lorg/apache/lucene/index/TermFreqVector;->getField()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Lorg/apache/lucene/index/FieldInfos;->fieldNumber(Ljava/lang/String;)I

    move-result v6

    .line 85
    .local v6, "fieldNumber":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 87
    aget-object v32, p1, v11

    invoke-interface/range {v32 .. v32}, Lorg/apache/lucene/index/TermFreqVector;->size()I

    move-result v20

    .line 88
    .local v20, "numTerms":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 96
    aget-object v32, p1, v11

    move-object/from16 v0, v32

    instance-of v0, v0, Lorg/apache/lucene/index/TermPositionVector;

    move/from16 v32, v0

    if-eqz v32, :cond_4

    .line 98
    aget-object v30, p1, v11

    check-cast v30, Lorg/apache/lucene/index/TermPositionVector;

    .line 99
    .local v30, "tpVector":Lorg/apache/lucene/index/TermPositionVector;
    invoke-interface/range {v30 .. v30}, Lorg/apache/lucene/index/TermPositionVector;->size()I

    move-result v32

    if-lez v32, :cond_0

    const/16 v32, 0x0

    move-object/from16 v0, v30

    move/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/apache/lucene/index/TermPositionVector;->getTermPositions(I)[I

    move-result-object v32

    if-eqz v32, :cond_0

    const/16 v27, 0x1

    .line 100
    .local v27, "storePositions":Z
    :goto_1
    invoke-interface/range {v30 .. v30}, Lorg/apache/lucene/index/TermPositionVector;->size()I

    move-result v32

    if-lez v32, :cond_1

    const/16 v32, 0x0

    move-object/from16 v0, v30

    move/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/apache/lucene/index/TermPositionVector;->getOffsets(I)[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    move-result-object v32

    if-eqz v32, :cond_1

    const/16 v26, 0x1

    .line 101
    .local v26, "storeOffsets":Z
    :goto_2
    if-eqz v27, :cond_2

    const/16 v32, 0x1

    move/from16 v33, v32

    :goto_3
    if-eqz v26, :cond_3

    const/16 v32, 0x2

    :goto_4
    add-int v32, v32, v33

    move/from16 v0, v32

    int-to-byte v4, v0

    .line 110
    .local v4, "bits":B
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 112
    aget-object v32, p1, v11

    invoke-interface/range {v32 .. v32}, Lorg/apache/lucene/index/TermFreqVector;->getTerms()[Ljava/lang/String;

    move-result-object v29

    .line 113
    .local v29, "terms":[Ljava/lang/String;
    aget-object v32, p1, v11

    invoke-interface/range {v32 .. v32}, Lorg/apache/lucene/index/TermFreqVector;->getTermFrequencies()[I

    move-result-object v10

    .line 115
    .local v10, "freqs":[I
    const/16 v31, 0x0

    .line 116
    .local v31, "utf8Upto":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    move-object/from16 v32, v0

    const/16 v33, 0x1

    aget-object v32, v32, v33

    const/16 v33, 0x0

    move/from16 v0, v33

    move-object/from16 v1, v32

    iput v0, v1, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->length:I

    .line 118
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_6
    move/from16 v0, v20

    if-ge v12, v0, :cond_b

    .line 120
    aget-object v32, v29, v12

    const/16 v33, 0x0

    aget-object v34, v29, v12

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->length()I

    move-result v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    move-object/from16 v35, v0

    aget-object v35, v35, v31

    invoke-static/range {v32 .. v35}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8(Ljava/lang/String;IILorg/apache/lucene/util/UnicodeUtil$UTF8Result;)V

    .line 122
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    move-object/from16 v32, v0

    rsub-int/lit8 v33, v31, 0x1

    aget-object v32, v32, v33

    move-object/from16 v0, v32

    iget-object v0, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    move-object/from16 v33, v0

    rsub-int/lit8 v34, v31, 0x1

    aget-object v33, v33, v34

    move-object/from16 v0, v33

    iget v0, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->length:I

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    move-object/from16 v34, v0

    aget-object v34, v34, v31

    move-object/from16 v0, v34

    iget-object v0, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    move-object/from16 v35, v0

    aget-object v35, v35, v31

    move-object/from16 v0, v35

    iget v0, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->length:I

    move/from16 v35, v0

    invoke-static/range {v32 .. v35}, Lorg/apache/lucene/util/StringHelper;->bytesDifference([BI[BI)I

    move-result v24

    .line 126
    .local v24, "start":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    move-object/from16 v32, v0

    aget-object v32, v32, v31

    move-object/from16 v0, v32

    iget v0, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->length:I

    move/from16 v32, v0

    sub-int v18, v32, v24

    .line 127
    .local v18, "length":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 129
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    move-object/from16 v33, v0

    aget-object v33, v33, v31

    move-object/from16 v0, v33

    iget-object v0, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    move-object/from16 v33, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    move/from16 v2, v24

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 130
    rsub-int/lit8 v31, v31, 0x1

    .line 132
    aget v28, v10, v12

    .line 134
    .local v28, "termFreq":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 136
    if-eqz v27, :cond_7

    .line 137
    move-object/from16 v0, v30

    invoke-interface {v0, v12}, Lorg/apache/lucene/index/TermPositionVector;->getTermPositions(I)[I

    move-result-object v23

    .line 138
    .local v23, "positions":[I
    if-nez v23, :cond_5

    .line 139
    new-instance v32, Ljava/lang/IllegalStateException;

    const-string/jumbo v33, "Trying to write positions that are null!"

    invoke-direct/range {v32 .. v33}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 99
    .end local v4    # "bits":B
    .end local v10    # "freqs":[I
    .end local v12    # "j":I
    .end local v18    # "length":I
    .end local v23    # "positions":[I
    .end local v24    # "start":I
    .end local v26    # "storeOffsets":Z
    .end local v27    # "storePositions":Z
    .end local v28    # "termFreq":I
    .end local v29    # "terms":[Ljava/lang/String;
    .end local v31    # "utf8Upto":I
    :cond_0
    const/16 v27, 0x0

    goto/16 :goto_1

    .line 100
    .restart local v27    # "storePositions":Z
    :cond_1
    const/16 v26, 0x0

    goto/16 :goto_2

    .line 101
    .restart local v26    # "storeOffsets":Z
    :cond_2
    const/16 v32, 0x0

    move/from16 v33, v32

    goto/16 :goto_3

    :cond_3
    const/16 v32, 0x0

    goto/16 :goto_4

    .line 104
    .end local v26    # "storeOffsets":Z
    .end local v27    # "storePositions":Z
    .end local v30    # "tpVector":Lorg/apache/lucene/index/TermPositionVector;
    :cond_4
    const/16 v30, 0x0

    .line 105
    .restart local v30    # "tpVector":Lorg/apache/lucene/index/TermPositionVector;
    const/4 v4, 0x0

    .line 106
    .restart local v4    # "bits":B
    const/16 v27, 0x0

    .line 107
    .restart local v27    # "storePositions":Z
    const/16 v26, 0x0

    .restart local v26    # "storeOffsets":Z
    goto/16 :goto_5

    .line 140
    .restart local v10    # "freqs":[I
    .restart local v12    # "j":I
    .restart local v18    # "length":I
    .restart local v23    # "positions":[I
    .restart local v24    # "start":I
    .restart local v28    # "termFreq":I
    .restart local v29    # "terms":[Ljava/lang/String;
    .restart local v31    # "utf8Upto":I
    :cond_5
    sget-boolean v32, Lorg/apache/lucene/index/TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v32, :cond_6

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    move/from16 v1, v28

    if-eq v0, v1, :cond_6

    new-instance v32, Ljava/lang/AssertionError;

    invoke-direct/range {v32 .. v32}, Ljava/lang/AssertionError;-><init>()V

    throw v32

    .line 143
    :cond_6
    const/4 v15, 0x0

    .line 144
    .local v15, "lastPosition":I
    const/4 v13, 0x0

    .local v13, "k":I
    :goto_7
    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    if-ge v13, v0, :cond_7

    .line 145
    aget v22, v23, v13

    .line 146
    .local v22, "position":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    sub-int v33, v22, v15

    invoke-virtual/range {v32 .. v33}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 147
    move/from16 v15, v22

    .line 144
    add-int/lit8 v13, v13, 0x1

    goto :goto_7

    .line 151
    .end local v13    # "k":I
    .end local v15    # "lastPosition":I
    .end local v22    # "position":I
    .end local v23    # "positions":[I
    :cond_7
    if-eqz v26, :cond_a

    .line 152
    move-object/from16 v0, v30

    invoke-interface {v0, v12}, Lorg/apache/lucene/index/TermPositionVector;->getOffsets(I)[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    move-result-object v21

    .line 153
    .local v21, "offsets":[Lorg/apache/lucene/index/TermVectorOffsetInfo;
    if-nez v21, :cond_8

    .line 154
    new-instance v32, Ljava/lang/IllegalStateException;

    const-string/jumbo v33, "Trying to write offsets that are null!"

    invoke-direct/range {v32 .. v33}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 155
    :cond_8
    sget-boolean v32, Lorg/apache/lucene/index/TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v32, :cond_9

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    move/from16 v1, v28

    if-eq v0, v1, :cond_9

    new-instance v32, Ljava/lang/AssertionError;

    invoke-direct/range {v32 .. v32}, Ljava/lang/AssertionError;-><init>()V

    throw v32

    .line 158
    :cond_9
    const/4 v14, 0x0

    .line 159
    .local v14, "lastEndOffset":I
    const/4 v13, 0x0

    .restart local v13    # "k":I
    :goto_8
    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    if-ge v13, v0, :cond_a

    .line 160
    aget-object v32, v21, v13

    invoke-virtual/range {v32 .. v32}, Lorg/apache/lucene/index/TermVectorOffsetInfo;->getStartOffset()I

    move-result v25

    .line 161
    .local v25, "startOffset":I
    aget-object v32, v21, v13

    invoke-virtual/range {v32 .. v32}, Lorg/apache/lucene/index/TermVectorOffsetInfo;->getEndOffset()I

    move-result v5

    .line 162
    .local v5, "endOffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    sub-int v33, v25, v14

    invoke-virtual/range {v32 .. v33}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    sub-int v33, v5, v25

    invoke-virtual/range {v32 .. v33}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 164
    move v14, v5

    .line 159
    add-int/lit8 v13, v13, 0x1

    goto :goto_8

    .line 118
    .end local v5    # "endOffset":I
    .end local v13    # "k":I
    .end local v14    # "lastEndOffset":I
    .end local v21    # "offsets":[Lorg/apache/lucene/index/TermVectorOffsetInfo;
    .end local v25    # "startOffset":I
    :cond_a
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_6

    .line 79
    .end local v18    # "length":I
    .end local v24    # "start":I
    .end local v28    # "termFreq":I
    :cond_b
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 171
    .end local v4    # "bits":B
    .end local v6    # "fieldNumber":I
    .end local v10    # "freqs":[I
    .end local v12    # "j":I
    .end local v20    # "numTerms":I
    .end local v26    # "storeOffsets":Z
    .end local v27    # "storePositions":Z
    .end local v29    # "terms":[Ljava/lang/String;
    .end local v30    # "tpVector":Lorg/apache/lucene/index/TermPositionVector;
    .end local v31    # "utf8Upto":I
    :cond_c
    const/16 v32, 0x1

    move/from16 v0, v19

    move/from16 v1, v32

    if-le v0, v1, :cond_e

    .line 172
    const/16 v32, 0x0

    aget-wide v16, v7, v32

    .line 173
    .local v16, "lastFieldPointer":J
    const/4 v11, 0x1

    :goto_9
    move/from16 v0, v19

    if-ge v11, v0, :cond_e

    .line 174
    aget-wide v8, v7, v11

    .line 175
    .local v8, "fieldPointer":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    sub-long v34, v8, v16

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 176
    move-wide/from16 v16, v8

    .line 173
    add-int/lit8 v11, v11, 0x1

    goto :goto_9

    .line 180
    .end local v7    # "fieldPointers":[J
    .end local v8    # "fieldPointer":J
    .end local v11    # "i":I
    .end local v16    # "lastFieldPointer":J
    .end local v19    # "numFields":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 181
    :cond_e
    return-void
.end method

.method final addRawDocuments(Lorg/apache/lucene/index/TermVectorsReader;[I[II)V
    .locals 16
    .param p1, "reader"    # Lorg/apache/lucene/index/TermVectorsReader;
    .param p2, "tvdLengths"    # [I
    .param p3, "tvfLengths"    # [I
    .param p4, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 189
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    .line 190
    .local v4, "tvdPosition":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v8

    .line 191
    .local v8, "tvfPosition":J
    move-wide v6, v4

    .line 192
    .local v6, "tvdStart":J
    move-wide v10, v8

    .line 193
    .local v10, "tvfStart":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    move/from16 v0, p4

    if-ge v2, v0, :cond_0

    .line 194
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 195
    aget v3, p2, v2

    int-to-long v12, v3

    add-long/2addr v4, v12

    .line 196
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 197
    aget v3, p3, v2

    int-to-long v12, v3

    add-long/2addr v8, v12

    .line 193
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 199
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/TermVectorsReader;->getTvdStream()Lorg/apache/lucene/store/IndexInput;

    move-result-object v12

    sub-long v14, v4, v6

    invoke-virtual {v3, v12, v14, v15}, Lorg/apache/lucene/store/IndexOutput;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V

    .line 200
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/TermVectorsReader;->getTvfStream()Lorg/apache/lucene/store/IndexInput;

    move-result-object v12

    sub-long v14, v8, v10

    invoke-virtual {v3, v12, v14, v15}, Lorg/apache/lucene/store/IndexOutput;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V

    .line 201
    sget-boolean v3, Lorg/apache/lucene/index/TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v12

    cmp-long v3, v12, v4

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 202
    :cond_1
    sget-boolean v3, Lorg/apache/lucene/index/TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v12

    cmp-long v3, v12, v8

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 203
    :cond_2
    return-void
.end method

.method final close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 222
    return-void
.end method

.method finish(I)V
    .locals 8
    .param p1, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 206
    const-wide/16 v2, 0x4

    int-to-long v4, p1

    const-wide/16 v6, 0x10

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 207
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsWriter;->segment:Ljava/lang/String;

    const-string/jumbo v2, "tvx"

    invoke-static {v1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 213
    .local v0, "idxName":Ljava/lang/String;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "tvx size mismatch: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " docs vs "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " length in bytes of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " file exists?="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 215
    .end local v0    # "idxName":Ljava/lang/String;
    :cond_0
    return-void
.end method
