.class final Lorg/apache/lucene/index/SegmentTermPositions;
.super Lorg/apache/lucene/index/SegmentTermDocs;
.source "SegmentTermPositions.java"

# interfaces
.implements Lorg/apache/lucene/index/TermPositions;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private lazySkipPointer:J

.field private lazySkipProxCount:I

.field private needToLoadPayload:Z

.field private payloadLength:I

.field private position:I

.field private proxCount:I

.field private proxStream:Lorg/apache/lucene/store/IndexInput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lorg/apache/lucene/index/SegmentTermPositions;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SegmentTermPositions;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/SegmentReader;)V
    .locals 2
    .param p1, "p"    # Lorg/apache/lucene/index/SegmentReader;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/SegmentTermDocs;-><init>(Lorg/apache/lucene/index/SegmentReader;)V

    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipPointer:J

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipProxCount:I

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    .line 45
    return-void
.end method

.method private lazySkip()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    .line 154
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    if-nez v0, :cond_0

    .line 156
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->parent:Lorg/apache/lucene/index/SegmentReader;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentCoreReaders;->proxStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/IndexInput;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    .line 161
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentTermPositions;->skipPayload()V

    .line 163
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipPointer:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 164
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipPointer:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 165
    iput-wide v4, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipPointer:J

    .line 168
    :cond_1
    iget v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipProxCount:I

    if-eqz v0, :cond_2

    .line 169
    iget v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipProxCount:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/SegmentTermPositions;->skipPositions(I)V

    .line 170
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipProxCount:I

    .line 172
    :cond_2
    return-void
.end method

.method private final readDeltaPosition()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 77
    .local v0, "delta":I
    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentTermPositions;->currentFieldStoresPayloads:Z

    if-eqz v1, :cond_1

    .line 82
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/index/SegmentTermPositions;->payloadLength:I

    .line 85
    :cond_0
    ushr-int/lit8 v0, v0, 0x1

    .line 86
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/index/SegmentTermPositions;->needToLoadPayload:Z

    .line 88
    :cond_1
    return v0
.end method

.method private skipPayload()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    iget-boolean v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->needToLoadPayload:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->payloadLength:I

    if-lez v0, :cond_0

    .line 138
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v2

    iget v1, p0, Lorg/apache/lucene/index/SegmentTermPositions;->payloadLength:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 140
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->needToLoadPayload:Z

    .line 141
    return-void
.end method

.method private skipPositions(I)V
    .locals 3
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    sget-boolean v1, Lorg/apache/lucene/index/SegmentTermPositions;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermPositions;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 130
    :cond_0
    move v0, p1

    .local v0, "f":I
    :goto_0
    if-lez v0, :cond_1

    .line 131
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentTermPositions;->readDeltaPosition()I

    .line 132
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentTermPositions;->skipPayload()V

    .line 130
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 134
    :cond_1
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-super {p0}, Lorg/apache/lucene/index/SegmentTermDocs;->close()V

    .line 62
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 63
    :cond_0
    return-void
.end method

.method public getPayload([BI)[B
    .locals 4
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 179
    iget-boolean v2, p0, Lorg/apache/lucene/index/SegmentTermPositions;->needToLoadPayload:Z

    if-nez v2, :cond_0

    .line 180
    new-instance v2, Ljava/io/IOException;

    const-string/jumbo v3, "Either no payload exists at this term position or an attempt was made to load it more than once."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 186
    :cond_0
    if-eqz p1, :cond_1

    array-length v2, p1

    sub-int/2addr v2, p2

    iget v3, p0, Lorg/apache/lucene/index/SegmentTermPositions;->payloadLength:I

    if-ge v2, v3, :cond_2

    .line 189
    :cond_1
    iget v2, p0, Lorg/apache/lucene/index/SegmentTermPositions;->payloadLength:I

    new-array v0, v2, [B

    .line 190
    .local v0, "retArray":[B
    const/4 v1, 0x0

    .line 195
    .local v1, "retOffset":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    iget v3, p0, Lorg/apache/lucene/index/SegmentTermPositions;->payloadLength:I

    invoke-virtual {v2, v0, v1, v3}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 196
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/lucene/index/SegmentTermPositions;->needToLoadPayload:Z

    .line 197
    return-object v0

    .line 192
    .end local v0    # "retArray":[B
    .end local v1    # "retOffset":I
    :cond_2
    move-object v0, p1

    .line 193
    .restart local v0    # "retArray":[B
    move v1, p2

    .restart local v1    # "retOffset":I
    goto :goto_0
.end method

.method public getPayloadLength()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->payloadLength:I

    return v0
.end method

.method public isPayloadAvailable()Z
    .locals 1

    .prologue
    .line 201
    iget-boolean v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->needToLoadPayload:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->payloadLength:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 101
    iget v1, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipProxCount:I

    iget v2, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxCount:I

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipProxCount:I

    .line 103
    invoke-super {p0}, Lorg/apache/lucene/index/SegmentTermDocs;->next()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    iget v1, p0, Lorg/apache/lucene/index/SegmentTermPositions;->freq:I

    iput v1, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxCount:I

    .line 105
    iput v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->position:I

    .line 106
    const/4 v0, 0x1

    .line 108
    :cond_0
    return v0
.end method

.method public final nextPosition()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v0, v1, :cond_0

    .line 68
    const/4 v0, 0x0

    .line 72
    :goto_0
    return v0

    .line 70
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkip()V

    .line 71
    iget v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxCount:I

    .line 72
    iget v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->position:I

    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentTermPositions;->readDeltaPosition()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->position:I

    goto :goto_0
.end method

.method public final read([I[I)I
    .locals 2
    .param p1, "docs"    # [I
    .param p2, "freqs"    # [I

    .prologue
    .line 113
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "TermPositions does not support processing multiple documents in one call. Use TermDocs instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final seek(Lorg/apache/lucene/index/TermInfo;Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "ti"    # Lorg/apache/lucene/index/TermInfo;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/index/SegmentTermDocs;->seek(Lorg/apache/lucene/index/TermInfo;Lorg/apache/lucene/index/Term;)V

    .line 50
    if-eqz p1, :cond_0

    .line 51
    iget-wide v0, p1, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipPointer:J

    .line 53
    :cond_0
    iput v2, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipProxCount:I

    .line 54
    iput v2, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxCount:I

    .line 55
    iput v2, p0, Lorg/apache/lucene/index/SegmentTermPositions;->payloadLength:I

    .line 56
    iput-boolean v2, p0, Lorg/apache/lucene/index/SegmentTermPositions;->needToLoadPayload:Z

    .line 57
    return-void
.end method

.method protected skipProx(JI)V
    .locals 1
    .param p1, "proxPointer"    # J
    .param p3, "payloadLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 121
    iput-wide p1, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipPointer:J

    .line 122
    iput v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipProxCount:I

    .line 123
    iput v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->proxCount:I

    .line 124
    iput p3, p0, Lorg/apache/lucene/index/SegmentTermPositions;->payloadLength:I

    .line 125
    iput-boolean v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->needToLoadPayload:Z

    .line 126
    return-void
.end method

.method protected final skippingDoc()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    iget v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipProxCount:I

    iget v1, p0, Lorg/apache/lucene/index/SegmentTermPositions;->freq:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/index/SegmentTermPositions;->lazySkipProxCount:I

    .line 95
    return-void
.end method
