.class public final Lorg/apache/lucene/index/CompoundFileWriter;
.super Ljava/lang/Object;
.source "CompoundFileWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/CompoundFileWriter$1;,
        Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final FORMAT_CURRENT:I = -0x1

.field static final FORMAT_NO_SEGMENT_PREFIX:I = -0x1

.field static final FORMAT_PRE_VERSION:I


# instance fields
.field private checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

.field private directory:Lorg/apache/lucene/store/Directory;

.field private entries:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;",
            ">;"
        }
    .end annotation
.end field

.field private fileName:Ljava/lang/String;

.field private ids:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private merged:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lorg/apache/lucene/index/CompoundFileWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/CompoundFileWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/index/CompoundFileWriter;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/SegmentMerger$CheckAbort;)V

    .line 92
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/SegmentMerger$CheckAbort;)V
    .locals 2
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "checkAbort"    # Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/CompoundFileWriter;->merged:Z

    .line 95
    if-nez p1, :cond_0

    .line 96
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "directory cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_0
    if-nez p2, :cond_1

    .line 98
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "name cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :cond_1
    iput-object p3, p0, Lorg/apache/lucene/index/CompoundFileWriter;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    .line 100
    iput-object p1, p0, Lorg/apache/lucene/index/CompoundFileWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 101
    iput-object p2, p0, Lorg/apache/lucene/index/CompoundFileWriter;->fileName:Ljava/lang/String;

    .line 102
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/CompoundFileWriter;->ids:Ljava/util/HashSet;

    .line 103
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/CompoundFileWriter;->entries:Ljava/util/LinkedList;

    .line 104
    return-void
.end method

.method private copyFile(Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;Lorg/apache/lucene/store/IndexOutput;)V
    .locals 12
    .param p1, "source"    # Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;
    .param p2, "os"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 231
    iget-object v5, p1, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v10, p1, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;->file:Ljava/lang/String;

    invoke-virtual {v5, v10}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    .line 233
    .local v4, "is":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v8

    .line 234
    .local v8, "startPtr":J
    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v6

    .line 235
    .local v6, "length":J
    invoke-virtual {p2, v4, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V

    .line 237
    iget-object v5, p0, Lorg/apache/lucene/index/CompoundFileWriter;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    if-eqz v5, :cond_0

    .line 238
    iget-object v5, p0, Lorg/apache/lucene/index/CompoundFileWriter;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    long-to-double v10, v6

    invoke-virtual {v5, v10, v11}, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->work(D)V

    .line 242
    :cond_0
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    .line 243
    .local v2, "endPtr":J
    sub-long v0, v2, v8

    .line 244
    .local v0, "diff":J
    cmp-long v5, v0, v6

    if-eqz v5, :cond_1

    .line 245
    new-instance v5, Ljava/io/IOException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Difference in the output file offsets "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " does not match the original file length "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    .end local v0    # "diff":J
    .end local v2    # "endPtr":J
    .end local v6    # "length":J
    .end local v8    # "startPtr":J
    :catchall_0
    move-exception v5

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->close()V

    throw v5

    .restart local v0    # "diff":J
    .restart local v2    # "endPtr":J
    .restart local v6    # "length":J
    .restart local v8    # "startPtr":J
    :cond_1
    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 251
    return-void
.end method


# virtual methods
.method public addFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    .line 125
    iget-object v0, p0, Lorg/apache/lucene/index/CompoundFileWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/CompoundFileWriter;->addFile(Ljava/lang/String;Lorg/apache/lucene/store/Directory;)V

    .line 126
    return-void
.end method

.method public addFile(Ljava/lang/String;Lorg/apache/lucene/store/Directory;)V
    .locals 4
    .param p1, "file"    # Ljava/lang/String;
    .param p2, "dir"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 133
    iget-boolean v1, p0, Lorg/apache/lucene/index/CompoundFileWriter;->merged:Z

    if-eqz v1, :cond_0

    .line 134
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Can\'t add extensions after merge has been called"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 137
    :cond_0
    if-nez p1, :cond_1

    .line 138
    new-instance v1, Ljava/lang/NullPointerException;

    const-string/jumbo v2, "file cannot be null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 141
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/CompoundFileWriter;->ids:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 142
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "File "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " already added"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 145
    :cond_2
    new-instance v0, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;-><init>(Lorg/apache/lucene/index/CompoundFileWriter$1;)V

    .line 146
    .local v0, "entry":Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;
    iput-object p1, v0, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;->file:Ljava/lang/String;

    .line 147
    iput-object p2, v0, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;->dir:Lorg/apache/lucene/store/Directory;

    .line 148
    iget-object v1, p0, Lorg/apache/lucene/index/CompoundFileWriter;->entries:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 149
    return-void
.end method

.method public close()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 158
    iget-boolean v10, p0, Lorg/apache/lucene/index/CompoundFileWriter;->merged:Z

    if-eqz v10, :cond_0

    .line 159
    new-instance v10, Ljava/lang/IllegalStateException;

    const-string/jumbo v11, "Merge already performed"

    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 161
    :cond_0
    iget-object v10, p0, Lorg/apache/lucene/index/CompoundFileWriter;->entries:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 162
    new-instance v10, Ljava/lang/IllegalStateException;

    const-string/jumbo v11, "No entries to merge have been defined"

    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 164
    :cond_1
    iput-boolean v12, p0, Lorg/apache/lucene/index/CompoundFileWriter;->merged:Z

    .line 167
    iget-object v10, p0, Lorg/apache/lucene/index/CompoundFileWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v11, p0, Lorg/apache/lucene/index/CompoundFileWriter;->fileName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v5

    .line 168
    .local v5, "os":Lorg/apache/lucene/store/IndexOutput;
    const/4 v6, 0x0

    .line 172
    .local v6, "priorException":Ljava/io/IOException;
    const/4 v10, -0x1

    :try_start_0
    invoke-virtual {v5, v10}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 175
    iget-object v10, p0, Lorg/apache/lucene/index/CompoundFileWriter;->entries:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v10

    invoke-virtual {v5, v10}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 180
    const-wide/16 v8, 0x0

    .line 181
    .local v8, "totalSize":J
    iget-object v10, p0, Lorg/apache/lucene/index/CompoundFileWriter;->entries:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;

    .line 182
    .local v1, "fe":Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;
    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v10

    iput-wide v10, v1, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;->directoryOffset:J

    .line 183
    const-wide/16 v10, 0x0

    invoke-virtual {v5, v10, v11}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 184
    iget-object v10, v1, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;->file:Ljava/lang/String;

    invoke-static {v10}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Lorg/apache/lucene/store/IndexOutput;->writeString(Ljava/lang/String;)V

    .line 185
    iget-object v10, v1, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v11, v1, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;->file:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lorg/apache/lucene/store/Directory;->fileLength(Ljava/lang/String;)J

    move-result-wide v10

    add-long/2addr v8, v10

    goto :goto_0

    .line 194
    .end local v1    # "fe":Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;
    :cond_2
    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v10

    add-long v2, v8, v10

    .line 195
    .local v2, "finalLength":J
    invoke-virtual {v5, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->setLength(J)V

    .line 199
    iget-object v10, p0, Lorg/apache/lucene/index/CompoundFileWriter;->entries:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;

    .line 200
    .restart local v1    # "fe":Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;
    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v10

    iput-wide v10, v1, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;->dataOffset:J

    .line 201
    invoke-direct {p0, v1, v5}, Lorg/apache/lucene/index/CompoundFileWriter;->copyFile(Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;Lorg/apache/lucene/store/IndexOutput;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 219
    .end local v1    # "fe":Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;
    .end local v2    # "finalLength":J
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v8    # "totalSize":J
    :catch_0
    move-exception v0

    .line 220
    .local v0, "e":Ljava/io/IOException;
    move-object v6, v0

    .line 222
    new-array v10, v12, [Ljava/io/Closeable;

    aput-object v5, v10, v13

    .end local v0    # "e":Ljava/io/IOException;
    :goto_2
    invoke-static {v6, v10}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    .line 224
    return-void

    .line 205
    .restart local v2    # "finalLength":J
    .restart local v4    # "i$":Ljava/util/Iterator;
    .restart local v8    # "totalSize":J
    :cond_3
    :try_start_1
    iget-object v10, p0, Lorg/apache/lucene/index/CompoundFileWriter;->entries:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;

    .line 206
    .restart local v1    # "fe":Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;
    iget-wide v10, v1, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;->directoryOffset:J

    invoke-virtual {v5, v10, v11}, Lorg/apache/lucene/store/IndexOutput;->seek(J)V

    .line 207
    iget-wide v10, v1, Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;->dataOffset:J

    invoke-virtual {v5, v10, v11}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 222
    .end local v1    # "fe":Lorg/apache/lucene/index/CompoundFileWriter$FileEntry;
    .end local v2    # "finalLength":J
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v8    # "totalSize":J
    :catchall_0
    move-exception v10

    new-array v11, v12, [Ljava/io/Closeable;

    aput-object v5, v11, v13

    invoke-static {v6, v11}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    throw v10

    .line 210
    .restart local v2    # "finalLength":J
    .restart local v4    # "i$":Ljava/util/Iterator;
    .restart local v8    # "totalSize":J
    :cond_4
    :try_start_2
    sget-boolean v10, Lorg/apache/lucene/index/CompoundFileWriter;->$assertionsDisabled:Z

    if-nez v10, :cond_5

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexOutput;->length()J

    move-result-wide v10

    cmp-long v10, v2, v10

    if-eqz v10, :cond_5

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 216
    :cond_5
    move-object v7, v5

    .line 217
    .local v7, "tmp":Lorg/apache/lucene/store/IndexOutput;
    const/4 v5, 0x0

    .line 218
    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexOutput;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 222
    new-array v10, v12, [Ljava/io/Closeable;

    aput-object v5, v10, v13

    goto :goto_2
.end method

.method public getDirectory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lorg/apache/lucene/index/CompoundFileWriter;->directory:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/lucene/index/CompoundFileWriter;->fileName:Ljava/lang/String;

    return-object v0
.end method
