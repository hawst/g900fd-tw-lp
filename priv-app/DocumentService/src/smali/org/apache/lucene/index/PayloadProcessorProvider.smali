.class public abstract Lorg/apache/lucene/index/PayloadProcessorProvider;
.super Ljava/lang/Object;
.source "PayloadProcessorProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/PayloadProcessorProvider$PayloadProcessor;,
        Lorg/apache/lucene/index/PayloadProcessorProvider$DirPayloadProcessor;,
        Lorg/apache/lucene/index/PayloadProcessorProvider$ReaderPayloadProcessor;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    return-void
.end method


# virtual methods
.method public getDirProcessor(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/PayloadProcessorProvider$DirPayloadProcessor;
    .locals 2
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 107
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "You must either implement getReaderProcessor() or getDirProcessor()."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getReaderProcessor(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/PayloadProcessorProvider$ReaderPayloadProcessor;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->directory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/PayloadProcessorProvider;->getDirProcessor(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/PayloadProcessorProvider$DirPayloadProcessor;

    move-result-object v0

    return-object v0
.end method
