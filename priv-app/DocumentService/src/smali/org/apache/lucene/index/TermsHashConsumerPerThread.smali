.class abstract Lorg/apache/lucene/index/TermsHashConsumerPerThread;
.super Ljava/lang/Object;
.source "TermsHashConsumerPerThread.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract abort()V
.end method

.method public abstract addField(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/TermsHashConsumerPerField;
.end method

.method abstract finishDocument()Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract startDocument()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
