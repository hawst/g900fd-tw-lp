.class Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;
.super Lorg/apache/lucene/store/RAMFile;
.source "DocumentsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PerDocBuffer"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/index/DocumentsWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 187
    const-class v0, Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/DocumentsWriter;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->this$0:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-direct {p0}, Lorg/apache/lucene/store/RAMFile;-><init>()V

    return-void
.end method


# virtual methods
.method protected newBuffer(I)[B
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 194
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const/16 v0, 0x400

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 195
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->this$0:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->perDocAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->getByteBlock()[B

    move-result-object v0

    return-object v0
.end method

.method declared-synchronized recycle()V
    .locals 2

    .prologue
    .line 202
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->buffers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 203
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->setLength(J)V

    .line 206
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->this$0:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->perDocAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->buffers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->recycleByteBlocks(Ljava/util/List;)V

    .line 207
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->buffers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 208
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->sizeInBytes:J

    .line 210
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->numBuffers()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 212
    :cond_0
    monitor-exit p0

    return-void
.end method
