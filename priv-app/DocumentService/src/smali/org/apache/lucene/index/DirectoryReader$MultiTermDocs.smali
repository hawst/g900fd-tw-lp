.class Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;
.super Ljava/lang/Object;
.source "DirectoryReader.java"

# interfaces
.implements Lorg/apache/lucene/index/TermDocs;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DirectoryReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MultiTermDocs"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected base:I

.field protected current:Lorg/apache/lucene/index/TermDocs;

.field matchingSegmentPos:I

.field protected pointer:I

.field private readerTermDocs:[Lorg/apache/lucene/index/TermDocs;

.field protected readers:[Lorg/apache/lucene/index/IndexReader;

.field smi:Lorg/apache/lucene/index/SegmentMergeInfo;

.field protected starts:[I

.field private tenum:Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

.field protected term:Lorg/apache/lucene/index/Term;

.field topReader:Lorg/apache/lucene/index/IndexReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1154
    const-class v0, Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[I)V
    .locals 1
    .param p1, "topReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "r"    # [Lorg/apache/lucene/index/IndexReader;
    .param p3, "s"    # [I

    .prologue
    const/4 v0, 0x0

    .line 1170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160
    iput v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->base:I

    .line 1161
    iput v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    .line 1171
    iput-object p1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->topReader:Lorg/apache/lucene/index/IndexReader;

    .line 1172
    iput-object p2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->readers:[Lorg/apache/lucene/index/IndexReader;

    .line 1173
    iput-object p3, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->starts:[I

    .line 1175
    array-length v0, p2

    new-array v0, v0, [Lorg/apache/lucene/index/TermDocs;

    iput-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->readerTermDocs:[Lorg/apache/lucene/index/TermDocs;

    .line 1176
    return-void
.end method

.method private termDocs(I)Lorg/apache/lucene/index/TermDocs;
    .locals 3
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1279
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->readerTermDocs:[Lorg/apache/lucene/index/TermDocs;

    aget-object v0, v1, p1

    .line 1280
    .local v0, "result":Lorg/apache/lucene/index/TermDocs;
    if-nez v0, :cond_0

    .line 1281
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->readerTermDocs:[Lorg/apache/lucene/index/TermDocs;

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->readers:[Lorg/apache/lucene/index/IndexReader;

    aget-object v2, v2, p1

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->termDocs(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    .end local v0    # "result":Lorg/apache/lucene/index/TermDocs;
    aput-object v0, v1, p1

    .line 1282
    .restart local v0    # "result":Lorg/apache/lucene/index/TermDocs;
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->smi:Lorg/apache/lucene/index/SegmentMergeInfo;

    if-eqz v1, :cond_3

    .line 1283
    sget-boolean v1, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->smi:Lorg/apache/lucene/index/SegmentMergeInfo;

    iget v1, v1, Lorg/apache/lucene/index/SegmentMergeInfo;->ord:I

    if-eq v1, p1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1284
    :cond_1
    sget-boolean v1, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->smi:Lorg/apache/lucene/index/SegmentMergeInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentMergeInfo;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1285
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->smi:Lorg/apache/lucene/index/SegmentMergeInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentMergeInfo;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-interface {v0, v1}, Lorg/apache/lucene/index/TermDocs;->seek(Lorg/apache/lucene/index/TermEnum;)V

    .line 1289
    :goto_0
    return-object v0

    .line 1287
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->term:Lorg/apache/lucene/index/Term;

    invoke-interface {v0, v1}, Lorg/apache/lucene/index/TermDocs;->seek(Lorg/apache/lucene/index/Term;)V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1298
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->readerTermDocs:[Lorg/apache/lucene/index/TermDocs;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1299
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->readerTermDocs:[Lorg/apache/lucene/index/TermDocs;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 1300
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->readerTermDocs:[Lorg/apache/lucene/index/TermDocs;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 1298
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1302
    :cond_1
    return-void
.end method

.method public doc()I
    .locals 2

    .prologue
    .line 1179
    iget v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->base:I

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->current:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public freq()I
    .locals 1

    .prologue
    .line 1182
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->current:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->freq()I

    move-result v0

    return v0
.end method

.method public next()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1206
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->current:Lorg/apache/lucene/index/TermDocs;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->current:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1207
    const/4 v0, 0x1

    .line 1221
    :cond_0
    :goto_1
    return v0

    .line 1209
    :cond_1
    iget v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->readers:[Lorg/apache/lucene/index/IndexReader;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 1210
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->tenum:Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    if-eqz v1, :cond_3

    .line 1211
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->tenum:Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    iget-object v1, v1, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->matchingSegments:[Lorg/apache/lucene/index/SegmentMergeInfo;

    iget v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->matchingSegmentPos:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->matchingSegmentPos:I

    aget-object v1, v1, v2

    iput-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->smi:Lorg/apache/lucene/index/SegmentMergeInfo;

    .line 1212
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->smi:Lorg/apache/lucene/index/SegmentMergeInfo;

    if-nez v1, :cond_2

    .line 1213
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->readers:[Lorg/apache/lucene/index/IndexReader;

    array-length v1, v1

    iput v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    goto :goto_1

    .line 1216
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->smi:Lorg/apache/lucene/index/SegmentMergeInfo;

    iget v1, v1, Lorg/apache/lucene/index/SegmentMergeInfo;->ord:I

    iput v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    .line 1218
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->starts:[I

    iget v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    aget v1, v1, v2

    iput v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->base:I

    .line 1219
    iget v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->termDocs(I)Lorg/apache/lucene/index/TermDocs;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->current:Lorg/apache/lucene/index/TermDocs;

    goto :goto_0
.end method

.method public read([I[I)I
    .locals 7
    .param p1, "docs"    # [I
    .param p2, "freqs"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1229
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->current:Lorg/apache/lucene/index/TermDocs;

    if-nez v4, :cond_4

    .line 1230
    iget v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    iget-object v5, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->readers:[Lorg/apache/lucene/index/IndexReader;

    array-length v5, v5

    if-ge v4, v5, :cond_3

    .line 1231
    iget-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->tenum:Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    if-eqz v4, :cond_2

    .line 1232
    iget-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->tenum:Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    iget-object v4, v4, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->matchingSegments:[Lorg/apache/lucene/index/SegmentMergeInfo;

    iget v5, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->matchingSegmentPos:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->matchingSegmentPos:I

    aget-object v4, v4, v5

    iput-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->smi:Lorg/apache/lucene/index/SegmentMergeInfo;

    .line 1233
    iget-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->smi:Lorg/apache/lucene/index/SegmentMergeInfo;

    if-nez v4, :cond_1

    .line 1234
    iget-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->readers:[Lorg/apache/lucene/index/IndexReader;

    array-length v4, v4

    iput v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    move v1, v3

    .line 1252
    :cond_0
    :goto_1
    return v1

    .line 1237
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->smi:Lorg/apache/lucene/index/SegmentMergeInfo;

    iget v4, v4, Lorg/apache/lucene/index/SegmentMergeInfo;->ord:I

    iput v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    .line 1239
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->starts:[I

    iget v5, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    aget v4, v4, v5

    iput v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->base:I

    .line 1240
    iget v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    invoke-direct {p0, v4}, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->termDocs(I)Lorg/apache/lucene/index/TermDocs;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->current:Lorg/apache/lucene/index/TermDocs;

    goto :goto_0

    :cond_3
    move v1, v3

    .line 1242
    goto :goto_1

    .line 1245
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->current:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v4, p1, p2}, Lorg/apache/lucene/index/TermDocs;->read([I[I)I

    move-result v1

    .line 1246
    .local v1, "end":I
    if-nez v1, :cond_5

    .line 1247
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->current:Lorg/apache/lucene/index/TermDocs;

    goto :goto_0

    .line 1249
    :cond_5
    iget v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->base:I

    .line 1250
    .local v0, "b":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v1, :cond_0

    .line 1251
    aget v3, p1, v2

    add-int/2addr v3, v0

    aput v3, p1, v2

    .line 1250
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public seek(Lorg/apache/lucene/index/Term;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1186
    iput-object p1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->term:Lorg/apache/lucene/index/Term;

    .line 1187
    iput v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->base:I

    .line 1188
    iput v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    .line 1189
    iput-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->current:Lorg/apache/lucene/index/TermDocs;

    .line 1190
    iput-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->tenum:Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    .line 1191
    iput-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->smi:Lorg/apache/lucene/index/SegmentMergeInfo;

    .line 1192
    iput v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->matchingSegmentPos:I

    .line 1193
    return-void
.end method

.method public seek(Lorg/apache/lucene/index/TermEnum;)V
    .locals 2
    .param p1, "termEnum"    # Lorg/apache/lucene/index/TermEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1196
    invoke-virtual {p1}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->seek(Lorg/apache/lucene/index/Term;)V

    .line 1197
    instance-of v0, p1, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    if-eqz v0, :cond_0

    .line 1198
    check-cast p1, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    .end local p1    # "termEnum":Lorg/apache/lucene/index/TermEnum;
    iput-object p1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->tenum:Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    .line 1199
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->topReader:Lorg/apache/lucene/index/IndexReader;

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->tenum:Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    iget-object v1, v1, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->topReader:Lorg/apache/lucene/index/IndexReader;

    if-eq v0, v1, :cond_0

    .line 1200
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->tenum:Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    .line 1202
    :cond_0
    return-void
.end method

.method public skipTo(I)Z
    .locals 5
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1260
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->current:Lorg/apache/lucene/index/TermDocs;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->current:Lorg/apache/lucene/index/TermDocs;

    iget v3, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->base:I

    sub-int v3, p1, v3

    invoke-interface {v2, v3}, Lorg/apache/lucene/index/TermDocs;->skipTo(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1261
    const/4 v1, 0x1

    .line 1274
    :cond_0
    :goto_1
    return v1

    .line 1262
    :cond_1
    iget v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->readers:[Lorg/apache/lucene/index/IndexReader;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 1263
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->tenum:Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    if-eqz v2, :cond_3

    .line 1264
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->tenum:Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    iget-object v2, v2, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->matchingSegments:[Lorg/apache/lucene/index/SegmentMergeInfo;

    iget v3, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->matchingSegmentPos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->matchingSegmentPos:I

    aget-object v0, v2, v3

    .line 1265
    .local v0, "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    if-nez v0, :cond_2

    .line 1266
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->readers:[Lorg/apache/lucene/index/IndexReader;

    array-length v2, v2

    iput v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    goto :goto_1

    .line 1269
    :cond_2
    iget v2, v0, Lorg/apache/lucene/index/SegmentMergeInfo;->ord:I

    iput v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    .line 1271
    .end local v0    # "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->starts:[I

    iget v3, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    aget v2, v2, v3

    iput v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->base:I

    .line 1272
    iget v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->pointer:I

    invoke-direct {p0, v2}, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->termDocs(I)Lorg/apache/lucene/index/TermDocs;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->current:Lorg/apache/lucene/index/TermDocs;

    goto :goto_0
.end method

.method protected termDocs(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/TermDocs;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1294
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;->term:Lorg/apache/lucene/index/Term;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/IndexReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->termDocs()Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    goto :goto_0
.end method
