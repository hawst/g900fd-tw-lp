.class final Lorg/apache/lucene/index/IndexFileDeleter$RefCount;
.super Ljava/lang/Object;
.source "IndexFileDeleter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/IndexFileDeleter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "RefCount"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field count:I

.field final fileName:Ljava/lang/String;

.field initDone:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 603
    const-class v0, Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 609
    iput-object p1, p0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->fileName:Ljava/lang/String;

    .line 610
    return-void
.end method


# virtual methods
.method public DecRef()I
    .locals 3

    .prologue
    .line 624
    sget-boolean v0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": RefCount is 0 pre-decrement for file \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->fileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 625
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    return v0
.end method

.method public IncRef()I
    .locals 3

    .prologue
    .line 615
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->initDone:Z

    if-nez v0, :cond_1

    .line 616
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->initDone:Z

    .line 620
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    return v0

    .line 618
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->count:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": RefCount is 0 pre-increment for file \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/IndexFileDeleter$RefCount;->fileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
