.class public final Lorg/apache/lucene/index/SegmentInfo;
.super Ljava/lang/Object;
.source "SegmentInfo.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final CHECK_DIR:I = 0x0

.field static final NO:I = -0x1

.field static final WITHOUT_GEN:I = 0x0

.field static final YES:I = 0x1


# instance fields
.field private bufferedDeletesGen:J

.field private delCount:I

.field private delGen:J

.field private diagnostics:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public dir:Lorg/apache/lucene/store/Directory;

.field public docCount:I

.field private docStoreIsCompoundFile:Z

.field private docStoreOffset:I

.field private docStoreSegment:Ljava/lang/String;

.field private volatile files:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasProx:Z

.field private hasSingleNormFile:Z

.field private hasVectors:Z

.field private isCompoundFile:B

.field public name:Ljava/lang/String;

.field private normGen:[J

.field private preLockless:Z

.field private volatile sizeInBytesNoStore:J

.field private volatile sizeInBytesWithStore:J

.field private version:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SegmentInfo;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;ILorg/apache/lucene/store/Directory;ZZZZ)V
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "docCount"    # I
    .param p3, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p4, "isCompoundFile"    # Z
    .param p5, "hasSingleNormFile"    # Z
    .param p6, "hasProx"    # Z
    .param p7, "hasVectors"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v1, -0x1

    const-wide/16 v2, -0x1

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytesNoStore:J

    .line 83
    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytesWithStore:J

    .line 112
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 113
    iput p2, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    .line 114
    iput-object p3, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    .line 115
    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    .line 116
    if-eqz p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    iput-byte v0, p0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:B

    .line 117
    iput-boolean v4, p0, Lorg/apache/lucene/index/SegmentInfo;->preLockless:Z

    .line 118
    iput-boolean p5, p0, Lorg/apache/lucene/index/SegmentInfo;->hasSingleNormFile:Z

    .line 119
    iput v1, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    .line 120
    iput v4, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    .line 121
    iput-boolean p6, p0, Lorg/apache/lucene/index/SegmentInfo;->hasProx:Z

    .line 122
    iput-boolean p7, p0, Lorg/apache/lucene/index/SegmentInfo;->hasVectors:Z

    .line 123
    sget-object v0, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    .line 124
    return-void

    :cond_0
    move v0, v1

    .line 116
    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;ILorg/apache/lucene/store/IndexInput;)V
    .locals 10
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "format"    # I
    .param p3, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytesNoStore:J

    .line 83
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytesWithStore:J

    .line 169
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    .line 170
    const/16 v6, -0xb

    if-gt p2, v6, :cond_0

    .line 171
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    .line 173
    :cond_0
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 174
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v6

    iput v6, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    .line 175
    const/4 v6, -0x2

    if-gt p2, v6, :cond_14

    .line 176
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    .line 177
    const/4 v6, -0x4

    if-gt p2, v6, :cond_4

    .line 178
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v6

    iput v6, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    .line 179
    iget v6, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_3

    .line 180
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    .line 181
    const/4 v6, 0x1

    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v7

    if-ne v6, v7, :cond_2

    const/4 v6, 0x1

    :goto_0
    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    .line 191
    :goto_1
    const/4 v6, -0x3

    if-gt p2, v6, :cond_6

    .line 192
    const/4 v6, 0x1

    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v7

    if-ne v6, v7, :cond_5

    const/4 v6, 0x1

    :goto_2
    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->hasSingleNormFile:Z

    .line 196
    :goto_3
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v4

    .line 197
    .local v4, "numNormGen":I
    const/4 v6, -0x1

    if-ne v4, v6, :cond_7

    .line 198
    const/4 v6, 0x0

    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    .line 205
    :cond_1
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v6

    iput-byte v6, p0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:B

    .line 206
    iget-byte v6, p0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:B

    if-nez v6, :cond_8

    const/4 v6, 0x1

    :goto_4
    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->preLockless:Z

    .line 207
    const/4 v6, -0x6

    if-gt p2, v6, :cond_9

    .line 208
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v6

    iput v6, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    .line 209
    sget-boolean v6, Lorg/apache/lucene/index/SegmentInfo;->$assertionsDisabled:Z

    if-nez v6, :cond_a

    iget v6, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    iget v7, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-le v6, v7, :cond_a

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 181
    .end local v4    # "numNormGen":I
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 183
    :cond_3
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    .line 184
    const/4 v6, 0x0

    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    goto :goto_1

    .line 187
    :cond_4
    const/4 v6, -0x1

    iput v6, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    .line 188
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    .line 189
    const/4 v6, 0x0

    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    goto :goto_1

    .line 192
    :cond_5
    const/4 v6, 0x0

    goto :goto_2

    .line 194
    :cond_6
    const/4 v6, 0x0

    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->hasSingleNormFile:Z

    goto :goto_3

    .line 200
    .restart local v4    # "numNormGen":I
    :cond_7
    new-array v6, v4, [J

    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    .line 201
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_5
    if-ge v3, v4, :cond_1

    .line 202
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v8

    aput-wide v8, v6, v3

    .line 201
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 206
    .end local v3    # "j":I
    :cond_8
    const/4 v6, 0x0

    goto :goto_4

    .line 211
    :cond_9
    const/4 v6, -0x1

    iput v6, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    .line 212
    :cond_a
    const/4 v6, -0x7

    if-gt p2, v6, :cond_d

    .line 213
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_c

    const/4 v6, 0x1

    :goto_6
    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->hasProx:Z

    .line 217
    :goto_7
    const/16 v6, -0x9

    if-gt p2, v6, :cond_e

    .line 218
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readStringStringMap()Ljava/util/Map;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->diagnostics:Ljava/util/Map;

    .line 223
    :goto_8
    const/16 v6, -0xa

    if-gt p2, v6, :cond_10

    .line 224
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_f

    const/4 v6, 0x1

    :goto_9
    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->hasVectors:Z

    .line 265
    .end local v4    # "numNormGen":I
    :cond_b
    :goto_a
    return-void

    .line 213
    .restart local v4    # "numNormGen":I
    :cond_c
    const/4 v6, 0x0

    goto :goto_6

    .line 215
    :cond_d
    const/4 v6, 0x1

    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->hasProx:Z

    goto :goto_7

    .line 220
    :cond_e
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->diagnostics:Ljava/util/Map;

    goto :goto_8

    .line 224
    :cond_f
    const/4 v6, 0x0

    goto :goto_9

    .line 229
    :cond_10
    iget v6, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_11

    .line 230
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    .line 231
    .local v5, "storesSegment":Ljava/lang/String;
    iget-boolean v2, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    .line 232
    .local v2, "isCompoundFile":Z
    const-string/jumbo v1, "cfx"

    .line 239
    .local v1, "ext":Ljava/lang/String;
    :goto_b
    if-eqz v2, :cond_12

    .line 240
    new-instance v0, Lorg/apache/lucene/index/CompoundFileReader;

    invoke-static {v5, v1}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, p1, v6}, Lorg/apache/lucene/index/CompoundFileReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    .line 245
    .local v0, "dirToTest":Lorg/apache/lucene/store/Directory;
    :goto_c
    :try_start_0
    const-string/jumbo v6, "tvx"

    invoke-static {v5, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->hasVectors:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247
    if-eqz v2, :cond_b

    .line 248
    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    goto :goto_a

    .line 234
    .end local v0    # "dirToTest":Lorg/apache/lucene/store/Directory;
    .end local v1    # "ext":Ljava/lang/String;
    .end local v2    # "isCompoundFile":Z
    .end local v5    # "storesSegment":Ljava/lang/String;
    :cond_11
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 235
    .restart local v5    # "storesSegment":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v2

    .line 236
    .restart local v2    # "isCompoundFile":Z
    const-string/jumbo v1, "cfs"

    .restart local v1    # "ext":Ljava/lang/String;
    goto :goto_b

    .line 242
    :cond_12
    move-object v0, p1

    .restart local v0    # "dirToTest":Lorg/apache/lucene/store/Directory;
    goto :goto_c

    .line 247
    :catchall_0
    move-exception v6

    if-eqz v2, :cond_13

    .line 248
    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    :cond_13
    throw v6

    .line 253
    .end local v0    # "dirToTest":Lorg/apache/lucene/store/Directory;
    .end local v1    # "ext":Ljava/lang/String;
    .end local v2    # "isCompoundFile":Z
    .end local v4    # "numNormGen":I
    .end local v5    # "storesSegment":Ljava/lang/String;
    :cond_14
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    .line 254
    const/4 v6, 0x0

    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    .line 255
    const/4 v6, 0x0

    iput-byte v6, p0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:B

    .line 256
    const/4 v6, 0x1

    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->preLockless:Z

    .line 257
    const/4 v6, 0x0

    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->hasSingleNormFile:Z

    .line 258
    const/4 v6, -0x1

    iput v6, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    .line 259
    const/4 v6, 0x0

    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    .line 260
    const/4 v6, 0x0

    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    .line 261
    const/4 v6, -0x1

    iput v6, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    .line 262
    const/4 v6, 0x1

    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->hasProx:Z

    .line 263
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfo;->diagnostics:Ljava/util/Map;

    goto :goto_a
.end method

.method private addIfExists(Ljava/util/Set;Ljava/lang/String;)V
    .locals 1
    .param p2, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 637
    .local p1, "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638
    invoke-interface {p1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 639
    :cond_0
    return-void
.end method

.method private clearFiles()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 748
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->files:Ljava/util/List;

    .line 749
    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytesNoStore:J

    .line 750
    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytesWithStore:J

    .line 751
    return-void
.end method


# virtual methods
.method advanceDelGen()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 360
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 361
    iput-wide v4, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    .line 365
    :goto_0
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentInfo;->clearFiles()V

    .line 366
    return-void

    .line 363
    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    add-long/2addr v0, v4

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    goto :goto_0
.end method

.method advanceNormGen(I)V
    .locals 6
    .param p1, "fieldIndex"    # I

    .prologue
    const-wide/16 v4, 0x1

    .line 480
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    aget-wide v0, v0, p1

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 481
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    aput-wide v4, v0, p1

    .line 485
    :goto_0
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentInfo;->clearFiles()V

    .line 486
    return-void

    .line 483
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    aget-wide v2, v0, p1

    add-long/2addr v2, v4

    aput-wide v2, v0, p1

    goto :goto_0
.end method

.method clearDelGen()V
    .locals 2

    .prologue
    .line 369
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    .line 370
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentInfo;->clearFiles()V

    .line 371
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 375
    new-instance v0, Lorg/apache/lucene/index/SegmentInfo;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget v2, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lorg/apache/lucene/index/SegmentInfo;->hasSingleNormFile:Z

    iget-boolean v6, p0, Lorg/apache/lucene/index/SegmentInfo;->hasProx:Z

    iget-boolean v7, p0, Lorg/apache/lucene/index/SegmentInfo;->hasVectors:Z

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/index/SegmentInfo;-><init>(Ljava/lang/String;ILorg/apache/lucene/store/Directory;ZZZZ)V

    .line 377
    .local v0, "si":Lorg/apache/lucene/index/SegmentInfo;
    iget v1, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    iput v1, v0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    .line 378
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    iput-object v1, v0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    .line 379
    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    iput-boolean v1, v0, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    .line 380
    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    iput-wide v2, v0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    .line 381
    iget v1, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    iput v1, v0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    .line 382
    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentInfo;->preLockless:Z

    iput-boolean v1, v0, Lorg/apache/lucene/index/SegmentInfo;->preLockless:Z

    .line 383
    iget-byte v1, p0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:B

    iput-byte v1, v0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:B

    .line 384
    new-instance v1, Ljava/util/HashMap;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfo;->diagnostics:Ljava/util/Map;

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v1, v0, Lorg/apache/lucene/index/SegmentInfo;->diagnostics:Ljava/util/Map;

    .line 385
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    if-eqz v1, :cond_0

    .line 386
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    invoke-virtual {v1}, [J->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [J

    iput-object v1, v0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    .line 388
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    iput-object v1, v0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    .line 389
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 831
    if-ne p0, p1, :cond_1

    .line 836
    :cond_0
    :goto_0
    return v1

    .line 832
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/index/SegmentInfo;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 833
    check-cast v0, Lorg/apache/lucene/index/SegmentInfo;

    .line 834
    .local v0, "other":Lorg/apache/lucene/index/SegmentInfo;
    iget-object v3, v0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    if-ne v3, v4, :cond_2

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "other":Lorg/apache/lucene/index/SegmentInfo;
    :cond_3
    move v1, v2

    .line 836
    goto :goto_0
.end method

.method public files()Ljava/util/List;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 649
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->files:Ljava/util/List;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    .line 651
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->files:Ljava/util/List;

    move-object/from16 v19, v0

    .line 742
    :goto_0
    return-object v19

    .line 654
    :cond_0
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 656
    .local v9, "filesSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v18

    .line 658
    .local v18, "useCompoundFile":Z
    if-eqz v18, :cond_2

    .line 659
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "cfs"

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 665
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    move/from16 v19, v0

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_9

    .line 668
    sget-boolean v19, Lorg/apache/lucene/index/SegmentInfo;->$assertionsDisabled:Z

    if-nez v19, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    move-object/from16 v19, v0

    if-nez v19, :cond_3

    new-instance v19, Ljava/lang/AssertionError;

    invoke-direct/range {v19 .. v19}, Ljava/lang/AssertionError;-><init>()V

    throw v19

    .line 661
    :cond_2
    sget-object v5, Lorg/apache/lucene/index/IndexFileNames;->NON_STORE_INDEX_EXTENSIONS:[Ljava/lang/String;

    .local v5, "arr$":[Ljava/lang/String;
    array-length v15, v5

    .local v15, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_1
    if-ge v14, v15, :cond_1

    aget-object v7, v5, v14

    .line 662
    .local v7, "ext":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v7}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v9, v1}, Lorg/apache/lucene/index/SegmentInfo;->addIfExists(Ljava/util/Set;Ljava/lang/String;)V

    .line 661
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 669
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v7    # "ext":Ljava/lang/String;
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    move/from16 v19, v0

    if-eqz v19, :cond_8

    .line 670
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "cfx"

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 690
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "del"

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-wide/from16 v2, v22

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v6

    .line 691
    .local v6, "delFileName":Ljava/lang/String;
    if-eqz v6, :cond_6

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x1

    cmp-long v19, v20, v22

    if-gez v19, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 692
    :cond_5
    invoke-virtual {v9, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 696
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    move-object/from16 v19, v0

    if-eqz v19, :cond_e

    .line 697
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v11, v0, :cond_12

    .line 698
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    move-object/from16 v19, v0

    aget-wide v12, v19, v11

    .line 699
    .local v12, "gen":J
    const-wide/16 v20, 0x1

    cmp-long v19, v12, v20

    if-ltz v19, :cond_a

    .line 701
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "s"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v12, v13}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 697
    :cond_7
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 672
    .end local v6    # "delFileName":Ljava/lang/String;
    .end local v11    # "i":I
    .end local v12    # "gen":J
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "fdx"

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 673
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "fdt"

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 674
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/SegmentInfo;->hasVectors:Z

    move/from16 v19, v0

    if-eqz v19, :cond_4

    .line 675
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "tvx"

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 676
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "tvd"

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 677
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "tvf"

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 680
    :cond_9
    if-nez v18, :cond_4

    .line 681
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "fdx"

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 682
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "fdt"

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 683
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/SegmentInfo;->hasVectors:Z

    move/from16 v19, v0

    if-eqz v19, :cond_4

    .line 684
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "tvx"

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 685
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "tvd"

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 686
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "tvf"

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 702
    .restart local v6    # "delFileName":Ljava/lang/String;
    .restart local v11    # "i":I
    .restart local v12    # "gen":J
    :cond_a
    const-wide/16 v20, -0x1

    cmp-long v19, v20, v12

    if-nez v19, :cond_b

    .line 705
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/SegmentInfo;->hasSingleNormFile:Z

    move/from16 v19, v0

    if-nez v19, :cond_7

    if-nez v18, :cond_7

    .line 706
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "f"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 707
    .local v8, "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 708
    invoke-virtual {v9, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 711
    .end local v8    # "fileName":Ljava/lang/String;
    :cond_b
    const-wide/16 v20, 0x0

    cmp-long v19, v20, v12

    if-nez v19, :cond_7

    .line 713
    const/4 v8, 0x0

    .line 714
    .restart local v8    # "fileName":Ljava/lang/String;
    if-eqz v18, :cond_d

    .line 715
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "s"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 719
    :cond_c
    :goto_5
    if-eqz v8, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 720
    invoke-virtual {v9, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 716
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/SegmentInfo;->hasSingleNormFile:Z

    move/from16 v19, v0

    if-nez v19, :cond_c

    .line 717
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "f"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto :goto_5

    .line 724
    .end local v8    # "fileName":Ljava/lang/String;
    .end local v11    # "i":I
    .end local v12    # "gen":J
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/SegmentInfo;->preLockless:Z

    move/from16 v19, v0

    if-nez v19, :cond_f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/SegmentInfo;->hasSingleNormFile:Z

    move/from16 v19, v0

    if-nez v19, :cond_12

    if-nez v18, :cond_12

    .line 728
    :cond_f
    if-eqz v18, :cond_11

    .line 729
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "s"

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 732
    .local v16, "prefix":Ljava/lang/String;
    :goto_6
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v17

    .line 733
    .local v17, "prefixLength":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v4

    .line 734
    .local v4, "allFiles":[Ljava/lang/String;
    invoke-static {}, Lorg/apache/lucene/index/IndexFileNameFilter;->getFilter()Lorg/apache/lucene/index/IndexFileNameFilter;

    move-result-object v10

    .line 735
    .local v10, "filter":Lorg/apache/lucene/index/IndexFileNameFilter;
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_7
    array-length v0, v4

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v11, v0, :cond_12

    .line 736
    aget-object v8, v4, v11

    .line 737
    .restart local v8    # "fileName":Ljava/lang/String;
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v10, v0, v8}, Lorg/apache/lucene/index/IndexFileNameFilter;->accept(Ljava/io/File;Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_10

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v19

    move/from16 v0, v19

    move/from16 v1, v17

    if-le v0, v1, :cond_10

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->charAt(I)C

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Character;->isDigit(C)Z

    move-result v19

    if-eqz v19, :cond_10

    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_10

    .line 738
    invoke-virtual {v9, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 735
    :cond_10
    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    .line 731
    .end local v4    # "allFiles":[Ljava/lang/String;
    .end local v8    # "fileName":Ljava/lang/String;
    .end local v10    # "filter":Lorg/apache/lucene/index/IndexFileNameFilter;
    .end local v11    # "i":I
    .end local v16    # "prefix":Ljava/lang/String;
    .end local v17    # "prefixLength":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string/jumbo v20, "f"

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .restart local v16    # "prefix":Ljava/lang/String;
    goto :goto_6

    .line 742
    .end local v16    # "prefix":Ljava/lang/String;
    :cond_12
    new-instance v19, Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-direct {v0, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/SegmentInfo;->files:Ljava/util/List;

    goto/16 :goto_0
.end method

.method getBufferedDeletesGen()J
    .locals 2

    .prologue
    .line 865
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfo;->bufferedDeletesGen:J

    return-wide v0
.end method

.method public getDelCount()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 545
    iget v1, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 546
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfo;->hasDeletions()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 547
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfo;->getDelFileName()Ljava/lang/String;

    move-result-object v0

    .line 548
    .local v0, "delFileName":Ljava/lang/String;
    new-instance v1, Lorg/apache/lucene/util/BitVector;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-direct {v1, v2, v0}, Lorg/apache/lucene/util/BitVector;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    .line 552
    .end local v0    # "delFileName":Ljava/lang/String;
    :cond_0
    :goto_0
    sget-boolean v1, Lorg/apache/lucene/index/SegmentInfo;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget v1, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    iget v2, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-le v1, v2, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 550
    :cond_1
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    goto :goto_0

    .line 553
    :cond_2
    iget v1, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    return v1
.end method

.method public getDelFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 393
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 396
    const/4 v0, 0x0

    .line 399
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string/jumbo v1, "del"

    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDiagnostics()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->diagnostics:Ljava/util/Map;

    return-object v0
.end method

.method public getDocStoreIsCompoundFile()Z
    .locals 1

    .prologue
    .line 566
    iget-boolean v0, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    return v0
.end method

.method public getDocStoreOffset()I
    .locals 1

    .prologue
    .line 562
    iget v0, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    return v0
.end method

.method public getDocStoreSegment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    return-object v0
.end method

.method public getHasProx()Z
    .locals 1

    .prologue
    .line 633
    iget-boolean v0, p0, Lorg/apache/lucene/index/SegmentInfo;->hasProx:Z

    return v0
.end method

.method public getHasVectors()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 325
    iget-boolean v0, p0, Lorg/apache/lucene/index/SegmentInfo;->hasVectors:Z

    return v0
.end method

.method public getNormFileName(I)Ljava/lang/String;
    .locals 8
    .param p1, "number"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 495
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    if-nez v2, :cond_0

    .line 496
    const-wide/16 v0, 0x0

    .line 501
    .local v0, "gen":J
    :goto_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfo;->hasSeparateNorms(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 503
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0, v1}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v2

    .line 512
    :goto_1
    return-object v2

    .line 498
    .end local v0    # "gen":J
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    aget-wide v0, v2, p1

    .restart local v0    # "gen":J
    goto :goto_0

    .line 506
    :cond_1
    iget-boolean v2, p0, Lorg/apache/lucene/index/SegmentInfo;->hasSingleNormFile:Z

    if-eqz v2, :cond_2

    .line 508
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string/jumbo v3, "nrm"

    invoke-static {v2, v3, v6, v7}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 512
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "f"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6, v7}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public getUseCompoundFile()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 535
    iget-byte v1, p0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:B

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 536
    const/4 v0, 0x0

    .line 540
    :cond_0
    :goto_0
    return v0

    .line 537
    :cond_1
    iget-byte v1, p0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:B

    if-eq v1, v0, :cond_0

    .line 540
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string/jumbo v2, "cfs"

    invoke-static {v1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 861
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    return-object v0
.end method

.method public hasDeletions()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 349
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 350
    const/4 v0, 0x0

    .line 354
    :goto_0
    return v0

    .line 351
    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 352
    const/4 v0, 0x1

    goto :goto_0

    .line 354
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfo;->getDelFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public hasSeparateNorms()Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 426
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    if-nez v8, :cond_4

    .line 427
    iget-boolean v8, p0, Lorg/apache/lucene/index/SegmentInfo;->preLockless:Z

    if-nez v8, :cond_1

    .line 470
    :cond_0
    :goto_0
    return v6

    .line 435
    :cond_1
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v8}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v5

    .line 436
    .local v5, "result":[Ljava/lang/String;
    if-nez v5, :cond_2

    .line 437
    new-instance v6, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "cannot read directory "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ": listAll() returned null"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 439
    :cond_2
    invoke-static {}, Lorg/apache/lucene/index/IndexFileNameFilter;->getFilter()Lorg/apache/lucene/index/IndexFileNameFilter;

    move-result-object v1

    .line 441
    .local v1, "filter":Lorg/apache/lucene/index/IndexFileNameFilter;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ".s"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 442
    .local v3, "pattern":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    .line 443
    .local v4, "patternLength":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v8, v5

    if-ge v2, v8, :cond_0

    .line 444
    aget-object v0, v5, v2

    .line 445
    .local v0, "fileName":Ljava/lang/String;
    const/4 v8, 0x0

    invoke-virtual {v1, v8, v0}, Lorg/apache/lucene/index/IndexFileNameFilter;->accept(Ljava/io/File;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Ljava/lang/Character;->isDigit(C)Z

    move-result v8

    if-eqz v8, :cond_3

    move v6, v7

    .line 446
    goto :goto_0

    .line 443
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 454
    .end local v0    # "fileName":Ljava/lang/String;
    .end local v1    # "filter":Lorg/apache/lucene/index/IndexFileNameFilter;
    .end local v2    # "i":I
    .end local v3    # "pattern":Ljava/lang/String;
    .end local v4    # "patternLength":I
    .end local v5    # "result":[Ljava/lang/String;
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    array-length v8, v8

    if-ge v2, v8, :cond_6

    .line 455
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    aget-wide v8, v8, v2

    const-wide/16 v10, 0x1

    cmp-long v8, v8, v10

    if-ltz v8, :cond_5

    move v6, v7

    .line 456
    goto :goto_0

    .line 454
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 461
    :cond_6
    const/4 v2, 0x0

    :goto_3
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    array-length v8, v8

    if-ge v2, v8, :cond_0

    .line 462
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    aget-wide v8, v8, v2

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_7

    .line 463
    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/SegmentInfo;->hasSeparateNorms(I)Z

    move-result v8

    if-eqz v8, :cond_7

    move v6, v7

    .line 464
    goto/16 :goto_0

    .line 461
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public hasSeparateNorms(I)Z
    .locals 6
    .param p1, "fieldNumber"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 410
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentInfo;->preLockless:Z

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    aget-wide v2, v1, p1

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 412
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 413
    .local v0, "fileName":Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v1

    .line 417
    .end local v0    # "fileName":Ljava/lang/String;
    :goto_0
    return v1

    .line 414
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    aget-wide v2, v1, p1

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_4

    .line 415
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 417
    :cond_4
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 842
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method reset(Lorg/apache/lucene/index/SegmentInfo;)V
    .locals 4
    .param p1, "src"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    const/4 v3, 0x0

    .line 130
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentInfo;->clearFiles()V

    .line 131
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    .line 132
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 133
    iget v0, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    iput v0, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    .line 134
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    .line 135
    iget-boolean v0, p1, Lorg/apache/lucene/index/SegmentInfo;->preLockless:Z

    iput-boolean v0, p0, Lorg/apache/lucene/index/SegmentInfo;->preLockless:Z

    .line 136
    iget-wide v0, p1, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    .line 137
    iget v0, p1, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    iput v0, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    .line 138
    iget-boolean v0, p1, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    iput-boolean v0, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    .line 139
    iget-boolean v0, p1, Lorg/apache/lucene/index/SegmentInfo;->hasVectors:Z

    iput-boolean v0, p0, Lorg/apache/lucene/index/SegmentInfo;->hasVectors:Z

    .line 140
    iget-boolean v0, p1, Lorg/apache/lucene/index/SegmentInfo;->hasProx:Z

    iput-boolean v0, p0, Lorg/apache/lucene/index/SegmentInfo;->hasProx:Z

    .line 141
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    if-nez v0, :cond_0

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    .line 147
    :goto_0
    iget-byte v0, p1, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:B

    iput-byte v0, p0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:B

    .line 148
    iget-boolean v0, p1, Lorg/apache/lucene/index/SegmentInfo;->hasSingleNormFile:Z

    iput-boolean v0, p0, Lorg/apache/lucene/index/SegmentInfo;->hasSingleNormFile:Z

    .line 149
    iget v0, p1, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    iput v0, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    .line 150
    return-void

    .line 144
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    array-length v0, v0

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    .line 145
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method setBufferedDeletesGen(J)V
    .locals 1
    .param p1, "v"    # J

    .prologue
    .line 869
    iput-wide p1, p0, Lorg/apache/lucene/index/SegmentInfo;->bufferedDeletesGen:J

    .line 870
    return-void
.end method

.method setDelCount(I)V
    .locals 1
    .param p1, "delCount"    # I

    .prologue
    .line 557
    iput p1, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    .line 558
    sget-boolean v0, Lorg/apache/lucene/index/SegmentInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-le p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 559
    :cond_0
    return-void
.end method

.method setDiagnostics(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 153
    .local p1, "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentInfo;->diagnostics:Ljava/util/Map;

    .line 154
    return-void
.end method

.method setDocStore(ILjava/lang/String;Z)V
    .locals 0
    .param p1, "offset"    # I
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "isCompoundFile"    # Z

    .prologue
    .line 588
    iput p1, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    .line 589
    iput-object p2, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    .line 590
    iput-boolean p3, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    .line 591
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentInfo;->clearFiles()V

    .line 592
    return-void
.end method

.method setDocStoreIsCompoundFile(Z)V
    .locals 0
    .param p1, "v"    # Z

    .prologue
    .line 570
    iput-boolean p1, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    .line 571
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentInfo;->clearFiles()V

    .line 572
    return-void
.end method

.method setDocStoreOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 583
    iput p1, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    .line 584
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentInfo;->clearFiles()V

    .line 585
    return-void
.end method

.method public setDocStoreSegment(Ljava/lang/String;)V
    .locals 0
    .param p1, "segment"    # Ljava/lang/String;

    .prologue
    .line 579
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    .line 580
    return-void
.end method

.method setHasProx(Z)V
    .locals 0
    .param p1, "hasProx"    # Z

    .prologue
    .line 628
    iput-boolean p1, p0, Lorg/apache/lucene/index/SegmentInfo;->hasProx:Z

    .line 629
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentInfo;->clearFiles()V

    .line 630
    return-void
.end method

.method public setHasVectors(Z)V
    .locals 0
    .param p1, "v"    # Z

    .prologue
    .line 329
    iput-boolean p1, p0, Lorg/apache/lucene/index/SegmentInfo;->hasVectors:Z

    .line 330
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentInfo;->clearFiles()V

    .line 331
    return-void
.end method

.method setNumFields(I)V
    .locals 4
    .param p1, "numFields"    # I

    .prologue
    .line 268
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    if-nez v1, :cond_0

    .line 272
    new-array v1, p1, [J

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    .line 274
    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentInfo;->preLockless:Z

    if-eqz v1, :cond_1

    .line 286
    :cond_0
    return-void

    .line 281
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 282
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    .line 281
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method setUseCompoundFile(Z)V
    .locals 1
    .param p1, "isCompoundFile"    # Z

    .prologue
    .line 522
    if-eqz p1, :cond_0

    .line 523
    const/4 v0, 0x1

    iput-byte v0, p0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:B

    .line 527
    :goto_0
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentInfo;->clearFiles()V

    .line 528
    return-void

    .line 525
    :cond_0
    const/4 v0, -0x1

    iput-byte v0, p0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:B

    goto :goto_0
.end method

.method setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    .line 856
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    .line 857
    return-void
.end method

.method public sizeInBytes(Z)J
    .locals 8
    .param p1, "includeDocStores"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, -0x1

    .line 294
    if-eqz p1, :cond_4

    .line 295
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytesWithStore:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 296
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytesWithStore:J

    .line 320
    :goto_0
    return-wide v4

    .line 298
    :cond_0
    const-wide/16 v2, 0x0

    .line 299
    .local v2, "sum":J
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 302
    .local v0, "fileName":Ljava/lang/String;
    iget v4, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    invoke-static {v0}, Lorg/apache/lucene/index/IndexFileNames;->isDocStoreFile(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 303
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4, v0}, Lorg/apache/lucene/store/Directory;->fileLength(Ljava/lang/String;)J

    move-result-wide v4

    add-long/2addr v2, v4

    goto :goto_1

    .line 306
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_3
    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytesWithStore:J

    .line 307
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytesWithStore:J

    goto :goto_0

    .line 309
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "sum":J
    :cond_4
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytesNoStore:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_5

    .line 310
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytesNoStore:J

    goto :goto_0

    .line 312
    :cond_5
    const-wide/16 v2, 0x0

    .line 313
    .restart local v2    # "sum":J
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_6
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 314
    .restart local v0    # "fileName":Ljava/lang/String;
    invoke-static {v0}, Lorg/apache/lucene/index/IndexFileNames;->isDocStoreFile(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 317
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4, v0}, Lorg/apache/lucene/store/Directory;->fileLength(Ljava/lang/String;)J

    move-result-wide v4

    add-long/2addr v2, v4

    goto :goto_2

    .line 319
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_7
    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytesNoStore:J

    .line 320
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytesNoStore:J

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 756
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/index/SegmentInfo;->toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;
    .locals 7
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "pendingDelCount"    # I

    .prologue
    const/4 v6, -0x1

    .line 773
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 774
    .local v3, "s":Ljava/lang/StringBuilder;
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x28

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    if-nez v4, :cond_5

    const-string/jumbo v4, "?"

    :goto_0
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x29

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x3a

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 778
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_6

    .line 779
    const/16 v0, 0x63

    .line 786
    .local v0, "cfs":C
    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 788
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    if-eq v4, p1, :cond_0

    .line 789
    const/16 v4, 0x78

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 791
    :cond_0
    iget-boolean v4, p0, Lorg/apache/lucene/index/SegmentInfo;->hasVectors:Z

    if-eqz v4, :cond_1

    .line 792
    const/16 v4, 0x76

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 794
    :cond_1
    iget v4, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 798
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 802
    .local v1, "delCount":I
    :goto_2
    if-eq v1, v6, :cond_2

    .line 803
    add-int/2addr v1, p2

    .line 805
    :cond_2
    if-eqz v1, :cond_3

    .line 806
    const/16 v4, 0x2f

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 807
    if-ne v1, v6, :cond_7

    .line 808
    const/16 v4, 0x3f

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 814
    :cond_3
    :goto_3
    iget v4, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    if-eq v4, v6, :cond_4

    .line 815
    const-string/jumbo v4, "->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 816
    iget-boolean v4, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    if-eqz v4, :cond_8

    .line 817
    const/16 v4, 0x63

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 821
    :goto_4
    const/16 v4, 0x2b

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 824
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 774
    .end local v0    # "cfs":C
    .end local v1    # "delCount":I
    :cond_5
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    goto :goto_0

    .line 781
    :cond_6
    const/16 v0, 0x43

    .restart local v0    # "cfs":C
    goto :goto_1

    .line 783
    .end local v0    # "cfs":C
    :catch_0
    move-exception v2

    .line 784
    .local v2, "ioe":Ljava/io/IOException;
    const/16 v0, 0x3f

    .restart local v0    # "cfs":C
    goto :goto_1

    .line 799
    .end local v2    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 800
    .restart local v2    # "ioe":Ljava/io/IOException;
    const/4 v1, -0x1

    .restart local v1    # "delCount":I
    goto :goto_2

    .line 810
    .end local v2    # "ioe":Ljava/io/IOException;
    :cond_7
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 819
    :cond_8
    const/16 v4, 0x43

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method write(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 7
    .param p1, "output"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 599
    sget-boolean v1, Lorg/apache/lucene/index/SegmentInfo;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    iget v4, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-le v1, v4, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "delCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " docCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " segment="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 601
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->version:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/IndexOutput;->writeString(Ljava/lang/String;)V

    .line 602
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/IndexOutput;->writeString(Ljava/lang/String;)V

    .line 603
    iget v1, p0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 604
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfo;->delGen:J

    invoke-virtual {p1, v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 605
    iget v1, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 606
    iget v1, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreOffset:I

    if-eq v1, v6, :cond_1

    .line 607
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreSegment:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/IndexOutput;->writeString(Ljava/lang/String;)V

    .line 608
    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentInfo;->docStoreIsCompoundFile:Z

    if-eqz v1, :cond_3

    move v1, v2

    :goto_0
    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 611
    :cond_1
    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentInfo;->hasSingleNormFile:Z

    if-eqz v1, :cond_4

    move v1, v2

    :goto_1
    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 612
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    if-nez v1, :cond_5

    .line 613
    invoke-virtual {p1, v6}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 620
    :cond_2
    iget-byte v1, p0, Lorg/apache/lucene/index/SegmentInfo;->isCompoundFile:B

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 621
    iget v1, p0, Lorg/apache/lucene/index/SegmentInfo;->delCount:I

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 622
    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentInfo;->hasProx:Z

    if-eqz v1, :cond_6

    move v1, v2

    :goto_2
    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 623
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->diagnostics:Ljava/util/Map;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/IndexOutput;->writeStringStringMap(Ljava/util/Map;)V

    .line 624
    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentInfo;->hasVectors:Z

    if-eqz v1, :cond_7

    :goto_3
    int-to-byte v1, v2

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 625
    return-void

    :cond_3
    move v1, v3

    .line 608
    goto :goto_0

    :cond_4
    move v1, v3

    .line 611
    goto :goto_1

    .line 615
    :cond_5
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    array-length v1, v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 616
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_4
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 617
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfo;->normGen:[J

    aget-wide v4, v1, v0

    invoke-virtual {p1, v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 616
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .end local v0    # "j":I
    :cond_6
    move v1, v3

    .line 622
    goto :goto_2

    :cond_7
    move v2, v3

    .line 624
    goto :goto_3
.end method
