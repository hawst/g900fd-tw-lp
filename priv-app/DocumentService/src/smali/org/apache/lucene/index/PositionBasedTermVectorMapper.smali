.class public Lorg/apache/lucene/index/PositionBasedTermVectorMapper;
.super Lorg/apache/lucene/index/TermVectorMapper;
.source "PositionBasedTermVectorMapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;
    }
.end annotation


# instance fields
.field private currentField:Ljava/lang/String;

.field private currentPositions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private fieldToTerms:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private storeOffsets:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0, v0, v0}, Lorg/apache/lucene/index/TermVectorMapper;-><init>(ZZ)V

    .line 48
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "ignoringOffsets"    # Z

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/index/TermVectorMapper;-><init>(ZZ)V

    .line 53
    return-void
.end method


# virtual methods
.method public getFieldToTerms()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper;->fieldToTerms:Ljava/util/Map;

    return-object v0
.end method

.method public isIgnoringPositions()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public map(Ljava/lang/String;I[Lorg/apache/lucene/index/TermVectorOffsetInfo;[I)V
    .locals 5
    .param p1, "term"    # Ljava/lang/String;
    .param p2, "frequency"    # I
    .param p3, "offsets"    # [Lorg/apache/lucene/index/TermVectorOffsetInfo;
    .param p4, "positions"    # [I

    .prologue
    .line 73
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p4

    if-ge v0, v3, :cond_2

    .line 74
    aget v3, p4, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 75
    .local v2, "posVal":Ljava/lang/Integer;
    iget-object v3, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper;->currentPositions:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;

    .line 76
    .local v1, "pos":Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;
    if-nez v1, :cond_0

    .line 77
    new-instance v1, Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;

    .end local v1    # "pos":Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;
    aget v3, p4, v0

    iget-boolean v4, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper;->storeOffsets:Z

    invoke-direct {v1, v3, v4}, Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;-><init>(IZ)V

    .line 78
    .restart local v1    # "pos":Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;
    iget-object v3, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper;->currentPositions:Ljava/util/Map;

    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    :cond_0
    if-eqz p3, :cond_1

    aget-object v3, p3, v0

    :goto_1
    invoke-virtual {v1, p1, v3}, Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;->addTerm(Ljava/lang/String;Lorg/apache/lucene/index/TermVectorOffsetInfo;)V

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 82
    .end local v1    # "pos":Lorg/apache/lucene/index/PositionBasedTermVectorMapper$TVPositionInfo;
    .end local v2    # "posVal":Ljava/lang/Integer;
    :cond_2
    return-void
.end method

.method public setExpectations(Ljava/lang/String;IZZ)V
    .locals 3
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "numTerms"    # I
    .param p3, "storeOffsets"    # Z
    .param p4, "storePositions"    # Z

    .prologue
    .line 93
    if-nez p4, :cond_0

    .line 95
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "You must store positions in order to use this Mapper"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_0
    const/4 v0, 0x1

    if-ne p3, v0, :cond_1

    .line 101
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper;->fieldToTerms:Ljava/util/Map;

    .line 102
    iput-boolean p3, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper;->storeOffsets:Z

    .line 103
    iput-object p1, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper;->currentField:Ljava/lang/String;

    .line 104
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper;->currentPositions:Ljava/util/Map;

    .line 105
    iget-object v0, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper;->fieldToTerms:Ljava/util/Map;

    iget-object v1, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper;->currentField:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/index/PositionBasedTermVectorMapper;->currentPositions:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    return-void
.end method
