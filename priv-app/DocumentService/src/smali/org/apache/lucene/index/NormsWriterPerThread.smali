.class final Lorg/apache/lucene/index/NormsWriterPerThread;
.super Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;
.source "NormsWriterPerThread.java"


# instance fields
.field final docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

.field final normsWriter:Lorg/apache/lucene/index/NormsWriter;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocInverterPerThread;Lorg/apache/lucene/index/NormsWriter;)V
    .locals 1
    .param p1, "docInverterPerThread"    # Lorg/apache/lucene/index/DocInverterPerThread;
    .param p2, "normsWriter"    # Lorg/apache/lucene/index/NormsWriter;

    .prologue
    .line 24
    invoke-direct {p0}, Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;-><init>()V

    .line 25
    iput-object p2, p0, Lorg/apache/lucene/index/NormsWriterPerThread;->normsWriter:Lorg/apache/lucene/index/NormsWriter;

    .line 26
    iget-object v0, p1, Lorg/apache/lucene/index/DocInverterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/NormsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 27
    return-void
.end method


# virtual methods
.method abort()V
    .locals 0

    .prologue
    .line 35
    return-void
.end method

.method addField(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;
    .locals 1
    .param p1, "docInverterPerField"    # Lorg/apache/lucene/index/DocInverterPerField;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 31
    new-instance v0, Lorg/apache/lucene/index/NormsWriterPerField;

    invoke-direct {v0, p1, p0, p2}, Lorg/apache/lucene/index/NormsWriterPerField;-><init>(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/NormsWriterPerThread;Lorg/apache/lucene/index/FieldInfo;)V

    return-object v0
.end method

.method finishDocument()V
    .locals 0

    .prologue
    .line 40
    return-void
.end method

.method freeRAM()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method startDocument()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method
