.class Lorg/apache/lucene/index/TermVectorsReader;
.super Ljava/lang/Object;
.source "TermVectorsReader.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final FORMAT_CURRENT:I = 0x4

.field static final FORMAT_SIZE:I = 0x4

.field static final FORMAT_UTF8_LENGTH_IN_BYTES:I = 0x4

.field static final FORMAT_VERSION:I = 0x2

.field static final FORMAT_VERSION2:I = 0x3

.field static final STORE_OFFSET_WITH_TERMVECTOR:B = 0x2t

.field static final STORE_POSITIONS_WITH_TERMVECTOR:B = 0x1t


# instance fields
.field private docStoreOffset:I

.field private fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private final format:I

.field private numTotalDocs:I

.field private size:I

.field private tvd:Lorg/apache/lucene/store/IndexInput;

.field private tvf:Lorg/apache/lucene/store/IndexInput;

.field private tvx:Lorg/apache/lucene/store/IndexInput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/index/TermVectorsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermVectorsReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 1
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    const/16 v0, 0x400

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/index/TermVectorsReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;I)V

    .line 68
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;I)V
    .locals 7
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "readBufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/TermVectorsReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;III)V

    .line 73
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;III)V
    .locals 12
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "readBufferSize"    # I
    .param p5, "docStoreOffset"    # I
    .param p6, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    const/4 v4, 0x0

    .line 80
    .local v4, "success":Z
    :try_start_0
    const-string/jumbo v7, "tvx"

    invoke-static {p2, v7}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 81
    .local v3, "idxName":Ljava/lang/String;
    move/from16 v0, p4

    invoke-virtual {p1, v3, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    .line 82
    iget-object v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-direct {p0, v3, v7}, Lorg/apache/lucene/index/TermVectorsReader;->checkValidFormat(Ljava/lang/String;Lorg/apache/lucene/store/IndexInput;)I

    move-result v7

    iput v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->format:I

    .line 83
    const-string/jumbo v7, "tvd"

    invoke-static {p2, v7}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "fn":Ljava/lang/String;
    move/from16 v0, p4

    invoke-virtual {p1, v2, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    .line 85
    iget-object v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-direct {p0, v2, v7}, Lorg/apache/lucene/index/TermVectorsReader;->checkValidFormat(Ljava/lang/String;Lorg/apache/lucene/store/IndexInput;)I

    move-result v5

    .line 86
    .local v5, "tvdFormat":I
    const-string/jumbo v7, "tvf"

    invoke-static {p2, v7}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 87
    move/from16 v0, p4

    invoke-virtual {p1, v2, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    .line 88
    iget-object v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-direct {p0, v2, v7}, Lorg/apache/lucene/index/TermVectorsReader;->checkValidFormat(Ljava/lang/String;Lorg/apache/lucene/store/IndexInput;)I

    move-result v6

    .line 90
    .local v6, "tvfFormat":I
    sget-boolean v7, Lorg/apache/lucene/index/TermVectorsReader;->$assertionsDisabled:Z

    if-nez v7, :cond_1

    iget v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->format:I

    if-eq v7, v5, :cond_1

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    .end local v2    # "fn":Ljava/lang/String;
    .end local v3    # "idxName":Ljava/lang/String;
    .end local v5    # "tvdFormat":I
    .end local v6    # "tvfFormat":I
    :catchall_0
    move-exception v7

    if-nez v4, :cond_0

    .line 121
    invoke-virtual {p0}, Lorg/apache/lucene/index/TermVectorsReader;->close()V

    :cond_0
    throw v7

    .line 91
    .restart local v2    # "fn":Ljava/lang/String;
    .restart local v3    # "idxName":Ljava/lang/String;
    .restart local v5    # "tvdFormat":I
    .restart local v6    # "tvfFormat":I
    :cond_1
    :try_start_1
    sget-boolean v7, Lorg/apache/lucene/index/TermVectorsReader;->$assertionsDisabled:Z

    if-nez v7, :cond_2

    iget v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->format:I

    if-eq v7, v6, :cond_2

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 93
    :cond_2
    iget v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->format:I

    const/4 v8, 0x3

    if-lt v7, v8, :cond_3

    .line 94
    iget-object v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v8

    const/4 v7, 0x4

    shr-long/2addr v8, v7

    long-to-int v7, v8

    iput v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->numTotalDocs:I

    .line 100
    :goto_0
    const/4 v7, -0x1

    move/from16 v0, p5

    if-ne v7, v0, :cond_5

    .line 101
    const/4 v7, 0x0

    iput v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->docStoreOffset:I

    .line 102
    iget v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->numTotalDocs:I

    iput v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->size:I

    .line 103
    sget-boolean v7, Lorg/apache/lucene/index/TermVectorsReader;->$assertionsDisabled:Z

    if-nez v7, :cond_6

    if-eqz p6, :cond_6

    iget v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->numTotalDocs:I

    move/from16 v0, p6

    if-eq v7, v0, :cond_6

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 96
    :cond_3
    sget-boolean v7, Lorg/apache/lucene/index/TermVectorsReader;->$assertionsDisabled:Z

    if-nez v7, :cond_4

    iget-object v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v8

    const-wide/16 v10, 0x4

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x8

    rem-long/2addr v8, v10

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_4

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 97
    :cond_4
    iget-object v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v8

    const/4 v7, 0x3

    shr-long/2addr v8, v7

    long-to-int v7, v8

    iput v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->numTotalDocs:I

    goto :goto_0

    .line 105
    :cond_5
    move/from16 v0, p5

    iput v0, p0, Lorg/apache/lucene/index/TermVectorsReader;->docStoreOffset:I

    .line 106
    move/from16 v0, p6

    iput v0, p0, Lorg/apache/lucene/index/TermVectorsReader;->size:I

    .line 109
    sget-boolean v7, Lorg/apache/lucene/index/TermVectorsReader;->$assertionsDisabled:Z

    if-nez v7, :cond_6

    iget v7, p0, Lorg/apache/lucene/index/TermVectorsReader;->numTotalDocs:I

    add-int v8, p6, p5

    if-ge v7, v8, :cond_6

    new-instance v7, Ljava/lang/AssertionError;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "numTotalDocs="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lorg/apache/lucene/index/TermVectorsReader;->numTotalDocs:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " size="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p6

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " docStoreOffset="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p5

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v7

    .line 112
    :cond_6
    iput-object p3, p0, Lorg/apache/lucene/index/TermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113
    const/4 v4, 0x1

    .line 120
    if-nez v4, :cond_7

    .line 121
    invoke-virtual {p0}, Lorg/apache/lucene/index/TermVectorsReader;->close()V

    .line 124
    :cond_7
    return-void
.end method

.method private checkValidFormat(Ljava/lang/String;Lorg/apache/lucene/store/IndexInput;)I
    .locals 4
    .param p1, "fn"    # Ljava/lang/String;
    .param p2, "in"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    .line 199
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    .line 200
    .local v0, "format":I
    if-le v0, v3, :cond_0

    .line 201
    new-instance v1, Lorg/apache/lucene/index/IndexFormatTooNewException;

    const/4 v2, 0x1

    invoke-direct {v1, p2, v0, v2, v3}, Lorg/apache/lucene/index/IndexFormatTooNewException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v1

    .line 203
    :cond_0
    return v0
.end method

.method private final readFields(I)[Ljava/lang/String;
    .locals 5
    .param p1, "fieldCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 289
    const/4 v2, 0x0

    .line 290
    .local v2, "number":I
    new-array v0, p1, [Ljava/lang/String;

    .line 292
    .local v0, "fields":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_1

    .line 293
    iget v3, p0, Lorg/apache/lucene/index/TermVectorsReader;->format:I

    const/4 v4, 0x2

    if-lt v3, v4, :cond_0

    .line 294
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    .line 298
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/index/FieldInfos;->fieldName(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    .line 292
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 296
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    .line 301
    :cond_1
    return-object v0
.end method

.method private readTermVector(Ljava/lang/String;JLorg/apache/lucene/index/TermVectorMapper;)V
    .locals 26
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "tvfPointer"    # J
    .param p4, "mapper"    # Lorg/apache/lucene/index/TermVectorMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 412
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v12

    .line 415
    .local v12, "numTerms":I
    if-nez v12, :cond_1

    .line 512
    :cond_0
    return-void

    .line 421
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->format:I

    move/from16 v24, v0

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_5

    .line 422
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v4

    .line 423
    .local v4, "bits":B
    and-int/lit8 v24, v4, 0x1

    if-eqz v24, :cond_3

    const/16 v21, 0x1

    .line 424
    .local v21, "storePositions":Z
    :goto_0
    and-int/lit8 v24, v4, 0x2

    if-eqz v24, :cond_4

    const/16 v20, 0x1

    .line 431
    .end local v4    # "bits":B
    .local v20, "storeOffsets":Z
    :goto_1
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v12, v2, v3}, Lorg/apache/lucene/index/TermVectorMapper;->setExpectations(Ljava/lang/String;IZZ)V

    .line 432
    const/16 v18, 0x0

    .line 433
    .local v18, "start":I
    const/4 v7, 0x0

    .line 434
    .local v7, "deltaLength":I
    const/16 v23, 0x0

    .line 437
    .local v23, "totalLength":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->format:I

    move/from16 v24, v0

    const/16 v25, 0x4

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_6

    const/4 v15, 0x1

    .line 440
    .local v15, "preUTF8":Z
    :goto_2
    if-eqz v15, :cond_7

    .line 441
    const/16 v24, 0xa

    move/from16 v0, v24

    new-array v6, v0, [C

    .line 442
    .local v6, "charBuffer":[C
    const/4 v5, 0x0

    .line 448
    .local v5, "byteBuffer":[B
    :goto_3
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_4
    if-ge v10, v12, :cond_0

    .line 449
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v18

    .line 450
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v7

    .line 451
    add-int v23, v18, v7

    .line 455
    if-eqz v15, :cond_8

    .line 457
    array-length v0, v6

    move/from16 v24, v0

    move/from16 v0, v24

    move/from16 v1, v23

    if-ge v0, v1, :cond_2

    .line 458
    move/from16 v0, v23

    invoke-static {v6, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([CI)[C

    move-result-object v6

    .line 460
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1, v7}, Lorg/apache/lucene/store/IndexInput;->readChars([CII)V

    .line 461
    new-instance v22, Ljava/lang/String;

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-direct {v0, v6, v1, v2}, Ljava/lang/String;-><init>([CII)V

    .line 470
    .local v22, "term":Ljava/lang/String;
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v9

    .line 471
    .local v9, "freq":I
    const/4 v14, 0x0

    .line 472
    .local v14, "positions":[I
    if-eqz v21, :cond_b

    .line 474
    invoke-virtual/range {p4 .. p4}, Lorg/apache/lucene/index/TermVectorMapper;->isIgnoringPositions()Z

    move-result v24

    if-nez v24, :cond_a

    .line 475
    new-array v14, v9, [I

    .line 476
    const/16 v17, 0x0

    .line 477
    .local v17, "prevPosition":I
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_6
    if-ge v11, v9, :cond_b

    .line 479
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v24

    add-int v24, v24, v17

    aput v24, v14, v11

    .line 480
    aget v17, v14, v11

    .line 477
    add-int/lit8 v11, v11, 0x1

    goto :goto_6

    .line 423
    .end local v5    # "byteBuffer":[B
    .end local v6    # "charBuffer":[C
    .end local v7    # "deltaLength":I
    .end local v9    # "freq":I
    .end local v10    # "i":I
    .end local v11    # "j":I
    .end local v14    # "positions":[I
    .end local v15    # "preUTF8":Z
    .end local v17    # "prevPosition":I
    .end local v18    # "start":I
    .end local v20    # "storeOffsets":Z
    .end local v21    # "storePositions":Z
    .end local v22    # "term":Ljava/lang/String;
    .end local v23    # "totalLength":I
    .restart local v4    # "bits":B
    :cond_3
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 424
    .restart local v21    # "storePositions":Z
    :cond_4
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 427
    .end local v4    # "bits":B
    .end local v21    # "storePositions":Z
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    .line 428
    const/16 v21, 0x0

    .line 429
    .restart local v21    # "storePositions":Z
    const/16 v20, 0x0

    .restart local v20    # "storeOffsets":Z
    goto/16 :goto_1

    .line 437
    .restart local v7    # "deltaLength":I
    .restart local v18    # "start":I
    .restart local v23    # "totalLength":I
    :cond_6
    const/4 v15, 0x0

    goto/16 :goto_2

    .line 444
    .restart local v15    # "preUTF8":Z
    :cond_7
    const/4 v6, 0x0

    .line 445
    .restart local v6    # "charBuffer":[C
    const/16 v24, 0x14

    move/from16 v0, v24

    new-array v5, v0, [B

    .restart local v5    # "byteBuffer":[B
    goto/16 :goto_3

    .line 464
    .restart local v10    # "i":I
    :cond_8
    array-length v0, v5

    move/from16 v24, v0

    move/from16 v0, v24

    move/from16 v1, v23

    if-ge v0, v1, :cond_9

    .line 465
    move/from16 v0, v23

    invoke-static {v5, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v5

    .line 467
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move/from16 v1, v18

    invoke-virtual {v0, v5, v1, v7}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 468
    new-instance v22, Ljava/lang/String;

    const/16 v24, 0x0

    const-string/jumbo v25, "UTF-8"

    move-object/from16 v0, v22

    move/from16 v1, v24

    move/from16 v2, v23

    move-object/from16 v3, v25

    invoke-direct {v0, v5, v1, v2, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .restart local v22    # "term":Ljava/lang/String;
    goto :goto_5

    .line 485
    .restart local v9    # "freq":I
    .restart local v14    # "positions":[I
    :cond_a
    const/4 v11, 0x0

    .restart local v11    # "j":I
    :goto_7
    if-ge v11, v9, :cond_b

    .line 487
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    .line 485
    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    .line 491
    .end local v11    # "j":I
    :cond_b
    const/4 v13, 0x0

    .line 492
    .local v13, "offsets":[Lorg/apache/lucene/index/TermVectorOffsetInfo;
    if-eqz v20, :cond_d

    .line 494
    invoke-virtual/range {p4 .. p4}, Lorg/apache/lucene/index/TermVectorMapper;->isIgnoringOffsets()Z

    move-result v24

    if-nez v24, :cond_c

    .line 495
    new-array v13, v9, [Lorg/apache/lucene/index/TermVectorOffsetInfo;

    .line 496
    const/16 v16, 0x0

    .line 497
    .local v16, "prevOffset":I
    const/4 v11, 0x0

    .restart local v11    # "j":I
    :goto_8
    if-ge v11, v9, :cond_d

    .line 498
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v24

    add-int v19, v16, v24

    .line 499
    .local v19, "startOffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v24

    add-int v8, v19, v24

    .line 500
    .local v8, "endOffset":I
    new-instance v24, Lorg/apache/lucene/index/TermVectorOffsetInfo;

    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-direct {v0, v1, v8}, Lorg/apache/lucene/index/TermVectorOffsetInfo;-><init>(II)V

    aput-object v24, v13, v11

    .line 501
    move/from16 v16, v8

    .line 497
    add-int/lit8 v11, v11, 0x1

    goto :goto_8

    .line 504
    .end local v8    # "endOffset":I
    .end local v11    # "j":I
    .end local v16    # "prevOffset":I
    .end local v19    # "startOffset":I
    :cond_c
    const/4 v11, 0x0

    .restart local v11    # "j":I
    :goto_9
    if-ge v11, v9, :cond_d

    .line 505
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    .line 506
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    .line 504
    add-int/lit8 v11, v11, 0x1

    goto :goto_9

    .line 510
    .end local v11    # "j":I
    :cond_d
    move-object/from16 v0, p4

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v9, v13, v14}, Lorg/apache/lucene/index/TermVectorMapper;->map(Ljava/lang/String;I[Lorg/apache/lucene/index/TermVectorOffsetInfo;[I)V

    .line 448
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_4
.end method

.method private readTermVectors([Ljava/lang/String;[JLorg/apache/lucene/index/TermVectorMapper;)V
    .locals 4
    .param p1, "fields"    # [Ljava/lang/String;
    .param p2, "tvfPointers"    # [J
    .param p3, "mapper"    # Lorg/apache/lucene/index/TermVectorMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 392
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 393
    aget-object v1, p1, v0

    aget-wide v2, p2, v0

    invoke-direct {p0, v1, v2, v3, p3}, Lorg/apache/lucene/index/TermVectorsReader;->readTermVector(Ljava/lang/String;JLorg/apache/lucene/index/TermVectorMapper;)V

    .line 392
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 395
    :cond_0
    return-void
.end method

.method private readTermVectors(I[Ljava/lang/String;[J)[Lorg/apache/lucene/index/SegmentTermVector;
    .locals 6
    .param p1, "docNum"    # I
    .param p2, "fields"    # [Ljava/lang/String;
    .param p3, "tvfPointers"    # [J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 380
    array-length v3, p2

    new-array v2, v3, [Lorg/apache/lucene/index/SegmentTermVector;

    .line 381
    .local v2, "res":[Lorg/apache/lucene/index/SegmentTermVector;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p2

    if-ge v0, v3, :cond_0

    .line 382
    new-instance v1, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;

    invoke-direct {v1}, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;-><init>()V

    .line 383
    .local v1, "mapper":Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;
    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->setDocumentNumber(I)V

    .line 384
    aget-object v3, p2, v0

    aget-wide v4, p3, v0

    invoke-direct {p0, v3, v4, v5, v1}, Lorg/apache/lucene/index/TermVectorsReader;->readTermVector(Ljava/lang/String;JLorg/apache/lucene/index/TermVectorMapper;)V

    .line 385
    invoke-virtual {v1}, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->materializeVector()Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/SegmentTermVector;

    aput-object v3, v2, v0

    .line 381
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 387
    .end local v1    # "mapper":Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;
    :cond_0
    return-object v2
.end method

.method private final readTvfPointers(I)[J
    .locals 6
    .param p1, "fieldCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 309
    iget v4, p0, Lorg/apache/lucene/index/TermVectorsReader;->format:I

    const/4 v5, 0x3

    if-lt v4, v5, :cond_0

    .line 310
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v2

    .line 314
    .local v2, "position":J
    :goto_0
    new-array v1, p1, [J

    .line 315
    .local v1, "tvfPointers":[J
    const/4 v4, 0x0

    aput-wide v2, v1, v4

    .line 317
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    if-ge v0, p1, :cond_1

    .line 318
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 319
    aput-wide v2, v1, v0

    .line 317
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 312
    .end local v0    # "i":I
    .end local v1    # "tvfPointers":[J
    .end local v2    # "position":J
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v2

    .restart local v2    # "position":J
    goto :goto_0

    .line 322
    .restart local v0    # "i":I
    .restart local v1    # "tvfPointers":[J
    :cond_1
    return-object v1
.end method

.method private final seekTvx(I)V
    .locals 8
    .param p1, "docNum"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x4

    .line 137
    iget v0, p0, Lorg/apache/lucene/index/TermVectorsReader;->format:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 138
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    iget v1, p0, Lorg/apache/lucene/index/TermVectorsReader;->docStoreOffset:I

    add-int/2addr v1, p1

    int-to-long v2, v1

    const-wide/16 v4, 0x8

    mul-long/2addr v2, v4

    add-long/2addr v2, v6

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 141
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    iget v1, p0, Lorg/apache/lucene/index/TermVectorsReader;->docStoreOffset:I

    add-int/2addr v1, p1

    int-to-long v2, v1

    const-wide/16 v4, 0x10

    mul-long/2addr v2, v4

    add-long/2addr v2, v6

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto :goto_0
.end method


# virtual methods
.method canReadRawDocs()Z
    .locals 2

    .prologue
    .line 144
    iget v0, p0, Lorg/apache/lucene/index/TermVectorsReader;->format:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected clone()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 517
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/TermVectorsReader;

    .line 521
    .local v0, "clone":Lorg/apache/lucene/index/TermVectorsReader;
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    if-eqz v1, :cond_0

    .line 522
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/store/IndexInput;

    iput-object v1, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    .line 523
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/store/IndexInput;

    iput-object v1, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    .line 524
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/store/IndexInput;

    iput-object v1, v0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    .line 527
    :cond_0
    return-object v0
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 208
    return-void
.end method

.method get(ILjava/lang/String;)Lorg/apache/lucene/index/TermFreqVector;
    .locals 2
    .param p1, "docNum"    # I
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 280
    new-instance v0, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;

    invoke-direct {v0}, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;-><init>()V

    .line 281
    .local v0, "mapper":Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;
    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/index/TermVectorsReader;->get(ILjava/lang/String;Lorg/apache/lucene/index/TermVectorMapper;)V

    .line 283
    invoke-virtual {v0}, Lorg/apache/lucene/index/ParallelArrayTermVectorMapper;->materializeVector()Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v1

    return-object v1
.end method

.method public get(ILjava/lang/String;Lorg/apache/lucene/index/TermVectorMapper;)V
    .locals 12
    .param p1, "docNum"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "mapper"    # Lorg/apache/lucene/index/TermVectorMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 219
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    if-eqz v5, :cond_5

    .line 220
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v5, p2}, Lorg/apache/lucene/index/FieldInfos;->fieldNumber(Ljava/lang/String;)I

    move-result v1

    .line 225
    .local v1, "fieldNumber":I
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/TermVectorsReader;->seekTvx(I)V

    .line 227
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v8

    .line 229
    .local v8, "tvdPosition":J
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5, v8, v9}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 230
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 235
    .local v0, "fieldCount":I
    const/4 v4, 0x0

    .line 236
    .local v4, "number":I
    const/4 v2, -0x1

    .line 237
    .local v2, "found":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_2

    .line 238
    iget v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->format:I

    const/4 v10, 0x2

    if-lt v5, v10, :cond_1

    .line 239
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    .line 243
    :goto_1
    if-ne v4, v1, :cond_0

    .line 244
    move v2, v3

    .line 237
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 241
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v5

    add-int/2addr v4, v5

    goto :goto_1

    .line 249
    :cond_2
    const/4 v5, -0x1

    if-eq v2, v5, :cond_5

    .line 252
    iget v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->format:I

    const/4 v10, 0x3

    if-lt v5, v10, :cond_3

    .line 253
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v6

    .line 256
    .local v6, "position":J
    :goto_2
    const/4 v3, 0x1

    :goto_3
    if-gt v3, v2, :cond_4

    .line 257
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v10

    add-long/2addr v6, v10

    .line 256
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 255
    .end local v6    # "position":J
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v6

    .restart local v6    # "position":J
    goto :goto_2

    .line 259
    :cond_4
    invoke-virtual {p3, p1}, Lorg/apache/lucene/index/TermVectorMapper;->setDocumentNumber(I)V

    .line 260
    invoke-direct {p0, p2, v6, v7, p3}, Lorg/apache/lucene/index/TermVectorsReader;->readTermVector(Ljava/lang/String;JLorg/apache/lucene/index/TermVectorMapper;)V

    .line 267
    .end local v0    # "fieldCount":I
    .end local v1    # "fieldNumber":I
    .end local v2    # "found":I
    .end local v3    # "i":I
    .end local v4    # "number":I
    .end local v6    # "position":J
    .end local v8    # "tvdPosition":J
    :cond_5
    return-void
.end method

.method public get(ILorg/apache/lucene/index/TermVectorMapper;)V
    .locals 6
    .param p1, "docNumber"    # I
    .param p2, "mapper"    # Lorg/apache/lucene/index/TermVectorMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 356
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    if-eqz v5, :cond_0

    .line 359
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/TermVectorsReader;->seekTvx(I)V

    .line 360
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v2

    .line 362
    .local v2, "tvdPosition":J
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 363
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 366
    .local v0, "fieldCount":I
    if-eqz v0, :cond_0

    .line 367
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/TermVectorsReader;->readFields(I)[Ljava/lang/String;

    move-result-object v1

    .line 368
    .local v1, "fields":[Ljava/lang/String;
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/TermVectorsReader;->readTvfPointers(I)[J

    move-result-object v4

    .line 369
    .local v4, "tvfPointers":[J
    invoke-virtual {p2, p1}, Lorg/apache/lucene/index/TermVectorMapper;->setDocumentNumber(I)V

    .line 370
    invoke-direct {p0, v1, v4, p2}, Lorg/apache/lucene/index/TermVectorsReader;->readTermVectors([Ljava/lang/String;[JLorg/apache/lucene/index/TermVectorMapper;)V

    .line 375
    .end local v0    # "fieldCount":I
    .end local v1    # "fields":[Ljava/lang/String;
    .end local v2    # "tvdPosition":J
    .end local v4    # "tvfPointers":[J
    :cond_0
    return-void
.end method

.method get(I)[Lorg/apache/lucene/index/TermFreqVector;
    .locals 7
    .param p1, "docNum"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 333
    const/4 v2, 0x0

    .line 334
    .local v2, "result":[Lorg/apache/lucene/index/TermFreqVector;
    iget-object v6, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    if-eqz v6, :cond_0

    .line 336
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/TermVectorsReader;->seekTvx(I)V

    .line 337
    iget-object v6, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v6}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v4

    .line 339
    .local v4, "tvdPosition":J
    iget-object v6, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v6, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 340
    iget-object v6, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v6}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 343
    .local v0, "fieldCount":I
    if-eqz v0, :cond_0

    .line 344
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/TermVectorsReader;->readFields(I)[Ljava/lang/String;

    move-result-object v1

    .line 345
    .local v1, "fields":[Ljava/lang/String;
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/TermVectorsReader;->readTvfPointers(I)[J

    move-result-object v3

    .line 346
    .local v3, "tvfPointers":[J
    invoke-direct {p0, p1, v1, v3}, Lorg/apache/lucene/index/TermVectorsReader;->readTermVectors(I[Ljava/lang/String;[J)[Lorg/apache/lucene/index/SegmentTermVector;

    move-result-object v2

    .line 351
    .end local v0    # "fieldCount":I
    .end local v1    # "fields":[Ljava/lang/String;
    .end local v3    # "tvfPointers":[J
    .end local v4    # "tvdPosition":J
    :cond_0
    return-object v2
.end method

.method getTvdStream()Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method getTvfStream()Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method final rawDocs([I[III)V
    .locals 14
    .param p1, "tvdLengths"    # [I
    .param p2, "tvfLengths"    # [I
    .param p3, "startDocID"    # I
    .param p4, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    iget-object v12, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    if-nez v12, :cond_1

    .line 156
    const/4 v12, 0x0

    invoke-static {p1, v12}, Ljava/util/Arrays;->fill([II)V

    .line 157
    const/4 v12, 0x0

    move-object/from16 v0, p2

    invoke-static {v0, v12}, Ljava/util/Arrays;->fill([II)V

    .line 195
    :cond_0
    return-void

    .line 163
    :cond_1
    iget v12, p0, Lorg/apache/lucene/index/TermVectorsReader;->format:I

    const/4 v13, 0x3

    if-ge v12, v13, :cond_2

    .line 164
    new-instance v12, Ljava/lang/IllegalStateException;

    const-string/jumbo v13, "cannot read raw docs with older term vector formats"

    invoke-direct {v12, v13}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 166
    :cond_2
    move/from16 v0, p3

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/TermVectorsReader;->seekTvx(I)V

    .line 168
    iget-object v12, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v8

    .line 169
    .local v8, "tvdPosition":J
    iget-object v12, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12, v8, v9}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 171
    iget-object v12, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v10

    .line 172
    .local v10, "tvfPosition":J
    iget-object v12, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12, v10, v11}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 174
    move-wide v4, v8

    .line 175
    .local v4, "lastTvdPosition":J
    move-wide v6, v10

    .line 177
    .local v6, "lastTvfPosition":J
    const/4 v2, 0x0

    .line 178
    .local v2, "count":I
    :goto_0
    move/from16 v0, p4

    if-ge v2, v0, :cond_0

    .line 179
    iget v12, p0, Lorg/apache/lucene/index/TermVectorsReader;->docStoreOffset:I

    add-int v12, v12, p3

    add-int/2addr v12, v2

    add-int/lit8 v3, v12, 0x1

    .line 180
    .local v3, "docID":I
    sget-boolean v12, Lorg/apache/lucene/index/TermVectorsReader;->$assertionsDisabled:Z

    if-nez v12, :cond_3

    iget v12, p0, Lorg/apache/lucene/index/TermVectorsReader;->numTotalDocs:I

    if-le v3, v12, :cond_3

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 181
    :cond_3
    iget v12, p0, Lorg/apache/lucene/index/TermVectorsReader;->numTotalDocs:I

    if-ge v3, v12, :cond_5

    .line 182
    iget-object v12, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v8

    .line 183
    iget-object v12, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v10

    .line 189
    :cond_4
    sub-long v12, v8, v4

    long-to-int v12, v12

    aput v12, p1, v2

    .line 190
    sub-long v12, v10, v6

    long-to-int v12, v12

    aput v12, p2, v2

    .line 191
    add-int/lit8 v2, v2, 0x1

    .line 192
    move-wide v4, v8

    .line 193
    move-wide v6, v10

    .line 194
    goto :goto_0

    .line 185
    :cond_5
    iget-object v12, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v8

    .line 186
    iget-object v12, p0, Lorg/apache/lucene/index/TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v10

    .line 187
    sget-boolean v12, Lorg/apache/lucene/index/TermVectorsReader;->$assertionsDisabled:Z

    if-nez v12, :cond_4

    add-int/lit8 v12, p4, -0x1

    if-eq v2, v12, :cond_4

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12
.end method

.method size()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lorg/apache/lucene/index/TermVectorsReader;->size:I

    return v0
.end method
