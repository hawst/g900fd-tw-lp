.class public Lorg/apache/lucene/index/MultiReader;
.super Lorg/apache/lucene/index/IndexReader;
.source "MultiReader.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private final decrefOnClose:[Z

.field private hasDeletions:Z

.field private final maxDoc:I

.field private final normsCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation
.end field

.field private numDocs:I

.field protected final starts:[I

.field protected final subReaders:[Lorg/apache/lucene/index/IndexReader;


# direct methods
.method public varargs constructor <init>([Lorg/apache/lucene/index/IndexReader;)V
    .locals 1
    .param p1, "subReaders"    # [Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 51
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/MultiReader;-><init>([Lorg/apache/lucene/index/IndexReader;Z)V

    .line 52
    return-void
.end method

.method public constructor <init>([Lorg/apache/lucene/index/IndexReader;Z)V
    .locals 3
    .param p1, "subReaders"    # [Lorg/apache/lucene/index/IndexReader;
    .param p2, "closeSubReaders"    # Z

    .prologue
    .line 63
    invoke-virtual {p1}, [Lorg/apache/lucene/index/IndexReader;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/apache/lucene/index/IndexReader;

    array-length v2, p1

    new-array v2, v2, [Z

    invoke-direct {p0, v1, v2}, Lorg/apache/lucene/index/MultiReader;-><init>([Lorg/apache/lucene/index/IndexReader;[Z)V

    .line 64
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 65
    if-nez p2, :cond_0

    .line 66
    aget-object v1, p1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->incRef()V

    .line 67
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->decrefOnClose:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    .line 64
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 69
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->decrefOnClose:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    goto :goto_1

    .line 72
    :cond_1
    return-void
.end method

.method private constructor <init>([Lorg/apache/lucene/index/IndexReader;[Z)V
    .locals 5
    .param p1, "subReaders"    # [Lorg/apache/lucene/index/IndexReader;
    .param p2, "decrefOnClose"    # [Z

    .prologue
    .line 74
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexReader;-><init>()V

    .line 38
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lorg/apache/lucene/index/MultiReader;->normsCache:Ljava/util/Map;

    .line 40
    const/4 v3, -0x1

    iput v3, p0, Lorg/apache/lucene/index/MultiReader;->numDocs:I

    .line 41
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/apache/lucene/index/MultiReader;->hasDeletions:Z

    .line 75
    iput-object p1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    .line 76
    iput-object p2, p0, Lorg/apache/lucene/index/MultiReader;->decrefOnClose:[Z

    .line 77
    array-length v3, p1

    add-int/lit8 v3, v3, 0x1

    new-array v3, v3, [I

    iput-object v3, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    .line 78
    const/4 v1, 0x0

    .line 79
    .local v1, "maxDoc":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_1

    .line 80
    aget-object v2, p1, v0

    .line 81
    .local v2, "reader":Lorg/apache/lucene/index/IndexReader;
    iget-object v3, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    aput v1, v3, v0

    .line 82
    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v3

    add-int/2addr v1, v3

    .line 83
    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 84
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/apache/lucene/index/MultiReader;->hasDeletions:Z

    .line 79
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    .end local v2    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    array-length v4, p1

    aput v1, v3, v4

    iput v1, p0, Lorg/apache/lucene/index/MultiReader;->maxDoc:I

    .line 88
    return-void
.end method

.method private doReopen(Z)Lorg/apache/lucene/index/IndexReader;
    .locals 9
    .param p1, "doClone"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 139
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 141
    const/4 v0, 0x0

    .line 142
    .local v0, "changed":Z
    iget-object v6, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v6, v6

    new-array v4, v6, [Lorg/apache/lucene/index/IndexReader;

    .line 144
    .local v4, "newSubReaders":[Lorg/apache/lucene/index/IndexReader;
    const/4 v5, 0x0

    .line 146
    .local v5, "success":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v6, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v6, v6

    if-ge v1, v6, :cond_3

    .line 147
    if-eqz p1, :cond_0

    .line 148
    iget-object v6, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexReader;->clone()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/IndexReader;

    aput-object v6, v4, v1

    .line 149
    const/4 v0, 0x1

    .line 146
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 151
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v6, v6, v1

    invoke-static {v6}, Lorg/apache/lucene/index/IndexReader;->openIfChanged(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/IndexReader;

    move-result-object v3

    .line 152
    .local v3, "newSubReader":Lorg/apache/lucene/index/IndexReader;
    if-eqz v3, :cond_1

    .line 153
    aput-object v3, v4, v1

    .line 154
    const/4 v0, 0x1

    goto :goto_1

    .line 156
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v6, v6, v1

    aput-object v6, v4, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 162
    .end local v3    # "newSubReader":Lorg/apache/lucene/index/IndexReader;
    :catchall_0
    move-exception v6

    if-nez v5, :cond_5

    if-eqz v0, :cond_5

    .line 163
    const/4 v1, 0x0

    :goto_2
    array-length v7, v4

    if-ge v1, v7, :cond_5

    .line 164
    aget-object v7, v4, v1

    iget-object v8, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v8, v8, v1

    if-eq v7, v8, :cond_2

    .line 166
    :try_start_1
    aget-object v7, v4, v1

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 163
    :cond_2
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 160
    :cond_3
    const/4 v5, 0x1

    .line 162
    if-nez v5, :cond_6

    if-eqz v0, :cond_6

    .line 163
    const/4 v1, 0x0

    :goto_4
    array-length v6, v4

    if-ge v1, v6, :cond_6

    .line 164
    aget-object v6, v4, v1

    iget-object v7, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v7, v7, v1

    if-eq v6, v7, :cond_4

    .line 166
    :try_start_2
    aget-object v6, v4, v1

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 163
    :cond_4
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 162
    :cond_5
    throw v6

    .line 175
    :cond_6
    if-eqz v0, :cond_9

    .line 176
    iget-object v6, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v6, v6

    new-array v2, v6, [Z

    .line 177
    .local v2, "newDecrefOnClose":[Z
    const/4 v1, 0x0

    :goto_6
    iget-object v6, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v6, v6

    if-ge v1, v6, :cond_8

    .line 178
    aget-object v6, v4, v1

    iget-object v7, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v7, v7, v1

    if-ne v6, v7, :cond_7

    .line 179
    aget-object v6, v4, v1

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexReader;->incRef()V

    .line 180
    const/4 v6, 0x1

    aput-boolean v6, v2, v1

    .line 177
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 183
    :cond_8
    new-instance v6, Lorg/apache/lucene/index/MultiReader;

    invoke-direct {v6, v4, v2}, Lorg/apache/lucene/index/MultiReader;-><init>([Lorg/apache/lucene/index/IndexReader;[Z)V

    .line 185
    .end local v2    # "newDecrefOnClose":[Z
    :goto_7
    return-object v6

    :cond_9
    const/4 v6, 0x0

    goto :goto_7

    .line 167
    :catch_0
    move-exception v6

    goto :goto_5

    :catch_1
    move-exception v7

    goto :goto_3
.end method


# virtual methods
.method public declared-synchronized clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 114
    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    invoke-direct {p0, v1}, Lorg/apache/lucene/index/MultiReader;->doReopen(Z)Lorg/apache/lucene/index/IndexReader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public clone(Z)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p1, "openReadOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 127
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "MultiReader does not support cloning with changing readOnly flag. Use IndexReader.clone() instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected declared-synchronized doClose()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 419
    monitor-enter p0

    const/4 v2, 0x0

    .line 420
    .local v2, "ioe":Ljava/io/IOException;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v3, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v1, v3, :cond_2

    .line 422
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/index/MultiReader;->decrefOnClose:[Z

    aget-boolean v3, v3, v1

    if-eqz v3, :cond_1

    .line 423
    iget-object v3, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 420
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 425
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 427
    :catch_0
    move-exception v0

    .line 428
    .local v0, "e":Ljava/io/IOException;
    if-nez v2, :cond_0

    move-object v2, v0

    goto :goto_1

    .line 432
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    if-eqz v2, :cond_3

    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 419
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 433
    :cond_3
    monitor-exit p0

    return-void
.end method

.method protected doCommit(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 413
    .local p1, "commitUserData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 414
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/IndexReader;->commit(Ljava/util/Map;)V

    .line 413
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 415
    :cond_0
    return-void
.end method

.method protected doDelete(I)V
    .locals 3
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 271
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/index/MultiReader;->numDocs:I

    .line 272
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MultiReader;->readerIndex(I)I

    move-result v0

    .line 273
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexReader;->deleteDocument(I)V

    .line 274
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/index/MultiReader;->hasDeletions:Z

    .line 275
    return-void
.end method

.method protected declared-synchronized doOpenIfChanged()Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/MultiReader;->doReopen(Z)Lorg/apache/lucene/index/IndexReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected doOpenIfChanged(Z)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p1, "openReadOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 107
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "MultiReader does not support reopening with changing readOnly flag. Use IndexReader.openIfChanged(IndexReader) instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected doSetNorm(ILjava/lang/String;B)V
    .locals 3
    .param p1, "n"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "value"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 339
    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->normsCache:Ljava/util/Map;

    monitor-enter v2

    .line 340
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->normsCache:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MultiReader;->readerIndex(I)I

    move-result v0

    .line 343
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2, p2, p3}, Lorg/apache/lucene/index/IndexReader;->setNorm(ILjava/lang/String;B)V

    .line 344
    return-void

    .line 341
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method protected doUndeleteAll()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 280
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 281
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->undeleteAll()V

    .line 280
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 283
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/lucene/index/MultiReader;->hasDeletions:Z

    .line 284
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/index/MultiReader;->numDocs:I

    .line 285
    return-void
.end method

.method public docFreq(Lorg/apache/lucene/index/Term;)I
    .locals 3
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 370
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 371
    const/4 v1, 0x0

    .line 372
    .local v1, "total":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 373
    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v2

    add-int/2addr v1, v2

    .line 372
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 374
    :cond_0
    return v1
.end method

.method public document(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;
    .locals 3
    .param p1, "n"    # I
    .param p2, "fieldSelector"    # Lorg/apache/lucene/document/FieldSelector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 250
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 251
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MultiReader;->readerIndex(I)I

    move-result v0

    .line 252
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2, p2}, Lorg/apache/lucene/index/IndexReader;->document(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;

    move-result-object v1

    return-object v1
.end method

.method public getFieldInfos()Lorg/apache/lucene/index/FieldInfos;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "call getFieldInfos() on each sub reader, or use ReaderUtil.getMergedFieldInfos, instead"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getSequentialSubReaders()[Lorg/apache/lucene/index/IndexReader;
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    return-object v0
.end method

.method public getTermFreqVector(ILjava/lang/String;)Lorg/apache/lucene/index/TermFreqVector;
    .locals 3
    .param p1, "n"    # I
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 199
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 200
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MultiReader;->readerIndex(I)I

    move-result v0

    .line 201
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2, p2}, Lorg/apache/lucene/index/IndexReader;->getTermFreqVector(ILjava/lang/String;)Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v1

    return-object v1
.end method

.method public getTermFreqVector(ILjava/lang/String;Lorg/apache/lucene/index/TermVectorMapper;)V
    .locals 3
    .param p1, "docNumber"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "mapper"    # Lorg/apache/lucene/index/TermVectorMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 208
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MultiReader;->readerIndex(I)I

    move-result v0

    .line 209
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2, p2, p3}, Lorg/apache/lucene/index/IndexReader;->getTermFreqVector(ILjava/lang/String;Lorg/apache/lucene/index/TermVectorMapper;)V

    .line 210
    return-void
.end method

.method public getTermFreqVector(ILorg/apache/lucene/index/TermVectorMapper;)V
    .locals 3
    .param p1, "docNumber"    # I
    .param p2, "mapper"    # Lorg/apache/lucene/index/TermVectorMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 215
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MultiReader;->readerIndex(I)I

    move-result v0

    .line 216
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2, p2}, Lorg/apache/lucene/index/IndexReader;->getTermFreqVector(ILorg/apache/lucene/index/TermVectorMapper;)V

    .line 217
    return-void
.end method

.method public getTermFreqVectors(I)[Lorg/apache/lucene/index/TermFreqVector;
    .locals 3
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 192
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MultiReader;->readerIndex(I)I

    move-result v0

    .line 193
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexReader;->getTermFreqVectors(I)[Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v1

    return-object v1
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 456
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "MultiReader does not support this method."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hasDeletions()Z
    .locals 1

    .prologue
    .line 264
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 265
    iget-boolean v0, p0, Lorg/apache/lucene/index/MultiReader;->hasDeletions:Z

    return v0
.end method

.method public hasNorms(Ljava/lang/String;)Z
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 293
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 294
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 295
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/IndexReader;->hasNorms(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 297
    :goto_1
    return v1

    .line 294
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 297
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isCurrent()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 440
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 441
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 442
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->isCurrent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 443
    const/4 v1, 0x0

    .line 448
    :goto_1
    return v1

    .line 441
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 448
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public isDeleted(I)Z
    .locals 3
    .param p1, "n"    # I

    .prologue
    .line 258
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MultiReader;->readerIndex(I)I

    move-result v0

    .line 259
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexReader;->isDeleted(I)Z

    move-result v1

    return v1
.end method

.method public isOptimized()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 223
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 224
    const/4 v0, 0x0

    return v0
.end method

.method public maxDoc()I
    .locals 1

    .prologue
    .line 244
    iget v0, p0, Lorg/apache/lucene/index/MultiReader;->maxDoc:I

    return v0
.end method

.method public declared-synchronized norms(Ljava/lang/String;[BI)V
    .locals 5
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "result"    # [B
    .param p3, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 319
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 320
    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->normsCache:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 321
    .local v0, "bytes":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 322
    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v2, v2, v1

    iget-object v3, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    aget v3, v3, v1

    add-int/2addr v3, p3

    invoke-virtual {v2, p1, p2, v3}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;[BI)V

    .line 321
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 324
    :cond_0
    if-nez v0, :cond_2

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MultiReader;->hasNorms(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 325
    array-length v2, p2

    invoke-static {}, Lorg/apache/lucene/search/Similarity;->getDefault()Lorg/apache/lucene/search/Similarity;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Similarity;->encodeNormValue(F)B

    move-result v3

    invoke-static {p2, p3, v2, v3}, Ljava/util/Arrays;->fill([BIIB)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 333
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 326
    :cond_2
    if-eqz v0, :cond_3

    .line 327
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->maxDoc()I

    move-result v3

    invoke-static {v0, v2, p2, p3, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 319
    .end local v0    # "bytes":[B
    .end local v1    # "i":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 329
    .restart local v0    # "bytes":[B
    .restart local v1    # "i":I
    :cond_3
    const/4 v1, 0x0

    :goto_2
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 330
    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v2, v2, v1

    iget-object v3, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    aget v3, v3, v1

    add-int/2addr v3, p3

    invoke-virtual {v2, p1, p2, v3}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;[BI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 329
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public declared-synchronized norms(Ljava/lang/String;)[B
    .locals 4
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 302
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 303
    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->normsCache:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 304
    .local v0, "bytes":[B
    if-eqz v0, :cond_0

    move-object v2, v0

    .line 313
    :goto_0
    monitor-exit p0

    return-object v2

    .line 306
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MultiReader;->hasNorms(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 307
    const/4 v2, 0x0

    goto :goto_0

    .line 309
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->maxDoc()I

    move-result v2

    new-array v0, v2, [B

    .line 310
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 311
    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v2, v2, v1

    iget-object v3, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    aget v3, v3, v1

    invoke-virtual {v2, p1, v0, v3}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;[BI)V

    .line 310
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 312
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->normsCache:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v2, v0

    .line 313
    goto :goto_0

    .line 302
    .end local v0    # "bytes":[B
    .end local v1    # "i":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public numDocs()I
    .locals 4

    .prologue
    .line 232
    iget v2, p0, Lorg/apache/lucene/index/MultiReader;->numDocs:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 233
    const/4 v1, 0x0

    .line 234
    .local v1, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 235
    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v2

    add-int/2addr v1, v2

    .line 234
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 236
    :cond_0
    iput v1, p0, Lorg/apache/lucene/index/MultiReader;->numDocs:I

    .line 238
    .end local v0    # "i":I
    .end local v1    # "n":I
    :cond_1
    iget v2, p0, Lorg/apache/lucene/index/MultiReader;->numDocs:I

    return v2
.end method

.method protected readerIndex(I)I
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 288
    iget-object v0, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/lucene/index/DirectoryReader;->readerIndex(I[II)I

    move-result v0

    return v0
.end method

.method public termDocs()Lorg/apache/lucene/index/TermDocs;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 379
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 380
    iget-object v0, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 382
    iget-object v0, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->termDocs()Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    .line 384
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;

    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;-><init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[I)V

    goto :goto_0
.end method

.method public termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 390
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 391
    iget-object v0, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 393
    iget-object v0, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    .line 395
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/lucene/index/IndexReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    goto :goto_0
.end method

.method public termPositions()Lorg/apache/lucene/index/TermPositions;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 401
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 402
    iget-object v0, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 404
    iget-object v0, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->termPositions()Lorg/apache/lucene/index/TermPositions;

    move-result-object v0

    .line 406
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/index/DirectoryReader$MultiTermPositions;

    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/lucene/index/DirectoryReader$MultiTermPositions;-><init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[I)V

    goto :goto_0
.end method

.method public terms()Lorg/apache/lucene/index/TermEnum;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 348
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 349
    iget-object v0, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 351
    iget-object v0, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->terms()Lorg/apache/lucene/index/TermEnum;

    move-result-object v0

    .line 353
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;-><init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[ILorg/apache/lucene/index/Term;)V

    goto :goto_0
.end method

.method public terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 359
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiReader;->ensureOpen()V

    .line 360
    iget-object v0, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 362
    iget-object v0, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v0

    .line 364
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    iget-object v1, p0, Lorg/apache/lucene/index/MultiReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    iget-object v2, p0, Lorg/apache/lucene/index/MultiReader;->starts:[I

    invoke-direct {v0, p0, v1, v2, p1}, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;-><init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[ILorg/apache/lucene/index/Term;)V

    goto :goto_0
.end method
