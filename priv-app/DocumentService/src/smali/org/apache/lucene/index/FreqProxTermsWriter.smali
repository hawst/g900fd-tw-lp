.class final Lorg/apache/lucene/index/FreqProxTermsWriter;
.super Lorg/apache/lucene/index/TermsHashConsumer;
.source "FreqProxTermsWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private payloadBuffer:[B

.field final termsUTF8:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/index/FreqProxTermsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FreqProxTermsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashConsumer;-><init>()V

    .line 323
    new-instance v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-direct {v0}, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriter;->termsUTF8:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    return-void
.end method

.method private static compareText([CI[CI)I
    .locals 5
    .param p0, "text1"    # [C
    .param p1, "pos1"    # I
    .param p2, "text2"    # [C
    .param p3, "pos2"    # I

    .prologue
    const v4, 0xffff

    .line 40
    :goto_0
    add-int/lit8 v2, p1, 0x1

    .end local p1    # "pos1":I
    .local v2, "pos1":I
    aget-char v0, p0, p1

    .line 41
    .local v0, "c1":C
    add-int/lit8 v3, p3, 0x1

    .end local p3    # "pos2":I
    .local v3, "pos2":I
    aget-char v1, p2, p3

    .line 42
    .local v1, "c2":C
    if-eq v0, v1, :cond_2

    .line 43
    if-ne v4, v1, :cond_0

    .line 44
    const/4 v4, 0x1

    .line 50
    :goto_1
    return v4

    .line 45
    :cond_0
    if-ne v4, v0, :cond_1

    .line 46
    const/4 v4, -0x1

    goto :goto_1

    .line 48
    :cond_1
    sub-int v4, v0, v1

    goto :goto_1

    .line 49
    :cond_2
    if-ne v4, v0, :cond_3

    .line 50
    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    move p3, v3

    .end local v3    # "pos2":I
    .restart local p3    # "pos2":I
    move p1, v2

    .line 51
    .end local v2    # "pos1":I
    .restart local p1    # "pos1":I
    goto :goto_0
.end method


# virtual methods
.method abort()V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method public addThread(Lorg/apache/lucene/index/TermsHashPerThread;)Lorg/apache/lucene/index/TermsHashConsumerPerThread;
    .locals 1
    .param p1, "perThread"    # Lorg/apache/lucene/index/TermsHashPerThread;

    .prologue
    .line 35
    new-instance v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;

    invoke-direct {v0, p1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;-><init>(Lorg/apache/lucene/index/TermsHashPerThread;)V

    return-object v0
.end method

.method appendPostings(Ljava/lang/String;Lorg/apache/lucene/index/SegmentWriteState;[Lorg/apache/lucene/index/FreqProxTermsWriterPerField;Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;)V
    .locals 35
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p3, "fields"    # [Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
    .param p4, "consumer"    # Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v16, v0

    .line 154
    .local v16, "numFields":I
    move/from16 v0, v16

    new-array v14, v0, [Lorg/apache/lucene/index/FreqProxFieldMergeState;

    .line 156
    .local v14, "mergeStates":[Lorg/apache/lucene/index/FreqProxFieldMergeState;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    move/from16 v0, v16

    if-ge v12, v0, :cond_2

    .line 157
    new-instance v11, Lorg/apache/lucene/index/FreqProxFieldMergeState;

    aget-object v33, p3, v12

    move-object/from16 v0, v33

    invoke-direct {v11, v0}, Lorg/apache/lucene/index/FreqProxFieldMergeState;-><init>(Lorg/apache/lucene/index/FreqProxTermsWriterPerField;)V

    aput-object v11, v14, v12

    .line 159
    .local v11, "fms":Lorg/apache/lucene/index/FreqProxFieldMergeState;
    sget-boolean v33, Lorg/apache/lucene/index/FreqProxTermsWriter;->$assertionsDisabled:Z

    if-nez v33, :cond_0

    iget-object v0, v11, Lorg/apache/lucene/index/FreqProxFieldMergeState;->field:Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    aget-object v34, p3, v34

    move-object/from16 v0, v34

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v34, v0

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-eq v0, v1, :cond_0

    new-instance v33, Ljava/lang/AssertionError;

    invoke-direct/range {v33 .. v33}, Ljava/lang/AssertionError;-><init>()V

    throw v33

    .line 162
    :cond_0
    invoke-virtual {v11}, Lorg/apache/lucene/index/FreqProxFieldMergeState;->nextTerm()Z

    move-result v24

    .line 163
    .local v24, "result":Z
    sget-boolean v33, Lorg/apache/lucene/index/FreqProxTermsWriter;->$assertionsDisabled:Z

    if-nez v33, :cond_1

    if-nez v24, :cond_1

    new-instance v33, Ljava/lang/AssertionError;

    invoke-direct/range {v33 .. v33}, Ljava/lang/AssertionError;-><init>()V

    throw v33

    .line 156
    :cond_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 166
    .end local v11    # "fms":Lorg/apache/lucene/index/FreqProxFieldMergeState;
    .end local v24    # "result":Z
    :cond_2
    const/16 v33, 0x0

    aget-object v33, p3, v33

    move-object/from16 v0, v33

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v33, v0

    move-object/from16 v0, p4

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;->addField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/FormatPostingsTermsConsumer;

    move-result-object v28

    .line 167
    .local v28, "termsConsumer":Lorg/apache/lucene/index/FormatPostingsTermsConsumer;
    new-instance v22, Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    .line 169
    .local v22, "protoTerm":Lorg/apache/lucene/index/Term;
    move/from16 v0, v16

    new-array v0, v0, [Lorg/apache/lucene/index/FreqProxFieldMergeState;

    move-object/from16 v27, v0

    .line 171
    .local v27, "termStates":[Lorg/apache/lucene/index/FreqProxFieldMergeState;
    const/16 v33, 0x0

    aget-object v33, p3, v33

    move-object/from16 v0, v33

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget-object v7, v0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 174
    .local v7, "currentFieldIndexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentWriteState;->segDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    move-object/from16 v33, v0

    if-eqz v33, :cond_3

    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentWriteState;->segDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletes;->terms:Ljava/util/Map;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Ljava/util/Map;->size()I

    move-result v33

    if-lez v33, :cond_3

    .line 175
    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentWriteState;->segDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletes;->terms:Ljava/util/Map;

    move-object/from16 v25, v0

    .line 186
    .local v25, "segDeletes":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    :goto_1
    if-lez v16, :cond_15

    .line 189
    const/16 v33, 0x0

    const/16 v34, 0x0

    :try_start_0
    aget-object v34, v14, v34

    aput-object v34, v27, v33

    .line 190
    const/16 v17, 0x1

    .line 193
    .local v17, "numToMerge":I
    const/4 v12, 0x1

    move/from16 v18, v17

    .end local v17    # "numToMerge":I
    .local v18, "numToMerge":I
    :goto_2
    move/from16 v0, v16

    if-ge v12, v0, :cond_5

    .line 194
    aget-object v33, v14, v12

    move-object/from16 v0, v33

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->text:[C

    move-object/from16 v29, v0

    .line 195
    .local v29, "text":[C
    aget-object v33, v14, v12

    move-object/from16 v0, v33

    iget v0, v0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->textOffset:I

    move/from16 v30, v0

    .line 196
    .local v30, "textOffset":I
    const/16 v33, 0x0

    aget-object v33, v27, v33

    move-object/from16 v0, v33

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->text:[C

    move-object/from16 v33, v0

    const/16 v34, 0x0

    aget-object v34, v27, v34

    move-object/from16 v0, v34

    iget v0, v0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->textOffset:I

    move/from16 v34, v0

    move-object/from16 v0, v29

    move/from16 v1, v30

    move-object/from16 v2, v33

    move/from16 v3, v34

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/index/FreqProxTermsWriter;->compareText([CI[CI)I

    move-result v5

    .line 198
    .local v5, "cmp":I
    if-gez v5, :cond_4

    .line 199
    const/16 v33, 0x0

    aget-object v34, v14, v12

    aput-object v34, v27, v33

    .line 200
    const/16 v17, 0x1

    .line 193
    .end local v18    # "numToMerge":I
    .restart local v17    # "numToMerge":I
    :goto_3
    add-int/lit8 v12, v12, 0x1

    move/from16 v18, v17

    .end local v17    # "numToMerge":I
    .restart local v18    # "numToMerge":I
    goto :goto_2

    .line 177
    .end local v5    # "cmp":I
    .end local v18    # "numToMerge":I
    .end local v25    # "segDeletes":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    .end local v29    # "text":[C
    .end local v30    # "textOffset":I
    :cond_3
    const/16 v25, 0x0

    .restart local v25    # "segDeletes":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    goto :goto_1

    .line 201
    .restart local v5    # "cmp":I
    .restart local v18    # "numToMerge":I
    .restart local v29    # "text":[C
    .restart local v30    # "textOffset":I
    :cond_4
    if-nez v5, :cond_19

    .line 202
    add-int/lit8 v17, v18, 0x1

    .end local v18    # "numToMerge":I
    .restart local v17    # "numToMerge":I
    aget-object v33, v14, v12

    aput-object v33, v27, v18
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 319
    .end local v5    # "cmp":I
    .end local v17    # "numToMerge":I
    .end local v29    # "text":[C
    .end local v30    # "textOffset":I
    :catchall_0
    move-exception v33

    invoke-virtual/range {v28 .. v28}, Lorg/apache/lucene/index/FormatPostingsTermsConsumer;->finish()V

    throw v33

    .line 205
    .restart local v18    # "numToMerge":I
    :cond_5
    const/16 v33, 0x0

    :try_start_1
    aget-object v33, v27, v33

    move-object/from16 v0, v33

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->text:[C

    move-object/from16 v33, v0

    const/16 v34, 0x0

    aget-object v34, v27, v34

    move-object/from16 v0, v34

    iget v0, v0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->textOffset:I

    move/from16 v34, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v33

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/FormatPostingsTermsConsumer;->addTerm([CI)Lorg/apache/lucene/index/FormatPostingsDocsConsumer;

    move-result-object v9

    .line 208
    .local v9, "docConsumer":Lorg/apache/lucene/index/FormatPostingsDocsConsumer;
    if-eqz v25, :cond_9

    .line 209
    const/16 v33, 0x0

    aget-object v33, v27, v33

    invoke-virtual/range {v33 .. v33}, Lorg/apache/lucene/index/FreqProxFieldMergeState;->termText()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v22

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Term;->createTerm(Ljava/lang/String;)Lorg/apache/lucene/index/Term;

    move-result-object v33

    move-object/from16 v0, v25

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    .line 210
    .local v10, "docIDUpto":Ljava/lang/Integer;
    if-eqz v10, :cond_8

    .line 211
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    .local v8, "delDocLimit":I
    :goto_4
    move/from16 v17, v18

    .line 223
    .end local v10    # "docIDUpto":Ljava/lang/Integer;
    .end local v18    # "numToMerge":I
    .restart local v17    # "numToMerge":I
    :cond_6
    :goto_5
    if-lez v17, :cond_16

    .line 225
    const/16 v33, 0x0

    :try_start_2
    aget-object v15, v27, v33

    .line 226
    .local v15, "minState":Lorg/apache/lucene/index/FreqProxFieldMergeState;
    const/4 v12, 0x1

    :goto_6
    move/from16 v0, v17

    if-ge v12, v0, :cond_a

    .line 227
    aget-object v33, v27, v12

    move-object/from16 v0, v33

    iget v0, v0, Lorg/apache/lucene/index/FreqProxFieldMergeState;->docID:I

    move/from16 v33, v0

    iget v0, v15, Lorg/apache/lucene/index/FreqProxFieldMergeState;->docID:I

    move/from16 v34, v0

    move/from16 v0, v33

    move/from16 v1, v34

    if-ge v0, v1, :cond_7

    .line 228
    aget-object v15, v27, v12

    .line 226
    :cond_7
    add-int/lit8 v12, v12, 0x1

    goto :goto_6

    .line 213
    .end local v8    # "delDocLimit":I
    .end local v15    # "minState":Lorg/apache/lucene/index/FreqProxFieldMergeState;
    .end local v17    # "numToMerge":I
    .restart local v10    # "docIDUpto":Ljava/lang/Integer;
    .restart local v18    # "numToMerge":I
    :cond_8
    const/4 v8, 0x0

    .restart local v8    # "delDocLimit":I
    goto :goto_4

    .line 216
    .end local v8    # "delDocLimit":I
    .end local v10    # "docIDUpto":Ljava/lang/Integer;
    :cond_9
    const/4 v8, 0x0

    .restart local v8    # "delDocLimit":I
    move/from16 v17, v18

    .end local v18    # "numToMerge":I
    .restart local v17    # "numToMerge":I
    goto :goto_5

    .line 230
    .restart local v15    # "minState":Lorg/apache/lucene/index/FreqProxFieldMergeState;
    :cond_a
    iget v0, v15, Lorg/apache/lucene/index/FreqProxFieldMergeState;->termFreq:I

    move/from16 v26, v0

    .line 232
    .local v26, "termDocFreq":I
    iget v0, v15, Lorg/apache/lucene/index/FreqProxFieldMergeState;->docID:I

    move/from16 v33, v0

    move/from16 v0, v33

    move/from16 v1, v26

    invoke-virtual {v9, v0, v1}, Lorg/apache/lucene/index/FormatPostingsDocsConsumer;->addDoc(II)Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;

    move-result-object v20

    .line 246
    .local v20, "posConsumer":Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;
    iget v0, v15, Lorg/apache/lucene/index/FreqProxFieldMergeState;->docID:I

    move/from16 v33, v0

    move/from16 v0, v33

    if-ge v0, v8, :cond_c

    .line 250
    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentWriteState;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    move-object/from16 v33, v0

    if-nez v33, :cond_b

    .line 251
    new-instance v33, Lorg/apache/lucene/util/BitVector;

    move-object/from16 v0, p2

    iget v0, v0, Lorg/apache/lucene/index/SegmentWriteState;->numDocs:I

    move/from16 v34, v0

    invoke-direct/range {v33 .. v34}, Lorg/apache/lucene/util/BitVector;-><init>(I)V

    move-object/from16 v0, v33

    move-object/from16 v1, p2

    iput-object v0, v1, Lorg/apache/lucene/index/SegmentWriteState;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    .line 253
    :cond_b
    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentWriteState;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    move-object/from16 v33, v0

    iget v0, v15, Lorg/apache/lucene/index/FreqProxFieldMergeState;->docID:I

    move/from16 v34, v0

    invoke-virtual/range {v33 .. v34}, Lorg/apache/lucene/util/BitVector;->set(I)V

    .line 256
    :cond_c
    iget-object v0, v15, Lorg/apache/lucene/index/FreqProxFieldMergeState;->prox:Lorg/apache/lucene/index/ByteSliceReader;

    move-object/from16 v23, v0

    .line 261
    .local v23, "prox":Lorg/apache/lucene/index/ByteSliceReader;
    sget-object v33, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object/from16 v0, v33

    if-ne v7, v0, :cond_11

    .line 265
    const/16 v21, 0x0

    .line 266
    .local v21, "position":I
    const/4 v13, 0x0

    .local v13, "j":I
    :goto_7
    move/from16 v0, v26

    if-ge v13, v0, :cond_10

    .line 267
    :try_start_3
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/ByteSliceReader;->readVInt()I

    move-result v6

    .line 268
    .local v6, "code":I
    ushr-int/lit8 v33, v6, 0x1

    add-int v21, v21, v33

    .line 271
    and-int/lit8 v33, v6, 0x1

    if-eqz v33, :cond_f

    .line 273
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/ByteSliceReader;->readVInt()I

    move-result v19

    .line 275
    .local v19, "payloadLength":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriter;->payloadBuffer:[B

    move-object/from16 v33, v0

    if-eqz v33, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriter;->payloadBuffer:[B

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    array-length v0, v0

    move/from16 v33, v0

    move/from16 v0, v33

    move/from16 v1, v19

    if-ge v0, v1, :cond_e

    .line 276
    :cond_d
    move/from16 v0, v19

    new-array v0, v0, [B

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/FreqProxTermsWriter;->payloadBuffer:[B

    .line 278
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriter;->payloadBuffer:[B

    move-object/from16 v33, v0

    const/16 v34, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v33

    move/from16 v2, v34

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/index/ByteSliceReader;->readBytes([BII)V

    .line 283
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriter;->payloadBuffer:[B

    move-object/from16 v33, v0

    const/16 v34, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v33

    move/from16 v3, v34

    move/from16 v4, v19

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;->addPosition(I[BII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 266
    add-int/lit8 v13, v13, 0x1

    goto :goto_7

    .line 281
    .end local v19    # "payloadLength":I
    :cond_f
    const/16 v19, 0x0

    .restart local v19    # "payloadLength":I
    goto :goto_8

    .line 286
    .end local v6    # "code":I
    .end local v19    # "payloadLength":I
    :catchall_1
    move-exception v33

    :try_start_4
    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;->finish()V

    throw v33
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 315
    .end local v13    # "j":I
    .end local v15    # "minState":Lorg/apache/lucene/index/FreqProxFieldMergeState;
    .end local v20    # "posConsumer":Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;
    .end local v21    # "position":I
    .end local v23    # "prox":Lorg/apache/lucene/index/ByteSliceReader;
    .end local v26    # "termDocFreq":I
    :catchall_2
    move-exception v33

    :try_start_5
    invoke-virtual {v9}, Lorg/apache/lucene/index/FormatPostingsDocsConsumer;->finish()V

    throw v33
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 286
    .restart local v13    # "j":I
    .restart local v15    # "minState":Lorg/apache/lucene/index/FreqProxFieldMergeState;
    .restart local v20    # "posConsumer":Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;
    .restart local v21    # "position":I
    .restart local v23    # "prox":Lorg/apache/lucene/index/ByteSliceReader;
    .restart local v26    # "termDocFreq":I
    :cond_10
    :try_start_6
    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;->finish()V

    .line 290
    .end local v13    # "j":I
    .end local v21    # "position":I
    :cond_11
    invoke-virtual {v15}, Lorg/apache/lucene/index/FreqProxFieldMergeState;->nextDoc()Z

    move-result v33

    if-nez v33, :cond_6

    .line 293
    const/16 v31, 0x0

    .line 294
    .local v31, "upto":I
    const/4 v12, 0x0

    move/from16 v32, v31

    .end local v31    # "upto":I
    .local v32, "upto":I
    :goto_9
    move/from16 v0, v17

    if-ge v12, v0, :cond_12

    .line 295
    aget-object v33, v27, v12

    move-object/from16 v0, v33

    if-eq v0, v15, :cond_18

    .line 296
    add-int/lit8 v31, v32, 0x1

    .end local v32    # "upto":I
    .restart local v31    # "upto":I
    aget-object v33, v27, v12

    aput-object v33, v27, v32

    .line 294
    :goto_a
    add-int/lit8 v12, v12, 0x1

    move/from16 v32, v31

    .end local v31    # "upto":I
    .restart local v32    # "upto":I
    goto :goto_9

    .line 297
    :cond_12
    add-int/lit8 v17, v17, -0x1

    .line 298
    sget-boolean v33, Lorg/apache/lucene/index/FreqProxTermsWriter;->$assertionsDisabled:Z

    if-nez v33, :cond_13

    move/from16 v0, v32

    move/from16 v1, v17

    if-eq v0, v1, :cond_13

    new-instance v33, Ljava/lang/AssertionError;

    invoke-direct/range {v33 .. v33}, Ljava/lang/AssertionError;-><init>()V

    throw v33

    .line 302
    :cond_13
    invoke-virtual {v15}, Lorg/apache/lucene/index/FreqProxFieldMergeState;->nextTerm()Z

    move-result v33

    if-nez v33, :cond_6

    .line 305
    const/16 v31, 0x0

    .line 306
    .end local v32    # "upto":I
    .restart local v31    # "upto":I
    const/4 v12, 0x0

    move/from16 v32, v31

    .end local v31    # "upto":I
    .restart local v32    # "upto":I
    :goto_b
    move/from16 v0, v16

    if-ge v12, v0, :cond_14

    .line 307
    aget-object v33, v14, v12

    move-object/from16 v0, v33

    if-eq v0, v15, :cond_17

    .line 308
    add-int/lit8 v31, v32, 0x1

    .end local v32    # "upto":I
    .restart local v31    # "upto":I
    aget-object v33, v14, v12

    aput-object v33, v14, v32

    .line 306
    :goto_c
    add-int/lit8 v12, v12, 0x1

    move/from16 v32, v31

    .end local v31    # "upto":I
    .restart local v32    # "upto":I
    goto :goto_b

    .line 309
    :cond_14
    add-int/lit8 v16, v16, -0x1

    .line 310
    sget-boolean v33, Lorg/apache/lucene/index/FreqProxTermsWriter;->$assertionsDisabled:Z

    if-nez v33, :cond_6

    move/from16 v0, v32

    move/from16 v1, v16

    if-eq v0, v1, :cond_6

    new-instance v33, Ljava/lang/AssertionError;

    invoke-direct/range {v33 .. v33}, Ljava/lang/AssertionError;-><init>()V

    throw v33
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 319
    .end local v8    # "delDocLimit":I
    .end local v9    # "docConsumer":Lorg/apache/lucene/index/FormatPostingsDocsConsumer;
    .end local v15    # "minState":Lorg/apache/lucene/index/FreqProxFieldMergeState;
    .end local v17    # "numToMerge":I
    .end local v20    # "posConsumer":Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;
    .end local v23    # "prox":Lorg/apache/lucene/index/ByteSliceReader;
    .end local v26    # "termDocFreq":I
    .end local v32    # "upto":I
    :cond_15
    invoke-virtual/range {v28 .. v28}, Lorg/apache/lucene/index/FormatPostingsTermsConsumer;->finish()V

    .line 321
    return-void

    .line 315
    .restart local v8    # "delDocLimit":I
    .restart local v9    # "docConsumer":Lorg/apache/lucene/index/FormatPostingsDocsConsumer;
    .restart local v17    # "numToMerge":I
    :cond_16
    :try_start_7
    invoke-virtual {v9}, Lorg/apache/lucene/index/FormatPostingsDocsConsumer;->finish()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1

    .restart local v15    # "minState":Lorg/apache/lucene/index/FreqProxFieldMergeState;
    .restart local v20    # "posConsumer":Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;
    .restart local v23    # "prox":Lorg/apache/lucene/index/ByteSliceReader;
    .restart local v26    # "termDocFreq":I
    .restart local v32    # "upto":I
    :cond_17
    move/from16 v31, v32

    .end local v32    # "upto":I
    .restart local v31    # "upto":I
    goto :goto_c

    .end local v31    # "upto":I
    .restart local v32    # "upto":I
    :cond_18
    move/from16 v31, v32

    .end local v32    # "upto":I
    .restart local v31    # "upto":I
    goto :goto_a

    .end local v8    # "delDocLimit":I
    .end local v9    # "docConsumer":Lorg/apache/lucene/index/FormatPostingsDocsConsumer;
    .end local v15    # "minState":Lorg/apache/lucene/index/FreqProxFieldMergeState;
    .end local v17    # "numToMerge":I
    .end local v20    # "posConsumer":Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;
    .end local v23    # "prox":Lorg/apache/lucene/index/ByteSliceReader;
    .end local v26    # "termDocFreq":I
    .end local v31    # "upto":I
    .restart local v5    # "cmp":I
    .restart local v18    # "numToMerge":I
    .restart local v29    # "text":[C
    .restart local v30    # "textOffset":I
    :cond_19
    move/from16 v17, v18

    .end local v18    # "numToMerge":I
    .restart local v17    # "numToMerge":I
    goto/16 :goto_3
.end method

.method public flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 20
    .param p2, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/TermsHashConsumerPerThread;",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/TermsHashConsumerPerField;",
            ">;>;",
            "Lorg/apache/lucene/index/SegmentWriteState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    .local p1, "threadsAndFields":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/TermsHashConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .local v2, "allFields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/FreqProxTermsWriterPerField;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 71
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/TermsHashConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Collection;

    .line 73
    .local v9, "fields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;"
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/index/TermsHashConsumerPerField;

    .local v10, "i":Lorg/apache/lucene/index/TermsHashConsumerPerField;
    move-object v15, v10

    .line 74
    check-cast v15, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    .line 75
    .local v15, "perField":Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
    iget-object v0, v15, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    move/from16 v18, v0

    if-lez v18, :cond_1

    .line 76
    invoke-interface {v2, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/TermsHashConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;>;"
    .end local v9    # "fields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;"
    .end local v10    # "i":Lorg/apache/lucene/index/TermsHashConsumerPerField;
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v15    # "perField":Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
    :cond_2
    invoke-static {v2}, Lorg/apache/lucene/util/CollectionUtil;->quickSort(Ljava/util/List;)V

    .line 82
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v13

    .line 85
    .local v13, "numAllFields":I
    new-instance v3, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-direct {v3, v0, v1}, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;-><init>(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/index/FieldInfos;)V

    .line 98
    .local v3, "consumer":Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;
    const/16 v17, 0x0

    .line 99
    .local v17, "start":I
    :goto_1
    move/from16 v0, v17

    if-ge v0, v13, :cond_7

    .line 100
    :try_start_0
    move/from16 v0, v17

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    move-object/from16 v0, v18

    iget-object v6, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 101
    .local v6, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    iget-object v7, v6, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    .line 103
    .local v7, "fieldName":Ljava/lang/String;
    add-int/lit8 v4, v17, 0x1

    .line 104
    .local v4, "end":I
    :goto_2
    if-ge v4, v13, :cond_3

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 105
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 107
    :cond_3
    sub-int v18, v4, v17

    move/from16 v0, v18

    new-array v8, v0, [Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    .line 108
    .local v8, "fields":[Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
    move/from16 v10, v17

    .local v10, "i":I
    :goto_3
    if-ge v10, v4, :cond_5

    .line 109
    sub-int v19, v10, v17

    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    aput-object v18, v8, v19

    .line 113
    iget-object v0, v6, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v18, v0

    sget-object v19, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 114
    iget-boolean v0, v6, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    move/from16 v18, v0

    sub-int v19, v10, v17

    aget-object v19, v8, v19

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasPayloads:Z

    move/from16 v19, v0

    or-int v18, v18, v19

    move/from16 v0, v18

    iput-boolean v0, v6, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    .line 108
    :cond_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 120
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v7, v1, v8, v3}, Lorg/apache/lucene/index/FreqProxTermsWriter;->appendPostings(Ljava/lang/String;Lorg/apache/lucene/index/SegmentWriteState;[Lorg/apache/lucene/index/FreqProxTermsWriterPerField;Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;)V

    .line 122
    const/4 v10, 0x0

    :goto_4
    array-length v0, v8

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v10, v0, :cond_6

    .line 123
    aget-object v18, v8, v10

    move-object/from16 v0, v18

    iget-object v15, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    .line 124
    .local v15, "perField":Lorg/apache/lucene/index/TermsHashPerField;
    iget v14, v15, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    .line 125
    .local v14, "numPostings":I
    invoke-virtual {v15}, Lorg/apache/lucene/index/TermsHashPerField;->reset()V

    .line 126
    invoke-virtual {v15, v14}, Lorg/apache/lucene/index/TermsHashPerField;->shrinkHash(I)V

    .line 127
    aget-object v18, v8, v10

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->reset()V

    .line 122
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .line 130
    .end local v14    # "numPostings":I
    .end local v15    # "perField":Lorg/apache/lucene/index/TermsHashPerField;
    :cond_6
    move/from16 v17, v4

    .line 131
    goto/16 :goto_1

    .line 133
    .end local v4    # "end":I
    .end local v6    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .end local v7    # "fieldName":Ljava/lang/String;
    .end local v8    # "fields":[Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
    .end local v10    # "i":I
    :cond_7
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_8

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 134
    .restart local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/TermsHashConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;

    .line 135
    .local v16, "perThread":Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;
    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;->termsHashPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/index/TermsHashPerThread;->reset(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_5

    .line 138
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/TermsHashConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;>;"
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v16    # "perThread":Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;
    :catchall_0
    move-exception v18

    invoke-virtual {v3}, Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;->finish()V

    throw v18

    .restart local v11    # "i$":Ljava/util/Iterator;
    :cond_8
    invoke-virtual {v3}, Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;->finish()V

    .line 140
    return-void
.end method
