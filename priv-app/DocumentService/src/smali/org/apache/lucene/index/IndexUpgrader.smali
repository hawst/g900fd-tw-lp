.class public final Lorg/apache/lucene/index/IndexUpgrader;
.super Ljava/lang/Object;
.source "IndexUpgrader.java"


# instance fields
.field private final deletePriorCommits:Z

.field private final dir:Lorg/apache/lucene/store/Directory;

.field private final infoStream:Ljava/io/PrintStream;

.field private final iwc:Lorg/apache/lucene/index/IndexWriterConfig;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;Ljava/io/PrintStream;Z)V
    .locals 0
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "iwc"    # Lorg/apache/lucene/index/IndexWriterConfig;
    .param p3, "infoStream"    # Ljava/io/PrintStream;
    .param p4, "deletePriorCommits"    # Z

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput-object p1, p0, Lorg/apache/lucene/index/IndexUpgrader;->dir:Lorg/apache/lucene/store/Directory;

    .line 132
    iput-object p2, p0, Lorg/apache/lucene/index/IndexUpgrader;->iwc:Lorg/apache/lucene/index/IndexWriterConfig;

    .line 133
    iput-object p3, p0, Lorg/apache/lucene/index/IndexUpgrader;->infoStream:Ljava/io/PrintStream;

    .line 134
    iput-boolean p4, p0, Lorg/apache/lucene/index/IndexUpgrader;->deletePriorCommits:Z

    .line 135
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/util/Version;)V
    .locals 3
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    const/4 v2, 0x0

    .line 117
    new-instance v0, Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-direct {v0, p2, v2}, Lorg/apache/lucene/index/IndexWriterConfig;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v2, v1}, Lorg/apache/lucene/index/IndexUpgrader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;Ljava/io/PrintStream;Z)V

    .line 118
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/util/Version;Ljava/io/PrintStream;Z)V
    .locals 2
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p3, "infoStream"    # Ljava/io/PrintStream;
    .param p4, "deletePriorCommits"    # Z

    .prologue
    .line 124
    new-instance v0, Lorg/apache/lucene/index/IndexWriterConfig;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Lorg/apache/lucene/index/IndexWriterConfig;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V

    invoke-direct {p0, p1, v0, p3, p4}, Lorg/apache/lucene/index/IndexUpgrader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;Ljava/io/PrintStream;Z)V

    .line 125
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 9
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    const/4 v6, 0x0

    .line 72
    .local v6, "path":Ljava/lang/String;
    const/4 v1, 0x0

    .line 73
    .local v1, "deletePriorCommits":Z
    const/4 v5, 0x0

    .line 74
    .local v5, "out":Ljava/io/PrintStream;
    const/4 v3, 0x0

    .line 75
    .local v3, "dirImpl":Ljava/lang/String;
    const/4 v4, 0x0

    .line 76
    .local v4, "i":I
    :goto_0
    array-length v7, p0

    if-ge v4, v7, :cond_5

    .line 77
    aget-object v0, p0, v4

    .line 78
    .local v0, "arg":Ljava/lang/String;
    const-string/jumbo v7, "-delete-prior-commits"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 79
    const/4 v1, 0x1

    .line 94
    :goto_1
    add-int/lit8 v4, v4, 0x1

    .line 95
    goto :goto_0

    .line 80
    :cond_0
    const-string/jumbo v7, "-verbose"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 81
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    goto :goto_1

    .line 82
    :cond_1
    if-nez v6, :cond_2

    .line 83
    move-object v6, v0

    goto :goto_1

    .line 84
    :cond_2
    const-string/jumbo v7, "-dir-impl"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 85
    array-length v7, p0

    add-int/lit8 v7, v7, -0x1

    if-ne v4, v7, :cond_3

    .line 86
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v8, "ERROR: missing value for -dir-impl option"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 87
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/System;->exit(I)V

    .line 89
    :cond_3
    add-int/lit8 v4, v4, 0x1

    .line 90
    aget-object v3, p0, v4

    goto :goto_1

    .line 92
    :cond_4
    invoke-static {}, Lorg/apache/lucene/index/IndexUpgrader;->printUsage()V

    goto :goto_1

    .line 96
    .end local v0    # "arg":Ljava/lang/String;
    :cond_5
    if-nez v6, :cond_6

    .line 97
    invoke-static {}, Lorg/apache/lucene/index/IndexUpgrader;->printUsage()V

    .line 100
    :cond_6
    const/4 v2, 0x0

    .line 101
    .local v2, "dir":Lorg/apache/lucene/store/Directory;
    if-nez v3, :cond_7

    .line 102
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lorg/apache/lucene/store/FSDirectory;->open(Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;

    move-result-object v2

    .line 106
    :goto_2
    new-instance v7, Lorg/apache/lucene/index/IndexUpgrader;

    sget-object v8, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    invoke-direct {v7, v2, v8, v5, v1}, Lorg/apache/lucene/index/IndexUpgrader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/util/Version;Ljava/io/PrintStream;Z)V

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexUpgrader;->upgrade()V

    .line 107
    return-void

    .line 104
    :cond_7
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v7}, Lorg/apache/lucene/util/CommandLineUtil;->newFSDirectory(Ljava/lang/String;Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;

    move-result-object v2

    goto :goto_2
.end method

.method private static printUsage()V
    .locals 3

    .prologue
    .line 55
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v1, "Upgrades an index so all segments created with a previous Lucene version are rewritten."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 56
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v1, "Usage:"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 57
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "  java "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Lorg/apache/lucene/index/IndexUpgrader;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " [-delete-prior-commits] [-verbose] [-dir-impl X] indexDir"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 58
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v1, "This tool keeps only the last commit in an index; for this"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 59
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v1, "reason, if the incoming index has more than one commit, the tool"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 60
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v1, "refuses to run by default. Specify -delete-prior-commits to override"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 61
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v1, "this, allowing the tool to delete all but the last commit."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 62
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Specify a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Lorg/apache/lucene/store/FSDirectory;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " implementation through the -dir-impl option to force its use. If no package is specified the "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Lorg/apache/lucene/store/FSDirectory;

    invoke-virtual {v2}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " package will be used."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 65
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string/jumbo v1, "WARNING: This tool may reorder document IDs!"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 66
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 67
    return-void
.end method


# virtual methods
.method public upgrade()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 138
    iget-object v3, p0, Lorg/apache/lucene/index/IndexUpgrader;->dir:Lorg/apache/lucene/store/Directory;

    invoke-static {v3}, Lorg/apache/lucene/index/IndexReader;->indexExists(Lorg/apache/lucene/store/Directory;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 139
    new-instance v3, Lorg/apache/lucene/index/IndexNotFoundException;

    iget-object v4, p0, Lorg/apache/lucene/index/IndexUpgrader;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4}, Lorg/apache/lucene/store/Directory;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/IndexNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 142
    :cond_0
    iget-boolean v3, p0, Lorg/apache/lucene/index/IndexUpgrader;->deletePriorCommits:Z

    if-nez v3, :cond_1

    .line 143
    iget-object v3, p0, Lorg/apache/lucene/index/IndexUpgrader;->dir:Lorg/apache/lucene/store/Directory;

    invoke-static {v3}, Lorg/apache/lucene/index/IndexReader;->listCommits(Lorg/apache/lucene/store/Directory;)Ljava/util/Collection;

    move-result-object v1

    .line 144
    .local v1, "commits":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/IndexCommit;>;"
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v3

    if-le v3, v4, :cond_1

    .line 145
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "This tool was invoked to not delete prior commit points, but the following commits were found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 149
    .end local v1    # "commits":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/IndexCommit;>;"
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/IndexUpgrader;->iwc:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriterConfig;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriterConfig;

    .line 150
    .local v0, "c":Lorg/apache/lucene/index/IndexWriterConfig;
    new-instance v3, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriterConfig;->getMergePolicy()Lorg/apache/lucene/index/MergePolicy;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;-><init>(Lorg/apache/lucene/index/MergePolicy;)V

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/IndexWriterConfig;->setMergePolicy(Lorg/apache/lucene/index/MergePolicy;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 151
    new-instance v3, Lorg/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy;

    invoke-direct {v3}, Lorg/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy;-><init>()V

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/IndexWriterConfig;->setIndexDeletionPolicy(Lorg/apache/lucene/index/IndexDeletionPolicy;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 153
    new-instance v2, Lorg/apache/lucene/index/IndexWriter;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexUpgrader;->dir:Lorg/apache/lucene/store/Directory;

    invoke-direct {v2, v3, v0}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    .line 155
    .local v2, "w":Lorg/apache/lucene/index/IndexWriter;
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/IndexUpgrader;->infoStream:Ljava/io/PrintStream;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexWriter;->setInfoStream(Ljava/io/PrintStream;)V

    .line 156
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Upgrading all pre-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " segments of index directory \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/IndexUpgrader;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "\' to version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 157
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexWriter;->forceMerge(I)V

    .line 158
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "All segments upgraded to version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexWriter;->close()V

    .line 162
    return-void

    .line 160
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexWriter;->close()V

    throw v3
.end method
