.class Lorg/apache/lucene/index/CoalescedDeletes$1;
.super Ljava/lang/Object;
.source "CoalescedDeletes.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/CoalescedDeletes;->termsIterable()Ljava/lang/Iterable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/lucene/index/Term;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/index/CoalescedDeletes;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/CoalescedDeletes;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lorg/apache/lucene/index/CoalescedDeletes$1;->this$0:Lorg/apache/lucene/index/CoalescedDeletes;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lorg/apache/lucene/index/CoalescedDeletes$1;->this$0:Lorg/apache/lucene/index/CoalescedDeletes;

    iget-object v3, v3, Lorg/apache/lucene/index/CoalescedDeletes;->iterables:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 53
    .local v2, "subs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/Iterator<Lorg/apache/lucene/index/Term;>;>;"
    iget-object v3, p0, Lorg/apache/lucene/index/CoalescedDeletes$1;->this$0:Lorg/apache/lucene/index/CoalescedDeletes;

    iget-object v3, v3, Lorg/apache/lucene/index/CoalescedDeletes;->iterables:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 54
    .local v1, "iterable":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/index/Term;>;"
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 56
    .end local v1    # "iterable":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/index/Term;>;"
    :cond_0
    invoke-static {v2}, Lorg/apache/lucene/index/CoalescedDeletes;->mergedIterator(Ljava/util/List;)Ljava/util/Iterator;

    move-result-object v3

    return-object v3
.end method
