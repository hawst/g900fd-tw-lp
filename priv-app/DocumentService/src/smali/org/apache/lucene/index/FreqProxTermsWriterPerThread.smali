.class final Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;
.super Lorg/apache/lucene/index/TermsHashConsumerPerThread;
.source "FreqProxTermsWriterPerThread.java"


# instance fields
.field final docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

.field final termsHashPerThread:Lorg/apache/lucene/index/TermsHashPerThread;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/TermsHashPerThread;)V
    .locals 1
    .param p1, "perThread"    # Lorg/apache/lucene/index/TermsHashPerThread;

    .prologue
    .line 24
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashConsumerPerThread;-><init>()V

    .line 25
    iget-object v0, p1, Lorg/apache/lucene/index/TermsHashPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 26
    iput-object p1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;->termsHashPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    .line 27
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public addField(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/TermsHashConsumerPerField;
    .locals 1
    .param p1, "termsHashPerField"    # Lorg/apache/lucene/index/TermsHashPerField;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 31
    new-instance v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    invoke-direct {v0, p1, p0, p2}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;-><init>(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;Lorg/apache/lucene/index/FieldInfo;)V

    return-object v0
.end method

.method finishDocument()Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return-object v0
.end method

.method startDocument()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method
