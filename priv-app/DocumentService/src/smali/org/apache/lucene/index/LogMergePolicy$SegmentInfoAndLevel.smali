.class Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;
.super Ljava/lang/Object;
.source "LogMergePolicy.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/LogMergePolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SegmentInfoAndLevel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;",
        ">;"
    }
.end annotation


# instance fields
.field index:I

.field info:Lorg/apache/lucene/index/SegmentInfo;

.field level:F


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/SegmentInfo;FI)V
    .locals 0
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "level"    # F
    .param p3, "index"    # I

    .prologue
    .line 467
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 468
    iput-object p1, p0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->info:Lorg/apache/lucene/index/SegmentInfo;

    .line 469
    iput p2, p0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    .line 470
    iput p3, p0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->index:I

    .line 471
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 462
    check-cast p1, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->compareTo(Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;)I
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    .prologue
    .line 475
    iget v0, p0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    iget v1, p1, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 476
    const/4 v0, 0x1

    .line 480
    :goto_0
    return v0

    .line 477
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    iget v1, p1, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 478
    const/4 v0, -0x1

    goto :goto_0

    .line 480
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
