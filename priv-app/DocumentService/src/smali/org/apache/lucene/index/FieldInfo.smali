.class public final Lorg/apache/lucene/index/FieldInfo;
.super Ljava/lang/Object;
.source "FieldInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

.field public isIndexed:Z

.field public final name:Ljava/lang/String;

.field public final number:I

.field public omitNorms:Z

.field public storePayloads:Z

.field public storeTermVector:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;ZIZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)V
    .locals 2
    .param p1, "na"    # Ljava/lang/String;
    .param p2, "tk"    # Z
    .param p3, "nu"    # I
    .param p4, "storeTermVector"    # Z
    .param p5, "omitNorms"    # Z
    .param p6, "storePayloads"    # Z
    .param p7, "indexOptions"    # Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    .line 57
    iput-boolean p2, p0, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    .line 58
    iput p3, p0, Lorg/apache/lucene/index/FieldInfo;->number:I

    .line 59
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    if-eqz v0, :cond_0

    .line 60
    iput-boolean p4, p0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    .line 61
    iput-boolean p6, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    .line 62
    iput-boolean p5, p0, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    .line 63
    iput-object p7, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 70
    :goto_0
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq p7, v0, :cond_1

    if-eqz p6, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 65
    :cond_0
    iput-boolean v1, p0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    .line 66
    iput-boolean v1, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    .line 68
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    iput-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    goto :goto_0

    .line 71
    :cond_1
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 75
    new-instance v0, Lorg/apache/lucene/index/FieldInfo;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-boolean v2, p0, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    iget v3, p0, Lorg/apache/lucene/index/FieldInfo;->number:I

    iget-boolean v4, p0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    iget-boolean v5, p0, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    iget-boolean v6, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    iget-object v7, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/index/FieldInfo;-><init>(Ljava/lang/String;ZIZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    return-object v0
.end method

.method update(ZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)V
    .locals 3
    .param p1, "isIndexed"    # Z
    .param p2, "storeTermVector"    # Z
    .param p3, "omitNorms"    # Z
    .param p4, "storePayloads"    # Z
    .param p5, "indexOptions"    # Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 82
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    if-eq v0, p1, :cond_0

    .line 83
    iput-boolean v1, p0, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    .line 85
    :cond_0
    if-eqz p1, :cond_5

    .line 86
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    if-eq v0, p2, :cond_1

    .line 87
    iput-boolean v1, p0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    .line 89
    :cond_1
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    if-eq v0, p4, :cond_2

    .line 90
    iput-boolean v1, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    .line 92
    :cond_2
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    if-eq v0, p3, :cond_3

    .line 93
    iput-boolean v2, p0, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    .line 95
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v0, p5, :cond_5

    .line 97
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, p5}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gez v0, :cond_4

    iget-object p5, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .end local p5    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_4
    iput-object p5, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 98
    iput-boolean v2, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    .line 101
    :cond_5
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfo;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v0, v1, :cond_6

    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 102
    :cond_6
    return-void
.end method
