.class Lorg/apache/lucene/index/SegmentMerger$CheckAbort;
.super Ljava/lang/Object;
.source "SegmentMerger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/SegmentMerger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CheckAbort"
.end annotation


# instance fields
.field private dir:Lorg/apache/lucene/store/Directory;

.field private merge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

.field private workCount:D


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/store/Directory;)V
    .locals 0
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .param p2, "dir"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 635
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->merge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 636
    iput-object p2, p0, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->dir:Lorg/apache/lucene/store/Directory;

    .line 637
    return-void
.end method


# virtual methods
.method public work(D)V
    .locals 5
    .param p1, "units"    # D
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;
        }
    .end annotation

    .prologue
    .line 648
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->workCount:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->workCount:D

    .line 649
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->workCount:D

    const-wide v2, 0x40c3880000000000L    # 10000.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    .line 650
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->merge:Lorg/apache/lucene/index/MergePolicy$OneMerge;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->checkAborted(Lorg/apache/lucene/store/Directory;)V

    .line 651
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->workCount:D

    .line 653
    :cond_0
    return-void
.end method
