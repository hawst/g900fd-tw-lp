.class Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;
.super Ljava/lang/Object;
.source "PrefixCodedTerms.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/PrefixCodedTerms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PrefixCodedTermsIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/index/Term;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field bytes:Lorg/apache/lucene/util/BytesRef;

.field field:Ljava/lang/String;

.field final input:Lorg/apache/lucene/store/IndexInput;

.field term:Lorg/apache/lucene/index/Term;

.field final synthetic this$0:Lorg/apache/lucene/index/PrefixCodedTerms;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lorg/apache/lucene/index/PrefixCodedTerms;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/PrefixCodedTerms;)V
    .locals 4

    .prologue
    .line 57
    iput-object p1, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->this$0:Lorg/apache/lucene/index/PrefixCodedTerms;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const-string/jumbo v1, ""

    iput-object v1, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->field:Ljava/lang/String;

    .line 54
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    .line 55
    new-instance v1, Lorg/apache/lucene/index/Term;

    iget-object v2, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->field:Ljava/lang/String;

    const-string/jumbo v3, ""

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->term:Lorg/apache/lucene/index/Term;

    .line 59
    :try_start_0
    new-instance v1, Lorg/apache/lucene/store/RAMInputStream;

    const-string/jumbo v2, "PrefixCodedTermsIterator"

    iget-object v3, p1, Lorg/apache/lucene/index/PrefixCodedTerms;->buffer:Lorg/apache/lucene/store/RAMFile;

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/store/RAMInputStream;-><init>(Ljava/lang/String;Lorg/apache/lucene/store/RAMFile;)V

    iput-object v1, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->input:Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public hasNext()Z
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v0

    iget-object v2, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->next()Lorg/apache/lucene/index/Term;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/index/Term;
    .locals 7

    .prologue
    .line 70
    sget-boolean v4, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 72
    :cond_0
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 73
    .local v0, "code":I
    and-int/lit8 v4, v0, 0x1

    if-eqz v4, :cond_1

    .line 75
    iget-object v4, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->field:Ljava/lang/String;

    .line 77
    :cond_1
    ushr-int/lit8 v2, v0, 0x1

    .line 78
    .local v2, "prefix":I
    iget-object v4, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    .line 79
    .local v3, "suffix":I
    iget-object v4, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    add-int v5, v2, v3

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 80
    iget-object v4, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->input:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v5, v5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-virtual {v4, v5, v2, v3}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 81
    iget-object v4, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    add-int v5, v2, v3

    iput v5, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 82
    iget-object v4, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->term:Lorg/apache/lucene/index/Term;

    iget-object v5, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->field:Ljava/lang/String;

    iget-object v6, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v6}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/index/Term;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v4, p0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;->term:Lorg/apache/lucene/index/Term;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v4

    .line 84
    .end local v0    # "code":I
    .end local v2    # "prefix":I
    .end local v3    # "suffix":I
    :catch_0
    move-exception v1

    .line 85
    .local v1, "e":Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
