.class Lorg/apache/lucene/index/ReadOnlySegmentReader;
.super Lorg/apache/lucene/index/SegmentReader;
.source "ReadOnlySegmentReader.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentReader;-><init>()V

    return-void
.end method

.method static noWrite()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This IndexReader cannot make any changes to the index (it was opened with readOnly = true)"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method protected acquireWriteLock()V
    .locals 0

    .prologue
    .line 28
    invoke-static {}, Lorg/apache/lucene/index/ReadOnlySegmentReader;->noWrite()V

    .line 29
    return-void
.end method

.method public isDeleted(I)Z
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 34
    iget-object v0, p0, Lorg/apache/lucene/index/ReadOnlySegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/ReadOnlySegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/BitVector;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
