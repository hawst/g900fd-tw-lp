.class final Lorg/apache/lucene/index/TermVectorsTermsWriter;
.super Lorg/apache/lucene/index/TermsHashConsumer;
.source "TermVectorsTermsWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field allocCount:I

.field docFreeList:[Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

.field final docWriter:Lorg/apache/lucene/index/DocumentsWriter;

.field freeCount:I

.field hasVectors:Z

.field lastDocID:I

.field tvd:Lorg/apache/lucene/store/IndexOutput;

.field tvf:Lorg/apache/lucene/store/IndexOutput;

.field tvx:Lorg/apache/lucene/store/IndexOutput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriter;)V
    .locals 1
    .param p1, "docWriter"    # Lorg/apache/lucene/index/DocumentsWriter;

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashConsumer;-><init>()V

    .line 34
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docFreeList:[Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    .line 43
    iput-object p1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 44
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 185
    iput-boolean v4, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->hasVectors:Z

    .line 187
    const/4 v1, 0x3

    :try_start_0
    new-array v1, v1, [Ljava/io/Closeable;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v1, v2

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->getSegment()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "tvx"

    invoke-static {v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 199
    :goto_0
    :try_start_2
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->getSegment()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "tvd"

    invoke-static {v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 204
    :goto_1
    :try_start_3
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->getSegment()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "tvf"

    invoke-static {v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 208
    :goto_2
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    .line 209
    iput v4, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->lastDocID:I

    .line 210
    return-void

    .line 188
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 205
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    goto :goto_2

    .line 200
    :catch_2
    move-exception v1

    goto :goto_1

    .line 195
    :catch_3
    move-exception v1

    goto :goto_0
.end method

.method public addThread(Lorg/apache/lucene/index/TermsHashPerThread;)Lorg/apache/lucene/index/TermsHashConsumerPerThread;
    .locals 1
    .param p1, "termsHashPerThread"    # Lorg/apache/lucene/index/TermsHashPerThread;

    .prologue
    .line 48
    new-instance v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    invoke-direct {v0, p1, p0}, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;-><init>(Lorg/apache/lucene/index/TermsHashPerThread;Lorg/apache/lucene/index/TermVectorsTermsWriter;)V

    return-object v0
.end method

.method fill(I)V
    .locals 6
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    iget v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->lastDocID:I

    if-ge v2, p1, :cond_0

    .line 111
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    .line 112
    .local v0, "tvfPosition":J
    :goto_0
    iget v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->lastDocID:I

    if-ge v2, p1, :cond_0

    .line 113
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 114
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 115
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2, v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 116
    iget v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->lastDocID:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->lastDocID:I

    goto :goto_0

    .line 119
    .end local v0    # "tvfPosition":J
    :cond_0
    return-void
.end method

.method declared-synchronized finishDocument(Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;)V
    .locals 10
    .param p1, "perDoc"    # Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lorg/apache/lucene/index/TermVectorsTermsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    const-string/jumbo v6, "TermVectorsTermsWriter.finishDocument start"

    invoke-virtual {v1, v6}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 151
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/TermVectorsTermsWriter;->initTermVectorsWriter()V

    .line 153
    iget v1, p1, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->docID:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/TermVectorsTermsWriter;->fill(I)V

    .line 156
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    iget-object v6, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v6}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 157
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    iget-object v6, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v6}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 158
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    iget v6, p1, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->numVectorFields:I

    invoke-virtual {v1, v6}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 159
    iget v1, p1, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->numVectorFields:I

    if-lez v1, :cond_4

    .line 160
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p1, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->numVectorFields:I

    if-ge v0, v1, :cond_1

    .line 161
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    iget-object v6, p1, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->fieldNumbers:[I

    aget v6, v6, v0

    invoke-virtual {v1, v6}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 160
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 163
    :cond_1
    sget-boolean v1, Lorg/apache/lucene/index/TermVectorsTermsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    const-wide/16 v6, 0x0

    iget-object v1, p1, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->fieldPointers:[J

    const/4 v8, 0x0

    aget-wide v8, v1, v8

    cmp-long v1, v6, v8

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 164
    :cond_2
    iget-object v1, p1, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->fieldPointers:[J

    const/4 v6, 0x0

    aget-wide v2, v1, v6

    .line 165
    .local v2, "lastPos":J
    const/4 v0, 0x1

    :goto_1
    iget v1, p1, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->numVectorFields:I

    if-ge v0, v1, :cond_3

    .line 166
    iget-object v1, p1, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->fieldPointers:[J

    aget-wide v4, v1, v0

    .line 167
    .local v4, "pos":J
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    sub-long v6, v4, v2

    invoke-virtual {v1, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 168
    move-wide v2, v4

    .line 165
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 170
    .end local v4    # "pos":J
    :cond_3
    iget-object v1, p1, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->perDocTvf:Lorg/apache/lucene/store/RAMOutputStream;

    iget-object v6, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, v6}, Lorg/apache/lucene/store/RAMOutputStream;->writeTo(Lorg/apache/lucene/store/IndexOutput;)V

    .line 171
    const/4 v1, 0x0

    iput v1, p1, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->numVectorFields:I

    .line 174
    .end local v0    # "i":I
    .end local v2    # "lastPos":J
    :cond_4
    sget-boolean v1, Lorg/apache/lucene/index/TermVectorsTermsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_5

    iget v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->lastDocID:I

    iget v6, p1, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->docID:I

    if-eq v1, v6, :cond_5

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "lastDocID="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->lastDocID:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " perDoc.docID="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->docID:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 176
    :cond_5
    iget v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->lastDocID:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->lastDocID:I

    .line 178
    invoke-virtual {p1}, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->reset()V

    .line 179
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/TermVectorsTermsWriter;->free(Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;)V

    .line 180
    sget-boolean v1, Lorg/apache/lucene/index/TermVectorsTermsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    const-string/jumbo v6, "TermVectorsTermsWriter.finishDocument end"

    invoke-virtual {v1, v6}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 181
    :cond_6
    monitor-exit p0

    return-void
.end method

.method declared-synchronized flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 16
    .param p2, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/TermsHashConsumerPerThread;",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/TermsHashConsumerPerField;",
            ">;>;",
            "Lorg/apache/lucene/index/SegmentWriteState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "threadsAndFields":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/TermsHashConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;>;"
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    if-eqz v9, :cond_2

    .line 55
    sget-boolean v9, Lorg/apache/lucene/index/TermVectorsTermsWriter;->$assertionsDisabled:Z

    if-nez v9, :cond_0

    move-object/from16 v0, p2

    iget-object v9, v0, Lorg/apache/lucene/index/SegmentWriteState;->segmentName:Ljava/lang/String;

    if-nez v9, :cond_0

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 56
    :cond_0
    :try_start_1
    move-object/from16 v0, p2

    iget v9, v0, Lorg/apache/lucene/index/SegmentWriteState;->numDocs:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lorg/apache/lucene/index/TermVectorsTermsWriter;->fill(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58
    const-wide/16 v10, 0x4

    :try_start_2
    move-object/from16 v0, p2

    iget v9, v0, Lorg/apache/lucene/index/SegmentWriteState;->numDocs:I

    int-to-long v12, v9

    const-wide/16 v14, 0x10

    mul-long/2addr v12, v14

    add-long/2addr v10, v12

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v9}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v12

    cmp-long v9, v10, v12

    if-eqz v9, :cond_1

    .line 64
    move-object/from16 v0, p2

    iget-object v9, v0, Lorg/apache/lucene/index/SegmentWriteState;->segmentName:Ljava/lang/String;

    const-string/jumbo v10, "tvx"

    invoke-static {v9, v10}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 65
    .local v6, "idxName":Ljava/lang/String;
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "tvx size mismatch: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p2

    iget v11, v0, Lorg/apache/lucene/index/SegmentWriteState;->numDocs:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " docs vs "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v11}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " length in bytes of "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " file exists?="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p2

    iget-object v11, v0, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v11, v6}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 68
    .end local v6    # "idxName":Ljava/lang/String;
    :catchall_1
    move-exception v9

    const/4 v10, 0x3

    :try_start_3
    new-array v10, v10, [Ljava/io/Closeable;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    aput-object v12, v10, v11

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    aput-object v12, v10, v11

    const/4 v11, 0x2

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    aput-object v12, v10, v11

    invoke-static {v10}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 69
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput-object v10, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v0, p0

    iput-object v10, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v0, p0

    iput-object v10, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    .line 68
    throw v9

    :cond_1
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/io/Closeable;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    aput-object v11, v9, v10

    const/4 v10, 0x2

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    aput-object v11, v9, v10

    invoke-static {v9}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 69
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-object v9, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v0, p0

    iput-object v9, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v0, p0

    iput-object v9, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    .line 72
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput v9, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->lastDocID:I

    .line 73
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->hasVectors:Z

    move-object/from16 v0, p2

    iput-boolean v9, v0, Lorg/apache/lucene/index/SegmentWriteState;->hasVectors:Z

    .line 74
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->hasVectors:Z

    .line 77
    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 78
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/TermsHashConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Collection;

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/TermsHashConsumerPerField;

    .line 79
    .local v3, "field":Lorg/apache/lucene/index/TermsHashConsumerPerField;
    move-object v0, v3

    check-cast v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;

    move-object v7, v0

    .line 80
    .local v7, "perField":Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;
    iget-object v9, v7, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v9}, Lorg/apache/lucene/index/TermsHashPerField;->reset()V

    .line 81
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->shrinkHash()V

    goto :goto_1

    .line 84
    .end local v3    # "field":Lorg/apache/lucene/index/TermsHashConsumerPerField;
    .end local v7    # "perField":Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;
    :cond_3
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    .line 85
    .local v8, "perThread":Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;
    iget-object v9, v8, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->termsHashPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lorg/apache/lucene/index/TermsHashPerThread;->reset(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 87
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/TermsHashConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/TermsHashConsumerPerField;>;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v8    # "perThread":Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;
    :cond_4
    monitor-exit p0

    return-void
.end method

.method declared-synchronized free(Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;)V
    .locals 3
    .param p1, "doc"    # Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    .prologue
    .line 213
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->freeCount:I

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docFreeList:[Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 214
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docFreeList:[Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    iget v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->freeCount:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->freeCount:I

    aput-object p1, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 215
    monitor-exit p0

    return-void
.end method

.method declared-synchronized getPerDoc()Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;
    .locals 2

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->freeCount:I

    if-nez v0, :cond_2

    .line 93
    iget v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->allocCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->allocCount:I

    .line 94
    iget v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->allocCount:I

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docFreeList:[Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    array-length v1, v1

    if-le v0, v1, :cond_1

    .line 98
    sget-boolean v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->allocCount:I

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docFreeList:[Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 99
    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->allocCount:I

    sget v1, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    new-array v0, v0, [Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docFreeList:[Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    .line 101
    :cond_1
    new-instance v0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;-><init>(Lorg/apache/lucene/index/TermVectorsTermsWriter;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_2
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docFreeList:[Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    iget v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->freeCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->freeCount:I

    aget-object v0, v0, v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method declared-synchronized initTermVectorsWriter()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v1, :cond_1

    .line 123
    const/4 v0, 0x0

    .line 129
    .local v0, "success":Z
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->hasVectors:Z

    .line 130
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->getSegment()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "tvx"

    invoke-static {v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    .line 131
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->getSegment()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "tvd"

    invoke-static {v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    .line 132
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->getSegment()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "tvf"

    invoke-static {v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    .line 134
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 135
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 136
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 137
    const/4 v0, 0x1

    .line 139
    if-nez v0, :cond_0

    .line 140
    const/4 v1, 0x3

    :try_start_2
    new-array v1, v1, [Ljava/io/Closeable;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v1, v2

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 143
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->lastDocID:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 145
    .end local v0    # "success":Z
    :cond_1
    monitor-exit p0

    return-void

    .line 139
    .restart local v0    # "success":Z
    :catchall_0
    move-exception v1

    if-nez v0, :cond_2

    .line 140
    const/4 v2, 0x3

    :try_start_3
    new-array v2, v2, [Ljava/io/Closeable;

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    aput-object v4, v2, v3

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 139
    :cond_2
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 122
    .end local v0    # "success":Z
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method
