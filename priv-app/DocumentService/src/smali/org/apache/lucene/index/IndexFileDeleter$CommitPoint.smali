.class final Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;
.super Lorg/apache/lucene/index/IndexCommit;
.source "IndexFileDeleter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/IndexFileDeleter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CommitPoint"
.end annotation


# instance fields
.field commitsToDelete:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;",
            ">;"
        }
    .end annotation
.end field

.field deleted:Z

.field directory:Lorg/apache/lucene/store/Directory;

.field files:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field generation:J

.field private final segmentCount:I

.field segmentsFileName:Ljava/lang/String;

.field final userData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field version:J


# direct methods
.method public constructor <init>(Ljava/util/Collection;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;)V
    .locals 2
    .param p2, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p3, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;",
            ">;",
            "Lorg/apache/lucene/store/Directory;",
            "Lorg/apache/lucene/index/SegmentInfos;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 648
    .local p1, "commitsToDelete":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;>;"
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexCommit;-><init>()V

    .line 649
    iput-object p2, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->directory:Lorg/apache/lucene/store/Directory;

    .line 650
    iput-object p1, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->commitsToDelete:Ljava/util/Collection;

    .line 651
    invoke-virtual {p3}, Lorg/apache/lucene/index/SegmentInfos;->getUserData()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->userData:Ljava/util/Map;

    .line 652
    invoke-virtual {p3}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->segmentsFileName:Ljava/lang/String;

    .line 653
    invoke-virtual {p3}, Lorg/apache/lucene/index/SegmentInfos;->getVersion()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->version:J

    .line 654
    invoke-virtual {p3}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->generation:J

    .line 655
    const/4 v0, 0x1

    invoke-virtual {p3, p2, v0}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->files:Ljava/util/Collection;

    .line 656
    invoke-virtual {p3}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->segmentCount:I

    .line 657
    return-void
.end method


# virtual methods
.method public delete()V
    .locals 1

    .prologue
    .line 705
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->deleted:Z

    if-nez v0, :cond_0

    .line 706
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->deleted:Z

    .line 707
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->commitsToDelete:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 709
    :cond_0
    return-void
.end method

.method public getDirectory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 681
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->directory:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method public getFileNames()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 676
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->files:Ljava/util/Collection;

    return-object v0
.end method

.method public getGeneration()J
    .locals 2

    .prologue
    .line 691
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->generation:J

    return-wide v0
.end method

.method public getSegmentCount()I
    .locals 1

    .prologue
    .line 666
    iget v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->segmentCount:I

    return v0
.end method

.method public getSegmentsFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->segmentsFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getUserData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 696
    iget-object v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->userData:Ljava/util/Map;

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 686
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->version:J

    return-wide v0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 713
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->deleted:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 661
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "IndexFileDeleter.CommitPoint("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/IndexFileDeleter$CommitPoint;->segmentsFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
