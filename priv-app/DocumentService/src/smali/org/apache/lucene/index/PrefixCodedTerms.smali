.class Lorg/apache/lucene/index/PrefixCodedTerms;
.super Ljava/lang/Object;
.source "PrefixCodedTerms.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/PrefixCodedTerms$1;,
        Lorg/apache/lucene/index/PrefixCodedTerms$Builder;,
        Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/lucene/index/Term;",
        ">;"
    }
.end annotation


# instance fields
.field final buffer:Lorg/apache/lucene/store/RAMFile;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/store/RAMFile;)V
    .locals 0
    .param p1, "buffer"    # Lorg/apache/lucene/store/RAMFile;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lorg/apache/lucene/index/PrefixCodedTerms;->buffer:Lorg/apache/lucene/store/RAMFile;

    .line 39
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/store/RAMFile;Lorg/apache/lucene/index/PrefixCodedTerms$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/store/RAMFile;
    .param p2, "x1"    # Lorg/apache/lucene/index/PrefixCodedTerms$1;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/PrefixCodedTerms;-><init>(Lorg/apache/lucene/store/RAMFile;)V

    return-void
.end method


# virtual methods
.method public getSizeInBytes()J
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/index/PrefixCodedTerms;->buffer:Lorg/apache/lucene/store/RAMFile;

    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMFile;->getSizeInBytes()J

    move-result-wide v0

    return-wide v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/PrefixCodedTerms$PrefixCodedTermsIterator;-><init>(Lorg/apache/lucene/index/PrefixCodedTerms;)V

    return-object v0
.end method
