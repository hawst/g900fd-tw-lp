.class Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;
.super Ljava/lang/Object;
.source "DocumentsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WaitQueue"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field nextWriteDocID:I

.field nextWriteLoc:I

.field numWaiting:I

.field final synthetic this$0:Lorg/apache/lucene/index/DocumentsWriter;

.field waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

.field waitingBytes:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1345
    const-class v0, Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriter;)V
    .locals 1

    .prologue
    .line 1352
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->this$0:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1353
    const/16 v0, 0xa

    new-array v0, v0, [Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    .line 1354
    return-void
.end method

.method private writeDocument(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)V
    .locals 3
    .param p1, "doc"    # Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1401
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->this$0:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

    if-eq p1, v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteDocID:I

    iget v2, p1, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->docID:I

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1402
    :cond_0
    const/4 v0, 0x0

    .line 1404
    .local v0, "success":Z
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->finish()V

    .line 1405
    iget v1, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteDocID:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteDocID:I

    .line 1406
    iget v1, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteLoc:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteLoc:I

    .line 1407
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget v1, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteLoc:I

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    array-length v2, v2

    if-le v1, v2, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1413
    :catchall_0
    move-exception v1

    if-nez v0, :cond_1

    .line 1414
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->this$0:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->setAborting()V

    :cond_1
    throw v1

    .line 1408
    :cond_2
    :try_start_1
    iget v1, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteLoc:I

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    array-length v2, v2

    if-ne v1, v2, :cond_3

    .line 1409
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteLoc:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1411
    :cond_3
    const/4 v0, 0x1

    .line 1413
    if-nez v0, :cond_4

    .line 1414
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->this$0:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriter;->setAborting()V

    .line 1417
    :cond_4
    return-void
.end method


# virtual methods
.method declared-synchronized abort()V
    .locals 6

    .prologue
    .line 1386
    monitor-enter p0

    const/4 v0, 0x0

    .line 1387
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 1388
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    aget-object v1, v3, v2

    .line 1389
    .local v1, "doc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    if-eqz v1, :cond_0

    .line 1390
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->abort()V

    .line 1391
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    const/4 v4, 0x0

    aput-object v4, v3, v2

    .line 1392
    add-int/lit8 v0, v0, 0x1

    .line 1387
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1395
    .end local v1    # "doc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :cond_1
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waitingBytes:J

    .line 1396
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->numWaiting:I

    if-eq v0, v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1386
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 1397
    :cond_2
    const/4 v3, 0x0

    :try_start_1
    iput v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->numWaiting:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1398
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized add(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)Z
    .locals 8
    .param p1, "doc"    # Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1421
    monitor-enter p0

    :try_start_0
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget v3, p1, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->docID:I

    iget v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteDocID:I

    if-ge v3, v4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 1423
    :cond_0
    :try_start_1
    iget v3, p1, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->docID:I

    iget v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteDocID:I

    if-ne v3, v4, :cond_1

    .line 1424
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->writeDocument(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)V

    .line 1426
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    iget v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteLoc:I

    aget-object p1, v3, v4

    .line 1427
    if-eqz p1, :cond_7

    .line 1428
    iget v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->numWaiting:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->numWaiting:I

    .line 1429
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    iget v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteLoc:I

    const/4 v5, 0x0

    aput-object v5, v3, v4

    .line 1430
    iget-wide v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waitingBytes:J

    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->sizeInBytes()J

    move-result-wide v6

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waitingBytes:J

    .line 1431
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->writeDocument(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)V

    goto :goto_0

    .line 1444
    :cond_1
    iget v3, p1, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->docID:I

    iget v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteDocID:I

    sub-int v0, v3, v4

    .line 1445
    .local v0, "gap":I
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    array-length v3, v3

    if-lt v0, v3, :cond_3

    .line 1447
    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v0, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v3

    new-array v2, v3, [Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    .line 1448
    .local v2, "newArray":[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteLoc:I

    if-gez v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 1449
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    iget v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteLoc:I

    const/4 v5, 0x0

    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    array-length v6, v6

    iget v7, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteLoc:I

    sub-int/2addr v6, v7

    invoke-static {v3, v4, v2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1450
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    const/4 v4, 0x0

    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    array-length v5, v5

    iget v6, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteLoc:I

    sub-int/2addr v5, v6

    iget v6, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteLoc:I

    invoke-static {v3, v4, v2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1451
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteLoc:I

    .line 1452
    iput-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    .line 1453
    iget v3, p1, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->docID:I

    iget v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteDocID:I

    sub-int v0, v3, v4

    .line 1456
    .end local v2    # "newArray":[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :cond_3
    iget v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteLoc:I

    add-int v1, v3, v0

    .line 1457
    .local v1, "loc":I
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    array-length v3, v3

    if-lt v1, v3, :cond_4

    .line 1458
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    array-length v3, v3

    sub-int/2addr v1, v3

    .line 1462
    :cond_4
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->$assertionsDisabled:Z

    if-nez v3, :cond_5

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    array-length v3, v3

    if-lt v1, v3, :cond_5

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 1465
    :cond_5
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->$assertionsDisabled:Z

    if-nez v3, :cond_6

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    aget-object v3, v3, v1

    if-eqz v3, :cond_6

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 1466
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waiting:[Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    aput-object p1, v3, v1

    .line 1467
    iget v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->numWaiting:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->numWaiting:I

    .line 1468
    iget-wide v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waitingBytes:J

    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->sizeInBytes()J

    move-result-wide v6

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waitingBytes:J

    .line 1471
    .end local v0    # "gap":I
    .end local v1    # "loc":I
    :cond_7
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->doPause()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    monitor-exit p0

    return v3
.end method

.method declared-synchronized doPause()Z
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    .line 1375
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->this$0:Lorg/apache/lucene/index/DocumentsWriter;

    # getter for: Lorg/apache/lucene/index/DocumentsWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;
    invoke-static {v4}, Lorg/apache/lucene/index/DocumentsWriter;->access$100(Lorg/apache/lucene/index/DocumentsWriter;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v0

    .line 1377
    .local v0, "mb":D
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v4, v0, v4

    if-nez v4, :cond_0

    .line 1378
    const-wide/32 v2, 0x400000

    .line 1382
    .local v2, "waitQueuePauseBytes":J
    :goto_0
    iget-wide v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waitingBytes:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v4, v4, v2

    if-lez v4, :cond_1

    const/4 v4, 0x1

    :goto_1
    monitor-exit p0

    return v4

    .line 1380
    .end local v2    # "waitQueuePauseBytes":J
    :cond_0
    mul-double v4, v0, v6

    mul-double/2addr v4, v6

    const-wide v6, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v4, v6

    double-to-long v2, v4

    .restart local v2    # "waitQueuePauseBytes":J
    goto :goto_0

    .line 1382
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 1375
    .end local v0    # "mb":D
    .end local v2    # "waitQueuePauseBytes":J
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method declared-synchronized doResume()Z
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    .line 1364
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->this$0:Lorg/apache/lucene/index/DocumentsWriter;

    # getter for: Lorg/apache/lucene/index/DocumentsWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;
    invoke-static {v4}, Lorg/apache/lucene/index/DocumentsWriter;->access$100(Lorg/apache/lucene/index/DocumentsWriter;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v0

    .line 1366
    .local v0, "mb":D
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v4, v0, v4

    if-nez v4, :cond_0

    .line 1367
    const-wide/32 v2, 0x200000

    .line 1371
    .local v2, "waitQueueResumeBytes":J
    :goto_0
    iget-wide v4, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waitingBytes:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v4, v4, v2

    if-gtz v4, :cond_1

    const/4 v4, 0x1

    :goto_1
    monitor-exit p0

    return v4

    .line 1369
    .end local v2    # "waitQueueResumeBytes":J
    :cond_0
    mul-double v4, v0, v6

    mul-double/2addr v4, v6

    const-wide v6, 0x3fa999999999999aL    # 0.05

    mul-double/2addr v4, v6

    double-to-long v2, v4

    .restart local v2    # "waitQueueResumeBytes":J
    goto :goto_0

    .line 1371
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 1364
    .end local v0    # "mb":D
    .end local v2    # "waitQueueResumeBytes":J
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method declared-synchronized reset()V
    .locals 4

    .prologue
    .line 1358
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->numWaiting:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1359
    :cond_0
    :try_start_1
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waitingBytes:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1360
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->nextWriteDocID:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1361
    monitor-exit p0

    return-void
.end method
