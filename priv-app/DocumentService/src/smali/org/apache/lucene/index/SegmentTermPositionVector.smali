.class Lorg/apache/lucene/index/SegmentTermPositionVector;
.super Lorg/apache/lucene/index/SegmentTermVector;
.source "SegmentTermPositionVector.java"

# interfaces
.implements Lorg/apache/lucene/index/TermPositionVector;


# static fields
.field public static final EMPTY_TERM_POS:[I


# instance fields
.field protected offsets:[[Lorg/apache/lucene/index/TermVectorOffsetInfo;

.field protected positions:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lorg/apache/lucene/index/SegmentTermPositionVector;->EMPTY_TERM_POS:[I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Ljava/lang/String;[I[[I[[Lorg/apache/lucene/index/TermVectorOffsetInfo;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "terms"    # [Ljava/lang/String;
    .param p3, "termFreqs"    # [I
    .param p4, "positions"    # [[I
    .param p5, "offsets"    # [[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/index/SegmentTermVector;-><init>(Ljava/lang/String;[Ljava/lang/String;[I)V

    .line 27
    iput-object p5, p0, Lorg/apache/lucene/index/SegmentTermPositionVector;->offsets:[[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    .line 28
    iput-object p4, p0, Lorg/apache/lucene/index/SegmentTermPositionVector;->positions:[[I

    .line 29
    return-void
.end method


# virtual methods
.method public getOffsets(I)[Lorg/apache/lucene/index/TermVectorOffsetInfo;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 39
    sget-object v0, Lorg/apache/lucene/index/TermVectorOffsetInfo;->EMPTY_OFFSET_INFO:[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    .line 40
    .local v0, "result":[Lorg/apache/lucene/index/TermVectorOffsetInfo;
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermPositionVector;->offsets:[[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    if-nez v1, :cond_0

    .line 41
    const/4 v1, 0x0

    .line 46
    :goto_0
    return-object v1

    .line 42
    :cond_0
    if-ltz p1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermPositionVector;->offsets:[[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    array-length v1, v1

    if-ge p1, v1, :cond_1

    .line 44
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermPositionVector;->offsets:[[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    aget-object v0, v1, p1

    :cond_1
    move-object v1, v0

    .line 46
    goto :goto_0
.end method

.method public getTermPositions(I)[I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 55
    sget-object v0, Lorg/apache/lucene/index/SegmentTermPositionVector;->EMPTY_TERM_POS:[I

    .line 56
    .local v0, "result":[I
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermPositionVector;->positions:[[I

    if-nez v1, :cond_0

    .line 57
    const/4 v1, 0x0

    .line 63
    :goto_0
    return-object v1

    .line 58
    :cond_0
    if-ltz p1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermPositionVector;->positions:[[I

    array-length v1, v1

    if-ge p1, v1, :cond_1

    .line 60
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentTermPositionVector;->positions:[[I

    aget-object v0, v1, p1

    :cond_1
    move-object v1, v0

    .line 63
    goto :goto_0
.end method
