.class final Lorg/apache/lucene/index/TermBuffer;
.super Ljava/lang/Object;
.source "TermBuffer.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private bytes:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

.field private dirty:Z

.field private field:Ljava/lang/String;

.field private preUTF8Strings:Z

.field private term:Lorg/apache/lucene/index/Term;

.field private text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    invoke-direct {v0}, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    .line 32
    new-instance v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-direct {v0}, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/TermBuffer;->bytes:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    return-void
.end method

.method private static final compareChars([CI[CI)I
    .locals 5
    .param p0, "chars1"    # [C
    .param p1, "len1"    # I
    .param p2, "chars2"    # [C
    .param p3, "len2"    # I

    .prologue
    .line 43
    if-ge p1, p3, :cond_0

    move v2, p1

    .line 44
    .local v2, "end":I
    :goto_0
    const/4 v3, 0x0

    .local v3, "k":I
    :goto_1
    if-ge v3, v2, :cond_2

    .line 45
    aget-char v0, p0, v3

    .line 46
    .local v0, "c1":C
    aget-char v1, p2, v3

    .line 47
    .local v1, "c2":C
    if-eq v0, v1, :cond_1

    .line 48
    sub-int v4, v0, v1

    .line 51
    .end local v0    # "c1":C
    .end local v1    # "c2":C
    :goto_2
    return v4

    .end local v2    # "end":I
    .end local v3    # "k":I
    :cond_0
    move v2, p3

    .line 43
    goto :goto_0

    .line 44
    .restart local v0    # "c1":C
    .restart local v1    # "c2":C
    .restart local v2    # "end":I
    .restart local v3    # "k":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 51
    .end local v0    # "c1":C
    .end local v1    # "c2":C
    :cond_2
    sub-int v4, p1, p3

    goto :goto_2
.end method


# virtual methods
.method protected clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 129
    const/4 v1, 0x0

    .line 131
    .local v1, "clone":Lorg/apache/lucene/index/TermBuffer;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lorg/apache/lucene/index/TermBuffer;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :goto_0
    const/4 v2, 0x1

    iput-boolean v2, v1, Lorg/apache/lucene/index/TermBuffer;->dirty:Z

    .line 135
    new-instance v2, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-direct {v2}, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;-><init>()V

    iput-object v2, v1, Lorg/apache/lucene/index/TermBuffer;->bytes:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    .line 136
    new-instance v2, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    invoke-direct {v2}, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;-><init>()V

    iput-object v2, v1, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    .line 137
    iget-object v2, v1, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget-object v3, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->copyText(Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;)V

    .line 138
    return-object v1

    .line 132
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public final compareTo(Lorg/apache/lucene/index/TermBuffer;)I
    .locals 4
    .param p1, "other"    # Lorg/apache/lucene/index/TermBuffer;

    .prologue
    .line 35
    iget-object v0, p0, Lorg/apache/lucene/index/TermBuffer;->field:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/index/TermBuffer;->field:Ljava/lang/String;

    if-ne v0, v1, :cond_0

    .line 36
    iget-object v0, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget-object v0, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    iget-object v1, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget v1, v1, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    iget-object v2, p1, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget-object v2, v2, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    iget-object v3, p1, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget v3, v3, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/index/TermBuffer;->compareChars([CI[CI)I

    move-result v0

    .line 38
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/TermBuffer;->field:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/index/TermBuffer;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final read(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 7
    .param p1, "input"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 63
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/index/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    .line 64
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    .line 65
    .local v1, "start":I
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 66
    .local v0, "length":I
    add-int v2, v1, v0

    .line 67
    .local v2, "totalLength":I
    iget-boolean v3, p0, Lorg/apache/lucene/index/TermBuffer;->preUTF8Strings:Z

    if-eqz v3, :cond_0

    .line 68
    iget-object v3, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->setLength(I)V

    .line 69
    iget-object v3, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget-object v3, v3, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    invoke-virtual {p1, v3, v1, v0}, Lorg/apache/lucene/store/IndexInput;->readChars([CII)V

    .line 86
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    invoke-virtual {p2, v3}, Lorg/apache/lucene/index/FieldInfos;->fieldName(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/index/TermBuffer;->field:Ljava/lang/String;

    .line 87
    return-void

    .line 72
    :cond_0
    iget-boolean v3, p0, Lorg/apache/lucene/index/TermBuffer;->dirty:Z

    if-eqz v3, :cond_1

    .line 74
    iget-object v3, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget-object v3, v3, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    iget-object v4, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget v4, v4, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    iget-object v5, p0, Lorg/apache/lucene/index/TermBuffer;->bytes:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-static {v3, v6, v4, v5}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8([CIILorg/apache/lucene/util/UnicodeUtil$UTF8Result;)V

    .line 75
    iget-object v3, p0, Lorg/apache/lucene/index/TermBuffer;->bytes:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->setLength(I)V

    .line 76
    iget-object v3, p0, Lorg/apache/lucene/index/TermBuffer;->bytes:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    iget-object v3, v3, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    invoke-virtual {p1, v3, v1, v0}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 77
    iget-object v3, p0, Lorg/apache/lucene/index/TermBuffer;->bytes:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    iget-object v3, v3, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    iget-object v4, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    invoke-static {v3, v6, v2, v4}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/UnicodeUtil$UTF16Result;)V

    .line 78
    iput-boolean v6, p0, Lorg/apache/lucene/index/TermBuffer;->dirty:Z

    goto :goto_0

    .line 81
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/TermBuffer;->bytes:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->setLength(I)V

    .line 82
    iget-object v3, p0, Lorg/apache/lucene/index/TermBuffer;->bytes:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    iget-object v3, v3, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    invoke-virtual {p1, v3, v1, v0}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 83
    iget-object v3, p0, Lorg/apache/lucene/index/TermBuffer;->bytes:Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    iget-object v3, v3, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    iget-object v4, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    invoke-static {v3, v1, v0, v4}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/UnicodeUtil$UTF16Result;)V

    goto :goto_0
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 111
    iput-object v2, p0, Lorg/apache/lucene/index/TermBuffer;->field:Ljava/lang/String;

    .line 112
    iget-object v0, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->setLength(I)V

    .line 113
    iput-object v2, p0, Lorg/apache/lucene/index/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/TermBuffer;->dirty:Z

    .line 115
    return-void
.end method

.method public final set(Lorg/apache/lucene/index/Term;)V
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    const/4 v3, 0x0

    .line 90
    if-nez p1, :cond_0

    .line 91
    invoke-virtual {p0}, Lorg/apache/lucene/index/TermBuffer;->reset()V

    .line 101
    :goto_0
    return-void

    .line 94
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    .line 95
    .local v1, "termText":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .line 96
    .local v0, "termLen":I
    iget-object v2, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->setLength(I)V

    .line 97
    iget-object v2, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget-object v2, v2, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    invoke-virtual {v1, v3, v0, v2, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 98
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/index/TermBuffer;->dirty:Z

    .line 99
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/index/TermBuffer;->field:Ljava/lang/String;

    .line 100
    iput-object p1, p0, Lorg/apache/lucene/index/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    goto :goto_0
.end method

.method public final set(Lorg/apache/lucene/index/TermBuffer;)V
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/index/TermBuffer;

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget-object v1, p1, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->copyText(Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;)V

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/TermBuffer;->dirty:Z

    .line 106
    iget-object v0, p1, Lorg/apache/lucene/index/TermBuffer;->field:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/index/TermBuffer;->field:Ljava/lang/String;

    .line 107
    iget-object v0, p1, Lorg/apache/lucene/index/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    iput-object v0, p0, Lorg/apache/lucene/index/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    .line 108
    return-void
.end method

.method setPreUTF8Strings()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/TermBuffer;->preUTF8Strings:Z

    .line 59
    return-void
.end method

.method public toTerm()Lorg/apache/lucene/index/Term;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 118
    iget-object v0, p0, Lorg/apache/lucene/index/TermBuffer;->field:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 119
    const/4 v0, 0x0

    .line 124
    :goto_0
    return-object v0

    .line 121
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    if-nez v0, :cond_1

    .line 122
    new-instance v0, Lorg/apache/lucene/index/Term;

    iget-object v1, p0, Lorg/apache/lucene/index/TermBuffer;->field:Ljava/lang/String;

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget-object v3, v3, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->result:[C

    iget-object v4, p0, Lorg/apache/lucene/index/TermBuffer;->text:Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;

    iget v4, v4, Lorg/apache/lucene/util/UnicodeUtil$UTF16Result;->length:I

    invoke-direct {v2, v3, v5, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lorg/apache/lucene/index/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    .line 124
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    goto :goto_0
.end method
