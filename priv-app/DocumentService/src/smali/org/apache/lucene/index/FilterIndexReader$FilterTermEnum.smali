.class public Lorg/apache/lucene/index/FilterIndexReader$FilterTermEnum;
.super Lorg/apache/lucene/index/TermEnum;
.source "FilterIndexReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FilterIndexReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FilterTermEnum"
.end annotation


# instance fields
.field protected in:Lorg/apache/lucene/index/TermEnum;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/TermEnum;)V
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/index/TermEnum;

    .prologue
    .line 90
    invoke-direct {p0}, Lorg/apache/lucene/index/TermEnum;-><init>()V

    iput-object p1, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermEnum;->in:Lorg/apache/lucene/index/TermEnum;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermEnum;->in:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermEnum;->close()V

    return-void
.end method

.method public docFreq()I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermEnum;->in:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermEnum;->docFreq()I

    move-result v0

    return v0
.end method

.method public next()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermEnum;->in:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermEnum;->next()Z

    move-result v0

    return v0
.end method

.method public term()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermEnum;->in:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    return-object v0
.end method
