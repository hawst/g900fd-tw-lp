.class Lorg/apache/lucene/index/ReadOnlyDirectoryReader;
.super Lorg/apache/lucene/index/DirectoryReader;
.source "ReadOnlyDirectoryReader.java"


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/SegmentInfos;IZ)V
    .locals 0
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p3, "termInfosIndexDivisor"    # I
    .param p4, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/index/DirectoryReader;-><init>(Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/SegmentInfos;IZ)V

    .line 37
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/IndexDeletionPolicy;I)V
    .locals 6
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "sis"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p3, "deletionPolicy"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .param p4, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/DirectoryReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/IndexDeletionPolicy;ZI)V

    .line 28
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;[Lorg/apache/lucene/index/SegmentReader;[ILjava/util/Map;ZI)V
    .locals 9
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p3, "oldReaders"    # [Lorg/apache/lucene/index/SegmentReader;
    .param p4, "oldStarts"    # [I
    .param p6, "doClone"    # Z
    .param p7, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "[",
            "Lorg/apache/lucene/index/SegmentReader;",
            "[I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;ZI)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    .local p5, "oldNormsCache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lorg/apache/lucene/index/DirectoryReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;[Lorg/apache/lucene/index/SegmentReader;[ILjava/util/Map;ZZI)V

    .line 33
    return-void
.end method


# virtual methods
.method protected acquireWriteLock()V
    .locals 0

    .prologue
    .line 41
    invoke-static {}, Lorg/apache/lucene/index/ReadOnlySegmentReader;->noWrite()V

    .line 42
    return-void
.end method
