.class public final enum Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
.super Ljava/lang/Enum;
.source "IndexWriterConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/IndexWriterConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OpenMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

.field public static final enum APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

.field public static final enum CREATE:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

.field public static final enum CREATE_OR_APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 56
    new-instance v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    const-string/jumbo v1, "CREATE"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    new-instance v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    const-string/jumbo v1, "APPEND"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    new-instance v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    const-string/jumbo v1, "CREATE_OR_APPEND"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE_OR_APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    sget-object v1, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE_OR_APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    aput-object v1, v0, v4

    sput-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->$VALUES:[Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    const-class v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    return-object v0
.end method

.method public static final values()[Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->$VALUES:[Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    invoke-virtual {v0}, [Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    return-object v0
.end method
