.class final Lorg/apache/lucene/index/FormatPostingsPositionsWriter;
.super Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;
.source "FormatPostingsPositionsWriter.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field lastPayloadLength:I

.field lastPosition:I

.field omitTermFreqAndPositions:Z

.field final out:Lorg/apache/lucene/store/IndexOutput;

.field final parent:Lorg/apache/lucene/index/FormatPostingsDocsWriter;

.field storePayloads:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/index/FormatPostingsDocsWriter;)V
    .locals 3
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "parent"    # Lorg/apache/lucene/index/FormatPostingsDocsWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;-><init>()V

    .line 35
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->lastPayloadLength:I

    .line 38
    iput-object p2, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsDocsWriter;

    .line 39
    iget-boolean v0, p2, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->omitTermFreqAndPositions:Z

    iput-boolean v0, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->omitTermFreqAndPositions:Z

    .line 40
    iget-object v0, p2, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsFieldsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfos;->hasProx()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p2, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsFieldsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v1, p2, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsFieldsWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->segment:Ljava/lang/String;

    const-string/jumbo v2, "prx"

    invoke-static {v1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    .line 44
    iget-object v0, p2, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->skipListWriter:Lorg/apache/lucene/index/DefaultSkipListWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DefaultSkipListWriter;->setProxOutput(Lorg/apache/lucene/store/IndexOutput;)V

    .line 48
    :goto_0
    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    goto :goto_0
.end method


# virtual methods
.method addPosition(I[BII)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "payload"    # [B
    .param p3, "payloadOffset"    # I
    .param p4, "payloadLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    sget-boolean v1, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->omitTermFreqAndPositions:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    const-string/jumbo v2, "omitTermFreqAndPositions is true"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 56
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 58
    :cond_1
    iget v1, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->lastPosition:I

    sub-int v0, p1, v1

    .line 59
    .local v0, "delta":I
    iput p1, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->lastPosition:I

    .line 61
    iget-boolean v1, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->storePayloads:Z

    if-eqz v1, :cond_4

    .line 62
    iget v1, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->lastPayloadLength:I

    if-eq p4, v1, :cond_3

    .line 63
    iput p4, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->lastPayloadLength:I

    .line 64
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    shl-int/lit8 v2, v0, 0x1

    or-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 65
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, p4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 68
    :goto_0
    if-lez p4, :cond_2

    .line 69
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, p2, p4}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V

    .line 72
    :cond_2
    :goto_1
    return-void

    .line 67
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    shl-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    goto :goto_0

    .line 71
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    goto :goto_1
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 88
    return-void
.end method

.method finish()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->lastPosition:I

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->lastPayloadLength:I

    .line 84
    return-void
.end method

.method setField(Lorg/apache/lucene/index/FieldInfo;)V
    .locals 3
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    const/4 v1, 0x0

    .line 75
    iget-object v0, p1, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->omitTermFreqAndPositions:Z

    .line 76
    iget-boolean v0, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->omitTermFreqAndPositions:Z

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->storePayloads:Z

    .line 77
    return-void

    :cond_0
    move v0, v1

    .line 75
    goto :goto_0

    .line 76
    :cond_1
    iget-boolean v1, p1, Lorg/apache/lucene/index/FieldInfo;->storePayloads:Z

    goto :goto_1
.end method
