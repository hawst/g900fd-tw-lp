.class public Lorg/apache/lucene/index/MergePolicy$MergeException;
.super Ljava/lang/RuntimeException;
.source "MergePolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MergePolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MergeException"
.end annotation


# instance fields
.field private dir:Lorg/apache/lucene/store/Directory;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/store/Directory;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "dir"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 226
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 227
    iput-object p2, p0, Lorg/apache/lucene/index/MergePolicy$MergeException;->dir:Lorg/apache/lucene/store/Directory;

    .line 228
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;Lorg/apache/lucene/store/Directory;)V
    .locals 0
    .param p1, "exc"    # Ljava/lang/Throwable;
    .param p2, "dir"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 231
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 232
    iput-object p2, p0, Lorg/apache/lucene/index/MergePolicy$MergeException;->dir:Lorg/apache/lucene/store/Directory;

    .line 233
    return-void
.end method


# virtual methods
.method public getDirectory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lorg/apache/lucene/index/MergePolicy$MergeException;->dir:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method
