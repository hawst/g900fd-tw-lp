.class abstract Lorg/apache/lucene/index/TermsHashConsumerPerField;
.super Ljava/lang/Object;
.source "TermsHashConsumerPerField.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract addTerm(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract createPostingsArray(I)Lorg/apache/lucene/index/ParallelPostingsArray;
.end method

.method abstract finish()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract getStreamCount()I
.end method

.method abstract newTerm(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract skippingLongTerm()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract start(Lorg/apache/lucene/document/Fieldable;)V
.end method

.method abstract start([Lorg/apache/lucene/document/Fieldable;I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
