.class Lorg/apache/lucene/index/SegmentReader$FieldsReaderLocal;
.super Lorg/apache/lucene/util/CloseableThreadLocal;
.source "SegmentReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/SegmentReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FieldsReaderLocal"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/CloseableThreadLocal",
        "<",
        "Lorg/apache/lucene/index/FieldsReader;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/index/SegmentReader;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/index/SegmentReader;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentReader$FieldsReaderLocal;->this$0:Lorg/apache/lucene/index/SegmentReader;

    invoke-direct {p0}, Lorg/apache/lucene/util/CloseableThreadLocal;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/index/SegmentReader;Lorg/apache/lucene/index/SegmentReader$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/index/SegmentReader;
    .param p2, "x1"    # Lorg/apache/lucene/index/SegmentReader$1;

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/SegmentReader$FieldsReaderLocal;-><init>(Lorg/apache/lucene/index/SegmentReader;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic initialValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader$FieldsReaderLocal;->initialValue()Lorg/apache/lucene/index/FieldsReader;

    move-result-object v0

    return-object v0
.end method

.method protected initialValue()Lorg/apache/lucene/index/FieldsReader;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader$FieldsReaderLocal;->this$0:Lorg/apache/lucene/index/SegmentReader;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentCoreReaders;->getFieldsReaderOrig()Lorg/apache/lucene/index/FieldsReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldsReader;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldsReader;

    return-object v0
.end method
