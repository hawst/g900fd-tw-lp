.class final Lorg/apache/lucene/index/IntBlockPool;
.super Ljava/lang/Object;
.source "IntBlockPool.java"


# instance fields
.field public buffer:[I

.field bufferUpto:I

.field public buffers:[[I

.field private final docWriter:Lorg/apache/lucene/index/DocumentsWriter;

.field public intOffset:I

.field public intUpto:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriter;)V
    .locals 1
    .param p1, "docWriter"    # Lorg/apache/lucene/index/DocumentsWriter;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/16 v0, 0xa

    new-array v0, v0, [[I

    iput-object v0, p0, Lorg/apache/lucene/index/IntBlockPool;->buffers:[[I

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/IntBlockPool;->bufferUpto:I

    .line 25
    const/16 v0, 0x2000

    iput v0, p0, Lorg/apache/lucene/index/IntBlockPool;->intUpto:I

    .line 28
    const/16 v0, -0x2000

    iput v0, p0, Lorg/apache/lucene/index/IntBlockPool;->intOffset:I

    .line 33
    iput-object p1, p0, Lorg/apache/lucene/index/IntBlockPool;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 34
    return-void
.end method


# virtual methods
.method public nextBuffer()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 51
    iget v1, p0, Lorg/apache/lucene/index/IntBlockPool;->bufferUpto:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/index/IntBlockPool;->buffers:[[I

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 52
    iget-object v1, p0, Lorg/apache/lucene/index/IntBlockPool;->buffers:[[I

    array-length v1, v1

    int-to-double v2, v1

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v2, v4

    double-to-int v1, v2

    new-array v0, v1, [[I

    .line 53
    .local v0, "newBuffers":[[I
    iget-object v1, p0, Lorg/apache/lucene/index/IntBlockPool;->buffers:[[I

    iget-object v2, p0, Lorg/apache/lucene/index/IntBlockPool;->buffers:[[I

    array-length v2, v2

    invoke-static {v1, v6, v0, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 54
    iput-object v0, p0, Lorg/apache/lucene/index/IntBlockPool;->buffers:[[I

    .line 56
    .end local v0    # "newBuffers":[[I
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/IntBlockPool;->buffers:[[I

    iget v2, p0, Lorg/apache/lucene/index/IntBlockPool;->bufferUpto:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lorg/apache/lucene/index/IntBlockPool;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocumentsWriter;->getIntBlock()[I

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v3, p0, Lorg/apache/lucene/index/IntBlockPool;->buffer:[I

    .line 57
    iget v1, p0, Lorg/apache/lucene/index/IntBlockPool;->bufferUpto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/IntBlockPool;->bufferUpto:I

    .line 59
    iput v6, p0, Lorg/apache/lucene/index/IntBlockPool;->intUpto:I

    .line 60
    iget v1, p0, Lorg/apache/lucene/index/IntBlockPool;->intOffset:I

    add-int/lit16 v1, v1, 0x2000

    iput v1, p0, Lorg/apache/lucene/index/IntBlockPool;->intOffset:I

    .line 61
    return-void
.end method

.method public reset()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 37
    iget v0, p0, Lorg/apache/lucene/index/IntBlockPool;->bufferUpto:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 38
    iget v0, p0, Lorg/apache/lucene/index/IntBlockPool;->bufferUpto:I

    if-lez v0, :cond_0

    .line 40
    iget-object v0, p0, Lorg/apache/lucene/index/IntBlockPool;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/IntBlockPool;->buffers:[[I

    const/4 v2, 0x1

    iget v3, p0, Lorg/apache/lucene/index/IntBlockPool;->bufferUpto:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/index/DocumentsWriter;->recycleIntBlocks([[III)V

    .line 43
    :cond_0
    iput v4, p0, Lorg/apache/lucene/index/IntBlockPool;->bufferUpto:I

    .line 44
    iput v4, p0, Lorg/apache/lucene/index/IntBlockPool;->intUpto:I

    .line 45
    iput v4, p0, Lorg/apache/lucene/index/IntBlockPool;->intOffset:I

    .line 46
    iget-object v0, p0, Lorg/apache/lucene/index/IntBlockPool;->buffers:[[I

    aget-object v0, v0, v4

    iput-object v0, p0, Lorg/apache/lucene/index/IntBlockPool;->buffer:[I

    .line 48
    :cond_1
    return-void
.end method
