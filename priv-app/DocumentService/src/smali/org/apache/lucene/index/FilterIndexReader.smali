.class public Lorg/apache/lucene/index/FilterIndexReader;
.super Lorg/apache/lucene/index/IndexReader;
.source "FilterIndexReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/FilterIndexReader$FilterTermEnum;,
        Lorg/apache/lucene/index/FilterIndexReader$FilterTermPositions;,
        Lorg/apache/lucene/index/FilterIndexReader$FilterTermDocs;
    }
.end annotation


# instance fields
.field protected in:Lorg/apache/lucene/index/IndexReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;)V
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 112
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexReader;-><init>()V

    .line 113
    iput-object p1, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    .line 114
    return-void
.end method


# virtual methods
.method public directory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 119
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->directory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    return-object v0
.end method

.method protected doClose()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 277
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->close()V

    .line 278
    return-void
.end method

.method protected doCommit(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 272
    .local p1, "commitUserData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->commit(Ljava/util/Map;)V

    .line 273
    return-void
.end method

.method protected doDelete(I)V
    .locals 1
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 267
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->deleteDocument(I)V

    return-void
.end method

.method protected doSetNorm(ILjava/lang/String;B)V
    .locals 1
    .param p1, "d"    # I
    .param p2, "f"    # Ljava/lang/String;
    .param p3, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 226
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/IndexReader;->setNorm(ILjava/lang/String;B)V

    .line 227
    return-void
.end method

.method protected doUndeleteAll()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->undeleteAll()V

    return-void
.end method

.method public docFreq(Lorg/apache/lucene/index/Term;)I
    .locals 1
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 243
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 244
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v0

    return v0
.end method

.method public document(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;
    .locals 1
    .param p1, "n"    # I
    .param p2, "fieldSelector"    # Lorg/apache/lucene/document/FieldSelector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 186
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexReader;->document(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;

    move-result-object v0

    return-object v0
.end method

.method public getCommitUserData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 306
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->getCommitUserData()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getCoreCacheKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getDeletesCacheKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->getDeletesCacheKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getFieldInfos()Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v0

    return-object v0
.end method

.method public getIndexCommit()Lorg/apache/lucene/index/IndexCommit;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 125
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->getIndexCommit()Lorg/apache/lucene/index/IndexCommit;

    move-result-object v0

    return-object v0
.end method

.method public getSequentialSubReaders()[Lorg/apache/lucene/index/IndexReader;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->getSequentialSubReaders()[Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method public getTermFreqVector(ILjava/lang/String;)Lorg/apache/lucene/index/TermFreqVector;
    .locals 1
    .param p1, "docNumber"    # I
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 150
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexReader;->getTermFreqVector(ILjava/lang/String;)Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v0

    return-object v0
.end method

.method public getTermFreqVector(ILjava/lang/String;Lorg/apache/lucene/index/TermVectorMapper;)V
    .locals 1
    .param p1, "docNumber"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "mapper"    # Lorg/apache/lucene/index/TermVectorMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 156
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/IndexReader;->getTermFreqVector(ILjava/lang/String;Lorg/apache/lucene/index/TermVectorMapper;)V

    .line 157
    return-void
.end method

.method public getTermFreqVector(ILorg/apache/lucene/index/TermVectorMapper;)V
    .locals 1
    .param p1, "docNumber"    # I
    .param p2, "mapper"    # Lorg/apache/lucene/index/TermVectorMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 162
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexReader;->getTermFreqVector(ILorg/apache/lucene/index/TermVectorMapper;)V

    .line 163
    return-void
.end method

.method public getTermFreqVectors(I)[Lorg/apache/lucene/index/TermFreqVector;
    .locals 1
    .param p1, "docNumber"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 143
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->getTermFreqVectors(I)[Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v0

    return-object v0
.end method

.method public getTermInfosIndexDivisor()I
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 136
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->getTermInfosIndexDivisor()I

    move-result v0

    return v0
.end method

.method public getUniqueTermCount()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 168
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->getUniqueTermCount()J

    move-result-wide v0

    return-wide v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 282
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 283
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->getVersion()J

    move-result-wide v0

    return-wide v0
.end method

.method public hasDeletions()Z
    .locals 1

    .prologue
    .line 197
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 198
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v0

    return v0
.end method

.method public hasNorms(Ljava/lang/String;)Z
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 208
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->hasNorms(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isCurrent()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 289
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->isCurrent()Z

    move-result v0

    return v0
.end method

.method public isDeleted(I)Z
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 192
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->isDeleted(I)Z

    move-result v0

    return v0
.end method

.method public isOptimized()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 295
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 296
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->isOptimized()Z

    move-result v0

    return v0
.end method

.method public maxDoc()I
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v0

    return v0
.end method

.method public norms(Ljava/lang/String;[BI)V
    .locals 1
    .param p1, "f"    # Ljava/lang/String;
    .param p2, "bytes"    # [B
    .param p3, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 219
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 220
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;[BI)V

    .line 221
    return-void
.end method

.method public norms(Ljava/lang/String;)[B
    .locals 1
    .param p1, "f"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 213
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 214
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public numDocs()I
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v0

    return v0
.end method

.method public termDocs()Lorg/apache/lucene/index/TermDocs;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 249
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 250
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->termDocs()Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    return-object v0
.end method

.method public termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 255
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 256
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    return-object v0
.end method

.method public termPositions()Lorg/apache/lucene/index/TermPositions;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 261
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 262
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->termPositions()Lorg/apache/lucene/index/TermPositions;

    move-result-object v0

    return-object v0
.end method

.method public terms()Lorg/apache/lucene/index/TermEnum;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 231
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 232
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->terms()Lorg/apache/lucene/index/TermEnum;

    move-result-object v0

    return-object v0
.end method

.method public terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;
    .locals 1
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 237
    invoke-virtual {p0}, Lorg/apache/lucene/index/FilterIndexReader;->ensureOpen()V

    .line 238
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 328
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "FilterReader("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 329
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/index/FilterIndexReader;->in:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 330
    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 331
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
