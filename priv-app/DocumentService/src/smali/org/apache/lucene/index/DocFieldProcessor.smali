.class final Lorg/apache/lucene/index/DocFieldProcessor;
.super Lorg/apache/lucene/index/DocConsumer;
.source "DocFieldProcessor.java"


# instance fields
.field final consumer:Lorg/apache/lucene/index/DocFieldConsumer;

.field final docWriter:Lorg/apache/lucene/index/DocumentsWriter;

.field final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field final fieldsWriter:Lorg/apache/lucene/index/StoredFieldsWriter;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriter;Lorg/apache/lucene/index/DocFieldConsumer;)V
    .locals 2
    .param p1, "docWriter"    # Lorg/apache/lucene/index/DocumentsWriter;
    .param p2, "consumer"    # Lorg/apache/lucene/index/DocFieldConsumer;

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/index/DocConsumer;-><init>()V

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/index/DocFieldProcessor;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 43
    iput-object p2, p0, Lorg/apache/lucene/index/DocFieldProcessor;->consumer:Lorg/apache/lucene/index/DocFieldConsumer;

    .line 44
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriter;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 45
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {p2, v0}, Lorg/apache/lucene/index/DocFieldConsumer;->setFieldInfos(Lorg/apache/lucene/index/FieldInfos;)V

    .line 46
    new-instance v0, Lorg/apache/lucene/index/StoredFieldsWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/index/StoredFieldsWriter;-><init>(Lorg/apache/lucene/index/DocumentsWriter;Lorg/apache/lucene/index/FieldInfos;)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldsWriter:Lorg/apache/lucene/index/StoredFieldsWriter;

    .line 47
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 2

    .prologue
    .line 73
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldsWriter:Lorg/apache/lucene/index/StoredFieldsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/StoredFieldsWriter;->abort()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->consumer:Lorg/apache/lucene/index/DocFieldConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocFieldConsumer;->abort()V

    .line 77
    return-void

    .line 75
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/lucene/index/DocFieldProcessor;->consumer:Lorg/apache/lucene/index/DocFieldConsumer;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocFieldConsumer;->abort()V

    throw v0
.end method

.method public addThread(Lorg/apache/lucene/index/DocumentsWriterThreadState;)Lorg/apache/lucene/index/DocConsumerPerThread;
    .locals 1
    .param p1, "threadState"    # Lorg/apache/lucene/index/DocumentsWriterThreadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;

    invoke-direct {v0, p1, p0}, Lorg/apache/lucene/index/DocFieldProcessorPerThread;-><init>(Lorg/apache/lucene/index/DocumentsWriterThreadState;Lorg/apache/lucene/index/DocFieldProcessor;)V

    return-object v0
.end method

.method public flush(Ljava/util/Collection;Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 7
    .param p2, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/DocConsumerPerThread;",
            ">;",
            "Lorg/apache/lucene/index/SegmentWriteState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "threads":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/DocConsumerPerThread;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 53
    .local v0, "childThreadsAndFields":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/DocFieldConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/DocFieldConsumerPerField;>;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/DocConsumerPerThread;

    .local v4, "thread":Lorg/apache/lucene/index/DocConsumerPerThread;
    move-object v3, v4

    .line 54
    check-cast v3, Lorg/apache/lucene/index/DocFieldProcessorPerThread;

    .line 55
    .local v3, "perThread":Lorg/apache/lucene/index/DocFieldProcessorPerThread;
    iget-object v5, v3, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->consumer:Lorg/apache/lucene/index/DocFieldConsumerPerThread;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fields()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    invoke-virtual {v3, p2}, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->trimFields(Lorg/apache/lucene/index/SegmentWriteState;)V

    goto :goto_0

    .line 59
    .end local v3    # "perThread":Lorg/apache/lucene/index/DocFieldProcessorPerThread;
    .end local v4    # "thread":Lorg/apache/lucene/index/DocConsumerPerThread;
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldsWriter:Lorg/apache/lucene/index/StoredFieldsWriter;

    invoke-virtual {v5, p2}, Lorg/apache/lucene/index/StoredFieldsWriter;->flush(Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 60
    iget-object v5, p0, Lorg/apache/lucene/index/DocFieldProcessor;->consumer:Lorg/apache/lucene/index/DocFieldConsumer;

    invoke-virtual {v5, v0, p2}, Lorg/apache/lucene/index/DocFieldConsumer;->flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 66
    iget-object v5, p2, Lorg/apache/lucene/index/SegmentWriteState;->segmentName:Ljava/lang/String;

    const-string/jumbo v6, "fnm"

    invoke-static {v5, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "fileName":Ljava/lang/String;
    iget-object v5, p0, Lorg/apache/lucene/index/DocFieldProcessor;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget-object v6, p2, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v5, v6, v1}, Lorg/apache/lucene/index/FieldInfos;->write(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method public freeRAM()Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessor;->consumer:Lorg/apache/lucene/index/DocFieldConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocFieldConsumer;->freeRAM()Z

    move-result v0

    return v0
.end method
