.class Lorg/apache/lucene/index/AllTermDocs;
.super Lorg/apache/lucene/index/AbstractAllTermDocs;
.source "AllTermDocs.java"


# instance fields
.field protected deletedDocs:Lorg/apache/lucene/util/BitVector;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/index/SegmentReader;)V
    .locals 1
    .param p1, "parent"    # Lorg/apache/lucene/index/SegmentReader;

    .prologue
    .line 27
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/AbstractAllTermDocs;-><init>(I)V

    .line 28
    monitor-enter p1

    .line 29
    :try_start_0
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    iput-object v0, p0, Lorg/apache/lucene/index/AllTermDocs;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    .line 30
    monitor-exit p1

    .line 31
    return-void

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public isDeleted(I)Z
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lorg/apache/lucene/index/AllTermDocs;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/AllTermDocs;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/BitVector;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
