.class Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "CoalescedDeletes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/CoalescedDeletes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TermMergeQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 172
    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;-><init>()V

    .line 173
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;->initialize(I)V

    .line 174
    return-void
.end method


# virtual methods
.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 171
    check-cast p1, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;->lessThan(Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;)Z

    move-result v0

    return v0
.end method

.method protected lessThan(Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;)Z
    .locals 5
    .param p1, "a"    # Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;
    .param p2, "b"    # Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 178
    iget-object v3, p1, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->current:Lorg/apache/lucene/index/Term;

    iget-object v4, p2, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->current:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v0

    .line 179
    .local v0, "cmp":I
    if-eqz v0, :cond_2

    .line 180
    if-gez v0, :cond_1

    .line 182
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 180
    goto :goto_0

    .line 182
    :cond_2
    iget v3, p1, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->index:I

    iget v4, p2, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->index:I

    if-lt v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method
