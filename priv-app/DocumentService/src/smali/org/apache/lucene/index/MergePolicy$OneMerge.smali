.class public Lorg/apache/lucene/index/MergePolicy$OneMerge;
.super Ljava/lang/Object;
.source "MergePolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MergePolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OneMerge"
.end annotation


# instance fields
.field aborted:Z

.field error:Ljava/lang/Throwable;

.field public estimatedMergeBytes:J

.field info:Lorg/apache/lucene/index/SegmentInfo;

.field isExternal:Z

.field maxNumSegments:I

.field mergeGen:J

.field paused:Z

.field readerClones:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentReader;",
            ">;"
        }
    .end annotation
.end field

.field readers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentReader;",
            ">;"
        }
    .end annotation
.end field

.field registerDone:Z

.field public final segments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final totalDocCount:I


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "segments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v3, -0x1

    iput v3, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    .line 83
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 84
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "segments must include at least one segment"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 86
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v3, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    .line 87
    const/4 v0, 0x0

    .line 88
    .local v0, "count":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentInfo;

    .line 89
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfo;
    iget v3, v2, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    add-int/2addr v0, v3

    goto :goto_0

    .line 91
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_1
    iput v0, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->totalDocCount:I

    .line 92
    return-void
.end method


# virtual methods
.method declared-synchronized abort()V
    .locals 1

    .prologue
    .line 110
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->aborted:Z

    .line 111
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    monitor-exit p0

    return-void

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized checkAborted(Lorg/apache/lucene/store/Directory;)V
    .locals 4
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;
        }
    .end annotation

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->aborted:Z

    if-eqz v1, :cond_0

    .line 121
    new-instance v1, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "merge is aborted: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 124
    :cond_0
    :try_start_1
    iget-boolean v1, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->paused:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_1

    .line 128
    const-wide/16 v2, 0x3e8

    :try_start_2
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 132
    :try_start_3
    iget-boolean v1, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->aborted:Z

    if-eqz v1, :cond_0

    .line 133
    new-instance v1, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "merge is aborted: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 129
    :catch_0
    move-exception v0

    .line 130
    .local v0, "ie":Ljava/lang/InterruptedException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 136
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :cond_1
    monitor-exit p0

    return-void
.end method

.method declared-synchronized getException()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->error:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPause()Z
    .locals 1

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->paused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized isAborted()Z
    .locals 1

    .prologue
    .line 116
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->aborted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;
    .locals 5
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    .local v0, "b":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 153
    .local v2, "numSegments":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 154
    if-lez v1, :cond_0

    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 155
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/SegmentInfo;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lorg/apache/lucene/index/SegmentInfo;->toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 157
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    if-eqz v3, :cond_2

    .line 158
    const-string/jumbo v3, " into "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    :cond_2
    iget v3, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    .line 160
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, " [maxNumSegments="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    :cond_3
    iget-boolean v3, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->aborted:Z

    if-eqz v3, :cond_4

    .line 162
    const-string/jumbo v3, " [ABORTED]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method declared-synchronized setException(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "error"    # Ljava/lang/Throwable;

    .prologue
    .line 97
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->error:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    monitor-exit p0

    return-void

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setPause(Z)V
    .locals 1
    .param p1, "paused"    # Z

    .prologue
    .line 139
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->paused:Z

    .line 140
    if-nez p1, :cond_0

    .line 142
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    :cond_0
    monitor-exit p0

    return-void

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public totalBytesSize()J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    const-wide/16 v2, 0x0

    .line 173
    .local v2, "total":J
    iget-object v4, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfo;

    .line 174
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfo;
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytes(Z)J

    move-result-wide v4

    add-long/2addr v2, v4

    goto :goto_0

    .line 176
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_0
    return-wide v2
.end method

.method public totalNumDocs()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    const/4 v2, 0x0

    .line 185
    .local v2, "total":I
    iget-object v3, p0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfo;

    .line 186
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfo;
    iget v3, v1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    add-int/2addr v2, v3

    goto :goto_0

    .line 188
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_0
    return v2
.end method
