.class final Lorg/apache/lucene/index/StoredFieldsWriterPerThread;
.super Ljava/lang/Object;
.source "StoredFieldsWriterPerThread.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

.field final docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

.field final localFieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

.field final storedFieldsWriter:Lorg/apache/lucene/index/StoredFieldsWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriter$DocState;Lorg/apache/lucene/index/StoredFieldsWriter;)V
    .locals 4
    .param p1, "docState"    # Lorg/apache/lucene/index/DocumentsWriter$DocState;
    .param p2, "storedFieldsWriter"    # Lorg/apache/lucene/index/StoredFieldsWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p2, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->storedFieldsWriter:Lorg/apache/lucene/index/StoredFieldsWriter;

    .line 34
    iput-object p1, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 35
    new-instance v2, Lorg/apache/lucene/index/FieldsWriter;

    move-object v0, v1

    check-cast v0, Lorg/apache/lucene/store/IndexOutput;

    check-cast v1, Lorg/apache/lucene/store/IndexOutput;

    iget-object v3, p2, Lorg/apache/lucene/index/StoredFieldsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-direct {v2, v0, v1, v3}, Lorg/apache/lucene/index/FieldsWriter;-><init>(Lorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/index/FieldInfos;)V

    iput-object v2, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->localFieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    .line 36
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    invoke-virtual {v0}, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->abort()V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    .line 78
    :cond_0
    return-void
.end method

.method public addField(Lorg/apache/lucene/document/Fieldable;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 4
    .param p1, "field"    # Lorg/apache/lucene/document/Fieldable;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 49
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    if-nez v0, :cond_2

    .line 50
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->storedFieldsWriter:Lorg/apache/lucene/index/StoredFieldsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/StoredFieldsWriter;->getPerDoc()Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    .line 51
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iput v1, v0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->docID:I

    .line 52
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->localFieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    iget-object v1, v1, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->fdt:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldsWriter;->setFieldsStream(Lorg/apache/lucene/store/IndexOutput;)V

    .line 53
    sget-boolean v0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    iget v0, v0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->numStoredFields:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "doc.numStoredFields="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    iget v2, v2, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->numStoredFields:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 54
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    iget-object v0, v0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->fdt:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMOutputStream;->length()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 55
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    iget-object v0, v0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->fdt:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMOutputStream;->getFilePointer()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 58
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->localFieldsWriter:Lorg/apache/lucene/index/FieldsWriter;

    invoke-virtual {v0, p2, p1}, Lorg/apache/lucene/index/FieldsWriter;->writeField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/document/Fieldable;)V

    .line 59
    sget-boolean v0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    const-string/jumbo v1, "StoredFieldsWriterPerThread.processFields.writeField"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DocumentsWriter$DocState;->testPoint(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 60
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    iget v1, v0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->numStoredFields:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->numStoredFields:I

    .line 61
    return-void
.end method

.method public finishDocument()Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    iput-object v1, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    .line 67
    return-object v0

    .line 69
    :catchall_0
    move-exception v0

    iput-object v1, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    throw v0
.end method

.method public startDocument()V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    invoke-virtual {v0}, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->reset()V

    .line 44
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->doc:Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;

    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iput v1, v0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->docID:I

    .line 46
    :cond_0
    return-void
.end method
