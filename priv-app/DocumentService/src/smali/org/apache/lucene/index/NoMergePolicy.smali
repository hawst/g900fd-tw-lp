.class public final Lorg/apache/lucene/index/NoMergePolicy;
.super Lorg/apache/lucene/index/MergePolicy;
.source "NoMergePolicy.java"


# static fields
.field public static final COMPOUND_FILES:Lorg/apache/lucene/index/MergePolicy;

.field public static final NO_COMPOUND_FILES:Lorg/apache/lucene/index/MergePolicy;


# instance fields
.field private final useCompoundFile:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lorg/apache/lucene/index/NoMergePolicy;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/NoMergePolicy;-><init>(Z)V

    sput-object v0, Lorg/apache/lucene/index/NoMergePolicy;->NO_COMPOUND_FILES:Lorg/apache/lucene/index/MergePolicy;

    .line 44
    new-instance v0, Lorg/apache/lucene/index/NoMergePolicy;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/NoMergePolicy;-><init>(Z)V

    sput-object v0, Lorg/apache/lucene/index/NoMergePolicy;->COMPOUND_FILES:Lorg/apache/lucene/index/MergePolicy;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0
    .param p1, "useCompoundFile"    # Z

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/apache/lucene/index/MergePolicy;-><init>()V

    .line 50
    iput-boolean p1, p0, Lorg/apache/lucene/index/NoMergePolicy;->useCompoundFile:Z

    .line 51
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public findForcedDeletesMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 1
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    const/4 v0, 0x0

    return-object v0
.end method

.method public findForcedMerges(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 1
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "maxSegmentCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "I",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lorg/apache/lucene/index/MergePolicy$MergeSpecification;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    .local p3, "segmentsToMerge":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/Boolean;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public findMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 1
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    const/4 v0, 0x0

    return-object v0
.end method

.method public setIndexWriter(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 0
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;

    .prologue
    .line 73
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    const-string/jumbo v0, "NoMergePolicy"

    return-object v0
.end method

.method public useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfo;)Z
    .locals 1
    .param p1, "segments"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "newSegment"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    .line 70
    iget-boolean v0, p0, Lorg/apache/lucene/index/NoMergePolicy;->useCompoundFile:Z

    return v0
.end method
