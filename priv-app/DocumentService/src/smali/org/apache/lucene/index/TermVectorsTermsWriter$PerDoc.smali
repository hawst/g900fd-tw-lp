.class Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;
.super Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
.source "TermVectorsTermsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/TermVectorsTermsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PerDoc"
.end annotation


# instance fields
.field final buffer:Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;

.field fieldNumbers:[I

.field fieldPointers:[J

.field numVectorFields:I

.field perDocTvf:Lorg/apache/lucene/store/RAMOutputStream;

.field final synthetic this$0:Lorg/apache/lucene/index/TermVectorsTermsWriter;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/TermVectorsTermsWriter;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 217
    iput-object p1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->this$0:Lorg/apache/lucene/index/TermVectorsTermsWriter;

    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;-><init>()V

    .line 219
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->this$0:Lorg/apache/lucene/index/TermVectorsTermsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter;->newPerDocBuffer()Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->buffer:Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;

    .line 220
    new-instance v0, Lorg/apache/lucene/store/RAMOutputStream;

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->buffer:Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/RAMOutputStream;-><init>(Lorg/apache/lucene/store/RAMFile;)V

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->perDocTvf:Lorg/apache/lucene/store/RAMOutputStream;

    .line 224
    new-array v0, v2, [I

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->fieldNumbers:[I

    .line 225
    new-array v0, v2, [J

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->fieldPointers:[J

    return-void
.end method


# virtual methods
.method abort()V
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0}, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->reset()V

    .line 236
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->this$0:Lorg/apache/lucene/index/TermVectorsTermsWriter;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/TermVectorsTermsWriter;->free(Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;)V

    .line 237
    return-void
.end method

.method addField(I)V
    .locals 4
    .param p1, "fieldNumber"    # I

    .prologue
    .line 240
    iget v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->numVectorFields:I

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->fieldNumbers:[I

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 241
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->fieldNumbers:[I

    invoke-static {v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([I)[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->fieldNumbers:[I

    .line 243
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->numVectorFields:I

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->fieldPointers:[J

    array-length v1, v1

    if-ne v0, v1, :cond_1

    .line 244
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->fieldPointers:[J

    invoke-static {v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([J)[J

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->fieldPointers:[J

    .line 246
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->fieldNumbers:[I

    iget v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->numVectorFields:I

    aput p1, v0, v1

    .line 247
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->fieldPointers:[J

    iget v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->numVectorFields:I

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->perDocTvf:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v2}, Lorg/apache/lucene/store/RAMOutputStream;->getFilePointer()J

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 248
    iget v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->numVectorFields:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->numVectorFields:I

    .line 249
    return-void
.end method

.method public finish()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 258
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->this$0:Lorg/apache/lucene/index/TermVectorsTermsWriter;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/TermVectorsTermsWriter;->finishDocument(Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;)V

    .line 259
    return-void
.end method

.method reset()V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->perDocTvf:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMOutputStream;->reset()V

    .line 229
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->buffer:Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->recycle()V

    .line 230
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->numVectorFields:I

    .line 231
    return-void
.end method

.method public sizeInBytes()J
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->buffer:Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->getSizeInBytes()J

    move-result-wide v0

    return-wide v0
.end method
