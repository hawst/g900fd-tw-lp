.class Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;
.super Lorg/apache/lucene/index/TermEnum;
.source "DirectoryReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DirectoryReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MultiTermEnum"
.end annotation


# instance fields
.field private docFreq:I

.field final matchingSegments:[Lorg/apache/lucene/index/SegmentMergeInfo;

.field private queue:Lorg/apache/lucene/index/SegmentMergeQueue;

.field private term:Lorg/apache/lucene/index/Term;

.field topReader:Lorg/apache/lucene/index/IndexReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[ILorg/apache/lucene/index/Term;)V
    .locals 6
    .param p1, "topReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "readers"    # [Lorg/apache/lucene/index/IndexReader;
    .param p3, "starts"    # [I
    .param p4, "t"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1077
    invoke-direct {p0}, Lorg/apache/lucene/index/TermEnum;-><init>()V

    .line 1078
    iput-object p1, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->topReader:Lorg/apache/lucene/index/IndexReader;

    .line 1079
    new-instance v4, Lorg/apache/lucene/index/SegmentMergeQueue;

    array-length v5, p2

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/SegmentMergeQueue;-><init>(I)V

    iput-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    .line 1080
    array-length v4, p2

    add-int/lit8 v4, v4, 0x1

    new-array v4, v4, [Lorg/apache/lucene/index/SegmentMergeInfo;

    iput-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->matchingSegments:[Lorg/apache/lucene/index/SegmentMergeInfo;

    .line 1081
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p2

    if-ge v0, v4, :cond_4

    .line 1082
    aget-object v1, p2, v0

    .line 1085
    .local v1, "reader":Lorg/apache/lucene/index/IndexReader;
    if-eqz p4, :cond_1

    .line 1086
    invoke-virtual {v1, p4}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v3

    .line 1090
    .local v3, "termEnum":Lorg/apache/lucene/index/TermEnum;
    :goto_1
    new-instance v2, Lorg/apache/lucene/index/SegmentMergeInfo;

    aget v4, p3, v0

    invoke-direct {v2, v4, v3, v1}, Lorg/apache/lucene/index/SegmentMergeInfo;-><init>(ILorg/apache/lucene/index/TermEnum;Lorg/apache/lucene/index/IndexReader;)V

    .line 1091
    .local v2, "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    iput v0, v2, Lorg/apache/lucene/index/SegmentMergeInfo;->ord:I

    .line 1092
    if-nez p4, :cond_2

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentMergeInfo;->next()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1093
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    invoke-virtual {v4, v2}, Lorg/apache/lucene/index/SegmentMergeQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1081
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1088
    .end local v2    # "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    .end local v3    # "termEnum":Lorg/apache/lucene/index/TermEnum;
    :cond_1
    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->terms()Lorg/apache/lucene/index/TermEnum;

    move-result-object v3

    .restart local v3    # "termEnum":Lorg/apache/lucene/index/TermEnum;
    goto :goto_1

    .line 1092
    .restart local v2    # "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    :cond_2
    invoke-virtual {v3}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v4

    if-nez v4, :cond_0

    .line 1095
    :cond_3
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentMergeInfo;->close()V

    goto :goto_2

    .line 1098
    .end local v1    # "reader":Lorg/apache/lucene/index/IndexReader;
    .end local v2    # "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    .end local v3    # "termEnum":Lorg/apache/lucene/index/TermEnum;
    :cond_4
    if-eqz p4, :cond_5

    iget-object v4, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentMergeQueue;->size()I

    move-result v4

    if-lez v4, :cond_5

    .line 1099
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->next()Z

    .line 1101
    :cond_5
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1150
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentMergeQueue;->close()V

    .line 1151
    return-void
.end method

.method public docFreq()I
    .locals 1

    .prologue
    .line 1145
    iget v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->docFreq:I

    return v0
.end method

.method public next()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1105
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->matchingSegments:[Lorg/apache/lucene/index/SegmentMergeInfo;

    array-length v6, v6

    if-ge v0, v6, :cond_0

    .line 1106
    iget-object v6, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->matchingSegments:[Lorg/apache/lucene/index/SegmentMergeInfo;

    aget-object v3, v6, v0

    .line 1107
    .local v3, "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    if-nez v3, :cond_1

    .line 1114
    .end local v3    # "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    :cond_0
    const/4 v1, 0x0

    .line 1115
    .local v1, "numMatchingSegments":I
    iget-object v6, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->matchingSegments:[Lorg/apache/lucene/index/SegmentMergeInfo;

    aput-object v7, v6, v5

    .line 1117
    iget-object v6, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentMergeQueue;->top()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SegmentMergeInfo;

    .line 1119
    .local v4, "top":Lorg/apache/lucene/index/SegmentMergeInfo;
    if-nez v4, :cond_3

    .line 1120
    iput-object v7, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->term:Lorg/apache/lucene/index/Term;

    .line 1135
    :goto_1
    return v5

    .line 1108
    .end local v1    # "numMatchingSegments":I
    .end local v4    # "top":Lorg/apache/lucene/index/SegmentMergeInfo;
    .restart local v3    # "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    :cond_1
    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentMergeInfo;->next()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1109
    iget-object v6, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    invoke-virtual {v6, v3}, Lorg/apache/lucene/index/SegmentMergeQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1105
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1111
    :cond_2
    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentMergeInfo;->close()V

    goto :goto_2

    .line 1124
    .end local v3    # "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    .restart local v1    # "numMatchingSegments":I
    .restart local v4    # "top":Lorg/apache/lucene/index/SegmentMergeInfo;
    :cond_3
    iget-object v6, v4, Lorg/apache/lucene/index/SegmentMergeInfo;->term:Lorg/apache/lucene/index/Term;

    iput-object v6, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->term:Lorg/apache/lucene/index/Term;

    .line 1125
    iput v5, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->docFreq:I

    move v2, v1

    .line 1127
    .end local v1    # "numMatchingSegments":I
    .local v2, "numMatchingSegments":I
    :goto_3
    if-eqz v4, :cond_4

    iget-object v5, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->term:Lorg/apache/lucene/index/Term;

    iget-object v6, v4, Lorg/apache/lucene/index/SegmentMergeInfo;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v5

    if-nez v5, :cond_4

    .line 1128
    iget-object v5, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->matchingSegments:[Lorg/apache/lucene/index/SegmentMergeInfo;

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "numMatchingSegments":I
    .restart local v1    # "numMatchingSegments":I
    aput-object v4, v5, v2

    .line 1129
    iget-object v5, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentMergeQueue;->pop()Ljava/lang/Object;

    .line 1130
    iget v5, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->docFreq:I

    iget-object v6, v4, Lorg/apache/lucene/index/SegmentMergeInfo;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v6}, Lorg/apache/lucene/index/TermEnum;->docFreq()I

    move-result v6

    add-int/2addr v5, v6

    iput v5, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->docFreq:I

    .line 1131
    iget-object v5, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentMergeQueue;->top()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "top":Lorg/apache/lucene/index/SegmentMergeInfo;
    check-cast v4, Lorg/apache/lucene/index/SegmentMergeInfo;

    .restart local v4    # "top":Lorg/apache/lucene/index/SegmentMergeInfo;
    move v2, v1

    .end local v1    # "numMatchingSegments":I
    .restart local v2    # "numMatchingSegments":I
    goto :goto_3

    .line 1134
    :cond_4
    iget-object v5, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->matchingSegments:[Lorg/apache/lucene/index/SegmentMergeInfo;

    aput-object v7, v5, v2

    .line 1135
    const/4 v5, 0x1

    move v1, v2

    .end local v2    # "numMatchingSegments":I
    .restart local v1    # "numMatchingSegments":I
    goto :goto_1
.end method

.method public term()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 1140
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method
