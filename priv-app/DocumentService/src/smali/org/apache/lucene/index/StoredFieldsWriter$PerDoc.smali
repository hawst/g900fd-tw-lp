.class Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;
.super Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
.source "StoredFieldsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/StoredFieldsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PerDoc"
.end annotation


# instance fields
.field final buffer:Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;

.field fdt:Lorg/apache/lucene/store/RAMOutputStream;

.field numStoredFields:I

.field final synthetic this$0:Lorg/apache/lucene/index/StoredFieldsWriter;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/StoredFieldsWriter;)V
    .locals 2

    .prologue
    .line 127
    iput-object p1, p0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->this$0:Lorg/apache/lucene/index/StoredFieldsWriter;

    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;-><init>()V

    .line 128
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->this$0:Lorg/apache/lucene/index/StoredFieldsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/StoredFieldsWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter;->newPerDocBuffer()Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->buffer:Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;

    .line 129
    new-instance v0, Lorg/apache/lucene/store/RAMOutputStream;

    iget-object v1, p0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->buffer:Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/RAMOutputStream;-><init>(Lorg/apache/lucene/store/RAMFile;)V

    iput-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->fdt:Lorg/apache/lucene/store/RAMOutputStream;

    return-void
.end method


# virtual methods
.method abort()V
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->reset()V

    .line 141
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->this$0:Lorg/apache/lucene/index/StoredFieldsWriter;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/StoredFieldsWriter;->free(Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;)V

    .line 142
    return-void
.end method

.method public finish()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->this$0:Lorg/apache/lucene/index/StoredFieldsWriter;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/StoredFieldsWriter;->finishDocument(Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;)V

    .line 152
    return-void
.end method

.method reset()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->fdt:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMOutputStream;->reset()V

    .line 134
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->buffer:Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->recycle()V

    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->numStoredFields:I

    .line 136
    return-void
.end method

.method public sizeInBytes()J
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lorg/apache/lucene/index/StoredFieldsWriter$PerDoc;->buffer:Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;->getSizeInBytes()J

    move-result-wide v0

    return-wide v0
.end method
