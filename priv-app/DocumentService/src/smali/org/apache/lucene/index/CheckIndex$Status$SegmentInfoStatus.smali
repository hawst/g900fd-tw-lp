.class public Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;
.super Ljava/lang/Object;
.source "CheckIndex.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/CheckIndex$Status;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SegmentInfoStatus"
.end annotation


# instance fields
.field public compound:Z

.field public deletionsFileName:Ljava/lang/String;

.field public diagnostics:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public docCount:I

.field public docStoreCompoundFile:Z

.field public docStoreOffset:I

.field public docStoreSegment:Ljava/lang/String;

.field public fieldNormStatus:Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;

.field public hasDeletions:Z

.field public hasProx:Z

.field public name:Ljava/lang/String;

.field public numDeleted:I

.field numFields:I

.field public numFiles:I

.field public openReaderPassed:Z

.field public sizeMB:D

.field public storedFieldStatus:Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;

.field public termIndexStatus:Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;

.field public termVectorStatus:Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->docStoreOffset:I

    return-void
.end method
