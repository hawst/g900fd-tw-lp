.class public Lorg/apache/lucene/index/ConcurrentMergeScheduler;
.super Lorg/apache/lucene/index/MergeScheduler;
.source "ConcurrentMergeScheduler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static allInstances:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/ConcurrentMergeScheduler;",
            ">;"
        }
    .end annotation
.end field

.field static anyExceptions:Z

.field protected static final compareByMergeDocCount:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private volatile closed:Z

.field protected dir:Lorg/apache/lucene/store/Directory;

.field private maxMergeCount:I

.field private maxThreadCount:I

.field protected mergeThreadCount:I

.field private mergeThreadPriority:I

.field protected mergeThreads:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;",
            ">;"
        }
    .end annotation
.end field

.field private suppressExceptions:Z

.field protected writer:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    const-class v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->$assertionsDisabled:Z

    .line 141
    new-instance v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$1;

    invoke-direct {v0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->compareByMergeDocCount:Ljava/util/Comparator;

    .line 512
    sput-boolean v1, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->anyExceptions:Z

    return-void

    :cond_0
    move v0, v1

    .line 46
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 72
    invoke-direct {p0}, Lorg/apache/lucene/index/MergeScheduler;-><init>()V

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    .line 60
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxThreadCount:I

    .line 64
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxThreadCount:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxMergeCount:I

    .line 73
    sget-object v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->allInstances:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 75
    invoke-direct {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->addMyself()V

    .line 77
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/index/ConcurrentMergeScheduler;)Z
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    .prologue
    .line 46
    iget-boolean v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->suppressExceptions:Z

    return v0
.end method

.method private addMyself()V
    .locals 8

    .prologue
    .line 539
    sget-object v6, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->allInstances:Ljava/util/List;

    monitor-enter v6

    .line 540
    :try_start_0
    sget-object v5, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->allInstances:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    .line 541
    .local v2, "size":I
    const/4 v3, 0x0

    .line 542
    .local v3, "upto":I
    const/4 v0, 0x0

    .local v0, "i":I
    move v4, v3

    .end local v3    # "upto":I
    .local v4, "upto":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 543
    sget-object v5, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->allInstances:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    .line 544
    .local v1, "other":Lorg/apache/lucene/index/ConcurrentMergeScheduler;
    iget-boolean v5, v1, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->closed:Z

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadCount()I

    move-result v5

    if-eqz v5, :cond_2

    .line 547
    :cond_0
    sget-object v5, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->allInstances:Ljava/util/List;

    add-int/lit8 v3, v4, 0x1

    .end local v4    # "upto":I
    .restart local v3    # "upto":I
    invoke-interface {v5, v4, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 542
    :goto_1
    add-int/lit8 v0, v0, 0x1

    move v4, v3

    .end local v3    # "upto":I
    .restart local v4    # "upto":I
    goto :goto_0

    .line 549
    .end local v1    # "other":Lorg/apache/lucene/index/ConcurrentMergeScheduler;
    :cond_1
    sget-object v5, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->allInstances:Ljava/util/List;

    sget-object v7, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->allInstances:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-interface {v5, v4, v7}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 550
    sget-object v5, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->allInstances:Ljava/util/List;

    invoke-interface {v5, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 551
    monitor-exit v6

    .line 552
    return-void

    .line 551
    .end local v0    # "i":I
    .end local v2    # "size":I
    .end local v4    # "upto":I
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .restart local v0    # "i":I
    .restart local v1    # "other":Lorg/apache/lucene/index/ConcurrentMergeScheduler;
    .restart local v2    # "size":I
    .restart local v4    # "upto":I
    :cond_2
    move v3, v4

    .end local v4    # "upto":I
    .restart local v3    # "upto":I
    goto :goto_1
.end method

.method public static anyUnhandledExceptions()Z
    .locals 5

    .prologue
    .line 516
    sget-object v3, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->allInstances:Ljava/util/List;

    if-nez v3, :cond_0

    .line 517
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "setTestMode() was not called; often this is because your test case\'s setUp method fails to call super.setUp in LuceneTestCase"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 519
    :cond_0
    sget-object v4, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->allInstances:Ljava/util/List;

    monitor-enter v4

    .line 520
    :try_start_0
    sget-object v3, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->allInstances:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 523
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 524
    sget-object v3, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->allInstances:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-virtual {v3}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->sync()V

    .line 523
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 525
    :cond_1
    sget-boolean v2, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->anyExceptions:Z

    .line 526
    .local v2, "v":Z
    const/4 v3, 0x0

    sput-boolean v3, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->anyExceptions:Z

    .line 527
    monitor-exit v4

    return v2

    .line 528
    .end local v2    # "v":Z
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public static clearUnhandledExceptions()V
    .locals 2

    .prologue
    .line 532
    sget-object v1, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->allInstances:Ljava/util/List;

    monitor-enter v1

    .line 533
    const/4 v0, 0x0

    :try_start_0
    sput-boolean v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->anyExceptions:Z

    .line 534
    monitor-exit v1

    .line 535
    return-void

    .line 534
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized initMergeThreadPriority()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 240
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 243
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getPriority()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    .line 244
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    if-le v0, v2, :cond_0

    .line 245
    const/16 v0, 0xa

    iput v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247
    :cond_0
    monitor-exit p0

    return-void

    .line 240
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static setTestMode()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 572
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->allInstances:Ljava/util/List;

    .line 573
    return-void
.end method


# virtual methods
.method clearSuppressExceptions()V
    .locals 1

    .prologue
    .line 563
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->suppressExceptions:Z

    .line 564
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->closed:Z

    .line 252
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->sync()V

    .line 253
    return-void
.end method

.method protected doMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 1
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 388
    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->merge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 389
    return-void
.end method

.method public getMaxMergeCount()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxMergeCount:I

    return v0
.end method

.method public getMaxThreadCount()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxThreadCount:I

    return v0
.end method

.method protected declared-synchronized getMergeThread(Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/MergePolicy$OneMerge;)Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    .locals 4
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 393
    monitor-enter p0

    :try_start_0
    new-instance v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;-><init>(Lorg/apache/lucene/index/ConcurrentMergeScheduler;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 394
    .local v0, "thread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    iget v1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->setThreadPriority(I)V

    .line 395
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->setDaemon(Z)V

    .line 396
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Lucene Merge Thread #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadCount:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->setName(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    monitor-exit p0

    return-object v0

    .line 393
    .end local v0    # "thread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getMergeThreadPriority()I
    .locals 1

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->initMergeThreadPriority()V

    .line 124
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected handleMergeException(Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "exc"    # Ljava/lang/Throwable;

    .prologue
    .line 505
    const-wide/16 v2, 0xfa

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 509
    new-instance v1, Lorg/apache/lucene/index/MergePolicy$MergeException;

    iget-object v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->dir:Lorg/apache/lucene/store/Directory;

    invoke-direct {v1, p1, v2}, Lorg/apache/lucene/index/MergePolicy$MergeException;-><init>(Ljava/lang/Throwable;Lorg/apache/lucene/store/Directory;)V

    throw v1

    .line 506
    :catch_0
    move-exception v0

    .line 507
    .local v0, "ie":Ljava/lang/InterruptedException;
    new-instance v1, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v1
.end method

.method public merge(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 10
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 296
    sget-boolean v6, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    invoke-static {p1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 298
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 300
    invoke-direct {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->initMergeThreadPriority()V

    .line 302
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriter;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->dir:Lorg/apache/lucene/store/Directory;

    .line 311
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 312
    const-string/jumbo v6, "now merge"

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 313
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "  index: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 320
    :cond_1
    :goto_0
    monitor-enter p0

    .line 321
    const-wide/16 v4, 0x0

    .line 322
    .local v4, "startStallTime":J
    :goto_1
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadCount()I

    move-result v6

    iget v7, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxMergeCount:I

    add-int/lit8 v7, v7, 0x1

    if-lt v6, v7, :cond_3

    .line 323
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 324
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 325
    const-string/jumbo v6, "    too many merges; stalling..."

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 329
    :catch_0
    move-exception v0

    .line 330
    .local v0, "ie":Ljava/lang/InterruptedException;
    :try_start_2
    new-instance v6, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v6, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v6

    .line 339
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v6

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .line 334
    :cond_3
    :try_start_3
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 335
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-eqz v6, :cond_4

    .line 336
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "  stalled for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v4

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " msec"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 339
    :cond_4
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 345
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriter;->getNextMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-result-object v1

    .line 346
    .local v1, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    if-nez v1, :cond_6

    .line 347
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 348
    const-string/jumbo v6, "  no more merges pending; now return"

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 349
    :cond_5
    return-void

    .line 354
    :cond_6
    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/IndexWriter;->mergeInit(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 356
    const/4 v3, 0x0

    .line 358
    .local v3, "success":Z
    :try_start_4
    monitor-enter p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 359
    :try_start_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "  consider merge "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, v7}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 363
    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->getMergeThread(Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/MergePolicy$OneMerge;)Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    move-result-object v2

    .line 364
    .local v2, "merger":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    iget-object v6, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 365
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 366
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "    launch new thread ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 369
    :cond_7
    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->start()V

    .line 374
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->updateMergeThreads()V

    .line 376
    const/4 v3, 0x1

    .line 377
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 379
    if-nez v3, :cond_1

    .line 380
    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/IndexWriter;->mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    goto/16 :goto_0

    .line 377
    .end local v2    # "merger":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    :catchall_1
    move-exception v6

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 379
    :catchall_2
    move-exception v6

    if-nez v3, :cond_8

    .line 380
    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/IndexWriter;->mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 379
    :cond_8
    throw v6
.end method

.method protected declared-synchronized mergeThreadCount()I
    .locals 4

    .prologue
    .line 284
    monitor-enter p0

    const/4 v0, 0x0

    .line 285
    .local v0, "count":I
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    .line 286
    .local v2, "mt":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getCurrentMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    if-eqz v3, :cond_0

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 290
    .end local v2    # "mt":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    :cond_1
    monitor-exit p0

    return v0

    .line 284
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method protected message(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 236
    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->writer:Lorg/apache/lucene/index/IndexWriter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "CMS: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 237
    return-void
.end method

.method public setMaxMergeCount(I)V
    .locals 3
    .param p1, "count"    # I

    .prologue
    .line 104
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 105
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "count should be at least 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxThreadCount:I

    if-ge p1, v0, :cond_1

    .line 108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "count should be >= maxThreadCount (= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxThreadCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_1
    iput p1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxMergeCount:I

    .line 111
    return-void
.end method

.method public setMaxThreadCount(I)V
    .locals 3
    .param p1, "count"    # I

    .prologue
    .line 83
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "count should be at least 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxMergeCount:I

    if-le p1, v0, :cond_1

    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "count should be <= maxMergeCount (= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxMergeCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_1
    iput p1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxThreadCount:I

    .line 90
    return-void
.end method

.method public declared-synchronized setMergeThreadPriority(I)V
    .locals 2
    .param p1, "pri"    # I

    .prologue
    .line 134
    monitor-enter p0

    const/16 v0, 0xa

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    if-ge p1, v0, :cond_1

    .line 135
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "priority must be in range 1 .. 10 inclusive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 136
    :cond_1
    :try_start_1
    iput p1, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    .line 137
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->updateMergeThreads()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    monitor-exit p0

    return-void
.end method

.method setSuppressExceptions()V
    .locals 1

    .prologue
    .line 558
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->suppressExceptions:Z

    .line 559
    return-void
.end method

.method public sync()V
    .locals 5

    .prologue
    .line 258
    :goto_0
    const/4 v3, 0x0

    .line 259
    .local v3, "toSync":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    monitor-enter p0

    .line 260
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    .line 261
    .local v2, "t":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    invoke-virtual {v2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->isAlive()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 262
    move-object v3, v2

    .line 266
    .end local v2    # "t":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    if-eqz v3, :cond_2

    .line 269
    :try_start_1
    invoke-virtual {v3}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 270
    :catch_0
    move-exception v1

    .line 271
    .local v1, "ie":Ljava/lang/InterruptedException;
    new-instance v4, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v4, v1}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v4

    .line 266
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "ie":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 277
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method protected declared-synchronized updateMergeThreads()V
    .locals 9

    .prologue
    .line 163
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 165
    .local v1, "activeMerges":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;>;"
    const/4 v6, 0x0

    .line 166
    .local v6, "threadIdx":I
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v6, v7, :cond_2

    .line 167
    iget-object v7, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    .line 168
    .local v4, "mergeThread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    invoke-virtual {v4}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->isAlive()Z

    move-result v7

    if-nez v7, :cond_0

    .line 170
    iget-object v7, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreads:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 163
    .end local v1    # "activeMerges":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;>;"
    .end local v4    # "mergeThread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    .end local v6    # "threadIdx":I
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 173
    .restart local v1    # "activeMerges":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;>;"
    .restart local v4    # "mergeThread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    .restart local v6    # "threadIdx":I
    :cond_0
    :try_start_1
    invoke-virtual {v4}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getCurrentMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 174
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    :cond_1
    add-int/lit8 v6, v6, 0x1

    .line 177
    goto :goto_0

    .line 180
    .end local v4    # "mergeThread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    :cond_2
    sget-object v7, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->compareByMergeDocCount:Ljava/util/Comparator;

    invoke-static {v1, v7}, Lorg/apache/lucene/util/CollectionUtil;->mergeSort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 182
    iget v5, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->mergeThreadPriority:I

    .line 183
    .local v5, "pri":I
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 184
    .local v0, "activeMergeCount":I
    const/4 v6, 0x0

    :goto_1
    if-ge v6, v0, :cond_a

    .line 185
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    .line 186
    .restart local v4    # "mergeThread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    invoke-virtual {v4}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getCurrentMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-result-object v3

    .line 187
    .local v3, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    if-nez v3, :cond_4

    .line 184
    :cond_3
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 192
    :cond_4
    iget v7, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->maxThreadCount:I

    sub-int v7, v0, v7

    if-ge v6, v7, :cond_8

    const/4 v2, 0x1

    .line 194
    .local v2, "doPause":Z
    :goto_3
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 195
    invoke-virtual {v3}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->getPause()Z

    move-result v7

    if-eq v2, v7, :cond_5

    .line 196
    if-eqz v2, :cond_9

    .line 197
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "pause thread "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 203
    :cond_5
    :goto_4
    invoke-virtual {v3}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->getPause()Z

    move-result v7

    if-eq v2, v7, :cond_6

    .line 204
    invoke-virtual {v3, v2}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->setPause(Z)V

    .line 207
    :cond_6
    if-nez v2, :cond_3

    .line 208
    invoke-virtual {p0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->verbose()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 209
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "set priority of merge thread "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V

    .line 211
    :cond_7
    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->setThreadPriority(I)V

    .line 212
    const/16 v7, 0xa

    add-int/lit8 v8, v5, 0x1

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v5

    goto :goto_2

    .line 192
    .end local v2    # "doPause":Z
    :cond_8
    const/4 v2, 0x0

    goto :goto_3

    .line 199
    .restart local v2    # "doPause":Z
    :cond_9
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "unpause thread "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->message(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 215
    .end local v2    # "doPause":Z
    .end local v3    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .end local v4    # "mergeThread":Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    :cond_a
    monitor-exit p0

    return-void
.end method

.method protected verbose()Z
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->verbose()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
