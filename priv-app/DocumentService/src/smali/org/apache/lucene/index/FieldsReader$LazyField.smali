.class Lorg/apache/lucene/index/FieldsReader$LazyField;
.super Lorg/apache/lucene/document/AbstractField;
.source "FieldsReader.java"

# interfaces
.implements Lorg/apache/lucene/document/Fieldable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FieldsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LazyField"
.end annotation


# instance fields
.field private cacheResult:Z

.field private isCompressed:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private pointer:J

.field final synthetic this$0:Lorg/apache/lucene/index/FieldsReader;

.field private toRead:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/FieldsReader;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;IJZZZ)V
    .locals 3
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "store"    # Lorg/apache/lucene/document/Field$Store;
    .param p4, "toRead"    # I
    .param p5, "pointer"    # J
    .param p7, "isBinary"    # Z
    .param p8, "isCompressed"    # Z
    .param p9, "cacheResult"    # Z

    .prologue
    .line 479
    iput-object p1, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->this$0:Lorg/apache/lucene/index/FieldsReader;

    .line 480
    sget-object v0, Lorg/apache/lucene/document/Field$Index;->NO:Lorg/apache/lucene/document/Field$Index;

    sget-object v1, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    invoke-direct {p0, p2, p3, v0, v1}, Lorg/apache/lucene/document/AbstractField;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)V

    .line 481
    iput p4, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->toRead:I

    .line 482
    iput-wide p5, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->pointer:J

    .line 483
    iput-boolean p7, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->isBinary:Z

    .line 484
    iput-boolean p9, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->cacheResult:Z

    .line 485
    if-eqz p7, :cond_0

    .line 486
    iput p4, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->binaryLength:I

    .line 487
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->lazy:Z

    .line 488
    iput-boolean p8, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->isCompressed:Z

    .line 489
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/FieldsReader;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;IJZZZ)V
    .locals 1
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "store"    # Lorg/apache/lucene/document/Field$Store;
    .param p4, "index"    # Lorg/apache/lucene/document/Field$Index;
    .param p5, "termVector"    # Lorg/apache/lucene/document/Field$TermVector;
    .param p6, "toRead"    # I
    .param p7, "pointer"    # J
    .param p9, "isBinary"    # Z
    .param p10, "isCompressed"    # Z
    .param p11, "cacheResult"    # Z

    .prologue
    .line 491
    iput-object p1, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->this$0:Lorg/apache/lucene/index/FieldsReader;

    .line 492
    invoke-direct {p0, p2, p3, p4, p5}, Lorg/apache/lucene/document/AbstractField;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)V

    .line 493
    iput p6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->toRead:I

    .line 494
    iput-wide p7, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->pointer:J

    .line 495
    iput-boolean p9, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->isBinary:Z

    .line 496
    iput-boolean p11, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->cacheResult:Z

    .line 497
    if-eqz p9, :cond_0

    .line 498
    iput p6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->binaryLength:I

    .line 499
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->lazy:Z

    .line 500
    iput-boolean p10, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->isCompressed:Z

    .line 501
    return-void
.end method

.method private getFieldStream()Lorg/apache/lucene/store/IndexInput;
    .locals 2

    .prologue
    .line 504
    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->this$0:Lorg/apache/lucene/index/FieldsReader;

    # getter for: Lorg/apache/lucene/index/FieldsReader;->fieldsStreamTL:Lorg/apache/lucene/util/CloseableThreadLocal;
    invoke-static {v1}, Lorg/apache/lucene/index/FieldsReader;->access$000(Lorg/apache/lucene/index/FieldsReader;)Lorg/apache/lucene/util/CloseableThreadLocal;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/util/CloseableThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/IndexInput;

    .line 505
    .local v0, "localFieldsStream":Lorg/apache/lucene/store/IndexInput;
    if-nez v0, :cond_0

    .line 506
    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->this$0:Lorg/apache/lucene/index/FieldsReader;

    # getter for: Lorg/apache/lucene/index/FieldsReader;->cloneableFieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v1}, Lorg/apache/lucene/index/FieldsReader;->access$100(Lorg/apache/lucene/index/FieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "localFieldsStream":Lorg/apache/lucene/store/IndexInput;
    check-cast v0, Lorg/apache/lucene/store/IndexInput;

    .line 507
    .restart local v0    # "localFieldsStream":Lorg/apache/lucene/store/IndexInput;
    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->this$0:Lorg/apache/lucene/index/FieldsReader;

    # getter for: Lorg/apache/lucene/index/FieldsReader;->fieldsStreamTL:Lorg/apache/lucene/util/CloseableThreadLocal;
    invoke-static {v1}, Lorg/apache/lucene/index/FieldsReader;->access$000(Lorg/apache/lucene/index/FieldsReader;)Lorg/apache/lucene/util/CloseableThreadLocal;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/lucene/util/CloseableThreadLocal;->set(Ljava/lang/Object;)V

    .line 509
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getBinaryValue([B)[B
    .locals 8
    .param p1, "result"    # [B

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 573
    iget-object v4, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->this$0:Lorg/apache/lucene/index/FieldsReader;

    # invokes: Lorg/apache/lucene/index/FieldsReader;->ensureOpen()V
    invoke-static {v4}, Lorg/apache/lucene/index/FieldsReader;->access$200(Lorg/apache/lucene/index/FieldsReader;)V

    .line 575
    iget-boolean v4, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->isBinary:Z

    if-eqz v4, :cond_5

    .line 576
    iget-object v4, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->fieldsData:Ljava/lang/Object;

    if-nez v4, :cond_4

    .line 580
    if-eqz p1, :cond_0

    array-length v4, p1

    iget v5, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->toRead:I

    if-ge v4, v5, :cond_2

    .line 581
    :cond_0
    iget v4, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->toRead:I

    new-array v0, v4, [B

    .line 585
    .local v0, "b":[B
    :goto_0
    invoke-direct {p0}, Lorg/apache/lucene/index/FieldsReader$LazyField;->getFieldStream()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    .line 590
    .local v2, "localFieldsStream":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    iget-wide v4, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->pointer:J

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 591
    const/4 v4, 0x0

    iget v5, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->toRead:I

    invoke-virtual {v2, v0, v4, v5}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 592
    iget-boolean v4, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->isCompressed:Z

    if-ne v4, v7, :cond_3

    .line 593
    iget-object v4, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->this$0:Lorg/apache/lucene/index/FieldsReader;

    # invokes: Lorg/apache/lucene/index/FieldsReader;->uncompress([B)[B
    invoke-static {v4, v0}, Lorg/apache/lucene/index/FieldsReader;->access$300(Lorg/apache/lucene/index/FieldsReader;[B)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 601
    .local v3, "value":[B
    :goto_1
    iput v6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->binaryOffset:I

    .line 602
    iget v4, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->toRead:I

    iput v4, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->binaryLength:I

    .line 603
    iget-boolean v4, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->cacheResult:Z

    if-ne v4, v7, :cond_1

    .line 604
    iput-object v3, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->fieldsData:Ljava/lang/Object;

    .line 611
    .end local v0    # "b":[B
    .end local v2    # "localFieldsStream":Lorg/apache/lucene/store/IndexInput;
    .end local v3    # "value":[B
    :cond_1
    :goto_2
    return-object v3

    .line 583
    :cond_2
    move-object v0, p1

    .restart local v0    # "b":[B
    goto :goto_0

    .line 595
    .restart local v2    # "localFieldsStream":Lorg/apache/lucene/store/IndexInput;
    :cond_3
    move-object v3, v0

    .restart local v3    # "value":[B
    goto :goto_1

    .line 597
    .end local v3    # "value":[B
    :catch_0
    move-exception v1

    .line 598
    .local v1, "e":Ljava/io/IOException;
    new-instance v4, Lorg/apache/lucene/index/FieldReaderException;

    invoke-direct {v4, v1}, Lorg/apache/lucene/index/FieldReaderException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 608
    .end local v0    # "b":[B
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "localFieldsStream":Lorg/apache/lucene/store/IndexInput;
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->fieldsData:Ljava/lang/Object;

    check-cast v4, [B

    check-cast v4, [B

    move-object v3, v4

    goto :goto_2

    .line 611
    :cond_5
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public readerValue()Ljava/io/Reader;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->this$0:Lorg/apache/lucene/index/FieldsReader;

    # invokes: Lorg/apache/lucene/index/FieldsReader;->ensureOpen()V
    invoke-static {v0}, Lorg/apache/lucene/index/FieldsReader;->access$200(Lorg/apache/lucene/index/FieldsReader;)V

    .line 517
    const/4 v0, 0x0

    return-object v0
.end method

.method public stringValue()Ljava/lang/String;
    .locals 8

    .prologue
    .line 532
    iget-object v6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->this$0:Lorg/apache/lucene/index/FieldsReader;

    # invokes: Lorg/apache/lucene/index/FieldsReader;->ensureOpen()V
    invoke-static {v6}, Lorg/apache/lucene/index/FieldsReader;->access$200(Lorg/apache/lucene/index/FieldsReader;)V

    .line 533
    iget-boolean v6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->isBinary:Z

    if-eqz v6, :cond_1

    .line 534
    const/4 v5, 0x0

    .line 565
    :cond_0
    :goto_0
    return-object v5

    .line 536
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->fieldsData:Ljava/lang/Object;

    if-nez v6, :cond_4

    .line 537
    invoke-direct {p0}, Lorg/apache/lucene/index/FieldsReader$LazyField;->getFieldStream()Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    .line 540
    .local v4, "localFieldsStream":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    iget-wide v6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->pointer:J

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 541
    iget-boolean v6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->isCompressed:Z

    if-eqz v6, :cond_2

    .line 542
    iget v6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->toRead:I

    new-array v0, v6, [B

    .line 543
    .local v0, "b":[B
    const/4 v6, 0x0

    array-length v7, v0

    invoke-virtual {v4, v0, v6, v7}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 544
    new-instance v5, Ljava/lang/String;

    iget-object v6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->this$0:Lorg/apache/lucene/index/FieldsReader;

    # invokes: Lorg/apache/lucene/index/FieldsReader;->uncompress([B)[B
    invoke-static {v6, v0}, Lorg/apache/lucene/index/FieldsReader;->access$300(Lorg/apache/lucene/index/FieldsReader;[B)[B

    move-result-object v6

    const-string/jumbo v7, "UTF-8"

    invoke-direct {v5, v6, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 560
    .end local v0    # "b":[B
    .local v5, "value":Ljava/lang/String;
    :goto_1
    iget-boolean v6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->cacheResult:Z

    if-eqz v6, :cond_0

    .line 561
    iput-object v5, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->fieldsData:Ljava/lang/Object;

    goto :goto_0

    .line 546
    .end local v5    # "value":Ljava/lang/String;
    :cond_2
    :try_start_1
    iget-object v6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->this$0:Lorg/apache/lucene/index/FieldsReader;

    # getter for: Lorg/apache/lucene/index/FieldsReader;->format:I
    invoke-static {v6}, Lorg/apache/lucene/index/FieldsReader;->access$400(Lorg/apache/lucene/index/FieldsReader;)I

    move-result v6

    const/4 v7, 0x1

    if-lt v6, v7, :cond_3

    .line 547
    iget v6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->toRead:I

    new-array v1, v6, [B

    .line 548
    .local v1, "bytes":[B
    const/4 v6, 0x0

    iget v7, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->toRead:I

    invoke-virtual {v4, v1, v6, v7}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 549
    new-instance v5, Ljava/lang/String;

    const-string/jumbo v6, "UTF-8"

    invoke-direct {v5, v1, v6}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 550
    .restart local v5    # "value":Ljava/lang/String;
    goto :goto_1

    .line 552
    .end local v1    # "bytes":[B
    .end local v5    # "value":Ljava/lang/String;
    :cond_3
    iget v6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->toRead:I

    new-array v2, v6, [C

    .line 553
    .local v2, "chars":[C
    const/4 v6, 0x0

    iget v7, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->toRead:I

    invoke-virtual {v4, v2, v6, v7}, Lorg/apache/lucene/store/IndexInput;->readChars([CII)V

    .line 554
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v2}, Ljava/lang/String;-><init>([C)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .restart local v5    # "value":Ljava/lang/String;
    goto :goto_1

    .line 557
    .end local v2    # "chars":[C
    .end local v5    # "value":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 558
    .local v3, "e":Ljava/io/IOException;
    new-instance v6, Lorg/apache/lucene/index/FieldReaderException;

    invoke-direct {v6, v3}, Lorg/apache/lucene/index/FieldReaderException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 565
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "localFieldsStream":Lorg/apache/lucene/store/IndexInput;
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->fieldsData:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    move-object v5, v6

    goto :goto_0
.end method

.method public tokenStreamValue()Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 524
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsReader$LazyField;->this$0:Lorg/apache/lucene/index/FieldsReader;

    # invokes: Lorg/apache/lucene/index/FieldsReader;->ensureOpen()V
    invoke-static {v0}, Lorg/apache/lucene/index/FieldsReader;->access$200(Lorg/apache/lucene/index/FieldsReader;)V

    .line 525
    const/4 v0, 0x0

    return-object v0
.end method
