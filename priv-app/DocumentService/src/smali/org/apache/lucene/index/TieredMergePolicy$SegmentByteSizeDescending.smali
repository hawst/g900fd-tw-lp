.class Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;
.super Ljava/lang/Object;
.source "TieredMergePolicy.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/TieredMergePolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SegmentByteSizeDescending"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/index/SegmentInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/index/TieredMergePolicy;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/index/TieredMergePolicy;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;->this$0:Lorg/apache/lucene/index/TieredMergePolicy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/index/TieredMergePolicy;Lorg/apache/lucene/index/TieredMergePolicy$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/index/TieredMergePolicy;
    .param p2, "x1"    # Lorg/apache/lucene/index/TieredMergePolicy$1;

    .prologue
    .line 240
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;-><init>(Lorg/apache/lucene/index/TieredMergePolicy;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 240
    check-cast p1, Lorg/apache/lucene/index/SegmentInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/index/SegmentInfo;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;->compare(Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/SegmentInfo;)I
    .locals 7
    .param p1, "o1"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "o2"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    .line 243
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;->this$0:Lorg/apache/lucene/index/TieredMergePolicy;

    # invokes: Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J
    invoke-static {v1, p1}, Lorg/apache/lucene/index/TieredMergePolicy;->access$000(Lorg/apache/lucene/index/TieredMergePolicy;Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v2

    .line 244
    .local v2, "sz1":J
    iget-object v1, p0, Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;->this$0:Lorg/apache/lucene/index/TieredMergePolicy;

    # invokes: Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J
    invoke-static {v1, p2}, Lorg/apache/lucene/index/TieredMergePolicy;->access$000(Lorg/apache/lucene/index/TieredMergePolicy;Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v4

    .line 245
    .local v4, "sz2":J
    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 246
    const/4 v1, -0x1

    .line 250
    :goto_0
    return v1

    .line 247
    :cond_0
    cmp-long v1, v4, v2

    if-lez v1, :cond_1

    .line 248
    const/4 v1, 0x1

    goto :goto_0

    .line 250
    :cond_1
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v6, p2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 252
    .end local v2    # "sz1":J
    .end local v4    # "sz2":J
    :catch_0
    move-exception v0

    .line 253
    .local v0, "ioe":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
