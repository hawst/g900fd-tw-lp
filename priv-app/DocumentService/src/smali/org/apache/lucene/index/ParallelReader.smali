.class public Lorg/apache/lucene/index/ParallelReader;
.super Lorg/apache/lucene/index/IndexReader;
.source "ParallelReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/ParallelReader$ParallelTermPositions;,
        Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;,
        Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;
    }
.end annotation


# instance fields
.field private decrefOnClose:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private fieldToReader:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;"
        }
    .end annotation
.end field

.field private hasDeletions:Z

.field incRefReaders:Z

.field private maxDoc:I

.field private numDocs:I

.field private readers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;"
        }
    .end annotation
.end field

.field private storedFieldReaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/ParallelReader;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2
    .param p1, "closeSubReaders"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexReader;-><init>()V

    .line 48
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    .line 49
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->decrefOnClose:Ljava/util/List;

    .line 50
    iput-boolean v0, p0, Lorg/apache/lucene/index/ParallelReader;->incRefReaders:Z

    .line 51
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;

    .line 52
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->storedFieldReaders:Ljava/util/List;

    .line 70
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lorg/apache/lucene/index/ParallelReader;->incRefReaders:Z

    .line 71
    new-instance v0, Lorg/apache/lucene/index/FieldInfos;

    invoke-direct {v0}, Lorg/apache/lucene/index/FieldInfos;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/ParallelReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/index/ParallelReader;)Ljava/util/SortedMap;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/ParallelReader;

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;

    return-object v0
.end method

.method static synthetic access$100(Lorg/apache/lucene/index/ParallelReader;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/ParallelReader;

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    return-object v0
.end method

.method private doReopen(Z)Lorg/apache/lucene/index/IndexReader;
    .locals 12
    .param p1, "doClone"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 187
    const/4 v8, 0x0

    .line 188
    .local v8, "reopened":Z
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 190
    .local v4, "newReaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexReader;>;"
    const/4 v9, 0x0

    .line 193
    .local v9, "success":Z
    :try_start_0
    iget-object v10, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/IndexReader;

    .line 194
    .local v5, "oldReader":Lorg/apache/lucene/index/IndexReader;
    const/4 v3, 0x0

    .line 195
    .local v3, "newReader":Lorg/apache/lucene/index/IndexReader;
    if-eqz p1, :cond_1

    .line 196
    invoke-virtual {v5}, Lorg/apache/lucene/index/IndexReader;->clone()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "newReader":Lorg/apache/lucene/index/IndexReader;
    check-cast v3, Lorg/apache/lucene/index/IndexReader;

    .line 197
    .restart local v3    # "newReader":Lorg/apache/lucene/index/IndexReader;
    const/4 v8, 0x1

    .line 206
    :goto_1
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 210
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "newReader":Lorg/apache/lucene/index/IndexReader;
    .end local v5    # "oldReader":Lorg/apache/lucene/index/IndexReader;
    :catchall_0
    move-exception v10

    if-nez v9, :cond_5

    if-eqz v8, :cond_5

    .line 211
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v11

    if-ge v0, v11, :cond_5

    .line 212
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/IndexReader;

    .line 213
    .local v7, "r":Lorg/apache/lucene/index/IndexReader;
    iget-object v11, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    if-eq v7, v11, :cond_0

    .line 215
    :try_start_1
    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 211
    :cond_0
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 199
    .end local v0    # "i":I
    .end local v7    # "r":Lorg/apache/lucene/index/IndexReader;
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v3    # "newReader":Lorg/apache/lucene/index/IndexReader;
    .restart local v5    # "oldReader":Lorg/apache/lucene/index/IndexReader;
    :cond_1
    :try_start_2
    invoke-static {v5}, Lorg/apache/lucene/index/IndexReader;->openIfChanged(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/IndexReader;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    .line 200
    if-eqz v3, :cond_2

    .line 201
    const/4 v8, 0x1

    goto :goto_1

    .line 203
    :cond_2
    move-object v3, v5

    goto :goto_1

    .line 208
    .end local v3    # "newReader":Lorg/apache/lucene/index/IndexReader;
    .end local v5    # "oldReader":Lorg/apache/lucene/index/IndexReader;
    :cond_3
    const/4 v9, 0x1

    .line 210
    if-nez v9, :cond_6

    if-eqz v8, :cond_6

    .line 211
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v10

    if-ge v0, v10, :cond_6

    .line 212
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/IndexReader;

    .line 213
    .restart local v7    # "r":Lorg/apache/lucene/index/IndexReader;
    iget-object v10, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    if-eq v7, v10, :cond_4

    .line 215
    :try_start_3
    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 211
    :cond_4
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 210
    .end local v0    # "i":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v7    # "r":Lorg/apache/lucene/index/IndexReader;
    :cond_5
    throw v10

    .line 224
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_6
    if-eqz v8, :cond_a

    .line 225
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 226
    .local v2, "newDecrefOnClose":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Boolean;>;"
    new-instance v6, Lorg/apache/lucene/index/ParallelReader;

    invoke-direct {v6}, Lorg/apache/lucene/index/ParallelReader;-><init>()V

    .line 227
    .local v6, "pr":Lorg/apache/lucene/index/ParallelReader;
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_6
    iget-object v10, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-ge v0, v10, :cond_9

    .line 228
    iget-object v10, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/IndexReader;

    .line 229
    .restart local v5    # "oldReader":Lorg/apache/lucene/index/IndexReader;
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/IndexReader;

    .line 230
    .restart local v3    # "newReader":Lorg/apache/lucene/index/IndexReader;
    if-ne v3, v5, :cond_7

    .line 231
    sget-object v10, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v2, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexReader;->incRef()V

    .line 238
    :goto_7
    iget-object v10, p0, Lorg/apache/lucene/index/ParallelReader;->storedFieldReaders:Ljava/util/List;

    invoke-interface {v10, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    const/4 v10, 0x1

    :goto_8
    invoke-virtual {v6, v3, v10}, Lorg/apache/lucene/index/ParallelReader;->add(Lorg/apache/lucene/index/IndexReader;Z)V

    .line 227
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 236
    :cond_7
    sget-object v10, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {v2, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 238
    :cond_8
    const/4 v10, 0x0

    goto :goto_8

    .line 240
    .end local v3    # "newReader":Lorg/apache/lucene/index/IndexReader;
    .end local v5    # "oldReader":Lorg/apache/lucene/index/IndexReader;
    :cond_9
    iput-object v2, v6, Lorg/apache/lucene/index/ParallelReader;->decrefOnClose:Ljava/util/List;

    .line 241
    iget-boolean v10, p0, Lorg/apache/lucene/index/ParallelReader;->incRefReaders:Z

    iput-boolean v10, v6, Lorg/apache/lucene/index/ParallelReader;->incRefReaders:Z

    .line 245
    .end local v0    # "i":I
    .end local v2    # "newDecrefOnClose":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Boolean;>;"
    .end local v6    # "pr":Lorg/apache/lucene/index/ParallelReader;
    :goto_9
    return-object v6

    :cond_a
    const/4 v6, 0x0

    goto :goto_9

    .line 216
    .end local v1    # "i$":Ljava/util/Iterator;
    .restart local v0    # "i":I
    .restart local v7    # "r":Lorg/apache/lucene/index/IndexReader;
    :catch_0
    move-exception v11

    goto :goto_3

    .restart local v1    # "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v10

    goto :goto_5
.end method


# virtual methods
.method public add(Lorg/apache/lucene/index/IndexReader;)V
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 94
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/ParallelReader;->add(Lorg/apache/lucene/index/IndexReader;Z)V

    .line 95
    return-void
.end method

.method public add(Lorg/apache/lucene/index/IndexReader;Z)V
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "ignoreStoredFields"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 111
    iget-object v3, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 112
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/index/ParallelReader;->maxDoc:I

    .line 113
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/index/ParallelReader;->numDocs:I

    .line 114
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v3

    iput-boolean v3, p0, Lorg/apache/lucene/index/ParallelReader;->hasDeletions:Z

    .line 117
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/index/ParallelReader;->maxDoc:I

    if-eq v3, v4, :cond_1

    .line 118
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "All readers must have same maxDoc: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/index/ParallelReader;->maxDoc:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "!="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 120
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/index/ParallelReader;->numDocs:I

    if-eq v3, v4, :cond_2

    .line 121
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "All readers must have same numDocs: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/index/ParallelReader;->numDocs:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "!="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 124
    :cond_2
    invoke-static {p1}, Lorg/apache/lucene/util/ReaderUtil;->getMergedFieldInfos(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    .line 125
    .local v2, "readerFieldInfos":Lorg/apache/lucene/index/FieldInfos;
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    .line 127
    .local v0, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    iget-object v3, p0, Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;

    iget-object v4, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_3

    .line 128
    iget-object v3, p0, Lorg/apache/lucene/index/ParallelReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/FieldInfos;->add(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/FieldInfo;

    .line 129
    iget-object v3, p0, Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;

    iget-object v4, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v3, v4, p1}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 133
    .end local v0    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    :cond_4
    if-nez p2, :cond_5

    .line 134
    iget-object v3, p0, Lorg/apache/lucene/index/ParallelReader;->storedFieldReaders:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    iget-boolean v3, p0, Lorg/apache/lucene/index/ParallelReader;->incRefReaders:Z

    if-eqz v3, :cond_6

    .line 138
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->incRef()V

    .line 140
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/index/ParallelReader;->decrefOnClose:Ljava/util/List;

    iget-boolean v4, p0, Lorg/apache/lucene/index/ParallelReader;->incRefReaders:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    return-void
.end method

.method public declared-synchronized clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 167
    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    invoke-direct {p0, v1}, Lorg/apache/lucene/index/ParallelReader;->doReopen(Z)Lorg/apache/lucene/index/IndexReader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    .line 168
    :catch_0
    move-exception v0

    .line 169
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 167
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public clone(Z)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p1, "openReadOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 180
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "ParallelReader does not support cloning with changing readOnly flag. Use IndexReader.clone() instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected declared-synchronized doClose()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 489
    monitor-enter p0

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 490
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->decrefOnClose:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 491
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 489
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 493
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 489
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 496
    :cond_1
    monitor-exit p0

    return-void
.end method

.method protected doCommit(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 483
    .local p1, "commitUserData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexReader;

    .line 484
    .local v1, "reader":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/IndexReader;->commit(Ljava/util/Map;)V

    goto :goto_0

    .line 485
    .end local v1    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    return-void
.end method

.method protected doDelete(I)V
    .locals 3
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 280
    iget-object v2, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexReader;

    .line 281
    .local v1, "reader":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/IndexReader;->deleteDocument(I)V

    goto :goto_0

    .line 283
    .end local v1    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/index/ParallelReader;->hasDeletions:Z

    .line 284
    return-void
.end method

.method protected declared-synchronized doOpenIfChanged()Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/ParallelReader;->doReopen(Z)Lorg/apache/lucene/index/IndexReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected doOpenIfChanged(Z)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p1, "openReadOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 160
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "ParallelReader does not support reopening with changing readOnly flag. Use IndexReader.openIfChanged(IndexReader) instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected doSetNorm(ILjava/lang/String;B)V
    .locals 2
    .param p1, "n"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "value"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 395
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-interface {v1, p2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;

    .line 396
    .local v0, "reader":Lorg/apache/lucene/index/IndexReader;
    if-eqz v0, :cond_0

    .line 397
    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/IndexReader;->doSetNorm(ILjava/lang/String;B)V

    .line 398
    :cond_0
    return-void
.end method

.method protected doUndeleteAll()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 289
    iget-object v2, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexReader;

    .line 290
    .local v1, "reader":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->undeleteAll()V

    goto :goto_0

    .line 292
    .end local v1    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/lucene/index/ParallelReader;->hasDeletions:Z

    .line 293
    return-void
.end method

.method public docFreq(Lorg/apache/lucene/index/Term;)I
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 414
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 415
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;

    .line 416
    .local v0, "reader":Lorg/apache/lucene/index/IndexReader;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v1

    goto :goto_0
.end method

.method public document(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;
    .locals 10
    .param p1, "n"    # I
    .param p2, "fieldSelector"    # Lorg/apache/lucene/document/FieldSelector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 298
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 299
    new-instance v7, Lorg/apache/lucene/document/Document;

    invoke-direct {v7}, Lorg/apache/lucene/document/Document;-><init>()V

    .line 300
    .local v7, "result":Lorg/apache/lucene/document/Document;
    iget-object v8, p0, Lorg/apache/lucene/index/ParallelReader;->storedFieldReaders:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/IndexReader;

    .line 302
    .local v6, "reader":Lorg/apache/lucene/index/IndexReader;
    if-nez p2, :cond_3

    const/4 v5, 0x1

    .line 303
    .local v5, "include":Z
    :goto_0
    if-nez v5, :cond_2

    .line 304
    iget-object v8, p0, Lorg/apache/lucene/index/ParallelReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v8}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/FieldInfo;

    .line 305
    .local v1, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    iget-object v8, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {p2, v8}, Lorg/apache/lucene/document/FieldSelector;->accept(Ljava/lang/String;)Lorg/apache/lucene/document/FieldSelectorResult;

    move-result-object v8

    sget-object v9, Lorg/apache/lucene/document/FieldSelectorResult;->NO_LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

    if-eq v8, v9, :cond_1

    .line 306
    const/4 v5, 0x1

    .line 311
    .end local v1    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_2
    if-eqz v5, :cond_0

    .line 312
    invoke-virtual {v6, p1, p2}, Lorg/apache/lucene/index/IndexReader;->document(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/lucene/document/Document;->getFields()Ljava/util/List;

    move-result-object v2

    .line 313
    .local v2, "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Fieldable;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Fieldable;

    .line 314
    .local v0, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-virtual {v7, v0}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/document/Fieldable;)V

    goto :goto_1

    .line 302
    .end local v0    # "field":Lorg/apache/lucene/document/Fieldable;
    .end local v2    # "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Fieldable;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "include":Z
    :cond_3
    const/4 v5, 0x0

    goto :goto_0

    .line 318
    .end local v6    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_4
    return-object v7
.end method

.method public getFieldInfos()Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    return-object v0
.end method

.method getSubReaders()[Lorg/apache/lucene/index/IndexReader;
    .locals 2

    .prologue
    .line 477
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/lucene/index/IndexReader;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/index/IndexReader;

    return-object v0
.end method

.method public getTermFreqVector(ILjava/lang/String;)Lorg/apache/lucene/index/TermFreqVector;
    .locals 2
    .param p1, "n"    # I
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 340
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 341
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-interface {v1, p2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;

    .line 342
    .local v0, "reader":Lorg/apache/lucene/index/IndexReader;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexReader;->getTermFreqVector(ILjava/lang/String;)Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v1

    goto :goto_0
.end method

.method public getTermFreqVector(ILjava/lang/String;Lorg/apache/lucene/index/TermVectorMapper;)V
    .locals 2
    .param p1, "docNumber"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "mapper"    # Lorg/apache/lucene/index/TermVectorMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 348
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 349
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-interface {v1, p2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;

    .line 350
    .local v0, "reader":Lorg/apache/lucene/index/IndexReader;
    if-eqz v0, :cond_0

    .line 351
    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/IndexReader;->getTermFreqVector(ILjava/lang/String;Lorg/apache/lucene/index/TermVectorMapper;)V

    .line 353
    :cond_0
    return-void
.end method

.method public getTermFreqVector(ILorg/apache/lucene/index/TermVectorMapper;)V
    .locals 5
    .param p1, "docNumber"    # I
    .param p2, "mapper"    # Lorg/apache/lucene/index/TermVectorMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 357
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 359
    iget-object v4, p0, Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-interface {v4}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 361
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/IndexReader;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 362
    .local v1, "field":Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/IndexReader;

    .line 363
    .local v3, "reader":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v3, p1, v1, p2}, Lorg/apache/lucene/index/IndexReader;->getTermFreqVector(ILjava/lang/String;Lorg/apache/lucene/index/TermVectorMapper;)V

    goto :goto_0

    .line 366
    .end local v0    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/IndexReader;>;"
    .end local v1    # "field":Ljava/lang/String;
    .end local v3    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    return-void
.end method

.method public getTermFreqVectors(I)[Lorg/apache/lucene/index/TermFreqVector;
    .locals 7
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 324
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 325
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 326
    .local v4, "results":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/index/TermFreqVector;>;"
    iget-object v6, p0, Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-interface {v6}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 328
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/IndexReader;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 329
    .local v1, "field":Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/IndexReader;

    .line 330
    .local v3, "reader":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v3, p1, v1}, Lorg/apache/lucene/index/IndexReader;->getTermFreqVector(ILjava/lang/String;)Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v5

    .line 331
    .local v5, "vector":Lorg/apache/lucene/index/TermFreqVector;
    if-eqz v5, :cond_0

    .line 332
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 334
    .end local v0    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/IndexReader;>;"
    .end local v1    # "field":Ljava/lang/String;
    .end local v3    # "reader":Lorg/apache/lucene/index/IndexReader;
    .end local v5    # "vector":Lorg/apache/lucene/index/TermFreqVector;
    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Lorg/apache/lucene/index/TermFreqVector;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lorg/apache/lucene/index/TermFreqVector;

    return-object v6
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 472
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "ParallelReader does not support this method."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hasDeletions()Z
    .locals 1

    .prologue
    .line 264
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 265
    iget-boolean v0, p0, Lorg/apache/lucene/index/ParallelReader;->hasDeletions:Z

    return v0
.end method

.method public hasNorms(Ljava/lang/String;)Z
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 370
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 371
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-interface {v1, p1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;

    .line 372
    .local v0, "reader":Lorg/apache/lucene/index/IndexReader;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->hasNorms(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isCurrent()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 442
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 443
    iget-object v2, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexReader;

    .line 444
    .local v1, "reader":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->isCurrent()Z

    move-result v2

    if-nez v2, :cond_0

    .line 445
    const/4 v2, 0x0

    .line 450
    .end local v1    # "reader":Lorg/apache/lucene/index/IndexReader;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public isDeleted(I)Z
    .locals 2
    .param p1, "n"    # I

    .prologue
    const/4 v0, 0x0

    .line 272
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 273
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->isDeleted(I)Z

    move-result v0

    .line 274
    :cond_0
    return v0
.end method

.method public isOptimized()Z
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 456
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 457
    iget-object v2, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexReader;

    .line 458
    .local v1, "reader":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->isOptimized()Z

    move-result v2

    if-nez v2, :cond_0

    .line 459
    const/4 v2, 0x0

    .line 464
    .end local v1    # "reader":Lorg/apache/lucene/index/IndexReader;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public maxDoc()I
    .locals 1

    .prologue
    .line 259
    iget v0, p0, Lorg/apache/lucene/index/ParallelReader;->maxDoc:I

    return v0
.end method

.method public norms(Ljava/lang/String;[BI)V
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "result"    # [B
    .param p3, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 385
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 386
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-interface {v1, p1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;

    .line 387
    .local v0, "reader":Lorg/apache/lucene/index/IndexReader;
    if-eqz v0, :cond_0

    .line 388
    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;[BI)V

    .line 389
    :cond_0
    return-void
.end method

.method public norms(Ljava/lang/String;)[B
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 377
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 378
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;

    invoke-interface {v1, p1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;

    .line 379
    .local v0, "reader":Lorg/apache/lucene/index/IndexReader;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v1

    goto :goto_0
.end method

.method public numDocs()I
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lorg/apache/lucene/index/ParallelReader;->numDocs:I

    return v0
.end method

.method public termDocs()Lorg/apache/lucene/index/TermDocs;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 427
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 428
    new-instance v0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;-><init>(Lorg/apache/lucene/index/ParallelReader;)V

    return-object v0
.end method

.method public termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 421
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 422
    new-instance v0, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/index/ParallelReader$ParallelTermDocs;-><init>(Lorg/apache/lucene/index/ParallelReader;Lorg/apache/lucene/index/Term;)V

    return-object v0
.end method

.method public termPositions()Lorg/apache/lucene/index/TermPositions;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 433
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 434
    new-instance v0, Lorg/apache/lucene/index/ParallelReader$ParallelTermPositions;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/ParallelReader$ParallelTermPositions;-><init>(Lorg/apache/lucene/index/ParallelReader;)V

    return-object v0
.end method

.method public terms()Lorg/apache/lucene/index/TermEnum;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 402
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 403
    new-instance v0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;-><init>(Lorg/apache/lucene/index/ParallelReader;)V

    return-object v0
.end method

.method public terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 408
    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelReader;->ensureOpen()V

    .line 409
    new-instance v0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;-><init>(Lorg/apache/lucene/index/ParallelReader;Lorg/apache/lucene/index/Term;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "ParallelReader("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 78
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lorg/apache/lucene/index/ParallelReader;->readers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 79
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/IndexReader;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 80
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 82
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 83
    const-string/jumbo v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 85
    :cond_1
    const/16 v2, 0x29

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 86
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
