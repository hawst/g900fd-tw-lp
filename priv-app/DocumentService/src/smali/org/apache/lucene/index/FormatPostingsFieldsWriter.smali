.class final Lorg/apache/lucene/index/FormatPostingsFieldsWriter;
.super Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;
.source "FormatPostingsFieldsWriter.java"


# instance fields
.field final dir:Lorg/apache/lucene/store/Directory;

.field final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field final segment:Ljava/lang/String;

.field final skipListWriter:Lorg/apache/lucene/index/DefaultSkipListWriter;

.field termsOut:Lorg/apache/lucene/index/TermInfosWriter;

.field termsWriter:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

.field final totalNumDocs:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 10
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 35
    invoke-direct {p0}, Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;-><init>()V

    .line 36
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    iput-object v0, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->dir:Lorg/apache/lucene/store/Directory;

    .line 37
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentName:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->segment:Ljava/lang/String;

    .line 38
    iget v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->numDocs:I

    iput v0, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->totalNumDocs:I

    .line 39
    iput-object p2, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 40
    const/4 v6, 0x0

    .line 42
    .local v6, "success":Z
    :try_start_0
    new-instance v0, Lorg/apache/lucene/index/TermInfosWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->segment:Ljava/lang/String;

    iget v3, p1, Lorg/apache/lucene/index/SegmentWriteState;->termIndexInterval:I

    invoke-direct {v0, v1, v2, p2, v3}, Lorg/apache/lucene/index/TermInfosWriter;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;I)V

    iput-object v0, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->termsOut:Lorg/apache/lucene/index/TermInfosWriter;

    .line 48
    new-instance v0, Lorg/apache/lucene/index/DefaultSkipListWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->termsOut:Lorg/apache/lucene/index/TermInfosWriter;

    iget v1, v1, Lorg/apache/lucene/index/TermInfosWriter;->skipInterval:I

    iget-object v2, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->termsOut:Lorg/apache/lucene/index/TermInfosWriter;

    iget v2, v2, Lorg/apache/lucene/index/TermInfosWriter;->maxSkipLevels:I

    iget v3, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->totalNumDocs:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/DefaultSkipListWriter;-><init>(IIILorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/IndexOutput;)V

    iput-object v0, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->skipListWriter:Lorg/apache/lucene/index/DefaultSkipListWriter;

    .line 51
    new-instance v0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    invoke-direct {v0, p1, p0}, Lorg/apache/lucene/index/FormatPostingsTermsWriter;-><init>(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/index/FormatPostingsFieldsWriter;)V

    iput-object v0, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->termsWriter:Lorg/apache/lucene/index/FormatPostingsTermsWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    const/4 v6, 0x1

    .line 54
    if-nez v6, :cond_0

    .line 55
    new-array v0, v9, [Ljava/io/Closeable;

    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->termsOut:Lorg/apache/lucene/index/TermInfosWriter;

    aput-object v1, v0, v7

    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->termsWriter:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    aput-object v1, v0, v8

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 58
    :cond_0
    return-void

    .line 54
    :catchall_0
    move-exception v0

    if-nez v6, :cond_1

    .line 55
    new-array v1, v9, [Ljava/io/Closeable;

    iget-object v2, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->termsOut:Lorg/apache/lucene/index/TermInfosWriter;

    aput-object v2, v1, v7

    iget-object v2, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->termsWriter:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    aput-object v2, v1, v8

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 54
    :cond_1
    throw v0
.end method


# virtual methods
.method addField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/FormatPostingsTermsConsumer;
    .locals 1
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->termsWriter:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->setField(Lorg/apache/lucene/index/FieldInfo;)V

    .line 64
    iget-object v0, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->termsWriter:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    return-object v0
.end method

.method finish()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->termsOut:Lorg/apache/lucene/index/TermInfosWriter;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->termsWriter:Lorg/apache/lucene/index/FormatPostingsTermsWriter;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 71
    return-void
.end method
