.class public final Lorg/apache/lucene/index/NoMergeScheduler;
.super Lorg/apache/lucene/index/MergeScheduler;
.source "NoMergeScheduler.java"


# static fields
.field public static final INSTANCE:Lorg/apache/lucene/index/MergeScheduler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lorg/apache/lucene/index/NoMergeScheduler;

    invoke-direct {v0}, Lorg/apache/lucene/index/NoMergeScheduler;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/NoMergeScheduler;->INSTANCE:Lorg/apache/lucene/index/MergeScheduler;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/lucene/index/MergeScheduler;-><init>()V

    .line 39
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public merge(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 0
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    return-void
.end method
