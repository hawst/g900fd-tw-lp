.class Lorg/apache/lucene/index/TermInfosReaderIndex;
.super Ljava/lang/Object;
.source "TermInfosReaderIndex.java"


# static fields
.field private static final MAX_PAGE_BITS:I = 0x12


# instance fields
.field private comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private final dataInput:Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

.field private fields:[Lorg/apache/lucene/index/Term;

.field private final indexSize:I

.field private final indexToDataOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;

.field private final skipInterval:I

.field private totalIndexInterval:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/SegmentTermEnum;IJI)V
    .locals 19
    .param p1, "indexEnum"    # Lorg/apache/lucene/index/SegmentTermEnum;
    .param p2, "indexDivisor"    # I
    .param p3, "tiiFileLength"    # J
    .param p5, "totalIndexInterval"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUTF16Comparator()Ljava/util/Comparator;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/lucene/index/TermInfosReaderIndex;->comparator:Ljava/util/Comparator;

    .line 64
    move/from16 v0, p5

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/index/TermInfosReaderIndex;->totalIndexInterval:I

    .line 65
    move-object/from16 v0, p1

    iget-wide v14, v0, Lorg/apache/lucene/index/SegmentTermEnum;->size:J

    long-to-int v14, v14

    add-int/lit8 v14, v14, -0x1

    div-int v14, v14, p2

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/index/TermInfosReaderIndex;->indexSize:I

    .line 66
    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/index/SegmentTermEnum;->skipInterval:I

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/index/TermInfosReaderIndex;->skipInterval:I

    .line 68
    move-wide/from16 v0, p3

    long-to-double v14, v0

    const-wide/high16 v16, 0x3ff8000000000000L    # 1.5

    mul-double v14, v14, v16

    double-to-long v14, v14

    move/from16 v0, p2

    int-to-long v0, v0

    move-wide/from16 v16, v0

    div-long v10, v14, v16

    .line 69
    .local v10, "initialSize":J
    new-instance v4, Lorg/apache/lucene/util/PagedBytes;

    invoke-static {v10, v11}, Lorg/apache/lucene/index/TermInfosReaderIndex;->estimatePageBits(J)I

    move-result v14

    invoke-direct {v4, v14}, Lorg/apache/lucene/util/PagedBytes;-><init>(I)V

    .line 70
    .local v4, "dataPagedBytes":Lorg/apache/lucene/util/PagedBytes;
    invoke-virtual {v4}, Lorg/apache/lucene/util/PagedBytes;->getDataOutput()Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;

    move-result-object v3

    .line 72
    .local v3, "dataOutput":Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;
    new-instance v8, Lorg/apache/lucene/util/packed/GrowableWriter;

    const/4 v14, 0x4

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/index/TermInfosReaderIndex;->indexSize:I

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-direct {v8, v14, v15, v0}, Lorg/apache/lucene/util/packed/GrowableWriter;-><init>(IIZ)V

    .line 73
    .local v8, "indexToTerms":Lorg/apache/lucene/util/packed/GrowableWriter;
    const/4 v2, 0x0

    .line 74
    .local v2, "currentField":Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 75
    .local v6, "fieldStrs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, -0x1

    .line 76
    .local v5, "fieldCounter":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentTermEnum;->next()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 77
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v12

    .line 78
    .local v12, "term":Lorg/apache/lucene/index/Term;
    iget-object v14, v12, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    if-eq v2, v14, :cond_0

    .line 79
    iget-object v2, v12, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    .line 80
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    add-int/lit8 v5, v5, 0x1

    .line 83
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo()Lorg/apache/lucene/index/TermInfo;

    move-result-object v13

    .line 84
    .local v13, "termInfo":Lorg/apache/lucene/index/TermInfo;
    invoke-virtual {v3}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->getPosition()J

    move-result-wide v14

    invoke-virtual {v8, v7, v14, v15}, Lorg/apache/lucene/util/packed/GrowableWriter;->set(IJ)V

    .line 85
    invoke-virtual {v3, v5}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->writeVInt(I)V

    .line 86
    invoke-virtual {v12}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v14}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->writeString(Ljava/lang/String;)V

    .line 87
    iget v14, v13, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    invoke-virtual {v3, v14}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->writeVInt(I)V

    .line 88
    iget v14, v13, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/index/TermInfosReaderIndex;->skipInterval:I

    if-lt v14, v15, :cond_1

    .line 89
    iget v14, v13, Lorg/apache/lucene/index/TermInfo;->skipOffset:I

    invoke-virtual {v3, v14}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->writeVInt(I)V

    .line 91
    :cond_1
    iget-wide v14, v13, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    invoke-virtual {v3, v14, v15}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->writeVLong(J)V

    .line 92
    iget-wide v14, v13, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    invoke-virtual {v3, v14, v15}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->writeVLong(J)V

    .line 93
    move-object/from16 v0, p1

    iget-wide v14, v0, Lorg/apache/lucene/index/SegmentTermEnum;->indexPointer:J

    invoke-virtual {v3, v14, v15}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->writeVLong(J)V

    .line 94
    const/4 v9, 0x1

    .local v9, "j":I
    :goto_1
    move/from16 v0, p2

    if-ge v9, v0, :cond_2

    .line 95
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentTermEnum;->next()Z

    move-result v14

    if-nez v14, :cond_3

    .line 76
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 94
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 101
    .end local v9    # "j":I
    .end local v12    # "term":Lorg/apache/lucene/index/Term;
    .end local v13    # "termInfo":Lorg/apache/lucene/index/TermInfo;
    :cond_4
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v14

    new-array v14, v14, [Lorg/apache/lucene/index/Term;

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/lucene/index/TermInfosReaderIndex;->fields:[Lorg/apache/lucene/index/Term;

    .line 102
    const/4 v7, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/TermInfosReaderIndex;->fields:[Lorg/apache/lucene/index/Term;

    array-length v14, v14

    if-ge v7, v14, :cond_5

    .line 103
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/TermInfosReaderIndex;->fields:[Lorg/apache/lucene/index/Term;

    new-instance v16, Lorg/apache/lucene/index/Term;

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v14}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    aput-object v16, v15, v7

    .line 102
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 106
    :cond_5
    const/4 v14, 0x1

    invoke-virtual {v4, v14}, Lorg/apache/lucene/util/PagedBytes;->freeze(Z)Lorg/apache/lucene/util/PagedBytes$Reader;

    .line 107
    invoke-virtual {v4}, Lorg/apache/lucene/util/PagedBytes;->getDataInput()Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/lucene/index/TermInfosReaderIndex;->dataInput:Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    .line 108
    invoke-virtual {v8}, Lorg/apache/lucene/util/packed/GrowableWriter;->getMutable()Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/lucene/index/TermInfosReaderIndex;->indexToDataOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 109
    return-void
.end method

.method private compareField(Lorg/apache/lucene/index/Term;ILorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;)I
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "termIndex"    # I
    .param p3, "input"    # Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->indexToDataOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v0, p2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v0

    invoke-virtual {p3, v0, v1}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->setPosition(J)V

    .line 249
    iget-object v0, p1, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->fields:[Lorg/apache/lucene/index/Term;

    invoke-virtual {p3}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVInt()I

    move-result v2

    aget-object v1, v1, v2

    iget-object v1, v1, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private compareTo(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/util/BytesRef;ILorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;Lorg/apache/lucene/util/BytesRef;)I
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "termBytesRef"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "termIndex"    # I
    .param p4, "input"    # Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;
    .param p5, "reuse"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 225
    invoke-direct {p0, p1, p3, p4}, Lorg/apache/lucene/index/TermInfosReaderIndex;->compareField(Lorg/apache/lucene/index/Term;ILorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;)I

    move-result v0

    .line 226
    .local v0, "c":I
    if-nez v0, :cond_0

    .line 227
    invoke-virtual {p4}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVInt()I

    move-result v1

    iput v1, p5, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 228
    iget v1, p5, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p5, v1}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 229
    iget-object v1, p5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    const/4 v2, 0x0

    iget v3, p5, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p4, v1, v2, v3}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readBytes([BII)V

    .line 230
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->comparator:Ljava/util/Comparator;

    invoke-interface {v1, p2, p5}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 232
    .end local v0    # "c":I
    :cond_0
    return v0
.end method

.method private static estimatePageBits(J)I
    .locals 2
    .param p0, "estSize"    # J

    .prologue
    .line 112
    invoke-static {p0, p1}, Lorg/apache/lucene/util/BitUtil;->nlz(J)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x40

    const/16 v1, 0x12

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method compareTo(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/util/BytesRef;I)I
    .locals 6
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "termBytesRef"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "termIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->dataInput:Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    new-instance v5, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v5}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/TermInfosReaderIndex;->compareTo(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/util/BytesRef;ILorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    return v0
.end method

.method getIndexOffset(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/util/BytesRef;)I
    .locals 9
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "termBytesRef"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    const/4 v8, 0x0

    .line 151
    .local v8, "lo":I
    iget v0, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->indexSize:I

    add-int/lit8 v7, v0, -0x1

    .line 152
    .local v7, "hi":I
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->dataInput:Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    .line 153
    .local v4, "input":Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;
    new-instance v5, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v5}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 154
    .local v5, "scratch":Lorg/apache/lucene/util/BytesRef;
    :goto_0
    if-lt v7, v8, :cond_1

    .line 155
    add-int v0, v8, v7

    ushr-int/lit8 v3, v0, 0x1

    .local v3, "mid":I
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 156
    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/TermInfosReaderIndex;->compareTo(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/util/BytesRef;ILorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;Lorg/apache/lucene/util/BytesRef;)I

    move-result v6

    .line 157
    .local v6, "delta":I
    if-gez v6, :cond_0

    .line 158
    add-int/lit8 v7, v3, -0x1

    goto :goto_0

    .line 159
    :cond_0
    if-lez v6, :cond_2

    .line 160
    add-int/lit8 v8, v3, 0x1

    goto :goto_0

    .end local v3    # "mid":I
    .end local v6    # "delta":I
    :cond_1
    move v3, v7

    .line 164
    :cond_2
    return v3
.end method

.method getTerm(I)Lorg/apache/lucene/index/Term;
    .locals 6
    .param p1, "termIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->dataInput:Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    invoke-virtual {v3}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    .line 177
    .local v2, "input":Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;
    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->indexToDataOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v3, p1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->setPosition(J)V

    .line 180
    invoke-virtual {v2}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVInt()I

    move-result v1

    .line 181
    .local v1, "fieldId":I
    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->fields:[Lorg/apache/lucene/index/Term;

    aget-object v0, v3, v1

    .line 182
    .local v0, "field":Lorg/apache/lucene/index/Term;
    invoke-virtual {v2}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/Term;->createTerm(Ljava/lang/String;)Lorg/apache/lucene/index/Term;

    move-result-object v3

    return-object v3
.end method

.method length()I
    .locals 1

    .prologue
    .line 191
    iget v0, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->indexSize:I

    return v0
.end method

.method seekEnum(Lorg/apache/lucene/index/SegmentTermEnum;I)V
    .locals 12
    .param p1, "enumerator"    # Lorg/apache/lucene/index/SegmentTermEnum;
    .param p2, "indexOffset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->dataInput:Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    invoke-virtual {v1}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->clone()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    .line 118
    .local v9, "input":Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->indexToDataOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v1, p2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v4

    invoke-virtual {v9, v4, v5}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->setPosition(J)V

    .line 121
    invoke-virtual {v9}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVInt()I

    move-result v8

    .line 122
    .local v8, "fieldId":I
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->fields:[Lorg/apache/lucene/index/Term;

    aget-object v0, v1, v8

    .line 123
    .local v0, "field":Lorg/apache/lucene/index/Term;
    invoke-virtual {v9}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Term;->createTerm(Ljava/lang/String;)Lorg/apache/lucene/index/Term;

    move-result-object v6

    .line 126
    .local v6, "term":Lorg/apache/lucene/index/Term;
    new-instance v7, Lorg/apache/lucene/index/TermInfo;

    invoke-direct {v7}, Lorg/apache/lucene/index/TermInfo;-><init>()V

    .line 127
    .local v7, "termInfo":Lorg/apache/lucene/index/TermInfo;
    invoke-virtual {v9}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVInt()I

    move-result v1

    iput v1, v7, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    .line 128
    iget v1, v7, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    iget v4, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->skipInterval:I

    if-lt v1, v4, :cond_0

    .line 129
    invoke-virtual {v9}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVInt()I

    move-result v1

    iput v1, v7, Lorg/apache/lucene/index/TermInfo;->skipOffset:I

    .line 133
    :goto_0
    invoke-virtual {v9}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVLong()J

    move-result-wide v4

    iput-wide v4, v7, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    .line 134
    invoke-virtual {v9}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVLong()J

    move-result-wide v4

    iput-wide v4, v7, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    .line 136
    invoke-virtual {v9}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVLong()J

    move-result-wide v2

    .line 139
    .local v2, "pointer":J
    int-to-long v4, p2

    iget v1, p0, Lorg/apache/lucene/index/TermInfosReaderIndex;->totalIndexInterval:I

    int-to-long v10, v1

    mul-long/2addr v4, v10

    const-wide/16 v10, 0x1

    sub-long/2addr v4, v10

    move-object v1, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/lucene/index/SegmentTermEnum;->seek(JJLorg/apache/lucene/index/Term;Lorg/apache/lucene/index/TermInfo;)V

    .line 140
    return-void

    .line 131
    .end local v2    # "pointer":J
    :cond_0
    const/4 v1, 0x0

    iput v1, v7, Lorg/apache/lucene/index/TermInfo;->skipOffset:I

    goto :goto_0
.end method
