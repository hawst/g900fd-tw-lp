.class public Lorg/apache/lucene/index/FilterIndexReader$FilterTermDocs;
.super Ljava/lang/Object;
.source "FilterIndexReader.java"

# interfaces
.implements Lorg/apache/lucene/index/TermDocs;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FilterIndexReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FilterTermDocs"
.end annotation


# instance fields
.field protected in:Lorg/apache/lucene/index/TermDocs;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/TermDocs;)V
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/index/TermDocs;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermDocs;->in:Lorg/apache/lucene/index/TermDocs;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermDocs;->in:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->close()V

    return-void
.end method

.method public doc()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermDocs;->in:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v0

    return v0
.end method

.method public freq()I
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermDocs;->in:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->freq()I

    move-result v0

    return v0
.end method

.method public next()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermDocs;->in:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v0

    return v0
.end method

.method public read([I[I)I
    .locals 1
    .param p1, "docs"    # [I
    .param p2, "freqs"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermDocs;->in:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0, p1, p2}, Lorg/apache/lucene/index/TermDocs;->read([I[I)I

    move-result v0

    return v0
.end method

.method public seek(Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermDocs;->in:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0, p1}, Lorg/apache/lucene/index/TermDocs;->seek(Lorg/apache/lucene/index/Term;)V

    return-void
.end method

.method public seek(Lorg/apache/lucene/index/TermEnum;)V
    .locals 1
    .param p1, "termEnum"    # Lorg/apache/lucene/index/TermEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermDocs;->in:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0, p1}, Lorg/apache/lucene/index/TermDocs;->seek(Lorg/apache/lucene/index/TermEnum;)V

    return-void
.end method

.method public skipTo(I)Z
    .locals 1
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/index/FilterIndexReader$FilterTermDocs;->in:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0, p1}, Lorg/apache/lucene/index/TermDocs;->skipTo(I)Z

    move-result v0

    return v0
.end method
