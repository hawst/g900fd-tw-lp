.class Lorg/apache/lucene/index/DocumentsWriter$DocState;
.super Ljava/lang/Object;
.source "DocumentsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DocState"
.end annotation


# instance fields
.field analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field doc:Lorg/apache/lucene/document/Document;

.field docID:I

.field docWriter:Lorg/apache/lucene/index/DocumentsWriter;

.field infoStream:Ljava/io/PrintStream;

.field maxFieldLength:I

.field maxTermPrefix:Ljava/lang/String;

.field similarity:Lorg/apache/lucene/search/Similarity;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 157
    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->doc:Lorg/apache/lucene/document/Document;

    .line 158
    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 159
    return-void
.end method

.method public testPoint(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
