.class public abstract Lorg/apache/lucene/index/LogMergePolicy;
.super Lorg/apache/lucene/index/MergePolicy;
.source "LogMergePolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT_MAX_MERGE_DOCS:I = 0x7fffffff

.field public static final DEFAULT_MERGE_FACTOR:I = 0xa

.field public static final DEFAULT_NO_CFS_RATIO:D = 0.1

.field public static final LEVEL_LOG_SPAN:D = 0.75


# instance fields
.field protected calibrateSizeByDeletes:Z

.field protected maxMergeDocs:I

.field protected maxMergeSize:J

.field protected maxMergeSizeForForcedMerge:J

.field protected mergeFactor:I

.field protected minMergeSize:J

.field protected noCFSRatio:D

.field protected useCompoundFile:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lorg/apache/lucene/index/LogMergePolicy;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/LogMergePolicy;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 81
    invoke-direct {p0}, Lorg/apache/lucene/index/MergePolicy;-><init>()V

    .line 65
    const/16 v0, 0xa

    iput v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    .line 71
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSizeForForcedMerge:J

    .line 72
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    .line 74
    const-wide v0, 0x3fb999999999999aL    # 0.1

    iput-wide v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->noCFSRatio:D

    .line 76
    iput-boolean v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->calibrateSizeByDeletes:Z

    .line 78
    iput-boolean v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->useCompoundFile:Z

    .line 82
    return-void
.end method

.method private findForcedMergesMaxNumSegments(Lorg/apache/lucene/index/SegmentInfos;II)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 16
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "maxNumSegments"    # I
    .param p3, "last"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    new-instance v9, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    invoke-direct {v9}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 289
    .local v9, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentInfos;->asList()Ljava/util/List;

    move-result-object v8

    .line 293
    .local v8, "segments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    :goto_0
    sub-int v12, p3, p2

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    if-lt v12, v13, :cond_0

    .line 294
    new-instance v12, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    sub-int v13, p3, v13

    move/from16 v0, p3

    invoke-interface {v8, v13, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v13

    invoke-direct {v12, v13}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v9, v12}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 295
    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    sub-int p3, p3, v12

    goto :goto_0

    .line 300
    :cond_0
    iget-object v12, v9, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-nez v12, :cond_2

    .line 301
    const/4 v12, 0x1

    move/from16 v0, p2

    if-ne v0, v12, :cond_4

    .line 305
    const/4 v12, 0x1

    move/from16 v0, p3

    if-gt v0, v12, :cond_1

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lorg/apache/lucene/index/LogMergePolicy;->isMerged(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 306
    :cond_1
    new-instance v12, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    const/4 v13, 0x0

    move/from16 v0, p3

    invoke-interface {v8, v13, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v13

    invoke-direct {v12, v13}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v9, v12}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 338
    :cond_2
    :goto_1
    iget-object v12, v9, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-nez v12, :cond_3

    const/4 v9, 0x0

    .end local v9    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_3
    return-object v9

    .line 308
    .restart local v9    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_4
    move/from16 v0, p3

    move/from16 v1, p2

    if-le v0, v1, :cond_2

    .line 319
    sub-int v12, p3, p2

    add-int/lit8 v5, v12, 0x1

    .line 322
    .local v5, "finalMergeSize":I
    const-wide/16 v2, 0x0

    .line 323
    .local v2, "bestSize":J
    const/4 v4, 0x0

    .line 325
    .local v4, "bestStart":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    sub-int v12, p3, v5

    add-int/lit8 v12, v12, 0x1

    if-ge v6, v12, :cond_8

    .line 326
    const-wide/16 v10, 0x0

    .line 327
    .local v10, "sumSize":J
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_3
    if-ge v7, v5, :cond_5

    .line 328
    add-int v12, v7, v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v12

    add-long/2addr v10, v12

    .line 327
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 329
    :cond_5
    if-eqz v6, :cond_6

    const-wide/16 v12, 0x2

    add-int/lit8 v14, v6, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v14

    mul-long/2addr v12, v14

    cmp-long v12, v10, v12

    if-gez v12, :cond_7

    cmp-long v12, v10, v2

    if-gez v12, :cond_7

    .line 330
    :cond_6
    move v4, v6

    .line 331
    move-wide v2, v10

    .line 325
    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 335
    .end local v7    # "j":I
    .end local v10    # "sumSize":J
    :cond_8
    new-instance v12, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    add-int v13, v4, v5

    invoke-interface {v8, v4, v13}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v13

    invoke-direct {v12, v13}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v9, v12}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    goto :goto_1
.end method

.method private findForcedMergesSizeLimit(Lorg/apache/lucene/index/SegmentInfos;II)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 8
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "maxNumSegments"    # I
    .param p3, "last"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 247
    new-instance v2, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    invoke-direct {v2}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 248
    .local v2, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->asList()Ljava/util/List;

    move-result-object v1

    .line 250
    .local v1, "segments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    add-int/lit8 v3, p3, -0x1

    .line 251
    .local v3, "start":I
    :goto_0
    if-ltz v3, :cond_6

    .line 252
    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v0

    .line 253
    .local v0, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v4

    iget-wide v6, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSizeForForcedMerge:J

    cmp-long v4, v4, v6

    if-gtz v4, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/LogMergePolicy;->sizeDocs(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v4

    iget v6, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    .line 254
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 255
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "findForcedMergesSizeLimit: skip segment="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ": size is > maxMergeSize ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSizeForForcedMerge:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ") or sizeDocs is > maxMergeDocs ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 259
    :cond_1
    sub-int v4, p3, v3

    add-int/lit8 v4, v4, -0x1

    const/4 v5, 0x1

    if-gt v4, v5, :cond_2

    add-int/lit8 v4, p3, -0x1

    if-eq v3, v4, :cond_3

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {p1, v4}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/LogMergePolicy;->isMerged(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 262
    :cond_2
    new-instance v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    add-int/lit8 v5, v3, 0x1

    invoke-interface {v1, v5, p3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 264
    :cond_3
    move p3, v3

    .line 270
    :cond_4
    :goto_1
    add-int/lit8 v3, v3, -0x1

    .line 271
    goto/16 :goto_0

    .line 265
    :cond_5
    sub-int v4, p3, v3

    iget v5, p0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    if-ne v4, v5, :cond_4

    .line 267
    new-instance v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-interface {v1, v3, p3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 268
    move p3, v3

    goto :goto_1

    .line 275
    .end local v0    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_6
    if-lez p3, :cond_8

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v4, v3, 0x1

    if-lt v4, p3, :cond_7

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/LogMergePolicy;->isMerged(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 276
    :cond_7
    new-instance v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-interface {v1, v3, p3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 279
    :cond_8
    iget-object v4, v2, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_9

    const/4 v2, 0x0

    .end local v2    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_9
    return-object v2
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 178
    return-void
.end method

.method public findForcedDeletesMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 11
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, -0x1

    .line 416
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->asList()Ljava/util/List;

    move-result-object v5

    .line 417
    .local v5, "segments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    .line 419
    .local v4, "numSegments":I
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 420
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "findForcedDeleteMerges: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " segments"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 422
    :cond_0
    new-instance v6, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    invoke-direct {v6}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 423
    .local v6, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    const/4 v1, -0x1

    .line 424
    .local v1, "firstSegmentWithDeletions":I
    iget-object v8, p0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v8}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/IndexWriter;

    .line 425
    .local v7, "w":Lorg/apache/lucene/index/IndexWriter;
    sget-boolean v8, Lorg/apache/lucene/index/LogMergePolicy;->$assertionsDisabled:Z

    if-nez v8, :cond_1

    if-nez v7, :cond_1

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 426
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_8

    .line 427
    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v3

    .line 428
    .local v3, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v7, v3}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v0

    .line 429
    .local v0, "delCount":I
    if-lez v0, :cond_6

    .line 430
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 431
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "  segment "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " has deletions"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 432
    :cond_2
    if-ne v1, v10, :cond_4

    .line 433
    move v1, v2

    .line 426
    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 434
    :cond_4
    sub-int v8, v2, v1

    iget v9, p0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    if-ne v8, v9, :cond_3

    .line 437
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 438
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "  add merge "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v2, -0x1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " inclusive"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 439
    :cond_5
    new-instance v8, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-interface {v5, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v6, v8}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 440
    move v1, v2

    goto :goto_1

    .line 442
    :cond_6
    if-eq v1, v10, :cond_3

    .line 446
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 447
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "  add merge "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v2, -0x1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " inclusive"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 448
    :cond_7
    new-instance v8, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-interface {v5, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v6, v8}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 449
    const/4 v1, -0x1

    goto/16 :goto_1

    .line 453
    .end local v0    # "delCount":I
    .end local v3    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_8
    if-eq v1, v10, :cond_a

    .line 454
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 455
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "  add merge "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v4, -0x1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " inclusive"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 456
    :cond_9
    new-instance v8, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-interface {v5, v1, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v6, v8}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 459
    :cond_a
    return-object v6
.end method

.method public findForcedMerges(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 8
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "maxNumSegments"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "I",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lorg/apache/lucene/index/MergePolicy$MergeSpecification;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p3, "segmentsToMerge":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/Boolean;>;"
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 355
    sget-boolean v5, Lorg/apache/lucene/index/LogMergePolicy;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    if-gtz p2, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 356
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 357
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "findForcedMerges: maxNumSegs="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " segsToMerge="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 362
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/index/LogMergePolicy;->isMerged(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 363
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 364
    const-string/jumbo v5, "already merged; skip"

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 404
    :cond_2
    :goto_0
    return-object v4

    .line 372
    :cond_3
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v3

    .line 373
    .local v3, "last":I
    :cond_4
    if-lez v3, :cond_5

    .line 374
    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v2

    .line 375
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 376
    add-int/lit8 v3, v3, 0x1

    .line 381
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_5
    if-eqz v3, :cond_2

    .line 384
    if-ne p2, v7, :cond_6

    if-ne v3, v7, :cond_6

    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/LogMergePolicy;->isMerged(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 385
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 386
    const-string/jumbo v5, "already 1 seg; skip"

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    goto :goto_0

    .line 392
    :cond_6
    const/4 v0, 0x0

    .line 393
    .local v0, "anyTooLarge":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_8

    .line 394
    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v2

    .line 395
    .restart local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v4

    iget-wide v6, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSizeForForcedMerge:J

    cmp-long v4, v4, v6

    if-gtz v4, :cond_7

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/LogMergePolicy;->sizeDocs(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v4

    iget v6, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_9

    .line 396
    :cond_7
    const/4 v0, 0x1

    .line 401
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_8
    if-eqz v0, :cond_a

    .line 402
    invoke-direct {p0, p1, p2, v3}, Lorg/apache/lucene/index/LogMergePolicy;->findForcedMergesSizeLimit(Lorg/apache/lucene/index/SegmentInfos;II)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v4

    goto :goto_0

    .line 393
    .restart local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 404
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_a
    invoke-direct {p0, p1, p2, v3}, Lorg/apache/lucene/index/LogMergePolicy;->findForcedMergesMaxNumSegments(Lorg/apache/lucene/index/SegmentInfos;II)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v4

    goto :goto_0
.end method

.method public findMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 34
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 494
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v18

    .line 495
    .local v18, "numSegments":I
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v26

    if-eqz v26, :cond_0

    .line 496
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "findMerges: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, " segments"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 500
    :cond_0
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 501
    .local v12, "levels":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;>;"
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-double v0, v0

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->log(D)D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v16, v0

    .line 503
    .local v16, "norm":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/index/IndexWriter;->getMergingSegments()Ljava/util/Collection;

    move-result-object v15

    .line 505
    .local v15, "mergingSegments":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/SegmentInfo;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    move/from16 v0, v18

    if-ge v6, v0, :cond_5

    .line 506
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v7

    .line 507
    .local v7, "info":Lorg/apache/lucene/index/SegmentInfo;
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v22

    .line 510
    .local v22, "size":J
    const-wide/16 v26, 0x1

    cmp-long v26, v22, v26

    if-gez v26, :cond_1

    .line 511
    const-wide/16 v22, 0x1

    .line 514
    :cond_1
    new-instance v8, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->log(D)D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v26, v0

    div-float v26, v26, v16

    move/from16 v0, v26

    invoke-direct {v8, v7, v0, v6}, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;-><init>(Lorg/apache/lucene/index/SegmentInfo;FI)V

    .line 515
    .local v8, "infoLevel":Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;
    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 517
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v26

    if-eqz v26, :cond_3

    .line 518
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/LogMergePolicy;->sizeBytes(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v20

    .line 519
    .local v20, "segBytes":J
    invoke-interface {v15, v7}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_4

    const-string/jumbo v5, " [merging]"

    .line 520
    .local v5, "extra":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSize:J

    move-wide/from16 v26, v0

    cmp-long v26, v22, v26

    if-ltz v26, :cond_2

    .line 521
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, " [skip: too large]"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 523
    :cond_2
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "seg="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/IndexWriter;->segString(Lorg/apache/lucene/index/SegmentInfo;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, " level="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    iget v0, v8, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, " size="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, "%.3f MB"

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-wide/16 v30, 0x400

    div-long v30, v20, v30

    move-wide/from16 v0, v30

    long-to-double v0, v0

    move-wide/from16 v30, v0

    const-wide/high16 v32, 0x4090000000000000L    # 1024.0

    div-double v30, v30, v32

    invoke-static/range {v30 .. v31}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v30

    aput-object v30, v28, v29

    invoke-static/range {v27 .. v28}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 505
    .end local v5    # "extra":Ljava/lang/String;
    .end local v20    # "segBytes":J
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 519
    .restart local v20    # "segBytes":J
    :cond_4
    const-string/jumbo v5, ""

    goto/16 :goto_1

    .line 528
    .end local v7    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v8    # "infoLevel":Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;
    .end local v20    # "segBytes":J
    .end local v22    # "size":J
    :cond_5
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->minMergeSize:J

    move-wide/from16 v26, v0

    const-wide/16 v28, 0x0

    cmp-long v26, v26, v28

    if-gtz v26, :cond_7

    .line 529
    const/4 v11, 0x0

    .line 540
    .local v11, "levelFloor":F
    :goto_2
    const/16 v19, 0x0

    .line 542
    .local v19, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v17

    .line 544
    .local v17, "numMergeableSegments":I
    const/16 v24, 0x0

    .line 545
    .local v24, "start":I
    :goto_3
    move/from16 v0, v24

    move/from16 v1, v17

    if-ge v0, v1, :cond_1a

    .line 549
    move/from16 v0, v24

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    move-object/from16 v0, v26

    iget v13, v0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    .line 550
    .local v13, "maxLevel":F
    add-int/lit8 v6, v24, 0x1

    :goto_4
    move/from16 v0, v17

    if-ge v6, v0, :cond_8

    .line 551
    invoke-interface {v12, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    move-object/from16 v0, v26

    iget v9, v0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    .line 552
    .local v9, "level":F
    cmpl-float v26, v9, v13

    if-lez v26, :cond_6

    .line 553
    move v13, v9

    .line 550
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 531
    .end local v9    # "level":F
    .end local v11    # "levelFloor":F
    .end local v13    # "maxLevel":F
    .end local v17    # "numMergeableSegments":I
    .end local v19    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .end local v24    # "start":I
    :cond_7
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->minMergeSize:J

    move-wide/from16 v26, v0

    move-wide/from16 v0, v26

    long-to-double v0, v0

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->log(D)D

    move-result-wide v26

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v28, v0

    div-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-float v11, v0

    .restart local v11    # "levelFloor":F
    goto :goto_2

    .line 559
    .restart local v13    # "maxLevel":F
    .restart local v17    # "numMergeableSegments":I
    .restart local v19    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .restart local v24    # "start":I
    :cond_8
    cmpg-float v26, v13, v11

    if-gtz v26, :cond_f

    .line 561
    const/high16 v10, -0x40800000    # -1.0f

    .line 570
    .local v10, "levelBottom":F
    :cond_9
    :goto_5
    add-int/lit8 v25, v17, -0x1

    .line 571
    .local v25, "upto":I
    :goto_6
    move/from16 v0, v25

    move/from16 v1, v24

    if-lt v0, v1, :cond_a

    .line 572
    move/from16 v0, v25

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    move-object/from16 v0, v26

    iget v0, v0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    move/from16 v26, v0

    cmpl-float v26, v26, v10

    if-ltz v26, :cond_10

    .line 577
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v26

    if-eqz v26, :cond_b

    .line 578
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "  level "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, " to "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, ": "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    add-int/lit8 v27, v25, 0x1

    sub-int v27, v27, v24

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, " segments"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 581
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    move/from16 v26, v0

    add-int v4, v24, v26

    .line 582
    .local v4, "end":I
    :goto_7
    add-int/lit8 v26, v25, 0x1

    move/from16 v0, v26

    if-gt v4, v0, :cond_19

    .line 583
    const/4 v3, 0x0

    .line 584
    .local v3, "anyTooLarge":Z
    const/4 v2, 0x0

    .line 585
    .local v2, "anyMerging":Z
    move/from16 v6, v24

    :goto_8
    if-ge v6, v4, :cond_d

    .line 586
    invoke-interface {v12, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    move-object/from16 v0, v26

    iget-object v7, v0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->info:Lorg/apache/lucene/index/SegmentInfo;

    .line 587
    .restart local v7    # "info":Lorg/apache/lucene/index/SegmentInfo;
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v26

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSize:J

    move-wide/from16 v28, v0

    cmp-long v26, v26, v28

    if-gez v26, :cond_c

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/LogMergePolicy;->sizeDocs(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v26

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-long v0, v0

    move-wide/from16 v28, v0

    cmp-long v26, v26, v28

    if-ltz v26, :cond_11

    :cond_c
    const/16 v26, 0x1

    :goto_9
    or-int v3, v3, v26

    .line 588
    invoke-interface {v15, v7}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_12

    .line 589
    const/4 v2, 0x1

    .line 594
    .end local v7    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_d
    if-eqz v2, :cond_13

    .line 612
    :cond_e
    :goto_a
    move/from16 v24, v4

    .line 613
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    move/from16 v26, v0

    add-int v4, v24, v26

    .line 614
    goto :goto_7

    .line 563
    .end local v2    # "anyMerging":Z
    .end local v3    # "anyTooLarge":Z
    .end local v4    # "end":I
    .end local v10    # "levelBottom":F
    .end local v25    # "upto":I
    :cond_f
    float-to-double v0, v13

    move-wide/from16 v26, v0

    const-wide/high16 v28, 0x3fe8000000000000L    # 0.75

    sub-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-float v10, v0

    .line 566
    .restart local v10    # "levelBottom":F
    cmpg-float v26, v10, v11

    if-gez v26, :cond_9

    cmpl-float v26, v13, v11

    if-ltz v26, :cond_9

    .line 567
    move v10, v11

    goto/16 :goto_5

    .line 575
    .restart local v25    # "upto":I
    :cond_10
    add-int/lit8 v25, v25, -0x1

    goto/16 :goto_6

    .line 587
    .restart local v2    # "anyMerging":Z
    .restart local v3    # "anyTooLarge":Z
    .restart local v4    # "end":I
    .restart local v7    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_11
    const/16 v26, 0x0

    goto :goto_9

    .line 585
    :cond_12
    add-int/lit8 v6, v6, 0x1

    goto :goto_8

    .line 596
    .end local v7    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_13
    if-nez v3, :cond_18

    .line 597
    if-nez v19, :cond_14

    .line 598
    new-instance v19, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    .end local v19    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-direct/range {v19 .. v19}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 599
    .restart local v19    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_14
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 600
    .local v14, "mergeInfos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    move/from16 v6, v24

    :goto_b
    if-ge v6, v4, :cond_16

    .line 601
    invoke-interface {v12, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    move-object/from16 v0, v26

    iget-object v0, v0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 602
    sget-boolean v26, Lorg/apache/lucene/index/LogMergePolicy;->$assertionsDisabled:Z

    if-nez v26, :cond_15

    invoke-interface {v12, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    move-object/from16 v0, v26

    iget-object v0, v0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v26, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->contains(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v26

    if-nez v26, :cond_15

    new-instance v26, Ljava/lang/AssertionError;

    invoke-direct/range {v26 .. v26}, Ljava/lang/AssertionError;-><init>()V

    throw v26

    .line 600
    :cond_15
    add-int/lit8 v6, v6, 0x1

    goto :goto_b

    .line 604
    :cond_16
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v26

    if-eqz v26, :cond_17

    .line 605
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "  add merge="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, " start="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, " end="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    .line 607
    :cond_17
    new-instance v26, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-object/from16 v0, v26

    invoke-direct {v0, v14}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    goto/16 :goto_a

    .line 608
    .end local v14    # "mergeInfos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    :cond_18
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v26

    if-eqz v26, :cond_e

    .line 609
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "    "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, " to "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, ": contains segment over maxMergeSize or maxMergeDocs; skipping"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/LogMergePolicy;->message(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 616
    .end local v2    # "anyMerging":Z
    .end local v3    # "anyTooLarge":Z
    :cond_19
    add-int/lit8 v24, v25, 0x1

    .line 617
    goto/16 :goto_3

    .line 619
    .end local v4    # "end":I
    .end local v10    # "levelBottom":F
    .end local v13    # "maxLevel":F
    .end local v25    # "upto":I
    :cond_1a
    return-object v19
.end method

.method public getCalibrateSizeByDeletes()Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->calibrateSizeByDeletes:Z

    return v0
.end method

.method public getMaxMergeDocs()I
    .locals 1

    .prologue
    .line 645
    iget v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    return v0
.end method

.method public getMergeFactor()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    return v0
.end method

.method public getNoCFSRatio()D
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->noCFSRatio:D

    return-wide v0
.end method

.method public getUseCompoundFile()Z
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->useCompoundFile:Z

    return v0
.end method

.method protected isMerged(Lorg/apache/lucene/index/SegmentInfo;)Z
    .locals 8
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 228
    iget-object v4, p0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v4}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexWriter;

    .line 229
    .local v1, "w":Lorg/apache/lucene/index/IndexWriter;
    sget-boolean v4, Lorg/apache/lucene/index/LogMergePolicy;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-nez v1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 230
    :cond_0
    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v4

    if-lez v4, :cond_2

    move v0, v2

    .line 231
    .local v0, "hasDeletions":Z
    :goto_0
    if-nez v0, :cond_3

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->hasSeparateNorms()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriter;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v5

    if-ne v4, v5, :cond_3

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v4

    iget-boolean v5, p0, Lorg/apache/lucene/index/LogMergePolicy;->useCompoundFile:Z

    if-eq v4, v5, :cond_1

    iget-wide v4, p0, Lorg/apache/lucene/index/LogMergePolicy;->noCFSRatio:D

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpg-double v4, v4, v6

    if-gez v4, :cond_3

    :cond_1
    :goto_1
    return v2

    .end local v0    # "hasDeletions":Z
    :cond_2
    move v0, v3

    .line 230
    goto :goto_0

    .restart local v0    # "hasDeletions":Z
    :cond_3
    move v2, v3

    .line 231
    goto :goto_1
.end method

.method protected isMerged(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Z
    .locals 9
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "maxNumSegments"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "I",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p3, "segmentsToMerge":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/Boolean;>;"
    const/4 v7, 0x1

    .line 205
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v4

    .line 206
    .local v4, "numSegments":I
    const/4 v5, 0x0

    .line 207
    .local v5, "numToMerge":I
    const/4 v3, 0x0

    .line 208
    .local v3, "mergeInfo":Lorg/apache/lucene/index/SegmentInfo;
    const/4 v6, 0x0

    .line 209
    .local v6, "segmentIsOriginal":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_1

    if-gt v5, p2, :cond_1

    .line 210
    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v1

    .line 211
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 212
    .local v2, "isOriginal":Ljava/lang/Boolean;
    if-eqz v2, :cond_0

    .line 213
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 214
    add-int/lit8 v5, v5, 0x1

    .line 215
    move-object v3, v1

    .line 209
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 219
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v2    # "isOriginal":Ljava/lang/Boolean;
    :cond_1
    if-gt v5, p2, :cond_3

    if-ne v5, v7, :cond_2

    if-eqz v6, :cond_2

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/LogMergePolicy;->isMerged(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v8

    if-eqz v8, :cond_3

    :cond_2
    :goto_1
    return v7

    :cond_3
    const/4 v7, 0x0

    goto :goto_1
.end method

.method protected message(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 107
    invoke-virtual {p0}, Lorg/apache/lucene/index/LogMergePolicy;->verbose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v0}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "LMP: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 109
    :cond_0
    return-void
.end method

.method public setCalibrateSizeByDeletes(Z)V
    .locals 0
    .param p1, "calibrateSizeByDeletes"    # Z

    .prologue
    .line 168
    iput-boolean p1, p0, Lorg/apache/lucene/index/LogMergePolicy;->calibrateSizeByDeletes:Z

    .line 169
    return-void
.end method

.method public setMaxMergeDocs(I)V
    .locals 0
    .param p1, "maxMergeDocs"    # I

    .prologue
    .line 638
    iput p1, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    .line 639
    return-void
.end method

.method public setMergeFactor(I)V
    .locals 2
    .param p1, "mergeFactor"    # I

    .prologue
    .line 128
    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    .line 129
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "mergeFactor cannot be less than 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_0
    iput p1, p0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    .line 131
    return-void
.end method

.method public setNoCFSRatio(D)V
    .locals 3
    .param p1, "noCFSRatio"    # D

    .prologue
    .line 100
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-ltz v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    .line 101
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "noCFSRatio must be 0.0 to 1.0 inclusive; got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_1
    iput-wide p1, p0, Lorg/apache/lucene/index/LogMergePolicy;->noCFSRatio:D

    .line 104
    return-void
.end method

.method public setUseCompoundFile(Z)V
    .locals 0
    .param p1, "useCompoundFile"    # Z

    .prologue
    .line 155
    iput-boolean p1, p0, Lorg/apache/lucene/index/LogMergePolicy;->useCompoundFile:Z

    .line 156
    return-void
.end method

.method protected abstract size(Lorg/apache/lucene/index/SegmentInfo;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected sizeBytes(Lorg/apache/lucene/index/SegmentInfo;)J
    .locals 10
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 193
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytes(Z)J

    move-result-wide v0

    .line 194
    .local v0, "byteSize":J
    iget-boolean v3, p0, Lorg/apache/lucene/index/LogMergePolicy;->calibrateSizeByDeletes:Z

    if-eqz v3, :cond_2

    .line 195
    iget-object v3, p0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v3}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v2

    .line 196
    .local v2, "delCount":I
    iget v3, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-gtz v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    float-to-double v4, v3

    .line 197
    .local v4, "delRatio":D
    sget-boolean v3, Lorg/apache/lucene/index/LogMergePolicy;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    cmpg-double v3, v4, v8

    if-lez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 196
    .end local v4    # "delRatio":D
    :cond_0
    int-to-float v3, v2

    iget v6, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    int-to-float v6, v6

    div-float/2addr v3, v6

    goto :goto_0

    .line 198
    .restart local v4    # "delRatio":D
    :cond_1
    iget v3, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-gtz v3, :cond_3

    .line 200
    .end local v0    # "byteSize":J
    .end local v2    # "delCount":I
    .end local v4    # "delRatio":D
    :cond_2
    :goto_1
    return-wide v0

    .line 198
    .restart local v0    # "byteSize":J
    .restart local v2    # "delCount":I
    .restart local v4    # "delRatio":D
    :cond_3
    long-to-double v6, v0

    sub-double/2addr v8, v4

    mul-double/2addr v6, v8

    double-to-long v0, v6

    goto :goto_1
.end method

.method protected sizeDocs(Lorg/apache/lucene/index/SegmentInfo;)J
    .locals 6
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    iget-boolean v1, p0, Lorg/apache/lucene/index/LogMergePolicy;->calibrateSizeByDeletes:Z

    if-eqz v1, :cond_1

    .line 184
    iget-object v1, p0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v1}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v0

    .line 185
    .local v0, "delCount":I
    sget-boolean v1, Lorg/apache/lucene/index/LogMergePolicy;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-le v0, v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 186
    :cond_0
    iget v1, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    int-to-long v2, v1

    int-to-long v4, v0

    sub-long/2addr v2, v4

    .line 188
    .end local v0    # "delCount":I
    :goto_0
    return-wide v2

    :cond_1
    iget v1, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    int-to-long v2, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 650
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 651
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "minMergeSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->minMergeSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 652
    const-string/jumbo v1, "mergeFactor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->mergeFactor:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 653
    const-string/jumbo v1, "maxMergeSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 654
    const-string/jumbo v1, "maxMergeSizeForForcedMerge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeSizeForForcedMerge:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 655
    const-string/jumbo v1, "calibrateSizeByDeletes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->calibrateSizeByDeletes:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 656
    const-string/jumbo v1, "maxMergeDocs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->maxMergeDocs:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 657
    const-string/jumbo v1, "useCompoundFile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->useCompoundFile:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 658
    const-string/jumbo v1, "noCFSRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/LogMergePolicy;->noCFSRatio:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 659
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 660
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfo;)Z
    .locals 12
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "mergedInfo"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    iget-boolean v3, p0, Lorg/apache/lucene/index/LogMergePolicy;->useCompoundFile:Z

    if-nez v3, :cond_0

    .line 139
    const/4 v0, 0x0

    .line 149
    .local v0, "doCFS":Z
    :goto_0
    return v0

    .line 140
    .end local v0    # "doCFS":Z
    :cond_0
    iget-wide v6, p0, Lorg/apache/lucene/index/LogMergePolicy;->noCFSRatio:D

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v6, v8

    if-nez v3, :cond_1

    .line 141
    const/4 v0, 0x1

    .restart local v0    # "doCFS":Z
    goto :goto_0

    .line 143
    .end local v0    # "doCFS":Z
    :cond_1
    const-wide/16 v4, 0x0

    .line 144
    .local v4, "totalSize":J
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentInfo;

    .line 145
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v6

    add-long/2addr v4, v6

    goto :goto_1

    .line 147
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_2
    invoke-virtual {p0, p2}, Lorg/apache/lucene/index/LogMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v6

    long-to-double v6, v6

    iget-wide v8, p0, Lorg/apache/lucene/index/LogMergePolicy;->noCFSRatio:D

    long-to-double v10, v4

    mul-double/2addr v8, v10

    cmpg-double v3, v6, v8

    if-gtz v3, :cond_3

    const/4 v0, 0x1

    .restart local v0    # "doCFS":Z
    :goto_2
    goto :goto_0

    .end local v0    # "doCFS":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method protected verbose()Z
    .locals 2

    .prologue
    .line 85
    iget-object v1, p0, Lorg/apache/lucene/index/LogMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v1}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriter;

    .line 86
    .local v0, "w":Lorg/apache/lucene/index/IndexWriter;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->verbose()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
