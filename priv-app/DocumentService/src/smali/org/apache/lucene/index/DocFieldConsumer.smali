.class abstract Lorg/apache/lucene/index/DocFieldConsumer;
.super Ljava/lang/Object;
.source "DocFieldConsumer.java"


# instance fields
.field fieldInfos:Lorg/apache/lucene/index/FieldInfos;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract abort()V
.end method

.method abstract addThread(Lorg/apache/lucene/index/DocFieldProcessorPerThread;)Lorg/apache/lucene/index/DocFieldConsumerPerThread;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/DocFieldConsumerPerThread;",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/DocFieldConsumerPerField;",
            ">;>;",
            "Lorg/apache/lucene/index/SegmentWriteState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract freeRAM()Z
.end method

.method setFieldInfos(Lorg/apache/lucene/index/FieldInfos;)V
    .locals 0
    .param p1, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;

    .prologue
    .line 44
    iput-object p1, p0, Lorg/apache/lucene/index/DocFieldConsumer;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 45
    return-void
.end method
