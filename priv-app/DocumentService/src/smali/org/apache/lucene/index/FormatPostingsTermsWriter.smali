.class final Lorg/apache/lucene/index/FormatPostingsTermsWriter;
.super Lorg/apache/lucene/index/FormatPostingsTermsConsumer;
.source "FormatPostingsTermsWriter.java"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field currentTerm:[C

.field currentTermStart:I

.field final docsWriter:Lorg/apache/lucene/index/FormatPostingsDocsWriter;

.field fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field freqStart:J

.field final parent:Lorg/apache/lucene/index/FormatPostingsFieldsWriter;

.field proxStart:J

.field final termsOut:Lorg/apache/lucene/index/TermInfosWriter;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/index/FormatPostingsFieldsWriter;)V
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "parent"    # Lorg/apache/lucene/index/FormatPostingsFieldsWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/index/FormatPostingsTermsConsumer;-><init>()V

    .line 31
    iput-object p2, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsFieldsWriter;

    .line 32
    iget-object v0, p2, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->termsOut:Lorg/apache/lucene/index/TermInfosWriter;

    iput-object v0, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->termsOut:Lorg/apache/lucene/index/TermInfosWriter;

    .line 33
    new-instance v0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;

    invoke-direct {v0, p1, p0}, Lorg/apache/lucene/index/FormatPostingsDocsWriter;-><init>(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/index/FormatPostingsTermsWriter;)V

    iput-object v0, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->docsWriter:Lorg/apache/lucene/index/FormatPostingsDocsWriter;

    .line 34
    return-void
.end method


# virtual methods
.method addTerm([CI)Lorg/apache/lucene/index/FormatPostingsDocsConsumer;
    .locals 2
    .param p1, "text"    # [C
    .param p2, "start"    # I

    .prologue
    .line 50
    iput-object p1, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->currentTerm:[C

    .line 51
    iput p2, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->currentTermStart:I

    .line 56
    iget-object v0, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->docsWriter:Lorg/apache/lucene/index/FormatPostingsDocsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->freqStart:J

    .line 57
    iget-object v0, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->docsWriter:Lorg/apache/lucene/index/FormatPostingsDocsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->posWriter:Lorg/apache/lucene/index/FormatPostingsPositionsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->docsWriter:Lorg/apache/lucene/index/FormatPostingsDocsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->posWriter:Lorg/apache/lucene/index/FormatPostingsPositionsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/FormatPostingsPositionsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->proxStart:J

    .line 60
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->parent:Lorg/apache/lucene/index/FormatPostingsFieldsWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;->skipListWriter:Lorg/apache/lucene/index/DefaultSkipListWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DefaultSkipListWriter;->resetSkip()V

    .line 62
    iget-object v0, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->docsWriter:Lorg/apache/lucene/index/FormatPostingsDocsWriter;

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->docsWriter:Lorg/apache/lucene/index/FormatPostingsDocsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->close()V

    .line 72
    return-void
.end method

.method finish()V
    .locals 0

    .prologue
    .line 68
    return-void
.end method

.method setField(Lorg/apache/lucene/index/FieldInfo;)V
    .locals 1
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 37
    iput-object p1, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 38
    iget-object v0, p0, Lorg/apache/lucene/index/FormatPostingsTermsWriter;->docsWriter:Lorg/apache/lucene/index/FormatPostingsDocsWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/FormatPostingsDocsWriter;->setField(Lorg/apache/lucene/index/FieldInfo;)V

    .line 39
    return-void
.end method
