.class Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
.super Ljava/lang/Object;
.source "SnapshotDeletionPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/SnapshotDeletionPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SnapshotInfo"
.end annotation


# instance fields
.field commit:Lorg/apache/lucene/index/IndexCommit;

.field id:Ljava/lang/String;

.field segmentsFileName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/index/IndexCommit;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "segmentsFileName"    # Ljava/lang/String;
    .param p3, "commit"    # Lorg/apache/lucene/index/IndexCommit;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->id:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->segmentsFileName:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->commit:Lorg/apache/lucene/index/IndexCommit;

    .line 64
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->segmentsFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
