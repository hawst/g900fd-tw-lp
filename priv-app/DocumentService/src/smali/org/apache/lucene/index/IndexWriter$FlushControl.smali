.class final Lorg/apache/lucene/index/IndexWriter$FlushControl;
.super Ljava/lang/Object;
.source "IndexWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/IndexWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "FlushControl"
.end annotation


# instance fields
.field private delCount:I

.field private docCount:I

.field private flushDeletes:Z

.field private flushPending:Z

.field private flushing:Z

.field final synthetic this$0:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 0

    .prologue
    .line 4815
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->this$0:Lorg/apache/lucene/index/IndexWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private declared-synchronized setFlushPending(Ljava/lang/String;Z)Z
    .locals 4
    .param p1, "reason"    # Ljava/lang/String;
    .param p2, "doWait"    # Z

    .prologue
    .line 4824
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushPending:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushing:Z

    if-eqz v1, :cond_3

    .line 4825
    :cond_0
    if-eqz p2, :cond_2

    .line 4826
    :goto_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushPending:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushing:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_2

    .line 4828
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 4829
    :catch_0
    move-exception v0

    .line 4830
    .local v0, "ie":Ljava/lang/InterruptedException;
    :try_start_2
    new-instance v1, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4824
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 4834
    :cond_2
    const/4 v1, 0x0

    .line 4840
    :goto_1
    monitor-exit p0

    return v1

    .line 4836
    :cond_3
    :try_start_3
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;
    invoke-static {v1}, Lorg/apache/lucene/index/IndexWriter;->access$400(Lorg/apache/lucene/index/IndexWriter;)Ljava/io/PrintStream;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 4837
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->this$0:Lorg/apache/lucene/index/IndexWriter;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "now trigger flush reason="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4839
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushPending:Z

    .line 4840
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushPending:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized clearDeletes()V
    .locals 1

    .prologue
    .line 4867
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->delCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4868
    monitor-exit p0

    return-void

    .line 4867
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clearFlushPending()V
    .locals 2

    .prologue
    .line 4857
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;
    invoke-static {v0}, Lorg/apache/lucene/index/IndexWriter;->access$400(Lorg/apache/lucene/index/IndexWriter;)Ljava/io/PrintStream;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4858
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->this$0:Lorg/apache/lucene/index/IndexWriter;

    const-string/jumbo v1, "clearFlushPending"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4860
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushPending:Z

    .line 4861
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushDeletes:Z

    .line 4862
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->docCount:I

    .line 4863
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4864
    monitor-exit p0

    return-void

    .line 4857
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized flushByRAMUsage(Ljava/lang/String;)Z
    .locals 12
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const-wide/high16 v10, 0x4090000000000000L    # 1024.0

    .line 4910
    monitor-enter p0

    :try_start_0
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;
    invoke-static {v7}, Lorg/apache/lucene/index/IndexWriter;->access$200(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v2

    .line 4911
    .local v2, "ramBufferSizeMB":D
    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    cmpl-double v7, v2, v8

    if-eqz v7, :cond_0

    .line 4912
    mul-double v8, v2, v10

    mul-double/2addr v8, v10

    double-to-long v0, v8

    .line 4913
    .local v0, "limit":J
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v7, v7, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v7}, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed()J

    move-result-wide v8

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;
    invoke-static {v7}, Lorg/apache/lucene/index/IndexWriter;->access$500(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/DocumentsWriter;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed()J

    move-result-wide v10

    add-long v4, v8, v10

    .line 4914
    .local v4, "used":J
    cmp-long v7, v4, v0

    if-ltz v7, :cond_0

    .line 4919
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;
    invoke-static {v7}, Lorg/apache/lucene/index/IndexWriter;->access$500(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/DocumentsWriter;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/lucene/index/DocumentsWriter;->balanceRAM()V

    .line 4921
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v7, v7, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v7}, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed()J

    move-result-wide v8

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;
    invoke-static {v7}, Lorg/apache/lucene/index/IndexWriter;->access$500(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/DocumentsWriter;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed()J

    move-result-wide v10

    add-long v4, v8, v10

    .line 4922
    cmp-long v7, v4, v0

    if-ltz v7, :cond_0

    .line 4923
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "ram full: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {p0, v6, v7}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->setFlushPending(Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 4927
    .end local v0    # "limit":J
    .end local v4    # "used":J
    :cond_0
    monitor-exit p0

    return v6

    .line 4910
    .end local v2    # "ramBufferSizeMB":D
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method public declared-synchronized getFlushDeletes()Z
    .locals 1

    .prologue
    .line 4853
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushDeletes:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getFlushPending()Z
    .locals 1

    .prologue
    .line 4849
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushPending:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setFlushPendingNoWait(Ljava/lang/String;)V
    .locals 1
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 4845
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->setFlushPending(Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4846
    monitor-exit p0

    return-void

    .line 4845
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized waitUpdate(II)Z
    .locals 1
    .param p1, "docInc"    # I
    .param p2, "delInc"    # I

    .prologue
    .line 4871
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->waitUpdate(IIZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized waitUpdate(IIZ)Z
    .locals 5
    .param p1, "docInc"    # I
    .param p2, "delInc"    # I
    .param p3, "skipWait"    # Z

    .prologue
    const/4 v4, -0x1

    .line 4875
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v3, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushPending:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    .line 4877
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 4878
    :catch_0
    move-exception v0

    .line 4879
    .local v0, "ie":Ljava/lang/InterruptedException;
    :try_start_2
    new-instance v3, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v3, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4875
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 4883
    :cond_0
    :try_start_3
    iget v3, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->docCount:I

    add-int/2addr v3, p1

    iput v3, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->docCount:I

    .line 4884
    iget v3, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->delCount:I

    add-int/2addr v3, p2

    iput v3, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->delCount:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4889
    if-eqz p3, :cond_1

    .line 4890
    const/4 v3, 0x0

    .line 4906
    :goto_1
    monitor-exit p0

    return v3

    .line 4893
    :cond_1
    :try_start_4
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;
    invoke-static {v3}, Lorg/apache/lucene/index/IndexWriter;->access$200(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriterConfig;->getMaxBufferedDocs()I

    move-result v2

    .line 4894
    .local v2, "maxBufferedDocs":I
    if-eq v2, v4, :cond_2

    iget v3, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->docCount:I

    if-lt v3, v2, :cond_2

    .line 4896
    const-string/jumbo v3, "maxBufferedDocs"

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->setFlushPending(Ljava/lang/String;Z)Z

    move-result v3

    goto :goto_1

    .line 4899
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;
    invoke-static {v3}, Lorg/apache/lucene/index/IndexWriter;->access$200(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriterConfig;->getMaxBufferedDeleteTerms()I

    move-result v1

    .line 4900
    .local v1, "maxBufferedDeleteTerms":I
    if-eq v1, v4, :cond_3

    iget v3, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->delCount:I

    if-lt v3, v1, :cond_3

    .line 4902
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushDeletes:Z

    .line 4903
    const-string/jumbo v3, "maxBufferedDeleteTerms"

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->setFlushPending(Ljava/lang/String;Z)Z

    move-result v3

    goto :goto_1

    .line 4906
    :cond_3
    const-string/jumbo v3, "add delete/doc"

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushByRAMUsage(Ljava/lang/String;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v3

    goto :goto_1
.end method
