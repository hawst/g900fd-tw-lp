.class final Lorg/apache/lucene/index/TermInfosReader;
.super Ljava/lang/Object;
.source "TermInfosReader.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/TermInfosReader$1;,
        Lorg/apache/lucene/index/TermInfosReader$ThreadResources;,
        Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;,
        Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final DEFAULT_CACHE_SIZE:I = 0x400


# instance fields
.field private final directory:Lorg/apache/lucene/store/Directory;

.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private final index:Lorg/apache/lucene/index/TermInfosReaderIndex;

.field private final indexLength:I

.field private final origEnum:Lorg/apache/lucene/index/SegmentTermEnum;

.field private final segment:Ljava/lang/String;

.field private final size:J

.field private final termsCache:Lorg/apache/lucene/util/DoubleBarrelLRUCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/DoubleBarrelLRUCache",
            "<",
            "Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;",
            "Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;",
            ">;"
        }
    .end annotation
.end field

.field private final threadResources:Lorg/apache/lucene/util/CloseableThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/CloseableThreadLocal",
            "<",
            "Lorg/apache/lucene/index/TermInfosReader$ThreadResources;",
            ">;"
        }
    .end annotation
.end field

.field private final totalIndexInterval:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/apache/lucene/index/TermInfosReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermInfosReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;II)V
    .locals 8
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "seg"    # Ljava/lang/String;
    .param p3, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "readBufferSize"    # I
    .param p5, "indexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v6, -0x1

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v1, Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-direct {v1}, Lorg/apache/lucene/util/CloseableThreadLocal;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/TermInfosReader;->threadResources:Lorg/apache/lucene/util/CloseableThreadLocal;

    .line 82
    new-instance v1, Lorg/apache/lucene/util/DoubleBarrelLRUCache;

    const/16 v3, 0x400

    invoke-direct {v1, v3}, Lorg/apache/lucene/util/DoubleBarrelLRUCache;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/index/TermInfosReader;->termsCache:Lorg/apache/lucene/util/DoubleBarrelLRUCache;

    .line 93
    const/4 v7, 0x0

    .line 95
    .local v7, "success":Z
    if-ge p5, v4, :cond_0

    if-eq p5, v6, :cond_0

    .line 96
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "indexDivisor must be -1 (don\'t load terms index) or greater than 0: got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 100
    :cond_0
    :try_start_0
    iput-object p1, p0, Lorg/apache/lucene/index/TermInfosReader;->directory:Lorg/apache/lucene/store/Directory;

    .line 101
    iput-object p2, p0, Lorg/apache/lucene/index/TermInfosReader;->segment:Ljava/lang/String;

    .line 102
    iput-object p3, p0, Lorg/apache/lucene/index/TermInfosReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 104
    new-instance v1, Lorg/apache/lucene/index/SegmentTermEnum;

    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosReader;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v4, p0, Lorg/apache/lucene/index/TermInfosReader;->segment:Ljava/lang/String;

    const-string/jumbo v5, "tis"

    invoke-static {v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/TermInfosReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    const/4 v5, 0x0

    invoke-direct {v1, v3, v4, v5}, Lorg/apache/lucene/index/SegmentTermEnum;-><init>(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfos;Z)V

    iput-object v1, p0, Lorg/apache/lucene/index/TermInfosReader;->origEnum:Lorg/apache/lucene/index/SegmentTermEnum;

    .line 106
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosReader;->origEnum:Lorg/apache/lucene/index/SegmentTermEnum;

    iget-wide v4, v1, Lorg/apache/lucene/index/SegmentTermEnum;->size:J

    iput-wide v4, p0, Lorg/apache/lucene/index/TermInfosReader;->size:J

    .line 109
    if-eq p5, v6, :cond_3

    .line 111
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosReader;->origEnum:Lorg/apache/lucene/index/SegmentTermEnum;

    iget v1, v1, Lorg/apache/lucene/index/SegmentTermEnum;->indexInterval:I

    mul-int/2addr v1, p5

    iput v1, p0, Lorg/apache/lucene/index/TermInfosReader;->totalIndexInterval:I

    .line 112
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosReader;->segment:Ljava/lang/String;

    const-string/jumbo v3, "tii"

    invoke-static {v1, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "indexFileName":Ljava/lang/String;
    new-instance v2, Lorg/apache/lucene/index/SegmentTermEnum;

    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosReader;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, v0, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    const/4 v4, 0x1

    invoke-direct {v2, v1, v3, v4}, Lorg/apache/lucene/index/SegmentTermEnum;-><init>(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfos;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 116
    .local v2, "indexEnum":Lorg/apache/lucene/index/SegmentTermEnum;
    :try_start_1
    new-instance v1, Lorg/apache/lucene/index/TermInfosReaderIndex;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/Directory;->fileLength(Ljava/lang/String;)J

    move-result-wide v4

    iget v6, p0, Lorg/apache/lucene/index/TermInfosReader;->totalIndexInterval:I

    move v3, p5

    invoke-direct/range {v1 .. v6}, Lorg/apache/lucene/index/TermInfosReaderIndex;-><init>(Lorg/apache/lucene/index/SegmentTermEnum;IJI)V

    iput-object v1, p0, Lorg/apache/lucene/index/TermInfosReader;->index:Lorg/apache/lucene/index/TermInfosReaderIndex;

    .line 117
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosReader;->index:Lorg/apache/lucene/index/TermInfosReaderIndex;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermInfosReaderIndex;->length()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/index/TermInfosReader;->indexLength:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119
    :try_start_2
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentTermEnum;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 127
    .end local v0    # "indexFileName":Ljava/lang/String;
    .end local v2    # "indexEnum":Lorg/apache/lucene/index/SegmentTermEnum;
    :goto_0
    const/4 v7, 0x1

    .line 134
    if-nez v7, :cond_1

    .line 135
    invoke-virtual {p0}, Lorg/apache/lucene/index/TermInfosReader;->close()V

    .line 138
    :cond_1
    return-void

    .line 119
    .restart local v0    # "indexFileName":Ljava/lang/String;
    .restart local v2    # "indexEnum":Lorg/apache/lucene/index/SegmentTermEnum;
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentTermEnum;->close()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 134
    .end local v0    # "indexFileName":Ljava/lang/String;
    .end local v2    # "indexEnum":Lorg/apache/lucene/index/SegmentTermEnum;
    :catchall_1
    move-exception v1

    if-nez v7, :cond_2

    .line 135
    invoke-virtual {p0}, Lorg/apache/lucene/index/TermInfosReader;->close()V

    .line 134
    :cond_2
    throw v1

    .line 123
    :cond_3
    const/4 v1, -0x1

    :try_start_4
    iput v1, p0, Lorg/apache/lucene/index/TermInfosReader;->totalIndexInterval:I

    .line 124
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/TermInfosReader;->index:Lorg/apache/lucene/index/TermInfosReaderIndex;

    .line 125
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/index/TermInfosReader;->indexLength:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0
.end method

.method private ensureIndexIsRead()V
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosReader;->index:Lorg/apache/lucene/index/TermInfosReaderIndex;

    if-nez v0, :cond_0

    .line 273
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "terms index was not loaded when this reader was created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 275
    :cond_0
    return-void
.end method

.method private get(Lorg/apache/lucene/index/Term;ZLorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermInfo;
    .locals 12
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "mustSeekEnum"    # Z
    .param p3, "termBytesRef"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    iget-wide v8, p0, Lorg/apache/lucene/index/TermInfosReader;->size:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_1

    const/4 v6, 0x0

    .line 249
    :cond_0
    :goto_0
    return-object v6

    .line 179
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/index/TermInfosReader;->ensureIndexIsRead()V

    .line 181
    new-instance v0, Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;

    invoke-direct {v0, p1}, Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 183
    .local v0, "cacheKey":Lorg/apache/lucene/index/TermInfosReader$CloneableTerm;
    iget-object v8, p0, Lorg/apache/lucene/index/TermInfosReader;->termsCache:Lorg/apache/lucene/util/DoubleBarrelLRUCache;

    invoke-virtual {v8, v0}, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->get(Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;

    .line 184
    .local v7, "tiOrd":Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;
    invoke-direct {p0}, Lorg/apache/lucene/index/TermInfosReader;->getThreadResources()Lorg/apache/lucene/index/TermInfosReader$ThreadResources;

    move-result-object v5

    .line 186
    .local v5, "resources":Lorg/apache/lucene/index/TermInfosReader$ThreadResources;
    if-nez p2, :cond_2

    if-eqz v7, :cond_2

    move-object v6, v7

    .line 187
    goto :goto_0

    .line 191
    :cond_2
    iget-object v2, v5, Lorg/apache/lucene/index/TermInfosReader$ThreadResources;->termEnum:Lorg/apache/lucene/index/SegmentTermEnum;

    .line 192
    .local v2, "enumerator":Lorg/apache/lucene/index/SegmentTermEnum;
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v8

    if-eqz v8, :cond_9

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentTermEnum;->prev()Lorg/apache/lucene/index/Term;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentTermEnum;->prev()Lorg/apache/lucene/index/Term;

    move-result-object v8

    invoke-virtual {p1, v8}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v8

    if-gtz v8, :cond_4

    :cond_3
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v8

    invoke-virtual {p1, v8}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v8

    if-ltz v8, :cond_9

    .line 195
    :cond_4
    iget-wide v8, v2, Lorg/apache/lucene/index/SegmentTermEnum;->position:J

    iget v10, p0, Lorg/apache/lucene/index/TermInfosReader;->totalIndexInterval:I

    int-to-long v10, v10

    div-long/2addr v8, v10

    long-to-int v8, v8

    add-int/lit8 v1, v8, 0x1

    .line 196
    .local v1, "enumOffset":I
    iget v8, p0, Lorg/apache/lucene/index/TermInfosReader;->indexLength:I

    if-eq v8, v1, :cond_5

    iget-object v8, p0, Lorg/apache/lucene/index/TermInfosReader;->index:Lorg/apache/lucene/index/TermInfosReaderIndex;

    invoke-virtual {v8, p1, p3, v1}, Lorg/apache/lucene/index/TermInfosReaderIndex;->compareTo(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/util/BytesRef;I)I

    move-result v8

    if-gez v8, :cond_9

    .line 202
    :cond_5
    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/SegmentTermEnum;->scanTo(Lorg/apache/lucene/index/Term;)I

    move-result v4

    .line 203
    .local v4, "numScans":I
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v8

    if-eqz v8, :cond_8

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v8

    invoke-virtual {p1, v8}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v8

    if-nez v8, :cond_8

    .line 204
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo()Lorg/apache/lucene/index/TermInfo;

    move-result-object v6

    .line 205
    .local v6, "ti":Lorg/apache/lucene/index/TermInfo;
    const/4 v8, 0x1

    if-le v4, v8, :cond_0

    .line 211
    if-nez v7, :cond_6

    .line 212
    iget-object v8, p0, Lorg/apache/lucene/index/TermInfosReader;->termsCache:Lorg/apache/lucene/util/DoubleBarrelLRUCache;

    new-instance v9, Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;

    iget-wide v10, v2, Lorg/apache/lucene/index/SegmentTermEnum;->position:J

    invoke-direct {v9, v6, v10, v11}, Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;-><init>(Lorg/apache/lucene/index/TermInfo;J)V

    invoke-virtual {v8, v0, v9}, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->put(Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;Ljava/lang/Object;)V

    goto :goto_0

    .line 214
    :cond_6
    sget-boolean v8, Lorg/apache/lucene/index/TermInfosReader;->$assertionsDisabled:Z

    if-nez v8, :cond_7

    invoke-direct {p0, v6, v7, v2}, Lorg/apache/lucene/index/TermInfosReader;->sameTermInfo(Lorg/apache/lucene/index/TermInfo;Lorg/apache/lucene/index/TermInfo;Lorg/apache/lucene/index/SegmentTermEnum;)Z

    move-result v8

    if-nez v8, :cond_7

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 215
    :cond_7
    sget-boolean v8, Lorg/apache/lucene/index/TermInfosReader;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    iget-wide v8, v2, Lorg/apache/lucene/index/SegmentTermEnum;->position:J

    long-to-int v8, v8

    int-to-long v8, v8

    iget-wide v10, v7, Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;->termOrd:J

    cmp-long v8, v8, v10

    if-eqz v8, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 219
    .end local v6    # "ti":Lorg/apache/lucene/index/TermInfo;
    :cond_8
    const/4 v6, 0x0

    .restart local v6    # "ti":Lorg/apache/lucene/index/TermInfo;
    goto/16 :goto_0

    .line 228
    .end local v1    # "enumOffset":I
    .end local v4    # "numScans":I
    .end local v6    # "ti":Lorg/apache/lucene/index/TermInfo;
    :cond_9
    if-eqz v7, :cond_a

    .line 229
    iget-wide v8, v7, Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;->termOrd:J

    iget v10, p0, Lorg/apache/lucene/index/TermInfosReader;->totalIndexInterval:I

    int-to-long v10, v10

    div-long/2addr v8, v10

    long-to-int v3, v8

    .line 235
    .local v3, "indexPos":I
    :goto_1
    iget-object v8, p0, Lorg/apache/lucene/index/TermInfosReader;->index:Lorg/apache/lucene/index/TermInfosReaderIndex;

    invoke-virtual {v8, v2, v3}, Lorg/apache/lucene/index/TermInfosReaderIndex;->seekEnum(Lorg/apache/lucene/index/SegmentTermEnum;I)V

    .line 236
    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/SegmentTermEnum;->scanTo(Lorg/apache/lucene/index/Term;)I

    .line 238
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v8

    if-eqz v8, :cond_d

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v8

    invoke-virtual {p1, v8}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v8

    if-nez v8, :cond_d

    .line 239
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentTermEnum;->termInfo()Lorg/apache/lucene/index/TermInfo;

    move-result-object v6

    .line 240
    .restart local v6    # "ti":Lorg/apache/lucene/index/TermInfo;
    if-nez v7, :cond_b

    .line 241
    iget-object v8, p0, Lorg/apache/lucene/index/TermInfosReader;->termsCache:Lorg/apache/lucene/util/DoubleBarrelLRUCache;

    new-instance v9, Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;

    iget-wide v10, v2, Lorg/apache/lucene/index/SegmentTermEnum;->position:J

    invoke-direct {v9, v6, v10, v11}, Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;-><init>(Lorg/apache/lucene/index/TermInfo;J)V

    invoke-virtual {v8, v0, v9}, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->put(Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 232
    .end local v3    # "indexPos":I
    .end local v6    # "ti":Lorg/apache/lucene/index/TermInfo;
    :cond_a
    iget-object v8, p0, Lorg/apache/lucene/index/TermInfosReader;->index:Lorg/apache/lucene/index/TermInfosReaderIndex;

    invoke-virtual {v8, p1, p3}, Lorg/apache/lucene/index/TermInfosReaderIndex;->getIndexOffset(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/util/BytesRef;)I

    move-result v3

    .restart local v3    # "indexPos":I
    goto :goto_1

    .line 243
    .restart local v6    # "ti":Lorg/apache/lucene/index/TermInfo;
    :cond_b
    sget-boolean v8, Lorg/apache/lucene/index/TermInfosReader;->$assertionsDisabled:Z

    if-nez v8, :cond_c

    invoke-direct {p0, v6, v7, v2}, Lorg/apache/lucene/index/TermInfosReader;->sameTermInfo(Lorg/apache/lucene/index/TermInfo;Lorg/apache/lucene/index/TermInfo;Lorg/apache/lucene/index/SegmentTermEnum;)Z

    move-result v8

    if-nez v8, :cond_c

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 244
    :cond_c
    sget-boolean v8, Lorg/apache/lucene/index/TermInfosReader;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    iget-wide v8, v2, Lorg/apache/lucene/index/SegmentTermEnum;->position:J

    iget-wide v10, v7, Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;->termOrd:J

    cmp-long v8, v8, v10

    if-eqz v8, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 247
    .end local v6    # "ti":Lorg/apache/lucene/index/TermInfo;
    :cond_d
    const/4 v6, 0x0

    .restart local v6    # "ti":Lorg/apache/lucene/index/TermInfo;
    goto/16 :goto_0
.end method

.method private getThreadResources()Lorg/apache/lucene/index/TermInfosReader$ThreadResources;
    .locals 2

    .prologue
    .line 160
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosReader;->threadResources:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v1}, Lorg/apache/lucene/util/CloseableThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/TermInfosReader$ThreadResources;

    .line 161
    .local v0, "resources":Lorg/apache/lucene/index/TermInfosReader$ThreadResources;
    if-nez v0, :cond_0

    .line 162
    new-instance v0, Lorg/apache/lucene/index/TermInfosReader$ThreadResources;

    .end local v0    # "resources":Lorg/apache/lucene/index/TermInfosReader$ThreadResources;
    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TermInfosReader$ThreadResources;-><init>(Lorg/apache/lucene/index/TermInfosReader$1;)V

    .line 163
    .restart local v0    # "resources":Lorg/apache/lucene/index/TermInfosReader$ThreadResources;
    invoke-virtual {p0}, Lorg/apache/lucene/index/TermInfosReader;->terms()Lorg/apache/lucene/index/SegmentTermEnum;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/index/TermInfosReader$ThreadResources;->termEnum:Lorg/apache/lucene/index/SegmentTermEnum;

    .line 164
    iget-object v1, p0, Lorg/apache/lucene/index/TermInfosReader;->threadResources:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/util/CloseableThreadLocal;->set(Ljava/lang/Object;)V

    .line 166
    :cond_0
    return-object v0
.end method

.method private final sameTermInfo(Lorg/apache/lucene/index/TermInfo;Lorg/apache/lucene/index/TermInfo;Lorg/apache/lucene/index/SegmentTermEnum;)Z
    .locals 6
    .param p1, "ti1"    # Lorg/apache/lucene/index/TermInfo;
    .param p2, "ti2"    # Lorg/apache/lucene/index/TermInfo;
    .param p3, "enumerator"    # Lorg/apache/lucene/index/SegmentTermEnum;

    .prologue
    const/4 v0, 0x0

    .line 254
    iget v1, p1, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    iget v2, p2, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    if-eq v1, v2, :cond_1

    .line 268
    :cond_0
    :goto_0
    return v0

    .line 257
    :cond_1
    iget-wide v2, p1, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    iget-wide v4, p2, Lorg/apache/lucene/index/TermInfo;->freqPointer:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 260
    iget-wide v2, p1, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    iget-wide v4, p2, Lorg/apache/lucene/index/TermInfo;->proxPointer:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 264
    iget v1, p1, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    iget v2, p3, Lorg/apache/lucene/index/SegmentTermEnum;->skipInterval:I

    if-lt v1, v2, :cond_2

    iget v1, p1, Lorg/apache/lucene/index/TermInfo;->skipOffset:I

    iget v2, p2, Lorg/apache/lucene/index/TermInfo;->skipOffset:I

    if-ne v1, v2, :cond_0

    .line 268
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosReader;->origEnum:Lorg/apache/lucene/index/SegmentTermEnum;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosReader;->origEnum:Lorg/apache/lucene/index/SegmentTermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentTermEnum;->close()V

    .line 151
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosReader;->threadResources:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v0}, Lorg/apache/lucene/util/CloseableThreadLocal;->close()V

    .line 152
    return-void
.end method

.method get(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermInfo;
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 171
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p1, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    .line 172
    .local v0, "termBytesRef":Lorg/apache/lucene/util/BytesRef;
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lorg/apache/lucene/index/TermInfosReader;->get(Lorg/apache/lucene/index/Term;ZLorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermInfo;

    move-result-object v1

    return-object v1
.end method

.method public getMaxSkipLevels()I
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosReader;->origEnum:Lorg/apache/lucene/index/SegmentTermEnum;

    iget v0, v0, Lorg/apache/lucene/index/SegmentTermEnum;->maxSkipLevels:I

    return v0
.end method

.method final getPosition(Lorg/apache/lucene/index/Term;)J
    .locals 10
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    .line 279
    iget-wide v6, p0, Lorg/apache/lucene/index/TermInfosReader;->size:J

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-nez v3, :cond_1

    .line 293
    :cond_0
    :goto_0
    return-wide v4

    .line 281
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/index/TermInfosReader;->ensureIndexIsRead()V

    .line 282
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    iget-object v3, p1, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    .line 283
    .local v2, "termBytesRef":Lorg/apache/lucene/util/BytesRef;
    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosReader;->index:Lorg/apache/lucene/index/TermInfosReaderIndex;

    invoke-virtual {v3, p1, v2}, Lorg/apache/lucene/index/TermInfosReaderIndex;->getIndexOffset(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/util/BytesRef;)I

    move-result v1

    .line 285
    .local v1, "indexOffset":I
    invoke-direct {p0}, Lorg/apache/lucene/index/TermInfosReader;->getThreadResources()Lorg/apache/lucene/index/TermInfosReader$ThreadResources;

    move-result-object v3

    iget-object v0, v3, Lorg/apache/lucene/index/TermInfosReader$ThreadResources;->termEnum:Lorg/apache/lucene/index/SegmentTermEnum;

    .line 286
    .local v0, "enumerator":Lorg/apache/lucene/index/SegmentTermEnum;
    iget-object v3, p0, Lorg/apache/lucene/index/TermInfosReader;->index:Lorg/apache/lucene/index/TermInfosReaderIndex;

    invoke-virtual {v3, v0, v1}, Lorg/apache/lucene/index/TermInfosReaderIndex;->seekEnum(Lorg/apache/lucene/index/SegmentTermEnum;I)V

    .line 288
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v3

    if-lez v3, :cond_3

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentTermEnum;->next()Z

    move-result v3

    if-nez v3, :cond_2

    .line 290
    :cond_3
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v3

    if-nez v3, :cond_0

    .line 291
    iget-wide v4, v0, Lorg/apache/lucene/index/SegmentTermEnum;->position:J

    goto :goto_0
.end method

.method public getSkipInterval()I
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosReader;->origEnum:Lorg/apache/lucene/index/SegmentTermEnum;

    iget v0, v0, Lorg/apache/lucene/index/SegmentTermEnum;->skipInterval:I

    return v0
.end method

.method final size()J
    .locals 2

    .prologue
    .line 156
    iget-wide v0, p0, Lorg/apache/lucene/index/TermInfosReader;->size:J

    return-wide v0
.end method

.method public terms()Lorg/apache/lucene/index/SegmentTermEnum;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lorg/apache/lucene/index/TermInfosReader;->origEnum:Lorg/apache/lucene/index/SegmentTermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentTermEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentTermEnum;

    return-object v0
.end method

.method public terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/SegmentTermEnum;
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 303
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p1, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    .line 304
    .local v0, "termBytesRef":Lorg/apache/lucene/util/BytesRef;
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1, v0}, Lorg/apache/lucene/index/TermInfosReader;->get(Lorg/apache/lucene/index/Term;ZLorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermInfo;

    .line 305
    invoke-direct {p0}, Lorg/apache/lucene/index/TermInfosReader;->getThreadResources()Lorg/apache/lucene/index/TermInfosReader$ThreadResources;

    move-result-object v1

    iget-object v1, v1, Lorg/apache/lucene/index/TermInfosReader$ThreadResources;->termEnum:Lorg/apache/lucene/index/SegmentTermEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentTermEnum;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentTermEnum;

    return-object v1
.end method
