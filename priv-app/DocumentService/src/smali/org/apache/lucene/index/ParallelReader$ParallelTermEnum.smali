.class Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;
.super Lorg/apache/lucene/index/TermEnum;
.source "ParallelReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/ParallelReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ParallelTermEnum"
.end annotation


# instance fields
.field private field:Ljava/lang/String;

.field private fieldIterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private termEnum:Lorg/apache/lucene/index/TermEnum;

.field final synthetic this$0:Lorg/apache/lucene/index/ParallelReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/ParallelReader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 503
    iput-object p1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->this$0:Lorg/apache/lucene/index/ParallelReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/TermEnum;-><init>()V

    .line 505
    :try_start_0
    # getter for: Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;
    invoke-static {p1}, Lorg/apache/lucene/index/ParallelReader;->access$000(Lorg/apache/lucene/index/ParallelReader;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->field:Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 510
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->field:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 511
    # getter for: Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;
    invoke-static {p1}, Lorg/apache/lucene/index/ParallelReader;->access$000(Lorg/apache/lucene/index/ParallelReader;)Ljava/util/SortedMap;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->field:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->terms()Lorg/apache/lucene/index/TermEnum;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    .line 512
    :cond_0
    :goto_0
    return-void

    .line 506
    :catch_0
    move-exception v0

    .line 508
    .local v0, "e":Ljava/util/NoSuchElementException;
    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/ParallelReader;Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 514
    iput-object p1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->this$0:Lorg/apache/lucene/index/ParallelReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/TermEnum;-><init>()V

    .line 515
    invoke-virtual {p2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->field:Ljava/lang/String;

    .line 516
    # getter for: Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;
    invoke-static {p1}, Lorg/apache/lucene/index/ParallelReader;->access$000(Lorg/apache/lucene/index/ParallelReader;)Ljava/util/SortedMap;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->field:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;

    .line 517
    .local v0, "reader":Lorg/apache/lucene/index/IndexReader;
    if-eqz v0, :cond_0

    .line 518
    invoke-virtual {v0, p2}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    .line 519
    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 568
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    if-eqz v0, :cond_0

    .line 569
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 570
    :cond_0
    return-void
.end method

.method public docFreq()I
    .locals 1

    .prologue
    .line 560
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    if-nez v0, :cond_0

    .line 561
    const/4 v0, 0x0

    .line 563
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermEnum;->docFreq()I

    move-result v0

    goto :goto_0
.end method

.method public next()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 523
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    if-nez v1, :cond_0

    move v1, v2

    .line 547
    :goto_0
    return v1

    .line 527
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermEnum;->next()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->field:Ljava/lang/String;

    if-ne v1, v4, :cond_1

    move v1, v3

    .line 528
    goto :goto_0

    .line 530
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 533
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->fieldIterator:Ljava/util/Iterator;

    if-nez v1, :cond_2

    .line 534
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->this$0:Lorg/apache/lucene/index/ParallelReader;

    # getter for: Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;
    invoke-static {v1}, Lorg/apache/lucene/index/ParallelReader;->access$000(Lorg/apache/lucene/index/ParallelReader;)Ljava/util/SortedMap;

    move-result-object v1

    iget-object v4, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->field:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->fieldIterator:Ljava/util/Iterator;

    .line 535
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->fieldIterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 537
    :cond_2
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->fieldIterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 538
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->fieldIterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->field:Ljava/lang/String;

    .line 539
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->this$0:Lorg/apache/lucene/index/ParallelReader;

    # getter for: Lorg/apache/lucene/index/ParallelReader;->fieldToReader:Ljava/util/SortedMap;
    invoke-static {v1}, Lorg/apache/lucene/index/ParallelReader;->access$000(Lorg/apache/lucene/index/ParallelReader;)Ljava/util/SortedMap;

    move-result-object v1

    iget-object v4, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->field:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexReader;

    new-instance v4, Lorg/apache/lucene/index/Term;

    iget-object v5, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->field:Ljava/lang/String;

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    .line 540
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    .line 541
    .local v0, "term":Lorg/apache/lucene/index/Term;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->field:Ljava/lang/String;

    if-ne v1, v4, :cond_3

    move v1, v3

    .line 542
    goto :goto_0

    .line 544
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermEnum;->close()V

    goto :goto_1

    .end local v0    # "term":Lorg/apache/lucene/index/Term;
    :cond_4
    move v1, v2

    .line 547
    goto/16 :goto_0
.end method

.method public term()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    if-nez v0, :cond_0

    .line 553
    const/4 v0, 0x0

    .line 555
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelReader$ParallelTermEnum;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    goto :goto_0
.end method
