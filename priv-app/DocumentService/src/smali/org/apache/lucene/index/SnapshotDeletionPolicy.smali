.class public Lorg/apache/lucene/index/SnapshotDeletionPolicy;
.super Ljava/lang/Object;
.source "SnapshotDeletionPolicy.java"

# interfaces
.implements Lorg/apache/lucene/index/IndexDeletionPolicy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;,
        Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    }
.end annotation


# instance fields
.field private idToSnapshot:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected lastCommit:Lorg/apache/lucene/index/IndexCommit;

.field private primary:Lorg/apache/lucene/index/IndexDeletionPolicy;

.field private segmentsFileToIDs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexDeletionPolicy;)V
    .locals 1
    .param p1, "primary"    # Lorg/apache/lucene/index/IndexDeletionPolicy;

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    .line 148
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    .line 154
    iput-object p1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->primary:Lorg/apache/lucene/index/IndexDeletionPolicy;

    .line 155
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexDeletionPolicy;Ljava/util/Map;)V
    .locals 5
    .param p1, "primary"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/IndexDeletionPolicy;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 173
    .local p2, "snapshotsInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;-><init>(Lorg/apache/lucene/index/IndexDeletionPolicy;)V

    .line 175
    if-eqz p2, :cond_0

    .line 178
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 179
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v3, v4}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->registerSnapshotInfo(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/index/IndexCommit;)V

    goto :goto_0

    .line 182
    .end local v0    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/index/SnapshotDeletionPolicy;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/SnapshotDeletionPolicy;

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method protected checkSnapshotted(Ljava/lang/String;)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 189
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->isSnapshotted(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Snapshot ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is already used - must be unique"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_0
    return-void
.end method

.method public declared-synchronized getSnapshot(Ljava/lang/String;)Lorg/apache/lucene/index/IndexCommit;
    .locals 4
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;

    .line 228
    .local v0, "snapshotInfo":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    if-nez v0, :cond_0

    .line 229
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "No snapshot exists by ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    .end local v0    # "snapshotInfo":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 231
    .restart local v0    # "snapshotInfo":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    :cond_0
    :try_start_1
    iget-object v1, v0, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->commit:Lorg/apache/lucene/index/IndexCommit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized getSnapshots()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 241
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 242
    .local v2, "snapshots":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 243
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;

    iget-object v3, v3, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->segmentsFileName:Ljava/lang/String;

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 241
    .end local v0    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "snapshots":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 245
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "snapshots":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    monitor-exit p0

    return-object v2
.end method

.method public isSnapshotted(Ljava/lang/String;)Z
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 254
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized onCommit(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 259
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->primary:Lorg/apache/lucene/index/IndexDeletionPolicy;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->wrapCommits(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/apache/lucene/index/IndexDeletionPolicy;->onCommit(Ljava/util/List;)V

    .line 260
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexCommit;

    iput-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    monitor-exit p0

    return-void

    .line 259
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onInit(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 265
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    monitor-enter p0

    :try_start_0
    iget-object v8, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->primary:Lorg/apache/lucene/index/IndexDeletionPolicy;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->wrapCommits(Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/apache/lucene/index/IndexDeletionPolicy;->onInit(Ljava/util/List;)V

    .line 266
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/index/IndexCommit;

    iput-object v8, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;

    .line 272
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexCommit;

    .line 273
    .local v0, "commit":Lorg/apache/lucene/index/IndexCommit;
    iget-object v8, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Set;

    .line 274
    .local v5, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v5, :cond_0

    .line 275
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 276
    .local v4, "id":Ljava/lang/String;
    iget-object v8, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-interface {v8, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;

    iput-object v0, v8, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->commit:Lorg/apache/lucene/index/IndexCommit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 265
    .end local v0    # "commit":Lorg/apache/lucene/index/IndexCommit;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "id":Ljava/lang/String;
    .end local v5    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 291
    :cond_1
    const/4 v6, 0x0

    .line 292
    .local v6, "idsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_1
    iget-object v8, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 293
    .local v1, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;

    iget-object v8, v8, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->commit:Lorg/apache/lucene/index/IndexCommit;

    if-nez v8, :cond_2

    .line 294
    if-nez v6, :cond_3

    .line 295
    new-instance v6, Ljava/util/ArrayList;

    .end local v6    # "idsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 297
    .restart local v6    # "idsToRemove":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 301
    .end local v1    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;>;"
    :cond_4
    if-eqz v6, :cond_5

    .line 302
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 303
    .restart local v4    # "id":Ljava/lang/String;
    iget-object v8, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-interface {v8, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;

    .line 304
    .local v7, "info":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    iget-object v8, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    iget-object v9, v7, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->segmentsFileName:Ljava/lang/String;

    invoke-interface {v8, v9}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 307
    .end local v4    # "id":Ljava/lang/String;
    .end local v7    # "info":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    :cond_5
    monitor-exit p0

    return-void
.end method

.method protected registerSnapshotInfo(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/index/IndexCommit;)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "commit"    # Lorg/apache/lucene/index/IndexCommit;

    .prologue
    .line 197
    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    new-instance v2, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;

    invoke-direct {v2, p1, p2, p3}, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/index/IndexCommit;)V

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 199
    .local v0, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez v0, :cond_0

    .line 200
    new-instance v0, Ljava/util/HashSet;

    .end local v0    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 201
    .restart local v0    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 204
    return-void
.end method

.method public declared-synchronized release(Ljava/lang/String;)V
    .locals 5
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 318
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->idToSnapshot:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;

    .line 319
    .local v1, "info":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    if-nez v1, :cond_0

    .line 320
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Snapshot doesn\'t exist: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318
    .end local v1    # "info":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 322
    .restart local v1    # "info":Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    iget-object v3, v1, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->segmentsFileName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 323
    .local v0, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    .line 324
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 325
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 326
    iget-object v2, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->segmentsFileToIDs:Ljava/util/Map;

    iget-object v3, v1, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotInfo;->segmentsFileName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 329
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized snapshot(Ljava/lang/String;)Lorg/apache/lucene/index/IndexCommit;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 354
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;

    if-nez v0, :cond_0

    .line 357
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "No index commit to snapshot"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 354
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 361
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->checkSnapshotted(Ljava/lang/String;)V

    .line 363
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->registerSnapshotInfo(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/index/IndexCommit;)V

    .line 364
    iget-object v0, p0, Lorg/apache/lucene/index/SnapshotDeletionPolicy;->lastCommit:Lorg/apache/lucene/index/IndexCommit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method protected wrapCommits(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 207
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 208
    .local v2, "wrappedCommits":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexCommit;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexCommit;

    .line 209
    .local v1, "ic":Lorg/apache/lucene/index/IndexCommit;
    new-instance v3, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;

    invoke-direct {v3, p0, v1}, Lorg/apache/lucene/index/SnapshotDeletionPolicy$SnapshotCommitPoint;-><init>(Lorg/apache/lucene/index/SnapshotDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 211
    .end local v1    # "ic":Lorg/apache/lucene/index/IndexCommit;
    :cond_0
    return-object v2
.end method
