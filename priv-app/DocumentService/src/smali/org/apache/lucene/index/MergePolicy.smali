.class public abstract Lorg/apache/lucene/index/MergePolicy;
.super Ljava/lang/Object;
.source "MergePolicy.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;,
        Lorg/apache/lucene/index/MergePolicy$MergeException;,
        Lorg/apache/lucene/index/MergePolicy$MergeSpecification;,
        Lorg/apache/lucene/index/MergePolicy$OneMerge;
    }
.end annotation


# instance fields
.field protected final writer:Lorg/apache/lucene/util/SetOnce;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/SetOnce",
            "<",
            "Lorg/apache/lucene/index/IndexWriter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 258
    new-instance v0, Lorg/apache/lucene/util/SetOnce;

    invoke-direct {v0}, Lorg/apache/lucene/util/SetOnce;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/MergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    .line 259
    return-void
.end method


# virtual methods
.method public abstract close()V
.end method

.method public abstract findForcedDeletesMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract findForcedMerges(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "I",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lorg/apache/lucene/index/MergePolicy$MergeSpecification;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract findMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public setIndexWriter(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;

    .prologue
    .line 269
    iget-object v0, p0, Lorg/apache/lucene/index/MergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/SetOnce;->set(Ljava/lang/Object;)V

    .line 270
    return-void
.end method

.method public abstract useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfo;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
