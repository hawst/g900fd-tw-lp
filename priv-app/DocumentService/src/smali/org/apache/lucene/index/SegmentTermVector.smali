.class Lorg/apache/lucene/index/SegmentTermVector;
.super Ljava/lang/Object;
.source "SegmentTermVector.java"

# interfaces
.implements Lorg/apache/lucene/index/TermFreqVector;


# instance fields
.field private field:Ljava/lang/String;

.field private termFreqs:[I

.field private terms:[Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;[Ljava/lang/String;[I)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "terms"    # [Ljava/lang/String;
    .param p3, "termFreqs"    # [I

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentTermVector;->field:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lorg/apache/lucene/index/SegmentTermVector;->terms:[Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lorg/apache/lucene/index/SegmentTermVector;->termFreqs:[I

    .line 32
    return-void
.end method


# virtual methods
.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermVector;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getTermFrequencies()[I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermVector;->termFreqs:[I

    return-object v0
.end method

.method public getTerms()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermVector;->terms:[Ljava/lang/String;

    return-object v0
.end method

.method public indexOf(Ljava/lang/String;)I
    .locals 3
    .param p1, "termText"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 71
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermVector;->terms:[Ljava/lang/String;

    if-nez v2, :cond_0

    .line 74
    :goto_0
    return v1

    .line 73
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermVector;->terms:[Ljava/lang/String;

    invoke-static {v2, p1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 74
    .local v0, "res":I
    if-ltz v0, :cond_1

    .end local v0    # "res":I
    :goto_1
    move v1, v0

    goto :goto_0

    .restart local v0    # "res":I
    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public indexesOf([Ljava/lang/String;II)[I
    .locals 3
    .param p1, "termNumbers"    # [Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    .line 83
    new-array v1, p3, [I

    .line 85
    .local v1, "res":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_0

    .line 86
    add-int v2, p2, v0

    aget-object v2, p1, v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/SegmentTermVector;->indexOf(Ljava/lang/String;)I

    move-result v2

    aput v2, v1, v0

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 88
    :cond_0
    return-object v1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermVector;->terms:[Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentTermVector;->terms:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/16 v2, 0x7b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 46
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermVector;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermVector;->terms:[Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 48
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermVector;->terms:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 49
    if-lez v0, :cond_0

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentTermVector;->terms:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentTermVector;->termFreqs:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    .end local v0    # "i":I
    :cond_1
    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 55
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
