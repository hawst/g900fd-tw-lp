.class abstract Lorg/apache/lucene/index/InvertedDocConsumerPerField;
.super Ljava/lang/Object;
.source "InvertedDocConsumerPerField.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract abort()V
.end method

.method abstract add()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract finish()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract start(Lorg/apache/lucene/document/Fieldable;)V
.end method

.method abstract start([Lorg/apache/lucene/document/Fieldable;I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
