.class final Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;
.super Lorg/apache/lucene/index/TermInfo;
.source "TermInfosReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/TermInfosReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TermInfoAndOrd"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final termOrd:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lorg/apache/lucene/index/TermInfosReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/TermInfo;J)V
    .locals 2
    .param p1, "ti"    # Lorg/apache/lucene/index/TermInfo;
    .param p2, "termOrd"    # J

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/TermInfo;-><init>(Lorg/apache/lucene/index/TermInfo;)V

    .line 53
    sget-boolean v0, Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 54
    :cond_0
    iput-wide p2, p0, Lorg/apache/lucene/index/TermInfosReader$TermInfoAndOrd;->termOrd:J

    .line 55
    return-void
.end method
