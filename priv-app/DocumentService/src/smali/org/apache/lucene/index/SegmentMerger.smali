.class final Lorg/apache/lucene/index/SegmentMerger;
.super Ljava/lang/Object;
.source "SegmentMerger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/SegmentMerger$CheckAbort;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final MAX_RAW_MERGE_DOCS:I = 0x1060


# instance fields
.field private final checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

.field private directory:Lorg/apache/lucene/store/Directory;

.field private docMaps:[[I

.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

.field private matchedCount:I

.field private matchingSegmentReaders:[Lorg/apache/lucene/index/SegmentReader;

.field private mergedDocs:I

.field private payloadBuffer:[B

.field private final payloadProcessorProvider:Lorg/apache/lucene/index/PayloadProcessorProvider;

.field private queue:Lorg/apache/lucene/index/SegmentMergeQueue;

.field private rawDocLengths:[I

.field private rawDocLengths2:[I

.field private readers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;"
        }
    .end annotation
.end field

.field private segment:Ljava/lang/String;

.field private segmentWriteState:Lorg/apache/lucene/index/SegmentWriteState;

.field private termIndexInterval:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lorg/apache/lucene/index/SegmentMerger;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SegmentMerger;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;ILjava/lang/String;Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/index/PayloadProcessorProvider;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 2
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "termIndexInterval"    # I
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .param p5, "payloadProcessorProvider"    # Lorg/apache/lucene/index/PayloadProcessorProvider;
    .param p6, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/16 v0, 0x80

    iput v0, p0, Lorg/apache/lucene/index/SegmentMerger;->termIndexInterval:I

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    .line 419
    iput-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    .line 64
    iput-object p5, p0, Lorg/apache/lucene/index/SegmentMerger;->payloadProcessorProvider:Lorg/apache/lucene/index/PayloadProcessorProvider;

    .line 65
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentMerger;->directory:Lorg/apache/lucene/store/Directory;

    .line 66
    iput-object p6, p0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 67
    iput-object p3, p0, Lorg/apache/lucene/index/SegmentMerger;->segment:Ljava/lang/String;

    .line 68
    if-eqz p4, :cond_0

    .line 69
    new-instance v0, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->directory:Lorg/apache/lucene/store/Directory;

    invoke-direct {v0, p4, v1}, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;-><init>(Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/store/Directory;)V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentMerger;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    .line 78
    :goto_0
    iput p2, p0, Lorg/apache/lucene/index/SegmentMerger;->termIndexInterval:I

    .line 79
    return-void

    .line 71
    :cond_0
    new-instance v0, Lorg/apache/lucene/index/SegmentMerger$1;

    invoke-direct {v0, p0, v1, v1}, Lorg/apache/lucene/index/SegmentMerger$1;-><init>(Lorg/apache/lucene/index/SegmentMerger;Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/store/Directory;)V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentMerger;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    goto :goto_0
.end method

.method private final appendPostings(Lorg/apache/lucene/index/FormatPostingsTermsConsumer;[Lorg/apache/lucene/index/SegmentMergeInfo;I)I
    .locals 17
    .param p1, "termsConsumer"    # Lorg/apache/lucene/index/FormatPostingsTermsConsumer;
    .param p2, "smis"    # [Lorg/apache/lucene/index/SegmentMergeInfo;
    .param p3, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 525
    const/4 v15, 0x0

    aget-object v15, p2, v15

    iget-object v15, v15, Lorg/apache/lucene/index/SegmentMergeInfo;->term:Lorg/apache/lucene/index/Term;

    iget-object v15, v15, Lorg/apache/lucene/index/Term;->text:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/apache/lucene/index/FormatPostingsTermsConsumer;->addTerm(Ljava/lang/String;)Lorg/apache/lucene/index/FormatPostingsDocsConsumer;

    move-result-object v4

    .line 526
    .local v4, "docConsumer":Lorg/apache/lucene/index/FormatPostingsDocsConsumer;
    const/4 v2, 0x0

    .line 527
    .local v2, "df":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    move/from16 v0, p3

    if-ge v7, v0, :cond_8

    .line 528
    aget-object v14, p2, v7

    .line 529
    .local v14, "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    invoke-virtual {v14}, Lorg/apache/lucene/index/SegmentMergeInfo;->getPositions()Lorg/apache/lucene/index/TermPositions;

    move-result-object v13

    .line 530
    .local v13, "postings":Lorg/apache/lucene/index/TermPositions;
    sget-boolean v15, Lorg/apache/lucene/index/SegmentMerger;->$assertionsDisabled:Z

    if-nez v15, :cond_0

    if-nez v13, :cond_0

    new-instance v15, Ljava/lang/AssertionError;

    invoke-direct {v15}, Ljava/lang/AssertionError;-><init>()V

    throw v15

    .line 531
    :cond_0
    iget v1, v14, Lorg/apache/lucene/index/SegmentMergeInfo;->base:I

    .line 532
    .local v1, "base":I
    invoke-virtual {v14}, Lorg/apache/lucene/index/SegmentMergeInfo;->getDocMap()[I

    move-result-object v5

    .line 533
    .local v5, "docMap":[I
    iget-object v15, v14, Lorg/apache/lucene/index/SegmentMergeInfo;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-interface {v13, v15}, Lorg/apache/lucene/index/TermPositions;->seek(Lorg/apache/lucene/index/TermEnum;)V

    .line 535
    const/4 v10, 0x0

    .line 536
    .local v10, "payloadProcessor":Lorg/apache/lucene/index/PayloadProcessorProvider$PayloadProcessor;
    iget-object v15, v14, Lorg/apache/lucene/index/SegmentMergeInfo;->readerPayloadProcessor:Lorg/apache/lucene/index/PayloadProcessorProvider$ReaderPayloadProcessor;

    if-eqz v15, :cond_1

    .line 537
    iget-object v15, v14, Lorg/apache/lucene/index/SegmentMergeInfo;->readerPayloadProcessor:Lorg/apache/lucene/index/PayloadProcessorProvider$ReaderPayloadProcessor;

    iget-object v0, v14, Lorg/apache/lucene/index/SegmentMergeInfo;->term:Lorg/apache/lucene/index/Term;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lorg/apache/lucene/index/PayloadProcessorProvider$ReaderPayloadProcessor;->getProcessor(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/PayloadProcessorProvider$PayloadProcessor;

    move-result-object v10

    .line 540
    :cond_1
    :goto_1
    invoke-interface {v13}, Lorg/apache/lucene/index/TermPositions;->next()Z

    move-result v15

    if-eqz v15, :cond_7

    .line 541
    add-int/lit8 v2, v2, 0x1

    .line 542
    invoke-interface {v13}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v3

    .line 543
    .local v3, "doc":I
    if-eqz v5, :cond_2

    .line 544
    aget v3, v5, v3

    .line 545
    :cond_2
    add-int/2addr v3, v1

    .line 547
    invoke-interface {v13}, Lorg/apache/lucene/index/TermPositions;->freq()I

    move-result v6

    .line 548
    .local v6, "freq":I
    invoke-virtual {v4, v3, v6}, Lorg/apache/lucene/index/FormatPostingsDocsConsumer;->addDoc(II)Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;

    move-result-object v11

    .line 550
    .local v11, "posConsumer":Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/SegmentMerger;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v16, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v0, v16

    if-ne v15, v0, :cond_1

    .line 551
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_2
    if-ge v8, v6, :cond_6

    .line 552
    invoke-interface {v13}, Lorg/apache/lucene/index/TermPositions;->nextPosition()I

    move-result v12

    .line 553
    .local v12, "position":I
    invoke-interface {v13}, Lorg/apache/lucene/index/TermPositions;->getPayloadLength()I

    move-result v9

    .line 554
    .local v9, "payloadLength":I
    if-lez v9, :cond_5

    .line 555
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/SegmentMerger;->payloadBuffer:[B

    if-eqz v15, :cond_3

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/SegmentMerger;->payloadBuffer:[B

    array-length v15, v15

    if-ge v15, v9, :cond_4

    .line 556
    :cond_3
    new-array v15, v9, [B

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/lucene/index/SegmentMerger;->payloadBuffer:[B

    .line 557
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/SegmentMerger;->payloadBuffer:[B

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v13, v15, v0}, Lorg/apache/lucene/index/TermPositions;->getPayload([BI)[B

    .line 558
    if-eqz v10, :cond_5

    .line 559
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/SegmentMerger;->payloadBuffer:[B

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v10, v15, v0, v9}, Lorg/apache/lucene/index/PayloadProcessorProvider$PayloadProcessor;->processPayload([BII)[B

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/lucene/index/SegmentMerger;->payloadBuffer:[B

    .line 560
    invoke-virtual {v10}, Lorg/apache/lucene/index/PayloadProcessorProvider$PayloadProcessor;->payloadLength()I

    move-result v9

    .line 563
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/SegmentMerger;->payloadBuffer:[B

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v11, v12, v15, v0, v9}, Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;->addPosition(I[BII)V

    .line 551
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 565
    .end local v9    # "payloadLength":I
    .end local v12    # "position":I
    :cond_6
    invoke-virtual {v11}, Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;->finish()V

    goto :goto_1

    .line 527
    .end local v3    # "doc":I
    .end local v6    # "freq":I
    .end local v8    # "j":I
    .end local v11    # "posConsumer":Lorg/apache/lucene/index/FormatPostingsPositionsConsumer;
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 569
    .end local v1    # "base":I
    .end local v5    # "docMap":[I
    .end local v10    # "payloadProcessor":Lorg/apache/lucene/index/PayloadProcessorProvider$PayloadProcessor;
    .end local v13    # "postings":Lorg/apache/lucene/index/TermPositions;
    .end local v14    # "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    :cond_8
    invoke-virtual {v4}, Lorg/apache/lucene/index/FormatPostingsDocsConsumer;->finish()V

    .line 571
    return v2
.end method

.method private copyFieldsNoDeletions(Lorg/apache/lucene/index/FieldsWriter;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/FieldsReader;)I
    .locals 8
    .param p1, "fieldsWriter"    # Lorg/apache/lucene/index/FieldsWriter;
    .param p2, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "matchingFieldsReader"    # Lorg/apache/lucene/index/FieldsReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;,
            Lorg/apache/lucene/index/CorruptIndexException;
        }
    .end annotation

    .prologue
    .line 290
    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v3

    .line 291
    .local v3, "maxDoc":I
    const/4 v1, 0x0

    .line 292
    .local v1, "docCount":I
    if-eqz p3, :cond_0

    .line 294
    :goto_0
    if-ge v1, v3, :cond_1

    .line 295
    const/16 v5, 0x1060

    sub-int v6, v3, v1

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 296
    .local v2, "len":I
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->rawDocLengths:[I

    invoke-virtual {p3, v5, v1, v2}, Lorg/apache/lucene/index/FieldsReader;->rawDocs([III)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    .line 297
    .local v4, "stream":Lorg/apache/lucene/store/IndexInput;
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->rawDocLengths:[I

    invoke-virtual {p1, v4, v5, v2}, Lorg/apache/lucene/index/FieldsWriter;->addRawDocuments(Lorg/apache/lucene/store/IndexInput;[II)V

    .line 298
    add-int/2addr v1, v2

    .line 299
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    mul-int/lit16 v6, v2, 0x12c

    int-to-double v6, v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->work(D)V

    goto :goto_0

    .line 302
    .end local v2    # "len":I
    .end local v4    # "stream":Lorg/apache/lucene/store/IndexInput;
    :cond_0
    :goto_1
    if-ge v1, v3, :cond_1

    .line 305
    invoke-virtual {p2, v1}, Lorg/apache/lucene/index/IndexReader;->document(I)Lorg/apache/lucene/document/Document;

    move-result-object v0

    .line 306
    .local v0, "doc":Lorg/apache/lucene/document/Document;
    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/FieldsWriter;->addDocument(Lorg/apache/lucene/document/Document;)V

    .line 307
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    const-wide v6, 0x4072c00000000000L    # 300.0

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->work(D)V

    .line 302
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 310
    .end local v0    # "doc":Lorg/apache/lucene/document/Document;
    :cond_1
    return v1
.end method

.method private copyFieldsWithDeletions(Lorg/apache/lucene/index/FieldsWriter;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/FieldsReader;)I
    .locals 10
    .param p1, "fieldsWriter"    # Lorg/apache/lucene/index/FieldsWriter;
    .param p2, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "matchingFieldsReader"    # Lorg/apache/lucene/index/FieldsReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;,
            Lorg/apache/lucene/index/CorruptIndexException;
        }
    .end annotation

    .prologue
    .line 242
    const/4 v1, 0x0

    .line 243
    .local v1, "docCount":I
    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v3

    .line 244
    .local v3, "maxDoc":I
    if-eqz p3, :cond_4

    .line 246
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    if-ge v2, v3, :cond_6

    .line 247
    invoke-virtual {p2, v2}, Lorg/apache/lucene/index/IndexReader;->isDeleted(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 249
    add-int/lit8 v2, v2, 0x1

    .line 250
    goto :goto_0

    .line 254
    :cond_0
    move v5, v2

    .local v5, "start":I
    const/4 v4, 0x0

    .line 256
    .local v4, "numDocs":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 257
    add-int/lit8 v4, v4, 0x1

    .line 258
    if-lt v2, v3, :cond_2

    .line 265
    :goto_1
    iget-object v7, p0, Lorg/apache/lucene/index/SegmentMerger;->rawDocLengths:[I

    invoke-virtual {p3, v7, v5, v4}, Lorg/apache/lucene/index/FieldsReader;->rawDocs([III)Lorg/apache/lucene/store/IndexInput;

    move-result-object v6

    .line 266
    .local v6, "stream":Lorg/apache/lucene/store/IndexInput;
    iget-object v7, p0, Lorg/apache/lucene/index/SegmentMerger;->rawDocLengths:[I

    invoke-virtual {p1, v6, v7, v4}, Lorg/apache/lucene/index/FieldsWriter;->addRawDocuments(Lorg/apache/lucene/store/IndexInput;[II)V

    .line 267
    add-int/2addr v1, v4

    .line 268
    iget-object v7, p0, Lorg/apache/lucene/index/SegmentMerger;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    mul-int/lit16 v8, v4, 0x12c

    int-to-double v8, v8

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->work(D)V

    goto :goto_0

    .line 259
    .end local v6    # "stream":Lorg/apache/lucene/store/IndexInput;
    :cond_2
    invoke-virtual {p2, v2}, Lorg/apache/lucene/index/IndexReader;->isDeleted(I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 260
    add-int/lit8 v2, v2, 0x1

    .line 261
    goto :goto_1

    .line 263
    :cond_3
    const/16 v7, 0x1060

    if-lt v4, v7, :cond_1

    goto :goto_1

    .line 271
    .end local v2    # "j":I
    .end local v4    # "numDocs":I
    .end local v5    # "start":I
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "j":I
    :goto_2
    if-ge v2, v3, :cond_6

    .line 272
    invoke-virtual {p2, v2}, Lorg/apache/lucene/index/IndexReader;->isDeleted(I)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 271
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 278
    :cond_5
    invoke-virtual {p2, v2}, Lorg/apache/lucene/index/IndexReader;->document(I)Lorg/apache/lucene/document/Document;

    move-result-object v0

    .line 279
    .local v0, "doc":Lorg/apache/lucene/document/Document;
    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/FieldsWriter;->addDocument(Lorg/apache/lucene/document/Document;)V

    .line 280
    add-int/lit8 v1, v1, 0x1

    .line 281
    iget-object v7, p0, Lorg/apache/lucene/index/SegmentMerger;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    const-wide v8, 0x4072c00000000000L    # 300.0

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->work(D)V

    goto :goto_3

    .line 284
    .end local v0    # "doc":Lorg/apache/lucene/document/Document;
    :cond_6
    return v1
.end method

.method private copyVectorsNoDeletions(Lorg/apache/lucene/index/TermVectorsWriter;Lorg/apache/lucene/index/TermVectorsReader;Lorg/apache/lucene/index/IndexReader;)V
    .locals 8
    .param p1, "termVectorsWriter"    # Lorg/apache/lucene/index/TermVectorsWriter;
    .param p2, "matchingVectorsReader"    # Lorg/apache/lucene/index/TermVectorsReader;
    .param p3, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;
        }
    .end annotation

    .prologue
    .line 397
    invoke-virtual {p3}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v3

    .line 398
    .local v3, "maxDoc":I
    if-eqz p2, :cond_0

    .line 400
    const/4 v0, 0x0

    .line 401
    .local v0, "docCount":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 402
    const/16 v5, 0x1060

    sub-int v6, v3, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 403
    .local v2, "len":I
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->rawDocLengths:[I

    iget-object v6, p0, Lorg/apache/lucene/index/SegmentMerger;->rawDocLengths2:[I

    invoke-virtual {p2, v5, v6, v0, v2}, Lorg/apache/lucene/index/TermVectorsReader;->rawDocs([I[III)V

    .line 404
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->rawDocLengths:[I

    iget-object v6, p0, Lorg/apache/lucene/index/SegmentMerger;->rawDocLengths2:[I

    invoke-virtual {p1, p2, v5, v6, v2}, Lorg/apache/lucene/index/TermVectorsWriter;->addRawDocuments(Lorg/apache/lucene/index/TermVectorsReader;[I[II)V

    .line 405
    add-int/2addr v0, v2

    .line 406
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    mul-int/lit16 v6, v2, 0x12c

    int-to-double v6, v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->work(D)V

    goto :goto_0

    .line 409
    .end local v0    # "docCount":I
    .end local v2    # "len":I
    :cond_0
    const/4 v1, 0x0

    .local v1, "docNum":I
    :goto_1
    if-ge v1, v3, :cond_1

    .line 412
    invoke-virtual {p3, v1}, Lorg/apache/lucene/index/IndexReader;->getTermFreqVectors(I)[Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v4

    .line 413
    .local v4, "vectors":[Lorg/apache/lucene/index/TermFreqVector;
    invoke-virtual {p1, v4}, Lorg/apache/lucene/index/TermVectorsWriter;->addAllDocVectors([Lorg/apache/lucene/index/TermFreqVector;)V

    .line 414
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    const-wide v6, 0x4072c00000000000L    # 300.0

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->work(D)V

    .line 409
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 417
    .end local v1    # "docNum":I
    .end local v4    # "vectors":[Lorg/apache/lucene/index/TermFreqVector;
    :cond_1
    return-void
.end method

.method private copyVectorsWithDeletions(Lorg/apache/lucene/index/TermVectorsWriter;Lorg/apache/lucene/index/TermVectorsReader;Lorg/apache/lucene/index/IndexReader;)V
    .locals 8
    .param p1, "termVectorsWriter"    # Lorg/apache/lucene/index/TermVectorsWriter;
    .param p2, "matchingVectorsReader"    # Lorg/apache/lucene/index/TermVectorsReader;
    .param p3, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;
        }
    .end annotation

    .prologue
    .line 351
    invoke-virtual {p3}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v1

    .line 352
    .local v1, "maxDoc":I
    if-eqz p2, :cond_4

    .line 354
    const/4 v0, 0x0

    .local v0, "docNum":I
    :goto_0
    if-ge v0, v1, :cond_6

    .line 355
    invoke-virtual {p3, v0}, Lorg/apache/lucene/index/IndexReader;->isDeleted(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 357
    add-int/lit8 v0, v0, 0x1

    .line 358
    goto :goto_0

    .line 362
    :cond_0
    move v3, v0

    .local v3, "start":I
    const/4 v2, 0x0

    .line 364
    .local v2, "numDocs":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 365
    add-int/lit8 v2, v2, 0x1

    .line 366
    if-lt v0, v1, :cond_2

    .line 373
    :goto_1
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->rawDocLengths:[I

    iget-object v6, p0, Lorg/apache/lucene/index/SegmentMerger;->rawDocLengths2:[I

    invoke-virtual {p2, v5, v6, v3, v2}, Lorg/apache/lucene/index/TermVectorsReader;->rawDocs([I[III)V

    .line 374
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->rawDocLengths:[I

    iget-object v6, p0, Lorg/apache/lucene/index/SegmentMerger;->rawDocLengths2:[I

    invoke-virtual {p1, p2, v5, v6, v2}, Lorg/apache/lucene/index/TermVectorsWriter;->addRawDocuments(Lorg/apache/lucene/index/TermVectorsReader;[I[II)V

    .line 375
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    mul-int/lit16 v6, v2, 0x12c

    int-to-double v6, v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->work(D)V

    goto :goto_0

    .line 367
    :cond_2
    invoke-virtual {p3, v0}, Lorg/apache/lucene/index/IndexReader;->isDeleted(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 368
    add-int/lit8 v0, v0, 0x1

    .line 369
    goto :goto_1

    .line 371
    :cond_3
    const/16 v5, 0x1060

    if-lt v2, v5, :cond_1

    goto :goto_1

    .line 378
    .end local v0    # "docNum":I
    .end local v2    # "numDocs":I
    .end local v3    # "start":I
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "docNum":I
    :goto_2
    if-ge v0, v1, :cond_6

    .line 379
    invoke-virtual {p3, v0}, Lorg/apache/lucene/index/IndexReader;->isDeleted(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 378
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 386
    :cond_5
    invoke-virtual {p3, v0}, Lorg/apache/lucene/index/IndexReader;->getTermFreqVectors(I)[Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v4

    .line 387
    .local v4, "vectors":[Lorg/apache/lucene/index/TermFreqVector;
    invoke-virtual {p1, v4}, Lorg/apache/lucene/index/TermVectorsWriter;->addAllDocVectors([Lorg/apache/lucene/index/TermFreqVector;)V

    .line 388
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    const-wide v6, 0x4072c00000000000L    # 300.0

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->work(D)V

    goto :goto_3

    .line 391
    .end local v4    # "vectors":[Lorg/apache/lucene/index/TermFreqVector;
    :cond_6
    return-void
.end method

.method private mergeFields()I
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 200
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/index/IndexReader;

    .line 201
    .local v17, "reader":Lorg/apache/lucene/index/IndexReader;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/index/IndexReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/FieldInfos;->add(Lorg/apache/lucene/index/FieldInfos;)V

    goto :goto_0

    .line 203
    .end local v17    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentMerger;->directory:Lorg/apache/lucene/store/Directory;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/SegmentMerger;->segment:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".fnm"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/index/FieldInfos;->write(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    .line 205
    const/4 v7, 0x0

    .line 207
    .local v7, "docCount":I
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentMerger;->setMatchingSegmentReaders()V

    .line 209
    new-instance v11, Lorg/apache/lucene/index/FieldsWriter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/SegmentMerger;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentMerger;->segment:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-direct {v11, v2, v3, v4}, Lorg/apache/lucene/index/FieldsWriter;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;)V

    .line 212
    .local v11, "fieldsWriter":Lorg/apache/lucene/index/FieldsWriter;
    const/4 v13, 0x0

    .line 213
    .local v13, "idx":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v12

    move v14, v13

    .end local v13    # "idx":I
    .local v14, "idx":I
    :goto_1
    :try_start_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/index/IndexReader;

    .line 214
    .restart local v17    # "reader":Lorg/apache/lucene/index/IndexReader;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/SegmentMerger;->matchingSegmentReaders:[Lorg/apache/lucene/index/SegmentReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    add-int/lit8 v13, v14, 0x1

    .end local v14    # "idx":I
    .restart local v13    # "idx":I
    :try_start_2
    aget-object v16, v2, v14

    .line 215
    .local v16, "matchingSegmentReader":Lorg/apache/lucene/index/SegmentReader;
    const/4 v15, 0x0

    .line 216
    .local v15, "matchingFieldsReader":Lorg/apache/lucene/index/FieldsReader;
    if-eqz v16, :cond_1

    .line 217
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/index/SegmentReader;->getFieldsReader()Lorg/apache/lucene/index/FieldsReader;

    move-result-object v10

    .line 218
    .local v10, "fieldsReader":Lorg/apache/lucene/index/FieldsReader;
    if-eqz v10, :cond_1

    invoke-virtual {v10}, Lorg/apache/lucene/index/FieldsReader;->canReadRawDocs()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 219
    move-object v15, v10

    .line 222
    .end local v10    # "fieldsReader":Lorg/apache/lucene/index/FieldsReader;
    :cond_1
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 223
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v11, v1, v15}, Lorg/apache/lucene/index/SegmentMerger;->copyFieldsWithDeletions(Lorg/apache/lucene/index/FieldsWriter;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/FieldsReader;)I

    move-result v2

    add-int/2addr v7, v2

    :goto_2
    move v14, v13

    .line 229
    .end local v13    # "idx":I
    .restart local v14    # "idx":I
    goto :goto_1

    .line 226
    .end local v14    # "idx":I
    .restart local v13    # "idx":I
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v11, v1, v15}, Lorg/apache/lucene/index/SegmentMerger;->copyFieldsNoDeletions(Lorg/apache/lucene/index/FieldsWriter;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/FieldsReader;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    add-int/2addr v7, v2

    goto :goto_2

    .line 230
    .end local v13    # "idx":I
    .end local v15    # "matchingFieldsReader":Lorg/apache/lucene/index/FieldsReader;
    .end local v16    # "matchingSegmentReader":Lorg/apache/lucene/index/SegmentReader;
    .end local v17    # "reader":Lorg/apache/lucene/index/IndexReader;
    .restart local v14    # "idx":I
    :cond_3
    :try_start_3
    invoke-virtual {v11, v7}, Lorg/apache/lucene/index/FieldsWriter;->finish(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 232
    invoke-virtual {v11}, Lorg/apache/lucene/index/FieldsWriter;->close()V

    .line 235
    new-instance v2, Lorg/apache/lucene/index/SegmentWriteState;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/SegmentMerger;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/SegmentMerger;->segment:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/lucene/index/SegmentMerger;->termIndexInterval:I

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lorg/apache/lucene/index/SegmentWriteState;-><init>(Ljava/io/PrintStream;Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;IILorg/apache/lucene/index/BufferedDeletes;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/lucene/index/SegmentMerger;->segmentWriteState:Lorg/apache/lucene/index/SegmentWriteState;

    .line 236
    return v7

    .line 232
    .end local v14    # "idx":I
    .restart local v13    # "idx":I
    :catchall_0
    move-exception v2

    :goto_3
    invoke-virtual {v11}, Lorg/apache/lucene/index/FieldsWriter;->close()V

    throw v2

    .end local v13    # "idx":I
    .restart local v14    # "idx":I
    :catchall_1
    move-exception v2

    move v13, v14

    .end local v14    # "idx":I
    .restart local v13    # "idx":I
    goto :goto_3
.end method

.method private mergeNorms()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 581
    const/4 v0, 0x0

    .line 582
    .local v0, "bufferSize":I
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/index/IndexReader;

    .line 583
    .local v9, "reader":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v9}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v11

    invoke-static {v0, v11}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    .line 586
    .end local v9    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    const/4 v6, 0x0

    .line 587
    .local v6, "normBuffer":[B
    const/4 v8, 0x0

    .line 588
    .local v8, "output":Lorg/apache/lucene/store/IndexOutput;
    const/4 v10, 0x0

    .line 590
    .local v10, "success":Z
    :try_start_0
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v11}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v7

    .line 591
    .local v7, "numFieldInfos":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v7, :cond_7

    .line 592
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v11, v2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v1

    .line 593
    .local v1, "fi":Lorg/apache/lucene/index/FieldInfo;
    iget-boolean v11, v1, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    if-eqz v11, :cond_6

    iget-boolean v11, v1, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    if-nez v11, :cond_6

    .line 594
    if-nez v8, :cond_1

    .line 595
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v12, p0, Lorg/apache/lucene/index/SegmentMerger;->segment:Ljava/lang/String;

    const-string/jumbo v13, "nrm"

    invoke-static {v12, v13}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v8

    .line 596
    sget-object v11, Lorg/apache/lucene/index/SegmentNorms;->NORMS_HEADER:[B

    sget-object v12, Lorg/apache/lucene/index/SegmentNorms;->NORMS_HEADER:[B

    array-length v12, v12

    invoke-virtual {v8, v11, v12}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V

    .line 598
    :cond_1
    if-nez v6, :cond_2

    .line 599
    new-array v6, v0, [B

    .line 601
    :cond_2
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/index/IndexReader;

    .line 602
    .restart local v9    # "reader":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v9}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v5

    .line 603
    .local v5, "maxDoc":I
    iget-object v11, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-virtual {v9, v11, v6, v12}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;[BI)V

    .line 604
    invoke-virtual {v9}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v11

    if-nez v11, :cond_4

    .line 606
    invoke-virtual {v8, v6, v5}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V

    .line 616
    :cond_3
    iget-object v11, p0, Lorg/apache/lucene/index/SegmentMerger;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    int-to-double v12, v5

    invoke-virtual {v11, v12, v13}, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->work(D)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 622
    .end local v1    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .end local v2    # "i":I
    .end local v5    # "maxDoc":I
    .end local v7    # "numFieldInfos":I
    .end local v9    # "reader":Lorg/apache/lucene/index/IndexReader;
    :catchall_0
    move-exception v11

    if-eqz v10, :cond_8

    .line 623
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/io/Closeable;

    const/4 v13, 0x0

    aput-object v8, v12, v13

    invoke-static {v12}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 622
    :goto_3
    throw v11

    .line 610
    .restart local v1    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .restart local v2    # "i":I
    .restart local v5    # "maxDoc":I
    .restart local v7    # "numFieldInfos":I
    .restart local v9    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_4
    const/4 v4, 0x0

    .local v4, "k":I
    :goto_4
    if-ge v4, v5, :cond_3

    .line 611
    :try_start_1
    invoke-virtual {v9, v4}, Lorg/apache/lucene/index/IndexReader;->isDeleted(I)Z

    move-result v11

    if-nez v11, :cond_5

    .line 612
    aget-byte v11, v6, v4

    invoke-virtual {v8, v11}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 591
    .end local v4    # "k":I
    .end local v5    # "maxDoc":I
    .end local v9    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 620
    .end local v1    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :cond_7
    const/4 v10, 0x1

    .line 622
    if-eqz v10, :cond_9

    .line 623
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    aput-object v8, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 628
    :goto_5
    return-void

    .line 625
    .end local v2    # "i":I
    .end local v7    # "numFieldInfos":I
    :cond_8
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/io/Closeable;

    const/4 v13, 0x0

    aput-object v8, v12, v13

    invoke-static {v12}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_3

    .restart local v2    # "i":I
    .restart local v7    # "numFieldInfos":I
    :cond_9
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    aput-object v8, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_5
.end method

.method private final mergeTermInfos(Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;)V
    .locals 26
    .param p1, "consumer"    # Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 444
    const/4 v4, 0x0

    .line 445
    .local v4, "base":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v14

    .line 446
    .local v14, "readerCount":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v14, :cond_5

    .line 447
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/lucene/index/IndexReader;

    .line 448
    .local v13, "reader":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v13}, Lorg/apache/lucene/index/IndexReader;->terms()Lorg/apache/lucene/index/TermEnum;

    move-result-object v17

    .line 449
    .local v17, "termEnum":Lorg/apache/lucene/index/TermEnum;
    new-instance v15, Lorg/apache/lucene/index/SegmentMergeInfo;

    move-object/from16 v0, v17

    invoke-direct {v15, v4, v0, v13}, Lorg/apache/lucene/index/SegmentMergeInfo;-><init>(ILorg/apache/lucene/index/TermEnum;Lorg/apache/lucene/index/IndexReader;)V

    .line 450
    .local v15, "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->payloadProcessorProvider:Lorg/apache/lucene/index/PayloadProcessorProvider;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    .line 451
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->payloadProcessorProvider:Lorg/apache/lucene/index/PayloadProcessorProvider;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Lorg/apache/lucene/index/PayloadProcessorProvider;->getReaderProcessor(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/PayloadProcessorProvider$ReaderPayloadProcessor;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v15, Lorg/apache/lucene/index/SegmentMergeInfo;->readerPayloadProcessor:Lorg/apache/lucene/index/PayloadProcessorProvider$ReaderPayloadProcessor;

    .line 453
    :cond_0
    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentMergeInfo;->getDocMap()[I

    move-result-object v7

    .line 454
    .local v7, "docMap":[I
    if-eqz v7, :cond_2

    .line 455
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->docMaps:[[I

    move-object/from16 v20, v0

    if-nez v20, :cond_1

    .line 456
    new-array v0, v14, [[I

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/SegmentMerger;->docMaps:[[I

    .line 458
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->docMaps:[[I

    move-object/from16 v20, v0

    aput-object v7, v20, v9

    .line 461
    :cond_2
    invoke-virtual {v13}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v20

    add-int v4, v4, v20

    .line 463
    sget-boolean v20, Lorg/apache/lucene/index/SegmentMerger;->$assertionsDisabled:Z

    if-nez v20, :cond_3

    invoke-virtual {v13}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v20

    invoke-virtual {v13}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v21

    iget v0, v15, Lorg/apache/lucene/index/SegmentMergeInfo;->delCount:I

    move/from16 v22, v0

    sub-int v21, v21, v22

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_3

    new-instance v20, Ljava/lang/AssertionError;

    invoke-direct/range {v20 .. v20}, Ljava/lang/AssertionError;-><init>()V

    throw v20

    .line 465
    :cond_3
    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentMergeInfo;->next()Z

    move-result v20

    if-eqz v20, :cond_4

    .line 466
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Lorg/apache/lucene/index/SegmentMergeQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 468
    :cond_4
    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentMergeInfo;->close()V

    goto :goto_1

    .line 471
    .end local v7    # "docMap":[I
    .end local v13    # "reader":Lorg/apache/lucene/index/IndexReader;
    .end local v15    # "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    .end local v17    # "termEnum":Lorg/apache/lucene/index/TermEnum;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v20

    move/from16 v0, v20

    new-array v10, v0, [Lorg/apache/lucene/index/SegmentMergeInfo;

    .line 473
    .local v10, "match":[Lorg/apache/lucene/index/SegmentMergeInfo;
    const/4 v5, 0x0

    .line 474
    .local v5, "currentField":Ljava/lang/String;
    const/16 v18, 0x0

    .line 476
    .local v18, "termsConsumer":Lorg/apache/lucene/index/FormatPostingsTermsConsumer;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/SegmentMergeQueue;->size()I

    move-result v20

    if-lez v20, :cond_b

    .line 477
    const/4 v11, 0x0

    .line 478
    .local v11, "matchSize":I
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "matchSize":I
    .local v12, "matchSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/SegmentMergeQueue;->pop()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lorg/apache/lucene/index/SegmentMergeInfo;

    aput-object v20, v10, v11

    .line 479
    const/16 v20, 0x0

    aget-object v20, v10, v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMergeInfo;->term:Lorg/apache/lucene/index/Term;

    move-object/from16 v16, v0

    .line 480
    .local v16, "term":Lorg/apache/lucene/index/Term;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/SegmentMergeQueue;->top()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/index/SegmentMergeInfo;

    .line 482
    .local v19, "top":Lorg/apache/lucene/index/SegmentMergeInfo;
    :goto_2
    if-eqz v19, :cond_7

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMergeInfo;->term:Lorg/apache/lucene/index/Term;

    move-object/from16 v20, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v20

    if-nez v20, :cond_7

    .line 483
    add-int/lit8 v11, v12, 0x1

    .end local v12    # "matchSize":I
    .restart local v11    # "matchSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/SegmentMergeQueue;->pop()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lorg/apache/lucene/index/SegmentMergeInfo;

    aput-object v20, v10, v12

    .line 484
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/SegmentMergeQueue;->top()Ljava/lang/Object;

    move-result-object v19

    .end local v19    # "top":Lorg/apache/lucene/index/SegmentMergeInfo;
    check-cast v19, Lorg/apache/lucene/index/SegmentMergeInfo;

    .restart local v19    # "top":Lorg/apache/lucene/index/SegmentMergeInfo;
    move v12, v11

    .end local v11    # "matchSize":I
    .restart local v12    # "matchSize":I
    goto :goto_2

    .line 487
    :cond_7
    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    if-eq v5, v0, :cond_9

    .line 488
    move-object/from16 v0, v16

    iget-object v5, v0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    .line 489
    if-eqz v18, :cond_8

    .line 490
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/FormatPostingsTermsConsumer;->finish()V

    .line 491
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v8

    .line 492
    .local v8, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;->addField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/FormatPostingsTermsConsumer;

    move-result-object v18

    .line 493
    iget-object v0, v8, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/SegmentMerger;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 496
    .end local v8    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v10, v12}, Lorg/apache/lucene/index/SegmentMerger;->appendPostings(Lorg/apache/lucene/index/FormatPostingsTermsConsumer;[Lorg/apache/lucene/index/SegmentMergeInfo;I)I

    move-result v6

    .line 497
    .local v6, "df":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    move-object/from16 v20, v0

    int-to-double v0, v6

    move-wide/from16 v22, v0

    const-wide/high16 v24, 0x4008000000000000L    # 3.0

    div-double v22, v22, v24

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/SegmentMerger$CheckAbort;->work(D)V

    move v11, v12

    .line 499
    .end local v12    # "matchSize":I
    .restart local v11    # "matchSize":I
    :goto_3
    if-lez v11, :cond_6

    .line 500
    add-int/lit8 v11, v11, -0x1

    aget-object v15, v10, v11

    .line 501
    .restart local v15    # "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentMergeInfo;->next()Z

    move-result v20

    if-eqz v20, :cond_a

    .line 502
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Lorg/apache/lucene/index/SegmentMergeQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 504
    :cond_a
    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentMergeInfo;->close()V

    goto :goto_3

    .line 507
    .end local v6    # "df":I
    .end local v11    # "matchSize":I
    .end local v15    # "smi":Lorg/apache/lucene/index/SegmentMergeInfo;
    .end local v16    # "term":Lorg/apache/lucene/index/Term;
    .end local v19    # "top":Lorg/apache/lucene/index/SegmentMergeInfo;
    :cond_b
    return-void
.end method

.method private final mergeTerms()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 423
    new-instance v0, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->segmentWriteState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/FormatPostingsFieldsWriter;-><init>(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/index/FieldInfos;)V

    .line 426
    .local v0, "fieldsConsumer":Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;
    :try_start_0
    new-instance v1, Lorg/apache/lucene/index/SegmentMergeQueue;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/SegmentMergeQueue;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    .line 428
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/SegmentMerger;->mergeTermInfos(Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 432
    :try_start_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;->finish()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 434
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    if-eqz v1, :cond_0

    .line 435
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentMergeQueue;->close()V

    .line 439
    :cond_0
    return-void

    .line 431
    :catchall_0
    move-exception v1

    .line 432
    :try_start_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/FormatPostingsFieldsConsumer;->finish()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 434
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    if-eqz v2, :cond_1

    .line 435
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentMergeQueue;->close()V

    .line 431
    :cond_1
    throw v1

    .line 434
    :catchall_1
    move-exception v1

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    if-eqz v2, :cond_2

    .line 435
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentMergeQueue;->close()V

    .line 434
    :cond_2
    throw v1

    :catchall_2
    move-exception v1

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    if-eqz v2, :cond_3

    .line 435
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentMerger;->queue:Lorg/apache/lucene/index/SegmentMergeQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentMergeQueue;->close()V

    .line 434
    :cond_3
    throw v1
.end method

.method private final mergeVectors()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 318
    new-instance v6, Lorg/apache/lucene/index/TermVectorsWriter;

    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v9, p0, Lorg/apache/lucene/index/SegmentMerger;->segment:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-direct {v6, v8, v9, v10}, Lorg/apache/lucene/index/TermVectorsWriter;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;)V

    .line 322
    .local v6, "termVectorsWriter":Lorg/apache/lucene/index/TermVectorsWriter;
    const/4 v1, 0x0

    .line 323
    .local v1, "idx":I
    :try_start_0
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    move v2, v1

    .end local v1    # "idx":I
    .local v2, "idx":I
    :goto_0
    :try_start_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/IndexReader;

    .line 324
    .local v5, "reader":Lorg/apache/lucene/index/IndexReader;
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->matchingSegmentReaders:[Lorg/apache/lucene/index/SegmentReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    :try_start_2
    aget-object v3, v8, v2

    .line 325
    .local v3, "matchingSegmentReader":Lorg/apache/lucene/index/SegmentReader;
    const/4 v4, 0x0

    .line 326
    .local v4, "matchingVectorsReader":Lorg/apache/lucene/index/TermVectorsReader;
    if-eqz v3, :cond_0

    .line 327
    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentReader;->getTermVectorsReader()Lorg/apache/lucene/index/TermVectorsReader;

    move-result-object v7

    .line 330
    .local v7, "vectorsReader":Lorg/apache/lucene/index/TermVectorsReader;
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lorg/apache/lucene/index/TermVectorsReader;->canReadRawDocs()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 331
    move-object v4, v7

    .line 334
    .end local v7    # "vectorsReader":Lorg/apache/lucene/index/TermVectorsReader;
    :cond_0
    invoke-virtual {v5}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 335
    invoke-direct {p0, v6, v4, v5}, Lorg/apache/lucene/index/SegmentMerger;->copyVectorsWithDeletions(Lorg/apache/lucene/index/TermVectorsWriter;Lorg/apache/lucene/index/TermVectorsReader;Lorg/apache/lucene/index/IndexReader;)V

    :goto_1
    move v2, v1

    .line 340
    .end local v1    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_0

    .line 337
    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    :cond_1
    invoke-direct {p0, v6, v4, v5}, Lorg/apache/lucene/index/SegmentMerger;->copyVectorsNoDeletions(Lorg/apache/lucene/index/TermVectorsWriter;Lorg/apache/lucene/index/TermVectorsReader;Lorg/apache/lucene/index/IndexReader;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 343
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v3    # "matchingSegmentReader":Lorg/apache/lucene/index/SegmentReader;
    .end local v4    # "matchingVectorsReader":Lorg/apache/lucene/index/TermVectorsReader;
    .end local v5    # "reader":Lorg/apache/lucene/index/IndexReader;
    :catchall_0
    move-exception v8

    :goto_2
    invoke-virtual {v6}, Lorg/apache/lucene/index/TermVectorsWriter;->close()V

    throw v8

    .line 341
    .end local v1    # "idx":I
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "idx":I
    :cond_2
    :try_start_3
    iget v8, p0, Lorg/apache/lucene/index/SegmentMerger;->mergedDocs:I

    invoke-virtual {v6, v8}, Lorg/apache/lucene/index/TermVectorsWriter;->finish(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 343
    invoke-virtual {v6}, Lorg/apache/lucene/index/TermVectorsWriter;->close()V

    .line 345
    return-void

    .line 343
    :catchall_1
    move-exception v8

    move v1, v2

    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    goto :goto_2
.end method

.method private setMatchingSegmentReaders()V
    .locals 11

    .prologue
    const/16 v10, 0x1060

    .line 155
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    .line 156
    .local v3, "numReaders":I
    new-array v8, v3, [Lorg/apache/lucene/index/SegmentReader;

    iput-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->matchingSegmentReaders:[Lorg/apache/lucene/index/SegmentReader;

    .line 162
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_3

    .line 163
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/IndexReader;

    .line 169
    .local v4, "reader":Lorg/apache/lucene/index/IndexReader;
    instance-of v8, v4, Lorg/apache/lucene/index/SegmentReader;

    if-eqz v8, :cond_1

    move-object v7, v4

    .line 170
    check-cast v7, Lorg/apache/lucene/index/SegmentReader;

    .line 171
    .local v7, "segmentReader":Lorg/apache/lucene/index/SegmentReader;
    const/4 v5, 0x1

    .line 172
    .local v5, "same":Z
    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v6

    .line 173
    .local v6, "segmentFieldInfos":Lorg/apache/lucene/index/FieldInfos;
    invoke-virtual {v6}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v2

    .line 174
    .local v2, "numFieldInfos":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-ge v1, v2, :cond_0

    .line 175
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v8, v1}, Lorg/apache/lucene/index/FieldInfos;->fieldName(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v1}, Lorg/apache/lucene/index/FieldInfos;->fieldName(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 176
    const/4 v5, 0x0

    .line 180
    :cond_0
    if-eqz v5, :cond_1

    .line 181
    iget-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->matchingSegmentReaders:[Lorg/apache/lucene/index/SegmentReader;

    aput-object v7, v8, v0

    .line 182
    iget v8, p0, Lorg/apache/lucene/index/SegmentMerger;->matchedCount:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lorg/apache/lucene/index/SegmentMerger;->matchedCount:I

    .line 162
    .end local v1    # "j":I
    .end local v2    # "numFieldInfos":I
    .end local v5    # "same":Z
    .end local v6    # "segmentFieldInfos":Lorg/apache/lucene/index/FieldInfos;
    .end local v7    # "segmentReader":Lorg/apache/lucene/index/SegmentReader;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 174
    .restart local v1    # "j":I
    .restart local v2    # "numFieldInfos":I
    .restart local v5    # "same":Z
    .restart local v6    # "segmentFieldInfos":Lorg/apache/lucene/index/FieldInfos;
    .restart local v7    # "segmentReader":Lorg/apache/lucene/index/SegmentReader;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 188
    .end local v1    # "j":I
    .end local v2    # "numFieldInfos":I
    .end local v4    # "reader":Lorg/apache/lucene/index/IndexReader;
    .end local v5    # "same":Z
    .end local v6    # "segmentFieldInfos":Lorg/apache/lucene/index/FieldInfos;
    .end local v7    # "segmentReader":Lorg/apache/lucene/index/SegmentReader;
    :cond_3
    new-array v8, v10, [I

    iput-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->rawDocLengths:[I

    .line 189
    new-array v8, v10, [I

    iput-object v8, p0, Lorg/apache/lucene/index/SegmentMerger;->rawDocLengths2:[I

    .line 190
    return-void
.end method


# virtual methods
.method final add(Lorg/apache/lucene/index/IndexReader;)V
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    invoke-static {v0, p1}, Lorg/apache/lucene/util/ReaderUtil;->gatherSubReaders(Ljava/util/List;Lorg/apache/lucene/index/IndexReader;)V

    .line 91
    return-void
.end method

.method final createCompoundFile(Ljava/lang/String;Lorg/apache/lucene/index/SegmentInfo;)Ljava/util/Collection;
    .locals 7
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v2

    .line 127
    .local v2, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    new-instance v0, Lorg/apache/lucene/index/CompoundFileWriter;

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentMerger;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v5, p0, Lorg/apache/lucene/index/SegmentMerger;->checkAbort:Lorg/apache/lucene/index/SegmentMerger$CheckAbort;

    invoke-direct {v0, v4, p1, v5}, Lorg/apache/lucene/index/CompoundFileWriter;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/SegmentMerger$CheckAbort;)V

    .line 128
    .local v0, "cfsWriter":Lorg/apache/lucene/index/CompoundFileWriter;
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 130
    .local v1, "file":Ljava/lang/String;
    sget-boolean v4, Lorg/apache/lucene/index/SegmentMerger;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    const-string/jumbo v4, "del"

    invoke-static {v1, v4}, Lorg/apache/lucene/index/IndexFileNames;->matchesExtension(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, ".del file is not allowed in .cfs: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 132
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/index/SegmentMerger;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    invoke-static {v1}, Lorg/apache/lucene/index/IndexFileNames;->isSeparateNormsFile(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "separate norms file (.s[0-9]+) is not allowed in .cfs: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 133
    :cond_1
    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/CompoundFileWriter;->addFile(Ljava/lang/String;)V

    goto :goto_0

    .line 137
    .end local v1    # "file":Ljava/lang/String;
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/CompoundFileWriter;->close()V

    .line 139
    return-object v2
.end method

.method public fieldInfos()Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    return-object v0
.end method

.method public getAnyNonBulkMerges()Z
    .locals 2

    .prologue
    .line 575
    sget-boolean v0, Lorg/apache/lucene/index/SegmentMerger;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/SegmentMerger;->matchedCount:I

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 576
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/SegmentMerger;->matchedCount:I

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMerger;->readers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMatchedSubReaderCount()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lorg/apache/lucene/index/SegmentMerger;->matchedCount:I

    return v0
.end method

.method final merge()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentMerger;->mergeFields()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/SegmentMerger;->mergedDocs:I

    .line 108
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentMerger;->mergeTerms()V

    .line 109
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentMerger;->mergeNorms()V

    .line 111
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfos;->hasVectors()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentMerger;->mergeVectors()V

    .line 114
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/SegmentMerger;->mergedDocs:I

    return v0
.end method
