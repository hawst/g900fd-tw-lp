.class Lorg/apache/lucene/index/DirectoryReader$1;
.super Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;
.source "DirectoryReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;ZI)Lorg/apache/lucene/index/IndexReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$deletionPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

.field final synthetic val$readOnly:Z

.field final synthetic val$termInfosIndexDivisor:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/Directory;ZLorg/apache/lucene/index/IndexDeletionPolicy;I)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    iput-boolean p2, p0, Lorg/apache/lucene/index/DirectoryReader$1;->val$readOnly:Z

    iput-object p3, p0, Lorg/apache/lucene/index/DirectoryReader$1;->val$deletionPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    iput p4, p0, Lorg/apache/lucene/index/DirectoryReader$1;->val$termInfosIndexDivisor:I

    invoke-direct {p0, p1}, Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;-><init>(Lorg/apache/lucene/store/Directory;)V

    return-void
.end method


# virtual methods
.method protected doBody(Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .param p1, "segmentFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    new-instance v2, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v2}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 76
    .local v2, "infos":Lorg/apache/lucene/index/SegmentInfos;
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$1;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v2, v0, p1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    .line 77
    iget-boolean v0, p0, Lorg/apache/lucene/index/DirectoryReader$1;->val$readOnly:Z

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Lorg/apache/lucene/index/ReadOnlyDirectoryReader;

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$1;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader$1;->val$deletionPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    iget v4, p0, Lorg/apache/lucene/index/DirectoryReader$1;->val$termInfosIndexDivisor:I

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/lucene/index/ReadOnlyDirectoryReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/IndexDeletionPolicy;I)V

    .line 80
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/index/DirectoryReader;

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$1;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader$1;->val$deletionPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    const/4 v4, 0x0

    iget v5, p0, Lorg/apache/lucene/index/DirectoryReader$1;->val$termInfosIndexDivisor:I

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/DirectoryReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/IndexDeletionPolicy;ZI)V

    goto :goto_0
.end method
