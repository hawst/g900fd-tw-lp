.class public Lorg/apache/lucene/index/TieredMergePolicy;
.super Lorg/apache/lucene/index/MergePolicy;
.source "TieredMergePolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;,
        Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private floorSegmentBytes:J

.field private forceMergeDeletesPctAllowed:D

.field private maxMergeAtOnce:I

.field private maxMergeAtOnceExplicit:I

.field private maxMergedSegmentBytes:J

.field private noCFSRatio:D

.field private reclaimDeletesWeight:D

.field private final segmentByteSizeDescending:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private segsPerTier:D

.field private useCompoundFile:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-class v0, Lorg/apache/lucene/index/TieredMergePolicy;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TieredMergePolicy;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    .line 74
    invoke-direct {p0}, Lorg/apache/lucene/index/MergePolicy;-><init>()V

    .line 76
    const/16 v0, 0xa

    iput v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    .line 77
    const-wide v0, 0x140000000L

    iput-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    .line 78
    const/16 v0, 0x1e

    iput v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    .line 80
    const-wide/32 v0, 0x200000

    iput-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->floorSegmentBytes:J

    .line 81
    iput-wide v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->segsPerTier:D

    .line 82
    iput-wide v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->forceMergeDeletesPctAllowed:D

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->useCompoundFile:Z

    .line 84
    const-wide v0, 0x3fb999999999999aL    # 0.1

    iput-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->noCFSRatio:D

    .line 85
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iput-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->reclaimDeletesWeight:D

    .line 258
    new-instance v0, Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;-><init>(Lorg/apache/lucene/index/TieredMergePolicy;Lorg/apache/lucene/index/TieredMergePolicy$1;)V

    iput-object v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->segmentByteSizeDescending:Ljava/util/Comparator;

    .line 262
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/index/TieredMergePolicy;Lorg/apache/lucene/index/SegmentInfo;)J
    .locals 2
    .param p0, "x0"    # Lorg/apache/lucene/index/TieredMergePolicy;
    .param p1, "x1"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v0

    return-wide v0
.end method

.method private floorSize(J)J
    .locals 3
    .param p1, "bytes"    # J

    .prologue
    .line 643
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->floorSegmentBytes:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private isMerged(Lorg/apache/lucene/index/SegmentInfo;)Z
    .locals 8
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 624
    iget-object v4, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v4}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexWriter;

    .line 625
    .local v1, "w":Lorg/apache/lucene/index/IndexWriter;
    sget-boolean v4, Lorg/apache/lucene/index/TieredMergePolicy;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-nez v1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 626
    :cond_0
    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v4

    if-lez v4, :cond_2

    move v0, v2

    .line 627
    .local v0, "hasDeletions":Z
    :goto_0
    if-nez v0, :cond_3

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->hasSeparateNorms()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriter;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v5

    if-ne v4, v5, :cond_3

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v4

    iget-boolean v5, p0, Lorg/apache/lucene/index/TieredMergePolicy;->useCompoundFile:Z

    if-eq v4, v5, :cond_1

    iget-wide v4, p0, Lorg/apache/lucene/index/TieredMergePolicy;->noCFSRatio:D

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpg-double v4, v4, v6

    if-gez v4, :cond_3

    :cond_1
    :goto_1
    return v2

    .end local v0    # "hasDeletions":Z
    :cond_2
    move v0, v3

    .line 626
    goto :goto_0

    .restart local v0    # "hasDeletions":Z
    :cond_3
    move v2, v3

    .line 627
    goto :goto_1
.end method

.method private message(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 652
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v0}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "TMP: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 655
    :cond_0
    return-void
.end method

.method private size(Lorg/apache/lucene/index/SegmentInfo;)J
    .locals 12
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 635
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytes(Z)J

    move-result-wide v0

    .line 636
    .local v0, "byteSize":J
    iget-object v3, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v3}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v2

    .line 637
    .local v2, "delCount":I
    iget v3, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-gtz v3, :cond_0

    const-wide/16 v4, 0x0

    .line 638
    .local v4, "delRatio":D
    :goto_0
    sget-boolean v3, Lorg/apache/lucene/index/TieredMergePolicy;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    cmpg-double v3, v4, v10

    if-lez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 637
    .end local v4    # "delRatio":D
    :cond_0
    int-to-double v6, v2

    iget v3, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    int-to-double v8, v3

    div-double v4, v6, v8

    goto :goto_0

    .line 639
    .restart local v4    # "delRatio":D
    :cond_1
    long-to-double v6, v0

    sub-double v8, v10, v4

    mul-double/2addr v6, v8

    double-to-long v6, v6

    return-wide v6
.end method

.method private verbose()Z
    .locals 2

    .prologue
    .line 647
    iget-object v1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v1}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriter;

    .line 648
    .local v0, "w":Lorg/apache/lucene/index/IndexWriter;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->verbose()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 620
    return-void
.end method

.method public findForcedDeletesMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 14
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 555
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 556
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "findForcedDeletesMerges infos="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v10, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v10}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v10, p1}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " forceMergeDeletesPctAllowed="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, p0, Lorg/apache/lucene/index/TieredMergePolicy;->forceMergeDeletesPctAllowed:D

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 558
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 559
    .local v0, "eligible":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    iget-object v10, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v10}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v10}, Lorg/apache/lucene/index/IndexWriter;->getMergingSegments()Ljava/util/Collection;

    move-result-object v5

    .line 560
    .local v5, "merging":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/SegmentInfo;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/SegmentInfo;

    .line 561
    .local v3, "info":Lorg/apache/lucene/index/SegmentInfo;
    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    iget-object v10, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v10}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v10, v3}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v10

    int-to-double v10, v10

    mul-double/2addr v10, v12

    iget v12, v3, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    int-to-double v12, v12

    div-double v6, v10, v12

    .line 562
    .local v6, "pctDeletes":D
    iget-wide v10, p0, Lorg/apache/lucene/index/TieredMergePolicy;->forceMergeDeletesPctAllowed:D

    cmpl-double v10, v6, v10

    if-lez v10, :cond_1

    invoke-interface {v5, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 563
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 567
    .end local v3    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v6    # "pctDeletes":D
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    if-nez v10, :cond_4

    .line 568
    const/4 v8, 0x0

    .line 597
    :cond_3
    return-object v8

    .line 571
    :cond_4
    iget-object v10, p0, Lorg/apache/lucene/index/TieredMergePolicy;->segmentByteSizeDescending:Ljava/util/Comparator;

    invoke-static {v0, v10}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 573
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 574
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "eligible="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 577
    :cond_5
    const/4 v9, 0x0

    .line 578
    .local v9, "start":I
    const/4 v8, 0x0

    .line 580
    .local v8, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    if-ge v9, v10, :cond_3

    .line 584
    iget v10, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    add-int/2addr v10, v9

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 585
    .local v1, "end":I
    if-nez v8, :cond_6

    .line 586
    new-instance v8, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    .end local v8    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-direct {v8}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 589
    .restart local v8    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_6
    new-instance v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-interface {v0, v9, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v10

    invoke-direct {v4, v10}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    .line 590
    .local v4, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 591
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "add merge="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v10, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v10}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/index/IndexWriter;

    iget-object v12, v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {v10, v12}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 593
    :cond_7
    invoke-virtual {v8, v4}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 594
    move v9, v1

    .line 595
    goto :goto_1
.end method

.method public findForcedMerges(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 15
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "maxSegmentCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "I",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lorg/apache/lucene/index/MergePolicy$MergeSpecification;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 482
    .local p3, "segmentsToMerge":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/Boolean;>;"
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 483
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "findForcedMerges maxSegmentCount="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " infos="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v12, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v12}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " segmentsToMerge="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v12}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 486
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 487
    .local v1, "eligible":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    const/4 v3, 0x0

    .line 488
    .local v3, "forceMergeRunning":Z
    iget-object v12, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v12}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v12}, Lorg/apache/lucene/index/IndexWriter;->getMergingSegments()Ljava/util/Collection;

    move-result-object v8

    .line 489
    .local v8, "merging":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/SegmentInfo;>;"
    const/4 v10, 0x0

    .line 490
    .local v10, "segmentIsOriginal":Z
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/SegmentInfo;

    .line 491
    .local v5, "info":Lorg/apache/lucene/index/SegmentInfo;
    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    .line 492
    .local v6, "isOriginal":Ljava/lang/Boolean;
    if-eqz v6, :cond_1

    .line 493
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    .line 494
    invoke-interface {v8, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 495
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 497
    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    .line 502
    .end local v5    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v6    # "isOriginal":Ljava/lang/Boolean;
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v12

    if-nez v12, :cond_5

    .line 503
    const/4 v11, 0x0

    .line 549
    :cond_4
    :goto_1
    return-object v11

    .line 506
    :cond_5
    const/4 v12, 0x1

    move/from16 v0, p2

    if-le v0, v12, :cond_6

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v12

    move/from16 v0, p2

    if-le v12, v0, :cond_7

    :cond_6
    const/4 v12, 0x1

    move/from16 v0, p2

    if-ne v0, v12, :cond_9

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_9

    if-eqz v10, :cond_7

    const/4 v12, 0x0

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/index/SegmentInfo;

    invoke-direct {p0, v12}, Lorg/apache/lucene/index/TieredMergePolicy;->isMerged(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 508
    :cond_7
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v12

    if-eqz v12, :cond_8

    .line 509
    const-string/jumbo v12, "already merged"

    invoke-direct {p0, v12}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 511
    :cond_8
    const/4 v11, 0x0

    goto :goto_1

    .line 514
    :cond_9
    iget-object v12, p0, Lorg/apache/lucene/index/TieredMergePolicy;->segmentByteSizeDescending:Ljava/util/Comparator;

    invoke-static {v1, v12}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 516
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v12

    if-eqz v12, :cond_a

    .line 517
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "eligible="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v12}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 518
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "forceMergeRunning="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v12}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 521
    :cond_a
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 523
    .local v2, "end":I
    const/4 v11, 0x0

    .line 526
    .local v11, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :goto_2
    iget v12, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    add-int v12, v12, p2

    add-int/lit8 v12, v12, -0x1

    if-lt v2, v12, :cond_d

    .line 527
    if-nez v11, :cond_b

    .line 528
    new-instance v11, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    .end local v11    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-direct {v11}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 530
    .restart local v11    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_b
    new-instance v7, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    iget v12, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    sub-int v12, v2, v12

    invoke-interface {v1, v12, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v12

    invoke-direct {v7, v12}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    .line 531
    .local v7, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v12

    if-eqz v12, :cond_c

    .line 532
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "add merge="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v12, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v12}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/index/IndexWriter;

    iget-object v14, v7, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {v12, v14}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v12}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 534
    :cond_c
    invoke-virtual {v11, v7}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 535
    iget v12, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    sub-int/2addr v2, v12

    .line 536
    goto :goto_2

    .line 538
    .end local v7    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_d
    if-nez v11, :cond_4

    if-nez v3, :cond_4

    .line 540
    sub-int v12, v2, p2

    add-int/lit8 v9, v12, 0x1

    .line 541
    .local v9, "numToMerge":I
    new-instance v7, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    sub-int v12, v2, v9

    invoke-interface {v1, v12, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v12

    invoke-direct {v7, v12}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    .line 542
    .restart local v7    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v12

    if-eqz v12, :cond_e

    .line 543
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "add final merge="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v12, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v12}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v12}, Lorg/apache/lucene/index/IndexWriter;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v12

    invoke-virtual {v7, v12}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v12}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 545
    :cond_e
    new-instance v11, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    .end local v11    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-direct {v11}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 546
    .restart local v11    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-virtual {v11, v7}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    goto/16 :goto_1
.end method

.method public findMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 52
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 269
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v44

    if-eqz v44, :cond_0

    .line 270
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v45, "findMerges: "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " segments"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 272
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v44

    if-nez v44, :cond_2

    .line 273
    const/16 v36, 0x0

    .line 419
    :cond_1
    return-object v36

    .line 275
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual/range {v44 .. v44}, Lorg/apache/lucene/index/IndexWriter;->getMergingSegments()Ljava/util/Collection;

    move-result-object v26

    .line 276
    .local v26, "merging":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/SegmentInfo;>;"
    new-instance v38, Ljava/util/HashSet;

    invoke-direct/range {v38 .. v38}, Ljava/util/HashSet;-><init>()V

    .line 278
    .local v38, "toBeMerged":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/SegmentInfo;>;"
    new-instance v21, Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentInfos;->asList()Ljava/util/List;

    move-result-object v44

    move-object/from16 v0, v21

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 279
    .local v21, "infosSorted":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->segmentByteSizeDescending:Ljava/util/Comparator;

    move-object/from16 v44, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v44

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 282
    const-wide/16 v42, 0x0

    .line 283
    .local v42, "totIndexBytes":J
    const-wide v30, 0x7fffffffffffffffL

    .line 284
    .local v30, "minSegmentBytes":J
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v44

    if-eqz v44, :cond_7

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lorg/apache/lucene/index/SegmentInfo;

    .line 285
    .local v20, "info":Lorg/apache/lucene/index/SegmentInfo;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v32

    .line 286
    .local v32, "segBytes":J
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v44

    if-eqz v44, :cond_4

    .line 287
    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_5

    const-string/jumbo v16, " [merging]"

    .line 288
    .local v16, "extra":Ljava/lang/String;
    :goto_1
    move-wide/from16 v0, v32

    long-to-double v0, v0

    move-wide/from16 v44, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    move-wide/from16 v46, v0

    move-wide/from16 v0, v46

    long-to-double v0, v0

    move-wide/from16 v46, v0

    const-wide/high16 v48, 0x4000000000000000L    # 2.0

    div-double v46, v46, v48

    cmpl-double v44, v44, v46

    if-ltz v44, :cond_6

    .line 289
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v44

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " [skip: too large]"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 293
    :cond_3
    :goto_2
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v45, "  seg="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, v44

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->segString(Lorg/apache/lucene/index/SegmentInfo;)Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v45

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " size="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, "%.3f"

    const/16 v46, 0x1

    move/from16 v0, v46

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v46, v0

    const/16 v47, 0x0

    const-wide/16 v48, 0x400

    div-long v48, v32, v48

    move-wide/from16 v0, v48

    long-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide/high16 v50, 0x4090000000000000L    # 1024.0

    div-double v48, v48, v50

    invoke-static/range {v48 .. v49}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v48

    aput-object v48, v46, v47

    invoke-static/range {v45 .. v46}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " MB"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 296
    .end local v16    # "extra":Ljava/lang/String;
    :cond_4
    move-wide/from16 v0, v32

    move-wide/from16 v2, v30

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v30

    .line 298
    add-long v42, v42, v32

    .line 299
    goto/16 :goto_0

    .line 287
    :cond_5
    const-string/jumbo v16, ""

    goto/16 :goto_1

    .line 290
    .restart local v16    # "extra":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->floorSegmentBytes:J

    move-wide/from16 v44, v0

    cmp-long v44, v32, v44

    if-gez v44, :cond_3

    .line 291
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v44

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " [floored]"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_2

    .line 303
    .end local v16    # "extra":Ljava/lang/String;
    .end local v20    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v32    # "segBytes":J
    :cond_7
    const/16 v39, 0x0

    .line 304
    .local v39, "tooBigCount":I
    :goto_3
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v44

    move/from16 v0, v39

    move/from16 v1, v44

    if-ge v0, v1, :cond_8

    move-object/from16 v0, v21

    move/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v44

    move-wide/from16 v0, v44

    long-to-double v0, v0

    move-wide/from16 v44, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    move-wide/from16 v46, v0

    move-wide/from16 v0, v46

    long-to-double v0, v0

    move-wide/from16 v46, v0

    const-wide/high16 v48, 0x4000000000000000L    # 2.0

    div-double v46, v46, v48

    cmpl-double v44, v44, v46

    if-ltz v44, :cond_8

    .line 305
    move-object/from16 v0, v21

    move/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v44

    sub-long v42, v42, v44

    .line 306
    add-int/lit8 v39, v39, 0x1

    goto :goto_3

    .line 309
    :cond_8
    move-object/from16 v0, p0

    move-wide/from16 v1, v30

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/TieredMergePolicy;->floorSize(J)J

    move-result-wide v30

    .line 312
    move-wide/from16 v22, v30

    .line 313
    .local v22, "levelSize":J
    move-wide/from16 v12, v42

    .line 314
    .local v12, "bytesLeft":J
    const-wide/16 v4, 0x0

    .line 316
    .local v4, "allowedSegCount":D
    :goto_4
    long-to-double v0, v12

    move-wide/from16 v44, v0

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v46, v0

    div-double v34, v44, v46

    .line 317
    .local v34, "segCountLevel":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->segsPerTier:D

    move-wide/from16 v44, v0

    cmpg-double v44, v34, v44

    if-gez v44, :cond_b

    .line 318
    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v44

    add-double v4, v4, v44

    .line 325
    double-to-int v6, v4

    .line 327
    .local v6, "allowedSegCountInt":I
    const/16 v36, 0x0

    .line 332
    .local v36, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_9
    :goto_5
    const-wide/16 v28, 0x0

    .line 337
    .local v28, "mergingBytes":J
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 338
    .local v15, "eligible":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    move/from16 v19, v39

    .local v19, "idx":I
    :goto_6
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v44

    move/from16 v0, v19

    move/from16 v1, v44

    if-ge v0, v1, :cond_d

    .line 339
    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lorg/apache/lucene/index/SegmentInfo;

    .line 340
    .restart local v20    # "info":Lorg/apache/lucene/index/SegmentInfo;
    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_c

    .line 341
    const/16 v44, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytes(Z)J

    move-result-wide v44

    add-long v28, v28, v44

    .line 338
    :cond_a
    :goto_7
    add-int/lit8 v19, v19, 0x1

    goto :goto_6

    .line 321
    .end local v6    # "allowedSegCountInt":I
    .end local v15    # "eligible":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    .end local v19    # "idx":I
    .end local v20    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v28    # "mergingBytes":J
    .end local v36    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_b
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->segsPerTier:D

    move-wide/from16 v44, v0

    add-double v4, v4, v44

    .line 322
    long-to-double v0, v12

    move-wide/from16 v44, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->segsPerTier:D

    move-wide/from16 v46, v0

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v48, v0

    mul-double v46, v46, v48

    sub-double v44, v44, v46

    move-wide/from16 v0, v44

    double-to-long v12, v0

    .line 323
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    move/from16 v44, v0

    move/from16 v0, v44

    int-to-long v0, v0

    move-wide/from16 v44, v0

    mul-long v22, v22, v44

    .line 324
    goto :goto_4

    .line 342
    .restart local v6    # "allowedSegCountInt":I
    .restart local v15    # "eligible":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    .restart local v19    # "idx":I
    .restart local v20    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .restart local v28    # "mergingBytes":J
    .restart local v36    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_c
    move-object/from16 v0, v38

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v44

    if-nez v44, :cond_a

    .line 343
    move-object/from16 v0, v20

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 347
    .end local v20    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_d
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    move-wide/from16 v44, v0

    cmp-long v44, v28, v44

    if-ltz v44, :cond_e

    const/16 v24, 0x1

    .line 349
    .local v24, "maxMergeIsRunning":Z
    :goto_8
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v45, "  allowedSegmentCount="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " vs count="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " (eligible count="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, ") tooBigCount="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 351
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v44

    if-eqz v44, :cond_1

    .line 355
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v44

    move/from16 v0, v44

    if-lt v0, v6, :cond_1

    .line 358
    const/4 v10, 0x0

    .line 359
    .local v10, "bestScore":Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
    const/4 v7, 0x0

    .line 360
    .local v7, "best":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    const/4 v11, 0x0

    .line 361
    .local v11, "bestTooLarge":Z
    const-wide/16 v8, 0x0

    .line 364
    .local v8, "bestMergeBytes":J
    const/16 v37, 0x0

    .local v37, "startIdx":I
    :goto_9
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v44

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    move/from16 v45, v0

    sub-int v44, v44, v45

    move/from16 v0, v37

    move/from16 v1, v44

    if-gt v0, v1, :cond_14

    .line 366
    const-wide/16 v40, 0x0

    .line 368
    .local v40, "totAfterMergeBytes":J
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 369
    .local v14, "candidate":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    const/16 v17, 0x0

    .line 370
    .local v17, "hitTooLarge":Z
    move/from16 v19, v37

    :goto_a
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v44

    move/from16 v0, v19

    move/from16 v1, v44

    if-ge v0, v1, :cond_10

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v44

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    move/from16 v45, v0

    move/from16 v0, v44

    move/from16 v1, v45

    if-ge v0, v1, :cond_10

    .line 371
    move/from16 v0, v19

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lorg/apache/lucene/index/SegmentInfo;

    .line 372
    .restart local v20    # "info":Lorg/apache/lucene/index/SegmentInfo;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v32

    .line 374
    .restart local v32    # "segBytes":J
    add-long v44, v40, v32

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    move-wide/from16 v46, v0

    cmp-long v44, v44, v46

    if-lez v44, :cond_f

    .line 375
    const/16 v17, 0x1

    .line 370
    :goto_b
    add-int/lit8 v19, v19, 0x1

    goto :goto_a

    .line 347
    .end local v7    # "best":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    .end local v8    # "bestMergeBytes":J
    .end local v10    # "bestScore":Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
    .end local v11    # "bestTooLarge":Z
    .end local v14    # "candidate":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    .end local v17    # "hitTooLarge":Z
    .end local v20    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v24    # "maxMergeIsRunning":Z
    .end local v32    # "segBytes":J
    .end local v37    # "startIdx":I
    .end local v40    # "totAfterMergeBytes":J
    :cond_e
    const/16 v24, 0x0

    goto/16 :goto_8

    .line 384
    .restart local v7    # "best":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    .restart local v8    # "bestMergeBytes":J
    .restart local v10    # "bestScore":Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
    .restart local v11    # "bestTooLarge":Z
    .restart local v14    # "candidate":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    .restart local v17    # "hitTooLarge":Z
    .restart local v20    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .restart local v24    # "maxMergeIsRunning":Z
    .restart local v32    # "segBytes":J
    .restart local v37    # "startIdx":I
    .restart local v40    # "totAfterMergeBytes":J
    :cond_f
    move-object/from16 v0, v20

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 385
    add-long v40, v40, v32

    goto :goto_b

    .line 388
    .end local v20    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v32    # "segBytes":J
    :cond_10
    move-object/from16 v0, p0

    move/from16 v1, v17

    move-wide/from16 v2, v28

    invoke-virtual {v0, v14, v1, v2, v3}, Lorg/apache/lucene/index/TieredMergePolicy;->score(Ljava/util/List;ZJ)Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;

    move-result-object v27

    .line 389
    .local v27, "score":Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v45, "  maybe="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, v44

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v45

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " score="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;->getScore()D

    move-result-wide v46

    move-object/from16 v0, v44

    move-wide/from16 v1, v46

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;->getExplanation()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " tooLarge="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " size="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, "%.3f MB"

    const/16 v46, 0x1

    move/from16 v0, v46

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v46, v0

    const/16 v47, 0x0

    move-wide/from16 v0, v40

    long-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide/high16 v50, 0x4090000000000000L    # 1024.0

    div-double v48, v48, v50

    const-wide/high16 v50, 0x4090000000000000L    # 1024.0

    div-double v48, v48, v50

    invoke-static/range {v48 .. v49}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v48

    aput-object v48, v46, v47

    invoke-static/range {v45 .. v46}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 394
    if-eqz v10, :cond_11

    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;->getScore()D

    move-result-wide v44

    invoke-virtual {v10}, Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;->getScore()D

    move-result-wide v46

    cmpg-double v44, v44, v46

    if-gez v44, :cond_13

    :cond_11
    if-eqz v17, :cond_12

    if-nez v24, :cond_13

    .line 395
    :cond_12
    move-object v7, v14

    .line 396
    move-object/from16 v10, v27

    .line 397
    move/from16 v11, v17

    .line 398
    move-wide/from16 v8, v40

    .line 364
    :cond_13
    add-int/lit8 v37, v37, 0x1

    goto/16 :goto_9

    .line 402
    .end local v14    # "candidate":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    .end local v17    # "hitTooLarge":Z
    .end local v27    # "score":Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
    .end local v40    # "totAfterMergeBytes":J
    :cond_14
    if-eqz v7, :cond_1

    .line 403
    if-nez v36, :cond_15

    .line 404
    new-instance v36, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    .end local v36    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-direct/range {v36 .. v36}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 406
    .restart local v36    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_15
    new-instance v25, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-object/from16 v0, v25

    invoke-direct {v0, v7}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    .line 407
    .local v25, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    move-object/from16 v0, v36

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 408
    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    move-object/from16 v44, v0

    invoke-interface/range {v44 .. v44}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_c
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v44

    if-eqz v44, :cond_16

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lorg/apache/lucene/index/SegmentInfo;

    .line 409
    .restart local v20    # "info":Lorg/apache/lucene/index/SegmentInfo;
    move-object/from16 v0, v38

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 412
    .end local v20    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_16
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v44

    if-eqz v44, :cond_9

    .line 413
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v45, "  add merge="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    move-object/from16 v46, v0

    move-object/from16 v0, v44

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v45

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " size="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, "%.3f MB"

    const/16 v46, 0x1

    move/from16 v0, v46

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v46, v0

    const/16 v47, 0x0

    long-to-double v0, v8

    move-wide/from16 v48, v0

    const-wide/high16 v50, 0x4090000000000000L    # 1024.0

    div-double v48, v48, v50

    const-wide/high16 v50, 0x4090000000000000L    # 1024.0

    div-double v48, v48, v50

    invoke-static/range {v48 .. v49}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v48

    aput-object v48, v46, v47

    invoke-static/range {v45 .. v46}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " score="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, "%.3f"

    const/16 v46, 0x1

    move/from16 v0, v46

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v46, v0

    const/16 v47, 0x0

    invoke-virtual {v10}, Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;->getScore()D

    move-result-wide v48

    invoke-static/range {v48 .. v49}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v48

    aput-object v48, v46, v47

    invoke-static/range {v45 .. v46}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string/jumbo v45, " "

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual {v10}, Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;->getExplanation()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    if-eqz v11, :cond_17

    const-string/jumbo v44, " [max merge]"

    :goto_d
    move-object/from16 v0, v45

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_17
    const-string/jumbo v44, ""

    goto :goto_d
.end method

.method public getFloorSegmentMB()D
    .locals 4

    .prologue
    .line 169
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->floorSegmentBytes:J

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    long-to-double v0, v0

    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public getForceMergeDeletesPctAllowed()D
    .locals 2

    .prologue
    .line 185
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->forceMergeDeletesPctAllowed:D

    return-wide v0
.end method

.method public getMaxMergeAtOnce()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    return v0
.end method

.method public getMaxMergeAtOnceExplicit()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    return v0
.end method

.method public getMaxMergedSegmentMB()D
    .locals 4

    .prologue
    .line 134
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    long-to-double v0, v0

    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public getNoCFSRatio()D
    .locals 2

    .prologue
    .line 237
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->noCFSRatio:D

    return-wide v0
.end method

.method public getReclaimDeletesWeight()D
    .locals 2

    .prologue
    .line 151
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->reclaimDeletesWeight:D

    return-wide v0
.end method

.method public getSegmentsPerTier()D
    .locals 2

    .prologue
    .line 206
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->segsPerTier:D

    return-wide v0
.end method

.method public getUseCompoundFile()Z
    .locals 1

    .prologue
    .line 219
    iget-boolean v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->useCompoundFile:Z

    return v0
.end method

.method protected score(Ljava/util/List;ZJ)Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
    .locals 24
    .param p2, "hitTooLarge"    # Z
    .param p3, "mergingBytes"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;ZJ)",
            "Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 426
    .local p1, "candidate":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    const-wide/16 v20, 0x0

    .line 427
    .local v20, "totBeforeMergeBytes":J
    const-wide/16 v16, 0x0

    .line 428
    .local v16, "totAfterMergeBytes":J
    const-wide/16 v18, 0x0

    .line 429
    .local v18, "totAfterMergeBytesFloored":J
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/index/SegmentInfo;

    .line 430
    .local v11, "info":Lorg/apache/lucene/index/SegmentInfo;
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v14

    .line 431
    .local v14, "segBytes":J
    add-long v16, v16, v14

    .line 432
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lorg/apache/lucene/index/TieredMergePolicy;->floorSize(J)J

    move-result-wide v2

    add-long v18, v18, v2

    .line 433
    const/4 v2, 0x1

    invoke-virtual {v11, v2}, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytes(Z)J

    move-result-wide v2

    add-long v20, v20, v2

    .line 434
    goto :goto_0

    .line 440
    .end local v11    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v14    # "segBytes":J
    :cond_0
    if-eqz p2, :cond_1

    .line 445
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-double v0, v0

    move-wide/from16 v22, v0

    div-double v6, v2, v22

    .line 452
    .local v6, "skew":D
    :goto_1
    move-wide v12, v6

    .line 458
    .local v12, "mergeScore":D
    move-wide/from16 v0, v16

    long-to-double v2, v0

    const-wide v22, 0x3fa999999999999aL    # 0.05

    move-wide/from16 v0, v22

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v12, v2

    .line 461
    move-wide/from16 v0, v16

    long-to-double v2, v0

    move-wide/from16 v0, v20

    long-to-double v0, v0

    move-wide/from16 v22, v0

    div-double v8, v2, v22

    .line 462
    .local v8, "nonDelRatio":D
    move-object/from16 v0, p0

    iget-wide v2, v0, Lorg/apache/lucene/index/TieredMergePolicy;->reclaimDeletesWeight:D

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v12, v2

    .line 464
    move-wide v4, v12

    .line 466
    .local v4, "finalMergeScore":D
    new-instance v2, Lorg/apache/lucene/index/TieredMergePolicy$1;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Lorg/apache/lucene/index/TieredMergePolicy$1;-><init>(Lorg/apache/lucene/index/TieredMergePolicy;DDD)V

    return-object v2

    .line 447
    .end local v4    # "finalMergeScore":D
    .end local v6    # "skew":D
    .end local v8    # "nonDelRatio":D
    .end local v12    # "mergeScore":D
    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lorg/apache/lucene/index/TieredMergePolicy;->floorSize(J)J

    move-result-wide v2

    long-to-double v2, v2

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v22, v0

    div-double v6, v2, v22

    .restart local v6    # "skew":D
    goto :goto_1
.end method

.method public setFloorSegmentMB(D)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 5
    .param p1, "v"    # D

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 160
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_0

    .line 161
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "floorSegmentMB must be >= 0.0 (got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_0
    mul-double v0, p1, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->floorSegmentBytes:J

    .line 164
    return-object p0
.end method

.method public setForceMergeDeletesPctAllowed(D)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "v"    # D

    .prologue
    .line 176
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-ltz v0, :cond_0

    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    .line 177
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "forceMergeDeletesPctAllowed must be between 0.0 and 100.0 inclusive (got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_1
    iput-wide p1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->forceMergeDeletesPctAllowed:D

    .line 180
    return-object p0
.end method

.method public setMaxMergeAtOnce(I)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "v"    # I

    .prologue
    .line 92
    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "maxMergeAtOnce must be > 1 (got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    iput p1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    .line 96
    return-object p0
.end method

.method public setMaxMergeAtOnceExplicit(I)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "v"    # I

    .prologue
    .line 110
    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "maxMergeAtOnceExplicit must be > 1 (got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_0
    iput p1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    .line 114
    return-object p0
.end method

.method public setMaxMergedSegmentMB(D)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 5
    .param p1, "v"    # D

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 128
    mul-double v0, p1, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    .line 129
    return-object p0
.end method

.method public setNoCFSRatio(D)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "noCFSRatio"    # D

    .prologue
    .line 228
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-ltz v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    .line 229
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "noCFSRatio must be 0.0 to 1.0 inclusive; got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :cond_1
    iput-wide p1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->noCFSRatio:D

    .line 232
    return-object p0
.end method

.method public setReclaimDeletesWeight(D)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "v"    # D

    .prologue
    .line 142
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "reclaimDeletesWeight must be >= 0.0 (got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    iput-wide p1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->reclaimDeletesWeight:D

    .line 146
    return-object p0
.end method

.method public setSegmentsPerTier(D)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "v"    # D

    .prologue
    .line 197
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 198
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "segmentsPerTier must be >= 2.0 (got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :cond_0
    iput-wide p1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->segsPerTier:D

    .line 201
    return-object p0
.end method

.method public setUseCompoundFile(Z)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 0
    .param p1, "useCompoundFile"    # Z

    .prologue
    .line 213
    iput-boolean p1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->useCompoundFile:Z

    .line 214
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, 0x400

    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    .line 659
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 660
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "maxMergeAtOnce="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 661
    const-string/jumbo v1, "maxMergeAtOnceExplicit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 662
    const-string/jumbo v1, "maxMergedSegmentMB="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    div-long/2addr v2, v6

    long-to-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 663
    const-string/jumbo v1, "floorSegmentMB="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->floorSegmentBytes:J

    div-long/2addr v2, v6

    long-to-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 664
    const-string/jumbo v1, "forceMergeDeletesPctAllowed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->forceMergeDeletesPctAllowed:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 665
    const-string/jumbo v1, "segmentsPerTier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->segsPerTier:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 666
    const-string/jumbo v1, "useCompoundFile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->useCompoundFile:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 667
    const-string/jumbo v1, "noCFSRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->noCFSRatio:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 668
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfo;)Z
    .locals 12
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "mergedInfo"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 604
    iget-boolean v3, p0, Lorg/apache/lucene/index/TieredMergePolicy;->useCompoundFile:Z

    if-nez v3, :cond_0

    .line 605
    const/4 v0, 0x0

    .line 615
    .local v0, "doCFS":Z
    :goto_0
    return v0

    .line 606
    .end local v0    # "doCFS":Z
    :cond_0
    iget-wide v6, p0, Lorg/apache/lucene/index/TieredMergePolicy;->noCFSRatio:D

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v6, v8

    if-nez v3, :cond_1

    .line 607
    const/4 v0, 0x1

    .restart local v0    # "doCFS":Z
    goto :goto_0

    .line 609
    .end local v0    # "doCFS":Z
    :cond_1
    const-wide/16 v4, 0x0

    .line 610
    .local v4, "totalSize":J
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentInfo;

    .line 611
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-direct {p0, v2}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v6

    add-long/2addr v4, v6

    goto :goto_1

    .line 613
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_2
    invoke-direct {p0, p2}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfo;)J

    move-result-wide v6

    long-to-double v6, v6

    iget-wide v8, p0, Lorg/apache/lucene/index/TieredMergePolicy;->noCFSRatio:D

    long-to-double v10, v4

    mul-double/2addr v8, v10

    cmpg-double v3, v6, v8

    if-gtz v3, :cond_3

    const/4 v0, 0x1

    .restart local v0    # "doCFS":Z
    :goto_2
    goto :goto_0

    .end local v0    # "doCFS":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method
