.class final Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;
.super Lorg/apache/lucene/store/BufferedIndexInput;
.source "CompoundFileReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/CompoundFileReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "CSIndexInput"
.end annotation


# instance fields
.field base:Lorg/apache/lucene/store/IndexInput;

.field fileOffset:J

.field length:J


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/IndexInput;JJ)V
    .locals 8
    .param p1, "base"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "fileOffset"    # J
    .param p4, "length"    # J

    .prologue
    .line 244
    const/16 v6, 0x400

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;-><init>(Lorg/apache/lucene/store/IndexInput;JJI)V

    .line 245
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/store/IndexInput;JJI)V
    .locals 2
    .param p1, "base"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "fileOffset"    # J
    .param p4, "length"    # J
    .param p6, "readBufferSize"    # I

    .prologue
    .line 248
    invoke-direct {p0, p6}, Lorg/apache/lucene/store/BufferedIndexInput;-><init>(I)V

    .line 249
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/IndexInput;

    iput-object v0, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    .line 250
    iput-wide p2, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->fileOffset:J

    .line 251
    iput-wide p4, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->length:J

    .line 252
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 256
    invoke-super {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;

    .line 257
    .local v0, "clone":Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;
    iget-object v1, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/store/IndexInput;

    iput-object v1, v0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    .line 258
    iget-wide v2, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->fileOffset:J

    iput-wide v2, v0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->fileOffset:J

    .line 259
    iget-wide v2, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->length:J

    iput-wide v2, v0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->length:J

    .line 260
    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    iget-object v0, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 289
    return-void
.end method

.method public copyBytes(Lorg/apache/lucene/store/IndexOutput;J)V
    .locals 6
    .param p1, "out"    # Lorg/apache/lucene/store/IndexOutput;
    .param p2, "numBytes"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 299
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->flushBuffer(Lorg/apache/lucene/store/IndexOutput;J)I

    move-result v2

    int-to-long v2, v2

    sub-long/2addr p2, v2

    .line 303
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-lez v2, :cond_1

    .line 304
    invoke-virtual {p0}, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->getFilePointer()J

    move-result-wide v0

    .line 305
    .local v0, "start":J
    add-long v2, v0, p2

    iget-wide v4, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->length:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 306
    new-instance v2, Ljava/io/EOFException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "read past EOF: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 308
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    iget-wide v4, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->fileOffset:J

    add-long/2addr v4, v0

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 309
    iget-object v2, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2, p1, p2, p3}, Lorg/apache/lucene/store/IndexInput;->copyBytes(Lorg/apache/lucene/store/IndexOutput;J)V

    .line 311
    .end local v0    # "start":J
    :cond_1
    return-void
.end method

.method public length()J
    .locals 2

    .prologue
    .line 293
    iget-wide v0, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->length:J

    return-wide v0
.end method

.method protected readInternal([BII)V
    .locals 6
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 271
    invoke-virtual {p0}, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->getFilePointer()J

    move-result-wide v0

    .line 272
    .local v0, "start":J
    int-to-long v2, p3

    add-long/2addr v2, v0

    iget-wide v4, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->length:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 273
    new-instance v2, Ljava/io/EOFException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "read past EOF: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 274
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    iget-wide v4, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->fileOffset:J

    add-long/2addr v4, v0

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 275
    iget-object v2, p0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, p2, p3, v3}, Lorg/apache/lucene/store/IndexInput;->readBytes([BIIZ)V

    .line 276
    return-void
.end method

.method protected seekInternal(J)V
    .locals 0
    .param p1, "pos"    # J

    .prologue
    .line 283
    return-void
.end method
