.class public abstract Lorg/apache/lucene/index/TermVectorMapper;
.super Ljava/lang/Object;
.source "TermVectorMapper.java"


# instance fields
.field private ignoringOffsets:Z

.field private ignoringPositions:Z


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method protected constructor <init>(ZZ)V
    .locals 0
    .param p1, "ignoringPositions"    # Z
    .param p2, "ignoringOffsets"    # Z

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-boolean p1, p0, Lorg/apache/lucene/index/TermVectorMapper;->ignoringPositions:Z

    .line 44
    iput-boolean p2, p0, Lorg/apache/lucene/index/TermVectorMapper;->ignoringOffsets:Z

    .line 45
    return-void
.end method


# virtual methods
.method public isIgnoringOffsets()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lorg/apache/lucene/index/TermVectorMapper;->ignoringOffsets:Z

    return v0
.end method

.method public isIgnoringPositions()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lorg/apache/lucene/index/TermVectorMapper;->ignoringPositions:Z

    return v0
.end method

.method public abstract map(Ljava/lang/String;I[Lorg/apache/lucene/index/TermVectorOffsetInfo;[I)V
.end method

.method public setDocumentNumber(I)V
    .locals 0
    .param p1, "documentNumber"    # I

    .prologue
    .line 99
    return-void
.end method

.method public abstract setExpectations(Ljava/lang/String;IZZ)V
.end method
