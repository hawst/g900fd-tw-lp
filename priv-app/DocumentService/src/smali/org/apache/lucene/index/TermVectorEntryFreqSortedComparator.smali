.class public Lorg/apache/lucene/index/TermVectorEntryFreqSortedComparator;
.super Ljava/lang/Object;
.source "TermVectorEntryFreqSortedComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/index/TermVectorEntry;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, Lorg/apache/lucene/index/TermVectorEntry;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/index/TermVectorEntry;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/TermVectorEntryFreqSortedComparator;->compare(Lorg/apache/lucene/index/TermVectorEntry;Lorg/apache/lucene/index/TermVectorEntry;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/index/TermVectorEntry;Lorg/apache/lucene/index/TermVectorEntry;)I
    .locals 3
    .param p1, "entry"    # Lorg/apache/lucene/index/TermVectorEntry;
    .param p2, "entry1"    # Lorg/apache/lucene/index/TermVectorEntry;

    .prologue
    .line 28
    const/4 v0, 0x0

    .line 29
    .local v0, "result":I
    invoke-virtual {p2}, Lorg/apache/lucene/index/TermVectorEntry;->getFrequency()I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/lucene/index/TermVectorEntry;->getFrequency()I

    move-result v2

    sub-int v0, v1, v2

    .line 30
    if-nez v0, :cond_0

    .line 32
    invoke-virtual {p1}, Lorg/apache/lucene/index/TermVectorEntry;->getTerm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lorg/apache/lucene/index/TermVectorEntry;->getTerm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 33
    if-nez v0, :cond_0

    .line 35
    invoke-virtual {p1}, Lorg/apache/lucene/index/TermVectorEntry;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lorg/apache/lucene/index/TermVectorEntry;->getField()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 38
    :cond_0
    return v0
.end method
