.class Lorg/apache/lucene/index/DirectoryReader$2;
.super Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;
.source "DirectoryReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/DirectoryReader;->doOpenNoWriter(ZLorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/index/DirectoryReader;

.field final synthetic val$openReadOnly:Z


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/DirectoryReader;Lorg/apache/lucene/store/Directory;Z)V
    .locals 0
    .param p2, "x0"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 479
    iput-object p1, p0, Lorg/apache/lucene/index/DirectoryReader$2;->this$0:Lorg/apache/lucene/index/DirectoryReader;

    iput-boolean p3, p0, Lorg/apache/lucene/index/DirectoryReader$2;->val$openReadOnly:Z

    invoke-direct {p0, p2}, Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;-><init>(Lorg/apache/lucene/store/Directory;)V

    return-void
.end method


# virtual methods
.method protected doBody(Ljava/lang/String;)Ljava/lang/Object;
    .locals 4
    .param p1, "segmentFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 480
    new-instance v0, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v0}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 481
    .local v0, "infos":Lorg/apache/lucene/index/SegmentInfos;
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$2;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    .line 482
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$2;->this$0:Lorg/apache/lucene/index/DirectoryReader;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lorg/apache/lucene/index/DirectoryReader$2;->val$openReadOnly:Z

    # invokes: Lorg/apache/lucene/index/DirectoryReader;->doOpenIfChanged(Lorg/apache/lucene/index/SegmentInfos;ZZ)Lorg/apache/lucene/index/DirectoryReader;
    invoke-static {v1, v0, v2, v3}, Lorg/apache/lucene/index/DirectoryReader;->access$000(Lorg/apache/lucene/index/DirectoryReader;Lorg/apache/lucene/index/SegmentInfos;ZZ)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v1

    return-object v1
.end method
