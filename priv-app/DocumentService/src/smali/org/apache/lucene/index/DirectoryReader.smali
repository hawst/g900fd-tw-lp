.class Lorg/apache/lucene/index/DirectoryReader;
.super Lorg/apache/lucene/index/IndexReader;
.source "DirectoryReader.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/DirectoryReader$MultiTermPositions;,
        Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;,
        Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;,
        Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final applyAllDeletes:Z

.field private deletionPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

.field protected directory:Lorg/apache/lucene/store/Directory;

.field private hasDeletions:Z

.field private maxDoc:I

.field private maxIndexVersion:J

.field private normsCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation
.end field

.field private numDocs:I

.field protected readOnly:Z

.field private rollbackHasChanges:Z

.field private final segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

.field private stale:Z

.field private starts:[I

.field private subReaders:[Lorg/apache/lucene/index/SegmentReader;

.field private final termInfosIndexDivisor:I

.field private writeLock:Lorg/apache/lucene/store/Lock;

.field writer:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DirectoryReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/SegmentInfos;IZ)V
    .locals 13
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p3, "termInfosIndexDivisor"    # I
    .param p4, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexReader;-><init>()V

    .line 58
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    iput-object v11, p0, Lorg/apache/lucene/index/DirectoryReader;->normsCache:Ljava/util/Map;

    .line 59
    const/4 v11, 0x0

    iput v11, p0, Lorg/apache/lucene/index/DirectoryReader;->maxDoc:I

    .line 60
    const/4 v11, -0x1

    iput v11, p0, Lorg/apache/lucene/index/DirectoryReader;->numDocs:I

    .line 61
    const/4 v11, 0x0

    iput-boolean v11, p0, Lorg/apache/lucene/index/DirectoryReader;->hasDeletions:Z

    .line 120
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriter;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    .line 121
    const/4 v11, 0x1

    iput-boolean v11, p0, Lorg/apache/lucene/index/DirectoryReader;->readOnly:Z

    .line 122
    move/from16 v0, p4

    iput-boolean v0, p0, Lorg/apache/lucene/index/DirectoryReader;->applyAllDeletes:Z

    .line 124
    move/from16 v0, p3

    iput v0, p0, Lorg/apache/lucene/index/DirectoryReader;->termInfosIndexDivisor:I

    .line 129
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v6

    .line 131
    .local v6, "numSegments":I
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 132
    .local v9, "readers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentReader;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriter;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v1

    .line 134
    .local v1, "dir":Lorg/apache/lucene/store/Directory;
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfos;->clone()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/index/SegmentInfos;

    iput-object v11, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    .line 135
    const/4 v5, 0x0

    .line 136
    .local v5, "infosUpto":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v6, :cond_5

    .line 137
    const/4 v7, 0x0

    .line 138
    .local v7, "prior":Ljava/io/IOException;
    const/4 v10, 0x0

    .line 140
    .local v10, "success":Z
    :try_start_0
    invoke-virtual {p2, v3}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v4

    .line 141
    .local v4, "info":Lorg/apache/lucene/index/SegmentInfo;
    sget-boolean v11, Lorg/apache/lucene/index/DirectoryReader;->$assertionsDisabled:Z

    if-nez v11, :cond_1

    iget-object v11, v4, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    if-eq v11, v1, :cond_1

    new-instance v11, Ljava/lang/AssertionError;

    invoke-direct {v11}, Ljava/lang/AssertionError;-><init>()V

    throw v11
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    .end local v4    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :catch_0
    move-exception v2

    .line 152
    .local v2, "ex":Ljava/io/IOException;
    move-object v7, v2

    .line 154
    if-nez v10, :cond_0

    .line 155
    .end local v2    # "ex":Ljava/io/IOException;
    :goto_1
    invoke-static {v7, v9}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;Ljava/lang/Iterable;)V

    .line 136
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 142
    .restart local v4    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_1
    :try_start_1
    iget-object v11, p1, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    const/4 v12, 0x1

    move/from16 v0, p3

    invoke-virtual {v11, v4, v12, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->getReadOnlyClone(Lorg/apache/lucene/index/SegmentInfo;ZI)Lorg/apache/lucene/index/SegmentReader;

    move-result-object v8

    .line 143
    .local v8, "reader":Lorg/apache/lucene/index/SegmentReader;
    invoke-virtual {v8}, Lorg/apache/lucene/index/SegmentReader;->numDocs()I

    move-result v11

    if-gtz v11, :cond_2

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriter;->getKeepFullyDeletedSegments()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 144
    :cond_2
    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    add-int/lit8 v5, v5, 0x1

    .line 150
    :goto_2
    const/4 v10, 0x1

    .line 154
    if-nez v10, :cond_0

    goto :goto_1

    .line 147
    :cond_3
    invoke-virtual {v8}, Lorg/apache/lucene/index/SegmentReader;->close()V

    .line 148
    iget-object v11, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v11, v5}, Lorg/apache/lucene/index/SegmentInfos;->remove(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 154
    .end local v4    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v8    # "reader":Lorg/apache/lucene/index/SegmentReader;
    :catchall_0
    move-exception v11

    if-nez v10, :cond_4

    .line 155
    invoke-static {v7, v9}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;Ljava/lang/Iterable;)V

    .line 154
    :cond_4
    throw v11

    .line 159
    .end local v7    # "prior":Ljava/io/IOException;
    .end local v10    # "success":Z
    :cond_5
    iput-object p1, p0, Lorg/apache/lucene/index/DirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 161
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v11

    new-array v11, v11, [Lorg/apache/lucene/index/SegmentReader;

    invoke-interface {v9, v11}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Lorg/apache/lucene/index/SegmentReader;

    invoke-direct {p0, v11}, Lorg/apache/lucene/index/DirectoryReader;->initialize([Lorg/apache/lucene/index/SegmentReader;)V

    .line 162
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/IndexDeletionPolicy;ZI)V
    .locals 7
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "sis"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p3, "deletionPolicy"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .param p4, "readOnly"    # Z
    .param p5, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 86
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexReader;-><init>()V

    .line 58
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lorg/apache/lucene/index/DirectoryReader;->normsCache:Ljava/util/Map;

    .line 59
    iput v6, p0, Lorg/apache/lucene/index/DirectoryReader;->maxDoc:I

    .line 60
    const/4 v5, -0x1

    iput v5, p0, Lorg/apache/lucene/index/DirectoryReader;->numDocs:I

    .line 61
    iput-boolean v6, p0, Lorg/apache/lucene/index/DirectoryReader;->hasDeletions:Z

    .line 87
    iput-object p1, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    .line 88
    iput-boolean p4, p0, Lorg/apache/lucene/index/DirectoryReader;->readOnly:Z

    .line 89
    iput-object p2, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    .line 90
    iput-object p3, p0, Lorg/apache/lucene/index/DirectoryReader;->deletionPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    .line 91
    iput p5, p0, Lorg/apache/lucene/index/DirectoryReader;->termInfosIndexDivisor:I

    .line 93
    iput-boolean v6, p0, Lorg/apache/lucene/index/DirectoryReader;->applyAllDeletes:Z

    .line 100
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v5

    new-array v3, v5, [Lorg/apache/lucene/index/SegmentReader;

    .line 101
    .local v3, "readers":[Lorg/apache/lucene/index/SegmentReader;
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_2

    .line 102
    const/4 v2, 0x0

    .line 103
    .local v2, "prior":Ljava/io/IOException;
    const/4 v4, 0x0

    .line 105
    .local v4, "success":Z
    :try_start_0
    invoke-virtual {p2, v1}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v5

    invoke-static {p4, v5, p5}, Lorg/apache/lucene/index/SegmentReader;->get(ZLorg/apache/lucene/index/SegmentInfo;I)Lorg/apache/lucene/index/SegmentReader;

    move-result-object v5

    aput-object v5, v3, v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    const/4 v4, 0x1

    .line 110
    if-nez v4, :cond_0

    .line 111
    :goto_1
    invoke-static {v2, v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    .line 101
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "ex":Ljava/io/IOException;
    move-object v2, v0

    .line 110
    if-nez v4, :cond_0

    goto :goto_1

    .end local v0    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    if-nez v4, :cond_1

    .line 111
    invoke-static {v2, v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    .line 110
    :cond_1
    throw v5

    .line 115
    .end local v2    # "prior":Ljava/io/IOException;
    .end local v4    # "success":Z
    :cond_2
    invoke-direct {p0, v3}, Lorg/apache/lucene/index/DirectoryReader;->initialize([Lorg/apache/lucene/index/SegmentReader;)V

    .line 116
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;[Lorg/apache/lucene/index/SegmentReader;[ILjava/util/Map;ZZI)V
    .locals 22
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p3, "oldReaders"    # [Lorg/apache/lucene/index/SegmentReader;
    .param p4, "oldStarts"    # [I
    .param p6, "readOnly"    # Z
    .param p7, "doClone"    # Z
    .param p8, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "[",
            "Lorg/apache/lucene/index/SegmentReader;",
            "[I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;ZZI)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    .local p5, "oldNormsCache":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/IndexReader;-><init>()V

    .line 58
    new-instance v18, Ljava/util/HashMap;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/DirectoryReader;->normsCache:Ljava/util/Map;

    .line 59
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/index/DirectoryReader;->maxDoc:I

    .line 60
    const/16 v18, -0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/index/DirectoryReader;->numDocs:I

    .line 61
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/lucene/index/DirectoryReader;->hasDeletions:Z

    .line 167
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    .line 168
    move/from16 v0, p6

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/lucene/index/DirectoryReader;->readOnly:Z

    .line 169
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    .line 170
    move/from16 v0, p8

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/index/DirectoryReader;->termInfosIndexDivisor:I

    .line 171
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/lucene/index/DirectoryReader;->applyAllDeletes:Z

    .line 175
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 177
    .local v16, "segmentReaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    if-eqz p3, :cond_0

    .line 179
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v8, v0, :cond_0

    .line 180
    aget-object v18, p3, v8

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/SegmentReader;->getSegmentName()Ljava/lang/String;

    move-result-object v18

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 184
    .end local v8    # "i":I
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v11, v0, [Lorg/apache/lucene/index/SegmentReader;

    .line 188
    .local v11, "newReaders":[Lorg/apache/lucene/index/SegmentReader;
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v15, v0, [Z

    .line 190
    .local v15, "readerShared":[Z
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v18

    add-int/lit8 v8, v18, -0x1

    .restart local v8    # "i":I
    :goto_1
    if-ltz v8, :cond_b

    .line 192
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    .line 193
    .local v13, "oldReaderIndex":Ljava/lang/Integer;
    if-nez v13, :cond_3

    .line 195
    const/16 v18, 0x0

    aput-object v18, v11, v8

    .line 201
    :goto_2
    const/4 v14, 0x0

    .line 202
    .local v14, "prior":Ljava/io/IOException;
    const/16 v17, 0x0

    .line 205
    .local v17, "success":Z
    :try_start_0
    aget-object v18, v11, v8

    if-eqz v18, :cond_1

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v18

    aget-object v19, v11, v8

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/SegmentReader;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_6

    .line 208
    :cond_1
    sget-boolean v18, Lorg/apache/lucene/index/DirectoryReader;->$assertionsDisabled:Z

    if-nez v18, :cond_4

    if-eqz p7, :cond_4

    new-instance v18, Ljava/lang/AssertionError;

    invoke-direct/range {v18 .. v18}, Ljava/lang/AssertionError;-><init>()V

    throw v18
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    :catch_0
    move-exception v6

    .line 229
    .local v6, "ex":Ljava/io/IOException;
    move-object v14, v6

    .line 231
    if-nez v17, :cond_11

    .line 232
    add-int/lit8 v8, v8, 0x1

    :goto_3
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v8, v0, :cond_11

    .line 233
    aget-object v18, v11, v8

    if-eqz v18, :cond_2

    .line 235
    :try_start_1
    aget-boolean v18, v15, v8

    if-nez v18, :cond_12

    .line 238
    aget-object v18, v11, v8

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/SegmentReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 232
    :cond_2
    :goto_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 198
    .end local v6    # "ex":Ljava/io/IOException;
    .end local v14    # "prior":Ljava/io/IOException;
    .end local v17    # "success":Z
    :cond_3
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v18

    aget-object v18, p3, v18

    aput-object v18, v11, v8

    goto :goto_2

    .line 211
    .restart local v14    # "prior":Ljava/io/IOException;
    .restart local v17    # "success":Z
    :cond_4
    :try_start_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v18

    move/from16 v0, p6

    move-object/from16 v1, v18

    move/from16 v2, p8

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/index/SegmentReader;->get(ZLorg/apache/lucene/index/SegmentInfo;I)Lorg/apache/lucene/index/SegmentReader;

    move-result-object v10

    .line 212
    .local v10, "newReader":Lorg/apache/lucene/index/SegmentReader;
    const/16 v18, 0x0

    aput-boolean v18, v15, v8

    .line 213
    aput-object v10, v11, v8
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 227
    :goto_5
    const/16 v17, 0x1

    .line 231
    if-nez v17, :cond_15

    .line 232
    add-int/lit8 v8, v8, 0x1

    :goto_6
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v8, v0, :cond_15

    .line 233
    aget-object v18, v11, v8

    if-eqz v18, :cond_5

    .line 235
    :try_start_3
    aget-boolean v18, v15, v8

    if-nez v18, :cond_16

    .line 238
    aget-object v18, v11, v8

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/SegmentReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 232
    :cond_5
    :goto_7
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    .line 215
    .end local v10    # "newReader":Lorg/apache/lucene/index/SegmentReader;
    :cond_6
    :try_start_4
    aget-object v18, v11, v8

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move/from16 v2, p7

    move/from16 v3, p6

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/index/SegmentReader;->reopenSegment(Lorg/apache/lucene/index/SegmentInfo;ZZ)Lorg/apache/lucene/index/SegmentReader;

    move-result-object v10

    .line 216
    .restart local v10    # "newReader":Lorg/apache/lucene/index/SegmentReader;
    if-nez v10, :cond_8

    .line 219
    const/16 v18, 0x1

    aput-boolean v18, v15, v8

    .line 220
    aget-object v18, v11, v8

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/SegmentReader;->incRef()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_5

    .line 231
    .end local v10    # "newReader":Lorg/apache/lucene/index/SegmentReader;
    :catchall_0
    move-exception v18

    if-nez v17, :cond_13

    .line 232
    add-int/lit8 v8, v8, 0x1

    :goto_8
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v8, v0, :cond_13

    .line 233
    aget-object v19, v11, v8

    if-eqz v19, :cond_7

    .line 235
    :try_start_5
    aget-boolean v19, v15, v8

    if-nez v19, :cond_14

    .line 238
    aget-object v19, v11, v8

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/SegmentReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 232
    :cond_7
    :goto_9
    add-int/lit8 v8, v8, 0x1

    goto :goto_8

    .line 222
    .restart local v10    # "newReader":Lorg/apache/lucene/index/SegmentReader;
    :cond_8
    const/16 v18, 0x0

    :try_start_6
    aput-boolean v18, v15, v8

    .line 224
    aput-object v10, v11, v8
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_5

    .line 231
    .end local v10    # "newReader":Lorg/apache/lucene/index/SegmentReader;
    :cond_9
    throw v18

    .line 190
    :cond_a
    add-int/lit8 v8, v8, -0x1

    goto/16 :goto_1

    .line 256
    .end local v13    # "oldReaderIndex":Ljava/lang/Integer;
    .end local v14    # "prior":Ljava/io/IOException;
    .end local v17    # "success":Z
    :cond_b
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lorg/apache/lucene/index/DirectoryReader;->initialize([Lorg/apache/lucene/index/SegmentReader;)V

    .line 259
    if-eqz p5, :cond_10

    .line 260
    invoke-interface/range {p5 .. p5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_c
    :goto_a
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_10

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 261
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[B>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 262
    .local v7, "field":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/DirectoryReader;->hasNorms(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_c

    .line 266
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [B

    .line 268
    .local v12, "oldBytes":[B
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DirectoryReader;->maxDoc()I

    move-result v18

    move/from16 v0, v18

    new-array v4, v0, [B

    .line 270
    .local v4, "bytes":[B
    const/4 v8, 0x0

    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v8, v0, :cond_f

    .line 271
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    move-object/from16 v18, v0

    aget-object v18, v18, v8

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/SegmentReader;->getSegmentName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    .line 274
    .restart local v13    # "oldReaderIndex":Ljava/lang/Integer;
    if-eqz v13, :cond_e

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v18

    aget-object v18, p3, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    move-object/from16 v19, v0

    aget-object v19, v19, v8

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_d

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v18

    aget-object v18, p3, v18

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    move-object/from16 v19, v0

    aget-object v19, v19, v8

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_e

    .line 280
    :cond_d
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v18

    aget v18, p4, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    move-object/from16 v19, v0

    aget v19, v19, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    move-object/from16 v20, v0

    add-int/lit8 v21, v8, 0x1

    aget v20, v20, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    move-object/from16 v21, v0

    aget v21, v21, v8

    sub-int v20, v20, v21

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v12, v0, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 270
    :goto_c
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_b

    .line 282
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    move-object/from16 v18, v0

    aget-object v18, v18, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    move-object/from16 v19, v0

    aget v19, v19, v8

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v7, v4, v1}, Lorg/apache/lucene/index/SegmentReader;->norms(Ljava/lang/String;[BI)V

    goto :goto_c

    .line 286
    .end local v13    # "oldReaderIndex":Ljava/lang/Integer;
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DirectoryReader;->normsCache:Ljava/util/Map;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_a

    .line 289
    .end local v4    # "bytes":[B
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[B>;"
    .end local v7    # "field":Ljava/lang/String;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v12    # "oldBytes":[B
    :cond_10
    return-void

    .line 251
    .restart local v6    # "ex":Ljava/io/IOException;
    .restart local v13    # "oldReaderIndex":Ljava/lang/Integer;
    .restart local v14    # "prior":Ljava/io/IOException;
    .restart local v17    # "success":Z
    :cond_11
    if-eqz v14, :cond_a

    throw v14

    .line 244
    :catch_1
    move-exception v6

    .line 245
    if-nez v14, :cond_2

    move-object v14, v6

    goto/16 :goto_4

    .line 242
    :cond_12
    :try_start_7
    aget-object v18, v11, v8

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/SegmentReader;->decRef()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_4

    .line 251
    .end local v6    # "ex":Ljava/io/IOException;
    :cond_13
    if-eqz v14, :cond_9

    throw v14

    .line 244
    :catch_2
    move-exception v6

    .line 245
    .restart local v6    # "ex":Ljava/io/IOException;
    if-nez v14, :cond_7

    move-object v14, v6

    goto/16 :goto_9

    .line 242
    .end local v6    # "ex":Ljava/io/IOException;
    :cond_14
    :try_start_8
    aget-object v19, v11, v8

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/SegmentReader;->decRef()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_9

    .line 251
    .restart local v10    # "newReader":Lorg/apache/lucene/index/SegmentReader;
    :cond_15
    if-eqz v14, :cond_a

    throw v14

    .line 244
    :catch_3
    move-exception v6

    .line 245
    .restart local v6    # "ex":Ljava/io/IOException;
    if-nez v14, :cond_5

    move-object v14, v6

    goto/16 :goto_7

    .line 242
    .end local v6    # "ex":Ljava/io/IOException;
    :cond_16
    :try_start_9
    aget-object v18, v11, v8

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/SegmentReader;->decRef()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto/16 :goto_7
.end method

.method static synthetic access$000(Lorg/apache/lucene/index/DirectoryReader;Lorg/apache/lucene/index/SegmentInfos;ZZ)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/DirectoryReader;
    .param p1, "x1"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/index/DirectoryReader;->doOpenIfChanged(Lorg/apache/lucene/index/SegmentInfos;ZZ)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method private final doOpenFromWriter(ZLorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;
    .locals 6
    .param p1, "openReadOnly"    # Z
    .param p2, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 399
    sget-boolean v2, Lorg/apache/lucene/index/DirectoryReader;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lorg/apache/lucene/index/DirectoryReader;->readOnly:Z

    if-nez v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 401
    :cond_0
    if-nez p1, :cond_1

    .line 402
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "a reader obtained from IndexWriter.getReader() can only be reopened with openReadOnly=true (got false)"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 405
    :cond_1
    if-eqz p2, :cond_2

    .line 406
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "a reader obtained from IndexWriter.getReader() cannot currently accept a commit"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 409
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexWriter;->nrtIsCurrent(Lorg/apache/lucene/index/SegmentInfos;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object v0, v1

    .line 421
    :cond_3
    :goto_0
    return-object v0

    .line 413
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    iget-boolean v3, p0, Lorg/apache/lucene/index/DirectoryReader;->applyAllDeletes:Z

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexWriter;->getReader(Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 416
    .local v0, "reader":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->getVersion()J

    move-result-wide v2

    iget-object v4, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfos;->getVersion()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 417
    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    move-object v0, v1

    .line 418
    goto :goto_0
.end method

.method private declared-synchronized doOpenIfChanged(Lorg/apache/lucene/index/SegmentInfos;ZZ)Lorg/apache/lucene/index/DirectoryReader;
    .locals 9
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "doClone"    # Z
    .param p3, "openReadOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 489
    monitor-enter p0

    if-eqz p3, :cond_0

    .line 490
    :try_start_0
    new-instance v0, Lorg/apache/lucene/index/ReadOnlyDirectoryReader;

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    iget-object v4, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    iget-object v5, p0, Lorg/apache/lucene/index/DirectoryReader;->normsCache:Ljava/util/Map;

    iget v7, p0, Lorg/apache/lucene/index/DirectoryReader;->termInfosIndexDivisor:I

    move-object v2, p1

    move v6, p2

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/index/ReadOnlyDirectoryReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;[Lorg/apache/lucene/index/SegmentReader;[ILjava/util/Map;ZI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 494
    .local v0, "reader":Lorg/apache/lucene/index/DirectoryReader;
    :goto_0
    monitor-exit p0

    return-object v0

    .line 492
    .end local v0    # "reader":Lorg/apache/lucene/index/DirectoryReader;
    :cond_0
    :try_start_1
    new-instance v0, Lorg/apache/lucene/index/DirectoryReader;

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    iget-object v4, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    iget-object v5, p0, Lorg/apache/lucene/index/DirectoryReader;->normsCache:Ljava/util/Map;

    const/4 v6, 0x0

    iget v8, p0, Lorg/apache/lucene/index/DirectoryReader;->termInfosIndexDivisor:I

    move-object v2, p1

    move v7, p2

    invoke-direct/range {v0 .. v8}, Lorg/apache/lucene/index/DirectoryReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfos;[Lorg/apache/lucene/index/SegmentReader;[ILjava/util/Map;ZZI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v0    # "reader":Lorg/apache/lucene/index/DirectoryReader;
    goto :goto_0

    .line 489
    .end local v0    # "reader":Lorg/apache/lucene/index/DirectoryReader;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private doOpenIfChanged(ZLorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .param p1, "openReadOnly"    # Z
    .param p2, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 425
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 427
    sget-boolean v0, Lorg/apache/lucene/index/DirectoryReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 431
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v0, :cond_1

    .line 432
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/index/DirectoryReader;->doOpenFromWriter(ZLorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 434
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/index/DirectoryReader;->doOpenNoWriter(ZLorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    goto :goto_0
.end method

.method private declared-synchronized doOpenNoWriter(ZLorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;
    .locals 3
    .param p1, "openReadOnly"    # Z
    .param p2, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 440
    monitor-enter p0

    if-nez p2, :cond_5

    .line 441
    :try_start_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->hasChanges:Z

    if-eqz v1, :cond_4

    .line 443
    sget-boolean v1, Lorg/apache/lucene/index/DirectoryReader;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->readOnly:Z

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 440
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 445
    :cond_0
    :try_start_1
    sget-boolean v1, Lorg/apache/lucene/index/DirectoryReader;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->writeLock:Lorg/apache/lucene/store/Lock;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 448
    :cond_1
    sget-boolean v1, Lorg/apache/lucene/index/DirectoryReader;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->isCurrent()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 450
    :cond_2
    if-eqz p1, :cond_3

    .line 451
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->clone(Z)Lorg/apache/lucene/index/IndexReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 477
    :cond_3
    :goto_0
    monitor-exit p0

    return-object v0

    .line 455
    :cond_4
    :try_start_2
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->isCurrent()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 456
    iget-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->readOnly:Z

    if-eq p1, v1, :cond_3

    .line 458
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->clone(Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    goto :goto_0

    .line 464
    :cond_5
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v2

    if-eq v1, v2, :cond_6

    .line 465
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "the specified commit does not match the specified Directory"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 467
    :cond_6
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    if-eqz v1, :cond_7

    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 468
    iget-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->readOnly:Z

    if-eq v1, p1, :cond_3

    .line 470
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->clone(Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    goto :goto_0

    .line 477
    :cond_7
    new-instance v0, Lorg/apache/lucene/index/DirectoryReader$2;

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    invoke-direct {v0, p0, v1, p1}, Lorg/apache/lucene/index/DirectoryReader$2;-><init>(Lorg/apache/lucene/index/DirectoryReader;Lorg/apache/lucene/store/Directory;Z)V

    invoke-virtual {v0, p2}, Lorg/apache/lucene/index/DirectoryReader$2;->run(Lorg/apache/lucene/index/IndexCommit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private initialize([Lorg/apache/lucene/index/SegmentReader;)V
    .locals 5
    .param p1, "subReaders"    # [Lorg/apache/lucene/index/SegmentReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 321
    iput-object p1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    .line 322
    array-length v2, p1

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [I

    iput-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    .line 323
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 324
    aget-object v1, p1, v0

    .line 325
    .local v1, "reader":Lorg/apache/lucene/index/SegmentReader;
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    iget v3, p0, Lorg/apache/lucene/index/DirectoryReader;->maxDoc:I

    aput v3, v2, v0

    .line 326
    iget v2, p0, Lorg/apache/lucene/index/DirectoryReader;->maxDoc:I

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lorg/apache/lucene/index/DirectoryReader;->maxDoc:I

    .line 327
    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentReader;->hasDeletions()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 328
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/index/DirectoryReader;->hasDeletions:Z

    .line 323
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 331
    .end local v1    # "reader":Lorg/apache/lucene/index/SegmentReader;
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    array-length v3, p1

    iget v4, p0, Lorg/apache/lucene/index/DirectoryReader;->maxDoc:I

    aput v4, v2, v3

    .line 333
    iget-boolean v2, p0, Lorg/apache/lucene/index/DirectoryReader;->readOnly:Z

    if-nez v2, :cond_2

    .line 334
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    invoke-static {v2}, Lorg/apache/lucene/index/SegmentInfos;->readCurrentVersion(Lorg/apache/lucene/store/Directory;)J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/index/DirectoryReader;->maxIndexVersion:J

    .line 336
    :cond_2
    return-void
.end method

.method public static listCommits(Lorg/apache/lucene/store/Directory;)Ljava/util/Collection;
    .locals 12
    .param p0, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 953
    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v4

    .line 955
    .local v4, "files":[Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 957
    .local v0, "commits":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexCommit;>;"
    new-instance v7, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v7}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 958
    .local v7, "latest":Lorg/apache/lucene/index/SegmentInfos;
    invoke-virtual {v7, p0}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V

    .line 959
    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v2

    .line 961
    .local v2, "currentGen":J
    new-instance v9, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;

    invoke-direct {v9, v7, p0}, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;-><init>(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/store/Directory;)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 963
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    array-length v9, v4

    if-ge v6, v9, :cond_1

    .line 965
    aget-object v1, v4, v6

    .line 967
    .local v1, "fileName":Ljava/lang/String;
    const-string/jumbo v9, "segments"

    invoke-virtual {v1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string/jumbo v9, "segments.gen"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    invoke-static {v1}, Lorg/apache/lucene/index/SegmentInfos;->generationFromSegmentsFileName(Ljava/lang/String;)J

    move-result-wide v10

    cmp-long v9, v10, v2

    if-gez v9, :cond_0

    .line 971
    new-instance v8, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v8}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 975
    .local v8, "sis":Lorg/apache/lucene/index/SegmentInfos;
    :try_start_0
    invoke-virtual {v8, p0, v1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 987
    :goto_1
    if-eqz v8, :cond_0

    .line 988
    new-instance v9, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;

    invoke-direct {v9, v8, p0}, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;-><init>(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/store/Directory;)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 963
    .end local v8    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 976
    .restart local v8    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :catch_0
    move-exception v5

    .line 984
    .local v5, "fnfe":Ljava/io/FileNotFoundException;
    const/4 v8, 0x0

    goto :goto_1

    .line 993
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v5    # "fnfe":Ljava/io/FileNotFoundException;
    .end local v8    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 995
    return-object v0
.end method

.method static open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;ZI)Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p1, "deletionPolicy"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .param p2, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .param p3, "readOnly"    # Z
    .param p4, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Lorg/apache/lucene/index/DirectoryReader$1;

    invoke-direct {v0, p0, p3, p1, p4}, Lorg/apache/lucene/index/DirectoryReader$1;-><init>(Lorg/apache/lucene/store/Directory;ZLorg/apache/lucene/index/IndexDeletionPolicy;I)V

    invoke-virtual {v0, p2}, Lorg/apache/lucene/index/DirectoryReader$1;->run(Lorg/apache/lucene/index/IndexCommit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;

    return-object v0
.end method

.method private readerIndex(I)I
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 604
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v1, v1

    invoke-static {p1, v0, v1}, Lorg/apache/lucene/index/DirectoryReader;->readerIndex(I[II)I

    move-result v0

    return v0
.end method

.method static final readerIndex(I[II)I
    .locals 5
    .param p0, "n"    # I
    .param p1, "starts"    # [I
    .param p2, "numSubReaders"    # I

    .prologue
    .line 608
    const/4 v1, 0x0

    .line 609
    .local v1, "lo":I
    add-int/lit8 v0, p2, -0x1

    .line 611
    .local v0, "hi":I
    :goto_0
    if-lt v0, v1, :cond_2

    .line 612
    add-int v4, v1, v0

    ushr-int/lit8 v2, v4, 0x1

    .line 613
    .local v2, "mid":I
    aget v3, p1, v2

    .line 614
    .local v3, "midValue":I
    if-ge p0, v3, :cond_0

    .line 615
    add-int/lit8 v0, v2, -0x1

    goto :goto_0

    .line 616
    :cond_0
    if-le p0, v3, :cond_1

    .line 617
    add-int/lit8 v1, v2, 0x1

    goto :goto_0

    .line 619
    :cond_1
    :goto_1
    add-int/lit8 v4, v2, 0x1

    if-ge v4, p2, :cond_3

    add-int/lit8 v4, v2, 0x1

    aget v4, p1, v4

    if-ne v4, v3, :cond_3

    .line 620
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v2    # "mid":I
    .end local v3    # "midValue":I
    :cond_2
    move v2, v0

    .line 625
    :cond_3
    return v2
.end method


# virtual methods
.method protected acquireWriteLock()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/StaleReaderException;,
            Lorg/apache/lucene/index/CorruptIndexException;,
            Lorg/apache/lucene/store/LockObtainFailedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 759
    iget-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->readOnly:Z

    if-eqz v1, :cond_0

    .line 763
    invoke-static {}, Lorg/apache/lucene/index/ReadOnlySegmentReader;->noWrite()V

    .line 766
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    if-eqz v1, :cond_3

    .line 767
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 768
    iget-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->stale:Z

    if-eqz v1, :cond_1

    .line 769
    new-instance v1, Lorg/apache/lucene/index/StaleReaderException;

    const-string/jumbo v2, "IndexReader out of date and no longer valid for delete, undelete, or setNorm operations"

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/StaleReaderException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 771
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->writeLock:Lorg/apache/lucene/store/Lock;

    if-nez v1, :cond_3

    .line 772
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    const-string/jumbo v2, "write.lock"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/Directory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v0

    .line 773
    .local v0, "writeLock":Lorg/apache/lucene/store/Lock;
    sget-wide v2, Lorg/apache/lucene/index/IndexWriterConfig;->WRITE_LOCK_TIMEOUT:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/Lock;->obtain(J)Z

    move-result v1

    if-nez v1, :cond_2

    .line 774
    new-instance v1, Lorg/apache/lucene/store/LockObtainFailedException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Index locked for write: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/store/LockObtainFailedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 775
    :cond_2
    iput-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->writeLock:Lorg/apache/lucene/store/Lock;

    .line 780
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    invoke-static {v1}, Lorg/apache/lucene/index/SegmentInfos;->readCurrentVersion(Lorg/apache/lucene/store/Directory;)J

    move-result-wide v2

    iget-wide v4, p0, Lorg/apache/lucene/index/DirectoryReader;->maxIndexVersion:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    .line 781
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->stale:Z

    .line 782
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->writeLock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v1}, Lorg/apache/lucene/store/Lock;->release()V

    .line 783
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->writeLock:Lorg/apache/lucene/store/Lock;

    .line 784
    new-instance v1, Lorg/apache/lucene/index/StaleReaderException;

    const-string/jumbo v2, "IndexReader out of date and no longer valid for delete, undelete, or setNorm operations"

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/StaleReaderException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 788
    .end local v0    # "writeLock":Lorg/apache/lucene/store/Lock;
    :cond_3
    return-void
.end method

.method public final declared-synchronized clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 341
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->readOnly:Z

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/DirectoryReader;->clone(Z)Lorg/apache/lucene/index/IndexReader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    .line 342
    :catch_0
    move-exception v0

    .line 343
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 341
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized clone(Z)Lorg/apache/lucene/index/IndexReader;
    .locals 3
    .param p1, "openReadOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 351
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfos;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfos;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2, p1}, Lorg/apache/lucene/index/DirectoryReader;->doOpenIfChanged(Lorg/apache/lucene/index/SegmentInfos;ZZ)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    .line 353
    .local v0, "newReader":Lorg/apache/lucene/index/DirectoryReader;
    if-eq p0, v0, :cond_0

    .line 354
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->deletionPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    iput-object v1, v0, Lorg/apache/lucene/index/DirectoryReader;->deletionPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    .line 356
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    iput-object v1, v0, Lorg/apache/lucene/index/DirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 359
    if-nez p1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->writeLock:Lorg/apache/lucene/store/Lock;

    if-eqz v1, :cond_2

    .line 361
    sget-boolean v1, Lorg/apache/lucene/index/DirectoryReader;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 351
    .end local v0    # "newReader":Lorg/apache/lucene/index/DirectoryReader;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 362
    .restart local v0    # "newReader":Lorg/apache/lucene/index/DirectoryReader;
    :cond_1
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->writeLock:Lorg/apache/lucene/store/Lock;

    iput-object v1, v0, Lorg/apache/lucene/index/DirectoryReader;->writeLock:Lorg/apache/lucene/store/Lock;

    .line 363
    iget-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->hasChanges:Z

    iput-boolean v1, v0, Lorg/apache/lucene/index/DirectoryReader;->hasChanges:Z

    .line 364
    iget-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->hasDeletions:Z

    iput-boolean v1, v0, Lorg/apache/lucene/index/DirectoryReader;->hasDeletions:Z

    .line 365
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->writeLock:Lorg/apache/lucene/store/Lock;

    .line 366
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->hasChanges:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 369
    :cond_2
    monitor-exit p0

    return-object v0
.end method

.method public directory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 931
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method protected declared-synchronized doClose()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 899
    monitor-enter p0

    const/4 v2, 0x0

    .line 900
    .local v2, "ioe":Ljava/io/IOException;
    const/4 v3, 0x0

    :try_start_0
    iput-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->normsCache:Ljava/util/Map;

    .line 901
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v3, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v1, v3, :cond_1

    .line 904
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentReader;->decRef()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 901
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 905
    :catch_0
    move-exception v0

    .line 906
    .local v0, "e":Ljava/io/IOException;
    if-nez v2, :cond_0

    move-object v2, v0

    goto :goto_1

    .line 910
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_2
    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v3, :cond_2

    .line 913
    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriter;->deletePendingFiles()V

    .line 917
    :cond_2
    if-eqz v2, :cond_3

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 899
    .end local v1    # "i":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 918
    .restart local v1    # "i":I
    :cond_3
    monitor-exit p0

    return-void
.end method

.method protected doCommit(Ljava/util/Map;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .local p1, "commitUserData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v4, 0x0

    const/4 v9, 0x0

    .line 801
    iget-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->hasChanges:Z

    if-eqz v1, :cond_3

    .line 802
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/SegmentInfos;->setUserData(Ljava/util/Map;)V

    .line 805
    new-instance v0, Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->deletionPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    if-nez v2, :cond_0

    new-instance v2, Lorg/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy;

    invoke-direct {v2}, Lorg/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy;-><init>()V

    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/IndexFileDeleter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/SegmentInfos;Ljava/io/PrintStream;Lorg/apache/lucene/index/IndexWriter;)V

    .line 808
    .local v0, "deleter":Lorg/apache/lucene/index/IndexFileDeleter;
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter;->getLastSegmentInfos()Lorg/apache/lucene/index/SegmentInfos;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/SegmentInfos;->updateGeneration(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 809
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfos;->changed()V

    .line 813
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->startCommit()V

    .line 815
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1, v9}, Lorg/apache/lucene/index/SegmentInfos;->createBackupSegmentInfos(Z)Ljava/util/List;

    move-result-object v7

    .line 817
    .local v7, "rollbackSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    const/4 v8, 0x0

    .line 819
    .local v8, "success":Z
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v1, v1

    if-ge v6, v1, :cond_1

    .line 820
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v1, v1, v6

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentReader;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 819
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 805
    .end local v0    # "deleter":Lorg/apache/lucene/index/IndexFileDeleter;
    .end local v6    # "i":I
    .end local v7    # "rollbackSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    .end local v8    # "success":Z
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->deletionPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    goto :goto_0

    .line 824
    .restart local v0    # "deleter":Lorg/apache/lucene/index/IndexFileDeleter;
    .restart local v6    # "i":I
    .restart local v7    # "rollbackSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    .restart local v8    # "success":Z
    :cond_1
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfos;->pruneDeletedSegments()V

    .line 827
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V

    .line 828
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/SegmentInfos;->commit(Lorg/apache/lucene/store/Directory;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 829
    const/4 v8, 0x1

    .line 832
    if-nez v8, :cond_2

    .line 839
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->rollbackCommit()V

    .line 844
    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh()V

    .line 847
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1, v7}, Lorg/apache/lucene/index/SegmentInfos;->rollbackSegmentInfos(Ljava/util/List;)V

    .line 853
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 854
    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter;->close()V

    .line 856
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfos;->getVersion()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/index/DirectoryReader;->maxIndexVersion:J

    .line 858
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->writeLock:Lorg/apache/lucene/store/Lock;

    if-eqz v1, :cond_3

    .line 859
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->writeLock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v1}, Lorg/apache/lucene/store/Lock;->release()V

    .line 860
    iput-object v4, p0, Lorg/apache/lucene/index/DirectoryReader;->writeLock:Lorg/apache/lucene/store/Lock;

    .line 863
    .end local v0    # "deleter":Lorg/apache/lucene/index/IndexFileDeleter;
    .end local v6    # "i":I
    .end local v7    # "rollbackSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    .end local v8    # "success":Z
    :cond_3
    iput-boolean v9, p0, Lorg/apache/lucene/index/DirectoryReader;->hasChanges:Z

    .line 864
    return-void

    .line 832
    .restart local v0    # "deleter":Lorg/apache/lucene/index/IndexFileDeleter;
    .restart local v6    # "i":I
    .restart local v7    # "rollbackSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    .restart local v8    # "success":Z
    :catchall_0
    move-exception v1

    if-nez v8, :cond_4

    .line 839
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->rollbackCommit()V

    .line 844
    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh()V

    .line 847
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, v7}, Lorg/apache/lucene/index/SegmentInfos;->rollbackSegmentInfos(Ljava/util/List;)V

    .line 832
    :cond_4
    throw v1
.end method

.method protected doDelete(I)V
    .locals 3
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 587
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/index/DirectoryReader;->numDocs:I

    .line 588
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->readerIndex(I)I

    move-result v0

    .line 589
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/SegmentReader;->deleteDocument(I)V

    .line 590
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->hasDeletions:Z

    .line 591
    return-void
.end method

.method protected final doOpenIfChanged()Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 375
    iget-boolean v0, p0, Lorg/apache/lucene/index/DirectoryReader;->readOnly:Z

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/DirectoryReader;->doOpenIfChanged(ZLorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method protected final doOpenIfChanged(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 386
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/index/DirectoryReader;->doOpenIfChanged(ZLorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method protected final doOpenIfChanged(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 391
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/index/DirectoryReader;->applyAllDeletes:Z

    if-ne p2, v0, :cond_0

    .line 392
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->doOpenIfChanged()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 394
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/index/IndexReader;->doOpenIfChanged(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    goto :goto_0
.end method

.method protected final doOpenIfChanged(Z)Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .param p1, "openReadOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 381
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/DirectoryReader;->doOpenIfChanged(ZLorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method protected doSetNorm(ILjava/lang/String;B)V
    .locals 3
    .param p1, "n"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "value"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 673
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->normsCache:Ljava/util/Map;

    monitor-enter v2

    .line 674
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->normsCache:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 675
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 676
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->readerIndex(I)I

    move-result v0

    .line 677
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2, p2, p3}, Lorg/apache/lucene/index/SegmentReader;->setNorm(ILjava/lang/String;B)V

    .line 678
    return-void

    .line 675
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method protected doUndeleteAll()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 596
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 597
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentReader;->undeleteAll()V

    .line 596
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 599
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->hasDeletions:Z

    .line 600
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/index/DirectoryReader;->numDocs:I

    .line 601
    return-void
.end method

.method public docFreq(Lorg/apache/lucene/index/Term;)I
    .locals 3
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 704
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 705
    const/4 v1, 0x0

    .line 706
    .local v1, "total":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 707
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/SegmentReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v2

    add-int/2addr v1, v2

    .line 706
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 708
    :cond_0
    return v1
.end method

.method public document(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;
    .locals 3
    .param p1, "n"    # I
    .param p2, "fieldSelector"    # Lorg/apache/lucene/document/FieldSelector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 566
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 567
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->readerIndex(I)I

    move-result v0

    .line 568
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2, p2}, Lorg/apache/lucene/index/SegmentReader;->document(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;

    move-result-object v1

    return-object v1
.end method

.method public getCommitUserData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 882
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 883
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->getUserData()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getFieldInfos()Lorg/apache/lucene/index/FieldInfos;
    .locals 2

    .prologue
    .line 317
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "call getFieldInfos() on each sub reader, or use ReaderUtil.getMergedFieldInfos, instead"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getIndexCommit()Lorg/apache/lucene/index/IndexCommit;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 947
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 948
    new-instance v0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;-><init>(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/store/Directory;)V

    return-object v0
.end method

.method public getSequentialSubReaders()[Lorg/apache/lucene/index/IndexReader;
    .locals 1

    .prologue
    .line 922
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    return-object v0
.end method

.method public getTermFreqVector(ILjava/lang/String;)Lorg/apache/lucene/index/TermFreqVector;
    .locals 3
    .param p1, "n"    # I
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 514
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 515
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->readerIndex(I)I

    move-result v0

    .line 516
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2, p2}, Lorg/apache/lucene/index/SegmentReader;->getTermFreqVector(ILjava/lang/String;)Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v1

    return-object v1
.end method

.method public getTermFreqVector(ILjava/lang/String;Lorg/apache/lucene/index/TermVectorMapper;)V
    .locals 3
    .param p1, "docNumber"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "mapper"    # Lorg/apache/lucene/index/TermVectorMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 522
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 523
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->readerIndex(I)I

    move-result v0

    .line 524
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2, p2, p3}, Lorg/apache/lucene/index/SegmentReader;->getTermFreqVector(ILjava/lang/String;Lorg/apache/lucene/index/TermVectorMapper;)V

    .line 525
    return-void
.end method

.method public getTermFreqVector(ILorg/apache/lucene/index/TermVectorMapper;)V
    .locals 3
    .param p1, "docNumber"    # I
    .param p2, "mapper"    # Lorg/apache/lucene/index/TermVectorMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 529
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 530
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->readerIndex(I)I

    move-result v0

    .line 531
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2, p2}, Lorg/apache/lucene/index/SegmentReader;->getTermFreqVector(ILorg/apache/lucene/index/TermVectorMapper;)V

    .line 532
    return-void
.end method

.method public getTermFreqVectors(I)[Lorg/apache/lucene/index/TermFreqVector;
    .locals 3
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 506
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 507
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->readerIndex(I)I

    move-result v0

    .line 508
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/SegmentReader;->getTermFreqVectors(I)[Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v1

    return-object v1
.end method

.method public getTermInfosIndexDivisor()I
    .locals 1

    .prologue
    .line 936
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 937
    iget v0, p0, Lorg/apache/lucene/index/DirectoryReader;->termInfosIndexDivisor:I

    return v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 500
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 501
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->getVersion()J

    move-result-wide v0

    return-wide v0
.end method

.method public hasDeletions()Z
    .locals 1

    .prologue
    .line 580
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 581
    iget-boolean v0, p0, Lorg/apache/lucene/index/DirectoryReader;->hasDeletions:Z

    return v0
.end method

.method public hasNorms(Ljava/lang/String;)Z
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 630
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 631
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 632
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/SegmentReader;->hasNorms(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 634
    :goto_1
    return v1

    .line 631
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 634
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isCurrent()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 888
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 889
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 891
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    invoke-static {v0}, Lorg/apache/lucene/index/SegmentInfos;->readCurrentVersion(Lorg/apache/lucene/store/Directory;)J

    move-result-wide v0

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfos;->getVersion()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 893
    :goto_0
    return v0

    .line 891
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 893
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->nrtIsCurrent(Lorg/apache/lucene/index/SegmentInfos;)Z

    move-result v0

    goto :goto_0
.end method

.method public isDeleted(I)Z
    .locals 3
    .param p1, "n"    # I

    .prologue
    .line 574
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->readerIndex(I)I

    move-result v0

    .line 575
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/SegmentReader;->isDeleted(I)Z

    move-result v1

    return v1
.end method

.method public isOptimized()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 538
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 539
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->hasDeletions()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public maxDoc()I
    .locals 1

    .prologue
    .line 560
    iget v0, p0, Lorg/apache/lucene/index/DirectoryReader;->maxDoc:I

    return v0
.end method

.method public declared-synchronized norms(Ljava/lang/String;[BI)V
    .locals 5
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "result"    # [B
    .param p3, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 656
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 657
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->normsCache:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 658
    .local v0, "bytes":[B
    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->hasNorms(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 659
    array-length v2, p2

    invoke-static {}, Lorg/apache/lucene/search/Similarity;->getDefault()Lorg/apache/lucene/search/Similarity;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Similarity;->encodeNormValue(F)B

    move-result v3

    invoke-static {p2, p3, v2, v3}, Ljava/util/Arrays;->fill([BIIB)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 667
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 660
    :cond_1
    if-eqz v0, :cond_2

    .line 661
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->maxDoc()I

    move-result v3

    invoke-static {v0, v2, p2, p3, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 656
    .end local v0    # "bytes":[B
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 663
    .restart local v0    # "bytes":[B
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 664
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v2, v2, v1

    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    aget v3, v3, v1

    add-int/2addr v3, p3

    invoke-virtual {v2, p1, p2, v3}, Lorg/apache/lucene/index/SegmentReader;->norms(Ljava/lang/String;[BI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 663
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public declared-synchronized norms(Ljava/lang/String;)[B
    .locals 4
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 639
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 640
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->normsCache:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 641
    .local v0, "bytes":[B
    if-eqz v0, :cond_0

    move-object v2, v0

    .line 650
    :goto_0
    monitor-exit p0

    return-object v2

    .line 643
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->hasNorms(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 644
    const/4 v2, 0x0

    goto :goto_0

    .line 646
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->maxDoc()I

    move-result v2

    new-array v0, v2, [B

    .line 647
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 648
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v2, v2, v1

    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    aget v3, v3, v1

    invoke-virtual {v2, p1, v0, v3}, Lorg/apache/lucene/index/SegmentReader;->norms(Ljava/lang/String;[BI)V

    .line 647
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 649
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->normsCache:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v2, v0

    .line 650
    goto :goto_0

    .line 639
    .end local v0    # "bytes":[B
    .end local v1    # "i":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public numDocs()I
    .locals 4

    .prologue
    .line 548
    iget v2, p0, Lorg/apache/lucene/index/DirectoryReader;->numDocs:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 549
    const/4 v1, 0x0

    .line 550
    .local v1, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 551
    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentReader;->numDocs()I

    move-result v2

    add-int/2addr v1, v2

    .line 550
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 552
    :cond_0
    iput v1, p0, Lorg/apache/lucene/index/DirectoryReader;->numDocs:I

    .line 554
    .end local v0    # "i":I
    .end local v1    # "n":I
    :cond_1
    iget v2, p0, Lorg/apache/lucene/index/DirectoryReader;->numDocs:I

    return v2
.end method

.method rollbackCommit()V
    .locals 2

    .prologue
    .line 874
    iget-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->rollbackHasChanges:Z

    iput-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->hasChanges:Z

    .line 875
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 876
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentReader;->rollbackCommit()V

    .line 875
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 878
    :cond_0
    return-void
.end method

.method startCommit()V
    .locals 2

    .prologue
    .line 867
    iget-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->hasChanges:Z

    iput-boolean v1, p0, Lorg/apache/lucene/index/DirectoryReader;->rollbackHasChanges:Z

    .line 868
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 869
    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentReader;->startCommit()V

    .line 868
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 871
    :cond_0
    return-void
.end method

.method public termDocs()Lorg/apache/lucene/index/TermDocs;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 713
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 714
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 716
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->termDocs()Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    .line 718
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/lucene/index/DirectoryReader$MultiTermDocs;-><init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[I)V

    goto :goto_0
.end method

.method public termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 724
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 725
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 727
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SegmentReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    .line 729
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/lucene/index/IndexReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    goto :goto_0
.end method

.method public termPositions()Lorg/apache/lucene/index/TermPositions;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 735
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 736
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 738
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->termPositions()Lorg/apache/lucene/index/TermPositions;

    move-result-object v0

    .line 740
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/index/DirectoryReader$MultiTermPositions;

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/lucene/index/DirectoryReader$MultiTermPositions;-><init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[I)V

    goto :goto_0
.end method

.method public terms()Lorg/apache/lucene/index/TermEnum;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 682
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 683
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 685
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->terms()Lorg/apache/lucene/index/TermEnum;

    move-result-object v0

    .line 687
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;-><init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[ILorg/apache/lucene/index/Term;)V

    goto :goto_0
.end method

.method public terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 693
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->ensureOpen()V

    .line 694
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 696
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SegmentReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v0

    .line 698
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    iget-object v2, p0, Lorg/apache/lucene/index/DirectoryReader;->starts:[I

    invoke-direct {v0, p0, v1, v2, p1}, Lorg/apache/lucene/index/DirectoryReader$MultiTermEnum;-><init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[ILorg/apache/lucene/index/Term;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 294
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 295
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-boolean v3, p0, Lorg/apache/lucene/index/DirectoryReader;->hasChanges:Z

    if-eqz v3, :cond_0

    .line 296
    const-string/jumbo v3, "*"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    const/16 v3, 0x28

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 300
    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v2

    .line 301
    .local v2, "segmentsFile":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 302
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v3, :cond_2

    .line 305
    const-string/jumbo v3, ":nrt"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 308
    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 309
    iget-object v3, p0, Lorg/apache/lucene/index/DirectoryReader;->subReaders:[Lorg/apache/lucene/index/SegmentReader;

    aget-object v3, v3, v1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 307
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 311
    :cond_3
    const/16 v3, 0x29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 312
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
