.class final Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;
.super Lorg/apache/lucene/index/TermsHashConsumerPerField;
.source "TermVectorsTermsWriterPerField.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field doVectorOffsets:Z

.field doVectorPositions:Z

.field doVectors:Z

.field final docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

.field final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field final fieldState:Lorg/apache/lucene/index/FieldInvertState;

.field maxNumPostings:I

.field offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field final perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

.field final termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

.field final termsWriter:Lorg/apache/lucene/index/TermVectorsTermsWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 1
    .param p1, "termsHashPerField"    # Lorg/apache/lucene/index/TermsHashPerField;
    .param p2, "perThread"    # Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;
    .param p3, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashConsumerPerField;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 45
    iput-object p1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    .line 46
    iput-object p2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    .line 47
    iget-object v0, p2, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->termsWriter:Lorg/apache/lucene/index/TermVectorsTermsWriter;

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsWriter:Lorg/apache/lucene/index/TermVectorsTermsWriter;

    .line 48
    iput-object p3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 49
    iget-object v0, p1, Lorg/apache/lucene/index/TermsHashPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 50
    iget-object v0, p1, Lorg/apache/lucene/index/TermsHashPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    .line 51
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method addTerm(I)V
    .locals 7
    .param p1, "termID"    # I

    .prologue
    const/4 v5, 0x1

    .line 246
    sget-boolean v3, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    const-string/jumbo v4, "TermVectorsTermsWriterPerField.addTerm start"

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/DocumentsWriter$DocState;->testPoint(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 248
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, v3, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    check-cast v1, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;

    .line 250
    .local v1, "postings":Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;
    iget-object v3, v1, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;->freqs:[I

    aget v4, v3, p1

    add-int/lit8 v4, v4, 0x1

    aput v4, v3, p1

    .line 252
    iget-boolean v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorOffsets:Z

    if-eqz v3, :cond_1

    .line 253
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v3, v3, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v4

    add-int v2, v3, v4

    .line 254
    .local v2, "startOffset":I
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v3, v3, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v4

    add-int v0, v3, v4

    .line 256
    .local v0, "endOffset":I
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v4, v1, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;->lastOffsets:[I

    aget v4, v4, p1

    sub-int v4, v2, v4

    invoke-virtual {v3, v5, v4}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 257
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    sub-int v4, v0, v2

    invoke-virtual {v3, v5, v4}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 258
    iget-object v3, v1, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;->lastOffsets:[I

    aput v0, v3, p1

    .line 261
    .end local v0    # "endOffset":I
    .end local v2    # "startOffset":I
    :cond_1
    iget-boolean v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorPositions:Z

    if-eqz v3, :cond_2

    .line 262
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    const/4 v4, 0x0

    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v5, v5, Lorg/apache/lucene/index/FieldInvertState;->position:I

    iget-object v6, v1, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;->lastPositions:[I

    aget v6, v6, p1

    sub-int/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 263
    iget-object v3, v1, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;->lastPositions:[I

    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v4, v4, Lorg/apache/lucene/index/FieldInvertState;->position:I

    aput v4, v3, p1

    .line 265
    :cond_2
    return-void
.end method

.method createPostingsArray(I)Lorg/apache/lucene/index/ParallelPostingsArray;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 272
    new-instance v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;

    invoke-direct {v0, p1}, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;-><init>(I)V

    return-object v0
.end method

.method finish()V
    .locals 25
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    sget-boolean v23, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v23, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v23, v0

    const-string/jumbo v24, "TermVectorsTermsWriterPerField.finish start"

    invoke-virtual/range {v23 .. v24}, Lorg/apache/lucene/index/DocumentsWriter$DocState;->testPoint(Ljava/lang/String;)Z

    move-result v23

    if-nez v23, :cond_0

    new-instance v23, Ljava/lang/AssertionError;

    invoke-direct/range {v23 .. v23}, Ljava/lang/AssertionError;-><init>()V

    throw v23

    .line 110
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v10, v0, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    .line 112
    .local v10, "numPostings":I
    sget-boolean v23, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v23, :cond_1

    if-gez v10, :cond_1

    new-instance v23, Ljava/lang/AssertionError;

    invoke-direct/range {v23 .. v23}, Ljava/lang/AssertionError;-><init>()V

    throw v23

    .line 114
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectors:Z

    move/from16 v23, v0

    if-eqz v23, :cond_2

    if-nez v10, :cond_3

    .line 203
    :cond_2
    :goto_0
    return-void

    .line 117
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->maxNumPostings:I

    move/from16 v23, v0

    move/from16 v0, v23

    if-le v10, v0, :cond_4

    .line 118
    move-object/from16 v0, p0

    iput v10, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->maxNumPostings:I

    .line 120
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->perDocTvf:Lorg/apache/lucene/store/RAMOutputStream;

    move-object/from16 v21, v0

    .line 126
    .local v21, "tvf":Lorg/apache/lucene/store/IndexOutput;
    sget-boolean v23, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v23, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-boolean v0, v0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    move/from16 v23, v0

    if-nez v23, :cond_5

    new-instance v23, Ljava/lang/AssertionError;

    invoke-direct/range {v23 .. v23}, Ljava/lang/AssertionError;-><init>()V

    throw v23

    .line 127
    :cond_5
    sget-boolean v23, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v23, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->vectorFieldsInOrder(Lorg/apache/lucene/index/FieldInfo;)Z

    move-result v23

    if-nez v23, :cond_6

    new-instance v23, Ljava/lang/AssertionError;

    invoke-direct/range {v23 .. v23}, Ljava/lang/AssertionError;-><init>()V

    throw v23

    .line 129
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/index/FieldInfo;->number:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->addField(I)V

    .line 130
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v11, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    check-cast v11, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;

    .line 132
    .local v11, "postings":Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/TermsHashPerField;->sortPostings()[I

    move-result-object v19

    .line 134
    .local v19, "termIDs":[I
    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 135
    const/4 v3, 0x0

    .line 136
    .local v3, "bits":B
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorPositions:Z

    move/from16 v23, v0

    if-eqz v23, :cond_7

    .line 137
    const/16 v23, 0x1

    move/from16 v0, v23

    int-to-byte v3, v0

    .line 138
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorOffsets:Z

    move/from16 v23, v0

    if-eqz v23, :cond_8

    .line 139
    or-int/lit8 v23, v3, 0x2

    move/from16 v0, v23

    int-to-byte v3, v0

    .line 140
    :cond_8
    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 142
    const/4 v5, 0x0

    .line 143
    .local v5, "encoderUpto":I
    const/4 v9, 0x0

    .line 145
    .local v9, "lastTermBytesCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v13, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->vectorSliceReader:Lorg/apache/lucene/index/ByteSliceReader;

    .line 146
    .local v13, "reader":Lorg/apache/lucene/index/ByteSliceReader;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->termsHashPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerThread;->charPool:Lorg/apache/lucene/index/CharBlockPool;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v4, v0, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    .line 147
    .local v4, "charBuffers":[[C
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_1
    if-ge v7, v10, :cond_d

    .line 148
    aget v18, v19, v7

    .line 149
    .local v18, "termID":I
    iget-object v0, v11, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;->freqs:[I

    move-object/from16 v23, v0

    aget v6, v23, v18

    .line 151
    .local v6, "freq":I
    iget-object v0, v11, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;->textStarts:[I

    move-object/from16 v23, v0

    aget v23, v23, v18

    shr-int/lit8 v23, v23, 0xe

    aget-object v20, v4, v23

    .line 152
    .local v20, "text2":[C
    iget-object v0, v11, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;->textStarts:[I

    move-object/from16 v23, v0

    aget v23, v23, v18

    move/from16 v0, v23

    and-int/lit16 v14, v0, 0x3fff

    .line 156
    .local v14, "start2":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    move-object/from16 v23, v0

    aget-object v22, v23, v5

    .line 159
    .local v22, "utf8Result":Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;
    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-static {v0, v14, v1}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8([CILorg/apache/lucene/util/UnicodeUtil$UTF8Result;)V

    .line 160
    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->length:I

    move/from16 v17, v0

    .line 165
    .local v17, "termBytesCount":I
    const/4 v12, 0x0

    .line 166
    .local v12, "prefix":I
    if-lez v7, :cond_9

    .line 167
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    move-object/from16 v23, v0

    rsub-int/lit8 v24, v5, 0x1

    aget-object v23, v23, v24

    move-object/from16 v0, v23

    iget-object v8, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    .line 168
    .local v8, "lastTermBytes":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->utf8Results:[Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;

    move-object/from16 v23, v0

    aget-object v23, v23, v5

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    move-object/from16 v16, v0

    .line 169
    .local v16, "termBytes":[B
    :goto_2
    if-ge v12, v9, :cond_9

    move/from16 v0, v17

    if-ge v12, v0, :cond_9

    .line 170
    aget-byte v23, v8, v12

    aget-byte v24, v16, v12

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_c

    .line 175
    .end local v8    # "lastTermBytes":[B
    .end local v16    # "termBytes":[B
    :cond_9
    rsub-int/lit8 v5, v5, 0x1

    .line 176
    move/from16 v9, v17

    .line 178
    sub-int v15, v17, v12

    .line 179
    .local v15, "suffix":I
    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 180
    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 181
    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;->result:[B

    move-object/from16 v23, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v12, v15}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 182
    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 184
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorPositions:Z

    move/from16 v23, v0

    if-eqz v23, :cond_a

    .line 185
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v18

    move/from16 v2, v24

    invoke-virtual {v0, v13, v1, v2}, Lorg/apache/lucene/index/TermsHashPerField;->initReader(Lorg/apache/lucene/index/ByteSliceReader;II)V

    .line 186
    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lorg/apache/lucene/index/ByteSliceReader;->writeTo(Lorg/apache/lucene/store/IndexOutput;)J

    .line 189
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorOffsets:Z

    move/from16 v23, v0

    if-eqz v23, :cond_b

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v18

    move/from16 v2, v24

    invoke-virtual {v0, v13, v1, v2}, Lorg/apache/lucene/index/TermsHashPerField;->initReader(Lorg/apache/lucene/index/ByteSliceReader;II)V

    .line 191
    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lorg/apache/lucene/index/ByteSliceReader;->writeTo(Lorg/apache/lucene/store/IndexOutput;)J

    .line 147
    :cond_b
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 172
    .end local v15    # "suffix":I
    .restart local v8    # "lastTermBytes":[B
    .restart local v16    # "termBytes":[B
    :cond_c
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 195
    .end local v6    # "freq":I
    .end local v8    # "lastTermBytes":[B
    .end local v12    # "prefix":I
    .end local v14    # "start2":I
    .end local v16    # "termBytes":[B
    .end local v17    # "termBytesCount":I
    .end local v18    # "termID":I
    .end local v20    # "text2":[C
    .end local v22    # "utf8Result":Lorg/apache/lucene/util/UnicodeUtil$UTF8Result;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/TermsHashPerField;->reset()V

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->termsHashPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lorg/apache/lucene/index/TermsHashPerThread;->reset(Z)V

    goto/16 :goto_0
.end method

.method getStreamCount()I
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x2

    return v0
.end method

.method newTerm(I)V
    .locals 6
    .param p1, "termID"    # I

    .prologue
    const/4 v5, 0x1

    .line 222
    sget-boolean v3, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    const-string/jumbo v4, "TermVectorsTermsWriterPerField.newTerm start"

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/DocumentsWriter$DocState;->testPoint(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 224
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, v3, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    check-cast v1, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;

    .line 226
    .local v1, "postings":Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;
    iget-object v3, v1, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;->freqs:[I

    aput v5, v3, p1

    .line 228
    iget-boolean v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorOffsets:Z

    if-eqz v3, :cond_1

    .line 229
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v3, v3, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v4

    add-int v2, v3, v4

    .line 230
    .local v2, "startOffset":I
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v3, v3, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v4

    add-int v0, v3, v4

    .line 232
    .local v0, "endOffset":I
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v3, v5, v2}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 233
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    sub-int v4, v0, v2

    invoke-virtual {v3, v5, v4}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 234
    iget-object v3, v1, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;->lastOffsets:[I

    aput v0, v3, p1

    .line 237
    .end local v0    # "endOffset":I
    .end local v2    # "startOffset":I
    :cond_1
    iget-boolean v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorPositions:Z

    if-eqz v3, :cond_2

    .line 238
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    const/4 v4, 0x0

    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v5, v5, Lorg/apache/lucene/index/FieldInvertState;->position:I

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 239
    iget-object v3, v1, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField$TermVectorsPostingsArray;->lastPositions:[I

    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v4, v4, Lorg/apache/lucene/index/FieldInvertState;->position:I

    aput v4, v3, p1

    .line 241
    :cond_2
    return-void
.end method

.method shrinkHash()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget v1, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->maxNumPostings:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/TermsHashPerField;->shrinkHash(I)V

    .line 207
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->maxNumPostings:I

    .line 208
    return-void
.end method

.method skippingLongTerm()V
    .locals 0

    .prologue
    .line 268
    return-void
.end method

.method start(Lorg/apache/lucene/document/Fieldable;)V
    .locals 2
    .param p1, "f"    # Lorg/apache/lucene/document/Fieldable;

    .prologue
    .line 212
    iget-boolean v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorOffsets:Z

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 217
    :goto_0
    return-void

    .line 215
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    goto :goto_0
.end method

.method start([Lorg/apache/lucene/document/Fieldable;I)Z
    .locals 8
    .param p1, "fields"    # [Lorg/apache/lucene/document/Fieldable;
    .param p2, "count"    # I

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 60
    iput-boolean v4, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectors:Z

    .line 61
    iput-boolean v4, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorPositions:Z

    .line 62
    iput-boolean v4, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorOffsets:Z

    .line 64
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_1

    .line 65
    aget-object v0, p1, v1

    .line 66
    .local v0, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->isIndexed()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->isTermVectorStored()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectors:Z

    .line 68
    iget-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorPositions:Z

    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->isStorePositionWithTermVector()Z

    move-result v3

    or-int/2addr v2, v3

    iput-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorPositions:Z

    .line 69
    iget-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorOffsets:Z

    invoke-interface {v0}, Lorg/apache/lucene/document/Fieldable;->isStoreOffsetWithTermVector()Z

    move-result v3

    or-int/2addr v2, v3

    iput-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectorOffsets:Z

    .line 64
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 73
    .end local v0    # "field":Lorg/apache/lucene/document/Fieldable;
    :cond_1
    iget-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectors:Z

    if-eqz v2, :cond_6

    .line 74
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    iget-object v2, v2, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    if-nez v2, :cond_4

    .line 75
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsWriter:Lorg/apache/lucene/index/TermVectorsTermsWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/TermVectorsTermsWriter;->getPerDoc()Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    move-result-object v3

    iput-object v3, v2, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    .line 76
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    iget-object v2, v2, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v3, v3, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iput v3, v2, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->docID:I

    .line 77
    sget-boolean v2, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    iget-object v2, v2, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    iget v2, v2, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->numVectorFields:I

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 78
    :cond_2
    sget-boolean v2, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    iget-object v2, v2, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    iget-object v2, v2, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->perDocTvf:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v2}, Lorg/apache/lucene/store/RAMOutputStream;->length()J

    move-result-wide v2

    cmp-long v2, v6, v2

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 79
    :cond_3
    sget-boolean v2, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    iget-object v2, v2, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    iget-object v2, v2, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->perDocTvf:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v2}, Lorg/apache/lucene/store/RAMOutputStream;->getFilePointer()J

    move-result-wide v2

    cmp-long v2, v6, v2

    if-eqz v2, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 82
    :cond_4
    sget-boolean v2, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    iget-object v2, v2, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->doc:Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;

    iget v2, v2, Lorg/apache/lucene/index/TermVectorsTermsWriter$PerDoc;->docID:I

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v3, v3, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    if-eq v2, v3, :cond_5

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 84
    :cond_5
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget v2, v2, Lorg/apache/lucene/index/TermsHashPerField;->numPostings:I

    if-eqz v2, :cond_6

    .line 88
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsHashPerField;->reset()V

    .line 89
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->perThread:Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;

    iget-object v2, v2, Lorg/apache/lucene/index/TermVectorsTermsWriterPerThread;->termsHashPerThread:Lorg/apache/lucene/index/TermsHashPerThread;

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/TermsHashPerThread;->reset(Z)V

    .line 96
    :cond_6
    iget-boolean v2, p0, Lorg/apache/lucene/index/TermVectorsTermsWriterPerField;->doVectors:Z

    return v2
.end method
