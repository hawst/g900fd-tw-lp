.class final Lorg/apache/lucene/index/CharBlockPool;
.super Ljava/lang/Object;
.source "CharBlockPool.java"


# instance fields
.field public buffer:[C

.field bufferUpto:I

.field public buffers:[[C

.field public charOffset:I

.field public charUpto:I

.field private final docWriter:Lorg/apache/lucene/index/DocumentsWriter;

.field numBuffer:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriter;)V
    .locals 1
    .param p1, "docWriter"    # Lorg/apache/lucene/index/DocumentsWriter;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/16 v0, 0xa

    new-array v0, v0, [[C

    iput-object v0, p0, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/CharBlockPool;->bufferUpto:I

    .line 29
    const/16 v0, 0x4000

    iput v0, p0, Lorg/apache/lucene/index/CharBlockPool;->charUpto:I

    .line 32
    const/16 v0, -0x4000

    iput v0, p0, Lorg/apache/lucene/index/CharBlockPool;->charOffset:I

    .line 36
    iput-object p1, p0, Lorg/apache/lucene/index/CharBlockPool;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 37
    return-void
.end method


# virtual methods
.method public nextBuffer()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 47
    iget v1, p0, Lorg/apache/lucene/index/CharBlockPool;->bufferUpto:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 48
    iget-object v1, p0, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    sget v2, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [[C

    .line 50
    .local v0, "newBuffers":[[C
    iget-object v1, p0, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    iget-object v2, p0, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    array-length v2, v2

    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 51
    iput-object v0, p0, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    .line 53
    .end local v0    # "newBuffers":[[C
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    iget v2, p0, Lorg/apache/lucene/index/CharBlockPool;->bufferUpto:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lorg/apache/lucene/index/CharBlockPool;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocumentsWriter;->getCharBlock()[C

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v3, p0, Lorg/apache/lucene/index/CharBlockPool;->buffer:[C

    .line 54
    iget v1, p0, Lorg/apache/lucene/index/CharBlockPool;->bufferUpto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/CharBlockPool;->bufferUpto:I

    .line 56
    iput v4, p0, Lorg/apache/lucene/index/CharBlockPool;->charUpto:I

    .line 57
    iget v1, p0, Lorg/apache/lucene/index/CharBlockPool;->charOffset:I

    add-int/lit16 v1, v1, 0x4000

    iput v1, p0, Lorg/apache/lucene/index/CharBlockPool;->charOffset:I

    .line 58
    return-void
.end method

.method public reset()V
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/lucene/index/CharBlockPool;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/CharBlockPool;->buffers:[[C

    iget v2, p0, Lorg/apache/lucene/index/CharBlockPool;->bufferUpto:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/DocumentsWriter;->recycleCharBlocks([[CI)V

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/CharBlockPool;->bufferUpto:I

    .line 42
    const/16 v0, 0x4000

    iput v0, p0, Lorg/apache/lucene/index/CharBlockPool;->charUpto:I

    .line 43
    const/16 v0, -0x4000

    iput v0, p0, Lorg/apache/lucene/index/CharBlockPool;->charOffset:I

    .line 44
    return-void
.end method
