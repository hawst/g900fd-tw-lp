.class final Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
.super Lorg/apache/lucene/index/TermsHashConsumerPerField;
.source "FreqProxTermsWriterPerField.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/index/TermsHashConsumerPerField;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/index/FreqProxTermsWriterPerField;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

.field final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field final fieldState:Lorg/apache/lucene/index/FieldInvertState;

.field hasPayloads:Z

.field indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

.field payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

.field final perThread:Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;

.field final termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 1
    .param p1, "termsHashPerField"    # Lorg/apache/lucene/index/TermsHashPerField;
    .param p2, "perThread"    # Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;
    .param p3, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashConsumerPerField;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    .line 42
    iput-object p2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->perThread:Lorg/apache/lucene/index/FreqProxTermsWriterPerThread;

    .line 43
    iput-object p3, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 44
    iget-object v0, p1, Lorg/apache/lucene/index/TermsHashPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 45
    iget-object v0, p1, Lorg/apache/lucene/index/TermsHashPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    .line 46
    iget-object v0, p3, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 47
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 0

    .prologue
    .line 226
    return-void
.end method

.method addTerm(I)V
    .locals 5
    .param p1, "termID"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 138
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    const-string/jumbo v2, "FreqProxTermsWriterPerField.addTerm start"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/DocumentsWriter$DocState;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 140
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v0, v1, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    check-cast v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    .line 142
    .local v0, "postings":Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v1, v2, :cond_1

    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->docFreqs:[I

    aget v1, v1, p1

    if-gtz v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 144
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v1, v2, :cond_4

    .line 145
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v2, v2, p1

    if-eq v1, v2, :cond_3

    .line 146
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v2, v2, p1

    if-gt v1, v2, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 147
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    aget v2, v2, p1

    invoke-virtual {v1, v3, v2}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 148
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iget-object v3, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v3, v3, p1

    sub-int/2addr v2, v3

    aput v2, v1, p1

    .line 149
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    aput v2, v1, p1

    .line 150
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v2, v1, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    .line 181
    :cond_3
    :goto_0
    return-void

    .line 153
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v2, v2, p1

    if-eq v1, v2, :cond_8

    .line 154
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v2, v2, p1

    if-gt v1, v2, :cond_5

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 160
    :cond_5
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->docFreqs:[I

    aget v1, v1, p1

    if-ne v4, v1, :cond_7

    .line 161
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    aget v2, v2, p1

    or-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v3, v2}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 166
    :goto_1
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->docFreqs:[I

    aput v4, v1, p1

    .line 167
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v2, v2, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v1, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    .line 168
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iget-object v3, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    aget v3, v3, p1

    sub-int/2addr v2, v3

    shl-int/lit8 v2, v2, 0x1

    aput v2, v1, p1

    .line 169
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    aput v2, v1, p1

    .line 170
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v1, v2, :cond_6

    .line 171
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v1, v1, Lorg/apache/lucene/index/FieldInvertState;->position:I

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->writeProx(II)V

    .line 173
    :cond_6
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v2, v1, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    goto :goto_0

    .line 163
    :cond_7
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    aget v2, v2, p1

    invoke-virtual {v1, v3, v2}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 164
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->docFreqs:[I

    aget v2, v2, p1

    invoke-virtual {v1, v3, v2}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    goto :goto_1

    .line 175
    :cond_8
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v2, v2, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    iget-object v3, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->docFreqs:[I

    aget v4, v3, p1

    add-int/lit8 v4, v4, 0x1

    aput v4, v3, p1

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v1, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    .line 176
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v1, v2, :cond_3

    .line 177
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v1, v1, Lorg/apache/lucene/index/FieldInvertState;->position:I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastPositions:[I

    aget v2, v2, p1

    sub-int/2addr v1, v2

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->writeProx(II)V

    goto/16 :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->compareTo(Lorg/apache/lucene/index/FreqProxTermsWriterPerField;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/index/FreqProxTermsWriterPerField;)I
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method createPostingsArray(I)Lorg/apache/lucene/index/ParallelPostingsArray;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 185
    new-instance v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    invoke-direct {v0, p1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;-><init>(I)V

    return-object v0
.end method

.method finish()V
    .locals 0

    .prologue
    .line 58
    return-void
.end method

.method getStreamCount()I
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v0, v1, :cond_0

    .line 52
    const/4 v0, 0x1

    .line 54
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method newTerm(I)V
    .locals 4
    .param p1, "termID"    # I

    .prologue
    const/4 v3, 0x1

    .line 118
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    const-string/jumbo v2, "FreqProxTermsWriterPerField.newTerm start"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/DocumentsWriter$DocState;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 120
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v0, v1, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    check-cast v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    .line 121
    .local v0, "postings":Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    aput v2, v1, p1

    .line 122
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v1, v2, :cond_2

    .line 123
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    aput v2, v1, p1

    .line 131
    :cond_1
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v2, v2, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v1, Lorg/apache/lucene/index/FieldInvertState;->maxTermFrequency:I

    .line 132
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v2, v1, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lorg/apache/lucene/index/FieldInvertState;->uniqueTermCount:I

    .line 133
    return-void

    .line 125
    :cond_2
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    shl-int/lit8 v2, v2, 0x1

    aput v2, v1, p1

    .line 126
    iget-object v1, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->docFreqs:[I

    aput v3, v1, p1

    .line 127
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v1, v2, :cond_1

    .line 128
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v1, v1, Lorg/apache/lucene/index/FieldInvertState;->position:I

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->writeProx(II)V

    goto :goto_0
.end method

.method reset()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    .line 74
    return-void
.end method

.method skippingLongTerm()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    return-void
.end method

.method start(Lorg/apache/lucene/document/Fieldable;)V
    .locals 2
    .param p1, "f"    # Lorg/apache/lucene/document/Fieldable;

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->hasAttribute(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    goto :goto_0
.end method

.method start([Lorg/apache/lucene/document/Fieldable;I)Z
    .locals 2
    .param p1, "fields"    # [Lorg/apache/lucene/document/Fieldable;
    .param p2, "count"    # I

    .prologue
    .line 78
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_1

    .line 79
    aget-object v1, p1, v0

    invoke-interface {v1}, Lorg/apache/lucene/document/Fieldable;->isIndexed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    const/4 v1, 0x1

    .line 81
    :goto_1
    return v1

    .line 78
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method writeProx(II)V
    .locals 7
    .param p1, "termID"    # I
    .param p2, "proxCode"    # I

    .prologue
    const/4 v6, 0x1

    .line 95
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    if-nez v2, :cond_0

    .line 96
    const/4 v0, 0x0

    .line 101
    .local v0, "payload":Lorg/apache/lucene/index/Payload;
    :goto_0
    if-eqz v0, :cond_1

    iget v2, v0, Lorg/apache/lucene/index/Payload;->length:I

    if-lez v2, :cond_1

    .line 102
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    shl-int/lit8 v3, p2, 0x1

    or-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v6, v3}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 103
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget v3, v0, Lorg/apache/lucene/index/Payload;->length:I

    invoke-virtual {v2, v6, v3}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    .line 104
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v3, v0, Lorg/apache/lucene/index/Payload;->data:[B

    iget v4, v0, Lorg/apache/lucene/index/Payload;->offset:I

    iget v5, v0, Lorg/apache/lucene/index/Payload;->length:I

    invoke-virtual {v2, v6, v3, v4, v5}, Lorg/apache/lucene/index/TermsHashPerField;->writeBytes(I[BII)V

    .line 105
    iput-boolean v6, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->hasPayloads:Z

    .line 109
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, v2, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    check-cast v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    .line 110
    .local v1, "postings":Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    iget-object v2, v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastPositions:[I

    iget-object v3, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iget v3, v3, Lorg/apache/lucene/index/FieldInvertState;->position:I

    aput v3, v2, p1

    .line 112
    return-void

    .line 98
    .end local v0    # "payload":Lorg/apache/lucene/index/Payload;
    .end local v1    # "postings":Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->payloadAttribute:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->getPayload()Lorg/apache/lucene/index/Payload;

    move-result-object v0

    .restart local v0    # "payload":Lorg/apache/lucene/index/Payload;
    goto :goto_0

    .line 107
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    shl-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v6, v3}, Lorg/apache/lucene/index/TermsHashPerField;->writeVInt(II)V

    goto :goto_1
.end method
