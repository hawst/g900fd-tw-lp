.class final Lorg/apache/lucene/index/DocInverterPerThread;
.super Lorg/apache/lucene/index/DocFieldConsumerPerThread;
.source "DocInverterPerThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/DocInverterPerThread$1;,
        Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;
    }
.end annotation


# instance fields
.field final consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerThread;

.field final docInverter:Lorg/apache/lucene/index/DocInverter;

.field final docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

.field final endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;

.field final fieldState:Lorg/apache/lucene/index/FieldInvertState;

.field final singleToken:Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;

.field final stringReader:Lorg/apache/lucene/index/ReusableStringReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocFieldProcessorPerThread;Lorg/apache/lucene/index/DocInverter;)V
    .locals 2
    .param p1, "docFieldProcessorPerThread"    # Lorg/apache/lucene/index/DocFieldProcessorPerThread;
    .param p2, "docInverter"    # Lorg/apache/lucene/index/DocInverter;

    .prologue
    .line 58
    invoke-direct {p0}, Lorg/apache/lucene/index/DocFieldConsumerPerThread;-><init>()V

    .line 34
    new-instance v0, Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;-><init>(Lorg/apache/lucene/index/DocInverterPerThread$1;)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread;->singleToken:Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;

    .line 53
    new-instance v0, Lorg/apache/lucene/index/FieldInvertState;

    invoke-direct {v0}, Lorg/apache/lucene/index/FieldInvertState;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    .line 56
    new-instance v0, Lorg/apache/lucene/index/ReusableStringReader;

    invoke-direct {v0}, Lorg/apache/lucene/index/ReusableStringReader;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread;->stringReader:Lorg/apache/lucene/index/ReusableStringReader;

    .line 59
    iput-object p2, p0, Lorg/apache/lucene/index/DocInverterPerThread;->docInverter:Lorg/apache/lucene/index/DocInverter;

    .line 60
    iget-object v0, p1, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 61
    iget-object v0, p2, Lorg/apache/lucene/index/DocInverter;->consumer:Lorg/apache/lucene/index/InvertedDocConsumer;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/InvertedDocConsumer;->addThread(Lorg/apache/lucene/index/DocInverterPerThread;)Lorg/apache/lucene/index/InvertedDocConsumerPerThread;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerThread;

    .line 62
    iget-object v0, p2, Lorg/apache/lucene/index/DocInverter;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/InvertedDocEndConsumer;->addThread(Lorg/apache/lucene/index/DocInverterPerThread;)Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;

    .line 63
    return-void
.end method


# virtual methods
.method abort()V
    .locals 2

    .prologue
    .line 82
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerThread;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocConsumerPerThread;->abort()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;->abort()V

    .line 86
    return-void

    .line 84
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/lucene/index/DocInverterPerThread;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;

    invoke-virtual {v1}, Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;->abort()V

    throw v0
.end method

.method public addField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/DocFieldConsumerPerField;
    .locals 1
    .param p1, "fi"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 90
    new-instance v0, Lorg/apache/lucene/index/DocInverterPerField;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/index/DocInverterPerField;-><init>(Lorg/apache/lucene/index/DocInverterPerThread;Lorg/apache/lucene/index/FieldInfo;)V

    return-object v0
.end method

.method public finishDocument()Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;->finishDocument()V

    .line 76
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerThread;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocConsumerPerThread;->finishDocument()Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    move-result-object v0

    return-object v0
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerThread;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocConsumerPerThread;->startDocument()V

    .line 68
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;->startDocument()V

    .line 69
    return-void
.end method
