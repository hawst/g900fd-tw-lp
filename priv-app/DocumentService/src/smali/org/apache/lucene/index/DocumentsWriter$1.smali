.class Lorg/apache/lucene/index/DocumentsWriter$1;
.super Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;
.source "DocumentsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 227
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;-><init>()V

    return-void
.end method


# virtual methods
.method getChain(Lorg/apache/lucene/index/DocumentsWriter;)Lorg/apache/lucene/index/DocConsumer;
    .locals 9
    .param p1, "documentsWriter"    # Lorg/apache/lucene/index/DocumentsWriter;

    .prologue
    .line 248
    new-instance v3, Lorg/apache/lucene/index/TermVectorsTermsWriter;

    invoke-direct {v3, p1}, Lorg/apache/lucene/index/TermVectorsTermsWriter;-><init>(Lorg/apache/lucene/index/DocumentsWriter;)V

    .line 249
    .local v3, "termVectorsWriter":Lorg/apache/lucene/index/TermsHashConsumer;
    new-instance v1, Lorg/apache/lucene/index/FreqProxTermsWriter;

    invoke-direct {v1}, Lorg/apache/lucene/index/FreqProxTermsWriter;-><init>()V

    .line 251
    .local v1, "freqProxWriter":Lorg/apache/lucene/index/TermsHashConsumer;
    new-instance v4, Lorg/apache/lucene/index/TermsHash;

    const/4 v5, 0x1

    new-instance v6, Lorg/apache/lucene/index/TermsHash;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, p1, v7, v3, v8}, Lorg/apache/lucene/index/TermsHash;-><init>(Lorg/apache/lucene/index/DocumentsWriter;ZLorg/apache/lucene/index/TermsHashConsumer;Lorg/apache/lucene/index/TermsHash;)V

    invoke-direct {v4, p1, v5, v1, v6}, Lorg/apache/lucene/index/TermsHash;-><init>(Lorg/apache/lucene/index/DocumentsWriter;ZLorg/apache/lucene/index/TermsHashConsumer;Lorg/apache/lucene/index/TermsHash;)V

    .line 253
    .local v4, "termsHash":Lorg/apache/lucene/index/InvertedDocConsumer;
    new-instance v2, Lorg/apache/lucene/index/NormsWriter;

    invoke-direct {v2}, Lorg/apache/lucene/index/NormsWriter;-><init>()V

    .line 254
    .local v2, "normsWriter":Lorg/apache/lucene/index/NormsWriter;
    new-instance v0, Lorg/apache/lucene/index/DocInverter;

    invoke-direct {v0, v4, v2}, Lorg/apache/lucene/index/DocInverter;-><init>(Lorg/apache/lucene/index/InvertedDocConsumer;Lorg/apache/lucene/index/InvertedDocEndConsumer;)V

    .line 255
    .local v0, "docInverter":Lorg/apache/lucene/index/DocInverter;
    new-instance v5, Lorg/apache/lucene/index/DocFieldProcessor;

    invoke-direct {v5, p1, v0}, Lorg/apache/lucene/index/DocFieldProcessor;-><init>(Lorg/apache/lucene/index/DocumentsWriter;Lorg/apache/lucene/index/DocFieldConsumer;)V

    return-object v5
.end method
