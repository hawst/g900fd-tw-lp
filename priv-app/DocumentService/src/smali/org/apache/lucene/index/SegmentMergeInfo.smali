.class final Lorg/apache/lucene/index/SegmentMergeInfo;
.super Ljava/lang/Object;
.source "SegmentMergeInfo.java"


# instance fields
.field base:I

.field delCount:I

.field private docMap:[I

.field ord:I

.field private postings:Lorg/apache/lucene/index/TermPositions;

.field reader:Lorg/apache/lucene/index/IndexReader;

.field readerPayloadProcessor:Lorg/apache/lucene/index/PayloadProcessorProvider$ReaderPayloadProcessor;

.field term:Lorg/apache/lucene/index/Term;

.field termEnum:Lorg/apache/lucene/index/TermEnum;


# direct methods
.method constructor <init>(ILorg/apache/lucene/index/TermEnum;Lorg/apache/lucene/index/IndexReader;)V
    .locals 1
    .param p1, "b"    # I
    .param p2, "te"    # Lorg/apache/lucene/index/TermEnum;
    .param p3, "r"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p1, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->base:I

    .line 38
    iput-object p3, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->reader:Lorg/apache/lucene/index/IndexReader;

    .line 39
    iput-object p2, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->termEnum:Lorg/apache/lucene/index/TermEnum;

    .line 40
    invoke-virtual {p2}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->term:Lorg/apache/lucene/index/Term;

    .line 41
    return-void
.end method


# virtual methods
.method final close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermEnum;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->postings:Lorg/apache/lucene/index/TermPositions;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->postings:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermPositions;->close()V

    .line 89
    :cond_0
    return-void

    .line 85
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->postings:Lorg/apache/lucene/index/TermPositions;

    if-eqz v1, :cond_1

    .line 86
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->postings:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermPositions;->close()V

    .line 85
    :cond_1
    throw v0
.end method

.method getDocMap()[I
    .locals 6

    .prologue
    .line 45
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->docMap:[I

    if-nez v4, :cond_1

    .line 46
    const/4 v4, 0x0

    iput v4, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->delCount:I

    .line 48
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 49
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v3

    .line 50
    .local v3, "maxDoc":I
    new-array v4, v3, [I

    iput-object v4, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->docMap:[I

    .line 51
    const/4 v1, 0x0

    .line 52
    .local v1, "j":I
    const/4 v0, 0x0

    .local v0, "i":I
    move v2, v1

    .end local v1    # "j":I
    .local v2, "j":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 53
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v4, v0}, Lorg/apache/lucene/index/IndexReader;->isDeleted(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 54
    iget v4, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->delCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->delCount:I

    .line 55
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->docMap:[I

    const/4 v5, -0x1

    aput v5, v4, v0

    move v1, v2

    .line 52
    .end local v2    # "j":I
    .restart local v1    # "j":I
    :goto_1
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    .end local v1    # "j":I
    .restart local v2    # "j":I
    goto :goto_0

    .line 57
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->docMap:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "j":I
    .restart local v1    # "j":I
    aput v2, v4, v0

    goto :goto_1

    .line 61
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v3    # "maxDoc":I
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->docMap:[I

    return-object v4
.end method

.method getPositions()Lorg/apache/lucene/index/TermPositions;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->postings:Lorg/apache/lucene/index/TermPositions;

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->termPositions()Lorg/apache/lucene/index/TermPositions;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->postings:Lorg/apache/lucene/index/TermPositions;

    .line 68
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->postings:Lorg/apache/lucene/index/TermPositions;

    return-object v0
.end method

.method final next()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermEnum;->next()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->termEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->term:Lorg/apache/lucene/index/Term;

    .line 74
    const/4 v0, 0x1

    .line 77
    :goto_0
    return v0

    .line 76
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentMergeInfo;->term:Lorg/apache/lucene/index/Term;

    .line 77
    const/4 v0, 0x0

    goto :goto_0
.end method
