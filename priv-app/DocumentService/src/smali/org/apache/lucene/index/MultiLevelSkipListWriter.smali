.class abstract Lorg/apache/lucene/index/MultiLevelSkipListWriter;
.super Ljava/lang/Object;
.source "MultiLevelSkipListWriter.java"


# instance fields
.field private numberOfSkipLevels:I

.field private skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

.field private skipInterval:I


# direct methods
.method protected constructor <init>(III)V
    .locals 4
    .param p1, "skipInterval"    # I
    .param p2, "maxSkipLevels"    # I
    .param p3, "df"    # I

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput p1, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipInterval:I

    .line 63
    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    .line 66
    iget v0, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    if-le v0, p2, :cond_0

    .line 67
    iput p2, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    .line 69
    :cond_0
    return-void

    .line 63
    :cond_1
    int-to-double v0, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    int-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0
.end method


# virtual methods
.method bufferSkip(I)V
    .locals 7
    .param p1, "df"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    const/4 v3, 0x0

    .local v3, "numLevels":I
    :goto_0
    iget v6, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipInterval:I

    rem-int v6, p1, v6

    if-nez v6, :cond_0

    iget v6, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    if-ge v3, v6, :cond_0

    .line 109
    add-int/lit8 v3, v3, 0x1

    .line 108
    iget v6, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipInterval:I

    div-int/2addr p1, v6

    goto :goto_0

    .line 112
    :cond_0
    const-wide/16 v0, 0x0

    .line 114
    .local v0, "childPointer":J
    const/4 v2, 0x0

    .local v2, "level":I
    :goto_1
    if-ge v2, v3, :cond_2

    .line 115
    iget-object v6, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    aget-object v6, v6, v2

    invoke-virtual {p0, v2, v6}, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->writeSkipData(ILorg/apache/lucene/store/IndexOutput;)V

    .line 117
    iget-object v6, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    aget-object v6, v6, v2

    invoke-virtual {v6}, Lorg/apache/lucene/store/RAMOutputStream;->getFilePointer()J

    move-result-wide v4

    .line 119
    .local v4, "newChildPointer":J
    if-eqz v2, :cond_1

    .line 121
    iget-object v6, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    aget-object v6, v6, v2

    invoke-virtual {v6, v0, v1}, Lorg/apache/lucene/store/RAMOutputStream;->writeVLong(J)V

    .line 125
    :cond_1
    move-wide v0, v4

    .line 114
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 127
    .end local v4    # "newChildPointer":J
    :cond_2
    return-void
.end method

.method protected init()V
    .locals 3

    .prologue
    .line 72
    iget v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    new-array v1, v1, [Lorg/apache/lucene/store/RAMOutputStream;

    iput-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    .line 73
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    if-ge v0, v1, :cond_0

    .line 74
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    new-instance v2, Lorg/apache/lucene/store/RAMOutputStream;

    invoke-direct {v2}, Lorg/apache/lucene/store/RAMOutputStream;-><init>()V

    aput-object v2, v1, v0

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_0
    return-void
.end method

.method protected resetSkip()V
    .locals 2

    .prologue
    .line 80
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    if-nez v1, :cond_1

    .line 81
    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->init()V

    .line 87
    :cond_0
    return-void

    .line 83
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 84
    iget-object v1, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/store/RAMOutputStream;->reset()V

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method writeSkip(Lorg/apache/lucene/store/IndexOutput;)J
    .locals 8
    .param p1, "output"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    .line 137
    .local v4, "skipPointer":J
    iget-object v3, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    array-length v3, v3

    if-nez v3, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-wide v4

    .line 139
    :cond_1
    iget v3, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    add-int/lit8 v2, v3, -0x1

    .local v2, "level":I
    :goto_1
    if-lez v2, :cond_3

    .line 140
    iget-object v3, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lorg/apache/lucene/store/RAMOutputStream;->getFilePointer()J

    move-result-wide v0

    .line 141
    .local v0, "length":J
    const-wide/16 v6, 0x0

    cmp-long v3, v0, v6

    if-lez v3, :cond_2

    .line 142
    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 143
    iget-object v3, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    aget-object v3, v3, v2

    invoke-virtual {v3, p1}, Lorg/apache/lucene/store/RAMOutputStream;->writeTo(Lorg/apache/lucene/store/IndexOutput;)V

    .line 139
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 146
    .end local v0    # "length":J
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/index/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    const/4 v6, 0x0

    aget-object v3, v3, v6

    invoke-virtual {v3, p1}, Lorg/apache/lucene/store/RAMOutputStream;->writeTo(Lorg/apache/lucene/store/IndexOutput;)V

    goto :goto_0
.end method

.method protected abstract writeSkipData(ILorg/apache/lucene/store/IndexOutput;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
