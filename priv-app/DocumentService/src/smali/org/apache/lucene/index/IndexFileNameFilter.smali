.class public Lorg/apache/lucene/index/IndexFileNameFilter;
.super Ljava/lang/Object;
.source "IndexFileNameFilter.java"

# interfaces
.implements Ljava/io/FilenameFilter;


# static fields
.field private static singleton:Lorg/apache/lucene/index/IndexFileNameFilter;


# instance fields
.field private extensions:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extensionsInCFS:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lorg/apache/lucene/index/IndexFileNameFilter;

    invoke-direct {v0}, Lorg/apache/lucene/index/IndexFileNameFilter;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/IndexFileNameFilter;->singleton:Lorg/apache/lucene/index/IndexFileNameFilter;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/index/IndexFileNameFilter;->extensions:Ljava/util/HashSet;

    .line 38
    sget-object v0, Lorg/apache/lucene/index/IndexFileNames;->INDEX_EXTENSIONS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 39
    .local v1, "ext":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/lucene/index/IndexFileNameFilter;->extensions:Ljava/util/HashSet;

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 38
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 41
    .end local v1    # "ext":Ljava/lang/String;
    :cond_0
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/index/IndexFileNameFilter;->extensionsInCFS:Ljava/util/HashSet;

    .line 42
    sget-object v0, Lorg/apache/lucene/index/IndexFileNames;->INDEX_EXTENSIONS_IN_COMPOUND_FILE:[Ljava/lang/String;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 43
    .restart local v1    # "ext":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/lucene/index/IndexFileNameFilter;->extensionsInCFS:Ljava/util/HashSet;

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 42
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 45
    .end local v1    # "ext":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public static getFilter()Lorg/apache/lucene/index/IndexFileNameFilter;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lorg/apache/lucene/index/IndexFileNameFilter;->singleton:Lorg/apache/lucene/index/IndexFileNameFilter;

    return-object v0
.end method


# virtual methods
.method public accept(Ljava/io/File;Ljava/lang/String;)Z
    .locals 4
    .param p1, "dir"    # Ljava/io/File;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 51
    const/16 v3, 0x2e

    invoke-virtual {p2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 52
    .local v1, "i":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_4

    .line 53
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "extension":Ljava/lang/String;
    iget-object v3, p0, Lorg/apache/lucene/index/IndexFileNameFilter;->extensions:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 67
    .end local v0    # "extension":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 56
    .restart local v0    # "extension":Ljava/lang/String;
    :cond_1
    const-string/jumbo v3, "f"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string/jumbo v3, "f\\d+"

    invoke-virtual {v0, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 59
    :cond_2
    const-string/jumbo v3, "s"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string/jumbo v3, "s\\d+"

    invoke-virtual {v0, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 67
    .end local v0    # "extension":Ljava/lang/String;
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 64
    :cond_4
    const-string/jumbo v3, "deletable"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 65
    const-string/jumbo v3, "segments"

    invoke-virtual {p2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public isCFSFile(Ljava/lang/String;)Z
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 77
    const/16 v3, 0x2e

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 78
    .local v1, "i":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_2

    .line 79
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "extension":Ljava/lang/String;
    iget-object v3, p0, Lorg/apache/lucene/index/IndexFileNameFilter;->extensionsInCFS:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 88
    .end local v0    # "extension":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 83
    .restart local v0    # "extension":Ljava/lang/String;
    :cond_1
    const-string/jumbo v3, "f"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string/jumbo v3, "f\\d+"

    invoke-virtual {v0, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 88
    .end local v0    # "extension":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
