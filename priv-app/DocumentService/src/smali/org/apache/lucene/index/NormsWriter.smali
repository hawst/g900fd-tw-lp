.class final Lorg/apache/lucene/index/NormsWriter;
.super Lorg/apache/lucene/index/InvertedDocEndConsumer;
.source "NormsWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final defaultNorm:B

.field private fieldInfos:Lorg/apache/lucene/index/FieldInfos;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/lucene/index/NormsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/NormsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/lucene/index/InvertedDocEndConsumer;-><init>()V

    .line 41
    invoke-static {}, Lorg/apache/lucene/search/Similarity;->getDefault()Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Similarity;->encodeNormValue(F)B

    move-result v0

    iput-byte v0, p0, Lorg/apache/lucene/index/NormsWriter;->defaultNorm:B

    return-void
.end method


# virtual methods
.method public abort()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public addThread(Lorg/apache/lucene/index/DocInverterPerThread;)Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;
    .locals 1
    .param p1, "docInverterPerThread"    # Lorg/apache/lucene/index/DocInverterPerThread;

    .prologue
    .line 45
    new-instance v0, Lorg/apache/lucene/index/NormsWriterPerThread;

    invoke-direct {v0, p1, p0}, Lorg/apache/lucene/index/NormsWriterPerThread;-><init>(Lorg/apache/lucene/index/DocInverterPerThread;Lorg/apache/lucene/index/NormsWriter;)V

    return-object v0
.end method

.method files(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    return-void
.end method

.method public flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 36
    .param p2, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;",
            ">;>;",
            "Lorg/apache/lucene/index/SegmentWriteState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "threadsAndFields":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;>;>;"
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 70
    .local v4, "byField":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/FieldInfo;Ljava/util/List<Lorg/apache/lucene/index/NormsWriterPerField;>;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 71
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;>;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Collection;

    .line 72
    .local v10, "fields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;>;"
    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 74
    .local v11, "fieldsIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;>;"
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_0

    .line 75
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lorg/apache/lucene/index/NormsWriterPerField;

    .line 77
    .local v23, "perField":Lorg/apache/lucene/index/NormsWriterPerField;
    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    move/from16 v28, v0

    if-lez v28, :cond_2

    .line 79
    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/NormsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/List;

    .line 80
    .local v14, "l":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NormsWriterPerField;>;"
    if-nez v14, :cond_1

    .line 81
    new-instance v14, Ljava/util/ArrayList;

    .end local v14    # "l":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NormsWriterPerField;>;"
    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .restart local v14    # "l":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NormsWriterPerField;>;"
    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/NormsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-interface {v4, v0, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    :cond_1
    move-object/from16 v0, v23

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 88
    .end local v14    # "l":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NormsWriterPerField;>;"
    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 92
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;>;>;"
    .end local v10    # "fields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;>;"
    .end local v11    # "fieldsIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;>;"
    .end local v23    # "perField":Lorg/apache/lucene/index/NormsWriterPerField;
    :cond_3
    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentWriteState;->segmentName:Ljava/lang/String;

    move-object/from16 v28, v0

    const-string/jumbo v29, "nrm"

    invoke-static/range {v28 .. v29}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 93
    .local v18, "normsFileName":Ljava/lang/String;
    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v19

    .line 94
    .local v19, "normsOut":Lorg/apache/lucene/store/IndexOutput;
    const/16 v24, 0x0

    .line 96
    .local v24, "success":Z
    :try_start_0
    sget-object v28, Lorg/apache/lucene/index/SegmentNorms;->NORMS_HEADER:[B

    const/16 v29, 0x0

    sget-object v30, Lorg/apache/lucene/index/SegmentNorms;->NORMS_HEADER:[B

    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 98
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/NormsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v20

    .line 100
    .local v20, "numField":I
    const/16 v17, 0x0

    .line 102
    .local v17, "normCount":I
    const/4 v8, 0x0

    .local v8, "fieldNumber":I
    :goto_1
    move/from16 v0, v20

    if-ge v8, v0, :cond_10

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/NormsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v8}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v7

    .line 106
    .local v7, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    invoke-interface {v4, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/List;

    .line 107
    .local v25, "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NormsWriterPerField;>;"
    const/16 v26, 0x0

    .line 108
    .local v26, "upto":I
    if-eqz v25, :cond_d

    .line 110
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v21

    .line 112
    .local v21, "numFields":I
    add-int/lit8 v17, v17, 0x1

    .line 114
    move/from16 v0, v21

    new-array v9, v0, [Lorg/apache/lucene/index/NormsWriterPerField;

    .line 115
    .local v9, "fields":[Lorg/apache/lucene/index/NormsWriterPerField;
    move/from16 v0, v21

    new-array v0, v0, [I

    move-object/from16 v27, v0

    .line 117
    .local v27, "uptos":[I
    const/4 v13, 0x0

    .local v13, "j":I
    :goto_2
    move/from16 v0, v21

    if-ge v13, v0, :cond_4

    .line 118
    move-object/from16 v0, v25

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lorg/apache/lucene/index/NormsWriterPerField;

    aput-object v28, v9, v13

    .line 117
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 120
    :cond_4
    move/from16 v22, v21

    .line 122
    .local v22, "numLeft":I
    :cond_5
    :goto_3
    if-lez v22, :cond_c

    .line 124
    sget-boolean v28, Lorg/apache/lucene/index/NormsWriter;->$assertionsDisabled:Z

    if-nez v28, :cond_6

    const/16 v28, 0x0

    aget v28, v27, v28

    const/16 v29, 0x0

    aget-object v29, v9, v29

    move-object/from16 v0, v29

    iget-object v0, v0, Lorg/apache/lucene/index/NormsWriterPerField;->docIDs:[I

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    array-length v0, v0

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-lt v0, v1, :cond_6

    new-instance v28, Ljava/lang/AssertionError;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v30, " uptos[0]="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const/16 v30, 0x0

    aget v30, v27, v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string/jumbo v30, " len="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const/16 v30, 0x0

    aget-object v30, v9, v30

    move-object/from16 v0, v30

    iget-object v0, v0, Lorg/apache/lucene/index/NormsWriterPerField;->docIDs:[I

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-direct/range {v28 .. v29}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v28
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    .end local v7    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .end local v8    # "fieldNumber":I
    .end local v9    # "fields":[Lorg/apache/lucene/index/NormsWriterPerField;
    .end local v13    # "j":I
    .end local v17    # "normCount":I
    .end local v20    # "numField":I
    .end local v21    # "numFields":I
    .end local v22    # "numLeft":I
    .end local v25    # "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NormsWriterPerField;>;"
    .end local v26    # "upto":I
    .end local v27    # "uptos":[I
    :catchall_0
    move-exception v28

    if-eqz v24, :cond_11

    .line 172
    const/16 v29, 0x1

    move/from16 v0, v29

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    aput-object v19, v29, v30

    invoke-static/range {v29 .. v29}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 171
    :goto_4
    throw v28

    .line 126
    .restart local v7    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .restart local v8    # "fieldNumber":I
    .restart local v9    # "fields":[Lorg/apache/lucene/index/NormsWriterPerField;
    .restart local v13    # "j":I
    .restart local v17    # "normCount":I
    .restart local v20    # "numField":I
    .restart local v21    # "numFields":I
    .restart local v22    # "numLeft":I
    .restart local v25    # "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NormsWriterPerField;>;"
    .restart local v26    # "upto":I
    .restart local v27    # "uptos":[I
    :cond_6
    const/16 v16, 0x0

    .line 127
    .local v16, "minLoc":I
    const/16 v28, 0x0

    :try_start_1
    aget-object v28, v9, v28

    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/index/NormsWriterPerField;->docIDs:[I

    move-object/from16 v28, v0

    const/16 v29, 0x0

    aget v29, v27, v29

    aget v15, v28, v29

    .line 129
    .local v15, "minDocID":I
    const/4 v13, 0x1

    :goto_5
    move/from16 v0, v22

    if-ge v13, v0, :cond_8

    .line 130
    aget-object v28, v9, v13

    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/index/NormsWriterPerField;->docIDs:[I

    move-object/from16 v28, v0

    aget v29, v27, v13

    aget v5, v28, v29

    .line 131
    .local v5, "docID":I
    if-ge v5, v15, :cond_7

    .line 132
    move v15, v5

    .line 133
    move/from16 v16, v13

    .line 129
    :cond_7
    add-int/lit8 v13, v13, 0x1

    goto :goto_5

    .line 137
    .end local v5    # "docID":I
    :cond_8
    sget-boolean v28, Lorg/apache/lucene/index/NormsWriter;->$assertionsDisabled:Z

    if-nez v28, :cond_9

    move-object/from16 v0, p2

    iget v0, v0, Lorg/apache/lucene/index/SegmentWriteState;->numDocs:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-lt v15, v0, :cond_9

    new-instance v28, Ljava/lang/AssertionError;

    invoke-direct/range {v28 .. v28}, Ljava/lang/AssertionError;-><init>()V

    throw v28

    .line 140
    :cond_9
    :goto_6
    move/from16 v0, v26

    if-ge v0, v15, :cond_a

    .line 141
    move-object/from16 v0, p0

    iget-byte v0, v0, Lorg/apache/lucene/index/NormsWriter;->defaultNorm:B

    move/from16 v28, v0

    move-object/from16 v0, v19

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 140
    add-int/lit8 v26, v26, 0x1

    goto :goto_6

    .line 143
    :cond_a
    aget-object v28, v9, v16

    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/index/NormsWriterPerField;->norms:[B

    move-object/from16 v28, v0

    aget v29, v27, v16

    aget-byte v28, v28, v29

    move-object/from16 v0, v19

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 144
    aget v28, v27, v16

    add-int/lit8 v28, v28, 0x1

    aput v28, v27, v16

    .line 145
    add-int/lit8 v26, v26, 0x1

    .line 147
    aget v28, v27, v16

    aget-object v29, v9, v16

    move-object/from16 v0, v29

    iget v0, v0, Lorg/apache/lucene/index/NormsWriterPerField;->upto:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_5

    .line 148
    aget-object v28, v9, v16

    invoke-virtual/range {v28 .. v28}, Lorg/apache/lucene/index/NormsWriterPerField;->reset()V

    .line 149
    add-int/lit8 v28, v22, -0x1

    move/from16 v0, v16

    move/from16 v1, v28

    if-eq v0, v1, :cond_b

    .line 150
    add-int/lit8 v28, v22, -0x1

    aget-object v28, v9, v28

    aput-object v28, v9, v16

    .line 151
    add-int/lit8 v28, v22, -0x1

    aget v28, v27, v28

    aput v28, v27, v16

    .line 153
    :cond_b
    add-int/lit8 v22, v22, -0x1

    goto/16 :goto_3

    .line 158
    .end local v15    # "minDocID":I
    .end local v16    # "minLoc":I
    :cond_c
    :goto_7
    move-object/from16 v0, p2

    iget v0, v0, Lorg/apache/lucene/index/SegmentWriteState;->numDocs:I

    move/from16 v28, v0

    move/from16 v0, v26

    move/from16 v1, v28

    if-ge v0, v1, :cond_e

    .line 159
    move-object/from16 v0, p0

    iget-byte v0, v0, Lorg/apache/lucene/index/NormsWriter;->defaultNorm:B

    move/from16 v28, v0

    move-object/from16 v0, v19

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 158
    add-int/lit8 v26, v26, 0x1

    goto :goto_7

    .line 160
    .end local v9    # "fields":[Lorg/apache/lucene/index/NormsWriterPerField;
    .end local v13    # "j":I
    .end local v21    # "numFields":I
    .end local v22    # "numLeft":I
    .end local v27    # "uptos":[I
    :cond_d
    iget-boolean v0, v7, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    move/from16 v28, v0

    if-eqz v28, :cond_e

    iget-boolean v0, v7, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    move/from16 v28, v0

    if-nez v28, :cond_e

    .line 161
    add-int/lit8 v17, v17, 0x1

    .line 163
    :goto_8
    move-object/from16 v0, p2

    iget v0, v0, Lorg/apache/lucene/index/SegmentWriteState;->numDocs:I

    move/from16 v28, v0

    move/from16 v0, v26

    move/from16 v1, v28

    if-ge v0, v1, :cond_e

    .line 164
    move-object/from16 v0, p0

    iget-byte v0, v0, Lorg/apache/lucene/index/NormsWriter;->defaultNorm:B

    move/from16 v28, v0

    move-object/from16 v0, v19

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 163
    add-int/lit8 v26, v26, 0x1

    goto :goto_8

    .line 167
    :cond_e
    sget-boolean v28, Lorg/apache/lucene/index/NormsWriter;->$assertionsDisabled:Z

    if-nez v28, :cond_f

    const-wide/16 v28, 0x4

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v30, v0

    move-object/from16 v0, p2

    iget v0, v0, Lorg/apache/lucene/index/SegmentWriteState;->numDocs:I

    move/from16 v32, v0

    move/from16 v0, v32

    int-to-long v0, v0

    move-wide/from16 v32, v0

    mul-long v30, v30, v32

    add-long v28, v28, v30

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v30

    cmp-long v28, v28, v30

    if-eqz v28, :cond_f

    new-instance v28, Ljava/lang/AssertionError;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v30, ".nrm file size mismatch: expected="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-wide/16 v30, 0x4

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v32, v0

    move-object/from16 v0, p2

    iget v0, v0, Lorg/apache/lucene/index/SegmentWriteState;->numDocs:I

    move/from16 v34, v0

    move/from16 v0, v34

    int-to-long v0, v0

    move-wide/from16 v34, v0

    mul-long v32, v32, v34

    add-long v30, v30, v32

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string/jumbo v30, " actual="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v30

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-direct/range {v28 .. v29}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v28
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :cond_f
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 169
    .end local v7    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .end local v25    # "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NormsWriterPerField;>;"
    .end local v26    # "upto":I
    :cond_10
    const/16 v24, 0x1

    .line 171
    if-eqz v24, :cond_12

    .line 172
    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    aput-object v19, v28, v29

    invoke-static/range {v28 .. v28}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 177
    :goto_9
    return-void

    .line 174
    .end local v8    # "fieldNumber":I
    .end local v17    # "normCount":I
    .end local v20    # "numField":I
    :cond_11
    const/16 v29, 0x1

    move/from16 v0, v29

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    aput-object v19, v29, v30

    invoke-static/range {v29 .. v29}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto/16 :goto_4

    .restart local v8    # "fieldNumber":I
    .restart local v17    # "normCount":I
    .restart local v20    # "numField":I
    :cond_12
    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    aput-object v19, v28, v29

    invoke-static/range {v28 .. v28}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_9
.end method

.method setFieldInfos(Lorg/apache/lucene/index/FieldInfos;)V
    .locals 0
    .param p1, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;

    .prologue
    .line 56
    iput-object p1, p0, Lorg/apache/lucene/index/NormsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 57
    return-void
.end method
