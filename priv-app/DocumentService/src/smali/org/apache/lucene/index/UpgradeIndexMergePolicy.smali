.class public Lorg/apache/lucene/index/UpgradeIndexMergePolicy;
.super Lorg/apache/lucene/index/MergePolicy;
.source "UpgradeIndexMergePolicy.java"


# instance fields
.field protected final base:Lorg/apache/lucene/index/MergePolicy;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/MergePolicy;)V
    .locals 0
    .param p1, "base"    # Lorg/apache/lucene/index/MergePolicy;

    .prologue
    .line 57
    invoke-direct {p0}, Lorg/apache/lucene/index/MergePolicy;-><init>()V

    .line 58
    iput-object p1, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    .line 59
    return-void
.end method

.method private message(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 154
    invoke-direct {p0}, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->verbose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v0}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "UPGMP: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 156
    :cond_0
    return-void
.end method

.method private verbose()Z
    .locals 2

    .prologue
    .line 149
    iget-object v1, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v1}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriter;

    .line 150
    .local v0, "w":Lorg/apache/lucene/index/IndexWriter;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->verbose()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MergePolicy;->close()V

    .line 141
    return-void
.end method

.method public findForcedDeletesMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 1
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/MergePolicy;->findForcedDeletesMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v0

    return-object v0
.end method

.method public findForcedMerges(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 9
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "maxSegmentCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "I",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lorg/apache/lucene/index/MergePolicy$MergeSpecification;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    .local p3, "segmentsToMerge":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/Boolean;>;"
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 85
    .local v2, "oldSegments":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/Boolean;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SegmentInfo;

    .line 86
    .local v4, "si":Lorg/apache/lucene/index/SegmentInfo;
    invoke-interface {p3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    .line 87
    .local v6, "v":Ljava/lang/Boolean;
    if-eqz v6, :cond_0

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->shouldUpgradeSegment(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 88
    invoke-interface {v2, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 92
    .end local v4    # "si":Lorg/apache/lucene/index/SegmentInfo;
    .end local v6    # "v":Ljava/lang/Boolean;
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->verbose()Z

    move-result v7

    if-eqz v7, :cond_2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "findForcedMerges: segmentsToUpgrade="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->message(Ljava/lang/String;)V

    .line 94
    :cond_2
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 95
    const/4 v5, 0x0

    .line 125
    :cond_3
    :goto_1
    return-object v5

    .line 97
    :cond_4
    iget-object v7, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v7, p1, p2, v2}, Lorg/apache/lucene/index/MergePolicy;->findForcedMerges(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v5

    .line 99
    .local v5, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    if-eqz v5, :cond_5

    .line 103
    iget-object v7, v5, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 104
    .local v3, "om":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    iget-object v8, v3, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v7, v8}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 108
    .end local v3    # "om":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_5
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 109
    invoke-direct {p0}, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->verbose()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 110
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "findForcedMerges: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " does not want to merge all old segments, merge remaining ones into new segment: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->message(Ljava/lang/String;)V

    .line 112
    :cond_6
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 113
    .local v1, "newInfos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_7
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SegmentInfo;

    .line 114
    .restart local v4    # "si":Lorg/apache/lucene/index/SegmentInfo;
    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 115
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 119
    .end local v4    # "si":Lorg/apache/lucene/index/SegmentInfo;
    :cond_8
    if-nez v5, :cond_9

    .line 120
    new-instance v5, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    .end local v5    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-direct {v5}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 122
    .restart local v5    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_9
    new-instance v7, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-direct {v7, v1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    invoke-virtual {v5, v7}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    goto/16 :goto_1
.end method

.method public findMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 1
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/MergePolicy;->findMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v0

    return-object v0
.end method

.method public setIndexWriter(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;

    .prologue
    .line 72
    invoke-super {p0, p1}, Lorg/apache/lucene/index/MergePolicy;->setIndexWriter(Lorg/apache/lucene/index/IndexWriter;)V

    .line 73
    iget-object v0, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/MergePolicy;->setIndexWriter(Lorg/apache/lucene/index/IndexWriter;)V

    .line 74
    return-void
.end method

.method protected shouldUpgradeSegment(Lorg/apache/lucene/index/SegmentInfo;)Z
    .locals 2
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    .line 67
    sget-object v0, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfo;)Z
    .locals 1
    .param p1, "segments"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "newSegment"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;->base:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/MergePolicy;->useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v0

    return v0
.end method
