.class Lorg/apache/lucene/index/BufferedDeletesStream;
.super Ljava/lang/Object;
.source "BufferedDeletesStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;,
        Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final sortByDelGen:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

.field private final deletes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/FrozenBufferedDeletes;",
            ">;"
        }
    .end annotation
.end field

.field private infoStream:Ljava/io/PrintStream;

.field private lastDeleteTerm:Lorg/apache/lucene/index/Term;

.field private final messageID:I

.field private nextGen:J

.field private final numTerms:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    .line 134
    new-instance v0, Lorg/apache/lucene/index/BufferedDeletesStream$1;

    invoke-direct {v0}, Lorg/apache/lucene/index/BufferedDeletesStream$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/BufferedDeletesStream;->sortByDelGen:Ljava/util/Comparator;

    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "messageID"    # I

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    .line 58
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    .line 64
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    .line 65
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 69
    iput p1, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->messageID:I

    .line 70
    return-void
.end method

.method private declared-synchronized applyQueryDeletes(Ljava/lang/Iterable;Lorg/apache/lucene/index/SegmentReader;)J
    .locals 12
    .param p2, "reader"    # Lorg/apache/lucene/index/SegmentReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;",
            ">;",
            "Lorg/apache/lucene/index/SegmentReader;",
            ")J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 374
    .local p1, "queriesIter":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;>;"
    monitor-enter p0

    const-wide/16 v0, 0x0

    .line 376
    .local v0, "delCount":J
    :try_start_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;

    .line 377
    .local v4, "ent":Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;
    iget-object v8, v4, Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;->query:Lorg/apache/lucene/search/Query;

    .line 378
    .local v8, "query":Lorg/apache/lucene/search/Query;
    iget v7, v4, Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;->limit:I

    .line 379
    .local v7, "limit":I
    new-instance v9, Lorg/apache/lucene/search/QueryWrapperFilter;

    invoke-direct {v9, v8}, Lorg/apache/lucene/search/QueryWrapperFilter;-><init>(Lorg/apache/lucene/search/Query;)V

    invoke-virtual {v9, p2}, Lorg/apache/lucene/search/QueryWrapperFilter;->getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v3

    .line 380
    .local v3, "docs":Lorg/apache/lucene/search/DocIdSet;
    if-eqz v3, :cond_0

    .line 381
    invoke-virtual {v3}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v6

    .line 382
    .local v6, "it":Lorg/apache/lucene/search/DocIdSetIterator;
    if-eqz v6, :cond_0

    .line 384
    :goto_0
    invoke-virtual {v6}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v2

    .line 385
    .local v2, "doc":I
    if-ge v2, v7, :cond_0

    .line 388
    invoke-virtual {p2, v2}, Lorg/apache/lucene/index/SegmentReader;->deleteDocument(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394
    const-wide/16 v10, 0x1

    add-long/2addr v0, v10

    .line 395
    goto :goto_0

    .line 400
    .end local v2    # "doc":I
    .end local v3    # "docs":Lorg/apache/lucene/search/DocIdSet;
    .end local v4    # "ent":Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;
    .end local v6    # "it":Lorg/apache/lucene/search/DocIdSetIterator;
    .end local v7    # "limit":I
    .end local v8    # "query":Lorg/apache/lucene/search/Query;
    :cond_1
    monitor-exit p0

    return-wide v0

    .line 374
    .end local v5    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9
.end method

.method private declared-synchronized applyTermDeletes(Ljava/lang/Iterable;Lorg/apache/lucene/index/SegmentReader;)J
    .locals 8
    .param p2, "reader"    # Lorg/apache/lucene/index/SegmentReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;",
            "Lorg/apache/lucene/index/SegmentReader;",
            ")J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 334
    .local p1, "termsIter":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/index/Term;>;"
    monitor-enter p0

    const-wide/16 v0, 0x0

    .line 336
    .local v0, "delCount":J
    :try_start_0
    sget-boolean v6, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteTerm(Lorg/apache/lucene/index/Term;)Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 338
    :cond_0
    :try_start_1
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentReader;->termDocs()Lorg/apache/lucene/index/TermDocs;

    move-result-object v3

    .line 340
    .local v3, "docs":Lorg/apache/lucene/index/TermDocs;
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/Term;

    .line 345
    .local v5, "term":Lorg/apache/lucene/index/Term;
    sget-boolean v6, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v6, :cond_2

    invoke-direct {p0, v5}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteTerm(Lorg/apache/lucene/index/Term;)Z

    move-result v6

    if-nez v6, :cond_2

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 346
    :cond_2
    invoke-interface {v3, v5}, Lorg/apache/lucene/index/TermDocs;->seek(Lorg/apache/lucene/index/Term;)V

    .line 348
    :goto_0
    invoke-interface {v3}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 349
    invoke-interface {v3}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v2

    .line 350
    .local v2, "docID":I
    invoke-virtual {p2, v2}, Lorg/apache/lucene/index/SegmentReader;->deleteDocument(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 356
    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    .line 357
    goto :goto_0

    .line 360
    .end local v2    # "docID":I
    .end local v5    # "term":Lorg/apache/lucene/index/Term;
    :cond_3
    monitor-exit p0

    return-wide v0
.end method

.method private checkDeleteStats()Z
    .locals 8

    .prologue
    .line 415
    const/4 v3, 0x0

    .line 416
    .local v3, "numTerms2":I
    const-wide/16 v0, 0x0

    .line 417
    .local v0, "bytesUsed2":J
    iget-object v5, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .line 418
    .local v4, "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    iget v5, v4, Lorg/apache/lucene/index/FrozenBufferedDeletes;->numTermDeletes:I

    add-int/2addr v3, v5

    .line 419
    iget v5, v4, Lorg/apache/lucene/index/FrozenBufferedDeletes;->bytesUsed:I

    int-to-long v6, v5

    add-long/2addr v0, v6

    goto :goto_0

    .line 421
    .end local v4    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    :cond_0
    sget-boolean v5, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    iget-object v5, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v5

    if-eq v3, v5, :cond_1

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "numTerms2="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " vs "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 422
    :cond_1
    sget-boolean v5, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v6

    cmp-long v5, v0, v6

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "bytesUsed2="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " vs "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 423
    :cond_2
    const/4 v5, 0x1

    return v5
.end method

.method private checkDeleteTerm(Lorg/apache/lucene/index/Term;)Z
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 405
    if-eqz p1, :cond_0

    .line 406
    sget-boolean v0, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->lastDeleteTerm:Lorg/apache/lucene/index/Term;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->lastDeleteTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "lastTerm="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->lastDeleteTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " vs term="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 409
    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->lastDeleteTerm:Lorg/apache/lucene/index/Term;

    .line 410
    const/4 v0, 0x1

    return v0

    .line 409
    :cond_1
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private declared-synchronized message(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "BD "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->messageID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    :cond_0
    monitor-exit p0

    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized prune(I)V
    .locals 6
    .param p1, "count"    # I

    .prologue
    .line 317
    monitor-enter p0

    if-lez p1, :cond_4

    .line 318
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_0

    .line 319
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pruneDeletes: prune "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " packets; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    sub-int/2addr v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " packets remain"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->message(Ljava/lang/String;)V

    .line 321
    :cond_0
    const/4 v0, 0x0

    .local v0, "delIDX":I
    :goto_0
    if-ge v0, p1, :cond_3

    .line 322
    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .line 323
    .local v1, "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    iget v3, v1, Lorg/apache/lucene/index/FrozenBufferedDeletes;->numTermDeletes:I

    neg-int v3, v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 324
    sget-boolean v2, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    if-gez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    .end local v0    # "delIDX":I
    .end local v1    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 325
    .restart local v0    # "delIDX":I
    .restart local v1    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    iget v3, v1, Lorg/apache/lucene/index/FrozenBufferedDeletes;->bytesUsed:I

    neg-int v3, v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 326
    sget-boolean v2, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 321
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 328
    .end local v1    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3, p1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330
    .end local v0    # "delIDX":I
    :cond_4
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public any()Z
    .locals 4

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized applyDeletes(Lorg/apache/lucene/index/IndexWriter$ReaderPool;Ljava/util/List;)Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;
    .locals 26
    .param p1, "readerPool"    # Lorg/apache/lucene/index/IndexWriter$ReaderPool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/IndexWriter$ReaderPool;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;)",
            "Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    .local p2, "infos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 154
    .local v18, "t0":J
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v15

    if-nez v15, :cond_0

    .line 155
    new-instance v15, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x1

    add-long v24, v24, v22

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    const/16 v21, 0x0

    move/from16 v0, v20

    move-wide/from16 v1, v22

    move-object/from16 v3, v21

    invoke-direct {v15, v0, v1, v2, v3}, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;-><init>(ZJLjava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279
    :goto_0
    monitor-exit p0

    return-object v15

    .line 158
    :cond_0
    :try_start_1
    sget-boolean v15, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v15, :cond_1

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteStats()Z

    move-result v15

    if-nez v15, :cond_1

    new-instance v15, Ljava/lang/AssertionError;

    invoke-direct {v15}, Ljava/lang/AssertionError;-><init>()V

    throw v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152
    .end local v18    # "t0":J
    :catchall_0
    move-exception v15

    monitor-exit p0

    throw v15

    .line 160
    .restart local v18    # "t0":J
    :cond_1
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->any()Z

    move-result v15

    if-nez v15, :cond_2

    .line 161
    const-string/jumbo v15, "applyDeletes: no deletes; skipping"

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/index/BufferedDeletesStream;->message(Ljava/lang/String;)V

    .line 162
    new-instance v15, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x1

    add-long v24, v24, v22

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    const/16 v21, 0x0

    move/from16 v0, v20

    move-wide/from16 v1, v22

    move-object/from16 v3, v21

    invoke-direct {v15, v0, v1, v2, v3}, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;-><init>(ZJLjava/util/List;)V

    goto :goto_0

    .line 165
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Ljava/io/PrintStream;

    if-eqz v15, :cond_3

    .line 166
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "applyDeletes: infos="

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v20, " packetCount="

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/index/BufferedDeletesStream;->message(Ljava/lang/String;)V

    .line 169
    :cond_3
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 170
    .local v10, "infos2":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    move-object/from16 v0, p2

    invoke-interface {v10, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 171
    sget-object v15, Lorg/apache/lucene/index/BufferedDeletesStream;->sortByDelGen:Ljava/util/Comparator;

    invoke-static {v10, v15}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 173
    const/4 v6, 0x0

    .line 174
    .local v6, "coalescedDeletes":Lorg/apache/lucene/index/CoalescedDeletes;
    const/4 v5, 0x0

    .line 176
    .local v5, "anyNewDeletes":Z
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v15

    add-int/lit8 v11, v15, -0x1

    .line 177
    .local v11, "infosIDX":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    add-int/lit8 v8, v15, -0x1

    .line 179
    .local v8, "delIDX":I
    const/4 v4, 0x0

    .line 181
    .local v4, "allDeleted":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    :goto_1
    if-ltz v11, :cond_1a

    .line 184
    if-ltz v8, :cond_5

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v15, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/index/FrozenBufferedDeletes;

    move-object v12, v15

    .line 185
    .local v12, "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    :goto_2
    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/index/SegmentInfo;

    .line 186
    .local v9, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v9}, Lorg/apache/lucene/index/SegmentInfo;->getBufferedDeletesGen()J

    move-result-wide v16

    .line 188
    .local v16, "segGen":J
    if-eqz v12, :cond_6

    iget-wide v0, v12, Lorg/apache/lucene/index/FrozenBufferedDeletes;->gen:J

    move-wide/from16 v20, v0

    cmp-long v15, v16, v20

    if-gez v15, :cond_6

    .line 190
    if-nez v6, :cond_4

    .line 191
    new-instance v6, Lorg/apache/lucene/index/CoalescedDeletes;

    .end local v6    # "coalescedDeletes":Lorg/apache/lucene/index/CoalescedDeletes;
    invoke-direct {v6}, Lorg/apache/lucene/index/CoalescedDeletes;-><init>()V

    .line 193
    .restart local v6    # "coalescedDeletes":Lorg/apache/lucene/index/CoalescedDeletes;
    :cond_4
    invoke-virtual {v6, v12}, Lorg/apache/lucene/index/CoalescedDeletes;->update(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    .line 194
    add-int/lit8 v8, v8, -0x1

    goto :goto_1

    .line 184
    .end local v9    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v12    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    .end local v16    # "segGen":J
    :cond_5
    const/4 v12, 0x0

    goto :goto_2

    .line 195
    .restart local v9    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .restart local v12    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    .restart local v16    # "segGen":J
    :cond_6
    if-eqz v12, :cond_11

    iget-wide v0, v12, Lorg/apache/lucene/index/FrozenBufferedDeletes;->gen:J

    move-wide/from16 v20, v0

    cmp-long v15, v16, v20

    if-nez v15, :cond_11

    .line 199
    sget-boolean v15, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v15, :cond_7

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->infoIsLive(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v15

    if-nez v15, :cond_7

    new-instance v15, Ljava/lang/AssertionError;

    invoke-direct {v15}, Ljava/lang/AssertionError;-><init>()V

    throw v15

    .line 200
    :cond_7
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v15}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfo;Z)Lorg/apache/lucene/index/SegmentReader;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v13

    .line 201
    .local v13, "reader":Lorg/apache/lucene/index/SegmentReader;
    const/4 v7, 0x0

    .line 204
    .local v7, "delCount":I
    if-eqz v6, :cond_8

    .line 206
    int-to-long v0, v7

    move-wide/from16 v20, v0

    :try_start_3
    invoke-virtual {v6}, Lorg/apache/lucene/index/CoalescedDeletes;->termsIterable()Ljava/lang/Iterable;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13}, Lorg/apache/lucene/index/BufferedDeletesStream;->applyTermDeletes(Ljava/lang/Iterable;Lorg/apache/lucene/index/SegmentReader;)J

    move-result-wide v22

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v7, v0

    .line 207
    int-to-long v0, v7

    move-wide/from16 v20, v0

    invoke-virtual {v6}, Lorg/apache/lucene/index/CoalescedDeletes;->queriesIterable()Ljava/lang/Iterable;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13}, Lorg/apache/lucene/index/BufferedDeletesStream;->applyQueryDeletes(Ljava/lang/Iterable;Lorg/apache/lucene/index/SegmentReader;)J

    move-result-wide v22

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v7, v0

    .line 212
    :cond_8
    int-to-long v0, v7

    move-wide/from16 v20, v0

    invoke-virtual {v12}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queriesIterable()Ljava/lang/Iterable;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13}, Lorg/apache/lucene/index/BufferedDeletesStream;->applyQueryDeletes(Ljava/lang/Iterable;Lorg/apache/lucene/index/SegmentReader;)J

    move-result-wide v22

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v7, v0

    .line 213
    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentReader;->numDocs()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v15

    if-nez v15, :cond_d

    const/4 v14, 0x1

    .line 215
    .local v14, "segAllDeletes":Z
    :goto_3
    :try_start_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/SegmentReader;)Z

    .line 217
    if-lez v7, :cond_e

    const/4 v15, 0x1

    :goto_4
    or-int/2addr v5, v15

    .line 219
    if-eqz v14, :cond_a

    .line 220
    if-nez v4, :cond_9

    .line 221
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "allDeleted":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 223
    .restart local v4    # "allDeleted":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    :cond_9
    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    :cond_a
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Ljava/io/PrintStream;

    if-eqz v15, :cond_b

    .line 227
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "seg="

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v20, " segGen="

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v20, " segDeletes=["

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v20, "]; coalesced deletes=["

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    if-nez v6, :cond_f

    const-string/jumbo v15, "null"

    :goto_5
    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v20, "] delCount="

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    if-eqz v14, :cond_10

    const-string/jumbo v15, " 100% deleted"

    :goto_6
    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/index/BufferedDeletesStream;->message(Ljava/lang/String;)V

    .line 230
    :cond_b
    if-nez v6, :cond_c

    .line 231
    new-instance v6, Lorg/apache/lucene/index/CoalescedDeletes;

    .end local v6    # "coalescedDeletes":Lorg/apache/lucene/index/CoalescedDeletes;
    invoke-direct {v6}, Lorg/apache/lucene/index/CoalescedDeletes;-><init>()V

    .line 233
    .restart local v6    # "coalescedDeletes":Lorg/apache/lucene/index/CoalescedDeletes;
    :cond_c
    invoke-virtual {v6, v12}, Lorg/apache/lucene/index/CoalescedDeletes;->update(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    .line 234
    add-int/lit8 v8, v8, -0x1

    .line 235
    add-int/lit8 v11, v11, -0x1

    .line 236
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v9, v0, v1}, Lorg/apache/lucene/index/SegmentInfo;->setBufferedDeletesGen(J)V

    goto/16 :goto_1

    .line 213
    .end local v14    # "segAllDeletes":Z
    :cond_d
    const/4 v14, 0x0

    goto/16 :goto_3

    .line 215
    :catchall_1
    move-exception v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/SegmentReader;)Z

    throw v15

    .line 217
    .restart local v14    # "segAllDeletes":Z
    :cond_e
    const/4 v15, 0x0

    goto/16 :goto_4

    :cond_f
    move-object v15, v6

    .line 227
    goto :goto_5

    :cond_10
    const-string/jumbo v15, ""

    goto :goto_6

    .line 241
    .end local v7    # "delCount":I
    .end local v13    # "reader":Lorg/apache/lucene/index/SegmentReader;
    .end local v14    # "segAllDeletes":Z
    :cond_11
    if-eqz v6, :cond_15

    .line 243
    sget-boolean v15, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v15, :cond_12

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->infoIsLive(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v15

    if-nez v15, :cond_12

    new-instance v15, Ljava/lang/AssertionError;

    invoke-direct {v15}, Ljava/lang/AssertionError;-><init>()V

    throw v15

    .line 244
    :cond_12
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v15}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfo;Z)Lorg/apache/lucene/index/SegmentReader;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v13

    .line 245
    .restart local v13    # "reader":Lorg/apache/lucene/index/SegmentReader;
    const/4 v7, 0x0

    .line 248
    .restart local v7    # "delCount":I
    int-to-long v0, v7

    move-wide/from16 v20, v0

    :try_start_5
    invoke-virtual {v6}, Lorg/apache/lucene/index/CoalescedDeletes;->termsIterable()Ljava/lang/Iterable;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13}, Lorg/apache/lucene/index/BufferedDeletesStream;->applyTermDeletes(Ljava/lang/Iterable;Lorg/apache/lucene/index/SegmentReader;)J

    move-result-wide v22

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v7, v0

    .line 249
    int-to-long v0, v7

    move-wide/from16 v20, v0

    invoke-virtual {v6}, Lorg/apache/lucene/index/CoalescedDeletes;->queriesIterable()Ljava/lang/Iterable;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v13}, Lorg/apache/lucene/index/BufferedDeletesStream;->applyQueryDeletes(Ljava/lang/Iterable;Lorg/apache/lucene/index/SegmentReader;)J

    move-result-wide v22

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v7, v0

    .line 250
    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentReader;->numDocs()I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result v15

    if-nez v15, :cond_16

    const/4 v14, 0x1

    .line 252
    .restart local v14    # "segAllDeletes":Z
    :goto_7
    :try_start_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/SegmentReader;)Z

    .line 254
    if-lez v7, :cond_17

    const/4 v15, 0x1

    :goto_8
    or-int/2addr v5, v15

    .line 256
    if-eqz v14, :cond_14

    .line 257
    if-nez v4, :cond_13

    .line 258
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "allDeleted":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 260
    .restart local v4    # "allDeleted":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    :cond_13
    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    :cond_14
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Ljava/io/PrintStream;

    if-eqz v15, :cond_15

    .line 264
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "seg="

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v20, " segGen="

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v20, " coalesced deletes=["

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    if-nez v6, :cond_18

    const-string/jumbo v15, "null"

    :goto_9
    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v20, "] delCount="

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    if-eqz v14, :cond_19

    const-string/jumbo v15, " 100% deleted"

    :goto_a
    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/index/BufferedDeletesStream;->message(Ljava/lang/String;)V

    .line 267
    .end local v7    # "delCount":I
    .end local v13    # "reader":Lorg/apache/lucene/index/SegmentReader;
    .end local v14    # "segAllDeletes":Z
    :cond_15
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v9, v0, v1}, Lorg/apache/lucene/index/SegmentInfo;->setBufferedDeletesGen(J)V

    .line 269
    add-int/lit8 v11, v11, -0x1

    goto/16 :goto_1

    .line 250
    .restart local v7    # "delCount":I
    .restart local v13    # "reader":Lorg/apache/lucene/index/SegmentReader;
    :cond_16
    const/4 v14, 0x0

    goto :goto_7

    .line 252
    :catchall_2
    move-exception v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/SegmentReader;)Z

    throw v15

    .line 254
    .restart local v14    # "segAllDeletes":Z
    :cond_17
    const/4 v15, 0x0

    goto :goto_8

    :cond_18
    move-object v15, v6

    .line 264
    goto :goto_9

    :cond_19
    const-string/jumbo v15, ""

    goto :goto_a

    .line 273
    .end local v7    # "delCount":I
    .end local v9    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v12    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    .end local v13    # "reader":Lorg/apache/lucene/index/SegmentReader;
    .end local v14    # "segAllDeletes":Z
    .end local v16    # "segGen":J
    :cond_1a
    sget-boolean v15, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v15, :cond_1b

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteStats()Z

    move-result v15

    if-nez v15, :cond_1b

    new-instance v15, Ljava/lang/AssertionError;

    invoke-direct {v15}, Ljava/lang/AssertionError;-><init>()V

    throw v15

    .line 274
    :cond_1b
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Ljava/io/PrintStream;

    if-eqz v15, :cond_1c

    .line 275
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "applyDeletes took "

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v20, v20, v18

    move-wide/from16 v0, v20

    invoke-virtual {v15, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v20, " msec"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/index/BufferedDeletesStream;->message(Ljava/lang/String;)V

    .line 279
    :cond_1c
    new-instance v15, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x1

    add-long v22, v22, v20

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    move-wide/from16 v0, v20

    invoke-direct {v15, v5, v0, v1, v4}, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;-><init>(ZJLjava/util/List;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0
.end method

.method public bytesUsed()J
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized clear()V
    .locals 4

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 99
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    .line 100
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 101
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    monitor-exit p0

    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNextGen()J
    .locals 4

    .prologue
    .line 283
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public numTerms()I
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public declared-synchronized prune(Lorg/apache/lucene/index/SegmentInfos;)V
    .locals 8
    .param p1, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;

    .prologue
    .line 291
    monitor-enter p0

    :try_start_0
    sget-boolean v6, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteStats()Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 292
    :cond_0
    const-wide v4, 0x7fffffffffffffffL

    .line 293
    .local v4, "minGen":J
    :try_start_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentInfo;

    .line 294
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->getBufferedDeletesGen()J

    move-result-wide v6

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    goto :goto_0

    .line 297
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Ljava/io/PrintStream;

    if-eqz v6, :cond_2

    .line 298
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "prune sis="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " minGen="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " packetCount="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lorg/apache/lucene/index/BufferedDeletesStream;->message(Ljava/lang/String;)V

    .line 301
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    .line 302
    .local v3, "limit":I
    const/4 v0, 0x0

    .local v0, "delIDX":I
    :goto_1
    if-ge v0, v3, :cond_4

    .line 303
    iget-object v6, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/FrozenBufferedDeletes;

    iget-wide v6, v6, Lorg/apache/lucene/index/FrozenBufferedDeletes;->gen:J

    cmp-long v6, v6, v4

    if-ltz v6, :cond_3

    .line 304
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/BufferedDeletesStream;->prune(I)V

    .line 305
    sget-boolean v6, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v6, :cond_6

    invoke-direct {p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteStats()Z

    move-result v6

    if-nez v6, :cond_6

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 302
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 311
    :cond_4
    invoke-direct {p0, v3}, Lorg/apache/lucene/index/BufferedDeletesStream;->prune(I)V

    .line 312
    sget-boolean v6, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v6, :cond_5

    invoke-virtual {p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->any()Z

    move-result v6

    if-eqz v6, :cond_5

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 313
    :cond_5
    sget-boolean v6, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v6, :cond_6

    invoke-direct {p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteStats()Z

    move-result v6

    if-nez v6, :cond_6

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 314
    :cond_6
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized push(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V
    .locals 4
    .param p1, "packet"    # Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .prologue
    .line 85
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->any()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 86
    :cond_0
    :try_start_1
    sget-boolean v0, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteStats()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 87
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-wide v0, p1, Lorg/apache/lucene/index/FrozenBufferedDeletes;->gen:J

    iget-wide v2, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->nextGen:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 88
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms:Ljava/util/concurrent/atomic/AtomicInteger;

    iget v1, p1, Lorg/apache/lucene/index/FrozenBufferedDeletes;->numTermDeletes:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 90
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    iget v1, p1, Lorg/apache/lucene/index/FrozenBufferedDeletes;->bytesUsed:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 91
    iget-object v0, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_3

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "push deletes "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " delGen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p1, Lorg/apache/lucene/index/FrozenBufferedDeletes;->gen:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " packetCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->deletes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/BufferedDeletesStream;->message(Ljava/lang/String;)V

    .line 94
    :cond_3
    sget-boolean v0, Lorg/apache/lucene/index/BufferedDeletesStream;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    invoke-direct {p0}, Lorg/apache/lucene/index/BufferedDeletesStream;->checkDeleteStats()Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 95
    :cond_4
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized setInfoStream(Ljava/io/PrintStream;)V
    .locals 1
    .param p1, "infoStream"    # Ljava/io/PrintStream;

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/lucene/index/BufferedDeletesStream;->infoStream:Ljava/io/PrintStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    monitor-exit p0

    return-void

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
