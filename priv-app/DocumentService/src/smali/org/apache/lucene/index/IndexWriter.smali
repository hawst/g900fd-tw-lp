.class public Lorg/apache/lucene/index/IndexWriter;
.super Ljava/lang/Object;
.source "IndexWriter.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Lorg/apache/lucene/util/TwoPhaseCommit;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/IndexWriter$FlushControl;,
        Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;,
        Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;,
        Lorg/apache/lucene/index/IndexWriter$ReaderPool;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT_MAX_BUFFERED_DELETE_TERMS:I = -0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DEFAULT_MAX_BUFFERED_DOCS:I = -0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DEFAULT_MAX_FIELD_LENGTH:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DEFAULT_RAM_BUFFER_SIZE_MB:D = 16.0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DEFAULT_TERM_INDEX_INTERVAL:I = 0x80
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DISABLE_AUTO_FLUSH:I = -0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MAX_TERM_LENGTH:I = 0x3fff

.field private static final MERGE_READ_BUFFER_SIZE:I = 0x1000

.field private static final MESSAGE_ID:Ljava/util/concurrent/atomic/AtomicInteger;

.field public static final WRITE_LOCK_NAME:Ljava/lang/String; = "write.lock"

.field public static WRITE_LOCK_TIMEOUT:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static defaultInfoStream:Ljava/io/PrintStream;


# instance fields
.field private final analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field anyNonBulkMerges:Z

.field final bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

.field private volatile changeCount:J

.field private volatile closed:Z

.field private volatile closing:Z

.field private final commitLock:Ljava/lang/Object;

.field private final config:Lorg/apache/lucene/index/IndexWriterConfig;

.field private deleter:Lorg/apache/lucene/index/IndexFileDeleter;

.field private final directory:Lorg/apache/lucene/store/Directory;

.field private docWriter:Lorg/apache/lucene/index/DocumentsWriter;

.field private filesToCommit:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

.field private final flushCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final flushDeletesCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field private volatile hitOOM:Z

.field private infoStream:Ljava/io/PrintStream;

.field private keepFullyDeletedSegments:Z

.field private lastCommitChangeCount:J

.field private maxFieldLength:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private mergeExceptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/MergePolicy$OneMerge;",
            ">;"
        }
    .end annotation
.end field

.field private mergeGen:J

.field private mergeMaxNumSegments:I

.field private mergePolicy:Lorg/apache/lucene/index/MergePolicy;

.field private mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

.field private mergingSegments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private messageID:I

.field private payloadProcessorProvider:Lorg/apache/lucene/index/PayloadProcessorProvider;

.field volatile pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

.field volatile pendingCommitChangeCount:J

.field private pendingMerges:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/lucene/index/MergePolicy$OneMerge;",
            ">;"
        }
    .end annotation
.end field

.field private volatile poolReaders:Z

.field final readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

.field private rollbackSegments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private runningMerges:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/MergePolicy$OneMerge;",
            ">;"
        }
    .end annotation
.end field

.field final segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

.field private segmentsToMerge:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private similarity:Lorg/apache/lucene/search/Similarity;

.field private stopMerges:Z

.field private writeLock:Lorg/apache/lucene/store/Lock;

.field private writeLockTimeout:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 178
    const-class v0, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    .line 186
    sget-wide v0, Lorg/apache/lucene/index/IndexWriterConfig;->WRITE_LOCK_TIMEOUT:J

    sput-wide v0, Lorg/apache/lucene/index/IndexWriter;->WRITE_LOCK_TIMEOUT:J

    .line 232
    sget-object v0, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;->UNLIMITED:Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;->getLimit()I

    move-result v0

    sput v0, Lorg/apache/lucene/index/IndexWriter;->DEFAULT_MAX_FIELD_LENGTH:I

    .line 258
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/IndexWriter;->MESSAGE_ID:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void

    .line 178
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;)V
    .locals 2
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "a"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "deletionPolicy"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .param p4, "mfl"    # Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Lorg/apache/lucene/store/LockObtainFailedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 979
    new-instance v0, Lorg/apache/lucene/index/IndexWriterConfig;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v1, p2}, Lorg/apache/lucene/index/IndexWriterConfig;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V

    invoke-virtual {v0, p3}, Lorg/apache/lucene/index/IndexWriterConfig;->setIndexDeletionPolicy(Lorg/apache/lucene/index/IndexDeletionPolicy;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    .line 980
    invoke-virtual {p4}, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;->getLimit()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->setMaxFieldLength(I)V

    .line 981
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;Lorg/apache/lucene/index/IndexCommit;)V
    .locals 2
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "a"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "deletionPolicy"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .param p4, "mfl"    # Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;
    .param p5, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Lorg/apache/lucene/store/LockObtainFailedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1052
    new-instance v0, Lorg/apache/lucene/index/IndexWriterConfig;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v1, p2}, Lorg/apache/lucene/index/IndexWriterConfig;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V

    sget-object v1, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriterConfig;->setOpenMode(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    invoke-virtual {v0, p3}, Lorg/apache/lucene/index/IndexWriterConfig;->setIndexDeletionPolicy(Lorg/apache/lucene/index/IndexDeletionPolicy;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    invoke-virtual {v0, p5}, Lorg/apache/lucene/index/IndexWriterConfig;->setIndexCommit(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    .line 1054
    invoke-virtual {p4}, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;->getLimit()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->setMaxFieldLength(I)V

    .line 1055
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;)V
    .locals 2
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "a"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "mfl"    # Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Lorg/apache/lucene/store/LockObtainFailedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 953
    new-instance v0, Lorg/apache/lucene/index/IndexWriterConfig;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v1, p2}, Lorg/apache/lucene/index/IndexWriterConfig;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    .line 954
    invoke-virtual {p3}, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;->getLimit()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->setMaxFieldLength(I)V

    .line 955
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/analysis/Analyzer;ZLorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;)V
    .locals 2
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "a"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "create"    # Z
    .param p4, "deletionPolicy"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .param p5, "mfl"    # Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Lorg/apache/lucene/store/LockObtainFailedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1011
    new-instance v1, Lorg/apache/lucene/index/IndexWriterConfig;

    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-direct {v1, v0, p2}, Lorg/apache/lucene/index/IndexWriterConfig;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V

    if-eqz p3, :cond_0

    sget-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    :goto_0
    invoke-virtual {v1, v0}, Lorg/apache/lucene/index/IndexWriterConfig;->setOpenMode(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    invoke-virtual {v0, p4}, Lorg/apache/lucene/index/IndexWriterConfig;->setIndexDeletionPolicy(Lorg/apache/lucene/index/IndexDeletionPolicy;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    .line 1013
    invoke-virtual {p5}, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;->getLimit()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->setMaxFieldLength(I)V

    .line 1014
    return-void

    .line 1011
    :cond_0
    sget-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/analysis/Analyzer;ZLorg/apache/lucene/index/IndexWriter$MaxFieldLength;)V
    .locals 2
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "a"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "create"    # Z
    .param p4, "mfl"    # Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Lorg/apache/lucene/store/LockObtainFailedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 926
    new-instance v1, Lorg/apache/lucene/index/IndexWriterConfig;

    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-direct {v1, v0, p2}, Lorg/apache/lucene/index/IndexWriterConfig;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V

    if-eqz p3, :cond_0

    sget-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    :goto_0
    invoke-virtual {v1, v0}, Lorg/apache/lucene/index/IndexWriterConfig;->setOpenMode(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    .line 928
    invoke-virtual {p4}, Lorg/apache/lucene/index/IndexWriter$MaxFieldLength;->getLimit()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->setMaxFieldLength(I)V

    .line 929
    return-void

    .line 926
    :cond_0
    sget-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V
    .locals 11
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "conf"    # Lorg/apache/lucene/index/IndexWriterConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Lorg/apache/lucene/store/LockObtainFailedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 259
    sget-object v0, Lorg/apache/lucene/index/IndexWriter;->MESSAGE_ID:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/IndexWriter;->messageID:I

    .line 266
    invoke-static {}, Lorg/apache/lucene/search/Similarity;->getDefault()Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 276
    new-instance v0, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v0}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    .line 283
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    .line 293
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    .line 298
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    .line 299
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    .line 300
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeExceptions:Ljava/util/List;

    .line 304
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->flushCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 305
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->flushDeletesCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 307
    new-instance v0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;-><init>(Lorg/apache/lucene/index/IndexWriter;)V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    .line 1987
    sget v0, Lorg/apache/lucene/index/IndexWriter;->DEFAULT_MAX_FIELD_LENGTH:I

    iput v0, p0, Lorg/apache/lucene/index/IndexWriter;->maxFieldLength:I

    .line 3411
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->commitLock:Ljava/lang/Object;

    .line 4931
    new-instance v0, Lorg/apache/lucene/index/IndexWriter$FlushControl;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/IndexWriter$FlushControl;-><init>(Lorg/apache/lucene/index/IndexWriter;)V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    .line 1083
    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexWriterConfig;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriterConfig;

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    .line 1084
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 1085
    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexWriterConfig;->getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 1086
    sget-object v0, Lorg/apache/lucene/index/IndexWriter;->defaultInfoStream:Ljava/io/PrintStream;

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    .line 1087
    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexWriterConfig;->getWriteLockTimeout()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->writeLockTimeout:J

    .line 1088
    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexWriterConfig;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 1089
    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexWriterConfig;->getMergePolicy()Lorg/apache/lucene/index/MergePolicy;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    .line 1090
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/MergePolicy;->setIndexWriter(Lorg/apache/lucene/index/IndexWriter;)V

    .line 1091
    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexWriterConfig;->getMergeScheduler()Lorg/apache/lucene/index/MergeScheduler;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    .line 1092
    new-instance v0, Lorg/apache/lucene/index/BufferedDeletesStream;

    iget v1, p0, Lorg/apache/lucene/index/IndexWriter;->messageID:I

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/BufferedDeletesStream;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    .line 1093
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/BufferedDeletesStream;->setInfoStream(Ljava/io/PrintStream;)V

    .line 1094
    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexWriterConfig;->getReaderPooling()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->poolReaders:Z

    .line 1096
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    const-string/jumbo v1, "write.lock"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/Directory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    .line 1098
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    iget-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->writeLockTimeout:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/Lock;->obtain(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1099
    new-instance v0, Lorg/apache/lucene/store/LockObtainFailedException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Index locked for write: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/LockObtainFailedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1102
    :cond_0
    const/4 v10, 0x0

    .line 1104
    .local v10, "success":Z
    :try_start_0
    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexWriterConfig;->getOpenMode()Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    move-result-object v8

    .line 1106
    .local v8, "mode":Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    sget-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v8, v0, :cond_6

    .line 1107
    const/4 v7, 0x1

    .line 1121
    .local v7, "create":Z
    :goto_0
    if-eqz v7, :cond_9

    .line 1127
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V

    .line 1128
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->clear()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1135
    :goto_1
    :try_start_2
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    .line 1136
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->changed()V

    .line 1159
    :cond_1
    :goto_2
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->createBackupSegmentInfos(Z)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->rollbackSegments:Ljava/util/List;

    .line 1161
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->getCurrentFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/DocumentsWriter;-><init>(Lorg/apache/lucene/index/IndexWriterConfig;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/BufferedDeletesStream;)V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 1162
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DocumentsWriter;->setInfoStream(Ljava/io/PrintStream;)V

    .line 1163
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget v1, p0, Lorg/apache/lucene/index/IndexWriter;->maxFieldLength:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DocumentsWriter;->setMaxFieldLength(I)V

    .line 1167
    monitor-enter p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1168
    :try_start_3
    new-instance v0, Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexWriterConfig;->getIndexDeletionPolicy()Lorg/apache/lucene/index/IndexDeletionPolicy;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/IndexFileDeleter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/SegmentInfos;Ljava/io/PrintStream;Lorg/apache/lucene/index/IndexWriter;)V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    .line 1172
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1174
    :try_start_4
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-boolean v0, v0, Lorg/apache/lucene/index/IndexFileDeleter;->startingCommitDeleted:Z

    if-eqz v0, :cond_2

    .line 1179
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    .line 1180
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->changed()V

    .line 1183
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_3

    .line 1184
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->messageState()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1187
    :cond_3
    const/4 v10, 0x1

    .line 1190
    if-nez v10, :cond_5

    .line 1191
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_4

    .line 1192
    const-string/jumbo v0, "init: hit exception on init; releasing write lock"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1195
    :cond_4
    :try_start_5
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Lock;->release()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    .line 1199
    :goto_3
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    .line 1202
    :cond_5
    return-void

    .line 1108
    .end local v7    # "create":Z
    :cond_6
    :try_start_6
    sget-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    if-ne v8, v0, :cond_7

    .line 1109
    const/4 v7, 0x0

    .restart local v7    # "create":Z
    goto/16 :goto_0

    .line 1112
    .end local v7    # "create":Z
    :cond_7
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-static {v0}, Lorg/apache/lucene/index/IndexReader;->indexExists(Lorg/apache/lucene/store/Directory;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v7, 0x1

    .restart local v7    # "create":Z
    :goto_4
    goto/16 :goto_0

    .end local v7    # "create":Z
    :cond_8
    const/4 v7, 0x0

    goto :goto_4

    .line 1138
    .restart local v7    # "create":Z
    :cond_9
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V

    .line 1140
    invoke-virtual {p2}, Lorg/apache/lucene/index/IndexWriterConfig;->getIndexCommit()Lorg/apache/lucene/index/IndexCommit;

    move-result-object v6

    .line 1141
    .local v6, "commit":Lorg/apache/lucene/index/IndexCommit;
    if-eqz v6, :cond_1

    .line 1147
    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    if-eq v0, v1, :cond_c

    .line 1148
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "IndexCommit\'s directory doesn\'t match my directory"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1190
    .end local v6    # "commit":Lorg/apache/lucene/index/IndexCommit;
    .end local v7    # "create":Z
    .end local v8    # "mode":Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    :catchall_0
    move-exception v0

    if-nez v10, :cond_b

    .line 1191
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_a

    .line 1192
    const-string/jumbo v1, "init: hit exception on init; releasing write lock"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1195
    :cond_a
    :try_start_7
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v1}, Lorg/apache/lucene/store/Lock;->release()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1

    .line 1199
    :goto_5
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    .line 1190
    :cond_b
    throw v0

    .line 1149
    .restart local v6    # "commit":Lorg/apache/lucene/index/IndexCommit;
    .restart local v7    # "create":Z
    .restart local v8    # "mode":Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    :cond_c
    :try_start_8
    new-instance v9, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v9}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 1150
    .local v9, "oldInfos":Lorg/apache/lucene/index/SegmentInfos;
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    .line 1151
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0, v9}, Lorg/apache/lucene/index/SegmentInfos;->replace(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 1152
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    .line 1153
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->changed()V

    .line 1154
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_1

    .line 1155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "init: loaded commit \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_2

    .line 1172
    .end local v6    # "commit":Lorg/apache/lucene/index/IndexCommit;
    .end local v9    # "oldInfos":Lorg/apache/lucene/index/SegmentInfos;
    :catchall_1
    move-exception v0

    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1196
    :catch_0
    move-exception v0

    goto/16 :goto_3

    .end local v7    # "create":Z
    .end local v8    # "mode":Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    :catch_1
    move-exception v1

    goto :goto_5

    .line 1129
    .restart local v7    # "create":Z
    .restart local v8    # "mode":Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    :catch_2
    move-exception v0

    goto/16 :goto_1
.end method

.method private declared-synchronized _mergeInit(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 18
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4015
    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    const-string/jumbo v2, "startMergeInit"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 4017
    :cond_0
    :try_start_1
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->registerDone:Z

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 4018
    :cond_1
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    move-object/from16 v0, p1

    iget v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    move-object/from16 v0, p1

    iget v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    if-gtz v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 4020
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v2, :cond_3

    .line 4021
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "this writer hit an OutOfMemoryError; cannot merge"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 4027
    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_5

    .line 4103
    :cond_4
    :goto_0
    monitor-exit p0

    return-void

    .line 4031
    :cond_5
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isAborted()Z

    move-result v2

    if-nez v2, :cond_4

    .line 4034
    const/4 v9, 0x0

    .line 4035
    .local v9, "hasVectors":Z
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/index/SegmentInfo;

    .line 4036
    .local v17, "sourceSegment":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/index/SegmentInfo;->getHasVectors()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 4037
    const/4 v9, 0x1

    goto :goto_1

    .line 4044
    .end local v17    # "sourceSegment":Lorg/apache/lucene/index/SegmentInfo;
    :cond_7
    new-instance v2, Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->newSegmentName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v9}, Lorg/apache/lucene/index/SegmentInfo;-><init>(Ljava/lang/String;ILorg/apache/lucene/store/Directory;ZZZZ)V

    move-object/from16 v0, p1

    iput-object v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    .line 4047
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    move-object/from16 v0, p1

    iget-object v4, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/index/BufferedDeletesStream;->applyDeletes(Lorg/apache/lucene/index/IndexWriter$ReaderPool;Ljava/util/List;)Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;

    move-result-object v16

    .line 4049
    .local v16, "result":Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;
    move-object/from16 v0, v16

    iget-boolean v2, v0, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->anyDeletes:Z

    if-eqz v2, :cond_8

    .line 4050
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 4053
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/lucene/index/IndexWriter;->keepFullyDeletedSegments:Z

    if-nez v2, :cond_d

    move-object/from16 v0, v16

    iget-object v2, v0, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->allDeleted:Ljava/util/List;

    if-eqz v2, :cond_d

    .line 4054
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_9

    .line 4055
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "drop 100% deleted segments: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    iget-object v3, v0, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->allDeleted:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4057
    :cond_9
    move-object/from16 v0, v16

    iget-object v2, v0, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->allDeleted:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_a
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/index/SegmentInfo;

    .line 4058
    .local v15, "info":Lorg/apache/lucene/index/SegmentInfo;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, v15}, Lorg/apache/lucene/index/SegmentInfos;->remove(Lorg/apache/lucene/index/SegmentInfo;)V

    .line 4059
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v2, v15}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 4060
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v2, v15}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 4061
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v2, v15}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 4064
    .end local v15    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    if-eqz v2, :cond_c

    .line 4065
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    move-object/from16 v0, v16

    iget-object v3, v0, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->allDeleted:Ljava/util/List;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->drop(Ljava/util/List;)V

    .line 4067
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 4070
    :cond_d
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, v16

    iget-wide v4, v0, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->gen:J

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/index/SegmentInfo;->setBufferedDeletesGen(J)V

    .line 4073
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/BufferedDeletesStream;->prune(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 4075
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 4076
    .local v11, "details":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v2, "mergeMaxNumSegments"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget v4, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v11, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4077
    const-string/jumbo v2, "mergeFactor"

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v11, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4078
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    const-string/jumbo v3, "merge"

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v11}, Lorg/apache/lucene/index/IndexWriter;->setDiagnostics(Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;Ljava/util/Map;)V

    .line 4080
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_e

    .line 4081
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "merge seg="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4084
    :cond_e
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_f

    move-object/from16 v0, p1

    iget-wide v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->estimatedMergeBytes:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 4085
    :cond_f
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_10
    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/index/SegmentInfo;

    .line 4086
    .restart local v15    # "info":Lorg/apache/lucene/index/SegmentInfo;
    iget v2, v15, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-lez v2, :cond_10

    .line 4087
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v10

    .line 4088
    .local v10, "delCount":I
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_11

    iget v2, v15, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-le v10, v2, :cond_11

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 4089
    :cond_11
    int-to-double v2, v10

    iget v4, v15, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    int-to-double v4, v4

    div-double v12, v2, v4

    .line 4090
    .local v12, "delRatio":D
    move-object/from16 v0, p1

    iget-wide v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->estimatedMergeBytes:J

    long-to-double v2, v2

    const/4 v4, 0x1

    invoke-virtual {v15, v4}, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytes(Z)J

    move-result-wide v4

    long-to-double v4, v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v6, v12

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-long v2, v2

    move-object/from16 v0, p1

    iput-wide v2, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->estimatedMergeBytes:J

    goto :goto_3

    .line 4102
    .end local v10    # "delCount":I
    .end local v12    # "delRatio":D
    .end local v15    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lorg/apache/lucene/index/IndexWriter;)Z
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/IndexWriter;

    .prologue
    .line 178
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->poolReaders:Z

    return v0
.end method

.method static synthetic access$100(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/IndexFileDeleter;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/IndexWriter;

    .prologue
    .line 178
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    return-object v0
.end method

.method static synthetic access$200(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/IndexWriter;

    .prologue
    .line 178
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    return-object v0
.end method

.method static synthetic access$300(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/store/Directory;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/IndexWriter;

    .prologue
    .line 178
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method static synthetic access$400(Lorg/apache/lucene/index/IndexWriter;)Ljava/io/PrintStream;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/IndexWriter;

    .prologue
    .line 178
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    return-object v0
.end method

.method static synthetic access$500(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/DocumentsWriter;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/IndexWriter;

    .prologue
    .line 178
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    return-object v0
.end method

.method private closeInternal(Z)V
    .locals 3
    .param p1, "waitForMerges"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1839
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    if-eqz v1, :cond_1

    .line 1840
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "cannot close: prepareCommit was already called with no corresponding call to commit"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1892
    :catch_0
    move-exception v0

    .line 1893
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_1
    const-string/jumbo v1, "closeInternal"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1895
    monitor-enter p0

    .line 1896
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    .line 1897
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1898
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    if-nez v1, :cond_0

    .line 1899
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_0

    .line 1900
    const-string/jumbo v1, "hit exception while closing"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1902
    :cond_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_6

    .line 1904
    .end local v0    # "oom":Ljava/lang/OutOfMemoryError;
    :goto_0
    return-void

    .line 1843
    :cond_1
    :try_start_3
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_2

    .line 1844
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "now flush at close waitForMerges="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1847
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriter;->close()V

    .line 1851
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-nez v1, :cond_3

    .line 1852
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V

    .line 1855
    :cond_3
    if-eqz p1, :cond_4

    .line 1858
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    invoke-virtual {v1, p0}, Lorg/apache/lucene/index/MergeScheduler;->merge(Lorg/apache/lucene/index/IndexWriter;)V

    .line 1860
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v1}, Lorg/apache/lucene/index/MergePolicy;->close()V

    .line 1862
    monitor-enter p0
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1863
    :try_start_4
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->finishMerges(Z)V

    .line 1864
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    .line 1865
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1867
    :try_start_5
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    invoke-virtual {v1}, Lorg/apache/lucene/index/MergeScheduler;->close()V

    .line 1869
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_5

    .line 1870
    const-string/jumbo v1, "now call final commit()"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1872
    :cond_5
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-nez v1, :cond_6

    .line 1873
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->commitInternal(Ljava/util/Map;)V

    .line 1876
    :cond_6
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_7

    .line 1877
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "at close: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1879
    :cond_7
    monitor-enter p0
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1880
    :try_start_6
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->close()V

    .line 1881
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    .line 1882
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexFileDeleter;->close()V

    .line 1883
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 1885
    :try_start_7
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    if-eqz v1, :cond_8

    .line 1886
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v1}, Lorg/apache/lucene/store/Lock;->release()V

    .line 1887
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->writeLock:Lorg/apache/lucene/store/Lock;

    .line 1889
    :cond_8
    monitor-enter p0
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1890
    const/4 v1, 0x1

    :try_start_8
    iput-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    .line 1891
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 1895
    monitor-enter p0

    .line 1896
    const/4 v1, 0x0

    :try_start_9
    iput-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    .line 1897
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1898
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    if-nez v1, :cond_9

    .line 1899
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_9

    .line 1900
    const-string/jumbo v1, "hit exception while closing"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1902
    :cond_9
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    throw v1

    .line 1865
    :catchall_1
    move-exception v1

    :try_start_a
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    throw v1
    :try_end_b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 1895
    :catchall_2
    move-exception v1

    monitor-enter p0

    .line 1896
    const/4 v2, 0x0

    :try_start_c
    iput-boolean v2, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    .line 1897
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1898
    iget-boolean v2, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    if-nez v2, :cond_a

    .line 1899
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_a

    .line 1900
    const-string/jumbo v2, "hit exception while closing"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1902
    :cond_a
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    .line 1895
    throw v1

    .line 1883
    :catchall_3
    move-exception v1

    :try_start_d
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :try_start_e
    throw v1
    :try_end_e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 1891
    :catchall_4
    move-exception v1

    :try_start_f
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    :try_start_10
    throw v1
    :try_end_10
    .catch Ljava/lang/OutOfMemoryError; {:try_start_10 .. :try_end_10} :catch_0
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    .line 1902
    :catchall_5
    move-exception v1

    :try_start_11
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    throw v1

    .restart local v0    # "oom":Ljava/lang/OutOfMemoryError;
    :catchall_6
    move-exception v1

    :try_start_12
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_6

    throw v1
.end method

.method private final declared-synchronized closeMergeReaders(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V
    .locals 9
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .param p2, "suppressExceptions"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4149
    monitor-enter p0

    :try_start_0
    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    .line 4150
    .local v3, "numSegments":I
    const/4 v5, 0x0

    .line 4152
    .local v5, "th":Ljava/lang/Throwable;
    const/4 v0, 0x0

    .line 4153
    .local v0, "anyChanges":Z
    if-nez p2, :cond_3

    const/4 v1, 0x1

    .line 4154
    .local v1, "drop":Z
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_6

    .line 4155
    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    if-eqz v6, :cond_1

    .line 4157
    :try_start_1
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v7, v6, v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/SegmentReader;Z)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    or-int/2addr v0, v6

    .line 4163
    :cond_0
    :goto_2
    :try_start_2
    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v6, v2, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 4166
    :cond_1
    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readerClones:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_5

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readerClones:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v6

    if-eqz v6, :cond_5

    .line 4168
    :try_start_3
    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readerClones:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentReader;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4176
    :cond_2
    :goto_3
    :try_start_4
    sget-boolean v6, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_4

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readerClones:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentReader;->getRefCount()I

    move-result v6

    if-eqz v6, :cond_4

    new-instance v7, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "refCount should be 0 but is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readerClones:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentReader;->getRefCount()I

    move-result v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4149
    .end local v0    # "anyChanges":Z
    .end local v1    # "drop":Z
    .end local v2    # "i":I
    .end local v3    # "numSegments":I
    .end local v5    # "th":Ljava/lang/Throwable;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 4153
    .restart local v0    # "anyChanges":Z
    .restart local v3    # "numSegments":I
    .restart local v5    # "th":Ljava/lang/Throwable;
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 4158
    .restart local v1    # "drop":Z
    .restart local v2    # "i":I
    :catch_0
    move-exception v4

    .line 4159
    .local v4, "t":Ljava/lang/Throwable;
    if-nez v5, :cond_0

    .line 4160
    move-object v5, v4

    goto :goto_2

    .line 4169
    .end local v4    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v4

    .line 4170
    .restart local v4    # "t":Ljava/lang/Throwable;
    if-nez v5, :cond_2

    .line 4171
    move-object v5, v4

    goto :goto_3

    .line 4177
    .end local v4    # "t":Ljava/lang/Throwable;
    :cond_4
    :try_start_5
    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readerClones:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v6, v2, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 4154
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 4181
    :cond_6
    if-eqz p2, :cond_7

    if-eqz v0, :cond_7

    .line 4182
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 4186
    :cond_7
    if-nez p2, :cond_b

    if-eqz v5, :cond_b

    .line 4187
    instance-of v6, v5, Ljava/io/IOException;

    if-eqz v6, :cond_8

    check-cast v5, Ljava/io/IOException;

    .end local v5    # "th":Ljava/lang/Throwable;
    throw v5

    .line 4188
    .restart local v5    # "th":Ljava/lang/Throwable;
    :cond_8
    instance-of v6, v5, Ljava/lang/RuntimeException;

    if-eqz v6, :cond_9

    check-cast v5, Ljava/lang/RuntimeException;

    .end local v5    # "th":Ljava/lang/Throwable;
    throw v5

    .line 4189
    .restart local v5    # "th":Ljava/lang/Throwable;
    :cond_9
    instance-of v6, v5, Ljava/lang/Error;

    if-eqz v6, :cond_a

    check-cast v5, Ljava/lang/Error;

    .end local v5    # "th":Ljava/lang/Throwable;
    throw v5

    .line 4190
    .restart local v5    # "th":Ljava/lang/Throwable;
    :cond_a
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 4192
    :cond_b
    monitor-exit p0

    return-void
.end method

.method private final commitInternal(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3465
    .local p1, "commitUserData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 3466
    const-string/jumbo v0, "commit: start"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3469
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->commitLock:Ljava/lang/Object;

    monitor-enter v1

    .line 3470
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_1

    .line 3471
    const-string/jumbo v0, "commit: enter lock"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3474
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    if-nez v0, :cond_4

    .line 3475
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_2

    .line 3476
    const-string/jumbo v0, "commit: now prepare"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3478
    :cond_2
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->prepareCommit(Ljava/util/Map;)V

    .line 3483
    :cond_3
    :goto_0
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->finishCommit()V

    .line 3484
    monitor-exit v1

    .line 3485
    return-void

    .line 3479
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_3

    .line 3480
    const-string/jumbo v0, "commit: already prepared"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    goto :goto_0

    .line 3484
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized commitMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/index/SegmentReader;)Z
    .locals 6
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .param p2, "mergedReader"    # Lorg/apache/lucene/index/SegmentReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3773
    monitor-enter p0

    :try_start_0
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    const-string/jumbo v4, "startCommitMerge"

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 3775
    :cond_0
    :try_start_1
    iget-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v4, :cond_1

    .line 3776
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "this writer hit an OutOfMemoryError; cannot complete merge"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3779
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_2

    .line 3780
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "commitMerge: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p1, v5}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " index="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3782
    :cond_2
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    iget-boolean v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->registerDone:Z

    if-nez v4, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 3790
    :cond_3
    invoke-virtual {p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isAborted()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 3791
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_4

    .line 3792
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "commitMerge: skipping merge "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p1, v4}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ": it was aborted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3844
    :cond_4
    :goto_0
    monitor-exit p0

    return v2

    .line 3796
    :cond_5
    :try_start_2
    iget-object v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget v4, v4, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-lez v4, :cond_6

    .line 3797
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->commitMergedDeletes(Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/index/SegmentReader;)V

    .line 3805
    :cond_6
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_7

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v5, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/SegmentInfos;->contains(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v4

    if-eqz v4, :cond_7

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 3807
    :cond_7
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentReader;->numDocs()I

    move-result v4

    if-nez v4, :cond_9

    move v0, v3

    .line 3809
    .local v0, "allDeleted":Z
    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_8

    if-eqz v0, :cond_8

    .line 3810
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "merged segment "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " is 100% deleted"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->keepFullyDeletedSegments:Z

    if-eqz v4, :cond_a

    const-string/jumbo v4, ""

    :goto_2
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3813
    :cond_8
    if-eqz v0, :cond_b

    iget-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->keepFullyDeletedSegments:Z

    if-nez v4, :cond_b

    move v1, v3

    .line 3814
    .local v1, "dropSegment":Z
    :goto_3
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, p1, v1}, Lorg/apache/lucene/index/SegmentInfos;->applyMergeChanges(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V

    .line 3816
    if-eqz v1, :cond_c

    .line 3817
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    iget-object v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->drop(Lorg/apache/lucene/index/SegmentInfo;)V

    .line 3818
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 3819
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_c

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/SegmentInfos;->contains(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v2

    if-eqz v2, :cond_c

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .end local v0    # "allDeleted":Z
    .end local v1    # "dropSegment":Z
    :cond_9
    move v0, v2

    .line 3807
    goto :goto_1

    .line 3810
    .restart local v0    # "allDeleted":Z
    :cond_a
    const-string/jumbo v4, "; skipping insert"

    goto :goto_2

    :cond_b
    move v1, v2

    .line 3813
    goto :goto_3

    .line 3822
    .restart local v1    # "dropSegment":Z
    :cond_c
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_d

    .line 3823
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "after commit: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3826
    :cond_d
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lorg/apache/lucene/index/IndexWriter;->closeMergeReaders(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V

    .line 3830
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 3835
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    iget-object v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->clear(Ljava/util/List;)V

    .line 3837
    iget v2, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_e

    if-nez v1, :cond_e

    .line 3839
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    iget-object v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 3840
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    iget-object v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_e
    move v2, v3

    .line 3844
    goto/16 :goto_0
.end method

.method private declared-synchronized commitMergedDeletes(Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/index/SegmentReader;)V
    .locals 16
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .param p2, "mergedReader"    # Lorg/apache/lucene/index/SegmentReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3692
    monitor-enter p0

    :try_start_0
    sget-boolean v13, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v13, :cond_0

    const-string/jumbo v13, "startCommitMergeDeletes"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_0

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v13

    monitor-exit p0

    throw v13

    .line 3694
    :cond_0
    :try_start_1
    move-object/from16 v0, p1

    iget-object v12, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    .line 3696
    .local v12, "sourceSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v13, :cond_1

    .line 3697
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "commitMergeDeletes "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3701
    :cond_1
    const/4 v5, 0x0

    .line 3702
    .local v5, "docUpto":I
    const/4 v3, 0x0

    .line 3703
    .local v3, "delCount":I
    const-wide v10, 0x7fffffffffffffffL

    .line 3705
    .local v10, "minGen":J
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v13

    if-ge v6, v13, :cond_b

    .line 3706
    invoke-interface {v12, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/SegmentInfo;

    .line 3707
    .local v7, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentInfo;->getBufferedDeletesGen()J

    move-result-wide v14

    invoke-static {v14, v15, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    .line 3708
    iget v4, v7, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    .line 3709
    .local v4, "docCount":I
    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readerClones:Ljava/util/List;

    invoke-interface {v13, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/index/SegmentReader;

    .line 3710
    .local v9, "previousReader":Lorg/apache/lucene/index/SegmentReader;
    if-nez v9, :cond_3

    .line 3705
    :cond_2
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 3714
    :cond_3
    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    invoke-interface {v13, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentReader;

    .line 3715
    .local v2, "currentReader":Lorg/apache/lucene/index/SegmentReader;
    invoke-virtual {v9}, Lorg/apache/lucene/index/SegmentReader;->hasDeletions()Z

    move-result v13

    if-eqz v13, :cond_8

    .line 3724
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentReader;->numDeletedDocs()I

    move-result v13

    invoke-virtual {v9}, Lorg/apache/lucene/index/SegmentReader;->numDeletedDocs()I

    move-result v14

    if-le v13, v14, :cond_7

    .line 3728
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_2
    if-ge v8, v4, :cond_2

    .line 3729
    invoke-virtual {v9, v8}, Lorg/apache/lucene/index/SegmentReader;->isDeleted(I)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 3730
    sget-boolean v13, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v13, :cond_6

    invoke-virtual {v2, v8}, Lorg/apache/lucene/index/SegmentReader;->isDeleted(I)Z

    move-result v13

    if-nez v13, :cond_6

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 3732
    :cond_4
    invoke-virtual {v2, v8}, Lorg/apache/lucene/index/SegmentReader;->isDeleted(I)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 3733
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/SegmentReader;->doDelete(I)V

    .line 3734
    add-int/lit8 v3, v3, 0x1

    .line 3736
    :cond_5
    add-int/lit8 v5, v5, 0x1

    .line 3728
    :cond_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 3740
    .end local v8    # "j":I
    :cond_7
    invoke-virtual {v9}, Lorg/apache/lucene/index/SegmentReader;->numDeletedDocs()I

    move-result v13

    sub-int v13, v4, v13

    add-int/2addr v5, v13

    goto :goto_1

    .line 3742
    :cond_8
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentReader;->hasDeletions()Z

    move-result v13

    if-eqz v13, :cond_a

    .line 3745
    const/4 v8, 0x0

    .restart local v8    # "j":I
    :goto_3
    if-ge v8, v4, :cond_2

    .line 3746
    invoke-virtual {v2, v8}, Lorg/apache/lucene/index/SegmentReader;->isDeleted(I)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 3747
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/SegmentReader;->doDelete(I)V

    .line 3748
    add-int/lit8 v3, v3, 0x1

    .line 3750
    :cond_9
    add-int/lit8 v5, v5, 0x1

    .line 3745
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 3754
    .end local v8    # "j":I
    :cond_a
    iget v13, v7, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    add-int/2addr v5, v13

    goto :goto_1

    .line 3757
    .end local v2    # "currentReader":Lorg/apache/lucene/index/SegmentReader;
    .end local v4    # "docCount":I
    .end local v7    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v9    # "previousReader":Lorg/apache/lucene/index/SegmentReader;
    :cond_b
    sget-boolean v13, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v13, :cond_c

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentReader;->numDeletedDocs()I

    move-result v13

    if-eq v13, v3, :cond_c

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 3759
    :cond_c
    if-lez v3, :cond_d

    const/4 v13, 0x1

    :goto_4
    move-object/from16 v0, p2

    iput-boolean v13, v0, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    .line 3766
    sget-boolean v13, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v13, :cond_e

    move-object/from16 v0, p2

    iget-boolean v13, v0, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    if-eqz v13, :cond_e

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentReader;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v13

    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getBufferedDeletesGen()J

    move-result-wide v14

    cmp-long v13, v10, v14

    if-gtz v13, :cond_e

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 3759
    :cond_d
    const/4 v13, 0x0

    goto :goto_4

    .line 3768
    :cond_e
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentReader;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v13

    invoke-virtual {v13, v10, v11}, Lorg/apache/lucene/index/SegmentInfo;->setBufferedDeletesGen(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3769
    monitor-exit p0

    return-void
.end method

.method private copySegmentAsIs(Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;Ljava/util/Map;Ljava/util/Set;)V
    .locals 8
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "segName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfo;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3256
    .local p3, "dsNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p4, "dsFilesCopied":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreSegment()Ljava/lang/String;

    move-result-object v0

    .line 3258
    .local v0, "dsName":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 3259
    invoke-interface {p3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 3260
    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 3270
    .local v3, "newDsName":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 3272
    .local v1, "file":Ljava/lang/String;
    invoke-static {v1}, Lorg/apache/lucene/index/IndexFileNames;->isDocStoreFile(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 3273
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 3274
    .local v4, "newFileName":Ljava/lang/String;
    invoke-interface {p4, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3277
    invoke-interface {p4, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3282
    :goto_2
    sget-boolean v5, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v5, v4}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "file \""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\" already exists"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 3262
    .end local v1    # "file":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "newDsName":Ljava/lang/String;
    .end local v4    # "newFileName":Ljava/lang/String;
    :cond_1
    invoke-interface {p3, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3263
    move-object v3, p2

    .restart local v3    # "newDsName":Ljava/lang/String;
    goto :goto_0

    .line 3266
    .end local v3    # "newDsName":Ljava/lang/String;
    :cond_2
    move-object v3, p2

    .restart local v3    # "newDsName":Ljava/lang/String;
    goto :goto_0

    .line 3279
    .restart local v1    # "file":Ljava/lang/String;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "newFileName":Ljava/lang/String;
    goto :goto_2

    .line 3283
    :cond_4
    iget-object v5, p1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v5, v6, v1, v4}, Lorg/apache/lucene/store/Directory;->copy(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3286
    .end local v1    # "file":Ljava/lang/String;
    .end local v4    # "newFileName":Ljava/lang/String;
    :cond_5
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreOffset()I

    move-result v5

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreIsCompoundFile()Z

    move-result v6

    invoke-virtual {p1, v5, v3, v6}, Lorg/apache/lucene/index/SegmentInfo;->setDocStore(ILjava/lang/String;Z)V

    .line 3287
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    iput-object v5, p1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    .line 3288
    iput-object p2, p1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 3289
    return-void
.end method

.method private declared-synchronized doFlush(Z)Z
    .locals 12
    .param p1, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3555
    monitor-enter p0

    :try_start_0
    iget-boolean v6, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v6, :cond_0

    .line 3556
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string/jumbo v7, "this writer hit an OutOfMemoryError; cannot flush"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3555
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 3559
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->doBeforeFlush()V

    .line 3561
    sget-boolean v6, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    const-string/jumbo v6, "startDoFlush"

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 3569
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    const-string/jumbo v7, "explicit flush"

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->setFlushPendingNoWait(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3571
    const/4 v5, 0x0

    .line 3575
    .local v5, "success":Z
    :try_start_2
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v6, :cond_2

    .line 3576
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "  start flush: applyAllDeletes="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3577
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "  index before flush "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3580
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    iget-object v9, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v6, p0, v7, v8, v9}, Lorg/apache/lucene/index/DocumentsWriter;->flush(Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/IndexFileDeleter;Lorg/apache/lucene/index/MergePolicy;Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v2

    .line 3581
    .local v2, "newSegment":Lorg/apache/lucene/index/SegmentInfo;
    if-eqz v2, :cond_3

    .line 3582
    const-string/jumbo v6, "flush"

    invoke-direct {p0, v2, v6}, Lorg/apache/lucene/index/IndexWriter;->setDiagnostics(Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;)V

    .line 3583
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v6, v2}, Lorg/apache/lucene/index/SegmentInfos;->add(Lorg/apache/lucene/index/SegmentInfo;)V

    .line 3584
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 3587
    :cond_3
    if-nez p1, :cond_5

    .line 3592
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->getFlushDeletes()Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v6

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    cmpl-double v6, v6, v8

    if-eqz v6, :cond_5

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v6}, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed()J

    move-result-wide v6

    long-to-double v6, v6

    const-wide/high16 v8, 0x4130000000000000L    # 1048576.0

    iget-object v10, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v10}, Lorg/apache/lucene/index/IndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v10

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v8, v10

    cmpl-double v6, v6, v8

    if-lez v6, :cond_5

    .line 3595
    :cond_4
    const/4 p1, 0x1

    .line 3596
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v6, :cond_5

    .line 3597
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "force apply deletes bytesUsed="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v7}, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " vs ramBuffer="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-wide/high16 v8, 0x4130000000000000L    # 1048576.0

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3602
    :cond_5
    if-eqz p1, :cond_10

    .line 3603
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v6, :cond_6

    .line 3604
    const-string/jumbo v6, "apply all deletes during flush"

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3607
    :cond_6
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->flushDeletesCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 3608
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v8}, Lorg/apache/lucene/index/SegmentInfos;->asList()Ljava/util/List;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/lucene/index/BufferedDeletesStream;->applyDeletes(Lorg/apache/lucene/index/IndexWriter$ReaderPool;Ljava/util/List;)Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;

    move-result-object v4

    .line 3610
    .local v4, "result":Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;
    iget-boolean v6, v4, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->anyDeletes:Z

    if-eqz v6, :cond_7

    .line 3611
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 3613
    :cond_7
    iget-boolean v6, p0, Lorg/apache/lucene/index/IndexWriter;->keepFullyDeletedSegments:Z

    if-nez v6, :cond_c

    iget-object v6, v4, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->allDeleted:Ljava/util/List;

    if-eqz v6, :cond_c

    .line 3614
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v6, :cond_8

    .line 3615
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "drop 100% deleted segments: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->allDeleted:Ljava/util/List;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3617
    :cond_8
    iget-object v6, v4, Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;->allDeleted:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_9
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfo;

    .line 3622
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfo;
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 3623
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v6, v1}, Lorg/apache/lucene/index/SegmentInfos;->remove(Lorg/apache/lucene/index/SegmentInfo;)V

    .line 3624
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    if-eqz v6, :cond_9

    .line 3625
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-virtual {v6, v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->drop(Lorg/apache/lucene/index/SegmentInfo;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 3647
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v2    # "newSegment":Lorg/apache/lucene/index/SegmentInfo;
    .end local v4    # "result":Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;
    :catch_0
    move-exception v3

    .line 3648
    .local v3, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_3
    const-string/jumbo v6, "doFlush"

    invoke-direct {p0, v3, v6}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3650
    const/4 v6, 0x0

    .line 3652
    :try_start_4
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->clearFlushPending()V

    .line 3653
    if-nez v5, :cond_a

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v7, :cond_a

    .line 3654
    const-string/jumbo v7, "hit exception during flush"

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3650
    .end local v3    # "oom":Ljava/lang/OutOfMemoryError;
    :cond_a
    :goto_1
    monitor-exit p0

    return v6

    .line 3629
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "newSegment":Lorg/apache/lucene/index/SegmentInfo;
    .restart local v4    # "result":Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;
    :cond_b
    :try_start_5
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 3631
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_c
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/BufferedDeletesStream;->prune(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 3633
    sget-boolean v6, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_e

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v6}, Lorg/apache/lucene/index/BufferedDeletesStream;->any()Z

    move-result v6

    if-eqz v6, :cond_e

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 3652
    .end local v2    # "newSegment":Lorg/apache/lucene/index/SegmentInfo;
    .end local v4    # "result":Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;
    :catchall_1
    move-exception v6

    :try_start_6
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->clearFlushPending()V

    .line 3653
    if-nez v5, :cond_d

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v7, :cond_d

    .line 3654
    const-string/jumbo v7, "hit exception during flush"

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3652
    :cond_d
    throw v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 3634
    .restart local v2    # "newSegment":Lorg/apache/lucene/index/SegmentInfo;
    .restart local v4    # "result":Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;
    :cond_e
    :try_start_7
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->clearDeletes()V

    .line 3640
    .end local v4    # "result":Lorg/apache/lucene/index/BufferedDeletesStream$ApplyDeletesResult;
    :cond_f
    :goto_2
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->doAfterFlush()V

    .line 3641
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->flushCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 3643
    const/4 v5, 0x1

    .line 3645
    if-eqz v2, :cond_11

    const/4 v6, 0x1

    .line 3652
    :goto_3
    :try_start_8
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->clearFlushPending()V

    .line 3653
    if-nez v5, :cond_a

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v7, :cond_a

    .line 3654
    const-string/jumbo v7, "hit exception during flush"

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    .line 3635
    :cond_10
    :try_start_9
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v6, :cond_f

    .line 3636
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "don\'t apply deletes now delTermCount="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v7}, Lorg/apache/lucene/index/BufferedDeletesStream;->numTerms()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " bytesUsed="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v7}, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/OutOfMemoryError; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_2

    .line 3645
    :cond_11
    const/4 v6, 0x0

    goto :goto_3
.end method

.method private declared-synchronized doWait()V
    .locals 4

    .prologue
    .line 4462
    monitor-enter p0

    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4466
    monitor-exit p0

    return-void

    .line 4463
    :catch_0
    move-exception v0

    .line 4464
    .local v0, "ie":Ljava/lang/InterruptedException;
    :try_start_1
    new-instance v1, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4462
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private ensureValidMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 5
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3674
    iget-object v2, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfo;

    .line 3675
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfo;
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/index/SegmentInfos;->contains(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3676
    new-instance v2, Lorg/apache/lucene/index/MergePolicy$MergeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "MergePolicy selected a segment ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ") that is not in the current index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/index/MergePolicy$MergeException;-><init>(Ljava/lang/String;Lorg/apache/lucene/store/Directory;)V

    throw v2

    .line 3679
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_1
    return-void
.end method

.method private filesExist(Lorg/apache/lucene/index/SegmentInfos;)Z
    .locals 6
    .param p1, "toSync"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4483
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v1

    .line 4484
    .local v1, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 4485
    .local v0, "fileName":Ljava/lang/String;
    sget-boolean v3, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " does not exist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 4491
    :cond_1
    sget-boolean v3, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/IndexFileDeleter;->exists(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "IndexFileDeleter doesn\'t know about file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 4493
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_2
    const/4 v3, 0x1

    return v3
.end method

.method private final declared-synchronized finishCommit()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3489
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_4

    .line 3491
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 3492
    const-string/jumbo v0, "commit: pendingCommit != null"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3494
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->finishCommit(Lorg/apache/lucene/store/Directory;)V

    .line 3495
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_1

    .line 3496
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "commit: wrote segments file \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3498
    :cond_1
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommitChangeCount:J

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->lastCommitChangeCount:J

    .line 3499
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->updateGeneration(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 3500
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfos;->getUserData()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->setUserData(Ljava/util/Map;)V

    .line 3501
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->createBackupSegmentInfos(Z)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->rollbackSegments:Ljava/util/List;

    .line 3502
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3505
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 3506
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 3507
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    .line 3508
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 3515
    :cond_2
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_3

    .line 3516
    const-string/jumbo v0, "commit: done"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3518
    :cond_3
    monitor-exit p0

    return-void

    .line 3505
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 3506
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 3507
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    .line 3508
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 3505
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3489
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 3511
    :cond_4
    :try_start_4
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_2

    .line 3512
    const-string/jumbo v0, "commit: pendingCommit == null; skip"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0
.end method

.method private declared-synchronized finishMerges(Z)V
    .locals 4
    .param p1, "waitForMerges"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2953
    monitor-enter p0

    if-nez p1, :cond_8

    .line 2955
    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    .line 2958
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 2959
    .local v1, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_0

    .line 2960
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "now abort pending merge "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2961
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->abort()V

    .line 2962
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2953
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 2964
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    .line 2966
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 2967
    .restart local v1    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_2

    .line 2968
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "now abort running merge "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2969
    :cond_2
    invoke-virtual {v1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->abort()V

    goto :goto_1

    .line 2977
    .end local v1    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_3
    :goto_2
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 2978
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_4

    .line 2979
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "now wait for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " running merge to abort"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2980
    :cond_4
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->doWait()V

    goto :goto_2

    .line 2983
    :cond_5
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    .line 2984
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 2986
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    if-eqz v2, :cond_6

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 2988
    :cond_6
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_7

    .line 2989
    const-string/jumbo v2, "all running merges have aborted"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2999
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_7
    :goto_3
    monitor-exit p0

    return-void

    .line 2997
    :cond_8
    :try_start_2
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->waitForMerges()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method private getCurrentFieldInfos()Lorg/apache/lucene/index/FieldInfos;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1222
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v6

    if-lez v6, :cond_3

    .line 1223
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfos;->getFormat()I

    move-result v6

    const/16 v7, -0x9

    if-le v6, v7, :cond_1

    .line 1226
    new-instance v1, Lorg/apache/lucene/index/FieldInfos;

    invoke-direct {v1}, Lorg/apache/lucene/index/FieldInfos;-><init>()V

    .line 1227
    .local v1, "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SegmentInfo;

    .line 1228
    .local v4, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-direct {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->getFieldInfos(Lorg/apache/lucene/index/SegmentInfo;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v5

    .line 1229
    .local v5, "segFieldInfos":Lorg/apache/lucene/index/FieldInfos;
    invoke-virtual {v5}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v0

    .line 1230
    .local v0, "fieldCount":I
    const/4 v2, 0x0

    .local v2, "fieldNumber":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 1231
    invoke-virtual {v5, v2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v6

    invoke-virtual {v1, v6}, Lorg/apache/lucene/index/FieldInfos;->add(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/FieldInfo;

    .line 1230
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1237
    .end local v0    # "fieldCount":I
    .end local v1    # "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    .end local v2    # "fieldNumber":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v5    # "segFieldInfos":Lorg/apache/lucene/index/FieldInfos;
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v6

    invoke-direct {p0, v6}, Lorg/apache/lucene/index/IndexWriter;->getFieldInfos(Lorg/apache/lucene/index/SegmentInfo;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v1

    .line 1242
    .restart local v1    # "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    :cond_2
    :goto_1
    return-object v1

    .line 1240
    .end local v1    # "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    :cond_3
    new-instance v1, Lorg/apache/lucene/index/FieldInfos;

    invoke-direct {v1}, Lorg/apache/lucene/index/FieldInfos;-><init>()V

    .restart local v1    # "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    goto :goto_1
.end method

.method public static getDefaultInfoStream()Ljava/io/PrintStream;
    .locals 1

    .prologue
    .line 1649
    sget-object v0, Lorg/apache/lucene/index/IndexWriter;->defaultInfoStream:Ljava/io/PrintStream;

    return-object v0
.end method

.method public static getDefaultWriteLockTimeout()J
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1730
    invoke-static {}, Lorg/apache/lucene/index/IndexWriterConfig;->getDefaultWriteLockTimeout()J

    move-result-wide v0

    return-wide v0
.end method

.method private getFieldInfos(Lorg/apache/lucene/index/SegmentInfo;)Lorg/apache/lucene/index/FieldInfos;
    .locals 5
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1205
    const/4 v0, 0x0

    .line 1207
    .local v0, "cfsDir":Lorg/apache/lucene/store/Directory;
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1208
    new-instance v1, Lorg/apache/lucene/index/CompoundFileReader;

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v3, p1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string/jumbo v4, "cfs"

    invoke-static {v3, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/index/CompoundFileReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    .end local v0    # "cfsDir":Lorg/apache/lucene/store/Directory;
    .local v1, "cfsDir":Lorg/apache/lucene/store/Directory;
    move-object v0, v1

    .line 1212
    .end local v1    # "cfsDir":Lorg/apache/lucene/store/Directory;
    .restart local v0    # "cfsDir":Lorg/apache/lucene/store/Directory;
    :goto_0
    new-instance v2, Lorg/apache/lucene/index/FieldInfos;

    iget-object v3, p1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string/jumbo v4, "fnm"

    invoke-static {v3, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lorg/apache/lucene/index/FieldInfos;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1214
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 1215
    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    .line 1212
    :cond_0
    return-object v2

    .line 1210
    :cond_1
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1214
    :catchall_0
    move-exception v2

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    .line 1215
    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    .line 1214
    :cond_2
    throw v2
.end method

.method private getLogMergePolicy()Lorg/apache/lucene/index/LogMergePolicy;
    .locals 2

    .prologue
    .line 789
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    instance-of v0, v0, Lorg/apache/lucene/index/LogMergePolicy;

    if-eqz v0, :cond_0

    .line 790
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    check-cast v0, Lorg/apache/lucene/index/LogMergePolicy;

    return-object v0

    .line 792
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "this method can only be called when the merge policy is the default LogMergePolicy"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final handleMergeException(Ljava/lang/Throwable;Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;
    .param p2, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3849
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 3850
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "handleMergeException: merge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p2, v1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " exc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3856
    :cond_0
    invoke-virtual {p2, p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->setException(Ljava/lang/Throwable;)V

    .line 3857
    invoke-virtual {p0, p2}, Lorg/apache/lucene/index/IndexWriter;->addMergeException(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 3859
    instance-of v0, p1, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;

    if-eqz v0, :cond_1

    .line 3866
    iget-boolean v0, p2, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isExternal:Z

    if-eqz v0, :cond_5

    .line 3867
    check-cast p1, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;

    .end local p1    # "t":Ljava/lang/Throwable;
    throw p1

    .line 3868
    .restart local p1    # "t":Ljava/lang/Throwable;
    :cond_1
    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_2

    .line 3869
    check-cast p1, Ljava/io/IOException;

    .end local p1    # "t":Ljava/lang/Throwable;
    throw p1

    .line 3870
    .restart local p1    # "t":Ljava/lang/Throwable;
    :cond_2
    instance-of v0, p1, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_3

    .line 3871
    check-cast p1, Ljava/lang/RuntimeException;

    .end local p1    # "t":Ljava/lang/Throwable;
    throw p1

    .line 3872
    .restart local p1    # "t":Ljava/lang/Throwable;
    :cond_3
    instance-of v0, p1, Ljava/lang/Error;

    if-eqz v0, :cond_4

    .line 3873
    check-cast p1, Ljava/lang/Error;

    .end local p1    # "t":Ljava/lang/Throwable;
    throw p1

    .line 3876
    .restart local p1    # "t":Ljava/lang/Throwable;
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 3877
    :cond_5
    return-void
.end method

.method private handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V
    .locals 2
    .param p1, "oom"    # Ljava/lang/OutOfMemoryError;
    .param p2, "location"    # Ljava/lang/String;

    .prologue
    .line 4714
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 4715
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "hit OutOfMemoryError inside "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4717
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    .line 4718
    throw p1
.end method

.method public static isLocked(Lorg/apache/lucene/store/Directory;)Z
    .locals 1
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4606
    const-string/jumbo v0, "write.lock"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/Directory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/store/Lock;->isLocked()Z

    move-result v0

    return v0
.end method

.method private declared-synchronized maxNumSegmentsMergesPending()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, -0x1

    .line 2579
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 2580
    .local v1, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iget v3, v1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v3, v4, :cond_0

    .line 2589
    .end local v1    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :goto_0
    monitor-exit p0

    return v2

    .line 2584
    :cond_1
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 2585
    .restart local v1    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iget v3, v1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v3, v4, :cond_2

    goto :goto_0

    .line 2589
    .end local v1    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 2579
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private final maybeMerge(I)V
    .locals 1
    .param p1, "maxNumSegments"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2738
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 2739
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->updatePendingMerges(I)V

    .line 2740
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/MergeScheduler;->merge(Lorg/apache/lucene/index/IndexWriter;)V

    .line 2741
    return-void
.end method

.method private final mergeMiddle(Lorg/apache/lucene/index/MergePolicy$OneMerge;)I
    .locals 32
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4200
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->checkAborted(Lorg/apache/lucene/store/Directory;)V

    .line 4202
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v7, v5, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 4204
    .local v7, "mergedName":Ljava/lang/String;
    const/16 v16, 0x0

    .line 4206
    .local v16, "mergedDocCount":I
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    move-object/from16 v22, v0

    .line 4208
    .local v22, "sourceSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    new-instance v4, Lorg/apache/lucene/index/SegmentMerger;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexWriterConfig;->getTermIndexInterval()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/lucene/index/IndexWriter;->payloadProcessorProvider:Lorg/apache/lucene/index/PayloadProcessorProvider;

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v8}, Lorg/apache/lucene/index/DocumentsWriter;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/lucene/index/FieldInfos;->clone()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v8, p1

    invoke-direct/range {v4 .. v10}, Lorg/apache/lucene/index/SegmentMerger;-><init>(Lorg/apache/lucene/store/Directory;ILjava/lang/String;Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/index/PayloadProcessorProvider;Lorg/apache/lucene/index/FieldInfos;)V

    .line 4212
    .local v4, "merger":Lorg/apache/lucene/index/SegmentMerger;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v5, :cond_0

    .line 4213
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "merging "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " mergeVectors="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfo;->getHasVectors()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4216
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p1

    iput-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    .line 4217
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p1

    iput-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readerClones:Ljava/util/List;

    .line 4221
    const/16 v23, 0x0

    .line 4223
    .local v23, "success":Z
    const/16 v26, 0x0

    .line 4224
    .local v26, "totDocCount":I
    const/16 v21, 0x0

    .line 4225
    .local v21, "segUpto":I
    :goto_0
    :try_start_0
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v5

    move/from16 v0, v21

    if-ge v0, v5, :cond_2

    .line 4227
    move-object/from16 v0, v22

    move/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/lucene/index/SegmentInfo;

    .line 4231
    .local v13, "info":Lorg/apache/lucene/index/SegmentInfo;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    const/4 v6, 0x1

    const/16 v8, 0x1000

    const/4 v9, -0x1

    invoke-virtual {v5, v13, v6, v8, v9}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfo;ZII)Lorg/apache/lucene/index/SegmentReader;

    move-result-object v20

    .line 4234
    .local v20, "reader":Lorg/apache/lucene/index/SegmentReader;
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    move-object/from16 v0, v20

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4239
    const/4 v5, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/SegmentReader;->clone(Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/index/SegmentReader;

    .line 4240
    .local v11, "clone":Lorg/apache/lucene/index/SegmentReader;
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readerClones:Ljava/util/List;

    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4242
    invoke-virtual {v11}, Lorg/apache/lucene/index/SegmentReader;->numDocs()I

    move-result v5

    if-lez v5, :cond_1

    .line 4243
    invoke-virtual {v4, v11}, Lorg/apache/lucene/index/SegmentMerger;->add(Lorg/apache/lucene/index/IndexReader;)V

    .line 4244
    invoke-virtual {v11}, Lorg/apache/lucene/index/SegmentReader;->numDocs()I

    move-result v5

    add-int v26, v26, v5

    .line 4246
    :cond_1
    add-int/lit8 v21, v21, 0x1

    .line 4247
    goto :goto_0

    .line 4249
    .end local v11    # "clone":Lorg/apache/lucene/index/SegmentReader;
    .end local v13    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v20    # "reader":Lorg/apache/lucene/index/SegmentReader;
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v5, :cond_3

    .line 4250
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "merge: total "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " docs"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4253
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->checkAborted(Lorg/apache/lucene/store/Directory;)V

    .line 4256
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentMerger;->merge()I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lorg/apache/lucene/index/SegmentInfo;->docCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_d

    .line 4259
    .end local v16    # "mergedDocCount":I
    .local v17, "mergedDocCount":I
    :try_start_1
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/FieldInfos;->hasVectors()Z

    move-result v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/SegmentInfo;->setHasVectors(Z)V

    .line 4261
    sget-boolean v5, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_5

    move/from16 v0, v17

    move/from16 v1, v26

    if-eq v0, v1, :cond_5

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4387
    :catchall_0
    move-exception v5

    move/from16 v16, v17

    .end local v17    # "mergedDocCount":I
    .restart local v16    # "mergedDocCount":I
    :goto_1
    if-nez v23, :cond_4

    .line 4388
    const/4 v6, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lorg/apache/lucene/index/IndexWriter;->closeMergeReaders(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V

    .line 4387
    :cond_4
    throw v5

    .line 4263
    .end local v16    # "mergedDocCount":I
    .restart local v17    # "mergedDocCount":I
    :cond_5
    :try_start_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v5, :cond_6

    .line 4264
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "merge store matchedCount="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentMerger;->getMatchedSubReaderCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " vs "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->readers:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4267
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/lucene/index/IndexWriter;->anyNonBulkMerges:Z

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentMerger;->getAnyNonBulkMerges()Z

    move-result v6

    or-int/2addr v5, v6

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lorg/apache/lucene/index/IndexWriter;->anyNonBulkMerges:Z

    .line 4269
    sget-boolean v5, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_7

    move/from16 v0, v17

    move/from16 v1, v26

    if-eq v0, v1, :cond_7

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "mergedDocCount="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v8, " vs "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 4274
    :cond_7
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/FieldInfos;->hasProx()Z

    move-result v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/SegmentInfo;->setHasProx(Z)V

    .line 4277
    monitor-enter p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4278
    :try_start_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v0, p1

    iget-object v8, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v5, v6, v8}, Lorg/apache/lucene/index/MergePolicy;->useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v27

    .line 4279
    .local v27, "useCompoundFile":Z
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 4281
    if-eqz v27, :cond_14

    .line 4283
    const/16 v23, 0x0

    .line 4284
    :try_start_4
    const-string/jumbo v5, "cfs"

    invoke-static {v7, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v12

    .line 4287
    .local v12, "compoundFileName":Ljava/lang/String;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v5, :cond_8

    .line 4288
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "create compound file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4290
    :cond_8
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4, v12, v5}, Lorg/apache/lucene/index/SegmentMerger;->createCompoundFile(Ljava/lang/String;Lorg/apache/lucene/index/SegmentInfo;)Ljava/util/Collection;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 4291
    const/16 v23, 0x1

    .line 4305
    if-nez v23, :cond_a

    .line 4306
    :try_start_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v5, :cond_9

    .line 4307
    const-string/jumbo v5, "hit exception creating compound file during merge"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4310
    :cond_9
    monitor-enter p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 4311
    :try_start_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v5, v12}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 4312
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 4313
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_c

    .line 4317
    :cond_a
    :goto_2
    const/16 v23, 0x0

    .line 4319
    :try_start_8
    monitor-enter p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 4323
    :try_start_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 4325
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isAborted()Z

    move-result v5

    if-eqz v5, :cond_13

    .line 4326
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v5, :cond_b

    .line 4327
    const-string/jumbo v5, "abort merge after building CFS"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4329
    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v5, v12}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 4330
    const/4 v5, 0x0

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    .line 4387
    if-nez v23, :cond_c

    .line 4388
    const/4 v6, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lorg/apache/lucene/index/IndexWriter;->closeMergeReaders(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V

    :cond_c
    move/from16 v17, v5

    .line 4392
    .end local v12    # "compoundFileName":Ljava/lang/String;
    .end local v17    # "mergedDocCount":I
    :cond_d
    :goto_3
    return v17

    .line 4279
    .end local v27    # "useCompoundFile":Z
    .restart local v17    # "mergedDocCount":I
    :catchall_1
    move-exception v5

    :try_start_a
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    throw v5
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 4292
    .restart local v12    # "compoundFileName":Ljava/lang/String;
    .restart local v27    # "useCompoundFile":Z
    :catch_0
    move-exception v14

    .line 4293
    .local v14, "ioe":Ljava/io/IOException;
    :try_start_c
    monitor-enter p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 4294
    :try_start_d
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isAborted()Z

    move-result v5

    if-eqz v5, :cond_f

    .line 4301
    :goto_4
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    .line 4305
    if-nez v23, :cond_a

    .line 4306
    :try_start_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v5, :cond_e

    .line 4307
    const-string/jumbo v5, "hit exception creating compound file during merge"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4310
    :cond_e
    monitor-enter p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 4311
    :try_start_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v5, v12}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 4312
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 4313
    monitor-exit p0

    goto :goto_2

    :catchall_2
    move-exception v5

    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    :try_start_10
    throw v5
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 4299
    :cond_f
    :try_start_11
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v14, v1}, Lorg/apache/lucene/index/IndexWriter;->handleMergeException(Ljava/lang/Throwable;Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    goto :goto_4

    .line 4301
    :catchall_3
    move-exception v5

    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    :try_start_12
    throw v5
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    .line 4305
    .end local v14    # "ioe":Ljava/io/IOException;
    :catchall_4
    move-exception v5

    if-nez v23, :cond_11

    .line 4306
    :try_start_13
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v6, :cond_10

    .line 4307
    const-string/jumbo v6, "hit exception creating compound file during merge"

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4310
    :cond_10
    monitor-enter p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 4311
    :try_start_14
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v6, v12}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 4312
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    move-object/from16 v0, p1

    iget-object v8, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v8}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v8

    invoke-virtual {v6, v8}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 4313
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_8

    .line 4305
    :cond_11
    :try_start_15
    throw v5
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    .line 4302
    :catch_1
    move-exception v24

    .line 4303
    .local v24, "t":Ljava/lang/Throwable;
    :try_start_16
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/IndexWriter;->handleMergeException(Ljava/lang/Throwable;Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_4

    .line 4305
    if-nez v23, :cond_a

    .line 4306
    :try_start_17
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v5, :cond_12

    .line 4307
    const-string/jumbo v5, "hit exception creating compound file during merge"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4310
    :cond_12
    monitor-enter p0
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    .line 4311
    :try_start_18
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v5, v12}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteFile(Ljava/lang/String;)V

    .line 4312
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 4313
    monitor-exit p0

    goto/16 :goto_2

    :catchall_5
    move-exception v5

    monitor-exit p0
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_5

    :try_start_19
    throw v5
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    .line 4332
    .end local v24    # "t":Ljava/lang/Throwable;
    :cond_13
    :try_start_1a
    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_6

    .line 4334
    :try_start_1b
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/SegmentInfo;->setUseCompoundFile(Z)V

    .line 4337
    .end local v12    # "compoundFileName":Ljava/lang/String;
    :cond_14
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v5, :cond_15

    .line 4338
    const-string/jumbo v5, "merged segment size=%.3f MB vs estimate=%.3f MB"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-object v9, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytes(Z)J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-double v0, v0

    move-wide/from16 v28, v0

    const-wide/high16 v30, 0x4090000000000000L    # 1024.0

    div-double v28, v28, v30

    const-wide/high16 v30, 0x4090000000000000L    # 1024.0

    div-double v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v6, v8

    const/4 v8, 0x1

    move-object/from16 v0, p1

    iget-wide v0, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->estimatedMergeBytes:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x400

    div-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-double v0, v0

    move-wide/from16 v28, v0

    const-wide/high16 v30, 0x4090000000000000L    # 1024.0

    div-double v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4341
    :cond_15
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v5}, Lorg/apache/lucene/index/IndexWriterConfig;->getMergedSegmentWarmer()Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    move-result-object v19

    .line 4346
    .local v19, "mergedSegmentWarmer":Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;
    if-eqz v19, :cond_19

    .line 4350
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v5}, Lorg/apache/lucene/index/IndexWriterConfig;->getReaderTermsIndexDivisor()I

    move-result v25

    .line 4351
    .local v25, "termsIndexDivisor":I
    const/4 v15, 0x1

    .line 4361
    .local v15, "loadDocStores":Z
    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    const/16 v8, 0x400

    move/from16 v0, v25

    invoke-virtual {v5, v6, v15, v8, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfo;ZII)Lorg/apache/lucene/index/SegmentReader;
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    move-result-object v18

    .line 4363
    .local v18, "mergedReader":Lorg/apache/lucene/index/SegmentReader;
    :try_start_1c
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/lucene/index/IndexWriter;->poolReaders:Z

    if-eqz v5, :cond_16

    if-eqz v19, :cond_16

    .line 4364
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;->warm(Lorg/apache/lucene/index/IndexReader;)V

    .line 4367
    :cond_16
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/IndexWriter;->commitMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/index/SegmentReader;)Z
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_7

    move-result v5

    if-nez v5, :cond_1b

    .line 4369
    const/4 v5, 0x0

    .line 4372
    :try_start_1d
    monitor-enter p0
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    .line 4373
    :try_start_1e
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/SegmentReader;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 4377
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 4379
    :cond_17
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_a

    .line 4387
    if-nez v23, :cond_18

    .line 4388
    const/4 v6, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lorg/apache/lucene/index/IndexWriter;->closeMergeReaders(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V

    :cond_18
    move/from16 v17, v5

    .line 4369
    goto/16 :goto_3

    .line 4332
    .end local v15    # "loadDocStores":Z
    .end local v18    # "mergedReader":Lorg/apache/lucene/index/SegmentReader;
    .end local v19    # "mergedSegmentWarmer":Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;
    .end local v25    # "termsIndexDivisor":I
    .restart local v12    # "compoundFileName":Ljava/lang/String;
    :catchall_6
    move-exception v5

    :try_start_1f
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_6

    :try_start_20
    throw v5

    .line 4353
    .end local v12    # "compoundFileName":Ljava/lang/String;
    .restart local v19    # "mergedSegmentWarmer":Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;
    :cond_19
    const/16 v25, -0x1

    .line 4354
    .restart local v25    # "termsIndexDivisor":I
    const/4 v15, 0x0

    .restart local v15    # "loadDocStores":Z
    goto :goto_5

    .line 4372
    .restart local v18    # "mergedReader":Lorg/apache/lucene/index/SegmentReader;
    :catchall_7
    move-exception v5

    monitor-enter p0
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    .line 4373
    :try_start_21
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/SegmentReader;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 4377
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 4379
    :cond_1a
    monitor-exit p0
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_9

    .line 4372
    :try_start_22
    throw v5

    :cond_1b
    monitor-enter p0
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_0

    .line 4373
    :try_start_23
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/SegmentReader;)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 4377
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 4379
    :cond_1c
    monitor-exit p0
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_b

    .line 4382
    const/16 v23, 0x1

    .line 4387
    if-nez v23, :cond_d

    .line 4388
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/index/IndexWriter;->closeMergeReaders(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V

    goto/16 :goto_3

    .line 4313
    .end local v15    # "loadDocStores":Z
    .end local v18    # "mergedReader":Lorg/apache/lucene/index/SegmentReader;
    .end local v19    # "mergedSegmentWarmer":Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;
    .end local v25    # "termsIndexDivisor":I
    .restart local v12    # "compoundFileName":Ljava/lang/String;
    :catchall_8
    move-exception v5

    :try_start_24
    monitor-exit p0
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_8

    :try_start_25
    throw v5
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_0

    .line 4379
    .end local v12    # "compoundFileName":Ljava/lang/String;
    .restart local v15    # "loadDocStores":Z
    .restart local v18    # "mergedReader":Lorg/apache/lucene/index/SegmentReader;
    .restart local v19    # "mergedSegmentWarmer":Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;
    .restart local v25    # "termsIndexDivisor":I
    :catchall_9
    move-exception v5

    :try_start_26
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_9

    :try_start_27
    throw v5
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_0

    :catchall_a
    move-exception v5

    :try_start_28
    monitor-exit p0
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_a

    :try_start_29
    throw v5
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_0

    :catchall_b
    move-exception v5

    :try_start_2a
    monitor-exit p0
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_b

    :try_start_2b
    throw v5
    :try_end_2b
    .catchall {:try_start_2b .. :try_end_2b} :catchall_0

    .line 4313
    .end local v15    # "loadDocStores":Z
    .end local v18    # "mergedReader":Lorg/apache/lucene/index/SegmentReader;
    .end local v19    # "mergedSegmentWarmer":Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;
    .end local v25    # "termsIndexDivisor":I
    .restart local v12    # "compoundFileName":Ljava/lang/String;
    :catchall_c
    move-exception v5

    :try_start_2c
    monitor-exit p0
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_c

    :try_start_2d
    throw v5
    :try_end_2d
    .catchall {:try_start_2d .. :try_end_2d} :catchall_0

    .line 4387
    .end local v12    # "compoundFileName":Ljava/lang/String;
    .end local v17    # "mergedDocCount":I
    .end local v27    # "useCompoundFile":Z
    .restart local v16    # "mergedDocCount":I
    :catchall_d
    move-exception v5

    goto/16 :goto_1
.end method

.method private messageState()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1667
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\ndir="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/util/Constants;->LUCENE_VERSION:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriterConfig;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1671
    return-void
.end method

.method private varargs noDupDirs([Lorg/apache/lucene/store/Directory;)V
    .locals 8
    .param p1, "dirs"    # [Lorg/apache/lucene/store/Directory;

    .prologue
    .line 3041
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 3042
    .local v2, "dups":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/store/Directory;>;"
    move-object v0, p1

    .local v0, "arr$":[Lorg/apache/lucene/store/Directory;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v1, v0, v3

    .line 3043
    .local v1, "dir":Lorg/apache/lucene/store/Directory;
    invoke-virtual {v2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3044
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Directory "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " appears more than once"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 3045
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    if-ne v1, v5, :cond_1

    .line 3046
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v6, "Cannot add directory to itself"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 3047
    :cond_1
    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 3042
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3049
    .end local v1    # "dir":Lorg/apache/lucene/store/Directory;
    :cond_2
    return-void
.end method

.method private pushMaxBufferedDocs()V
    .locals 5

    .prologue
    .line 1477
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriterConfig;->getMaxBufferedDocs()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 1478
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    .line 1479
    .local v2, "mp":Lorg/apache/lucene/index/MergePolicy;
    instance-of v3, v2, Lorg/apache/lucene/index/LogDocMergePolicy;

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 1480
    check-cast v0, Lorg/apache/lucene/index/LogDocMergePolicy;

    .line 1481
    .local v0, "lmp":Lorg/apache/lucene/index/LogDocMergePolicy;
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriterConfig;->getMaxBufferedDocs()I

    move-result v1

    .line 1482
    .local v1, "maxBufferedDocs":I
    invoke-virtual {v0}, Lorg/apache/lucene/index/LogDocMergePolicy;->getMinMergeDocs()I

    move-result v3

    if-eq v3, v1, :cond_1

    .line 1483
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_0

    .line 1484
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "now push maxBufferedDocs "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " to LogDocMergePolicy"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1485
    :cond_0
    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/LogDocMergePolicy;->setMinMergeDocs(I)V

    .line 1489
    .end local v0    # "lmp":Lorg/apache/lucene/index/LogDocMergePolicy;
    .end local v1    # "maxBufferedDocs":I
    .end local v2    # "mp":Lorg/apache/lucene/index/MergePolicy;
    :cond_1
    return-void
.end method

.method private declared-synchronized resetMergeExceptions()V
    .locals 4

    .prologue
    .line 3036
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeExceptions:Ljava/util/List;

    .line 3037
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeGen:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeGen:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3038
    monitor-exit p0

    return-void

    .line 3036
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private rollbackInternal()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2829
    const/4 v1, 0x0

    .line 2831
    .local v1, "success":Z
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_0

    .line 2832
    const-string/jumbo v2, "rollback"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2836
    :cond_0
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 2837
    const/4 v2, 0x0

    :try_start_1
    invoke-direct {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->finishMerges(Z)V

    .line 2838
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    .line 2839
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2841
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_1

    .line 2842
    const-string/jumbo v2, "rollback: done finish merges"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2848
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MergePolicy;->close()V

    .line 2849
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MergeScheduler;->close()V

    .line 2851
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->clear()V

    .line 2853
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2855
    :try_start_3
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    if-eqz v2, :cond_2

    .line 2856
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/SegmentInfos;->rollbackCommit(Lorg/apache/lucene/store/Directory;)V

    .line 2857
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 2858
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    .line 2859
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 2867
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->rollbackSegments:Ljava/util/List;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/SegmentInfos;->rollbackSegmentInfos(Ljava/util/List;)V

    .line 2868
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_3

    .line 2869
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "rollback: infos="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2872
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 2874
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_6

    const-string/jumbo v2, "rollback before checkpoint"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 2880
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2888
    :catch_0
    move-exception v0

    .line 2889
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_5
    const-string/jumbo v2, "rollbackInternal"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2891
    monitor-enter p0

    .line 2892
    if-nez v1, :cond_4

    .line 2893
    const/4 v2, 0x0

    :try_start_6
    iput-boolean v2, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    .line 2894
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 2895
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_4

    .line 2896
    const-string/jumbo v2, "hit exception during rollback"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2898
    :cond_4
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    .line 2901
    .end local v0    # "oom":Ljava/lang/OutOfMemoryError;
    :goto_0
    invoke-direct {p0, v5}, Lorg/apache/lucene/index/IndexWriter;->closeInternal(Z)V

    .line 2902
    return-void

    .line 2839
    :catchall_1
    move-exception v2

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v2
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 2891
    :catchall_2
    move-exception v2

    monitor-enter p0

    .line 2892
    if-nez v1, :cond_5

    .line 2893
    const/4 v3, 0x0

    :try_start_9
    iput-boolean v3, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    .line 2894
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 2895
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_5

    .line 2896
    const-string/jumbo v3, "hit exception during rollback"

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2898
    :cond_5
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 2891
    throw v2

    .line 2878
    :cond_6
    :try_start_a
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 2879
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh()V

    .line 2880
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 2883
    :try_start_b
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->clear(Ljava/util/List;)V

    .line 2885
    iget-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    iput-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->lastCommitChangeCount:J
    :try_end_b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 2887
    const/4 v1, 0x1

    .line 2891
    monitor-enter p0

    .line 2892
    if-nez v1, :cond_7

    .line 2893
    const/4 v2, 0x0

    :try_start_c
    iput-boolean v2, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    .line 2894
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 2895
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_7

    .line 2896
    const-string/jumbo v2, "hit exception during rollback"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2898
    :cond_7
    monitor-exit p0

    goto :goto_0

    :catchall_3
    move-exception v2

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    throw v2

    :catchall_4
    move-exception v2

    :try_start_d
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    throw v2

    .restart local v0    # "oom":Ljava/lang/OutOfMemoryError;
    :catchall_5
    move-exception v2

    :try_start_e
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    throw v2
.end method

.method public static setDefaultInfoStream(Ljava/io/PrintStream;)V
    .locals 0
    .param p0, "infoStream"    # Ljava/io/PrintStream;

    .prologue
    .line 1640
    sput-object p0, Lorg/apache/lucene/index/IndexWriter;->defaultInfoStream:Ljava/io/PrintStream;

    .line 1641
    return-void
.end method

.method public static setDefaultWriteLockTimeout(J)V
    .locals 0
    .param p0, "writeLockTimeout"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1719
    invoke-static {p0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setDefaultWriteLockTimeout(J)V

    .line 1720
    return-void
.end method

.method private setDiagnostics(Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;)V
    .locals 1
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "source"    # Ljava/lang/String;

    .prologue
    .line 4106
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/index/IndexWriter;->setDiagnostics(Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;Ljava/util/Map;)V

    .line 4107
    return-void
.end method

.method private setDiagnostics(Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "source"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfo;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4110
    .local p3, "details":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 4111
    .local v0, "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v1, "source"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4112
    const-string/jumbo v1, "lucene.version"

    sget-object v2, Lorg/apache/lucene/util/Constants;->LUCENE_VERSION:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4113
    const-string/jumbo v1, "os"

    sget-object v2, Lorg/apache/lucene/util/Constants;->OS_NAME:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4114
    const-string/jumbo v1, "os.arch"

    sget-object v2, Lorg/apache/lucene/util/Constants;->OS_ARCH:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4115
    const-string/jumbo v1, "os.version"

    sget-object v2, Lorg/apache/lucene/util/Constants;->OS_VERSION:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4116
    const-string/jumbo v1, "java.version"

    sget-object v2, Lorg/apache/lucene/util/Constants;->JAVA_VERSION:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4117
    const-string/jumbo v1, "java.vendor"

    sget-object v2, Lorg/apache/lucene/util/Constants;->JAVA_VENDOR:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4118
    if-eqz p3, :cond_0

    .line 4119
    invoke-interface {v0, p3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 4121
    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/SegmentInfo;->setDiagnostics(Ljava/util/Map;)V

    .line 4122
    return-void
.end method

.method private declared-synchronized shouldClose()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1821
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    if-nez v1, :cond_1

    .line 1822
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    if-nez v1, :cond_0

    .line 1823
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1832
    :goto_1
    monitor-exit p0

    return v0

    .line 1829
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->doWait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1821
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1832
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private startCommit(Lorg/apache/lucene/index/SegmentInfos;Ljava/util/Map;)V
    .locals 6
    .param p1, "toSync"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4503
    .local p2, "commitUserData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    const-string/jumbo v2, "startStartCommit"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 4504
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 4506
    :cond_1
    iget-boolean v2, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v2, :cond_2

    .line 4507
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "this writer hit an OutOfMemoryError; cannot commit"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 4512
    :cond_2
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_3

    .line 4513
    const-string/jumbo v2, "startCommit(): start"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4516
    :cond_3
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 4518
    :try_start_1
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    iget-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->lastCommitChangeCount:J

    iget-wide v4, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 4541
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    .line 4593
    :catch_0
    move-exception v0

    .line 4594
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    const-string/jumbo v2, "startCommit"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    .line 4596
    .end local v0    # "oom":Ljava/lang/OutOfMemoryError;
    :goto_0
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_6

    const-string/jumbo v2, "finishStartCommit"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 4520
    :cond_4
    :try_start_3
    iget-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommitChangeCount:J

    iget-wide v4, p0, Lorg/apache/lucene/index/IndexWriter;->lastCommitChangeCount:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_7

    .line 4521
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_5

    .line 4522
    const-string/jumbo v2, "  skip startCommit(): no changes pending"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4524
    :cond_5
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 4525
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 4526
    monitor-exit p0

    .line 4597
    :cond_6
    return-void

    .line 4533
    :cond_7
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_8

    .line 4534
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startCommit index="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " changeCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4536
    :cond_8
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_9

    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->filesExist(Lorg/apache/lucene/index/SegmentInfos;)Z

    move-result v2

    if-nez v2, :cond_9

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 4538
    :cond_9
    if-eqz p2, :cond_a

    .line 4539
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/SegmentInfos;->setUserData(Ljava/util/Map;)V

    .line 4541
    :cond_a
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4543
    :try_start_4
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_b

    const-string/jumbo v2, "midStartCommit"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0

    .line 4545
    :cond_b
    const/4 v1, 0x0

    .line 4550
    .local v1, "pendingCommitSet":Z
    :try_start_5
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V

    .line 4552
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_e

    const-string/jumbo v2, "midStartCommit2"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_e

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 4575
    :catchall_1
    move-exception v2

    :try_start_6
    monitor-enter p0
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_0

    .line 4581
    :try_start_7
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/SegmentInfos;->updateGeneration(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 4583
    if-nez v1, :cond_d

    .line 4584
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_c

    .line 4585
    const-string/jumbo v3, "hit exception committing segments file"

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4588
    :cond_c
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 4589
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 4591
    :cond_d
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 4575
    :try_start_8
    throw v2
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_0

    .line 4554
    :cond_e
    :try_start_9
    monitor-enter p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 4556
    :try_start_a
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_f

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    if-eqz v2, :cond_f

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 4566
    :catchall_2
    move-exception v2

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :try_start_b
    throw v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 4558
    :cond_f
    :try_start_c
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_10

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v2

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_10

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 4563
    :cond_10
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/SegmentInfos;->prepareCommit(Lorg/apache/lucene/store/Directory;)V

    .line 4564
    const/4 v1, 0x1

    .line 4565
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    .line 4566
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 4568
    :try_start_d
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_11

    .line 4569
    const-string/jumbo v2, "done all syncs"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4572
    :cond_11
    sget-boolean v2, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_12

    const-string/jumbo v2, "midStartCommitSuccess"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_12

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 4591
    :catchall_3
    move-exception v2

    :try_start_e
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    :try_start_f
    throw v2

    .line 4575
    :cond_12
    monitor-enter p0
    :try_end_f
    .catch Ljava/lang/OutOfMemoryError; {:try_start_f .. :try_end_f} :catch_0

    .line 4581
    :try_start_10
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/SegmentInfos;->updateGeneration(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 4583
    if-nez v1, :cond_14

    .line 4584
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_13

    .line 4585
    const-string/jumbo v2, "hit exception committing segments file"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4588
    :cond_13
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 4589
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 4591
    :cond_14
    monitor-exit p0

    goto/16 :goto_0

    :catchall_4
    move-exception v2

    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    :try_start_11
    throw v2
    :try_end_11
    .catch Ljava/lang/OutOfMemoryError; {:try_start_11 .. :try_end_11} :catch_0
.end method

.method public static unlock(Lorg/apache/lucene/store/Directory;)V
    .locals 1
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4617
    const-string/jumbo v0, "write.lock"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/Directory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/store/Lock;->release()V

    .line 4618
    return-void
.end method

.method private declared-synchronized updatePendingMerges(I)V
    .locals 7
    .param p1, "maxNumSegments"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    .line 2745
    monitor-enter p0

    :try_start_0
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-eq p1, v5, :cond_0

    if-gtz p1, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 2747
    :cond_0
    :try_start_1
    iget-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v4, :cond_2

    .line 2777
    :cond_1
    monitor-exit p0

    return-void

    .line 2752
    :cond_2
    :try_start_2
    iget-boolean v4, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-nez v4, :cond_1

    .line 2757
    if-eq p1, v5, :cond_3

    .line 2758
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v4, v5, p1, v6}, Lorg/apache/lucene/index/MergePolicy;->findForcedMerges(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v3

    .line 2759
    .local v3, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    if-eqz v3, :cond_4

    .line 2760
    iget-object v4, v3, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 2761
    .local v2, "numMerges":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_4

    .line 2762
    iget-object v4, v3, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 2763
    .local v1, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iput p1, v1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    .line 2761
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2768
    .end local v0    # "i":I
    .end local v1    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .end local v2    # "numMerges":I
    .end local v3    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/MergePolicy;->findMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v3

    .line 2771
    .restart local v3    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_4
    if-eqz v3, :cond_1

    .line 2772
    iget-object v4, v3, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 2773
    .restart local v2    # "numMerges":I
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 2774
    iget-object v4, v3, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->registerMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2773
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public addDocument(Lorg/apache/lucene/document/Document;)V
    .locals 1
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2034
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;->addDocument(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 2035
    return-void
.end method

.method public addDocument(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 5
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2055
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2056
    const/4 v0, 0x0

    .line 2057
    .local v0, "doFlush":Z
    const/4 v2, 0x0

    .line 2060
    .local v2, "success":Z
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, p2, v4}, Lorg/apache/lucene/index/DocumentsWriter;->updateDocument(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/Term;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2061
    const/4 v2, 0x1

    .line 2063
    if-nez v2, :cond_0

    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_0

    .line 2064
    const-string/jumbo v3, "hit exception adding document"

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2066
    :cond_0
    if-eqz v0, :cond_1

    .line 2067
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V

    .line 2071
    :cond_1
    :goto_0
    return-void

    .line 2063
    :catchall_0
    move-exception v3

    if-nez v2, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_2

    .line 2064
    const-string/jumbo v4, "hit exception adding document"

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2063
    :cond_2
    throw v3
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    .line 2068
    :catch_0
    move-exception v1

    .line 2069
    .local v1, "oom":Ljava/lang/OutOfMemoryError;
    const-string/jumbo v3, "addDocument"

    invoke-direct {p0, v1, v3}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public addDocuments(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2109
    .local p1, "docs":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/document/Document;>;"
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;->addDocuments(Ljava/util/Collection;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 2110
    return-void
.end method

.method public addDocuments(Ljava/util/Collection;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2125
    .local p1, "docs":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/document/Document;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->updateDocuments(Lorg/apache/lucene/index/Term;Ljava/util/Collection;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 2126
    return-void
.end method

.method public varargs addIndexes([Lorg/apache/lucene/index/IndexReader;)V
    .locals 17
    .param p1, "readers"    # [Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3182
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 3185
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_0

    .line 3186
    const-string/jumbo v2, "flush at addIndexes(IndexReader...)"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3187
    :cond_0
    const/4 v2, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V

    .line 3189
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->newSegmentName()Ljava/lang/String;

    move-result-object v4

    .line 3192
    .local v4, "mergedName":Ljava/lang/String;
    new-instance v1, Lorg/apache/lucene/index/SegmentMerger;

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexWriterConfig;->getTermIndexInterval()I

    move-result v3

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->payloadProcessorProvider:Lorg/apache/lucene/index/PayloadProcessorProvider;

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v7}, Lorg/apache/lucene/index/DocumentsWriter;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfos;->clone()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/FieldInfos;

    invoke-direct/range {v1 .. v7}, Lorg/apache/lucene/index/SegmentMerger;-><init>(Lorg/apache/lucene/store/Directory;ILjava/lang/String;Lorg/apache/lucene/index/MergePolicy$OneMerge;Lorg/apache/lucene/index/PayloadProcessorProvider;Lorg/apache/lucene/index/FieldInfos;)V

    .line 3196
    .local v1, "merger":Lorg/apache/lucene/index/SegmentMerger;
    move-object/from16 v11, p1

    .local v11, "arr$":[Lorg/apache/lucene/index/IndexReader;
    array-length v13, v11

    .local v13, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_0
    if-ge v12, v13, :cond_1

    aget-object v15, v11, v12

    .line 3197
    .local v15, "reader":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v1, v15}, Lorg/apache/lucene/index/SegmentMerger;->add(Lorg/apache/lucene/index/IndexReader;)V

    .line 3196
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 3199
    .end local v15    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_1
    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentMerger;->merge()I

    move-result v5

    .line 3201
    .local v5, "docCount":I
    new-instance v3, Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfos;->hasProx()Z

    move-result v9

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentMerger;->fieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfos;->hasVectors()Z

    move-result v10

    invoke-direct/range {v3 .. v10}, Lorg/apache/lucene/index/SegmentInfo;-><init>(Ljava/lang/String;ILorg/apache/lucene/store/Directory;ZZZZ)V

    .line 3205
    .local v3, "info":Lorg/apache/lucene/index/SegmentInfo;
    const-string/jumbo v2, "addIndexes(IndexReader...)"

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lorg/apache/lucene/index/IndexWriter;->setDiagnostics(Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;)V

    .line 3208
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 3209
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    if-eqz v2, :cond_2

    .line 3210
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v2, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 3211
    monitor-exit p0

    .line 3243
    .end local v1    # "merger":Lorg/apache/lucene/index/SegmentMerger;
    .end local v3    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v4    # "mergedName":Ljava/lang/String;
    .end local v5    # "docCount":I
    .end local v11    # "arr$":[Lorg/apache/lucene/index/IndexReader;
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    :goto_1
    return-void

    .line 3213
    .restart local v1    # "merger":Lorg/apache/lucene/index/SegmentMerger;
    .restart local v3    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .restart local v4    # "mergedName":Ljava/lang/String;
    .restart local v5    # "docCount":I
    .restart local v11    # "arr$":[Lorg/apache/lucene/index/IndexReader;
    .restart local v12    # "i$":I
    .restart local v13    # "len$":I
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 3214
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, v6, v3}, Lorg/apache/lucene/index/MergePolicy;->useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v16

    .line 3215
    .local v16, "useCompoundFile":Z
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3218
    if-eqz v16, :cond_3

    .line 3219
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ".cfs"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/index/SegmentMerger;->createCompoundFile(Ljava/lang/String;Lorg/apache/lucene/index/SegmentInfo;)Ljava/util/Collection;

    .line 3223
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    .line 3224
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v2, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 3225
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 3226
    const/4 v2, 0x1

    :try_start_4
    invoke-virtual {v3, v2}, Lorg/apache/lucene/index/SegmentInfo;->setUseCompoundFile(Z)V

    .line 3230
    :cond_3
    monitor-enter p0
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0

    .line 3231
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    if-eqz v2, :cond_4

    .line 3232
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v2, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 3233
    monitor-exit p0

    goto :goto_1

    .line 3238
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v2
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_0

    .line 3240
    .end local v1    # "merger":Lorg/apache/lucene/index/SegmentMerger;
    .end local v3    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v4    # "mergedName":Ljava/lang/String;
    .end local v5    # "docCount":I
    .end local v11    # "arr$":[Lorg/apache/lucene/index/IndexReader;
    .end local v12    # "i$":I
    .end local v13    # "len$":I
    .end local v16    # "useCompoundFile":Z
    :catch_0
    move-exception v14

    .line 3241
    .local v14, "oom":Ljava/lang/OutOfMemoryError;
    const-string/jumbo v2, "addIndexes(IndexReader...)"

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v2}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_1

    .line 3215
    .end local v14    # "oom":Ljava/lang/OutOfMemoryError;
    .restart local v1    # "merger":Lorg/apache/lucene/index/SegmentMerger;
    .restart local v3    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .restart local v4    # "mergedName":Ljava/lang/String;
    .restart local v5    # "docCount":I
    .restart local v11    # "arr$":[Lorg/apache/lucene/index/IndexReader;
    .restart local v12    # "i$":I
    .restart local v13    # "len$":I
    :catchall_1
    move-exception v2

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v2
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_0

    .line 3225
    .restart local v16    # "useCompoundFile":Z
    :catchall_2
    move-exception v2

    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v2
    :try_end_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_0

    .line 3235
    :cond_4
    :try_start_b
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 3236
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/SegmentInfos;->add(Lorg/apache/lucene/index/SegmentInfo;)V

    .line 3237
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 3238
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_1
.end method

.method public varargs addIndexes([Lorg/apache/lucene/store/Directory;)V
    .locals 20
    .param p1, "dirs"    # [Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3106
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 3108
    invoke-direct/range {p0 .. p1}, Lorg/apache/lucene/index/IndexWriter;->noDupDirs([Lorg/apache/lucene/store/Directory;)V

    .line 3111
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 3112
    const-string/jumbo v17, "flush at addIndexes(Directory...)"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3113
    :cond_0
    const/16 v17, 0x0

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V

    .line 3115
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 3116
    .local v11, "infos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    invoke-static {}, Lorg/apache/lucene/util/StringHelper;->getVersionComparator()Ljava/util/Comparator;

    move-result-object v16

    .line 3117
    .local v16, "versionComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Ljava/lang/String;>;"
    move-object/from16 v3, p1

    .local v3, "arr$":[Lorg/apache/lucene/store/Directory;
    array-length v12, v3

    .local v12, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    move v9, v8

    .end local v8    # "i$":I
    .local v9, "i$":I
    :goto_0
    if-ge v9, v12, :cond_5

    aget-object v4, v3, v9

    .line 3118
    .local v4, "dir":Lorg/apache/lucene/store/Directory;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    .line 3119
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "addIndexes: process directory "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3121
    :cond_1
    new-instance v15, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v15}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 3122
    .local v15, "sis":Lorg/apache/lucene/index/SegmentInfos;
    invoke-virtual {v15, v4}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V

    .line 3123
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 3124
    .local v5, "dsFilesCopied":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 3125
    .local v7, "dsNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v15}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .end local v9    # "i$":I
    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/index/SegmentInfo;

    .line 3126
    .local v10, "info":Lorg/apache/lucene/index/SegmentInfo;
    sget-boolean v17, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v17, :cond_2

    invoke-interface {v11, v10}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    new-instance v17, Ljava/lang/AssertionError;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "dup info dir="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget-object v0, v10, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, " name="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget-object v0, v10, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v17
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 3147
    .end local v3    # "arr$":[Lorg/apache/lucene/store/Directory;
    .end local v4    # "dir":Lorg/apache/lucene/store/Directory;
    .end local v5    # "dsFilesCopied":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v7    # "dsNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v11    # "infos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    .end local v12    # "len$":I
    .end local v15    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    .end local v16    # "versionComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Ljava/lang/String;>;"
    :catch_0
    move-exception v14

    .line 3148
    .local v14, "oom":Ljava/lang/OutOfMemoryError;
    const-string/jumbo v17, "addIndexes(Directory...)"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v14, v1}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    .line 3150
    .end local v14    # "oom":Ljava/lang/OutOfMemoryError;
    :goto_2
    return-void

    .line 3128
    .restart local v3    # "arr$":[Lorg/apache/lucene/store/Directory;
    .restart local v4    # "dir":Lorg/apache/lucene/store/Directory;
    .restart local v5    # "dsFilesCopied":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v7    # "dsNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v10    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .restart local v11    # "infos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    .restart local v12    # "len$":I
    .restart local v15    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    .restart local v16    # "versionComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Ljava/lang/String;>;"
    :cond_2
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->newSegmentName()Ljava/lang/String;

    move-result-object v13

    .line 3129
    .local v13, "newSegName":Ljava/lang/String;
    invoke-virtual {v10}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreSegment()Ljava/lang/String;

    move-result-object v6

    .line 3131
    .local v6, "dsName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v17, v0

    if-eqz v17, :cond_3

    .line 3132
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "addIndexes: process segment origName="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    iget-object v0, v10, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " newName="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " dsName="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " info="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3135
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v13, v7, v5}, Lorg/apache/lucene/index/IndexWriter;->copySegmentAsIs(Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;Ljava/util/Map;Ljava/util/Set;)V

    .line 3137
    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 3117
    .end local v6    # "dsName":Ljava/lang/String;
    .end local v10    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v13    # "newSegName":Ljava/lang/String;
    :cond_4
    add-int/lit8 v8, v9, 0x1

    .local v8, "i$":I
    move v9, v8

    .end local v8    # "i$":I
    .restart local v9    # "i$":I
    goto/16 :goto_0

    .line 3141
    .end local v4    # "dir":Lorg/apache/lucene/store/Directory;
    .end local v5    # "dsFilesCopied":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v7    # "dsNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v15    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_5
    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    .line 3142
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 3143
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/SegmentInfos;->addAll(Ljava/lang/Iterable;)V

    .line 3144
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->checkpoint()V

    .line 3145
    monitor-exit p0

    goto :goto_2

    :catchall_0
    move-exception v17

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v17
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
.end method

.method public varargs addIndexesNoOptimize([Lorg/apache/lucene/store/Directory;)V
    .locals 0
    .param p1, "dirs"    # [Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3057
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->addIndexes([Lorg/apache/lucene/store/Directory;)V

    .line 3058
    return-void
.end method

.method declared-synchronized addMergeException(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 4
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .prologue
    .line 4396
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->getException()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 4397
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeExceptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeGen:J

    iget-wide v2, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->mergeGen:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 4398
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeExceptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4399
    :cond_1
    monitor-exit p0

    return-void
.end method

.method declared-synchronized checkpoint()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3030
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    .line 3031
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->changed()V

    .line 3032
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3033
    monitor-exit p0

    return-void

    .line 3030
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1776
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->close(Z)V

    .line 1777
    return-void
.end method

.method public close(Z)V
    .locals 1
    .param p1, "waitForMerges"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1805
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->shouldClose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1809
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v0, :cond_1

    .line 1810
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->rollbackInternal()V

    .line 1814
    :cond_0
    :goto_0
    return-void

    .line 1812
    :cond_1
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->closeInternal(Z)V

    goto :goto_0
.end method

.method public final commit()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3444
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->commit(Ljava/util/Map;)V

    .line 3445
    return-void
.end method

.method public final commit(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3458
    .local p1, "commitUserData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 3460
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->commitInternal(Ljava/util/Map;)V

    .line 3461
    return-void
.end method

.method public declared-synchronized deleteAll()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2921
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2925
    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->finishMerges(Z)V

    .line 2928
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 2931
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfos;->clear()V

    .line 2934
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 2935
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh()V

    .line 2938
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->dropAll()V

    .line 2941
    iget-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    .line 2942
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfos;->changed()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2946
    :try_start_2
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_0

    .line 2947
    const-string/jumbo v1, "hit exception during deleteAll"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2950
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2943
    :catch_0
    move-exception v0

    .line 2944
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_3
    const-string/jumbo v1, "deleteAll"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2946
    :try_start_4
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_0

    .line 2947
    const-string/jumbo v1, "hit exception during deleteAll"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2921
    .end local v0    # "oom":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 2946
    :catchall_1
    move-exception v1

    :try_start_5
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_1

    .line 2947
    const-string/jumbo v2, "hit exception during deleteAll"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2946
    :cond_1
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public deleteDocuments(Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2194
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2196
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lorg/apache/lucene/index/DocumentsWriter;->deleteTerm(Lorg/apache/lucene/index/Term;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2197
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2202
    :cond_0
    :goto_0
    return-void

    .line 2199
    :catch_0
    move-exception v0

    .line 2200
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    const-string/jumbo v1, "deleteDocuments(Term)"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deleteDocuments(Lorg/apache/lucene/search/Query;)V
    .locals 3
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2240
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2242
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/DocumentsWriter;->deleteQuery(Lorg/apache/lucene/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2243
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2248
    :cond_0
    :goto_0
    return-void

    .line 2245
    :catch_0
    move-exception v0

    .line 2246
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    const-string/jumbo v1, "deleteDocuments(Query)"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public varargs deleteDocuments([Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "terms"    # [Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2218
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2220
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/DocumentsWriter;->deleteTerms([Lorg/apache/lucene/index/Term;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2221
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2226
    :cond_0
    :goto_0
    return-void

    .line 2223
    :catch_0
    move-exception v0

    .line 2224
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    const-string/jumbo v1, "deleteDocuments(Term..)"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public varargs deleteDocuments([Lorg/apache/lucene/search/Query;)V
    .locals 3
    .param p1, "queries"    # [Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2264
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2266
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueries([Lorg/apache/lucene/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2267
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2272
    :cond_0
    :goto_0
    return-void

    .line 2269
    :catch_0
    move-exception v0

    .line 2270
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    const-string/jumbo v1, "deleteDocuments(Query..)"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method declared-synchronized deletePendingFiles()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4779
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter;->deletePendingFiles()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4780
    monitor-exit p0

    return-void

    .line 4779
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized deleteUnusedFiles()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4772
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 4773
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter;->deletePendingFiles()V

    .line 4774
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexFileDeleter;->revisitPolicy()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4775
    monitor-exit p0

    return-void

    .line 4772
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected doAfterFlush()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3296
    return-void
.end method

.method protected doBeforeFlush()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3302
    return-void
.end method

.method protected final ensureOpen()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 771
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 772
    return-void
.end method

.method protected final ensureOpen(Z)V
    .locals 2
    .param p1, "includePendingClose"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 765
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    if-eqz v0, :cond_1

    .line 766
    :cond_0
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string/jumbo v1, "this IndexWriter is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 768
    :cond_1
    return-void
.end method

.method public expungeDeletes()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2689
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->forceMergeDeletes()V

    .line 2690
    return-void
.end method

.method public expungeDeletes(Z)V
    .locals 0
    .param p1, "doWait"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2601
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->forceMergeDeletes(Z)V

    .line 2602
    return-void
.end method

.method protected final flush(ZZ)V
    .locals 1
    .param p1, "triggerMerge"    # Z
    .param p2, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3544
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 3545
    invoke-direct {p0, p2}, Lorg/apache/lucene/index/IndexWriter;->doFlush(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 3546
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->maybeMerge()V

    .line 3548
    :cond_0
    return-void
.end method

.method protected final flush(ZZZ)V
    .locals 0
    .param p1, "triggerMerge"    # Z
    .param p2, "flushDocStores"    # Z
    .param p3, "flushDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3524
    invoke-virtual {p0, p1, p3}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V

    .line 3525
    return-void
.end method

.method public forceMerge(I)V
    .locals 1
    .param p1, "maxNumSegments"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2485
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;->forceMerge(IZ)V

    .line 2486
    return-void
.end method

.method public forceMerge(IZ)V
    .locals 10
    .param p1, "maxNumSegments"    # I
    .param p2, "doWait"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 2499
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2501
    if-ge p1, v9, :cond_0

    .line 2502
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "maxNumSegments must be >= 1; got "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 2504
    :cond_0
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v7, :cond_1

    .line 2505
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "forceMerge: index now "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2506
    const-string/jumbo v7, "now flush at forceMerge"

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2509
    :cond_1
    invoke-virtual {p0, v9, v9}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V

    .line 2511
    monitor-enter p0

    .line 2512
    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->resetMergeExceptions()V

    .line 2513
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->clear()V

    .line 2514
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/SegmentInfo;

    .line 2515
    .local v3, "info":Lorg/apache/lucene/index/SegmentInfo;
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v7, v3, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2529
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :catchall_0
    move-exception v7

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .line 2517
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    iput p1, p0, Lorg/apache/lucene/index/IndexWriter;->mergeMaxNumSegments:I

    .line 2520
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 2521
    .local v4, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iput p1, v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    .line 2522
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    iget-object v8, v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2525
    .end local v4    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_3
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 2526
    .restart local v4    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iput p1, v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    .line 2527
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    iget-object v8, v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2529
    .end local v4    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2531
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->maybeMerge(I)V

    .line 2533
    if-eqz p2, :cond_a

    .line 2534
    monitor-enter p0

    .line 2537
    :goto_3
    :try_start_2
    iget-boolean v7, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v7, :cond_5

    .line 2538
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string/jumbo v8, "this writer hit an OutOfMemoryError; cannot complete forceMerge"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 2562
    :catchall_1
    move-exception v7

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v7

    .line 2541
    :cond_5
    :try_start_3
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergeExceptions:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_8

    .line 2544
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergeExceptions:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    .line 2545
    .local v5, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_4
    if-ge v1, v5, :cond_8

    .line 2546
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergeExceptions:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 2547
    .restart local v4    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iget v7, v4, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    const/4 v8, -0x1

    if-eq v7, v8, :cond_7

    .line 2548
    new-instance v0, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "background merge hit exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4, v8}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 2549
    .local v0, "err":Ljava/io/IOException;
    invoke-virtual {v4}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->getException()Ljava/lang/Throwable;

    move-result-object v6

    .line 2550
    .local v6, "t":Ljava/lang/Throwable;
    if-eqz v6, :cond_6

    .line 2551
    invoke-virtual {v0, v6}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 2552
    :cond_6
    throw v0

    .line 2545
    .end local v0    # "err":Ljava/io/IOException;
    .end local v6    # "t":Ljava/lang/Throwable;
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2557
    .end local v1    # "i":I
    .end local v4    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .end local v5    # "size":I
    :cond_8
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->maxNumSegmentsMergesPending()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 2558
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->doWait()V

    goto :goto_3

    .line 2562
    :cond_9
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2568
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2574
    :cond_a
    return-void
.end method

.method public forceMergeDeletes()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2716
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->forceMergeDeletes(Z)V

    .line 2717
    return-void
.end method

.method public forceMergeDeletes(Z)V
    .locals 9
    .param p1, "doWait"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 2621
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2623
    invoke-virtual {p0, v7, v7}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V

    .line 2625
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v7, :cond_0

    .line 2626
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "forceMergeDeletes: index now "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2630
    :cond_0
    monitor-enter p0

    .line 2631
    :try_start_0
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/index/MergePolicy;->findForcedDeletesMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    move-result-object v5

    .line 2632
    .local v5, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    if-eqz v5, :cond_1

    .line 2633
    iget-object v7, v5, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    .line 2634
    .local v3, "numMerges":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 2635
    iget-object v7, v5, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/IndexWriter;->registerMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)Z

    .line 2634
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2637
    .end local v0    # "i":I
    .end local v3    # "numMerges":I
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2639
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    invoke-virtual {v7, p0}, Lorg/apache/lucene/index/MergeScheduler;->merge(Lorg/apache/lucene/index/IndexWriter;)V

    .line 2641
    if-eqz v5, :cond_9

    if-eqz p1, :cond_9

    .line 2642
    iget-object v7, v5, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    .line 2643
    .restart local v3    # "numMerges":I
    monitor-enter p0

    .line 2644
    const/4 v4, 0x1

    .line 2645
    .local v4, "running":Z
    :cond_2
    :goto_1
    if-eqz v4, :cond_8

    .line 2647
    :try_start_1
    iget-boolean v7, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v7, :cond_3

    .line 2648
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string/jumbo v8, "this writer hit an OutOfMemoryError; cannot complete forceMergeDeletes"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 2671
    :catchall_0
    move-exception v7

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    .line 2637
    .end local v3    # "numMerges":I
    .end local v4    # "running":Z
    .end local v5    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :catchall_1
    move-exception v7

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v7

    .line 2654
    .restart local v3    # "numMerges":I
    .restart local v4    # "running":Z
    .restart local v5    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_3
    const/4 v4, 0x0

    .line 2655
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    if-ge v0, v3, :cond_7

    .line 2656
    :try_start_3
    iget-object v7, v5, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->merges:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 2657
    .local v2, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v7, v2}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v7, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2658
    :cond_4
    const/4 v4, 0x1

    .line 2659
    :cond_5
    invoke-virtual {v2}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->getException()Ljava/lang/Throwable;

    move-result-object v6

    .line 2660
    .local v6, "t":Ljava/lang/Throwable;
    if-eqz v6, :cond_6

    .line 2661
    new-instance v1, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "background merge hit exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v2, v8}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 2662
    .local v1, "ioe":Ljava/io/IOException;
    invoke-virtual {v1, v6}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 2663
    throw v1

    .line 2655
    .end local v1    # "ioe":Ljava/io/IOException;
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2668
    .end local v2    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .end local v6    # "t":Ljava/lang/Throwable;
    :cond_7
    if-eqz v4, :cond_2

    .line 2669
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->doWait()V

    goto :goto_1

    .line 2671
    .end local v0    # "i":I
    :cond_8
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2677
    .end local v3    # "numMerges":I
    .end local v4    # "running":Z
    :cond_9
    return-void
.end method

.method public getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;
    .locals 1

    .prologue
    .line 1915
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1916
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    return-object v0
.end method

.method final getBufferedDeleteTermsSize()I
    .locals 1

    .prologue
    .line 4403
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter;->getPendingDeletes()Lorg/apache/lucene/index/BufferedDeletes;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletes;->terms:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public getConfig()Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 1

    .prologue
    .line 1257
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 1258
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    return-object v0
.end method

.method public getDirectory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 1909
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 1910
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method final declared-synchronized getDocCount(I)I
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 2347
    monitor-enter p0

    if-ltz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2348
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v0

    iget v0, v0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2350
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 2347
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final getFlushCount()I
    .locals 1

    .prologue
    .line 2356
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->flushCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method final getFlushDeletesCount()I
    .locals 1

    .prologue
    .line 2361
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->flushDeletesCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public getInfoStream()Ljava/io/PrintStream;
    .locals 1

    .prologue
    .line 1678
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1679
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    return-object v0
.end method

.method getKeepFullyDeletedSegments()Z
    .locals 1

    .prologue
    .line 4478
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->keepFullyDeletedSegments:Z

    return v0
.end method

.method public getMaxBufferedDeleteTerms()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1592
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1593
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriterConfig;->getMaxBufferedDeleteTerms()I

    move-result v0

    return v0
.end method

.method public getMaxBufferedDocs()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1499
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1500
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriterConfig;->getMaxBufferedDocs()I

    move-result v0

    return v0
.end method

.method public getMaxFieldLength()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1412
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1413
    iget v0, p0, Lorg/apache/lucene/index/IndexWriter;->maxFieldLength:I

    return v0
.end method

.method public getMaxMergeDocs()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1370
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->getLogMergePolicy()Lorg/apache/lucene/index/LogMergePolicy;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/LogMergePolicy;->getMaxMergeDocs()I

    move-result v0

    return v0
.end method

.method public getMergeFactor()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1632
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->getLogMergePolicy()Lorg/apache/lucene/index/LogMergePolicy;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/LogMergePolicy;->getMergeFactor()I

    move-result v0

    return v0
.end method

.method public getMergePolicy()Lorg/apache/lucene/index/MergePolicy;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1292
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1293
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    return-object v0
.end method

.method public getMergeScheduler()Lorg/apache/lucene/index/MergeScheduler;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1326
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1327
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    return-object v0
.end method

.method public getMergedSegmentWarmer()Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 4710
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriterConfig;->getMergedSegmentWarmer()Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getMergingSegments()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2788
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNextMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .locals 2

    .prologue
    .line 2798
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 2799
    const/4 v0, 0x0

    .line 2804
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2802
    :cond_0
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .line 2803
    .local v0, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2798
    .end local v0    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method final getNumBufferedDeleteTerms()I
    .locals 1

    .prologue
    .line 4408
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter;->getPendingDeletes()Lorg/apache/lucene/index/BufferedDeletes;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletes;->numTermDeletes:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method final declared-synchronized getNumBufferedDocuments()I
    .locals 1

    .prologue
    .line 2342
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter;->getNumDocs()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPayloadProcessorProvider()Lorg/apache/lucene/index/PayloadProcessorProvider;
    .locals 1

    .prologue
    .line 4810
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 4811
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->payloadProcessorProvider:Lorg/apache/lucene/index/PayloadProcessorProvider;

    return-object v0
.end method

.method public getRAMBufferSizeMB()D
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1558
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v0

    return-wide v0
.end method

.method public getReader()Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 395
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriterConfig;->getReaderTermsIndexDivisor()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/index/IndexWriter;->getReader(IZ)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method public getReader(I)Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .param p1, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 428
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;->getReader(IZ)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method getReader(IZ)Lorg/apache/lucene/index/IndexReader;
    .locals 6
    .param p1, "termInfosIndexDivisor"    # I
    .param p2, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 432
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 434
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 436
    .local v2, "tStart":J
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_0

    .line 437
    const-string/jumbo v1, "flush at getReader"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 443
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/index/IndexWriter;->poolReaders:Z

    .line 449
    monitor-enter p0

    .line 450
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, v1, p2}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V

    .line 451
    new-instance v0, Lorg/apache/lucene/index/ReadOnlyDirectoryReader;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v0, p0, v1, p1, p2}, Lorg/apache/lucene/index/ReadOnlyDirectoryReader;-><init>(Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/SegmentInfos;IZ)V

    .line 452
    .local v0, "r":Lorg/apache/lucene/index/IndexReader;
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_1

    .line 453
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "return reader version="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->getVersion()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, " reader="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 455
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 457
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->maybeMerge()V

    .line 459
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_2

    .line 460
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "getReader took "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, " msec"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 462
    :cond_2
    return-object v0

    .line 455
    .end local v0    # "r":Lorg/apache/lucene/index/IndexReader;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method getReader(Z)Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .param p1, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 399
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriterConfig;->getReaderTermsIndexDivisor()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lorg/apache/lucene/index/IndexWriter;->getReader(IZ)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method public getReaderTermsIndexDivisor()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1435
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1436
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriterConfig;->getReaderTermsIndexDivisor()I

    move-result v0

    return v0
.end method

.method final declared-synchronized getSegmentCount()I
    .locals 1

    .prologue
    .line 2337
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSimilarity()Lorg/apache/lucene/search/Similarity;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 856
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 857
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->similarity:Lorg/apache/lucene/search/Similarity;

    return-object v0
.end method

.method public getTermIndexInterval()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 896
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 897
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriterConfig;->getTermIndexInterval()I

    move-result v0

    return v0
.end method

.method public getUseCompoundFile()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 812
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->getLogMergePolicy()Lorg/apache/lucene/index/LogMergePolicy;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/LogMergePolicy;->getUseCompoundFile()Z

    move-result v0

    return v0
.end method

.method public getWriteLockTimeout()J
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1708
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1709
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriter;->writeLockTimeout:J

    return-wide v0
.end method

.method public declared-synchronized hasDeletions()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1956
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1957
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v3}, Lorg/apache/lucene/index/BufferedDeletesStream;->any()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    .line 1968
    :cond_0
    :goto_0
    monitor-exit p0

    return v2

    .line 1960
    :cond_1
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocumentsWriter;->anyDeletions()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1963
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfo;

    .line 1964
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->hasDeletions()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .line 1968
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 1956
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method declared-synchronized isClosed()Z
    .locals 1

    .prologue
    .line 4743
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method keepFullyDeletedSegments()V
    .locals 1

    .prologue
    .line 4474
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexWriter;->keepFullyDeletedSegments:Z

    .line 4475
    return-void
.end method

.method public declared-synchronized maxDoc()I
    .locals 2

    .prologue
    .line 1924
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1926
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    if-eqz v1, :cond_0

    .line 1927
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriter;->getNumDocs()I

    move-result v0

    .line 1931
    .local v0, "count":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfos;->totalDocCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    add-int/2addr v0, v1

    .line 1932
    monitor-exit p0

    return v0

    .line 1929
    .end local v0    # "count":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "count":I
    goto :goto_0

    .line 1924
    .end local v0    # "count":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final maybeMerge()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2734
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->maybeMerge(I)V

    .line 2735
    return-void
.end method

.method public merge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 9
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, -0x1

    .line 3888
    const/4 v1, 0x0

    .line 3890
    .local v1, "success":Z
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 3896
    .local v4, "t0":J
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->mergeInit(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 3898
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_0

    .line 3899
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "now merge\n  merge="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p1, v6}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, "\n  index="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->segString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3901
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->mergeMiddle(Lorg/apache/lucene/index/MergePolicy$OneMerge;)I

    .line 3902
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->mergeSuccess(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3903
    const/4 v1, 0x1

    .line 3908
    :goto_0
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    .line 3909
    :try_start_2
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 3911
    if-nez v1, :cond_2

    .line 3912
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_1

    .line 3913
    const-string/jumbo v3, "hit exception during merge"

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3914
    :cond_1
    iget-object v3, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3, v6}, Lorg/apache/lucene/index/SegmentInfos;->contains(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 3915
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v6, v6, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V

    .line 3921
    :cond_2
    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isAborted()Z

    move-result v3

    if-nez v3, :cond_4

    iget v3, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    if-ne v3, v8, :cond_3

    iget-boolean v3, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    if-nez v3, :cond_4

    iget-boolean v3, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    if-nez v3, :cond_4

    .line 3922
    :cond_3
    iget v3, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    invoke-direct {p0, v3}, Lorg/apache/lucene/index/IndexWriter;->updatePendingMerges(I)V

    .line 3924
    :cond_4
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 3929
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_5

    iget-object v3, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    if-eqz v3, :cond_5

    .line 3930
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "merge time "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v4

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, " msec for "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget v6, v6, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, " docs"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3933
    :cond_5
    return-void

    .line 3904
    :catch_0
    move-exception v2

    .line 3905
    .local v2, "t":Ljava/lang/Throwable;
    :try_start_3
    invoke-direct {p0, v2, p1}, Lorg/apache/lucene/index/IndexWriter;->handleMergeException(Ljava/lang/Throwable;Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 3908
    .end local v2    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    :try_start_4
    monitor-enter p0
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_1

    .line 3909
    :try_start_5
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 3911
    if-nez v1, :cond_7

    .line 3912
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v6, :cond_6

    .line 3913
    const-string/jumbo v6, "hit exception during merge"

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3914
    :cond_6
    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    if-eqz v6, :cond_7

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/SegmentInfos;->contains(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 3915
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V

    .line 3921
    :cond_7
    if-eqz v1, :cond_9

    invoke-virtual {p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isAborted()Z

    move-result v6

    if-nez v6, :cond_9

    iget v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    if-ne v6, v8, :cond_8

    iget-boolean v6, p0, Lorg/apache/lucene/index/IndexWriter;->closed:Z

    if-nez v6, :cond_9

    iget-boolean v6, p0, Lorg/apache/lucene/index/IndexWriter;->closing:Z

    if-nez v6, :cond_9

    .line 3922
    :cond_8
    iget v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    invoke-direct {p0, v6}, Lorg/apache/lucene/index/IndexWriter;->updatePendingMerges(I)V

    .line 3924
    :cond_9
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 3908
    :try_start_6
    throw v3
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_1

    .line 3926
    :catch_1
    move-exception v0

    .line 3927
    .local v0, "oom":Ljava/lang/OutOfMemoryError;
    const-string/jumbo v3, "merge"

    invoke-direct {p0, v0, v3}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3924
    .end local v0    # "oom":Ljava/lang/OutOfMemoryError;
    :catchall_1
    move-exception v3

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v3
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_1

    :catchall_2
    move-exception v3

    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v3
    :try_end_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_1
.end method

.method final declared-synchronized mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 5
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4130
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 4134
    iget-boolean v3, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->registerDone:Z

    if-eqz v3, :cond_1

    .line 4135
    iget-object v2, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    .line 4136
    .local v2, "sourceSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfo;

    .line 4137
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfo;
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4130
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v2    # "sourceSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 4141
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "sourceSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    :cond_0
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    iget-object v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 4142
    const/4 v3, 0x0

    iput-boolean v3, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->registerDone:Z

    .line 4145
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "sourceSegments":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfo;>;"
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4146
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized mergeInit(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 3
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3999
    monitor-enter p0

    const/4 v0, 0x0

    .line 4001
    .local v0, "success":Z
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->_mergeInit(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4002
    const/4 v0, 0x1

    .line 4004
    if-nez v0, :cond_1

    .line 4005
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v1, :cond_0

    .line 4006
    const-string/jumbo v1, "hit exception in mergeInit"

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4008
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 4011
    :cond_1
    monitor-exit p0

    return-void

    .line 4004
    :catchall_0
    move-exception v1

    if-nez v0, :cond_3

    .line 4005
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_2

    .line 4006
    const-string/jumbo v2, "hit exception in mergeInit"

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 4008
    :cond_2
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->mergeFinish(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 4004
    :cond_3
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3999
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method mergeSuccess(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V
    .locals 0
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;

    .prologue
    .line 3937
    return-void
.end method

.method public message(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 780
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 781
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "IW "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/IndexWriter;->messageID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 782
    :cond_0
    return-void
.end method

.method final newSegmentName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2367
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    monitor-enter v1

    .line 2373
    :try_start_0
    iget-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    .line 2374
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->changed()V

    .line 2375
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget v3, v2, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    add-int/lit8 v4, v3, 0x1

    iput v4, v2, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    const/16 v2, 0x24

    invoke-static {v3, v2}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 2376
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method declared-synchronized newestSegment()Lorg/apache/lucene/index/SegmentInfo;
    .locals 2

    .prologue
    .line 4413
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized nrtIsCurrent(Lorg/apache/lucene/index/SegmentInfos;)Z
    .locals 4
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;

    .prologue
    .line 4738
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 4739
    iget-wide v0, p1, Lorg/apache/lucene/index/SegmentInfos;->version:J

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-wide v2, v2, Lorg/apache/lucene/index/SegmentInfos;->version:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter;->anyChanges()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v0}, Lorg/apache/lucene/index/BufferedDeletesStream;->any()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4738
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public numDeletedDocs(Lorg/apache/lucene/index/SegmentInfo;)I
    .locals 3
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 743
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 744
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->getIfExists(Lorg/apache/lucene/index/SegmentInfo;)Lorg/apache/lucene/index/SegmentReader;

    move-result-object v0

    .line 746
    .local v0, "reader":Lorg/apache/lucene/index/SegmentReader;
    if-eqz v0, :cond_1

    .line 747
    :try_start_0
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->numDeletedDocs()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 752
    if-eqz v0, :cond_0

    .line 753
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    :goto_0
    invoke-virtual {v2, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/SegmentReader;)Z

    .line 749
    :cond_0
    return v1

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 752
    if-eqz v0, :cond_0

    .line 753
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    goto :goto_0

    .line 752
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_2

    .line 753
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/SegmentReader;)Z

    .line 752
    :cond_2
    throw v1
.end method

.method public declared-synchronized numDocs()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1942
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1944
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    if-eqz v3, :cond_0

    .line 1945
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocumentsWriter;->getNumDocs()I

    move-result v0

    .line 1949
    .local v0, "count":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentInfo;

    .line 1950
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfo;
    iget v3, v2, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfo;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    sub-int/2addr v3, v4

    add-int/2addr v0, v3

    goto :goto_1

    .line 1947
    .end local v0    # "count":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "count":I
    goto :goto_0

    .line 1952
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    monitor-exit p0

    return v0

    .line 1942
    .end local v0    # "count":I
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public final declared-synchronized numRamDocs()I
    .locals 1

    .prologue
    .line 3669
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 3670
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter;->getNumDocs()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 3669
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public optimize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2393
    invoke-virtual {p0, v0, v0}, Lorg/apache/lucene/index/IndexWriter;->forceMerge(IZ)V

    .line 2394
    return-void
.end method

.method public optimize(I)V
    .locals 1
    .param p1, "maxNumSegments"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2405
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/IndexWriter;->forceMerge(IZ)V

    .line 2406
    return-void
.end method

.method public optimize(Z)V
    .locals 1
    .param p1, "doWait"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2417
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lorg/apache/lucene/index/IndexWriter;->forceMerge(IZ)V

    .line 2418
    return-void
.end method

.method public final prepareCommit()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3312
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 3313
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->prepareCommit(Ljava/util/Map;)V

    .line 3314
    return-void
.end method

.method public final prepareCommit(Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "commitUserData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 3348
    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 3350
    iget-boolean v6, p0, Lorg/apache/lucene/index/IndexWriter;->hitOOM:Z

    if-eqz v6, :cond_0

    .line 3351
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string/jumbo v7, "this writer hit an OutOfMemoryError; cannot commit"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 3355
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommit:Lorg/apache/lucene/index/SegmentInfos;

    if-eqz v6, :cond_1

    .line 3356
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string/jumbo v7, "prepareCommit was already called with no corresponding call to commit"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 3359
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v6, :cond_2

    .line 3360
    const-string/jumbo v6, "prepareCommit: flush"

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3362
    :cond_2
    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 3363
    const/4 v2, 0x0

    .line 3364
    .local v2, "anySegmentsFlushed":Z
    const/4 v5, 0x0

    .line 3365
    .local v5, "toCommit":Lorg/apache/lucene/index/SegmentInfos;
    const/4 v4, 0x0

    .line 3368
    .local v4, "success":Z
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 3369
    const/4 v6, 0x1

    :try_start_1
    invoke-direct {p0, v6}, Lorg/apache/lucene/index/IndexWriter;->doFlush(Z)Z

    move-result v2

    .line 3370
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->commit(Lorg/apache/lucene/index/SegmentInfos;)V

    .line 3371
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfos;->clone()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lorg/apache/lucene/index/SegmentInfos;

    move-object v5, v0

    .line 3372
    iget-wide v6, p0, Lorg/apache/lucene/index/IndexWriter;->changeCount:J

    iput-wide v6, p0, Lorg/apache/lucene/index/IndexWriter;->pendingCommitChangeCount:J

    .line 3378
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 3379
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexFileDeleter;->incRef(Ljava/util/Collection;)V

    .line 3380
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3381
    const/4 v4, 0x1

    .line 3383
    if-nez v4, :cond_3

    :try_start_2
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v6, :cond_3

    .line 3384
    const-string/jumbo v6, "hit exception during prepareCommit"

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3386
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->doAfterFlush()V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    .line 3392
    :goto_0
    const/4 v4, 0x0

    .line 3394
    if-eqz v2, :cond_4

    .line 3395
    :try_start_3
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->maybeMerge()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 3397
    :cond_4
    const/4 v4, 0x1

    .line 3399
    if-nez v4, :cond_5

    .line 3400
    monitor-enter p0

    .line 3401
    :try_start_4
    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 3402
    const/4 v6, 0x0

    iput-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 3403
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 3407
    :cond_5
    invoke-direct {p0, v5, p1}, Lorg/apache/lucene/index/IndexWriter;->startCommit(Lorg/apache/lucene/index/SegmentInfos;Ljava/util/Map;)V

    .line 3408
    return-void

    .line 3380
    :catchall_0
    move-exception v6

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 3383
    :catchall_1
    move-exception v6

    if-nez v4, :cond_6

    :try_start_7
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v7, :cond_6

    .line 3384
    const-string/jumbo v7, "hit exception during prepareCommit"

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3386
    :cond_6
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->doAfterFlush()V

    .line 3383
    throw v6
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_0

    .line 3388
    :catch_0
    move-exception v3

    .line 3389
    .local v3, "oom":Ljava/lang/OutOfMemoryError;
    const-string/jumbo v6, "prepareCommit"

    invoke-direct {p0, v3, v6}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0

    .line 3399
    .end local v3    # "oom":Ljava/lang/OutOfMemoryError;
    :catchall_2
    move-exception v6

    if-nez v4, :cond_7

    .line 3400
    monitor-enter p0

    .line 3401
    :try_start_8
    iget-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v8, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/index/IndexFileDeleter;->decRef(Ljava/util/Collection;)V

    .line 3402
    const/4 v7, 0x0

    iput-object v7, p0, Lorg/apache/lucene/index/IndexWriter;->filesToCommit:Ljava/util/Collection;

    .line 3403
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 3399
    :cond_7
    throw v6

    .line 3403
    :catchall_3
    move-exception v6

    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v6

    :catchall_4
    move-exception v6

    :try_start_a
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    throw v6
.end method

.method public final ramSizeInBytes()J
    .locals 4

    .prologue
    .line 3662
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 3663
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed()J

    move-result-wide v0

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed()J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method final declared-synchronized registerMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)Z
    .locals 7
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 3947
    monitor-enter p0

    :try_start_0
    iget-boolean v5, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->registerDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v5, :cond_0

    .line 3993
    :goto_0
    monitor-exit p0

    return v3

    .line 3950
    :cond_0
    :try_start_1
    iget-boolean v5, p0, Lorg/apache/lucene/index/IndexWriter;->stopMerges:Z

    if-eqz v5, :cond_1

    .line 3951
    invoke-virtual {p1}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->abort()V

    .line 3952
    new-instance v3, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "merge is aborted: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p1, v5}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/MergePolicy$MergeAbortedException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3947
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 3955
    :cond_1
    const/4 v2, 0x0

    .line 3956
    .local v2, "isExternal":Z
    :try_start_2
    iget-object v5, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfo;

    .line 3957
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfo;
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v3, v4

    .line 3958
    goto :goto_0

    .line 3960
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v5, v1}, Lorg/apache/lucene/index/SegmentInfos;->contains(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v5

    if-nez v5, :cond_4

    move v3, v4

    .line 3961
    goto :goto_0

    .line 3963
    :cond_4
    iget-object v5, v1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v6, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    if-eq v5, v6, :cond_5

    .line 3964
    const/4 v2, 0x1

    .line 3966
    :cond_5
    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->segmentsToMerge:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3967
    iget v5, p0, Lorg/apache/lucene/index/IndexWriter;->mergeMaxNumSegments:I

    iput v5, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->maxNumSegments:I

    goto :goto_1

    .line 3971
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_6
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->ensureValidMerge(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 3973
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v4, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 3975
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_7

    .line 3976
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "add merge to pendingMerges: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p1, v5}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " [total "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " pending]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3978
    :cond_7
    iget-wide v4, p0, Lorg/apache/lucene/index/IndexWriter;->mergeGen:J

    iput-wide v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->mergeGen:J

    .line 3979
    iput-boolean v2, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->isExternal:Z

    .line 3985
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "registerMerge merging="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3986
    iget-object v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfo;

    .line 3987
    .restart local v1    # "info":Lorg/apache/lucene/index/SegmentInfo;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "registerMerge info="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3988
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 3992
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_8
    const/4 v4, 0x1

    iput-boolean v4, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->registerDone:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public rollback()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2820
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2823
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->shouldClose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2824
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->rollbackInternal()V

    .line 2825
    :cond_0
    return-void
.end method

.method public declared-synchronized segString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4418
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized segString(Ljava/lang/Iterable;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4423
    .local p1, "infos":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/index/SegmentInfo;>;"
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4424
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentInfo;

    .line 4425
    .local v2, "s":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 4426
    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 4428
    :cond_0
    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexWriter;->segString(Lorg/apache/lucene/index/SegmentInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4423
    .end local v0    # "buffer":Ljava/lang/StringBuilder;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "s":Lorg/apache/lucene/index/SegmentInfo;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 4430
    .restart local v0    # "buffer":Ljava/lang/StringBuilder;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    monitor-exit p0

    return-object v3
.end method

.method public declared-synchronized segString(Lorg/apache/lucene/index/SegmentInfo;)Ljava/lang/String;
    .locals 4
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4435
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4436
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->getIfExists(Lorg/apache/lucene/index/SegmentInfo;)Lorg/apache/lucene/index/SegmentReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 4438
    .local v1, "reader":Lorg/apache/lucene/index/SegmentReader;
    if-eqz v1, :cond_2

    .line 4439
    :try_start_1
    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentReader;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4447
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 4448
    :try_start_2
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/SegmentReader;)Z

    .line 4451
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v2

    monitor-exit p0

    return-object v2

    .line 4441
    :cond_2
    :try_start_3
    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Lorg/apache/lucene/index/SegmentInfo;->toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4442
    iget-object v2, p1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;

    if-eq v2, v3, :cond_0

    .line 4443
    const-string/jumbo v2, "**"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 4447
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_3

    .line 4448
    :try_start_4
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    invoke-virtual {v3, v1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/SegmentReader;)Z

    .line 4447
    :cond_3
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 4435
    .end local v0    # "buffer":Ljava/lang/StringBuilder;
    .end local v1    # "reader":Lorg/apache/lucene/index/SegmentReader;
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public setInfoStream(Ljava/io/PrintStream;)V
    .locals 1
    .param p1, "infoStream"    # Ljava/io/PrintStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1657
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1658
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    .line 1659
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/DocumentsWriter;->setInfoStream(Ljava/io/PrintStream;)V

    .line 1660
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexFileDeleter;->setInfoStream(Ljava/io/PrintStream;)V

    .line 1661
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/BufferedDeletesStream;->setInfoStream(Ljava/io/PrintStream;)V

    .line 1662
    if-eqz p1, :cond_0

    .line 1663
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->messageState()V

    .line 1664
    :cond_0
    return-void
.end method

.method public setMaxBufferedDeleteTerms(I)V
    .locals 2
    .param p1, "maxBufferedDeleteTerms"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1576
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1577
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 1578
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setMaxBufferedDeleteTerms "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1581
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setMaxBufferedDeleteTerms(I)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 1582
    return-void
.end method

.method public setMaxBufferedDocs(I)V
    .locals 2
    .param p1, "maxBufferedDocs"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1461
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1462
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->pushMaxBufferedDocs()V

    .line 1463
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 1464
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setMaxBufferedDocs "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1468
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setMaxBufferedDocs(I)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 1469
    return-void
.end method

.method public setMaxFieldLength(I)V
    .locals 2
    .param p1, "maxFieldLength"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1397
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1398
    iput p1, p0, Lorg/apache/lucene/index/IndexWriter;->maxFieldLength:I

    .line 1399
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/DocumentsWriter;->setMaxFieldLength(I)V

    .line 1400
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 1401
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setMaxFieldLength "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1402
    :cond_0
    return-void
.end method

.method public setMaxMergeDocs(I)V
    .locals 1
    .param p1, "maxMergeDocs"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1353
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->getLogMergePolicy()Lorg/apache/lucene/index/LogMergePolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/LogMergePolicy;->setMaxMergeDocs(I)V

    .line 1354
    return-void
.end method

.method public setMergeFactor(I)V
    .locals 1
    .param p1, "mergeFactor"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1614
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->getLogMergePolicy()Lorg/apache/lucene/index/LogMergePolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/LogMergePolicy;->setMergeFactor(I)V

    .line 1615
    return-void
.end method

.method public setMergePolicy(Lorg/apache/lucene/index/MergePolicy;)V
    .locals 2
    .param p1, "mp"    # Lorg/apache/lucene/index/MergePolicy;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1268
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1269
    if-nez p1, :cond_0

    .line 1270
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "MergePolicy must be non-null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1272
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    if-eq v0, p1, :cond_1

    .line 1273
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MergePolicy;->close()V

    .line 1274
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    .line 1275
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/MergePolicy;->setIndexWriter(Lorg/apache/lucene/index/IndexWriter;)V

    .line 1276
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->pushMaxBufferedDocs()V

    .line 1277
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_2

    .line 1278
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setMergePolicy "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1281
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setMergePolicy(Lorg/apache/lucene/index/MergePolicy;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 1282
    return-void
.end method

.method public declared-synchronized setMergeScheduler(Lorg/apache/lucene/index/MergeScheduler;)V
    .locals 2
    .param p1, "mergeScheduler"    # Lorg/apache/lucene/index/MergeScheduler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1302
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1303
    if-nez p1, :cond_0

    .line 1304
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "MergeScheduler must be non-null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1302
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1306
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    if-eq v0, p1, :cond_1

    .line 1307
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->finishMerges(Z)V

    .line 1308
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MergeScheduler;->close()V

    .line 1310
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriter;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    .line 1311
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_2

    .line 1312
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setMergeScheduler "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1315
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setMergeScheduler(Lorg/apache/lucene/index/MergeScheduler;)Lorg/apache/lucene/index/IndexWriterConfig;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1316
    monitor-exit p0

    return-void
.end method

.method public setMergedSegmentWarmer(Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;)V
    .locals 1
    .param p1, "warmer"    # Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 4700
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setMergedSegmentWarmer(Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 4701
    return-void
.end method

.method public setPayloadProcessorProvider(Lorg/apache/lucene/index/PayloadProcessorProvider;)V
    .locals 0
    .param p1, "pcp"    # Lorg/apache/lucene/index/PayloadProcessorProvider;

    .prologue
    .line 4801
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 4802
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriter;->payloadProcessorProvider:Lorg/apache/lucene/index/PayloadProcessorProvider;

    .line 4803
    return-void
.end method

.method public setRAMBufferSizeMB(D)V
    .locals 3
    .param p1, "mb"    # D
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1544
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 1545
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setRAMBufferSizeMB "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1549
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexWriterConfig;->setRAMBufferSizeMB(D)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 1550
    return-void
.end method

.method public setReaderTermsIndexDivisor(I)V
    .locals 2
    .param p1, "divisor"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1422
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1423
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setReaderTermsIndexDivisor(I)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 1424
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 1425
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setReaderTermsIndexDivisor "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 1427
    :cond_0
    return-void
.end method

.method public setSimilarity(Lorg/apache/lucene/search/Similarity;)V
    .locals 1
    .param p1, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 841
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 842
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriter;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 843
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/DocumentsWriter;->setSimilarity(Lorg/apache/lucene/search/Similarity;)V

    .line 846
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setSimilarity(Lorg/apache/lucene/search/Similarity;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 847
    return-void
.end method

.method public setTermIndexInterval(I)V
    .locals 1
    .param p1, "interval"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 884
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 885
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriterConfig;->setTermIndexInterval(I)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 886
    return-void
.end method

.method public setUseCompoundFile(Z)V
    .locals 1
    .param p1, "value"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 831
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->getLogMergePolicy()Lorg/apache/lucene/index/LogMergePolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/LogMergePolicy;->setUseCompoundFile(Z)V

    .line 832
    return-void
.end method

.method public setWriteLockTimeout(J)V
    .locals 1
    .param p1, "writeLockTimeout"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1694
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 1695
    iput-wide p1, p0, Lorg/apache/lucene/index/IndexWriter;->writeLockTimeout:J

    .line 1698
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexWriterConfig;->setWriteLockTimeout(J)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 1699
    return-void
.end method

.method testPoint(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 4733
    const/4 v0, 0x1

    return v0
.end method

.method public updateDocument(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/document/Document;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "doc"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2292
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2293
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/index/IndexWriter;->updateDocument(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/document/Document;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 2294
    return-void
.end method

.method public updateDocument(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/document/Document;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 5
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "doc"    # Lorg/apache/lucene/document/Document;
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2316
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2318
    const/4 v0, 0x0

    .line 2319
    .local v0, "doFlush":Z
    const/4 v2, 0x0

    .line 2321
    .local v2, "success":Z
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v3, p2, p3, p1}, Lorg/apache/lucene/index/DocumentsWriter;->updateDocument(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/Term;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2322
    const/4 v2, 0x1

    .line 2324
    if-nez v2, :cond_0

    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_0

    .line 2325
    const-string/jumbo v3, "hit exception updating document"

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2327
    :cond_0
    if-eqz v0, :cond_1

    .line 2328
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V

    .line 2333
    :cond_1
    :goto_0
    return-void

    .line 2324
    :catchall_0
    move-exception v3

    if-nez v2, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_2

    .line 2325
    const-string/jumbo v4, "hit exception updating document"

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2324
    :cond_2
    throw v3
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    .line 2330
    :catch_0
    move-exception v1

    .line 2331
    .local v1, "oom":Ljava/lang/OutOfMemoryError;
    const-string/jumbo v3, "updateDocument"

    invoke-direct {p0, v1, v3}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateDocuments(Lorg/apache/lucene/index/Term;Ljava/util/Collection;)V
    .locals 1
    .param p1, "delTerm"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2143
    .local p2, "docs":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/document/Document;>;"
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/index/IndexWriter;->updateDocuments(Lorg/apache/lucene/index/Term;Ljava/util/Collection;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 2144
    return-void
.end method

.method public updateDocuments(Lorg/apache/lucene/index/Term;Ljava/util/Collection;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 5
    .param p1, "delTerm"    # Lorg/apache/lucene/index/Term;
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2162
    .local p2, "docs":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/document/Document;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen()V

    .line 2164
    const/4 v2, 0x0

    .line 2165
    .local v2, "success":Z
    const/4 v0, 0x0

    .line 2167
    .local v0, "doFlush":Z
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v3, p2, p3, p1}, Lorg/apache/lucene/index/DocumentsWriter;->updateDocuments(Ljava/util/Collection;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/Term;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2168
    const/4 v2, 0x1

    .line 2170
    if-nez v2, :cond_0

    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_0

    .line 2171
    const-string/jumbo v3, "hit exception updating document"

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2174
    :cond_0
    if-eqz v0, :cond_1

    .line 2175
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lorg/apache/lucene/index/IndexWriter;->flush(ZZ)V

    .line 2180
    :cond_1
    :goto_0
    return-void

    .line 2170
    :catchall_0
    move-exception v3

    if-nez v2, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_2

    .line 2171
    const-string/jumbo v4, "hit exception updating document"

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 2170
    :cond_2
    throw v3
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    .line 2177
    :catch_0
    move-exception v1

    .line 2178
    .local v1, "oom":Ljava/lang/OutOfMemoryError;
    const-string/jumbo v3, "updateDocuments"

    invoke-direct {p0, v1, v3}, Lorg/apache/lucene/index/IndexWriter;->handleOOM(Ljava/lang/OutOfMemoryError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public verbose()Z
    .locals 1

    .prologue
    .line 1684
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized waitForMerges()V
    .locals 1

    .prologue
    .line 3008
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->ensureOpen(Z)V

    .line 3009
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 3010
    const-string/jumbo v0, "waitForMerges"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 3012
    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->pendingMerges:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->runningMerges:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 3013
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexWriter;->doWait()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3008
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 3017
    :cond_2
    :try_start_1
    sget-boolean v0, Lorg/apache/lucene/index/IndexWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->mergingSegments:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 3019
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_4

    .line 3020
    const-string/jumbo v0, "waitForMerges done"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3022
    :cond_4
    monitor-exit p0

    return-void
.end method
