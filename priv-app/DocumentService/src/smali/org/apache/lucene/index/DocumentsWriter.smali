.class final Lorg/apache/lucene/index/DocumentsWriter;
.super Ljava/lang/Object;
.source "DocumentsWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;,
        Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;,
        Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;,
        Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;,
        Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;,
        Lorg/apache/lucene/index/DocumentsWriter$DocWriter;,
        Lorg/apache/lucene/index/DocumentsWriter$DocState;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final BYTE_BLOCK_MASK:I = 0x7fff

.field static final BYTE_BLOCK_NOT_MASK:I = -0x8000

.field static final BYTE_BLOCK_SHIFT:I = 0xf

.field static final BYTE_BLOCK_SIZE:I = 0x8000

.field static final CHAR_BLOCK_MASK:I = 0x3fff

.field static final CHAR_BLOCK_SHIFT:I = 0xe

.field static final CHAR_BLOCK_SIZE:I = 0x4000

.field static final INT_BLOCK_MASK:I = 0x1fff

.field static final INT_BLOCK_SHIFT:I = 0xd

.field static final INT_BLOCK_SIZE:I = 0x2000

.field static final MAX_TERM_LENGTH:I = 0x3fff

.field static final PER_DOC_BLOCK_SIZE:I = 0x400

.field static final defaultIndexingChain:Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;


# instance fields
.field private aborting:Z

.field bufferIsFull:Z

.field private final bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

.field byteBlockAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

.field final bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

.field private closed:Z

.field private final config:Lorg/apache/lucene/index/IndexWriterConfig;

.field final consumer:Lorg/apache/lucene/index/DocConsumer;

.field directory:Lorg/apache/lucene/store/Directory;

.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private final flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

.field private freeCharBlocks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[C>;"
        }
    .end annotation
.end field

.field private freeIntBlocks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation
.end field

.field infoStream:Ljava/io/PrintStream;

.field maxFieldLength:I

.field private final maxThreadStates:I

.field private nextDocID:I

.field nf:Ljava/text/NumberFormat;

.field private numDocs:I

.field private pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

.field final perDocAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

.field segment:Ljava/lang/String;

.field similarity:Lorg/apache/lucene/search/Similarity;

.field final skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

.field private final threadBindings:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Thread;",
            "Lorg/apache/lucene/index/DocumentsWriterThreadState;",
            ">;"
        }
    .end annotation
.end field

.field private threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

.field final waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

.field writer:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    const-class v0, Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    .line 224
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriter$1;

    invoke-direct {v0}, Lorg/apache/lucene/index/DocumentsWriter$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/DocumentsWriter;->defaultIndexingChain:Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;

    return-void

    .line 110
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/IndexWriterConfig;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/BufferedDeletesStream;)V
    .locals 4
    .param p1, "config"    # Lorg/apache/lucene/index/IndexWriterConfig;
    .param p2, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p3, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p4, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p5, "bufferedDeletesStream"    # Lorg/apache/lucene/index/BufferedDeletesStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    .line 122
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/lucene/index/DocumentsWriterThreadState;

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    .line 123
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadBindings:Ljava/util/HashMap;

    .line 129
    sget v0, Lorg/apache/lucene/index/IndexWriter;->DEFAULT_MAX_FIELD_LENGTH:I

    iput v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->maxFieldLength:I

    .line 137
    new-instance v0, Lorg/apache/lucene/index/BufferedDeletes;

    invoke-direct {v0}, Lorg/apache/lucene/index/BufferedDeletes;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    .line 1082
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;-><init>(Lorg/apache/lucene/index/DocumentsWriter$1;)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

    .line 1084
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->nf:Ljava/text/NumberFormat;

    .line 1147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->freeIntBlocks:Ljava/util/List;

    .line 1178
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    const v1, 0x8000

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;-><init>(Lorg/apache/lucene/index/DocumentsWriter;I)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->byteBlockAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    .line 1182
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    const/16 v1, 0x400

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;-><init>(Lorg/apache/lucene/index/DocumentsWriter;I)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->perDocAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    .line 1193
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->freeCharBlocks:Ljava/util/ArrayList;

    .line 1343
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;-><init>(Lorg/apache/lucene/index/DocumentsWriter;)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    .line 273
    iput-object p2, p0, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 274
    iput-object p3, p0, Lorg/apache/lucene/index/DocumentsWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 275
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 276
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getMaxThreadStates()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->maxThreadStates:I

    .line 277
    iput-object p4, p0, Lorg/apache/lucene/index/DocumentsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 278
    iput-object p5, p0, Lorg/apache/lucene/index/DocumentsWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    .line 279
    iget-object v0, p3, Lorg/apache/lucene/index/IndexWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    .line 281
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getIndexingChain()Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;->getChain(Lorg/apache/lucene/index/DocumentsWriter;)Lorg/apache/lucene/index/DocConsumer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->consumer:Lorg/apache/lucene/index/DocConsumer;

    .line 282
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    .line 283
    return-void
.end method

.method static synthetic access$100(Lorg/apache/lucene/index/DocumentsWriter;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/DocumentsWriter;

    .prologue
    .line 110
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    return-object v0
.end method

.method private declared-synchronized allThreadsIdle()Z
    .locals 2

    .prologue
    .line 464
    monitor-enter p0

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 465
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    aget-object v1, v1, v0

    iget-boolean v1, v1, Lorg/apache/lucene/index/DocumentsWriterThreadState;->isIdle:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 466
    const/4 v1, 0x0

    .line 469
    :goto_1
    monitor-exit p0

    return v1

    .line 464
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 469
    :cond_1
    const/4 v1, 0x1

    goto :goto_1

    .line 464
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private doAfterFlush()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 451
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriter;->allThreadsIdle()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 452
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadBindings:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 453
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->reset()V

    .line 454
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->segment:Ljava/lang/String;

    .line 455
    iput v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    .line 456
    iput v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->nextDocID:I

    .line 457
    iput-boolean v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->bufferIsFull:Z

    .line 458
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 459
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterThreadState;->doAfterFlush()V

    .line 458
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 461
    :cond_1
    return-void
.end method

.method private finishDocument(Lorg/apache/lucene/index/DocumentsWriterThreadState;Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)V
    .locals 3
    .param p1, "perThread"    # Lorg/apache/lucene/index/DocumentsWriterThreadState;
    .param p2, "docWriter"    # Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1013
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriter;->balanceRAM()V

    .line 1015
    monitor-enter p0

    .line 1017
    :try_start_0
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-eqz p2, :cond_0

    iget v1, p2, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->docID:I

    iget-object v2, p1, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1057
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1019
    :cond_0
    :try_start_1
    iget-boolean v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_2

    .line 1025
    if-eqz p2, :cond_1

    .line 1027
    :try_start_2
    invoke-virtual {p2}, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->abort()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1032
    :cond_1
    :goto_0
    const/4 v1, 0x1

    :try_start_3
    iput-boolean v1, p1, Lorg/apache/lucene/index/DocumentsWriterThreadState;->isIdle:Z

    .line 1035
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1037
    monitor-exit p0

    .line 1058
    :goto_1
    return-void

    .line 1042
    :cond_2
    if-eqz p2, :cond_4

    .line 1043
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    invoke-virtual {v1, p2}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->add(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)Z

    move-result v0

    .line 1049
    .local v0, "doPause":Z
    :goto_2
    if-eqz v0, :cond_3

    .line 1050
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriter;->waitForWaitQueue()V

    .line 1053
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p1, Lorg/apache/lucene/index/DocumentsWriterThreadState;->isIdle:Z

    .line 1056
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1057
    monitor-exit p0

    goto :goto_1

    .line 1045
    .end local v0    # "doPause":Z
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

    iget-object v2, p1, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iput v2, v1, Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;->docID:I

    .line 1046
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->add(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    .restart local v0    # "doPause":Z
    goto :goto_2

    .line 1028
    .end local v0    # "doPause":Z
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private pushDeletes(Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/SegmentInfos;)V
    .locals 6
    .param p1, "newSegment"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;

    .prologue
    .line 483
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v3}, Lorg/apache/lucene/index/BufferedDeletesStream;->getNextGen()J

    move-result-wide v0

    .line 484
    .local v0, "delGen":J
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v3}, Lorg/apache/lucene/index/BufferedDeletes;->any()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 485
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v3

    if-gtz v3, :cond_0

    if-eqz p1, :cond_5

    .line 486
    :cond_0
    new-instance v2, Lorg/apache/lucene/index/FrozenBufferedDeletes;

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-direct {v2, v3, v0, v1}, Lorg/apache/lucene/index/FrozenBufferedDeletes;-><init>(Lorg/apache/lucene/index/BufferedDeletes;J)V

    .line 487
    .local v2, "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_1

    .line 488
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "flush: push buffered deletes startSize="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v4, v4, Lorg/apache/lucene/index/BufferedDeletes;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " frozenSize="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v2, Lorg/apache/lucene/index/FrozenBufferedDeletes;->bytesUsed:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 490
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/index/BufferedDeletesStream;->push(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    .line 491
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_2

    .line 492
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "flush: delGen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v2, Lorg/apache/lucene/index/FrozenBufferedDeletes;->gen:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 494
    :cond_2
    if-eqz p1, :cond_3

    .line 495
    iget-wide v4, v2, Lorg/apache/lucene/index/FrozenBufferedDeletes;->gen:J

    invoke-virtual {p1, v4, v5}, Lorg/apache/lucene/index/SegmentInfo;->setBufferedDeletesGen(J)V

    .line 505
    .end local v2    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    :cond_3
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v3}, Lorg/apache/lucene/index/BufferedDeletes;->clear()V

    .line 509
    :cond_4
    :goto_1
    return-void

    .line 498
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_3

    .line 499
    const-string/jumbo v3, "flush: drop buffered deletes: no segments"

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    goto :goto_0

    .line 506
    :cond_6
    if-eqz p1, :cond_4

    .line 507
    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/index/SegmentInfo;->setBufferedDeletesGen(J)V

    goto :goto_1
.end method


# virtual methods
.method declared-synchronized abort()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 394
    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v5, :cond_0

    .line 395
    const-string/jumbo v5, "docWriter: abort"

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 398
    :cond_0
    const/4 v3, 0x0

    .line 404
    .local v3, "success":Z
    :try_start_1
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->abort()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 411
    :goto_0
    :try_start_2
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriter;->waitIdle()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 413
    :try_start_3
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v5, :cond_1

    .line 414
    const-string/jumbo v5, "docWriter: abort waitIdle done"

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 417
    :cond_1
    sget-boolean v5, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_8

    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    iget v5, v5, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->numWaiting:I

    if-eqz v5, :cond_8

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "waitQueue.numWaiting="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    iget v7, v7, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->numWaiting:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 440
    :catchall_0
    move-exception v5

    const/4 v6, 0x0

    :try_start_4
    iput-boolean v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    .line 441
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 442
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v6, :cond_2

    .line 443
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "docWriter: done abort; success="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 440
    :cond_2
    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 394
    .end local v3    # "success":Z
    :catchall_1
    move-exception v5

    monitor-exit p0

    throw v5

    .line 430
    .local v0, "arr$":[Lorg/apache/lucene/index/DocumentsWriterThreadState;
    .local v1, "i$":I
    .local v2, "len$":I
    .restart local v3    # "success":Z
    :cond_3
    :try_start_5
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->consumer:Lorg/apache/lucene/index/DocConsumer;

    invoke-virtual {v6}, Lorg/apache/lucene/index/DocConsumer;->abort()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 435
    :goto_1
    :try_start_6
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriter;->doAfterFlush()V

    .line 413
    throw v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 430
    :cond_4
    :try_start_7
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->consumer:Lorg/apache/lucene/index/DocConsumer;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocConsumer;->abort()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 435
    :goto_2
    :try_start_8
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriter;->doAfterFlush()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 438
    const/4 v3, 0x1

    .line 440
    const/4 v5, 0x0

    :try_start_9
    iput-boolean v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    .line 441
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 442
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v5, :cond_5

    .line 443
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "docWriter: done abort; success="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 446
    :cond_5
    monitor-exit p0

    return-void

    .line 413
    .end local v0    # "arr$":[Lorg/apache/lucene/index/DocumentsWriterThreadState;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    :catchall_2
    move-exception v5

    :try_start_a
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v6, :cond_6

    .line 414
    const-string/jumbo v6, "docWriter: abort waitIdle done"

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 417
    :cond_6
    sget-boolean v6, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_7

    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    iget v6, v6, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->numWaiting:I

    if-eqz v6, :cond_7

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "waitQueue.numWaiting="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    iget v7, v7, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->numWaiting:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 418
    :cond_7
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    const-wide/16 v8, 0x0

    iput-wide v8, v6, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waitingBytes:J

    .line 420
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v6}, Lorg/apache/lucene/index/BufferedDeletes;->clear()V

    .line 422
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    .restart local v0    # "arr$":[Lorg/apache/lucene/index/DocumentsWriterThreadState;
    array-length v2, v0

    .restart local v2    # "len$":I
    const/4 v1, 0x0

    .restart local v1    # "i$":I
    :goto_3
    if-ge v1, v2, :cond_3

    aget-object v4, v0, v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 424
    .local v4, "threadState":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    :try_start_b
    iget-object v6, v4, Lorg/apache/lucene/index/DocumentsWriterThreadState;->consumer:Lorg/apache/lucene/index/DocConsumerPerThread;

    invoke-virtual {v6}, Lorg/apache/lucene/index/DocConsumerPerThread;->abort()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 422
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 418
    .end local v0    # "arr$":[Lorg/apache/lucene/index/DocumentsWriterThreadState;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v4    # "threadState":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    :cond_8
    :try_start_c
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    const-wide/16 v6, 0x0

    iput-wide v6, v5, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waitingBytes:J

    .line 420
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v5}, Lorg/apache/lucene/index/BufferedDeletes;->clear()V

    .line 422
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    .restart local v0    # "arr$":[Lorg/apache/lucene/index/DocumentsWriterThreadState;
    array-length v2, v0

    .restart local v2    # "len$":I
    const/4 v1, 0x0

    .restart local v1    # "i$":I
    :goto_5
    if-ge v1, v2, :cond_4

    aget-object v4, v0, v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 424
    .restart local v4    # "threadState":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    :try_start_d
    iget-object v5, v4, Lorg/apache/lucene/index/DocumentsWriterThreadState;->consumer:Lorg/apache/lucene/index/DocConsumerPerThread;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocConsumerPerThread;->abort()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 422
    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 425
    :catch_0
    move-exception v5

    goto :goto_6

    .line 431
    .end local v4    # "threadState":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    :catch_1
    move-exception v5

    goto/16 :goto_2

    .line 425
    .restart local v4    # "threadState":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    :catch_2
    move-exception v6

    goto :goto_4

    .line 431
    .end local v4    # "threadState":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    :catch_3
    move-exception v6

    goto/16 :goto_1

    .line 405
    .end local v0    # "arr$":[Lorg/apache/lucene/index/DocumentsWriterThreadState;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    :catch_4
    move-exception v5

    goto/16 :goto_0
.end method

.method addDocument(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/analysis/Analyzer;)Z
    .locals 1
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 744
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/index/DocumentsWriter;->updateDocument(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/Term;)Z

    move-result v0

    return v0
.end method

.method declared-synchronized anyChanges()Z
    .locals 1

    .prologue
    .line 473
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v0}, Lorg/apache/lucene/index/BufferedDeletes;->any()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public anyDeletions()Z
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v0}, Lorg/apache/lucene/index/BufferedDeletes;->any()Z

    move-result v0

    return v0
.end method

.method balanceRAM()V
    .locals 24

    .prologue
    .line 1241
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->bufferedDeletesStream:Lorg/apache/lucene/index/BufferedDeletesStream;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/BufferedDeletesStream;->bytesUsed()J

    move-result-wide v6

    .line 1244
    .local v6, "deletesRAMUsed":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->config:Lorg/apache/lucene/index/IndexWriterConfig;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/IndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v12

    .line 1245
    .local v12, "mb":D
    const-wide/high16 v18, -0x4010000000000000L    # -1.0

    cmpl-double v18, v12, v18

    if-nez v18, :cond_2

    .line 1246
    const-wide/16 v14, -0x1

    .line 1251
    .local v14, "ramBufferSize":J
    :goto_0
    monitor-enter p0

    .line 1252
    const-wide/16 v18, -0x1

    cmp-long v18, v14, v18

    if-eqz v18, :cond_0

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->bufferIsFull:Z

    move/from16 v18, v0

    if-eqz v18, :cond_3

    .line 1253
    :cond_0
    monitor-exit p0

    .line 1341
    :cond_1
    :goto_1
    return-void

    .line 1248
    .end local v14    # "ramBufferSize":J
    :cond_2
    const-wide/high16 v18, 0x4090000000000000L    # 1024.0

    mul-double v18, v18, v12

    const-wide/high16 v20, 0x4090000000000000L    # 1024.0

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-long v14, v0

    .restart local v14    # "ramBufferSize":J
    goto :goto_0

    .line 1256
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed()J

    move-result-wide v18

    add-long v18, v18, v6

    cmp-long v18, v18, v14

    if-ltz v18, :cond_7

    const/4 v5, 0x1

    .line 1257
    .local v5, "doBalance":Z
    :goto_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1259
    if-eqz v5, :cond_1

    .line 1261
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    .line 1262
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "  RAM: balance allocations: usedMB="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed()J

    move-result-wide v20

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/DocumentsWriter;->toMB(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, " vs trigger="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lorg/apache/lucene/index/DocumentsWriter;->toMB(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, " deletesMB="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lorg/apache/lucene/index/DocumentsWriter;->toMB(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, " byteBlockFree="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->byteBlockAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    const v20, 0x8000

    mul-int v19, v19, v20

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/DocumentsWriter;->toMB(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, " perDocFree="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->perDocAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    mul-int/lit16 v0, v0, 0x400

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/DocumentsWriter;->toMB(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, " charBlockFree="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->freeCharBlocks:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    mul-int/lit16 v0, v0, 0x4000

    move/from16 v19, v0

    mul-int/lit8 v19, v19, 0x2

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/DocumentsWriter;->toMB(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 1270
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed()J

    move-result-wide v18

    add-long v16, v18, v6

    .line 1272
    .local v16, "startBytesUsed":J
    const/4 v11, 0x0

    .line 1278
    .local v11, "iter":I
    const/4 v4, 0x1

    .line 1280
    .local v4, "any":Z
    const-wide v18, 0x3fee666666666666L    # 0.95

    long-to-double v0, v14

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-long v8, v0

    .line 1282
    .local v8, "freeLevel":J
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed()J

    move-result-wide v18

    add-long v18, v18, v6

    cmp-long v18, v18, v8

    if-lez v18, :cond_6

    .line 1284
    monitor-enter p0

    .line 1285
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->perDocAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-nez v18, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->byteBlockAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-nez v18, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->freeCharBlocks:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-nez v18, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->freeIntBlocks:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v18

    if-nez v18, :cond_a

    if-nez v4, :cond_a

    .line 1291
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed()J

    move-result-wide v18

    add-long v18, v18, v6

    cmp-long v18, v18, v14

    if-lez v18, :cond_8

    const/16 v18, 0x1

    :goto_4
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/lucene/index/DocumentsWriter;->bufferIsFull:Z

    .line 1292
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    .line 1293
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed()J

    move-result-wide v18

    add-long v18, v18, v6

    cmp-long v18, v18, v14

    if-lez v18, :cond_9

    .line 1294
    const-string/jumbo v18, "    nothing to free; set bufferIsFull"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 1299
    :cond_5
    :goto_5
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1337
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    .line 1338
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "    after free: freedMB="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->nf:Ljava/text/NumberFormat;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed()J

    move-result-wide v20

    sub-long v20, v16, v20

    sub-long v20, v20, v6

    move-wide/from16 v0, v20

    long-to-double v0, v0

    move-wide/from16 v20, v0

    const-wide/high16 v22, 0x4090000000000000L    # 1024.0

    div-double v20, v20, v22

    const-wide/high16 v22, 0x4090000000000000L    # 1024.0

    div-double v20, v20, v22

    invoke-virtual/range {v19 .. v21}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, " usedMB="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->nf:Ljava/text/NumberFormat;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed()J

    move-result-wide v20

    add-long v20, v20, v6

    move-wide/from16 v0, v20

    long-to-double v0, v0

    move-wide/from16 v20, v0

    const-wide/high16 v22, 0x4090000000000000L    # 1024.0

    div-double v20, v20, v22

    const-wide/high16 v22, 0x4090000000000000L    # 1024.0

    div-double v20, v20, v22

    invoke-virtual/range {v19 .. v21}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1256
    .end local v4    # "any":Z
    .end local v5    # "doBalance":Z
    .end local v8    # "freeLevel":J
    .end local v11    # "iter":I
    .end local v16    # "startBytesUsed":J
    :cond_7
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 1257
    :catchall_0
    move-exception v18

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v18

    .line 1291
    .restart local v4    # "any":Z
    .restart local v5    # "doBalance":Z
    .restart local v8    # "freeLevel":J
    .restart local v11    # "iter":I
    .restart local v16    # "startBytesUsed":J
    :cond_8
    const/16 v18, 0x0

    goto/16 :goto_4

    .line 1296
    :cond_9
    :try_start_3
    const-string/jumbo v18, "    nothing to free"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1327
    :catchall_1
    move-exception v18

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v18

    .line 1302
    :cond_a
    :try_start_4
    rem-int/lit8 v18, v11, 0x5

    if-nez v18, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->byteBlockAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-lez v18, :cond_b

    .line 1303
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->byteBlockAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->byteBlockAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1304
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v18, v0

    const-wide/16 v20, -0x8000

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 1307
    :cond_b
    const/16 v18, 0x1

    rem-int/lit8 v19, v11, 0x5

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->freeCharBlocks:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-lez v18, :cond_c

    .line 1308
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->freeCharBlocks:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->freeCharBlocks:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1309
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v18, v0

    const-wide/16 v20, -0x8000

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 1312
    :cond_c
    const/16 v18, 0x2

    rem-int/lit8 v19, v11, 0x5

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->freeIntBlocks:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v18

    if-lez v18, :cond_d

    .line 1313
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->freeIntBlocks:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->freeIntBlocks:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1314
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v18, v0

    const-wide/16 v20, -0x8000

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 1317
    :cond_d
    const/16 v18, 0x3

    rem-int/lit8 v19, v11, 0x5

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->perDocAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-lez v18, :cond_e

    .line 1319
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_6
    const/16 v18, 0x20

    move/from16 v0, v18

    if-ge v10, v0, :cond_e

    .line 1320
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->perDocAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->perDocAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1321
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v18, v0

    const-wide/16 v20, -0x400

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 1322
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->perDocAllocator:Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$ByteBlockAllocator;->freeByteBlocks:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-nez v18, :cond_10

    .line 1327
    .end local v10    # "i":I
    :cond_e
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1329
    const/16 v18, 0x4

    rem-int/lit8 v19, v11, 0x5

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_f

    if-eqz v4, :cond_f

    .line 1331
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->consumer:Lorg/apache/lucene/index/DocConsumer;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/DocConsumer;->freeRAM()Z

    move-result v4

    .line 1334
    :cond_f
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_3

    .line 1319
    .restart local v10    # "i":I
    :cond_10
    add-int/lit8 v10, v10, 0x1

    goto :goto_6
.end method

.method bytesUsed()J
    .locals 4

    .prologue
    .line 1167
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v2, v2, Lorg/apache/lucene/index/BufferedDeletes;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method declared-synchronized bytesUsed(J)V
    .locals 1
    .param p1, "numBytes"    # J

    .prologue
    .line 1163
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1164
    monitor-exit p0

    return-void

    .line 1163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized close()V
    .locals 1

    .prologue
    .line 675
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->closed:Z

    .line 676
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 677
    monitor-exit p0

    return-void

    .line 675
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized deleteDocID(I)V
    .locals 1
    .param p1, "docIDUpto"    # I

    .prologue
    .line 288
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/BufferedDeletes;->addDocID(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    monitor-exit p0

    return-void

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method varargs deleteQueries([Lorg/apache/lucene/search/Query;)Z
    .locals 8
    .param p1, "queries"    # [Lorg/apache/lucene/search/Query;

    .prologue
    .line 301
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    const/4 v6, 0x0

    array-length v7, p1

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->waitUpdate(II)Z

    move-result v1

    .line 302
    .local v1, "doFlush":Z
    monitor-enter p0

    .line 303
    move-object v0, p1

    .local v0, "arr$":[Lorg/apache/lucene/search/Query;
    :try_start_0
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 304
    .local v4, "query":Lorg/apache/lucene/search/Query;
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    invoke-virtual {v5, v4, v6}, Lorg/apache/lucene/index/BufferedDeletes;->addQuery(Lorg/apache/lucene/search/Query;I)V

    .line 303
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 306
    .end local v4    # "query":Lorg/apache/lucene/search/Query;
    :cond_0
    monitor-exit p0

    .line 307
    return v1

    .line 306
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method deleteQuery(Lorg/apache/lucene/search/Query;)Z
    .locals 4
    .param p1, "query"    # Lorg/apache/lucene/search/Query;

    .prologue
    .line 311
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->waitUpdate(II)Z

    move-result v0

    .line 312
    .local v0, "doFlush":Z
    monitor-enter p0

    .line 313
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    invoke-virtual {v1, p1, v2}, Lorg/apache/lucene/index/BufferedDeletes;->addQuery(Lorg/apache/lucene/search/Query;I)V

    .line 314
    monitor-exit p0

    .line 315
    return v0

    .line 314
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method deleteTerm(Lorg/apache/lucene/index/Term;Z)Z
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "skipWait"    # Z

    .prologue
    .line 332
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, p2}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->waitUpdate(IIZ)Z

    move-result v0

    .line 333
    .local v0, "doFlush":Z
    monitor-enter p0

    .line 334
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    invoke-virtual {v1, p1, v2}, Lorg/apache/lucene/index/BufferedDeletes;->addTerm(Lorg/apache/lucene/index/Term;I)V

    .line 335
    monitor-exit p0

    .line 336
    return v0

    .line 335
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method varargs deleteTerms([Lorg/apache/lucene/index/Term;)Z
    .locals 8
    .param p1, "terms"    # [Lorg/apache/lucene/index/Term;

    .prologue
    .line 319
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    const/4 v6, 0x0

    array-length v7, p1

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->waitUpdate(II)Z

    move-result v1

    .line 320
    .local v1, "doFlush":Z
    monitor-enter p0

    .line 321
    move-object v0, p1

    .local v0, "arr$":[Lorg/apache/lucene/index/Term;
    :try_start_0
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 322
    .local v4, "term":Lorg/apache/lucene/index/Term;
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    invoke-virtual {v5, v4, v6}, Lorg/apache/lucene/index/BufferedDeletes;->addTerm(Lorg/apache/lucene/index/Term;I)V

    .line 321
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 324
    .end local v4    # "term":Lorg/apache/lucene/index/Term;
    :cond_0
    monitor-exit p0

    .line 325
    return v1

    .line 324
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method declared-synchronized flush(Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/IndexFileDeleter;Lorg/apache/lucene/index/MergePolicy;Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/SegmentInfo;
    .locals 32
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "deleter"    # Lorg/apache/lucene/index/IndexFileDeleter;
    .param p3, "mergePolicy"    # Lorg/apache/lucene/index/MergePolicy;
    .param p4, "segmentInfos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 519
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    .line 522
    .local v26, "startTime":J
    sget-boolean v4, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    invoke-static/range {p1 .. p1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 519
    .end local v26    # "startTime":J
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 524
    .restart local v26    # "startTime":J
    :cond_0
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->waitIdle()V

    .line 526
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    if-nez v4, :cond_3

    .line 528
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_1

    .line 529
    const-string/jumbo v4, "flush: no docs; skipping"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 532
    :cond_1
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/index/DocumentsWriter;->pushDeletes(Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/SegmentInfos;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 533
    const/4 v3, 0x0

    .line 671
    :cond_2
    :goto_0
    monitor-exit p0

    return-object v3

    .line 536
    :cond_3
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    if-eqz v4, :cond_5

    .line 537
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_4

    .line 538
    const-string/jumbo v4, "flush: skip because aborting is set"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 540
    :cond_4
    const/4 v3, 0x0

    goto :goto_0

    .line 543
    :cond_5
    const/16 v28, 0x0

    .line 549
    .local v28, "success":Z
    :try_start_3
    sget-boolean v4, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_8

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->nextDocID:I

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    if-eq v4, v5, :cond_8

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "nextDocID="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/index/DocumentsWriter;->nextDocID:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " numDocs="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 654
    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notifyAll()V

    .line 655
    if-nez v28, :cond_7

    .line 656
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->segment:Ljava/lang/String;

    if-eqz v5, :cond_6

    .line 657
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->segment:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V

    .line 659
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 654
    :cond_7
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 550
    :cond_8
    :try_start_5
    sget-boolean v4, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    iget v4, v4, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->numWaiting:I

    if-eqz v4, :cond_9

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "numWaiting="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    iget v6, v6, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->numWaiting:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 551
    :cond_9
    sget-boolean v4, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    iget-wide v4, v4, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->waitingBytes:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_a

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 553
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_b

    .line 554
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "flush postings as segment "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->segment:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " numDocs="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 557
    :cond_b
    new-instance v2, Lorg/apache/lucene/index/SegmentWriteState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->segment:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v0, p0

    iget v7, v0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/IndexWriter;->getConfig()Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/lucene/index/IndexWriterConfig;->getTermIndexInterval()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-direct/range {v2 .. v9}, Lorg/apache/lucene/index/SegmentWriteState;-><init>(Ljava/io/PrintStream;Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;IILorg/apache/lucene/index/BufferedDeletes;)V

    .line 563
    .local v2, "flushState":Lorg/apache/lucene/index/SegmentWriteState;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v4, v4, Lorg/apache/lucene/index/BufferedDeletes;->docIDs:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_d

    .line 564
    new-instance v4, Lorg/apache/lucene/util/BitVector;

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/BitVector;-><init>(I)V

    iput-object v4, v2, Lorg/apache/lucene/index/SegmentWriteState;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    .line 565
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v4, v4, Lorg/apache/lucene/index/BufferedDeletes;->docIDs:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 566
    .local v15, "delDocID":I
    iget-object v4, v2, Lorg/apache/lucene/index/SegmentWriteState;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v4, v15}, Lorg/apache/lucene/util/BitVector;->set(I)V

    goto :goto_1

    .line 568
    .end local v15    # "delDocID":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v4, v4, Lorg/apache/lucene/index/BufferedDeletes;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v5, v5, Lorg/apache/lucene/index/BufferedDeletes;->docIDs:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    neg-int v5, v5

    sget v6, Lorg/apache/lucene/index/BufferedDeletes;->BYTES_PER_DEL_DOCID:I

    mul-int/2addr v5, v6

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 569
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v4, v4, Lorg/apache/lucene/index/BufferedDeletes;->docIDs:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 572
    .end local v18    # "i$":Ljava/util/Iterator;
    :cond_d
    new-instance v3, Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->segment:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/lucene/index/DocumentsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v9}, Lorg/apache/lucene/index/FieldInfos;->hasProx()Z

    move-result v9

    const/4 v10, 0x0

    invoke-direct/range {v3 .. v10}, Lorg/apache/lucene/index/SegmentInfo;-><init>(Ljava/lang/String;ILorg/apache/lucene/store/Directory;ZZZZ)V

    .line 574
    .local v3, "newSegment":Lorg/apache/lucene/index/SegmentInfo;
    new-instance v31, Ljava/util/HashSet;

    invoke-direct/range {v31 .. v31}, Ljava/util/HashSet;-><init>()V

    .line 575
    .local v31, "threads":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/DocConsumerPerThread;>;"
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    .local v11, "arr$":[Lorg/apache/lucene/index/DocumentsWriterThreadState;
    array-length v0, v11

    move/from16 v19, v0

    .local v19, "len$":I
    const/16 v18, 0x0

    .local v18, "i$":I
    :goto_2
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_e

    aget-object v30, v11, v18

    .line 576
    .local v30, "threadState":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    move-object/from16 v0, v30

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriterThreadState;->consumer:Lorg/apache/lucene/index/DocConsumerPerThread;

    move-object/from16 v0, v31

    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 575
    add-int/lit8 v18, v18, 0x1

    goto :goto_2

    .line 579
    .end local v30    # "threadState":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed()J

    move-result-wide v4

    long-to-double v4, v4

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    div-double v24, v4, v6

    .line 581
    .local v24, "startMBUsed":D
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->consumer:Lorg/apache/lucene/index/DocConsumer;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0, v2}, Lorg/apache/lucene/index/DocConsumer;->flush(Ljava/util/Collection;Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 583
    iget-boolean v4, v2, Lorg/apache/lucene/index/SegmentWriteState;->hasVectors:Z

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/SegmentInfo;->setHasVectors(Z)V

    .line 585
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_10

    .line 586
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "new segment has "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v4, v2, Lorg/apache/lucene/index/SegmentWriteState;->hasVectors:Z

    if-eqz v4, :cond_12

    const-string/jumbo v4, "vectors"

    :goto_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 587
    iget-object v4, v2, Lorg/apache/lucene/index/SegmentWriteState;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v4, :cond_f

    .line 588
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "new segment has "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lorg/apache/lucene/index/SegmentWriteState;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v5}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " deleted docs"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 590
    :cond_f
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "flushedFiles="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 593
    :cond_10
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v3}, Lorg/apache/lucene/index/MergePolicy;->useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 594
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->segment:Ljava/lang/String;

    const-string/jumbo v5, "cfs"

    invoke-static {v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 596
    .local v12, "cfsFileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_11

    .line 597
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "flush: create compound file \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 600
    :cond_11
    new-instance v13, Lorg/apache/lucene/index/CompoundFileWriter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-direct {v13, v4, v12}, Lorg/apache/lucene/index/CompoundFileWriter;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    .line 601
    .local v13, "cfsWriter":Lorg/apache/lucene/index/CompoundFileWriter;
    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_13

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 602
    .local v17, "fileName":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/apache/lucene/index/CompoundFileWriter;->addFile(Ljava/lang/String;)V

    goto :goto_4

    .line 586
    .end local v12    # "cfsFileName":Ljava/lang/String;
    .end local v13    # "cfsWriter":Lorg/apache/lucene/index/CompoundFileWriter;
    .end local v17    # "fileName":Ljava/lang/String;
    .local v18, "i$":I
    :cond_12
    const-string/jumbo v4, "no vectors"

    goto/16 :goto_3

    .line 604
    .restart local v12    # "cfsFileName":Ljava/lang/String;
    .restart local v13    # "cfsWriter":Lorg/apache/lucene/index/CompoundFileWriter;
    .local v18, "i$":Ljava/util/Iterator;
    :cond_13
    invoke-virtual {v13}, Lorg/apache/lucene/index/CompoundFileWriter;->close()V

    .line 605
    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/IndexFileDeleter;->deleteNewFiles(Ljava/util/Collection;)V

    .line 606
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/SegmentInfo;->setUseCompoundFile(Z)V

    .line 611
    .end local v12    # "cfsFileName":Ljava/lang/String;
    .end local v13    # "cfsWriter":Lorg/apache/lucene/index/CompoundFileWriter;
    .end local v18    # "i$":Ljava/util/Iterator;
    :cond_14
    iget-object v4, v2, Lorg/apache/lucene/index/SegmentWriteState;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v4, :cond_17

    .line 612
    iget-object v4, v2, Lorg/apache/lucene/index/SegmentWriteState;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v4}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v14

    .line 613
    .local v14, "delCount":I
    sget-boolean v4, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_15

    if-gtz v14, :cond_15

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 614
    :cond_15
    invoke-virtual {v3, v14}, Lorg/apache/lucene/index/SegmentInfo;->setDelCount(I)V

    .line 615
    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->advanceDelGen()V

    .line 616
    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDelFileName()Ljava/lang/String;

    move-result-object v16

    .line 617
    .local v16, "delFileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_16

    .line 618
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "flush: write "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " deletes to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 620
    :cond_16
    const/16 v29, 0x0

    .line 627
    .local v29, "success2":Z
    :try_start_6
    iget-object v4, v2, Lorg/apache/lucene/index/SegmentWriteState;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, v16

    invoke-virtual {v4, v5, v0}, Lorg/apache/lucene/util/BitVector;->write(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 628
    const/16 v29, 0x1

    .line 630
    if-nez v29, :cond_17

    .line 632
    :try_start_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 641
    .end local v14    # "delCount":I
    .end local v16    # "delFileName":Ljava/lang/String;
    .end local v29    # "success2":Z
    :cond_17
    :goto_5
    :try_start_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_18

    .line 642
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "flush: segment="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 643
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytes(Z)J

    move-result-wide v4

    long-to-double v4, v4

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    div-double v22, v4, v6

    .line 644
    .local v22, "newSegmentSizeNoStore":D
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytes(Z)J

    move-result-wide v4

    long-to-double v4, v4

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    div-double v20, v4, v6

    .line 645
    .local v20, "newSegmentSize":D
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "  ramUsed="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->nf:Ljava/text/NumberFormat;

    move-wide/from16 v0, v24

    invoke-virtual {v5, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " MB"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " newFlushedSize="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->nf:Ljava/text/NumberFormat;

    move-wide/from16 v0, v20

    invoke-virtual {v5, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " MB"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->nf:Ljava/text/NumberFormat;

    move-wide/from16 v0, v22

    invoke-virtual {v5, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " MB w/o doc stores)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " docs/MB="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->nf:Ljava/text/NumberFormat;

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    int-to-double v6, v6

    div-double v6, v6, v20

    invoke-virtual {v5, v6, v7}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " new/old="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->nf:Ljava/text/NumberFormat;

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double v6, v6, v22

    div-double v6, v6, v24

    invoke-virtual {v5, v6, v7}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 652
    .end local v20    # "newSegmentSize":D
    .end local v22    # "newSegmentSizeNoStore":D
    :cond_18
    const/16 v28, 0x1

    .line 654
    :try_start_9
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notifyAll()V

    .line 655
    if-nez v28, :cond_1a

    .line 656
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->segment:Ljava/lang/String;

    if-eqz v4, :cond_19

    .line 657
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->segment:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/IndexFileDeleter;->refresh(Ljava/lang/String;)V

    .line 659
    :cond_19
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 663
    :cond_1a
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->doAfterFlush()V

    .line 666
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v3, v1}, Lorg/apache/lucene/index/DocumentsWriter;->pushDeletes(Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/SegmentInfos;)V

    .line 667
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_2

    .line 668
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "flush time "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v6, v6, v26

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " msec"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 630
    .restart local v14    # "delCount":I
    .restart local v16    # "delFileName":Ljava/lang/String;
    .restart local v29    # "success2":Z
    :catchall_2
    move-exception v4

    if-nez v29, :cond_1b

    .line 632
    :try_start_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 630
    :cond_1b
    :goto_6
    :try_start_b
    throw v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 633
    :catch_0
    move-exception v4

    goto/16 :goto_5

    :catch_1
    move-exception v5

    goto :goto_6
.end method

.method declared-synchronized getCharBlock()[C
    .locals 6

    .prologue
    .line 1197
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->freeCharBlocks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 1199
    .local v1, "size":I
    if-nez v1, :cond_0

    .line 1200
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/32 v4, 0x8000

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 1201
    const/16 v2, 0x4000

    new-array v0, v2, [C
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1208
    .local v0, "c":[C
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1203
    .end local v0    # "c":[C
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->freeCharBlocks:Ljava/util/ArrayList;

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [C
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v0    # "c":[C
    goto :goto_0

    .line 1197
    .end local v0    # "c":[C
    .end local v1    # "size":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public getFieldInfos()Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    return-object v0
.end method

.method declared-synchronized getIntBlock()[I
    .locals 6

    .prologue
    .line 1151
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->freeIntBlocks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 1153
    .local v1, "size":I
    if-nez v1, :cond_0

    .line 1154
    const/16 v2, 0x2000

    new-array v0, v2, [I

    .line 1155
    .local v0, "b":[I
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/32 v4, 0x8000

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1159
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1157
    .end local v0    # "b":[I
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->freeIntBlocks:Ljava/util/List;

    add-int/lit8 v3, v1, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v0    # "b":[I
    goto :goto_0

    .line 1151
    .end local v0    # "b":[I
    .end local v1    # "size":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method declared-synchronized getNumDocs()I
    .locals 1

    .prologue
    .line 373
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPendingDeletes()Lorg/apache/lucene/index/BufferedDeletes;
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    return-object v0
.end method

.method declared-synchronized getSegment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 368
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->segment:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized getThreadState(Lorg/apache/lucene/index/Term;I)Lorg/apache/lucene/index/DocumentsWriterThreadState;
    .locals 10
    .param p1, "delTerm"    # Lorg/apache/lucene/index/Term;
    .param p2, "docCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 686
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 687
    .local v0, "currentThread":Ljava/lang/Thread;
    sget-boolean v6, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-static {v6}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 686
    .end local v0    # "currentThread":Ljava/lang/Thread;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 692
    .restart local v0    # "currentThread":Ljava/lang/Thread;
    :cond_0
    :try_start_1
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadBindings:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/DocumentsWriterThreadState;

    .line 693
    .local v4, "state":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    if-nez v4, :cond_5

    .line 697
    const/4 v2, 0x0

    .line 698
    .local v2, "minThreadState":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    array-length v6, v6

    if-ge v1, v6, :cond_3

    .line 699
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    aget-object v5, v6, v1

    .line 700
    .local v5, "ts":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    if-eqz v2, :cond_1

    iget v6, v5, Lorg/apache/lucene/index/DocumentsWriterThreadState;->numThreads:I

    iget v7, v2, Lorg/apache/lucene/index/DocumentsWriterThreadState;->numThreads:I

    if-ge v6, v7, :cond_2

    .line 701
    :cond_1
    move-object v2, v5

    .line 698
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 704
    .end local v5    # "ts":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    :cond_3
    if-eqz v2, :cond_6

    iget v6, v2, Lorg/apache/lucene/index/DocumentsWriterThreadState;->numThreads:I

    if-eqz v6, :cond_4

    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    array-length v6, v6

    iget v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->maxThreadStates:I

    if-lt v6, v7, :cond_6

    .line 705
    :cond_4
    move-object v4, v2

    .line 706
    iget v6, v4, Lorg/apache/lucene/index/DocumentsWriterThreadState;->numThreads:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v4, Lorg/apache/lucene/index/DocumentsWriterThreadState;->numThreads:I

    .line 716
    :goto_1
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadBindings:Ljava/util/HashMap;

    invoke-virtual {v6, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 722
    .end local v1    # "i":I
    .end local v2    # "minThreadState":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    :cond_5
    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/DocumentsWriter;->waitReady(Lorg/apache/lucene/index/DocumentsWriterThreadState;)V

    .line 726
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->segment:Ljava/lang/String;

    if-nez v6, :cond_8

    .line 727
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexWriter;->newSegmentName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->segment:Ljava/lang/String;

    .line 728
    sget-boolean v6, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_8

    iget v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    if-eqz v6, :cond_8

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 709
    .restart local v1    # "i":I
    .restart local v2    # "minThreadState":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    :cond_6
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    array-length v6, v6

    add-int/lit8 v6, v6, 0x1

    new-array v3, v6, [Lorg/apache/lucene/index/DocumentsWriterThreadState;

    .line 710
    .local v3, "newArray":[Lorg/apache/lucene/index/DocumentsWriterThreadState;
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    array-length v6, v6

    if-lez v6, :cond_7

    .line 711
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-object v9, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    array-length v9, v9

    invoke-static {v6, v7, v3, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 713
    :cond_7
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    array-length v6, v6

    new-instance v4, Lorg/apache/lucene/index/DocumentsWriterThreadState;

    .end local v4    # "state":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    invoke-direct {v4, p0}, Lorg/apache/lucene/index/DocumentsWriterThreadState;-><init>(Lorg/apache/lucene/index/DocumentsWriter;)V

    aput-object v4, v3, v6

    .line 714
    .restart local v4    # "state":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    iput-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    goto :goto_1

    .line 731
    .end local v1    # "i":I
    .end local v2    # "minThreadState":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    .end local v3    # "newArray":[Lorg/apache/lucene/index/DocumentsWriterThreadState;
    :cond_8
    iget-object v6, v4, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->nextDocID:I

    iput v7, v6, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    .line 732
    iget v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->nextDocID:I

    add-int/2addr v6, p2

    iput v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->nextDocID:I

    .line 734
    if-eqz p1, :cond_9

    .line 735
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v7, v4, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v7, v7, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    invoke-virtual {v6, p1, v7}, Lorg/apache/lucene/index/BufferedDeletes;->addTerm(Lorg/apache/lucene/index/Term;I)V

    .line 738
    :cond_9
    iget v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    add-int/2addr v6, p2

    iput v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocs:I

    .line 739
    const/4 v6, 0x0

    iput-boolean v6, v4, Lorg/apache/lucene/index/DocumentsWriterThreadState;->isIdle:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 740
    monitor-exit p0

    return-object v4
.end method

.method message(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 377
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "DW: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->message(Ljava/lang/String;)V

    .line 380
    :cond_0
    return-void
.end method

.method newPerDocBuffer()Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;
    .locals 1

    .prologue
    .line 181
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/DocumentsWriter$PerDocBuffer;-><init>(Lorg/apache/lucene/index/DocumentsWriter;)V

    return-object v0
.end method

.method declared-synchronized recycleCharBlocks([[CI)V
    .locals 3
    .param p1, "blocks"    # [[C
    .param p2, "numBlocks"    # I

    .prologue
    .line 1213
    monitor-enter p0

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 1214
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->freeCharBlocks:Ljava/util/ArrayList;

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1215
    const/4 v1, 0x0

    aput-object v1, p1, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1213
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1217
    :cond_0
    monitor-exit p0

    return-void

    .line 1213
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method declared-synchronized recycleIntBlocks([[III)V
    .locals 3
    .param p1, "blocks"    # [[I
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 1172
    monitor-enter p0

    move v0, p2

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_0

    .line 1173
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->freeIntBlocks:Ljava/util/List;

    aget-object v2, p1, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1174
    const/4 v1, 0x0

    aput-object v1, p1, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1172
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1176
    :cond_0
    monitor-exit p0

    return-void

    .line 1172
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method declared-synchronized setAborting()V
    .locals 1

    .prologue
    .line 383
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 384
    const-string/jumbo v0, "setAborting"

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 386
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 387
    monitor-exit p0

    return-void

    .line 383
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized setInfoStream(Ljava/io/PrintStream;)V
    .locals 2
    .param p1, "infoStream"    # Ljava/io/PrintStream;

    .prologue
    .line 346
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    .line 347
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 348
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput-object p1, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->infoStream:Ljava/io/PrintStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 350
    :cond_0
    monitor-exit p0

    return-void

    .line 346
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method declared-synchronized setMaxFieldLength(I)V
    .locals 2
    .param p1, "maxFieldLength"    # I

    .prologue
    .line 353
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lorg/apache/lucene/index/DocumentsWriter;->maxFieldLength:I

    .line 354
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 355
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput p1, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->maxFieldLength:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 354
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 357
    :cond_0
    monitor-exit p0

    return-void

    .line 353
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method declared-synchronized setSimilarity(Lorg/apache/lucene/search/Similarity;)V
    .locals 2
    .param p1, "similarity"    # Lorg/apache/lucene/search/Similarity;

    .prologue
    .line 360
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriter;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 361
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 362
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->threadStates:[Lorg/apache/lucene/index/DocumentsWriterThreadState;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput-object p1, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->similarity:Lorg/apache/lucene/search/Similarity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 364
    :cond_0
    monitor-exit p0

    return-void

    .line 360
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method toMB(J)Ljava/lang/String;
    .locals 7
    .param p1, "v"    # J

    .prologue
    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    .line 1220
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->nf:Ljava/text/NumberFormat;

    long-to-double v2, p1

    div-double/2addr v2, v4

    div-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method updateDocument(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/Term;)Z
    .locals 10
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "delTerm"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 751
    iget-object v9, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    if-eqz p3, :cond_3

    move v6, v7

    :goto_0
    invoke-virtual {v9, v7, v6}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->waitUpdate(II)Z

    move-result v0

    .line 754
    .local v0, "doFlush":Z
    invoke-virtual {p0, p3, v7}, Lorg/apache/lucene/index/DocumentsWriter;->getThreadState(Lorg/apache/lucene/index/Term;I)Lorg/apache/lucene/index/DocumentsWriterThreadState;

    move-result-object v3

    .line 756
    .local v3, "state":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    iget-object v1, v3, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 757
    .local v1, "docState":Lorg/apache/lucene/index/DocumentsWriter$DocState;
    iput-object p1, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->doc:Lorg/apache/lucene/document/Document;

    .line 758
    iput-object p2, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 760
    const/4 v4, 0x0

    .line 766
    .local v4, "success":Z
    :try_start_0
    iget-object v6, v3, Lorg/apache/lucene/index/DocumentsWriterThreadState;->consumer:Lorg/apache/lucene/index/DocConsumerPerThread;

    invoke-virtual {v6}, Lorg/apache/lucene/index/DocConsumerPerThread;->processDocument()Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 768
    .local v2, "perDoc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :try_start_1
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriter$DocState;->clear()V

    .line 772
    invoke-direct {p0, v3, v2}, Lorg/apache/lucene/index/DocumentsWriter;->finishDocument(Lorg/apache/lucene/index/DocumentsWriterThreadState;Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 774
    const/4 v4, 0x1

    .line 776
    if-nez v4, :cond_2

    .line 780
    if-eqz v0, :cond_0

    .line 781
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->clearFlushPending()V

    .line 784
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v6, :cond_1

    .line 785
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "exception in updateDocument aborting="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 788
    :cond_1
    monitor-enter p0

    .line 790
    const/4 v6, 0x1

    :try_start_2
    iput-boolean v6, v3, Lorg/apache/lucene/index/DocumentsWriterThreadState;->isIdle:Z

    .line 791
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 793
    iget-boolean v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    if-eqz v6, :cond_a

    .line 794
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 814
    :goto_1
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 818
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    const-string/jumbo v7, "new document"

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushByRAMUsage(Ljava/lang/String;)Z

    move-result v6

    or-int/2addr v0, v6

    move v8, v0

    .line 820
    .end local v2    # "perDoc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :goto_2
    return v8

    .end local v0    # "doFlush":Z
    .end local v1    # "docState":Lorg/apache/lucene/index/DocumentsWriter$DocState;
    .end local v3    # "state":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    .end local v4    # "success":Z
    :cond_3
    move v6, v8

    .line 751
    goto :goto_0

    .line 768
    .restart local v0    # "doFlush":Z
    .restart local v1    # "docState":Lorg/apache/lucene/index/DocumentsWriter$DocState;
    .restart local v3    # "state":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    .restart local v4    # "success":Z
    :catchall_0
    move-exception v6

    :try_start_3
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriter$DocState;->clear()V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 776
    :catchall_1
    move-exception v6

    if-nez v4, :cond_6

    .line 780
    if-eqz v0, :cond_4

    .line 781
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->clearFlushPending()V

    .line 784
    :cond_4
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v7, :cond_5

    .line 785
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "exception in updateDocument aborting="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v9, p0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 788
    :cond_5
    monitor-enter p0

    .line 790
    const/4 v7, 0x1

    :try_start_4
    iput-boolean v7, v3, Lorg/apache/lucene/index/DocumentsWriterThreadState;->isIdle:Z

    .line 791
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 793
    iget-boolean v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    if-eqz v7, :cond_7

    .line 794
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 814
    :goto_3
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 776
    :cond_6
    throw v6

    .line 814
    :catchall_2
    move-exception v6

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v6

    .line 796
    :cond_7
    :try_start_6
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

    iget v9, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iput v9, v7, Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;->docID:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 797
    const/4 v5, 0x0

    .line 799
    .local v5, "success2":Z
    :try_start_7
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    iget-object v9, p0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

    invoke-virtual {v7, v9}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->add(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 800
    const/4 v5, 0x1

    .line 802
    if-nez v5, :cond_9

    .line 803
    :try_start_8
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 804
    monitor-exit p0

    goto :goto_2

    .line 802
    :cond_8
    throw v6

    :catchall_3
    move-exception v6

    if-nez v5, :cond_8

    .line 803
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 804
    monitor-exit p0

    goto :goto_2

    .line 812
    :cond_9
    iget-object v7, v3, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v7, v7, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    invoke-virtual {p0, v7}, Lorg/apache/lucene/index/DocumentsWriter;->deleteDocID(I)V

    goto :goto_3

    .line 796
    .end local v5    # "success2":Z
    .restart local v2    # "perDoc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :cond_a
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

    iget v7, v1, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iput v7, v6, Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;->docID:I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 797
    const/4 v5, 0x0

    .line 799
    .restart local v5    # "success2":Z
    :try_start_9
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->add(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 800
    const/4 v5, 0x1

    .line 802
    if-nez v5, :cond_c

    .line 803
    :try_start_a
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 804
    monitor-exit p0

    goto :goto_2

    .line 802
    :cond_b
    throw v6

    :catchall_4
    move-exception v6

    if-nez v5, :cond_b

    .line 803
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 804
    monitor-exit p0

    goto/16 :goto_2

    .line 812
    :cond_c
    iget-object v6, v3, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v6, v6, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    invoke-virtual {p0, v6}, Lorg/apache/lucene/index/DocumentsWriter;->deleteDocID(I)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    goto/16 :goto_1
.end method

.method updateDocuments(Ljava/util/Collection;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/Term;)Z
    .locals 17
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "delTerm"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            "Lorg/apache/lucene/index/Term;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 827
    .local p1, "docs":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/document/Document;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v16

    if-eqz p3, :cond_5

    const/4 v14, 0x1

    :goto_0
    move/from16 v0, v16

    invoke-virtual {v15, v0, v14}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->waitUpdate(II)Z

    move-result v1

    .line 829
    .local v1, "doFlush":Z
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v3

    .line 833
    .local v3, "docCount":I
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v3}, Lorg/apache/lucene/index/DocumentsWriter;->getThreadState(Lorg/apache/lucene/index/Term;I)Lorg/apache/lucene/index/DocumentsWriterThreadState;

    move-result-object v11

    .line 834
    .local v11, "state":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    iget-object v6, v11, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 836
    .local v6, "docState":Lorg/apache/lucene/index/DocumentsWriter$DocState;
    iget v10, v6, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    .line 837
    .local v10, "startDocID":I
    move v4, v10

    .line 840
    .local v4, "docID":I
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/document/Document;

    .line 841
    .local v2, "doc":Lorg/apache/lucene/document/Document;
    iput-object v2, v6, Lorg/apache/lucene/index/DocumentsWriter$DocState;->doc:Lorg/apache/lucene/document/Document;

    .line 842
    move-object/from16 v0, p2

    iput-object v0, v6, Lorg/apache/lucene/index/DocumentsWriter$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 844
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "docID":I
    .local v5, "docID":I
    iput v4, v6, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    .line 846
    const/4 v12, 0x0

    .line 852
    .local v12, "success":Z
    :try_start_0
    iget-object v14, v11, Lorg/apache/lucene/index/DocumentsWriterThreadState;->consumer:Lorg/apache/lucene/index/DocConsumerPerThread;

    invoke-virtual {v14}, Lorg/apache/lucene/index/DocConsumerPerThread;->processDocument()Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 854
    .local v9, "perDoc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :try_start_1
    invoke-virtual {v6}, Lorg/apache/lucene/index/DocumentsWriter$DocState;->clear()V

    .line 859
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->balanceRAM()V

    .line 862
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 863
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    if-eqz v14, :cond_8

    .line 864
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 878
    if-nez v12, :cond_1e

    .line 883
    if-eqz v1, :cond_0

    .line 884
    const-string/jumbo v14, "clearFlushPending!"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 885
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    invoke-virtual {v14}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->clearFlushPending()V

    .line 888
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v14, :cond_1

    .line 889
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "exception in updateDocuments aborting="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 892
    :cond_1
    monitor-enter p0

    .line 894
    const/4 v14, 0x1

    :try_start_3
    iput-boolean v14, v11, Lorg/apache/lucene/index/DocumentsWriterThreadState;->isIdle:Z

    .line 895
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notifyAll()V

    .line 897
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    if-eqz v14, :cond_13

    .line 898
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_8

    move v4, v5

    .line 927
    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    :goto_2
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 932
    .end local v2    # "doc":Lorg/apache/lucene/document/Document;
    .end local v9    # "perDoc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    .end local v12    # "success":Z
    :cond_2
    :goto_3
    monitor-enter p0

    .line 937
    :try_start_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    invoke-virtual {v14}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->doPause()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 938
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->waitForWaitQueue()V

    .line 943
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    if-eqz v14, :cond_d

    .line 949
    const/4 v14, 0x1

    iput-boolean v14, v11, Lorg/apache/lucene/index/DocumentsWriterThreadState;->isIdle:Z

    .line 952
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notifyAll()V

    .line 954
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 956
    if-eqz v1, :cond_4

    .line 957
    const-string/jumbo v14, "clearFlushPending!"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 958
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    invoke-virtual {v14}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->clearFlushPending()V

    .line 961
    :cond_4
    const/4 v14, 0x0

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 980
    :goto_4
    return v14

    .line 827
    .end local v1    # "doFlush":Z
    .end local v3    # "docCount":I
    .end local v4    # "docID":I
    .end local v6    # "docState":Lorg/apache/lucene/index/DocumentsWriter$DocState;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "startDocID":I
    .end local v11    # "state":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    :cond_5
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 854
    .restart local v1    # "doFlush":Z
    .restart local v2    # "doc":Lorg/apache/lucene/document/Document;
    .restart local v3    # "docCount":I
    .restart local v5    # "docID":I
    .restart local v6    # "docState":Lorg/apache/lucene/index/DocumentsWriter$DocState;
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v10    # "startDocID":I
    .restart local v11    # "state":Lorg/apache/lucene/index/DocumentsWriterThreadState;
    .restart local v12    # "success":Z
    :catchall_0
    move-exception v14

    :try_start_6
    invoke-virtual {v6}, Lorg/apache/lucene/index/DocumentsWriter$DocState;->clear()V

    throw v14
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 878
    :catchall_1
    move-exception v14

    if-nez v12, :cond_20

    .line 883
    if-eqz v1, :cond_6

    .line 884
    const-string/jumbo v15, "clearFlushPending!"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 885
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    invoke-virtual {v15}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->clearFlushPending()V

    .line 888
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v15, :cond_7

    .line 889
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "exception in updateDocuments aborting="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 892
    :cond_7
    monitor-enter p0

    .line 894
    const/4 v15, 0x1

    :try_start_7
    iput-boolean v15, v11, Lorg/apache/lucene/index/DocumentsWriterThreadState;->isIdle:Z

    .line 895
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notifyAll()V

    .line 897
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    if-eqz v15, :cond_f

    .line 898
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_8

    move v4, v5

    .line 927
    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    :goto_5
    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 878
    :goto_6
    throw v14

    .line 866
    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    .restart local v9    # "perDoc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :cond_8
    :try_start_9
    sget-boolean v14, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v14, :cond_9

    if-eqz v9, :cond_9

    iget v14, v9, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->docID:I

    iget v15, v6, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    if-eq v14, v15, :cond_9

    new-instance v14, Ljava/lang/AssertionError;

    invoke-direct {v14}, Ljava/lang/AssertionError;-><init>()V

    throw v14

    .line 874
    :catchall_2
    move-exception v14

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v14
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 868
    :cond_9
    if-eqz v9, :cond_c

    .line 869
    :try_start_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    invoke-virtual {v14, v9}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->add(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)Z

    .line 874
    :goto_7
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 876
    const/4 v12, 0x1

    .line 878
    if-nez v12, :cond_1c

    .line 883
    if-eqz v1, :cond_a

    .line 884
    const-string/jumbo v14, "clearFlushPending!"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 885
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    invoke-virtual {v14}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->clearFlushPending()V

    .line 888
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Ljava/io/PrintStream;

    if-eqz v14, :cond_b

    .line 889
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "exception in updateDocuments aborting="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/DocumentsWriter;->message(Ljava/lang/String;)V

    .line 892
    :cond_b
    monitor-enter p0

    .line 894
    const/4 v14, 0x1

    :try_start_c
    iput-boolean v14, v11, Lorg/apache/lucene/index/DocumentsWriterThreadState;->isIdle:Z

    .line 895
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notifyAll()V

    .line 897
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z

    if-eqz v14, :cond_17

    .line 898
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_8

    move v4, v5

    .line 927
    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    :goto_8
    :try_start_d
    monitor-exit p0

    goto/16 :goto_1

    .end local v9    # "perDoc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :catchall_3
    move-exception v14

    :goto_9
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    throw v14

    .line 871
    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    .restart local v9    # "perDoc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :cond_c
    :try_start_e
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

    iget v15, v6, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iput v15, v14, Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;->docID:I

    .line 872
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

    invoke-virtual {v14, v15}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->add(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    goto :goto_7

    .line 967
    .end local v2    # "doc":Lorg/apache/lucene/document/Document;
    .end local v5    # "docID":I
    .end local v9    # "perDoc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    .end local v12    # "success":Z
    .restart local v4    # "docID":I
    :cond_d
    if-eqz p3, :cond_e

    .line 968
    :try_start_f
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->pendingDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    move-object/from16 v0, p3

    invoke-virtual {v14, v0, v10}, Lorg/apache/lucene/index/BufferedDeletes;->addTerm(Lorg/apache/lucene/index/Term;I)V

    .line 971
    :cond_e
    const/4 v14, 0x1

    iput-boolean v14, v11, Lorg/apache/lucene/index/DocumentsWriterThreadState;->isIdle:Z

    .line 974
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notifyAll()V

    .line 975
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    .line 977
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/IndexWriter$FlushControl;

    const-string/jumbo v15, "new document"

    invoke-virtual {v14, v15}, Lorg/apache/lucene/index/IndexWriter$FlushControl;->flushByRAMUsage(Ljava/lang/String;)Z

    move-result v14

    or-int/2addr v1, v14

    move v14, v1

    .line 980
    goto/16 :goto_4

    .line 975
    :catchall_4
    move-exception v14

    :try_start_10
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    throw v14

    .line 904
    .end local v4    # "docID":I
    .restart local v2    # "doc":Lorg/apache/lucene/document/Document;
    .restart local v5    # "docID":I
    .restart local v12    # "success":Z
    :cond_f
    add-int v7, v10, v3

    .line 905
    .local v7, "endDocID":I
    :try_start_11
    iget v4, v6, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    move v5, v4

    .line 906
    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    :goto_a
    if-ge v5, v7, :cond_10

    .line 907
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_8

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    :try_start_12
    iput v5, v15, Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;->docID:I
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    .line 908
    const/4 v13, 0x0

    .line 910
    .local v13, "success2":Z
    :try_start_13
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->add(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)Z
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_5

    .line 911
    const/4 v13, 0x1

    .line 913
    if-nez v13, :cond_12

    .line 914
    :try_start_14
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 915
    const/4 v14, 0x0

    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    goto/16 :goto_4

    .line 922
    .end local v4    # "docID":I
    .end local v13    # "success2":Z
    .restart local v5    # "docID":I
    :cond_10
    move v4, v10

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    move v5, v4

    .line 923
    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    :goto_b
    :try_start_15
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_8

    move-result v15

    add-int/2addr v15, v10

    if-ge v5, v15, :cond_1f

    .line 924
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    :try_start_16
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/DocumentsWriter;->deleteDocID(I)V

    move v5, v4

    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    goto :goto_b

    .line 913
    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    .restart local v13    # "success2":Z
    :cond_11
    throw v14

    :catchall_5
    move-exception v14

    if-nez v13, :cond_11

    .line 914
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 915
    const/4 v14, 0x0

    monitor-exit p0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_3

    goto/16 :goto_4

    :cond_12
    move v5, v4

    .line 918
    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    goto :goto_a

    .line 904
    .end local v7    # "endDocID":I
    .end local v13    # "success2":Z
    .restart local v9    # "perDoc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :cond_13
    add-int v7, v10, v3

    .line 905
    .restart local v7    # "endDocID":I
    :try_start_17
    iget v4, v6, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    move v5, v4

    .line 906
    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    :goto_c
    if-ge v5, v7, :cond_14

    .line 907
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_8

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    :try_start_18
    iput v5, v14, Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;->docID:I
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_3

    .line 908
    const/4 v13, 0x0

    .line 910
    .restart local v13    # "success2":Z
    :try_start_19
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

    invoke-virtual {v14, v15}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->add(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)Z
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_6

    .line 911
    const/4 v13, 0x1

    .line 913
    if-nez v13, :cond_16

    .line 914
    :try_start_1a
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 915
    const/4 v14, 0x0

    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_3

    goto/16 :goto_4

    .line 922
    .end local v4    # "docID":I
    .end local v13    # "success2":Z
    .restart local v5    # "docID":I
    :cond_14
    move v4, v10

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    move v5, v4

    .line 923
    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    :goto_d
    :try_start_1b
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_8

    move-result v14

    add-int/2addr v14, v10

    if-ge v5, v14, :cond_1d

    .line 924
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    :try_start_1c
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/DocumentsWriter;->deleteDocID(I)V

    move v5, v4

    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    goto :goto_d

    .line 913
    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    .restart local v13    # "success2":Z
    :cond_15
    throw v14

    :catchall_6
    move-exception v14

    if-nez v13, :cond_15

    .line 914
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 915
    const/4 v14, 0x0

    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_3

    goto/16 :goto_4

    :cond_16
    move v5, v4

    .line 918
    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    goto :goto_c

    .line 904
    .end local v7    # "endDocID":I
    .end local v13    # "success2":Z
    :cond_17
    add-int v7, v10, v3

    .line 905
    .restart local v7    # "endDocID":I
    :try_start_1d
    iget v4, v6, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    move v5, v4

    .line 906
    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    :goto_e
    if-ge v5, v7, :cond_18

    .line 907
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_8

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    :try_start_1e
    iput v5, v14, Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;->docID:I
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_3

    .line 908
    const/4 v13, 0x0

    .line 910
    .restart local v13    # "success2":Z
    :try_start_1f
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/index/DocumentsWriter;->skipDocWriter:Lorg/apache/lucene/index/DocumentsWriter$SkipDocWriter;

    invoke-virtual {v14, v15}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->add(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)Z
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_7

    .line 911
    const/4 v13, 0x1

    .line 913
    if-nez v13, :cond_1a

    .line 914
    :try_start_20
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 915
    const/4 v14, 0x0

    monitor-exit p0
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_3

    goto/16 :goto_4

    .line 922
    .end local v4    # "docID":I
    .end local v13    # "success2":Z
    .restart local v5    # "docID":I
    :cond_18
    move v4, v10

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    move v5, v4

    .line 923
    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    :goto_f
    :try_start_21
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_8

    move-result v14

    add-int/2addr v14, v10

    if-ge v5, v14, :cond_1b

    .line 924
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    :try_start_22
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/DocumentsWriter;->deleteDocID(I)V

    move v5, v4

    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    goto :goto_f

    .line 913
    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    .restart local v13    # "success2":Z
    :cond_19
    throw v14

    :catchall_7
    move-exception v14

    if-nez v13, :cond_19

    .line 914
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocumentsWriter;->abort()V

    .line 915
    const/4 v14, 0x0

    monitor-exit p0
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_3

    goto/16 :goto_4

    :cond_1a
    move v5, v4

    .line 918
    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    goto :goto_e

    .line 927
    .end local v7    # "endDocID":I
    .end local v9    # "perDoc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    .end local v13    # "success2":Z
    :catchall_8
    move-exception v14

    move v4, v5

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    goto/16 :goto_9

    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    .restart local v7    # "endDocID":I
    .restart local v9    # "perDoc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :cond_1b
    move v4, v5

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    goto/16 :goto_8

    .end local v4    # "docID":I
    .end local v7    # "endDocID":I
    .restart local v5    # "docID":I
    :cond_1c
    move v4, v5

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    goto/16 :goto_1

    .end local v4    # "docID":I
    .restart local v5    # "docID":I
    .restart local v7    # "endDocID":I
    :cond_1d
    move v4, v5

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    goto/16 :goto_2

    .end local v4    # "docID":I
    .end local v7    # "endDocID":I
    .restart local v5    # "docID":I
    :cond_1e
    move v4, v5

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    goto/16 :goto_3

    .end local v4    # "docID":I
    .end local v9    # "perDoc":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    .restart local v5    # "docID":I
    .restart local v7    # "endDocID":I
    :cond_1f
    move v4, v5

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    goto/16 :goto_5

    .end local v4    # "docID":I
    .end local v7    # "endDocID":I
    .restart local v5    # "docID":I
    :cond_20
    move v4, v5

    .end local v5    # "docID":I
    .restart local v4    # "docID":I
    goto/16 :goto_6
.end method

.method declared-synchronized waitForWaitQueue()V
    .locals 2

    .prologue
    .line 1063
    monitor-enter p0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1067
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->waitQueue:Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriter$WaitQueue;->doResume()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 1068
    monitor-exit p0

    return-void

    .line 1064
    :catch_0
    move-exception v0

    .line 1065
    .local v0, "ie":Ljava/lang/InterruptedException;
    :try_start_2
    new-instance v1, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1063
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized waitIdle()V
    .locals 2

    .prologue
    .line 984
    monitor-enter p0

    :goto_0
    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriter;->allThreadsIdle()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 986
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 987
    :catch_0
    move-exception v0

    .line 988
    .local v0, "ie":Ljava/lang/InterruptedException;
    :try_start_2
    new-instance v1, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 984
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 991
    :cond_0
    monitor-exit p0

    return-void
.end method

.method declared-synchronized waitReady(Lorg/apache/lucene/index/DocumentsWriterThreadState;)V
    .locals 3
    .param p1, "state"    # Lorg/apache/lucene/index/DocumentsWriterThreadState;

    .prologue
    .line 994
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->closed:Z

    if-nez v1, :cond_1

    iget-boolean v1, p1, Lorg/apache/lucene/index/DocumentsWriterThreadState;->isIdle:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->aborting:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 996
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 997
    :catch_0
    move-exception v0

    .line 998
    .local v0, "ie":Ljava/lang/InterruptedException;
    :try_start_2
    new-instance v1, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 994
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1002
    :cond_1
    :try_start_3
    iget-boolean v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->closed:Z

    if-eqz v1, :cond_2

    .line 1003
    new-instance v1, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string/jumbo v2, "this IndexWriter is closed"

    invoke-direct {v1, v2}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1005
    :cond_2
    monitor-exit p0

    return-void
.end method
