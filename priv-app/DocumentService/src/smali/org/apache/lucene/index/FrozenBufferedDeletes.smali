.class Lorg/apache/lucene/index/FrozenBufferedDeletes;
.super Ljava/lang/Object;
.source "FrozenBufferedDeletes.java"


# static fields
.field static final BYTES_PER_DEL_QUERY:I


# instance fields
.field final bytesUsed:I

.field final gen:J

.field final numTermDeletes:I

.field final queries:[Lorg/apache/lucene/search/Query;

.field final queryLimits:[I

.field termCount:I

.field final terms:Lorg/apache/lucene/index/PrefixCodedTerms;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x18

    sput v0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->BYTES_PER_DEL_QUERY:I

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/BufferedDeletes;J)V
    .locals 12
    .param p1, "deletes"    # Lorg/apache/lucene/index/BufferedDeletes;
    .param p2, "gen"    # J

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iget-object v8, p1, Lorg/apache/lucene/index/BufferedDeletes;->terms:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    iget-object v9, p1, Lorg/apache/lucene/index/BufferedDeletes;->terms:Ljava/util/Map;

    invoke-interface {v9}, Ljava/util/Map;->size()I

    move-result v9

    new-array v9, v9, [Lorg/apache/lucene/index/Term;

    invoke-interface {v8, v9}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lorg/apache/lucene/index/Term;

    .line 52
    .local v6, "termsArray":[Lorg/apache/lucene/index/Term;
    array-length v8, v6

    iput v8, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->termCount:I

    .line 53
    invoke-static {v6}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Comparable;)V

    .line 54
    new-instance v1, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;

    invoke-direct {v1}, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;-><init>()V

    .line 55
    .local v1, "builder":Lorg/apache/lucene/index/PrefixCodedTerms$Builder;
    move-object v0, v6

    .local v0, "arr$":[Lorg/apache/lucene/index/Term;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    .line 56
    .local v5, "term":Lorg/apache/lucene/index/Term;
    invoke-virtual {v1, v5}, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->add(Lorg/apache/lucene/index/Term;)V

    .line 55
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 58
    .end local v5    # "term":Lorg/apache/lucene/index/Term;
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->finish()Lorg/apache/lucene/index/PrefixCodedTerms;

    move-result-object v8

    iput-object v8, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->terms:Lorg/apache/lucene/index/PrefixCodedTerms;

    .line 60
    iget-object v8, p1, Lorg/apache/lucene/index/BufferedDeletes;->queries:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v8

    new-array v8, v8, [Lorg/apache/lucene/search/Query;

    iput-object v8, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    .line 61
    iget-object v8, p1, Lorg/apache/lucene/index/BufferedDeletes;->queries:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v8

    new-array v8, v8, [I

    iput-object v8, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queryLimits:[I

    .line 62
    const/4 v7, 0x0

    .line 63
    .local v7, "upto":I
    iget-object v8, p1, Lorg/apache/lucene/index/BufferedDeletes;->queries:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 64
    .local v2, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;"
    iget-object v9, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/search/Query;

    aput-object v8, v9, v7

    .line 65
    iget-object v9, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queryLimits:[I

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    aput v8, v9, v7

    .line 66
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 69
    .end local v2    # "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;"
    :cond_1
    iget-object v8, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->terms:Lorg/apache/lucene/index/PrefixCodedTerms;

    invoke-virtual {v8}, Lorg/apache/lucene/index/PrefixCodedTerms;->getSizeInBytes()J

    move-result-wide v8

    long-to-int v8, v8

    iget-object v9, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    array-length v9, v9

    sget v10, Lorg/apache/lucene/index/FrozenBufferedDeletes;->BYTES_PER_DEL_QUERY:I

    mul-int/2addr v9, v10

    add-int/2addr v8, v9

    iput v8, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->bytesUsed:I

    .line 70
    iget-object v8, p1, Lorg/apache/lucene/index/BufferedDeletes;->numTermDeletes:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v8

    iput v8, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->numTermDeletes:I

    .line 71
    iput-wide p2, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->gen:J

    .line 72
    return-void
.end method


# virtual methods
.method any()Z
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->termCount:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    array-length v0, v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public queriesIterable()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    new-instance v0, Lorg/apache/lucene/index/FrozenBufferedDeletes$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/FrozenBufferedDeletes$2;-><init>(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    return-object v0
.end method

.method public termsIterable()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    new-instance v0, Lorg/apache/lucene/index/FrozenBufferedDeletes$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/FrozenBufferedDeletes$1;-><init>(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 113
    const-string/jumbo v0, ""

    .line 114
    .local v0, "s":Ljava/lang/String;
    iget v1, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->numTermDeletes:I

    if-eqz v1, :cond_0

    .line 115
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->numTermDeletes:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " deleted terms (unique count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->termCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 117
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    array-length v1, v1

    if-eqz v1, :cond_1

    .line 118
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " deleted queries"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 120
    :cond_1
    iget v1, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->bytesUsed:I

    if-eqz v1, :cond_2

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " bytesUsed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->bytesUsed:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 124
    :cond_2
    return-object v0
.end method
