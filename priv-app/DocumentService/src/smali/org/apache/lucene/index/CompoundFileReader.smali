.class Lorg/apache/lucene/index/CompoundFileReader;
.super Lorg/apache/lucene/store/Directory;
.source "CompoundFileReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/CompoundFileReader$1;,
        Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;,
        Lorg/apache/lucene/index/CompoundFileReader$FileEntry;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private directory:Lorg/apache/lucene/store/Directory;

.field private entries:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/CompoundFileReader$FileEntry;",
            ">;"
        }
    .end annotation
.end field

.field private fileName:Ljava/lang/String;

.field private readBufferSize:I

.field private stream:Lorg/apache/lucene/store/IndexInput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lorg/apache/lucene/index/CompoundFileReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/CompoundFileReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    const/16 v0, 0x400

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/index/CompoundFileReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;I)V
    .locals 16
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "readBufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/store/Directory;-><init>()V

    .line 50
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v11, v0, Lorg/apache/lucene/index/CompoundFileReader;->entries:Ljava/util/HashMap;

    .line 57
    sget-boolean v11, Lorg/apache/lucene/index/CompoundFileReader;->$assertionsDisabled:Z

    if-nez v11, :cond_0

    move-object/from16 v0, p1

    instance-of v11, v0, Lorg/apache/lucene/index/CompoundFileReader;

    if-eqz v11, :cond_0

    new-instance v11, Ljava/lang/AssertionError;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "compound file inside of compound file: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v11

    .line 58
    :cond_0
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/CompoundFileReader;->directory:Lorg/apache/lucene/store/Directory;

    .line 59
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/CompoundFileReader;->fileName:Ljava/lang/String;

    .line 60
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/index/CompoundFileReader;->readBufferSize:I

    .line 62
    const/4 v10, 0x0

    .line 65
    .local v10, "success":Z
    :try_start_0
    invoke-virtual/range {p1 .. p3}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;

    .line 69
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v11}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    .line 73
    .local v4, "firstInt":I
    if-gez v4, :cond_5

    .line 74
    const/4 v11, -0x1

    if-ge v4, v11, :cond_2

    .line 75
    new-instance v11, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Incompatible format version: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " expected "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, -0x1

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " (resource: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    .end local v4    # "firstInt":I
    :catchall_0
    move-exception v11

    if-nez v10, :cond_1

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;

    if-eqz v12, :cond_1

    .line 118
    :try_start_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 116
    :cond_1
    :goto_0
    throw v11

    .line 79
    .restart local v4    # "firstInt":I
    :cond_2
    :try_start_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v11}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    .line 80
    .local v2, "count":I
    const/4 v7, 0x0

    .line 87
    .local v7, "stripSegmentName":Z
    :goto_1
    const/4 v3, 0x0

    .line 88
    .local v3, "entry":Lorg/apache/lucene/index/CompoundFileReader$FileEntry;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v2, :cond_6

    .line 89
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v11}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v8

    .line 90
    .local v8, "offset":J
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v11}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v6

    .line 92
    .local v6, "id":Ljava/lang/String;
    if-eqz v7, :cond_3

    .line 95
    invoke-static {v6}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 98
    :cond_3
    if-eqz v3, :cond_4

    .line 100
    iget-wide v12, v3, Lorg/apache/lucene/index/CompoundFileReader$FileEntry;->offset:J

    sub-long v12, v8, v12

    iput-wide v12, v3, Lorg/apache/lucene/index/CompoundFileReader$FileEntry;->length:J

    .line 103
    :cond_4
    new-instance v3, Lorg/apache/lucene/index/CompoundFileReader$FileEntry;

    .end local v3    # "entry":Lorg/apache/lucene/index/CompoundFileReader$FileEntry;
    const/4 v11, 0x0

    invoke-direct {v3, v11}, Lorg/apache/lucene/index/CompoundFileReader$FileEntry;-><init>(Lorg/apache/lucene/index/CompoundFileReader$1;)V

    .line 104
    .restart local v3    # "entry":Lorg/apache/lucene/index/CompoundFileReader$FileEntry;
    iput-wide v8, v3, Lorg/apache/lucene/index/CompoundFileReader$FileEntry;->offset:J

    .line 105
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/CompoundFileReader;->entries:Ljava/util/HashMap;

    invoke-virtual {v11, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 82
    .end local v2    # "count":I
    .end local v3    # "entry":Lorg/apache/lucene/index/CompoundFileReader$FileEntry;
    .end local v5    # "i":I
    .end local v6    # "id":Ljava/lang/String;
    .end local v7    # "stripSegmentName":Z
    .end local v8    # "offset":J
    :cond_5
    move v2, v4

    .line 83
    .restart local v2    # "count":I
    const/4 v7, 0x1

    .restart local v7    # "stripSegmentName":Z
    goto :goto_1

    .line 109
    .restart local v3    # "entry":Lorg/apache/lucene/index/CompoundFileReader$FileEntry;
    .restart local v5    # "i":I
    :cond_6
    if-eqz v3, :cond_7

    .line 110
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v11}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v12

    iget-wide v14, v3, Lorg/apache/lucene/index/CompoundFileReader$FileEntry;->offset:J

    sub-long/2addr v12, v14

    iput-wide v12, v3, Lorg/apache/lucene/index/CompoundFileReader$FileEntry;->length:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 113
    :cond_7
    const/4 v10, 0x1

    .line 116
    if-nez v10, :cond_8

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;

    if-eqz v11, :cond_8

    .line 118
    :try_start_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v11}, Lorg/apache/lucene/store/IndexInput;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 122
    :cond_8
    :goto_3
    return-void

    .line 119
    :catch_0
    move-exception v11

    goto :goto_3

    .end local v2    # "count":I
    .end local v3    # "entry":Lorg/apache/lucene/index/CompoundFileReader$FileEntry;
    .end local v4    # "firstInt":I
    .end local v5    # "i":I
    .end local v7    # "stripSegmentName":Z
    :catch_1
    move-exception v12

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 141
    :goto_0
    monitor-exit p0

    return-void

    .line 138
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/CompoundFileReader;->entries:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 139
    iget-object v0, p0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 223
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 200
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public fileExists(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 178
    iget-object v0, p0, Lorg/apache/lucene/index/CompoundFileReader;->entries:Ljava/util/HashMap;

    invoke-static {p1}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public fileLength(Ljava/lang/String;)J
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 213
    iget-object v1, p0, Lorg/apache/lucene/index/CompoundFileReader;->entries:Ljava/util/HashMap;

    invoke-static {p1}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/CompoundFileReader$FileEntry;

    .line 214
    .local v0, "e":Lorg/apache/lucene/index/CompoundFileReader$FileEntry;
    if-nez v0, :cond_0

    .line 215
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1, p1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 216
    :cond_0
    iget-wide v2, v0, Lorg/apache/lucene/index/CompoundFileReader$FileEntry;->length:J

    return-wide v2
.end method

.method public fileModified(Ljava/lang/String;)J
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lorg/apache/lucene/index/CompoundFileReader;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v1, p0, Lorg/apache/lucene/index/CompoundFileReader;->fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/Directory;->fileModified(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getDirectory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lorg/apache/lucene/index/CompoundFileReader;->directory:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/lucene/index/CompoundFileReader;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public listAll()[Ljava/lang/String;
    .locals 7

    .prologue
    .line 166
    iget-object v3, p0, Lorg/apache/lucene/index/CompoundFileReader;->entries:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/CompoundFileReader;->entries:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 168
    .local v1, "res":[Ljava/lang/String;
    iget-object v3, p0, Lorg/apache/lucene/index/CompoundFileReader;->fileName:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lorg/apache/lucene/index/CompoundFileReader;->fileName:Ljava/lang/String;

    const/16 v6, 0x2e

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 169
    .local v2, "seg":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 170
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 169
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 172
    :cond_0
    return-object v1
.end method

.method public makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 230
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public declared-synchronized openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/index/CompoundFileReader;->readBufferSize:I

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/CompoundFileReader;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;
    .locals 8
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "readBufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;

    if-nez v0, :cond_0

    .line 152
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "Stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 154
    :cond_0
    :try_start_1
    invoke-static {p1}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 155
    iget-object v0, p0, Lorg/apache/lucene/index/CompoundFileReader;->entries:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/CompoundFileReader$FileEntry;

    .line 156
    .local v7, "entry":Lorg/apache/lucene/index/CompoundFileReader$FileEntry;
    if-nez v7, :cond_1

    .line 157
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No sub-file with id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " found (fileName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/CompoundFileReader;->fileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " files: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/CompoundFileReader;->entries:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_1
    new-instance v0, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;

    iget-object v1, p0, Lorg/apache/lucene/index/CompoundFileReader;->stream:Lorg/apache/lucene/store/IndexInput;

    iget-wide v2, v7, Lorg/apache/lucene/index/CompoundFileReader$FileEntry;->offset:J

    iget-wide v4, v7, Lorg/apache/lucene/index/CompoundFileReader$FileEntry;->length:J

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/CompoundFileReader$CSIndexInput;-><init>(Lorg/apache/lucene/store/IndexInput;JJI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public renameFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "from"    # Ljava/lang/String;
    .param p2, "to"    # Ljava/lang/String;

    .prologue
    .line 206
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public touchFile(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lorg/apache/lucene/index/CompoundFileReader;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v1, p0, Lorg/apache/lucene/index/CompoundFileReader;->fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/Directory;->touchFile(Ljava/lang/String;)V

    .line 194
    return-void
.end method
