.class abstract Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
.super Ljava/lang/Object;
.source "DocumentsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "DocWriter"
.end annotation


# instance fields
.field docID:I

.field next:Lorg/apache/lucene/index/DocumentsWriter$DocWriter;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract abort()V
.end method

.method abstract finish()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method setNext(Lorg/apache/lucene/index/DocumentsWriter$DocWriter;)V
    .locals 0
    .param p1, "next"    # Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    .prologue
    .line 173
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->next:Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    .line 174
    return-void
.end method

.method abstract sizeInBytes()J
.end method
