.class public Lorg/apache/lucene/index/FieldSortedTermVectorMapper;
.super Lorg/apache/lucene/index/TermVectorMapper;
.source "FieldSortedTermVectorMapper.java"


# instance fields
.field private comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/index/TermVectorEntry;",
            ">;"
        }
    .end annotation
.end field

.field private currentField:Ljava/lang/String;

.field private currentSet:Ljava/util/SortedSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedSet",
            "<",
            "Lorg/apache/lucene/index/TermVectorEntry;",
            ">;"
        }
    .end annotation
.end field

.field private fieldToTerms:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/SortedSet",
            "<",
            "Lorg/apache/lucene/index/TermVectorEntry;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/index/TermVectorEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/index/TermVectorEntry;>;"
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, v0, v0, p1}, Lorg/apache/lucene/index/FieldSortedTermVectorMapper;-><init>(ZZLjava/util/Comparator;)V

    .line 38
    return-void
.end method

.method public constructor <init>(ZZLjava/util/Comparator;)V
    .locals 1
    .param p1, "ignoringPositions"    # Z
    .param p2, "ignoringOffsets"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/index/TermVectorEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p3, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/index/TermVectorEntry;>;"
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/index/TermVectorMapper;-><init>(ZZ)V

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/FieldSortedTermVectorMapper;->fieldToTerms:Ljava/util/Map;

    .line 43
    iput-object p3, p0, Lorg/apache/lucene/index/FieldSortedTermVectorMapper;->comparator:Ljava/util/Comparator;

    .line 44
    return-void
.end method


# virtual methods
.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/index/TermVectorEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/index/FieldSortedTermVectorMapper;->comparator:Ljava/util/Comparator;

    return-object v0
.end method

.method public getFieldToTerms()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/SortedSet",
            "<",
            "Lorg/apache/lucene/index/TermVectorEntry;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/index/FieldSortedTermVectorMapper;->fieldToTerms:Ljava/util/Map;

    return-object v0
.end method

.method public map(Ljava/lang/String;I[Lorg/apache/lucene/index/TermVectorOffsetInfo;[I)V
    .locals 6
    .param p1, "term"    # Ljava/lang/String;
    .param p2, "frequency"    # I
    .param p3, "offsets"    # [Lorg/apache/lucene/index/TermVectorOffsetInfo;
    .param p4, "positions"    # [I

    .prologue
    .line 48
    new-instance v0, Lorg/apache/lucene/index/TermVectorEntry;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldSortedTermVectorMapper;->currentField:Ljava/lang/String;

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/TermVectorEntry;-><init>(Ljava/lang/String;Ljava/lang/String;I[Lorg/apache/lucene/index/TermVectorOffsetInfo;[I)V

    .line 49
    .local v0, "entry":Lorg/apache/lucene/index/TermVectorEntry;
    iget-object v1, p0, Lorg/apache/lucene/index/FieldSortedTermVectorMapper;->currentSet:Ljava/util/SortedSet;

    invoke-interface {v1, v0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    .line 50
    return-void
.end method

.method public setExpectations(Ljava/lang/String;IZZ)V
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "numTerms"    # I
    .param p3, "storeOffsets"    # Z
    .param p4, "storePositions"    # Z

    .prologue
    .line 54
    new-instance v0, Ljava/util/TreeSet;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldSortedTermVectorMapper;->comparator:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lorg/apache/lucene/index/FieldSortedTermVectorMapper;->currentSet:Ljava/util/SortedSet;

    .line 55
    iput-object p1, p0, Lorg/apache/lucene/index/FieldSortedTermVectorMapper;->currentField:Ljava/lang/String;

    .line 56
    iget-object v0, p0, Lorg/apache/lucene/index/FieldSortedTermVectorMapper;->fieldToTerms:Ljava/util/Map;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldSortedTermVectorMapper;->currentSet:Ljava/util/SortedSet;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    return-void
.end method
