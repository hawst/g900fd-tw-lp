.class public Lorg/apache/lucene/index/CheckIndex;
.super Ljava/lang/Object;
.source "CheckIndex.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/CheckIndex$MySegmentTermDocs;,
        Lorg/apache/lucene/index/CheckIndex$Status;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static assertsOn:Z


# instance fields
.field private dir:Lorg/apache/lucene/store/Directory;

.field private infoStream:Ljava/io/PrintStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lorg/apache/lucene/index/CheckIndex;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260
    iput-object p1, p0, Lorg/apache/lucene/index/CheckIndex;->dir:Lorg/apache/lucene/store/Directory;

    .line 261
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    .line 262
    return-void
.end method

.method private static assertsOn()Z
    .locals 1

    .prologue
    .line 939
    sget-boolean v0, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/apache/lucene/index/CheckIndex;->testAsserts()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 940
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/CheckIndex;->assertsOn:Z

    return v0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 15
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 977
    const/4 v4, 0x0

    .line 978
    .local v4, "doFix":Z
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 979
    .local v8, "onlySegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 980
    .local v7, "indexPath":Ljava/lang/String;
    const/4 v3, 0x0

    .line 981
    .local v3, "dirImpl":Ljava/lang/String;
    const/4 v6, 0x0

    .line 982
    .local v6, "i":I
    :goto_0
    array-length v12, p0

    if-ge v6, v12, :cond_6

    .line 983
    aget-object v0, p0, v6

    .line 984
    .local v0, "arg":Ljava/lang/String;
    const-string/jumbo v12, "-fix"

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 985
    const/4 v4, 0x1

    .line 986
    add-int/lit8 v6, v6, 0x1

    .line 1008
    :goto_1
    add-int/lit8 v6, v6, 0x1

    .line 1009
    goto :goto_0

    .line 987
    :cond_0
    aget-object v12, p0, v6

    const-string/jumbo v13, "-segment"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 988
    array-length v12, p0

    add-int/lit8 v12, v12, -0x1

    if-ne v6, v12, :cond_1

    .line 989
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v13, "ERROR: missing name for -segment option"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 990
    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/System;->exit(I)V

    .line 992
    :cond_1
    add-int/lit8 v6, v6, 0x1

    .line 993
    aget-object v12, p0, v6

    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 994
    :cond_2
    const-string/jumbo v12, "-dir-impl"

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 995
    array-length v12, p0

    add-int/lit8 v12, v12, -0x1

    if-ne v6, v12, :cond_3

    .line 996
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v13, "ERROR: missing value for -dir-impl option"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 997
    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/System;->exit(I)V

    .line 999
    :cond_3
    add-int/lit8 v6, v6, 0x1

    .line 1000
    aget-object v3, p0, v6

    goto :goto_1

    .line 1002
    :cond_4
    if-eqz v7, :cond_5

    .line 1003
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "ERROR: unexpected extra argument \'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    aget-object v14, p0, v6

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "\'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1004
    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/System;->exit(I)V

    .line 1006
    :cond_5
    aget-object v7, p0, v6

    goto :goto_1

    .line 1011
    .end local v0    # "arg":Ljava/lang/String;
    :cond_6
    if-nez v7, :cond_7

    .line 1012
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v13, "\nERROR: index path not specified"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1013
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "\nUsage: java org.apache.lucene.index.CheckIndex pathToIndex [-fix] [-segment X] [-segment Y] [-dir-impl X]\n\n  -fix: actually write a new segments_N file, removing any problematic segments\n  -segment X: only check the specified segments.  This can be specified multiple\n              times, to check more than one segment, eg \'-segment _2 -segment _a\'.\n              You can\'t use this with the -fix option\n  -dir-impl X: use a specific "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-class v14, Lorg/apache/lucene/store/FSDirectory;

    invoke-virtual {v14}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " implementation. "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "If no package is specified the "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-class v14, Lorg/apache/lucene/store/FSDirectory;

    invoke-virtual {v14}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " package will be used.\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "**WARNING**: -fix should only be used on an emergency basis as it will cause\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "documents (perhaps many) to be permanently removed from the index.  Always make\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "a backup copy of your index before running this!  Do not run this tool on an index\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "that is actively being written to.  You have been warned!\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "Run without -fix, this tool will open the index, report version information\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "and report any exceptions it hits and what action it would take if -fix were\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "specified.  With -fix, this tool will remove any segments that have issues and\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "write a new segments_N file.  This means all documents contained in the affected\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "segments will be removed.\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "This tool exits with exit code 1 if the index cannot be opened or has any\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "corruption, else 0.\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1034
    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/System;->exit(I)V

    .line 1037
    :cond_7
    invoke-static {}, Lorg/apache/lucene/index/CheckIndex;->assertsOn()Z

    move-result v12

    if-nez v12, :cond_8

    .line 1038
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v13, "\nNOTE: testing will be more thorough if you run java with \'-ea:org.apache.lucene...\', so assertions are enabled"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1040
    :cond_8
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v12

    if-nez v12, :cond_c

    .line 1041
    const/4 v8, 0x0

    .line 1047
    :cond_9
    :goto_2
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "\nOpening index @ "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1048
    const/4 v2, 0x0

    .line 1050
    .local v2, "dir":Lorg/apache/lucene/store/Directory;
    if-nez v3, :cond_d

    .line 1051
    :try_start_0
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v12}, Lorg/apache/lucene/store/FSDirectory;->open(Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1061
    :goto_3
    new-instance v1, Lorg/apache/lucene/index/CheckIndex;

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/CheckIndex;-><init>(Lorg/apache/lucene/store/Directory;)V

    .line 1062
    .local v1, "checker":Lorg/apache/lucene/index/CheckIndex;
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v12}, Lorg/apache/lucene/index/CheckIndex;->setInfoStream(Ljava/io/PrintStream;)V

    .line 1064
    invoke-virtual {v1, v8}, Lorg/apache/lucene/index/CheckIndex;->checkIndex(Ljava/util/List;)Lorg/apache/lucene/index/CheckIndex$Status;

    move-result-object v9

    .line 1065
    .local v9, "result":Lorg/apache/lucene/index/CheckIndex$Status;
    iget-boolean v12, v9, Lorg/apache/lucene/index/CheckIndex$Status;->missingSegments:Z

    if-eqz v12, :cond_a

    .line 1066
    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/System;->exit(I)V

    .line 1069
    :cond_a
    iget-boolean v12, v9, Lorg/apache/lucene/index/CheckIndex$Status;->clean:Z

    if-nez v12, :cond_b

    .line 1070
    if-nez v4, :cond_e

    .line 1071
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "WARNING: would write new segments file, and "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v9, Lorg/apache/lucene/index/CheckIndex$Status;->totLoseDocCount:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " documents would be lost, if -fix were specified\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1085
    :cond_b
    :goto_4
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v13, ""

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1088
    iget-boolean v12, v9, Lorg/apache/lucene/index/CheckIndex$Status;->clean:Z

    const/4 v13, 0x1

    if-ne v12, v13, :cond_10

    .line 1089
    const/4 v5, 0x0

    .line 1092
    .local v5, "exitCode":I
    :goto_5
    invoke-static {v5}, Ljava/lang/System;->exit(I)V

    .line 1093
    return-void

    .line 1042
    .end local v1    # "checker":Lorg/apache/lucene/index/CheckIndex;
    .end local v2    # "dir":Lorg/apache/lucene/store/Directory;
    .end local v5    # "exitCode":I
    .end local v9    # "result":Lorg/apache/lucene/index/CheckIndex$Status;
    :cond_c
    if-eqz v4, :cond_9

    .line 1043
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v13, "ERROR: cannot specify both -fix and -segment"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1044
    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/System;->exit(I)V

    goto/16 :goto_2

    .line 1053
    .restart local v2    # "dir":Lorg/apache/lucene/store/Directory;
    :cond_d
    :try_start_1
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v12}, Lorg/apache/lucene/util/CommandLineUtil;->newFSDirectory(Ljava/lang/String;Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    goto :goto_3

    .line 1055
    :catch_0
    move-exception v11

    .line 1056
    .local v11, "t":Ljava/lang/Throwable;
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "ERROR: could not open directory \""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "\"; exiting"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1057
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v11, v12}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 1058
    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/System;->exit(I)V

    goto/16 :goto_3

    .line 1073
    .end local v11    # "t":Ljava/lang/Throwable;
    .restart local v1    # "checker":Lorg/apache/lucene/index/CheckIndex;
    .restart local v9    # "result":Lorg/apache/lucene/index/CheckIndex$Status;
    :cond_e
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "WARNING: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v9, Lorg/apache/lucene/index/CheckIndex$Status;->totLoseDocCount:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " documents will be lost\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1074
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "NOTE: will write new segments file in 5 seconds; this will remove "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v14, v9, Lorg/apache/lucene/index/CheckIndex$Status;->totLoseDocCount:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " docs from the index. THIS IS YOUR LAST CHANCE TO CTRL+C!"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1075
    const/4 v10, 0x0

    .local v10, "s":I
    :goto_6
    const/4 v12, 0x5

    if-ge v10, v12, :cond_f

    .line 1076
    const-wide/16 v12, 0x3e8

    invoke-static {v12, v13}, Ljava/lang/Thread;->sleep(J)V

    .line 1077
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "  "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    rsub-int/lit8 v14, v10, 0x5

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "..."

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1075
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 1079
    :cond_f
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v13, "Writing..."

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1080
    invoke-virtual {v1, v9}, Lorg/apache/lucene/index/CheckIndex;->fixIndex(Lorg/apache/lucene/index/CheckIndex$Status;)V

    .line 1081
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v13, "OK"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1082
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Wrote new segments file \""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v9, Lorg/apache/lucene/index/CheckIndex$Status;->newSegments:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v14}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "\""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1091
    .end local v10    # "s":I
    :cond_10
    const/4 v5, 0x1

    .restart local v5    # "exitCode":I
    goto/16 :goto_5
.end method

.method private msg(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 271
    iget-object v0, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 273
    :cond_0
    return-void
.end method

.method private static testAsserts()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 934
    sput-boolean v0, Lorg/apache/lucene/index/CheckIndex;->assertsOn:Z

    .line 935
    return v0
.end method

.method private testFieldNorms(Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentReader;)Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;
    .locals 10
    .param p1, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p2, "reader"    # Lorg/apache/lucene/index/SegmentReader;

    .prologue
    .line 642
    new-instance v4, Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;

    invoke-direct {v4}, Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;-><init>()V

    .line 646
    .local v4, "status":Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;
    :try_start_0
    iget-object v5, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    if-eqz v5, :cond_0

    .line 647
    iget-object v5, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    const-string/jumbo v6, "    test: field norms........."

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 649
    :cond_0
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v5

    new-array v0, v5, [B

    .line 650
    .local v0, "b":[B
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/FieldInfo;

    .line 651
    .local v2, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    iget-object v5, v2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p2, v5}, Lorg/apache/lucene/index/SegmentReader;->hasNorms(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 652
    iget-object v5, v2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {p2, v5, v0, v6}, Lorg/apache/lucene/index/SegmentReader;->norms(Ljava/lang/String;[BI)V

    .line 653
    iget-wide v6, v4, Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;->totFields:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v4, Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;->totFields:J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 658
    .end local v0    # "b":[B
    .end local v2    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v1

    .line 659
    .local v1, "e":Ljava/lang/Throwable;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "ERROR ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 660
    iput-object v1, v4, Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;->error:Ljava/lang/Throwable;

    .line 661
    iget-object v5, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    if-eqz v5, :cond_2

    .line 662
    iget-object v5, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    invoke-virtual {v1, v5}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 666
    .end local v1    # "e":Ljava/lang/Throwable;
    :cond_2
    :goto_1
    return-object v4

    .line 657
    .restart local v0    # "b":[B
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "OK ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v4, Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;->totFields:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " fields]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private testStoredFields(Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/SegmentReader;Ljava/text/NumberFormat;)Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;
    .locals 8
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "reader"    # Lorg/apache/lucene/index/SegmentReader;
    .param p3, "format"    # Ljava/text/NumberFormat;

    .prologue
    .line 843
    new-instance v3, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;

    invoke-direct {v3}, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;-><init>()V

    .line 846
    .local v3, "status":Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_0

    .line 847
    iget-object v4, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    const-string/jumbo v5, "    test: stored fields......."

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 851
    :cond_0
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    iget v4, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-ge v2, v4, :cond_2

    .line 852
    invoke-virtual {p2, v2}, Lorg/apache/lucene/index/SegmentReader;->document(I)Lorg/apache/lucene/document/Document;

    move-result-object v0

    .line 853
    .local v0, "doc":Lorg/apache/lucene/document/Document;
    invoke-virtual {p2, v2}, Lorg/apache/lucene/index/SegmentReader;->isDeleted(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 854
    iget v4, v3, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->docCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->docCount:I

    .line 855
    iget-wide v4, v3, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->totFields:J

    invoke-virtual {v0}, Lorg/apache/lucene/document/Document;->getFields()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    int-to-long v6, v6

    add-long/2addr v4, v6

    iput-wide v4, v3, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->totFields:J

    .line 851
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 860
    .end local v0    # "doc":Lorg/apache/lucene/document/Document;
    :cond_2
    iget v4, v3, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->docCount:I

    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentReader;->numDocs()I

    move-result v5

    if-eq v4, v5, :cond_4

    .line 861
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "docCount="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->docCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " but saw "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->docCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " undeleted docs"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 866
    .end local v2    # "j":I
    :catch_0
    move-exception v1

    .line 867
    .local v1, "e":Ljava/lang/Throwable;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "ERROR ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 868
    iput-object v1, v3, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->error:Ljava/lang/Throwable;

    .line 869
    iget-object v4, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_3

    .line 870
    iget-object v4, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    invoke-virtual {v1, v4}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 874
    .end local v1    # "e":Ljava/lang/Throwable;
    :cond_3
    :goto_1
    return-object v3

    .line 864
    .restart local v2    # "j":I
    :cond_4
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "OK ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, v3, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->totFields:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " total field count; avg "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, v3, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->totFields:J

    long-to-float v5, v6

    iget v6, v3, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->docCount:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    float-to-double v6, v5

    invoke-virtual {p3, v6, v7}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " fields per doc]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private testTermIndex(Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentReader;)Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;
    .locals 36
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p3, "reader"    # Lorg/apache/lucene/index/SegmentReader;

    .prologue
    .line 673
    new-instance v26, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;

    invoke-direct/range {v26 .. v26}, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;-><init>()V

    .line 675
    .local v26, "status":Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;
    new-instance v13, Lorg/apache/lucene/search/IndexSearcher;

    move-object/from16 v0, p3

    invoke-direct {v13, v0}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    .line 678
    .local v13, "is":Lorg/apache/lucene/search/IndexSearcher;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v32, v0

    if-eqz v32, :cond_0

    .line 679
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v32, v0

    const-string/jumbo v33, "    test: terms, freq, prox..."

    invoke-virtual/range {v32 .. v33}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 682
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentReader;->terms()Lorg/apache/lucene/index/TermEnum;

    move-result-object v28

    .line 683
    .local v28, "termEnum":Lorg/apache/lucene/index/TermEnum;
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentReader;->termPositions()Lorg/apache/lucene/index/TermPositions;

    move-result-object v29

    .line 686
    .local v29, "termPositions":Lorg/apache/lucene/index/TermPositions;
    new-instance v21, Lorg/apache/lucene/index/CheckIndex$MySegmentTermDocs;

    move-object/from16 v0, v21

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex$MySegmentTermDocs;-><init>(Lorg/apache/lucene/index/SegmentReader;)V

    .line 688
    .local v21, "myTermDocs":Lorg/apache/lucene/index/CheckIndex$MySegmentTermDocs;
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v20

    .line 689
    .local v20, "maxDoc":I
    const/16 v19, 0x0

    .line 690
    .local v19, "lastTerm":Lorg/apache/lucene/index/Term;
    const/16 v16, 0x0

    .line 691
    .local v16, "lastField":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {v28 .. v28}, Lorg/apache/lucene/index/TermEnum;->next()Z

    move-result v32

    if-eqz v32, :cond_19

    .line 692
    move-object/from16 v0, v26

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v32, v0

    const-wide/16 v34, 0x1

    add-long v32, v32, v34

    move-wide/from16 v0, v32

    move-object/from16 v2, v26

    iput-wide v0, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    .line 693
    invoke-virtual/range {v28 .. v28}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v27

    .line 695
    .local v27, "term":Lorg/apache/lucene/index/Term;
    if-eqz v19, :cond_3

    move-object/from16 v0, v27

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v32

    if-gtz v32, :cond_3

    .line 696
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "terms out of order: lastTerm="

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " term="

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 828
    .end local v16    # "lastField":Ljava/lang/String;
    .end local v19    # "lastTerm":Lorg/apache/lucene/index/Term;
    .end local v20    # "maxDoc":I
    .end local v21    # "myTermDocs":Lorg/apache/lucene/index/CheckIndex$MySegmentTermDocs;
    .end local v27    # "term":Lorg/apache/lucene/index/Term;
    .end local v28    # "termEnum":Lorg/apache/lucene/index/TermEnum;
    .end local v29    # "termPositions":Lorg/apache/lucene/index/TermPositions;
    :catch_0
    move-exception v8

    .line 829
    .local v8, "e":Ljava/lang/Throwable;
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v33, "ERROR ["

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual {v8}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string/jumbo v33, "]"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 830
    move-object/from16 v0, v26

    iput-object v8, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->error:Ljava/lang/Throwable;

    .line 831
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v32, v0

    if-eqz v32, :cond_2

    .line 832
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v8, v0}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 836
    .end local v8    # "e":Ljava/lang/Throwable;
    :cond_2
    :goto_0
    return-object v26

    .line 698
    .restart local v16    # "lastField":Ljava/lang/String;
    .restart local v19    # "lastTerm":Lorg/apache/lucene/index/Term;
    .restart local v20    # "maxDoc":I
    .restart local v21    # "myTermDocs":Lorg/apache/lucene/index/CheckIndex$MySegmentTermDocs;
    .restart local v27    # "term":Lorg/apache/lucene/index/Term;
    .restart local v28    # "termEnum":Lorg/apache/lucene/index/TermEnum;
    .restart local v29    # "termPositions":Lorg/apache/lucene/index/TermPositions;
    :cond_3
    move-object/from16 v19, v27

    .line 700
    :try_start_1
    move-object/from16 v0, v27

    iget-object v0, v0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v16

    if-eq v0, v1, :cond_6

    .line 702
    move-object/from16 v0, v27

    iget-object v0, v0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v9

    .line 703
    .local v9, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-nez v9, :cond_4

    .line 704
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "terms inconsistent with fieldInfos, no fieldInfos for: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v27

    iget-object v0, v0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 706
    :cond_4
    iget-boolean v0, v9, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    move/from16 v32, v0

    if-nez v32, :cond_5

    .line 707
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "terms inconsistent with fieldInfos, isIndexed == false for: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v27

    iget-object v0, v0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 709
    :cond_5
    move-object/from16 v0, v27

    iget-object v0, v0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 711
    .end local v9    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :cond_6
    invoke-virtual/range {v28 .. v28}, Lorg/apache/lucene/index/TermEnum;->docFreq()I

    move-result v6

    .line 712
    .local v6, "docFreq":I
    if-gtz v6, :cond_7

    .line 713
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "docfreq: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " is out of bounds"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 715
    :cond_7
    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Lorg/apache/lucene/index/TermPositions;->seek(Lorg/apache/lucene/index/Term;)V

    .line 716
    const/4 v15, -0x1

    .line 717
    .local v15, "lastDoc":I
    const/4 v11, 0x0

    .line 718
    .local v11, "freq0":I
    move-object/from16 v0, v26

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totFreq:J

    move-wide/from16 v32, v0

    int-to-long v0, v6

    move-wide/from16 v34, v0

    add-long v32, v32, v34

    move-wide/from16 v0, v32

    move-object/from16 v2, v26

    iput-wide v0, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totFreq:J

    .line 719
    :cond_8
    invoke-interface/range {v29 .. v29}, Lorg/apache/lucene/index/TermPositions;->next()Z

    move-result v32

    if-eqz v32, :cond_e

    .line 720
    add-int/lit8 v11, v11, 0x1

    .line 721
    invoke-interface/range {v29 .. v29}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v5

    .line 722
    .local v5, "doc":I
    invoke-interface/range {v29 .. v29}, Lorg/apache/lucene/index/TermPositions;->freq()I

    move-result v10

    .line 723
    .local v10, "freq":I
    if-gt v5, v15, :cond_9

    .line 724
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "term "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, ": doc "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " <= lastDoc "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 725
    :cond_9
    move/from16 v0, v20

    if-lt v5, v0, :cond_a

    .line 726
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "term "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, ": doc "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " >= maxDoc "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 728
    :cond_a
    move v15, v5

    .line 729
    if-gtz v10, :cond_b

    .line 730
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "term "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, ": doc "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, ": freq "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " is out of bounds"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 732
    :cond_b
    const/16 v17, -0x1

    .line 733
    .local v17, "lastPos":I
    move-object/from16 v0, v26

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totPos:J

    move-wide/from16 v32, v0

    int-to-long v0, v10

    move-wide/from16 v34, v0

    add-long v32, v32, v34

    move-wide/from16 v0, v32

    move-object/from16 v2, v26

    iput-wide v0, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totPos:J

    .line 734
    const/4 v14, 0x0

    .local v14, "j":I
    :goto_1
    if-ge v14, v10, :cond_8

    .line 735
    invoke-interface/range {v29 .. v29}, Lorg/apache/lucene/index/TermPositions;->nextPosition()I

    move-result v23

    .line 741
    .local v23, "pos":I
    const/16 v32, -0x1

    move/from16 v0, v23

    move/from16 v1, v32

    if-ge v0, v1, :cond_c

    .line 742
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "term "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, ": doc "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, ": pos "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " is out of bounds"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 744
    :cond_c
    move/from16 v0, v23

    move/from16 v1, v17

    if-ge v0, v1, :cond_d

    .line 745
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "term "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, ": doc "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, ": pos "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " < lastPos "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 747
    :cond_d
    move/from16 v17, v23

    .line 734
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_1

    .line 752
    .end local v5    # "doc":I
    .end local v10    # "freq":I
    .end local v14    # "j":I
    .end local v17    # "lastPos":I
    .end local v23    # "pos":I
    :cond_e
    const/4 v12, 0x0

    .local v12, "idx":I
    :goto_2
    const/16 v32, 0x7

    move/from16 v0, v32

    if-ge v12, v0, :cond_f

    .line 753
    add-int/lit8 v32, v12, 0x1

    move/from16 v0, v32

    int-to-long v0, v0

    move-wide/from16 v32, v0

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v34, v0

    mul-long v32, v32, v34

    const-wide/16 v34, 0x8

    div-long v32, v32, v34

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v25, v0

    .line 754
    .local v25, "skipDocID":I
    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Lorg/apache/lucene/index/TermPositions;->seek(Lorg/apache/lucene/index/Term;)V

    .line 755
    move-object/from16 v0, v29

    move/from16 v1, v25

    invoke-interface {v0, v1}, Lorg/apache/lucene/index/TermPositions;->skipTo(I)Z

    move-result v32

    if-nez v32, :cond_11

    .line 798
    .end local v25    # "skipDocID":I
    :cond_f
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentReader;->hasDeletions()Z

    move-result v32

    if-eqz v32, :cond_18

    .line 799
    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/CheckIndex$MySegmentTermDocs;->seek(Lorg/apache/lucene/index/Term;)V

    .line 800
    :cond_10
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/index/CheckIndex$MySegmentTermDocs;->next()Z

    move-result v32

    if-nez v32, :cond_10

    .line 801
    move-object/from16 v0, v21

    iget v4, v0, Lorg/apache/lucene/index/CheckIndex$MySegmentTermDocs;->delCount:I

    .line 806
    .local v4, "delCount":I
    :goto_3
    add-int v32, v11, v4

    move/from16 v0, v32

    if-eq v0, v6, :cond_1

    .line 807
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "term "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " docFreq="

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " != num docs seen "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " + num docs deleted "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 759
    .end local v4    # "delCount":I
    .restart local v25    # "skipDocID":I
    :cond_11
    invoke-interface/range {v29 .. v29}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v7

    .line 760
    .local v7, "docID":I
    move/from16 v0, v25

    if-ge v7, v0, :cond_12

    .line 761
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "term "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, ": skipTo(docID="

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, ") returned docID="

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 763
    :cond_12
    invoke-interface/range {v29 .. v29}, Lorg/apache/lucene/index/TermPositions;->freq()I

    move-result v10

    .line 764
    .restart local v10    # "freq":I
    if-gtz v10, :cond_13

    .line 765
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "termFreq "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " is out of bounds"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 767
    :cond_13
    const/16 v18, -0x1

    .line 768
    .local v18, "lastPosition":I
    const/16 v24, 0x0

    .local v24, "posUpto":I
    :goto_4
    move/from16 v0, v24

    if-ge v0, v10, :cond_16

    .line 769
    invoke-interface/range {v29 .. v29}, Lorg/apache/lucene/index/TermPositions;->nextPosition()I

    move-result v23

    .line 775
    .restart local v23    # "pos":I
    const/16 v32, -0x1

    move/from16 v0, v23

    move/from16 v1, v32

    if-ge v0, v1, :cond_14

    .line 776
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "position "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " is out of bounds"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 779
    :cond_14
    move/from16 v0, v23

    move/from16 v1, v18

    if-ge v0, v1, :cond_15

    .line 780
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "position "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " is < lastPosition "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 782
    :cond_15
    move/from16 v18, v23

    .line 768
    add-int/lit8 v24, v24, 0x1

    goto :goto_4

    .line 785
    .end local v23    # "pos":I
    :cond_16
    invoke-interface/range {v29 .. v29}, Lorg/apache/lucene/index/TermPositions;->next()Z

    move-result v32

    if-eqz v32, :cond_f

    .line 788
    invoke-interface/range {v29 .. v29}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v22

    .line 789
    .local v22, "nextDocID":I
    move/from16 v0, v22

    if-gt v0, v7, :cond_17

    .line 790
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "term "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, ": skipTo(docID="

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, "), then .next() returned docID="

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " vs prev docID="

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32

    .line 752
    :cond_17
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2

    .line 803
    .end local v7    # "docID":I
    .end local v10    # "freq":I
    .end local v18    # "lastPosition":I
    .end local v22    # "nextDocID":I
    .end local v24    # "posUpto":I
    .end local v25    # "skipDocID":I
    :cond_18
    const/4 v4, 0x0

    .restart local v4    # "delCount":I
    goto/16 :goto_3

    .line 813
    .end local v4    # "delCount":I
    .end local v6    # "docFreq":I
    .end local v11    # "freq0":I
    .end local v12    # "idx":I
    .end local v15    # "lastDoc":I
    .end local v27    # "term":Lorg/apache/lucene/index/Term;
    :cond_19
    if-eqz v19, :cond_1a

    .line 814
    new-instance v32, Lorg/apache/lucene/search/TermQuery;

    move-object/from16 v0, v32

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    const/16 v33, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v33

    invoke-virtual {v13, v0, v1}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Query;I)Lorg/apache/lucene/search/TopDocs;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 818
    :cond_1a
    :try_start_2
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentReader;->getUniqueTermCount()J

    move-result-wide v30

    .line 819
    .local v30, "uniqueTermCountAllFields":J
    move-object/from16 v0, v26

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v32, v0

    cmp-long v32, v32, v30

    if-eqz v32, :cond_1b

    .line 820
    new-instance v32, Ljava/lang/RuntimeException;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "termCount mismatch "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string/jumbo v34, " vs "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v26

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v34, v0

    invoke-virtual/range {v33 .. v35}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v32 .. v33}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v32
    :try_end_2
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 822
    .end local v30    # "uniqueTermCountAllFields":J
    :catch_1
    move-exception v32

    .line 826
    :cond_1b
    :try_start_3
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v33, "OK ["

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v26

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v34, v0

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string/jumbo v33, " terms; "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v26

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totFreq:J

    move-wide/from16 v34, v0

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string/jumbo v33, " terms/docs pairs; "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v26

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totPos:J

    move-wide/from16 v34, v0

    move-object/from16 v0, v32

    move-wide/from16 v1, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string/jumbo v33, " tokens]"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method

.method private testTermVectors(Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/SegmentReader;Ljava/text/NumberFormat;)Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;
    .locals 8
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "reader"    # Lorg/apache/lucene/index/SegmentReader;
    .param p3, "format"    # Ljava/text/NumberFormat;

    .prologue
    .line 881
    new-instance v2, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;

    invoke-direct {v2}, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;-><init>()V

    .line 884
    .local v2, "status":Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_0

    .line 885
    iget-object v4, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    const-string/jumbo v5, "    test: term vectors........"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 888
    :cond_0
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    iget v4, p1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-ge v1, v4, :cond_2

    .line 889
    invoke-virtual {p2, v1}, Lorg/apache/lucene/index/SegmentReader;->isDeleted(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 890
    iget v4, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->docCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->docCount:I

    .line 891
    invoke-virtual {p2, v1}, Lorg/apache/lucene/index/SegmentReader;->getTermFreqVectors(I)[Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v3

    .line 892
    .local v3, "tfv":[Lorg/apache/lucene/index/TermFreqVector;
    if-eqz v3, :cond_1

    .line 893
    iget-wide v4, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->totVectors:J

    array-length v6, v3

    int-to-long v6, v6

    add-long/2addr v4, v6

    iput-wide v4, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->totVectors:J

    .line 888
    .end local v3    # "tfv":[Lorg/apache/lucene/index/TermFreqVector;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 898
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "OK ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->totVectors:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " total vector count; avg "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->totVectors:J

    long-to-float v5, v6

    iget v6, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->docCount:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    float-to-double v6, v5

    invoke-virtual {p3, v6, v7}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " term/freq vector fields per doc]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 908
    .end local v1    # "j":I
    :cond_3
    :goto_1
    return-object v2

    .line 900
    :catch_0
    move-exception v0

    .line 901
    .local v0, "e":Ljava/lang/Throwable;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "ERROR ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 902
    iput-object v0, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->error:Ljava/lang/Throwable;

    .line 903
    iget-object v4, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    if-eqz v4, :cond_3

    .line 904
    iget-object v4, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    invoke-virtual {v0, v4}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_1
.end method


# virtual methods
.method public checkIndex()Lorg/apache/lucene/index/CheckIndex$Status;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 305
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/CheckIndex;->checkIndex(Ljava/util/List;)Lorg/apache/lucene/index/CheckIndex$Status;

    move-result-object v0

    return-object v0
.end method

.method public checkIndex(Ljava/util/List;)Lorg/apache/lucene/index/CheckIndex$Status;
    .locals 44
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/lucene/index/CheckIndex$Status;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 321
    .local p1, "onlySegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v17

    .line 322
    .local v17, "nf":Ljava/text/NumberFormat;
    new-instance v31, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct/range {v31 .. v31}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 323
    .local v31, "sis":Lorg/apache/lucene/index/SegmentInfos;
    new-instance v24, Lorg/apache/lucene/index/CheckIndex$Status;

    invoke-direct/range {v24 .. v24}, Lorg/apache/lucene/index/CheckIndex$Status;-><init>()V

    .line 324
    .local v24, "result":Lorg/apache/lucene/index/CheckIndex$Status;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->dir:Lorg/apache/lucene/store/Directory;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v24

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->dir:Lorg/apache/lucene/store/Directory;

    .line 326
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->dir:Lorg/apache/lucene/store/Directory;

    move-object/from16 v39, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    const v39, 0x7fffffff

    invoke-static/range {v39 .. v39}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v22

    .local v22, "oldest":Ljava/lang/String;
    const/high16 v39, -0x80000000

    invoke-static/range {v39 .. v39}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v16

    .line 337
    .local v16, "newest":Ljava/lang/String;
    const/16 v21, 0x0

    .line 338
    .local v21, "oldSegs":Ljava/lang/String;
    const/4 v10, 0x0

    .line 339
    .local v10, "foundNonNullVersion":Z
    invoke-static {}, Lorg/apache/lucene/util/StringHelper;->getVersionComparator()Ljava/util/Comparator;

    move-result-object v37

    .line 340
    .local v37, "versionComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Ljava/lang/String;>;"
    invoke-virtual/range {v31 .. v31}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v39

    if-eqz v39, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lorg/apache/lucene/index/SegmentInfo;

    .line 341
    .local v30, "si":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual/range {v30 .. v30}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v36

    .line 342
    .local v36, "version":Ljava/lang/String;
    if-nez v36, :cond_2

    .line 344
    const-string/jumbo v21, "pre-3.1"

    goto :goto_0

    .line 327
    .end local v10    # "foundNonNullVersion":Z
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v16    # "newest":Ljava/lang/String;
    .end local v21    # "oldSegs":Ljava/lang/String;
    .end local v22    # "oldest":Ljava/lang/String;
    .end local v30    # "si":Lorg/apache/lucene/index/SegmentInfo;
    .end local v36    # "version":Ljava/lang/String;
    .end local v37    # "versionComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Ljava/lang/String;>;"
    :catch_0
    move-exception v33

    .line 328
    .local v33, "t":Ljava/lang/Throwable;
    const-string/jumbo v39, "ERROR: could not read any segments file in directory"

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 329
    const/16 v39, 0x1

    move/from16 v0, v39

    move-object/from16 v1, v24

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->missingSegments:Z

    .line 330
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    if-eqz v39, :cond_1

    .line 331
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    move-object/from16 v0, v33

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 635
    .end local v33    # "t":Ljava/lang/Throwable;
    :cond_1
    :goto_1
    return-object v24

    .line 345
    .restart local v10    # "foundNonNullVersion":Z
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v16    # "newest":Ljava/lang/String;
    .restart local v21    # "oldSegs":Ljava/lang/String;
    .restart local v22    # "oldest":Ljava/lang/String;
    .restart local v30    # "si":Lorg/apache/lucene/index/SegmentInfo;
    .restart local v36    # "version":Ljava/lang/String;
    .restart local v37    # "versionComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Ljava/lang/String;>;"
    :cond_2
    const-string/jumbo v39, "2.x"

    move-object/from16 v0, v36

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_3

    .line 347
    const-string/jumbo v21, "2.x"

    goto :goto_0

    .line 349
    :cond_3
    const/4 v10, 0x1

    .line 350
    move-object/from16 v0, v37

    move-object/from16 v1, v36

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v39

    if-gez v39, :cond_4

    .line 351
    move-object/from16 v22, v36

    .line 353
    :cond_4
    move-object/from16 v0, v37

    move-object/from16 v1, v36

    move-object/from16 v2, v16

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v39

    if-lez v39, :cond_0

    .line 354
    move-object/from16 v16, v36

    goto :goto_0

    .line 359
    .end local v30    # "si":Lorg/apache/lucene/index/SegmentInfo;
    .end local v36    # "version":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {v31 .. v31}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v20

    .line 360
    .local v20, "numSegments":I
    invoke-virtual/range {v31 .. v31}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v29

    .line 361
    .local v29, "segmentsFileName":Ljava/lang/String;
    const/4 v14, 0x0

    .line 363
    .local v14, "input":Lorg/apache/lucene/store/IndexInput;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->dir:Lorg/apache/lucene/store/Directory;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v14

    .line 371
    const/4 v9, 0x0

    .line 373
    .local v9, "format":I
    :try_start_2
    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->readInt()I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v9

    .line 381
    if-eqz v14, :cond_6

    .line 382
    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 385
    :cond_6
    const-string/jumbo v26, ""

    .line 386
    .local v26, "sFormat":Ljava/lang/String;
    const/16 v32, 0x0

    .line 388
    .local v32, "skip":Z
    const/16 v39, -0x1

    move/from16 v0, v39

    if-ne v9, v0, :cond_7

    .line 389
    const-string/jumbo v26, "FORMAT [Lucene Pre-2.1]"

    .line 390
    :cond_7
    const/16 v39, -0x2

    move/from16 v0, v39

    if-ne v9, v0, :cond_d

    .line 391
    const-string/jumbo v26, "FORMAT_LOCKLESS [Lucene 2.1]"

    .line 421
    :goto_2
    move-object/from16 v0, v29

    move-object/from16 v1, v24

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->segmentsFileName:Ljava/lang/String;

    .line 422
    move/from16 v0, v20

    move-object/from16 v1, v24

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->numSegments:I

    .line 423
    move-object/from16 v0, v26

    move-object/from16 v1, v24

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->segmentFormat:Ljava/lang/String;

    .line 424
    invoke-virtual/range {v31 .. v31}, Lorg/apache/lucene/index/SegmentInfos;->getUserData()Ljava/util/Map;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v24

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->userData:Ljava/util/Map;

    .line 426
    invoke-virtual/range {v31 .. v31}, Lorg/apache/lucene/index/SegmentInfos;->getUserData()Ljava/util/Map;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Ljava/util/Map;->size()I

    move-result v39

    if-lez v39, :cond_19

    .line 427
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, " userData="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v31 .. v31}, Lorg/apache/lucene/index/SegmentInfos;->getUserData()Ljava/util/Map;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    .line 432
    .local v35, "userDataString":Ljava/lang/String;
    :goto_3
    const/16 v38, 0x0

    .line 433
    .local v38, "versionString":Ljava/lang/String;
    if-eqz v21, :cond_1b

    .line 434
    if-eqz v10, :cond_1a

    .line 435
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "versions=["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, " .. "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, "]"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    .line 443
    :goto_4
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "Segments file="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, " numSegments="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, " "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, " format="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 446
    if-eqz p1, :cond_1e

    .line 447
    const/16 v39, 0x1

    move/from16 v0, v39

    move-object/from16 v1, v24

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->partial:Z

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    if-eqz v39, :cond_8

    .line 449
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    const-string/jumbo v40, "\nChecking only these segments:"

    invoke-virtual/range {v39 .. v40}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 450
    :cond_8
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_9
    :goto_5
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v39

    if-eqz v39, :cond_1d

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    .line 451
    .local v25, "s":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    if-eqz v39, :cond_9

    .line 452
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v41, " "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_5

    .line 364
    .end local v9    # "format":I
    .end local v25    # "s":Ljava/lang/String;
    .end local v26    # "sFormat":Ljava/lang/String;
    .end local v32    # "skip":Z
    .end local v35    # "userDataString":Ljava/lang/String;
    .end local v38    # "versionString":Ljava/lang/String;
    :catch_1
    move-exception v33

    .line 365
    .restart local v33    # "t":Ljava/lang/Throwable;
    const-string/jumbo v39, "ERROR: could not open segments file in directory"

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 366
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    if-eqz v39, :cond_a

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    move-object/from16 v0, v33

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 368
    :cond_a
    const/16 v39, 0x1

    move/from16 v0, v39

    move-object/from16 v1, v24

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->cantOpenSegments:Z

    goto/16 :goto_1

    .line 374
    .end local v33    # "t":Ljava/lang/Throwable;
    .restart local v9    # "format":I
    :catch_2
    move-exception v33

    .line 375
    .restart local v33    # "t":Ljava/lang/Throwable;
    :try_start_3
    const-string/jumbo v39, "ERROR: could not read segment file version in directory"

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 376
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    if-eqz v39, :cond_b

    .line 377
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    move-object/from16 v0, v33

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 378
    :cond_b
    const/16 v39, 0x1

    move/from16 v0, v39

    move-object/from16 v1, v24

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->missingSegmentVersion:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 381
    if-eqz v14, :cond_1

    .line 382
    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->close()V

    goto/16 :goto_1

    .line 381
    .end local v33    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v39

    if-eqz v14, :cond_c

    .line 382
    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 381
    :cond_c
    throw v39

    .line 392
    .restart local v26    # "sFormat":Ljava/lang/String;
    .restart local v32    # "skip":Z
    :cond_d
    const/16 v39, -0x3

    move/from16 v0, v39

    if-ne v9, v0, :cond_e

    .line 393
    const-string/jumbo v26, "FORMAT_SINGLE_NORM_FILE [Lucene 2.2]"

    goto/16 :goto_2

    .line 394
    :cond_e
    const/16 v39, -0x4

    move/from16 v0, v39

    if-ne v9, v0, :cond_f

    .line 395
    const-string/jumbo v26, "FORMAT_SHARED_DOC_STORE [Lucene 2.3]"

    goto/16 :goto_2

    .line 397
    :cond_f
    const/16 v39, -0x5

    move/from16 v0, v39

    if-ne v9, v0, :cond_10

    .line 398
    const-string/jumbo v26, "FORMAT_CHECKSUM [Lucene 2.4]"

    goto/16 :goto_2

    .line 399
    :cond_10
    const/16 v39, -0x6

    move/from16 v0, v39

    if-ne v9, v0, :cond_11

    .line 400
    const-string/jumbo v26, "FORMAT_DEL_COUNT [Lucene 2.4]"

    goto/16 :goto_2

    .line 401
    :cond_11
    const/16 v39, -0x7

    move/from16 v0, v39

    if-ne v9, v0, :cond_12

    .line 402
    const-string/jumbo v26, "FORMAT_HAS_PROX [Lucene 2.4]"

    goto/16 :goto_2

    .line 403
    :cond_12
    const/16 v39, -0x8

    move/from16 v0, v39

    if-ne v9, v0, :cond_13

    .line 404
    const-string/jumbo v26, "FORMAT_USER_DATA [Lucene 2.9]"

    goto/16 :goto_2

    .line 405
    :cond_13
    const/16 v39, -0x9

    move/from16 v0, v39

    if-ne v9, v0, :cond_14

    .line 406
    const-string/jumbo v26, "FORMAT_DIAGNOSTICS [Lucene 2.9]"

    goto/16 :goto_2

    .line 407
    :cond_14
    const/16 v39, -0xa

    move/from16 v0, v39

    if-ne v9, v0, :cond_15

    .line 408
    const-string/jumbo v26, "FORMAT_HAS_VECTORS [Lucene 3.1]"

    goto/16 :goto_2

    .line 409
    :cond_15
    const/16 v39, -0xb

    move/from16 v0, v39

    if-ne v9, v0, :cond_16

    .line 410
    const-string/jumbo v26, "FORMAT_3_1 [Lucene 3.1+]"

    goto/16 :goto_2

    .line 411
    :cond_16
    const/16 v39, -0xb

    move/from16 v0, v39

    if-ne v9, v0, :cond_17

    .line 412
    new-instance v39, Ljava/lang/RuntimeException;

    const-string/jumbo v40, "BUG: You should update this tool!"

    invoke-direct/range {v39 .. v40}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v39

    .line 413
    :cond_17
    const/16 v39, -0xb

    move/from16 v0, v39

    if-ge v9, v0, :cond_18

    .line 414
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "int="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, " [newer version of Lucene than this tool]"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 415
    const/16 v32, 0x1

    goto/16 :goto_2

    .line 417
    :cond_18
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v39

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, " [Lucene 1.3 or prior]"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_2

    .line 429
    :cond_19
    const-string/jumbo v35, ""

    .restart local v35    # "userDataString":Ljava/lang/String;
    goto/16 :goto_3

    .line 437
    .restart local v38    # "versionString":Ljava/lang/String;
    :cond_1a
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "version="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    goto/16 :goto_4

    .line 440
    :cond_1b
    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_1c

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "version="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    :goto_6
    goto/16 :goto_4

    :cond_1c
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "versions=["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, " .. "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, "]"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    goto :goto_6

    .line 454
    :cond_1d
    move-object/from16 v0, v24

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->segmentsChecked:Ljava/util/List;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 455
    const-string/jumbo v39, ":"

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 458
    :cond_1e
    if-eqz v32, :cond_1f

    .line 459
    const-string/jumbo v39, "\nERROR: this index appears to be created by a newer version of Lucene than this tool was compiled on; please re-compile this tool on the matching version of Lucene; exiting"

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 460
    const/16 v39, 0x1

    move/from16 v0, v39

    move-object/from16 v1, v24

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->toolOutOfDate:Z

    goto/16 :goto_1

    .line 465
    :cond_1f
    invoke-virtual/range {v31 .. v31}, Lorg/apache/lucene/index/SegmentInfos;->clone()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v0, v39

    move-object/from16 v1, v24

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->newSegments:Lorg/apache/lucene/index/SegmentInfos;

    .line 466
    move-object/from16 v0, v24

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->newSegments:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lorg/apache/lucene/index/SegmentInfos;->clear()V

    .line 467
    const/16 v39, -0x1

    move/from16 v0, v39

    move-object/from16 v1, v24

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->maxSegmentName:I

    .line 469
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_7
    move/from16 v0, v20

    if-ge v11, v0, :cond_3a

    .line 470
    move-object/from16 v0, v31

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v13

    .line 471
    .local v13, "info":Lorg/apache/lucene/index/SegmentInfo;
    iget-object v0, v13, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v39

    const/16 v40, 0x24

    invoke-static/range {v39 .. v40}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v28

    .line 472
    .local v28, "segmentName":I
    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->maxSegmentName:I

    move/from16 v39, v0

    move/from16 v0, v28

    move/from16 v1, v39

    if-le v0, v1, :cond_20

    .line 473
    move/from16 v0, v28

    move-object/from16 v1, v24

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->maxSegmentName:I

    .line 475
    :cond_20
    if-eqz p1, :cond_22

    iget-object v0, v13, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v39

    if-nez v39, :cond_22

    .line 469
    :cond_21
    :goto_8
    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    .line 477
    :cond_22
    new-instance v27, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;

    invoke-direct/range {v27 .. v27}, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;-><init>()V

    .line 478
    .local v27, "segInfoStat":Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;
    move-object/from16 v0, v24

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->segmentInfos:Ljava/util/List;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 479
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "  "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    add-int/lit8 v40, v11, 0x1

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, " of "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, ": name="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    iget-object v0, v13, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, " docCount="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    iget v0, v13, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    move/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 480
    iget-object v0, v13, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v27

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->name:Ljava/lang/String;

    .line 481
    iget v0, v13, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    move/from16 v39, v0

    move/from16 v0, v39

    move-object/from16 v1, v27

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->docCount:I

    .line 483
    iget v0, v13, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    move/from16 v34, v0

    .line 485
    .local v34, "toLoseDocCount":I
    const/16 v23, 0x0

    .line 488
    .local v23, "reader":Lorg/apache/lucene/index/SegmentReader;
    :try_start_4
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "    compound="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 489
    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v39

    move/from16 v0, v39

    move-object/from16 v1, v27

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->compound:Z

    .line 490
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "    hasProx="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getHasProx()Z

    move-result v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 491
    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getHasProx()Z

    move-result v39

    move/from16 v0, v39

    move-object/from16 v1, v27

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->hasProx:Z

    .line 492
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "    numFiles="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v40

    invoke-interface/range {v40 .. v40}, Ljava/util/List;->size()I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 493
    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Ljava/util/List;->size()I

    move-result v39

    move/from16 v0, v39

    move-object/from16 v1, v27

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->numFiles:I

    .line 494
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v13, v0}, Lorg/apache/lucene/index/SegmentInfo;->sizeInBytes(Z)J

    move-result-wide v40

    move-wide/from16 v0, v40

    long-to-double v0, v0

    move-wide/from16 v40, v0

    const-wide/high16 v42, 0x4130000000000000L    # 1048576.0

    div-double v40, v40, v42

    move-wide/from16 v0, v40

    move-object/from16 v2, v27

    iput-wide v0, v2, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->sizeMB:D

    .line 495
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "    size (MB)="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v27

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->sizeMB:D

    move-wide/from16 v40, v0

    move-object/from16 v0, v17

    move-wide/from16 v1, v40

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 496
    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getDiagnostics()Ljava/util/Map;

    move-result-object v6

    .line 497
    .local v6, "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, v27

    iput-object v6, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->diagnostics:Ljava/util/Map;

    .line 498
    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v39

    if-lez v39, :cond_23

    .line 499
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "    diagnostics = "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 502
    :cond_23
    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreOffset()I

    move-result v7

    .line 503
    .local v7, "docStoreOffset":I
    const/16 v39, -0x1

    move/from16 v0, v39

    if-eq v7, v0, :cond_24

    .line 504
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "    docStoreOffset="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 505
    move-object/from16 v0, v27

    iput v7, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->docStoreOffset:I

    .line 506
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "    docStoreSegment="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreSegment()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 507
    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreSegment()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v27

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->docStoreSegment:Ljava/lang/String;

    .line 508
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "    docStoreIsCompoundFile="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreIsCompoundFile()Z

    move-result v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 509
    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getDocStoreIsCompoundFile()Z

    move-result v39

    move/from16 v0, v39

    move-object/from16 v1, v27

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->docStoreCompoundFile:Z

    .line 511
    :cond_24
    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getDelFileName()Ljava/lang/String;

    move-result-object v5

    .line 512
    .local v5, "delFileName":Ljava/lang/String;
    if-nez v5, :cond_27

    .line 513
    const-string/jumbo v39, "    no deletions"

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 514
    const/16 v39, 0x0

    move/from16 v0, v39

    move-object/from16 v1, v27

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->hasDeletions:Z

    .line 521
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    if-eqz v39, :cond_25

    .line 522
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    const-string/jumbo v40, "    test: open reader........."

    invoke-virtual/range {v39 .. v40}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 523
    :cond_25
    const/16 v39, 0x1

    sget v40, Lorg/apache/lucene/index/IndexReader;->DEFAULT_TERMS_INDEX_DIVISOR:I

    move/from16 v0, v39

    move/from16 v1, v40

    invoke-static {v0, v13, v1}, Lorg/apache/lucene/index/SegmentReader;->get(ZLorg/apache/lucene/index/SegmentInfo;I)Lorg/apache/lucene/index/SegmentReader;

    move-result-object v23

    .line 525
    const/16 v39, 0x1

    move/from16 v0, v39

    move-object/from16 v1, v27

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->openReaderPassed:Z

    .line 527
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/SegmentReader;->numDocs()I

    move-result v18

    .line 528
    .local v18, "numDocs":I
    move/from16 v34, v18

    .line 529
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/SegmentReader;->hasDeletions()Z

    move-result v39

    if-eqz v39, :cond_2f

    .line 530
    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v39

    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I

    move-result v40

    move/from16 v0, v39

    move/from16 v1, v40

    if-eq v0, v1, :cond_29

    .line 531
    new-instance v39, Ljava/lang/RuntimeException;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v41, "delete count mismatch: info="

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I

    move-result v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string/jumbo v41, " vs deletedDocs.count()="

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-direct/range {v39 .. v40}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v39
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 600
    .end local v5    # "delFileName":Ljava/lang/String;
    .end local v6    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "docStoreOffset":I
    .end local v18    # "numDocs":I
    :catch_3
    move-exception v33

    .line 601
    .restart local v33    # "t":Ljava/lang/Throwable;
    :try_start_5
    const-string/jumbo v39, "FAILED"

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 603
    const-string/jumbo v4, "fixIndex() would remove reference to this segment"

    .line 604
    .local v4, "comment":Ljava/lang/String;
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "    WARNING: "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, "; full exception:"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 605
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    if-eqz v39, :cond_26

    .line 606
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    move-object/from16 v0, v33

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 607
    :cond_26
    const-string/jumbo v39, ""

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 608
    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->totLoseDocCount:I

    move/from16 v39, v0

    add-int v39, v39, v34

    move/from16 v0, v39

    move-object/from16 v1, v24

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->totLoseDocCount:I

    .line 609
    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->numBadSegments:I

    move/from16 v39, v0

    add-int/lit8 v39, v39, 0x1

    move/from16 v0, v39

    move-object/from16 v1, v24

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->numBadSegments:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 612
    if-eqz v23, :cond_21

    .line 613
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/SegmentReader;->close()V

    goto/16 :goto_8

    .line 517
    .end local v4    # "comment":Ljava/lang/String;
    .end local v33    # "t":Ljava/lang/Throwable;
    .restart local v5    # "delFileName":Ljava/lang/String;
    .restart local v6    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v7    # "docStoreOffset":I
    :cond_27
    :try_start_6
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "    has deletions [delFileName="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, "]"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 518
    const/16 v39, 0x1

    move/from16 v0, v39

    move-object/from16 v1, v27

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->hasDeletions:Z

    .line 519
    move-object/from16 v0, v27

    iput-object v5, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->deletionsFileName:Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_9

    .line 612
    .end local v5    # "delFileName":Ljava/lang/String;
    .end local v6    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "docStoreOffset":I
    :catchall_1
    move-exception v39

    if-eqz v23, :cond_28

    .line 613
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/SegmentReader;->close()V

    .line 612
    :cond_28
    throw v39

    .line 533
    .restart local v5    # "delFileName":Ljava/lang/String;
    .restart local v6    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v7    # "docStoreOffset":I
    .restart local v18    # "numDocs":I
    :cond_29
    :try_start_7
    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v39

    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v40

    move/from16 v0, v39

    move/from16 v1, v40

    if-le v0, v1, :cond_2a

    .line 534
    new-instance v39, Ljava/lang/RuntimeException;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v41, "too many deleted docs: maxDoc()="

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string/jumbo v41, " vs deletedDocs.count()="

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-direct/range {v39 .. v40}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v39

    .line 536
    :cond_2a
    iget v0, v13, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    move/from16 v39, v0

    sub-int v39, v39, v18

    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I

    move-result v40

    move/from16 v0, v39

    move/from16 v1, v40

    if-eq v0, v1, :cond_2b

    .line 537
    new-instance v39, Ljava/lang/RuntimeException;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v41, "delete count mismatch: info="

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I

    move-result v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string/jumbo v41, " vs reader="

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    iget v0, v13, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    move/from16 v41, v0

    sub-int v41, v41, v18

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-direct/range {v39 .. v40}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v39

    .line 540
    :cond_2b
    const/16 v19, 0x0

    .line 541
    .local v19, "numLive":I
    const/4 v15, 0x0

    .local v15, "j":I
    :goto_a
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v39

    move/from16 v0, v39

    if-ge v15, v0, :cond_2d

    .line 542
    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Lorg/apache/lucene/index/SegmentReader;->isDeleted(I)Z

    move-result v39

    if-nez v39, :cond_2c

    .line 543
    add-int/lit8 v19, v19, 0x1

    .line 541
    :cond_2c
    add-int/lit8 v15, v15, 0x1

    goto :goto_a

    .line 546
    :cond_2d
    move/from16 v0, v19

    move/from16 v1, v18

    if-eq v0, v1, :cond_2e

    .line 547
    new-instance v39, Ljava/lang/RuntimeException;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v41, "liveDocs count mismatch: info="

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string/jumbo v41, ", vs bits="

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-direct/range {v39 .. v40}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v39

    .line 549
    :cond_2e
    iget v0, v13, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    move/from16 v39, v0

    sub-int v39, v39, v18

    move/from16 v0, v39

    move-object/from16 v1, v27

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->numDeleted:I

    .line 550
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "OK ["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v27

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->numDeleted:I

    move/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, " deleted docs]"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 563
    .end local v19    # "numLive":I
    :goto_b
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v39

    iget v0, v13, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    move/from16 v40, v0

    move/from16 v0, v39

    move/from16 v1, v40

    if-eq v0, v1, :cond_33

    .line 564
    new-instance v39, Ljava/lang/RuntimeException;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v41, "SegmentReader.maxDoc() "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string/jumbo v41, " != SegmentInfos.docCount "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    iget v0, v13, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    move/from16 v41, v0

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-direct/range {v39 .. v40}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v39

    .line 552
    .end local v15    # "j":I
    :cond_2f
    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I

    move-result v39

    if-eqz v39, :cond_30

    .line 553
    new-instance v39, Ljava/lang/RuntimeException;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v41, "delete count mismatch: info="

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I

    move-result v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string/jumbo v41, " vs reader="

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    iget v0, v13, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    move/from16 v41, v0

    sub-int v41, v41, v18

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-direct/range {v39 .. v40}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v39

    .line 556
    :cond_30
    const/4 v15, 0x0

    .restart local v15    # "j":I
    :goto_c
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v39

    move/from16 v0, v39

    if-ge v15, v0, :cond_32

    .line 557
    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Lorg/apache/lucene/index/SegmentReader;->isDeleted(I)Z

    move-result v39

    if-eqz v39, :cond_31

    .line 558
    new-instance v39, Ljava/lang/RuntimeException;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v41, "liveDocs mismatch: info says no deletions but doc "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string/jumbo v41, " is deleted."

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-direct/range {v39 .. v40}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v39

    .line 556
    :cond_31
    add-int/lit8 v15, v15, 0x1

    goto :goto_c

    .line 561
    :cond_32
    const-string/jumbo v39, "OK"

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 567
    :cond_33
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    if-eqz v39, :cond_34

    .line 568
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v39, v0

    const-string/jumbo v40, "    test: fields.............."

    invoke-virtual/range {v39 .. v40}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 570
    :cond_34
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/SegmentReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v8

    .line 571
    .local v8, "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "OK ["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v8}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, " fields]"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 572
    invoke-virtual {v8}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v39

    move/from16 v0, v39

    move-object/from16 v1, v27

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->numFields:I

    .line 575
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v8, v1}, Lorg/apache/lucene/index/CheckIndex;->testFieldNorms(Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentReader;)Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v27

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->fieldNormStatus:Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;

    .line 578
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v13, v8, v1}, Lorg/apache/lucene/index/CheckIndex;->testTermIndex(Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentReader;)Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v27

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->termIndexStatus:Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;

    .line 581
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v17

    invoke-direct {v0, v13, v1, v2}, Lorg/apache/lucene/index/CheckIndex;->testStoredFields(Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/SegmentReader;Ljava/text/NumberFormat;)Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v27

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->storedFieldStatus:Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;

    .line 584
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v17

    invoke-direct {v0, v13, v1, v2}, Lorg/apache/lucene/index/CheckIndex;->testTermVectors(Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/SegmentReader;Ljava/text/NumberFormat;)Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v27

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->termVectorStatus:Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;

    .line 588
    move-object/from16 v0, v27

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->fieldNormStatus:Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;->error:Ljava/lang/Throwable;

    move-object/from16 v39, v0

    if-eqz v39, :cond_35

    .line 589
    new-instance v39, Ljava/lang/RuntimeException;

    const-string/jumbo v40, "Field Norm test failed"

    invoke-direct/range {v39 .. v40}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v39

    .line 590
    :cond_35
    move-object/from16 v0, v27

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->termIndexStatus:Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->error:Ljava/lang/Throwable;

    move-object/from16 v39, v0

    if-eqz v39, :cond_36

    .line 591
    new-instance v39, Ljava/lang/RuntimeException;

    const-string/jumbo v40, "Term Index test failed"

    invoke-direct/range {v39 .. v40}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v39

    .line 592
    :cond_36
    move-object/from16 v0, v27

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->storedFieldStatus:Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->error:Ljava/lang/Throwable;

    move-object/from16 v39, v0

    if-eqz v39, :cond_37

    .line 593
    new-instance v39, Ljava/lang/RuntimeException;

    const-string/jumbo v40, "Stored Field test failed"

    invoke-direct/range {v39 .. v40}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v39

    .line 594
    :cond_37
    move-object/from16 v0, v27

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->termVectorStatus:Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->error:Ljava/lang/Throwable;

    move-object/from16 v39, v0

    if-eqz v39, :cond_38

    .line 595
    new-instance v39, Ljava/lang/RuntimeException;

    const-string/jumbo v40, "Term Vector test failed"

    invoke-direct/range {v39 .. v40}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v39

    .line 598
    :cond_38
    const-string/jumbo v39, ""

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 612
    if-eqz v23, :cond_39

    .line 613
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/SegmentReader;->close()V

    .line 617
    :cond_39
    move-object/from16 v0, v24

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->newSegments:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v40, v0

    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->clone()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v0, v40

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->add(Lorg/apache/lucene/index/SegmentInfo;)V

    goto/16 :goto_8

    .line 620
    .end local v5    # "delFileName":Ljava/lang/String;
    .end local v6    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "docStoreOffset":I
    .end local v8    # "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    .end local v13    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v15    # "j":I
    .end local v18    # "numDocs":I
    .end local v23    # "reader":Lorg/apache/lucene/index/SegmentReader;
    .end local v27    # "segInfoStat":Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;
    .end local v28    # "segmentName":I
    .end local v34    # "toLoseDocCount":I
    :cond_3a
    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->numBadSegments:I

    move/from16 v39, v0

    if-nez v39, :cond_3c

    .line 621
    const/16 v39, 0x1

    move/from16 v0, v39

    move-object/from16 v1, v24

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->clean:Z

    .line 625
    :goto_d
    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->maxSegmentName:I

    move/from16 v39, v0

    move-object/from16 v0, v31

    iget v0, v0, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    move/from16 v40, v0

    move/from16 v0, v39

    move/from16 v1, v40

    if-ge v0, v1, :cond_3d

    const/16 v39, 0x1

    :goto_e
    move/from16 v0, v39

    move-object/from16 v1, v24

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->validCounter:Z

    if-nez v39, :cond_3b

    .line 626
    const/16 v39, 0x0

    move/from16 v0, v39

    move-object/from16 v1, v24

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->clean:Z

    .line 627
    move-object/from16 v0, v24

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->newSegments:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v39, v0

    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->maxSegmentName:I

    move/from16 v40, v0

    add-int/lit8 v40, v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput v0, v1, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    .line 628
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "ERROR: Next segment name counter "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v31

    iget v0, v0, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    move/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, " is not greater than max segment name "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->maxSegmentName:I

    move/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    .line 631
    :cond_3b
    move-object/from16 v0, v24

    iget-boolean v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->clean:Z

    move/from16 v39, v0

    if-eqz v39, :cond_1

    .line 632
    const-string/jumbo v39, "No problems were detected with this index.\n"

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 623
    :cond_3c
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v40, "WARNING: "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->numBadSegments:I

    move/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, " broken segments (containing "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->totLoseDocCount:I

    move/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string/jumbo v40, " documents) detected"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/lang/String;)V

    goto/16 :goto_d

    .line 625
    :cond_3d
    const/16 v39, 0x0

    goto/16 :goto_e
.end method

.method public fixIndex(Lorg/apache/lucene/index/CheckIndex$Status;)V
    .locals 2
    .param p1, "result"    # Lorg/apache/lucene/index/CheckIndex$Status;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 925
    iget-boolean v0, p1, Lorg/apache/lucene/index/CheckIndex$Status;->partial:Z

    if-eqz v0, :cond_0

    .line 926
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "can only fix an index that was fully checked (this status checked a subset of segments)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 927
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/index/CheckIndex$Status;->newSegments:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->changed()V

    .line 928
    iget-object v0, p1, Lorg/apache/lucene/index/CheckIndex$Status;->newSegments:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v1, p1, Lorg/apache/lucene/index/CheckIndex$Status;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->commit(Lorg/apache/lucene/store/Directory;)V

    .line 929
    return-void
.end method

.method public setInfoStream(Ljava/io/PrintStream;)V
    .locals 0
    .param p1, "out"    # Ljava/io/PrintStream;

    .prologue
    .line 267
    iput-object p1, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    .line 268
    return-void
.end method
