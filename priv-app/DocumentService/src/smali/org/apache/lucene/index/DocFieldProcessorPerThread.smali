.class final Lorg/apache/lucene/index/DocFieldProcessorPerThread;
.super Lorg/apache/lucene/index/DocConsumerPerThread;
.source "DocFieldProcessorPerThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final fieldsComp:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/index/DocFieldProcessorPerField;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field allocCount:I

.field final consumer:Lorg/apache/lucene/index/DocFieldConsumerPerThread;

.field docBoost:F

.field final docFieldProcessor:Lorg/apache/lucene/index/DocFieldProcessor;

.field docFreeList:[Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;

.field final docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

.field fieldCount:I

.field fieldGen:I

.field fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

.field final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

.field final fieldsWriter:Lorg/apache/lucene/index/StoredFieldsWriterPerThread;

.field freeCount:I

.field hashMask:I

.field totalFieldCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->$assertionsDisabled:Z

    .line 300
    new-instance v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$1;

    invoke-direct {v0}, Lorg/apache/lucene/index/DocFieldProcessorPerThread$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldsComp:Ljava/util/Comparator;

    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriterThreadState;Lorg/apache/lucene/index/DocFieldProcessor;)V
    .locals 2
    .param p1, "threadState"    # Lorg/apache/lucene/index/DocumentsWriterThreadState;
    .param p2, "docFieldProcessor"    # Lorg/apache/lucene/index/DocFieldProcessor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 60
    invoke-direct {p0}, Lorg/apache/lucene/index/DocConsumerPerThread;-><init>()V

    .line 48
    new-array v0, v1, [Lorg/apache/lucene/index/DocFieldProcessorPerField;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 52
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/index/DocFieldProcessorPerField;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 53
    iput v1, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->hashMask:I

    .line 306
    new-array v0, v1, [Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docFreeList:[Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;

    .line 61
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterThreadState;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 62
    iput-object p2, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docFieldProcessor:Lorg/apache/lucene/index/DocFieldProcessor;

    .line 63
    iget-object v0, p2, Lorg/apache/lucene/index/DocFieldProcessor;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 64
    iget-object v0, p2, Lorg/apache/lucene/index/DocFieldProcessor;->consumer:Lorg/apache/lucene/index/DocFieldConsumer;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/DocFieldConsumer;->addThread(Lorg/apache/lucene/index/DocFieldProcessorPerThread;)Lorg/apache/lucene/index/DocFieldConsumerPerThread;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->consumer:Lorg/apache/lucene/index/DocFieldConsumerPerThread;

    .line 65
    iget-object v0, p2, Lorg/apache/lucene/index/DocFieldProcessor;->fieldsWriter:Lorg/apache/lucene/index/StoredFieldsWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/StoredFieldsWriter;->addThread(Lorg/apache/lucene/index/DocumentsWriter$DocState;)Lorg/apache/lucene/index/StoredFieldsWriterPerThread;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldsWriter:Lorg/apache/lucene/index/StoredFieldsWriterPerThread;

    .line 66
    return-void
.end method

.method private rehash()V
    .locals 8

    .prologue
    .line 163
    iget-object v7, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v7, v7

    mul-int/lit8 v5, v7, 0x2

    .line 164
    .local v5, "newHashSize":I
    sget-boolean v7, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->$assertionsDisabled:Z

    if-nez v7, :cond_0

    iget-object v7, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v7, v7

    if-gt v5, v7, :cond_0

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 166
    :cond_0
    new-array v3, v5, [Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 169
    .local v3, "newHashArray":[Lorg/apache/lucene/index/DocFieldProcessorPerField;
    add-int/lit8 v4, v5, -0x1

    .line 170
    .local v4, "newHashMask":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v7, v7

    if-ge v2, v7, :cond_2

    .line 171
    iget-object v7, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aget-object v0, v7, v2

    .line 172
    .local v0, "fp0":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :goto_1
    if-eqz v0, :cond_1

    .line 173
    iget-object v7, v0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v7, v7, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v7

    and-int v1, v7, v4

    .line 174
    .local v1, "hashPos2":I
    iget-object v6, v0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 175
    .local v6, "nextFP0":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    aget-object v7, v3, v1

    iput-object v7, v0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 176
    aput-object v0, v3, v1

    .line 177
    move-object v0, v6

    .line 178
    goto :goto_1

    .line 170
    .end local v1    # "hashPos2":I
    .end local v6    # "nextFP0":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 181
    .end local v0    # "fp0":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :cond_2
    iput-object v3, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 182
    iput v4, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->hashMask:I

    .line 183
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 8

    .prologue
    .line 70
    const/4 v6, 0x0

    .line 72
    .local v6, "th":Ljava/lang/Throwable;
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .local v0, "arr$":[Lorg/apache/lucene/index/DocFieldProcessorPerField;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 73
    .local v1, "field":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :goto_1
    if-eqz v1, :cond_1

    .line 74
    iget-object v4, v1, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 76
    .local v4, "next":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :try_start_0
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocFieldProcessorPerField;->abort()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :cond_0
    :goto_2
    move-object v1, v4

    .line 83
    goto :goto_1

    .line 77
    :catch_0
    move-exception v5

    .line 78
    .local v5, "t":Ljava/lang/Throwable;
    if-nez v6, :cond_0

    .line 79
    move-object v6, v5

    goto :goto_2

    .line 72
    .end local v4    # "next":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    .end local v5    # "t":Ljava/lang/Throwable;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 87
    .end local v1    # "field":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :cond_2
    :try_start_1
    iget-object v7, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldsWriter:Lorg/apache/lucene/index/StoredFieldsWriterPerThread;

    invoke-virtual {v7}, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->abort()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 95
    :cond_3
    :goto_3
    :try_start_2
    iget-object v7, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->consumer:Lorg/apache/lucene/index/DocFieldConsumerPerThread;

    invoke-virtual {v7}, Lorg/apache/lucene/index/DocFieldConsumerPerThread;->abort()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 103
    :cond_4
    :goto_4
    if-eqz v6, :cond_7

    .line 104
    instance-of v7, v6, Ljava/lang/RuntimeException;

    if-eqz v7, :cond_5

    check-cast v6, Ljava/lang/RuntimeException;

    .end local v6    # "th":Ljava/lang/Throwable;
    throw v6

    .line 88
    .restart local v6    # "th":Ljava/lang/Throwable;
    :catch_1
    move-exception v5

    .line 89
    .restart local v5    # "t":Ljava/lang/Throwable;
    if-nez v6, :cond_3

    .line 90
    move-object v6, v5

    goto :goto_3

    .line 96
    .end local v5    # "t":Ljava/lang/Throwable;
    :catch_2
    move-exception v5

    .line 97
    .restart local v5    # "t":Ljava/lang/Throwable;
    if-nez v6, :cond_4

    .line 98
    move-object v6, v5

    goto :goto_4

    .line 105
    .end local v5    # "t":Ljava/lang/Throwable;
    :cond_5
    instance-of v7, v6, Ljava/lang/Error;

    if-eqz v7, :cond_6

    check-cast v6, Ljava/lang/Error;

    .end local v6    # "th":Ljava/lang/Throwable;
    throw v6

    .line 107
    .restart local v6    # "th":Ljava/lang/Throwable;
    :cond_6
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 109
    :cond_7
    return-void
.end method

.method public fields()Ljava/util/Collection;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/DocFieldConsumerPerField;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 113
    .local v1, "fields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/DocFieldConsumerPerField;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 114
    iget-object v3, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aget-object v0, v3, v2

    .line 115
    .local v0, "field":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :goto_1
    if-eqz v0, :cond_0

    .line 116
    iget-object v3, v0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->consumer:Lorg/apache/lucene/index/DocFieldConsumerPerField;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 117
    iget-object v0, v0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    goto :goto_1

    .line 113
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 120
    .end local v0    # "field":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :cond_1
    sget-boolean v3, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->totalFieldCount:I

    if-eq v3, v4, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 121
    :cond_2
    return-object v1
.end method

.method declared-synchronized freePerDoc(Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;)V
    .locals 3
    .param p1, "perDoc"    # Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;

    .prologue
    .line 326
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->freeCount:I

    iget-object v1, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docFreeList:[Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 327
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docFreeList:[Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;

    iget v1, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->freeCount:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->freeCount:I

    aput-object p1, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 328
    monitor-exit p0

    return-void
.end method

.method declared-synchronized getPerDoc()Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;
    .locals 2

    .prologue
    .line 311
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->freeCount:I

    if-nez v0, :cond_2

    .line 312
    iget v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->allocCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->allocCount:I

    .line 313
    iget v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->allocCount:I

    iget-object v1, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docFreeList:[Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;

    array-length v1, v1

    if-le v0, v1, :cond_1

    .line 317
    sget-boolean v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->allocCount:I

    iget-object v1, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docFreeList:[Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 318
    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->allocCount:I

    sget v1, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    new-array v0, v0, [Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docFreeList:[Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;

    .line 320
    :cond_1
    new-instance v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;-><init>(Lorg/apache/lucene/index/DocFieldProcessorPerThread;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 322
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_2
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docFreeList:[Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;

    iget v1, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->freeCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->freeCount:I

    aget-object v0, v0, v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public processDocument()Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 188
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->consumer:Lorg/apache/lucene/index/DocFieldConsumerPerThread;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocFieldConsumerPerThread;->startDocument()V

    .line 189
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldsWriter:Lorg/apache/lucene/index/StoredFieldsWriterPerThread;

    invoke-virtual {v2}, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->startDocument()V

    .line 191
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget-object v11, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->doc:Lorg/apache/lucene/document/Document;

    .line 193
    .local v11, "doc":Lorg/apache/lucene/document/Document;
    sget-boolean v2, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docFieldProcessor:Lorg/apache/lucene/index/DocFieldProcessor;

    iget-object v2, v2, Lorg/apache/lucene/index/DocFieldProcessor;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    const-string/jumbo v4, "DocumentsWriter.ThreadState.init start"

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 195
    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldCount:I

    .line 197
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldGen:I

    move/from16 v22, v0

    add-int/lit8 v2, v22, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldGen:I

    .line 199
    .local v22, "thisFieldGen":I
    invoke-virtual {v11}, Lorg/apache/lucene/document/Document;->getFields()Ljava/util/List;

    move-result-object v12

    .line 200
    .local v12, "docFields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/document/Fieldable;>;"
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v20

    .line 207
    .local v20, "numDocFields":I
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    move/from16 v0, v17

    move/from16 v1, v20

    if-ge v0, v1, :cond_8

    .line 208
    move/from16 v0, v17

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/lucene/document/Fieldable;

    .line 209
    .local v14, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v14}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v3

    .line 212
    .local v3, "fieldName":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->hashMask:I

    and-int v16, v2, v4

    .line 213
    .local v16, "hashPos":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aget-object v15, v2, v16

    .line 214
    .local v15, "fp":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :goto_1
    if-eqz v15, :cond_1

    iget-object v2, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 215
    iget-object v15, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    goto :goto_1

    .line 217
    :cond_1
    if-nez v15, :cond_7

    .line 224
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-interface {v14}, Lorg/apache/lucene/document/Fieldable;->isIndexed()Z

    move-result v4

    invoke-interface {v14}, Lorg/apache/lucene/document/Fieldable;->isTermVectorStored()Z

    move-result v5

    invoke-interface {v14}, Lorg/apache/lucene/document/Fieldable;->getOmitNorms()Z

    move-result v6

    const/4 v7, 0x0

    invoke-interface {v14}, Lorg/apache/lucene/document/Fieldable;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v8

    invoke-virtual/range {v2 .. v8}, Lorg/apache/lucene/index/FieldInfos;->add(Ljava/lang/String;ZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v13

    .line 227
    .local v13, "fi":Lorg/apache/lucene/index/FieldInfo;
    new-instance v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .end local v15    # "fp":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    move-object/from16 v0, p0

    invoke-direct {v15, v0, v13}, Lorg/apache/lucene/index/DocFieldProcessorPerField;-><init>(Lorg/apache/lucene/index/DocFieldProcessorPerThread;Lorg/apache/lucene/index/FieldInfo;)V

    .line 228
    .restart local v15    # "fp":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aget-object v2, v2, v16

    iput-object v2, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 229
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aput-object v15, v2, v16

    .line 230
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->totalFieldCount:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->totalFieldCount:I

    .line 232
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->totalFieldCount:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v4, v4

    div-int/lit8 v4, v4, 0x2

    if-lt v2, v4, :cond_2

    .line 233
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->rehash()V

    .line 239
    .end local v13    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :cond_2
    :goto_2
    iget v2, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->lastGen:I

    move/from16 v0, v22

    if-eq v0, v2, :cond_4

    .line 242
    const/4 v2, 0x0

    iput v2, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldCount:I

    .line 244
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldCount:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v4, v4

    if-ne v2, v4, :cond_3

    .line 245
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v2, v2

    mul-int/lit8 v19, v2, 0x2

    .line 246
    .local v19, "newSize":I
    move/from16 v0, v19

    new-array v0, v0, [Lorg/apache/lucene/index/DocFieldProcessorPerField;

    move-object/from16 v18, v0

    .line 247
    .local v18, "newArray":[Lorg/apache/lucene/index/DocFieldProcessorPerField;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldCount:I

    move-object/from16 v0, v18

    invoke-static {v2, v4, v0, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 248
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .line 251
    .end local v18    # "newArray":[Lorg/apache/lucene/index/DocFieldProcessorPerField;
    .end local v19    # "newSize":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldCount:I

    add-int/lit8 v5, v4, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldCount:I

    aput-object v15, v2, v4

    .line 252
    move/from16 v0, v22

    iput v0, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->lastGen:I

    .line 255
    :cond_4
    iget v2, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldCount:I

    iget-object v4, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fields:[Lorg/apache/lucene/document/Fieldable;

    array-length v4, v4

    if-ne v2, v4, :cond_5

    .line 256
    iget-object v2, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fields:[Lorg/apache/lucene/document/Fieldable;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    new-array v0, v2, [Lorg/apache/lucene/document/Fieldable;

    move-object/from16 v18, v0

    .line 257
    .local v18, "newArray":[Lorg/apache/lucene/document/Fieldable;
    iget-object v2, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fields:[Lorg/apache/lucene/document/Fieldable;

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v6, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldCount:I

    move-object/from16 v0, v18

    invoke-static {v2, v4, v0, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 258
    move-object/from16 v0, v18

    iput-object v0, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fields:[Lorg/apache/lucene/document/Fieldable;

    .line 261
    .end local v18    # "newArray":[Lorg/apache/lucene/document/Fieldable;
    :cond_5
    iget-object v2, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fields:[Lorg/apache/lucene/document/Fieldable;

    iget v4, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldCount:I

    add-int/lit8 v5, v4, 0x1

    iput v5, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldCount:I

    aput-object v14, v2, v4

    .line 262
    invoke-interface {v14}, Lorg/apache/lucene/document/Fieldable;->isStored()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 263
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldsWriter:Lorg/apache/lucene/index/StoredFieldsWriterPerThread;

    iget-object v4, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v2, v14, v4}, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->addField(Lorg/apache/lucene/document/Fieldable;Lorg/apache/lucene/index/FieldInfo;)V

    .line 207
    :cond_6
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_0

    .line 235
    :cond_7
    iget-object v4, v15, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-interface {v14}, Lorg/apache/lucene/document/Fieldable;->isIndexed()Z

    move-result v5

    invoke-interface {v14}, Lorg/apache/lucene/document/Fieldable;->isTermVectorStored()Z

    move-result v6

    invoke-interface {v14}, Lorg/apache/lucene/document/Fieldable;->getOmitNorms()Z

    move-result v7

    const/4 v8, 0x0

    invoke-interface {v14}, Lorg/apache/lucene/document/Fieldable;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v9

    invoke-virtual/range {v4 .. v9}, Lorg/apache/lucene/index/FieldInfo;->update(ZZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    goto/16 :goto_2

    .line 273
    .end local v3    # "fieldName":Ljava/lang/String;
    .end local v14    # "field":Lorg/apache/lucene/document/Fieldable;
    .end local v15    # "fp":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    .end local v16    # "hashPos":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldCount:I

    sget-object v6, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldsComp:Ljava/util/Comparator;

    invoke-static {v2, v4, v5, v6}, Lorg/apache/lucene/util/ArrayUtil;->quickSort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 275
    const/16 v17, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldCount:I

    move/from16 v0, v17

    if-ge v0, v2, :cond_9

    .line 276
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aget-object v2, v2, v17

    iget-object v2, v2, Lorg/apache/lucene/index/DocFieldProcessorPerField;->consumer:Lorg/apache/lucene/index/DocFieldConsumerPerField;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aget-object v4, v4, v17

    iget-object v4, v4, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fields:[Lorg/apache/lucene/document/Fieldable;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fields:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aget-object v5, v5, v17

    iget v5, v5, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldCount:I

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/index/DocFieldConsumerPerField;->processFields([Lorg/apache/lucene/document/Fieldable;I)V

    .line 275
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 278
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->maxTermPrefix:Ljava/lang/String;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->infoStream:Ljava/io/PrintStream;

    if-eqz v2, :cond_a

    .line 279
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->infoStream:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "WARNING: document contains at least one immense term (longer than the max length 16383), all of which were skipped.  Please correct the analyzer to not produce such terms.  The prefix of the first immense term is: \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget-object v5, v5, Lorg/apache/lucene/index/DocumentsWriter$DocState;->maxTermPrefix:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "...\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 280
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    const/4 v4, 0x0

    iput-object v4, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->maxTermPrefix:Ljava/lang/String;

    .line 283
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldsWriter:Lorg/apache/lucene/index/StoredFieldsWriterPerThread;

    invoke-virtual {v2}, Lorg/apache/lucene/index/StoredFieldsWriterPerThread;->finishDocument()Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    move-result-object v21

    .line 284
    .local v21, "one":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->consumer:Lorg/apache/lucene/index/DocFieldConsumerPerThread;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocFieldConsumerPerThread;->finishDocument()Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    move-result-object v23

    .line 285
    .local v23, "two":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    if-nez v21, :cond_b

    .line 296
    .end local v23    # "two":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :goto_4
    return-object v23

    .line 287
    .restart local v23    # "two":Lorg/apache/lucene/index/DocumentsWriter$DocWriter;
    :cond_b
    if-nez v23, :cond_c

    move-object/from16 v23, v21

    .line 288
    goto :goto_4

    .line 290
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->getPerDoc()Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;

    move-result-object v10

    .line 291
    .local v10, "both":Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    iput v2, v10, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->docID:I

    .line 292
    sget-boolean v2, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->$assertionsDisabled:Z

    if-nez v2, :cond_d

    move-object/from16 v0, v21

    iget v2, v0, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->docID:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v4, v4, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    if-eq v2, v4, :cond_d

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 293
    :cond_d
    sget-boolean v2, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->$assertionsDisabled:Z

    if-nez v2, :cond_e

    move-object/from16 v0, v23

    iget v2, v0, Lorg/apache/lucene/index/DocumentsWriter$DocWriter;->docID:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iget v4, v4, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docID:I

    if-eq v2, v4, :cond_e

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 294
    :cond_e
    move-object/from16 v0, v21

    iput-object v0, v10, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->one:Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    .line 295
    move-object/from16 v0, v23

    iput-object v0, v10, Lorg/apache/lucene/index/DocFieldProcessorPerThread$PerDoc;->two:Lorg/apache/lucene/index/DocumentsWriter$DocWriter;

    move-object/from16 v23, v10

    .line 296
    goto :goto_4
.end method

.method trimFields(Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 7
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;

    .prologue
    const/4 v6, -0x1

    .line 129
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 130
    iget-object v3, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aget-object v2, v3, v0

    .line 131
    .local v2, "perField":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    const/4 v1, 0x0

    .line 133
    .local v1, "lastPerField":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :goto_1
    if-eqz v2, :cond_3

    .line 135
    iget v3, v2, Lorg/apache/lucene/index/DocFieldProcessorPerField;->lastGen:I

    if-ne v3, v6, :cond_2

    .line 141
    if-nez v1, :cond_1

    .line 142
    iget-object v3, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->fieldHash:[Lorg/apache/lucene/index/DocFieldProcessorPerField;

    iget-object v4, v2, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    aput-object v4, v3, v0

    .line 146
    :goto_2
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentWriteState;->infoStream:Ljava/io/PrintStream;

    if-eqz v3, :cond_0

    .line 147
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentWriteState;->infoStream:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "  purge field="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v5, v5, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 149
    :cond_0
    iget v3, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->totalFieldCount:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/lucene/index/DocFieldProcessorPerThread;->totalFieldCount:I

    .line 157
    :goto_3
    iget-object v2, v2, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    goto :goto_1

    .line 144
    :cond_1
    iget-object v3, v2, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    iput-object v3, v1, Lorg/apache/lucene/index/DocFieldProcessorPerField;->next:Lorg/apache/lucene/index/DocFieldProcessorPerField;

    goto :goto_2

    .line 153
    :cond_2
    iput v6, v2, Lorg/apache/lucene/index/DocFieldProcessorPerField;->lastGen:I

    .line 154
    move-object v1, v2

    goto :goto_3

    .line 129
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 160
    .end local v1    # "lastPerField":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    .end local v2    # "perField":Lorg/apache/lucene/index/DocFieldProcessorPerField;
    :cond_4
    return-void
.end method
