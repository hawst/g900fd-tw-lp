.class abstract Lorg/apache/lucene/index/FormatPostingsTermsConsumer;
.super Ljava/lang/Object;
.source "FormatPostingsTermsConsumer.java"


# instance fields
.field termBuffer:[C


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method addTerm(Ljava/lang/String;)Lorg/apache/lucene/index/FormatPostingsDocsConsumer;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 37
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 38
    .local v0, "len":I
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsTermsConsumer;->termBuffer:[C

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsTermsConsumer;->termBuffer:[C

    array-length v1, v1

    add-int/lit8 v2, v0, 0x1

    if-ge v1, v2, :cond_1

    .line 39
    :cond_0
    add-int/lit8 v1, v0, 0x1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v1, v1, [C

    iput-object v1, p0, Lorg/apache/lucene/index/FormatPostingsTermsConsumer;->termBuffer:[C

    .line 40
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsTermsConsumer;->termBuffer:[C

    invoke-virtual {p1, v3, v0, v1, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 41
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsTermsConsumer;->termBuffer:[C

    const v2, 0xffff

    aput-char v2, v1, v0

    .line 42
    iget-object v1, p0, Lorg/apache/lucene/index/FormatPostingsTermsConsumer;->termBuffer:[C

    invoke-virtual {p0, v1, v3}, Lorg/apache/lucene/index/FormatPostingsTermsConsumer;->addTerm([CI)Lorg/apache/lucene/index/FormatPostingsDocsConsumer;

    move-result-object v1

    return-object v1
.end method

.method abstract addTerm([CI)Lorg/apache/lucene/index/FormatPostingsDocsConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract finish()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
