.class public interface abstract Lorg/apache/lucene/index/TermFreqVector;
.super Ljava/lang/Object;
.source "TermFreqVector.java"


# virtual methods
.method public abstract getField()Ljava/lang/String;
.end method

.method public abstract getTermFrequencies()[I
.end method

.method public abstract getTerms()[Ljava/lang/String;
.end method

.method public abstract indexOf(Ljava/lang/String;)I
.end method

.method public abstract indexesOf([Ljava/lang/String;II)[I
.end method

.method public abstract size()I
.end method
