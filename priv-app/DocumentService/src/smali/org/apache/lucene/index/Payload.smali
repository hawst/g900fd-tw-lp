.class public Lorg/apache/lucene/index/Payload;
.super Ljava/lang/Object;
.source "Payload.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# instance fields
.field protected data:[B

.field protected length:I

.field protected offset:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "data"    # [B

    .prologue
    .line 60
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/index/Payload;-><init>([BII)V

    .line 61
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    if-ltz p2, :cond_0

    add-int v0, p2, p3

    array-length v1, p1

    if-le v0, v1, :cond_1

    .line 74
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 76
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/index/Payload;->data:[B

    .line 77
    iput p2, p0, Lorg/apache/lucene/index/Payload;->offset:I

    .line 78
    iput p3, p0, Lorg/apache/lucene/index/Payload;->length:I

    .line 79
    return-void
.end method


# virtual methods
.method public byteAt(I)B
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 127
    if-ltz p1, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/Payload;->length:I

    if-ge p1, v0, :cond_0

    .line 128
    iget-object v0, p0, Lorg/apache/lucene/index/Payload;->data:[B

    iget v1, p0, Lorg/apache/lucene/index/Payload;->offset:I

    add-int/2addr v1, p1

    aget-byte v0, v0, v1

    return v0

    .line 130
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 163
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/Payload;

    .line 165
    .local v0, "clone":Lorg/apache/lucene/index/Payload;
    iget v2, p0, Lorg/apache/lucene/index/Payload;->offset:I

    if-nez v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/index/Payload;->length:I

    iget-object v3, p0, Lorg/apache/lucene/index/Payload;->data:[B

    array-length v3, v3

    if-ne v2, v3, :cond_0

    .line 167
    iget-object v2, p0, Lorg/apache/lucene/index/Payload;->data:[B

    invoke-virtual {v2}, [B->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    iput-object v2, v0, Lorg/apache/lucene/index/Payload;->data:[B

    .line 174
    :goto_0
    return-object v0

    .line 171
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/Payload;->toByteArray()[B

    move-result-object v2

    iput-object v2, v0, Lorg/apache/lucene/index/Payload;->data:[B

    .line 172
    const/4 v2, 0x0

    iput v2, v0, Lorg/apache/lucene/index/Payload;->offset:I
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 175
    .end local v0    # "clone":Lorg/apache/lucene/index/Payload;
    :catch_0
    move-exception v1

    .line 176
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public copyTo([BI)V
    .locals 3
    .param p1, "target"    # [B
    .param p2, "targetOffset"    # I

    .prologue
    .line 149
    iget v0, p0, Lorg/apache/lucene/index/Payload;->length:I

    array-length v1, p1

    add-int/2addr v1, p2

    if-le v0, v1, :cond_0

    .line 150
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 152
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/Payload;->data:[B

    iget v1, p0, Lorg/apache/lucene/index/Payload;->offset:I

    iget v2, p0, Lorg/apache/lucene/index/Payload;->length:I

    invoke-static {v0, v1, p1, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 153
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 182
    if-ne p1, p0, :cond_1

    .line 194
    :cond_0
    :goto_0
    return v2

    .line 184
    :cond_1
    instance-of v4, p1, Lorg/apache/lucene/index/Payload;

    if-eqz v4, :cond_4

    move-object v1, p1

    .line 185
    check-cast v1, Lorg/apache/lucene/index/Payload;

    .line 186
    .local v1, "other":Lorg/apache/lucene/index/Payload;
    iget v4, p0, Lorg/apache/lucene/index/Payload;->length:I

    iget v5, v1, Lorg/apache/lucene/index/Payload;->length:I

    if-ne v4, v5, :cond_3

    .line 187
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v4, p0, Lorg/apache/lucene/index/Payload;->length:I

    if-ge v0, v4, :cond_0

    .line 188
    iget-object v4, p0, Lorg/apache/lucene/index/Payload;->data:[B

    iget v5, p0, Lorg/apache/lucene/index/Payload;->offset:I

    add-int/2addr v5, v0

    aget-byte v4, v4, v5

    iget-object v5, v1, Lorg/apache/lucene/index/Payload;->data:[B

    iget v6, v1, Lorg/apache/lucene/index/Payload;->offset:I

    add-int/2addr v6, v0

    aget-byte v5, v5, v6

    if-eq v4, v5, :cond_2

    move v2, v3

    .line 189
    goto :goto_0

    .line 187
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    :cond_3
    move v2, v3

    .line 192
    goto :goto_0

    .end local v1    # "other":Lorg/apache/lucene/index/Payload;
    :cond_4
    move v2, v3

    .line 194
    goto :goto_0
.end method

.method public getData()[B
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/lucene/index/Payload;->data:[B

    return-object v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lorg/apache/lucene/index/Payload;->offset:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 199
    iget-object v0, p0, Lorg/apache/lucene/index/Payload;->data:[B

    iget v1, p0, Lorg/apache/lucene/index/Payload;->offset:I

    iget v2, p0, Lorg/apache/lucene/index/Payload;->offset:I

    iget v3, p0, Lorg/apache/lucene/index/Payload;->length:I

    add-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->hashCode([BII)I

    move-result v0

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lorg/apache/lucene/index/Payload;->length:I

    return v0
.end method

.method public setData([B)V
    .locals 2
    .param p1, "data"    # [B

    .prologue
    .line 87
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/index/Payload;->setData([BII)V

    .line 88
    return-void
.end method

.method public setData([BII)V
    .locals 0
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 96
    iput-object p1, p0, Lorg/apache/lucene/index/Payload;->data:[B

    .line 97
    iput p2, p0, Lorg/apache/lucene/index/Payload;->offset:I

    .line 98
    iput p3, p0, Lorg/apache/lucene/index/Payload;->length:I

    .line 99
    return-void
.end method

.method public toByteArray()[B
    .locals 5

    .prologue
    .line 137
    iget v1, p0, Lorg/apache/lucene/index/Payload;->length:I

    new-array v0, v1, [B

    .line 138
    .local v0, "retArray":[B
    iget-object v1, p0, Lorg/apache/lucene/index/Payload;->data:[B

    iget v2, p0, Lorg/apache/lucene/index/Payload;->offset:I

    const/4 v3, 0x0

    iget v4, p0, Lorg/apache/lucene/index/Payload;->length:I

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 139
    return-object v0
.end method
