.class public abstract Lorg/apache/lucene/index/IndexReader;
.super Ljava/lang/Object;
.source "IndexReader.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static DEFAULT_TERMS_INDEX_DIVISOR:I

.field private static final doOpenIfChangedMethod1:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final doOpenIfChangedMethod2:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final doOpenIfChangedMethod3:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final doOpenIfChangedMethod4:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final reopenMethod1:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final reopenMethod2:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final reopenMethod3:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final reopenMethod4:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private closed:Z

.field protected hasChanges:Z

.field private final hasNewReopenAPI1:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final hasNewReopenAPI2:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final hasNewReopenAPI3:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final hasNewReopenAPI4:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final readerClosedListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final refCount:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    const-class v0, Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/IndexReader;->$assertionsDisabled:Z

    .line 124
    sput v1, Lorg/apache/lucene/index/IndexReader;->DEFAULT_TERMS_INDEX_DIVISOR:I

    .line 1637
    new-instance v0, Lorg/apache/lucene/util/VirtualMethod;

    const-class v3, Lorg/apache/lucene/index/IndexReader;

    const-string/jumbo v4, "reopen"

    new-array v5, v2, [Ljava/lang/Class;

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lorg/apache/lucene/index/IndexReader;->reopenMethod1:Lorg/apache/lucene/util/VirtualMethod;

    .line 1640
    new-instance v0, Lorg/apache/lucene/util/VirtualMethod;

    const-class v3, Lorg/apache/lucene/index/IndexReader;

    const-string/jumbo v4, "doOpenIfChanged"

    new-array v5, v2, [Ljava/lang/Class;

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lorg/apache/lucene/index/IndexReader;->doOpenIfChangedMethod1:Lorg/apache/lucene/util/VirtualMethod;

    .line 1649
    new-instance v0, Lorg/apache/lucene/util/VirtualMethod;

    const-class v3, Lorg/apache/lucene/index/IndexReader;

    const-string/jumbo v4, "reopen"

    new-array v5, v1, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v6, v5, v2

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lorg/apache/lucene/index/IndexReader;->reopenMethod2:Lorg/apache/lucene/util/VirtualMethod;

    .line 1652
    new-instance v0, Lorg/apache/lucene/util/VirtualMethod;

    const-class v3, Lorg/apache/lucene/index/IndexReader;

    const-string/jumbo v4, "doOpenIfChanged"

    new-array v5, v1, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v6, v5, v2

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lorg/apache/lucene/index/IndexReader;->doOpenIfChangedMethod2:Lorg/apache/lucene/util/VirtualMethod;

    .line 1661
    new-instance v0, Lorg/apache/lucene/util/VirtualMethod;

    const-class v3, Lorg/apache/lucene/index/IndexReader;

    const-string/jumbo v4, "reopen"

    new-array v5, v1, [Ljava/lang/Class;

    const-class v6, Lorg/apache/lucene/index/IndexCommit;

    aput-object v6, v5, v2

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lorg/apache/lucene/index/IndexReader;->reopenMethod3:Lorg/apache/lucene/util/VirtualMethod;

    .line 1664
    new-instance v0, Lorg/apache/lucene/util/VirtualMethod;

    const-class v3, Lorg/apache/lucene/index/IndexReader;

    const-string/jumbo v4, "doOpenIfChanged"

    new-array v5, v1, [Ljava/lang/Class;

    const-class v6, Lorg/apache/lucene/index/IndexCommit;

    aput-object v6, v5, v2

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lorg/apache/lucene/index/IndexReader;->doOpenIfChangedMethod3:Lorg/apache/lucene/util/VirtualMethod;

    .line 1673
    new-instance v0, Lorg/apache/lucene/util/VirtualMethod;

    const-class v3, Lorg/apache/lucene/index/IndexReader;

    const-string/jumbo v4, "reopen"

    new-array v5, v7, [Ljava/lang/Class;

    const-class v6, Lorg/apache/lucene/index/IndexWriter;

    aput-object v6, v5, v2

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v6, v5, v1

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lorg/apache/lucene/index/IndexReader;->reopenMethod4:Lorg/apache/lucene/util/VirtualMethod;

    .line 1676
    new-instance v0, Lorg/apache/lucene/util/VirtualMethod;

    const-class v3, Lorg/apache/lucene/index/IndexReader;

    const-string/jumbo v4, "doOpenIfChanged"

    new-array v5, v7, [Ljava/lang/Class;

    const-class v6, Lorg/apache/lucene/index/IndexWriter;

    aput-object v6, v5, v2

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v2, v5, v1

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lorg/apache/lucene/index/IndexReader;->doOpenIfChangedMethod4:Lorg/apache/lucene/util/VirtualMethod;

    return-void

    :cond_0
    move v0, v2

    .line 79
    goto/16 :goto_0
.end method

.method protected constructor <init>()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexReader;->readerClosedListeners:Ljava/util/Set;

    .line 119
    iput-boolean v2, p0, Lorg/apache/lucene/index/IndexReader;->closed:Z

    .line 122
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1643
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    sget-object v3, Lorg/apache/lucene/index/IndexReader;->doOpenIfChangedMethod1:Lorg/apache/lucene/util/VirtualMethod;

    sget-object v4, Lorg/apache/lucene/index/IndexReader;->reopenMethod1:Lorg/apache/lucene/util/VirtualMethod;

    invoke-static {v0, v3, v4}, Lorg/apache/lucene/util/VirtualMethod;->compareImplementationDistance(Ljava/lang/Class;Lorg/apache/lucene/util/VirtualMethod;Lorg/apache/lucene/util/VirtualMethod;)I

    move-result v0

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexReader;->hasNewReopenAPI1:Z

    .line 1655
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    sget-object v3, Lorg/apache/lucene/index/IndexReader;->doOpenIfChangedMethod2:Lorg/apache/lucene/util/VirtualMethod;

    sget-object v4, Lorg/apache/lucene/index/IndexReader;->reopenMethod2:Lorg/apache/lucene/util/VirtualMethod;

    invoke-static {v0, v3, v4}, Lorg/apache/lucene/util/VirtualMethod;->compareImplementationDistance(Ljava/lang/Class;Lorg/apache/lucene/util/VirtualMethod;Lorg/apache/lucene/util/VirtualMethod;)I

    move-result v0

    if-ltz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexReader;->hasNewReopenAPI2:Z

    .line 1667
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    sget-object v3, Lorg/apache/lucene/index/IndexReader;->doOpenIfChangedMethod3:Lorg/apache/lucene/util/VirtualMethod;

    sget-object v4, Lorg/apache/lucene/index/IndexReader;->reopenMethod3:Lorg/apache/lucene/util/VirtualMethod;

    invoke-static {v0, v3, v4}, Lorg/apache/lucene/util/VirtualMethod;->compareImplementationDistance(Ljava/lang/Class;Lorg/apache/lucene/util/VirtualMethod;Lorg/apache/lucene/util/VirtualMethod;)I

    move-result v0

    if-ltz v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexReader;->hasNewReopenAPI3:Z

    .line 1679
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    sget-object v3, Lorg/apache/lucene/index/IndexReader;->doOpenIfChangedMethod4:Lorg/apache/lucene/util/VirtualMethod;

    sget-object v4, Lorg/apache/lucene/index/IndexReader;->reopenMethod4:Lorg/apache/lucene/util/VirtualMethod;

    invoke-static {v0, v3, v4}, Lorg/apache/lucene/util/VirtualMethod;->compareImplementationDistance(Ljava/lang/Class;Lorg/apache/lucene/util/VirtualMethod;Lorg/apache/lucene/util/VirtualMethod;)I

    move-result v0

    if-ltz v0, :cond_0

    move v2, v1

    :cond_0
    iput-boolean v2, p0, Lorg/apache/lucene/index/IndexReader;->hasNewReopenAPI4:Z

    .line 237
    iget-object v0, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 238
    return-void

    :cond_1
    move v0, v2

    .line 1643
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1655
    goto :goto_1

    :cond_3
    move v0, v2

    .line 1667
    goto :goto_2
.end method

.method public static getCommitUserData(Lorg/apache/lucene/store/Directory;)Ljava/util/Map;
    .locals 2
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 961
    new-instance v0, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v0}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 962
    .local v0, "sis":Lorg/apache/lucene/index/SegmentInfos;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V

    .line 963
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->getUserData()Ljava/util/Map;

    move-result-object v1

    return-object v1
.end method

.method public static getCurrentVersion(Lorg/apache/lucene/store/Directory;)J
    .locals 2
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 940
    invoke-static {p0}, Lorg/apache/lucene/index/SegmentInfos;->readCurrentVersion(Lorg/apache/lucene/store/Directory;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static indexExists(Lorg/apache/lucene/store/Directory;)Z
    .locals 2
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1099
    :try_start_0
    new-instance v1, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v1}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    invoke-virtual {v1, p0}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1100
    const/4 v1, 0x1

    .line 1102
    :goto_0
    return v1

    .line 1101
    :catch_0
    move-exception v0

    .line 1102
    .local v0, "ioe":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static lastModified(Lorg/apache/lucene/store/Directory;)J
    .locals 2
    .param p0, "directory2"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 919
    new-instance v0, Lorg/apache/lucene/index/IndexReader$1;

    invoke-direct {v0, p0, p0}, Lorg/apache/lucene/index/IndexReader$1;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/Directory;)V

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader$1;->run()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public static listCommits(Lorg/apache/lucene/store/Directory;)Ljava/util/Collection;
    .locals 1
    .param p0, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1583
    invoke-static {p0}, Lorg/apache/lucene/index/DirectoryReader;->listCommits(Lorg/apache/lucene/store/Directory;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private final notifyReaderClosedListeners()V
    .locals 4

    .prologue
    .line 112
    iget-object v3, p0, Lorg/apache/lucene/index/IndexReader;->readerClosedListeners:Ljava/util/Set;

    monitor-enter v3

    .line 113
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/IndexReader;->readerClosedListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;

    .line 114
    .local v1, "listener":Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;
    invoke-interface {v1, p0}, Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;->onClose(Lorg/apache/lucene/index/IndexReader;)V

    goto :goto_0

    .line 116
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 117
    return-void
.end method

.method public static open(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;
    .locals 4
    .param p0, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 306
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    sget v3, Lorg/apache/lucene/index/IndexReader;->DEFAULT_TERMS_INDEX_DIVISOR:I

    invoke-static {v0, v1, p0, v2, v3}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;ZI)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/index/IndexCommit;I)Lorg/apache/lucene/index/IndexReader;
    .locals 3
    .param p0, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .param p1, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 474
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, p0, v2, p1}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;ZI)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/index/IndexCommit;Lorg/apache/lucene/index/IndexDeletionPolicy;Z)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p0, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .param p1, "deletionPolicy"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .param p2, "readOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 398
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    sget v1, Lorg/apache/lucene/index/IndexReader;->DEFAULT_TERMS_INDEX_DIVISOR:I

    invoke-static {v0, p1, p0, p2, v1}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;ZI)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/index/IndexCommit;Lorg/apache/lucene/index/IndexDeletionPolicy;ZI)Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .param p0, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .param p1, "deletionPolicy"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .param p2, "readOnly"    # Z
    .param p3, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 434
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-static {v0, p1, p0, p2, p3}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;ZI)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/index/IndexCommit;Z)Lorg/apache/lucene/index/IndexReader;
    .locals 3
    .param p0, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .param p1, "readOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 323
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    const/4 v1, 0x0

    sget v2, Lorg/apache/lucene/index/IndexReader;->DEFAULT_TERMS_INDEX_DIVISOR:I

    invoke-static {v0, v1, p0, p1, v2}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;ZI)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .param p0, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p1, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 296
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->getReader(Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/IndexReader;
    .locals 3
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 256
    const/4 v0, 0x1

    sget v1, Lorg/apache/lucene/index/IndexReader;->DEFAULT_TERMS_INDEX_DIVISOR:I

    invoke-static {p0, v2, v2, v0, v1}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;ZI)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/store/Directory;I)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p1, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 454
    const/4 v0, 0x1

    invoke-static {p0, v1, v1, v0, p1}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;ZI)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Z)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p1, "deletionPolicy"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .param p2, "readOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 344
    const/4 v0, 0x0

    sget v1, Lorg/apache/lucene/index/IndexReader;->DEFAULT_TERMS_INDEX_DIVISOR:I

    invoke-static {p0, p1, v0, p2, v1}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;ZI)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;ZI)Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p1, "deletionPolicy"    # Lorg/apache/lucene/index/IndexDeletionPolicy;
    .param p2, "readOnly"    # Z
    .param p3, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 375
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2, p3}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;ZI)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/store/Directory;Z)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p1, "readOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 273
    sget v0, Lorg/apache/lucene/index/IndexReader;->DEFAULT_TERMS_INDEX_DIVISOR:I

    invoke-static {p0, v1, v1, p1, v0}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexDeletionPolicy;Lorg/apache/lucene/index/IndexCommit;ZI)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method public static openIfChanged(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p0, "oldReader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 507
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexReader;->hasNewReopenAPI1:Z

    if-eqz v1, :cond_1

    .line 508
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->doOpenIfChanged()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 509
    .local v0, "newReader":Lorg/apache/lucene/index/IndexReader;
    sget-boolean v1, Lorg/apache/lucene/index/IndexReader;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-ne v0, p0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_0
    move-object v1, v0

    .line 516
    :goto_0
    return-object v1

    .line 512
    .end local v0    # "newReader":Lorg/apache/lucene/index/IndexReader;
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->reopen()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 513
    .restart local v0    # "newReader":Lorg/apache/lucene/index/IndexReader;
    if-ne v0, p0, :cond_2

    .line 514
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 516
    goto :goto_0
.end method

.method public static openIfChanged(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p0, "oldReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 557
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexReader;->hasNewReopenAPI3:Z

    if-eqz v1, :cond_1

    .line 558
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexReader;->doOpenIfChanged(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 559
    .local v0, "newReader":Lorg/apache/lucene/index/IndexReader;
    sget-boolean v1, Lorg/apache/lucene/index/IndexReader;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-ne v0, p0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_0
    move-object v1, v0

    .line 566
    :goto_0
    return-object v1

    .line 562
    .end local v0    # "newReader":Lorg/apache/lucene/index/IndexReader;
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexReader;->reopen(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 563
    .restart local v0    # "newReader":Lorg/apache/lucene/index/IndexReader;
    if-ne v0, p0, :cond_2

    .line 564
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 566
    goto :goto_0
.end method

.method public static openIfChanged(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p0, "oldReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 633
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexReader;->hasNewReopenAPI4:Z

    if-eqz v1, :cond_1

    .line 634
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/IndexReader;->doOpenIfChanged(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 635
    .local v0, "newReader":Lorg/apache/lucene/index/IndexReader;
    sget-boolean v1, Lorg/apache/lucene/index/IndexReader;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-ne v0, p0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_0
    move-object v1, v0

    .line 642
    :goto_0
    return-object v1

    .line 638
    .end local v0    # "newReader":Lorg/apache/lucene/index/IndexReader;
    :cond_1
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/IndexReader;->reopen(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 639
    .restart local v0    # "newReader":Lorg/apache/lucene/index/IndexReader;
    if-ne v0, p0, :cond_2

    .line 640
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 642
    goto :goto_0
.end method

.method public static openIfChanged(Lorg/apache/lucene/index/IndexReader;Z)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p0, "oldReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "readOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 533
    iget-boolean v1, p0, Lorg/apache/lucene/index/IndexReader;->hasNewReopenAPI2:Z

    if-eqz v1, :cond_1

    .line 534
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexReader;->doOpenIfChanged(Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 535
    .local v0, "newReader":Lorg/apache/lucene/index/IndexReader;
    sget-boolean v1, Lorg/apache/lucene/index/IndexReader;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-ne v0, p0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_0
    move-object v1, v0

    .line 542
    :goto_0
    return-object v1

    .line 538
    .end local v0    # "newReader":Lorg/apache/lucene/index/IndexReader;
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexReader;->reopen(Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 539
    .restart local v0    # "newReader":Lorg/apache/lucene/index/IndexReader;
    if-ne v0, p0, :cond_2

    .line 540
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 542
    goto :goto_0
.end method


# virtual methods
.method protected declared-synchronized acquireWriteLock()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1465
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public final addReaderClosedListener(Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;

    .prologue
    .line 99
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 100
    iget-object v0, p0, Lorg/apache/lucene/index/IndexReader;->readerClosedListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 101
    return-void
.end method

.method public declared-synchronized clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 879
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This reader does not implement clone()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clone(Z)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p1, "openReadOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 892
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This reader does not implement clone()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1537
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexReader;->closed:Z

    if-nez v0, :cond_0

    .line 1538
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 1539
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexReader;->closed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1541
    :cond_0
    monitor-exit p0

    return-void

    .line 1537
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final declared-synchronized commit()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1504
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexReader;->commit(Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1505
    monitor-exit p0

    return-void

    .line 1504
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized commit(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1520
    .local p1, "commitUserData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexReader;->doCommit(Ljava/util/Map;)V

    .line 1521
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexReader;->hasChanges:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1522
    monitor-exit p0

    return-void

    .line 1520
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final decRef()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 216
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 217
    iget-object v2, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 218
    .local v0, "rc":I
    if-nez v0, :cond_3

    .line 219
    const/4 v1, 0x0

    .line 221
    .local v1, "success":Z
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->commit()V

    .line 222
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->doClose()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    const/4 v1, 0x1

    .line 225
    if-nez v1, :cond_0

    .line 227
    iget-object v2, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 230
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexReader;->notifyReaderClosedListeners()V

    .line 234
    .end local v1    # "success":Z
    :cond_1
    return-void

    .line 225
    .restart local v1    # "success":Z
    :catchall_0
    move-exception v2

    if-nez v1, :cond_2

    .line 227
    iget-object v3, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    :cond_2
    throw v2

    .line 231
    .end local v1    # "success":Z
    :cond_3
    if-gez v0, :cond_1

    .line 232
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "too many decRef calls: refCount is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " after decrement"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public final declared-synchronized deleteDocument(I)V
    .locals 1
    .param p1, "docNum"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/StaleReaderException;,
            Lorg/apache/lucene/index/CorruptIndexException;,
            Lorg/apache/lucene/store/LockObtainFailedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1370
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 1371
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->acquireWriteLock()V

    .line 1372
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexReader;->hasChanges:Z

    .line 1373
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexReader;->doDelete(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1374
    monitor-exit p0

    return-void

    .line 1370
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final deleteDocuments(Lorg/apache/lucene/index/Term;)I
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/StaleReaderException;,
            Lorg/apache/lucene/index/CorruptIndexException;,
            Lorg/apache/lucene/store/LockObtainFailedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1407
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 1408
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    .line 1409
    .local v0, "docs":Lorg/apache/lucene/index/TermDocs;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 1419
    :goto_0
    return v1

    .line 1410
    :cond_0
    const/4 v1, 0x0

    .line 1412
    .local v1, "n":I
    :goto_1
    :try_start_0
    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1413
    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/IndexReader;->deleteDocument(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1414
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1417
    :cond_1
    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->close()V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->close()V

    throw v2
.end method

.method public directory()Lorg/apache/lucene/store/Directory;
    .locals 2

    .prologue
    .line 903
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 904
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This reader does not support this method."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected abstract doClose()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract doCommit(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method protected abstract doDelete(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method protected doOpenIfChanged()Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 824
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This reader does not support reopen()."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected doOpenIfChanged(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 847
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This reader does not support reopen(IndexCommit)."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected doOpenIfChanged(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 857
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/IndexWriter;->getReader(Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    return-object v0
.end method

.method protected doOpenIfChanged(Z)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p1, "openReadOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 837
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This reader does not support reopen()."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected abstract doSetNorm(ILjava/lang/String;B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method protected abstract doUndeleteAll()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract docFreq(Lorg/apache/lucene/index/Term;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final document(I)Lorg/apache/lucene/document/Document;
    .locals 3
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1134
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 1135
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 1136
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "docID must be >= 0 and < maxDoc="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " (got docID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1138
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/IndexReader;->document(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;

    move-result-object v0

    return-object v0
.end method

.method public abstract document(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected final ensureOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 244
    iget-object v0, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-gtz v0, :cond_0

    .line 245
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string/jumbo v1, "this IndexReader is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247
    :cond_0
    return-void
.end method

.method public final declared-synchronized flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1474
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 1475
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1476
    monitor-exit p0

    return-void

    .line 1474
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized flush(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1488
    .local p1, "commitUserData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 1489
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexReader;->commit(Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1490
    monitor-exit p0

    return-void

    .line 1488
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getCommitUserData()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 994
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This reader does not support this method."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCoreCacheKey()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 1610
    return-object p0
.end method

.method public getDeletesCacheKey()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 1616
    return-object p0
.end method

.method public abstract getFieldInfos()Lorg/apache/lucene/index/FieldInfos;
.end method

.method public getIndexCommit()Lorg/apache/lucene/index/IndexCommit;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1564
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This reader does not support this method."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getRefCount()I
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public getSequentialSubReaders()[Lorg/apache/lucene/index/IndexReader;
    .locals 1

    .prologue
    .line 1602
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 1603
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getTermFreqVector(ILjava/lang/String;)Lorg/apache/lucene/index/TermFreqVector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getTermFreqVector(ILjava/lang/String;Lorg/apache/lucene/index/TermVectorMapper;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getTermFreqVector(ILorg/apache/lucene/index/TermVectorMapper;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getTermFreqVectors(I)[Lorg/apache/lucene/index/TermFreqVector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getTermInfosIndexDivisor()I
    .locals 2

    .prologue
    .line 1689
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This reader does not support this method."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getUniqueTermCount()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1632
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "this reader does not implement getUniqueTermCount()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 980
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This reader does not support this method."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract hasDeletions()Z
.end method

.method public hasNorms(Ljava/lang/String;)Z
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1184
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 1185
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final incRef()V
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 148
    iget-object v0, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 149
    return-void
.end method

.method public isCurrent()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1026
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This reader does not support this method."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract isDeleted(I)Z
.end method

.method public isOptimized()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1033
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This reader does not support this method."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract maxDoc()I
.end method

.method public abstract norms(Ljava/lang/String;[BI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract norms(Ljava/lang/String;)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final numDeletedDocs()I
    .locals 2

    .prologue
    .line 1117
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public abstract numDocs()I
.end method

.method public final removeReaderClosedListener(Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;

    .prologue
    .line 107
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 108
    iget-object v0, p0, Lorg/apache/lucene/index/IndexReader;->readerClosedListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 109
    return-void
.end method

.method public reopen()Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 695
    invoke-static {p0}, Lorg/apache/lucene/index/IndexReader;->openIfChanged(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 696
    .local v0, "newReader":Lorg/apache/lucene/index/IndexReader;
    if-nez v0, :cond_0

    .line 699
    .end local p0    # "this":Lorg/apache/lucene/index/IndexReader;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    move-object p0, v0

    goto :goto_0
.end method

.method public reopen(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 730
    invoke-static {p0, p1}, Lorg/apache/lucene/index/IndexReader;->openIfChanged(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 731
    .local v0, "newReader":Lorg/apache/lucene/index/IndexReader;
    if-nez v0, :cond_0

    .line 734
    .end local p0    # "this":Lorg/apache/lucene/index/IndexReader;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    move-object p0, v0

    goto :goto_0
.end method

.method public reopen(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 809
    invoke-static {p0, p1, p2}, Lorg/apache/lucene/index/IndexReader;->openIfChanged(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 810
    .local v0, "newReader":Lorg/apache/lucene/index/IndexReader;
    if-nez v0, :cond_0

    .line 813
    .end local p0    # "this":Lorg/apache/lucene/index/IndexReader;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    move-object p0, v0

    goto :goto_0
.end method

.method public reopen(Z)Lorg/apache/lucene/index/IndexReader;
    .locals 1
    .param p1, "openReadOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 712
    invoke-static {p0, p1}, Lorg/apache/lucene/index/IndexReader;->openIfChanged(Lorg/apache/lucene/index/IndexReader;Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 713
    .local v0, "newReader":Lorg/apache/lucene/index/IndexReader;
    if-nez v0, :cond_0

    .line 716
    .end local p0    # "this":Lorg/apache/lucene/index/IndexReader;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    move-object p0, v0

    goto :goto_0
.end method

.method public final declared-synchronized setNorm(ILjava/lang/String;B)V
    .locals 1
    .param p1, "doc"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "value"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/StaleReaderException;,
            Lorg/apache/lucene/index/CorruptIndexException;,
            Lorg/apache/lucene/store/LockObtainFailedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1229
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 1230
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->acquireWriteLock()V

    .line 1231
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexReader;->hasChanges:Z

    .line 1232
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/index/IndexReader;->doSetNorm(ILjava/lang/String;B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1233
    monitor-exit p0

    return-void

    .line 1229
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final setNorm(ILjava/lang/String;F)V
    .locals 1
    .param p1, "doc"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "value"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/StaleReaderException;,
            Lorg/apache/lucene/index/CorruptIndexException;,
            Lorg/apache/lucene/store/LockObtainFailedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1262
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 1263
    invoke-static {}, Lorg/apache/lucene/search/Similarity;->getDefault()Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    invoke-virtual {v0, p3}, Lorg/apache/lucene/search/Similarity;->encodeNormValue(F)B

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/index/IndexReader;->setNorm(ILjava/lang/String;B)V

    .line 1264
    return-void
.end method

.method public abstract termDocs()Lorg/apache/lucene/index/TermDocs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1304
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 1305
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->termDocs()Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    .line 1306
    .local v0, "termDocs":Lorg/apache/lucene/index/TermDocs;
    invoke-interface {v0, p1}, Lorg/apache/lucene/index/TermDocs;->seek(Lorg/apache/lucene/index/Term;)V

    .line 1307
    return-object v0
.end method

.method public abstract termPositions()Lorg/apache/lucene/index/TermPositions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final termPositions(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermPositions;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1338
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 1339
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->termPositions()Lorg/apache/lucene/index/TermPositions;

    move-result-object v0

    .line 1340
    .local v0, "termPositions":Lorg/apache/lucene/index/TermPositions;
    invoke-interface {v0, p1}, Lorg/apache/lucene/index/TermPositions;->seek(Lorg/apache/lucene/index/Term;)V

    .line 1341
    return-object v0
.end method

.method public abstract terms()Lorg/apache/lucene/index/TermEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-boolean v3, p0, Lorg/apache/lucene/index/IndexReader;->hasChanges:Z

    if-eqz v3, :cond_0

    .line 189
    const/16 v3, 0x2a

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 191
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    const/16 v3, 0x28

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 193
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->getSequentialSubReaders()[Lorg/apache/lucene/index/IndexReader;

    move-result-object v2

    .line 194
    .local v2, "subReaders":[Lorg/apache/lucene/index/IndexReader;
    if-eqz v2, :cond_1

    array-length v3, v2

    if-lez v3, :cond_1

    .line 195
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 196
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 197
    const-string/jumbo v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v2, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 196
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 200
    .end local v1    # "i":I
    :cond_1
    const/16 v3, 0x29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public final tryIncRef()Z
    .locals 3

    .prologue
    .line 176
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .local v0, "count":I
    if-lez v0, :cond_1

    .line 177
    iget-object v1, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    const/4 v1, 0x1

    .line 181
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized undeleteAll()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/StaleReaderException;,
            Lorg/apache/lucene/index/CorruptIndexException;,
            Lorg/apache/lucene/store/LockObtainFailedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1445
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 1446
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->acquireWriteLock()V

    .line 1447
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexReader;->hasChanges:Z

    .line 1448
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->doUndeleteAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1449
    monitor-exit p0

    return-void

    .line 1445
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
