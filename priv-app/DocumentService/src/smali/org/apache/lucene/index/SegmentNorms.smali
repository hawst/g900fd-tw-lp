.class final Lorg/apache/lucene/index/SegmentNorms;
.super Ljava/lang/Object;
.source "SegmentNorms.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final NORMS_HEADER:[B


# instance fields
.field private bytes:[B

.field private bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

.field dirty:Z

.field private in:Lorg/apache/lucene/store/IndexInput;

.field private normSeek:J

.field private number:I

.field private origNorm:Lorg/apache/lucene/index/SegmentNorms;

.field private final owner:Lorg/apache/lucene/index/SegmentReader;

.field refCount:I

.field rollbackDirty:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lorg/apache/lucene/index/SegmentNorms;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    .line 38
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/index/SegmentNorms;->NORMS_HEADER:[B

    return-void

    .line 35
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 38
    :array_0
    .array-data 1
        0x4et
        0x52t
        0x4dt
        -0x1t
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/lucene/store/IndexInput;IJLorg/apache/lucene/index/SegmentReader;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "number"    # I
    .param p3, "normSeek"    # J
    .param p5, "owner"    # Lorg/apache/lucene/index/SegmentReader;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    .line 60
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentNorms;->in:Lorg/apache/lucene/store/IndexInput;

    .line 61
    iput p2, p0, Lorg/apache/lucene/index/SegmentNorms;->number:I

    .line 62
    iput-wide p3, p0, Lorg/apache/lucene/index/SegmentNorms;->normSeek:J

    .line 63
    iput-object p5, p0, Lorg/apache/lucene/index/SegmentNorms;->owner:Lorg/apache/lucene/index/SegmentReader;

    .line 64
    return-void
.end method

.method private closeInput()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 72
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->in:Lorg/apache/lucene/store/IndexInput;

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->in:Lorg/apache/lucene/store/IndexInput;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->owner:Lorg/apache/lucene/index/SegmentReader;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentReader;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    if-eq v0, v1, :cond_2

    .line 75
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 85
    :cond_0
    :goto_0
    iput-object v2, p0, Lorg/apache/lucene/index/SegmentNorms;->in:Lorg/apache/lucene/store/IndexInput;

    .line 87
    :cond_1
    return-void

    .line 79
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->owner:Lorg/apache/lucene/index/SegmentReader;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentReader;->singleNormRef:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    .line 80
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->owner:Lorg/apache/lucene/index/SegmentReader;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentReader;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 81
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->owner:Lorg/apache/lucene/index/SegmentReader;

    iput-object v2, v0, Lorg/apache/lucene/index/SegmentReader;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized bytes([BII)V
    .locals 4
    .param p1, "bytesOut"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    iget v0, v0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-gtz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 115
    :cond_1
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B

    if-eqz v0, :cond_3

    .line 117
    sget-boolean v0, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->owner:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v0

    if-le p3, v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 118
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B

    const/4 v1, 0x0

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132
    :goto_0
    monitor-exit p0

    return-void

    .line 121
    :cond_3
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    if-eqz v0, :cond_4

    .line 123
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/SegmentNorms;->bytes([BII)V

    goto :goto_0

    .line 126
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->in:Lorg/apache/lucene/store/IndexInput;

    monitor-enter v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 127
    :try_start_3
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->in:Lorg/apache/lucene/store/IndexInput;

    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentNorms;->normSeek:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 128
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->in:Lorg/apache/lucene/store/IndexInput;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, p2, p3, v2}, Lorg/apache/lucene/store/IndexInput;->readBytes([BIIZ)V

    .line 129
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public declared-synchronized bytes()[B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget v1, p0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    iget v1, v1, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-gtz v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 137
    :cond_1
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B

    if-nez v1, :cond_3

    .line 138
    sget-boolean v1, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 139
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    if-eqz v1, :cond_4

    .line 143
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentNorms;->bytes()[B

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B

    .line 144
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 145
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 149
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentNorms;->decRef()V

    .line 150
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    .line 172
    :cond_3
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1

    .line 155
    :cond_4
    :try_start_2
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->owner:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v0

    .line 156
    .local v0, "count":I
    new-array v1, v0, [B

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B

    .line 159
    sget-boolean v1, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->in:Lorg/apache/lucene/store/IndexInput;

    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 162
    :cond_5
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentNorms;->in:Lorg/apache/lucene/store/IndexInput;

    monitor-enter v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 163
    :try_start_3
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->in:Lorg/apache/lucene/store/IndexInput;

    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentNorms;->normSeek:J

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 164
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->in:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v4, v0, v5}, Lorg/apache/lucene/store/IndexInput;->readBytes([BIIZ)V

    .line 165
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 167
    :try_start_4
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 168
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentNorms;->closeInput()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 165
    :catchall_1
    move-exception v1

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method bytesRef()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method public declared-synchronized clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 205
    monitor-enter p0

    :try_start_0
    sget-boolean v2, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget v2, p0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-lez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    iget v2, v2, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-gtz v2, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 209
    :cond_1
    :try_start_1
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentNorms;
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214
    .local v0, "clone":Lorg/apache/lucene/index/SegmentNorms;
    const/4 v2, 0x1

    :try_start_2
    iput v2, v0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    .line 216
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B

    if-eqz v2, :cond_4

    .line 217
    sget-boolean v2, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 210
    .end local v0    # "clone":Lorg/apache/lucene/index/SegmentNorms;
    :catch_0
    move-exception v1

    .line 212
    .local v1, "cnse":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string/jumbo v3, "unexpected CloneNotSupportedException"

    invoke-direct {v2, v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 218
    .end local v1    # "cnse":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "clone":Lorg/apache/lucene/index/SegmentNorms;
    :cond_2
    sget-boolean v2, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 221
    :cond_3
    iget-object v2, v0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 232
    :goto_0
    const/4 v2, 0x0

    iput-object v2, v0, Lorg/apache/lucene/index/SegmentNorms;->in:Lorg/apache/lucene/store/IndexInput;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 234
    monitor-exit p0

    return-object v0

    .line 223
    :cond_4
    :try_start_3
    sget-boolean v2, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    if-eqz v2, :cond_5

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 224
    :cond_5
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    if-nez v2, :cond_6

    .line 226
    iput-object p0, v0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    .line 228
    :cond_6
    iget-object v2, v0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentNorms;->incRef()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized copyOnWrite()[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 183
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget v1, p0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    iget v1, v1, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-gtz v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 184
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentNorms;->bytes()[B

    .line 185
    sget-boolean v1, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 186
    :cond_2
    sget-boolean v1, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 187
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-le v1, v2, :cond_5

    .line 191
    sget-boolean v1, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    iget v1, p0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-eq v1, v2, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 192
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 193
    .local v0, "oldRef":Ljava/util/concurrent/atomic/AtomicInteger;
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->owner:Lorg/apache/lucene/index/SegmentReader;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/SegmentReader;->cloneNormBytes([B)[B

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B

    .line 194
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 195
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 197
    .end local v0    # "oldRef":Ljava/util/concurrent/atomic/AtomicInteger;
    :cond_5
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/index/SegmentNorms;->dirty:Z

    .line 198
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized decRef()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    iget v0, v0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-gtz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 92
    :cond_1
    :try_start_1
    iget v0, p0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-nez v0, :cond_4

    .line 93
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    if-eqz v0, :cond_2

    .line 94
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentNorms;->decRef()V

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    .line 100
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B

    if-eqz v0, :cond_5

    .line 101
    sget-boolean v0, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 97
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentNorms;->closeInput()V

    goto :goto_0

    .line 102
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 103
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109
    :cond_4
    monitor-exit p0

    return-void

    .line 106
    :cond_5
    :try_start_2
    sget-boolean v0, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->bytesRef:Ljava/util/concurrent/atomic/AtomicInteger;

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public declared-synchronized incRef()V
    .locals 1

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    iget v0, v0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-gtz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 68
    :cond_1
    :try_start_1
    iget v0, p0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    monitor-exit p0

    return-void
.end method

.method public reWrite(Lorg/apache/lucene/index/SegmentInfo;)V
    .locals 7
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 240
    sget-boolean v3, Lorg/apache/lucene/index/SegmentNorms;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget v3, p0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-lez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    iget v3, v3, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-gtz v3, :cond_1

    :cond_0
    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "refCount="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " origNorm="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/index/SegmentNorms;->origNorm:Lorg/apache/lucene/index/SegmentNorms;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 243
    :cond_1
    iget v3, p0, Lorg/apache/lucene/index/SegmentNorms;->number:I

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/SegmentInfo;->advanceNormGen(I)V

    .line 244
    iget v3, p0, Lorg/apache/lucene/index/SegmentNorms;->number:I

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/SegmentInfo;->getNormFileName(I)Ljava/lang/String;

    move-result-object v0

    .line 245
    .local v0, "normFileName":Ljava/lang/String;
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentNorms;->owner:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentReader;->directory()Lorg/apache/lucene/store/Directory;

    move-result-object v3

    invoke-virtual {v3, v0}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    .line 246
    .local v1, "out":Lorg/apache/lucene/store/IndexOutput;
    const/4 v2, 0x0

    .line 249
    .local v2, "success":Z
    :try_start_0
    sget-object v3, Lorg/apache/lucene/index/SegmentNorms;->NORMS_HEADER:[B

    const/4 v4, 0x0

    sget-object v5, Lorg/apache/lucene/index/SegmentNorms;->NORMS_HEADER:[B

    array-length v5, v5

    invoke-virtual {v1, v3, v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 250
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentNorms;->bytes:[B

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentNorms;->owner:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    :try_start_1
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 254
    const/4 v2, 0x1

    .line 256
    if-nez v2, :cond_2

    .line 258
    :try_start_2
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentNorms;->owner:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentReader;->directory()Lorg/apache/lucene/store/Directory;

    move-result-object v3

    invoke-virtual {v3, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 265
    :cond_2
    :goto_0
    iput-boolean v6, p0, Lorg/apache/lucene/index/SegmentNorms;->dirty:Z

    .line 266
    return-void

    .line 252
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->close()V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 256
    :catchall_1
    move-exception v3

    if-nez v2, :cond_3

    .line 258
    :try_start_4
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentNorms;->owner:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentReader;->directory()Lorg/apache/lucene/store/Directory;

    move-result-object v4

    invoke-virtual {v4, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    .line 262
    :cond_3
    :goto_1
    throw v3

    .line 259
    :catch_0
    move-exception v4

    goto :goto_1

    :catch_1
    move-exception v3

    goto :goto_0
.end method
