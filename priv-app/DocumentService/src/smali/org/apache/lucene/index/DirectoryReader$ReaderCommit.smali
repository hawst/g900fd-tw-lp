.class final Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;
.super Lorg/apache/lucene/index/IndexCommit;
.source "DirectoryReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DirectoryReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ReaderCommit"
.end annotation


# instance fields
.field dir:Lorg/apache/lucene/store/Directory;

.field files:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field generation:J

.field private final segmentCount:I

.field private segmentsFileName:Ljava/lang/String;

.field final userData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field version:J


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/store/Directory;)V
    .locals 2
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1007
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexCommit;-><init>()V

    .line 1008
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->segmentsFileName:Ljava/lang/String;

    .line 1009
    iput-object p2, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->dir:Lorg/apache/lucene/store/Directory;

    .line 1010
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->getUserData()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->userData:Ljava/util/Map;

    .line 1011
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Lorg/apache/lucene/index/SegmentInfos;->files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->files:Ljava/util/Collection;

    .line 1012
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->getVersion()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->version:J

    .line 1013
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->generation:J

    .line 1014
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->segmentCount:I

    .line 1015
    return-void
.end method


# virtual methods
.method public delete()V
    .locals 2

    .prologue
    .line 1064
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This IndexCommit does not support deletions"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getDirectory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 1039
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->dir:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method public getFileNames()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1034
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->files:Ljava/util/Collection;

    return-object v0
.end method

.method public getGeneration()J
    .locals 2

    .prologue
    .line 1049
    iget-wide v0, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->generation:J

    return-wide v0
.end method

.method public getSegmentCount()I
    .locals 1

    .prologue
    .line 1024
    iget v0, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->segmentCount:I

    return v0
.end method

.method public getSegmentsFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1029
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->segmentsFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getUserData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1059
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->userData:Ljava/util/Map;

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 1044
    iget-wide v0, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->version:J

    return-wide v0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 1054
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1019
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "DirectoryReader.ReaderCommit("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/DirectoryReader$ReaderCommit;->segmentsFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
