.class Lorg/apache/lucene/index/CoalescedDeletes$3;
.super Ljava/lang/Object;
.source "CoalescedDeletes.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/CoalescedDeletes;->mergedIterator(Ljava/util/List;)Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/index/Term;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field current:Lorg/apache/lucene/index/Term;

.field numTop:I

.field queue:Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;

.field top:[Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

.field final synthetic val$iterators:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149
    const-class v0, Lorg/apache/lucene/index/CoalescedDeletes;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/CoalescedDeletes$3;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/util/List;)V
    .locals 7

    .prologue
    .line 149
    iput-object p1, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->val$iterators:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-instance v5, Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;

    iget-object v6, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->val$iterators:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v5, v6}, Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;-><init>(I)V

    iput-object v5, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->queue:Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;

    .line 90
    iget-object v5, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->val$iterators:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    iput-object v5, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->top:[Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    .line 94
    const/4 v1, 0x0

    .line 95
    .local v1, "index":I
    iget-object v5, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->val$iterators:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Iterator;

    .line 96
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/Term;>;"
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 97
    new-instance v4, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;-><init>(Lorg/apache/lucene/index/CoalescedDeletes$1;)V

    .line 98
    .local v4, "sub":Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/Term;

    iput-object v5, v4, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->current:Lorg/apache/lucene/index/Term;

    .line 99
    iput-object v3, v4, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->iterator:Ljava/util/Iterator;

    .line 100
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .local v2, "index":I
    iput v1, v4, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->index:I

    .line 101
    iget-object v5, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->queue:Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;

    invoke-virtual {v5, v4}, Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    goto :goto_0

    .line 104
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/Term;>;"
    .end local v4    # "sub":Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;
    :cond_1
    return-void
.end method

.method private pullTop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 138
    sget-boolean v0, Lorg/apache/lucene/index/CoalescedDeletes$3;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->numTop:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 140
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->top:[Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    iget v2, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->numTop:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->numTop:I

    iget-object v0, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->queue:Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    aput-object v0, v1, v2

    .line 141
    iget-object v0, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->queue:Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;->size()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->queue:Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    iget-object v0, v0, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->current:Lorg/apache/lucene/index/Term;

    iget-object v1, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->top:[Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    aget-object v1, v1, v3

    iget-object v1, v1, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->current:Lorg/apache/lucene/index/Term;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->top:[Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    aget-object v0, v0, v3

    iget-object v0, v0, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->current:Lorg/apache/lucene/index/Term;

    iput-object v0, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->current:Lorg/apache/lucene/index/Term;

    .line 147
    return-void
.end method

.method private pushTop()V
    .locals 3

    .prologue
    .line 151
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->numTop:I

    if-ge v0, v1, :cond_1

    .line 152
    iget-object v1, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->top:[Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->iterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    iget-object v1, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->top:[Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    aget-object v2, v1, v0

    iget-object v1, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->top:[Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->iterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/Term;

    iput-object v1, v2, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->current:Lorg/apache/lucene/index/Term;

    .line 154
    iget-object v1, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->queue:Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;

    iget-object v2, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->top:[Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->top:[Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    iput-object v2, v1, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->current:Lorg/apache/lucene/index/Term;

    goto :goto_1

    .line 160
    :cond_1
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->numTop:I

    .line 161
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 107
    iget-object v2, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->queue:Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 116
    :cond_0
    :goto_0
    return v1

    .line 111
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->numTop:I

    if-ge v0, v2, :cond_2

    .line 112
    iget-object v2, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->top:[Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;

    aget-object v2, v2, v0

    iget-object v2, v2, Lorg/apache/lucene/index/CoalescedDeletes$SubIterator;->iterator:Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 116
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 149
    invoke-virtual {p0}, Lorg/apache/lucene/index/CoalescedDeletes$3;->next()Lorg/apache/lucene/index/Term;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lorg/apache/lucene/index/CoalescedDeletes$3;->pushTop()V

    .line 124
    iget-object v0, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->queue:Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/CoalescedDeletes$TermMergeQueue;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 125
    invoke-direct {p0}, Lorg/apache/lucene/index/CoalescedDeletes$3;->pullTop()V

    .line 129
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->current:Lorg/apache/lucene/index/Term;

    return-object v0

    .line 127
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/CoalescedDeletes$3;->current:Lorg/apache/lucene/index/Term;

    goto :goto_0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 133
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
