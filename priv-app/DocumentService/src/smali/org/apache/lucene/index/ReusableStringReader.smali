.class final Lorg/apache/lucene/index/ReusableStringReader;
.super Ljava/io/Reader;
.source "ReusableStringReader.java"


# instance fields
.field left:I

.field s:Ljava/lang/String;

.field upto:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/io/Reader;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 58
    return-void
.end method

.method init(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lorg/apache/lucene/index/ReusableStringReader;->s:Ljava/lang/String;

    .line 31
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/ReusableStringReader;->left:I

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/ReusableStringReader;->upto:I

    .line 33
    return-void
.end method

.method public read([C)I
    .locals 2
    .param p1, "c"    # [C

    .prologue
    .line 36
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/index/ReusableStringReader;->read([CII)I

    move-result v0

    return v0
.end method

.method public read([CII)I
    .locals 5
    .param p1, "c"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 40
    iget v1, p0, Lorg/apache/lucene/index/ReusableStringReader;->left:I

    if-le v1, p3, :cond_0

    .line 41
    iget-object v1, p0, Lorg/apache/lucene/index/ReusableStringReader;->s:Ljava/lang/String;

    iget v2, p0, Lorg/apache/lucene/index/ReusableStringReader;->upto:I

    iget v3, p0, Lorg/apache/lucene/index/ReusableStringReader;->upto:I

    add-int/2addr v3, p3

    invoke-virtual {v1, v2, v3, p1, p2}, Ljava/lang/String;->getChars(II[CI)V

    .line 42
    iget v1, p0, Lorg/apache/lucene/index/ReusableStringReader;->upto:I

    add-int/2addr v1, p3

    iput v1, p0, Lorg/apache/lucene/index/ReusableStringReader;->upto:I

    .line 43
    iget v1, p0, Lorg/apache/lucene/index/ReusableStringReader;->left:I

    sub-int/2addr v1, p3

    iput v1, p0, Lorg/apache/lucene/index/ReusableStringReader;->left:I

    .line 54
    .end local p3    # "len":I
    :goto_0
    return p3

    .line 45
    .restart local p3    # "len":I
    :cond_0
    iget v1, p0, Lorg/apache/lucene/index/ReusableStringReader;->left:I

    if-nez v1, :cond_1

    .line 47
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/ReusableStringReader;->s:Ljava/lang/String;

    .line 48
    const/4 p3, -0x1

    goto :goto_0

    .line 50
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/ReusableStringReader;->s:Ljava/lang/String;

    iget v2, p0, Lorg/apache/lucene/index/ReusableStringReader;->upto:I

    iget v3, p0, Lorg/apache/lucene/index/ReusableStringReader;->upto:I

    iget v4, p0, Lorg/apache/lucene/index/ReusableStringReader;->left:I

    add-int/2addr v3, v4

    invoke-virtual {v1, v2, v3, p1, p2}, Ljava/lang/String;->getChars(II[CI)V

    .line 51
    iget v0, p0, Lorg/apache/lucene/index/ReusableStringReader;->left:I

    .line 52
    .local v0, "r":I
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/index/ReusableStringReader;->left:I

    .line 53
    iget-object v1, p0, Lorg/apache/lucene/index/ReusableStringReader;->s:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/index/ReusableStringReader;->upto:I

    move p3, v0

    .line 54
    goto :goto_0
.end method
