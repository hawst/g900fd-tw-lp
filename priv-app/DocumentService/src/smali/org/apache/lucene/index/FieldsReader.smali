.class final Lorg/apache/lucene/index/FieldsReader;
.super Ljava/lang/Object;
.source "FieldsReader.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/FieldsReader$1;,
        Lorg/apache/lucene/index/FieldsReader$LazyField;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final cloneableFieldsStream:Lorg/apache/lucene/store/IndexInput;

.field private final cloneableIndexStream:Lorg/apache/lucene/store/IndexInput;

.field private closed:Z

.field private docStoreOffset:I

.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private final fieldsStream:Lorg/apache/lucene/store/IndexInput;

.field private fieldsStreamTL:Lorg/apache/lucene/util/CloseableThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/CloseableThreadLocal",
            "<",
            "Lorg/apache/lucene/store/IndexInput;",
            ">;"
        }
    .end annotation
.end field

.field private final format:I

.field private final formatSize:I

.field private final indexStream:Lorg/apache/lucene/store/IndexInput;

.field private isOriginal:Z

.field private numTotalDocs:I

.field private size:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lorg/apache/lucene/index/FieldsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FieldsReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/index/FieldInfos;IIIIILorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)V
    .locals 1
    .param p1, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p2, "numTotalDocs"    # I
    .param p3, "size"    # I
    .param p4, "format"    # I
    .param p5, "formatSize"    # I
    .param p6, "docStoreOffset"    # I
    .param p7, "cloneableFieldsStream"    # Lorg/apache/lucene/store/IndexInput;
    .param p8, "cloneableIndexStream"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-direct {v0}, Lorg/apache/lucene/util/CloseableThreadLocal;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStreamTL:Lorg/apache/lucene/util/CloseableThreadLocal;

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/FieldsReader;->isOriginal:Z

    .line 105
    iput-object p1, p0, Lorg/apache/lucene/index/FieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 106
    iput p2, p0, Lorg/apache/lucene/index/FieldsReader;->numTotalDocs:I

    .line 107
    iput p3, p0, Lorg/apache/lucene/index/FieldsReader;->size:I

    .line 108
    iput p4, p0, Lorg/apache/lucene/index/FieldsReader;->format:I

    .line 109
    iput p5, p0, Lorg/apache/lucene/index/FieldsReader;->formatSize:I

    .line 110
    iput p6, p0, Lorg/apache/lucene/index/FieldsReader;->docStoreOffset:I

    .line 111
    iput-object p7, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableFieldsStream:Lorg/apache/lucene/store/IndexInput;

    .line 112
    iput-object p8, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableIndexStream:Lorg/apache/lucene/store/IndexInput;

    .line 113
    invoke-virtual {p7}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/IndexInput;

    iput-object v0, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    .line 114
    invoke-virtual {p8}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/IndexInput;

    iput-object v0, p0, Lorg/apache/lucene/index/FieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    .line 115
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 7
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "fn"    # Lorg/apache/lucene/index/FieldInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    const/16 v4, 0x400

    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/FieldsReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;III)V

    .line 119
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;I)V
    .locals 7
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "fn"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "readBufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    const/4 v5, -0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/FieldsReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;III)V

    .line 123
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;III)V
    .locals 12
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "fn"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "readBufferSize"    # I
    .param p5, "docStoreOffset"    # I
    .param p6, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v7, Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-direct {v7}, Lorg/apache/lucene/util/CloseableThreadLocal;-><init>()V

    iput-object v7, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStreamTL:Lorg/apache/lucene/util/CloseableThreadLocal;

    .line 69
    const/4 v7, 0x0

    iput-boolean v7, p0, Lorg/apache/lucene/index/FieldsReader;->isOriginal:Z

    .line 126
    const/4 v6, 0x0

    .line 127
    .local v6, "success":Z
    const/4 v7, 0x1

    iput-boolean v7, p0, Lorg/apache/lucene/index/FieldsReader;->isOriginal:Z

    .line 129
    :try_start_0
    iput-object p3, p0, Lorg/apache/lucene/index/FieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 131
    const-string/jumbo v7, "fdt"

    invoke-static {p2, v7}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move/from16 v0, p4

    invoke-virtual {p1, v7, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableFieldsStream:Lorg/apache/lucene/store/IndexInput;

    .line 132
    const-string/jumbo v7, "fdx"

    invoke-static {p2, v7}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 133
    .local v3, "indexStreamFN":Ljava/lang/String;
    move/from16 v0, p4

    invoke-virtual {p1, v3, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableIndexStream:Lorg/apache/lucene/store/IndexInput;

    .line 138
    iget-object v7, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableIndexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    .line 139
    .local v2, "firstInt":I
    if-nez v2, :cond_1

    .line 140
    const/4 v7, 0x0

    iput v7, p0, Lorg/apache/lucene/index/FieldsReader;->format:I

    .line 144
    :goto_0
    iget v7, p0, Lorg/apache/lucene/index/FieldsReader;->format:I

    const/4 v8, 0x3

    if-le v7, v8, :cond_2

    .line 145
    new-instance v7, Lorg/apache/lucene/index/IndexFormatTooNewException;

    iget-object v8, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableIndexStream:Lorg/apache/lucene/store/IndexInput;

    iget v9, p0, Lorg/apache/lucene/index/FieldsReader;->format:I

    const/4 v10, 0x0

    const/4 v11, 0x3

    invoke-direct {v7, v8, v9, v10, v11}, Lorg/apache/lucene/index/IndexFormatTooNewException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    .end local v2    # "firstInt":I
    .end local v3    # "indexStreamFN":Ljava/lang/String;
    :catchall_0
    move-exception v7

    if-nez v6, :cond_0

    .line 182
    invoke-virtual {p0}, Lorg/apache/lucene/index/FieldsReader;->close()V

    :cond_0
    throw v7

    .line 142
    .restart local v2    # "firstInt":I
    .restart local v3    # "indexStreamFN":Ljava/lang/String;
    :cond_1
    :try_start_1
    iput v2, p0, Lorg/apache/lucene/index/FieldsReader;->format:I

    goto :goto_0

    .line 147
    :cond_2
    iget v7, p0, Lorg/apache/lucene/index/FieldsReader;->format:I

    if-lez v7, :cond_4

    .line 148
    const/4 v7, 0x4

    iput v7, p0, Lorg/apache/lucene/index/FieldsReader;->formatSize:I

    .line 152
    :goto_1
    iget v7, p0, Lorg/apache/lucene/index/FieldsReader;->format:I

    const/4 v8, 0x1

    if-ge v7, v8, :cond_3

    .line 153
    iget-object v7, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableFieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->setModifiedUTF8StringsMode()V

    .line 155
    :cond_3
    iget-object v7, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableFieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/store/IndexInput;

    iput-object v7, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    .line 157
    iget-object v7, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableIndexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v8

    iget v7, p0, Lorg/apache/lucene/index/FieldsReader;->formatSize:I

    int-to-long v10, v7

    sub-long v4, v8, v10

    .line 159
    .local v4, "indexSize":J
    const/4 v7, -0x1

    move/from16 v0, p5

    if-eq v0, v7, :cond_5

    .line 161
    move/from16 v0, p5

    iput v0, p0, Lorg/apache/lucene/index/FieldsReader;->docStoreOffset:I

    .line 162
    move/from16 v0, p6

    iput v0, p0, Lorg/apache/lucene/index/FieldsReader;->size:I

    .line 166
    sget-boolean v7, Lorg/apache/lucene/index/FieldsReader;->$assertionsDisabled:Z

    if-nez v7, :cond_6

    const-wide/16 v8, 0x8

    div-long v8, v4, v8

    long-to-int v7, v8

    iget v8, p0, Lorg/apache/lucene/index/FieldsReader;->docStoreOffset:I

    add-int v8, v8, p6

    if-ge v7, v8, :cond_6

    new-instance v7, Ljava/lang/AssertionError;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "indexSize="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " size="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p6

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " docStoreOffset="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p5

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v7

    .line 150
    .end local v4    # "indexSize":J
    :cond_4
    const/4 v7, 0x0

    iput v7, p0, Lorg/apache/lucene/index/FieldsReader;->formatSize:I

    goto :goto_1

    .line 168
    .restart local v4    # "indexSize":J
    :cond_5
    const/4 v7, 0x0

    iput v7, p0, Lorg/apache/lucene/index/FieldsReader;->docStoreOffset:I

    .line 169
    const/4 v7, 0x3

    shr-long v8, v4, v7

    long-to-int v7, v8

    iput v7, p0, Lorg/apache/lucene/index/FieldsReader;->size:I

    .line 172
    :cond_6
    iget-object v7, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableIndexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->clone()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/store/IndexInput;

    iput-object v7, p0, Lorg/apache/lucene/index/FieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    .line 173
    const/4 v7, 0x3

    shr-long v8, v4, v7

    long-to-int v7, v8

    iput v7, p0, Lorg/apache/lucene/index/FieldsReader;->numTotalDocs:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 174
    const/4 v6, 0x1

    .line 181
    if-nez v6, :cond_7

    .line 182
    invoke-virtual {p0}, Lorg/apache/lucene/index/FieldsReader;->close()V

    .line 185
    :cond_7
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/index/FieldsReader;)Lorg/apache/lucene/util/CloseableThreadLocal;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/FieldsReader;

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStreamTL:Lorg/apache/lucene/util/CloseableThreadLocal;

    return-object v0
.end method

.method static synthetic access$100(Lorg/apache/lucene/index/FieldsReader;)Lorg/apache/lucene/store/IndexInput;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/FieldsReader;

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableFieldsStream:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method static synthetic access$200(Lorg/apache/lucene/index/FieldsReader;)V
    .locals 0
    .param p0, "x0"    # Lorg/apache/lucene/index/FieldsReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/lucene/index/FieldsReader;->ensureOpen()V

    return-void
.end method

.method static synthetic access$300(Lorg/apache/lucene/index/FieldsReader;[B)[B
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/FieldsReader;
    .param p1, "x1"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/FieldsReader;->uncompress([B)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lorg/apache/lucene/index/FieldsReader;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/index/FieldsReader;

    .prologue
    .line 46
    iget v0, p0, Lorg/apache/lucene/index/FieldsReader;->format:I

    return v0
.end method

.method private addField(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/index/FieldInfo;ZZZI)V
    .locals 12
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "fi"    # Lorg/apache/lucene/index/FieldInfo;
    .param p3, "binary"    # Z
    .param p4, "compressed"    # Z
    .param p5, "tokenize"    # Z
    .param p6, "numeric"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 397
    if-eqz p3, :cond_1

    .line 398
    iget-object v2, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v9

    .line 399
    .local v9, "toRead":I
    new-array v8, v9, [B

    .line 400
    .local v8, "b":[B
    iget-object v2, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    const/4 v3, 0x0

    array-length v4, v8

    invoke-virtual {v2, v8, v3, v4}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 401
    if-eqz p4, :cond_0

    .line 402
    new-instance v1, Lorg/apache/lucene/document/Field;

    iget-object v2, p2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v8}, Lorg/apache/lucene/index/FieldsReader;->uncompress([B)[B

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;[B)V

    .line 432
    .end local v8    # "b":[B
    .end local v9    # "toRead":I
    .local v1, "f":Lorg/apache/lucene/document/AbstractField;
    :goto_0
    iget-object v2, p2, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/document/AbstractField;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 433
    iget-boolean v2, p2, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    invoke-virtual {v1, v2}, Lorg/apache/lucene/document/AbstractField;->setOmitNorms(Z)V

    .line 434
    invoke-virtual {p1, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/document/Fieldable;)V

    .line 435
    return-void

    .line 404
    .end local v1    # "f":Lorg/apache/lucene/document/AbstractField;
    .restart local v8    # "b":[B
    .restart local v9    # "toRead":I
    :cond_0
    new-instance v1, Lorg/apache/lucene/document/Field;

    iget-object v2, p2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v8}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;[B)V

    .restart local v1    # "f":Lorg/apache/lucene/document/AbstractField;
    goto :goto_0

    .line 406
    .end local v1    # "f":Lorg/apache/lucene/document/AbstractField;
    .end local v8    # "b":[B
    .end local v9    # "toRead":I
    :cond_1
    if-eqz p6, :cond_2

    .line 407
    move/from16 v0, p6

    invoke-direct {p0, p2, v0}, Lorg/apache/lucene/index/FieldsReader;->loadNumericField(Lorg/apache/lucene/index/FieldInfo;I)Lorg/apache/lucene/document/NumericField;

    move-result-object v1

    .restart local v1    # "f":Lorg/apache/lucene/document/AbstractField;
    goto :goto_0

    .line 409
    .end local v1    # "f":Lorg/apache/lucene/document/AbstractField;
    :cond_2
    sget-object v5, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    .line 410
    .local v5, "store":Lorg/apache/lucene/document/Field$Store;
    iget-boolean v2, p2, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    move/from16 v0, p5

    invoke-static {v2, v0}, Lorg/apache/lucene/document/Field$Index;->toIndex(ZZ)Lorg/apache/lucene/document/Field$Index;

    move-result-object v6

    .line 411
    .local v6, "index":Lorg/apache/lucene/document/Field$Index;
    iget-boolean v2, p2, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/document/Field$TermVector;->toTermVector(ZZZ)Lorg/apache/lucene/document/Field$TermVector;

    move-result-object v7

    .line 412
    .local v7, "termVector":Lorg/apache/lucene/document/Field$TermVector;
    if-eqz p4, :cond_3

    .line 413
    iget-object v2, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v9

    .line 414
    .restart local v9    # "toRead":I
    new-array v8, v9, [B

    .line 415
    .restart local v8    # "b":[B
    iget-object v2, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    const/4 v3, 0x0

    array-length v4, v8

    invoke-virtual {v2, v8, v3, v4}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 416
    new-instance v1, Lorg/apache/lucene/document/Field;

    iget-object v2, p2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/String;

    invoke-direct {p0, v8}, Lorg/apache/lucene/index/FieldsReader;->uncompress([B)[B

    move-result-object v10

    const-string/jumbo v11, "UTF-8"

    invoke-direct {v4, v10, v11}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-direct/range {v1 .. v7}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;ZLjava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)V

    .line 422
    .restart local v1    # "f":Lorg/apache/lucene/document/AbstractField;
    goto :goto_0

    .line 423
    .end local v1    # "f":Lorg/apache/lucene/document/AbstractField;
    .end local v8    # "b":[B
    .end local v9    # "toRead":I
    :cond_3
    new-instance v1, Lorg/apache/lucene/document/Field;

    iget-object v2, p2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v1 .. v7}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;ZLjava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)V

    .restart local v1    # "f":Lorg/apache/lucene/document/AbstractField;
    goto :goto_0
.end method

.method private addFieldLazy(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/index/FieldInfo;ZZZZI)V
    .locals 25
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "fi"    # Lorg/apache/lucene/index/FieldInfo;
    .param p3, "binary"    # Z
    .param p4, "compressed"    # Z
    .param p5, "tokenize"    # Z
    .param p6, "cacheResult"    # Z
    .param p7, "numeric"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 356
    if-eqz p3, :cond_0

    .line 357
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v9

    .line 358
    .local v9, "toRead":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v10

    .line 359
    .local v10, "pointer":J
    new-instance v5, Lorg/apache/lucene/index/FieldsReader$LazyField;

    move-object/from16 v0, p2

    iget-object v7, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    sget-object v8, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    move-object/from16 v6, p0

    move/from16 v12, p3

    move/from16 v13, p4

    move/from16 v14, p6

    invoke-direct/range {v5 .. v14}, Lorg/apache/lucene/index/FieldsReader$LazyField;-><init>(Lorg/apache/lucene/index/FieldsReader;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;IJZZZ)V

    .line 361
    .local v5, "f":Lorg/apache/lucene/document/AbstractField;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    int-to-long v6, v9

    add-long/2addr v6, v10

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 388
    .end local v9    # "toRead":I
    .end local v10    # "pointer":J
    :goto_0
    move-object/from16 v0, p2

    iget-boolean v4, v0, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    invoke-virtual {v5, v4}, Lorg/apache/lucene/document/AbstractField;->setOmitNorms(Z)V

    .line 389
    move-object/from16 v0, p2

    iget-object v4, v0, Lorg/apache/lucene/index/FieldInfo;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v5, v4}, Lorg/apache/lucene/document/AbstractField;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 390
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/document/Fieldable;)V

    .line 391
    return-void

    .line 362
    .end local v5    # "f":Lorg/apache/lucene/document/AbstractField;
    :cond_0
    if-eqz p7, :cond_1

    .line 363
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p7

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/FieldsReader;->loadNumericField(Lorg/apache/lucene/index/FieldInfo;I)Lorg/apache/lucene/document/NumericField;

    move-result-object v5

    .restart local v5    # "f":Lorg/apache/lucene/document/AbstractField;
    goto :goto_0

    .line 365
    .end local v5    # "f":Lorg/apache/lucene/document/AbstractField;
    :cond_1
    sget-object v8, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    .line 366
    .local v8, "store":Lorg/apache/lucene/document/Field$Store;
    move-object/from16 v0, p2

    iget-boolean v4, v0, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    move/from16 v0, p5

    invoke-static {v4, v0}, Lorg/apache/lucene/document/Field$Index;->toIndex(ZZ)Lorg/apache/lucene/document/Field$Index;

    move-result-object v17

    .line 367
    .local v17, "index":Lorg/apache/lucene/document/Field$Index;
    move-object/from16 v0, p2

    iget-boolean v4, v0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v6, v7}, Lorg/apache/lucene/document/Field$TermVector;->toTermVector(ZZZ)Lorg/apache/lucene/document/Field$TermVector;

    move-result-object v18

    .line 369
    .local v18, "termVector":Lorg/apache/lucene/document/Field$TermVector;
    if-eqz p4, :cond_2

    .line 370
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v9

    .line 371
    .restart local v9    # "toRead":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v10

    .line 372
    .restart local v10    # "pointer":J
    new-instance v5, Lorg/apache/lucene/index/FieldsReader$LazyField;

    move-object/from16 v0, p2

    iget-object v7, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v6, p0

    move/from16 v12, p3

    move/from16 v13, p4

    move/from16 v14, p6

    invoke-direct/range {v5 .. v14}, Lorg/apache/lucene/index/FieldsReader$LazyField;-><init>(Lorg/apache/lucene/index/FieldsReader;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;IJZZZ)V

    .line 374
    .restart local v5    # "f":Lorg/apache/lucene/document/AbstractField;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    int-to-long v6, v9

    add-long/2addr v6, v10

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto :goto_0

    .line 376
    .end local v5    # "f":Lorg/apache/lucene/document/AbstractField;
    .end local v9    # "toRead":I
    .end local v10    # "pointer":J
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v19

    .line 377
    .local v19, "length":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v10

    .line 379
    .restart local v10    # "pointer":J
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/index/FieldsReader;->format:I

    const/4 v6, 0x1

    if-lt v4, v6, :cond_3

    .line 380
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    move/from16 v0, v19

    int-to-long v6, v0

    add-long/2addr v6, v10

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 384
    :goto_1
    new-instance v5, Lorg/apache/lucene/index/FieldsReader$LazyField;

    move-object/from16 v0, p2

    iget-object v15, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object v13, v5

    move-object/from16 v14, p0

    move-object/from16 v16, v8

    move-wide/from16 v20, v10

    move/from16 v22, p3

    move/from16 v23, p4

    move/from16 v24, p6

    invoke-direct/range {v13 .. v24}, Lorg/apache/lucene/index/FieldsReader$LazyField;-><init>(Lorg/apache/lucene/index/FieldsReader;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;IJZZZ)V

    .restart local v5    # "f":Lorg/apache/lucene/document/AbstractField;
    goto/16 :goto_0

    .line 382
    .end local v5    # "f":Lorg/apache/lucene/document/AbstractField;
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Lorg/apache/lucene/store/IndexInput;->skipChars(I)V

    goto :goto_1
.end method

.method private addFieldSize(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/index/FieldInfo;ZZI)I
    .locals 6
    .param p1, "doc"    # Lorg/apache/lucene/document/Document;
    .param p2, "fi"    # Lorg/apache/lucene/index/FieldInfo;
    .param p3, "binary"    # Z
    .param p4, "compressed"    # Z
    .param p5, "numeric"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 442
    sparse-switch p5, :sswitch_data_0

    .line 456
    new-instance v3, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Invalid numeric type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 444
    :sswitch_0
    iget-object v3, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    .line 445
    .local v1, "size":I
    if-nez p3, :cond_0

    if-eqz p4, :cond_1

    :cond_0
    move v0, v1

    .line 458
    .local v0, "bytesize":I
    :goto_0
    const/4 v3, 0x4

    new-array v2, v3, [B

    .line 459
    .local v2, "sizebytes":[B
    const/4 v3, 0x0

    ushr-int/lit8 v4, v0, 0x18

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 460
    const/4 v3, 0x1

    ushr-int/lit8 v4, v0, 0x10

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 461
    const/4 v3, 0x2

    ushr-int/lit8 v4, v0, 0x8

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 462
    const/4 v3, 0x3

    int-to-byte v4, v0

    aput-byte v4, v2, v3

    .line 463
    new-instance v3, Lorg/apache/lucene/document/Field;

    iget-object v4, p2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v4, v2}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;[B)V

    invoke-virtual {p1, v3}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/document/Fieldable;)V

    .line 464
    return v1

    .line 445
    .end local v0    # "bytesize":I
    .end local v2    # "sizebytes":[B
    :cond_1
    mul-int/lit8 v0, v1, 0x2

    goto :goto_0

    .line 449
    .end local v1    # "size":I
    :sswitch_1
    const/4 v0, 0x4

    .restart local v0    # "bytesize":I
    move v1, v0

    .line 450
    .restart local v1    # "size":I
    goto :goto_0

    .line 453
    .end local v0    # "bytesize":I
    .end local v1    # "size":I
    :sswitch_2
    const/16 v0, 0x8

    .restart local v0    # "bytesize":I
    move v1, v0

    .line 454
    .restart local v1    # "size":I
    goto :goto_0

    .line 442
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_1
        0x20 -> :sswitch_2
    .end sparse-switch
.end method

.method static detectCodeVersion(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p1, "segment"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    const-string/jumbo v2, "fdx"

    invoke-static {p1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x400

    invoke-virtual {p0, v2, v3}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 91
    .local v1, "idxStream":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    .line 92
    .local v0, "format":I
    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    .line 93
    const-string/jumbo v2, "2.x"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    :goto_0
    return-object v2

    .line 95
    :cond_0
    :try_start_1
    const-string/jumbo v2, "3.0"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 98
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    goto :goto_0

    .end local v0    # "format":I
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    throw v2
.end method

.method private ensureOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 191
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldsReader;->closed:Z

    if-eqz v0, :cond_0

    .line 192
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string/jumbo v1, "this FieldsReader is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_0
    return-void
.end method

.method private loadNumericField(Lorg/apache/lucene/index/FieldInfo;I)Lorg/apache/lucene/document/NumericField;
    .locals 4
    .param p1, "fi"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "numeric"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 339
    sget-boolean v0, Lorg/apache/lucene/index/FieldsReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 340
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 350
    new-instance v0, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid numeric type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 342
    :sswitch_0
    new-instance v0, Lorg/apache/lucene/document/NumericField;

    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    sget-object v2, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    iget-boolean v3, p1, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/NumericField;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Z)V

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/NumericField;->setIntValue(I)Lorg/apache/lucene/document/NumericField;

    move-result-object v0

    .line 348
    :goto_0
    return-object v0

    .line 344
    :sswitch_1
    new-instance v0, Lorg/apache/lucene/document/NumericField;

    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    sget-object v2, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    iget-boolean v3, p1, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/NumericField;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Z)V

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/document/NumericField;->setLongValue(J)Lorg/apache/lucene/document/NumericField;

    move-result-object v0

    goto :goto_0

    .line 346
    :sswitch_2
    new-instance v0, Lorg/apache/lucene/document/NumericField;

    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    sget-object v2, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    iget-boolean v3, p1, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/NumericField;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Z)V

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/NumericField;->setFloatValue(F)Lorg/apache/lucene/document/NumericField;

    move-result-object v0

    goto :goto_0

    .line 348
    :sswitch_3
    new-instance v0, Lorg/apache/lucene/document/NumericField;

    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    sget-object v2, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    iget-boolean v3, p1, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/NumericField;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Z)V

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/document/NumericField;->setDoubleValue(D)Lorg/apache/lucene/document/NumericField;

    move-result-object v0

    goto :goto_0

    .line 340
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch
.end method

.method private final seekIndex(I)V
    .locals 8
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    iget v1, p0, Lorg/apache/lucene/index/FieldsReader;->formatSize:I

    int-to-long v2, v1

    iget v1, p0, Lorg/apache/lucene/index/FieldsReader;->docStoreOffset:I

    add-int/2addr v1, p1

    int-to-long v4, v1

    const-wide/16 v6, 0x8

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 219
    return-void
.end method

.method private skipField(ZZI)V
    .locals 4
    .param p1, "binary"    # Z
    .param p2, "compressed"    # Z
    .param p3, "numeric"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 310
    sparse-switch p3, :sswitch_data_0

    .line 323
    new-instance v1, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Invalid numeric type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 312
    :sswitch_0
    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 326
    .local v0, "numBytes":I
    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/index/FieldsReader;->skipFieldBytes(ZZI)V

    .line 327
    return-void

    .line 316
    .end local v0    # "numBytes":I
    :sswitch_1
    const/4 v0, 0x4

    .line 317
    .restart local v0    # "numBytes":I
    goto :goto_0

    .line 320
    .end local v0    # "numBytes":I
    :sswitch_2
    const/16 v0, 0x8

    .line 321
    .restart local v0    # "numBytes":I
    goto :goto_0

    .line 310
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_1
        0x20 -> :sswitch_2
    .end sparse-switch
.end method

.method private skipFieldBytes(ZZI)V
    .locals 6
    .param p1, "binary"    # Z
    .param p2, "compressed"    # Z
    .param p3, "toRead"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 330
    iget v0, p0, Lorg/apache/lucene/index/FieldsReader;->format:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    .line 331
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v2

    int-to-long v4, p3

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 336
    :goto_0
    return-void

    .line 334
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0, p3}, Lorg/apache/lucene/store/IndexInput;->skipChars(I)V

    goto :goto_0
.end method

.method private uncompress([B)[B
    .locals 4
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;
        }
    .end annotation

    .prologue
    .line 619
    :try_start_0
    invoke-static {p1}, Lorg/apache/lucene/document/CompressionTools;->decompress([B)[B
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 620
    :catch_0
    move-exception v0

    .line 622
    .local v0, "e":Ljava/util/zip/DataFormatException;
    new-instance v1, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "field data are in wrong format: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/zip/DataFormatException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    .line 623
    .local v1, "newException":Lorg/apache/lucene/index/CorruptIndexException;
    invoke-virtual {v1, v0}, Lorg/apache/lucene/index/CorruptIndexException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 624
    throw v1
.end method


# virtual methods
.method canReadRawDocs()Z
    .locals 2

    .prologue
    .line 226
    iget v0, p0, Lorg/apache/lucene/index/FieldsReader;->format:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clone()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 78
    invoke-direct {p0}, Lorg/apache/lucene/index/FieldsReader;->ensureOpen()V

    .line 79
    new-instance v0, Lorg/apache/lucene/index/FieldsReader;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget v2, p0, Lorg/apache/lucene/index/FieldsReader;->numTotalDocs:I

    iget v3, p0, Lorg/apache/lucene/index/FieldsReader;->size:I

    iget v4, p0, Lorg/apache/lucene/index/FieldsReader;->format:I

    iget v5, p0, Lorg/apache/lucene/index/FieldsReader;->formatSize:I

    iget v6, p0, Lorg/apache/lucene/index/FieldsReader;->docStoreOffset:I

    iget-object v7, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableFieldsStream:Lorg/apache/lucene/store/IndexInput;

    iget-object v8, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableIndexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-direct/range {v0 .. v8}, Lorg/apache/lucene/index/FieldsReader;-><init>(Lorg/apache/lucene/index/FieldInfos;IIIIILorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)V

    return-object v0
.end method

.method public final close()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 203
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldsReader;->closed:Z

    if-nez v0, :cond_0

    .line 204
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldsReader;->isOriginal:Z

    if-eqz v0, :cond_1

    .line 205
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/io/Closeable;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v1, v0, v2

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v1, v0, v3

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStreamTL:Lorg/apache/lucene/util/CloseableThreadLocal;

    aput-object v1, v0, v4

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableFieldsStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v1, v0, v5

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/lucene/index/FieldsReader;->cloneableIndexStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 209
    :goto_0
    iput-boolean v3, p0, Lorg/apache/lucene/index/FieldsReader;->closed:Z

    .line 211
    :cond_0
    return-void

    .line 207
    :cond_1
    new-array v0, v5, [Ljava/io/Closeable;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v1, v0, v2

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v1, v0, v3

    iget-object v1, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStreamTL:Lorg/apache/lucene/util/CloseableThreadLocal;

    aput-object v1, v0, v4

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    goto :goto_0
.end method

.method final doc(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;
    .locals 24
    .param p1, "n"    # I
    .param p2, "fieldSelector"    # Lorg/apache/lucene/document/FieldSelector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 230
    invoke-direct/range {p0 .. p1}, Lorg/apache/lucene/index/FieldsReader;->seekIndex(I)V

    .line 231
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/FieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v22

    .line 232
    .local v22, "position":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    move-wide/from16 v0, v22

    invoke-virtual {v2, v0, v1}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 234
    new-instance v3, Lorg/apache/lucene/document/Document;

    invoke-direct {v3}, Lorg/apache/lucene/document/Document;-><init>()V

    .line 235
    .local v3, "doc":Lorg/apache/lucene/document/Document;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v21

    .line 236
    .local v21, "numFields":I
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_0
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_7

    .line 237
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v19

    .line 238
    .local v19, "fieldNumber":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/FieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v4

    .line 239
    .local v4, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-nez p2, :cond_0

    sget-object v17, Lorg/apache/lucene/document/FieldSelectorResult;->LOAD:Lorg/apache/lucene/document/FieldSelectorResult;

    .line 241
    .local v17, "acceptField":Lorg/apache/lucene/document/FieldSelectorResult;
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v2

    and-int/lit16 v0, v2, 0xff

    move/from16 v18, v0

    .line 242
    .local v18, "bits":I
    sget-boolean v2, Lorg/apache/lucene/index/FieldsReader;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    const/16 v2, 0x3f

    move/from16 v0, v18

    if-le v0, v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "bits="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v2, v9}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 239
    .end local v17    # "acceptField":Lorg/apache/lucene/document/FieldSelectorResult;
    .end local v18    # "bits":I
    :cond_0
    iget-object v2, v4, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lorg/apache/lucene/document/FieldSelector;->accept(Ljava/lang/String;)Lorg/apache/lucene/document/FieldSelectorResult;

    move-result-object v17

    goto :goto_1

    .line 244
    .restart local v17    # "acceptField":Lorg/apache/lucene/document/FieldSelectorResult;
    .restart local v18    # "bits":I
    :cond_1
    and-int/lit8 v2, v18, 0x4

    if-eqz v2, :cond_3

    const/4 v6, 0x1

    .line 246
    .local v6, "compressed":Z
    :goto_2
    sget-boolean v2, Lorg/apache/lucene/index/FieldsReader;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    if-eqz v6, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/index/FieldsReader;->format:I

    const/4 v9, 0x2

    if-ge v2, v9, :cond_4

    .line 247
    :cond_2
    and-int/lit8 v2, v18, 0x1

    if-eqz v2, :cond_5

    const/4 v7, 0x1

    .line 248
    .local v7, "tokenize":Z
    :goto_3
    and-int/lit8 v2, v18, 0x2

    if-eqz v2, :cond_6

    const/4 v5, 0x1

    .line 249
    .local v5, "binary":Z
    :goto_4
    and-int/lit8 v8, v18, 0x38

    .line 251
    .local v8, "numeric":I
    sget-object v2, Lorg/apache/lucene/index/FieldsReader$1;->$SwitchMap$org$apache$lucene$document$FieldSelectorResult:[I

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/document/FieldSelectorResult;->ordinal()I

    move-result v9

    aget v2, v2, v9

    packed-switch v2, :pswitch_data_0

    .line 271
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6, v8}, Lorg/apache/lucene/index/FieldsReader;->skipField(ZZI)V

    .line 236
    :goto_5
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_0

    .line 244
    .end local v5    # "binary":Z
    .end local v6    # "compressed":Z
    .end local v7    # "tokenize":Z
    .end local v8    # "numeric":I
    :cond_3
    const/4 v6, 0x0

    goto :goto_2

    .line 246
    .restart local v6    # "compressed":Z
    :cond_4
    new-instance v2, Ljava/lang/AssertionError;

    const-string/jumbo v9, "compressed fields are only allowed in indexes of version <= 2.9"

    invoke-direct {v2, v9}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 247
    :cond_5
    const/4 v7, 0x0

    goto :goto_3

    .line 248
    .restart local v7    # "tokenize":Z
    :cond_6
    const/4 v5, 0x0

    goto :goto_4

    .restart local v5    # "binary":Z
    .restart local v8    # "numeric":I
    :pswitch_0
    move-object/from16 v2, p0

    .line 253
    invoke-direct/range {v2 .. v8}, Lorg/apache/lucene/index/FieldsReader;->addField(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/index/FieldInfo;ZZZI)V

    goto :goto_5

    :pswitch_1
    move-object/from16 v2, p0

    .line 256
    invoke-direct/range {v2 .. v8}, Lorg/apache/lucene/index/FieldsReader;->addField(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/index/FieldInfo;ZZZI)V

    .line 275
    .end local v4    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .end local v5    # "binary":Z
    .end local v6    # "compressed":Z
    .end local v7    # "tokenize":Z
    .end local v8    # "numeric":I
    .end local v17    # "acceptField":Lorg/apache/lucene/document/FieldSelectorResult;
    .end local v18    # "bits":I
    .end local v19    # "fieldNumber":I
    :cond_7
    :goto_6
    return-object v3

    .line 259
    .restart local v4    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .restart local v5    # "binary":Z
    .restart local v6    # "compressed":Z
    .restart local v7    # "tokenize":Z
    .restart local v8    # "numeric":I
    .restart local v17    # "acceptField":Lorg/apache/lucene/document/FieldSelectorResult;
    .restart local v18    # "bits":I
    .restart local v19    # "fieldNumber":I
    :pswitch_2
    const/4 v15, 0x1

    move-object/from16 v9, p0

    move-object v10, v3

    move-object v11, v4

    move v12, v5

    move v13, v6

    move v14, v7

    move/from16 v16, v8

    invoke-direct/range {v9 .. v16}, Lorg/apache/lucene/index/FieldsReader;->addFieldLazy(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/index/FieldInfo;ZZZZI)V

    goto :goto_5

    .line 262
    :pswitch_3
    const/4 v15, 0x0

    move-object/from16 v9, p0

    move-object v10, v3

    move-object v11, v4

    move v12, v5

    move v13, v6

    move v14, v7

    move/from16 v16, v8

    invoke-direct/range {v9 .. v16}, Lorg/apache/lucene/index/FieldsReader;->addFieldLazy(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/index/FieldInfo;ZZZZI)V

    goto :goto_5

    :pswitch_4
    move-object/from16 v9, p0

    move-object v10, v3

    move-object v11, v4

    move v12, v5

    move v13, v6

    move v14, v8

    .line 265
    invoke-direct/range {v9 .. v14}, Lorg/apache/lucene/index/FieldsReader;->addFieldSize(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/index/FieldInfo;ZZI)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6, v2}, Lorg/apache/lucene/index/FieldsReader;->skipFieldBytes(ZZI)V

    goto :goto_5

    :pswitch_5
    move-object/from16 v9, p0

    move-object v10, v3

    move-object v11, v4

    move v12, v5

    move v13, v6

    move v14, v8

    .line 268
    invoke-direct/range {v9 .. v14}, Lorg/apache/lucene/index/FieldsReader;->addFieldSize(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/index/FieldInfo;ZZI)I

    goto :goto_6

    .line 251
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method final rawDocs([III)Lorg/apache/lucene/store/IndexInput;
    .locals 12
    .param p1, "lengths"    # [I
    .param p2, "startDocID"    # I
    .param p3, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 283
    invoke-direct {p0, p2}, Lorg/apache/lucene/index/FieldsReader;->seekIndex(I)V

    .line 284
    iget-object v3, p0, Lorg/apache/lucene/index/FieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v8

    .line 285
    .local v8, "startOffset":J
    move-wide v4, v8

    .line 286
    .local v4, "lastOffset":J
    const/4 v0, 0x0

    .local v0, "count":I
    move v1, v0

    .line 287
    .end local v0    # "count":I
    .local v1, "count":I
    :goto_0
    if-ge v1, p3, :cond_2

    .line 289
    iget v3, p0, Lorg/apache/lucene/index/FieldsReader;->docStoreOffset:I

    add-int/2addr v3, p2

    add-int/2addr v3, v1

    add-int/lit8 v2, v3, 0x1

    .line 290
    .local v2, "docID":I
    sget-boolean v3, Lorg/apache/lucene/index/FieldsReader;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget v3, p0, Lorg/apache/lucene/index/FieldsReader;->numTotalDocs:I

    if-le v2, v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 291
    :cond_0
    iget v3, p0, Lorg/apache/lucene/index/FieldsReader;->numTotalDocs:I

    if-ge v2, v3, :cond_1

    .line 292
    iget-object v3, p0, Lorg/apache/lucene/index/FieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v6

    .line 295
    .local v6, "offset":J
    :goto_1
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    sub-long v10, v6, v4

    long-to-int v3, v10

    aput v3, p1, v1

    .line 296
    move-wide v4, v6

    move v1, v0

    .line 297
    .end local v0    # "count":I
    .restart local v1    # "count":I
    goto :goto_0

    .line 294
    .end local v6    # "offset":J
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v6

    .restart local v6    # "offset":J
    goto :goto_1

    .line 299
    .end local v2    # "docID":I
    .end local v6    # "offset":J
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3, v8, v9}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 301
    iget-object v3, p0, Lorg/apache/lucene/index/FieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    return-object v3
.end method

.method final size()I
    .locals 1

    .prologue
    .line 214
    iget v0, p0, Lorg/apache/lucene/index/FieldsReader;->size:I

    return v0
.end method
