.class Lorg/apache/lucene/index/CheckIndex$MySegmentTermDocs;
.super Lorg/apache/lucene/index/SegmentTermDocs;
.source "CheckIndex.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/CheckIndex;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MySegmentTermDocs"
.end annotation


# instance fields
.field delCount:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/SegmentReader;)V
    .locals 0
    .param p1, "p"    # Lorg/apache/lucene/index/SegmentReader;

    .prologue
    .line 280
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/SegmentTermDocs;-><init>(Lorg/apache/lucene/index/SegmentReader;)V

    .line 281
    return-void
.end method


# virtual methods
.method public seek(Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 285
    invoke-super {p0, p1}, Lorg/apache/lucene/index/SegmentTermDocs;->seek(Lorg/apache/lucene/index/Term;)V

    .line 286
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/CheckIndex$MySegmentTermDocs;->delCount:I

    .line 287
    return-void
.end method

.method protected skippingDoc()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 291
    iget v0, p0, Lorg/apache/lucene/index/CheckIndex$MySegmentTermDocs;->delCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/CheckIndex$MySegmentTermDocs;->delCount:I

    .line 292
    return-void
.end method
