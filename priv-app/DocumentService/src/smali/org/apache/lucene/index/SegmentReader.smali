.class public Lorg/apache/lucene/index/SegmentReader;
.super Lorg/apache/lucene/index/IndexReader;
.source "SegmentReader.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/SegmentReader$1;,
        Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;,
        Lorg/apache/lucene/index/SegmentReader$FieldsReaderLocal;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field core:Lorg/apache/lucene/index/SegmentCoreReaders;

.field deletedDocs:Lorg/apache/lucene/util/BitVector;

.field private deletedDocsDirty:Z

.field deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

.field fieldsReaderLocal:Lorg/apache/lucene/util/CloseableThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/CloseableThreadLocal",
            "<",
            "Lorg/apache/lucene/index/FieldsReader;",
            ">;"
        }
    .end annotation
.end field

.field norms:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/SegmentNorms;",
            ">;"
        }
    .end annotation
.end field

.field private normsDirty:Z

.field private pendingDeleteCount:I

.field private readBufferSize:I

.field protected readOnly:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private rollbackDeletedDocsDirty:Z

.field private rollbackHasChanges:Z

.field private rollbackNormsDirty:Z

.field private rollbackPendingDeleteCount:I

.field private rollbackSegmentInfo:Lorg/apache/lucene/index/SegmentInfo;

.field private si:Lorg/apache/lucene/index/SegmentInfo;

.field singleNormRef:Ljava/util/concurrent/atomic/AtomicInteger;

.field singleNormStream:Lorg/apache/lucene/store/IndexInput;

.field termVectorsLocal:Lorg/apache/lucene/util/CloseableThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/CloseableThreadLocal",
            "<",
            "Lorg/apache/lucene/index/TermVectorsReader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexReader;-><init>()V

    .line 53
    new-instance v0, Lorg/apache/lucene/index/SegmentReader$FieldsReaderLocal;

    invoke-direct {v0, p0, v2}, Lorg/apache/lucene/index/SegmentReader$FieldsReaderLocal;-><init>(Lorg/apache/lucene/index/SegmentReader;Lorg/apache/lucene/index/SegmentReader$1;)V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->fieldsReaderLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    .line 54
    new-instance v0, Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-direct {v0}, Lorg/apache/lucene/util/CloseableThreadLocal;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->termVectorsLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    .line 56
    iput-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    .line 57
    iput-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 58
    iput-boolean v1, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsDirty:Z

    .line 59
    iput-boolean v1, p0, Lorg/apache/lucene/index/SegmentReader;->normsDirty:Z

    .line 65
    iput-boolean v1, p0, Lorg/apache/lucene/index/SegmentReader;->rollbackHasChanges:Z

    .line 66
    iput-boolean v1, p0, Lorg/apache/lucene/index/SegmentReader;->rollbackDeletedDocsDirty:Z

    .line 67
    iput-boolean v1, p0, Lorg/apache/lucene/index/SegmentReader;->rollbackNormsDirty:Z

    .line 87
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    .line 920
    return-void
.end method

.method private checkDeletedCounts()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BitVector;->getRecomputedCount()I

    move-result v0

    .line 144
    .local v0, "recomputedCount":I
    sget-boolean v1, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v1

    if-eq v1, v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "deleted count="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v3}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " vs recomputed count="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 147
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I

    move-result v1

    if-eq v1, v0, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "delete count mismatch: info="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " vs BitVector="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 152
    :cond_1
    sget-boolean v1, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I

    move-result v1

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v2

    if-le v1, v2, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "delete count mismatch: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") exceeds max doc ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") for segment "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 154
    :cond_2
    const/4 v1, 0x1

    return v1
.end method

.method private declared-synchronized commitChanges(Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 334
    .local p1, "commitUserData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v4, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsDirty:Z

    if-eqz v4, :cond_3

    .line 335
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->advanceDelGen()V

    .line 337
    sget-boolean v4, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v4}, Lorg/apache/lucene/util/BitVector;->size()I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    iget v5, v5, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-eq v4, v5, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 342
    :cond_0
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getDelFileName()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 343
    .local v0, "delFileName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 345
    .local v3, "success":Z
    :try_start_2
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->directory()Lorg/apache/lucene/store/Directory;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Lorg/apache/lucene/util/BitVector;->write(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 346
    const/4 v3, 0x1

    .line 348
    if-nez v3, :cond_1

    .line 350
    :try_start_3
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->directory()Lorg/apache/lucene/store/Directory;

    move-result-object v4

    invoke-virtual {v4, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 358
    :cond_1
    :goto_0
    :try_start_4
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v5, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I

    move-result v5

    iget v6, p0, Lorg/apache/lucene/index/SegmentReader;->pendingDeleteCount:I

    add-int/2addr v5, v6

    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/SegmentInfo;->setDelCount(I)V

    .line 359
    const/4 v4, 0x0

    iput v4, p0, Lorg/apache/lucene/index/SegmentReader;->pendingDeleteCount:I

    .line 360
    sget-boolean v4, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v4}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I

    move-result v5

    if-eq v4, v5, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "delete count mismatch during commit: info="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " vs BitVector="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v6}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 348
    :catchall_1
    move-exception v4

    if-nez v3, :cond_2

    .line 350
    :try_start_5
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->directory()Lorg/apache/lucene/store/Directory;

    move-result-object v5

    invoke-virtual {v5, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 354
    :cond_2
    :goto_1
    :try_start_6
    throw v4

    .line 362
    .end local v0    # "delFileName":Ljava/lang/String;
    .end local v3    # "success":Z
    :cond_3
    sget-boolean v4, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    iget v4, p0, Lorg/apache/lucene/index/SegmentReader;->pendingDeleteCount:I

    if-eqz v4, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 365
    :cond_4
    iget-boolean v4, p0, Lorg/apache/lucene/index/SegmentReader;->normsDirty:Z

    if-eqz v4, :cond_6

    .line 366
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v5, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v5}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/SegmentInfo;->setNumFields(I)V

    .line 367
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentNorms;

    .line 368
    .local v2, "norm":Lorg/apache/lucene/index/SegmentNorms;
    iget-boolean v4, v2, Lorg/apache/lucene/index/SegmentNorms;->dirty:Z

    if-eqz v4, :cond_5

    .line 369
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/SegmentNorms;->reWrite(Lorg/apache/lucene/index/SegmentInfo;)V

    goto :goto_2

    .line 373
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "norm":Lorg/apache/lucene/index/SegmentNorms;
    :cond_6
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsDirty:Z

    .line 374
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/apache/lucene/index/SegmentReader;->normsDirty:Z

    .line 375
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 376
    monitor-exit p0

    return-void

    .line 351
    .restart local v0    # "delFileName":Ljava/lang/String;
    .restart local v3    # "success":Z
    :catch_0
    move-exception v5

    goto :goto_1

    :catch_1
    move-exception v4

    goto/16 :goto_0
.end method

.method public static get(ZLorg/apache/lucene/index/SegmentInfo;I)Lorg/apache/lucene/index/SegmentReader;
    .locals 6
    .param p0, "readOnly"    # Z
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    const/16 v3, 0x400

    const/4 v4, 0x1

    move v0, p0

    move-object v2, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/index/SegmentReader;->get(ZLorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;IZI)Lorg/apache/lucene/index/SegmentReader;

    move-result-object v0

    return-object v0
.end method

.method public static get(ZLorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;IZI)Lorg/apache/lucene/index/SegmentReader;
    .locals 7
    .param p0, "readOnly"    # Z
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "readBufferSize"    # I
    .param p4, "doOpenStores"    # Z
    .param p5, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    if-eqz p0, :cond_2

    new-instance v1, Lorg/apache/lucene/index/ReadOnlySegmentReader;

    invoke-direct {v1}, Lorg/apache/lucene/index/ReadOnlySegmentReader;-><init>()V

    .line 109
    .local v1, "instance":Lorg/apache/lucene/index/SegmentReader;
    :goto_0
    iput-boolean p0, v1, Lorg/apache/lucene/index/SegmentReader;->readOnly:Z

    .line 110
    iput-object p2, v1, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    .line 111
    iput p3, v1, Lorg/apache/lucene/index/SegmentReader;->readBufferSize:I

    .line 113
    const/4 v6, 0x0

    .line 116
    .local v6, "success":Z
    :try_start_0
    new-instance v0, Lorg/apache/lucene/index/SegmentCoreReaders;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/SegmentCoreReaders;-><init>(Lorg/apache/lucene/index/SegmentReader;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;II)V

    iput-object v0, v1, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    .line 117
    if-eqz p4, :cond_0

    .line 118
    iget-object v0, v1, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/index/SegmentCoreReaders;->openDocStores(Lorg/apache/lucene/index/SegmentInfo;)V

    .line 120
    :cond_0
    invoke-direct {v1}, Lorg/apache/lucene/index/SegmentReader;->loadDeletedDocs()V

    .line 121
    iget-object v0, v1, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentCoreReaders;->cfsDir:Lorg/apache/lucene/store/Directory;

    invoke-direct {v1, v0, p3}, Lorg/apache/lucene/index/SegmentReader;->openNorms(Lorg/apache/lucene/store/Directory;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    const/4 v6, 0x1

    .line 130
    if-nez v6, :cond_1

    .line 131
    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentReader;->doClose()V

    .line 134
    :cond_1
    return-object v1

    .line 108
    .end local v1    # "instance":Lorg/apache/lucene/index/SegmentReader;
    .end local v6    # "success":Z
    :cond_2
    new-instance v1, Lorg/apache/lucene/index/SegmentReader;

    invoke-direct {v1}, Lorg/apache/lucene/index/SegmentReader;-><init>()V

    goto :goto_0

    .line 130
    .restart local v1    # "instance":Lorg/apache/lucene/index/SegmentReader;
    .restart local v6    # "success":Z
    :catchall_0
    move-exception v0

    if-nez v6, :cond_3

    .line 131
    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentReader;->doClose()V

    :cond_3
    throw v0
.end method

.method static getOnlySegmentReader(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/SegmentReader;
    .locals 4
    .param p0, "reader"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 888
    instance-of v1, p0, Lorg/apache/lucene/index/SegmentReader;

    if-eqz v1, :cond_0

    .line 889
    check-cast p0, Lorg/apache/lucene/index/SegmentReader;

    .line 896
    .end local p0    # "reader":Lorg/apache/lucene/index/IndexReader;
    .local v0, "subReaders":[Lorg/apache/lucene/index/IndexReader;
    :goto_0
    return-object p0

    .line 891
    .end local v0    # "subReaders":[Lorg/apache/lucene/index/IndexReader;
    .restart local p0    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    instance-of v1, p0, Lorg/apache/lucene/index/DirectoryReader;

    if-eqz v1, :cond_2

    .line 892
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->getSequentialSubReaders()[Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 893
    .restart local v0    # "subReaders":[Lorg/apache/lucene/index/IndexReader;
    array-length v1, v0

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 894
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " has "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " segments instead of exactly one"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 896
    :cond_1
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Lorg/apache/lucene/index/SegmentReader;

    move-object p0, v1

    goto :goto_0

    .line 899
    .end local v0    # "subReaders":[Lorg/apache/lucene/index/IndexReader;
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " is not a SegmentReader or a single-segment DirectoryReader"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static getOnlySegmentReader(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/SegmentReader;
    .locals 1
    .param p0, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 884
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/lucene/index/IndexReader;->open(Lorg/apache/lucene/store/Directory;Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/index/SegmentReader;->getOnlySegmentReader(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/SegmentReader;

    move-result-object v0

    return-object v0
.end method

.method static hasDeletions(Lorg/apache/lucene/index/SegmentInfo;)Z
    .locals 1
    .param p0, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 403
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfo;->hasDeletions()Z

    move-result v0

    return v0
.end method

.method static hasSeparateNorms(Lorg/apache/lucene/index/SegmentInfo;)Z
    .locals 1
    .param p0, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 417
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfo;->hasSeparateNorms()Z

    move-result v0

    return v0
.end method

.method private loadDeletedDocs()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-static {v0}, Lorg/apache/lucene/index/SegmentReader;->hasDeletions(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    new-instance v0, Lorg/apache/lucene/util/BitVector;

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->directory()Lorg/apache/lucene/store/Directory;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->getDelFileName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/BitVector;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    .line 161
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 162
    sget-boolean v0, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/index/SegmentReader;->checkDeletedCounts()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 163
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v0}, Lorg/apache/lucene/util/BitVector;->size()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    iget v1, v1, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    if-eq v0, v1, :cond_2

    .line 164
    new-instance v0, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "document count mismatch: deleted docs count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v2}, Lorg/apache/lucene/util/BitVector;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " vs segment doc count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    iget v2, v2, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " segment="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfo;->getDelCount()I

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 168
    :cond_2
    return-void
.end method

.method private openNorms(Lorg/apache/lucene/store/Directory;I)V
    .locals 24
    .param p1, "cfsDir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "readBufferSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 614
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v16

    .line 615
    .local v16, "normsInitiallyEmpty":Z
    sget-object v3, Lorg/apache/lucene/index/SegmentNorms;->NORMS_HEADER:[B

    array-length v3, v3

    int-to-long v14, v3

    .line 616
    .local v14, "nextNormSeek":J
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v13

    .line 617
    .local v13, "maxDoc":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v3

    if-ge v11, v3, :cond_8

    .line 618
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v3, v11}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v9

    .line 619
    .local v9, "fi":Lorg/apache/lucene/index/FieldInfo;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    iget-object v5, v9, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 617
    :cond_0
    :goto_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 624
    :cond_1
    iget-boolean v3, v9, Lorg/apache/lucene/index/FieldInfo;->isIndexed:Z

    if-eqz v3, :cond_0

    iget-boolean v3, v9, Lorg/apache/lucene/index/FieldInfo;->omitNorms:Z

    if-nez v3, :cond_0

    .line 625
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentReader;->directory()Lorg/apache/lucene/store/Directory;

    move-result-object v2

    .line 626
    .local v2, "d":Lorg/apache/lucene/store/Directory;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    iget v5, v9, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v3, v5}, Lorg/apache/lucene/index/SegmentInfo;->getNormFileName(I)Ljava/lang/String;

    move-result-object v10

    .line 627
    .local v10, "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    iget v5, v9, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v3, v5}, Lorg/apache/lucene/index/SegmentInfo;->hasSeparateNorms(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 628
    move-object/from16 v2, p1

    .line 632
    :cond_2
    const-string/jumbo v3, "nrm"

    invoke-static {v10, v3}, Lorg/apache/lucene/index/IndexFileNames;->matchesExtension(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v17

    .line 633
    .local v17, "singleNormFile":Z
    const/4 v4, 0x0

    .line 636
    .local v4, "normInput":Lorg/apache/lucene/store/IndexInput;
    if-eqz v17, :cond_4

    .line 637
    move-wide v6, v14

    .line 638
    .local v6, "normSeek":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentReader;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    if-nez v3, :cond_3

    .line 639
    move/from16 v0, p2

    invoke-virtual {v2, v10, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;I)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/lucene/index/SegmentReader;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    .line 640
    new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v5, 0x1

    invoke-direct {v3, v5}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/lucene/index/SegmentReader;->singleNormRef:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 647
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/index/SegmentReader;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    .line 665
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    move-object/from16 v19, v0

    iget-object v0, v9, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    new-instance v3, Lorg/apache/lucene/index/SegmentNorms;

    iget v5, v9, Lorg/apache/lucene/index/FieldInfo;->number:I

    move-object/from16 v8, p0

    invoke-direct/range {v3 .. v8}, Lorg/apache/lucene/index/SegmentNorms;-><init>(Lorg/apache/lucene/store/IndexInput;IJLorg/apache/lucene/index/SegmentReader;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 666
    int-to-long v0, v13

    move-wide/from16 v20, v0

    add-long v14, v14, v20

    goto :goto_1

    .line 642
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentReader;->singleNormRef:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    goto :goto_2

    .line 649
    .end local v6    # "normSeek":J
    :cond_4
    invoke-virtual {v2, v10}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    .line 654
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v18

    .line 655
    .local v18, "version":Ljava/lang/String;
    if-eqz v18, :cond_5

    invoke-static {}, Lorg/apache/lucene/util/StringHelper;->getVersionComparator()Ljava/util/Comparator;

    move-result-object v3

    const-string/jumbo v5, "3.2"

    move-object/from16 v0, v18

    invoke-interface {v3, v0, v5}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-gez v3, :cond_6

    :cond_5
    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v20

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v3

    int-to-long v0, v3

    move-wide/from16 v22, v0

    cmp-long v3, v20, v22

    if-nez v3, :cond_6

    const/4 v12, 0x1

    .line 658
    .local v12, "isUnversioned":Z
    :goto_4
    if-eqz v12, :cond_7

    .line 659
    const-wide/16 v6, 0x0

    .restart local v6    # "normSeek":J
    goto :goto_3

    .line 655
    .end local v6    # "normSeek":J
    .end local v12    # "isUnversioned":Z
    :cond_6
    const/4 v12, 0x0

    goto :goto_4

    .line 661
    .restart local v12    # "isUnversioned":Z
    :cond_7
    sget-object v3, Lorg/apache/lucene/index/SegmentNorms;->NORMS_HEADER:[B

    array-length v3, v3

    int-to-long v6, v3

    .restart local v6    # "normSeek":J
    goto :goto_3

    .line 669
    .end local v2    # "d":Lorg/apache/lucene/store/Directory;
    .end local v4    # "normInput":Lorg/apache/lucene/store/IndexInput;
    .end local v6    # "normSeek":J
    .end local v9    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .end local v10    # "fileName":Ljava/lang/String;
    .end local v12    # "isUnversioned":Z
    .end local v17    # "singleNormFile":Z
    .end local v18    # "version":Ljava/lang/String;
    :cond_8
    sget-boolean v3, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentReader;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    if-eqz v3, :cond_9

    if-eqz v16, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentReader;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v20

    cmp-long v3, v14, v20

    if-eqz v3, :cond_9

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 670
    :cond_9
    return-void
.end method

.method static usesCompoundFile(Lorg/apache/lucene/index/SegmentInfo;)Z
    .locals 1
    .param p0, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 413
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public addCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    .prologue
    .line 926
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 927
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SegmentCoreReaders;->addCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V

    .line 928
    return-void
.end method

.method public final declared-synchronized clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 198
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentReader;->readOnly:Z

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/SegmentReader;->clone(Z)Lorg/apache/lucene/index/IndexReader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 198
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized clone(Z)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p1, "openReadOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 207
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, p1}, Lorg/apache/lucene/index/SegmentReader;->reopenSegment(Lorg/apache/lucene/index/SegmentInfo;ZZ)Lorg/apache/lucene/index/SegmentReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected cloneDeletedDocs(Lorg/apache/lucene/util/BitVector;)Lorg/apache/lucene/util/BitVector;
    .locals 1
    .param p1, "bv"    # Lorg/apache/lucene/util/BitVector;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 191
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 192
    invoke-virtual {p1}, Lorg/apache/lucene/util/BitVector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/BitVector;

    return-object v0
.end method

.method protected cloneNormBytes([B)[B
    .locals 3
    .param p1, "bytes"    # [B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 178
    array-length v1, p1

    new-array v0, v1, [B

    .line 179
    .local v0, "cloneBytes":[B
    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 180
    return-object v0
.end method

.method public directory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 854
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentCoreReaders;->dir:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method protected doClose()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 384
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->termVectorsLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v2}, Lorg/apache/lucene/util/CloseableThreadLocal;->close()V

    .line 385
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->fieldsReaderLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v2}, Lorg/apache/lucene/util/CloseableThreadLocal;->close()V

    .line 387
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v2, :cond_0

    .line 388
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 390
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    .line 393
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentNorms;

    .line 394
    .local v1, "norm":Lorg/apache/lucene/index/SegmentNorms;
    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentNorms;->decRef()V

    goto :goto_0

    .line 396
    .end local v1    # "norm":Lorg/apache/lucene/index/SegmentNorms;
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    if-eqz v2, :cond_2

    .line 397
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentCoreReaders;->decRef()V

    .line 399
    :cond_2
    return-void
.end method

.method protected doCommit(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 319
    .local p1, "commitUserData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    if-eqz v1, :cond_0

    .line 320
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->startCommit()V

    .line 321
    const/4 v0, 0x0

    .line 323
    .local v0, "success":Z
    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/SegmentReader;->commitChanges(Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 324
    const/4 v0, 0x1

    .line 326
    if-nez v0, :cond_0

    .line 327
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->rollbackCommit()V

    .line 331
    .end local v0    # "success":Z
    :cond_0
    return-void

    .line 326
    .restart local v0    # "success":Z
    :catchall_0
    move-exception v1

    if-nez v0, :cond_1

    .line 327
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->rollbackCommit()V

    :cond_1
    throw v1
.end method

.method protected doDelete(I)V
    .locals 4
    .param p1, "docNum"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 423
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-nez v1, :cond_0

    .line 424
    new-instance v1, Lorg/apache/lucene/util/BitVector;

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/BitVector;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    .line 425
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 430
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-le v1, v3, :cond_1

    .line 431
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 432
    .local v0, "oldRef":Ljava/util/concurrent/atomic/AtomicInteger;
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/SegmentReader;->cloneDeletedDocs(Lorg/apache/lucene/util/BitVector;)Lorg/apache/lucene/util/BitVector;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    .line 433
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 434
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 436
    .end local v0    # "oldRef":Ljava/util/concurrent/atomic/AtomicInteger;
    :cond_1
    iput-boolean v3, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsDirty:Z

    .line 437
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/BitVector;->getAndSet(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 438
    iget v1, p0, Lorg/apache/lucene/index/SegmentReader;->pendingDeleteCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/SegmentReader;->pendingDeleteCount:I

    .line 440
    :cond_2
    return-void
.end method

.method protected declared-synchronized doOpenIfChanged()Lorg/apache/lucene/index/IndexReader;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 213
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lorg/apache/lucene/index/SegmentReader;->readOnly:Z

    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/lucene/index/SegmentReader;->reopenSegment(Lorg/apache/lucene/index/SegmentInfo;ZZ)Lorg/apache/lucene/index/SegmentReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized doOpenIfChanged(Z)Lorg/apache/lucene/index/IndexReader;
    .locals 2
    .param p1, "openReadOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 220
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lorg/apache/lucene/index/SegmentReader;->reopenSegment(Lorg/apache/lucene/index/SegmentInfo;ZZ)Lorg/apache/lucene/index/SegmentReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected doSetNorm(ILjava/lang/String;B)V
    .locals 4
    .param p1, "doc"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "value"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 582
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentNorms;

    .line 583
    .local v0, "norm":Lorg/apache/lucene/index/SegmentNorms;
    if-nez v0, :cond_0

    .line 585
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Cannot setNorm for field "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": norms were omitted"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 588
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/index/SegmentReader;->normsDirty:Z

    .line 589
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentNorms;->copyOnWrite()[B

    move-result-object v1

    aput-byte p3, v1, p1

    .line 590
    return-void
.end method

.method protected doUndeleteAll()V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 445
    iput-boolean v1, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsDirty:Z

    .line 446
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v0, :cond_2

    .line 447
    sget-boolean v0, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 448
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 449
    iput-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    .line 450
    iput-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 451
    iput v1, p0, Lorg/apache/lucene/index/SegmentReader;->pendingDeleteCount:I

    .line 452
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfo;->clearDelGen()V

    .line 453
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfo;->setDelCount(I)V

    .line 458
    :cond_1
    return-void

    .line 455
    :cond_2
    sget-boolean v0, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 456
    :cond_3
    sget-boolean v0, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/index/SegmentReader;->pendingDeleteCount:I

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public docFreq(Lorg/apache/lucene/index/Term;)I
    .locals 2
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 538
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 539
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentCoreReaders;->getTermsReader()Lorg/apache/lucene/index/TermInfosReader;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/TermInfosReader;->get(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermInfo;

    move-result-object v0

    .line 540
    .local v0, "ti":Lorg/apache/lucene/index/TermInfo;
    if-eqz v0, :cond_0

    .line 541
    iget v1, v0, Lorg/apache/lucene/index/TermInfo;->docFreq:I

    .line 543
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public document(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;
    .locals 3
    .param p1, "n"    # I
    .param p2, "fieldSelector"    # Lorg/apache/lucene/document/FieldSelector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 488
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 489
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 490
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "docID must be >= 0 and < maxDoc="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " (got docID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 492
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->getFieldsReader()Lorg/apache/lucene/index/FieldsReader;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/FieldsReader;->doc(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;

    move-result-object v0

    return-object v0
.end method

.method fieldInfos()Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    return-object v0
.end method

.method files()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 461
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final getCoreCacheKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 862
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentCoreReaders;->freqStream:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method public getDeletesCacheKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 867
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    return-object v0
.end method

.method public getFieldInfos()Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    return-object v0
.end method

.method getFieldsReader()Lorg/apache/lucene/index/FieldsReader;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->fieldsReaderLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v0}, Lorg/apache/lucene/util/CloseableThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldsReader;

    return-object v0
.end method

.method getPostingsSkipInterval()I
    .locals 1

    .prologue
    .line 610
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentCoreReaders;->getTermsReader()Lorg/apache/lucene/index/TermInfosReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermInfosReader;->getSkipInterval()I

    move-result v0

    return v0
.end method

.method getSegmentInfo()Lorg/apache/lucene/index/SegmentInfo;
    .locals 1

    .prologue
    .line 819
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    return-object v0
.end method

.method public getSegmentName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 812
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentCoreReaders;->segment:Ljava/lang/String;

    return-object v0
.end method

.method public getTermFreqVector(ILjava/lang/String;)Lorg/apache/lucene/index/TermFreqVector;
    .locals 4
    .param p1, "docNumber"    # I
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 738
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 739
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v3, p2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 740
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-eqz v0, :cond_0

    iget-boolean v3, v0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    if-nez v3, :cond_1

    .line 747
    :cond_0
    :goto_0
    return-object v2

    .line 743
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->getTermVectorsReader()Lorg/apache/lucene/index/TermVectorsReader;

    move-result-object v1

    .line 744
    .local v1, "termVectorsReader":Lorg/apache/lucene/index/TermVectorsReader;
    if-eqz v1, :cond_0

    .line 747
    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/index/TermVectorsReader;->get(ILjava/lang/String;)Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v2

    goto :goto_0
.end method

.method public getTermFreqVector(ILjava/lang/String;Lorg/apache/lucene/index/TermVectorMapper;)V
    .locals 3
    .param p1, "docNumber"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "mapper"    # Lorg/apache/lucene/index/TermVectorMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 753
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 754
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v2, p2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 755
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-eqz v0, :cond_0

    iget-boolean v2, v0, Lorg/apache/lucene/index/FieldInfo;->storeTermVector:Z

    if-nez v2, :cond_1

    .line 765
    :cond_0
    :goto_0
    return-void

    .line 758
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->getTermVectorsReader()Lorg/apache/lucene/index/TermVectorsReader;

    move-result-object v1

    .line 759
    .local v1, "termVectorsReader":Lorg/apache/lucene/index/TermVectorsReader;
    if-eqz v1, :cond_0

    .line 764
    invoke-virtual {v1, p1, p2, p3}, Lorg/apache/lucene/index/TermVectorsReader;->get(ILjava/lang/String;Lorg/apache/lucene/index/TermVectorMapper;)V

    goto :goto_0
.end method

.method public getTermFreqVector(ILorg/apache/lucene/index/TermVectorMapper;)V
    .locals 1
    .param p1, "docNumber"    # I
    .param p2, "mapper"    # Lorg/apache/lucene/index/TermVectorMapper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 770
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 772
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->getTermVectorsReader()Lorg/apache/lucene/index/TermVectorsReader;

    move-result-object v0

    .line 773
    .local v0, "termVectorsReader":Lorg/apache/lucene/index/TermVectorsReader;
    if-nez v0, :cond_0

    .line 777
    :goto_0
    return-void

    .line 776
    :cond_0
    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/TermVectorsReader;->get(ILorg/apache/lucene/index/TermVectorMapper;)V

    goto :goto_0
.end method

.method public getTermFreqVectors(I)[Lorg/apache/lucene/index/TermFreqVector;
    .locals 2
    .param p1, "docNumber"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 788
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 790
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->getTermVectorsReader()Lorg/apache/lucene/index/TermVectorsReader;

    move-result-object v0

    .line 791
    .local v0, "termVectorsReader":Lorg/apache/lucene/index/TermVectorsReader;
    if-nez v0, :cond_0

    .line 792
    const/4 v1, 0x0

    .line 794
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/TermVectorsReader;->get(I)[Lorg/apache/lucene/index/TermFreqVector;

    move-result-object v1

    goto :goto_0
.end method

.method public getTermInfosIndexDivisor()I
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget v0, v0, Lorg/apache/lucene/index/SegmentCoreReaders;->termsIndexDivisor:I

    return v0
.end method

.method getTermVectorsReader()Lorg/apache/lucene/index/TermVectorsReader;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 708
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentReader;->termVectorsLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v4}, Lorg/apache/lucene/util/CloseableThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/TermVectorsReader;

    .line 709
    .local v3, "tvReader":Lorg/apache/lucene/index/TermVectorsReader;
    if-nez v3, :cond_1

    .line 710
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentCoreReaders;->getTermVectorsReaderOrig()Lorg/apache/lucene/index/TermVectorsReader;

    move-result-object v2

    .line 711
    .local v2, "orig":Lorg/apache/lucene/index/TermVectorsReader;
    if-nez v2, :cond_0

    move-object v4, v5

    .line 722
    .end local v2    # "orig":Lorg/apache/lucene/index/TermVectorsReader;
    :goto_0
    return-object v4

    .line 715
    .restart local v2    # "orig":Lorg/apache/lucene/index/TermVectorsReader;
    :cond_0
    :try_start_0
    invoke-virtual {v2}, Lorg/apache/lucene/index/TermVectorsReader;->clone()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lorg/apache/lucene/index/TermVectorsReader;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 720
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentReader;->termVectorsLocal:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v4, v3}, Lorg/apache/lucene/util/CloseableThreadLocal;->set(Ljava/lang/Object;)V

    .end local v2    # "orig":Lorg/apache/lucene/index/TermVectorsReader;
    :cond_1
    move-object v4, v3

    .line 722
    goto :goto_0

    .line 716
    .restart local v2    # "orig":Lorg/apache/lucene/index/TermVectorsReader;
    :catch_0
    move-exception v1

    .local v1, "cnse":Ljava/lang/CloneNotSupportedException;
    move-object v4, v5

    .line 717
    goto :goto_0
.end method

.method getTermVectorsReaderOrig()Lorg/apache/lucene/index/TermVectorsReader;
    .locals 1

    .prologue
    .line 726
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentCoreReaders;->getTermVectorsReaderOrig()Lorg/apache/lucene/index/TermVectorsReader;

    move-result-object v0

    return-object v0
.end method

.method public getUniqueTermCount()J
    .locals 2

    .prologue
    .line 872
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentCoreReaders;->getTermsReader()Lorg/apache/lucene/index/TermInfosReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermInfosReader;->size()J

    move-result-wide v0

    return-wide v0
.end method

.method public hasDeletions()Z
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNorms(Ljava/lang/String;)Z
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 563
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 564
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized isDeleted(I)Z
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 497
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/BitVector;->get(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method loadTermsIndex(I)V
    .locals 2
    .param p1, "termsIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 682
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/index/SegmentCoreReaders;->loadTermsIndex(Lorg/apache/lucene/index/SegmentInfo;I)V

    .line 683
    return-void
.end method

.method public maxDoc()I
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    iget v0, v0, Lorg/apache/lucene/index/SegmentInfo;->docCount:I

    return v0
.end method

.method public declared-synchronized norms(Ljava/lang/String;[BI)V
    .locals 4
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "bytes"    # [B
    .param p3, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 597
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 598
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentNorms;

    .line 599
    .local v0, "norm":Lorg/apache/lucene/index/SegmentNorms;
    if-nez v0, :cond_0

    .line 600
    array-length v1, p2

    invoke-static {}, Lorg/apache/lucene/search/Similarity;->getDefault()Lorg/apache/lucene/search/Similarity;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Similarity;->encodeNormValue(F)B

    move-result v2

    invoke-static {p2, p3, v1, v2}, Ljava/util/Arrays;->fill([BIIB)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 605
    :goto_0
    monitor-exit p0

    return-void

    .line 604
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v1

    invoke-virtual {v0, p2, p3, v1}, Lorg/apache/lucene/index/SegmentNorms;->bytes([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 597
    .end local v0    # "norm":Lorg/apache/lucene/index/SegmentNorms;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public norms(Ljava/lang/String;)[B
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 569
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 570
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentNorms;

    .line 571
    .local v0, "norm":Lorg/apache/lucene/index/SegmentNorms;
    if-nez v0, :cond_0

    .line 573
    const/4 v1, 0x0

    .line 575
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentNorms;->bytes()[B

    move-result-object v1

    goto :goto_0
.end method

.method normsClosed()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 687
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentReader;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    if-eqz v3, :cond_0

    .line 695
    :goto_0
    return v2

    .line 690
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentNorms;

    .line 691
    .local v1, "norm":Lorg/apache/lucene/index/SegmentNorms;
    iget v3, v1, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-lez v3, :cond_1

    goto :goto_0

    .line 695
    .end local v1    # "norm":Lorg/apache/lucene/index/SegmentNorms;
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method normsClosed(Ljava/lang/String;)Z
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 700
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentNorms;

    iget v0, v0, Lorg/apache/lucene/index/SegmentNorms;->refCount:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public numDocs()I
    .locals 2

    .prologue
    .line 549
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v0

    .line 550
    .local v0, "n":I
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v1, :cond_0

    .line 551
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BitVector;->count()I

    move-result v1

    sub-int/2addr v0, v1

    .line 552
    :cond_0
    return v0
.end method

.method openDocStores()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentCoreReaders;->openDocStores(Lorg/apache/lucene/index/SegmentInfo;)V

    .line 139
    return-void
.end method

.method public rawTermDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 516
    if-nez p1, :cond_0

    .line 517
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "term must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 519
    :cond_0
    new-instance v0, Lorg/apache/lucene/index/SegmentTermDocs;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/index/SegmentTermDocs;-><init>(Lorg/apache/lucene/index/SegmentReader;Z)V

    .line 520
    .local v0, "td":Lorg/apache/lucene/index/TermDocs;
    invoke-interface {v0, p1}, Lorg/apache/lucene/index/TermDocs;->seek(Lorg/apache/lucene/index/Term;)V

    .line 521
    return-object v0
.end method

.method public removeCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    .prologue
    .line 932
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 933
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SegmentCoreReaders;->removeCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V

    .line 934
    return-void
.end method

.method declared-synchronized reopenSegment(Lorg/apache/lucene/index/SegmentInfo;ZZ)Lorg/apache/lucene/index/SegmentReader;
    .locals 12
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "doClone"    # Z
    .param p3, "openReadOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v9, 0x0

    .line 224
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 225
    iget-object v10, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v10}, Lorg/apache/lucene/index/SegmentInfo;->hasDeletions()Z

    move-result v10

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->hasDeletions()Z

    move-result v11

    if-ne v10, v11, :cond_2

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->hasDeletions()Z

    move-result v10

    if-eqz v10, :cond_0

    iget-object v10, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v10}, Lorg/apache/lucene/index/SegmentInfo;->getDelFileName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDelFileName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 227
    .local v2, "deletionsUpToDate":Z
    :cond_0
    :goto_0
    const/4 v7, 0x1

    .line 229
    .local v7, "normsUpToDate":Z
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v9, v9, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v9}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v9

    new-array v4, v9, [Z

    .line 230
    .local v4, "fieldNormsChanged":[Z
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v9, v9, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v9}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v3

    .line 231
    .local v3, "fieldCount":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v3, :cond_3

    .line 232
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v9, v5}, Lorg/apache/lucene/index/SegmentInfo;->getNormFileName(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v5}, Lorg/apache/lucene/index/SegmentInfo;->getNormFileName(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 233
    const/4 v7, 0x0

    .line 234
    const/4 v9, 0x1

    aput-boolean v9, v4, v5

    .line 231
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .end local v2    # "deletionsUpToDate":Z
    .end local v3    # "fieldCount":I
    .end local v4    # "fieldNormsChanged":[Z
    .end local v5    # "i":I
    .end local v7    # "normsUpToDate":Z
    :cond_2
    move v2, v9

    .line 225
    goto :goto_0

    .line 240
    .restart local v2    # "deletionsUpToDate":Z
    .restart local v3    # "fieldCount":I
    .restart local v4    # "fieldNormsChanged":[Z
    .restart local v5    # "i":I
    .restart local v7    # "normsUpToDate":Z
    :cond_3
    if-eqz v7, :cond_5

    if-eqz v2, :cond_5

    if-nez p2, :cond_5

    if-eqz p3, :cond_5

    iget-boolean v9, p0, Lorg/apache/lucene/index/SegmentReader;->readOnly:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v9, :cond_5

    .line 241
    const/4 v0, 0x0

    .line 313
    :cond_4
    :goto_2
    monitor-exit p0

    return-object v0

    .line 246
    :cond_5
    :try_start_1
    sget-boolean v9, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v9, :cond_7

    if-eqz p2, :cond_7

    if-eqz v7, :cond_6

    if-nez v2, :cond_7

    :cond_6
    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 224
    .end local v2    # "deletionsUpToDate":Z
    .end local v3    # "fieldCount":I
    .end local v4    # "fieldNormsChanged":[Z
    .end local v5    # "i":I
    .end local v7    # "normsUpToDate":Z
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 249
    .restart local v2    # "deletionsUpToDate":Z
    .restart local v3    # "fieldCount":I
    .restart local v4    # "fieldNormsChanged":[Z
    .restart local v5    # "i":I
    .restart local v7    # "normsUpToDate":Z
    :cond_7
    if-eqz p3, :cond_c

    :try_start_2
    new-instance v0, Lorg/apache/lucene/index/ReadOnlySegmentReader;

    invoke-direct {v0}, Lorg/apache/lucene/index/ReadOnlySegmentReader;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 251
    .local v0, "clone":Lorg/apache/lucene/index/SegmentReader;
    :goto_3
    const/4 v8, 0x0

    .line 253
    .local v8, "success":Z
    :try_start_3
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v9}, Lorg/apache/lucene/index/SegmentCoreReaders;->incRef()V

    .line 254
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iput-object v9, v0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    .line 255
    iput-boolean p3, v0, Lorg/apache/lucene/index/SegmentReader;->readOnly:Z

    .line 256
    iput-object p1, v0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    .line 257
    iget v9, p0, Lorg/apache/lucene/index/SegmentReader;->readBufferSize:I

    iput v9, v0, Lorg/apache/lucene/index/SegmentReader;->readBufferSize:I

    .line 258
    iget v9, p0, Lorg/apache/lucene/index/SegmentReader;->pendingDeleteCount:I

    iput v9, v0, Lorg/apache/lucene/index/SegmentReader;->pendingDeleteCount:I

    .line 260
    if-nez p3, :cond_8

    iget-boolean v9, p0, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    if-eqz v9, :cond_8

    .line 262
    iget-boolean v9, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsDirty:Z

    iput-boolean v9, v0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsDirty:Z

    .line 263
    iget-boolean v9, p0, Lorg/apache/lucene/index/SegmentReader;->normsDirty:Z

    iput-boolean v9, v0, Lorg/apache/lucene/index/SegmentReader;->normsDirty:Z

    .line 264
    iget-boolean v9, p0, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    iput-boolean v9, v0, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    .line 265
    const/4 v9, 0x0

    iput-boolean v9, p0, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    .line 268
    :cond_8
    if-eqz p2, :cond_d

    .line 269
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v9, :cond_9

    .line 270
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 271
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    iput-object v9, v0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    .line 272
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object v9, v0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 286
    :cond_9
    :goto_4
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    iput-object v9, v0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    .line 289
    const/4 v5, 0x0

    :goto_5
    array-length v9, v4

    if-ge v5, v9, :cond_11

    .line 292
    if-nez p2, :cond_a

    aget-boolean v9, v4, v5

    if-nez v9, :cond_b

    .line 293
    :cond_a
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v9, v9, Lorg/apache/lucene/index/SegmentCoreReaders;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v9, v5}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v9

    iget-object v1, v9, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    .line 294
    .local v1, "curField":Ljava/lang/String;
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/SegmentNorms;

    .line 295
    .local v6, "norm":Lorg/apache/lucene/index/SegmentNorms;
    if-eqz v6, :cond_b

    .line 296
    iget-object v10, v0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    invoke-virtual {v6}, Lorg/apache/lucene/index/SegmentNorms;->clone()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/index/SegmentNorms;

    invoke-interface {v10, v1, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 289
    .end local v1    # "curField":Ljava/lang/String;
    .end local v6    # "norm":Lorg/apache/lucene/index/SegmentNorms;
    :cond_b
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 249
    .end local v0    # "clone":Lorg/apache/lucene/index/SegmentReader;
    .end local v8    # "success":Z
    :cond_c
    :try_start_4
    new-instance v0, Lorg/apache/lucene/index/SegmentReader;

    invoke-direct {v0}, Lorg/apache/lucene/index/SegmentReader;-><init>()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 275
    .restart local v0    # "clone":Lorg/apache/lucene/index/SegmentReader;
    .restart local v8    # "success":Z
    :cond_d
    if-nez v2, :cond_10

    .line 277
    :try_start_5
    sget-boolean v9, Lorg/apache/lucene/index/SegmentReader;->$assertionsDisabled:Z

    if-nez v9, :cond_f

    iget-object v9, v0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v9, :cond_f

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 306
    :catchall_1
    move-exception v9

    if-nez v8, :cond_e

    .line 309
    :try_start_6
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->decRef()V

    :cond_e
    throw v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 278
    :cond_f
    :try_start_7
    invoke-direct {v0}, Lorg/apache/lucene/index/SegmentReader;->loadDeletedDocs()V

    goto :goto_4

    .line 279
    :cond_10
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    if-eqz v9, :cond_9

    .line 280
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 281
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    iput-object v9, v0, Lorg/apache/lucene/index/SegmentReader;->deletedDocs:Lorg/apache/lucene/util/BitVector;

    .line 282
    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object v9, v0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsRef:Ljava/util/concurrent/atomic/AtomicInteger;

    goto :goto_4

    .line 302
    :cond_11
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v9

    if-eqz v9, :cond_12

    iget-object v9, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v9}, Lorg/apache/lucene/index/SegmentCoreReaders;->getCFSReader()Lorg/apache/lucene/store/Directory;

    move-result-object v9

    :goto_6
    iget v10, p0, Lorg/apache/lucene/index/SegmentReader;->readBufferSize:I

    invoke-direct {v0, v9, v10}, Lorg/apache/lucene/index/SegmentReader;->openNorms(Lorg/apache/lucene/store/Directory;I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 304
    const/4 v8, 0x1

    .line 306
    if-nez v8, :cond_4

    .line 309
    :try_start_8
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->decRef()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_2

    .line 302
    :cond_12
    :try_start_9
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->directory()Lorg/apache/lucene/store/Directory;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v9

    goto :goto_6
.end method

.method rollbackCommit()V
    .locals 4

    .prologue
    .line 838
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v3, p0, Lorg/apache/lucene/index/SegmentReader;->rollbackSegmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/SegmentInfo;->reset(Lorg/apache/lucene/index/SegmentInfo;)V

    .line 839
    iget-boolean v2, p0, Lorg/apache/lucene/index/SegmentReader;->rollbackHasChanges:Z

    iput-boolean v2, p0, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    .line 840
    iget-boolean v2, p0, Lorg/apache/lucene/index/SegmentReader;->rollbackDeletedDocsDirty:Z

    iput-boolean v2, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsDirty:Z

    .line 841
    iget-boolean v2, p0, Lorg/apache/lucene/index/SegmentReader;->rollbackNormsDirty:Z

    iput-boolean v2, p0, Lorg/apache/lucene/index/SegmentReader;->normsDirty:Z

    .line 842
    iget v2, p0, Lorg/apache/lucene/index/SegmentReader;->rollbackPendingDeleteCount:I

    iput v2, p0, Lorg/apache/lucene/index/SegmentReader;->pendingDeleteCount:I

    .line 843
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentNorms;

    .line 844
    .local v1, "norm":Lorg/apache/lucene/index/SegmentNorms;
    iget-boolean v2, v1, Lorg/apache/lucene/index/SegmentNorms;->rollbackDirty:Z

    iput-boolean v2, v1, Lorg/apache/lucene/index/SegmentNorms;->dirty:Z

    goto :goto_0

    .line 846
    .end local v1    # "norm":Lorg/apache/lucene/index/SegmentNorms;
    :cond_0
    return-void
.end method

.method setSegmentInfo(Lorg/apache/lucene/index/SegmentInfo;)V
    .locals 0
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    .line 823
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    .line 824
    return-void
.end method

.method startCommit()V
    .locals 3

    .prologue
    .line 827
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentInfo;

    iput-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->rollbackSegmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    .line 828
    iget-boolean v2, p0, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    iput-boolean v2, p0, Lorg/apache/lucene/index/SegmentReader;->rollbackHasChanges:Z

    .line 829
    iget-boolean v2, p0, Lorg/apache/lucene/index/SegmentReader;->deletedDocsDirty:Z

    iput-boolean v2, p0, Lorg/apache/lucene/index/SegmentReader;->rollbackDeletedDocsDirty:Z

    .line 830
    iget-boolean v2, p0, Lorg/apache/lucene/index/SegmentReader;->normsDirty:Z

    iput-boolean v2, p0, Lorg/apache/lucene/index/SegmentReader;->rollbackNormsDirty:Z

    .line 831
    iget v2, p0, Lorg/apache/lucene/index/SegmentReader;->pendingDeleteCount:I

    iput v2, p0, Lorg/apache/lucene/index/SegmentReader;->rollbackPendingDeleteCount:I

    .line 832
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->norms:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentNorms;

    .line 833
    .local v1, "norm":Lorg/apache/lucene/index/SegmentNorms;
    iget-boolean v2, v1, Lorg/apache/lucene/index/SegmentNorms;->dirty:Z

    iput-boolean v2, v1, Lorg/apache/lucene/index/SegmentNorms;->rollbackDirty:Z

    goto :goto_0

    .line 835
    .end local v1    # "norm":Lorg/apache/lucene/index/SegmentNorms;
    :cond_0
    return-void
.end method

.method public termDocs()Lorg/apache/lucene/index/TermDocs;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 526
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 527
    new-instance v0, Lorg/apache/lucene/index/SegmentTermDocs;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/SegmentTermDocs;-><init>(Lorg/apache/lucene/index/SegmentReader;)V

    return-object v0
.end method

.method public termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 502
    if-nez p1, :cond_0

    .line 503
    new-instance v0, Lorg/apache/lucene/index/AllTermDocs;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/AllTermDocs;-><init>(Lorg/apache/lucene/index/SegmentReader;)V

    .line 505
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/lucene/index/IndexReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    goto :goto_0
.end method

.method public termPositions()Lorg/apache/lucene/index/TermPositions;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 532
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 533
    new-instance v0, Lorg/apache/lucene/index/SegmentTermPositions;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/SegmentTermPositions;-><init>(Lorg/apache/lucene/index/SegmentReader;)V

    return-object v0
.end method

.method public terms()Lorg/apache/lucene/index/TermEnum;
    .locals 1

    .prologue
    .line 466
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 467
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentCoreReaders;->getTermsReader()Lorg/apache/lucene/index/TermInfosReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermInfosReader;->terms()Lorg/apache/lucene/index/SegmentTermEnum;

    move-result-object v0

    return-object v0
.end method

.method public terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;
    .locals 1
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 472
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentReader;->ensureOpen()V

    .line 473
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentCoreReaders;->getTermsReader()Lorg/apache/lucene/index/TermInfosReader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/TermInfosReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/SegmentTermEnum;

    move-result-object v0

    return-object v0
.end method

.method termsIndexLoaded()Z
    .locals 1

    .prologue
    .line 673
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentCoreReaders;->termsIndexIsLoaded()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 800
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 801
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-boolean v1, p0, Lorg/apache/lucene/index/SegmentReader;->hasChanges:Z

    if-eqz v1, :cond_0

    .line 802
    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 804
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentReader;->si:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentCoreReaders;->dir:Lorg/apache/lucene/store/Directory;

    iget v3, p0, Lorg/apache/lucene/index/SegmentReader;->pendingDeleteCount:I

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/index/SegmentInfo;->toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 805
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
