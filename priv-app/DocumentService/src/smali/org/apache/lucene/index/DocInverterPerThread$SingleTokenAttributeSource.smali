.class Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;
.super Lorg/apache/lucene/util/AttributeSource;
.source "DocInverterPerThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocInverterPerThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SingleTokenAttributeSource"
.end annotation


# instance fields
.field final offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field final termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeSource;-><init>()V

    .line 41
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 42
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/index/DocInverterPerThread$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/index/DocInverterPerThread$1;

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;-><init>()V

    return-void
.end method


# virtual methods
.method public reinit(Ljava/lang/String;II)V
    .locals 1
    .param p1, "stringValue"    # Ljava/lang/String;
    .param p2, "startOffset"    # I
    .param p3, "endOffset"    # I

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v0

    invoke-interface {v0, p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 47
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v0, p2, p3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 48
    return-void
.end method
