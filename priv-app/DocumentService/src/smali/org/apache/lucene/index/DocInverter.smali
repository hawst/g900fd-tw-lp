.class final Lorg/apache/lucene/index/DocInverter;
.super Lorg/apache/lucene/index/DocFieldConsumer;
.source "DocInverter.java"


# instance fields
.field final consumer:Lorg/apache/lucene/index/InvertedDocConsumer;

.field final endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/InvertedDocConsumer;Lorg/apache/lucene/index/InvertedDocEndConsumer;)V
    .locals 0
    .param p1, "consumer"    # Lorg/apache/lucene/index/InvertedDocConsumer;
    .param p2, "endConsumer"    # Lorg/apache/lucene/index/InvertedDocEndConsumer;

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/lucene/index/DocFieldConsumer;-><init>()V

    .line 38
    iput-object p1, p0, Lorg/apache/lucene/index/DocInverter;->consumer:Lorg/apache/lucene/index/InvertedDocConsumer;

    .line 39
    iput-object p2, p0, Lorg/apache/lucene/index/DocInverter;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;

    .line 40
    return-void
.end method


# virtual methods
.method abort()V
    .locals 2

    .prologue
    .line 77
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverter;->consumer:Lorg/apache/lucene/index/InvertedDocConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocConsumer;->abort()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverter;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocEndConsumer;->abort()V

    .line 81
    return-void

    .line 79
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/lucene/index/DocInverter;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;

    invoke-virtual {v1}, Lorg/apache/lucene/index/InvertedDocEndConsumer;->abort()V

    throw v0
.end method

.method public addThread(Lorg/apache/lucene/index/DocFieldProcessorPerThread;)Lorg/apache/lucene/index/DocFieldConsumerPerThread;
    .locals 1
    .param p1, "docFieldProcessorPerThread"    # Lorg/apache/lucene/index/DocFieldProcessorPerThread;

    .prologue
    .line 90
    new-instance v0, Lorg/apache/lucene/index/DocInverterPerThread;

    invoke-direct {v0, p1, p0}, Lorg/apache/lucene/index/DocInverterPerThread;-><init>(Lorg/apache/lucene/index/DocFieldProcessorPerThread;Lorg/apache/lucene/index/DocInverter;)V

    return-object v0
.end method

.method flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 11
    .param p2, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/DocFieldConsumerPerThread;",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/DocFieldConsumerPerField;",
            ">;>;",
            "Lorg/apache/lucene/index/SegmentWriteState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "threadsAndFields":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/DocFieldConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/DocFieldConsumerPerField;>;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 53
    .local v1, "childThreadsAndFields":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/InvertedDocConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;>;"
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 55
    .local v3, "endChildThreadsAndFields":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;>;>;"
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 56
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/DocFieldConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/DocFieldConsumerPerField;>;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/index/DocInverterPerThread;

    .line 58
    .local v9, "perThread":Lorg/apache/lucene/index/DocInverterPerThread;
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 59
    .local v0, "childFields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 60
    .local v2, "endChildFields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Collection;

    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/DocFieldConsumerPerField;

    .local v5, "field":Lorg/apache/lucene/index/DocFieldConsumerPerField;
    move-object v8, v5

    .line 61
    check-cast v8, Lorg/apache/lucene/index/DocInverterPerField;

    .line 62
    .local v8, "perField":Lorg/apache/lucene/index/DocInverterPerField;
    iget-object v10, v8, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    invoke-interface {v0, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 63
    iget-object v10, v8, Lorg/apache/lucene/index/DocInverterPerField;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;

    invoke-interface {v2, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 66
    .end local v5    # "field":Lorg/apache/lucene/index/DocFieldConsumerPerField;
    .end local v8    # "perField":Lorg/apache/lucene/index/DocInverterPerField;
    :cond_0
    iget-object v10, v9, Lorg/apache/lucene/index/DocInverterPerThread;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerThread;

    invoke-interface {v1, v10, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v10, v9, Lorg/apache/lucene/index/DocInverterPerThread;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;

    invoke-interface {v3, v10, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 70
    .end local v0    # "childFields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    .end local v2    # "endChildFields":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;>;"
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/index/DocFieldConsumerPerThread;Ljava/util/Collection<Lorg/apache/lucene/index/DocFieldConsumerPerField;>;>;"
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v9    # "perThread":Lorg/apache/lucene/index/DocInverterPerThread;
    :cond_1
    iget-object v10, p0, Lorg/apache/lucene/index/DocInverter;->consumer:Lorg/apache/lucene/index/InvertedDocConsumer;

    invoke-virtual {v10, v1, p2}, Lorg/apache/lucene/index/InvertedDocConsumer;->flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 71
    iget-object v10, p0, Lorg/apache/lucene/index/DocInverter;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;

    invoke-virtual {v10, v3, p2}, Lorg/apache/lucene/index/InvertedDocEndConsumer;->flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 72
    return-void
.end method

.method public freeRAM()Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverter;->consumer:Lorg/apache/lucene/index/InvertedDocConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocConsumer;->freeRAM()Z

    move-result v0

    return v0
.end method

.method setFieldInfos(Lorg/apache/lucene/index/FieldInfos;)V
    .locals 1
    .param p1, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;

    .prologue
    .line 44
    invoke-super {p0, p1}, Lorg/apache/lucene/index/DocFieldConsumer;->setFieldInfos(Lorg/apache/lucene/index/FieldInfos;)V

    .line 45
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverter;->consumer:Lorg/apache/lucene/index/InvertedDocConsumer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/InvertedDocConsumer;->setFieldInfos(Lorg/apache/lucene/index/FieldInfos;)V

    .line 46
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverter;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/InvertedDocEndConsumer;->setFieldInfos(Lorg/apache/lucene/index/FieldInfos;)V

    .line 47
    return-void
.end method
