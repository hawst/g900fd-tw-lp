.class public Lorg/apache/lucene/index/SortedTermVectorMapper;
.super Lorg/apache/lucene/index/TermVectorMapper;
.source "SortedTermVectorMapper.java"


# static fields
.field public static final ALL:Ljava/lang/String; = "_ALL_"


# instance fields
.field private currentSet:Ljava/util/SortedSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedSet",
            "<",
            "Lorg/apache/lucene/index/TermVectorEntry;",
            ">;"
        }
    .end annotation
.end field

.field private storeOffsets:Z

.field private storePositions:Z

.field private termToTVE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/TermVectorEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/index/TermVectorEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/index/TermVectorEntry;>;"
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, v0, v0, p1}, Lorg/apache/lucene/index/SortedTermVectorMapper;-><init>(ZZLjava/util/Comparator;)V

    .line 47
    return-void
.end method

.method public constructor <init>(ZZLjava/util/Comparator;)V
    .locals 1
    .param p1, "ignoringPositions"    # Z
    .param p2, "ignoringOffsets"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/index/TermVectorEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p3, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/index/TermVectorEntry;>;"
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/index/TermVectorMapper;-><init>(ZZ)V

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SortedTermVectorMapper;->termToTVE:Ljava/util/Map;

    .line 52
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0, p3}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lorg/apache/lucene/index/SortedTermVectorMapper;->currentSet:Ljava/util/SortedSet;

    .line 53
    return-void
.end method


# virtual methods
.method public getTermVectorEntrySet()Ljava/util/SortedSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedSet",
            "<",
            "Lorg/apache/lucene/index/TermVectorEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/lucene/index/SortedTermVectorMapper;->currentSet:Ljava/util/SortedSet;

    return-object v0
.end method

.method public map(Ljava/lang/String;I[Lorg/apache/lucene/index/TermVectorOffsetInfo;[I)V
    .locals 10
    .param p1, "term"    # Ljava/lang/String;
    .param p2, "frequency"    # I
    .param p3, "offsets"    # [Lorg/apache/lucene/index/TermVectorOffsetInfo;
    .param p4, "positions"    # [I

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 65
    iget-object v1, p0, Lorg/apache/lucene/index/SortedTermVectorMapper;->termToTVE:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/TermVectorEntry;

    .line 66
    .local v0, "entry":Lorg/apache/lucene/index/TermVectorEntry;
    if-nez v0, :cond_3

    .line 67
    new-instance v0, Lorg/apache/lucene/index/TermVectorEntry;

    .end local v0    # "entry":Lorg/apache/lucene/index/TermVectorEntry;
    const-string/jumbo v1, "_ALL_"

    iget-boolean v3, p0, Lorg/apache/lucene/index/SortedTermVectorMapper;->storeOffsets:Z

    if-ne v3, v5, :cond_1

    move-object v4, p3

    :goto_0
    iget-boolean v3, p0, Lorg/apache/lucene/index/SortedTermVectorMapper;->storePositions:Z

    if-ne v3, v5, :cond_2

    move-object v5, p4

    :goto_1
    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/TermVectorEntry;-><init>(Ljava/lang/String;Ljava/lang/String;I[Lorg/apache/lucene/index/TermVectorOffsetInfo;[I)V

    .line 70
    .restart local v0    # "entry":Lorg/apache/lucene/index/TermVectorEntry;
    iget-object v1, p0, Lorg/apache/lucene/index/SortedTermVectorMapper;->termToTVE:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v1, p0, Lorg/apache/lucene/index/SortedTermVectorMapper;->currentSet:Ljava/util/SortedSet;

    invoke-interface {v1, v0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    .line 110
    :cond_0
    :goto_2
    return-void

    .end local v0    # "entry":Lorg/apache/lucene/index/TermVectorEntry;
    :cond_1
    move-object v4, v2

    .line 67
    goto :goto_0

    :cond_2
    move-object v5, v2

    goto :goto_1

    .line 73
    .restart local v0    # "entry":Lorg/apache/lucene/index/TermVectorEntry;
    :cond_3
    invoke-virtual {v0}, Lorg/apache/lucene/index/TermVectorEntry;->getFrequency()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/TermVectorEntry;->setFrequency(I)V

    .line 74
    iget-boolean v1, p0, Lorg/apache/lucene/index/SortedTermVectorMapper;->storeOffsets:Z

    if-eqz v1, :cond_4

    .line 76
    invoke-virtual {v0}, Lorg/apache/lucene/index/TermVectorEntry;->getOffsets()[Lorg/apache/lucene/index/TermVectorOffsetInfo;

    move-result-object v6

    .line 78
    .local v6, "existingOffsets":[Lorg/apache/lucene/index/TermVectorOffsetInfo;
    if-eqz v6, :cond_5

    if-eqz p3, :cond_5

    array-length v1, p3

    if-lez v1, :cond_5

    .line 81
    array-length v1, v6

    array-length v2, p3

    add-int/2addr v1, v2

    new-array v8, v1, [Lorg/apache/lucene/index/TermVectorOffsetInfo;

    .line 82
    .local v8, "newOffsets":[Lorg/apache/lucene/index/TermVectorOffsetInfo;
    array-length v1, v6

    invoke-static {v6, v3, v8, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 83
    array-length v1, v6

    array-length v2, p3

    invoke-static {p3, v3, v8, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 84
    invoke-virtual {v0, v8}, Lorg/apache/lucene/index/TermVectorEntry;->setOffsets([Lorg/apache/lucene/index/TermVectorOffsetInfo;)V

    .line 92
    .end local v6    # "existingOffsets":[Lorg/apache/lucene/index/TermVectorOffsetInfo;
    .end local v8    # "newOffsets":[Lorg/apache/lucene/index/TermVectorOffsetInfo;
    :cond_4
    :goto_3
    iget-boolean v1, p0, Lorg/apache/lucene/index/SortedTermVectorMapper;->storePositions:Z

    if-eqz v1, :cond_0

    .line 94
    invoke-virtual {v0}, Lorg/apache/lucene/index/TermVectorEntry;->getPositions()[I

    move-result-object v7

    .line 95
    .local v7, "existingPositions":[I
    if-eqz v7, :cond_6

    if-eqz p4, :cond_6

    array-length v1, p4

    if-lez v1, :cond_6

    .line 97
    array-length v1, v7

    array-length v2, p4

    add-int/2addr v1, v2

    new-array v9, v1, [I

    .line 98
    .local v9, "newPositions":[I
    array-length v1, v7

    invoke-static {v7, v3, v9, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 99
    array-length v1, v7

    array-length v2, p4

    invoke-static {p4, v3, v9, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 100
    invoke-virtual {v0, v9}, Lorg/apache/lucene/index/TermVectorEntry;->setPositions([I)V

    goto :goto_2

    .line 86
    .end local v7    # "existingPositions":[I
    .end local v9    # "newPositions":[I
    .restart local v6    # "existingOffsets":[Lorg/apache/lucene/index/TermVectorOffsetInfo;
    :cond_5
    if-nez v6, :cond_4

    if-eqz p3, :cond_4

    array-length v1, p3

    if-lez v1, :cond_4

    .line 88
    invoke-virtual {v0, p3}, Lorg/apache/lucene/index/TermVectorEntry;->setOffsets([Lorg/apache/lucene/index/TermVectorOffsetInfo;)V

    goto :goto_3

    .line 102
    .end local v6    # "existingOffsets":[Lorg/apache/lucene/index/TermVectorOffsetInfo;
    .restart local v7    # "existingPositions":[I
    :cond_6
    if-nez v7, :cond_0

    if-eqz p4, :cond_0

    array-length v1, p4

    if-lez v1, :cond_0

    .line 104
    invoke-virtual {v0, p4}, Lorg/apache/lucene/index/TermVectorEntry;->setPositions([I)V

    goto :goto_2
.end method

.method public setExpectations(Ljava/lang/String;IZZ)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "numTerms"    # I
    .param p3, "storeOffsets"    # Z
    .param p4, "storePositions"    # Z

    .prologue
    .line 115
    iput-boolean p3, p0, Lorg/apache/lucene/index/SortedTermVectorMapper;->storeOffsets:Z

    .line 116
    iput-boolean p4, p0, Lorg/apache/lucene/index/SortedTermVectorMapper;->storePositions:Z

    .line 117
    return-void
.end method
