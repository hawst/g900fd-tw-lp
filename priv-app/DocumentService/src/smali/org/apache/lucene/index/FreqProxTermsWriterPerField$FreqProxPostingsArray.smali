.class final Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
.super Lorg/apache/lucene/index/ParallelPostingsArray;
.source "FreqProxTermsWriterPerField.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "FreqProxPostingsArray"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field docFreqs:[I

.field lastDocCodes:[I

.field lastDocIDs:[I

.field lastPositions:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 188
    const-class v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 190
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/ParallelPostingsArray;-><init>(I)V

    .line 191
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->docFreqs:[I

    .line 192
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    .line 193
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    .line 194
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastPositions:[I

    .line 195
    return-void
.end method


# virtual methods
.method bytesPerPosting()I
    .locals 1

    .prologue
    .line 222
    const/16 v0, 0x1c

    return v0
.end method

.method copyTo(Lorg/apache/lucene/index/ParallelPostingsArray;I)V
    .locals 4
    .param p1, "toArray"    # Lorg/apache/lucene/index/ParallelPostingsArray;
    .param p2, "numToCopy"    # I

    .prologue
    const/4 v3, 0x0

    .line 209
    sget-boolean v1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_0
    move-object v0, p1

    .line 210
    check-cast v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    .line 212
    .local v0, "to":Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/index/ParallelPostingsArray;->copyTo(Lorg/apache/lucene/index/ParallelPostingsArray;I)V

    .line 214
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->docFreqs:[I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->docFreqs:[I

    invoke-static {v1, v3, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 215
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocIDs:[I

    invoke-static {v1, v3, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 216
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastDocCodes:[I

    invoke-static {v1, v3, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 217
    iget-object v1, p0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastPositions:[I

    iget-object v2, v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;->lastPositions:[I

    invoke-static {v1, v3, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 218
    return-void
.end method

.method newInstance(I)Lorg/apache/lucene/index/ParallelPostingsArray;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 204
    new-instance v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;

    invoke-direct {v0, p1}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField$FreqProxPostingsArray;-><init>(I)V

    return-object v0
.end method
