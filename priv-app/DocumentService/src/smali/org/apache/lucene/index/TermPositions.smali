.class public interface abstract Lorg/apache/lucene/index/TermPositions;
.super Ljava/lang/Object;
.source "TermPositions.java"

# interfaces
.implements Lorg/apache/lucene/index/TermDocs;


# virtual methods
.method public abstract getPayload([BI)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getPayloadLength()I
.end method

.method public abstract isPayloadAvailable()Z
.end method

.method public abstract nextPosition()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
