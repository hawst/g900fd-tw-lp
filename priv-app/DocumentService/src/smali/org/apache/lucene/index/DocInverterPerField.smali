.class final Lorg/apache/lucene/index/DocInverterPerField;
.super Lorg/apache/lucene/index/DocFieldConsumerPerField;
.source "DocInverterPerField.java"


# instance fields
.field final consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

.field final docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

.field final endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;

.field private final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field final fieldState:Lorg/apache/lucene/index/FieldInvertState;

.field private final perThread:Lorg/apache/lucene/index/DocInverterPerThread;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocInverterPerThread;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 1
    .param p1, "perThread"    # Lorg/apache/lucene/index/DocInverterPerThread;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/apache/lucene/index/DocFieldConsumerPerField;-><init>()V

    .line 46
    iput-object p1, p0, Lorg/apache/lucene/index/DocInverterPerField;->perThread:Lorg/apache/lucene/index/DocInverterPerThread;

    .line 47
    iput-object p2, p0, Lorg/apache/lucene/index/DocInverterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 48
    iget-object v0, p1, Lorg/apache/lucene/index/DocInverterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    .line 49
    iget-object v0, p1, Lorg/apache/lucene/index/DocInverterPerThread;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    .line 50
    iget-object v0, p1, Lorg/apache/lucene/index/DocInverterPerThread;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerThread;

    invoke-virtual {v0, p0, p2}, Lorg/apache/lucene/index/InvertedDocConsumerPerThread;->addField(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    .line 51
    iget-object v0, p1, Lorg/apache/lucene/index/DocInverterPerThread;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;

    invoke-virtual {v0, p0, p2}, Lorg/apache/lucene/index/InvertedDocEndConsumerPerThread;->addField(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerField;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;

    .line 52
    return-void
.end method


# virtual methods
.method abort()V
    .locals 2

    .prologue
    .line 57
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocConsumerPerField;->abort()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverterPerField;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;->abort()V

    .line 61
    return-void

    .line 59
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/lucene/index/DocInverterPerField;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;

    invoke-virtual {v1}, Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;->abort()V

    throw v0
.end method

.method public processFields([Lorg/apache/lucene/document/Fieldable;I)V
    .locals 26
    .param p1, "fields"    # [Lorg/apache/lucene/document/Fieldable;
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->doc:Lorg/apache/lucene/document/Document;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/document/Document;->getBoost()F

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lorg/apache/lucene/index/FieldInvertState;->reset(F)V

    .line 69
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v9, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->maxFieldLength:I

    .line 71
    .local v9, "maxFieldLength":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/InvertedDocConsumerPerField;->start([Lorg/apache/lucene/document/Fieldable;I)Z

    move-result v5

    .line 73
    .local v5, "doInvert":Z
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    move/from16 v0, p2

    if-ge v8, v0, :cond_13

    .line 75
    aget-object v6, p1, v8

    .line 80
    .local v6, "field":Lorg/apache/lucene/document/Fieldable;
    invoke-interface {v6}, Lorg/apache/lucene/document/Fieldable;->isIndexed()Z

    move-result v22

    if-eqz v22, :cond_3

    if-eqz v5, :cond_3

    .line 82
    invoke-interface {v6}, Lorg/apache/lucene/document/Fieldable;->getBoost()F

    move-result v4

    .line 84
    .local v4, "boost":F
    invoke-interface {v6}, Lorg/apache/lucene/document/Fieldable;->getOmitNorms()Z

    move-result v22

    if-eqz v22, :cond_0

    const/high16 v22, 0x3f800000    # 1.0f

    cmpl-float v22, v4, v22

    if-eqz v22, :cond_0

    .line 85
    new-instance v22, Ljava/lang/UnsupportedOperationException;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "You cannot set an index-time boost: norms are omitted for field \'"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-interface {v6}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, "\'"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 88
    :cond_0
    if-lez v8, :cond_1

    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    move-object/from16 v22, v0

    if-nez v22, :cond_4

    const/16 v22, 0x0

    :goto_1
    add-int v22, v22, v24

    move/from16 v0, v22

    move-object/from16 v1, v23

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->position:I

    .line 91
    :cond_1
    invoke-interface {v6}, Lorg/apache/lucene/document/Fieldable;->isTokenized()Z

    move-result v22

    if-nez v22, :cond_6

    .line 92
    invoke-interface {v6}, Lorg/apache/lucene/document/Fieldable;->stringValue()Ljava/lang/String;

    move-result-object v19

    .line 93
    .local v19, "stringValue":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    .line 94
    .local v21, "valueLength":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->perThread:Lorg/apache/lucene/index/DocInverterPerThread;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerThread;->singleToken:Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    move/from16 v2, v23

    move/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;->reinit(Ljava/lang/String;II)V

    .line 95
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->perThread:Lorg/apache/lucene/index/DocInverterPerThread;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerThread;->singleToken:Lorg/apache/lucene/index/DocInverterPerThread$SingleTokenAttributeSource;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    iput-object v0, v1, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    .line 96
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Lorg/apache/lucene/index/InvertedDocConsumerPerField;->start(Lorg/apache/lucene/document/Fieldable;)V

    .line 98
    const/16 v20, 0x0

    .line 100
    .local v20, "success":Z
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/index/InvertedDocConsumerPerField;->add()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    const/16 v20, 0x1

    .line 103
    if-nez v20, :cond_2

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/index/DocumentsWriter;->setAborting()V

    .line 106
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    move/from16 v23, v0

    add-int v23, v23, v21

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    .line 107
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->length:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->length:I

    .line 108
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->position:I

    .line 208
    .end local v19    # "stringValue":Ljava/lang/String;
    .end local v20    # "success":Z
    .end local v21    # "valueLength":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    move-object/from16 v22, v0

    if-nez v22, :cond_12

    const/16 v22, 0x0

    :goto_3
    add-int v22, v22, v24

    move/from16 v0, v22

    move-object/from16 v1, v23

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->boost:F

    move/from16 v23, v0

    mul-float v23, v23, v4

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->boost:F

    .line 214
    .end local v4    # "boost":F
    :cond_3
    const/16 v22, 0x0

    aput-object v22, p1, v8

    .line 73
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 89
    .restart local v4    # "boost":F
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/Analyzer;->getPositionIncrementGap(Ljava/lang/String;)I

    move-result v22

    goto/16 :goto_1

    .line 103
    .restart local v19    # "stringValue":Ljava/lang/String;
    .restart local v20    # "success":Z
    .restart local v21    # "valueLength":I
    :catchall_0
    move-exception v22

    if-nez v20, :cond_5

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/DocumentsWriter;->setAborting()V

    .line 103
    :cond_5
    throw v22

    .line 111
    .end local v19    # "stringValue":Ljava/lang/String;
    .end local v20    # "success":Z
    .end local v21    # "valueLength":I
    :cond_6
    invoke-interface {v6}, Lorg/apache/lucene/document/Fieldable;->tokenStreamValue()Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v18

    .line 113
    .local v18, "streamValue":Lorg/apache/lucene/analysis/TokenStream;
    if-eqz v18, :cond_8

    .line 114
    move-object/from16 v17, v18

    .line 136
    .local v17, "stream":Lorg/apache/lucene/analysis/TokenStream;
    :goto_4
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 138
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->length:I

    move/from16 v16, v0

    .line 141
    .local v16, "startLength":I
    :try_start_1
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v7

    .line 143
    .local v7, "hasMoreTokens":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    iput-object v0, v1, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    .line 145
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    move-object/from16 v22, v0

    const-class v23, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual/range {v22 .. v23}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 146
    .local v10, "offsetAttribute":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    move-object/from16 v22, v0

    const-class v23, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual/range {v22 .. v23}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 148
    .local v12, "posIncrAttribute":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Lorg/apache/lucene/index/InvertedDocConsumerPerField;->start(Lorg/apache/lucene/document/Fieldable;)V

    .line 159
    :goto_5
    if-nez v7, :cond_b

    .line 200
    :cond_7
    :goto_6
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    move/from16 v23, v0

    invoke-interface {v10}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v24

    add-int v23, v23, v24

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->offset:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 204
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/analysis/TokenStream;->close()V

    goto/16 :goto_2

    .line 119
    .end local v7    # "hasMoreTokens":Z
    .end local v10    # "offsetAttribute":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    .end local v12    # "posIncrAttribute":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .end local v16    # "startLength":I
    .end local v17    # "stream":Lorg/apache/lucene/analysis/TokenStream;
    :cond_8
    invoke-interface {v6}, Lorg/apache/lucene/document/Fieldable;->readerValue()Ljava/io/Reader;

    move-result-object v15

    .line 121
    .local v15, "readerValue":Ljava/io/Reader;
    if-eqz v15, :cond_9

    .line 122
    move-object v14, v15

    .line 132
    .local v14, "reader":Ljava/io/Reader;
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v14}, Lorg/apache/lucene/analysis/Analyzer;->reusableTokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v17

    .restart local v17    # "stream":Lorg/apache/lucene/analysis/TokenStream;
    goto/16 :goto_4

    .line 124
    .end local v14    # "reader":Ljava/io/Reader;
    .end local v17    # "stream":Lorg/apache/lucene/analysis/TokenStream;
    :cond_9
    invoke-interface {v6}, Lorg/apache/lucene/document/Fieldable;->stringValue()Ljava/lang/String;

    move-result-object v19

    .line 125
    .restart local v19    # "stringValue":Ljava/lang/String;
    if-nez v19, :cond_a

    .line 126
    new-instance v22, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v23, "field must have either TokenStream, String or Reader value"

    invoke-direct/range {v22 .. v23}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 127
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->perThread:Lorg/apache/lucene/index/DocInverterPerThread;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerThread;->stringReader:Lorg/apache/lucene/index/ReusableStringReader;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ReusableStringReader;->init(Ljava/lang/String;)V

    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->perThread:Lorg/apache/lucene/index/DocInverterPerThread;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v14, v0, Lorg/apache/lucene/index/DocInverterPerThread;->stringReader:Lorg/apache/lucene/index/ReusableStringReader;

    .restart local v14    # "reader":Ljava/io/Reader;
    goto :goto_7

    .line 161
    .end local v14    # "reader":Ljava/io/Reader;
    .end local v15    # "readerValue":Ljava/io/Reader;
    .end local v19    # "stringValue":Ljava/lang/String;
    .restart local v7    # "hasMoreTokens":Z
    .restart local v10    # "offsetAttribute":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    .restart local v12    # "posIncrAttribute":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .restart local v16    # "startLength":I
    .restart local v17    # "stream":Lorg/apache/lucene/analysis/TokenStream;
    :cond_b
    :try_start_2
    invoke-interface {v12}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v11

    .line 162
    .local v11, "posIncr":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    move/from16 v22, v0

    add-int v13, v22, v11

    .line 163
    .local v13, "position":I
    if-lez v13, :cond_f

    .line 164
    add-int/lit8 v13, v13, -0x1

    .line 171
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iput v13, v0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    .line 173
    if-nez v11, :cond_d

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->numOverlap:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->numOverlap:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 176
    :cond_d
    const/16 v20, 0x0

    .line 184
    .restart local v20    # "success":Z
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/index/InvertedDocConsumerPerField;->add()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 185
    const/16 v20, 0x1

    .line 187
    if-nez v20, :cond_e

    .line 188
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/index/DocumentsWriter;->setAborting()V

    .line 190
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->position:I

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->length:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->length:I

    move/from16 v0, v23

    if-lt v0, v9, :cond_11

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v22, v0

    if-eqz v22, :cond_7

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v22, v0

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "maxFieldLength "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, " reached for field "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, ", ignoring following tokens"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_6

    .line 204
    .end local v7    # "hasMoreTokens":Z
    .end local v10    # "offsetAttribute":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    .end local v11    # "posIncr":I
    .end local v12    # "posIncrAttribute":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .end local v13    # "position":I
    .end local v20    # "success":Z
    :catchall_1
    move-exception v22

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/analysis/TokenStream;->close()V

    throw v22

    .line 165
    .restart local v7    # "hasMoreTokens":Z
    .restart local v10    # "offsetAttribute":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    .restart local v11    # "posIncr":I
    .restart local v12    # "posIncrAttribute":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .restart local v13    # "position":I
    :cond_f
    if-gez v13, :cond_c

    .line 166
    :try_start_5
    new-instance v22, Ljava/lang/IllegalArgumentException;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "position overflow for field \'"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-interface {v6}, Lorg/apache/lucene/document/Fieldable;->name()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, "\'"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 187
    .restart local v20    # "success":Z
    :catchall_2
    move-exception v22

    if-nez v20, :cond_10

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->docWriter:Lorg/apache/lucene/index/DocumentsWriter;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/DocumentsWriter;->setAborting()V

    .line 187
    :cond_10
    throw v22

    .line 197
    :cond_11
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v7

    .line 198
    goto/16 :goto_5

    .line 208
    .end local v7    # "hasMoreTokens":Z
    .end local v10    # "offsetAttribute":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    .end local v11    # "posIncr":I
    .end local v12    # "posIncrAttribute":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .end local v13    # "position":I
    .end local v16    # "startLength":I
    .end local v17    # "stream":Lorg/apache/lucene/analysis/TokenStream;
    .end local v18    # "streamValue":Lorg/apache/lucene/analysis/TokenStream;
    .end local v20    # "success":Z
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriter$DocState;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriter$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Lorg/apache/lucene/analysis/Analyzer;->getOffsetGap(Lorg/apache/lucene/document/Fieldable;)I

    move-result v22

    goto/16 :goto_3

    .line 217
    .end local v4    # "boost":F
    .end local v6    # "field":Lorg/apache/lucene/document/Fieldable;
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/index/InvertedDocConsumerPerField;->finish()V

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;->finish()V

    .line 219
    return-void
.end method
