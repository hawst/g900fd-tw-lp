.class public final Lorg/apache/lucene/index/IndexWriterConfig;
.super Ljava/lang/Object;
.source "IndexWriterConfig.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    }
.end annotation


# static fields
.field public static final DEFAULT_MAX_BUFFERED_DELETE_TERMS:I = -0x1

.field public static final DEFAULT_MAX_BUFFERED_DOCS:I = -0x1

.field public static final DEFAULT_MAX_THREAD_STATES:I = 0x8

.field public static final DEFAULT_RAM_BUFFER_SIZE_MB:D = 16.0

.field public static final DEFAULT_READER_POOLING:Z = false

.field public static final DEFAULT_READER_TERMS_INDEX_DIVISOR:I

.field public static final DEFAULT_TERM_INDEX_INTERVAL:I = 0x80

.field public static final DISABLE_AUTO_FLUSH:I = -0x1

.field public static WRITE_LOCK_TIMEOUT:J


# instance fields
.field private final analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field private volatile commit:Lorg/apache/lucene/index/IndexCommit;

.field private volatile delPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

.field private volatile indexingChain:Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;

.field private matchVersion:Lorg/apache/lucene/util/Version;

.field private volatile maxBufferedDeleteTerms:I

.field private volatile maxBufferedDocs:I

.field private volatile maxThreadStates:I

.field private volatile mergePolicy:Lorg/apache/lucene/index/MergePolicy;

.field private volatile mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

.field private volatile mergedSegmentWarmer:Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

.field private volatile openMode:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

.field private volatile ramBufferSizeMB:D

.field private volatile readerPooling:Z

.field private volatile readerTermsIndexDivisor:I

.field private volatile similarity:Lorg/apache/lucene/search/Similarity;

.field private volatile termIndexInterval:I

.field private volatile writeLockTimeout:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 81
    const-wide/16 v0, 0x3e8

    sput-wide v0, Lorg/apache/lucene/index/IndexWriterConfig;->WRITE_LOCK_TIMEOUT:J

    .line 93
    sget v0, Lorg/apache/lucene/index/IndexReader;->DEFAULT_TERMS_INDEX_DIVISOR:I

    sput v0, Lorg/apache/lucene/index/IndexWriterConfig;->DEFAULT_READER_TERMS_INDEX_DIVISOR:I

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 4
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 147
    iput-object p2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 148
    new-instance v0, Lorg/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy;

    invoke-direct {v0}, Lorg/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->delPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    .line 149
    iput-object v3, p0, Lorg/apache/lucene/index/IndexWriterConfig;->commit:Lorg/apache/lucene/index/IndexCommit;

    .line 150
    sget-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE_OR_APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->openMode:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    .line 151
    invoke-static {}, Lorg/apache/lucene/search/Similarity;->getDefault()Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 152
    const/16 v0, 0x80

    iput v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->termIndexInterval:I

    .line 153
    new-instance v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-direct {v0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    .line 154
    sget-wide v0, Lorg/apache/lucene/index/IndexWriterConfig;->WRITE_LOCK_TIMEOUT:J

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->writeLockTimeout:J

    .line 155
    iput v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->maxBufferedDeleteTerms:I

    .line 156
    const-wide/high16 v0, 0x4030000000000000L    # 16.0

    iput-wide v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->ramBufferSizeMB:D

    .line 157
    iput v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->maxBufferedDocs:I

    .line 158
    sget-object v0, Lorg/apache/lucene/index/DocumentsWriter;->defaultIndexingChain:Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->indexingChain:Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;

    .line 159
    iput-object v3, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergedSegmentWarmer:Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    .line 160
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_32:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    new-instance v0, Lorg/apache/lucene/index/TieredMergePolicy;

    invoke-direct {v0}, Lorg/apache/lucene/index/TieredMergePolicy;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    .line 165
    :goto_0
    const/16 v0, 0x8

    iput v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->maxThreadStates:I

    .line 166
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->readerPooling:Z

    .line 167
    sget v0, Lorg/apache/lucene/index/IndexWriterConfig;->DEFAULT_READER_TERMS_INDEX_DIVISOR:I

    iput v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->readerTermsIndexDivisor:I

    .line 168
    return-void

    .line 163
    :cond_0
    new-instance v0, Lorg/apache/lucene/index/LogByteSizeMergePolicy;

    invoke-direct {v0}, Lorg/apache/lucene/index/LogByteSizeMergePolicy;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    goto :goto_0
.end method

.method public static getDefaultWriteLockTimeout()J
    .locals 2

    .prologue
    .line 110
    sget-wide v0, Lorg/apache/lucene/index/IndexWriterConfig;->WRITE_LOCK_TIMEOUT:J

    return-wide v0
.end method

.method public static setDefaultWriteLockTimeout(J)V
    .locals 0
    .param p0, "writeLockTimeout"    # J

    .prologue
    .line 100
    sput-wide p0, Lorg/apache/lucene/index/IndexWriterConfig;->WRITE_LOCK_TIMEOUT:J

    .line 101
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 175
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 176
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    return-object v0
.end method

.method public getIndexCommit()Lorg/apache/lucene/index/IndexCommit;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->commit:Lorg/apache/lucene/index/IndexCommit;

    return-object v0
.end method

.method public getIndexDeletionPolicy()Lorg/apache/lucene/index/IndexDeletionPolicy;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->delPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    return-object v0
.end method

.method getIndexingChain()Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->indexingChain:Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;

    return-object v0
.end method

.method public getMaxBufferedDeleteTerms()I
    .locals 1

    .prologue
    .line 383
    iget v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->maxBufferedDeleteTerms:I

    return v0
.end method

.method public getMaxBufferedDocs()I
    .locals 1

    .prologue
    .line 488
    iget v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->maxBufferedDocs:I

    return v0
.end method

.method public getMaxThreadStates()I
    .locals 1

    .prologue
    .line 541
    iget v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->maxThreadStates:I

    return v0
.end method

.method public getMergePolicy()Lorg/apache/lucene/index/MergePolicy;
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    return-object v0
.end method

.method public getMergeScheduler()Lorg/apache/lucene/index/MergeScheduler;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    return-object v0
.end method

.method public getMergedSegmentWarmer()Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergedSegmentWarmer:Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    return-object v0
.end method

.method public getOpenMode()Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->openMode:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    return-object v0
.end method

.method public getRAMBufferSizeMB()D
    .locals 2

    .prologue
    .line 442
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->ramBufferSizeMB:D

    return-wide v0
.end method

.method public getReaderPooling()Z
    .locals 1

    .prologue
    .line 562
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->readerPooling:Z

    return v0
.end method

.method public getReaderTermsIndexDivisor()I
    .locals 1

    .prologue
    .line 598
    iget v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->readerTermsIndexDivisor:I

    return v0
.end method

.method public getSimilarity()Lorg/apache/lucene/search/Similarity;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->similarity:Lorg/apache/lucene/search/Similarity;

    return-object v0
.end method

.method public getTermIndexInterval()I
    .locals 1

    .prologue
    .line 308
    iget v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->termIndexInterval:I

    return v0
.end method

.method public getWriteLockTimeout()J
    .locals 2

    .prologue
    .line 349
    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->writeLockTimeout:J

    return-wide v0
.end method

.method public setIndexCommit(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 0
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;

    .prologue
    .line 237
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->commit:Lorg/apache/lucene/index/IndexCommit;

    .line 238
    return-object p0
.end method

.method public setIndexDeletionPolicy(Lorg/apache/lucene/index/IndexDeletionPolicy;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 0
    .param p1, "delPolicy"    # Lorg/apache/lucene/index/IndexDeletionPolicy;

    .prologue
    .line 218
    if-nez p1, :cond_0

    new-instance p1, Lorg/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy;

    .end local p1    # "delPolicy":Lorg/apache/lucene/index/IndexDeletionPolicy;
    invoke-direct {p1}, Lorg/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy;-><init>()V

    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->delPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    .line 219
    return-object p0
.end method

.method setIndexingChain(Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 0
    .param p1, "indexingChain"    # Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;

    .prologue
    .line 569
    if-nez p1, :cond_0

    sget-object p1, Lorg/apache/lucene/index/DocumentsWriter;->defaultIndexingChain:Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;

    .end local p1    # "indexingChain":Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->indexingChain:Lorg/apache/lucene/index/DocumentsWriter$IndexingChain;

    .line 570
    return-object p0
.end method

.method public setMaxBufferedDeleteTerms(I)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 2
    .param p1, "maxBufferedDeleteTerms"    # I

    .prologue
    .line 368
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 370
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "maxBufferedDeleteTerms must at least be 1 when enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 372
    :cond_0
    iput p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->maxBufferedDeleteTerms:I

    .line 373
    return-object p0
.end method

.method public setMaxBufferedDocs(I)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 4
    .param p1, "maxBufferedDocs"    # I

    .prologue
    const/4 v1, -0x1

    .line 470
    if-eq p1, v1, :cond_0

    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    .line 471
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "maxBufferedDocs must at least be 2 when enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 473
    :cond_0
    if-ne p1, v1, :cond_1

    iget-wide v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->ramBufferSizeMB:D

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 475
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "at least one of ramBufferSize and maxBufferedDocs must be enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 477
    :cond_1
    iput p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->maxBufferedDocs:I

    .line 478
    return-object p0
.end method

.method public setMaxThreadStates(I)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 1
    .param p1, "maxThreadStates"    # I

    .prologue
    .line 534
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    const/16 p1, 0x8

    .end local p1    # "maxThreadStates":I
    :cond_0
    iput p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->maxThreadStates:I

    .line 535
    return-object p0
.end method

.method public setMergePolicy(Lorg/apache/lucene/index/MergePolicy;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 0
    .param p1, "mergePolicy"    # Lorg/apache/lucene/index/MergePolicy;

    .prologue
    .line 513
    if-nez p1, :cond_0

    new-instance p1, Lorg/apache/lucene/index/LogByteSizeMergePolicy;

    .end local p1    # "mergePolicy":Lorg/apache/lucene/index/MergePolicy;
    invoke-direct {p1}, Lorg/apache/lucene/index/LogByteSizeMergePolicy;-><init>()V

    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    .line 514
    return-object p0
.end method

.method public setMergeScheduler(Lorg/apache/lucene/index/MergeScheduler;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 0
    .param p1, "mergeScheduler"    # Lorg/apache/lucene/index/MergeScheduler;

    .prologue
    .line 320
    if-nez p1, :cond_0

    new-instance p1, Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    .end local p1    # "mergeScheduler":Lorg/apache/lucene/index/MergeScheduler;
    invoke-direct {p1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;-><init>()V

    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    .line 321
    return-object p0
.end method

.method public setMergedSegmentWarmer(Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 0
    .param p1, "mergeSegmentWarmer"    # Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    .prologue
    .line 495
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergedSegmentWarmer:Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    .line 496
    return-object p0
.end method

.method public setOpenMode(Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 0
    .param p1, "openMode"    # Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    .prologue
    .line 191
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->openMode:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    .line 192
    return-object p0
.end method

.method public setRAMBufferSizeMB(D)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 5
    .param p1, "ramBufferSizeMB"    # D

    .prologue
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    .line 426
    const-wide/high16 v0, 0x40a0000000000000L    # 2048.0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_0

    .line 427
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ramBufferSize "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is too large; should be comfortably less than 2048"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 430
    :cond_0
    cmpl-double v0, p1, v2

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_1

    .line 431
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "ramBufferSize should be > 0.0 MB when enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 433
    :cond_1
    cmpl-double v0, p1, v2

    if-nez v0, :cond_2

    iget v0, p0, Lorg/apache/lucene/index/IndexWriterConfig;->maxBufferedDocs:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 434
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "at least one of ramBufferSize and maxBufferedDocs must be enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 436
    :cond_2
    iput-wide p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->ramBufferSizeMB:D

    .line 437
    return-object p0
.end method

.method public setReaderPooling(Z)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 0
    .param p1, "readerPooling"    # Z

    .prologue
    .line 555
    iput-boolean p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->readerPooling:Z

    .line 556
    return-object p0
.end method

.method public setReaderTermsIndexDivisor(I)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 3
    .param p1, "divisor"    # I

    .prologue
    .line 589
    if-gtz p1, :cond_0

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 590
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "divisor must be >= 1, or -1 (got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 592
    :cond_0
    iput p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->readerTermsIndexDivisor:I

    .line 593
    return-object p0
.end method

.method public setSimilarity(Lorg/apache/lucene/search/Similarity;)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 0
    .param p1, "similarity"    # Lorg/apache/lucene/search/Similarity;

    .prologue
    .line 260
    if-nez p1, :cond_0

    invoke-static {}, Lorg/apache/lucene/search/Similarity;->getDefault()Lorg/apache/lucene/search/Similarity;

    move-result-object p1

    .end local p1    # "similarity":Lorg/apache/lucene/search/Similarity;
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 261
    return-object p0
.end method

.method public setTermIndexInterval(I)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 0
    .param p1, "interval"    # I

    .prologue
    .line 298
    iput p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->termIndexInterval:I

    .line 299
    return-object p0
.end method

.method public setWriteLockTimeout(J)Lorg/apache/lucene/index/IndexWriterConfig;
    .locals 1
    .param p1, "writeLockTimeout"    # J

    .prologue
    .line 339
    iput-wide p1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->writeLockTimeout:J

    .line 340
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 603
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 604
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "matchVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 605
    const-string/jumbo v1, "analyzer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    if-nez v1, :cond_0

    const-string/jumbo v1, "null"

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    const-string/jumbo v1, "delPolicy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->delPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 607
    const-string/jumbo v1, "commit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->commit:Lorg/apache/lucene/index/IndexCommit;

    if-nez v1, :cond_1

    const-string/jumbo v1, "null"

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 608
    const-string/jumbo v1, "openMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->openMode:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 609
    const-string/jumbo v1, "similarity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->similarity:Lorg/apache/lucene/search/Similarity;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 610
    const-string/jumbo v1, "termIndexInterval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->termIndexInterval:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 611
    const-string/jumbo v1, "mergeScheduler="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 612
    const-string/jumbo v1, "default WRITE_LOCK_TIMEOUT="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-wide v2, Lorg/apache/lucene/index/IndexWriterConfig;->WRITE_LOCK_TIMEOUT:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 613
    const-string/jumbo v1, "writeLockTimeout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->writeLockTimeout:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 614
    const-string/jumbo v1, "maxBufferedDeleteTerms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->maxBufferedDeleteTerms:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 615
    const-string/jumbo v1, "ramBufferSizeMB="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->ramBufferSizeMB:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 616
    const-string/jumbo v1, "maxBufferedDocs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->maxBufferedDocs:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 617
    const-string/jumbo v1, "mergedSegmentWarmer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergedSegmentWarmer:Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 618
    const-string/jumbo v1, "mergePolicy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 619
    const-string/jumbo v1, "maxThreadStates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->maxThreadStates:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 620
    const-string/jumbo v1, "readerPooling="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->readerPooling:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 621
    const-string/jumbo v1, "readerTermsIndexDivisor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/IndexWriterConfig;->readerTermsIndexDivisor:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 605
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 607
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriterConfig;->commit:Lorg/apache/lucene/index/IndexCommit;

    goto/16 :goto_1
.end method
