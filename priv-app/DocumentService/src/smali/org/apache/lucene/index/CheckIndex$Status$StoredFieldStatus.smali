.class public final Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;
.super Ljava/lang/Object;
.source "CheckIndex.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/CheckIndex$Status;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoredFieldStatus"
.end annotation


# instance fields
.field public docCount:I

.field public error:Ljava/lang/Throwable;

.field public totFields:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->docCount:I

    .line 236
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->totFields:J

    .line 239
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->error:Ljava/lang/Throwable;

    return-void
.end method
