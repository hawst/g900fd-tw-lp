.class public abstract Lorg/apache/lucene/search/FieldComparator$NumericComparator;
.super Lorg/apache/lucene/search/FieldComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "NumericComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Number;",
        ">",
        "Lorg/apache/lucene/search/FieldComparator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected docsWithField:Lorg/apache/lucene/util/Bits;

.field protected final field:Ljava/lang/String;

.field protected final missingValue:Ljava/lang/Number;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Number;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 191
    .local p0, "this":Lorg/apache/lucene/search/FieldComparator$NumericComparator;, "Lorg/apache/lucene/search/FieldComparator$NumericComparator<TT;>;"
    .local p2, "missingValue":Ljava/lang/Number;, "TT;"
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldComparator;-><init>()V

    .line 192
    iput-object p1, p0, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->field:Ljava/lang/String;

    .line 193
    iput-object p2, p0, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->missingValue:Ljava/lang/Number;

    .line 194
    return-void
.end method


# virtual methods
.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/FieldComparator$NumericComparator;, "Lorg/apache/lucene/search/FieldComparator$NumericComparator<TT;>;"
    const/4 v2, 0x0

    .line 198
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->missingValue:Ljava/lang/Number;

    if-eqz v0, :cond_1

    .line 199
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->field:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lorg/apache/lucene/search/FieldCache;->getDocsWithField(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/util/Bits;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    .line 201
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    instance-of v0, v0, Lorg/apache/lucene/util/Bits$MatchAllBits;

    if-eqz v0, :cond_0

    .line 202
    iput-object v2, p0, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    iput-object v2, p0, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    goto :goto_0
.end method
