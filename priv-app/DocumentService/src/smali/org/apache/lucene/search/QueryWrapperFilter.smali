.class public Lorg/apache/lucene/search/QueryWrapperFilter;
.super Lorg/apache/lucene/search/Filter;
.source "QueryWrapperFilter.java"


# instance fields
.field private query:Lorg/apache/lucene/search/Query;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Query;)V
    .locals 0
    .param p1, "query"    # Lorg/apache/lucene/search/Query;

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/search/QueryWrapperFilter;->query:Lorg/apache/lucene/search/Query;

    .line 43
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 65
    instance-of v0, p1, Lorg/apache/lucene/search/QueryWrapperFilter;

    if-nez v0, :cond_0

    .line 66
    const/4 v0, 0x0

    .line 67
    .end local p1    # "o":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/QueryWrapperFilter;->query:Lorg/apache/lucene/search/Query;

    check-cast p1, Lorg/apache/lucene/search/QueryWrapperFilter;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/lucene/search/QueryWrapperFilter;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    new-instance v1, Lorg/apache/lucene/search/IndexSearcher;

    invoke-direct {v1, p1}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    iget-object v2, p0, Lorg/apache/lucene/search/QueryWrapperFilter;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    .line 48
    .local v0, "weight":Lorg/apache/lucene/search/Weight;
    new-instance v1, Lorg/apache/lucene/search/QueryWrapperFilter$1;

    invoke-direct {v1, p0, v0, p1}, Lorg/apache/lucene/search/QueryWrapperFilter$1;-><init>(Lorg/apache/lucene/search/QueryWrapperFilter;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/index/IndexReader;)V

    return-object v1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/search/QueryWrapperFilter;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v0

    const v1, -0x6dc09b47

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "QueryWrapperFilter("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/QueryWrapperFilter;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
