.class Lorg/apache/lucene/search/ConjunctionScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "ConjunctionScorer.java"


# instance fields
.field private final coord:F

.field private lastDoc:I

.field private final scorers:[Lorg/apache/lucene/search/Scorer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Weight;FLjava/util/Collection;)V
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "coord"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Weight;",
            "F",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    .local p3, "scorers":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/search/Scorer;>;"
    invoke-interface {p3}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Lorg/apache/lucene/search/Scorer;

    invoke-interface {p3, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/search/Scorer;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/ConjunctionScorer;-><init>(Lorg/apache/lucene/search/Weight;F[Lorg/apache/lucene/search/Scorer;)V

    .line 34
    return-void
.end method

.method public varargs constructor <init>(Lorg/apache/lucene/search/Weight;F[Lorg/apache/lucene/search/Scorer;)V
    .locals 7
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "coord"    # F
    .param p3, "scorers"    # [Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v6, 0x7fffffff

    .line 37
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 30
    const/4 v5, -0x1

    iput v5, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    .line 38
    iput-object p3, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    .line 39
    iput p2, p0, Lorg/apache/lucene/search/ConjunctionScorer;->coord:F

    .line 41
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, p3

    if-ge v1, v5, :cond_2

    .line 42
    aget-object v5, p3, v1

    invoke-virtual {v5}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v5

    if-ne v5, v6, :cond_1

    .line 45
    iput v6, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    .line 92
    :cond_0
    :goto_1
    return-void

    .line 41
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 57
    :cond_2
    new-instance v5, Lorg/apache/lucene/search/ConjunctionScorer$1;

    invoke-direct {v5, p0}, Lorg/apache/lucene/search/ConjunctionScorer$1;-><init>(Lorg/apache/lucene/search/ConjunctionScorer;)V

    invoke-static {p3, v5}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 72
    invoke-direct {p0}, Lorg/apache/lucene/search/ConjunctionScorer;->doNext()I

    move-result v5

    if-ne v5, v6, :cond_3

    .line 74
    iput v6, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    goto :goto_1

    .line 84
    :cond_3
    array-length v5, p3

    add-int/lit8 v0, v5, -0x1

    .line 85
    .local v0, "end":I
    shr-int/lit8 v3, v0, 0x1

    .line 86
    .local v3, "max":I
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v3, :cond_0

    .line 87
    aget-object v4, p3, v1

    .line 88
    .local v4, "tmp":Lorg/apache/lucene/search/Scorer;
    sub-int v5, v0, v1

    add-int/lit8 v2, v5, -0x1

    .line 89
    .local v2, "idx":I
    aget-object v5, p3, v2

    aput-object v5, p3, v1

    .line 90
    aput-object v4, p3, v2

    .line 86
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private doNext()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    const/4 v1, 0x0

    .line 96
    .local v1, "first":I
    iget-object v3, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    iget-object v4, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    .line 98
    .local v0, "doc":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v2, v3, v1

    .local v2, "firstScorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v3

    if-ge v3, v0, :cond_1

    .line 99
    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    .line 100
    iget-object v3, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_0

    const/4 v1, 0x0

    :goto_1
    goto :goto_0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 102
    :cond_1
    return v0
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    iget v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 108
    iget v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    .line 112
    :goto_0
    return v0

    .line 109
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 110
    iget-object v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    .line 112
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/search/ConjunctionScorer;->doNext()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    return v0
.end method

.method public nextDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    iget v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 123
    iget v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    .line 128
    :goto_0
    return v0

    .line 124
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 125
    iget-object v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    goto :goto_0

    .line 127
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    iget-object v1, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    .line 128
    invoke-direct {p0}, Lorg/apache/lucene/search/ConjunctionScorer;->doNext()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ConjunctionScorer;->lastDoc:I

    goto :goto_0
.end method

.method public score()F
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    const/4 v1, 0x0

    .line 134
    .local v1, "sum":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 135
    iget-object v2, p0, Lorg/apache/lucene/search/ConjunctionScorer;->scorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    add-float/2addr v1, v2

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    :cond_0
    iget v2, p0, Lorg/apache/lucene/search/ConjunctionScorer;->coord:F

    mul-float/2addr v2, v1

    return v2
.end method
