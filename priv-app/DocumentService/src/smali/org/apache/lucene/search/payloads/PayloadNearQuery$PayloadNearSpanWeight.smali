.class public Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;
.super Lorg/apache/lucene/search/spans/SpanWeight;
.source "PayloadNearQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/payloads/PayloadNearQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PayloadNearSpanWeight"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/payloads/PayloadNearQuery;Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/Searcher;)V
    .locals 0
    .param p2, "query"    # Lorg/apache/lucene/search/spans/SpanQuery;
    .param p3, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    iput-object p1, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    .line 142
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/search/spans/SpanWeight;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/Searcher;)V

    .line 143
    return-void
.end method


# virtual methods
.method public scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    new-instance v0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v2

    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v5

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;-><init>(Lorg/apache/lucene/search/payloads/PayloadNearQuery;Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Similarity;[B)V

    return-object v0
.end method
