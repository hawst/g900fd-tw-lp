.class public abstract Lorg/apache/lucene/search/function/FieldCacheSource;
.super Lorg/apache/lucene/search/function/ValueSource;
.source "FieldCacheSource.java"


# instance fields
.field private field:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0}, Lorg/apache/lucene/search/function/ValueSource;-><init>()V

    .line 53
    iput-object p1, p0, Lorg/apache/lucene/search/function/FieldCacheSource;->field:Ljava/lang/String;

    .line 54
    return-void
.end method


# virtual methods
.method public abstract cachedFieldSourceEquals(Lorg/apache/lucene/search/function/FieldCacheSource;)Z
.end method

.method public abstract cachedFieldSourceHashCode()I
.end method

.method public description()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/search/function/FieldCacheSource;->field:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 79
    instance-of v2, p1, Lorg/apache/lucene/search/function/FieldCacheSource;

    if-nez v2, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 82
    check-cast v0, Lorg/apache/lucene/search/function/FieldCacheSource;

    .line 83
    .local v0, "other":Lorg/apache/lucene/search/function/FieldCacheSource;
    iget-object v2, p0, Lorg/apache/lucene/search/function/FieldCacheSource;->field:Ljava/lang/String;

    iget-object v3, v0, Lorg/apache/lucene/search/function/FieldCacheSource;->field:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/function/FieldCacheSource;->cachedFieldSourceEquals(Lorg/apache/lucene/search/function/FieldCacheSource;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public abstract getCachedFieldValues(Lorg/apache/lucene/search/FieldCache;Ljava/lang/String;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/function/DocValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final getValues(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/function/DocValues;
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v1, p0, Lorg/apache/lucene/search/function/FieldCacheSource;->field:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, p1}, Lorg/apache/lucene/search/function/FieldCacheSource;->getCachedFieldValues(Lorg/apache/lucene/search/FieldCache;Ljava/lang/String;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/function/DocValues;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/lucene/search/function/FieldCacheSource;->field:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/function/FieldCacheSource;->cachedFieldSourceHashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
