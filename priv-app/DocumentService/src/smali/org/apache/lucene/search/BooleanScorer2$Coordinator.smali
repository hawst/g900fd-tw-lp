.class Lorg/apache/lucene/search/BooleanScorer2$Coordinator;
.super Ljava/lang/Object;
.source "BooleanScorer2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/BooleanScorer2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Coordinator"
.end annotation


# instance fields
.field coordFactors:[F

.field maxCoord:I

.field nrMatchers:I

.field final synthetic this$0:Lorg/apache/lucene/search/BooleanScorer2;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/search/BooleanScorer2;)V
    .locals 1

    .prologue
    .line 40
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->this$0:Lorg/apache/lucene/search/BooleanScorer2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->coordFactors:[F

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->maxCoord:I

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/BooleanScorer2$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/search/BooleanScorer2;
    .param p2, "x1"    # Lorg/apache/lucene/search/BooleanScorer2$1;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;-><init>(Lorg/apache/lucene/search/BooleanScorer2;)V

    return-void
.end method


# virtual methods
.method init(Lorg/apache/lucene/search/Similarity;Z)V
    .locals 3
    .param p1, "sim"    # Lorg/apache/lucene/search/Similarity;
    .param p2, "disableCoord"    # Z

    .prologue
    .line 46
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->this$0:Lorg/apache/lucene/search/BooleanScorer2;

    # getter for: Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;
    invoke-static {v1}, Lorg/apache/lucene/search/BooleanScorer2;->access$000(Lorg/apache/lucene/search/BooleanScorer2;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->this$0:Lorg/apache/lucene/search/BooleanScorer2;

    # getter for: Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;
    invoke-static {v2}, Lorg/apache/lucene/search/BooleanScorer2;->access$100(Lorg/apache/lucene/search/BooleanScorer2;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [F

    iput-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->coordFactors:[F

    .line 47
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->coordFactors:[F

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 48
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->coordFactors:[F

    if-eqz p2, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_1
    aput v1, v2, v0

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->maxCoord:I

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/search/Similarity;->coord(II)F

    move-result v1

    goto :goto_1

    .line 50
    :cond_1
    return-void
.end method
