.class public Lorg/apache/lucene/search/payloads/MinPayloadFunction;
.super Lorg/apache/lucene/search/payloads/PayloadFunction;
.source "MinPayloadFunction.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lorg/apache/lucene/search/payloads/PayloadFunction;-><init>()V

    return-void
.end method


# virtual methods
.method public currentScore(ILjava/lang/String;IIIFF)F
    .locals 0
    .param p1, "docId"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "start"    # I
    .param p4, "end"    # I
    .param p5, "numPayloadsSeen"    # I
    .param p6, "currentScore"    # F
    .param p7, "currentPayloadScore"    # F

    .prologue
    .line 29
    if-nez p5, :cond_0

    .line 32
    .end local p7    # "currentPayloadScore":F
    :goto_0
    return p7

    .restart local p7    # "currentPayloadScore":F
    :cond_0
    invoke-static {p7, p6}, Ljava/lang/Math;->min(FF)F

    move-result p7

    goto :goto_0
.end method

.method public docScore(ILjava/lang/String;IF)F
    .locals 0
    .param p1, "docId"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "numPayloadsSeen"    # I
    .param p4, "payloadScore"    # F

    .prologue
    .line 38
    if-lez p3, :cond_0

    .end local p4    # "payloadScore":F
    :goto_0
    return p4

    .restart local p4    # "payloadScore":F
    :cond_0
    const/high16 p4, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 59
    if-ne p0, p1, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v0

    .line 61
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 62
    goto :goto_0

    .line 63
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 64
    goto :goto_0
.end method

.method public explain(IIF)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "doc"    # I
    .param p2, "numPayloadsSeen"    # I
    .param p3, "payloadScore"    # F

    .prologue
    .line 43
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v0}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 44
    .local v0, "expl":Lorg/apache/lucene/search/Explanation;
    if-lez p2, :cond_0

    move v1, p3

    .line 45
    .local v1, "minPayloadScore":F
    :goto_0
    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 46
    const-string/jumbo v2, "MinPayloadFunction(...)"

    invoke-virtual {v0, v2}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 47
    return-object v0

    .line 44
    .end local v1    # "minPayloadScore":F
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 51
    const/16 v0, 0x1f

    .line 52
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 53
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/lit8 v1, v2, 0x1f

    .line 54
    return v1
.end method
