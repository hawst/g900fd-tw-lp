.class public abstract Lorg/apache/lucene/search/DocIdSet;
.super Ljava/lang/Object;
.source "DocIdSet.java"


# static fields
.field public static final EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lorg/apache/lucene/search/DocIdSet$1;

    invoke-direct {v0}, Lorg/apache/lucene/search/DocIdSet$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isCacheable()Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method

.method public abstract iterator()Lorg/apache/lucene/search/DocIdSetIterator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
