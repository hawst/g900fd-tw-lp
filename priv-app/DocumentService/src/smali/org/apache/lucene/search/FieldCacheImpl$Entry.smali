.class Lorg/apache/lucene/search/FieldCacheImpl$Entry;
.super Ljava/lang/Object;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Entry"
.end annotation


# instance fields
.field final custom:Ljava/lang/Object;

.field final field:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "custom"    # Ljava/lang/Object;

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    invoke-static {p1}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->field:Ljava/lang/String;

    .line 280
    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->custom:Ljava/lang/Object;

    .line 281
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    .line 286
    instance-of v2, p1, Lorg/apache/lucene/search/FieldCacheImpl$Entry;

    if-eqz v2, :cond_2

    move-object v0, p1

    .line 287
    check-cast v0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;

    .line 288
    .local v0, "other":Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    iget-object v2, v0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->field:Ljava/lang/String;

    if-ne v2, v3, :cond_2

    .line 289
    iget-object v2, v0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->custom:Ljava/lang/Object;

    if-nez v2, :cond_1

    .line 290
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->custom:Ljava/lang/Object;

    if-nez v2, :cond_2

    .line 296
    .end local v0    # "other":Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    :cond_0
    :goto_0
    return v1

    .line 291
    .restart local v0    # "other":Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    :cond_1
    iget-object v2, v0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->custom:Ljava/lang/Object;

    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->custom:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 296
    .end local v0    # "other":Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 302
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->field:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->custom:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    xor-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->custom:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
