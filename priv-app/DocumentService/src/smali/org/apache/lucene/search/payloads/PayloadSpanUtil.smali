.class public Lorg/apache/lucene/search/payloads/PayloadSpanUtil;
.super Ljava/lang/Object;
.source "PayloadSpanUtil.java"


# instance fields
.field private reader:Lorg/apache/lucene/index/IndexReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;)V
    .locals 0
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->reader:Lorg/apache/lucene/index/IndexReader;

    .line 59
    return-void
.end method

.method private getPayloads(Ljava/util/Collection;Lorg/apache/lucene/search/spans/SpanQuery;)V
    .locals 6
    .param p2, "query"    # Lorg/apache/lucene/search/spans/SpanQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<[B>;",
            "Lorg/apache/lucene/search/spans/SpanQuery;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 173
    .local p1, "payloads":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {p2, v4}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v3

    .line 175
    .local v3, "spans":Lorg/apache/lucene/search/spans/Spans;
    :cond_0
    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 176
    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 177
    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v2

    .line 178
    .local v2, "payload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 179
    .local v0, "bytes":[B
    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 184
    .end local v0    # "bytes":[B
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "payload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    :cond_1
    return-void
.end method

.method private queryToSpanQuery(Lorg/apache/lucene/search/Query;Ljava/util/Collection;)V
    .locals 29
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Query;",
            "Ljava/util/Collection",
            "<[B>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    .local p2, "payloads":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/search/BooleanQuery;

    move/from16 v27, v0

    if-eqz v27, :cond_1

    .line 77
    check-cast p1, Lorg/apache/lucene/search/BooleanQuery;

    .end local p1    # "query":Lorg/apache/lucene/search/Query;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/BooleanQuery;->getClauses()[Lorg/apache/lucene/search/BooleanClause;

    move-result-object v20

    .line 79
    .local v20, "queryClauses":[Lorg/apache/lucene/search/BooleanClause;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    if-ge v8, v0, :cond_4

    .line 80
    aget-object v27, v20, v8

    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/search/BooleanClause;->isProhibited()Z

    move-result v27

    if-nez v27, :cond_0

    .line 81
    aget-object v27, v20, v8

    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/search/BooleanClause;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->queryToSpanQuery(Lorg/apache/lucene/search/Query;Ljava/util/Collection;)V

    .line 79
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 85
    .end local v8    # "i":I
    .end local v20    # "queryClauses":[Lorg/apache/lucene/search/BooleanClause;
    .restart local p1    # "query":Lorg/apache/lucene/search/Query;
    :cond_1
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/search/PhraseQuery;

    move/from16 v27, v0

    if-eqz v27, :cond_5

    move-object/from16 v27, p1

    .line 86
    check-cast v27, Lorg/apache/lucene/search/PhraseQuery;

    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/search/PhraseQuery;->getTerms()[Lorg/apache/lucene/index/Term;

    move-result-object v15

    .line 87
    .local v15, "phraseQueryTerms":[Lorg/apache/lucene/index/Term;
    array-length v0, v15

    move/from16 v27, v0

    move/from16 v0, v27

    new-array v4, v0, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 88
    .local v4, "clauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_1
    array-length v0, v15

    move/from16 v27, v0

    move/from16 v0, v27

    if-ge v8, v0, :cond_2

    .line 89
    new-instance v27, Lorg/apache/lucene/search/spans/SpanTermQuery;

    aget-object v28, v15, v8

    invoke-direct/range {v27 .. v28}, Lorg/apache/lucene/search/spans/SpanTermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    aput-object v27, v4, v8

    .line 88
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    move-object/from16 v27, p1

    .line 92
    check-cast v27, Lorg/apache/lucene/search/PhraseQuery;

    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/search/PhraseQuery;->getSlop()I

    move-result v21

    .line 93
    .local v21, "slop":I
    const/4 v10, 0x0

    .line 95
    .local v10, "inorder":Z
    if-nez v21, :cond_3

    .line 96
    const/4 v10, 0x1

    .line 99
    :cond_3
    new-instance v22, Lorg/apache/lucene/search/spans/SpanNearQuery;

    move-object/from16 v0, v22

    move/from16 v1, v21

    invoke-direct {v0, v4, v1, v10}, Lorg/apache/lucene/search/spans/SpanNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V

    .line 100
    .local v22, "sp":Lorg/apache/lucene/search/spans/SpanNearQuery;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/Query;->getBoost()F

    move-result v27

    move-object/from16 v0, v22

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->setBoost(F)V

    .line 101
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->getPayloads(Ljava/util/Collection;Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 169
    .end local v4    # "clauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    .end local v8    # "i":I
    .end local v10    # "inorder":Z
    .end local v15    # "phraseQueryTerms":[Lorg/apache/lucene/index/Term;
    .end local v21    # "slop":I
    .end local v22    # "sp":Lorg/apache/lucene/search/spans/SpanNearQuery;
    .end local p1    # "query":Lorg/apache/lucene/search/Query;
    :cond_4
    :goto_2
    return-void

    .line 102
    .restart local p1    # "query":Lorg/apache/lucene/search/Query;
    :cond_5
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/search/TermQuery;

    move/from16 v27, v0

    if-eqz v27, :cond_6

    .line 103
    new-instance v23, Lorg/apache/lucene/search/spans/SpanTermQuery;

    move-object/from16 v27, p1

    check-cast v27, Lorg/apache/lucene/search/TermQuery;

    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/search/TermQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/spans/SpanTermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 104
    .local v23, "stq":Lorg/apache/lucene/search/spans/SpanTermQuery;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/Query;->getBoost()F

    move-result v27

    move-object/from16 v0, v23

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/SpanTermQuery;->setBoost(F)V

    .line 105
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->getPayloads(Ljava/util/Collection;Lorg/apache/lucene/search/spans/SpanQuery;)V

    goto :goto_2

    .line 106
    .end local v23    # "stq":Lorg/apache/lucene/search/spans/SpanTermQuery;
    :cond_6
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/search/spans/SpanQuery;

    move/from16 v27, v0

    if-eqz v27, :cond_7

    .line 107
    check-cast p1, Lorg/apache/lucene/search/spans/SpanQuery;

    .end local p1    # "query":Lorg/apache/lucene/search/Query;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->getPayloads(Ljava/util/Collection;Lorg/apache/lucene/search/spans/SpanQuery;)V

    goto :goto_2

    .line 108
    .restart local p1    # "query":Lorg/apache/lucene/search/Query;
    :cond_7
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/search/FilteredQuery;

    move/from16 v27, v0

    if-eqz v27, :cond_8

    .line 109
    check-cast p1, Lorg/apache/lucene/search/FilteredQuery;

    .end local p1    # "query":Lorg/apache/lucene/search/Query;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/FilteredQuery;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->queryToSpanQuery(Lorg/apache/lucene/search/Query;Ljava/util/Collection;)V

    goto :goto_2

    .line 110
    .restart local p1    # "query":Lorg/apache/lucene/search/Query;
    :cond_8
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/search/DisjunctionMaxQuery;

    move/from16 v27, v0

    if-eqz v27, :cond_9

    .line 112
    check-cast p1, Lorg/apache/lucene/search/DisjunctionMaxQuery;

    .end local p1    # "query":Lorg/apache/lucene/search/Query;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/DisjunctionMaxQuery;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 113
    .local v11, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/Query;>;"
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_4

    .line 114
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lorg/apache/lucene/search/Query;

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->queryToSpanQuery(Lorg/apache/lucene/search/Query;Ljava/util/Collection;)V

    goto :goto_3

    .line 117
    .end local v11    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/Query;>;"
    .restart local p1    # "query":Lorg/apache/lucene/search/Query;
    :cond_9
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/lucene/search/MultiPhraseQuery;

    move/from16 v27, v0

    if-eqz v27, :cond_4

    move-object/from16 v14, p1

    .line 118
    check-cast v14, Lorg/apache/lucene/search/MultiPhraseQuery;

    .line 119
    .local v14, "mpq":Lorg/apache/lucene/search/MultiPhraseQuery;
    invoke-virtual {v14}, Lorg/apache/lucene/search/MultiPhraseQuery;->getTermArrays()Ljava/util/List;

    move-result-object v26

    .line 120
    .local v26, "termArrays":Ljava/util/List;, "Ljava/util/List<[Lorg/apache/lucene/index/Term;>;"
    invoke-virtual {v14}, Lorg/apache/lucene/search/MultiPhraseQuery;->getPositions()[I

    move-result-object v19

    .line 121
    .local v19, "positions":[I
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v27, v0

    if-lez v27, :cond_4

    .line 123
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v27, v0

    add-int/lit8 v27, v27, -0x1

    aget v13, v19, v27

    .line 124
    .local v13, "maxPosition":I
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_4
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v27, v0

    add-int/lit8 v27, v27, -0x1

    move/from16 v0, v27

    if-ge v8, v0, :cond_b

    .line 125
    aget v27, v19, v8

    move/from16 v0, v27

    if-le v0, v13, :cond_a

    .line 126
    aget v13, v19, v8

    .line 124
    :cond_a
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 130
    :cond_b
    add-int/lit8 v27, v13, 0x1

    move/from16 v0, v27

    new-array v5, v0, [Ljava/util/List;

    .line 132
    .local v5, "disjunctLists":[Ljava/util/List;, "[Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    const/4 v7, 0x0

    .line 134
    .local v7, "distinctPositions":I
    const/4 v8, 0x0

    :goto_5
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v27

    move/from16 v0, v27

    if-ge v8, v0, :cond_e

    .line 135
    move-object/from16 v0, v26

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, [Lorg/apache/lucene/index/Term;

    .line 136
    .local v25, "termArray":[Lorg/apache/lucene/index/Term;
    aget v27, v19, v8

    aget-object v6, v5, v27

    .line 137
    .local v6, "disjuncts":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    if-nez v6, :cond_c

    .line 138
    aget v27, v19, v8

    new-instance v6, Ljava/util/ArrayList;

    .end local v6    # "disjuncts":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    aput-object v6, v5, v27

    .line 140
    .restart local v6    # "disjuncts":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    add-int/lit8 v7, v7, 0x1

    .line 142
    :cond_c
    move-object/from16 v3, v25

    .local v3, "arr$":[Lorg/apache/lucene/index/Term;
    array-length v12, v3

    .local v12, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_6
    if-ge v9, v12, :cond_d

    aget-object v24, v3, v9

    .line 143
    .local v24, "term":Lorg/apache/lucene/index/Term;
    new-instance v27, Lorg/apache/lucene/search/spans/SpanTermQuery;

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/spans/SpanTermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    move-object/from16 v0, v27

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    add-int/lit8 v9, v9, 0x1

    goto :goto_6

    .line 134
    .end local v24    # "term":Lorg/apache/lucene/index/Term;
    :cond_d
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 147
    .end local v3    # "arr$":[Lorg/apache/lucene/index/Term;
    .end local v6    # "disjuncts":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    .end local v9    # "i$":I
    .end local v12    # "len$":I
    .end local v25    # "termArray":[Lorg/apache/lucene/index/Term;
    :cond_e
    const/16 v18, 0x0

    .line 148
    .local v18, "positionGaps":I
    const/16 v16, 0x0

    .line 149
    .local v16, "position":I
    new-array v4, v7, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 150
    .restart local v4    # "clauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    const/4 v8, 0x0

    :goto_7
    array-length v0, v5

    move/from16 v27, v0

    move/from16 v0, v27

    if-ge v8, v0, :cond_10

    .line 151
    aget-object v6, v5, v8

    .line 152
    .restart local v6    # "disjuncts":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    if-eqz v6, :cond_f

    .line 153
    add-int/lit8 v17, v16, 0x1

    .end local v16    # "position":I
    .local v17, "position":I
    new-instance v28, Lorg/apache/lucene/search/spans/SpanOrQuery;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v27

    move/from16 v0, v27

    new-array v0, v0, [Lorg/apache/lucene/search/spans/SpanQuery;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v27

    check-cast v27, [Lorg/apache/lucene/search/spans/SpanQuery;

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/spans/SpanOrQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;)V

    aput-object v28, v4, v16

    move/from16 v16, v17

    .line 150
    .end local v17    # "position":I
    .restart local v16    # "position":I
    :goto_8
    add-int/lit8 v8, v8, 0x1

    goto :goto_7

    .line 156
    :cond_f
    add-int/lit8 v18, v18, 0x1

    goto :goto_8

    .line 160
    .end local v6    # "disjuncts":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Query;>;"
    :cond_10
    invoke-virtual {v14}, Lorg/apache/lucene/search/MultiPhraseQuery;->getSlop()I

    move-result v21

    .line 161
    .restart local v21    # "slop":I
    if-nez v21, :cond_11

    const/4 v10, 0x1

    .line 163
    .restart local v10    # "inorder":Z
    :goto_9
    new-instance v22, Lorg/apache/lucene/search/spans/SpanNearQuery;

    add-int v27, v21, v18

    move-object/from16 v0, v22

    move/from16 v1, v27

    invoke-direct {v0, v4, v1, v10}, Lorg/apache/lucene/search/spans/SpanNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V

    .line 165
    .restart local v22    # "sp":Lorg/apache/lucene/search/spans/SpanNearQuery;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/Query;->getBoost()F

    move-result v27

    move-object/from16 v0, v22

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->setBoost(F)V

    .line 166
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->getPayloads(Ljava/util/Collection;Lorg/apache/lucene/search/spans/SpanQuery;)V

    goto/16 :goto_2

    .line 161
    .end local v10    # "inorder":Z
    .end local v22    # "sp":Lorg/apache/lucene/search/spans/SpanNearQuery;
    :cond_11
    const/4 v10, 0x0

    goto :goto_9
.end method


# virtual methods
.method public getPayloadsForQuery(Lorg/apache/lucene/search/Query;)Ljava/util/Collection;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Query;",
            ")",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v0, "payloads":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/payloads/PayloadSpanUtil;->queryToSpanQuery(Lorg/apache/lucene/search/Query;Ljava/util/Collection;)V

    .line 71
    return-object v0
.end method
