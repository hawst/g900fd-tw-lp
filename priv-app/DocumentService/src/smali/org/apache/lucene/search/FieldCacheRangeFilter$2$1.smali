.class Lorg/apache/lucene/search/FieldCacheRangeFilter$2$1;
.super Lorg/apache/lucene/search/FieldCacheDocIdSet;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter$2;->getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/FieldCacheRangeFilter$2;

.field final synthetic val$inclusiveLowerPoint:B

.field final synthetic val$inclusiveUpperPoint:B

.field final synthetic val$values:[B


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$2;Lorg/apache/lucene/index/IndexReader;[BBB)V
    .locals 0
    .param p2, "x0"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 173
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2$1;->this$0:Lorg/apache/lucene/search/FieldCacheRangeFilter$2;

    iput-object p3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2$1;->val$values:[B

    iput-byte p4, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2$1;->val$inclusiveLowerPoint:B

    iput-byte p5, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2$1;->val$inclusiveUpperPoint:B

    invoke-direct {p0, p2}, Lorg/apache/lucene/search/FieldCacheDocIdSet;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    return-void
.end method


# virtual methods
.method protected matchDoc(I)Z
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 174
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2$1;->val$values:[B

    aget-byte v0, v0, p1

    iget-byte v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2$1;->val$inclusiveLowerPoint:B

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2$1;->val$values:[B

    aget-byte v0, v0, p1

    iget-byte v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$2$1;->val$inclusiveUpperPoint:B

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
