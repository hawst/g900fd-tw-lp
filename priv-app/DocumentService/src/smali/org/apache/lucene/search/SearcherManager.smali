.class public final Lorg/apache/lucene/search/SearcherManager;
.super Lorg/apache/lucene/search/ReferenceManager;
.source "SearcherManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/ReferenceManager",
        "<",
        "Lorg/apache/lucene/search/IndexSearcher;",
        ">;"
    }
.end annotation


# instance fields
.field private final searcherFactory:Lorg/apache/lucene/search/SearcherFactory;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexWriter;ZLorg/apache/lucene/search/SearcherFactory;)V
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "applyAllDeletes"    # Z
    .param p3, "searcherFactory"    # Lorg/apache/lucene/search/SearcherFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;-><init>()V

    .line 78
    if-nez p3, :cond_0

    .line 79
    new-instance p3, Lorg/apache/lucene/search/SearcherFactory;

    .end local p3    # "searcherFactory":Lorg/apache/lucene/search/SearcherFactory;
    invoke-direct {p3}, Lorg/apache/lucene/search/SearcherFactory;-><init>()V

    .line 81
    .restart local p3    # "searcherFactory":Lorg/apache/lucene/search/SearcherFactory;
    :cond_0
    iput-object p3, p0, Lorg/apache/lucene/search/SearcherManager;->searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

    .line 82
    invoke-static {p1, p2}, Lorg/apache/lucene/index/IndexReader;->open(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-static {p3, v0}, Lorg/apache/lucene/search/SearcherManager;->getSearcher(Lorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/SearcherManager;->current:Ljava/lang/Object;

    .line 83
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/search/SearcherFactory;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "searcherFactory"    # Lorg/apache/lucene/search/SearcherFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;-><init>()V

    .line 95
    if-nez p2, :cond_0

    .line 96
    new-instance p2, Lorg/apache/lucene/search/SearcherFactory;

    .end local p2    # "searcherFactory":Lorg/apache/lucene/search/SearcherFactory;
    invoke-direct {p2}, Lorg/apache/lucene/search/SearcherFactory;-><init>()V

    .line 98
    .restart local p2    # "searcherFactory":Lorg/apache/lucene/search/SearcherFactory;
    :cond_0
    iput-object p2, p0, Lorg/apache/lucene/search/SearcherManager;->searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

    .line 99
    invoke-static {p1}, Lorg/apache/lucene/index/IndexReader;->open(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-static {p2, v0}, Lorg/apache/lucene/search/SearcherManager;->getSearcher(Lorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/SearcherManager;->current:Ljava/lang/Object;

    .line 100
    return-void
.end method

.method static getSearcher(Lorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;
    .locals 5
    .param p0, "searcherFactory"    # Lorg/apache/lucene/search/SearcherFactory;
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    const/4 v1, 0x0

    .line 147
    .local v1, "success":Z
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/SearcherFactory;->newSearcher(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v0

    .line 148
    .local v0, "searcher":Lorg/apache/lucene/search/IndexSearcher;
    invoke-virtual {v0}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v2

    if-eq v2, p1, :cond_1

    .line 149
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "SearcherFactory must wrap exactly the provided reader (got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " but expected "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    .end local v0    # "searcher":Lorg/apache/lucene/search/IndexSearcher;
    :catchall_0
    move-exception v2

    if-nez v1, :cond_0

    .line 154
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 153
    :cond_0
    throw v2

    .line 151
    .restart local v0    # "searcher":Lorg/apache/lucene/search/IndexSearcher;
    :cond_1
    const/4 v1, 0x1

    .line 153
    if-nez v1, :cond_2

    .line 154
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 157
    :cond_2
    return-object v0
.end method


# virtual methods
.method protected bridge synthetic decRef(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    check-cast p1, Lorg/apache/lucene/search/IndexSearcher;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/SearcherManager;->decRef(Lorg/apache/lucene/search/IndexSearcher;)V

    return-void
.end method

.method protected decRef(Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "reference"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 105
    return-void
.end method

.method public isSearcherCurrent()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    invoke-virtual {p0}, Lorg/apache/lucene/search/SearcherManager;->acquire()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/IndexSearcher;

    .line 136
    .local v0, "searcher":Lorg/apache/lucene/search/IndexSearcher;
    :try_start_0
    invoke-virtual {v0}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->isCurrent()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 138
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/SearcherManager;->release(Ljava/lang/Object;)V

    .line 136
    return v1

    .line 138
    :catchall_0
    move-exception v1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/SearcherManager;->release(Ljava/lang/Object;)V

    throw v1
.end method

.method public maybeReopen()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 125
    invoke-virtual {p0}, Lorg/apache/lucene/search/SearcherManager;->maybeRefresh()Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic refreshIfNeeded(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    check-cast p1, Lorg/apache/lucene/search/IndexSearcher;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/SearcherManager;->refreshIfNeeded(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v0

    return-object v0
.end method

.method protected refreshIfNeeded(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/IndexSearcher;
    .locals 2
    .param p1, "referenceToRefresh"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/lucene/index/IndexReader;->openIfChanged(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 110
    .local v0, "newReader":Lorg/apache/lucene/index/IndexReader;
    if-nez v0, :cond_0

    .line 111
    const/4 v1, 0x0

    .line 113
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/SearcherManager;->searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

    invoke-static {v1, v0}, Lorg/apache/lucene/search/SearcherManager;->getSearcher(Lorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v1

    goto :goto_0
.end method

.method protected bridge synthetic tryIncRef(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 57
    check-cast p1, Lorg/apache/lucene/search/IndexSearcher;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/SearcherManager;->tryIncRef(Lorg/apache/lucene/search/IndexSearcher;)Z

    move-result v0

    return v0
.end method

.method protected tryIncRef(Lorg/apache/lucene/search/IndexSearcher;)Z
    .locals 1
    .param p1, "reference"    # Lorg/apache/lucene/search/IndexSearcher;

    .prologue
    .line 119
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->tryIncRef()Z

    move-result v0

    return v0
.end method
