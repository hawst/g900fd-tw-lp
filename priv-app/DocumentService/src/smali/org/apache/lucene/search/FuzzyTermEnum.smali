.class public final Lorg/apache/lucene/search/FuzzyTermEnum;
.super Lorg/apache/lucene/search/FilteredTermEnum;
.source "FuzzyTermEnum.java"


# instance fields
.field private d:[I

.field private endEnum:Z

.field private final field:Ljava/lang/String;

.field private final minimumSimilarity:F

.field private p:[I

.field private final prefix:Ljava/lang/String;

.field private final scale_factor:F

.field private searchTerm:Lorg/apache/lucene/index/Term;

.field private similarity:F

.field private final text:[C


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/Term;)V
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    const/high16 v0, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lorg/apache/lucene/search/FuzzyTermEnum;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/Term;FI)V

    .line 63
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/Term;F)V
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .param p3, "minSimilarity"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/search/FuzzyTermEnum;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/Term;FI)V

    .line 79
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/Term;FI)V
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .param p3, "minSimilarity"    # F
    .param p4, "prefixLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 96
    invoke-direct {p0}, Lorg/apache/lucene/search/FilteredTermEnum;-><init>()V

    .line 40
    iput-boolean v4, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->endEnum:Z

    .line 42
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->searchTerm:Lorg/apache/lucene/index/Term;

    .line 98
    cmpl-float v2, p3, v3

    if-ltz v2, :cond_0

    .line 99
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "minimumSimilarity cannot be greater than or equal to 1"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 100
    :cond_0
    const/4 v2, 0x0

    cmpg-float v2, p3, v2

    if-gez v2, :cond_1

    .line 101
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "minimumSimilarity cannot be less than 0"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 102
    :cond_1
    if-gez p4, :cond_2

    .line 103
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "prefixLength cannot be less than 0"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 105
    :cond_2
    iput p3, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->minimumSimilarity:F

    .line 106
    iget v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->minimumSimilarity:F

    sub-float v2, v3, v2

    div-float v2, v3, v2

    iput v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->scale_factor:F

    .line 107
    iput-object p2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->searchTerm:Lorg/apache/lucene/index/Term;

    .line 108
    iget-object v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->searchTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->field:Ljava/lang/String;

    .line 112
    iget-object v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->searchTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .line 113
    .local v0, "fullSearchTermLength":I
    if-le p4, v0, :cond_3

    move v1, v0

    .line 115
    .local v1, "realPrefixLength":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->searchTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->text:[C

    .line 116
    iget-object v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->searchTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->prefix:Ljava/lang/String;

    .line 118
    iget-object v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->text:[C

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [I

    iput-object v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->p:[I

    .line 119
    iget-object v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->text:[C

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [I

    iput-object v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->d:[I

    .line 121
    new-instance v2, Lorg/apache/lucene/index/Term;

    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->searchTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->prefix:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/search/FuzzyTermEnum;->setEnum(Lorg/apache/lucene/index/TermEnum;)V

    .line 122
    return-void

    .end local v1    # "realPrefixLength":I
    :cond_3
    move v1, p4

    .line 113
    goto :goto_0
.end method

.method private calculateMaxDistance(I)I
    .locals 3
    .param p1, "m"    # I

    .prologue
    .line 273
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->minimumSimilarity:F

    sub-float/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->text:[C

    array-length v1, v1

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->prefix:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private similarity(Ljava/lang/String;)F
    .locals 14
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    .line 193
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 194
    .local v4, "m":I
    iget-object v9, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->text:[C

    array-length v6, v9

    .line 195
    .local v6, "n":I
    if-nez v6, :cond_2

    .line 198
    iget-object v9, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->prefix:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_1

    .line 262
    :cond_0
    :goto_0
    return v8

    .line 198
    :cond_1
    int-to-float v8, v4

    iget-object v9, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->prefix:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    sub-float v8, v13, v8

    goto :goto_0

    .line 200
    :cond_2
    if-nez v4, :cond_3

    .line 201
    iget-object v9, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->prefix:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_0

    int-to-float v8, v6

    iget-object v9, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->prefix:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    sub-float v8, v13, v8

    goto :goto_0

    .line 204
    :cond_3
    invoke-direct {p0, v4}, Lorg/apache/lucene/search/FuzzyTermEnum;->calculateMaxDistance(I)I

    move-result v5

    .line 206
    .local v5, "maxDistance":I
    sub-int v9, v4, v6

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v9

    if-lt v5, v9, :cond_0

    .line 218
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-gt v2, v6, :cond_4

    .line 219
    iget-object v9, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->p:[I

    aput v2, v9, v2

    .line 218
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 223
    :cond_4
    const/4 v3, 0x1

    .local v3, "j":I
    :goto_2
    if-gt v3, v4, :cond_8

    .line 224
    move v1, v4

    .line 225
    .local v1, "bestPossibleEditDistance":I
    add-int/lit8 v9, v3, -0x1

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 226
    .local v7, "t_j":C
    iget-object v9, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->d:[I

    const/4 v10, 0x0

    aput v3, v9, v10

    .line 228
    const/4 v2, 0x1

    :goto_3
    if-gt v2, v6, :cond_6

    .line 230
    iget-object v9, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->text:[C

    add-int/lit8 v10, v2, -0x1

    aget-char v9, v9, v10

    if-eq v7, v9, :cond_5

    .line 231
    iget-object v9, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->d:[I

    iget-object v10, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->d:[I

    add-int/lit8 v11, v2, -0x1

    aget v10, v10, v11

    iget-object v11, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->p:[I

    aget v11, v11, v2

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v10

    iget-object v11, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->p:[I

    add-int/lit8 v12, v2, -0x1

    aget v11, v11, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    aput v10, v9, v2

    .line 235
    :goto_4
    iget-object v9, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->d:[I

    aget v9, v9, v2

    invoke-static {v1, v9}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 228
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 233
    :cond_5
    iget-object v9, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->d:[I

    iget-object v10, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->d:[I

    add-int/lit8 v11, v2, -0x1

    aget v10, v10, v11

    add-int/lit8 v10, v10, 0x1

    iget-object v11, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->p:[I

    aget v11, v11, v2

    add-int/lit8 v11, v11, 0x1

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v10

    iget-object v11, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->p:[I

    add-int/lit8 v12, v2, -0x1

    aget v11, v11, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v10

    aput v10, v9, v2

    goto :goto_4

    .line 242
    :cond_6
    if-le v3, v5, :cond_7

    if-gt v1, v5, :cond_0

    .line 249
    :cond_7
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->p:[I

    .line 250
    .local v0, "_d":[I
    iget-object v9, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->d:[I

    iput-object v9, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->p:[I

    .line 251
    iput-object v0, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->d:[I

    .line 223
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 262
    .end local v0    # "_d":[I
    .end local v1    # "bestPossibleEditDistance":I
    .end local v7    # "t_j":C
    :cond_8
    iget-object v8, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->p:[I

    aget v8, v8, v6

    int-to-float v8, v8

    iget-object v9, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->prefix:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    move-result v10

    add-int/2addr v9, v10

    int-to-float v9, v9

    div-float/2addr v8, v9

    sub-float v8, v13, v8

    goto/16 :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 279
    iput-object v0, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->d:[I

    iput-object v0, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->p:[I

    .line 280
    iput-object v0, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->searchTerm:Lorg/apache/lucene/index/Term;

    .line 281
    invoke-super {p0}, Lorg/apache/lucene/search/FilteredTermEnum;->close()V

    .line 282
    return-void
.end method

.method public final difference()F
    .locals 2

    .prologue
    .line 142
    iget v0, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->similarity:F

    iget v1, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->minimumSimilarity:F

    sub-float/2addr v0, v1

    iget v1, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->scale_factor:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public final endEnum()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->endEnum:Z

    return v0
.end method

.method protected final termCompare(Lorg/apache/lucene/index/Term;)Z
    .locals 5
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->field:Ljava/lang/String;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v4

    if-ne v3, v4, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->prefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 131
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->prefix:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "target":Ljava/lang/String;
    invoke-direct {p0, v0}, Lorg/apache/lucene/search/FuzzyTermEnum;->similarity(Ljava/lang/String;)F

    move-result v3

    iput v3, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->similarity:F

    .line 133
    iget v3, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->similarity:F

    iget v4, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->minimumSimilarity:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 136
    .end local v0    # "target":Ljava/lang/String;
    :goto_0
    return v1

    .restart local v0    # "target":Ljava/lang/String;
    :cond_0
    move v1, v2

    .line 133
    goto :goto_0

    .line 135
    .end local v0    # "target":Ljava/lang/String;
    :cond_1
    iput-boolean v1, p0, Lorg/apache/lucene/search/FuzzyTermEnum;->endEnum:Z

    move v1, v2

    .line 136
    goto :goto_0
.end method
