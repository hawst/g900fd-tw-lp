.class public Lorg/apache/lucene/search/TimeLimitingCollector;
.super Lorg/apache/lucene/search/Collector;
.source "TimeLimitingCollector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/TimeLimitingCollector$1;,
        Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;,
        Lorg/apache/lucene/search/TimeLimitingCollector$TimerThreadHolder;,
        Lorg/apache/lucene/search/TimeLimitingCollector$TimeExceededException;
    }
.end annotation


# instance fields
.field private final clock:Lorg/apache/lucene/util/Counter;

.field private collector:Lorg/apache/lucene/search/Collector;

.field private docBase:I

.field private greedy:Z

.field private t0:J

.field private final ticksAllowed:J

.field private timeout:J


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Collector;Lorg/apache/lucene/util/Counter;J)V
    .locals 3
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "clock"    # Lorg/apache/lucene/util/Counter;
    .param p3, "ticksAllowed"    # J

    .prologue
    const-wide/high16 v0, -0x8000000000000000L

    .line 76
    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    .line 61
    iput-wide v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->t0:J

    .line 62
    iput-wide v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->timeout:J

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->greedy:Z

    .line 77
    iput-object p1, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->collector:Lorg/apache/lucene/search/Collector;

    .line 78
    iput-object p2, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->clock:Lorg/apache/lucene/util/Counter;

    .line 79
    iput-wide p3, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->ticksAllowed:J

    .line 80
    return-void
.end method

.method public static getGlobalCounter()Lorg/apache/lucene/util/Counter;
    .locals 1

    .prologue
    .line 200
    sget-object v0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThreadHolder;->THREAD:Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;

    iget-object v0, v0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;->counter:Lorg/apache/lucene/util/Counter;

    return-object v0
.end method

.method public static getGlobalTimerThread()Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;
    .locals 1

    .prologue
    .line 216
    sget-object v0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThreadHolder;->THREAD:Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;

    return-object v0
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->collector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Collector;->acceptsDocsOutOfOrder()Z

    move-result v0

    return v0
.end method

.method public collect(I)V
    .locals 10
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->clock:Lorg/apache/lucene/util/Counter;

    invoke-virtual {v0}, Lorg/apache/lucene/util/Counter;->get()J

    move-result-wide v8

    .line 145
    .local v8, "time":J
    iget-wide v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->timeout:J

    cmp-long v0, v0, v8

    if-gez v0, :cond_1

    .line 146
    iget-boolean v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->greedy:Z

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->collector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 151
    :cond_0
    new-instance v1, Lorg/apache/lucene/search/TimeLimitingCollector$TimeExceededException;

    iget-wide v2, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->timeout:J

    iget-wide v4, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->t0:J

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->t0:J

    sub-long v4, v8, v4

    iget v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->docBase:I

    add-int v6, v0, p1

    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lorg/apache/lucene/search/TimeLimitingCollector$TimeExceededException;-><init>(JJILorg/apache/lucene/search/TimeLimitingCollector$1;)V

    throw v1

    .line 154
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->collector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 155
    return-void
.end method

.method public isGreedy()Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->greedy:Z

    return v0
.end method

.method public setBaseline()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->clock:Lorg/apache/lucene/util/Counter;

    invoke-virtual {v0}, Lorg/apache/lucene/util/Counter;->get()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/search/TimeLimitingCollector;->setBaseline(J)V

    .line 112
    return-void
.end method

.method public setBaseline(J)V
    .locals 5
    .param p1, "clockTime"    # J

    .prologue
    .line 102
    iput-wide p1, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->t0:J

    .line 103
    iget-wide v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->t0:J

    iget-wide v2, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->ticksAllowed:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->timeout:J

    .line 104
    return-void
.end method

.method public setCollector(Lorg/apache/lucene/search/Collector;)V
    .locals 0
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;

    .prologue
    .line 184
    iput-object p1, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->collector:Lorg/apache/lucene/search/Collector;

    .line 185
    return-void
.end method

.method public setGreedy(Z)V
    .locals 0
    .param p1, "greedy"    # Z

    .prologue
    .line 132
    iput-boolean p1, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->greedy:Z

    .line 133
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "base"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->collector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/Collector;->setNextReader(Lorg/apache/lucene/index/IndexReader;I)V

    .line 160
    iput p2, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->docBase:I

    .line 161
    const-wide/high16 v0, -0x8000000000000000L

    iget-wide v2, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->t0:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 162
    invoke-virtual {p0}, Lorg/apache/lucene/search/TimeLimitingCollector;->setBaseline()V

    .line 164
    :cond_0
    return-void
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 168
    iget-object v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector;->collector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 169
    return-void
.end method
