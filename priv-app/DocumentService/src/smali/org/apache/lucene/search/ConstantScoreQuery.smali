.class public Lorg/apache/lucene/search/ConstantScoreQuery;
.super Lorg/apache/lucene/search/Query;
.source "ConstantScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;,
        Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;
    }
.end annotation


# instance fields
.field protected final filter:Lorg/apache/lucene/search/Filter;

.field protected final query:Lorg/apache/lucene/search/Query;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Filter;)V
    .locals 2
    .param p1, "filter"    # Lorg/apache/lucene/search/Filter;

    .prologue
    .line 57
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 58
    if-nez p1, :cond_0

    .line 59
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Filter may not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->filter:Lorg/apache/lucene/search/Filter;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    .line 62
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/Query;)V
    .locals 2
    .param p1, "query"    # Lorg/apache/lucene/search/Query;

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 45
    if-nez p1, :cond_0

    .line 46
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Query may not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->filter:Lorg/apache/lucene/search/Filter;

    .line 48
    iput-object p1, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    .line 49
    return-void
.end method


# virtual methods
.method public createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 260
    new-instance v0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantWeight;-><init>(Lorg/apache/lucene/search/ConstantScoreQuery;Lorg/apache/lucene/search/Searcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 274
    if-ne p0, p1, :cond_1

    .line 283
    :cond_0
    :goto_0
    return v1

    .line 275
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 276
    goto :goto_0

    .line 277
    :cond_2
    instance-of v3, p1, Lorg/apache/lucene/search/ConstantScoreQuery;

    if-eqz v3, :cond_6

    move-object v0, p1

    .line 278
    check-cast v0, Lorg/apache/lucene/search/ConstantScoreQuery;

    .line 279
    .local v0, "other":Lorg/apache/lucene/search/ConstantScoreQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->filter:Lorg/apache/lucene/search/Filter;

    if-nez v3, :cond_4

    iget-object v3, v0, Lorg/apache/lucene/search/ConstantScoreQuery;->filter:Lorg/apache/lucene/search/Filter;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    if-nez v3, :cond_5

    iget-object v3, v0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->filter:Lorg/apache/lucene/search/Filter;

    iget-object v4, v0, Lorg/apache/lucene/search/ConstantScoreQuery;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    iget-object v4, v0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    .end local v0    # "other":Lorg/apache/lucene/search/ConstantScoreQuery;
    :cond_6
    move v1, v2

    .line 283
    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Query;->extractTerms(Ljava/util/Set;)V

    .line 95
    :cond_0
    return-void
.end method

.method public getFilter()Lorg/apache/lucene/search/Filter;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->filter:Lorg/apache/lucene/search/Filter;

    return-object v0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 288
    invoke-super {p0}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v0

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->filter:Lorg/apache/lucene/search/Filter;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    goto :goto_0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    if-eqz v2, :cond_0

    .line 77
    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 78
    .local v0, "rewritten":Lorg/apache/lucene/search/Query;
    iget-object v2, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    if-eq v0, v2, :cond_0

    .line 79
    new-instance v1, Lorg/apache/lucene/search/ConstantScoreQuery;

    invoke-direct {v1, v0}, Lorg/apache/lucene/search/ConstantScoreQuery;-><init>(Lorg/apache/lucene/search/Query;)V

    .line 80
    .end local v0    # "rewritten":Lorg/apache/lucene/search/Query;
    .local v1, "rewritten":Lorg/apache/lucene/search/Query;
    invoke-virtual {p0}, Lorg/apache/lucene/search/ConstantScoreQuery;->getBoost()F

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 84
    .end local v1    # "rewritten":Lorg/apache/lucene/search/Query;
    :goto_0
    return-object v1

    :cond_0
    move-object v1, p0

    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 265
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v0, "ConstantScore("

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/ConstantScoreQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
