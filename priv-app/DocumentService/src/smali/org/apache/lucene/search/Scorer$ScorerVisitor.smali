.class public abstract Lorg/apache/lucene/search/Scorer$ScorerVisitor;
.super Ljava/lang/Object;
.source "Scorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/Scorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ScorerVisitor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Lorg/apache/lucene/search/Query;",
        "C:",
        "Lorg/apache/lucene/search/Query;",
        "S:",
        "Lorg/apache/lucene/search/Scorer;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 148
    .local p0, "this":Lorg/apache/lucene/search/Scorer$ScorerVisitor;, "Lorg/apache/lucene/search/Scorer$ScorerVisitor<TP;TC;TS;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visitOptional(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;TC;TS;)V"
        }
    .end annotation

    .prologue
    .line 156
    .local p0, "this":Lorg/apache/lucene/search/Scorer$ScorerVisitor;, "Lorg/apache/lucene/search/Scorer$ScorerVisitor<TP;TC;TS;>;"
    .local p1, "parent":Lorg/apache/lucene/search/Query;, "TP;"
    .local p2, "child":Lorg/apache/lucene/search/Query;, "TC;"
    .local p3, "scorer":Lorg/apache/lucene/search/Scorer;, "TS;"
    return-void
.end method

.method public visitProhibited(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;TC;TS;)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p0, "this":Lorg/apache/lucene/search/Scorer$ScorerVisitor;, "Lorg/apache/lucene/search/Scorer$ScorerVisitor<TP;TC;TS;>;"
    .local p1, "parent":Lorg/apache/lucene/search/Query;, "TP;"
    .local p2, "child":Lorg/apache/lucene/search/Query;, "TC;"
    .local p3, "scorer":Lorg/apache/lucene/search/Scorer;, "TS;"
    return-void
.end method

.method public visitRequired(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;TC;TS;)V"
        }
    .end annotation

    .prologue
    .line 165
    .local p0, "this":Lorg/apache/lucene/search/Scorer$ScorerVisitor;, "Lorg/apache/lucene/search/Scorer$ScorerVisitor<TP;TC;TS;>;"
    .local p1, "parent":Lorg/apache/lucene/search/Query;, "TP;"
    .local p2, "child":Lorg/apache/lucene/search/Query;, "TC;"
    .local p3, "scorer":Lorg/apache/lucene/search/Scorer;, "TS;"
    return-void
.end method
