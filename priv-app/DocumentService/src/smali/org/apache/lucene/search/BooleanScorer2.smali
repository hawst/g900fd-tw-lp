.class Lorg/apache/lucene/search/BooleanScorer2;
.super Lorg/apache/lucene/search/Scorer;
.source "BooleanScorer2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;,
        Lorg/apache/lucene/search/BooleanScorer2$Coordinator;
    }
.end annotation


# instance fields
.field private final coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

.field private final countingSumScorer:Lorg/apache/lucene/search/Scorer;

.field private doc:I

.field private final minNrShouldMatch:I

.field private final optionalScorers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;"
        }
    .end annotation
.end field

.field private final prohibitedScorers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;"
        }
    .end annotation
.end field

.field private final requiredScorers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Weight;ZLorg/apache/lucene/search/Similarity;ILjava/util/List;Ljava/util/List;Ljava/util/List;I)V
    .locals 2
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "disableCoord"    # Z
    .param p3, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p4, "minNrShouldMatch"    # I
    .param p8, "maxCoord"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Weight;",
            "Z",
            "Lorg/apache/lucene/search/Similarity;",
            "I",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    .local p5, "required":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    .local p6, "prohibited":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    .local p7, "optional":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    .line 89
    if-gez p4, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Minimum number of optional scorers should not be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;-><init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/BooleanScorer2$1;)V

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    .line 93
    iput p4, p0, Lorg/apache/lucene/search/BooleanScorer2;->minNrShouldMatch:I

    .line 94
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    iput p8, v0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->maxCoord:I

    .line 96
    iput-object p7, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    .line 97
    iput-object p5, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    .line 98
    iput-object p6, p0, Lorg/apache/lucene/search/BooleanScorer2;->prohibitedScorers:Ljava/util/List;

    .line 100
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    invoke-virtual {v0, p3, p2}, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->init(Lorg/apache/lucene/search/Similarity;Z)V

    .line 101
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/search/BooleanScorer2;->makeCountingSumScorer(ZLorg/apache/lucene/search/Similarity;)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->countingSumScorer:Lorg/apache/lucene/search/Scorer;

    .line 102
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/search/BooleanScorer2;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/BooleanScorer2;

    .prologue
    .line 34
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lorg/apache/lucene/search/BooleanScorer2;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/BooleanScorer2;

    .prologue
    .line 34
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lorg/apache/lucene/search/BooleanScorer2;)Lorg/apache/lucene/search/BooleanScorer2$Coordinator;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/BooleanScorer2;

    .prologue
    .line 34
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    return-object v0
.end method

.method private addProhibitedScorers(Lorg/apache/lucene/search/Scorer;)Lorg/apache/lucene/search/Scorer;
    .locals 4
    .param p1, "requiredCountingSumScorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 266
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->prohibitedScorers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .end local p1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :goto_0
    return-object p1

    .restart local p1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :cond_0
    new-instance v1, Lorg/apache/lucene/search/ReqExclScorer;

    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->prohibitedScorers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->prohibitedScorers:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/Scorer;

    :goto_1
    invoke-direct {v1, p1, v0}, Lorg/apache/lucene/search/ReqExclScorer;-><init>(Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/DocIdSetIterator;)V

    move-object p1, v1

    goto :goto_0

    :cond_1
    new-instance v0, Lorg/apache/lucene/search/DisjunctionSumScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->weight:Lorg/apache/lucene/search/Weight;

    iget-object v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->prohibitedScorers:Ljava/util/List;

    invoke-direct {v0, v2, v3}, Lorg/apache/lucene/search/DisjunctionSumScorer;-><init>(Lorg/apache/lucene/search/Weight;Ljava/util/List;)V

    goto :goto_1
.end method

.method private countingConjunctionSumScorer(ZLorg/apache/lucene/search/Similarity;Ljava/util/List;)Lorg/apache/lucene/search/Scorer;
    .locals 6
    .param p1, "disableCoord"    # Z
    .param p2, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lorg/apache/lucene/search/Similarity;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;)",
            "Lorg/apache/lucene/search/Scorer;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    .local p3, "requiredScorers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v5

    .line 173
    .local v5, "requiredNrMatchers":I
    new-instance v0, Lorg/apache/lucene/search/BooleanScorer2$2;

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->weight:Lorg/apache/lucene/search/Weight;

    if-eqz p1, :cond_0

    const/high16 v3, 0x3f800000    # 1.0f

    :goto_0
    move-object v1, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/BooleanScorer2$2;-><init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Weight;FLjava/util/Collection;I)V

    return-object v0

    :cond_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {p2, v1, v3}, Lorg/apache/lucene/search/Similarity;->coord(II)F

    move-result v3

    goto :goto_0
.end method

.method private countingDisjunctionSumScorer(Ljava/util/List;I)Lorg/apache/lucene/search/Scorer;
    .locals 2
    .param p2, "minNrShouldMatch"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;I)",
            "Lorg/apache/lucene/search/Scorer;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    .local p1, "scorers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    new-instance v0, Lorg/apache/lucene/search/BooleanScorer2$1;

    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2;->weight:Lorg/apache/lucene/search/Weight;

    invoke-direct {v0, p0, v1, p1, p2}, Lorg/apache/lucene/search/BooleanScorer2$1;-><init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Weight;Ljava/util/List;I)V

    return-object v0
.end method

.method private dualConjunctionSumScorer(ZLorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/Scorer;)Lorg/apache/lucene/search/Scorer;
    .locals 5
    .param p1, "disableCoord"    # Z
    .param p2, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p3, "req1"    # Lorg/apache/lucene/search/Scorer;
    .param p4, "req2"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 199
    new-instance v1, Lorg/apache/lucene/search/ConjunctionScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->weight:Lorg/apache/lucene/search/Weight;

    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    new-array v3, v3, [Lorg/apache/lucene/search/Scorer;

    const/4 v4, 0x0

    aput-object p3, v3, v4

    const/4 v4, 0x1

    aput-object p4, v3, v4

    invoke-direct {v1, v2, v0, v3}, Lorg/apache/lucene/search/ConjunctionScorer;-><init>(Lorg/apache/lucene/search/Weight;F[Lorg/apache/lucene/search/Scorer;)V

    return-object v1

    :cond_0
    invoke-virtual {p2, v3, v3}, Lorg/apache/lucene/search/Similarity;->coord(II)F

    move-result v0

    goto :goto_0
.end method

.method private makeCountingSumScorer(ZLorg/apache/lucene/search/Similarity;)Lorg/apache/lucene/search/Scorer;
    .locals 1
    .param p1, "disableCoord"    # Z
    .param p2, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/BooleanScorer2;->makeCountingSumScorerNoReq(ZLorg/apache/lucene/search/Similarity;)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/BooleanScorer2;->makeCountingSumScorerSomeReq(ZLorg/apache/lucene/search/Similarity;)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    goto :goto_0
.end method

.method private makeCountingSumScorerNoReq(ZLorg/apache/lucene/search/Similarity;)Lorg/apache/lucene/search/Scorer;
    .locals 4
    .param p1, "disableCoord"    # Z
    .param p2, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 218
    iget v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->minNrShouldMatch:I

    if-ge v3, v2, :cond_0

    move v0, v2

    .line 220
    .local v0, "nrOptRequired":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v0, :cond_1

    .line 221
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-direct {p0, v2, v0}, Lorg/apache/lucene/search/BooleanScorer2;->countingDisjunctionSumScorer(Ljava/util/List;I)Lorg/apache/lucene/search/Scorer;

    move-result-object v1

    .line 227
    .local v1, "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :goto_1
    invoke-direct {p0, v1}, Lorg/apache/lucene/search/BooleanScorer2;->addProhibitedScorers(Lorg/apache/lucene/search/Scorer;)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    return-object v2

    .line 218
    .end local v0    # "nrOptRequired":I
    .end local v1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->minNrShouldMatch:I

    goto :goto_0

    .line 222
    .restart local v0    # "nrOptRequired":I
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v2, :cond_2

    .line 223
    new-instance v1, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/Scorer;

    invoke-direct {v1, p0, v2}, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;-><init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Scorer;)V

    .restart local v1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    goto :goto_1

    .line 225
    .end local v1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-direct {p0, p1, p2, v2}, Lorg/apache/lucene/search/BooleanScorer2;->countingConjunctionSumScorer(ZLorg/apache/lucene/search/Similarity;Ljava/util/List;)Lorg/apache/lucene/search/Scorer;

    move-result-object v1

    .restart local v1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    goto :goto_1
.end method

.method private makeCountingSumScorerSomeReq(ZLorg/apache/lucene/search/Similarity;)Lorg/apache/lucene/search/Scorer;
    .locals 8
    .param p1, "disableCoord"    # Z
    .param p2, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 231
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->minNrShouldMatch:I

    if-ne v2, v3, :cond_0

    .line 232
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 233
    .local v0, "allReq":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/Scorer;>;"
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 234
    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/BooleanScorer2;->countingConjunctionSumScorer(ZLorg/apache/lucene/search/Similarity;Ljava/util/List;)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/search/BooleanScorer2;->addProhibitedScorers(Lorg/apache/lucene/search/Scorer;)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    .line 250
    .end local v0    # "allReq":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/Scorer;>;"
    :goto_0
    return-object v2

    .line 236
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v6, :cond_1

    new-instance v1, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/Scorer;

    invoke-direct {v1, p0, v2}, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;-><init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Scorer;)V

    .line 240
    .local v1, "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :goto_1
    iget v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->minNrShouldMatch:I

    if-lez v2, :cond_2

    .line 241
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    iget v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->minNrShouldMatch:I

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/search/BooleanScorer2;->countingDisjunctionSumScorer(Ljava/util/List;I)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/lucene/search/BooleanScorer2;->dualConjunctionSumScorer(ZLorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/Scorer;)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/search/BooleanScorer2;->addProhibitedScorers(Lorg/apache/lucene/search/Scorer;)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    goto :goto_0

    .line 236
    .end local v1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    invoke-direct {p0, p1, p2, v2}, Lorg/apache/lucene/search/BooleanScorer2;->countingConjunctionSumScorer(ZLorg/apache/lucene/search/Similarity;Ljava/util/List;)Lorg/apache/lucene/search/Scorer;

    move-result-object v1

    goto :goto_1

    .line 250
    .restart local v1    # "requiredCountingSumScorer":Lorg/apache/lucene/search/Scorer;
    :cond_2
    new-instance v4, Lorg/apache/lucene/search/ReqOptSumScorer;

    invoke-direct {p0, v1}, Lorg/apache/lucene/search/BooleanScorer2;->addProhibitedScorers(Lorg/apache/lucene/search/Scorer;)Lorg/apache/lucene/search/Scorer;

    move-result-object v5

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v6, :cond_3

    new-instance v3, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/Scorer;

    invoke-direct {v3, p0, v2}, Lorg/apache/lucene/search/BooleanScorer2$SingleMatchScorer;-><init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Scorer;)V

    move-object v2, v3

    :goto_2
    invoke-direct {v4, v5, v2}, Lorg/apache/lucene/search/ReqOptSumScorer;-><init>(Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/Scorer;)V

    move-object v2, v4

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-direct {p0, v2, v6}, Lorg/apache/lucene/search/BooleanScorer2;->countingDisjunctionSumScorer(Ljava/util/List;I)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    goto :goto_2
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 320
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->countingSumScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    return v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 298
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    return v0
.end method

.method public freq()F
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    iget v0, v0, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    int-to-float v0, v0

    return v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 303
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->countingSumScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    return v0
.end method

.method public score()F
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 308
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    const/4 v2, 0x0

    iput v2, v1, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    .line 309
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2;->countingSumScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    .line 310
    .local v0, "sum":F
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    iget-object v1, v1, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->coordFactors:[F

    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    iget v2, v2, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    aget v1, v1, v2

    mul-float/2addr v1, v0

    return v1
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 279
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 280
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->countingSumScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    .line 281
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Collector;->collect(I)V

    goto :goto_0

    .line 283
    :cond_0
    return-void
.end method

.method protected score(Lorg/apache/lucene/search/Collector;II)Z
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "max"    # I
    .param p3, "firstDocID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 287
    iput p3, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    .line 288
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 289
    :goto_0
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    if-ge v0, p2, :cond_0

    .line 290
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 291
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->countingSumScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    goto :goto_0

    .line 293
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer2;->doc:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected visitSubScorers(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/search/Scorer$ScorerVisitor;)V
    .locals 4
    .param p1, "parent"    # Lorg/apache/lucene/search/Query;
    .param p2, "relationship"    # Lorg/apache/lucene/search/BooleanClause$Occur;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Query;",
            "Lorg/apache/lucene/search/BooleanClause$Occur;",
            "Lorg/apache/lucene/search/Scorer$ScorerVisitor",
            "<",
            "Lorg/apache/lucene/search/Query;",
            "Lorg/apache/lucene/search/Query;",
            "Lorg/apache/lucene/search/Scorer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 325
    .local p3, "visitor":Lorg/apache/lucene/search/Scorer$ScorerVisitor;, "Lorg/apache/lucene/search/Scorer$ScorerVisitor<Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Scorer;>;"
    invoke-super {p0, p1, p2, p3}, Lorg/apache/lucene/search/Scorer;->visitSubScorers(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/search/Scorer$ScorerVisitor;)V

    .line 326
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->weight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Weight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v1

    .line 327
    .local v1, "q":Lorg/apache/lucene/search/Query;
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->optionalScorers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/Scorer;

    .line 328
    .local v2, "s":Lorg/apache/lucene/search/Scorer;
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v1, v3, p3}, Lorg/apache/lucene/search/Scorer;->visitSubScorers(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/search/Scorer$ScorerVisitor;)V

    goto :goto_0

    .line 330
    .end local v2    # "s":Lorg/apache/lucene/search/Scorer;
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->prohibitedScorers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/Scorer;

    .line 331
    .restart local v2    # "s":Lorg/apache/lucene/search/Scorer;
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v1, v3, p3}, Lorg/apache/lucene/search/Scorer;->visitSubScorers(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/search/Scorer$ScorerVisitor;)V

    goto :goto_1

    .line 333
    .end local v2    # "s":Lorg/apache/lucene/search/Scorer;
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanScorer2;->requiredScorers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/Scorer;

    .line 334
    .restart local v2    # "s":Lorg/apache/lucene/search/Scorer;
    sget-object v3, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v2, v1, v3, p3}, Lorg/apache/lucene/search/Scorer;->visitSubScorers(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/search/Scorer$ScorerVisitor;)V

    goto :goto_2

    .line 336
    .end local v2    # "s":Lorg/apache/lucene/search/Scorer;
    :cond_2
    return-void
.end method
