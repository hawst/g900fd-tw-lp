.class public Lorg/apache/lucene/search/function/CustomScoreProvider;
.super Ljava/lang/Object;
.source "CustomScoreProvider.java"


# instance fields
.field protected final reader:Lorg/apache/lucene/index/IndexReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;)V
    .locals 0
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lorg/apache/lucene/search/function/CustomScoreProvider;->reader:Lorg/apache/lucene/index/IndexReader;

    .line 47
    return-void
.end method


# virtual methods
.method public customExplain(ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;
    .locals 4
    .param p1, "doc"    # I
    .param p2, "subQueryExpl"    # Lorg/apache/lucene/search/Explanation;
    .param p3, "valSrcExpl"    # Lorg/apache/lucene/search/Explanation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    const/high16 v1, 0x3f800000    # 1.0f

    .line 153
    .local v1, "valSrcScore":F
    if-eqz p3, :cond_0

    .line 154
    invoke-virtual {p3}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v2

    mul-float/2addr v1, v2

    .line 156
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v2

    mul-float/2addr v2, v1

    const-string/jumbo v3, "custom score: product of:"

    invoke-direct {v0, v2, v3}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 157
    .local v0, "exp":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v0, p2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 158
    invoke-virtual {v0, p3}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 159
    return-object v0
.end method

.method public customExplain(ILorg/apache/lucene/search/Explanation;[Lorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;
    .locals 5
    .param p1, "doc"    # I
    .param p2, "subQueryExpl"    # Lorg/apache/lucene/search/Explanation;
    .param p3, "valSrcExpls"    # [Lorg/apache/lucene/search/Explanation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    array-length v3, p3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 123
    const/4 v3, 0x0

    aget-object v3, p3, v3

    invoke-virtual {p0, p1, p2, v3}, Lorg/apache/lucene/search/function/CustomScoreProvider;->customExplain(ILorg/apache/lucene/search/Explanation;Lorg/apache/lucene/search/Explanation;)Lorg/apache/lucene/search/Explanation;

    move-result-object p2

    .line 137
    .end local p2    # "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    :cond_0
    :goto_0
    return-object p2

    .line 125
    .restart local p2    # "subQueryExpl":Lorg/apache/lucene/search/Explanation;
    :cond_1
    array-length v3, p3

    if-eqz v3, :cond_0

    .line 128
    const/high16 v2, 0x3f800000    # 1.0f

    .line 129
    .local v2, "valSrcScore":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, p3

    if-ge v1, v3, :cond_2

    .line 130
    aget-object v3, p3, v1

    invoke-virtual {v3}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v3

    mul-float/2addr v2, v3

    .line 129
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 132
    :cond_2
    new-instance v0, Lorg/apache/lucene/search/Explanation;

    invoke-virtual {p2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v3

    mul-float/2addr v3, v2

    const-string/jumbo v4, "custom score: product of:"

    invoke-direct {v0, v3, v4}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 133
    .local v0, "exp":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v0, p2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 134
    const/4 v1, 0x0

    :goto_2
    array-length v3, p3

    if-ge v1, v3, :cond_3

    .line 135
    aget-object v3, p3, v1

    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 134
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    move-object p2, v0

    .line 137
    goto :goto_0
.end method

.method public customScore(IFF)F
    .locals 1
    .param p1, "doc"    # I
    .param p2, "subQueryScore"    # F
    .param p3, "valSrcScore"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    mul-float v0, p2, p3

    return v0
.end method

.method public customScore(IF[F)F
    .locals 4
    .param p1, "doc"    # I
    .param p2, "subQueryScore"    # F
    .param p3, "valSrcScores"    # [F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    array-length v2, p3

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 74
    const/4 v2, 0x0

    aget v2, p3, v2

    invoke-virtual {p0, p1, p2, v2}, Lorg/apache/lucene/search/function/CustomScoreProvider;->customScore(IFF)F

    move-result v1

    .line 83
    :cond_0
    :goto_0
    return v1

    .line 76
    :cond_1
    array-length v2, p3

    if-nez v2, :cond_2

    .line 77
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1, p2, v2}, Lorg/apache/lucene/search/function/CustomScoreProvider;->customScore(IFF)F

    move-result v1

    goto :goto_0

    .line 79
    :cond_2
    move v1, p2

    .line 80
    .local v1, "score":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p3

    if-ge v0, v2, :cond_0

    .line 81
    aget v2, p3, v0

    mul-float/2addr v1, v2

    .line 80
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
