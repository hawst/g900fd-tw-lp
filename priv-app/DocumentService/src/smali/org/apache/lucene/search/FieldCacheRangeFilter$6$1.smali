.class Lorg/apache/lucene/search/FieldCacheRangeFilter$6$1;
.super Lorg/apache/lucene/search/FieldCacheDocIdSet;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/FieldCacheRangeFilter$6;

.field final synthetic val$inclusiveLowerPoint:F

.field final synthetic val$inclusiveUpperPoint:F

.field final synthetic val$values:[F


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$6;Lorg/apache/lucene/index/IndexReader;[FFF)V
    .locals 0
    .param p2, "x0"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 377
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6$1;->this$0:Lorg/apache/lucene/search/FieldCacheRangeFilter$6;

    iput-object p3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6$1;->val$values:[F

    iput p4, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6$1;->val$inclusiveLowerPoint:F

    iput p5, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6$1;->val$inclusiveUpperPoint:F

    invoke-direct {p0, p2}, Lorg/apache/lucene/search/FieldCacheDocIdSet;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    return-void
.end method


# virtual methods
.method protected matchDoc(I)Z
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 378
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6$1;->val$values:[F

    aget v0, v0, p1

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6$1;->val$inclusiveLowerPoint:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6$1;->val$values:[F

    aget v0, v0, p1

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6$1;->val$inclusiveUpperPoint:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
