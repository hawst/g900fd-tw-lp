.class public Lorg/apache/lucene/search/function/ByteFieldSource;
.super Lorg/apache/lucene/search/function/FieldCacheSource;
.source "ByteFieldSource.java"


# instance fields
.field private parser:Lorg/apache/lucene/search/FieldCache$ByteParser;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/function/ByteFieldSource;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "parser"    # Lorg/apache/lucene/search/FieldCache$ByteParser;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/function/FieldCacheSource;-><init>(Ljava/lang/String;)V

    .line 59
    iput-object p2, p0, Lorg/apache/lucene/search/function/ByteFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    .line 60
    return-void
.end method


# virtual methods
.method public cachedFieldSourceEquals(Lorg/apache/lucene/search/function/FieldCacheSource;)Z
    .locals 5
    .param p1, "o"    # Lorg/apache/lucene/search/function/FieldCacheSource;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 99
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-class v4, Lorg/apache/lucene/search/function/ByteFieldSource;

    if-eq v3, v4, :cond_0

    .line 103
    :goto_0
    return v2

    :cond_0
    move-object v0, p1

    .line 102
    check-cast v0, Lorg/apache/lucene/search/function/ByteFieldSource;

    .line 103
    .local v0, "other":Lorg/apache/lucene/search/function/ByteFieldSource;
    iget-object v3, p0, Lorg/apache/lucene/search/function/ByteFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    if-nez v3, :cond_3

    iget-object v3, v0, Lorg/apache/lucene/search/function/ByteFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    if-nez v3, :cond_2

    :cond_1
    :goto_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/search/function/ByteFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    iget-object v4, v0, Lorg/apache/lucene/search/function/ByteFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_1

    move v1, v2

    goto :goto_1
.end method

.method public cachedFieldSourceHashCode()I
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/lucene/search/function/ByteFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    if-nez v0, :cond_0

    const-class v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/function/ByteFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "byte("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lorg/apache/lucene/search/function/FieldCacheSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCachedFieldValues(Lorg/apache/lucene/search/FieldCache;Ljava/lang/String;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/function/DocValues;
    .locals 2
    .param p1, "cache"    # Lorg/apache/lucene/search/FieldCache;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v1, p0, Lorg/apache/lucene/search/function/ByteFieldSource;->parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    invoke-interface {p1, p3, p2, v1}, Lorg/apache/lucene/search/FieldCache;->getBytes(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;)[B

    move-result-object v0

    .line 72
    .local v0, "arr":[B
    new-instance v1, Lorg/apache/lucene/search/function/ByteFieldSource$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/search/function/ByteFieldSource$1;-><init>(Lorg/apache/lucene/search/function/ByteFieldSource;[B)V

    return-object v1
.end method
