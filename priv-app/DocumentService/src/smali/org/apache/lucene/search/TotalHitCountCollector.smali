.class public Lorg/apache/lucene/search/TotalHitCountCollector;
.super Lorg/apache/lucene/search/Collector;
.source "TotalHitCountCollector.java"


# instance fields
.field private totalHits:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    return v0
.end method

.method public collect(I)V
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 40
    iget v0, p0, Lorg/apache/lucene/search/TotalHitCountCollector;->totalHits:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/TotalHitCountCollector;->totalHits:I

    .line 41
    return-void
.end method

.method public getTotalHits()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lorg/apache/lucene/search/TotalHitCountCollector;->totalHits:I

    return v0
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 0
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I

    .prologue
    .line 45
    return-void
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 36
    return-void
.end method
