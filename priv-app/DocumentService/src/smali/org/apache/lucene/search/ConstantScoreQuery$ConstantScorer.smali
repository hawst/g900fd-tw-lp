.class public Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "ConstantScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/ConstantScoreQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ConstantScorer"
.end annotation


# instance fields
.field final docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

.field final theScore:F

.field final synthetic this$0:Lorg/apache/lucene/search/ConstantScoreQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/ConstantScoreQuery;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/Weight;)V
    .locals 1
    .param p2, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p3, "docIdSetIterator"    # Lorg/apache/lucene/search/DocIdSetIterator;
    .param p4, "w"    # Lorg/apache/lucene/search/Weight;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    iput-object p1, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->this$0:Lorg/apache/lucene/search/ConstantScoreQuery;

    .line 184
    invoke-direct {p0, p2, p4}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;)V

    .line 185
    invoke-virtual {p4}, Lorg/apache/lucene/search/Weight;->getValue()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->theScore:F

    .line 186
    iput-object p3, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    .line 187
    return-void
.end method

.method private wrapCollector(Lorg/apache/lucene/search/Collector;)Lorg/apache/lucene/search/Collector;
    .locals 1
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;

    .prologue
    .line 210
    new-instance v0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer$1;-><init>(Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;Lorg/apache/lucene/search/Collector;)V

    return-object v0
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v0

    return v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v0

    return v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    return v0
.end method

.method public score()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 201
    iget v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->theScore:F

    return v0
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 238
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    instance-of v0, v0, Lorg/apache/lucene/search/Scorer;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    check-cast v0, Lorg/apache/lucene/search/Scorer;

    invoke-direct {p0, p1}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->wrapCollector(Lorg/apache/lucene/search/Collector;)Lorg/apache/lucene/search/Collector;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;)V

    .line 243
    :goto_0
    return-void

    .line 241
    :cond_0
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;)V

    goto :goto_0
.end method

.method protected score(Lorg/apache/lucene/search/Collector;II)Z
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "max"    # I
    .param p3, "firstDocID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 250
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    instance-of v0, v0, Lorg/apache/lucene/search/Scorer;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->docIdSetIterator:Lorg/apache/lucene/search/DocIdSetIterator;

    check-cast v0, Lorg/apache/lucene/search/Scorer;

    invoke-direct {p0, p1}, Lorg/apache/lucene/search/ConstantScoreQuery$ConstantScorer;->wrapCollector(Lorg/apache/lucene/search/Collector;)Lorg/apache/lucene/search/Collector;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;II)Z

    move-result v0

    .line 253
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;II)Z

    move-result v0

    goto :goto_0
.end method
