.class public Lorg/apache/lucene/search/TermRangeFilter;
.super Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;
.source "TermRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/MultiTermQueryWrapperFilter",
        "<",
        "Lorg/apache/lucene/search/TermRangeQuery;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "lowerTerm"    # Ljava/lang/String;
    .param p3, "upperTerm"    # Ljava/lang/String;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z

    .prologue
    .line 49
    new-instance v0, Lorg/apache/lucene/search/TermRangeQuery;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/TermRangeQuery;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;-><init>(Lorg/apache/lucene/search/MultiTermQuery;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/text/Collator;)V
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "lowerTerm"    # Ljava/lang/String;
    .param p3, "upperTerm"    # Ljava/lang/String;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z
    .param p6, "collator"    # Ljava/text/Collator;

    .prologue
    .line 72
    new-instance v0, Lorg/apache/lucene/search/TermRangeQuery;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/TermRangeQuery;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/text/Collator;)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/MultiTermQueryWrapperFilter;-><init>(Lorg/apache/lucene/search/MultiTermQuery;)V

    .line 73
    return-void
.end method

.method public static Less(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/TermRangeFilter;
    .locals 6
    .param p0, "fieldName"    # Ljava/lang/String;
    .param p1, "upperTerm"    # Ljava/lang/String;

    .prologue
    .line 80
    new-instance v0, Lorg/apache/lucene/search/TermRangeFilter;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/TermRangeFilter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public static More(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/search/TermRangeFilter;
    .locals 6
    .param p0, "fieldName"    # Ljava/lang/String;
    .param p1, "lowerTerm"    # Ljava/lang/String;

    .prologue
    .line 88
    new-instance v0, Lorg/apache/lucene/search/TermRangeFilter;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/TermRangeFilter;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method


# virtual methods
.method public getCollator()Ljava/text/Collator;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/TermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/TermRangeQuery;->getCollator()Ljava/text/Collator;

    move-result-object v0

    return-object v0
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/TermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/TermRangeQuery;->getField()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLowerTerm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/TermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/TermRangeQuery;->getLowerTerm()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUpperTerm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/TermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/TermRangeQuery;->getUpperTerm()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public includesLower()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/TermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/TermRangeQuery;->includesLower()Z

    move-result v0

    return v0
.end method

.method public includesUpper()Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeFilter;->query:Lorg/apache/lucene/search/MultiTermQuery;

    check-cast v0, Lorg/apache/lucene/search/TermRangeQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/TermRangeQuery;->includesUpper()Z

    move-result v0

    return v0
.end method
