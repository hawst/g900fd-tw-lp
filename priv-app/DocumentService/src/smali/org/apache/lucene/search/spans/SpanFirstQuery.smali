.class public Lorg/apache/lucene/search/spans/SpanFirstQuery;
.super Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;
.source "SpanFirstQuery.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/search/spans/SpanFirstQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/spans/SpanFirstQuery;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanQuery;I)V
    .locals 1
    .param p1, "match"    # Lorg/apache/lucene/search/spans/SpanQuery;
    .param p2, "end"    # I

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/lucene/search/spans/SpanPositionRangeQuery;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;II)V

    .line 37
    return-void
.end method


# virtual methods
.method protected acceptPosition(Lorg/apache/lucene/search/spans/Spans;)Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;
    .locals 2
    .param p1, "spans"    # Lorg/apache/lucene/search/spans/Spans;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    sget-boolean v0, Lorg/apache/lucene/search/spans/SpanFirstQuery;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v0

    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 42
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/spans/SpanFirstQuery;->end:I

    if-lt v0, v1, :cond_1

    .line 43
    sget-object v0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->NO_AND_ADVANCE:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    .line 47
    :goto_0
    return-object v0

    .line 44
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/spans/SpanFirstQuery;->end:I

    if-gt v0, v1, :cond_2

    .line 45
    sget-object v0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->YES:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    goto :goto_0

    .line 47
    :cond_2
    sget-object v0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->NO:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    goto :goto_0
.end method

.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 65
    new-instance v0, Lorg/apache/lucene/search/spans/SpanFirstQuery;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanFirstQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanQuery;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/spans/SpanQuery;

    iget v2, p0, Lorg/apache/lucene/search/spans/SpanFirstQuery;->end:I

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/spans/SpanFirstQuery;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;I)V

    .line 66
    .local v0, "spanFirstQuery":Lorg/apache/lucene/search/spans/SpanFirstQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanFirstQuery;->getBoost()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/spans/SpanFirstQuery;->setBoost(F)V

    .line 67
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    if-ne p0, p1, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v1

    .line 73
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/search/spans/SpanFirstQuery;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 75
    check-cast v0, Lorg/apache/lucene/search/spans/SpanFirstQuery;

    .line 76
    .local v0, "other":Lorg/apache/lucene/search/spans/SpanFirstQuery;
    iget v3, p0, Lorg/apache/lucene/search/spans/SpanFirstQuery;->end:I

    iget v4, v0, Lorg/apache/lucene/search/spans/SpanFirstQuery;->end:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanFirstQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    iget-object v4, v0, Lorg/apache/lucene/search/spans/SpanFirstQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/spans/SpanQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanFirstQuery;->getBoost()F

    move-result v3

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanFirstQuery;->getBoost()F

    move-result v4

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 83
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanFirstQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanQuery;->hashCode()I

    move-result v0

    .line 84
    .local v0, "h":I
    shl-int/lit8 v1, v0, 0x8

    ushr-int/lit8 v2, v0, 0x19

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 85
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanFirstQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/spans/SpanFirstQuery;->end:I

    xor-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 86
    return v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "spanFirst("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanFirstQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    iget v1, p0, Lorg/apache/lucene/search/spans/SpanFirstQuery;->end:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 58
    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanFirstQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
