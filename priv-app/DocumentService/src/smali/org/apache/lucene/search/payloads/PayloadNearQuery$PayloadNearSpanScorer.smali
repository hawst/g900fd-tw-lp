.class public Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;
.super Lorg/apache/lucene/search/spans/SpanScorer;
.source "PayloadNearQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/payloads/PayloadNearQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PayloadNearSpanScorer"
.end annotation


# instance fields
.field protected payloadScore:F

.field private payloadsSeen:I

.field similarity:Lorg/apache/lucene/search/Similarity;

.field spans:Lorg/apache/lucene/search/spans/Spans;

.field final synthetic this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/search/payloads/PayloadNearQuery;Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Similarity;[B)V
    .locals 1
    .param p2, "spans"    # Lorg/apache/lucene/search/spans/Spans;
    .param p3, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p4, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p5, "norms"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 160
    iput-object p1, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    .line 161
    invoke-direct {p0, p2, p3, p4, p5}, Lorg/apache/lucene/search/spans/SpanScorer;-><init>(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Similarity;[B)V

    .line 157
    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 162
    iput-object p2, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    .line 163
    return-void
.end method


# virtual methods
.method protected explain(I)Lorg/apache/lucene/search/Explanation;
    .locals 6
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 233
    new-instance v2, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v2}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 235
    .local v2, "result":Lorg/apache/lucene/search/Explanation;
    invoke-super {p0, p1}, Lorg/apache/lucene/search/spans/SpanScorer;->explain(I)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    .line 236
    .local v0, "nonPayloadExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 238
    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    iget-object v3, v3, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    iget v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadsSeen:I

    iget v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadScore:F

    invoke-virtual {v3, p1, v4, v5}, Lorg/apache/lucene/search/payloads/PayloadFunction;->explain(IIF)Lorg/apache/lucene/search/Explanation;

    move-result-object v1

    .line 239
    .local v1, "payloadExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v2, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 240
    invoke-virtual {v0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v3

    invoke-virtual {v1}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v4

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 241
    const-string/jumbo v3, "PayloadNearQuery, product of:"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 242
    return-object v2
.end method

.method public getPayloads([Lorg/apache/lucene/search/spans/Spans;)V
    .locals 4
    .param p1, "subSpans"    # [Lorg/apache/lucene/search/spans/Spans;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_4

    .line 168
    aget-object v1, p1, v0

    instance-of v1, v1, Lorg/apache/lucene/search/spans/NearSpansOrdered;

    if-eqz v1, :cond_2

    .line 169
    aget-object v1, p1, v0

    check-cast v1, Lorg/apache/lucene/search/spans/NearSpansOrdered;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->isPayloadAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    aget-object v1, p1, v0

    check-cast v1, Lorg/apache/lucene/search/spans/NearSpansOrdered;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->getPayload()Ljava/util/Collection;

    move-result-object v1

    aget-object v2, p1, v0

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v2

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->processPayloads(Ljava/util/Collection;II)V

    .line 173
    :cond_0
    aget-object v1, p1, v0

    check-cast v1, Lorg/apache/lucene/search/spans/NearSpansOrdered;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->getSubSpans()[Lorg/apache/lucene/search/spans/Spans;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->getPayloads([Lorg/apache/lucene/search/spans/Spans;)V

    .line 167
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 174
    :cond_2
    aget-object v1, p1, v0

    instance-of v1, v1, Lorg/apache/lucene/search/spans/NearSpansUnordered;

    if-eqz v1, :cond_1

    .line 175
    aget-object v1, p1, v0

    check-cast v1, Lorg/apache/lucene/search/spans/NearSpansUnordered;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->isPayloadAvailable()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 176
    aget-object v1, p1, v0

    check-cast v1, Lorg/apache/lucene/search/spans/NearSpansUnordered;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->getPayload()Ljava/util/Collection;

    move-result-object v1

    aget-object v2, p1, v0

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v2

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->processPayloads(Ljava/util/Collection;II)V

    .line 179
    :cond_3
    aget-object v1, p1, v0

    check-cast v1, Lorg/apache/lucene/search/spans/NearSpansUnordered;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->getSubSpans()[Lorg/apache/lucene/search/spans/Spans;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->getPayloads([Lorg/apache/lucene/search/spans/Spans;)V

    goto :goto_1

    .line 182
    :cond_4
    return-void
.end method

.method protected processPayloads(Ljava/util/Collection;II)V
    .locals 15
    .param p2, "start"    # I
    .param p3, "end"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<[B>;II)V"
        }
    .end annotation

    .prologue
    .line 195
    .local p1, "payLoads":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    .line 196
    .local v5, "thePayload":[B
    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    iget-object v9, v0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    iget v10, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->doc:I

    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    iget-object v8, v0, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->fieldName:Ljava/lang/String;

    iget v11, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadsSeen:I

    iget v12, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadScore:F

    iget-object v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->similarity:Lorg/apache/lucene/search/Similarity;

    iget v1, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->doc:I

    iget-object v2, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    iget-object v2, v2, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->fieldName:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v4

    const/4 v6, 0x0

    array-length v7, v5

    invoke-virtual/range {v0 .. v7}, Lorg/apache/lucene/search/Similarity;->scorePayload(ILjava/lang/String;II[BII)F

    move-result v13

    move-object v6, v9

    move v7, v10

    move/from16 v9, p2

    move/from16 v10, p3

    invoke-virtual/range {v6 .. v13}, Lorg/apache/lucene/search/payloads/PayloadFunction;->currentScore(ILjava/lang/String;IIIFF)F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadScore:F

    .line 199
    iget v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadsSeen:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadsSeen:I

    goto :goto_0

    .line 201
    .end local v5    # "thePayload":[B
    :cond_0
    return-void
.end method

.method public score()F
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 227
    invoke-super {p0}, Lorg/apache/lucene/search/spans/SpanScorer;->score()F

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    iget-object v1, v1, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->function:Lorg/apache/lucene/search/payloads/PayloadFunction;

    iget v2, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->doc:I

    iget-object v3, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->this$0:Lorg/apache/lucene/search/payloads/PayloadNearQuery;

    iget-object v3, v3, Lorg/apache/lucene/search/payloads/PayloadNearQuery;->fieldName:Ljava/lang/String;

    iget v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadsSeen:I

    iget v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadScore:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lorg/apache/lucene/search/payloads/PayloadFunction;->docScore(ILjava/lang/String;IF)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method protected setFreqCurrentDoc()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 206
    iget-boolean v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->more:Z

    if-nez v4, :cond_0

    .line 221
    :goto_0
    return v2

    .line 209
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->doc:I

    .line 210
    iput v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->freq:F

    .line 211
    iput v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadScore:F

    .line 212
    iput v2, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->payloadsSeen:I

    .line 214
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v5

    sub-int v0, v4, v5

    .line 215
    .local v0, "matchLength":I
    iget v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->freq:F

    invoke-virtual {p0}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v5

    invoke-virtual {v5, v0}, Lorg/apache/lucene/search/Similarity;->sloppyFreq(I)F

    move-result v5

    add-float/2addr v4, v5

    iput v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->freq:F

    .line 216
    new-array v1, v3, [Lorg/apache/lucene/search/spans/Spans;

    .line 217
    .local v1, "spansArr":[Lorg/apache/lucene/search/spans/Spans;
    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    aput-object v4, v1, v2

    .line 218
    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->getPayloads([Lorg/apache/lucene/search/spans/Spans;)V

    .line 219
    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v4

    iput-boolean v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->more:Z

    .line 220
    iget-boolean v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->more:Z

    if-eqz v4, :cond_2

    iget v4, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->doc:I

    iget-object v5, p0, Lorg/apache/lucene/search/payloads/PayloadNearQuery$PayloadNearSpanScorer;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v5

    if-eq v4, v5, :cond_1

    :cond_2
    move v2, v3

    .line 221
    goto :goto_0
.end method
