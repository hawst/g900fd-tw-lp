.class final Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;
.super Ljava/lang/Object;
.source "IndexSearcher.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/IndexSearcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MultiSearcherCallableNoSort"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lorg/apache/lucene/search/TopDocs;",
        ">;"
    }
.end annotation


# instance fields
.field private final after:Lorg/apache/lucene/search/ScoreDoc;

.field private final filter:Lorg/apache/lucene/search/Filter;

.field private final hq:Lorg/apache/lucene/search/HitQueue;

.field private final lock:Ljava/util/concurrent/locks/Lock;

.field private final nDocs:I

.field private final searchable:Lorg/apache/lucene/search/IndexSearcher;

.field private final weight:Lorg/apache/lucene/search/Weight;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/locks/Lock;Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/ScoreDoc;ILorg/apache/lucene/search/HitQueue;)V
    .locals 0
    .param p1, "lock"    # Ljava/util/concurrent/locks/Lock;
    .param p2, "searchable"    # Lorg/apache/lucene/search/IndexSearcher;
    .param p3, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p4, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p5, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p6, "nDocs"    # I
    .param p7, "hq"    # Lorg/apache/lucene/search/HitQueue;

    .prologue
    .line 681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 682
    iput-object p1, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->lock:Ljava/util/concurrent/locks/Lock;

    .line 683
    iput-object p2, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->searchable:Lorg/apache/lucene/search/IndexSearcher;

    .line 684
    iput-object p3, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->weight:Lorg/apache/lucene/search/Weight;

    .line 685
    iput-object p4, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->filter:Lorg/apache/lucene/search/Filter;

    .line 686
    iput-object p5, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->after:Lorg/apache/lucene/search/ScoreDoc;

    .line 687
    iput p6, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->nDocs:I

    .line 688
    iput-object p7, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->hq:Lorg/apache/lucene/search/HitQueue;

    .line 689
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 670
    invoke-virtual {p0}, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->call()Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method public call()Lorg/apache/lucene/search/TopDocs;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 695
    iget-object v4, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->after:Lorg/apache/lucene/search/ScoreDoc;

    if-nez v4, :cond_1

    .line 696
    iget-object v4, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->searchable:Lorg/apache/lucene/search/IndexSearcher;

    iget-object v5, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->weight:Lorg/apache/lucene/search/Weight;

    iget-object v6, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->filter:Lorg/apache/lucene/search/Filter;

    iget v7, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->nDocs:I

    invoke-virtual {v4, v5, v6, v7}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    .line 700
    .local v0, "docs":Lorg/apache/lucene/search/TopDocs;
    :goto_0
    iget-object v3, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .line 702
    .local v3, "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    iget-object v4, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 704
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    :try_start_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    .line 705
    aget-object v2, v3, v1

    .line 706
    .local v2, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    iget-object v4, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->hq:Lorg/apache/lucene/search/HitQueue;

    invoke-virtual {v4, v2}, Lorg/apache/lucene/search/HitQueue;->insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    if-ne v2, v4, :cond_2

    .line 711
    .end local v2    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 713
    return-object v0

    .line 698
    .end local v0    # "docs":Lorg/apache/lucene/search/TopDocs;
    .end local v1    # "j":I
    .end local v3    # "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->searchable:Lorg/apache/lucene/search/IndexSearcher;

    iget-object v5, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->weight:Lorg/apache/lucene/search/Weight;

    iget-object v6, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->filter:Lorg/apache/lucene/search/Filter;

    iget-object v7, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->after:Lorg/apache/lucene/search/ScoreDoc;

    iget v8, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->nDocs:I

    invoke-virtual {v4, v5, v6, v7, v8}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    .restart local v0    # "docs":Lorg/apache/lucene/search/TopDocs;
    goto :goto_0

    .line 704
    .restart local v1    # "j":I
    .restart local v2    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    .restart local v3    # "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 711
    .end local v2    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v4
.end method
