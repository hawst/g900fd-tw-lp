.class Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;
.super Lorg/apache/lucene/search/TopScoreDocCollector;
.source "TopScoreDocCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopScoreDocCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InOrderPagingScoreDocCollector"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final after:Lorg/apache/lucene/search/ScoreDoc;

.field private afterDoc:I

.field private collectedHits:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lorg/apache/lucene/search/TopScoreDocCollector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/search/ScoreDoc;I)V
    .locals 1
    .param p1, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "numHits"    # I

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lorg/apache/lucene/search/TopScoreDocCollector;-><init>(ILorg/apache/lucene/search/TopScoreDocCollector$1;)V

    .line 80
    iput-object p1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->after:Lorg/apache/lucene/search/ScoreDoc;

    .line 81
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/search/ScoreDoc;ILorg/apache/lucene/search/TopScoreDocCollector$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lorg/apache/lucene/search/TopScoreDocCollector$1;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;-><init>(Lorg/apache/lucene/search/ScoreDoc;I)V

    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public collect(I)V
    .locals 3
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    .line 88
    .local v0, "score":F
    sget-boolean v1, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    const/high16 v1, -0x800000    # Float.NEGATIVE_INFINITY

    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 89
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 91
    :cond_1
    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->totalHits:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->totalHits:I

    .line 93
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->after:Lorg/apache/lucene/search/ScoreDoc;

    iget v1, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->after:Lorg/apache/lucene/search/ScoreDoc;

    iget v1, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    cmpl-float v1, v0, v1

    if-nez v1, :cond_3

    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->afterDoc:I

    if-gt p1, v1, :cond_3

    .line 108
    :cond_2
    :goto_0
    return-void

    .line 98
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iget v1, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    cmpg-float v1, v0, v1

    if-lez v1, :cond_2

    .line 104
    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->collectedHits:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->collectedHits:I

    .line 105
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iget v2, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->docBase:I

    add-int/2addr v2, p1

    iput v2, v1, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    .line 106
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    iput v0, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    .line 107
    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/PriorityQueue;->updateTop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/ScoreDoc;

    iput-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->pqTop:Lorg/apache/lucene/search/ScoreDoc;

    goto :goto_0
.end method

.method protected newTopDocs([Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;
    .locals 4
    .param p1, "results"    # [Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "start"    # I

    .prologue
    .line 128
    if-nez p1, :cond_0

    new-instance v0, Lorg/apache/lucene/search/TopDocs;

    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->totalHits:I

    const/4 v2, 0x0

    new-array v2, v2, [Lorg/apache/lucene/search/ScoreDoc;

    const/high16 v3, 0x7fc00000    # NaNf

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/search/TopDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;F)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/search/TopDocs;

    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->totalHits:I

    invoke-direct {v0, v1, p1}, Lorg/apache/lucene/search/TopDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;)V

    goto :goto_0
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "base"    # I

    .prologue
    .line 117
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/search/TopScoreDocCollector;->setNextReader(Lorg/apache/lucene/index/IndexReader;I)V

    .line 118
    iget-object v0, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->after:Lorg/apache/lucene/search/ScoreDoc;

    iget v0, v0, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    iget v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->docBase:I

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->afterDoc:I

    .line 119
    return-void
.end method

.method protected topDocsSize()I
    .locals 2

    .prologue
    .line 123
    iget v0, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->collectedHits:I

    iget-object v1, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/PriorityQueue;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->collectedHits:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/TopScoreDocCollector$InOrderPagingScoreDocCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PriorityQueue;->size()I

    move-result v0

    goto :goto_0
.end method
