.class Lorg/apache/lucene/search/CachingCollector$1;
.super Lorg/apache/lucene/search/Collector;
.source "CachingCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/CachingCollector;->create(ZZD)Lorg/apache/lucene/search/CachingCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$acceptDocsOutOfOrder:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    .prologue
    .line 349
    iput-boolean p1, p0, Lorg/apache/lucene/search/CachingCollector$1;->val$acceptDocsOutOfOrder:Z

    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 339
    iget-boolean v0, p0, Lorg/apache/lucene/search/CachingCollector$1;->val$acceptDocsOutOfOrder:Z

    return v0
.end method

.method public collect(I)V
    .locals 0
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 346
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 0
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 349
    return-void
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 343
    return-void
.end method
