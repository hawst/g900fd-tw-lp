.class Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;
.super Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;
.source "TopFieldCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopFieldCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MultiComparatorScoringMaxScoreCollector"
.end annotation


# instance fields
.field scorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V
    .locals 1
    .param p2, "numHits"    # I
    .param p3, "fillFields"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;IZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 541
    .local p1, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    .line 543
    const/high16 v0, -0x800000    # Float.NEGATIVE_INFINITY

    iput v0, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->maxScore:F

    .line 544
    return-void
.end method


# virtual methods
.method public collect(I)V
    .locals 6
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 554
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    .line 555
    .local v2, "score":F
    iget v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->maxScore:F

    cmpl-float v4, v2, v4

    if-lez v4, :cond_0

    .line 556
    iput v2, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->maxScore:F

    .line 558
    :cond_0
    iget v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->totalHits:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->totalHits:I

    .line 559
    iget-boolean v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->queueFull:Z

    if-eqz v4, :cond_5

    .line 561
    const/4 v1, 0x0

    .line 562
    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->reverseMul:[I

    aget v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v5, v5, v1

    invoke-virtual {v5, p1}, Lorg/apache/lucene/search/FieldComparator;->compareBottom(I)I

    move-result v5

    mul-int v0, v4, v5

    .line 563
    .local v0, "c":I
    if-gez v0, :cond_2

    .line 601
    .end local v0    # "c":I
    :cond_1
    return-void

    .line 566
    .restart local v0    # "c":I
    :cond_2
    if-lez v0, :cond_3

    .line 578
    const/4 v1, 0x0

    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-ge v1, v4, :cond_4

    .line 579
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v5, v5, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v4, v5, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 578
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 569
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-eq v1, v4, :cond_1

    .line 561
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 582
    :cond_4
    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->updateBottom(IF)V

    .line 584
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 585
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v5, v5, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 584
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 589
    .end local v0    # "c":I
    .end local v1    # "i":I
    :cond_5
    iget v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->totalHits:I

    add-int/lit8 v3, v4, -0x1

    .line 591
    .local v3, "slot":I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-ge v1, v4, :cond_6

    .line 592
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    invoke-virtual {v4, v3, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 591
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 594
    :cond_6
    invoke-virtual {p0, v3, p1, v2}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->add(IIF)V

    .line 595
    iget-boolean v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->queueFull:Z

    if-eqz v4, :cond_1

    .line 596
    const/4 v1, 0x0

    :goto_4
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 597
    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    iget-object v5, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v5, v5, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 596
    add-int/lit8 v1, v1, 0x1

    goto :goto_4
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 605
    iput-object p1, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 606
    invoke-super {p0, p1}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 607
    return-void
.end method

.method final updateBottom(IF)V
    .locals 2
    .param p1, "doc"    # I
    .param p2, "score"    # F

    .prologue
    .line 547
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v1, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->docBase:I

    add-int/2addr v1, p1

    iput v1, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    .line 548
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput p2, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->score:F

    .line 549
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PriorityQueue;->updateTop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .line 550
    return-void
.end method
