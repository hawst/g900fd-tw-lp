.class Lorg/apache/lucene/search/TopDocs$ScoreMergeSortQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "TopDocs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopDocs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScoreMergeSortQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/search/TopDocs$ShardRef;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final shardHits:[[Lorg/apache/lucene/search/ScoreDoc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const-class v0, Lorg/apache/lucene/search/TopDocs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TopDocs$ScoreMergeSortQueue;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([Lorg/apache/lucene/search/TopDocs;)V
    .locals 3
    .param p1, "shardHits"    # [Lorg/apache/lucene/search/TopDocs;

    .prologue
    .line 85
    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;-><init>()V

    .line 86
    array-length v1, p1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/TopDocs$ScoreMergeSortQueue;->initialize(I)V

    .line 87
    array-length v1, p1

    new-array v1, v1, [[Lorg/apache/lucene/search/ScoreDoc;

    iput-object v1, p0, Lorg/apache/lucene/search/TopDocs$ScoreMergeSortQueue;->shardHits:[[Lorg/apache/lucene/search/ScoreDoc;

    .line 88
    const/4 v0, 0x0

    .local v0, "shardIDX":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 89
    iget-object v1, p0, Lorg/apache/lucene/search/TopDocs$ScoreMergeSortQueue;->shardHits:[[Lorg/apache/lucene/search/ScoreDoc;

    aget-object v2, p1, v0

    iget-object v2, v2, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    aput-object v2, v1, v0

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 82
    check-cast p1, Lorg/apache/lucene/search/TopDocs$ShardRef;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/search/TopDocs$ShardRef;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/TopDocs$ScoreMergeSortQueue;->lessThan(Lorg/apache/lucene/search/TopDocs$ShardRef;Lorg/apache/lucene/search/TopDocs$ShardRef;)Z

    move-result v0

    return v0
.end method

.method public lessThan(Lorg/apache/lucene/search/TopDocs$ShardRef;Lorg/apache/lucene/search/TopDocs$ShardRef;)Z
    .locals 6
    .param p1, "first"    # Lorg/apache/lucene/search/TopDocs$ShardRef;
    .param p2, "second"    # Lorg/apache/lucene/search/TopDocs$ShardRef;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 95
    sget-boolean v4, Lorg/apache/lucene/search/TopDocs$ScoreMergeSortQueue;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-ne p1, p2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 96
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/search/TopDocs$ScoreMergeSortQueue;->shardHits:[[Lorg/apache/lucene/search/ScoreDoc;

    iget v5, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    aget-object v4, v4, v5

    iget v5, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    aget-object v4, v4, v5

    iget v0, v4, Lorg/apache/lucene/search/ScoreDoc;->score:F

    .line 97
    .local v0, "firstScore":F
    iget-object v4, p0, Lorg/apache/lucene/search/TopDocs$ScoreMergeSortQueue;->shardHits:[[Lorg/apache/lucene/search/ScoreDoc;

    iget v5, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    aget-object v4, v4, v5

    iget v5, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    aget-object v4, v4, v5

    iget v1, v4, Lorg/apache/lucene/search/ScoreDoc;->score:F

    .line 99
    .local v1, "secondScore":F
    cmpg-float v4, v0, v1

    if-gez v4, :cond_2

    move v2, v3

    .line 113
    :cond_1
    :goto_0
    return v2

    .line 101
    :cond_2
    cmpl-float v4, v0, v1

    if-gtz v4, :cond_1

    .line 105
    iget v4, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    iget v5, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    if-lt v4, v5, :cond_1

    .line 107
    iget v4, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    iget v5, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    if-le v4, v5, :cond_3

    move v2, v3

    .line 108
    goto :goto_0

    .line 112
    :cond_3
    sget-boolean v4, Lorg/apache/lucene/search/TopDocs$ScoreMergeSortQueue;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    iget v4, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    iget v5, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    if-ne v4, v5, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 113
    :cond_4
    iget v4, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    iget v5, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    if-lt v4, v5, :cond_1

    move v2, v3

    goto :goto_0
.end method
