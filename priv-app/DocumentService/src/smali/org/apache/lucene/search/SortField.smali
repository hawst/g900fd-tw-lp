.class public Lorg/apache/lucene/search/SortField;
.super Ljava/lang/Object;
.source "SortField.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final BYTE:I = 0xa

.field public static final CUSTOM:I = 0x9

.field public static final DOC:I = 0x1

.field public static final DOUBLE:I = 0x7

.field public static final FIELD_DOC:Lorg/apache/lucene/search/SortField;

.field public static final FIELD_SCORE:Lorg/apache/lucene/search/SortField;

.field public static final FLOAT:I = 0x5

.field public static final INT:I = 0x4

.field public static final LONG:I = 0x6

.field public static final SCORE:I = 0x0

.field public static final SHORT:I = 0x8

.field public static final STRING:I = 0x3

.field public static final STRING_VAL:I = 0xb


# instance fields
.field private comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

.field private field:Ljava/lang/String;

.field private locale:Ljava/util/Locale;

.field private missingValue:Ljava/lang/Object;

.field private parser:Lorg/apache/lucene/search/FieldCache$Parser;

.field reverse:Z

.field private type:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35
    const-class v0, Lorg/apache/lucene/search/SortField;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/SortField;->$assertionsDisabled:Z

    .line 91
    new-instance v0, Lorg/apache/lucene/search/SortField;

    invoke-direct {v0, v3, v2}, Lorg/apache/lucene/search/SortField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/search/SortField;->FIELD_SCORE:Lorg/apache/lucene/search/SortField;

    .line 94
    new-instance v0, Lorg/apache/lucene/search/SortField;

    invoke-direct {v0, v3, v1}, Lorg/apache/lucene/search/SortField;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/search/SortField;->FIELD_DOC:Lorg/apache/lucene/search/SortField;

    return-void

    :cond_0
    move v0, v2

    .line 35
    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 114
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;I)V

    .line 115
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "reverse"    # Z

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 125
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;I)V

    .line 126
    iput-boolean p3, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 127
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Locale;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/util/Locale;

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 174
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;I)V

    .line 175
    iput-object p2, p0, Lorg/apache/lucene/search/SortField;->locale:Ljava/util/Locale;

    .line 176
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Locale;Z)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/util/Locale;
    .param p3, "reverse"    # Z

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 184
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;I)V

    .line 185
    iput-object p2, p0, Lorg/apache/lucene/search/SortField;->locale:Ljava/util/Locale;

    .line 186
    iput-boolean p3, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 187
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "parser"    # Lorg/apache/lucene/search/FieldCache$Parser;

    .prologue
    .line 140
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/SortField;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Z)V

    .line 141
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Z)V
    .locals 3
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "parser"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "reverse"    # Z

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 155
    instance-of v0, p2, Lorg/apache/lucene/search/FieldCache$IntParser;

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;I)V

    .line 164
    :goto_0
    iput-boolean p3, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 165
    iput-object p2, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    .line 166
    return-void

    .line 156
    :cond_0
    instance-of v0, p2, Lorg/apache/lucene/search/FieldCache$FloatParser;

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;I)V

    goto :goto_0

    .line 157
    :cond_1
    instance-of v0, p2, Lorg/apache/lucene/search/FieldCache$ShortParser;

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;I)V

    goto :goto_0

    .line 158
    :cond_2
    instance-of v0, p2, Lorg/apache/lucene/search/FieldCache$ByteParser;

    if-eqz v0, :cond_3

    const/16 v0, 0xa

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;I)V

    goto :goto_0

    .line 159
    :cond_3
    instance-of v0, p2, Lorg/apache/lucene/search/FieldCache$LongParser;

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;I)V

    goto :goto_0

    .line 160
    :cond_4
    instance-of v0, p2, Lorg/apache/lucene/search/FieldCache$DoubleParser;

    if-eqz v0, :cond_5

    const/4 v0, 0x7

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;I)V

    goto :goto_0

    .line 162
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Parser instance does not subclass existing numeric parser from FieldCache (got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldComparatorSource;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "comparator"    # Lorg/apache/lucene/search/FieldComparatorSource;

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 194
    const/16 v0, 0x9

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;I)V

    .line 195
    iput-object p2, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    .line 196
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldComparatorSource;Z)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "comparator"    # Lorg/apache/lucene/search/FieldComparatorSource;
    .param p3, "reverse"    # Z

    .prologue
    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 204
    const/16 v0, 0x9

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/SortField;->initFieldType(Ljava/lang/String;I)V

    .line 205
    iput-boolean p3, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    .line 206
    iput-object p2, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    .line 207
    return-void
.end method

.method private initFieldType(Ljava/lang/String;I)V
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 222
    iput p2, p0, Lorg/apache/lucene/search/SortField;->type:I

    .line 223
    if-nez p1, :cond_0

    .line 224
    if-eqz p2, :cond_1

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    .line 225
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "field can only be null when type is SCORE or DOC"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_0
    invoke-static {p1}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    .line 229
    :cond_1
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 372
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 373
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    .line 375
    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 342
    if-ne p0, p1, :cond_1

    .line 345
    :cond_0
    :goto_0
    return v1

    .line 343
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/search/SortField;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 344
    check-cast v0, Lorg/apache/lucene/search/SortField;

    .line 345
    .local v0, "other":Lorg/apache/lucene/search/SortField;
    iget-object v3, v0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    if-ne v3, v4, :cond_3

    iget v3, v0, Lorg/apache/lucene/search/SortField;->type:I

    iget v4, p0, Lorg/apache/lucene/search/SortField;->type:I

    if-ne v3, v4, :cond_3

    iget-boolean v3, v0, Lorg/apache/lucene/search/SortField;->reverse:Z

    iget-boolean v4, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    if-ne v3, v4, :cond_3

    iget-object v3, v0, Lorg/apache/lucene/search/SortField;->locale:Ljava/util/Locale;

    if-nez v3, :cond_4

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->locale:Ljava/util/Locale;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, v0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    if-nez v3, :cond_5

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    if-nez v3, :cond_3

    :goto_2
    iget-object v3, v0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    if-nez v3, :cond_6

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, v0, Lorg/apache/lucene/search/SortField;->locale:Ljava/util/Locale;

    iget-object v4, p0, Lorg/apache/lucene/search/SortField;->locale:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, v0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    iget-object v4, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, v0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    iget-object v4, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getComparator(II)Lorg/apache/lucene/search/FieldComparator;
    .locals 4
    .param p1, "numHits"    # I
    .param p2, "sortPos"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lorg/apache/lucene/search/FieldComparator",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 391
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->locale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    .line 395
    new-instance v0, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;

    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->locale:Ljava/util/Locale;

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/lucene/search/FieldComparator$StringComparatorLocale;-><init>(ILjava/lang/String;Ljava/util/Locale;)V

    .line 431
    :goto_0
    return-object v0

    .line 398
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/SortField;->type:I

    packed-switch v0, :pswitch_data_0

    .line 434
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Illegal sort type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/SortField;->type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 400
    :pswitch_1
    new-instance v0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;-><init>(I)V

    goto :goto_0

    .line 403
    :pswitch_2
    new-instance v0, Lorg/apache/lucene/search/FieldComparator$DocComparator;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/FieldComparator$DocComparator;-><init>(I)V

    goto :goto_0

    .line 406
    :pswitch_3
    new-instance v1, Lorg/apache/lucene/search/FieldComparator$IntComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-direct {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldComparator$IntComparator;-><init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Integer;)V

    move-object v0, v1

    goto :goto_0

    .line 409
    :pswitch_4
    new-instance v1, Lorg/apache/lucene/search/FieldComparator$FloatComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-direct {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldComparator$FloatComparator;-><init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Float;)V

    move-object v0, v1

    goto :goto_0

    .line 412
    :pswitch_5
    new-instance v1, Lorg/apache/lucene/search/FieldComparator$LongComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-direct {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldComparator$LongComparator;-><init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Long;)V

    move-object v0, v1

    goto :goto_0

    .line 415
    :pswitch_6
    new-instance v1, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-direct {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;-><init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Double;)V

    move-object v0, v1

    goto :goto_0

    .line 418
    :pswitch_7
    new-instance v1, Lorg/apache/lucene/search/FieldComparator$ByteComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Byte;

    invoke-direct {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldComparator$ByteComparator;-><init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Byte;)V

    move-object v0, v1

    goto :goto_0

    .line 421
    :pswitch_8
    new-instance v1, Lorg/apache/lucene/search/FieldComparator$ShortComparator;

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Short;

    invoke-direct {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldComparator$ShortComparator;-><init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Short;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 424
    :pswitch_9
    sget-boolean v0, Lorg/apache/lucene/search/SortField;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 425
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-boolean v2, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    invoke-virtual {v0, v1, p1, p2, v2}, Lorg/apache/lucene/search/FieldComparatorSource;->newComparator(Ljava/lang/String;IIZ)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v0

    goto/16 :goto_0

    .line 428
    :pswitch_a
    new-instance v0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;

    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    iget-boolean v2, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    invoke-direct {v0, p1, v1, p2, v2}, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;-><init>(ILjava/lang/String;IZ)V

    goto/16 :goto_0

    .line 431
    :pswitch_b
    new-instance v0, Lorg/apache/lucene/search/FieldComparator$StringValComparator;

    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/search/FieldComparator$StringValComparator;-><init>(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 398
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_a
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_b
    .end packed-switch
.end method

.method public getComparatorSource()Lorg/apache/lucene/search/FieldComparatorSource;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    return-object v0
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public getParser()Lorg/apache/lucene/search/FieldCache$Parser;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    return-object v0
.end method

.method public getReverse()Z
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lorg/apache/lucene/search/SortField;->type:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 362
    iget v1, p0, Lorg/apache/lucene/search/SortField;->type:I

    const v2, 0x346565dd

    iget-boolean v3, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->hashCode()I

    move-result v3

    add-int/2addr v2, v3

    xor-int/2addr v1, v2

    const v2, -0x50a66745

    xor-int v0, v1, v2

    .line 363
    .local v0, "hash":I
    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0xa97a23

    xor-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 364
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->locale:Ljava/util/Locale;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->hashCode()I

    move-result v1

    const v2, 0x8150815

    xor-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 365
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 366
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    const v2, 0x3aaf56ff

    xor-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 367
    :cond_3
    return v0
.end method

.method public setMissingValue(Ljava/lang/Object;)Lorg/apache/lucene/search/SortField;
    .locals 2
    .param p1, "missingValue"    # Ljava/lang/Object;

    .prologue
    .line 211
    iget v0, p0, Lorg/apache/lucene/search/SortField;->type:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/search/SortField;->type:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/search/SortField;->type:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/search/SortField;->type:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/search/SortField;->type:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/search/SortField;->type:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    .line 212
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Missing value only works for numeric types"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 214
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/search/SortField;->missingValue:Ljava/lang/Object;

    .line 216
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x29

    const/16 v3, 0x28

    .line 278
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 279
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget v1, p0, Lorg/apache/lucene/search/SortField;->type:I

    packed-switch v1, :pswitch_data_0

    .line 325
    :pswitch_0
    const-string/jumbo v1, "<???: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->locale:Ljava/util/Locale;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 330
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    if-eqz v1, :cond_1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 331
    :cond_1
    iget-boolean v1, p0, Lorg/apache/lucene/search/SortField;->reverse:Z

    if-eqz v1, :cond_2

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 333
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 281
    :pswitch_1
    const-string/jumbo v1, "<score>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 285
    :pswitch_2
    const-string/jumbo v1, "<doc>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 289
    :pswitch_3
    const-string/jumbo v1, "<string: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 293
    :pswitch_4
    const-string/jumbo v1, "<string_val: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 297
    :pswitch_5
    const-string/jumbo v1, "<byte: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 301
    :pswitch_6
    const-string/jumbo v1, "<short: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 305
    :pswitch_7
    const-string/jumbo v1, "<int: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 309
    :pswitch_8
    const-string/jumbo v1, "<long: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 313
    :pswitch_9
    const-string/jumbo v1, "<float: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 317
    :pswitch_a
    const-string/jumbo v1, "<double: \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 321
    :pswitch_b
    const-string/jumbo v1, "<custom:\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/SortField;->comparatorSource:Lorg/apache/lucene/search/FieldComparatorSource;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x3e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 279
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_7
        :pswitch_9
        :pswitch_8
        :pswitch_a
        :pswitch_6
        :pswitch_b
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method
