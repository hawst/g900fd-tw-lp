.class public Lorg/apache/lucene/search/MultiCollector;
.super Lorg/apache/lucene/search/Collector;
.source "MultiCollector.java"


# instance fields
.field private final collectors:[Lorg/apache/lucene/search/Collector;


# direct methods
.method private varargs constructor <init>([Lorg/apache/lucene/search/Collector;)V
    .locals 0
    .param p1, "collectors"    # [Lorg/apache/lucene/search/Collector;

    .prologue
    .line 89
    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    .line 90
    iput-object p1, p0, Lorg/apache/lucene/search/MultiCollector;->collectors:[Lorg/apache/lucene/search/Collector;

    .line 91
    return-void
.end method

.method public static varargs wrap([Lorg/apache/lucene/search/Collector;)Lorg/apache/lucene/search/Collector;
    .locals 10
    .param p0, "collectors"    # [Lorg/apache/lucene/search/Collector;

    .prologue
    .line 54
    const/4 v6, 0x0

    .line 55
    .local v6, "n":I
    move-object v0, p0

    .local v0, "arr$":[Lorg/apache/lucene/search/Collector;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v1, v0, v4

    .line 56
    .local v1, "c":Lorg/apache/lucene/search/Collector;
    if-eqz v1, :cond_0

    .line 57
    add-int/lit8 v6, v6, 0x1

    .line 55
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 61
    .end local v1    # "c":Lorg/apache/lucene/search/Collector;
    :cond_1
    if-nez v6, :cond_2

    .line 62
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v9, "At least 1 collector must not be null"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 63
    :cond_2
    const/4 v8, 0x1

    if-ne v6, v8, :cond_5

    .line 65
    const/4 v2, 0x0

    .line 66
    .local v2, "col":Lorg/apache/lucene/search/Collector;
    move-object v0, p0

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_3

    aget-object v1, v0, v4

    .line 67
    .restart local v1    # "c":Lorg/apache/lucene/search/Collector;
    if-eqz v1, :cond_4

    .line 68
    move-object v2, v1

    .line 83
    .end local v1    # "c":Lorg/apache/lucene/search/Collector;
    .end local v2    # "col":Lorg/apache/lucene/search/Collector;
    :cond_3
    :goto_2
    return-object v2

    .line 66
    .restart local v1    # "c":Lorg/apache/lucene/search/Collector;
    .restart local v2    # "col":Lorg/apache/lucene/search/Collector;
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 73
    .end local v1    # "c":Lorg/apache/lucene/search/Collector;
    .end local v2    # "col":Lorg/apache/lucene/search/Collector;
    :cond_5
    array-length v8, p0

    if-ne v6, v8, :cond_6

    .line 74
    new-instance v2, Lorg/apache/lucene/search/MultiCollector;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/MultiCollector;-><init>([Lorg/apache/lucene/search/Collector;)V

    goto :goto_2

    .line 76
    :cond_6
    new-array v3, v6, [Lorg/apache/lucene/search/Collector;

    .line 77
    .local v3, "colls":[Lorg/apache/lucene/search/Collector;
    const/4 v6, 0x0

    .line 78
    move-object v0, p0

    array-length v5, v0

    const/4 v4, 0x0

    move v7, v6

    .end local v6    # "n":I
    .local v7, "n":I
    :goto_3
    if-ge v4, v5, :cond_7

    aget-object v1, v0, v4

    .line 79
    .restart local v1    # "c":Lorg/apache/lucene/search/Collector;
    if-eqz v1, :cond_8

    .line 80
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "n":I
    .restart local v6    # "n":I
    aput-object v1, v3, v7

    .line 78
    :goto_4
    add-int/lit8 v4, v4, 0x1

    move v7, v6

    .end local v6    # "n":I
    .restart local v7    # "n":I
    goto :goto_3

    .line 83
    .end local v1    # "c":Lorg/apache/lucene/search/Collector;
    :cond_7
    new-instance v2, Lorg/apache/lucene/search/MultiCollector;

    invoke-direct {v2, v3}, Lorg/apache/lucene/search/MultiCollector;-><init>([Lorg/apache/lucene/search/Collector;)V

    move v6, v7

    .end local v7    # "n":I
    .restart local v6    # "n":I
    goto :goto_2

    .end local v6    # "n":I
    .restart local v1    # "c":Lorg/apache/lucene/search/Collector;
    .restart local v7    # "n":I
    :cond_8
    move v6, v7

    .end local v7    # "n":I
    .restart local v6    # "n":I
    goto :goto_4
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 5

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/lucene/search/MultiCollector;->collectors:[Lorg/apache/lucene/search/Collector;

    .local v0, "arr$":[Lorg/apache/lucene/search/Collector;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 96
    .local v1, "c":Lorg/apache/lucene/search/Collector;
    invoke-virtual {v1}, Lorg/apache/lucene/search/Collector;->acceptsDocsOutOfOrder()Z

    move-result v4

    if-nez v4, :cond_0

    .line 97
    const/4 v4, 0x0

    .line 100
    .end local v1    # "c":Lorg/apache/lucene/search/Collector;
    :goto_1
    return v4

    .line 95
    .restart local v1    # "c":Lorg/apache/lucene/search/Collector;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 100
    .end local v1    # "c":Lorg/apache/lucene/search/Collector;
    :cond_1
    const/4 v4, 0x1

    goto :goto_1
.end method

.method public collect(I)V
    .locals 4
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/lucene/search/MultiCollector;->collectors:[Lorg/apache/lucene/search/Collector;

    .local v0, "arr$":[Lorg/apache/lucene/search/Collector;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 106
    .local v1, "c":Lorg/apache/lucene/search/Collector;
    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 105
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 108
    .end local v1    # "c":Lorg/apache/lucene/search/Collector;
    :cond_0
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "o"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/lucene/search/MultiCollector;->collectors:[Lorg/apache/lucene/search/Collector;

    .local v0, "arr$":[Lorg/apache/lucene/search/Collector;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 113
    .local v1, "c":Lorg/apache/lucene/search/Collector;
    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/search/Collector;->setNextReader(Lorg/apache/lucene/index/IndexReader;I)V

    .line 112
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 115
    .end local v1    # "c":Lorg/apache/lucene/search/Collector;
    :cond_0
    return-void
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 4
    .param p1, "s"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lorg/apache/lucene/search/MultiCollector;->collectors:[Lorg/apache/lucene/search/Collector;

    .local v0, "arr$":[Lorg/apache/lucene/search/Collector;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 120
    .local v1, "c":Lorg/apache/lucene/search/Collector;
    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 119
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 122
    .end local v1    # "c":Lorg/apache/lucene/search/Collector;
    :cond_0
    return-void
.end method
