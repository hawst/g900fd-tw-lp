.class public abstract Lorg/apache/lucene/search/FieldCacheDocIdSet;
.super Lorg/apache/lucene/search/DocIdSet;
.source "FieldCacheDocIdSet.java"


# instance fields
.field protected final reader:Lorg/apache/lucene/index/IndexReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;)V
    .locals 0
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSet;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->reader:Lorg/apache/lucene/index/IndexReader;

    .line 38
    return-void
.end method


# virtual methods
.method public final isCacheable()Z
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Lorg/apache/lucene/search/DocIdSetIterator;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v2

    if-nez v2, :cond_0

    .line 55
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v0

    .line 56
    .local v0, "maxDoc":I
    new-instance v2, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;

    invoke-direct {v2, p0, v0}, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;-><init>(Lorg/apache/lucene/search/FieldCacheDocIdSet;I)V

    .line 88
    .end local v0    # "maxDoc":I
    :goto_0
    return-object v2

    .line 87
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet;->reader:Lorg/apache/lucene/index/IndexReader;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v1

    .line 88
    .local v1, "termDocs":Lorg/apache/lucene/index/TermDocs;
    new-instance v2, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;

    invoke-direct {v2, p0, v1}, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;-><init>(Lorg/apache/lucene/search/FieldCacheDocIdSet;Lorg/apache/lucene/index/TermDocs;)V

    goto :goto_0
.end method

.method protected abstract matchDoc(I)Z
.end method
