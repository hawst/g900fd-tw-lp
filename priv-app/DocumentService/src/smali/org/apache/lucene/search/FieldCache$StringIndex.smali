.class public Lorg/apache/lucene/search/FieldCache$StringIndex;
.super Ljava/lang/Object;
.source "FieldCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StringIndex"
.end annotation


# instance fields
.field public final lookup:[Ljava/lang/String;

.field public final order:[I


# direct methods
.method public constructor <init>([I[Ljava/lang/String;)V
    .locals 0
    .param p1, "values"    # [I
    .param p2, "lookup"    # [Ljava/lang/String;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCache$StringIndex;->order:[I

    .line 87
    iput-object p2, p0, Lorg/apache/lucene/search/FieldCache$StringIndex;->lookup:[Ljava/lang/String;

    .line 88
    return-void
.end method


# virtual methods
.method public binarySearchLookup(Ljava/lang/String;)I
    .locals 5
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 58
    if-nez p1, :cond_1

    .line 59
    const/4 v3, 0x0

    .line 75
    :cond_0
    :goto_0
    return v3

    .line 61
    :cond_1
    const/4 v2, 0x1

    .line 62
    .local v2, "low":I
    iget-object v4, p0, Lorg/apache/lucene/search/FieldCache$StringIndex;->lookup:[Ljava/lang/String;

    array-length v4, v4

    add-int/lit8 v1, v4, -0x1

    .line 64
    .local v1, "high":I
    :goto_1
    if-gt v2, v1, :cond_3

    .line 65
    add-int v4, v2, v1

    ushr-int/lit8 v3, v4, 0x1

    .line 66
    .local v3, "mid":I
    iget-object v4, p0, Lorg/apache/lucene/search/FieldCache$StringIndex;->lookup:[Ljava/lang/String;

    aget-object v4, v4, v3

    invoke-virtual {v4, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 68
    .local v0, "cmp":I
    if-gez v0, :cond_2

    .line 69
    add-int/lit8 v2, v3, 0x1

    goto :goto_1

    .line 70
    :cond_2
    if-lez v0, :cond_0

    .line 71
    add-int/lit8 v1, v3, -0x1

    goto :goto_1

    .line 75
    .end local v0    # "cmp":I
    .end local v3    # "mid":I
    :cond_3
    add-int/lit8 v4, v2, 0x1

    neg-int v3, v4

    goto :goto_0
.end method
