.class public Lorg/apache/lucene/search/QueryTermVector;
.super Ljava/lang/Object;
.source "QueryTermVector.java"

# interfaces
.implements Lorg/apache/lucene/index/TermFreqVector;


# instance fields
.field private termFreqs:[I

.field private terms:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 7
    .param p1, "queryString"    # Ljava/lang/String;
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    const/4 v6, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-array v5, v6, [Ljava/lang/String;

    iput-object v5, p0, Lorg/apache/lucene/search/QueryTermVector;->terms:[Ljava/lang/String;

    .line 41
    new-array v5, v6, [I

    iput-object v5, p0, Lorg/apache/lucene/search/QueryTermVector;->termFreqs:[I

    .line 55
    if-eqz p2, :cond_1

    .line 59
    :try_start_0
    const-string/jumbo v5, ""

    new-instance v6, Ljava/io/StringReader;

    invoke-direct {v6, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v5, v6}, Lorg/apache/lucene/analysis/Analyzer;->reusableTokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 63
    .local v2, "stream":Lorg/apache/lucene/analysis/TokenStream;
    :goto_0
    if-eqz v2, :cond_1

    .line 65
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v4, "terms":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 69
    .local v1, "hasMoreTokens":Z
    :try_start_1
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 70
    const-class v5, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v2, v5}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 72
    .local v3, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v1

    .line 73
    :goto_1
    if-eqz v1, :cond_0

    .line 74
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v1

    goto :goto_1

    .line 60
    .end local v1    # "hasMoreTokens":Z
    .end local v2    # "stream":Lorg/apache/lucene/analysis/TokenStream;
    .end local v3    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .end local v4    # "terms":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e1":Ljava/io/IOException;
    const/4 v2, 0x0

    .restart local v2    # "stream":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_0

    .line 77
    .end local v0    # "e1":Ljava/io/IOException;
    .restart local v1    # "hasMoreTokens":Z
    .restart local v3    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .restart local v4    # "terms":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    invoke-direct {p0, v5}, Lorg/apache/lucene/search/QueryTermVector;->processTerms([Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 82
    .end local v1    # "hasMoreTokens":Z
    .end local v2    # "stream":Lorg/apache/lucene/analysis/TokenStream;
    .end local v3    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .end local v4    # "terms":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    :goto_2
    return-void

    .line 78
    .restart local v1    # "hasMoreTokens":Z
    .restart local v2    # "stream":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v4    # "terms":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_1
    move-exception v5

    goto :goto_2
.end method

.method public constructor <init>([Ljava/lang/String;)V
    .locals 2
    .param p1, "queryTerms"    # [Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/search/QueryTermVector;->terms:[Ljava/lang/String;

    .line 41
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/search/QueryTermVector;->termFreqs:[I

    .line 51
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/QueryTermVector;->processTerms([Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method private processTerms([Ljava/lang/String;)V
    .locals 13
    .param p1, "queryTerms"    # [Ljava/lang/String;

    .prologue
    .line 85
    if-eqz p1, :cond_2

    .line 86
    invoke-static {p1}, Lorg/apache/lucene/util/ArrayUtil;->quickSort([Ljava/lang/Comparable;)V

    .line 87
    new-instance v10, Ljava/util/HashMap;

    array-length v11, p1

    invoke-direct {v10, v11}, Ljava/util/HashMap;-><init>(I)V

    .line 89
    .local v10, "tmpSet":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v9, Ljava/util/ArrayList;

    array-length v11, p1

    invoke-direct {v9, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 90
    .local v9, "tmpList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/ArrayList;

    array-length v11, p1

    invoke-direct {v8, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 91
    .local v8, "tmpFreqs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v4, 0x0

    .line 92
    .local v4, "j":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v11, p1

    if-ge v0, v11, :cond_1

    .line 93
    aget-object v7, p1, v0

    .line 94
    .local v7, "term":Ljava/lang/String;
    invoke-interface {v10, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 95
    .local v6, "position":Ljava/lang/Integer;
    if-nez v6, :cond_0

    .line 96
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "j":I
    .local v5, "j":I
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v10, v7, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v4, v5

    .line 92
    .end local v5    # "j":I
    .restart local v4    # "j":I
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_0
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 102
    .local v3, "integer":Ljava/lang/Integer;
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v12

    add-int/lit8 v12, v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v8, v11, v12}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 105
    .end local v3    # "integer":Ljava/lang/Integer;
    .end local v6    # "position":Ljava/lang/Integer;
    .end local v7    # "term":Ljava/lang/String;
    :cond_1
    iget-object v11, p0, Lorg/apache/lucene/search/QueryTermVector;->terms:[Ljava/lang/String;

    invoke-interface {v9, v11}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Ljava/lang/String;

    iput-object v11, p0, Lorg/apache/lucene/search/QueryTermVector;->terms:[Ljava/lang/String;

    .line 107
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v11

    new-array v11, v11, [I

    iput-object v11, p0, Lorg/apache/lucene/search/QueryTermVector;->termFreqs:[I

    .line 108
    const/4 v0, 0x0

    .line 109
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 110
    .restart local v3    # "integer":Ljava/lang/Integer;
    iget-object v11, p0, Lorg/apache/lucene/search/QueryTermVector;->termFreqs:[I

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v12

    aput v12, v11, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_2

    .line 113
    .end local v0    # "i":I
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "integer":Ljava/lang/Integer;
    .end local v4    # "j":I
    .end local v8    # "tmpFreqs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v9    # "tmpList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v10    # "tmpSet":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_2
    return-void
.end method


# virtual methods
.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTermFrequencies()[I
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lorg/apache/lucene/search/QueryTermVector;->termFreqs:[I

    return-object v0
.end method

.method public getTerms()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lorg/apache/lucene/search/QueryTermVector;->terms:[Ljava/lang/String;

    return-object v0
.end method

.method public indexOf(Ljava/lang/String;)I
    .locals 2
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    .line 141
    iget-object v1, p0, Lorg/apache/lucene/search/QueryTermVector;->terms:[Ljava/lang/String;

    invoke-static {v1, p1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 142
    .local v0, "res":I
    if-ltz v0, :cond_0

    .end local v0    # "res":I
    :goto_0
    return v0

    .restart local v0    # "res":I
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public indexesOf([Ljava/lang/String;II)[I
    .locals 3
    .param p1, "terms"    # [Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    .line 146
    new-array v1, p3, [I

    .line 148
    .local v1, "res":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_0

    .line 149
    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, Lorg/apache/lucene/search/QueryTermVector;->indexOf(Ljava/lang/String;)I

    move-result v2

    aput v2, v1, v0

    .line 148
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 151
    :cond_0
    return-object v1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/lucene/search/QueryTermVector;->terms:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 117
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/16 v2, 0x7b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 119
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/QueryTermVector;->terms:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 120
    if-lez v0, :cond_0

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/QueryTermVector;->terms:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/QueryTermVector;->termFreqs:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_1
    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
