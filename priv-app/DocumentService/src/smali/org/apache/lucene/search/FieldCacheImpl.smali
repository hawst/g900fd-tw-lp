.class Lorg/apache/lucene/search/FieldCacheImpl;
.super Ljava/lang/Object;
.source "FieldCacheImpl.java"

# interfaces
.implements Lorg/apache/lucene/search/FieldCache;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/FieldCacheImpl$StringIndexCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$StringCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$DoubleCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$LongCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$FloatCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$IntCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$ShortCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$ByteCache;,
        Lorg/apache/lucene/search/FieldCacheImpl$Entry;,
        Lorg/apache/lucene/search/FieldCacheImpl$Cache;,
        Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException;,
        Lorg/apache/lucene/search/FieldCacheImpl$CacheEntryImpl;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private caches:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/apache/lucene/search/FieldCacheImpl$Cache;",
            ">;"
        }
    .end annotation
.end field

.field private volatile infoStream:Ljava/io/PrintStream;

.field final purgeCore:Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

.field final purgeReader:Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lorg/apache/lucene/search/FieldCacheImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FieldCacheImpl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    new-instance v0, Lorg/apache/lucene/search/FieldCacheImpl$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/FieldCacheImpl$1;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    iput-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->purgeCore:Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    .line 148
    new-instance v0, Lorg/apache/lucene/search/FieldCacheImpl$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/FieldCacheImpl$2;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    iput-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->purgeReader:Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;

    .line 50
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldCacheImpl;->init()V

    .line 51
    return-void
.end method

.method private declared-synchronized init()V
    .locals 3

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    .line 54
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$ByteCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$ShortCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$ShortCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$IntCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$IntCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$LongCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$LongCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$DoubleCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$DoubleCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v1, Ljava/lang/String;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$StringCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$StringCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v1, Lorg/apache/lucene/search/FieldCache$StringIndex;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$StringIndexCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$StringIndexCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v1, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    monitor-exit p0

    return-void

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public getBytes(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[B
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 308
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lorg/apache/lucene/search/FieldCacheImpl;->getBytes(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Z)[B

    move-result-object v0

    return-object v0
.end method

.method public getBytes(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;)[B
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$ByteParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 314
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/apache/lucene/search/FieldCacheImpl;->getBytes(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Z)[B

    move-result-object v0

    return-object v0
.end method

.method public getBytes(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Z)[B
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$ByteParser;
    .param p4, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 319
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v1, Lorg/apache/lucene/search/FieldCacheImpl$Entry;

    invoke-direct {v1, p2, p3}, Lorg/apache/lucene/search/FieldCacheImpl$Entry;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1, p4}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public declared-synchronized getCacheEntries()[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    .locals 18

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    new-instance v16, Ljava/util/ArrayList;

    const/16 v1, 0x11

    move-object/from16 v0, v16

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 77
    .local v16, "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    .line 78
    .local v8, "cacheEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<*>;Lorg/apache/lucene/search/FieldCacheImpl$Cache;>;"
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    .line 79
    .local v7, "cache":Lorg/apache/lucene/search/FieldCacheImpl$Cache;
    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Class;

    .line 80
    .local v4, "cacheType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v0, v7, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    move-object/from16 v17, v0

    monitor-enter v17
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 81
    :try_start_1
    iget-object v1, v7, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/Map$Entry;

    .line 82
    .local v15, "readerCacheEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;>;"
    invoke-interface {v15}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 83
    .local v2, "readerKey":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 84
    invoke-interface {v15}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map;

    .line 85
    .local v13, "innerCache":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;"
    invoke-interface {v13}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Map$Entry;

    .line 86
    .local v14, "mapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;"
    invoke-interface {v14}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/search/FieldCacheImpl$Entry;

    .line 87
    .local v9, "entry":Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    new-instance v1, Lorg/apache/lucene/search/FieldCacheImpl$CacheEntryImpl;

    iget-object v3, v9, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->field:Ljava/lang/String;

    iget-object v5, v9, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->custom:Ljava/lang/Object;

    invoke-interface {v14}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lorg/apache/lucene/search/FieldCacheImpl$CacheEntryImpl;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 92
    .end local v2    # "readerKey":Ljava/lang/Object;
    .end local v9    # "entry":Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "innerCache":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;"
    .end local v14    # "mapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;"
    .end local v15    # "readerCacheEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;>;"
    :catchall_0
    move-exception v1

    monitor-exit v17
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 76
    .end local v4    # "cacheType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v7    # "cache":Lorg/apache/lucene/search/FieldCacheImpl$Cache;
    .end local v8    # "cacheEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<*>;Lorg/apache/lucene/search/FieldCacheImpl$Cache;>;"
    .end local v16    # "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1

    .line 92
    .restart local v4    # "cacheType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v7    # "cache":Lorg/apache/lucene/search/FieldCacheImpl$Cache;
    .restart local v8    # "cacheEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<*>;Lorg/apache/lucene/search/FieldCacheImpl$Cache;>;"
    .restart local v16    # "result":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/FieldCache$CacheEntry;>;"
    :cond_1
    :try_start_3
    monitor-exit v17
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 94
    .end local v4    # "cacheType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v7    # "cache":Lorg/apache/lucene/search/FieldCacheImpl$Cache;
    .end local v8    # "cacheEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<*>;Lorg/apache/lucene/search/FieldCacheImpl$Cache;>;"
    :cond_2
    :try_start_4
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/lucene/search/FieldCache$CacheEntry;

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/apache/lucene/search/FieldCache$CacheEntry;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    monitor-exit p0

    return-object v1
.end method

.method public getDocsWithField(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/util/Bits;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 536
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v1, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v1, Lorg/apache/lucene/search/FieldCacheImpl$Entry;

    const/4 v2, 0x0

    invoke-direct {v1, p2, v2}, Lorg/apache/lucene/search/FieldCacheImpl$Entry;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/Bits;

    return-object v0
.end method

.method public getDoubles(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[D
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 734
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lorg/apache/lucene/search/FieldCacheImpl;->getDoubles(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Z)[D

    move-result-object v0

    return-object v0
.end method

.method public getDoubles(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;)[D
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$DoubleParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 740
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/apache/lucene/search/FieldCacheImpl;->getDoubles(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Z)[D

    move-result-object v0

    return-object v0
.end method

.method public getDoubles(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Z)[D
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$DoubleParser;
    .param p4, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 746
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v1, Lorg/apache/lucene/search/FieldCacheImpl$Entry;

    invoke-direct {v1, p2, p3}, Lorg/apache/lucene/search/FieldCacheImpl$Entry;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1, p4}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [D

    check-cast v0, [D

    return-object v0
.end method

.method public getFloats(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[F
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 583
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lorg/apache/lucene/search/FieldCacheImpl;->getFloats(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Z)[F

    move-result-object v0

    return-object v0
.end method

.method public getFloats(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;)[F
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$FloatParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 589
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/apache/lucene/search/FieldCacheImpl;->getFloats(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Z)[F

    move-result-object v0

    return-object v0
.end method

.method public getFloats(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Z)[F
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$FloatParser;
    .param p4, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 595
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v1, Lorg/apache/lucene/search/FieldCacheImpl$Entry;

    invoke-direct {v1, p2, p3}, Lorg/apache/lucene/search/FieldCacheImpl$Entry;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1, p4}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    check-cast v0, [F

    return-object v0
.end method

.method public getInfoStream()Ljava/io/PrintStream;
    .locals 1

    .prologue
    .line 924
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->infoStream:Ljava/io/PrintStream;

    return-object v0
.end method

.method public getInts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[I
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 460
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/search/FieldCacheImpl;->getInts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;)[I

    move-result-object v0

    return-object v0
.end method

.method public getInts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;)[I
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$IntParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 466
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/apache/lucene/search/FieldCacheImpl;->getInts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;Z)[I

    move-result-object v0

    return-object v0
.end method

.method public getInts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$IntParser;Z)[I
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$IntParser;
    .param p4, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 472
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v1, Lorg/apache/lucene/search/FieldCacheImpl$Entry;

    invoke-direct {v1, p2, p3}, Lorg/apache/lucene/search/FieldCacheImpl$Entry;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1, p4}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    check-cast v0, [I

    return-object v0
.end method

.method public getLongs(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[J
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 658
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lorg/apache/lucene/search/FieldCacheImpl;->getLongs(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Z)[J

    move-result-object v0

    return-object v0
.end method

.method public getLongs(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;)[J
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$LongParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 664
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/apache/lucene/search/FieldCacheImpl;->getLongs(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Z)[J

    move-result-object v0

    return-object v0
.end method

.method public getLongs(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Z)[J
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$LongParser;
    .param p4, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 670
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v1, Lorg/apache/lucene/search/FieldCacheImpl$Entry;

    invoke-direct {v1, p2, p3}, Lorg/apache/lucene/search/FieldCacheImpl$Entry;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1, p4}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    check-cast v0, [J

    return-object v0
.end method

.method public getShorts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[S
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 373
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lorg/apache/lucene/search/FieldCacheImpl;->getShorts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;Z)[S

    move-result-object v0

    return-object v0
.end method

.method public getShorts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;)[S
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$ShortParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 379
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/apache/lucene/search/FieldCacheImpl;->getShorts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;Z)[S

    move-result-object v0

    return-object v0
.end method

.method public getShorts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;Z)[S
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$ShortParser;
    .param p4, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 385
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    sget-object v1, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v1, Lorg/apache/lucene/search/FieldCacheImpl$Entry;

    invoke-direct {v1, p2, p3}, Lorg/apache/lucene/search/FieldCacheImpl$Entry;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1, p4}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [S

    check-cast v0, [S

    return-object v0
.end method

.method public getStringIndex(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/search/FieldCache$StringIndex;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 856
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v1, Lorg/apache/lucene/search/FieldCache$StringIndex;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$Entry;

    const/4 v1, 0x0

    check-cast v1, Lorg/apache/lucene/search/FieldCache$Parser;

    invoke-direct {v2, p2, v1}, Lorg/apache/lucene/search/FieldCacheImpl$Entry;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v2, v1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldCache$StringIndex;

    return-object v0
.end method

.method public getStrings(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 811
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v1, Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v2, Lorg/apache/lucene/search/FieldCacheImpl$Entry;

    const/4 v1, 0x0

    check-cast v1, Lorg/apache/lucene/search/FieldCache$Parser;

    invoke-direct {v2, p2, v1}, Lorg/apache/lucene/search/FieldCacheImpl$Entry;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v2, v1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->get(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized purge(Lorg/apache/lucene/index/IndexReader;)V
    .locals 3
    .param p1, "r"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    .line 71
    .local v0, "c":Lorg/apache/lucene/search/FieldCacheImpl$Cache;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->purge(Lorg/apache/lucene/index/IndexReader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 70
    .end local v0    # "c":Lorg/apache/lucene/search/FieldCacheImpl$Cache;
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 73
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized purgeAllCaches()V
    .locals 1

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldCacheImpl;->init()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    monitor-exit p0

    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method setDocsWithField(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/util/Bits;)V
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "docsWithField"    # Lorg/apache/lucene/util/Bits;

    .prologue
    .line 439
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v1

    .line 441
    .local v1, "maxDoc":I
    if-nez p3, :cond_0

    .line 442
    new-instance v0, Lorg/apache/lucene/util/Bits$MatchNoBits;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/Bits$MatchNoBits;-><init>(I)V

    .line 455
    .local v0, "bits":Lorg/apache/lucene/util/Bits;
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheImpl;->caches:Ljava/util/Map;

    const-class v4, Lorg/apache/lucene/search/FieldCacheImpl$DocsWithFieldCache;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/FieldCacheImpl$Cache;

    new-instance v4, Lorg/apache/lucene/search/FieldCacheImpl$Entry;

    const/4 v5, 0x0

    invoke-direct {v4, p2, v5}, Lorg/apache/lucene/search/FieldCacheImpl$Entry;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v3, p1, v4, v0}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->put(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;)V

    .line 456
    return-void

    .line 443
    .end local v0    # "bits":Lorg/apache/lucene/util/Bits;
    :cond_0
    instance-of v3, p3, Lorg/apache/lucene/util/FixedBitSet;

    if-eqz v3, :cond_3

    move-object v3, p3

    .line 444
    check-cast v3, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {v3}, Lorg/apache/lucene/util/FixedBitSet;->cardinality()I

    move-result v2

    .line 445
    .local v2, "numSet":I
    if-lt v2, v1, :cond_2

    .line 447
    sget-boolean v3, Lorg/apache/lucene/search/FieldCacheImpl;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    if-eq v2, v1, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 448
    :cond_1
    new-instance v0, Lorg/apache/lucene/util/Bits$MatchAllBits;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/Bits$MatchAllBits;-><init>(I)V

    .restart local v0    # "bits":Lorg/apache/lucene/util/Bits;
    goto :goto_0

    .line 450
    .end local v0    # "bits":Lorg/apache/lucene/util/Bits;
    :cond_2
    move-object v0, p3

    .restart local v0    # "bits":Lorg/apache/lucene/util/Bits;
    goto :goto_0

    .line 453
    .end local v0    # "bits":Lorg/apache/lucene/util/Bits;
    .end local v2    # "numSet":I
    :cond_3
    move-object v0, p3

    .restart local v0    # "bits":Lorg/apache/lucene/util/Bits;
    goto :goto_0
.end method

.method public setInfoStream(Ljava/io/PrintStream;)V
    .locals 0
    .param p1, "stream"    # Ljava/io/PrintStream;

    .prologue
    .line 920
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl;->infoStream:Ljava/io/PrintStream;

    .line 921
    return-void
.end method
