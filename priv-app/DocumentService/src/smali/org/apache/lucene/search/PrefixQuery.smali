.class public Lorg/apache/lucene/search/PrefixQuery;
.super Lorg/apache/lucene/search/MultiTermQuery;
.source "PrefixQuery.java"


# instance fields
.field private prefix:Lorg/apache/lucene/index/Term;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 0
    .param p1, "prefix"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/lucene/search/MultiTermQuery;-><init>()V

    .line 37
    iput-object p1, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    .line 38
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    if-ne p0, p1, :cond_1

    .line 84
    :cond_0
    :goto_0
    return v1

    .line 74
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 75
    goto :goto_0

    .line 76
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 77
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 78
    check-cast v0, Lorg/apache/lucene/search/PrefixQuery;

    .line 79
    .local v0, "other":Lorg/apache/lucene/search/PrefixQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    if-nez v3, :cond_4

    .line 80
    iget-object v3, v0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    if-eqz v3, :cond_0

    move v1, v2

    .line 81
    goto :goto_0

    .line 82
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    iget-object v4, v0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 83
    goto :goto_0
.end method

.method protected getEnum(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/FilteredTermEnum;
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Lorg/apache/lucene/search/PrefixTermEnum;

    iget-object v1, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/search/PrefixTermEnum;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/Term;)V

    return-object v0
.end method

.method public getPrefix()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 64
    const/16 v0, 0x1f

    .line 65
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v1

    .line 66
    .local v1, "result":I
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v3, v2

    .line 67
    return v1

    .line 66
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 53
    iget-object v1, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/PrefixQuery;->prefix:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 58
    invoke-virtual {p0}, Lorg/apache/lucene/search/PrefixQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
