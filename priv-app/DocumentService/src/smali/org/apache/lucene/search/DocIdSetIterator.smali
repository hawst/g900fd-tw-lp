.class public abstract Lorg/apache/lucene/search/DocIdSetIterator;
.super Ljava/lang/Object;
.source "DocIdSetIterator.java"


# static fields
.field public static final NO_MORE_DOCS:I = 0x7fffffff


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract advance(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract docID()I
.end method

.method public abstract nextDoc()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
