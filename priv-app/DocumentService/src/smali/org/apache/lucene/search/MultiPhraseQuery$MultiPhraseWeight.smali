.class Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;
.super Lorg/apache/lucene/search/Weight;
.source "MultiPhraseQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MultiPhraseQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MultiPhraseWeight"
.end annotation


# instance fields
.field private idf:F

.field private final idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

.field private queryNorm:F

.field private queryWeight:F

.field private similarity:Lorg/apache/lucene/search/Similarity;

.field final synthetic this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

.field private value:F


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/MultiPhraseQuery;Lorg/apache/lucene/search/Searcher;)V
    .locals 8
    .param p2, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    iput-object p1, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 138
    invoke-virtual {p1, p2}, Lorg/apache/lucene/search/MultiPhraseQuery;->getSimilarity(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Similarity;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 142
    .local v0, "allTerms":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/index/Term;>;"
    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;
    invoke-static {p1}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$000(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lorg/apache/lucene/index/Term;

    .line 143
    .local v6, "terms":[Lorg/apache/lucene/index/Term;
    move-object v1, v6

    .local v1, "arr$":[Lorg/apache/lucene/index/Term;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v1, v3

    .line 144
    .local v5, "term":Lorg/apache/lucene/index/Term;
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 147
    .end local v1    # "arr$":[Lorg/apache/lucene/index/Term;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "term":Lorg/apache/lucene/index/Term;
    .end local v6    # "terms":[Lorg/apache/lucene/index/Term;
    :cond_1
    iget-object v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    invoke-virtual {v7, v0, p2}, Lorg/apache/lucene/search/Similarity;->idfExplain(Ljava/util/Collection;Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Explanation$IDFExplanation;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

    .line 148
    iget-object v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

    invoke-virtual {v7}, Lorg/apache/lucene/search/Explanation$IDFExplanation;->getIdf()F

    move-result v7

    iput v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->idf:F

    .line 149
    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;
    .locals 20
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 225
    new-instance v14, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v14}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 226
    .local v14, "result":Lorg/apache/lucene/search/ComplexExplanation;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "weight("

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " in "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, "), product of:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 228
    new-instance v10, Lorg/apache/lucene/search/Explanation;

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->idf:F

    move/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "idf("

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    move-object/from16 v19, v0

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$300(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, ":"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/search/Explanation$IDFExplanation;->explain()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, ")"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v10, v0, v1}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 231
    .local v10, "idfExpl":Lorg/apache/lucene/search/Explanation;
    new-instance v12, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v12}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 232
    .local v12, "queryExpl":Lorg/apache/lucene/search/Explanation;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "queryWeight("

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, "), product of:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 234
    new-instance v4, Lorg/apache/lucene/search/Explanation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v17

    const-string/jumbo v18, "boost"

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v4, v0, v1}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 235
    .local v4, "boostExpl":Lorg/apache/lucene/search/Explanation;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v17

    const/high16 v18, 0x3f800000    # 1.0f

    cmpl-float v17, v17, v18

    if-eqz v17, :cond_0

    .line 236
    invoke-virtual {v12, v4}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 238
    :cond_0
    invoke-virtual {v12, v10}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 240
    new-instance v13, Lorg/apache/lucene/search/Explanation;

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->queryNorm:F

    move/from16 v17, v0

    const-string/jumbo v18, "queryNorm"

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v13, v0, v1}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 241
    .local v13, "queryNormExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v12, v13}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 243
    invoke-virtual {v4}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v17

    invoke-virtual {v10}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v18

    mul-float v17, v17, v18

    invoke-virtual {v13}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v18

    mul-float v17, v17, v18

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 247
    invoke-virtual {v14, v12}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 250
    new-instance v6, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v6}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 251
    .local v6, "fieldExpl":Lorg/apache/lucene/search/ComplexExplanation;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "fieldWeight("

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " in "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, "), product of:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 254
    const/16 v17, 0x1

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;

    move-result-object v15

    .line 255
    .local v15, "scorer":Lorg/apache/lucene/search/Scorer;
    if-nez v15, :cond_2

    .line 256
    new-instance v6, Lorg/apache/lucene/search/Explanation;

    .end local v6    # "fieldExpl":Lorg/apache/lucene/search/ComplexExplanation;
    const/16 v17, 0x0

    const-string/jumbo v18, "no matching docs"

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v6, v0, v1}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 295
    :cond_1
    :goto_0
    return-object v6

    .line 259
    .restart local v6    # "fieldExpl":Lorg/apache/lucene/search/ComplexExplanation;
    :cond_2
    new-instance v16, Lorg/apache/lucene/search/Explanation;

    invoke-direct/range {v16 .. v16}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 260
    .local v16, "tfExplanation":Lorg/apache/lucene/search/Explanation;
    move/from16 v0, p2

    invoke-virtual {v15, v0}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v5

    .line 262
    .local v5, "d":I
    move/from16 v0, p2

    if-ne v5, v0, :cond_3

    .line 263
    invoke-virtual {v15}, Lorg/apache/lucene/search/Scorer;->freq()F

    move-result v11

    .line 268
    .local v11, "phraseFreq":F
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lorg/apache/lucene/search/Similarity;->tf(F)F

    move-result v17

    invoke-virtual/range {v16 .. v17}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 269
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "tf(phraseFreq="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, ")"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 270
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 271
    invoke-virtual {v6, v10}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 273
    new-instance v8, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v8}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 274
    .local v8, "fieldNormExpl":Lorg/apache/lucene/search/Explanation;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    move-object/from16 v17, v0

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$300(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v9

    .line 275
    .local v9, "fieldNorms":[B
    if-eqz v9, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    move-object/from16 v17, v0

    aget-byte v18, v9, p2

    invoke-virtual/range {v17 .. v18}, Lorg/apache/lucene/search/Similarity;->decodeNormValue(B)F

    move-result v7

    .line 277
    .local v7, "fieldNorm":F
    :goto_2
    invoke-virtual {v8, v7}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 278
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "fieldNorm(field="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    move-object/from16 v18, v0

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$300(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, ", doc="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, ")"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 279
    invoke-virtual {v6, v8}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 281
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/search/Explanation;->isMatch()Z

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 282
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v17

    invoke-virtual {v10}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v18

    mul-float v17, v17, v18

    invoke-virtual {v8}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v18

    mul-float v17, v17, v18

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 286
    invoke-virtual {v14, v6}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 287
    invoke-virtual {v6}, Lorg/apache/lucene/search/ComplexExplanation;->getMatch()Ljava/lang/Boolean;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 290
    invoke-virtual {v12}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v17

    invoke-virtual {v6}, Lorg/apache/lucene/search/ComplexExplanation;->getValue()F

    move-result v18

    mul-float v17, v17, v18

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 292
    invoke-virtual {v12}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v17

    const/high16 v18, 0x3f800000    # 1.0f

    cmpl-float v17, v17, v18

    if-eqz v17, :cond_1

    move-object v6, v14

    .line 295
    goto/16 :goto_0

    .line 265
    .end local v7    # "fieldNorm":F
    .end local v8    # "fieldNormExpl":Lorg/apache/lucene/search/Explanation;
    .end local v9    # "fieldNorms":[B
    .end local v11    # "phraseFreq":F
    :cond_3
    const/4 v11, 0x0

    .restart local v11    # "phraseFreq":F
    goto/16 :goto_1

    .line 275
    .restart local v8    # "fieldNormExpl":Lorg/apache/lucene/search/Explanation;
    .restart local v9    # "fieldNorms":[B
    :cond_4
    const/high16 v7, 0x3f800000    # 1.0f

    goto/16 :goto_2
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->value:F

    return v0
.end method

.method public normalize(F)V
    .locals 2
    .param p1, "queryNorm"    # F

    .prologue
    .line 165
    iput p1, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->queryNorm:F

    .line 166
    iget v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->queryWeight:F

    mul-float/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->queryWeight:F

    .line 167
    iget v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->idf:F

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->value:F

    .line 168
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;
    .locals 12
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 172
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$000(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    move-object v9, v1

    .line 217
    :cond_0
    :goto_0
    return-object v9

    .line 175
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$000(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;

    .line 177
    .local v2, "postingsFreqs":[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
    const/4 v8, 0x0

    .local v8, "pos":I
    :goto_1
    array-length v0, v2

    if-ge v8, v0, :cond_4

    .line 178
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$000(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Lorg/apache/lucene/index/Term;

    .line 183
    .local v11, "terms":[Lorg/apache/lucene/index/Term;
    array-length v0, v11

    const/4 v3, 0x1

    if-le v0, v3, :cond_2

    .line 184
    new-instance v7, Lorg/apache/lucene/index/MultipleTermPositions;

    invoke-direct {v7, p1, v11}, Lorg/apache/lucene/index/MultipleTermPositions;-><init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/Term;)V

    .line 188
    .local v7, "p":Lorg/apache/lucene/index/TermPositions;
    const/4 v6, 0x0

    .line 189
    .local v6, "docFreq":I
    const/4 v10, 0x0

    .local v10, "termIdx":I
    :goto_2
    array-length v0, v11

    if-ge v10, v0, :cond_3

    .line 190
    aget-object v0, v11, v10

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v0

    add-int/2addr v6, v0

    .line 189
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 193
    .end local v6    # "docFreq":I
    .end local v7    # "p":Lorg/apache/lucene/index/TermPositions;
    .end local v10    # "termIdx":I
    :cond_2
    aget-object v0, v11, v4

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/IndexReader;->termPositions(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermPositions;

    move-result-object v7

    .line 194
    .restart local v7    # "p":Lorg/apache/lucene/index/TermPositions;
    aget-object v0, v11, v4

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v6

    .line 196
    .restart local v6    # "docFreq":I
    if-nez v7, :cond_3

    move-object v9, v1

    .line 197
    goto :goto_0

    .line 200
    :cond_3
    new-instance v3, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;

    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;
    invoke-static {v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$100(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v3, v7, v6, v0, v11}, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;-><init>(Lorg/apache/lucene/index/TermPositions;II[Lorg/apache/lucene/index/Term;)V

    aput-object v3, v2, v8

    .line 177
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 204
    .end local v6    # "docFreq":I
    .end local v7    # "p":Lorg/apache/lucene/index/TermPositions;
    .end local v11    # "terms":[Lorg/apache/lucene/index/Term;
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I
    invoke-static {v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$200(Lorg/apache/lucene/search/MultiPhraseQuery;)I

    move-result v0

    if-nez v0, :cond_5

    .line 205
    invoke-static {v2}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Comparable;)V

    .line 208
    :cond_5
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I
    invoke-static {v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$200(Lorg/apache/lucene/search/MultiPhraseQuery;)I

    move-result v0

    if-nez v0, :cond_6

    .line 209
    new-instance v9, Lorg/apache/lucene/search/ExactPhraseScorer;

    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    iget-object v3, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;
    invoke-static {v3}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$300(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v3

    invoke-direct {v9, p0, v2, v0, v3}, Lorg/apache/lucene/search/ExactPhraseScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;Lorg/apache/lucene/search/Similarity;[B)V

    .line 211
    .local v9, "s":Lorg/apache/lucene/search/ExactPhraseScorer;
    iget-boolean v0, v9, Lorg/apache/lucene/search/ExactPhraseScorer;->noDocs:Z

    if-eqz v0, :cond_0

    move-object v9, v1

    .line 212
    goto/16 :goto_0

    .line 217
    .end local v9    # "s":Lorg/apache/lucene/search/ExactPhraseScorer;
    :cond_6
    new-instance v0, Lorg/apache/lucene/search/SloppyPhraseScorer;

    iget-object v3, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I
    invoke-static {v1}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$200(Lorg/apache/lucene/search/MultiPhraseQuery;)I

    move-result v4

    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    # getter for: Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;
    invoke-static {v1}, Lorg/apache/lucene/search/MultiPhraseQuery;->access$300(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/SloppyPhraseScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;Lorg/apache/lucene/search/Similarity;I[B)V

    move-object v9, v0

    goto/16 :goto_0
.end method

.method public sumOfSquaredWeights()F
    .locals 2

    .prologue
    .line 159
    iget v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->idf:F

    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->this$0:Lorg/apache/lucene/search/MultiPhraseQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->queryWeight:F

    .line 160
    iget v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;->queryWeight:F

    mul-float/2addr v0, v1

    return v0
.end method
