.class Lorg/apache/lucene/search/FilteredQuery$1;
.super Lorg/apache/lucene/search/Weight;
.source "FilteredQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FilteredQuery;->createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/FilteredQuery;

.field final synthetic val$similarity:Lorg/apache/lucene/search/Similarity;

.field final synthetic val$weight:Lorg/apache/lucene/search/Weight;

.field private value:F


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FilteredQuery;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Similarity;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    iput-object p1, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    iput-object p2, p0, Lorg/apache/lucene/search/FilteredQuery$1;->val$weight:Lorg/apache/lucene/search/Weight;

    iput-object p3, p0, Lorg/apache/lucene/search/FilteredQuery$1;->val$similarity:Lorg/apache/lucene/search/Similarity;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;
    .locals 8
    .param p1, "ir"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v5, p0, Lorg/apache/lucene/search/FilteredQuery$1;->val$weight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v5, p1, p2}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v3

    .line 90
    .local v3, "inner":Lorg/apache/lucene/search/Explanation;
    iget-object v5, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    iget-object v2, v5, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    .line 91
    .local v2, "f":Lorg/apache/lucene/search/Filter;
    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Filter;->getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v0

    .line 92
    .local v0, "docIdSet":Lorg/apache/lucene/search/DocIdSet;
    if-nez v0, :cond_1

    sget-object v5, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v5}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    .line 93
    .local v1, "docIdSetIterator":Lorg/apache/lucene/search/DocIdSetIterator;
    :goto_0
    if-nez v1, :cond_0

    .line 94
    sget-object v5, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    invoke-virtual {v5}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    .line 96
    :cond_0
    invoke-virtual {v1, p2}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v5

    if-ne v5, p2, :cond_2

    .line 102
    .end local v3    # "inner":Lorg/apache/lucene/search/Explanation;
    :goto_1
    return-object v3

    .line 92
    .end local v1    # "docIdSetIterator":Lorg/apache/lucene/search/DocIdSetIterator;
    .restart local v3    # "inner":Lorg/apache/lucene/search/Explanation;
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    goto :goto_0

    .line 99
    .restart local v1    # "docIdSetIterator":Lorg/apache/lucene/search/DocIdSetIterator;
    :cond_2
    new-instance v4, Lorg/apache/lucene/search/Explanation;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "failure to match filter: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 101
    .local v4, "result":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v4, v3}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    move-object v3, v4

    .line 102
    goto :goto_1
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lorg/apache/lucene/search/FilteredQuery$1;->value:F

    return v0
.end method

.method public normalize(F)V
    .locals 2
    .param p1, "v"    # F

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$1;->val$weight:Lorg/apache/lucene/search/Weight;

    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/FilteredQuery;->getBoost()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/Weight;->normalize(F)V

    .line 84
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$1;->val$weight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Weight;->getValue()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/FilteredQuery$1;->value:F

    .line 85
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;
    .locals 3
    .param p1, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$1;->val$similarity:Lorg/apache/lucene/search/Similarity;

    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$1;->val$weight:Lorg/apache/lucene/search/Weight;

    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    iget-object v2, v2, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    invoke-static {p1, v0, v1, p0, v2}, Lorg/apache/lucene/search/FilteredQuery;->getFilteredScorer(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    return-object v0
.end method

.method public scoresDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public sumOfSquaredWeights()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery$1;->val$weight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Weight;->sumOfSquaredWeights()F

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/FilteredQuery;->getBoost()F

    move-result v1

    mul-float/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery$1;->this$0:Lorg/apache/lucene/search/FilteredQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/FilteredQuery;->getBoost()F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method
