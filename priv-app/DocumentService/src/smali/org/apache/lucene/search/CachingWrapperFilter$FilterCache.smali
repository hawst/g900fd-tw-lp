.class abstract Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;
.super Ljava/lang/Object;
.source "CachingWrapperFilter.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/CachingWrapperFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "FilterCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field transient cache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "TT;>;"
        }
    .end annotation
.end field

.field private final deletesMode:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lorg/apache/lucene/search/CachingWrapperFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;)V
    .locals 0
    .param p1, "deletesMode"    # Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    .prologue
    .line 77
    .local p0, "this":Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;, "Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->deletesMode:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    .line 79
    return-void
.end method


# virtual methods
.method public declared-synchronized get(Lorg/apache/lucene/index/IndexReader;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "coreKey"    # Ljava/lang/Object;
    .param p3, "delCoreKey"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/IndexReader;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    .local p0, "this":Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;, "Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache<TT;>;"
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->cache:Ljava/util/Map;

    if-nez v1, :cond_0

    .line 85
    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->cache:Ljava/util/Map;

    .line 88
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->deletesMode:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    sget-object v2, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->IGNORE:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    if-ne v1, v2, :cond_2

    .line 90
    iget-object v1, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->cache:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 110
    .local v0, "value":Ljava/lang/Object;, "TT;"
    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    .line 91
    .end local v0    # "value":Ljava/lang/Object;, "TT;"
    :cond_2
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->deletesMode:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    sget-object v2, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->RECACHE:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    if-ne v1, v2, :cond_3

    .line 93
    iget-object v1, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->cache:Ljava/util/Map;

    invoke-interface {v1, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .restart local v0    # "value":Ljava/lang/Object;, "TT;"
    goto :goto_0

    .line 95
    .end local v0    # "value":Ljava/lang/Object;, "TT;"
    :cond_3
    sget-boolean v1, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->deletesMode:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    sget-object v2, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->DYNAMIC:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    if-eq v1, v2, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 98
    :cond_4
    :try_start_2
    iget-object v1, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->cache:Ljava/util/Map;

    invoke-interface {v1, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 100
    .restart local v0    # "value":Ljava/lang/Object;, "TT;"
    if-nez v0, :cond_1

    .line 103
    iget-object v1, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->cache:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 105
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->mergeDeletes(Lorg/apache/lucene/index/IndexReader;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract mergeDeletes(Lorg/apache/lucene/index/IndexReader;Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/IndexReader;",
            "TT;)TT;"
        }
    .end annotation
.end method

.method public declared-synchronized put(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "coreKey"    # Ljava/lang/Object;
    .param p2, "delCoreKey"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p0, "this":Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;, "Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache<TT;>;"
    .local p3, "value":Ljava/lang/Object;, "TT;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->deletesMode:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    sget-object v1, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->IGNORE:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    if-ne v0, v1, :cond_0

    .line 117
    iget-object v0, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->cache:Ljava/util/Map;

    invoke-interface {v0, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    :goto_0
    monitor-exit p0

    return-void

    .line 118
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->deletesMode:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    sget-object v1, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->RECACHE:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    if-ne v0, v1, :cond_1

    .line 119
    iget-object v0, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->cache:Ljava/util/Map;

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 121
    :cond_1
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->cache:Ljava/util/Map;

    invoke-interface {v0, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    iget-object v0, p0, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->cache:Ljava/util/Map;

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
