.class public Lorg/apache/lucene/search/PrefixTermEnum;
.super Lorg/apache/lucene/search/FilteredTermEnum;
.source "PrefixTermEnum.java"


# instance fields
.field private endEnum:Z

.field private final prefix:Lorg/apache/lucene/index/Term;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "prefix"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/search/FilteredTermEnum;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/PrefixTermEnum;->endEnum:Z

    .line 39
    iput-object p2, p0, Lorg/apache/lucene/search/PrefixTermEnum;->prefix:Lorg/apache/lucene/index/Term;

    .line 41
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-virtual {p2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/PrefixTermEnum;->setEnum(Lorg/apache/lucene/index/TermEnum;)V

    .line 42
    return-void
.end method


# virtual methods
.method public difference()F
    .locals 1

    .prologue
    .line 46
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method protected endEnum()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lorg/apache/lucene/search/PrefixTermEnum;->endEnum:Z

    return v0
.end method

.method protected getPrefixTerm()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/lucene/search/PrefixTermEnum;->prefix:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method protected termCompare(Lorg/apache/lucene/index/Term;)Z
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    const/4 v0, 0x1

    .line 60
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/PrefixTermEnum;->prefix:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/PrefixTermEnum;->prefix:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    :goto_0
    return v0

    .line 63
    :cond_0
    iput-boolean v0, p0, Lorg/apache/lucene/search/PrefixTermEnum;->endEnum:Z

    .line 64
    const/4 v0, 0x0

    goto :goto_0
.end method
