.class public Lorg/apache/lucene/search/TermRangeTermEnum;
.super Lorg/apache/lucene/search/FilteredTermEnum;
.source "TermRangeTermEnum.java"


# instance fields
.field private collator:Ljava/text/Collator;

.field private endEnum:Z

.field private field:Ljava/lang/String;

.field private includeLower:Z

.field private includeUpper:Z

.field private lowerTermText:Ljava/lang/String;

.field private upperTermText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/text/Collator;)V
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "lowerTermText"    # Ljava/lang/String;
    .param p4, "upperTermText"    # Ljava/lang/String;
    .param p5, "includeLower"    # Z
    .param p6, "includeUpper"    # Z
    .param p7, "collator"    # Ljava/text/Collator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 73
    invoke-direct {p0}, Lorg/apache/lucene/search/FilteredTermEnum;-><init>()V

    .line 37
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->collator:Ljava/text/Collator;

    .line 38
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->endEnum:Z

    .line 74
    iput-object p7, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->collator:Ljava/text/Collator;

    .line 75
    iput-object p4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->upperTermText:Ljava/lang/String;

    .line 76
    iput-object p3, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->lowerTermText:Ljava/lang/String;

    .line 77
    iput-boolean p5, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->includeLower:Z

    .line 78
    iput-boolean p6, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->includeUpper:Z

    .line 79
    invoke-static {p2}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->field:Ljava/lang/String;

    .line 83
    iget-object v1, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->lowerTermText:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 84
    const-string/jumbo v1, ""

    iput-object v1, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->lowerTermText:Ljava/lang/String;

    .line 85
    iput-boolean v2, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->includeLower:Z

    .line 88
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->upperTermText:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 89
    iput-boolean v2, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->includeUpper:Z

    .line 92
    :cond_1
    if-nez p7, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->lowerTermText:Ljava/lang/String;

    .line 93
    .local v0, "startTermText":Ljava/lang/String;
    :goto_0
    new-instance v1, Lorg/apache/lucene/index/Term;

    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->field:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/TermRangeTermEnum;->setEnum(Lorg/apache/lucene/index/TermEnum;)V

    .line 94
    return-void

    .line 92
    .end local v0    # "startTermText":Ljava/lang/String;
    :cond_2
    const-string/jumbo v0, ""

    goto :goto_0
.end method


# virtual methods
.method public difference()F
    .locals 1

    .prologue
    .line 98
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method protected endEnum()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->endEnum:Z

    return v0
.end method

.method protected termCompare(Lorg/apache/lucene/index/Term;)Z
    .locals 7
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 108
    iget-object v4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->collator:Ljava/text/Collator;

    if-nez v4, :cond_6

    .line 110
    const/4 v0, 0x0

    .line 111
    .local v0, "checkLower":Z
    iget-boolean v4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->includeLower:Z

    if-nez v4, :cond_0

    .line 112
    const/4 v0, 0x1

    .line 113
    :cond_0
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->field:Ljava/lang/String;

    if-ne v4, v5, :cond_5

    .line 114
    if-eqz v0, :cond_1

    iget-object v4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->lowerTermText:Ljava/lang/String;

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->lowerTermText:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_3

    .line 115
    :cond_1
    const/4 v0, 0x0

    .line 116
    iget-object v4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->upperTermText:Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 117
    iget-object v4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->upperTermText:Ljava/lang/String;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    .line 122
    .local v1, "compare":I
    if-ltz v1, :cond_2

    iget-boolean v4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->includeUpper:Z

    if-nez v4, :cond_4

    if-nez v1, :cond_4

    .line 124
    :cond_2
    iput-boolean v3, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->endEnum:Z

    .line 151
    .end local v0    # "checkLower":Z
    .end local v1    # "compare":I
    :cond_3
    :goto_0
    return v2

    .restart local v0    # "checkLower":Z
    :cond_4
    move v2, v3

    .line 128
    goto :goto_0

    .line 132
    :cond_5
    iput-boolean v3, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->endEnum:Z

    goto :goto_0

    .line 137
    .end local v0    # "checkLower":Z
    :cond_6
    if-eqz p1, :cond_b

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->field:Ljava/lang/String;

    if-ne v4, v5, :cond_b

    .line 138
    iget-object v4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->lowerTermText:Ljava/lang/String;

    if-eqz v4, :cond_7

    iget-boolean v4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->includeLower:Z

    if-eqz v4, :cond_9

    iget-object v4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->collator:Ljava/text/Collator;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->lowerTermText:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_3

    :cond_7
    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->upperTermText:Ljava/lang/String;

    if-eqz v4, :cond_8

    iget-boolean v4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->includeUpper:Z

    if-eqz v4, :cond_a

    iget-object v4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->collator:Ljava/text/Collator;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->upperTermText:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-gtz v4, :cond_3

    :cond_8
    :goto_2
    move v2, v3

    .line 146
    goto :goto_0

    .line 138
    :cond_9
    iget-object v4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->collator:Ljava/text/Collator;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->lowerTermText:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_3

    goto :goto_1

    :cond_a
    iget-object v4, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->collator:Ljava/text/Collator;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->upperTermText:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_3

    goto :goto_2

    .line 150
    :cond_b
    iput-boolean v3, p0, Lorg/apache/lucene/search/TermRangeTermEnum;->endEnum:Z

    goto :goto_0
.end method
