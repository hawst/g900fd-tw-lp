.class Lorg/apache/lucene/search/FieldCacheRangeFilter$1$1;
.super Lorg/apache/lucene/search/FieldCacheDocIdSet;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/FieldCacheRangeFilter$1;

.field final synthetic val$fcsi:Lorg/apache/lucene/search/FieldCache$StringIndex;

.field final synthetic val$inclusiveLowerPoint:I

.field final synthetic val$inclusiveUpperPoint:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$1;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCache$StringIndex;II)V
    .locals 0
    .param p2, "x0"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 123
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1$1;->this$0:Lorg/apache/lucene/search/FieldCacheRangeFilter$1;

    iput-object p3, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1$1;->val$fcsi:Lorg/apache/lucene/search/FieldCache$StringIndex;

    iput p4, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1$1;->val$inclusiveLowerPoint:I

    iput p5, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1$1;->val$inclusiveUpperPoint:I

    invoke-direct {p0, p2}, Lorg/apache/lucene/search/FieldCacheDocIdSet;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    return-void
.end method


# virtual methods
.method protected final matchDoc(I)Z
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 124
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1$1;->val$fcsi:Lorg/apache/lucene/search/FieldCache$StringIndex;

    iget-object v0, v0, Lorg/apache/lucene/search/FieldCache$StringIndex;->order:[I

    aget v0, v0, p1

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1$1;->val$inclusiveLowerPoint:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1$1;->val$fcsi:Lorg/apache/lucene/search/FieldCache$StringIndex;

    iget-object v0, v0, Lorg/apache/lucene/search/FieldCache$StringIndex;->order:[I

    aget v0, v0, p1

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1$1;->val$inclusiveUpperPoint:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
