.class Lorg/apache/lucene/search/MultiSearcher$CachedDfSource;
.super Lorg/apache/lucene/search/Searcher;
.source "MultiSearcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MultiSearcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CachedDfSource"
.end annotation


# instance fields
.field private final dfMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final maxDoc:I


# direct methods
.method public constructor <init>(Ljava/util/Map;ILorg/apache/lucene/search/Similarity;)V
    .locals 0
    .param p2, "maxDoc"    # I
    .param p3, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;I",
            "Lorg/apache/lucene/search/Similarity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, "dfMap":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/Searcher;-><init>()V

    .line 59
    iput-object p1, p0, Lorg/apache/lucene/search/MultiSearcher$CachedDfSource;->dfMap:Ljava/util/Map;

    .line 60
    iput p2, p0, Lorg/apache/lucene/search/MultiSearcher$CachedDfSource;->maxDoc:I

    .line 61
    invoke-virtual {p0, p3}, Lorg/apache/lucene/search/MultiSearcher$CachedDfSource;->setSimilarity(Lorg/apache/lucene/search/Similarity;)V

    .line 62
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 101
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public doc(I)Lorg/apache/lucene/document/Document;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 106
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public doc(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;
    .locals 1
    .param p1, "i"    # I
    .param p2, "fieldSelector"    # Lorg/apache/lucene/document/FieldSelector;

    .prologue
    .line 111
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public docFreq(Lorg/apache/lucene/index/Term;)I
    .locals 5
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 68
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/search/MultiSearcher$CachedDfSource;->dfMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 73
    .local v0, "df":I
    return v0

    .line 69
    .end local v0    # "df":I
    :catch_0
    move-exception v1

    .line 70
    .local v1, "e":Ljava/lang/NullPointerException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "df for term "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " not available"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public docFreqs([Lorg/apache/lucene/index/Term;)[I
    .locals 3
    .param p1, "terms"    # [Lorg/apache/lucene/index/Term;

    .prologue
    .line 78
    array-length v2, p1

    new-array v1, v2, [I

    .line 79
    .local v1, "result":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 80
    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, Lorg/apache/lucene/search/MultiSearcher$CachedDfSource;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v2

    aput v2, v1, v0

    .line 79
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82
    :cond_0
    return-object v1
.end method

.method public explain(Lorg/apache/lucene/search/Weight;I)Lorg/apache/lucene/search/Explanation;
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "doc"    # I

    .prologue
    .line 116
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public maxDoc()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lorg/apache/lucene/search/MultiSearcher$CachedDfSource;->maxDoc:I

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Query;
    .locals 0
    .param p1, "query"    # Lorg/apache/lucene/search/Query;

    .prologue
    .line 96
    return-object p1
.end method

.method public search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "n"    # I

    .prologue
    .line 126
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "n"    # I
    .param p4, "sort"    # Lorg/apache/lucene/search/Sort;

    .prologue
    .line 131
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "results"    # Lorg/apache/lucene/search/Collector;

    .prologue
    .line 121
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
