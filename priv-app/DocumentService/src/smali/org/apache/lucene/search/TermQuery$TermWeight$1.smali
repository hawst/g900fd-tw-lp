.class Lorg/apache/lucene/search/TermQuery$TermWeight$1;
.super Lorg/apache/lucene/util/ReaderUtil$Gather;
.source "TermQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/TermQuery$TermWeight;-><init>(Lorg/apache/lucene/search/TermQuery;Lorg/apache/lucene/search/Searcher;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/search/TermQuery$TermWeight;

.field final synthetic val$dfSum:[I

.field final synthetic val$this$0:Lorg/apache/lucene/search/TermQuery;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/TermQuery$TermWeight;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/TermQuery;[I)V
    .locals 0
    .param p2, "x0"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    iput-object p1, p0, Lorg/apache/lucene/search/TermQuery$TermWeight$1;->this$1:Lorg/apache/lucene/search/TermQuery$TermWeight;

    iput-object p3, p0, Lorg/apache/lucene/search/TermQuery$TermWeight$1;->val$this$0:Lorg/apache/lucene/search/TermQuery;

    iput-object p4, p0, Lorg/apache/lucene/search/TermQuery$TermWeight$1;->val$dfSum:[I

    invoke-direct {p0, p2}, Lorg/apache/lucene/util/ReaderUtil$Gather;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    return-void
.end method


# virtual methods
.method protected add(ILorg/apache/lucene/index/IndexReader;)V
    .locals 4
    .param p1, "base"    # I
    .param p2, "r"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery$TermWeight$1;->this$1:Lorg/apache/lucene/search/TermQuery$TermWeight;

    iget-object v1, v1, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v1}, Lorg/apache/lucene/search/TermQuery;->access$000(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v1

    invoke-virtual {p2, v1}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v0

    .line 57
    .local v0, "df":I
    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery$TermWeight$1;->val$dfSum:[I

    const/4 v2, 0x0

    aget v3, v1, v2

    add-int/2addr v3, v0

    aput v3, v1, v2

    .line 58
    if-lez v0, :cond_0

    .line 59
    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery$TermWeight$1;->this$1:Lorg/apache/lucene/search/TermQuery$TermWeight;

    # getter for: Lorg/apache/lucene/search/TermQuery$TermWeight;->hash:Ljava/util/Set;
    invoke-static {v1}, Lorg/apache/lucene/search/TermQuery$TermWeight;->access$100(Lorg/apache/lucene/search/TermQuery$TermWeight;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 61
    :cond_0
    return-void
.end method
