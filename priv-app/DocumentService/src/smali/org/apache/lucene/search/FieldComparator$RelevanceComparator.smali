.class public final Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;
.super Lorg/apache/lucene/search/FieldComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RelevanceComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator",
        "<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# instance fields
.field private bottom:F

.field private scorer:Lorg/apache/lucene/search/Scorer;

.field private final scores:[F


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "numHits"    # I

    .prologue
    .line 639
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldComparator;-><init>()V

    .line 640
    new-array v0, p1, [F

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scores:[F

    .line 641
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 3
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 645
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scores:[F

    aget v0, v2, p1

    .line 646
    .local v0, "score1":F
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scores:[F

    aget v1, v2, p2

    .line 647
    .local v1, "score2":F
    cmpl-float v2, v0, v1

    if-lez v2, :cond_0

    const/4 v2, -0x1

    :goto_0
    return v2

    :cond_0
    cmpg-float v2, v0, v1

    if-gez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public compareBottom(I)I
    .locals 2
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 652
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    .line 653
    .local v0, "score":F
    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->bottom:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->bottom:F

    cmpg-float v1, v1, v0

    if-gez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public compareValues(Ljava/lang/Float;Ljava/lang/Float;)I
    .locals 1
    .param p1, "first"    # Ljava/lang/Float;
    .param p2, "second"    # Ljava/lang/Float;

    .prologue
    .line 692
    invoke-virtual {p2, p1}, Ljava/lang/Float;->compareTo(Ljava/lang/Float;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareValues(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 634
    check-cast p1, Ljava/lang/Float;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Float;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->compareValues(Ljava/lang/Float;Ljava/lang/Float;)I

    move-result v0

    return v0
.end method

.method public copy(II)V
    .locals 2
    .param p1, "slot"    # I
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 658
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scores:[F

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v1

    aput v1, v0, p1

    .line 659
    return-void
.end method

.method public setBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 667
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scores:[F

    aget v0, v0, p1

    iput v0, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->bottom:F

    .line 668
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 0
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I

    .prologue
    .line 663
    return-void
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 675
    instance-of v0, p1, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;

    if-nez v0, :cond_0

    .line 676
    new-instance v0, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/ScoreCachingWrappingScorer;-><init>(Lorg/apache/lucene/search/Scorer;)V

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 680
    :goto_0
    return-void

    .line 678
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scorer:Lorg/apache/lucene/search/Scorer;

    goto :goto_0
.end method

.method public value(I)Ljava/lang/Float;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 684
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->scores:[F

    aget v0, v0, p1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 634
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$RelevanceComparator;->value(I)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method
