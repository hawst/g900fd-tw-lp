.class Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "CustomScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/function/CustomScoreQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomScorer"
.end annotation


# instance fields
.field private final provider:Lorg/apache/lucene/search/function/CustomScoreProvider;

.field private final qWeight:F

.field private subQueryScorer:Lorg/apache/lucene/search/Scorer;

.field final synthetic this$0:Lorg/apache/lucene/search/function/CustomScoreQuery;

.field private vScores:[F

.field private valSrcScorers:[Lorg/apache/lucene/search/Scorer;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/search/function/CustomScoreQuery;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;Lorg/apache/lucene/search/Scorer;[Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p2, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p3, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p4, "w"    # Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;
    .param p5, "subQueryScorer"    # Lorg/apache/lucene/search/Scorer;
    .param p6, "valSrcScorers"    # [Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 306
    iput-object p1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->this$0:Lorg/apache/lucene/search/function/CustomScoreQuery;

    .line 307
    invoke-direct {p0, p2, p4}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;)V

    .line 308
    invoke-virtual {p4}, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;->getValue()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->qWeight:F

    .line 309
    iput-object p5, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    .line 310
    iput-object p6, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->valSrcScorers:[Lorg/apache/lucene/search/Scorer;

    .line 311
    array-length v0, p6

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->vScores:[F

    .line 312
    invoke-virtual {p1, p3}, Lorg/apache/lucene/search/function/CustomScoreQuery;->getCustomScoreProvider(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/function/CustomScoreProvider;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->provider:Lorg/apache/lucene/search/function/CustomScoreProvider;

    .line 313
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/search/function/CustomScoreQuery;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;Lorg/apache/lucene/search/Scorer;[Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/function/CustomScoreQuery$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/search/function/CustomScoreQuery;
    .param p2, "x1"    # Lorg/apache/lucene/search/Similarity;
    .param p3, "x2"    # Lorg/apache/lucene/index/IndexReader;
    .param p4, "x3"    # Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;
    .param p5, "x4"    # Lorg/apache/lucene/search/Scorer;
    .param p6, "x5"    # [Lorg/apache/lucene/search/Scorer;
    .param p7, "x6"    # Lorg/apache/lucene/search/function/CustomScoreQuery$1;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 297
    invoke-direct/range {p0 .. p6}, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;-><init>(Lorg/apache/lucene/search/function/CustomScoreQuery;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;Lorg/apache/lucene/search/Scorer;[Lorg/apache/lucene/search/Scorer;)V

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 342
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    .line 343
    .local v0, "doc":I
    const v2, 0x7fffffff

    if-eq v0, v2, :cond_0

    .line 344
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->valSrcScorers:[Lorg/apache/lucene/search/Scorer;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 345
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->valSrcScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    .line 344
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 348
    .end local v1    # "i":I
    :cond_0
    return v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    return v0
.end method

.method public nextDoc()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 317
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    .line 318
    .local v0, "doc":I
    const v2, 0x7fffffff

    if-eq v0, v2, :cond_0

    .line 319
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->valSrcScorers:[Lorg/apache/lucene/search/Scorer;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 320
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->valSrcScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    .line 319
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 323
    .end local v1    # "i":I
    :cond_0
    return v0
.end method

.method public score()F
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 334
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->valSrcScorers:[Lorg/apache/lucene/search/Scorer;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 335
    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->vScores:[F

    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->valSrcScorers:[Lorg/apache/lucene/search/Scorer;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v2

    aput v2, v1, v0

    .line 334
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 337
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->qWeight:F

    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->provider:Lorg/apache/lucene/search/function/CustomScoreProvider;

    iget-object v3, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->subQueryScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;->vScores:[F

    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/lucene/search/function/CustomScoreProvider;->customScore(IF[F)F

    move-result v2

    mul-float/2addr v1, v2

    return v1
.end method
