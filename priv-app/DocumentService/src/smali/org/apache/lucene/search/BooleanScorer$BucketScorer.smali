.class final Lorg/apache/lucene/search/BooleanScorer$BucketScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "BooleanScorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/BooleanScorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BucketScorer"
.end annotation


# instance fields
.field doc:I

.field freq:I

.field score:F


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Weight;)V
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 116
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;->doc:I

    .line 119
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    const v0, 0x7fffffff

    return v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;->doc:I

    return v0
.end method

.method public freq()F
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;->freq:I

    int-to-float v0, v0

    return v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    const v0, 0x7fffffff

    return v0
.end method

.method public score()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    iget v0, p0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;->score:F

    return v0
.end method
