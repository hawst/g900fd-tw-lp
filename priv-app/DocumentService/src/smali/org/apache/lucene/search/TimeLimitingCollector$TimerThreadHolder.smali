.class final Lorg/apache/lucene/search/TimeLimitingCollector$TimerThreadHolder;
.super Ljava/lang/Object;
.source "TimeLimitingCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TimeLimitingCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TimerThreadHolder"
.end annotation


# static fields
.field static final THREAD:Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 222
    new-instance v0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;

    const/4 v1, 0x1

    invoke-static {v1}, Lorg/apache/lucene/util/Counter;->newCounter(Z)Lorg/apache/lucene/util/Counter;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;-><init>(Lorg/apache/lucene/util/Counter;)V

    sput-object v0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThreadHolder;->THREAD:Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;

    .line 223
    sget-object v0, Lorg/apache/lucene/search/TimeLimitingCollector$TimerThreadHolder;->THREAD:Lorg/apache/lucene/search/TimeLimitingCollector$TimerThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 224
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
