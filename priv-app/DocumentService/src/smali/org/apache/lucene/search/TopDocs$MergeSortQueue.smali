.class Lorg/apache/lucene/search/TopDocs$MergeSortQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "TopDocs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopDocs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MergeSortQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/search/TopDocs$ShardRef;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final comparators:[Lorg/apache/lucene/search/FieldComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/search/FieldComparator",
            "<*>;"
        }
    .end annotation
.end field

.field final reverseMul:[I

.field final shardHits:[[Lorg/apache/lucene/search/ScoreDoc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 120
    const-class v0, Lorg/apache/lucene/search/TopDocs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/Sort;[Lorg/apache/lucene/search/TopDocs;)V
    .locals 11
    .param p1, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p2, "shardHits"    # [Lorg/apache/lucene/search/TopDocs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 126
    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;-><init>()V

    .line 127
    array-length v8, p2

    invoke-virtual {p0, v8}, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->initialize(I)V

    .line 128
    array-length v8, p2

    new-array v8, v8, [[Lorg/apache/lucene/search/ScoreDoc;

    iput-object v8, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->shardHits:[[Lorg/apache/lucene/search/ScoreDoc;

    .line 129
    const/4 v5, 0x0

    .local v5, "shardIDX":I
    :goto_0
    array-length v8, p2

    if-ge v5, v8, :cond_3

    .line 130
    aget-object v8, p2, v5

    iget-object v4, v8, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .line 132
    .local v4, "shard":[Lorg/apache/lucene/search/ScoreDoc;
    if-eqz v4, :cond_2

    .line 133
    iget-object v8, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->shardHits:[[Lorg/apache/lucene/search/ScoreDoc;

    aput-object v4, v8, v5

    .line 135
    const/4 v2, 0x0

    .local v2, "hitIDX":I
    :goto_1
    array-length v8, v4

    if-ge v2, v8, :cond_2

    .line 136
    aget-object v3, v4, v2

    .line 137
    .local v3, "sd":Lorg/apache/lucene/search/ScoreDoc;
    instance-of v8, v3, Lorg/apache/lucene/search/FieldDoc;

    if-nez v8, :cond_0

    .line 138
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "shard "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " was not sorted by the provided Sort (expected FieldDoc but got ScoreDoc)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_0
    move-object v1, v3

    .line 140
    check-cast v1, Lorg/apache/lucene/search/FieldDoc;

    .line 141
    .local v1, "fd":Lorg/apache/lucene/search/FieldDoc;
    iget-object v8, v1, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    if-nez v8, :cond_1

    .line 142
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "shard "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " did not set sort field values (FieldDoc.fields is null); you must pass fillFields=true to IndexSearcher.search on each shard"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 135
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 129
    .end local v1    # "fd":Lorg/apache/lucene/search/FieldDoc;
    .end local v2    # "hitIDX":I
    .end local v3    # "sd":Lorg/apache/lucene/search/ScoreDoc;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 148
    .end local v4    # "shard":[Lorg/apache/lucene/search/ScoreDoc;
    :cond_3
    invoke-virtual {p1}, Lorg/apache/lucene/search/Sort;->getSort()[Lorg/apache/lucene/search/SortField;

    move-result-object v7

    .line 149
    .local v7, "sortFields":[Lorg/apache/lucene/search/SortField;
    array-length v8, v7

    new-array v8, v8, [Lorg/apache/lucene/search/FieldComparator;

    iput-object v8, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    .line 150
    array-length v8, v7

    new-array v8, v8, [I

    iput-object v8, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->reverseMul:[I

    .line 151
    const/4 v0, 0x0

    .local v0, "compIDX":I
    :goto_2
    array-length v8, v7

    if-ge v0, v8, :cond_5

    .line 152
    aget-object v6, v7, v0

    .line 153
    .local v6, "sortField":Lorg/apache/lucene/search/SortField;
    iget-object v8, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v6, v9, v0}, Lorg/apache/lucene/search/SortField;->getComparator(II)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v10

    aput-object v10, v8, v0

    .line 154
    iget-object v10, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->reverseMul:[I

    invoke-virtual {v6}, Lorg/apache/lucene/search/SortField;->getReverse()Z

    move-result v8

    if-eqz v8, :cond_4

    const/4 v8, -0x1

    :goto_3
    aput v8, v10, v0

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move v8, v9

    .line 154
    goto :goto_3

    .line 156
    .end local v6    # "sortField":Lorg/apache/lucene/search/SortField;
    :cond_5
    return-void
.end method


# virtual methods
.method public bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 120
    check-cast p1, Lorg/apache/lucene/search/TopDocs$ShardRef;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/search/TopDocs$ShardRef;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->lessThan(Lorg/apache/lucene/search/TopDocs$ShardRef;Lorg/apache/lucene/search/TopDocs$ShardRef;)Z

    move-result v0

    return v0
.end method

.method public lessThan(Lorg/apache/lucene/search/TopDocs$ShardRef;Lorg/apache/lucene/search/TopDocs$ShardRef;)Z
    .locals 10
    .param p1, "first"    # Lorg/apache/lucene/search/TopDocs$ShardRef;
    .param p2, "second"    # Lorg/apache/lucene/search/TopDocs$ShardRef;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 161
    sget-boolean v7, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->$assertionsDisabled:Z

    if-nez v7, :cond_0

    if-ne p1, p2, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 162
    :cond_0
    iget-object v7, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->shardHits:[[Lorg/apache/lucene/search/ScoreDoc;

    iget v8, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    aget-object v7, v7, v8

    iget v8, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    aget-object v3, v7, v8

    check-cast v3, Lorg/apache/lucene/search/FieldDoc;

    .line 163
    .local v3, "firstFD":Lorg/apache/lucene/search/FieldDoc;
    iget-object v7, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->shardHits:[[Lorg/apache/lucene/search/ScoreDoc;

    iget v8, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    aget-object v7, v7, v8

    iget v8, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    aget-object v4, v7, v8

    check-cast v4, Lorg/apache/lucene/search/FieldDoc;

    .line 166
    .local v4, "secondFD":Lorg/apache/lucene/search/FieldDoc;
    const/4 v2, 0x0

    .local v2, "compIDX":I
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v7, v7

    if-ge v2, v7, :cond_4

    .line 167
    iget-object v7, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v1, v7, v2

    .line 170
    .local v1, "comp":Lorg/apache/lucene/search/FieldComparator;
    iget-object v7, p0, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->reverseMul:[I

    aget v7, v7, v2

    iget-object v8, v3, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    aget-object v8, v8, v2

    iget-object v9, v4, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    aget-object v9, v9, v2

    invoke-virtual {v1, v8, v9}, Lorg/apache/lucene/search/FieldComparator;->compareValues(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v8

    mul-int v0, v7, v8

    .line 172
    .local v0, "cmp":I
    if-eqz v0, :cond_3

    .line 174
    if-gez v0, :cond_2

    .line 190
    .end local v0    # "cmp":I
    .end local v1    # "comp":Lorg/apache/lucene/search/FieldComparator;
    :cond_1
    :goto_1
    return v5

    .restart local v0    # "cmp":I
    .restart local v1    # "comp":Lorg/apache/lucene/search/FieldComparator;
    :cond_2
    move v5, v6

    .line 174
    goto :goto_1

    .line 166
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 179
    .end local v0    # "cmp":I
    .end local v1    # "comp":Lorg/apache/lucene/search/FieldComparator;
    :cond_4
    iget v7, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    iget v8, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    if-lt v7, v8, :cond_1

    .line 182
    iget v7, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    iget v8, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->shardIndex:I

    if-le v7, v8, :cond_5

    move v5, v6

    .line 184
    goto :goto_1

    .line 189
    :cond_5
    sget-boolean v7, Lorg/apache/lucene/search/TopDocs$MergeSortQueue;->$assertionsDisabled:Z

    if-nez v7, :cond_6

    iget v7, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    iget v8, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    if-ne v7, v8, :cond_6

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 190
    :cond_6
    iget v7, p1, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    iget v8, p2, Lorg/apache/lucene/search/TopDocs$ShardRef;->hitIndex:I

    if-lt v7, v8, :cond_1

    move v5, v6

    goto :goto_1
.end method
