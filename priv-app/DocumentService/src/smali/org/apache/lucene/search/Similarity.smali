.class public abstract Lorg/apache/lucene/search/Similarity;
.super Ljava/lang/Object;
.source "Similarity.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final NORM_TABLE:[F

.field public static final NO_DOC_ID_PROVIDED:I = -0x1

.field private static defaultImpl:Lorg/apache/lucene/search/Similarity;

.field private static final withDocFreqMethod:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/search/Similarity;",
            ">;"
        }
    .end annotation
.end field

.field private static final withoutDocFreqMethod:Lorg/apache/lucene/util/VirtualMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/VirtualMethod",
            "<",
            "Lorg/apache/lucene/search/Similarity;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final hasIDFExplainWithDocFreqAPI:Z


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/16 v9, 0x100

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 533
    new-instance v1, Lorg/apache/lucene/util/VirtualMethod;

    const-class v2, Lorg/apache/lucene/search/Similarity;

    const-string/jumbo v3, "idfExplain"

    new-array v4, v8, [Ljava/lang/Class;

    const-class v5, Lorg/apache/lucene/index/Term;

    aput-object v5, v4, v6

    const-class v5, Lorg/apache/lucene/search/Searcher;

    aput-object v5, v4, v7

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v1, Lorg/apache/lucene/search/Similarity;->withoutDocFreqMethod:Lorg/apache/lucene/util/VirtualMethod;

    .line 535
    new-instance v1, Lorg/apache/lucene/util/VirtualMethod;

    const-class v2, Lorg/apache/lucene/search/Similarity;

    const-string/jumbo v3, "idfExplain"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Class;

    const-class v5, Lorg/apache/lucene/index/Term;

    aput-object v5, v4, v6

    const-class v5, Lorg/apache/lucene/search/Searcher;

    aput-object v5, v4, v7

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v4, v8

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/util/VirtualMethod;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v1, Lorg/apache/lucene/search/Similarity;->withDocFreqMethod:Lorg/apache/lucene/util/VirtualMethod;

    .line 544
    new-instance v1, Lorg/apache/lucene/search/DefaultSimilarity;

    invoke-direct {v1}, Lorg/apache/lucene/search/DefaultSimilarity;-><init>()V

    sput-object v1, Lorg/apache/lucene/search/Similarity;->defaultImpl:Lorg/apache/lucene/search/Similarity;

    .line 571
    new-array v1, v9, [F

    sput-object v1, Lorg/apache/lucene/search/Similarity;->NORM_TABLE:[F

    .line 574
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v9, :cond_0

    .line 575
    sget-object v1, Lorg/apache/lucene/search/Similarity;->NORM_TABLE:[F

    int-to-byte v2, v0

    invoke-static {v2}, Lorg/apache/lucene/util/SmallFloat;->byte315ToFloat(B)F

    move-result v2

    aput v2, v1, v0

    .line 574
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 576
    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/search/Similarity;->withDocFreqMethod:Lorg/apache/lucene/util/VirtualMethod;

    sget-object v2, Lorg/apache/lucene/search/Similarity;->withoutDocFreqMethod:Lorg/apache/lucene/util/VirtualMethod;

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/VirtualMethod;->compareImplementationDistance(Ljava/lang/Class;Lorg/apache/lucene/util/VirtualMethod;Lorg/apache/lucene/util/VirtualMethod;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/search/Similarity;->hasIDFExplainWithDocFreqAPI:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static decodeNorm(B)F
    .locals 2
    .param p0, "b"    # B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 585
    sget-object v0, Lorg/apache/lucene/search/Similarity;->NORM_TABLE:[F

    and-int/lit16 v1, p0, 0xff

    aget v0, v0, v1

    return v0
.end method

.method public static encodeNorm(F)B
    .locals 1
    .param p0, "f"    # F
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 719
    invoke-static {p0}, Lorg/apache/lucene/util/SmallFloat;->floatToByte315(F)B

    move-result v0

    return v0
.end method

.method public static getDefault()Lorg/apache/lucene/search/Similarity;
    .locals 1

    .prologue
    .line 567
    sget-object v0, Lorg/apache/lucene/search/Similarity;->defaultImpl:Lorg/apache/lucene/search/Similarity;

    return-object v0
.end method

.method public static getNormDecoder()[F
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 608
    sget-object v0, Lorg/apache/lucene/search/Similarity;->NORM_TABLE:[F

    return-object v0
.end method

.method public static setDefault(Lorg/apache/lucene/search/Similarity;)V
    .locals 0
    .param p0, "similarity"    # Lorg/apache/lucene/search/Similarity;

    .prologue
    .line 555
    sput-object p0, Lorg/apache/lucene/search/Similarity;->defaultImpl:Lorg/apache/lucene/search/Similarity;

    .line 556
    return-void
.end method


# virtual methods
.method public abstract computeNorm(Ljava/lang/String;Lorg/apache/lucene/index/FieldInvertState;)F
.end method

.method public abstract coord(II)F
.end method

.method public decodeNormValue(B)F
    .locals 2
    .param p1, "b"    # B

    .prologue
    .line 597
    sget-object v0, Lorg/apache/lucene/search/Similarity;->NORM_TABLE:[F

    and-int/lit16 v1, p1, 0xff

    aget v0, v0, v1

    return v0
.end method

.method public encodeNormValue(F)B
    .locals 1
    .param p1, "f"    # F

    .prologue
    .line 706
    invoke-static {p1}, Lorg/apache/lucene/util/SmallFloat;->floatToByte315(F)B

    move-result v0

    return v0
.end method

.method public abstract idf(II)F
.end method

.method public idfExplain(Ljava/util/Collection;Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Explanation$IDFExplanation;
    .locals 8
    .param p2, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;",
            "Lorg/apache/lucene/search/Searcher;",
            ")",
            "Lorg/apache/lucene/search/Explanation$IDFExplanation;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 846
    .local p1, "terms":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/Term;>;"
    invoke-virtual {p2}, Lorg/apache/lucene/search/Searcher;->maxDoc()I

    move-result v5

    .line 847
    .local v5, "max":I
    const/4 v4, 0x0

    .line 848
    .local v4, "idf":F
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 849
    .local v1, "exp":Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/Term;

    .line 850
    .local v6, "term":Lorg/apache/lucene/index/Term;
    invoke-virtual {p2, v6}, Lorg/apache/lucene/search/Searcher;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v0

    .line 851
    .local v0, "df":I
    invoke-virtual {p0, v0, v5}, Lorg/apache/lucene/search/Similarity;->idf(II)F

    move-result v7

    add-float/2addr v4, v7

    .line 852
    const-string/jumbo v7, " "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 853
    invoke-virtual {v6}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 854
    const-string/jumbo v7, "="

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 855
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 857
    .end local v0    # "df":I
    .end local v6    # "term":Lorg/apache/lucene/index/Term;
    :cond_0
    move v2, v4

    .line 858
    .local v2, "fIdf":F
    new-instance v7, Lorg/apache/lucene/search/Similarity$2;

    invoke-direct {v7, p0, v2, v1}, Lorg/apache/lucene/search/Similarity$2;-><init>(Lorg/apache/lucene/search/Similarity;FLjava/lang/StringBuilder;)V

    return-object v7
.end method

.method public idfExplain(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Explanation$IDFExplanation;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 828
    invoke-virtual {p2, p1}, Lorg/apache/lucene/search/Searcher;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/search/Similarity;->idfExplain(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/search/Searcher;I)Lorg/apache/lucene/search/Explanation$IDFExplanation;

    move-result-object v0

    return-object v0
.end method

.method public idfExplain(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/search/Searcher;I)Lorg/apache/lucene/search/Explanation$IDFExplanation;
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .param p3, "docFreq"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 798
    iget-boolean v3, p0, Lorg/apache/lucene/search/Similarity;->hasIDFExplainWithDocFreqAPI:Z

    if-nez v3, :cond_0

    .line 800
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/Similarity;->idfExplain(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Explanation$IDFExplanation;

    move-result-object v3

    .line 805
    :goto_0
    return-object v3

    .line 802
    :cond_0
    move v0, p3

    .line 803
    .local v0, "df":I
    invoke-virtual {p2}, Lorg/apache/lucene/search/Searcher;->maxDoc()I

    move-result v2

    .line 804
    .local v2, "max":I
    invoke-virtual {p0, v0, v2}, Lorg/apache/lucene/search/Similarity;->idf(II)F

    move-result v1

    .line 805
    .local v1, "idf":F
    new-instance v3, Lorg/apache/lucene/search/Similarity$1;

    invoke-direct {v3, p0, v0, v2, v1}, Lorg/apache/lucene/search/Similarity$1;-><init>(Lorg/apache/lucene/search/Similarity;IIF)V

    goto :goto_0
.end method

.method public final lengthNorm(Ljava/lang/String;I)F
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "numTokens"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 669
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "please use computeNorm instead"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract queryNorm(F)F
.end method

.method public scorePayload(ILjava/lang/String;II[BII)F
    .locals 1
    .param p1, "docId"    # I
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p3, "start"    # I
    .param p4, "end"    # I
    .param p5, "payload"    # [B
    .param p6, "offset"    # I
    .param p7, "length"    # I

    .prologue
    .line 918
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public abstract sloppyFreq(I)F
.end method

.method public abstract tf(F)F
.end method

.method public tf(I)F
    .locals 1
    .param p1, "freq"    # I

    .prologue
    .line 739
    int-to-float v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/Similarity;->tf(F)F

    move-result v0

    return v0
.end method
