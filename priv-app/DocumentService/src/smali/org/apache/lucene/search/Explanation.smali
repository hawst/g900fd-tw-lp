.class public Lorg/apache/lucene/search/Explanation;
.super Ljava/lang/Object;
.source "Explanation.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/Explanation$IDFExplanation;
    }
.end annotation


# instance fields
.field private description:Ljava/lang/String;

.field private details:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/search/Explanation;",
            ">;"
        }
    .end annotation
.end field

.field private value:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(FLjava/lang/String;)V
    .locals 0
    .param p1, "value"    # F
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput p1, p0, Lorg/apache/lucene/search/Explanation;->value:F

    .line 33
    iput-object p2, p0, Lorg/apache/lucene/search/Explanation;->description:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public addDetail(Lorg/apache/lucene/search/Explanation;)V
    .locals 1
    .param p1, "detail"    # Lorg/apache/lucene/search/Explanation;

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/search/Explanation;->details:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/Explanation;->details:Ljava/util/ArrayList;

    .line 81
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/Explanation;->details:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    return-void
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/search/Explanation;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDetails()[Lorg/apache/lucene/search/Explanation;
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/search/Explanation;->details:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 73
    const/4 v0, 0x0

    .line 74
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/Explanation;->details:Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-array v1, v1, [Lorg/apache/lucene/search/Explanation;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/search/Explanation;

    goto :goto_0
.end method

.method protected getSummary()Ljava/lang/String;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/Explanation;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lorg/apache/lucene/search/Explanation;->value:F

    return v0
.end method

.method public isMatch()Z
    .locals 2

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-virtual {p0}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lorg/apache/lucene/search/Explanation;->description:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public setValue(F)V
    .locals 0
    .param p1, "value"    # F

    .prologue
    .line 53
    iput p1, p0, Lorg/apache/lucene/search/Explanation;->value:F

    return-void
.end method

.method public toHtml()Ljava/lang/String;
    .locals 4

    .prologue
    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "<ul>\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    const-string/jumbo v3, "<li>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    invoke-virtual {p0}, Lorg/apache/lucene/search/Explanation;->getSummary()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    const-string/jumbo v3, "<br />\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    invoke-virtual {p0}, Lorg/apache/lucene/search/Explanation;->getDetails()[Lorg/apache/lucene/search/Explanation;

    move-result-object v1

    .line 118
    .local v1, "details":[Lorg/apache/lucene/search/Explanation;
    if-eqz v1, :cond_0

    .line 119
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    .line 120
    aget-object v3, v1, v2

    invoke-virtual {v3}, Lorg/apache/lucene/search/Explanation;->toHtml()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 124
    .end local v2    # "i":I
    :cond_0
    const-string/jumbo v3, "</li>\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    const-string/jumbo v3, "</ul>\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/Explanation;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected toString(I)Ljava/lang/String;
    .locals 5
    .param p1, "depth"    # I

    .prologue
    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p1, :cond_0

    .line 92
    const-string/jumbo v3, "  "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 94
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/Explanation;->getSummary()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    const-string/jumbo v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    invoke-virtual {p0}, Lorg/apache/lucene/search/Explanation;->getDetails()[Lorg/apache/lucene/search/Explanation;

    move-result-object v1

    .line 98
    .local v1, "details":[Lorg/apache/lucene/search/Explanation;
    if-eqz v1, :cond_1

    .line 99
    const/4 v2, 0x0

    :goto_1
    array-length v3, v1

    if-ge v2, v3, :cond_1

    .line 100
    aget-object v3, v1, v2

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Explanation;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 104
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
