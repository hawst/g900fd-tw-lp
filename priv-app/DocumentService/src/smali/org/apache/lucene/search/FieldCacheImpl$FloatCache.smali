.class final Lorg/apache/lucene/search/FieldCacheImpl$FloatCache;
.super Lorg/apache/lucene/search/FieldCacheImpl$Cache;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "FloatCache"
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl;)V
    .locals 0
    .param p1, "wrapper"    # Lorg/apache/lucene/search/FieldCacheImpl;

    .prologue
    .line 600
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    .line 601
    return-void
.end method


# virtual methods
.method protected createValue(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;
    .locals 19
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "entryKey"    # Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 606
    move-object/from16 v7, p2

    .line 607
    .local v7, "entry":Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    iget-object v8, v7, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->field:Ljava/lang/String;

    .line 608
    .local v8, "field":Ljava/lang/String;
    iget-object v11, v7, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->custom:Ljava/lang/Object;

    check-cast v11, Lorg/apache/lucene/search/FieldCache$FloatParser;

    .line 609
    .local v11, "parser":Lorg/apache/lucene/search/FieldCache$FloatParser;
    if-nez v11, :cond_1

    .line 611
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    move-object/from16 v17, v0

    sget-object v18, Lorg/apache/lucene/search/FieldCache;->DEFAULT_FLOAT_PARSER:Lorg/apache/lucene/search/FieldCache$FloatParser;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move/from16 v3, p3

    invoke-virtual {v0, v1, v8, v2, v3}, Lorg/apache/lucene/search/FieldCacheImpl;->getFloats(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Z)[F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 653
    :cond_0
    :goto_0
    return-object v12

    .line 612
    :catch_0
    move-exception v10

    .line 613
    .local v10, "ne":Ljava/lang/NumberFormatException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    move-object/from16 v17, v0

    sget-object v18, Lorg/apache/lucene/search/FieldCache;->NUMERIC_UTILS_FLOAT_PARSER:Lorg/apache/lucene/search/FieldCache$FloatParser;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move/from16 v3, p3

    invoke-virtual {v0, v1, v8, v2, v3}, Lorg/apache/lucene/search/FieldCacheImpl;->getFloats(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Z)[F

    move-result-object v12

    goto :goto_0

    .line 616
    .end local v10    # "ne":Ljava/lang/NumberFormatException;
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v9

    .line 617
    .local v9, "maxDoc":I
    const/4 v12, 0x0

    .line 618
    .local v12, "retArray":[F
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/IndexReader;->termDocs()Lorg/apache/lucene/index/TermDocs;

    move-result-object v14

    .line 619
    .local v14, "termDocs":Lorg/apache/lucene/index/TermDocs;
    new-instance v17, Lorg/apache/lucene/index/Term;

    move-object/from16 v0, v17

    invoke-direct {v0, v8}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v15

    .line 620
    .local v15, "termEnum":Lorg/apache/lucene/index/TermEnum;
    const/4 v5, 0x0

    .line 623
    .local v5, "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :goto_1
    :try_start_1
    invoke-virtual {v15}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v13

    .line 624
    .local v13, "term":Lorg/apache/lucene/index/Term;
    if-eqz v13, :cond_2

    invoke-virtual {v13}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;
    :try_end_1
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v17

    move-object/from16 v0, v17

    if-eq v0, v8, :cond_4

    .line 644
    :cond_2
    :goto_2
    invoke-interface {v14}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 645
    .end local v13    # "term":Lorg/apache/lucene/index/Term;
    :goto_3
    invoke-virtual {v15}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 647
    if-eqz p3, :cond_3

    .line 648
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/search/FieldCacheImpl$FloatCache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v8, v5}, Lorg/apache/lucene/search/FieldCacheImpl;->setDocsWithField(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/util/Bits;)V

    .line 650
    :cond_3
    if-nez v12, :cond_0

    .line 651
    new-array v12, v9, [F

    goto :goto_0

    .line 625
    .restart local v13    # "term":Lorg/apache/lucene/index/Term;
    :cond_4
    :try_start_2
    invoke-virtual {v13}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v11, v0}, Lorg/apache/lucene/search/FieldCache$FloatParser;->parseFloat(Ljava/lang/String;)F

    move-result v16

    .line 626
    .local v16, "termval":F
    if-nez v12, :cond_5

    .line 627
    new-array v12, v9, [F

    .line 629
    :cond_5
    invoke-interface {v14, v15}, Lorg/apache/lucene/index/TermDocs;->seek(Lorg/apache/lucene/index/TermEnum;)V
    :try_end_2
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v6, v5

    .line 630
    .end local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .local v6, "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :goto_4
    :try_start_3
    invoke-interface {v14}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v17

    if-eqz v17, :cond_6

    .line 631
    invoke-interface {v14}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v4

    .line 632
    .local v4, "docID":I
    aput v16, v12, v4

    .line 633
    if-eqz p3, :cond_9

    .line 634
    if-nez v6, :cond_8

    .line 636
    new-instance v5, Lorg/apache/lucene/util/FixedBitSet;

    invoke-direct {v5, v9}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V
    :try_end_3
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 638
    .end local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :goto_5
    :try_start_4
    invoke-virtual {v5, v4}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V
    :try_end_4
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_6
    move-object v6, v5

    .line 640
    .end local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_4

    .line 641
    .end local v4    # "docID":I
    :cond_6
    :try_start_5
    invoke-virtual {v15}, Lorg/apache/lucene/index/TermEnum;->next()Z
    :try_end_5
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v17

    if-nez v17, :cond_7

    move-object v5, v6

    .end local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_2

    .line 644
    .end local v13    # "term":Lorg/apache/lucene/index/Term;
    .end local v16    # "termval":F
    :catchall_0
    move-exception v17

    :goto_7
    invoke-interface {v14}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 645
    invoke-virtual {v15}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 644
    throw v17

    .line 642
    :catch_1
    move-exception v17

    .line 644
    :goto_8
    invoke-interface {v14}, Lorg/apache/lucene/index/TermDocs;->close()V

    goto :goto_3

    .end local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v13    # "term":Lorg/apache/lucene/index/Term;
    .restart local v16    # "termval":F
    :catchall_1
    move-exception v17

    move-object v5, v6

    .end local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_7

    .line 642
    .end local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :catch_2
    move-exception v17

    move-object v5, v6

    .end local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_8

    .end local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :cond_7
    move-object v5, v6

    .end local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_1

    .end local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v4    # "docID":I
    .restart local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :cond_8
    move-object v5, v6

    .end local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_5

    .end local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :cond_9
    move-object v5, v6

    .end local v6    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v5    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_6
.end method
