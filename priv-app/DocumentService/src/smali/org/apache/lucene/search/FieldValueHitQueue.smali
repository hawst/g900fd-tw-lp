.class public abstract Lorg/apache/lucene/search/FieldValueHitQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "FieldValueHitQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/FieldValueHitQueue$1;,
        Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;,
        Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;,
        Lorg/apache/lucene/search/FieldValueHitQueue$Entry;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
        ">",
        "Lorg/apache/lucene/util/PriorityQueue",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected final comparators:[Lorg/apache/lucene/search/FieldComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/search/FieldComparator",
            "<*>;"
        }
    .end annotation
.end field

.field protected final fields:[Lorg/apache/lucene/search/SortField;

.field protected final reverseMul:[I


# direct methods
.method private constructor <init>([Lorg/apache/lucene/search/SortField;)V
    .locals 2
    .param p1, "fields"    # [Lorg/apache/lucene/search/SortField;

    .prologue
    .line 140
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>;"
    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;-><init>()V

    .line 147
    iput-object p1, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->fields:[Lorg/apache/lucene/search/SortField;

    .line 148
    array-length v0, p1

    .line 149
    .local v0, "numComparators":I
    new-array v1, v0, [Lorg/apache/lucene/search/FieldComparator;

    iput-object v1, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    .line 150
    new-array v1, v0, [I

    iput-object v1, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->reverseMul:[I

    .line 151
    return-void
.end method

.method synthetic constructor <init>([Lorg/apache/lucene/search/SortField;Lorg/apache/lucene/search/FieldValueHitQueue$1;)V
    .locals 0
    .param p1, "x0"    # [Lorg/apache/lucene/search/SortField;
    .param p2, "x1"    # Lorg/apache/lucene/search/FieldValueHitQueue$1;

    .prologue
    .line 34
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldValueHitQueue;-><init>([Lorg/apache/lucene/search/SortField;)V

    return-void
.end method

.method public static create([Lorg/apache/lucene/search/SortField;I)Lorg/apache/lucene/search/FieldValueHitQueue;
    .locals 2
    .param p0, "fields"    # [Lorg/apache/lucene/search/SortField;
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">([",
            "Lorg/apache/lucene/search/SortField;",
            "I)",
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 168
    array-length v0, p0

    if-nez v0, :cond_0

    .line 169
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Sort must contain at least one field"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_0
    array-length v0, p0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 173
    new-instance v0, Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/FieldValueHitQueue$OneComparatorFieldValueHitQueue;-><init>([Lorg/apache/lucene/search/SortField;I)V

    .line 175
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/FieldValueHitQueue$MultiComparatorsFieldValueHitQueue;-><init>([Lorg/apache/lucene/search/SortField;I)V

    goto :goto_0
.end method


# virtual methods
.method fillFields(Lorg/apache/lucene/search/FieldValueHitQueue$Entry;)Lorg/apache/lucene/search/FieldDoc;
    .locals 6
    .param p1, "entry"    # Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .prologue
    .line 207
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>;"
    iget-object v3, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v2, v3

    .line 208
    .local v2, "n":I
    new-array v0, v2, [Ljava/lang/Object;

    .line 209
    .local v0, "fields":[Ljava/lang/Object;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 210
    iget-object v3, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v3, v3, v1

    iget v4, p1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/FieldComparator;->value(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v0, v1

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 213
    :cond_0
    new-instance v3, Lorg/apache/lucene/search/FieldDoc;

    iget v4, p1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    iget v5, p1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->score:F

    invoke-direct {v3, v4, v5, v0}, Lorg/apache/lucene/search/FieldDoc;-><init>(IF[Ljava/lang/Object;)V

    return-object v3
.end method

.method public getComparators()[Lorg/apache/lucene/search/FieldComparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[",
            "Lorg/apache/lucene/search/FieldComparator",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 180
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    return-object v0
.end method

.method getFields()[Lorg/apache/lucene/search/SortField;
    .locals 1

    .prologue
    .line 218
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->fields:[Lorg/apache/lucene/search/SortField;

    return-object v0
.end method

.method public getReverseMul()[I
    .locals 1

    .prologue
    .line 184
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/FieldValueHitQueue;->reverseMul:[I

    return-object v0
.end method

.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 34
    .local p0, "this":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<TT;>;"
    check-cast p1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldValueHitQueue;->lessThan(Lorg/apache/lucene/search/FieldValueHitQueue$Entry;Lorg/apache/lucene/search/FieldValueHitQueue$Entry;)Z

    move-result v0

    return v0
.end method

.method protected abstract lessThan(Lorg/apache/lucene/search/FieldValueHitQueue$Entry;Lorg/apache/lucene/search/FieldValueHitQueue$Entry;)Z
.end method
