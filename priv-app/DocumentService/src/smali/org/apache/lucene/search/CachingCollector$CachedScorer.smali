.class final Lorg/apache/lucene/search/CachingCollector$CachedScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "CachingCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/CachingCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CachedScorer"
.end annotation


# instance fields
.field doc:I

.field score:F


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    check-cast v0, Lorg/apache/lucene/search/Weight;

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/search/CachingCollector$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/search/CachingCollector$1;

    .prologue
    .line 69
    invoke-direct {p0}, Lorg/apache/lucene/search/CachingCollector$CachedScorer;-><init>()V

    return-void
.end method


# virtual methods
.method public final advance(I)I
    .locals 1
    .param p1, "target"    # I

    .prologue
    .line 84
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final docID()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lorg/apache/lucene/search/CachingCollector$CachedScorer;->doc:I

    return v0
.end method

.method public final freq()F
    .locals 1

    .prologue
    .line 90
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final nextDoc()I
    .locals 1

    .prologue
    .line 93
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final score()F
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lorg/apache/lucene/search/CachingCollector$CachedScorer;->score:F

    return v0
.end method
