.class public abstract Lorg/apache/lucene/search/Searcher;
.super Ljava/lang/Object;
.source "Searcher.java"

# interfaces
.implements Lorg/apache/lucene/search/Searchable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private similarity:Lorg/apache/lucene/search/Similarity;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    invoke-static {}, Lorg/apache/lucene/search/Similarity;->getDefault()Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/Searcher;->similarity:Lorg/apache/lucene/search/Similarity;

    return-void
.end method


# virtual methods
.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;
    .locals 4
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/Searcher;->rewrite(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Query;

    move-result-object p1

    .line 168
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v2

    .line 169
    .local v2, "weight":Lorg/apache/lucene/search/Weight;
    invoke-virtual {v2}, Lorg/apache/lucene/search/Weight;->sumOfSquaredWeights()F

    move-result v1

    .line 171
    .local v1, "sum":F
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Query;->getSimilarity(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Similarity;

    move-result-object v3

    invoke-virtual {v3, v1}, Lorg/apache/lucene/search/Similarity;->queryNorm(F)F

    move-result v0

    .line 172
    .local v0, "norm":F
    invoke-static {v0}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 173
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 174
    :cond_1
    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/Weight;->normalize(F)V

    .line 175
    return-object v2
.end method

.method protected final createWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 188
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/Searcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    return-object v0
.end method

.method public abstract doc(I)Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract doc(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract docFreq(Lorg/apache/lucene/index/Term;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public docFreqs([Lorg/apache/lucene/index/Term;)[I
    .locals 3
    .param p1, "terms"    # [Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    array-length v2, p1

    new-array v1, v2, [I

    .line 194
    .local v1, "result":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 195
    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, Lorg/apache/lucene/search/Searcher;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v2

    aput v2, v1, v0

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 197
    :cond_0
    return-object v1
.end method

.method public explain(Lorg/apache/lucene/search/Query;I)Lorg/apache/lucene/search/Explanation;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/Searcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/lucene/search/Searcher;->explain(Lorg/apache/lucene/search/Weight;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    return-object v0
.end method

.method public abstract explain(Lorg/apache/lucene/search/Weight;I)Lorg/apache/lucene/search/Explanation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getSimilarity()Lorg/apache/lucene/search/Similarity;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lorg/apache/lucene/search/Searcher;->similarity:Lorg/apache/lucene/search/Similarity;

    return-object v0
.end method

.method public abstract maxDoc()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract rewrite(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public search(Lorg/apache/lucene/search/Query;I)Lorg/apache/lucene/search/TopDocs;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/lucene/search/Searcher;->search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method public search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/Searcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lorg/apache/lucene/search/Searcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method public abstract search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public search(Lorg/apache/lucene/search/Query;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 2
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "n"    # I
    .param p3, "sort"    # Lorg/apache/lucene/search/Sort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/Searcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p2, p3}, Lorg/apache/lucene/search/Searcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method public search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "n"    # I
    .param p4, "sort"    # Lorg/apache/lucene/search/Sort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/Searcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lorg/apache/lucene/search/Searcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method public abstract search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Collector;)V
    .locals 2
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "results"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/Searcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p2}, Lorg/apache/lucene/search/Searcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V

    .line 85
    return-void
.end method

.method public search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "results"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/Searcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lorg/apache/lucene/search/Searcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V

    .line 106
    return-void
.end method

.method public abstract search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public setSimilarity(Lorg/apache/lucene/search/Similarity;)V
    .locals 0
    .param p1, "similarity"    # Lorg/apache/lucene/search/Similarity;

    .prologue
    .line 148
    iput-object p1, p0, Lorg/apache/lucene/search/Searcher;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 149
    return-void
.end method
