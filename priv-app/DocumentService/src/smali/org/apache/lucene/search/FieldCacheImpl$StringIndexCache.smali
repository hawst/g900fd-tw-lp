.class final Lorg/apache/lucene/search/FieldCacheImpl$StringIndexCache;
.super Lorg/apache/lucene/search/FieldCacheImpl$Cache;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "StringIndexCache"
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl;)V
    .locals 0
    .param p1, "wrapper"    # Lorg/apache/lucene/search/FieldCacheImpl;

    .prologue
    .line 861
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    .line 862
    return-void
.end method


# virtual methods
.method protected createValue(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;
    .locals 12
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "entryKey"    # Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 867
    iget-object v10, p2, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->field:Ljava/lang/String;

    invoke-static {v10}, Lorg/apache/lucene/util/StringHelper;->intern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 868
    .local v0, "field":Ljava/lang/String;
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v10

    new-array v2, v10, [I

    .line 869
    .local v2, "retArray":[I
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    new-array v1, v10, [Ljava/lang/String;

    .line 870
    .local v1, "mterms":[Ljava/lang/String;
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->termDocs()Lorg/apache/lucene/index/TermDocs;

    move-result-object v6

    .line 871
    .local v6, "termDocs":Lorg/apache/lucene/index/TermDocs;
    new-instance v10, Lorg/apache/lucene/index/Term;

    invoke-direct {v10, v0}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v10}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v7

    .line 872
    .local v7, "termEnum":Lorg/apache/lucene/index/TermEnum;
    const/4 v3, 0x0

    .line 878
    .local v3, "t":I
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "t":I
    .local v4, "t":I
    const/4 v10, 0x0

    aput-object v10, v1, v3

    move v3, v4

    .line 882
    .end local v4    # "t":I
    .restart local v3    # "t":I
    :cond_0
    :try_start_0
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v5

    .line 883
    .local v5, "term":Lorg/apache/lucene/index/Term;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v10

    if-ne v10, v0, :cond_1

    array-length v10, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lt v3, v10, :cond_3

    .line 896
    :cond_1
    :goto_0
    invoke-interface {v6}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 897
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 900
    if-nez v3, :cond_5

    .line 903
    const/4 v10, 0x1

    new-array v1, v10, [Ljava/lang/String;

    .line 912
    :cond_2
    :goto_1
    new-instance v9, Lorg/apache/lucene/search/FieldCache$StringIndex;

    invoke-direct {v9, v2, v1}, Lorg/apache/lucene/search/FieldCache$StringIndex;-><init>([I[Ljava/lang/String;)V

    .line 913
    .local v9, "value":Lorg/apache/lucene/search/FieldCache$StringIndex;
    return-object v9

    .line 886
    .end local v9    # "value":Lorg/apache/lucene/search/FieldCache$StringIndex;
    :cond_3
    :try_start_1
    invoke-virtual {v5}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v1, v3

    .line 888
    invoke-interface {v6, v7}, Lorg/apache/lucene/index/TermDocs;->seek(Lorg/apache/lucene/index/TermEnum;)V

    .line 889
    :goto_2
    invoke-interface {v6}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 890
    invoke-interface {v6}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v10

    aput v3, v2, v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 896
    .end local v5    # "term":Lorg/apache/lucene/index/Term;
    :catchall_0
    move-exception v10

    invoke-interface {v6}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 897
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 896
    throw v10

    .line 893
    .restart local v5    # "term":Lorg/apache/lucene/index/Term;
    :cond_4
    add-int/lit8 v3, v3, 0x1

    .line 894
    :try_start_2
    invoke-virtual {v7}, Lorg/apache/lucene/index/TermEnum;->next()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v10

    if-nez v10, :cond_0

    goto :goto_0

    .line 904
    :cond_5
    array-length v10, v1

    if-ge v3, v10, :cond_2

    .line 907
    new-array v8, v3, [Ljava/lang/String;

    .line 908
    .local v8, "terms":[Ljava/lang/String;
    invoke-static {v1, v11, v8, v11, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 909
    move-object v1, v8

    goto :goto_1
.end method
