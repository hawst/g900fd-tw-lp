.class public Lorg/apache/lucene/search/function/CustomScoreQuery;
.super Lorg/apache/lucene/search/Query;
.source "CustomScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/function/CustomScoreQuery$1;,
        Lorg/apache/lucene/search/function/CustomScoreQuery$CustomScorer;,
        Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;
    }
.end annotation


# instance fields
.field private strict:Z

.field private subQuery:Lorg/apache/lucene/search/Query;

.field private valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Query;)V
    .locals 1
    .param p1, "subQuery"    # Lorg/apache/lucene/search/Query;

    .prologue
    .line 58
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/lucene/search/function/ValueSourceQuery;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/function/CustomScoreQuery;-><init>(Lorg/apache/lucene/search/Query;[Lorg/apache/lucene/search/function/ValueSourceQuery;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/function/ValueSourceQuery;)V
    .locals 2
    .param p1, "subQuery"    # Lorg/apache/lucene/search/Query;
    .param p2, "valSrcQuery"    # Lorg/apache/lucene/search/function/ValueSourceQuery;

    .prologue
    const/4 v1, 0x0

    .line 70
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/lucene/search/function/ValueSourceQuery;

    aput-object p2, v0, v1

    :goto_0
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/function/CustomScoreQuery;-><init>(Lorg/apache/lucene/search/Query;[Lorg/apache/lucene/search/function/ValueSourceQuery;)V

    .line 72
    return-void

    .line 70
    :cond_0
    new-array v0, v1, [Lorg/apache/lucene/search/function/ValueSourceQuery;

    goto :goto_0
.end method

.method public varargs constructor <init>(Lorg/apache/lucene/search/Query;[Lorg/apache/lucene/search/function/ValueSourceQuery;)V
    .locals 2
    .param p1, "subQuery"    # Lorg/apache/lucene/search/Query;
    .param p2, "valSrcQueries"    # [Lorg/apache/lucene/search/function/ValueSourceQuery;

    .prologue
    const/4 v0, 0x0

    .line 82
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 51
    iput-boolean v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->strict:Z

    .line 83
    iput-object p1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    .line 84
    if-eqz p2, :cond_0

    .end local p2    # "valSrcQueries":[Lorg/apache/lucene/search/function/ValueSourceQuery;
    :goto_0
    iput-object p2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    .line 86
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "<subquery> must not be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    .restart local p2    # "valSrcQueries":[Lorg/apache/lucene/search/function/ValueSourceQuery;
    :cond_0
    new-array p2, v0, [Lorg/apache/lucene/search/function/ValueSourceQuery;

    goto :goto_0

    .line 87
    .end local p2    # "valSrcQueries":[Lorg/apache/lucene/search/function/ValueSourceQuery;
    :cond_1
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/search/function/CustomScoreQuery;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/function/CustomScoreQuery;

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    return-object v0
.end method

.method static synthetic access$100(Lorg/apache/lucene/search/function/CustomScoreQuery;)[Lorg/apache/lucene/search/function/ValueSourceQuery;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/function/CustomScoreQuery;

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    return-object v0
.end method

.method static synthetic access$200(Lorg/apache/lucene/search/function/CustomScoreQuery;)Z
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/function/CustomScoreQuery;

    .prologue
    .line 47
    iget-boolean v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->strict:Z

    return v0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 123
    invoke-super {p0}, Lorg/apache/lucene/search/Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/function/CustomScoreQuery;

    .line 124
    .local v0, "clone":Lorg/apache/lucene/search/function/CustomScoreQuery;
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Query;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/Query;

    iput-object v2, v0, Lorg/apache/lucene/search/function/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    .line 125
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    array-length v2, v2

    new-array v2, v2, [Lorg/apache/lucene/search/function/ValueSourceQuery;

    iput-object v2, v0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    .line 126
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 127
    iget-object v3, v0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/apache/lucene/search/function/ValueSourceQuery;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/function/ValueSourceQuery;

    aput-object v2, v3, v1

    .line 126
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129
    :cond_0
    return-object v0
.end method

.method public createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 354
    new-instance v0, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/function/CustomScoreQuery$CustomWeight;-><init>(Lorg/apache/lucene/search/function/CustomScoreQuery;Lorg/apache/lucene/search/Searcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 148
    if-ne p0, p1, :cond_1

    .line 149
    const/4 v1, 0x1

    .line 162
    :cond_0
    :goto_0
    return v1

    .line 150
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 152
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    move-object v0, p1

    .line 155
    check-cast v0, Lorg/apache/lucene/search/function/CustomScoreQuery;

    .line 156
    .local v0, "other":Lorg/apache/lucene/search/function/CustomScoreQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/function/CustomScoreQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/function/CustomScoreQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    iget-object v3, v0, Lorg/apache/lucene/search/function/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->strict:Z

    iget-boolean v3, v0, Lorg/apache/lucene/search/function/CustomScoreQuery;->strict:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    array-length v2, v2

    iget-object v3, v0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    array-length v3, v3

    if-ne v2, v3, :cond_0

    .line 162
    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    iget-object v2, v0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Query;->extractTerms(Ljava/util/Set;)V

    .line 115
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 116
    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/function/ValueSourceQuery;->extractTerms(Ljava/util/Set;)V

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_0
    return-void
.end method

.method protected getCustomScoreProvider(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/function/CustomScoreProvider;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 179
    new-instance v0, Lorg/apache/lucene/search/function/CustomScoreProvider;

    invoke-direct {v0, p1}, Lorg/apache/lucene/search/function/CustomScoreProvider;-><init>(Lorg/apache/lucene/index/IndexReader;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 168
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v1}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lorg/apache/lucene/search/function/CustomScoreQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    xor-int/2addr v1, v0

    iget-boolean v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->strict:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4d2

    :goto_0
    xor-int/2addr v0, v1

    return v0

    :cond_0
    const/16 v0, 0x10e1

    goto :goto_0
.end method

.method public isStrict()Z
    .locals 1

    .prologue
    .line 367
    iget-boolean v0, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->strict:Z

    return v0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 383
    const-string/jumbo v0, "custom"

    return-object v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    const/4 v0, 0x0

    .line 94
    .local v0, "clone":Lorg/apache/lucene/search/function/CustomScoreQuery;
    iget-object v4, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    .line 95
    .local v2, "sq":Lorg/apache/lucene/search/Query;
    iget-object v4, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    if-eq v2, v4, :cond_0

    .line 96
    invoke-virtual {p0}, Lorg/apache/lucene/search/function/CustomScoreQuery;->clone()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "clone":Lorg/apache/lucene/search/function/CustomScoreQuery;
    check-cast v0, Lorg/apache/lucene/search/function/CustomScoreQuery;

    .line 97
    .restart local v0    # "clone":Lorg/apache/lucene/search/function/CustomScoreQuery;
    iput-object v2, v0, Lorg/apache/lucene/search/function/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    .line 100
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 101
    iget-object v4, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    aget-object v4, v4, v1

    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/function/ValueSourceQuery;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/function/ValueSourceQuery;

    .line 102
    .local v3, "v":Lorg/apache/lucene/search/function/ValueSourceQuery;
    iget-object v4, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    aget-object v4, v4, v1

    if-eq v3, v4, :cond_2

    .line 103
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/lucene/search/function/CustomScoreQuery;->clone()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "clone":Lorg/apache/lucene/search/function/CustomScoreQuery;
    check-cast v0, Lorg/apache/lucene/search/function/CustomScoreQuery;

    .line 104
    .restart local v0    # "clone":Lorg/apache/lucene/search/function/CustomScoreQuery;
    :cond_1
    iget-object v4, v0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    aput-object v3, v4, v1

    .line 100
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 108
    .end local v3    # "v":Lorg/apache/lucene/search/function/ValueSourceQuery;
    :cond_3
    if-nez v0, :cond_4

    .end local p0    # "this":Lorg/apache/lucene/search/function/CustomScoreQuery;
    :goto_1
    return-object p0

    .restart local p0    # "this":Lorg/apache/lucene/search/function/CustomScoreQuery;
    :cond_4
    move-object p0, v0

    goto :goto_1
.end method

.method public setStrict(Z)V
    .locals 0
    .param p1, "strict"    # Z

    .prologue
    .line 376
    iput-boolean p1, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->strict:Z

    .line 377
    return-void
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 135
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/search/function/CustomScoreQuery;->name()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 136
    .local v1, "sb":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->subQuery:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 138
    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->valSrcQueries:[Lorg/apache/lucene/search/function/ValueSourceQuery;

    aget-object v3, v3, v0

    invoke-virtual {v3, p1}, Lorg/apache/lucene/search/function/ValueSourceQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_0
    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    iget-boolean v2, p0, Lorg/apache/lucene/search/function/CustomScoreQuery;->strict:Z

    if-eqz v2, :cond_1

    const-string/jumbo v2, " STRICT"

    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/search/function/CustomScoreQuery;->getBoost()F

    move-result v3

    invoke-static {v3}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 141
    :cond_1
    const-string/jumbo v2, ""

    goto :goto_1
.end method
