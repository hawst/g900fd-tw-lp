.class Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
.super Lorg/apache/lucene/search/spans/Spans;
.source "NearSpansUnordered.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/spans/NearSpansUnordered;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SpansCell"
.end annotation


# instance fields
.field private index:I

.field private length:I

.field private next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

.field private spans:Lorg/apache/lucene/search/spans/Spans;

.field final synthetic this$0:Lorg/apache/lucene/search/spans/NearSpansUnordered;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/spans/NearSpansUnordered;Lorg/apache/lucene/search/spans/Spans;I)V
    .locals 1
    .param p2, "spans"    # Lorg/apache/lucene/search/spans/Spans;
    .param p3, "index"    # I

    .prologue
    .line 77
    iput-object p1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->this$0:Lorg/apache/lucene/search/spans/NearSpansUnordered;

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/Spans;-><init>()V

    .line 74
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->length:I

    .line 78
    iput-object p2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->spans:Lorg/apache/lucene/search/spans/Spans;

    .line 79
    iput p3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->index:I

    .line 80
    return-void
.end method

.method static synthetic access$300(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/Spans;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->spans:Lorg/apache/lucene/search/spans/Spans;

    return-object v0
.end method

.method static synthetic access$400(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    return-object v0
.end method

.method static synthetic access$402(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    .locals 0
    .param p0, "x0"    # Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    .param p1, "x1"    # Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .prologue
    .line 71
    iput-object p1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    return-object p1
.end method

.method private adjust(Z)Z
    .locals 2
    .param p1, "condition"    # Z

    .prologue
    .line 93
    iget v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->length:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 94
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->this$0:Lorg/apache/lucene/search/spans/NearSpansUnordered;

    iget v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->length:I

    # -= operator for: Lorg/apache/lucene/search/spans/NearSpansUnordered;->totalLength:I
    invoke-static {v0, v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->access$020(Lorg/apache/lucene/search/spans/NearSpansUnordered;I)I

    .line 96
    :cond_0
    if-eqz p1, :cond_2

    .line 97
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->end()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->start()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->length:I

    .line 98
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->this$0:Lorg/apache/lucene/search/spans/NearSpansUnordered;

    iget v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->length:I

    # += operator for: Lorg/apache/lucene/search/spans/NearSpansUnordered;->totalLength:I
    invoke-static {v0, v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->access$012(Lorg/apache/lucene/search/spans/NearSpansUnordered;I)I

    .line 100
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->this$0:Lorg/apache/lucene/search/spans/NearSpansUnordered;

    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->access$100(Lorg/apache/lucene/search/spans/NearSpansUnordered;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->this$0:Lorg/apache/lucene/search/spans/NearSpansUnordered;

    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->access$100(Lorg/apache/lucene/search/spans/NearSpansUnordered;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v1

    if-gt v0, v1, :cond_1

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->this$0:Lorg/apache/lucene/search/spans/NearSpansUnordered;

    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->access$100(Lorg/apache/lucene/search/spans/NearSpansUnordered;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v1

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->end()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->this$0:Lorg/apache/lucene/search/spans/NearSpansUnordered;

    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->access$100(Lorg/apache/lucene/search/spans/NearSpansUnordered;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->end()I

    move-result v1

    if-le v0, v1, :cond_2

    .line 102
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->this$0:Lorg/apache/lucene/search/spans/NearSpansUnordered;

    # setter for: Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0, p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->access$102(Lorg/apache/lucene/search/spans/NearSpansUnordered;Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 105
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->this$0:Lorg/apache/lucene/search/spans/NearSpansUnordered;

    # setter for: Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z
    invoke-static {v0, p1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->access$202(Lorg/apache/lucene/search/spans/NearSpansUnordered;Z)Z

    .line 106
    return p1
.end method


# virtual methods
.method public doc()I
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    return v0
.end method

.method public end()I
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    return v0
.end method

.method public getPayload()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public isPayloadAvailable()Z
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v0

    return v0
.end method

.method public next()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->adjust(Z)Z

    move-result v0

    return v0
.end method

.method public skipTo(I)Z
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->adjust(Z)Z

    move-result v0

    return v0
.end method

.method public start()I
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->index:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
