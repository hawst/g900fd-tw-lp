.class Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
.super Ljava/lang/Object;
.source "PhraseQuery.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/PhraseQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PostingsAndFreq"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;",
        ">;"
    }
.end annotation


# instance fields
.field final docFreq:I

.field final nTerms:I

.field final position:I

.field final postings:Lorg/apache/lucene/index/TermPositions;

.field final terms:[Lorg/apache/lucene/index/Term;


# direct methods
.method public varargs constructor <init>(Lorg/apache/lucene/index/TermPositions;II[Lorg/apache/lucene/index/Term;)V
    .locals 4
    .param p1, "postings"    # Lorg/apache/lucene/index/TermPositions;
    .param p2, "docFreq"    # I
    .param p3, "position"    # I
    .param p4, "terms"    # [Lorg/apache/lucene/index/Term;

    .prologue
    const/4 v2, 0x0

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput-object p1, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->postings:Lorg/apache/lucene/index/TermPositions;

    .line 131
    iput p2, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    .line 132
    iput p3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    .line 133
    if-nez p4, :cond_0

    move v1, v2

    :goto_0
    iput v1, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    .line 134
    iget v1, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    if-lez v1, :cond_2

    .line 135
    array-length v1, p4

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 136
    iput-object p4, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    .line 146
    :goto_1
    return-void

    .line 133
    :cond_0
    array-length v1, p4

    goto :goto_0

    .line 138
    :cond_1
    array-length v1, p4

    new-array v0, v1, [Lorg/apache/lucene/index/Term;

    .line 139
    .local v0, "terms2":[Lorg/apache/lucene/index/Term;
    array-length v1, p4

    invoke-static {p4, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 140
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 141
    iput-object v0, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    goto :goto_1

    .line 144
    .end local v0    # "terms2":[Lorg/apache/lucene/index/Term;
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    goto :goto_1
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 122
    check-cast p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->compareTo(Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;)I
    .locals 5
    .param p1, "other"    # Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;

    .prologue
    const/4 v2, 0x0

    .line 149
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    iget v4, p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    if-eq v3, v4, :cond_1

    .line 150
    iget v2, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    iget v3, p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    sub-int v1, v2, v3

    .line 165
    :cond_0
    :goto_0
    return v1

    .line 152
    :cond_1
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    iget v4, p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    if-eq v3, v4, :cond_2

    .line 153
    iget v2, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    iget v3, p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    sub-int v1, v2, v3

    goto :goto_0

    .line 155
    :cond_2
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    iget v4, p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    if-eq v3, v4, :cond_3

    .line 156
    iget v2, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    iget v3, p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    sub-int v1, v2, v3

    goto :goto_0

    .line 158
    :cond_3
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    if-nez v3, :cond_4

    move v1, v2

    .line 159
    goto :goto_0

    .line 161
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 162
    iget-object v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    aget-object v3, v3, v0

    iget-object v4, p1, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v1

    .line 163
    .local v1, "res":I
    if-nez v1, :cond_0

    .line 161
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v1    # "res":I
    :cond_5
    move v1, v2

    .line 165
    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 182
    if-ne p0, p1, :cond_1

    move v2, v1

    .line 189
    :cond_0
    :goto_0
    return v2

    .line 183
    :cond_1
    if-eqz p1, :cond_0

    .line 184
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-ne v3, v4, :cond_0

    move-object v0, p1

    .line 185
    check-cast v0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;

    .line 186
    .local v0, "other":Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    iget v4, v0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    if-ne v3, v4, :cond_0

    .line 187
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    iget v4, v0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    if-ne v3, v4, :cond_0

    .line 188
    iget-object v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    if-nez v3, :cond_3

    iget-object v3, v0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    if-nez v3, :cond_2

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    .line 189
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    iget-object v2, v0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 170
    const/16 v1, 0x1f

    .line 171
    .local v1, "prime":I
    const/4 v2, 0x1

    .line 172
    .local v2, "result":I
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    add-int/lit8 v2, v3, 0x1f

    .line 173
    mul-int/lit8 v3, v2, 0x1f

    iget v4, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    add-int v2, v3, v4

    .line 174
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->nTerms:I

    if-ge v0, v3, :cond_0

    .line 175
    mul-int/lit8 v3, v2, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v4

    add-int v2, v3, v4

    .line 174
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_0
    return v2
.end method
