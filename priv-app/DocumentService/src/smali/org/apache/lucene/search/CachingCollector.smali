.class public abstract Lorg/apache/lucene/search/CachingCollector;
.super Lorg/apache/lucene/search/Collector;
.source "CachingCollector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;,
        Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;,
        Lorg/apache/lucene/search/CachingCollector$CachedScorer;,
        Lorg/apache/lucene/search/CachingCollector$SegStart;
    }
.end annotation


# static fields
.field private static final EMPTY_INT_ARRAY:[I

.field private static final INITIAL_ARRAY_SIZE:I = 0x80

.field private static final MAX_ARRAY_SIZE:I = 0x80000


# instance fields
.field protected base:I

.field protected final cachedDocs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation
.end field

.field protected final cachedSegs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/CachingCollector$SegStart;",
            ">;"
        }
    .end annotation
.end field

.field protected curDocs:[I

.field protected lastDocBase:I

.field private lastReader:Lorg/apache/lucene/index/IndexReader;

.field protected final maxDocsToCache:I

.field protected final other:Lorg/apache/lucene/search/Collector;

.field protected upto:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lorg/apache/lucene/search/CachingCollector;->EMPTY_INT_ARRAY:[I

    return-void
.end method

.method private constructor <init>(Lorg/apache/lucene/search/Collector;DZ)V
    .locals 6
    .param p1, "other"    # Lorg/apache/lucene/search/Collector;
    .param p2, "maxRAMMB"    # D
    .param p4, "cacheScores"    # Z

    .prologue
    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    .line 392
    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    .line 317
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/search/CachingCollector;->cachedSegs:Ljava/util/List;

    .line 393
    iput-object p1, p0, Lorg/apache/lucene/search/CachingCollector;->other:Lorg/apache/lucene/search/Collector;

    .line 395
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/search/CachingCollector;->cachedDocs:Ljava/util/List;

    .line 396
    const/16 v1, 0x80

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/search/CachingCollector;->curDocs:[I

    .line 397
    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector;->cachedDocs:Ljava/util/List;

    iget-object v2, p0, Lorg/apache/lucene/search/CachingCollector;->curDocs:[I

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399
    const/4 v0, 0x4

    .line 400
    .local v0, "bytesPerDoc":I
    if-eqz p4, :cond_0

    .line 401
    add-int/lit8 v0, v0, 0x4

    .line 403
    :cond_0
    mul-double v2, p2, v4

    mul-double/2addr v2, v4

    int-to-double v4, v0

    div-double/2addr v2, v4

    double-to-int v1, v2

    iput v1, p0, Lorg/apache/lucene/search/CachingCollector;->maxDocsToCache:I

    .line 404
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/search/Collector;DZLorg/apache/lucene/search/CachingCollector$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/search/Collector;
    .param p2, "x1"    # D
    .param p4, "x2"    # Z
    .param p5, "x3"    # Lorg/apache/lucene/search/CachingCollector$1;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/search/CachingCollector;-><init>(Lorg/apache/lucene/search/Collector;DZ)V

    return-void
.end method

.method private constructor <init>(Lorg/apache/lucene/search/Collector;I)V
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/search/Collector;
    .param p2, "maxDocsToCache"    # I

    .prologue
    .line 406
    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    .line 317
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/CachingCollector;->cachedSegs:Ljava/util/List;

    .line 407
    iput-object p1, p0, Lorg/apache/lucene/search/CachingCollector;->other:Lorg/apache/lucene/search/Collector;

    .line 409
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/CachingCollector;->cachedDocs:Ljava/util/List;

    .line 410
    const/16 v0, 0x80

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/search/CachingCollector;->curDocs:[I

    .line 411
    iget-object v0, p0, Lorg/apache/lucene/search/CachingCollector;->cachedDocs:Ljava/util/List;

    iget-object v1, p0, Lorg/apache/lucene/search/CachingCollector;->curDocs:[I

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 412
    iput p2, p0, Lorg/apache/lucene/search/CachingCollector;->maxDocsToCache:I

    .line 413
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/search/Collector;ILorg/apache/lucene/search/CachingCollector$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/search/Collector;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lorg/apache/lucene/search/CachingCollector$1;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/CachingCollector;-><init>(Lorg/apache/lucene/search/Collector;I)V

    return-void
.end method

.method static synthetic access$300()[I
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lorg/apache/lucene/search/CachingCollector;->EMPTY_INT_ARRAY:[I

    return-object v0
.end method

.method public static create(Lorg/apache/lucene/search/Collector;ZD)Lorg/apache/lucene/search/CachingCollector;
    .locals 2
    .param p0, "other"    # Lorg/apache/lucene/search/Collector;
    .param p1, "cacheScores"    # Z
    .param p2, "maxRAMMB"    # D

    .prologue
    .line 370
    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;

    invoke-direct {v0, p0, p2, p3}, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;-><init>(Lorg/apache/lucene/search/Collector;D)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;

    invoke-direct {v0, p0, p2, p3}, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;-><init>(Lorg/apache/lucene/search/Collector;D)V

    goto :goto_0
.end method

.method public static create(Lorg/apache/lucene/search/Collector;ZI)Lorg/apache/lucene/search/CachingCollector;
    .locals 1
    .param p0, "other"    # Lorg/apache/lucene/search/Collector;
    .param p1, "cacheScores"    # Z
    .param p2, "maxDocsToCache"    # I

    .prologue
    .line 388
    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;

    invoke-direct {v0, p0, p2}, Lorg/apache/lucene/search/CachingCollector$ScoreCachingCollector;-><init>(Lorg/apache/lucene/search/Collector;I)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;

    invoke-direct {v0, p0, p2}, Lorg/apache/lucene/search/CachingCollector$NoScoreCachingCollector;-><init>(Lorg/apache/lucene/search/Collector;I)V

    goto :goto_0
.end method

.method public static create(ZZD)Lorg/apache/lucene/search/CachingCollector;
    .locals 2
    .param p0, "acceptDocsOutOfOrder"    # Z
    .param p1, "cacheScores"    # Z
    .param p2, "maxRAMMB"    # D

    .prologue
    .line 336
    new-instance v0, Lorg/apache/lucene/search/CachingCollector$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/CachingCollector$1;-><init>(Z)V

    .line 352
    .local v0, "other":Lorg/apache/lucene/search/Collector;
    invoke-static {v0, p1, p2, p3}, Lorg/apache/lucene/search/CachingCollector;->create(Lorg/apache/lucene/search/Collector;ZD)Lorg/apache/lucene/search/CachingCollector;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lorg/apache/lucene/search/CachingCollector;->other:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Collector;->acceptsDocsOutOfOrder()Z

    move-result v0

    return v0
.end method

.method public isCached()Z
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lorg/apache/lucene/search/CachingCollector;->curDocs:[I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract replay(Lorg/apache/lucene/search/Collector;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method replayInit(Lorg/apache/lucene/search/Collector;)V
    .locals 6
    .param p1, "other"    # Lorg/apache/lucene/search/Collector;

    .prologue
    .line 436
    invoke-virtual {p0}, Lorg/apache/lucene/search/CachingCollector;->isCached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 437
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "cannot replay: cache was cleared because too much RAM was required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 440
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/search/Collector;->acceptsDocsOutOfOrder()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/search/CachingCollector;->other:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Collector;->acceptsDocsOutOfOrder()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 441
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "cannot replay: given collector does not support out-of-order collection, while the wrapped collector does. Therefore cached documents may be out-of-order."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 448
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/CachingCollector;->lastReader:Lorg/apache/lucene/index/IndexReader;

    if-eqz v0, :cond_2

    .line 449
    iget-object v0, p0, Lorg/apache/lucene/search/CachingCollector;->cachedSegs:Ljava/util/List;

    new-instance v1, Lorg/apache/lucene/search/CachingCollector$SegStart;

    iget-object v2, p0, Lorg/apache/lucene/search/CachingCollector;->lastReader:Lorg/apache/lucene/index/IndexReader;

    iget v3, p0, Lorg/apache/lucene/search/CachingCollector;->lastDocBase:I

    iget v4, p0, Lorg/apache/lucene/search/CachingCollector;->base:I

    iget v5, p0, Lorg/apache/lucene/search/CachingCollector;->upto:I

    add-int/2addr v4, v5

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/search/CachingCollector$SegStart;-><init>(Lorg/apache/lucene/index/IndexReader;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 450
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/CachingCollector;->lastReader:Lorg/apache/lucene/index/IndexReader;

    .line 452
    :cond_2
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 426
    iget-object v0, p0, Lorg/apache/lucene/search/CachingCollector;->other:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/Collector;->setNextReader(Lorg/apache/lucene/index/IndexReader;I)V

    .line 427
    iget-object v0, p0, Lorg/apache/lucene/search/CachingCollector;->lastReader:Lorg/apache/lucene/index/IndexReader;

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lorg/apache/lucene/search/CachingCollector;->cachedSegs:Ljava/util/List;

    new-instance v1, Lorg/apache/lucene/search/CachingCollector$SegStart;

    iget-object v2, p0, Lorg/apache/lucene/search/CachingCollector;->lastReader:Lorg/apache/lucene/index/IndexReader;

    iget v3, p0, Lorg/apache/lucene/search/CachingCollector;->lastDocBase:I

    iget v4, p0, Lorg/apache/lucene/search/CachingCollector;->base:I

    iget v5, p0, Lorg/apache/lucene/search/CachingCollector;->upto:I

    add-int/2addr v4, v5

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/search/CachingCollector$SegStart;-><init>(Lorg/apache/lucene/index/IndexReader;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 430
    :cond_0
    iput p2, p0, Lorg/apache/lucene/search/CachingCollector;->lastDocBase:I

    .line 431
    iput-object p1, p0, Lorg/apache/lucene/search/CachingCollector;->lastReader:Lorg/apache/lucene/index/IndexReader;

    .line 432
    return-void
.end method
