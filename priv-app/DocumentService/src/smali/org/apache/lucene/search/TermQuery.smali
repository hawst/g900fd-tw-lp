.class public Lorg/apache/lucene/search/TermQuery;
.super Lorg/apache/lucene/search/Query;
.source "TermQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/TermQuery$TermWeight;
    }
.end annotation


# instance fields
.field private term:Lorg/apache/lucene/index/Term;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 0
    .param p1, "t"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 190
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 191
    iput-object p1, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    .line 192
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/TermQuery;

    .prologue
    .line 34
    iget-object v0, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method


# virtual methods
.method public createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 199
    new-instance v0, Lorg/apache/lucene/search/TermQuery$TermWeight;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/TermQuery$TermWeight;-><init>(Lorg/apache/lucene/search/TermQuery;Lorg/apache/lucene/search/Searcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 223
    instance-of v2, p1, Lorg/apache/lucene/search/TermQuery;

    if-nez v2, :cond_1

    .line 226
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 225
    check-cast v0, Lorg/apache/lucene/search/TermQuery;

    .line 226
    .local v0, "other":Lorg/apache/lucene/search/TermQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/TermQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    iget-object v3, v0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 204
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 205
    return-void
.end method

.method public getTerm()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 233
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermQuery;->getBoost()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 211
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 212
    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
