.class public Lorg/apache/lucene/search/FilterManager$FilterItem;
.super Ljava/lang/Object;
.source "FilterManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FilterManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "FilterItem"
.end annotation


# instance fields
.field public filter:Lorg/apache/lucene/search/Filter;

.field final synthetic this$0:Lorg/apache/lucene/search/FilterManager;

.field public timestamp:J


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FilterManager;Lorg/apache/lucene/search/Filter;)V
    .locals 2
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;

    .prologue
    .line 131
    iput-object p1, p0, Lorg/apache/lucene/search/FilterManager$FilterItem;->this$0:Lorg/apache/lucene/search/FilterManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iput-object p2, p0, Lorg/apache/lucene/search/FilterManager$FilterItem;->filter:Lorg/apache/lucene/search/Filter;

    .line 133
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/search/FilterManager$FilterItem;->timestamp:J

    .line 134
    return-void
.end method
