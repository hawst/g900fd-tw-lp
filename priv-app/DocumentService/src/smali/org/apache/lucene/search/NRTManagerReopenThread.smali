.class public Lorg/apache/lucene/search/NRTManagerReopenThread;
.super Ljava/lang/Thread;
.source "NRTManagerReopenThread.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Lorg/apache/lucene/search/NRTManager$WaitingListener;


# instance fields
.field private finish:Z

.field private final manager:Lorg/apache/lucene/search/NRTManager;

.field private final targetMaxStaleNS:J

.field private final targetMinStaleNS:J

.field private waitingGen:J


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/NRTManager;DD)V
    .locals 4
    .param p1, "manager"    # Lorg/apache/lucene/search/NRTManager;
    .param p2, "targetMaxStaleSec"    # D
    .param p4, "targetMinStaleSec"    # D

    .prologue
    const-wide v2, 0x41cdcd6500000000L    # 1.0E9

    .line 106
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 107
    cmpg-double v0, p2, p4

    if-gez v0, :cond_0

    .line 108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "targetMaxScaleSec (= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") < targetMinStaleSec (="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->manager:Lorg/apache/lucene/search/NRTManager;

    .line 111
    mul-double v0, v2, p2

    double-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->targetMaxStaleNS:J

    .line 112
    mul-double v0, v2, p4

    double-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->targetMinStaleNS:J

    .line 113
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/NRTManager;->addWaitingListener(Lorg/apache/lucene/search/NRTManager$WaitingListener;)V

    .line 114
    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 2

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->manager:Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v1, p0}, Lorg/apache/lucene/search/NRTManager;->removeWaitingListener(Lorg/apache/lucene/search/NRTManager$WaitingListener;)V

    .line 119
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->finish:Z

    .line 120
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/lucene/search/NRTManagerReopenThread;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 126
    monitor-exit p0

    return-void

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "ie":Ljava/lang/InterruptedException;
    :try_start_2
    new-instance v1, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 118
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public run()V
    .locals 14

    .prologue
    .line 138
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 144
    .local v4, "lastReopenStartNS":J
    :goto_0
    const/4 v0, 0x0

    .line 146
    .local v0, "hasWaiting":Z
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 150
    :goto_1
    :try_start_1
    iget-boolean v10, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->finish:Z

    if-nez v10, :cond_0

    .line 154
    iget-wide v10, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->waitingGen:J

    iget-object v12, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->manager:Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v12}, Lorg/apache/lucene/search/NRTManager;->getCurrentSearchingGen()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-lez v10, :cond_1

    const/4 v0, 0x1

    .line 155
    :goto_2
    if-eqz v0, :cond_2

    iget-wide v10, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->targetMinStaleNS:J

    :goto_3
    add-long v6, v4, v10

    .line 157
    .local v6, "nextReopenStartNS":J
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v10

    sub-long v8, v6, v10

    .line 159
    .local v8, "sleepNS":J
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-lez v10, :cond_0

    .line 162
    const-wide/32 v10, 0xf4240

    :try_start_2
    div-long v10, v8, v10

    const-wide/32 v12, 0xf4240

    rem-long v12, v8, v12

    long-to-int v12, v12

    invoke-virtual {p0, v10, v11, v12}, Ljava/lang/Object;->wait(JI)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 163
    :catch_0
    move-exception v1

    .line 164
    .local v1, "ie":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Thread;->interrupt()V

    .line 166
    const/4 v10, 0x1

    iput-boolean v10, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->finish:Z

    .line 174
    .end local v1    # "ie":Ljava/lang/InterruptedException;
    .end local v6    # "nextReopenStartNS":J
    .end local v8    # "sleepNS":J
    :cond_0
    iget-boolean v10, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->finish:Z

    if-eqz v10, :cond_3

    .line 176
    monitor-exit p0

    return-void

    .line 154
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 155
    :cond_2
    iget-wide v10, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->targetMaxStaleNS:J

    goto :goto_3

    .line 179
    :cond_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 181
    :try_start_4
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    move-result-wide v4

    .line 184
    :try_start_5
    iget-object v10, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->manager:Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v10}, Lorg/apache/lucene/search/NRTManager;->maybeRefresh()Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 186
    :catch_1
    move-exception v2

    .line 189
    .local v2, "ioe":Ljava/io/IOException;
    :try_start_6
    new-instance v10, Ljava/lang/RuntimeException;

    invoke-direct {v10, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v10
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2

    .line 192
    .end local v2    # "ioe":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 195
    .local v3, "t":Ljava/lang/Throwable;
    new-instance v10, Ljava/lang/RuntimeException;

    invoke-direct {v10, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v10

    .line 179
    .end local v3    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v10

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    throw v10
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_2
.end method

.method public declared-synchronized waiting(J)V
    .locals 3
    .param p1, "targetGen"    # J

    .prologue
    .line 129
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->waitingGen:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/search/NRTManagerReopenThread;->waitingGen:J

    .line 130
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    monitor-exit p0

    return-void

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
