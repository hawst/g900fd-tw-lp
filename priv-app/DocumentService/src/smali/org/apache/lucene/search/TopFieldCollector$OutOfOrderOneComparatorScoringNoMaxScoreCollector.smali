.class Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;
.super Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;
.source "TopFieldCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopFieldCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OutOfOrderOneComparatorScoringNoMaxScoreCollector"
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V
    .locals 0
    .param p2, "numHits"    # I
    .param p3, "fillFields"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;IZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 223
    .local p1, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    .line 224
    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x1

    return v0
.end method

.method public collect(I)V
    .locals 5
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 228
    iget v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->totalHits:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->totalHits:I

    .line 229
    iget-boolean v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->queueFull:Z

    if-eqz v3, :cond_2

    .line 231
    iget v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->reverseMul:I

    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/FieldComparator;->compareBottom(I)I

    move-result v4

    mul-int v0, v3, v4

    .line 232
    .local v0, "cmp":I
    if-ltz v0, :cond_0

    if-nez v0, :cond_1

    iget v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->docBase:I

    add-int/2addr v3, p1

    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v4, v4, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    if-le v3, v4, :cond_1

    .line 256
    .end local v0    # "cmp":I
    :cond_0
    :goto_0
    return-void

    .line 237
    .restart local v0    # "cmp":I
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v1

    .line 240
    .local v1, "score":F
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v4, v4, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v3, v4, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 241
    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->updateBottom(IF)V

    .line 242
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v4, v4, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    goto :goto_0

    .line 245
    .end local v0    # "cmp":I
    .end local v1    # "score":F
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v1

    .line 248
    .restart local v1    # "score":F
    iget v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->totalHits:I

    add-int/lit8 v2, v3, -0x1

    .line 250
    .local v2, "slot":I
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v3, v2, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 251
    invoke-virtual {p0, v2, p1, v1}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->add(IIF)V

    .line 252
    iget-boolean v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->queueFull:Z

    if-eqz v3, :cond_0

    .line 253
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v4, v4, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    goto :goto_0
.end method
