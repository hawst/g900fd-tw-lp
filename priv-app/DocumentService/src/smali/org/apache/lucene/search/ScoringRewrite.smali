.class public abstract Lorg/apache/lucene/search/ScoringRewrite;
.super Lorg/apache/lucene/search/TermCollectingRewrite;
.source "ScoringRewrite.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q:",
        "Lorg/apache/lucene/search/Query;",
        ">",
        "Lorg/apache/lucene/search/TermCollectingRewrite",
        "<TQ;>;"
    }
.end annotation


# static fields
.field public static final CONSTANT_SCORE_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

.field public static final SCORING_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/ScoringRewrite;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/search/ScoringRewrite",
            "<",
            "Lorg/apache/lucene/search/BooleanQuery;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lorg/apache/lucene/search/ScoringRewrite$1;

    invoke-direct {v0}, Lorg/apache/lucene/search/ScoringRewrite$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/ScoringRewrite;->SCORING_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/ScoringRewrite;

    .line 70
    new-instance v0, Lorg/apache/lucene/search/ScoringRewrite$2;

    invoke-direct {v0}, Lorg/apache/lucene/search/ScoringRewrite$2;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/ScoringRewrite;->CONSTANT_SCORE_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    .local p0, "this":Lorg/apache/lucene/search/ScoringRewrite;, "Lorg/apache/lucene/search/ScoringRewrite<TQ;>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/TermCollectingRewrite;-><init>()V

    return-void
.end method


# virtual methods
.method public rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "query"    # Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/IndexReader;",
            "Lorg/apache/lucene/search/MultiTermQuery;",
            ")TQ;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    .local p0, "this":Lorg/apache/lucene/search/ScoringRewrite;, "Lorg/apache/lucene/search/ScoringRewrite<TQ;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/search/ScoringRewrite;->getTopLevelQuery()Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 92
    .local v0, "result":Lorg/apache/lucene/search/Query;, "TQ;"
    const/4 v2, 0x1

    new-array v1, v2, [I

    .line 93
    .local v1, "size":[I
    new-instance v2, Lorg/apache/lucene/search/ScoringRewrite$3;

    invoke-direct {v2, p0, v0, p2, v1}, Lorg/apache/lucene/search/ScoringRewrite$3;-><init>(Lorg/apache/lucene/search/ScoringRewrite;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/MultiTermQuery;[I)V

    invoke-virtual {p0, p1, p2, v2}, Lorg/apache/lucene/search/ScoringRewrite;->collectTerms(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;)V

    .line 100
    const/4 v2, 0x0

    aget v2, v1, v2

    invoke-virtual {p2, v2}, Lorg/apache/lucene/search/MultiTermQuery;->incTotalNumberOfTerms(I)V

    .line 101
    return-object v0
.end method
