.class public final Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;
.super Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;
.source "SpanMultiTermQueryWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TopTermsSpanBooleanQueryRewrite"
.end annotation


# instance fields
.field private final delegate:Lorg/apache/lucene/search/TopTermsRewrite;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/search/TopTermsRewrite",
            "<",
            "Lorg/apache/lucene/search/spans/SpanOrQuery;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 220
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$SpanRewriteMethod;-><init>()V

    .line 221
    new-instance v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite$1;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite$1;-><init>(Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;I)V

    iput-object v0, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;->delegate:Lorg/apache/lucene/search/TopTermsRewrite;

    .line 239
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 258
    if-ne p0, p1, :cond_1

    const/4 v1, 0x1

    .line 262
    :cond_0
    :goto_0
    return v1

    .line 259
    :cond_1
    if-eqz p1, :cond_0

    .line 260
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    move-object v0, p1

    .line 261
    check-cast v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;

    .line 262
    .local v0, "other":Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;->delegate:Lorg/apache/lucene/search/TopTermsRewrite;

    iget-object v2, v0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;->delegate:Lorg/apache/lucene/search/TopTermsRewrite;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/TopTermsRewrite;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;->delegate:Lorg/apache/lucene/search/TopTermsRewrite;

    invoke-virtual {v0}, Lorg/apache/lucene/search/TopTermsRewrite;->getSize()I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;->delegate:Lorg/apache/lucene/search/TopTermsRewrite;

    invoke-virtual {v0}, Lorg/apache/lucene/search/TopTermsRewrite;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    return v0
.end method

.method public bridge synthetic rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "x0"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "x1"    # Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 213
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;->rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v0

    return-object v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "query"    # Lorg/apache/lucene/search/MultiTermQuery;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$TopTermsSpanBooleanQueryRewrite;->delegate:Lorg/apache/lucene/search/TopTermsRewrite;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/search/TopTermsRewrite;->rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/SpanQuery;

    return-object v0
.end method
