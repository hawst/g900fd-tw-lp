.class Lorg/apache/lucene/search/FieldCacheDocIdSet$2;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "FieldCacheDocIdSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheDocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private doc:I

.field final synthetic this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

.field final synthetic val$termDocs:Lorg/apache/lucene/index/TermDocs;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheDocIdSet;Lorg/apache/lucene/index/TermDocs;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->val$termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->doc:I

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    .line 107
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->val$termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v1, p1}, Lorg/apache/lucene/index/TermDocs;->skipTo(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 108
    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->doc:I

    .line 113
    :goto_0
    return v0

    .line 109
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->val$termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v2}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->doc:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/FieldCacheDocIdSet;->matchDoc(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 110
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->val$termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->doc:I

    goto :goto_0

    .line 113
    :cond_1
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->doc:I

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->doc:I

    return v0
.end method

.method public nextDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->val$termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->doc:I

    .line 102
    :goto_0
    return v0

    .line 101
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->val$termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v1}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->doc:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/FieldCacheDocIdSet;->matchDoc(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$2;->doc:I

    goto :goto_0
.end method
