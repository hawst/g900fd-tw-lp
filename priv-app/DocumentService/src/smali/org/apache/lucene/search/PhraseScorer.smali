.class abstract Lorg/apache/lucene/search/PhraseScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "PhraseScorer.java"


# instance fields
.field private freq:F

.field max:Lorg/apache/lucene/search/PhrasePositions;

.field min:Lorg/apache/lucene/search/PhrasePositions;

.field protected norms:[B

.field protected value:F


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;Lorg/apache/lucene/search/Similarity;[B)V
    .locals 8
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "postings"    # [Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
    .param p3, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p4, "norms"    # [B

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 42
    invoke-direct {p0, p3, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;)V

    .line 43
    iput-object p4, p0, Lorg/apache/lucene/search/PhraseScorer;->norms:[B

    .line 44
    invoke-virtual {p1}, Lorg/apache/lucene/search/Weight;->getValue()F

    move-result v2

    iput v2, p0, Lorg/apache/lucene/search/PhraseScorer;->value:F

    .line 51
    array-length v2, p2

    if-lez v2, :cond_1

    .line 52
    new-instance v2, Lorg/apache/lucene/search/PhrasePositions;

    aget-object v3, p2, v6

    iget-object v3, v3, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->postings:Lorg/apache/lucene/index/TermPositions;

    aget-object v4, p2, v6

    iget v4, v4, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    aget-object v5, p2, v6

    iget-object v5, v5, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    invoke-direct {v2, v3, v4, v6, v5}, Lorg/apache/lucene/search/PhrasePositions;-><init>(Lorg/apache/lucene/index/TermPositions;II[Lorg/apache/lucene/index/Term;)V

    iput-object v2, p0, Lorg/apache/lucene/search/PhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    .line 53
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    iput-object v2, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    .line 54
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iput v7, v2, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    .line 55
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    .line 56
    new-instance v1, Lorg/apache/lucene/search/PhrasePositions;

    aget-object v2, p2, v0

    iget-object v2, v2, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->postings:Lorg/apache/lucene/index/TermPositions;

    aget-object v3, p2, v0

    iget v3, v3, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    aget-object v4, p2, v0

    iget-object v4, v4, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->terms:[Lorg/apache/lucene/index/Term;

    invoke-direct {v1, v2, v3, v0, v4}, Lorg/apache/lucene/search/PhrasePositions;-><init>(Lorg/apache/lucene/index/TermPositions;II[Lorg/apache/lucene/index/Term;)V

    .line 57
    .local v1, "pp":Lorg/apache/lucene/search/PhrasePositions;
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iput-object v1, v2, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    .line 58
    iput-object v1, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    .line 59
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iput v7, v2, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    .end local v1    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget-object v3, p0, Lorg/apache/lucene/search/PhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    iput-object v3, v2, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    .line 63
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method private advanceMin(I)Z
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/PhrasePositions;->skipTo(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    const v1, 0x7fffffff

    iput v1, v0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    .line 78
    const/4 v0, 0x0

    .line 82
    :goto_0
    return v0

    .line 80
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    iget-object v0, v0, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    iput-object v0, p0, Lorg/apache/lucene/search/PhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    .line 81
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget-object v0, v0, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    iput-object v0, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    .line 82
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public advance(I)I
    .locals 5
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v1, 0x7fffffff

    const/4 v4, 0x0

    .line 94
    iput v4, p0, Lorg/apache/lucene/search/PhraseScorer;->freq:F

    .line 95
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/PhraseScorer;->advanceMin(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 112
    :goto_0
    return v1

    .line 98
    :cond_0
    const/4 v0, 0x0

    .line 99
    .local v0, "restart":Z
    :goto_1
    iget v2, p0, Lorg/apache/lucene/search/PhraseScorer;->freq:F

    cmpl-float v2, v2, v4

    if-nez v2, :cond_4

    .line 100
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    iget v2, v2, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    iget-object v3, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget v3, v3, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    if-lt v2, v3, :cond_2

    if-eqz v0, :cond_3

    .line 101
    :cond_2
    const/4 v0, 0x0

    .line 102
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget v2, v2, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    invoke-direct {p0, v2}, Lorg/apache/lucene/search/PhraseScorer;->advanceMin(I)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 107
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhraseScorer;->phraseFreq()F

    move-result v2

    iput v2, p0, Lorg/apache/lucene/search/PhraseScorer;->freq:F

    .line 108
    const/4 v0, 0x1

    goto :goto_1

    .line 112
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget v1, v1, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget v0, v0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    return v0
.end method

.method public final freq()F
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lorg/apache/lucene/search/PhraseScorer;->freq:F

    return v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget v0, v0, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/PhraseScorer;->advance(I)I

    move-result v0

    return v0
.end method

.method abstract phraseFreq()F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public score()F
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhraseScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/PhraseScorer;->freq:F

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Similarity;->tf(F)F

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/PhraseScorer;->value:F

    mul-float v0, v1, v2

    .line 89
    .local v0, "raw":F
    iget-object v1, p0, Lorg/apache/lucene/search/PhraseScorer;->norms:[B

    if-nez v1, :cond_0

    .end local v0    # "raw":F
    :goto_0
    return v0

    .restart local v0    # "raw":F
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhraseScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/PhraseScorer;->norms:[B

    iget-object v3, p0, Lorg/apache/lucene/search/PhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    iget v3, v3, Lorg/apache/lucene/search/PhrasePositions;->doc:I

    aget-byte v2, v2, v3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Similarity;->decodeNormValue(B)F

    move-result v1

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "scorer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/PhraseScorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
