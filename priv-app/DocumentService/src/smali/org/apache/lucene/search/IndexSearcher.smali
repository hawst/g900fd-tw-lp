.class public Lorg/apache/lucene/search/IndexSearcher;
.super Lorg/apache/lucene/search/Searcher;
.source "IndexSearcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;,
        Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;,
        Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;
    }
.end annotation


# instance fields
.field private closeReader:Z

.field private final docBase:I

.field protected final docStarts:[I

.field private final executor:Ljava/util/concurrent/ExecutorService;

.field private fieldSortDoMaxScore:Z

.field private fieldSortDoTrackScores:Z

.field reader:Lorg/apache/lucene/index/IndexReader;

.field protected final subReaders:[Lorg/apache/lucene/index/IndexReader;

.field protected final subSearchers:[Lorg/apache/lucene/search/IndexSearcher;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;)V
    .locals 2
    .param p1, "r"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 115
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReader;ZLjava/util/concurrent/ExecutorService;)V

    .line 116
    return-void
.end method

.method private constructor <init>(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 4
    .param p1, "r"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 144
    invoke-direct {p0}, Lorg/apache/lucene/search/Searcher;-><init>()V

    .line 145
    iput-object p1, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    .line 146
    iput-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    .line 147
    iput-boolean v1, p0, Lorg/apache/lucene/search/IndexSearcher;->closeReader:Z

    .line 148
    iput p2, p0, Lorg/apache/lucene/search/IndexSearcher;->docBase:I

    .line 149
    new-array v0, v2, [Lorg/apache/lucene/index/IndexReader;

    aput-object p1, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    .line 150
    new-array v0, v2, [I

    aput v1, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->docStarts:[I

    .line 151
    iput-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->subSearchers:[Lorg/apache/lucene/search/IndexSearcher;

    .line 152
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p1, "r"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "executor"    # Ljava/util/concurrent/ExecutorService;

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReader;ZLjava/util/concurrent/ExecutorService;)V

    .line 131
    return-void
.end method

.method private constructor <init>(Lorg/apache/lucene/index/IndexReader;ZLjava/util/concurrent/ExecutorService;)V
    .locals 7
    .param p1, "r"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "closeReader"    # Z
    .param p3, "executor"    # Ljava/util/concurrent/ExecutorService;

    .prologue
    .line 184
    invoke-direct {p0}, Lorg/apache/lucene/search/Searcher;-><init>()V

    .line 185
    iput-object p1, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    .line 186
    iput-object p3, p0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    .line 187
    iput-boolean p2, p0, Lorg/apache/lucene/search/IndexSearcher;->closeReader:Z

    .line 189
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 190
    .local v2, "subReadersList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexReader;>;"
    iget-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {p0, v2, v3}, Lorg/apache/lucene/search/IndexSearcher;->gatherSubReaders(Ljava/util/List;Lorg/apache/lucene/index/IndexReader;)V

    .line 191
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/lucene/index/IndexReader;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/index/IndexReader;

    iput-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    .line 192
    iget-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v3, v3

    new-array v3, v3, [I

    iput-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->docStarts:[I

    .line 193
    const/4 v1, 0x0

    .line 194
    .local v1, "maxDoc":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 195
    iget-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->docStarts:[I

    aput v1, v3, v0

    .line 196
    iget-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v3

    add-int/2addr v1, v3

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    :cond_0
    if-nez p3, :cond_2

    .line 199
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->subSearchers:[Lorg/apache/lucene/search/IndexSearcher;

    .line 206
    :cond_1
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/lucene/search/IndexSearcher;->docBase:I

    .line 207
    return-void

    .line 201
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v3, v3

    new-array v3, v3, [Lorg/apache/lucene/search/IndexSearcher;

    iput-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->subSearchers:[Lorg/apache/lucene/search/IndexSearcher;

    .line 202
    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 203
    iget-object v3, p0, Lorg/apache/lucene/search/IndexSearcher;->subSearchers:[Lorg/apache/lucene/search/IndexSearcher;

    new-instance v4, Lorg/apache/lucene/search/IndexSearcher;

    iget-object v5, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v5, v5, v0

    iget-object v6, p0, Lorg/apache/lucene/search/IndexSearcher;->docStarts:[I

    aget v6, v6, v0

    invoke-direct {v4, v5, v6}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReader;I)V

    aput-object v4, v3, v0

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[I)V
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "subReaders"    # [Lorg/apache/lucene/index/IndexReader;
    .param p3, "docStarts"    # [I

    .prologue
    .line 138
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[ILjava/util/concurrent/ExecutorService;)V

    .line 139
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;[Lorg/apache/lucene/index/IndexReader;[ILjava/util/concurrent/ExecutorService;)V
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "subReaders"    # [Lorg/apache/lucene/index/IndexReader;
    .param p3, "docStarts"    # [I
    .param p4, "executor"    # Ljava/util/concurrent/ExecutorService;

    .prologue
    const/4 v5, 0x0

    .line 167
    invoke-direct {p0}, Lorg/apache/lucene/search/Searcher;-><init>()V

    .line 168
    iput-object p1, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    .line 169
    iput-object p2, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    .line 170
    iput-object p3, p0, Lorg/apache/lucene/search/IndexSearcher;->docStarts:[I

    .line 171
    if-nez p4, :cond_1

    .line 172
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/search/IndexSearcher;->subSearchers:[Lorg/apache/lucene/search/IndexSearcher;

    .line 179
    :cond_0
    iput-boolean v5, p0, Lorg/apache/lucene/search/IndexSearcher;->closeReader:Z

    .line 180
    iput-object p4, p0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    .line 181
    iput v5, p0, Lorg/apache/lucene/search/IndexSearcher;->docBase:I

    .line 182
    return-void

    .line 174
    :cond_1
    array-length v1, p2

    new-array v1, v1, [Lorg/apache/lucene/search/IndexSearcher;

    iput-object v1, p0, Lorg/apache/lucene/search/IndexSearcher;->subSearchers:[Lorg/apache/lucene/search/IndexSearcher;

    .line 175
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 176
    iget-object v1, p0, Lorg/apache/lucene/search/IndexSearcher;->subSearchers:[Lorg/apache/lucene/search/IndexSearcher;

    new-instance v2, Lorg/apache/lucene/search/IndexSearcher;

    aget-object v3, p2, v0

    aget v4, p3, v0

    invoke-direct {v2, v3, v4}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReader;I)V

    aput-object v2, v1, v0

    .line 175
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;)V
    .locals 3
    .param p1, "path"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 93
    invoke-static {p1, v2}, Lorg/apache/lucene/index/IndexReader;->open(Lorg/apache/lucene/store/Directory;Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v2, v1}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReader;ZLjava/util/concurrent/ExecutorService;)V

    .line 94
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Z)V
    .locals 3
    .param p1, "path"    # Lorg/apache/lucene/store/Directory;
    .param p2, "readOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 110
    invoke-static {p1, p2}, Lorg/apache/lucene/index/IndexReader;->open(Lorg/apache/lucene/store/Directory;Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/lucene/search/IndexSearcher;-><init>(Lorg/apache/lucene/index/IndexReader;ZLjava/util/concurrent/ExecutorService;)V

    .line 111
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/search/IndexSearcher;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/IndexSearcher;

    .prologue
    .line 69
    iget v0, p0, Lorg/apache/lucene/search/IndexSearcher;->docBase:I

    return v0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 289
    iget-boolean v0, p0, Lorg/apache/lucene/search/IndexSearcher;->closeReader:Z

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->close()V

    .line 292
    :cond_0
    return-void
.end method

.method public createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 664
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Searcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    return-object v0
.end method

.method public doc(I)Lorg/apache/lucene/document/Document;
    .locals 1
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 258
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->document(I)Lorg/apache/lucene/document/Document;

    move-result-object v0

    return-object v0
.end method

.method public doc(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;
    .locals 1
    .param p1, "docID"    # I
    .param p2, "fieldSelector"    # Lorg/apache/lucene/document/FieldSelector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 264
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexReader;->document(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;

    move-result-object v0

    return-object v0
.end method

.method public docFreq(Lorg/apache/lucene/index/Term;)I
    .locals 7
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    iget-object v6, p0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    if-nez v6, :cond_1

    .line 236
    iget-object v6, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v6, p1}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v0

    .line 251
    :cond_0
    return v0

    .line 238
    :cond_1
    new-instance v4, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;

    iget-object v6, p0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v4, v6}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;-><init>(Ljava/util/concurrent/Executor;)V

    .line 239
    .local v4, "runner":Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v6, v6

    if-ge v1, v6, :cond_2

    .line 240
    iget-object v6, p0, Lorg/apache/lucene/search/IndexSearcher;->subSearchers:[Lorg/apache/lucene/search/IndexSearcher;

    aget-object v5, v6, v1

    .line 241
    .local v5, "searchable":Lorg/apache/lucene/search/IndexSearcher;
    new-instance v6, Lorg/apache/lucene/search/IndexSearcher$1;

    invoke-direct {v6, p0, v5, p1}, Lorg/apache/lucene/search/IndexSearcher$1;-><init>(Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/index/Term;)V

    invoke-virtual {v4, v6}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;->submit(Ljava/util/concurrent/Callable;)V

    .line 239
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 247
    .end local v5    # "searchable":Lorg/apache/lucene/search/IndexSearcher;
    :cond_2
    const/4 v0, 0x0

    .line 248
    .local v0, "docFreq":I
    invoke-virtual {v4}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 249
    .local v3, "num":Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int/2addr v0, v6

    goto :goto_1
.end method

.method public explain(Lorg/apache/lucene/search/Query;I)Lorg/apache/lucene/search/Explanation;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 609
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/lucene/search/IndexSearcher;->explain(Lorg/apache/lucene/search/Weight;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v0

    return-object v0
.end method

.method public explain(Lorg/apache/lucene/search/Weight;I)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 625
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher;->docStarts:[I

    invoke-static {p2, v2}, Lorg/apache/lucene/util/ReaderUtil;->subIndex(I[I)I

    move-result v1

    .line 626
    .local v1, "n":I
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher;->docStarts:[I

    aget v2, v2, v1

    sub-int v0, p2, v2

    .line 628
    .local v0, "deBasedDoc":I
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2, v0}, Lorg/apache/lucene/search/Weight;->explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v2

    return-object v2
.end method

.method protected gatherSubReaders(Ljava/util/List;Lorg/apache/lucene/index/IndexReader;)V
    .locals 0
    .param p2, "r"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;",
            "Lorg/apache/lucene/index/IndexReader;",
            ")V"
        }
    .end annotation

    .prologue
    .line 210
    .local p1, "allSubReaders":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexReader;>;"
    invoke-static {p1, p2}, Lorg/apache/lucene/util/ReaderUtil;->gatherSubReaders(Ljava/util/List;Lorg/apache/lucene/index/IndexReader;)V

    .line 211
    return-void
.end method

.method public getIndexReader()Lorg/apache/lucene/index/IndexReader;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    return-object v0
.end method

.method public getSimilarity()Lorg/apache/lucene/search/Similarity;
    .locals 1

    .prologue
    .line 278
    invoke-super {p0}, Lorg/apache/lucene/search/Searcher;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    return-object v0
.end method

.method public getSubReaders()[Lorg/apache/lucene/index/IndexReader;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    return-object v0
.end method

.method public maxDoc()I
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v0

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "original"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 591
    move-object v0, p1

    .line 592
    .local v0, "query":Lorg/apache/lucene/search/Query;
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    .local v1, "rewrittenQuery":Lorg/apache/lucene/search/Query;
    :goto_0
    if-eq v1, v0, :cond_0

    .line 594
    move-object v0, v1

    .line 593
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    goto :goto_0

    .line 596
    :cond_0
    return-object v0
.end method

.method public search(Lorg/apache/lucene/search/Query;I)Lorg/apache/lucene/search/TopDocs;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 330
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method public search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 342
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method public search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "nDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 426
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method protected search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;
    .locals 20
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p4, "nDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 437
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    if-nez v3, :cond_2

    .line 439
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v14

    .line 440
    .local v14, "limit":I
    if-nez v14, :cond_0

    .line 441
    const/4 v14, 0x1

    .line 443
    :cond_0
    move/from16 v0, p4

    invoke-static {v0, v14}, Ljava/lang/Math;->min(II)I

    move-result p4

    .line 444
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/Weight;->scoresDocsOutOfOrder()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    move/from16 v0, p4

    move-object/from16 v1, p3

    invoke-static {v0, v1, v3}, Lorg/apache/lucene/search/TopScoreDocCollector;->create(ILorg/apache/lucene/search/ScoreDoc;Z)Lorg/apache/lucene/search/TopScoreDocCollector;

    move-result-object v11

    .line 445
    .local v11, "collector":Lorg/apache/lucene/search/TopScoreDocCollector;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v11}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V

    .line 446
    invoke-virtual {v11}, Lorg/apache/lucene/search/TopScoreDocCollector;->topDocs()Lorg/apache/lucene/search/TopDocs;

    move-result-object v3

    .line 470
    .end local v11    # "collector":Lorg/apache/lucene/search/TopScoreDocCollector;
    .end local v14    # "limit":I
    :goto_1
    return-object v3

    .line 444
    .restart local v14    # "limit":I
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 448
    .end local v14    # "limit":I
    :cond_2
    new-instance v10, Lorg/apache/lucene/search/HitQueue;

    const/4 v3, 0x0

    move/from16 v0, p4

    invoke-direct {v10, v0, v3}, Lorg/apache/lucene/search/HitQueue;-><init>(IZ)V

    .line 449
    .local v10, "hq":Lorg/apache/lucene/search/HitQueue;
    new-instance v4, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v4}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 450
    .local v4, "lock":Ljava/util/concurrent/locks/Lock;
    new-instance v16, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;-><init>(Ljava/util/concurrent/Executor;)V

    .line 452
    .local v16, "runner":Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper<Lorg/apache/lucene/search/TopDocs;>;"
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v3, v3

    if-ge v12, v3, :cond_3

    .line 453
    new-instance v3, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/search/IndexSearcher;->subSearchers:[Lorg/apache/lucene/search/IndexSearcher;

    aget-object v5, v5, v12

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move/from16 v9, p4

    invoke-direct/range {v3 .. v10}, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableNoSort;-><init>(Ljava/util/concurrent/locks/Lock;Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/ScoreDoc;ILorg/apache/lucene/search/HitQueue;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;->submit(Ljava/util/concurrent/Callable;)V

    .line 452
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 457
    :cond_3
    const/16 v19, 0x0

    .line 458
    .local v19, "totalHits":I
    const/high16 v15, -0x800000    # Float.NEGATIVE_INFINITY

    .line 459
    .local v15, "maxScore":F
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/lucene/search/TopDocs;

    .line 460
    .local v18, "topDocs":Lorg/apache/lucene/search/TopDocs;
    move-object/from16 v0, v18

    iget v3, v0, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    if-eqz v3, :cond_4

    .line 461
    move-object/from16 v0, v18

    iget v3, v0, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    add-int v19, v19, v3

    .line 462
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/search/TopDocs;->getMaxScore()F

    move-result v3

    invoke-static {v15, v3}, Ljava/lang/Math;->max(FF)F

    move-result v15

    goto :goto_3

    .line 466
    .end local v18    # "topDocs":Lorg/apache/lucene/search/TopDocs;
    :cond_5
    invoke-virtual {v10}, Lorg/apache/lucene/search/HitQueue;->size()I

    move-result v3

    new-array v0, v3, [Lorg/apache/lucene/search/ScoreDoc;

    move-object/from16 v17, v0

    .line 467
    .local v17, "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    invoke-virtual {v10}, Lorg/apache/lucene/search/HitQueue;->size()I

    move-result v3

    add-int/lit8 v12, v3, -0x1

    :goto_4
    if-ltz v12, :cond_6

    .line 468
    invoke-virtual {v10}, Lorg/apache/lucene/search/HitQueue;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/ScoreDoc;

    aput-object v3, v17, v12

    .line 467
    add-int/lit8 v12, v12, -0x1

    goto :goto_4

    .line 470
    :cond_6
    new-instance v3, Lorg/apache/lucene/search/TopDocs;

    move/from16 v0, v19

    move-object/from16 v1, v17

    invoke-direct {v3, v0, v1, v15}, Lorg/apache/lucene/search/TopDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;F)V

    goto/16 :goto_1
.end method

.method public search(Lorg/apache/lucene/search/Query;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 2
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "n"    # I
    .param p3, "sort"    # Lorg/apache/lucene/search/Sort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 414
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p2, p3}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method public search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "n"    # I
    .param p4, "sort"    # Lorg/apache/lucene/search/Sort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 400
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method public search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 6
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "nDocs"    # I
    .param p4, "sort"    # Lorg/apache/lucene/search/Sort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 487
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;Z)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method protected search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;Z)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 20
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "nDocs"    # I
    .param p4, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p5, "fillFields"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 505
    if-nez p4, :cond_0

    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    .line 507
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    if-nez v3, :cond_3

    .line 509
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v14

    .line 510
    .local v14, "limit":I
    if-nez v14, :cond_1

    .line 511
    const/4 v14, 0x1

    .line 513
    :cond_1
    move/from16 v0, p3

    invoke-static {v0, v14}, Ljava/lang/Math;->min(II)I

    move-result p3

    .line 515
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lorg/apache/lucene/search/IndexSearcher;->fieldSortDoTrackScores:Z

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lorg/apache/lucene/search/IndexSearcher;->fieldSortDoMaxScore:Z

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/search/Weight;->scoresDocsOutOfOrder()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v8, 0x1

    :goto_0
    move-object/from16 v3, p4

    move/from16 v4, p3

    move/from16 v5, p5

    invoke-static/range {v3 .. v8}, Lorg/apache/lucene/search/TopFieldCollector;->create(Lorg/apache/lucene/search/Sort;IZZZZ)Lorg/apache/lucene/search/TopFieldCollector;

    move-result-object v11

    .line 517
    .local v11, "collector":Lorg/apache/lucene/search/TopFieldCollector;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v11}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V

    .line 518
    invoke-virtual {v11}, Lorg/apache/lucene/search/TopFieldCollector;->topDocs()Lorg/apache/lucene/search/TopDocs;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/TopFieldDocs;

    .line 543
    .end local v11    # "collector":Lorg/apache/lucene/search/TopFieldCollector;
    .end local v14    # "limit":I
    :goto_1
    return-object v3

    .line 515
    .restart local v14    # "limit":I
    :cond_2
    const/4 v8, 0x0

    goto :goto_0

    .line 520
    .end local v14    # "limit":I
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lorg/apache/lucene/search/IndexSearcher;->fieldSortDoTrackScores:Z

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lorg/apache/lucene/search/IndexSearcher;->fieldSortDoMaxScore:Z

    const/4 v8, 0x0

    move-object/from16 v3, p4

    move/from16 v4, p3

    move/from16 v5, p5

    invoke-static/range {v3 .. v8}, Lorg/apache/lucene/search/TopFieldCollector;->create(Lorg/apache/lucene/search/Sort;IZZZZ)Lorg/apache/lucene/search/TopFieldCollector;

    move-result-object v9

    .line 526
    .local v9, "topCollector":Lorg/apache/lucene/search/TopFieldCollector;
    new-instance v4, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v4}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 527
    .local v4, "lock":Ljava/util/concurrent/locks/Lock;
    new-instance v16, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/IndexSearcher;->executor:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;-><init>(Ljava/util/concurrent/Executor;)V

    .line 528
    .local v16, "runner":Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper<Lorg/apache/lucene/search/TopFieldDocs;>;"
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v3, v3

    if-ge v12, v3, :cond_4

    .line 529
    new-instance v3, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/search/IndexSearcher;->subSearchers:[Lorg/apache/lucene/search/IndexSearcher;

    aget-object v5, v5, v12

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move/from16 v8, p3

    move-object/from16 v10, p4

    invoke-direct/range {v3 .. v10}, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;-><init>(Ljava/util/concurrent/locks/Lock;Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/TopFieldCollector;Lorg/apache/lucene/search/Sort;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;->submit(Ljava/util/concurrent/Callable;)V

    .line 528
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 532
    :cond_4
    const/16 v19, 0x0

    .line 533
    .local v19, "totalHits":I
    const/high16 v15, -0x800000    # Float.NEGATIVE_INFINITY

    .line 534
    .local v15, "maxScore":F
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/search/IndexSearcher$ExecutionHelper;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/lucene/search/TopFieldDocs;

    .line 535
    .local v18, "topFieldDocs":Lorg/apache/lucene/search/TopFieldDocs;
    move-object/from16 v0, v18

    iget v3, v0, Lorg/apache/lucene/search/TopFieldDocs;->totalHits:I

    if-eqz v3, :cond_5

    .line 536
    move-object/from16 v0, v18

    iget v3, v0, Lorg/apache/lucene/search/TopFieldDocs;->totalHits:I

    add-int v19, v19, v3

    .line 537
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/search/TopFieldDocs;->getMaxScore()F

    move-result v3

    invoke-static {v15, v3}, Ljava/lang/Math;->max(FF)F

    move-result v15

    goto :goto_3

    .line 541
    .end local v18    # "topFieldDocs":Lorg/apache/lucene/search/TopFieldDocs;
    :cond_6
    invoke-virtual {v9}, Lorg/apache/lucene/search/TopFieldCollector;->topDocs()Lorg/apache/lucene/search/TopDocs;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/search/TopFieldDocs;

    .line 543
    .local v17, "topDocs":Lorg/apache/lucene/search/TopFieldDocs;
    new-instance v3, Lorg/apache/lucene/search/TopFieldDocs;

    move-object/from16 v0, v17

    iget-object v5, v0, Lorg/apache/lucene/search/TopFieldDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    move-object/from16 v0, v17

    iget-object v6, v0, Lorg/apache/lucene/search/TopFieldDocs;->fields:[Lorg/apache/lucene/search/SortField;

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/search/TopFieldDocs;->getMaxScore()F

    move-result v7

    move/from16 v0, v19

    invoke-direct {v3, v0, v5, v6, v7}, Lorg/apache/lucene/search/TopFieldDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;[Lorg/apache/lucene/search/SortField;F)V

    goto/16 :goto_1
.end method

.method public search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Collector;)V
    .locals 2
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "results"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 383
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p2}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V

    .line 384
    return-void
.end method

.method public search(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V
    .locals 1
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "results"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 364
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V

    .line 365
    return-void
.end method

.method public search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V
    .locals 6
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 575
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 576
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v2, v2, v0

    iget v4, p0, Lorg/apache/lucene/search/IndexSearcher;->docBase:I

    iget-object v5, p0, Lorg/apache/lucene/search/IndexSearcher;->docStarts:[I

    aget v5, v5, v0

    add-int/2addr v4, v5

    invoke-virtual {p3, v2, v4}, Lorg/apache/lucene/search/Collector;->setNextReader(Lorg/apache/lucene/index/IndexReader;I)V

    .line 577
    if-nez p2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v4, v2, v0

    invoke-virtual {p3}, Lorg/apache/lucene/search/Collector;->acceptsDocsOutOfOrder()Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    invoke-virtual {p1, v4, v2, v3}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;

    move-result-object v1

    .line 580
    .local v1, "scorer":Lorg/apache/lucene/search/Scorer;
    :goto_2
    if-eqz v1, :cond_0

    .line 581
    invoke-virtual {v1, p3}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;)V

    .line 575
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 577
    .end local v1    # "scorer":Lorg/apache/lucene/search/Scorer;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/IndexSearcher;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v2, v2, v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/IndexSearcher;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v4

    invoke-static {v2, v4, p1, p1, p2}, Lorg/apache/lucene/search/FilteredQuery;->getFilteredScorer(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;)Lorg/apache/lucene/search/Scorer;

    move-result-object v1

    goto :goto_2

    .line 584
    :cond_3
    return-void
.end method

.method public searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;I)Lorg/apache/lucene/search/TopDocs;
    .locals 1
    .param p1, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "query"    # Lorg/apache/lucene/search/Query;
    .param p3, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 305
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lorg/apache/lucene/search/IndexSearcher;->searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method public searchAfter(Lorg/apache/lucene/search/ScoreDoc;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;
    .locals 1
    .param p1, "after"    # Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "query"    # Lorg/apache/lucene/search/Query;
    .param p3, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p4, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 319
    invoke-virtual {p0, p2}, Lorg/apache/lucene/search/IndexSearcher;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v0

    invoke-virtual {p0, v0, p3, p1, p4}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultFieldSortScoring(ZZ)V
    .locals 5
    .param p1, "doTrackScores"    # Z
    .param p2, "doMaxScore"    # Z

    .prologue
    .line 647
    iput-boolean p1, p0, Lorg/apache/lucene/search/IndexSearcher;->fieldSortDoTrackScores:Z

    .line 648
    iput-boolean p2, p0, Lorg/apache/lucene/search/IndexSearcher;->fieldSortDoMaxScore:Z

    .line 649
    iget-object v4, p0, Lorg/apache/lucene/search/IndexSearcher;->subSearchers:[Lorg/apache/lucene/search/IndexSearcher;

    if-eqz v4, :cond_0

    .line 650
    iget-object v0, p0, Lorg/apache/lucene/search/IndexSearcher;->subSearchers:[Lorg/apache/lucene/search/IndexSearcher;

    .local v0, "arr$":[Lorg/apache/lucene/search/IndexSearcher;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 651
    .local v3, "sub":Lorg/apache/lucene/search/IndexSearcher;
    invoke-virtual {v3, p1, p2}, Lorg/apache/lucene/search/IndexSearcher;->setDefaultFieldSortScoring(ZZ)V

    .line 650
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 654
    .end local v0    # "arr$":[Lorg/apache/lucene/search/IndexSearcher;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "sub":Lorg/apache/lucene/search/IndexSearcher;
    :cond_0
    return-void
.end method

.method public setSimilarity(Lorg/apache/lucene/search/Similarity;)V
    .locals 0
    .param p1, "similarity"    # Lorg/apache/lucene/search/Similarity;

    .prologue
    .line 273
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Searcher;->setSimilarity(Lorg/apache/lucene/search/Similarity;)V

    .line 274
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 862
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "IndexSearcher("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/IndexSearcher;->reader:Lorg/apache/lucene/index/IndexReader;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
