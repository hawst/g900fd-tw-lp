.class public Lorg/apache/lucene/search/TermRangeQuery;
.super Lorg/apache/lucene/search/MultiTermQuery;
.source "TermRangeQuery.java"


# instance fields
.field private collator:Ljava/text/Collator;

.field private field:Ljava/lang/String;

.field private includeLower:Z

.field private includeUpper:Z

.field private lowerTerm:Ljava/lang/String;

.field private upperTerm:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 7
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "lowerTerm"    # Ljava/lang/String;
    .param p3, "upperTerm"    # Ljava/lang/String;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z

    .prologue
    .line 72
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/search/TermRangeQuery;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/text/Collator;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/text/Collator;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "lowerTerm"    # Ljava/lang/String;
    .param p3, "upperTerm"    # Ljava/lang/String;
    .param p4, "includeLower"    # Z
    .param p5, "includeUpper"    # Z
    .param p6, "collator"    # Ljava/text/Collator;

    .prologue
    .line 106
    invoke-direct {p0}, Lorg/apache/lucene/search/MultiTermQuery;-><init>()V

    .line 107
    iput-object p1, p0, Lorg/apache/lucene/search/TermRangeQuery;->field:Ljava/lang/String;

    .line 108
    iput-object p2, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Ljava/lang/String;

    .line 109
    iput-object p3, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Ljava/lang/String;

    .line 110
    iput-boolean p4, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    .line 111
    iput-boolean p5, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeUpper:Z

    .line 112
    iput-object p6, p0, Lorg/apache/lucene/search/TermRangeQuery;->collator:Ljava/text/Collator;

    .line 113
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 171
    if-ne p0, p1, :cond_1

    .line 202
    :cond_0
    :goto_0
    return v1

    .line 173
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 174
    goto :goto_0

    .line 175
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 176
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 177
    check-cast v0, Lorg/apache/lucene/search/TermRangeQuery;

    .line 178
    .local v0, "other":Lorg/apache/lucene/search/TermRangeQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->collator:Ljava/text/Collator;

    if-nez v3, :cond_4

    .line 179
    iget-object v3, v0, Lorg/apache/lucene/search/TermRangeQuery;->collator:Ljava/text/Collator;

    if-eqz v3, :cond_5

    move v1, v2

    .line 180
    goto :goto_0

    .line 181
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->collator:Ljava/text/Collator;

    iget-object v4, v0, Lorg/apache/lucene/search/TermRangeQuery;->collator:Ljava/text/Collator;

    invoke-virtual {v3, v4}, Ljava/text/Collator;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 182
    goto :goto_0

    .line 183
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->field:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 184
    iget-object v3, v0, Lorg/apache/lucene/search/TermRangeQuery;->field:Ljava/lang/String;

    if-eqz v3, :cond_7

    move v1, v2

    .line 185
    goto :goto_0

    .line 186
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->field:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/search/TermRangeQuery;->field:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 187
    goto :goto_0

    .line 188
    :cond_7
    iget-boolean v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 189
    goto :goto_0

    .line 190
    :cond_8
    iget-boolean v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeUpper:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/TermRangeQuery;->includeUpper:Z

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 191
    goto :goto_0

    .line 192
    :cond_9
    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Ljava/lang/String;

    if-nez v3, :cond_a

    .line 193
    iget-object v3, v0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v1, v2

    .line 194
    goto :goto_0

    .line 195
    :cond_a
    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    move v1, v2

    .line 196
    goto :goto_0

    .line 197
    :cond_b
    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Ljava/lang/String;

    if-nez v3, :cond_c

    .line 198
    iget-object v3, v0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 199
    goto :goto_0

    .line 200
    :cond_c
    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 201
    goto :goto_0
.end method

.method public getCollator()Ljava/text/Collator;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->collator:Ljava/text/Collator;

    return-object v0
.end method

.method protected getEnum(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/FilteredTermEnum;
    .locals 8
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    new-instance v0, Lorg/apache/lucene/search/TermRangeTermEnum;

    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Ljava/lang/String;

    iget-boolean v5, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    iget-boolean v6, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeUpper:Z

    iget-object v7, p0, Lorg/apache/lucene/search/TermRangeQuery;->collator:Ljava/text/Collator;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/TermRangeTermEnum;-><init>(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/text/Collator;)V

    return-object v0
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getLowerTerm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Ljava/lang/String;

    return-object v0
.end method

.method public getUpperTerm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v5, 0x4d5

    const/16 v4, 0x4cf

    const/4 v3, 0x0

    .line 158
    const/16 v0, 0x1f

    .line 159
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v1

    .line 160
    .local v1, "result":I
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->collator:Ljava/text/Collator;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 161
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->field:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v6, v2

    .line 162
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    if-eqz v2, :cond_2

    move v2, v4

    :goto_2
    add-int v1, v6, v2

    .line 163
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeUpper:Z

    if-eqz v6, :cond_3

    :goto_3
    add-int v1, v2, v4

    .line 164
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 165
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Ljava/lang/String;

    if-nez v4, :cond_5

    :goto_5
    add-int v1, v2, v3

    .line 166
    return v1

    .line 160
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->collator:Ljava/text/Collator;

    invoke-virtual {v2}, Ljava/text/Collator;->hashCode()I

    move-result v2

    goto :goto_0

    .line 161
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->field:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    move v2, v5

    .line 162
    goto :goto_2

    :cond_3
    move v4, v5

    .line 163
    goto :goto_3

    .line 164
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    .line 165
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_5
.end method

.method public includesLower()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    return v0
.end method

.method public includesUpper()Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeUpper:Z

    return v0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 143
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermRangeQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 144
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermRangeQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeLower:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x5b

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 148
    iget-object v1, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string/jumbo v1, "*"

    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "\\*"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    const-string/jumbo v1, " TO "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    iget-object v1, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string/jumbo v1, "*"

    iget-object v2, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string/jumbo v1, "\\*"

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    iget-boolean v1, p0, Lorg/apache/lucene/search/TermRangeQuery;->includeUpper:Z

    if-eqz v1, :cond_6

    const/16 v1, 0x5d

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 152
    invoke-virtual {p0}, Lorg/apache/lucene/search/TermRangeQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 147
    :cond_1
    const/16 v1, 0x7b

    goto :goto_0

    .line 148
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/TermRangeQuery;->lowerTerm:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const-string/jumbo v1, "*"

    goto :goto_1

    .line 150
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/search/TermRangeQuery;->upperTerm:Ljava/lang/String;

    goto :goto_2

    :cond_5
    const-string/jumbo v1, "*"

    goto :goto_2

    .line 151
    :cond_6
    const/16 v1, 0x7d

    goto :goto_3
.end method
