.class Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "NearSpansUnordered.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/spans/NearSpansUnordered;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CellQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/spans/NearSpansUnordered;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/spans/NearSpansUnordered;I)V
    .locals 0
    .param p2, "size"    # I

    .prologue
    .line 55
    iput-object p1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->this$0:Lorg/apache/lucene/search/spans/NearSpansUnordered;

    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;-><init>()V

    .line 56
    invoke-virtual {p0, p2}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->initialize(I)V

    .line 57
    return-void
.end method


# virtual methods
.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 54
    check-cast p1, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->lessThan(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Z

    move-result v0

    return v0
.end method

.method protected final lessThan(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Z
    .locals 2
    .param p1, "spans1"    # Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    .param p2, "spans2"    # Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .prologue
    .line 61
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v0

    invoke-virtual {p2}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 62
    invoke-static {p1, p2}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->docSpansOrdered(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/spans/Spans;)Z

    move-result v0

    .line 64
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v0

    invoke-virtual {p2}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v1

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
