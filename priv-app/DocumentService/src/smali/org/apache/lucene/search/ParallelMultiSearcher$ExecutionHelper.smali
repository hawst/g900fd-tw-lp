.class final Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;
.super Ljava/lang/Object;
.source "ParallelMultiSearcher.java"

# interfaces
.implements Ljava/lang/Iterable;
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/ParallelMultiSearcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ExecutionHelper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;",
        "Ljava/lang/Iterable",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private numTasks:I

.field private final service:Ljava/util/concurrent/CompletionService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CompletionService",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 257
    .local p0, "this":Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 258
    new-instance v0, Ljava/util/concurrent/ExecutorCompletionService;

    invoke-direct {v0, p1}, Ljava/util/concurrent/ExecutorCompletionService;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->service:Ljava/util/concurrent/CompletionService;

    .line 259
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 262
    .local p0, "this":Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper<TT;>;"
    iget v0, p0, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->numTasks:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 290
    .local p0, "this":Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper<TT;>;"
    return-object p0
.end method

.method public next()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 271
    .local p0, "this":Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper<TT;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 272
    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    throw v1

    .line 274
    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->service:Ljava/util/concurrent/CompletionService;

    invoke-interface {v1}, Ljava/util/concurrent/CompletionService;->take()Ljava/util/concurrent/Future;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 280
    iget v2, p0, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->numTasks:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->numTasks:I

    .line 274
    return-object v1

    .line 275
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    new-instance v1, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 280
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    iget v2, p0, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->numTasks:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->numTasks:I

    throw v1

    .line 277
    :catch_1
    move-exception v0

    .line 278
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    :try_start_2
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 285
    .local p0, "this":Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public submit(Ljava/util/concurrent/Callable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 266
    .local p0, "this":Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;, "Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper<TT;>;"
    .local p1, "task":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->service:Ljava/util/concurrent/CompletionService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/CompletionService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    .line 267
    iget v0, p0, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->numTasks:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/ParallelMultiSearcher$ExecutionHelper;->numTasks:I

    .line 268
    return-void
.end method
