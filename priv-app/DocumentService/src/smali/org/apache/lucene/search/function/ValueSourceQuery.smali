.class public Lorg/apache/lucene/search/function/ValueSourceQuery;
.super Lorg/apache/lucene/search/Query;
.source "ValueSourceQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/function/ValueSourceQuery$1;,
        Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;,
        Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;
    }
.end annotation


# instance fields
.field valSrc:Lorg/apache/lucene/search/function/ValueSource;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/function/ValueSource;)V
    .locals 0
    .param p1, "valSrc"    # Lorg/apache/lucene/search/function/ValueSource;

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 50
    iput-object p1, p0, Lorg/apache/lucene/search/function/ValueSourceQuery;->valSrc:Lorg/apache/lucene/search/function/ValueSource;

    .line 51
    return-void
.end method


# virtual methods
.method public createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/Searcher;

    .prologue
    .line 166
    new-instance v0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;-><init>(Lorg/apache/lucene/search/function/ValueSourceQuery;Lorg/apache/lucene/search/Searcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 177
    if-ne p0, p1, :cond_1

    .line 185
    :cond_0
    :goto_0
    return v1

    .line 179
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 180
    goto :goto_0

    .line 181
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 182
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 184
    check-cast v0, Lorg/apache/lucene/search/function/ValueSourceQuery;

    .line 185
    .local v0, "other":Lorg/apache/lucene/search/function/ValueSourceQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/function/ValueSourceQuery;->getBoost()F

    move-result v3

    invoke-virtual {v0}, Lorg/apache/lucene/search/function/ValueSourceQuery;->getBoost()F

    move-result v4

    cmpl-float v3, v3, v4

    if-nez v3, :cond_4

    iget-object v3, p0, Lorg/apache/lucene/search/function/ValueSourceQuery;->valSrc:Lorg/apache/lucene/search/function/ValueSource;

    iget-object v4, v0, Lorg/apache/lucene/search/function/ValueSourceQuery;->valSrc:Lorg/apache/lucene/search/function/ValueSource;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/function/ValueSource;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    return-void
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 192
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/function/ValueSourceQuery;->valSrc:Lorg/apache/lucene/search/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/search/function/ValueSource;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lorg/apache/lucene/search/function/ValueSourceQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 0
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    return-object p0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/lucene/search/function/ValueSourceQuery;->valSrc:Lorg/apache/lucene/search/function/ValueSource;

    invoke-virtual {v1}, Lorg/apache/lucene/search/function/ValueSource;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/function/ValueSourceQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
