.class public Lorg/apache/lucene/search/MultiSearcher;
.super Lorg/apache/lucene/search/Searcher;
.source "MultiSearcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;,
        Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;,
        Lorg/apache/lucene/search/MultiSearcher$CachedDfSource;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private maxDoc:I

.field private searchables:[Lorg/apache/lucene/search/Searchable;

.field private starts:[I


# direct methods
.method public varargs constructor <init>([Lorg/apache/lucene/search/Searchable;)V
    .locals 4
    .param p1, "searchables"    # [Lorg/apache/lucene/search/Searchable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    invoke-direct {p0}, Lorg/apache/lucene/search/Searcher;-><init>()V

    .line 137
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/search/MultiSearcher;->maxDoc:I

    .line 141
    iput-object p1, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    .line 143
    array-length v1, p1

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/lucene/search/MultiSearcher;->starts:[I

    .line 144
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 145
    iget-object v1, p0, Lorg/apache/lucene/search/MultiSearcher;->starts:[I

    iget v2, p0, Lorg/apache/lucene/search/MultiSearcher;->maxDoc:I

    aput v2, v1, v0

    .line 146
    iget v1, p0, Lorg/apache/lucene/search/MultiSearcher;->maxDoc:I

    aget-object v2, p1, v0

    invoke-interface {v2}, Lorg/apache/lucene/search/Searchable;->maxDoc()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/search/MultiSearcher;->maxDoc:I

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/MultiSearcher;->starts:[I

    array-length v2, p1

    iget v3, p0, Lorg/apache/lucene/search/MultiSearcher;->maxDoc:I

    aput v3, v1, v2

    .line 149
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 163
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 164
    iget-object v1, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lorg/apache/lucene/search/Searchable;->close()V

    .line 163
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 165
    :cond_0
    return-void
.end method

.method createDocFrequencyMap(Ljava/util/Set;)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 339
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v10

    new-array v10, v10, [Lorg/apache/lucene/index/Term;

    invoke-interface {p1, v10}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lorg/apache/lucene/index/Term;

    .line 340
    .local v1, "allTermsArray":[Lorg/apache/lucene/index/Term;
    array-length v10, v1

    new-array v0, v10, [I

    .line 341
    .local v0, "aggregatedDfs":[I
    iget-object v2, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    .local v2, "arr$":[Lorg/apache/lucene/search/Searchable;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v8, :cond_1

    aget-object v9, v2, v6

    .line 342
    .local v9, "searchable":Lorg/apache/lucene/search/Searchable;
    invoke-interface {v9, v1}, Lorg/apache/lucene/search/Searchable;->docFreqs([Lorg/apache/lucene/index/Term;)[I

    move-result-object v4

    .line 343
    .local v4, "dfs":[I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_1
    array-length v10, v0

    if-ge v7, v10, :cond_0

    .line 344
    aget v10, v0, v7

    aget v11, v4, v7

    add-int/2addr v10, v11

    aput v10, v0, v7

    .line 343
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 341
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 347
    .end local v4    # "dfs":[I
    .end local v7    # "j":I
    .end local v9    # "searchable":Lorg/apache/lucene/search/Searchable;
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 348
    .local v3, "dfMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    array-length v10, v1

    if-ge v5, v10, :cond_2

    .line 349
    aget-object v10, v1, v5

    aget v11, v0, v5

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v3, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 351
    :cond_2
    return-object v3
.end method

.method public createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;
    .locals 6
    .param p1, "original"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 315
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/MultiSearcher;->rewrite(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    .line 318
    .local v3, "rewrittenQuery":Lorg/apache/lucene/search/Query;
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 319
    .local v4, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Query;->extractTerms(Ljava/util/Set;)V

    .line 322
    invoke-virtual {p0, v4}, Lorg/apache/lucene/search/MultiSearcher;->createDocFrequencyMap(Ljava/util/Set;)Ljava/util/Map;

    move-result-object v1

    .line 325
    .local v1, "dfMap":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiSearcher;->maxDoc()I

    move-result v2

    .line 326
    .local v2, "numDocs":I
    new-instance v0, Lorg/apache/lucene/search/MultiSearcher$CachedDfSource;

    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiSearcher;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v5

    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/search/MultiSearcher$CachedDfSource;-><init>(Ljava/util/Map;ILorg/apache/lucene/search/Similarity;)V

    .line 328
    .local v0, "cacheSim":Lorg/apache/lucene/search/MultiSearcher$CachedDfSource;
    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/MultiSearcher$CachedDfSource;->createNormalizedWeight(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Weight;

    move-result-object v5

    return-object v5
.end method

.method public doc(I)Lorg/apache/lucene/document/Document;
    .locals 3
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/MultiSearcher;->subSearcher(I)I

    move-result v0

    .line 179
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/search/MultiSearcher;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-interface {v1, v2}, Lorg/apache/lucene/search/Searchable;->doc(I)Lorg/apache/lucene/document/Document;

    move-result-object v1

    return-object v1
.end method

.method public doc(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;
    .locals 3
    .param p1, "n"    # I
    .param p2, "fieldSelector"    # Lorg/apache/lucene/document/FieldSelector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/MultiSearcher;->subSearcher(I)I

    move-result v0

    .line 186
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/search/MultiSearcher;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-interface {v1, v2, p2}, Lorg/apache/lucene/search/Searchable;->doc(ILorg/apache/lucene/document/FieldSelector;)Lorg/apache/lucene/document/Document;

    move-result-object v1

    return-object v1
.end method

.method public docFreq(Lorg/apache/lucene/index/Term;)I
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    const/4 v0, 0x0

    .line 170
    .local v0, "docFreq":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 171
    iget-object v2, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    aget-object v2, v2, v1

    invoke-interface {v2, p1}, Lorg/apache/lucene/search/Searchable;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v2

    add-int/2addr v0, v2

    .line 170
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 172
    :cond_0
    return v0
.end method

.method public explain(Lorg/apache/lucene/search/Weight;I)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 293
    invoke-virtual {p0, p2}, Lorg/apache/lucene/search/MultiSearcher;->subSearcher(I)I

    move-result v0

    .line 294
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/search/MultiSearcher;->starts:[I

    aget v2, v2, v0

    sub-int v2, p2, v2

    invoke-interface {v1, p1, v2}, Lorg/apache/lucene/search/Searchable;->explain(Lorg/apache/lucene/search/Weight;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v1

    return-object v1
.end method

.method public getSearchables()[Lorg/apache/lucene/search/Searchable;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    return-object v0
.end method

.method protected getStarts()[I
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/lucene/search/MultiSearcher;->starts:[I

    return-object v0
.end method

.method public maxDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 203
    iget v0, p0, Lorg/apache/lucene/search/MultiSearcher;->maxDoc:I

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "original"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 284
    iget-object v2, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    array-length v2, v2

    new-array v1, v2, [Lorg/apache/lucene/search/Query;

    .line 285
    .local v1, "queries":[Lorg/apache/lucene/search/Query;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 286
    iget-object v2, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Lorg/apache/lucene/search/Searchable;->rewrite(Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    aput-object v2, v1, v0

    .line 285
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 288
    :cond_0
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v2, v1}, Lorg/apache/lucene/search/Query;->combine([Lorg/apache/lucene/search/Query;)Lorg/apache/lucene/search/Query;

    move-result-object v2

    return-object v2
.end method

.method public search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;
    .locals 14
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "nDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 210
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiSearcher;->maxDoc()I

    move-result v1

    move/from16 v0, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result p3

    .line 211
    new-instance v7, Lorg/apache/lucene/search/HitQueue;

    const/4 v1, 0x0

    move/from16 v0, p3

    invoke-direct {v7, v0, v1}, Lorg/apache/lucene/search/HitQueue;-><init>(IZ)V

    .line 212
    .local v7, "hq":Lorg/apache/lucene/search/HitQueue;
    const/4 v13, 0x0

    .line 214
    .local v13, "totalHits":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    array-length v1, v1

    if-ge v8, v1, :cond_0

    .line 215
    new-instance v1, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;

    sget-object v2, Lorg/apache/lucene/util/DummyConcurrentLock;->INSTANCE:Lorg/apache/lucene/util/DummyConcurrentLock;

    iget-object v3, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    aget-object v3, v3, v8

    iget-object v9, p0, Lorg/apache/lucene/search/MultiSearcher;->starts:[I

    move-object v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    invoke-direct/range {v1 .. v9}, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;-><init>(Ljava/util/concurrent/locks/Lock;Lorg/apache/lucene/search/Searchable;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/HitQueue;I[I)V

    invoke-virtual {v1}, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->call()Lorg/apache/lucene/search/TopDocs;

    move-result-object v10

    .line 217
    .local v10, "docs":Lorg/apache/lucene/search/TopDocs;
    iget v1, v10, Lorg/apache/lucene/search/TopDocs;->totalHits:I

    add-int/2addr v13, v1

    .line 214
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 220
    .end local v10    # "docs":Lorg/apache/lucene/search/TopDocs;
    :cond_0
    invoke-virtual {v7}, Lorg/apache/lucene/search/HitQueue;->size()I

    move-result v1

    new-array v12, v1, [Lorg/apache/lucene/search/ScoreDoc;

    .line 221
    .local v12, "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    invoke-virtual {v7}, Lorg/apache/lucene/search/HitQueue;->size()I

    move-result v1

    add-int/lit8 v8, v1, -0x1

    :goto_1
    if-ltz v8, :cond_1

    .line 222
    invoke-virtual {v7}, Lorg/apache/lucene/search/HitQueue;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/ScoreDoc;

    aput-object v1, v12, v8

    .line 221
    add-int/lit8 v8, v8, -0x1

    goto :goto_1

    .line 224
    :cond_1
    if-nez v13, :cond_2

    const/high16 v11, -0x800000    # Float.NEGATIVE_INFINITY

    .line 226
    .local v11, "maxScore":F
    :goto_2
    new-instance v1, Lorg/apache/lucene/search/TopDocs;

    invoke-direct {v1, v13, v12, v11}, Lorg/apache/lucene/search/TopDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;F)V

    return-object v1

    .line 224
    .end local v11    # "maxScore":F
    :cond_2
    const/4 v1, 0x0

    aget-object v1, v12, v1

    iget v11, v1, Lorg/apache/lucene/search/ScoreDoc;->score:F

    goto :goto_2
.end method

.method public search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;
    .locals 15
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "n"    # I
    .param p4, "sort"    # Lorg/apache/lucene/search/Sort;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 231
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiSearcher;->maxDoc()I

    move-result v1

    move/from16 v0, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result p3

    .line 232
    new-instance v7, Lorg/apache/lucene/search/FieldDocSortedHitQueue;

    move/from16 v0, p3

    invoke-direct {v7, v0}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;-><init>(I)V

    .line 233
    .local v7, "hq":Lorg/apache/lucene/search/FieldDocSortedHitQueue;
    const/4 v14, 0x0

    .line 235
    .local v14, "totalHits":I
    const/high16 v12, -0x800000    # Float.NEGATIVE_INFINITY

    .line 237
    .local v12, "maxScore":F
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    array-length v1, v1

    if-ge v9, v1, :cond_0

    .line 238
    new-instance v1, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;

    sget-object v2, Lorg/apache/lucene/util/DummyConcurrentLock;->INSTANCE:Lorg/apache/lucene/util/DummyConcurrentLock;

    iget-object v3, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    aget-object v3, v3, v9

    iget-object v10, p0, Lorg/apache/lucene/search/MultiSearcher;->starts:[I

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    move-object/from16 v8, p4

    invoke-direct/range {v1 .. v10}, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;-><init>(Ljava/util/concurrent/locks/Lock;Lorg/apache/lucene/search/Searchable;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/FieldDocSortedHitQueue;Lorg/apache/lucene/search/Sort;I[I)V

    invoke-virtual {v1}, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->call()Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v11

    .line 240
    .local v11, "docs":Lorg/apache/lucene/search/TopFieldDocs;
    iget v1, v11, Lorg/apache/lucene/search/TopFieldDocs;->totalHits:I

    add-int/2addr v14, v1

    .line 241
    invoke-virtual {v11}, Lorg/apache/lucene/search/TopFieldDocs;->getMaxScore()F

    move-result v1

    invoke-static {v12, v1}, Ljava/lang/Math;->max(FF)F

    move-result v12

    .line 237
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 244
    .end local v11    # "docs":Lorg/apache/lucene/search/TopFieldDocs;
    :cond_0
    invoke-virtual {v7}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->size()I

    move-result v1

    new-array v13, v1, [Lorg/apache/lucene/search/ScoreDoc;

    .line 245
    .local v13, "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    invoke-virtual {v7}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->size()I

    move-result v1

    add-int/lit8 v9, v1, -0x1

    :goto_1
    if-ltz v9, :cond_1

    .line 246
    invoke-virtual {v7}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/ScoreDoc;

    aput-object v1, v13, v9

    .line 245
    add-int/lit8 v9, v9, -0x1

    goto :goto_1

    .line 248
    :cond_1
    new-instance v1, Lorg/apache/lucene/search/TopFieldDocs;

    invoke-virtual {v7}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->getFields()[Lorg/apache/lucene/search/SortField;

    move-result-object v2

    invoke-direct {v1, v14, v13, v2, v12}, Lorg/apache/lucene/search/TopFieldDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;[Lorg/apache/lucene/search/SortField;F)V

    return-object v1
.end method

.method public search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V
    .locals 4
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p3, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 255
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 257
    iget-object v3, p0, Lorg/apache/lucene/search/MultiSearcher;->starts:[I

    aget v2, v3, v1

    .line 259
    .local v2, "start":I
    new-instance v0, Lorg/apache/lucene/search/MultiSearcher$1;

    invoke-direct {v0, p0, p3, v2}, Lorg/apache/lucene/search/MultiSearcher$1;-><init>(Lorg/apache/lucene/search/MultiSearcher;Lorg/apache/lucene/search/Collector;I)V

    .line 278
    .local v0, "hc":Lorg/apache/lucene/search/Collector;
    iget-object v3, p0, Lorg/apache/lucene/search/MultiSearcher;->searchables:[Lorg/apache/lucene/search/Searchable;

    aget-object v3, v3, v1

    invoke-interface {v3, p1, p2, v0}, Lorg/apache/lucene/search/Searchable;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V

    .line 255
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 280
    .end local v0    # "hc":Lorg/apache/lucene/search/Collector;
    .end local v2    # "start":I
    :cond_0
    return-void
.end method

.method public subDoc(I)I
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 198
    iget-object v0, p0, Lorg/apache/lucene/search/MultiSearcher;->starts:[I

    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/MultiSearcher;->subSearcher(I)I

    move-result v1

    aget v0, v0, v1

    sub-int v0, p1, v0

    return v0
.end method

.method public subSearcher(I)I
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 192
    iget-object v0, p0, Lorg/apache/lucene/search/MultiSearcher;->starts:[I

    invoke-static {p1, v0}, Lorg/apache/lucene/util/ReaderUtil;->subIndex(I[I)I

    move-result v0

    return v0
.end method
