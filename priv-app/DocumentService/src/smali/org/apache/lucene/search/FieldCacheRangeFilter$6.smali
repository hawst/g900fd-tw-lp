.class Lorg/apache/lucene/search/FieldCacheRangeFilter$6;
.super Lorg/apache/lucene/search/FieldCacheRangeFilter;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter;->newFloatRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;Ljava/lang/Float;Ljava/lang/Float;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldCacheRangeFilter",
        "<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Float;Ljava/lang/Float;ZZ)V
    .locals 8
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "x2"    # Ljava/lang/Float;
    .param p4, "x3"    # Ljava/lang/Float;
    .param p5, "x4"    # Z
    .param p6, "x5"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 348
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZLorg/apache/lucene/search/FieldCacheRangeFilter$1;)V

    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 8
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 352
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->lowerVal:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 353
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->lowerVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 354
    .local v6, "f":F
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->includeUpper:Z

    if-nez v0, :cond_0

    cmpl-float v0, v6, v1

    if-lez v0, :cond_0

    invoke-static {v6}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 375
    .end local v6    # "f":F
    :goto_0
    return-object v0

    .line 356
    .restart local v6    # "f":F
    :cond_0
    invoke-static {v6}, Lorg/apache/lucene/util/NumericUtils;->floatToSortableInt(F)I

    move-result v7

    .line 357
    .local v7, "i":I
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->includeLower:Z

    if-eqz v0, :cond_1

    .end local v7    # "i":I
    :goto_1
    invoke-static {v7}, Lorg/apache/lucene/util/NumericUtils;->sortableIntToFloat(I)F

    move-result v4

    .line 361
    .end local v6    # "f":F
    .local v4, "inclusiveLowerPoint":F
    :goto_2
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->upperVal:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 362
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->upperVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 363
    .restart local v6    # "f":F
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->includeUpper:Z

    if-nez v0, :cond_3

    cmpg-float v0, v6, v1

    if-gez v0, :cond_3

    invoke-static {v6}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 364
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 357
    .end local v4    # "inclusiveLowerPoint":F
    .restart local v7    # "i":I
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 359
    .end local v6    # "f":F
    .end local v7    # "i":I
    :cond_2
    const/high16 v4, -0x800000    # Float.NEGATIVE_INFINITY

    .restart local v4    # "inclusiveLowerPoint":F
    goto :goto_2

    .line 365
    .restart local v6    # "f":F
    :cond_3
    invoke-static {v6}, Lorg/apache/lucene/util/NumericUtils;->floatToSortableInt(F)I

    move-result v7

    .line 366
    .restart local v7    # "i":I
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->includeUpper:Z

    if-eqz v0, :cond_4

    .end local v7    # "i":I
    :goto_3
    invoke-static {v7}, Lorg/apache/lucene/util/NumericUtils;->sortableIntToFloat(I)F

    move-result v5

    .line 371
    .end local v6    # "f":F
    .local v5, "inclusiveUpperPoint":F
    :goto_4
    cmpl-float v0, v4, v5

    if-lez v0, :cond_6

    .line 372
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 366
    .end local v5    # "inclusiveUpperPoint":F
    .restart local v6    # "f":F
    .restart local v7    # "i":I
    :cond_4
    add-int/lit8 v7, v7, -0x1

    goto :goto_3

    .line 368
    .end local v6    # "f":F
    .end local v7    # "i":I
    :cond_5
    const/high16 v5, 0x7f800000    # Float.POSITIVE_INFINITY

    .restart local v5    # "inclusiveUpperPoint":F
    goto :goto_4

    .line 374
    :cond_6
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->field:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    check-cast v0, Lorg/apache/lucene/search/FieldCache$FloatParser;

    invoke-interface {v1, p1, v2, v0}, Lorg/apache/lucene/search/FieldCache;->getFloats(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$FloatParser;)[F

    move-result-object v3

    .line 375
    .local v3, "values":[F
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$6$1;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/FieldCacheRangeFilter$6$1;-><init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$6;Lorg/apache/lucene/index/IndexReader;[FFF)V

    goto :goto_0
.end method
