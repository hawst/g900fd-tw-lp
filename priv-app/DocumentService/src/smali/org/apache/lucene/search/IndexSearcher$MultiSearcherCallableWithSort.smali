.class final Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;
.super Ljava/lang/Object;
.source "IndexSearcher.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/IndexSearcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MultiSearcherCallableWithSort"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort$FakeScorer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lorg/apache/lucene/search/TopFieldDocs;",
        ">;"
    }
.end annotation


# instance fields
.field private final fakeScorer:Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort$FakeScorer;

.field private final filter:Lorg/apache/lucene/search/Filter;

.field private final hq:Lorg/apache/lucene/search/TopFieldCollector;

.field private final lock:Ljava/util/concurrent/locks/Lock;

.field private final nDocs:I

.field private final searchable:Lorg/apache/lucene/search/IndexSearcher;

.field private final sort:Lorg/apache/lucene/search/Sort;

.field private final weight:Lorg/apache/lucene/search/Weight;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/locks/Lock;Lorg/apache/lucene/search/IndexSearcher;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/TopFieldCollector;Lorg/apache/lucene/search/Sort;)V
    .locals 1
    .param p1, "lock"    # Ljava/util/concurrent/locks/Lock;
    .param p2, "searchable"    # Lorg/apache/lucene/search/IndexSearcher;
    .param p3, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p4, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p5, "nDocs"    # I
    .param p6, "hq"    # Lorg/apache/lucene/search/TopFieldCollector;
    .param p7, "sort"    # Lorg/apache/lucene/search/Sort;

    .prologue
    .line 732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 776
    new-instance v0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort$FakeScorer;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort$FakeScorer;-><init>(Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;)V

    iput-object v0, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->fakeScorer:Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort$FakeScorer;

    .line 733
    iput-object p1, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    .line 734
    iput-object p2, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->searchable:Lorg/apache/lucene/search/IndexSearcher;

    .line 735
    iput-object p3, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->weight:Lorg/apache/lucene/search/Weight;

    .line 736
    iput-object p4, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->filter:Lorg/apache/lucene/search/Filter;

    .line 737
    iput p5, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->nDocs:I

    .line 738
    iput-object p6, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->hq:Lorg/apache/lucene/search/TopFieldCollector;

    .line 739
    iput-object p7, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->sort:Lorg/apache/lucene/search/Sort;

    .line 740
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 721
    invoke-virtual {p0}, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->call()Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method public call()Lorg/apache/lucene/search/TopFieldDocs;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 779
    iget-object v9, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->searchable:Lorg/apache/lucene/search/IndexSearcher;

    iget-object v10, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->weight:Lorg/apache/lucene/search/Weight;

    iget-object v11, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->filter:Lorg/apache/lucene/search/Filter;

    iget v12, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->nDocs:I

    iget-object v13, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->sort:Lorg/apache/lucene/search/Sort;

    invoke-virtual {v9, v10, v11, v12, v13}, Lorg/apache/lucene/search/IndexSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v2

    .line 783
    .local v2, "docs":Lorg/apache/lucene/search/TopFieldDocs;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_0
    iget-object v9, v2, Lorg/apache/lucene/search/TopFieldDocs;->fields:[Lorg/apache/lucene/search/SortField;

    array-length v9, v9

    if-ge v5, v9, :cond_1

    .line 784
    iget-object v9, v2, Lorg/apache/lucene/search/TopFieldDocs;->fields:[Lorg/apache/lucene/search/SortField;

    aget-object v9, v9, v5

    invoke-virtual {v9}, Lorg/apache/lucene/search/SortField;->getType()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_0

    .line 786
    const/4 v6, 0x0

    .local v6, "j2":I
    :goto_1
    iget-object v9, v2, Lorg/apache/lucene/search/TopFieldDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    array-length v9, v9

    if-ge v6, v9, :cond_1

    .line 787
    iget-object v9, v2, Lorg/apache/lucene/search/TopFieldDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    aget-object v3, v9, v6

    check-cast v3, Lorg/apache/lucene/search/FieldDoc;

    .line 788
    .local v3, "fd":Lorg/apache/lucene/search/FieldDoc;
    iget-object v10, v3, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    iget-object v9, v3, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    aget-object v9, v9, v5

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v10, v5

    .line 786
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 783
    .end local v3    # "fd":Lorg/apache/lucene/search/FieldDoc;
    .end local v6    # "j2":I
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 794
    :cond_1
    iget-object v9, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v9}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 796
    :try_start_0
    iget-object v9, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->hq:Lorg/apache/lucene/search/TopFieldCollector;

    iget-object v10, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->searchable:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v10}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v10

    iget-object v11, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->searchable:Lorg/apache/lucene/search/IndexSearcher;

    # getter for: Lorg/apache/lucene/search/IndexSearcher;->docBase:I
    invoke-static {v11}, Lorg/apache/lucene/search/IndexSearcher;->access$000(Lorg/apache/lucene/search/IndexSearcher;)I

    move-result v11

    invoke-virtual {v9, v10, v11}, Lorg/apache/lucene/search/TopFieldCollector;->setNextReader(Lorg/apache/lucene/index/IndexReader;I)V

    .line 797
    iget-object v9, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->hq:Lorg/apache/lucene/search/TopFieldCollector;

    iget-object v10, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->fakeScorer:Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort$FakeScorer;

    invoke-virtual {v9, v10}, Lorg/apache/lucene/search/TopFieldCollector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 798
    iget-object v0, v2, Lorg/apache/lucene/search/TopFieldDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .local v0, "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_2
    if-ge v4, v7, :cond_2

    aget-object v8, v0, v4

    .line 799
    .local v8, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    iget v9, v8, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    iget-object v10, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->searchable:Lorg/apache/lucene/search/IndexSearcher;

    # getter for: Lorg/apache/lucene/search/IndexSearcher;->docBase:I
    invoke-static {v10}, Lorg/apache/lucene/search/IndexSearcher;->access$000(Lorg/apache/lucene/search/IndexSearcher;)I

    move-result v10

    sub-int v1, v9, v10

    .line 800
    .local v1, "docID":I
    iget-object v9, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->fakeScorer:Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort$FakeScorer;

    iput v1, v9, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort$FakeScorer;->doc:I

    .line 801
    iget-object v9, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->fakeScorer:Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort$FakeScorer;

    iget v10, v8, Lorg/apache/lucene/search/ScoreDoc;->score:F

    iput v10, v9, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort$FakeScorer;->score:F

    .line 802
    iget-object v9, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->hq:Lorg/apache/lucene/search/TopFieldCollector;

    invoke-virtual {v9, v1}, Lorg/apache/lucene/search/TopFieldCollector;->collect(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 805
    .end local v0    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .end local v1    # "docID":I
    .end local v4    # "i$":I
    .end local v7    # "len$":I
    .end local v8    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :catchall_0
    move-exception v9

    iget-object v10, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v10}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v9

    .restart local v0    # "arr$":[Lorg/apache/lucene/search/ScoreDoc;
    .restart local v4    # "i$":I
    .restart local v7    # "len$":I
    :cond_2
    iget-object v9, p0, Lorg/apache/lucene/search/IndexSearcher$MultiSearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v9}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 808
    return-object v2
.end method
