.class public Lorg/apache/lucene/search/DefaultSimilarity;
.super Lorg/apache/lucene/search/Similarity;
.source "DefaultSimilarity.java"


# instance fields
.field protected discountOverlaps:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lorg/apache/lucene/search/Similarity;-><init>()V

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/search/DefaultSimilarity;->discountOverlaps:Z

    return-void
.end method


# virtual methods
.method public computeNorm(Ljava/lang/String;Lorg/apache/lucene/index/FieldInvertState;)F
    .locals 6
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "state"    # Lorg/apache/lucene/index/FieldInvertState;

    .prologue
    .line 36
    iget-boolean v1, p0, Lorg/apache/lucene/search/DefaultSimilarity;->discountOverlaps:Z

    if-eqz v1, :cond_0

    .line 37
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInvertState;->getLength()I

    move-result v1

    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInvertState;->getNumOverlap()I

    move-result v2

    sub-int v0, v1, v2

    .line 40
    .local v0, "numTerms":I
    :goto_0
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInvertState;->getBoost()F

    move-result v1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    int-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-float v2, v2

    mul-float/2addr v1, v2

    return v1

    .line 39
    .end local v0    # "numTerms":I
    :cond_0
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInvertState;->getLength()I

    move-result v0

    .restart local v0    # "numTerms":I
    goto :goto_0
.end method

.method public coord(II)F
    .locals 2
    .param p1, "overlap"    # I
    .param p2, "maxOverlap"    # I

    .prologue
    .line 70
    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    return v0
.end method

.method public getDiscountOverlaps()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lorg/apache/lucene/search/DefaultSimilarity;->discountOverlaps:Z

    return v0
.end method

.method public idf(II)F
    .locals 4
    .param p1, "docFreq"    # I
    .param p2, "numDocs"    # I

    .prologue
    .line 64
    int-to-double v0, p2

    add-int/lit8 v2, p1, 0x1

    int-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method public queryNorm(F)F
    .locals 4
    .param p1, "sumOfSquaredWeights"    # F

    .prologue
    .line 46
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method public setDiscountOverlaps(Z)V
    .locals 0
    .param p1, "v"    # Z

    .prologue
    .line 86
    iput-boolean p1, p0, Lorg/apache/lucene/search/DefaultSimilarity;->discountOverlaps:Z

    .line 87
    return-void
.end method

.method public sloppyFreq(I)F
    .locals 2
    .param p1, "distance"    # I

    .prologue
    .line 58
    const/high16 v0, 0x3f800000    # 1.0f

    add-int/lit8 v1, p1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public tf(F)F
    .locals 2
    .param p1, "freq"    # F

    .prologue
    .line 52
    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method
