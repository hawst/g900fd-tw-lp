.class public Lorg/apache/lucene/search/CachingSpanFilter;
.super Lorg/apache/lucene/search/SpanFilter;
.source "CachingSpanFilter.java"


# instance fields
.field private final cache:Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache",
            "<",
            "Lorg/apache/lucene/search/SpanFilterResult;",
            ">;"
        }
    .end annotation
.end field

.field private filter:Lorg/apache/lucene/search/SpanFilter;

.field hitCount:I

.field missCount:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/SpanFilter;)V
    .locals 1
    .param p1, "filter"    # Lorg/apache/lucene/search/SpanFilter;

    .prologue
    .line 41
    sget-object v0, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->RECACHE:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/CachingSpanFilter;-><init>(Lorg/apache/lucene/search/SpanFilter;Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/SpanFilter;Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;)V
    .locals 2
    .param p1, "filter"    # Lorg/apache/lucene/search/SpanFilter;
    .param p2, "deletesMode"    # Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/apache/lucene/search/SpanFilter;-><init>()V

    .line 49
    iput-object p1, p0, Lorg/apache/lucene/search/CachingSpanFilter;->filter:Lorg/apache/lucene/search/SpanFilter;

    .line 50
    sget-object v0, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->DYNAMIC:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    if-ne p2, v0, :cond_0

    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "DeletesMode.DYNAMIC is not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/CachingSpanFilter$1;

    invoke-direct {v0, p0, p2}, Lorg/apache/lucene/search/CachingSpanFilter$1;-><init>(Lorg/apache/lucene/search/CachingSpanFilter;Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;)V

    iput-object v0, p0, Lorg/apache/lucene/search/CachingSpanFilter;->cache:Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;

    .line 59
    return-void
.end method

.method private getCachedResult(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/SpanFilterResult;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v0

    .line 73
    .local v0, "coreKey":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->getDeletesCacheKey()Ljava/lang/Object;

    move-result-object v1

    .line 75
    .local v1, "delCoreKey":Ljava/lang/Object;
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/search/CachingSpanFilter;->cache:Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;

    invoke-virtual {v4, p1, v0, v1}, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->get(Lorg/apache/lucene/index/IndexReader;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/SpanFilterResult;

    .line 76
    .local v2, "result":Lorg/apache/lucene/search/SpanFilterResult;
    if-eqz v2, :cond_1

    .line 77
    iget v4, p0, Lorg/apache/lucene/search/CachingSpanFilter;->hitCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/search/CachingSpanFilter;->hitCount:I

    move-object v3, v2

    .line 85
    .end local v2    # "result":Lorg/apache/lucene/search/SpanFilterResult;
    .local v3, "result":Lorg/apache/lucene/search/SpanFilterResult;
    :goto_1
    return-object v3

    .end local v1    # "delCoreKey":Ljava/lang/Object;
    .end local v3    # "result":Lorg/apache/lucene/search/SpanFilterResult;
    :cond_0
    move-object v1, v0

    .line 73
    goto :goto_0

    .line 81
    .restart local v1    # "delCoreKey":Ljava/lang/Object;
    .restart local v2    # "result":Lorg/apache/lucene/search/SpanFilterResult;
    :cond_1
    iget v4, p0, Lorg/apache/lucene/search/CachingSpanFilter;->missCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/search/CachingSpanFilter;->missCount:I

    .line 82
    iget-object v4, p0, Lorg/apache/lucene/search/CachingSpanFilter;->filter:Lorg/apache/lucene/search/SpanFilter;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/SpanFilter;->bitSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/SpanFilterResult;

    move-result-object v2

    .line 84
    iget-object v4, p0, Lorg/apache/lucene/search/CachingSpanFilter;->cache:Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;

    invoke-virtual {v4, v0, v1, v2}, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;->put(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v3, v2

    .line 85
    .end local v2    # "result":Lorg/apache/lucene/search/SpanFilterResult;
    .restart local v3    # "result":Lorg/apache/lucene/search/SpanFilterResult;
    goto :goto_1
.end method


# virtual methods
.method public bitSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/SpanFilterResult;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/CachingSpanFilter;->getCachedResult(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/SpanFilterResult;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 101
    instance-of v0, p1, Lorg/apache/lucene/search/CachingSpanFilter;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 102
    .end local p1    # "o":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/CachingSpanFilter;->filter:Lorg/apache/lucene/search/SpanFilter;

    check-cast p1, Lorg/apache/lucene/search/CachingSpanFilter;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lorg/apache/lucene/search/CachingSpanFilter;->filter:Lorg/apache/lucene/search/SpanFilter;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/CachingSpanFilter;->getCachedResult(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/SpanFilterResult;

    move-result-object v0

    .line 64
    .local v0, "result":Lorg/apache/lucene/search/SpanFilterResult;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/lucene/search/SpanFilterResult;->getDocIdSet()Lorg/apache/lucene/search/DocIdSet;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/lucene/search/CachingSpanFilter;->filter:Lorg/apache/lucene/search/SpanFilter;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0x1117bf25

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "CachingSpanFilter("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/CachingSpanFilter;->filter:Lorg/apache/lucene/search/SpanFilter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
