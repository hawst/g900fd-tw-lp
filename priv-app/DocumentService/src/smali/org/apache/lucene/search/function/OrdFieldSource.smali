.class public Lorg/apache/lucene/search/function/OrdFieldSource;
.super Lorg/apache/lucene/search/function/ValueSource;
.source "OrdFieldSource.java"


# static fields
.field private static final hcode:I


# instance fields
.field protected field:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    const-class v0, Lorg/apache/lucene/search/function/OrdFieldSource;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    sput v0, Lorg/apache/lucene/search/function/OrdFieldSource;->hcode:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-direct {p0}, Lorg/apache/lucene/search/function/ValueSource;-><init>()V

    .line 60
    iput-object p1, p0, Lorg/apache/lucene/search/function/OrdFieldSource;->field:Ljava/lang/String;

    .line 61
    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ord("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/function/OrdFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 101
    if-ne p1, p0, :cond_1

    const/4 v1, 0x1

    .line 105
    :cond_0
    :goto_0
    return v1

    .line 102
    :cond_1
    if-eqz p1, :cond_0

    .line 103
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lorg/apache/lucene/search/function/OrdFieldSource;

    if-ne v2, v3, :cond_0

    move-object v0, p1

    .line 104
    check-cast v0, Lorg/apache/lucene/search/function/OrdFieldSource;

    .line 105
    .local v0, "other":Lorg/apache/lucene/search/function/OrdFieldSource;
    iget-object v1, p0, Lorg/apache/lucene/search/function/OrdFieldSource;->field:Ljava/lang/String;

    iget-object v2, v0, Lorg/apache/lucene/search/function/OrdFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getValues(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/function/DocValues;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v2, p0, Lorg/apache/lucene/search/function/OrdFieldSource;->field:Ljava/lang/String;

    invoke-interface {v1, p1, v2}, Lorg/apache/lucene/search/FieldCache;->getStringIndex(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/search/FieldCache$StringIndex;

    move-result-object v1

    iget-object v0, v1, Lorg/apache/lucene/search/FieldCache$StringIndex;->order:[I

    .line 73
    .local v0, "arr":[I
    new-instance v1, Lorg/apache/lucene/search/function/OrdFieldSource$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/search/function/OrdFieldSource$1;-><init>(Lorg/apache/lucene/search/function/OrdFieldSource;[I)V

    return-object v1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 113
    sget v0, Lorg/apache/lucene/search/function/OrdFieldSource;->hcode:I

    iget-object v1, p0, Lorg/apache/lucene/search/function/OrdFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
