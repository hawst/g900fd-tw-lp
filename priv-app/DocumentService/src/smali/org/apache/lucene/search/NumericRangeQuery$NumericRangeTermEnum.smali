.class final Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;
.super Lorg/apache/lucene/search/FilteredTermEnum;
.source "NumericRangeQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/NumericRangeQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "NumericRangeTermEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private currentUpperBound:Ljava/lang/String;

.field private final rangeBounds:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final reader:Lorg/apache/lucene/index/IndexReader;

.field private final termTemplate:Lorg/apache/lucene/index/Term;

.field final synthetic this$0:Lorg/apache/lucene/search/NumericRangeQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 411
    const-class v0, Lorg/apache/lucene/search/NumericRangeQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/NumericRangeQuery;Lorg/apache/lucene/index/IndexReader;)V
    .locals 12
    .param p2, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>.NumericRangeTermEnum;"
    const-wide v4, 0x7fffffffffffffffL

    const-wide/16 v10, 0x1

    const-wide/high16 v0, -0x8000000000000000L

    const v7, 0x7fffffff

    const/high16 v6, -0x80000000

    .line 418
    iput-object p1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->this$0:Lorg/apache/lucene/search/NumericRangeQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/FilteredTermEnum;-><init>()V

    .line 414
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    iput-object v8, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->rangeBounds:Ljava/util/LinkedList;

    .line 415
    new-instance v8, Lorg/apache/lucene/index/Term;

    iget-object v9, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->this$0:Lorg/apache/lucene/search/NumericRangeQuery;

    iget-object v9, v9, Lorg/apache/lucene/search/NumericRangeQuery;->field:Ljava/lang/String;

    invoke-direct {v8, v9}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    iput-object v8, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->termTemplate:Lorg/apache/lucene/index/Term;

    .line 416
    const/4 v8, 0x0

    iput-object v8, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->currentUpperBound:Ljava/lang/String;

    .line 419
    iput-object p2, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->reader:Lorg/apache/lucene/index/IndexReader;

    .line 421
    sget-object v8, Lorg/apache/lucene/search/NumericRangeQuery$1;->$SwitchMap$org$apache$lucene$document$NumericField$DataType:[I

    iget-object v9, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/NumericField$DataType;

    invoke-virtual {v9}, Lorg/apache/lucene/document/NumericField$DataType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 504
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid numeric DataType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 426
    :pswitch_0
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/NumericField$DataType;

    sget-object v7, Lorg/apache/lucene/document/NumericField$DataType;->LONG:Lorg/apache/lucene/document/NumericField$DataType;

    if-ne v6, v7, :cond_2

    .line 427
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-nez v6, :cond_1

    move-wide v2, v0

    .line 433
    .local v2, "minBound":J
    :goto_0
    iget-boolean v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->minInclusive:Z

    if-nez v6, :cond_6

    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-eqz v6, :cond_6

    .line 434
    cmp-long v6, v2, v4

    if-nez v6, :cond_5

    .line 508
    .end local v2    # "minBound":J
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->next()Z

    .line 509
    return-void

    .line 427
    :cond_1
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    goto :goto_0

    .line 429
    :cond_2
    sget-boolean v6, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->$assertionsDisabled:Z

    if-nez v6, :cond_3

    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/NumericField$DataType;

    sget-object v7, Lorg/apache/lucene/document/NumericField$DataType;->DOUBLE:Lorg/apache/lucene/document/NumericField$DataType;

    if-eq v6, v7, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 430
    :cond_3
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-nez v6, :cond_4

    sget-wide v2, Lorg/apache/lucene/search/NumericRangeQuery;->LONG_NEGATIVE_INFINITY:J

    .restart local v2    # "minBound":J
    :goto_2
    goto :goto_0

    .end local v2    # "minBound":J
    :cond_4
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v6

    invoke-static {v6, v7}, Lorg/apache/lucene/util/NumericUtils;->doubleToSortableLong(D)J

    move-result-wide v2

    goto :goto_2

    .line 435
    .restart local v2    # "minBound":J
    :cond_5
    add-long/2addr v2, v10

    .line 440
    :cond_6
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/NumericField$DataType;

    sget-object v7, Lorg/apache/lucene/document/NumericField$DataType;->LONG:Lorg/apache/lucene/document/NumericField$DataType;

    if-ne v6, v7, :cond_9

    .line 441
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-nez v6, :cond_8

    .line 447
    .local v4, "maxBound":J
    :goto_3
    iget-boolean v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->maxInclusive:Z

    if-nez v6, :cond_7

    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-eqz v6, :cond_7

    .line 448
    cmp-long v0, v4, v0

    if-eqz v0, :cond_0

    .line 449
    sub-long/2addr v4, v10

    .line 452
    :cond_7
    new-instance v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum$1;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum$1;-><init>(Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;Lorg/apache/lucene/search/NumericRangeQuery;)V

    iget v1, p1, Lorg/apache/lucene/search/NumericRangeQuery;->precisionStep:I

    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/util/NumericUtils;->splitLongRange(Lorg/apache/lucene/util/NumericUtils$LongRangeBuilder;IJJ)V

    goto :goto_1

    .line 441
    .end local v4    # "maxBound":J
    :cond_8
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->longValue()J

    move-result-wide v4

    goto :goto_3

    .line 443
    :cond_9
    sget-boolean v6, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->$assertionsDisabled:Z

    if-nez v6, :cond_a

    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/NumericField$DataType;

    sget-object v7, Lorg/apache/lucene/document/NumericField$DataType;->DOUBLE:Lorg/apache/lucene/document/NumericField$DataType;

    if-eq v6, v7, :cond_a

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 444
    :cond_a
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-nez v6, :cond_b

    sget-wide v4, Lorg/apache/lucene/search/NumericRangeQuery;->LONG_POSITIVE_INFINITY:J

    .restart local v4    # "maxBound":J
    :goto_4
    goto :goto_3

    .end local v4    # "maxBound":J
    :cond_b
    iget-object v6, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v6

    invoke-static {v6, v7}, Lorg/apache/lucene/util/NumericUtils;->doubleToSortableLong(D)J

    move-result-wide v4

    goto :goto_4

    .line 466
    .end local v2    # "minBound":J
    :pswitch_1
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/NumericField$DataType;

    sget-object v1, Lorg/apache/lucene/document/NumericField$DataType;->INT:Lorg/apache/lucene/document/NumericField$DataType;

    if-ne v0, v1, :cond_f

    .line 467
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-nez v0, :cond_e

    move v2, v6

    .line 473
    .local v2, "minBound":I
    :goto_5
    iget-boolean v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->minInclusive:Z

    if-nez v0, :cond_c

    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-eqz v0, :cond_c

    .line 474
    if-eq v2, v7, :cond_0

    .line 475
    add-int/lit8 v2, v2, 0x1

    .line 480
    :cond_c
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/NumericField$DataType;

    sget-object v1, Lorg/apache/lucene/document/NumericField$DataType;->INT:Lorg/apache/lucene/document/NumericField$DataType;

    if-ne v0, v1, :cond_13

    .line 481
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-nez v0, :cond_12

    move v4, v7

    .line 487
    .local v4, "maxBound":I
    :goto_6
    iget-boolean v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->maxInclusive:Z

    if-nez v0, :cond_d

    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-eqz v0, :cond_d

    .line 488
    if-eq v4, v6, :cond_0

    .line 489
    add-int/lit8 v4, v4, -0x1

    .line 492
    :cond_d
    new-instance v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum$2;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum$2;-><init>(Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;Lorg/apache/lucene/search/NumericRangeQuery;)V

    iget v1, p1, Lorg/apache/lucene/search/NumericRangeQuery;->precisionStep:I

    invoke-static {v0, v1, v2, v4}, Lorg/apache/lucene/util/NumericUtils;->splitIntRange(Lorg/apache/lucene/util/NumericUtils$IntRangeBuilder;III)V

    goto/16 :goto_1

    .line 467
    .end local v2    # "minBound":I
    .end local v4    # "maxBound":I
    :cond_e
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v2

    goto :goto_5

    .line 469
    :cond_f
    sget-boolean v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_10

    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/NumericField$DataType;

    sget-object v1, Lorg/apache/lucene/document/NumericField$DataType;->FLOAT:Lorg/apache/lucene/document/NumericField$DataType;

    if-eq v0, v1, :cond_10

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 470
    :cond_10
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    if-nez v0, :cond_11

    sget v2, Lorg/apache/lucene/search/NumericRangeQuery;->INT_NEGATIVE_INFINITY:I

    .restart local v2    # "minBound":I
    :goto_7
    goto :goto_5

    .end local v2    # "minBound":I
    :cond_11
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->min:Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/util/NumericUtils;->floatToSortableInt(F)I

    move-result v2

    goto :goto_7

    .line 481
    .restart local v2    # "minBound":I
    :cond_12
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v4

    goto :goto_6

    .line 483
    :cond_13
    sget-boolean v0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_14

    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->dataType:Lorg/apache/lucene/document/NumericField$DataType;

    sget-object v1, Lorg/apache/lucene/document/NumericField$DataType;->FLOAT:Lorg/apache/lucene/document/NumericField$DataType;

    if-eq v0, v1, :cond_14

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 484
    :cond_14
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    if-nez v0, :cond_15

    sget v4, Lorg/apache/lucene/search/NumericRangeQuery;->INT_POSITIVE_INFINITY:I

    .restart local v4    # "maxBound":I
    :goto_8
    goto :goto_6

    .end local v4    # "maxBound":I
    :cond_15
    iget-object v0, p1, Lorg/apache/lucene/search/NumericRangeQuery;->max:Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/util/NumericUtils;->floatToSortableInt(F)I

    move-result v4

    goto :goto_8

    .line 421
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic access$000(Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;

    .prologue
    .line 411
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->rangeBounds:Ljava/util/LinkedList;

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 582
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>.NumericRangeTermEnum;"
    iget-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->rangeBounds:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 583
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->currentUpperBound:Ljava/lang/String;

    .line 584
    invoke-super {p0}, Lorg/apache/lucene/search/FilteredTermEnum;->close()V

    .line 585
    return-void
.end method

.method public difference()F
    .locals 1

    .prologue
    .line 513
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>.NumericRangeTermEnum;"
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method protected endEnum()Z
    .locals 2

    .prologue
    .line 519
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>.NumericRangeTermEnum;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public next()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>.NumericRangeTermEnum;"
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 544
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    if-eqz v1, :cond_1

    .line 545
    sget-boolean v1, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 546
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermEnum;->next()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 547
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    .line 548
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->termCompare(Lorg/apache/lucene/index/Term;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    .line 576
    :goto_0
    return v1

    .line 555
    :cond_1
    iput-object v4, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    .line 556
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->rangeBounds:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v3, 0x2

    if-lt v1, v3, :cond_5

    .line 557
    sget-boolean v1, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->rangeBounds:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 559
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    if-eqz v1, :cond_3

    .line 560
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 561
    iput-object v4, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    .line 563
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->rangeBounds:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 564
    .local v0, "lowerBound":Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->rangeBounds:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->currentUpperBound:Ljava/lang/String;

    .line 566
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->reader:Lorg/apache/lucene/index/IndexReader;

    iget-object v3, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->termTemplate:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/Term;->createTerm(Ljava/lang/String;)Lorg/apache/lucene/index/Term;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    .line 567
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->actualEnum:Lorg/apache/lucene/index/TermEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    .line 568
    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->termCompare(Lorg/apache/lucene/index/Term;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    .line 569
    goto :goto_0

    .line 571
    :cond_4
    iput-object v4, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    goto :goto_1

    .line 575
    .end local v0    # "lowerBound":Ljava/lang/String;
    :cond_5
    sget-boolean v1, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->rangeBounds:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->currentTerm:Lorg/apache/lucene/index/Term;

    if-eqz v1, :cond_7

    :cond_6
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 576
    :cond_7
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected setEnum(Lorg/apache/lucene/index/TermEnum;)V
    .locals 2
    .param p1, "tenum"    # Lorg/apache/lucene/index/TermEnum;

    .prologue
    .line 525
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>.NumericRangeTermEnum;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected termCompare(Lorg/apache/lucene/index/Term;)Z
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 536
    .local p0, "this":Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;, "Lorg/apache/lucene/search/NumericRangeQuery<TT;>.NumericRangeTermEnum;"
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->this$0:Lorg/apache/lucene/search/NumericRangeQuery;

    iget-object v1, v1, Lorg/apache/lucene/search/NumericRangeQuery;->field:Ljava/lang/String;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/NumericRangeQuery$NumericRangeTermEnum;->currentUpperBound:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
