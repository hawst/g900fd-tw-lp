.class Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "MatchAllDocsQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MatchAllDocsQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MatchAllScorer"
.end annotation


# instance fields
.field private doc:I

.field final norms:[B

.field final score:F

.field final termDocs:Lorg/apache/lucene/index/TermDocs;

.field final synthetic this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/MatchAllDocsQuery;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;[B)V
    .locals 1
    .param p2, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p4, "w"    # Lorg/apache/lucene/search/Weight;
    .param p5, "norms"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    iput-object p1, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->this$0:Lorg/apache/lucene/search/MatchAllDocsQuery;

    .line 55
    invoke-direct {p0, p3, p4}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;)V

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    .line 56
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lorg/apache/lucene/index/IndexReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    .line 57
    invoke-virtual {p4}, Lorg/apache/lucene/search/Weight;->getValue()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->score:F

    .line 58
    iput-object p5, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->norms:[B

    .line 59
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0, p1}, Lorg/apache/lucene/index/TermDocs;->skipTo(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v0

    :goto_0
    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    return v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->termDocs:Lorg/apache/lucene/index/TermDocs;

    invoke-interface {v0}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v0

    :goto_0
    iput v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->doc:I

    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method public score()F
    .locals 4

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->norms:[B

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->score:F

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->score:F

    invoke-virtual {p0}, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->norms:[B

    invoke-virtual {p0}, Lorg/apache/lucene/search/MatchAllDocsQuery$MatchAllScorer;->docID()I

    move-result v3

    aget-byte v2, v2, v3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Similarity;->decodeNormValue(B)F

    move-result v1

    mul-float/2addr v0, v1

    goto :goto_0
.end method
