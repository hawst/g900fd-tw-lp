.class public Lorg/apache/lucene/search/function/ReverseOrdFieldSource;
.super Lorg/apache/lucene/search/function/ValueSource;
.source "ReverseOrdFieldSource.java"


# static fields
.field private static final hcode:I


# instance fields
.field public field:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 118
    const-class v0, Lorg/apache/lucene/search/function/ReverseOrdFieldSource;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    sput v0, Lorg/apache/lucene/search/function/ReverseOrdFieldSource;->hcode:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0}, Lorg/apache/lucene/search/function/ValueSource;-><init>()V

    .line 61
    iput-object p1, p0, Lorg/apache/lucene/search/function/ReverseOrdFieldSource;->field:Ljava/lang/String;

    .line 62
    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "rord("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/function/ReverseOrdFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 111
    if-ne p1, p0, :cond_1

    const/4 v1, 0x1

    .line 115
    :cond_0
    :goto_0
    return v1

    .line 112
    :cond_1
    if-eqz p1, :cond_0

    .line 113
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lorg/apache/lucene/search/function/ReverseOrdFieldSource;

    if-ne v2, v3, :cond_0

    move-object v0, p1

    .line 114
    check-cast v0, Lorg/apache/lucene/search/function/ReverseOrdFieldSource;

    .line 115
    .local v0, "other":Lorg/apache/lucene/search/function/ReverseOrdFieldSource;
    iget-object v1, p0, Lorg/apache/lucene/search/function/ReverseOrdFieldSource;->field:Ljava/lang/String;

    iget-object v2, v0, Lorg/apache/lucene/search/function/ReverseOrdFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getValues(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/function/DocValues;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    sget-object v3, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v4, p0, Lorg/apache/lucene/search/function/ReverseOrdFieldSource;->field:Ljava/lang/String;

    invoke-interface {v3, p1, v4}, Lorg/apache/lucene/search/FieldCache;->getStringIndex(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/search/FieldCache$StringIndex;

    move-result-object v2

    .line 75
    .local v2, "sindex":Lorg/apache/lucene/search/FieldCache$StringIndex;
    iget-object v0, v2, Lorg/apache/lucene/search/FieldCache$StringIndex;->order:[I

    .line 76
    .local v0, "arr":[I
    iget-object v3, v2, Lorg/apache/lucene/search/FieldCache$StringIndex;->lookup:[Ljava/lang/String;

    array-length v1, v3

    .line 78
    .local v1, "end":I
    new-instance v3, Lorg/apache/lucene/search/function/ReverseOrdFieldSource$1;

    invoke-direct {v3, p0, v1, v0}, Lorg/apache/lucene/search/function/ReverseOrdFieldSource$1;-><init>(Lorg/apache/lucene/search/function/ReverseOrdFieldSource;I[I)V

    return-object v3
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 123
    sget v0, Lorg/apache/lucene/search/function/ReverseOrdFieldSource;->hcode:I

    iget-object v1, p0, Lorg/apache/lucene/search/function/ReverseOrdFieldSource;->field:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
