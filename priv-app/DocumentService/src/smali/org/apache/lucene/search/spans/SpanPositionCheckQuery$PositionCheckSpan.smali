.class public Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;
.super Lorg/apache/lucene/search/spans/Spans;
.source "SpanPositionCheckQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "PositionCheckSpan"
.end annotation


# instance fields
.field private spans:Lorg/apache/lucene/search/spans/Spans;

.field final synthetic this$0:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;Lorg/apache/lucene/index/IndexReader;)V
    .locals 1
    .param p2, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->this$0:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/Spans;-><init>()V

    .line 108
    iget-object v0, p1, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;->match:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    .line 109
    return-void
.end method


# virtual methods
.method protected doNext()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 129
    :cond_0
    :goto_0
    sget-object v1, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$1;->$SwitchMap$org$apache$lucene$search$spans$SpanPositionCheckQuery$AcceptStatus:[I

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->this$0:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;

    invoke-virtual {v2, p0}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;->acceptPosition(Lorg/apache/lucene/search/spans/Spans;)Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$AcceptStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 130
    :pswitch_0
    const/4 v0, 0x1

    .line 137
    :goto_1
    return v0

    .line 132
    :pswitch_1
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 136
    :pswitch_2
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public doc()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v0

    return v0
.end method

.method public end()I
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v0

    return v0
.end method

.method public getPayload()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    const/4 v0, 0x0

    .line 156
    .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 159
    .restart local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    :cond_0
    return-object v0
.end method

.method public isPayloadAvailable()Z
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v0

    return v0
.end method

.method public next()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    const/4 v0, 0x0

    .line 116
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->doNext()Z

    move-result v0

    goto :goto_0
.end method

.method public skipTo(I)Z
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    const/4 v0, 0x0

    .line 124
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->doNext()Z

    move-result v0

    goto :goto_0
.end method

.method public start()I
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->spans:Lorg/apache/lucene/search/spans/Spans;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "spans("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery$PositionCheckSpan;->this$0:Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanPositionCheckQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
