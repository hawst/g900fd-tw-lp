.class Lorg/apache/lucene/search/DisjunctionSumScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "DisjunctionSumScorer.java"


# instance fields
.field private currentDoc:I

.field private currentScore:D

.field private final minimumNrMatchers:I

.field protected nrMatchers:I

.field private final nrScorers:I

.field private scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

.field protected final subScorers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Weight;Ljava/util/List;)V
    .locals 1
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Weight;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    .local p2, "subScorers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/DisjunctionSumScorer;-><init>(Lorg/apache/lucene/search/Weight;Ljava/util/List;I)V

    .line 94
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/Weight;Ljava/util/List;I)V
    .locals 2
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p3, "minimumNrMatchers"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Weight;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p2, "subScorers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    const/4 v0, -0x1

    .line 72
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 53
    iput v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    .line 56
    iput v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrMatchers:I

    .line 58
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentScore:D

    .line 74
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrScorers:I

    .line 76
    if-gtz p3, :cond_0

    .line 77
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Minimum nr of matchers must be positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrScorers:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    .line 80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "There must be at least 2 subScorers"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_1
    iput p3, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->minimumNrMatchers:I

    .line 84
    iput-object p2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->subScorers:Ljava/util/List;

    .line 86
    invoke-direct {p0}, Lorg/apache/lucene/search/DisjunctionSumScorer;->initScorerDocQueue()V

    .line 87
    return-void
.end method

.method private initScorerDocQueue()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    new-instance v2, Lorg/apache/lucene/util/ScorerDocQueue;

    iget v3, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrScorers:I

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/ScorerDocQueue;-><init>(I)V

    iput-object v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

    .line 101
    iget-object v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->subScorers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Scorer;

    .line 102
    .local v1, "se":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v2

    const v3, 0x7fffffff

    if-eq v2, v3, :cond_0

    .line 103
    iget-object v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/util/ScorerDocQueue;->insert(Lorg/apache/lucene/search/Scorer;)Z

    goto :goto_0

    .line 106
    .end local v1    # "se":Lorg/apache/lucene/search/Scorer;
    :cond_1
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    .line 221
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/ScorerDocQueue;->size()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->minimumNrMatchers:I

    if-ge v1, v2, :cond_0

    .line 222
    iput v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    .line 232
    :goto_0
    return v0

    .line 224
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    if-gt p1, v1, :cond_1

    .line 225
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    goto :goto_0

    .line 228
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/ScorerDocQueue;->topDoc()I

    move-result v1

    if-lt v1, p1, :cond_3

    .line 229
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionSumScorer;->advanceAfterCurrent()Z

    move-result v1

    if-eqz v1, :cond_2

    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    goto :goto_0

    :cond_2
    iput v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    goto :goto_0

    .line 230
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/ScorerDocQueue;->topSkipToAndAdjustElsePop(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 231
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/ScorerDocQueue;->size()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->minimumNrMatchers:I

    if-ge v1, v2, :cond_1

    .line 232
    iput v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    goto :goto_0
.end method

.method protected advanceAfterCurrent()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 167
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/ScorerDocQueue;->topDoc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    .line 168
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/ScorerDocQueue;->topScore()F

    move-result v1

    float-to-double v2, v1

    iput-wide v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentScore:D

    .line 169
    iput v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrMatchers:I

    .line 171
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/ScorerDocQueue;->topNextAndAdjustElsePop()Z

    move-result v1

    if-nez v1, :cond_2

    .line 172
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/ScorerDocQueue;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 183
    :cond_1
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrMatchers:I

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->minimumNrMatchers:I

    if-lt v1, v2, :cond_3

    .line 186
    :goto_1
    return v0

    .line 176
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/ScorerDocQueue;->topDoc()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    if-ne v1, v2, :cond_1

    .line 179
    iget-wide v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentScore:D

    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/ScorerDocQueue;->topScore()F

    move-result v1

    float-to-double v4, v1

    add-double/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentScore:D

    .line 180
    iget v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrMatchers:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrMatchers:I

    goto :goto_0

    .line 185
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/util/ScorerDocQueue;->size()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->minimumNrMatchers:I

    if-ge v1, v2, :cond_0

    .line 186
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    return v0
.end method

.method public nextDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->scorerDocQueue:Lorg/apache/lucene/util/ScorerDocQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/util/ScorerDocQueue;->size()I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->minimumNrMatchers:I

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionSumScorer;->advanceAfterCurrent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 142
    :cond_0
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    .line 144
    :cond_1
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    return v0
.end method

.method public nrMatchers()I
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrMatchers:I

    return v0
.end method

.method public score()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 195
    iget-wide v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentScore:D

    double-to-float v0, v0

    return v0
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 114
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionSumScorer;->nextDoc()I

    move-result v0

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    .line 115
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Collector;->collect(I)V

    goto :goto_0

    .line 117
    :cond_0
    return-void
.end method

.method protected score(Lorg/apache/lucene/search/Collector;II)Z
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "max"    # I
    .param p3, "firstDocID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    invoke-virtual {p1, p0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 130
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    if-ge v0, p2, :cond_1

    .line 131
    iget v0, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->currentDoc:I

    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 132
    invoke-virtual {p0}, Lorg/apache/lucene/search/DisjunctionSumScorer;->nextDoc()I

    move-result v0

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 133
    const/4 v0, 0x0

    .line 136
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
