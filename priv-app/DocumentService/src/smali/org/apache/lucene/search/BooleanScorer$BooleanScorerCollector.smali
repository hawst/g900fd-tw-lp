.class final Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;
.super Lorg/apache/lucene/search/Collector;
.source "BooleanScorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/BooleanScorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BooleanScorerCollector"
.end annotation


# instance fields
.field private bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

.field private mask:I

.field private scorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method public constructor <init>(ILorg/apache/lucene/search/BooleanScorer$BucketTable;)V
    .locals 0
    .param p1, "mask"    # I
    .param p2, "bucketTable"    # Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    .prologue
    .line 66
    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    .line 67
    iput p1, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->mask:I

    .line 68
    iput-object p2, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    .line 69
    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    return v0
.end method

.method public collect(I)V
    .locals 5
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    .line 74
    .local v2, "table":Lorg/apache/lucene/search/BooleanScorer$BucketTable;
    and-int/lit16 v1, p1, 0x7ff

    .line 75
    .local v1, "i":I
    iget-object v3, v2, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->buckets:[Lorg/apache/lucene/search/BooleanScorer$Bucket;

    aget-object v0, v3, v1

    .line 77
    .local v0, "bucket":Lorg/apache/lucene/search/BooleanScorer$Bucket;
    iget v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->doc:I

    if-eq v3, p1, :cond_0

    .line 78
    iput p1, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->doc:I

    .line 79
    iget-object v3, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v3

    iput v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->score:F

    .line 80
    iget v3, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->mask:I

    iput v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->bits:I

    .line 81
    const/4 v3, 0x1

    iput v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->coord:I

    .line 83
    iget-object v3, v2, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->next:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 84
    iput-object v0, v2, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 90
    :goto_0
    return-void

    .line 86
    :cond_0
    iget v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->score:F

    iget-object v4, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v4}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v4

    add-float/2addr v3, v4

    iput v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->score:F

    .line 87
    iget v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->bits:I

    iget v4, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->mask:I

    or-int/2addr v3, v4

    iput v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->bits:I

    .line 88
    iget v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->coord:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lorg/apache/lucene/search/BooleanScorer$Bucket;->coord:I

    goto :goto_0
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 0
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I

    .prologue
    .line 95
    return-void
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 0
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;->scorer:Lorg/apache/lucene/search/Scorer;

    .line 100
    return-void
.end method
