.class Lorg/apache/lucene/search/FieldCacheRangeFilter$1;
.super Lorg/apache/lucene/search/FieldCacheRangeFilter;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter;->newStringRange(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldCacheRangeFilter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const-class v0, Lorg/apache/lucene/search/FieldCacheRangeFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 8
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Z
    .param p6, "x5"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZLorg/apache/lucene/search/FieldCacheRangeFilter$1;)V

    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 8
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->field:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lorg/apache/lucene/search/FieldCache;->getStringIndex(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/search/FieldCache$StringIndex;

    move-result-object v3

    .line 85
    .local v3, "fcsi":Lorg/apache/lucene/search/FieldCache$StringIndex;
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->lowerVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/search/FieldCache$StringIndex;->binarySearchLookup(Ljava/lang/String;)I

    move-result v6

    .line 86
    .local v6, "lowerPoint":I
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->upperVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/search/FieldCache$StringIndex;->binarySearchLookup(Ljava/lang/String;)I

    move-result v7

    .line 94
    .local v7, "upperPoint":I
    if-nez v6, :cond_1

    .line 95
    sget-boolean v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->lowerVal:Ljava/lang/Object;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 96
    :cond_0
    const/4 v4, 0x1

    .line 105
    .local v4, "inclusiveLowerPoint":I
    :goto_0
    if-nez v7, :cond_6

    .line 106
    sget-boolean v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->upperVal:Ljava/lang/Object;

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 97
    .end local v4    # "inclusiveLowerPoint":I
    :cond_1
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->includeLower:Z

    if-eqz v0, :cond_2

    if-lez v6, :cond_2

    .line 98
    move v4, v6

    .restart local v4    # "inclusiveLowerPoint":I
    goto :goto_0

    .line 99
    .end local v4    # "inclusiveLowerPoint":I
    :cond_2
    if-lez v6, :cond_3

    .line 100
    add-int/lit8 v4, v6, 0x1

    .restart local v4    # "inclusiveLowerPoint":I
    goto :goto_0

    .line 102
    .end local v4    # "inclusiveLowerPoint":I
    :cond_3
    const/4 v0, 0x1

    neg-int v1, v6

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .restart local v4    # "inclusiveLowerPoint":I
    goto :goto_0

    .line 107
    :cond_4
    const v5, 0x7fffffff

    .line 116
    .local v5, "inclusiveUpperPoint":I
    :goto_1
    if-lez v5, :cond_5

    if-le v4, v5, :cond_9

    .line 117
    :cond_5
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 121
    :goto_2
    return-object v0

    .line 108
    .end local v5    # "inclusiveUpperPoint":I
    :cond_6
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->includeUpper:Z

    if-eqz v0, :cond_7

    if-lez v7, :cond_7

    .line 109
    move v5, v7

    .restart local v5    # "inclusiveUpperPoint":I
    goto :goto_1

    .line 110
    .end local v5    # "inclusiveUpperPoint":I
    :cond_7
    if-lez v7, :cond_8

    .line 111
    add-int/lit8 v5, v7, -0x1

    .restart local v5    # "inclusiveUpperPoint":I
    goto :goto_1

    .line 113
    .end local v5    # "inclusiveUpperPoint":I
    :cond_8
    neg-int v0, v7

    add-int/lit8 v5, v0, -0x2

    .restart local v5    # "inclusiveUpperPoint":I
    goto :goto_1

    .line 119
    :cond_9
    sget-boolean v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1;->$assertionsDisabled:Z

    if-nez v0, :cond_b

    if-lez v4, :cond_a

    if-gtz v5, :cond_b

    :cond_a
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 121
    :cond_b
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$1$1;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/FieldCacheRangeFilter$1$1;-><init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$1;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCache$StringIndex;II)V

    goto :goto_2
.end method
