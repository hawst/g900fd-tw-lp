.class public final Lorg/apache/lucene/search/FieldComparator$ByteComparator;
.super Lorg/apache/lucene/search/FieldComparator$NumericComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ByteComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator$NumericComparator",
        "<",
        "Ljava/lang/Byte;",
        ">;"
    }
.end annotation


# instance fields
.field private bottom:B

.field private currentReaderValues:[B

.field private final parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

.field private final values:[B


# direct methods
.method constructor <init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Byte;)V
    .locals 1
    .param p1, "numHits"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p4, "missingValue"    # Ljava/lang/Byte;

    .prologue
    .line 219
    invoke-direct {p0, p2, p4}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;-><init>(Ljava/lang/String;Ljava/lang/Number;)V

    .line 220
    new-array v0, p1, [B

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->values:[B

    .line 221
    check-cast p3, Lorg/apache/lucene/search/FieldCache$ByteParser;

    .end local p3    # "parser":Lorg/apache/lucene/search/FieldCache$Parser;
    iput-object p3, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    .line 222
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 2
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 226
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->values:[B

    aget-byte v0, v0, p1

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->values:[B

    aget-byte v1, v1, p2

    sub-int/2addr v0, v1

    return v0
.end method

.method public compareBottom(I)I
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 231
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->currentReaderValues:[B

    aget-byte v0, v1, p1

    .line 234
    .local v0, "v2":B
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 235
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->missingValue:Ljava/lang/Number;

    check-cast v1, Ljava/lang/Byte;

    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    .line 237
    :cond_0
    iget-byte v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->bottom:B

    sub-int/2addr v1, v0

    return v1
.end method

.method public copy(II)V
    .locals 2
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 242
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->currentReaderValues:[B

    aget-byte v0, v1, p2

    .line 245
    .local v0, "v2":B
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 246
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->missingValue:Ljava/lang/Number;

    check-cast v1, Ljava/lang/Byte;

    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    .line 248
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->values:[B

    aput-byte v0, v1, p1

    .line 249
    return-void
.end method

.method public setBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 261
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->values:[B

    aget-byte v0, v0, p1

    iput-byte v0, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->bottom:B

    .line 262
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 255
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->parser:Lorg/apache/lucene/search/FieldCache$ByteParser;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->missingValue:Ljava/lang/Number;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldCache;->getBytes(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ByteParser;Z)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->currentReaderValues:[B

    .line 256
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->setNextReader(Lorg/apache/lucene/index/IndexReader;I)V

    .line 257
    return-void

    .line 255
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public value(I)Ljava/lang/Byte;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 266
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->values:[B

    aget-byte v0, v0, p1

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 212
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$ByteComparator;->value(I)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method
