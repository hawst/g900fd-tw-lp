.class final Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;
.super Ljava/lang/Object;
.source "MultiSearcher.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MultiSearcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "MultiSearcherCallableNoSort"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lorg/apache/lucene/search/TopDocs;",
        ">;"
    }
.end annotation


# instance fields
.field private final filter:Lorg/apache/lucene/search/Filter;

.field private final hq:Lorg/apache/lucene/search/HitQueue;

.field private final i:I

.field private final lock:Ljava/util/concurrent/locks/Lock;

.field private final nDocs:I

.field private final searchable:Lorg/apache/lucene/search/Searchable;

.field private final starts:[I

.field private final weight:Lorg/apache/lucene/search/Weight;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/locks/Lock;Lorg/apache/lucene/search/Searchable;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/HitQueue;I[I)V
    .locals 0
    .param p1, "lock"    # Ljava/util/concurrent/locks/Lock;
    .param p2, "searchable"    # Lorg/apache/lucene/search/Searchable;
    .param p3, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p4, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p5, "nDocs"    # I
    .param p6, "hq"    # Lorg/apache/lucene/search/HitQueue;
    .param p7, "i"    # I
    .param p8, "starts"    # [I

    .prologue
    .line 369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 370
    iput-object p1, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->lock:Ljava/util/concurrent/locks/Lock;

    .line 371
    iput-object p2, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->searchable:Lorg/apache/lucene/search/Searchable;

    .line 372
    iput-object p3, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->weight:Lorg/apache/lucene/search/Weight;

    .line 373
    iput-object p4, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->filter:Lorg/apache/lucene/search/Filter;

    .line 374
    iput p5, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->nDocs:I

    .line 375
    iput-object p6, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->hq:Lorg/apache/lucene/search/HitQueue;

    .line 376
    iput p7, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->i:I

    .line 377
    iput-object p8, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->starts:[I

    .line 378
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 357
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->call()Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    return-object v0
.end method

.method public call()Lorg/apache/lucene/search/TopDocs;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 381
    iget-object v4, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->searchable:Lorg/apache/lucene/search/Searchable;

    iget-object v5, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->weight:Lorg/apache/lucene/search/Weight;

    iget-object v6, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->filter:Lorg/apache/lucene/search/Filter;

    iget v7, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->nDocs:I

    invoke-interface {v4, v5, v6, v7}, Lorg/apache/lucene/search/Searchable;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;I)Lorg/apache/lucene/search/TopDocs;

    move-result-object v0

    .line 382
    .local v0, "docs":Lorg/apache/lucene/search/TopDocs;
    iget-object v3, v0, Lorg/apache/lucene/search/TopDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .line 383
    .local v3, "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    .line 384
    aget-object v2, v3, v1

    .line 385
    .local v2, "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    iget v4, v2, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    iget-object v5, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->starts:[I

    iget v6, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->i:I

    aget v5, v5, v6

    add-int/2addr v4, v5

    iput v4, v2, Lorg/apache/lucene/search/ScoreDoc;->doc:I

    .line 387
    iget-object v4, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 389
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->hq:Lorg/apache/lucene/search/HitQueue;

    invoke-virtual {v4, v2}, Lorg/apache/lucene/search/HitQueue;->insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    if-ne v2, v4, :cond_1

    .line 392
    iget-object v4, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 395
    .end local v2    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :cond_0
    return-object v0

    .line 392
    .restart local v2    # "scoreDoc":Lorg/apache/lucene/search/ScoreDoc;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v4

    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableNoSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 383
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
