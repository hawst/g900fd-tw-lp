.class public Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;
.super Lorg/apache/lucene/search/spans/SpanWeight;
.source "PayloadTermQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/payloads/PayloadTermQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "PayloadTermWeight"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/payloads/PayloadTermQuery;Lorg/apache/lucene/search/payloads/PayloadTermQuery;Lorg/apache/lucene/search/Searcher;)V
    .locals 0
    .param p2, "query"    # Lorg/apache/lucene/search/payloads/PayloadTermQuery;
    .param p3, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iput-object p1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    .line 72
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/search/spans/SpanWeight;-><init>(Lorg/apache/lucene/search/spans/SpanQuery;Lorg/apache/lucene/search/Searcher;)V

    .line 73
    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->this$0:Lorg/apache/lucene/search/payloads/PayloadTermQuery;

    # getter for: Lorg/apache/lucene/search/payloads/PayloadTermQuery;->includeSpanScore:Z
    invoke-static {v1}, Lorg/apache/lucene/search/payloads/PayloadTermQuery;->access$000(Lorg/apache/lucene/search/payloads/PayloadTermQuery;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/search/spans/SpanWeight;->explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;

    move-result-object v1

    .line 89
    :goto_0
    return-object v1

    .line 88
    :cond_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;

    .line 89
    .local v0, "scorer":Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;
    invoke-virtual {v0, p2}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;->explain(I)Lorg/apache/lucene/search/Explanation;

    move-result-object v1

    goto :goto_0
.end method

.method public scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/spans/TermSpans;

    iget-object v4, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    iget-object v1, p0, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;->query:Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v5

    move-object v1, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight$PayloadTermSpanScorer;-><init>(Lorg/apache/lucene/search/payloads/PayloadTermQuery$PayloadTermWeight;Lorg/apache/lucene/search/spans/TermSpans;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Similarity;[B)V

    return-object v0
.end method
