.class public Lorg/apache/lucene/search/FuzzyQuery;
.super Lorg/apache/lucene/search/MultiTermQuery;
.source "FuzzyQuery.java"


# static fields
.field public static final defaultMaxExpansions:I = 0x7fffffff

.field public static final defaultMinSimilarity:F = 0.5f

.field public static final defaultPrefixLength:I


# instance fields
.field private minimumSimilarity:F

.field private prefixLength:I

.field protected term:Lorg/apache/lucene/index/Term;

.field private termLongEnough:Z


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 110
    const/high16 v0, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    const v2, 0x7fffffff

    invoke-direct {p0, p1, v0, v1, v2}, Lorg/apache/lucene/search/FuzzyQuery;-><init>(Lorg/apache/lucene/index/Term;FII)V

    .line 111
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;F)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "minimumSimilarity"    # F

    .prologue
    .line 103
    const/4 v0, 0x0

    const v1, 0x7fffffff

    invoke-direct {p0, p1, p2, v0, v1}, Lorg/apache/lucene/search/FuzzyQuery;-><init>(Lorg/apache/lucene/index/Term;FII)V

    .line 104
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;FI)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "minimumSimilarity"    # F
    .param p3, "prefixLength"    # I

    .prologue
    .line 96
    const v0, 0x7fffffff

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/search/FuzzyQuery;-><init>(Lorg/apache/lucene/index/Term;FII)V

    .line 97
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/Term;FII)V
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "minimumSimilarity"    # F
    .param p3, "prefixLength"    # I
    .param p4, "maxExpansions"    # I

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 70
    invoke-direct {p0}, Lorg/apache/lucene/search/MultiTermQuery;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/FuzzyQuery;->termLongEnough:Z

    .line 71
    iput-object p1, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    .line 73
    cmpl-float v0, p2, v2

    if-ltz v0, :cond_0

    .line 74
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "minimumSimilarity >= 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_0
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-gez v0, :cond_1

    .line 76
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "minimumSimilarity < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_1
    if-gez p3, :cond_2

    .line 78
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "prefixLength < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_2
    if-gez p4, :cond_3

    .line 80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "maxExpansions < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_3
    new-instance v0, Lorg/apache/lucene/search/MultiTermQuery$TopTermsScoringBooleanQueryRewrite;

    invoke-direct {v0, p4}, Lorg/apache/lucene/search/MultiTermQuery$TopTermsScoringBooleanQueryRewrite;-><init>(I)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/FuzzyQuery;->setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V

    .line 84
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    int-to-float v0, v0

    sub-float v1, v2, p2

    div-float v1, v2, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/search/FuzzyQuery;->termLongEnough:Z

    .line 88
    :cond_4
    iput p2, p0, Lorg/apache/lucene/search/FuzzyQuery;->minimumSimilarity:F

    .line 89
    iput p3, p0, Lorg/apache/lucene/search/FuzzyQuery;->prefixLength:I

    .line 90
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 171
    if-ne p0, p1, :cond_1

    .line 188
    :cond_0
    :goto_0
    return v1

    .line 173
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 174
    goto :goto_0

    .line 175
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 176
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 177
    check-cast v0, Lorg/apache/lucene/search/FuzzyQuery;

    .line 178
    .local v0, "other":Lorg/apache/lucene/search/FuzzyQuery;
    iget v3, p0, Lorg/apache/lucene/search/FuzzyQuery;->minimumSimilarity:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    iget v4, v0, Lorg/apache/lucene/search/FuzzyQuery;->minimumSimilarity:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 180
    goto :goto_0

    .line 181
    :cond_4
    iget v3, p0, Lorg/apache/lucene/search/FuzzyQuery;->prefixLength:I

    iget v4, v0, Lorg/apache/lucene/search/FuzzyQuery;->prefixLength:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 182
    goto :goto_0

    .line 183
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v3, :cond_6

    .line 184
    iget-object v3, v0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    if-eqz v3, :cond_0

    move v1, v2

    .line 185
    goto :goto_0

    .line 186
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    iget-object v4, v0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 187
    goto :goto_0
.end method

.method protected getEnum(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/FilteredTermEnum;
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    iget-boolean v0, p0, Lorg/apache/lucene/search/FuzzyQuery;->termLongEnough:Z

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Lorg/apache/lucene/search/SingleTermEnum;

    iget-object v1, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/search/SingleTermEnum;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/Term;)V

    .line 135
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/search/FuzzyTermEnum;

    invoke-virtual {p0}, Lorg/apache/lucene/search/FuzzyQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/FuzzyQuery;->minimumSimilarity:F

    iget v3, p0, Lorg/apache/lucene/search/FuzzyQuery;->prefixLength:I

    invoke-direct {v0, p1, v1, v2, v3}, Lorg/apache/lucene/search/FuzzyTermEnum;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/Term;FI)V

    goto :goto_0
.end method

.method public getMinSimilarity()F
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lorg/apache/lucene/search/FuzzyQuery;->minimumSimilarity:F

    return v0
.end method

.method public getPrefixLength()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lorg/apache/lucene/search/FuzzyQuery;->prefixLength:I

    return v0
.end method

.method public getTerm()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 161
    const/16 v0, 0x1f

    .line 162
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v1

    .line 163
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/lucene/search/FuzzyQuery;->minimumSimilarity:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 164
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/lucene/search/FuzzyQuery;->prefixLength:I

    add-int v1, v2, v3

    .line 165
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v3, v2

    .line 166
    return v1

    .line 165
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 148
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 149
    iget-object v1, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FuzzyQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    const/16 v1, 0x7e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 154
    iget v1, p0, Lorg/apache/lucene/search/FuzzyQuery;->minimumSimilarity:F

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    invoke-virtual {p0}, Lorg/apache/lucene/search/FuzzyQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
