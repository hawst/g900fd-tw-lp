.class Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;
.super Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;
.source "TopFieldCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopFieldCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OutOfOrderOneComparatorNonScoringCollector"
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V
    .locals 0
    .param p2, "numHits"    # I
    .param p3, "fillFields"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;IZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    .line 116
    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x1

    return v0
.end method

.method public collect(I)V
    .locals 4
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->totalHits:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->totalHits:I

    .line 121
    iget-boolean v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->queueFull:Z

    if-eqz v2, :cond_2

    .line 123
    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->reverseMul:I

    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/search/FieldComparator;->compareBottom(I)I

    move-result v3

    mul-int v0, v2, v3

    .line 124
    .local v0, "cmp":I
    if-ltz v0, :cond_0

    if-nez v0, :cond_1

    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->docBase:I

    add-int/2addr v2, p1

    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v3, v3, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    if-le v2, v3, :cond_1

    .line 142
    .end local v0    # "cmp":I
    :cond_0
    :goto_0
    return-void

    .line 129
    .restart local v0    # "cmp":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v3, v3, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v2, v3, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 130
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->updateBottom(I)V

    .line 131
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v3, v3, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    goto :goto_0

    .line 134
    .end local v0    # "cmp":I
    :cond_2
    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->totalHits:I

    add-int/lit8 v1, v2, -0x1

    .line 136
    .local v1, "slot":I
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v2, v1, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 137
    const/high16 v2, 0x7fc00000    # NaNf

    invoke-virtual {p0, v1, p1, v2}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->add(IIF)V

    .line 138
    iget-boolean v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->queueFull:Z

    if-eqz v2, :cond_0

    .line 139
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->comparator:Lorg/apache/lucene/search/FieldComparator;

    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v3, v3, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    goto :goto_0
.end method
