.class public abstract Lorg/apache/lucene/search/MultiTermQuery;
.super Lorg/apache/lucene/search/Query;
.source "MultiTermQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/MultiTermQuery$ConstantScoreAutoRewrite;,
        Lorg/apache/lucene/search/MultiTermQuery$TopTermsBoostOnlyBooleanQueryRewrite;,
        Lorg/apache/lucene/search/MultiTermQuery$TopTermsScoringBooleanQueryRewrite;,
        Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    }
.end annotation


# static fields
.field public static final CONSTANT_SCORE_AUTO_REWRITE_DEFAULT:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

.field public static final CONSTANT_SCORE_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

.field public static final CONSTANT_SCORE_FILTER_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

.field public static final SCORING_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;


# instance fields
.field transient numberOfTerms:I

.field protected rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lorg/apache/lucene/search/MultiTermQuery$1;

    invoke-direct {v0}, Lorg/apache/lucene/search/MultiTermQuery$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_FILTER_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 117
    sget-object v0, Lorg/apache/lucene/search/ScoringRewrite;->SCORING_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/ScoringRewrite;

    sput-object v0, Lorg/apache/lucene/search/MultiTermQuery;->SCORING_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 129
    sget-object v0, Lorg/apache/lucene/search/ScoringRewrite;->CONSTANT_SCORE_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    sput-object v0, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_BOOLEAN_QUERY_REWRITE:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 237
    new-instance v0, Lorg/apache/lucene/search/MultiTermQuery$2;

    invoke-direct {v0}, Lorg/apache/lucene/search/MultiTermQuery$2;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_AUTO_REWRITE_DEFAULT:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 258
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 62
    sget-object v0, Lorg/apache/lucene/search/MultiTermQuery;->CONSTANT_SCORE_AUTO_REWRITE_DEFAULT:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    iput-object v0, p0, Lorg/apache/lucene/search/MultiTermQuery;->rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 63
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/search/MultiTermQuery;->numberOfTerms:I

    .line 259
    return-void
.end method


# virtual methods
.method public clearTotalNumberOfTerms()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 294
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/search/MultiTermQuery;->numberOfTerms:I

    .line 295
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 342
    if-ne p0, p1, :cond_1

    .line 354
    :cond_0
    :goto_0
    return v1

    .line 344
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 345
    goto :goto_0

    .line 346
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 347
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 348
    check-cast v0, Lorg/apache/lucene/search/MultiTermQuery;

    .line 349
    .local v0, "other":Lorg/apache/lucene/search/MultiTermQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 350
    goto :goto_0

    .line 351
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/MultiTermQuery;->rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    iget-object v4, v0, Lorg/apache/lucene/search/MultiTermQuery;->rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 352
    goto :goto_0
.end method

.method protected abstract getEnum(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/FilteredTermEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getRewriteMethod()Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lorg/apache/lucene/search/MultiTermQuery;->rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    return-object v0
.end method

.method public getTotalNumberOfTerms()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 283
    iget v0, p0, Lorg/apache/lucene/search/MultiTermQuery;->numberOfTerms:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 332
    const/16 v0, 0x1f

    .line 333
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 334
    .local v1, "result":I
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiTermQuery;->getBoost()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/lit8 v1, v2, 0x1f

    .line 335
    mul-int/lit8 v1, v1, 0x1f

    .line 336
    iget-object v2, p0, Lorg/apache/lucene/search/MultiTermQuery;->rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    .line 337
    return v1
.end method

.method protected incTotalNumberOfTerms(I)V
    .locals 1
    .param p1, "inc"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 302
    iget v0, p0, Lorg/apache/lucene/search/MultiTermQuery;->numberOfTerms:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/search/MultiTermQuery;->numberOfTerms:I

    .line 303
    return-void
.end method

.method public final rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 312
    iget-object v0, p0, Lorg/apache/lucene/search/MultiTermQuery;->rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    invoke-virtual {v0, p1, p0}, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;->rewrite(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public setRewriteMethod(Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;)V
    .locals 0
    .param p1, "method"    # Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .prologue
    .line 327
    iput-object p1, p0, Lorg/apache/lucene/search/MultiTermQuery;->rewriteMethod:Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;

    .line 328
    return-void
.end method
