.class public Lorg/apache/lucene/search/FilterManager$FilterCleaner;
.super Ljava/lang/Object;
.source "FilterManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FilterManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "FilterCleaner"
.end annotation


# instance fields
.field private running:Z

.field private sortedFilterItems:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/search/FilterManager$FilterItem;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/lucene/search/FilterManager;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FilterManager;)V
    .locals 2

    .prologue
    .line 157
    iput-object p1, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->this$0:Lorg/apache/lucene/search/FilterManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->running:Z

    .line 158
    new-instance v0, Ljava/util/TreeSet;

    new-instance v1, Lorg/apache/lucene/search/FilterManager$FilterCleaner$1;

    invoke-direct {v1, p0, p1}, Lorg/apache/lucene/search/FilterManager$FilterCleaner$1;-><init>(Lorg/apache/lucene/search/FilterManager$FilterCleaner;Lorg/apache/lucene/search/FilterManager;)V

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->sortedFilterItems:Ljava/util/TreeSet;

    .line 174
    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 177
    :goto_0
    iget-boolean v6, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->running:Z

    if-eqz v6, :cond_3

    .line 181
    iget-object v6, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->this$0:Lorg/apache/lucene/search/FilterManager;

    iget-object v6, v6, Lorg/apache/lucene/search/FilterManager;->cache:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v6

    iget-object v7, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->this$0:Lorg/apache/lucene/search/FilterManager;

    iget v7, v7, Lorg/apache/lucene/search/FilterManager;->cacheCleanSize:I

    if-le v6, v7, :cond_2

    .line 183
    iget-object v6, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->sortedFilterItems:Ljava/util/TreeSet;

    invoke-virtual {v6}, Ljava/util/TreeSet;->clear()V

    .line 184
    iget-object v6, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->this$0:Lorg/apache/lucene/search/FilterManager;

    iget-object v7, v6, Lorg/apache/lucene/search/FilterManager;->cache:Ljava/util/Map;

    monitor-enter v7

    .line 185
    :try_start_0
    iget-object v6, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->sortedFilterItems:Ljava/util/TreeSet;

    iget-object v8, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->this$0:Lorg/apache/lucene/search/FilterManager;

    iget-object v8, v8, Lorg/apache/lucene/search/FilterManager;->cache:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    .line 186
    iget-object v6, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->sortedFilterItems:Ljava/util/TreeSet;

    invoke-virtual {v6}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 187
    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Lorg/apache/lucene/search/FilterManager$FilterItem;>;>;"
    iget-object v6, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->this$0:Lorg/apache/lucene/search/FilterManager;

    iget-object v6, v6, Lorg/apache/lucene/search/FilterManager;->cache:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v6

    iget-object v8, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->this$0:Lorg/apache/lucene/search/FilterManager;

    iget v8, v8, Lorg/apache/lucene/search/FilterManager;->cacheCleanSize:I

    sub-int/2addr v6, v8

    int-to-double v8, v6

    const-wide/high16 v10, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v8, v10

    double-to-int v5, v8

    .line 188
    .local v5, "numToDelete":I
    const/4 v0, 0x0

    .local v0, "counter":I
    move v1, v0

    .line 190
    .end local v0    # "counter":I
    .local v1, "counter":I
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "counter":I
    .restart local v0    # "counter":I
    if-ge v1, v5, :cond_1

    .line 191
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 192
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lorg/apache/lucene/search/FilterManager$FilterItem;>;"
    iget-object v6, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->this$0:Lorg/apache/lucene/search/FilterManager;

    iget-object v6, v6, Lorg/apache/lucene/search/FilterManager;->cache:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v0

    .line 193
    .end local v0    # "counter":I
    .restart local v1    # "counter":I
    goto :goto_1

    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lorg/apache/lucene/search/FilterManager$FilterItem;>;"
    :cond_0
    move v0, v1

    .line 194
    .end local v1    # "counter":I
    .restart local v0    # "counter":I
    :cond_1
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    iget-object v6, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->sortedFilterItems:Ljava/util/TreeSet;

    invoke-virtual {v6}, Ljava/util/TreeSet;->clear()V

    .line 200
    .end local v0    # "counter":I
    .end local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Lorg/apache/lucene/search/FilterManager$FilterItem;>;>;"
    .end local v5    # "numToDelete":I
    :cond_2
    :try_start_1
    iget-object v6, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner;->this$0:Lorg/apache/lucene/search/FilterManager;

    iget-wide v6, v6, Lorg/apache/lucene/search/FilterManager;->cleanSleepTime:J

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 201
    :catch_0
    move-exception v3

    .line 202
    .local v3, "ie":Ljava/lang/InterruptedException;
    new-instance v6, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v6, v3}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v6

    .line 194
    .end local v3    # "ie":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v6

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .line 205
    :cond_3
    return-void
.end method
