.class public Lorg/apache/lucene/search/SingleTermEnum;
.super Lorg/apache/lucene/search/FilteredTermEnum;
.source "SingleTermEnum.java"


# instance fields
.field private endEnum:Z

.field private singleTerm:Lorg/apache/lucene/index/Term;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "singleTerm"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/lucene/search/FilteredTermEnum;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/search/SingleTermEnum;->endEnum:Z

    .line 44
    iput-object p2, p0, Lorg/apache/lucene/search/SingleTermEnum;->singleTerm:Lorg/apache/lucene/index/Term;

    .line 45
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/SingleTermEnum;->setEnum(Lorg/apache/lucene/index/TermEnum;)V

    .line 46
    return-void
.end method


# virtual methods
.method public difference()F
    .locals 1

    .prologue
    .line 50
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method protected endEnum()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lorg/apache/lucene/search/SingleTermEnum;->endEnum:Z

    return v0
.end method

.method protected termCompare(Lorg/apache/lucene/index/Term;)Z
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    const/4 v0, 0x1

    .line 60
    iget-object v1, p0, Lorg/apache/lucene/search/SingleTermEnum;->singleTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    :goto_0
    return v0

    .line 63
    :cond_0
    iput-boolean v0, p0, Lorg/apache/lucene/search/SingleTermEnum;->endEnum:Z

    .line 64
    const/4 v0, 0x0

    goto :goto_0
.end method
