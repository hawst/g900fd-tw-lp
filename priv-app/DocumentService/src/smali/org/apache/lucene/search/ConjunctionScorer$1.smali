.class Lorg/apache/lucene/search/ConjunctionScorer$1;
.super Ljava/lang/Object;
.source "ConjunctionScorer.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/ConjunctionScorer;-><init>(Lorg/apache/lucene/search/Weight;F[Lorg/apache/lucene/search/Scorer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/search/Scorer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/ConjunctionScorer;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/ConjunctionScorer;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lorg/apache/lucene/search/ConjunctionScorer$1;->this$0:Lorg/apache/lucene/search/ConjunctionScorer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 58
    check-cast p1, Lorg/apache/lucene/search/Scorer;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/search/Scorer;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/ConjunctionScorer$1;->compare(Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/Scorer;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/Scorer;)I
    .locals 2
    .param p1, "o1"    # Lorg/apache/lucene/search/Scorer;
    .param p2, "o2"    # Lorg/apache/lucene/search/Scorer;

    .prologue
    .line 59
    invoke-virtual {p1}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v0

    invoke-virtual {p2}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method
