.class Lorg/apache/lucene/search/MultiSearcher$1;
.super Lorg/apache/lucene/search/Collector;
.source "MultiSearcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/MultiSearcher;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;Lorg/apache/lucene/search/Collector;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/MultiSearcher;

.field final synthetic val$collector:Lorg/apache/lucene/search/Collector;

.field final synthetic val$start:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/MultiSearcher;Lorg/apache/lucene/search/Collector;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 273
    iput-object p1, p0, Lorg/apache/lucene/search/MultiSearcher$1;->this$0:Lorg/apache/lucene/search/MultiSearcher;

    iput-object p2, p0, Lorg/apache/lucene/search/MultiSearcher$1;->val$collector:Lorg/apache/lucene/search/Collector;

    iput p3, p0, Lorg/apache/lucene/search/MultiSearcher$1;->val$start:I

    invoke-direct {p0}, Lorg/apache/lucene/search/Collector;-><init>()V

    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lorg/apache/lucene/search/MultiSearcher$1;->val$collector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Collector;->acceptsDocsOutOfOrder()Z

    move-result v0

    return v0
.end method

.method public collect(I)V
    .locals 1
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 266
    iget-object v0, p0, Lorg/apache/lucene/search/MultiSearcher$1;->val$collector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 267
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 270
    iget-object v0, p0, Lorg/apache/lucene/search/MultiSearcher$1;->val$collector:Lorg/apache/lucene/search/Collector;

    iget v1, p0, Lorg/apache/lucene/search/MultiSearcher$1;->val$start:I

    add-int/2addr v1, p2

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/search/Collector;->setNextReader(Lorg/apache/lucene/index/IndexReader;I)V

    .line 271
    return-void
.end method

.method public setScorer(Lorg/apache/lucene/search/Scorer;)V
    .locals 1
    .param p1, "scorer"    # Lorg/apache/lucene/search/Scorer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 262
    iget-object v0, p0, Lorg/apache/lucene/search/MultiSearcher$1;->val$collector:Lorg/apache/lucene/search/Collector;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 263
    return-void
.end method
