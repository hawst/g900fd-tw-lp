.class final Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;
.super Ljava/lang/Object;
.source "MultiSearcher.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/MultiSearcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "MultiSearcherCallableWithSort"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lorg/apache/lucene/search/TopFieldDocs;",
        ">;"
    }
.end annotation


# instance fields
.field private final filter:Lorg/apache/lucene/search/Filter;

.field private final hq:Lorg/apache/lucene/search/FieldDocSortedHitQueue;

.field private final i:I

.field private final lock:Ljava/util/concurrent/locks/Lock;

.field private final nDocs:I

.field private final searchable:Lorg/apache/lucene/search/Searchable;

.field private final sort:Lorg/apache/lucene/search/Sort;

.field private final starts:[I

.field private final weight:Lorg/apache/lucene/search/Weight;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/locks/Lock;Lorg/apache/lucene/search/Searchable;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/FieldDocSortedHitQueue;Lorg/apache/lucene/search/Sort;I[I)V
    .locals 0
    .param p1, "lock"    # Ljava/util/concurrent/locks/Lock;
    .param p2, "searchable"    # Lorg/apache/lucene/search/Searchable;
    .param p3, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p4, "filter"    # Lorg/apache/lucene/search/Filter;
    .param p5, "nDocs"    # I
    .param p6, "hq"    # Lorg/apache/lucene/search/FieldDocSortedHitQueue;
    .param p7, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p8, "i"    # I
    .param p9, "starts"    # [I

    .prologue
    .line 415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 416
    iput-object p1, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    .line 417
    iput-object p2, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->searchable:Lorg/apache/lucene/search/Searchable;

    .line 418
    iput-object p3, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->weight:Lorg/apache/lucene/search/Weight;

    .line 419
    iput-object p4, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->filter:Lorg/apache/lucene/search/Filter;

    .line 420
    iput p5, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->nDocs:I

    .line 421
    iput-object p6, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->hq:Lorg/apache/lucene/search/FieldDocSortedHitQueue;

    .line 422
    iput p8, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->i:I

    .line 423
    iput-object p9, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->starts:[I

    .line 424
    iput-object p7, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->sort:Lorg/apache/lucene/search/Sort;

    .line 425
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 402
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->call()Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    return-object v0
.end method

.method public call()Lorg/apache/lucene/search/TopFieldDocs;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 428
    iget-object v6, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->searchable:Lorg/apache/lucene/search/Searchable;

    iget-object v7, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->weight:Lorg/apache/lucene/search/Weight;

    iget-object v8, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->filter:Lorg/apache/lucene/search/Filter;

    iget v9, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->nDocs:I

    iget-object v10, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->sort:Lorg/apache/lucene/search/Sort;

    invoke-interface {v6, v7, v8, v9, v10}, Lorg/apache/lucene/search/Searchable;->search(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;ILorg/apache/lucene/search/Sort;)Lorg/apache/lucene/search/TopFieldDocs;

    move-result-object v0

    .line 432
    .local v0, "docs":Lorg/apache/lucene/search/TopFieldDocs;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_0
    iget-object v6, v0, Lorg/apache/lucene/search/TopFieldDocs;->fields:[Lorg/apache/lucene/search/SortField;

    array-length v6, v6

    if-ge v3, v6, :cond_1

    .line 433
    iget-object v6, v0, Lorg/apache/lucene/search/TopFieldDocs;->fields:[Lorg/apache/lucene/search/SortField;

    aget-object v6, v6, v3

    invoke-virtual {v6}, Lorg/apache/lucene/search/SortField;->getType()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 435
    const/4 v4, 0x0

    .local v4, "j2":I
    :goto_1
    iget-object v6, v0, Lorg/apache/lucene/search/TopFieldDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    array-length v6, v6

    if-ge v4, v6, :cond_1

    .line 436
    iget-object v6, v0, Lorg/apache/lucene/search/TopFieldDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    aget-object v1, v6, v4

    check-cast v1, Lorg/apache/lucene/search/FieldDoc;

    .line 437
    .local v1, "fd":Lorg/apache/lucene/search/FieldDoc;
    iget-object v7, v1, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    iget-object v6, v1, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    aget-object v6, v6, v3

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v8, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->starts:[I

    iget v9, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->i:I

    aget v8, v8, v9

    add-int/2addr v6, v8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v7, v3

    .line 435
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 432
    .end local v1    # "fd":Lorg/apache/lucene/search/FieldDoc;
    .end local v4    # "j2":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 443
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v6}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 445
    :try_start_0
    iget-object v6, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->hq:Lorg/apache/lucene/search/FieldDocSortedHitQueue;

    iget-object v7, v0, Lorg/apache/lucene/search/TopFieldDocs;->fields:[Lorg/apache/lucene/search/SortField;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->setFields([Lorg/apache/lucene/search/SortField;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 447
    iget-object v6, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v6}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 450
    iget-object v5, v0, Lorg/apache/lucene/search/TopFieldDocs;->scoreDocs:[Lorg/apache/lucene/search/ScoreDoc;

    .line 451
    .local v5, "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    const/4 v3, 0x0

    :goto_2
    array-length v6, v5

    if-ge v3, v6, :cond_2

    .line 452
    aget-object v2, v5, v3

    check-cast v2, Lorg/apache/lucene/search/FieldDoc;

    .line 453
    .local v2, "fieldDoc":Lorg/apache/lucene/search/FieldDoc;
    iget v6, v2, Lorg/apache/lucene/search/FieldDoc;->doc:I

    iget-object v7, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->starts:[I

    iget v8, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->i:I

    aget v7, v7, v8

    add-int/2addr v6, v7

    iput v6, v2, Lorg/apache/lucene/search/FieldDoc;->doc:I

    .line 455
    iget-object v6, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v6}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 457
    :try_start_1
    iget-object v6, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->hq:Lorg/apache/lucene/search/FieldDocSortedHitQueue;

    invoke-virtual {v6, v2}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->insertWithOverflow(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    if-ne v2, v6, :cond_3

    .line 460
    iget-object v6, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v6}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 463
    .end local v2    # "fieldDoc":Lorg/apache/lucene/search/FieldDoc;
    :cond_2
    return-object v0

    .line 447
    .end local v5    # "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    :catchall_0
    move-exception v6

    iget-object v7, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v7}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v6

    .line 460
    .restart local v2    # "fieldDoc":Lorg/apache/lucene/search/FieldDoc;
    .restart local v5    # "scoreDocs":[Lorg/apache/lucene/search/ScoreDoc;
    :catchall_1
    move-exception v6

    iget-object v7, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v7}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v6

    :cond_3
    iget-object v6, p0, Lorg/apache/lucene/search/MultiSearcher$MultiSearcherCallableWithSort;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v6}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 451
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method
