.class public Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;
.super Ljava/lang/Object;
.source "NRTManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/NRTManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TrackingIndexWriter"
.end annotation


# instance fields
.field private final indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

.field private final writer:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 4
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    .line 154
    iput-object p1, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 155
    return-void
.end method


# virtual methods
.method public addDocument(Lorg/apache/lucene/document/Document;)J
    .locals 2
    .param p1, "d"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->addDocument(Lorg/apache/lucene/document/Document;)V

    .line 226
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public addDocument(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/analysis/Analyzer;)J
    .locals 2
    .param p1, "d"    # Lorg/apache/lucene/document/Document;
    .param p2, "a"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 212
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->addDocument(Lorg/apache/lucene/document/Document;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 214
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public addDocuments(Ljava/util/Collection;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;)J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 230
    .local p1, "docs":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/document/Document;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->addDocuments(Ljava/util/Collection;)V

    .line 232
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public addDocuments(Ljava/util/Collection;Lorg/apache/lucene/analysis/Analyzer;)J
    .locals 2
    .param p2, "a"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ")J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 218
    .local p1, "docs":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/document/Document;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->addDocuments(Ljava/util/Collection;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 220
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public varargs addIndexes([Lorg/apache/lucene/index/IndexReader;)J
    .locals 2
    .param p1, "readers"    # [Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->addIndexes([Lorg/apache/lucene/index/IndexReader;)V

    .line 244
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public varargs addIndexes([Lorg/apache/lucene/store/Directory;)J
    .locals 2
    .param p1, "dirs"    # [Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 236
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->addIndexes([Lorg/apache/lucene/store/Directory;)V

    .line 238
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public deleteAll()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->deleteAll()V

    .line 208
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public deleteDocuments(Lorg/apache/lucene/index/Term;)J
    .locals 2
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->deleteDocuments(Lorg/apache/lucene/index/Term;)V

    .line 184
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public deleteDocuments(Lorg/apache/lucene/search/Query;)J
    .locals 2
    .param p1, "q"    # Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->deleteDocuments(Lorg/apache/lucene/search/Query;)V

    .line 196
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public varargs deleteDocuments([Lorg/apache/lucene/index/Term;)J
    .locals 2
    .param p1, "terms"    # [Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->deleteDocuments([Lorg/apache/lucene/index/Term;)V

    .line 190
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public varargs deleteDocuments([Lorg/apache/lucene/search/Query;)J
    .locals 2
    .param p1, "queries"    # [Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexWriter;->deleteDocuments([Lorg/apache/lucene/search/Query;)V

    .line 202
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method getAndIncrementGeneration()J
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    return-wide v0
.end method

.method public getGeneration()J
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public getIndexWriter()Lorg/apache/lucene/index/IndexWriter;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    return-object v0
.end method

.method public updateDocument(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/document/Document;)J
    .locals 2
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .param p2, "d"    # Lorg/apache/lucene/document/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->updateDocument(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/document/Document;)V

    .line 166
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public updateDocument(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/document/Document;Lorg/apache/lucene/analysis/Analyzer;)J
    .locals 2
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .param p2, "d"    # Lorg/apache/lucene/document/Document;
    .param p3, "a"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/IndexWriter;->updateDocument(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/document/Document;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 160
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public updateDocuments(Lorg/apache/lucene/index/Term;Ljava/util/Collection;)J
    .locals 2
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;)J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    .local p2, "docs":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/document/Document;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/IndexWriter;->updateDocuments(Lorg/apache/lucene/index/Term;Ljava/util/Collection;)V

    .line 178
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public updateDocuments(Lorg/apache/lucene/index/Term;Ljava/util/Collection;Lorg/apache/lucene/analysis/Analyzer;)J
    .locals 2
    .param p1, "t"    # Lorg/apache/lucene/index/Term;
    .param p3, "a"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/document/Document;",
            ">;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ")J"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    .local p2, "docs":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/document/Document;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/IndexWriter;->updateDocuments(Lorg/apache/lucene/index/Term;Ljava/util/Collection;Lorg/apache/lucene/analysis/Analyzer;)V

    .line 172
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->indexingGen:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method
