.class public Lorg/apache/lucene/search/spans/SpanTermQuery;
.super Lorg/apache/lucene/search/spans/SpanQuery;
.source "SpanTermQuery.java"


# instance fields
.field protected term:Lorg/apache/lucene/index/Term;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 0
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanQuery;-><init>()V

    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    if-ne p0, p1, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v1

    .line 68
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 69
    goto :goto_0

    .line 70
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 71
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 72
    check-cast v0, Lorg/apache/lucene/search/spans/SpanTermQuery;

    .line 73
    .local v0, "other":Lorg/apache/lucene/search/spans/SpanTermQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v3, :cond_4

    .line 74
    iget-object v3, v0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    if-eqz v3, :cond_0

    move v1, v2

    .line 75
    goto :goto_0

    .line 76
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    iget-object v4, v0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 77
    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    return-void
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    new-instance v0, Lorg/apache/lucene/search/spans/TermSpans;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/IndexReader;->termPositions(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermPositions;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/spans/TermSpans;-><init>(Lorg/apache/lucene/index/TermPositions;Lorg/apache/lucene/index/Term;)V

    return-object v0
.end method

.method public getTerm()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 58
    const/16 v0, 0x1f

    .line 59
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/spans/SpanQuery;->hashCode()I

    move-result v1

    .line 60
    .local v1, "result":I
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v3, v2

    .line 61
    return v1

    .line 60
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 48
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanTermQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 51
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanTermQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
