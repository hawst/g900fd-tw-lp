.class Lorg/apache/lucene/search/ReqExclScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "ReqExclScorer.java"


# instance fields
.field private doc:I

.field private exclDisi:Lorg/apache/lucene/search/DocIdSetIterator;

.field private reqScorer:Lorg/apache/lucene/search/Scorer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Scorer;Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 1
    .param p1, "reqScorer"    # Lorg/apache/lucene/search/Scorer;
    .param p2, "exclDisi"    # Lorg/apache/lucene/search/DocIdSetIterator;

    .prologue
    .line 39
    iget-object v0, p1, Lorg/apache/lucene/search/Scorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    .line 40
    iput-object p1, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    .line 41
    iput-object p2, p0, Lorg/apache/lucene/search/ReqExclScorer;->exclDisi:Lorg/apache/lucene/search/DocIdSetIterator;

    .line 42
    return-void
.end method

.method private toNonExcluded()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const v2, 0x7fffffff

    .line 72
    iget-object v3, p0, Lorg/apache/lucene/search/ReqExclScorer;->exclDisi:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v3}, Lorg/apache/lucene/search/DocIdSetIterator;->docID()I

    move-result v0

    .line 73
    .local v0, "exclDoc":I
    iget-object v3, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v1

    .line 75
    .local v1, "reqDoc":I
    :cond_0
    if-ge v1, v0, :cond_1

    move v2, v1

    .line 89
    :goto_0
    return v2

    .line 77
    :cond_1
    if-le v1, v0, :cond_3

    .line 78
    iget-object v3, p0, Lorg/apache/lucene/search/ReqExclScorer;->exclDisi:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v3, v1}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v0

    .line 79
    if-ne v0, v2, :cond_2

    .line 80
    iput-object v4, p0, Lorg/apache/lucene/search/ReqExclScorer;->exclDisi:Lorg/apache/lucene/search/DocIdSetIterator;

    move v2, v1

    .line 81
    goto :goto_0

    .line 83
    :cond_2
    if-le v0, v1, :cond_3

    move v2, v1

    .line 84
    goto :goto_0

    .line 87
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v3}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 88
    iput-object v4, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    goto :goto_0
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v0, 0x7fffffff

    .line 108
    iget-object v1, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    if-nez v1, :cond_0

    .line 109
    iput v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    .line 118
    :goto_0
    return v0

    .line 111
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/ReqExclScorer;->exclDisi:Lorg/apache/lucene/search/DocIdSetIterator;

    if-nez v1, :cond_1

    .line 112
    iget-object v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    goto :goto_0

    .line 114
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Scorer;->advance(I)I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 115
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    .line 116
    iput v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    goto :goto_0

    .line 118
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/search/ReqExclScorer;->toNonExcluded()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    return v0
.end method

.method public nextDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    if-nez v0, :cond_0

    .line 47
    iget v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    .line 57
    :goto_0
    return v0

    .line 49
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    .line 50
    iget v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    .line 52
    iget v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    goto :goto_0

    .line 54
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->exclDisi:Lorg/apache/lucene/search/DocIdSetIterator;

    if-nez v0, :cond_2

    .line 55
    iget v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    goto :goto_0

    .line 57
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/search/ReqExclScorer;->toNonExcluded()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->doc:I

    goto :goto_0
.end method

.method public score()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/lucene/search/ReqExclScorer;->reqScorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Scorer;->score()F

    move-result v0

    return v0
.end method
