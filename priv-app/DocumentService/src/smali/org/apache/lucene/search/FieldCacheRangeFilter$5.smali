.class Lorg/apache/lucene/search/FieldCacheRangeFilter$5;
.super Lorg/apache/lucene/search/FieldCacheRangeFilter;
.source "FieldCacheRangeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheRangeFilter;->newLongRange(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;Ljava/lang/Long;Ljava/lang/Long;ZZ)Lorg/apache/lucene/search/FieldCacheRangeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldCacheRangeFilter",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Long;Ljava/lang/Long;ZZ)V
    .locals 8
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p3, "x2"    # Ljava/lang/Long;
    .param p4, "x3"    # Ljava/lang/Long;
    .param p5, "x4"    # Z
    .param p6, "x5"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 298
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter;-><init>(Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Object;Ljava/lang/Object;ZZLorg/apache/lucene/search/FieldCacheRangeFilter$1;)V

    return-void
.end method


# virtual methods
.method public getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 12
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x1

    .line 300
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->lowerVal:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 301
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->lowerVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 302
    .local v8, "i":J
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->includeLower:Z

    if-nez v0, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, v8, v0

    if-nez v0, :cond_0

    .line 303
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    .line 321
    .end local v8    # "i":J
    :goto_0
    return-object v0

    .line 304
    .restart local v8    # "i":J
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->includeLower:Z

    if-eqz v0, :cond_1

    move-wide v4, v8

    .line 308
    .end local v8    # "i":J
    .local v4, "inclusiveLowerPoint":J
    :goto_1
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->upperVal:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 309
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->upperVal:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 310
    .restart local v8    # "i":J
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->includeUpper:Z

    if-nez v0, :cond_3

    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, v8, v0

    if-nez v0, :cond_3

    .line 311
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 304
    .end local v4    # "inclusiveLowerPoint":J
    :cond_1
    add-long v4, v8, v10

    goto :goto_1

    .line 306
    .end local v8    # "i":J
    :cond_2
    const-wide/high16 v4, -0x8000000000000000L

    .restart local v4    # "inclusiveLowerPoint":J
    goto :goto_1

    .line 312
    .restart local v8    # "i":J
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->includeUpper:Z

    if-eqz v0, :cond_4

    move-wide v6, v8

    .line 317
    .end local v8    # "i":J
    .local v6, "inclusiveUpperPoint":J
    :goto_2
    cmp-long v0, v4, v6

    if-lez v0, :cond_6

    .line 318
    sget-object v0, Lorg/apache/lucene/search/DocIdSet;->EMPTY_DOCIDSET:Lorg/apache/lucene/search/DocIdSet;

    goto :goto_0

    .line 312
    .end local v6    # "inclusiveUpperPoint":J
    .restart local v8    # "i":J
    :cond_4
    sub-long v6, v8, v10

    goto :goto_2

    .line 314
    .end local v8    # "i":J
    :cond_5
    const-wide v6, 0x7fffffffffffffffL

    .restart local v6    # "inclusiveUpperPoint":J
    goto :goto_2

    .line 320
    :cond_6
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->field:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5;->parser:Lorg/apache/lucene/search/FieldCache$Parser;

    check-cast v0, Lorg/apache/lucene/search/FieldCache$LongParser;

    invoke-interface {v1, p1, v2, v0}, Lorg/apache/lucene/search/FieldCache;->getLongs(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$LongParser;)[J

    move-result-object v3

    .line 321
    .local v3, "values":[J
    new-instance v0, Lorg/apache/lucene/search/FieldCacheRangeFilter$5$1;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/search/FieldCacheRangeFilter$5$1;-><init>(Lorg/apache/lucene/search/FieldCacheRangeFilter$5;Lorg/apache/lucene/index/IndexReader;[JJJ)V

    goto :goto_0
.end method
