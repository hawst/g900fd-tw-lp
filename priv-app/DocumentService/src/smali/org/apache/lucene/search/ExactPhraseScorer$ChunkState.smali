.class final Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;
.super Ljava/lang/Object;
.source "ExactPhraseScorer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/ExactPhraseScorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ChunkState"
.end annotation


# instance fields
.field lastPos:I

.field final offset:I

.field pos:I

.field final posEnum:Lorg/apache/lucene/index/TermPositions;

.field posLimit:I

.field posUpto:I

.field final useAdvance:Z


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/TermPositions;IZ)V
    .locals 0
    .param p1, "posEnum"    # Lorg/apache/lucene/index/TermPositions;
    .param p2, "offset"    # I
    .param p3, "useAdvance"    # Z

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    .line 53
    iput p2, p0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->offset:I

    .line 54
    iput-boolean p3, p0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->useAdvance:Z

    .line 55
    return-void
.end method
