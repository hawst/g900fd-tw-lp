.class public Lorg/apache/lucene/search/spans/SpanNearQuery;
.super Lorg/apache/lucene/search/spans/SpanQuery;
.source "SpanNearQuery.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field protected clauses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/spans/SpanQuery;",
            ">;"
        }
    .end annotation
.end field

.field private collectPayloads:Z

.field protected field:Ljava/lang/String;

.field protected inOrder:Z

.field protected slop:I


# direct methods
.method public constructor <init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V
    .locals 1
    .param p1, "clauses"    # [Lorg/apache/lucene/search/spans/SpanQuery;
    .param p2, "slop"    # I
    .param p3, "inOrder"    # Z

    .prologue
    .line 54
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/search/spans/SpanNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZZ)V

    .line 55
    return-void
.end method

.method public constructor <init>([Lorg/apache/lucene/search/spans/SpanQuery;IZZ)V
    .locals 4
    .param p1, "clauses"    # [Lorg/apache/lucene/search/spans/SpanQuery;
    .param p2, "slop"    # I
    .param p3, "inOrder"    # Z
    .param p4, "collectPayloads"    # Z

    .prologue
    .line 57
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/SpanQuery;-><init>()V

    .line 60
    new-instance v2, Ljava/util/ArrayList;

    array-length v3, p1

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    .line 61
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_2

    .line 62
    aget-object v0, p1, v1

    .line 63
    .local v0, "clause":Lorg/apache/lucene/search/spans/SpanQuery;
    if-nez v1, :cond_1

    .line 64
    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->field:Ljava/lang/String;

    .line 68
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 65
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanQuery;->getField()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->field:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 66
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Clauses must have same field."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 70
    .end local v0    # "clause":Lorg/apache/lucene/search/spans/SpanQuery;
    :cond_2
    iput-boolean p4, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->collectPayloads:Z

    .line 71
    iput p2, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->slop:I

    .line 72
    iput-boolean p3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    .line 73
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 152
    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 153
    .local v3, "sz":I
    new-array v1, v3, [Lorg/apache/lucene/search/spans/SpanQuery;

    .line 155
    .local v1, "newClauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 156
    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/SpanQuery;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/spans/SpanQuery;

    aput-object v4, v1, v0

    .line 155
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 158
    :cond_0
    new-instance v2, Lorg/apache/lucene/search/spans/SpanNearQuery;

    iget v4, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->slop:I

    iget-boolean v5, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    invoke-direct {v2, v1, v4, v5}, Lorg/apache/lucene/search/spans/SpanNearQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;IZ)V

    .line 159
    .local v2, "spanNearQuery":Lorg/apache/lucene/search/spans/SpanNearQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getBoost()F

    move-result v4

    invoke-virtual {v2, v4}, Lorg/apache/lucene/search/spans/SpanNearQuery;->setBoost(F)V

    .line 160
    return-object v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 166
    if-ne p0, p1, :cond_1

    move v2, v1

    .line 175
    :cond_0
    :goto_0
    return v2

    .line 167
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/search/spans/SpanNearQuery;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 169
    check-cast v0, Lorg/apache/lucene/search/spans/SpanNearQuery;

    .line 171
    .local v0, "spanNearQuery":Lorg/apache/lucene/search/spans/SpanNearQuery;
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    if-ne v3, v4, :cond_0

    .line 172
    iget v3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->slop:I

    iget v4, v0, Lorg/apache/lucene/search/spans/SpanNearQuery;->slop:I

    if-ne v3, v4, :cond_0

    .line 173
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    iget-object v4, v0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 175
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getBoost()F

    move-result v3

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getBoost()F

    move-result v4

    cmpl-float v3, v3, v4

    if-nez v3, :cond_2

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v2, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 92
    .local v0, "clause":Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->extractTerms(Ljava/util/Set;)V

    goto :goto_0

    .line 94
    .end local v0    # "clause":Lorg/apache/lucene/search/spans/SpanQuery;
    :cond_0
    return-void
.end method

.method public getClauses()[Lorg/apache/lucene/search/spans/SpanQuery;
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/search/spans/SpanQuery;

    return-object v0
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getSlop()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->slop:I

    return v0
.end method

.method public getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 121
    new-instance v0, Lorg/apache/lucene/search/spans/SpanOrQuery;

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getClauses()[Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/spans/SpanOrQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;)V

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/SpanOrQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    .line 126
    :goto_0
    return-object v0

    .line 123
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 124
    iget-object v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v0

    goto :goto_0

    .line 126
    :cond_1
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    if-eqz v0, :cond_2

    new-instance v0, Lorg/apache/lucene/search/spans/NearSpansOrdered;

    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->collectPayloads:Z

    invoke-direct {v0, p0, p1, v1}, Lorg/apache/lucene/search/spans/NearSpansOrdered;-><init>(Lorg/apache/lucene/search/spans/SpanNearQuery;Lorg/apache/lucene/index/IndexReader;Z)V

    goto :goto_0

    :cond_2
    new-instance v0, Lorg/apache/lucene/search/spans/NearSpansUnordered;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;-><init>(Lorg/apache/lucene/search/spans/SpanNearQuery;Lorg/apache/lucene/index/IndexReader;)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 181
    iget-object v1, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 185
    .local v0, "result":I
    shl-int/lit8 v1, v0, 0xe

    ushr-int/lit8 v2, v0, 0x13

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    .line 186
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    iget v1, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->slop:I

    add-int/2addr v0, v1

    .line 188
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    if-eqz v1, :cond_0

    const v1, -0x66502c43

    :goto_0
    xor-int/2addr v0, v1

    .line 189
    return v0

    .line 188
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isInOrder()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 5
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    const/4 v1, 0x0

    .line 134
    .local v1, "clone":Lorg/apache/lucene/search/spans/SpanNearQuery;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 135
    iget-object v4, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 136
    .local v0, "c":Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 137
    .local v3, "query":Lorg/apache/lucene/search/spans/SpanQuery;
    if-eq v3, v0, :cond_1

    .line 138
    if-nez v1, :cond_0

    .line 139
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->clone()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "clone":Lorg/apache/lucene/search/spans/SpanNearQuery;
    check-cast v1, Lorg/apache/lucene/search/spans/SpanNearQuery;

    .line 140
    .restart local v1    # "clone":Lorg/apache/lucene/search/spans/SpanNearQuery;
    :cond_0
    iget-object v4, v1, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v4, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 134
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 143
    .end local v0    # "c":Lorg/apache/lucene/search/spans/SpanQuery;
    .end local v3    # "query":Lorg/apache/lucene/search/spans/SpanQuery;
    :cond_2
    if-eqz v1, :cond_3

    .line 146
    .end local v1    # "clone":Lorg/apache/lucene/search/spans/SpanNearQuery;
    :goto_1
    return-object v1

    .restart local v1    # "clone":Lorg/apache/lucene/search/spans/SpanNearQuery;
    :cond_3
    move-object v1, p0

    goto :goto_1
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "spanNear(["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    iget-object v3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->clauses:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 102
    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/search/spans/SpanQuery;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 103
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/spans/SpanQuery;

    .line 104
    .local v1, "clause":Lorg/apache/lucene/search/spans/SpanQuery;
    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/spans/SpanQuery;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 106
    const-string/jumbo v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 109
    .end local v1    # "clause":Lorg/apache/lucene/search/spans/SpanQuery;
    :cond_1
    const-string/jumbo v3, "], "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    iget v3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->slop:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 111
    const-string/jumbo v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/SpanNearQuery;->inOrder:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 113
    const-string/jumbo v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getBoost()F

    move-result v3

    invoke-static {v3}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
