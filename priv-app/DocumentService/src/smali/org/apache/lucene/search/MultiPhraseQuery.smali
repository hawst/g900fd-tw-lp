.class public Lorg/apache/lucene/search/MultiPhraseQuery;
.super Lorg/apache/lucene/search/Query;
.source "MultiPhraseQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;
    }
.end annotation


# instance fields
.field private field:Ljava/lang/String;

.field private positions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private slop:I

.field private termArrays:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    .line 128
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/MultiPhraseQuery;

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/MultiPhraseQuery;

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lorg/apache/lucene/search/MultiPhraseQuery;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/MultiPhraseQuery;

    .prologue
    .line 41
    iget v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    return v0
.end method

.method static synthetic access$300(Lorg/apache/lucene/search/MultiPhraseQuery;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/MultiPhraseQuery;

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;

    return-object v0
.end method

.method private termArraysEquals(Ljava/util/List;Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[",
            "Lorg/apache/lucene/index/Term;",
            ">;",
            "Ljava/util/List",
            "<[",
            "Lorg/apache/lucene/index/Term;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "termArrays1":Ljava/util/List;, "Ljava/util/List<[Lorg/apache/lucene/index/Term;>;"
    .local p2, "termArrays2":Ljava/util/List;, "Ljava/util/List<[Lorg/apache/lucene/index/Term;>;"
    const/4 v4, 0x0

    .line 401
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    if-eq v5, v6, :cond_1

    .line 414
    :cond_0
    :goto_0
    return v4

    .line 404
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 405
    .local v0, "iterator1":Ljava/util/ListIterator;, "Ljava/util/ListIterator<[Lorg/apache/lucene/index/Term;>;"
    invoke-interface {p2}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 406
    .local v1, "iterator2":Ljava/util/ListIterator;, "Ljava/util/ListIterator<[Lorg/apache/lucene/index/Term;>;"
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 407
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/apache/lucene/index/Term;

    .line 408
    .local v2, "termArray1":[Lorg/apache/lucene/index/Term;
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/index/Term;

    .line 409
    .local v3, "termArray2":[Lorg/apache/lucene/index/Term;
    if-nez v2, :cond_3

    if-nez v3, :cond_0

    goto :goto_1

    :cond_3
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    goto :goto_0

    .line 414
    .end local v2    # "termArray1":[Lorg/apache/lucene/index/Term;
    .end local v3    # "termArray2":[Lorg/apache/lucene/index/Term;
    :cond_4
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private termArraysHashCode()I
    .locals 5

    .prologue
    .line 391
    const/4 v0, 0x1

    .line 392
    .local v0, "hashCode":I
    iget-object v3, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/apache/lucene/index/Term;

    .line 393
    .local v2, "termArray":[Lorg/apache/lucene/index/Term;
    mul-int/lit8 v4, v0, 0x1f

    if-nez v2, :cond_0

    const/4 v3, 0x0

    :goto_1
    add-int v0, v4, v3

    goto :goto_0

    :cond_0
    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    goto :goto_1

    .line 396
    .end local v2    # "termArray":[Lorg/apache/lucene/index/Term;
    :cond_1
    return v0
.end method


# virtual methods
.method public add(Lorg/apache/lucene/index/Term;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 61
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/lucene/index/Term;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;)V

    return-void
.end method

.method public add([Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "terms"    # [Lorg/apache/lucene/index/Term;

    .prologue
    .line 69
    const/4 v0, 0x0

    .line 70
    .local v0, "position":I
    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 71
    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 73
    :cond_0
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->add([Lorg/apache/lucene/index/Term;I)V

    .line 74
    return-void
.end method

.method public add([Lorg/apache/lucene/index/Term;I)V
    .locals 4
    .param p1, "terms"    # [Lorg/apache/lucene/index/Term;
    .param p2, "position"    # I

    .prologue
    .line 84
    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 85
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;

    .line 87
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 88
    aget-object v1, p1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;

    if-eq v1, v2, :cond_1

    .line 89
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "All phrase terms must be in the same field ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 87
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    return-void
.end method

.method public createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;
    .locals 1
    .param p1, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 316
    new-instance v0, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/search/MultiPhraseQuery$MultiPhraseWeight;-><init>(Lorg/apache/lucene/search/MultiPhraseQuery;Lorg/apache/lucene/search/Searcher;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 371
    instance-of v2, p1, Lorg/apache/lucene/search/MultiPhraseQuery;

    if-nez v2, :cond_1

    .line 373
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 372
    check-cast v0, Lorg/apache/lucene/search/MultiPhraseQuery;

    .line 373
    .local v0, "other":Lorg/apache/lucene/search/MultiPhraseQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    iget v3, v0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    iget-object v3, v0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/search/MultiPhraseQuery;->termArraysEquals(Ljava/util/List;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    iget-object v3, v0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v6, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/index/Term;

    .line 121
    .local v0, "arr":[Lorg/apache/lucene/index/Term;
    move-object v1, v0

    .local v1, "arr$":[Lorg/apache/lucene/index/Term;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v1, v3

    .line 122
    .local v5, "term":Lorg/apache/lucene/index/Term;
    invoke-interface {p1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 121
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 125
    .end local v0    # "arr":[Lorg/apache/lucene/index/Term;
    .end local v1    # "arr$":[Lorg/apache/lucene/index/Term;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "term":Lorg/apache/lucene/index/Term;
    :cond_1
    return-void
.end method

.method public getPositions()[I
    .locals 3

    .prologue
    .line 111
    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v1, v2, [I

    .line 112
    .local v1, "result":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 113
    iget-object v2, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v1, v0

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    :cond_0
    return-object v1
.end method

.method public getSlop()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    return v0
.end method

.method public getTermArrays()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<[",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 382
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    xor-int/2addr v0, v1

    invoke-direct {p0}, Lorg/apache/lucene/search/MultiPhraseQuery;->termArraysHashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    const v1, 0x4ac65113    # 6498441.5f

    xor-int/2addr v0, v1

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    const/4 v5, 0x1

    .line 301
    iget-object v3, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v5, :cond_1

    .line 302
    iget-object v3, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/apache/lucene/index/Term;

    .line 303
    .local v2, "terms":[Lorg/apache/lucene/index/Term;
    new-instance v0, Lorg/apache/lucene/search/BooleanQuery;

    invoke-direct {v0, v5}, Lorg/apache/lucene/search/BooleanQuery;-><init>(Z)V

    .line 304
    .local v0, "boq":Lorg/apache/lucene/search/BooleanQuery;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 305
    new-instance v3, Lorg/apache/lucene/search/TermQuery;

    aget-object v4, v2, v1

    invoke-direct {v3, v4}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    sget-object v4, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0, v3, v4}, Lorg/apache/lucene/search/BooleanQuery;->add(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;)V

    .line 304
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 307
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v3

    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/BooleanQuery;->setBoost(F)V

    .line 310
    .end local v0    # "boq":Lorg/apache/lucene/search/BooleanQuery;
    .end local v1    # "i":I
    .end local v2    # "terms":[Lorg/apache/lucene/index/Term;
    :goto_1
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_1
.end method

.method public setSlop(I)V
    .locals 0
    .param p1, "s"    # I

    .prologue
    .line 51
    iput p1, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    return-void
.end method

.method public final toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "f"    # Ljava/lang/String;

    .prologue
    .line 322
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 323
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 324
    :cond_0
    iget-object v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    const-string/jumbo v7, ":"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    :cond_1
    const-string/jumbo v7, "\""

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    const/4 v4, -0x1

    .line 330
    .local v4, "lastPos":I
    const/4 v1, 0x1

    .line 331
    .local v1, "first":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_7

    .line 332
    iget-object v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->termArrays:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lorg/apache/lucene/index/Term;

    .line 333
    .local v6, "terms":[Lorg/apache/lucene/index/Term;
    iget-object v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 334
    .local v5, "position":I
    if-eqz v1, :cond_4

    .line 335
    const/4 v1, 0x0

    .line 342
    :cond_2
    array-length v7, v6

    const/4 v8, 0x1

    if-le v7, v8, :cond_6

    .line 343
    const-string/jumbo v7, "("

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v7, v6

    if-ge v3, v7, :cond_5

    .line 345
    aget-object v7, v6, v3

    invoke-virtual {v7}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    array-length v7, v6

    add-int/lit8 v7, v7, -0x1

    if-ge v3, v7, :cond_3

    .line 347
    const-string/jumbo v7, " "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 337
    .end local v3    # "j":I
    :cond_4
    const-string/jumbo v7, " "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    const/4 v3, 0x1

    .restart local v3    # "j":I
    :goto_2
    sub-int v7, v5, v4

    if-ge v3, v7, :cond_2

    .line 339
    const-string/jumbo v7, "? "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 349
    :cond_5
    const-string/jumbo v7, ")"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    .end local v3    # "j":I
    :goto_3
    move v4, v5

    .line 331
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 351
    :cond_6
    const/4 v7, 0x0

    aget-object v7, v6, v7

    invoke-virtual {v7}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 355
    .end local v5    # "position":I
    .end local v6    # "terms":[Lorg/apache/lucene/index/Term;
    :cond_7
    const-string/jumbo v7, "\""

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 357
    iget v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    if-eqz v7, :cond_8

    .line 358
    const-string/jumbo v7, "~"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    iget v7, p0, Lorg/apache/lucene/search/MultiPhraseQuery;->slop:I

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 362
    :cond_8
    invoke-virtual {p0}, Lorg/apache/lucene/search/MultiPhraseQuery;->getBoost()F

    move-result v7

    invoke-static {v7}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method
