.class Lorg/apache/lucene/search/BooleanScorer2$1;
.super Lorg/apache/lucene/search/DisjunctionSumScorer;
.source "BooleanScorer2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/BooleanScorer2;->countingDisjunctionSumScorer(Ljava/util/List;I)Lorg/apache/lucene/search/Scorer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private lastDocScore:F

.field private lastScoredDoc:I

.field final synthetic this$0:Lorg/apache/lucene/search/BooleanScorer2;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/BooleanScorer2;Lorg/apache/lucene/search/Weight;Ljava/util/List;I)V
    .locals 1
    .param p2, "x0"    # Lorg/apache/lucene/search/Weight;
    .param p4, "x2"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 154
    .local p3, "x1":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    iput-object p1, p0, Lorg/apache/lucene/search/BooleanScorer2$1;->this$0:Lorg/apache/lucene/search/BooleanScorer2;

    invoke-direct {p0, p2, p3, p4}, Lorg/apache/lucene/search/DisjunctionSumScorer;-><init>(Lorg/apache/lucene/search/Weight;Ljava/util/List;I)V

    .line 150
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2$1;->lastScoredDoc:I

    .line 153
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2$1;->lastDocScore:F

    return-void
.end method


# virtual methods
.method public score()F
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    invoke-virtual {p0}, Lorg/apache/lucene/search/BooleanScorer2$1;->docID()I

    move-result v0

    .line 156
    .local v0, "doc":I
    iget v1, p0, Lorg/apache/lucene/search/BooleanScorer2$1;->lastScoredDoc:I

    if-lt v0, v1, :cond_1

    .line 157
    iget v1, p0, Lorg/apache/lucene/search/BooleanScorer2$1;->lastScoredDoc:I

    if-le v0, v1, :cond_0

    .line 158
    invoke-super {p0}, Lorg/apache/lucene/search/DisjunctionSumScorer;->score()F

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/BooleanScorer2$1;->lastDocScore:F

    .line 159
    iput v0, p0, Lorg/apache/lucene/search/BooleanScorer2$1;->lastScoredDoc:I

    .line 161
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer2$1;->this$0:Lorg/apache/lucene/search/BooleanScorer2;

    # getter for: Lorg/apache/lucene/search/BooleanScorer2;->coordinator:Lorg/apache/lucene/search/BooleanScorer2$Coordinator;
    invoke-static {v1}, Lorg/apache/lucene/search/BooleanScorer2;->access$300(Lorg/apache/lucene/search/BooleanScorer2;)Lorg/apache/lucene/search/BooleanScorer2$Coordinator;

    move-result-object v1

    iget v2, v1, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    iget v3, p0, Lorg/apache/lucene/search/DisjunctionSumScorer;->nrMatchers:I

    add-int/2addr v2, v3

    iput v2, v1, Lorg/apache/lucene/search/BooleanScorer2$Coordinator;->nrMatchers:I

    .line 163
    :cond_1
    iget v1, p0, Lorg/apache/lucene/search/BooleanScorer2$1;->lastDocScore:F

    return v1
.end method
