.class public Lorg/apache/lucene/search/WildcardQuery;
.super Lorg/apache/lucene/search/MultiTermQuery;
.source "WildcardQuery.java"


# instance fields
.field protected term:Lorg/apache/lucene/index/Term;

.field private termContainsWildcard:Z

.field private termIsPrefix:Z


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 7
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    const/16 v6, 0x3f

    const/16 v5, 0x2a

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 43
    invoke-direct {p0}, Lorg/apache/lucene/search/MultiTermQuery;-><init>()V

    .line 44
    iput-object p1, p0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    .line 45
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ne v1, v4, :cond_0

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-eq v1, v4, :cond_1

    :cond_0
    move v1, v3

    :goto_0
    iput-boolean v1, p0, Lorg/apache/lucene/search/WildcardQuery;->termContainsWildcard:Z

    .line 48
    iget-boolean v1, p0, Lorg/apache/lucene/search/WildcardQuery;->termContainsWildcard:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ne v1, v4, :cond_2

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v1, v4, :cond_2

    :goto_1
    iput-boolean v3, p0, Lorg/apache/lucene/search/WildcardQuery;->termIsPrefix:Z

    .line 51
    return-void

    :cond_1
    move v1, v2

    .line 46
    goto :goto_0

    :cond_2
    move v3, v2

    .line 48
    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 95
    if-ne p0, p1, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v1

    .line 97
    :cond_1
    invoke-super {p0, p1}, Lorg/apache/lucene/search/MultiTermQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 98
    goto :goto_0

    .line 99
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 100
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 101
    check-cast v0, Lorg/apache/lucene/search/WildcardQuery;

    .line 102
    .local v0, "other":Lorg/apache/lucene/search/WildcardQuery;
    iget-object v3, p0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v3, :cond_4

    .line 103
    iget-object v3, v0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    if-eqz v3, :cond_0

    move v1, v2

    .line 104
    goto :goto_0

    .line 105
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    iget-object v4, v0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 106
    goto :goto_0
.end method

.method protected getEnum(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/FilteredTermEnum;
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    iget-boolean v0, p0, Lorg/apache/lucene/search/WildcardQuery;->termIsPrefix:Z

    if-eqz v0, :cond_0

    .line 56
    new-instance v0, Lorg/apache/lucene/search/PrefixTermEnum;

    iget-object v1, p0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    iget-object v2, p0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x2a

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/Term;->createTerm(Ljava/lang/String;)Lorg/apache/lucene/index/Term;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/search/PrefixTermEnum;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/Term;)V

    .line 61
    :goto_0
    return-object v0

    .line 58
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/WildcardQuery;->termContainsWildcard:Z

    if-eqz v0, :cond_1

    .line 59
    new-instance v0, Lorg/apache/lucene/search/WildcardTermEnum;

    invoke-virtual {p0}, Lorg/apache/lucene/search/WildcardQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/search/WildcardTermEnum;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/Term;)V

    goto :goto_0

    .line 61
    :cond_1
    new-instance v0, Lorg/apache/lucene/search/SingleTermEnum;

    invoke-virtual {p0}, Lorg/apache/lucene/search/WildcardQuery;->getTerm()Lorg/apache/lucene/index/Term;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/search/SingleTermEnum;-><init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/Term;)V

    goto :goto_0
.end method

.method public getTerm()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 87
    const/16 v0, 0x1f

    .line 88
    .local v0, "prime":I
    invoke-super {p0}, Lorg/apache/lucene/search/MultiTermQuery;->hashCode()I

    move-result v1

    .line 89
    .local v1, "result":I
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int v1, v3, v2

    .line 90
    return v1

    .line 89
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    iget-object v1, p0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/WildcardQuery;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    invoke-virtual {p0}, Lorg/apache/lucene/search/WildcardQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
