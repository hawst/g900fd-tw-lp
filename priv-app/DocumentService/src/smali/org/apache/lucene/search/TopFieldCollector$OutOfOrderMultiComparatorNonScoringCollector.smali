.class Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;
.super Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;
.source "TopFieldCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TopFieldCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OutOfOrderMultiComparatorNonScoringCollector"
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V
    .locals 0
    .param p2, "numHits"    # I
    .param p3, "fillFields"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/FieldValueHitQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;IZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 472
    .local p1, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    .line 473
    return-void
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 526
    const/4 v0, 0x1

    return v0
.end method

.method public collect(I)V
    .locals 5
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 477
    iget v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->totalHits:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->totalHits:I

    .line 478
    iget-boolean v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->queueFull:Z

    if-eqz v3, :cond_6

    .line 480
    const/4 v1, 0x0

    .line 481
    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->reverseMul:[I

    aget v3, v3, v1

    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v4, v4, v1

    invoke-virtual {v4, p1}, Lorg/apache/lucene/search/FieldComparator;->compareBottom(I)I

    move-result v4

    mul-int v0, v3, v4

    .line 482
    .local v0, "c":I
    if-gez v0, :cond_1

    .line 522
    .end local v0    # "c":I
    :cond_0
    :goto_1
    return-void

    .line 485
    .restart local v0    # "c":I
    :cond_1
    if-lez v0, :cond_3

    .line 499
    :cond_2
    const/4 v1, 0x0

    :goto_2
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 500
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v3, v3, v1

    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v4, v4, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v3, v4, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 499
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 488
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_4

    .line 490
    iget v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->docBase:I

    add-int/2addr v3, p1

    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v4, v4, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    if-le v3, v4, :cond_2

    goto :goto_1

    .line 480
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 503
    :cond_5
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->updateBottom(I)V

    .line 505
    const/4 v1, 0x0

    :goto_3
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 506
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v3, v3, v1

    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v4, v4, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 505
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 510
    .end local v0    # "c":I
    .end local v1    # "i":I
    :cond_6
    iget v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->totalHits:I

    add-int/lit8 v2, v3, -0x1

    .line 512
    .local v2, "slot":I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 513
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2, p1}, Lorg/apache/lucene/search/FieldComparator;->copy(II)V

    .line 512
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 515
    :cond_7
    const/high16 v3, 0x7fc00000    # NaNf

    invoke-virtual {p0, v2, p1, v3}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->add(IIF)V

    .line 516
    iget-boolean v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->queueFull:Z

    if-eqz v3, :cond_0

    .line 517
    const/4 v1, 0x0

    :goto_5
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 518
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v3, v3, v1

    iget-object v4, p0, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v4, v4, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->slot:I

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/FieldComparator;->setBottom(I)V

    .line 517
    add-int/lit8 v1, v1, 0x1

    goto :goto_5
.end method
