.class Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1$1;
.super Lorg/apache/lucene/search/ScoringRewrite;
.source "SpanMultiTermQueryWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/ScoringRewrite",
        "<",
        "Lorg/apache/lucene/search/spans/SpanOrQuery;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1$1;->this$0:Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1;

    invoke-direct {p0}, Lorg/apache/lucene/search/ScoringRewrite;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic addClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/index/Term;F)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/search/Query;
    .param p2, "x1"    # Lorg/apache/lucene/index/Term;
    .param p3, "x2"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    check-cast p1, Lorg/apache/lucene/search/spans/SpanOrQuery;

    .end local p1    # "x0":Lorg/apache/lucene/search/Query;
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1$1;->addClause(Lorg/apache/lucene/search/spans/SpanOrQuery;Lorg/apache/lucene/index/Term;F)V

    return-void
.end method

.method protected addClause(Lorg/apache/lucene/search/spans/SpanOrQuery;Lorg/apache/lucene/index/Term;F)V
    .locals 1
    .param p1, "topLevel"    # Lorg/apache/lucene/search/spans/SpanOrQuery;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .param p3, "boost"    # F

    .prologue
    .line 185
    new-instance v0, Lorg/apache/lucene/search/spans/SpanTermQuery;

    invoke-direct {v0, p2}, Lorg/apache/lucene/search/spans/SpanTermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 186
    .local v0, "q":Lorg/apache/lucene/search/spans/SpanTermQuery;
    invoke-virtual {v0, p3}, Lorg/apache/lucene/search/spans/SpanTermQuery;->setBoost(F)V

    .line 187
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/spans/SpanOrQuery;->addClause(Lorg/apache/lucene/search/spans/SpanQuery;)V

    .line 188
    return-void
.end method

.method protected bridge synthetic getTopLevelQuery()Lorg/apache/lucene/search/Query;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/SpanMultiTermQueryWrapper$1$1;->getTopLevelQuery()Lorg/apache/lucene/search/spans/SpanOrQuery;

    move-result-object v0

    return-object v0
.end method

.method protected getTopLevelQuery()Lorg/apache/lucene/search/spans/SpanOrQuery;
    .locals 2

    .prologue
    .line 180
    new-instance v0, Lorg/apache/lucene/search/spans/SpanOrQuery;

    const/4 v1, 0x0

    new-array v1, v1, [Lorg/apache/lucene/search/spans/SpanQuery;

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/spans/SpanOrQuery;-><init>([Lorg/apache/lucene/search/spans/SpanQuery;)V

    return-object v0
.end method
