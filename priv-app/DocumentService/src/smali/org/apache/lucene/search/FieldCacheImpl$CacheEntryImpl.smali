.class final Lorg/apache/lucene/search/FieldCacheImpl$CacheEntryImpl;
.super Lorg/apache/lucene/search/FieldCache$CacheEntry;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CacheEntryImpl"
.end annotation


# instance fields
.field private final cacheType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final custom:Ljava/lang/Object;

.field private final fieldName:Ljava/lang/String;

.field private final readerKey:Ljava/lang/Object;

.field private final value:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "readerKey"    # Ljava/lang/Object;
    .param p2, "fieldName"    # Ljava/lang/String;
    .param p4, "custom"    # Ljava/lang/Object;
    .param p5, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 106
    .local p3, "cacheType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldCache$CacheEntry;-><init>()V

    .line 107
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheEntryImpl;->readerKey:Ljava/lang/Object;

    .line 108
    iput-object p2, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheEntryImpl;->fieldName:Ljava/lang/String;

    .line 109
    iput-object p3, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheEntryImpl;->cacheType:Ljava/lang/Class;

    .line 110
    iput-object p4, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheEntryImpl;->custom:Ljava/lang/Object;

    .line 111
    iput-object p5, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheEntryImpl;->value:Ljava/lang/Object;

    .line 118
    return-void
.end method


# virtual methods
.method public getCacheType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheEntryImpl;->cacheType:Ljava/lang/Class;

    return-object v0
.end method

.method public getCustom()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheEntryImpl;->custom:Ljava/lang/Object;

    return-object v0
.end method

.method public getFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheEntryImpl;->fieldName:Ljava/lang/String;

    return-object v0
.end method

.method public getReaderKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheEntryImpl;->readerKey:Ljava/lang/Object;

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$CacheEntryImpl;->value:Ljava/lang/Object;

    return-object v0
.end method
