.class abstract Lorg/apache/lucene/search/FieldCacheImpl$Cache;
.super Ljava/lang/Object;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "Cache"
.end annotation


# instance fields
.field final readerCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/search/FieldCacheImpl$Entry;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field final wrapper:Lorg/apache/lucene/search/FieldCacheImpl;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    .line 158
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    .line 159
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl;)V
    .locals 1
    .param p1, "wrapper"    # Lorg/apache/lucene/search/FieldCacheImpl;

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    .line 162
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    .line 163
    return-void
.end method

.method private printNewInsanity(Ljava/io/PrintStream;Ljava/lang/Object;)V
    .locals 7
    .param p1, "infoStream"    # Ljava/io/PrintStream;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 255
    iget-object v5, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    invoke-static {v5}, Lorg/apache/lucene/util/FieldCacheSanityChecker;->checkSanity(Lorg/apache/lucene/search/FieldCache;)[Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;

    move-result-object v2

    .line 256
    .local v2, "insanities":[Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v2

    if-ge v1, v5, :cond_2

    .line 257
    aget-object v3, v2, v1

    .line 258
    .local v3, "insanity":Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;
    invoke-virtual {v3}, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;->getCacheEntries()[Lorg/apache/lucene/search/FieldCache$CacheEntry;

    move-result-object v0

    .line 259
    .local v0, "entries":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    array-length v5, v0

    if-ge v4, v5, :cond_0

    .line 260
    aget-object v5, v0, v4

    invoke-virtual {v5}, Lorg/apache/lucene/search/FieldCache$CacheEntry;->getValue()Ljava/lang/Object;

    move-result-object v5

    if-ne v5, p2, :cond_1

    .line 262
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "WARNING: new FieldCache insanity created\nDetails: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 263
    const-string/jumbo v5, "\nStack:\n"

    invoke-virtual {p1, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 264
    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 256
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 259
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 269
    .end local v0    # "entries":[Lorg/apache/lucene/search/FieldCache$CacheEntry;
    .end local v3    # "insanity":Lorg/apache/lucene/util/FieldCacheSanityChecker$Insanity;
    .end local v4    # "j":I
    :cond_2
    return-void
.end method


# virtual methods
.method protected abstract createValue(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public get(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;
    .locals 9
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "key"    # Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v4

    .line 209
    .local v4, "readerKey":Ljava/lang/Object;
    iget-object v7, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    monitor-enter v7

    .line 210
    :try_start_0
    iget-object v6, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 211
    .local v2, "innerCache":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;"
    if-nez v2, :cond_3

    .line 213
    new-instance v2, Ljava/util/HashMap;

    .end local v2    # "innerCache":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;"
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 214
    .restart local v2    # "innerCache":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;"
    iget-object v6, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    invoke-interface {v6, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    instance-of v6, p1, Lorg/apache/lucene/index/SegmentReader;

    if-eqz v6, :cond_2

    .line 216
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/index/SegmentReader;

    move-object v6, v0

    iget-object v8, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    iget-object v8, v8, Lorg/apache/lucene/search/FieldCacheImpl;->purgeCore:Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    invoke-virtual {v6, v8}, Lorg/apache/lucene/index/SegmentReader;->addCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V

    .line 220
    :goto_0
    const/4 v5, 0x0

    .line 224
    :goto_1
    if-nez v5, :cond_4

    .line 225
    new-instance v5, Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;

    invoke-direct {v5}, Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;-><init>()V

    .line 226
    .local v5, "value":Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;
    invoke-interface {v2, p2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v6, v5

    .line 228
    .end local v5    # "value":Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;
    :goto_2
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    instance-of v7, v6, Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;

    if-eqz v7, :cond_1

    .line 230
    monitor-enter v6

    .line 231
    :try_start_1
    move-object v0, v6

    check-cast v0, Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;

    move-object v3, v0

    .line 232
    .local v3, "progress":Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;
    iget-object v7, v3, Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;->value:Ljava/lang/Object;

    if-nez v7, :cond_0

    .line 233
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->createValue(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;

    move-result-object v7

    iput-object v7, v3, Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;->value:Ljava/lang/Object;

    .line 234
    iget-object v8, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    monitor-enter v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 235
    :try_start_2
    iget-object v7, v3, Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;->value:Ljava/lang/Object;

    invoke-interface {v2, p2, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 241
    :try_start_3
    iget-object v7, p2, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->custom:Ljava/lang/Object;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    if-eqz v7, :cond_0

    .line 242
    iget-object v7, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    invoke-virtual {v7}, Lorg/apache/lucene/search/FieldCacheImpl;->getInfoStream()Ljava/io/PrintStream;

    move-result-object v1

    .line 243
    .local v1, "infoStream":Ljava/io/PrintStream;
    if-eqz v1, :cond_0

    .line 244
    iget-object v7, v3, Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;->value:Ljava/lang/Object;

    invoke-direct {p0, v1, v7}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->printNewInsanity(Ljava/io/PrintStream;Ljava/lang/Object;)V

    .line 248
    .end local v1    # "infoStream":Ljava/io/PrintStream;
    :cond_0
    iget-object v7, v3, Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;->value:Ljava/lang/Object;

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-object v6, v7

    .line 251
    .end local v3    # "progress":Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;
    :cond_1
    return-object v6

    .line 218
    :cond_2
    :try_start_4
    iget-object v6, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    iget-object v6, v6, Lorg/apache/lucene/search/FieldCacheImpl;->purgeReader:Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;

    invoke-virtual {p1, v6}, Lorg/apache/lucene/index/IndexReader;->addReaderClosedListener(Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;)V

    goto :goto_0

    .line 228
    .end local v2    # "innerCache":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;"
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v6

    .line 222
    .restart local v2    # "innerCache":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;"
    :cond_3
    :try_start_5
    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v5

    .local v5, "value":Ljava/lang/Object;
    goto :goto_1

    .line 236
    .end local v5    # "value":Ljava/lang/Object;
    .restart local v3    # "progress":Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;
    :catchall_1
    move-exception v7

    :try_start_6
    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v7

    .line 249
    .end local v3    # "progress":Lorg/apache/lucene/search/FieldCache$CreationPlaceholder;
    :catchall_2
    move-exception v7

    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v7

    :cond_4
    move-object v6, v5

    goto :goto_2
.end method

.method public purge(Lorg/apache/lucene/index/IndexReader;)V
    .locals 3
    .param p1, "r"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 174
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v0

    .line 175
    .local v0, "readerKey":Ljava/lang/Object;
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    monitor-enter v2

    .line 176
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    monitor-exit v2

    .line 178
    return-void

    .line 177
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public put(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;)V
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "key"    # Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    .param p3, "value"    # Ljava/lang/Object;

    .prologue
    .line 183
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->getCoreCacheKey()Ljava/lang/Object;

    move-result-object v1

    .line 184
    .local v1, "readerKey":Ljava/lang/Object;
    iget-object v3, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    monitor-enter v3

    .line 185
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 186
    .local v0, "innerCache":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;"
    if-nez v0, :cond_0

    .line 188
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "innerCache":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 189
    .restart local v0    # "innerCache":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;"
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->readerCache:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    instance-of v2, p1, Lorg/apache/lucene/index/SegmentReader;

    if-eqz v2, :cond_2

    .line 191
    check-cast p1, Lorg/apache/lucene/index/SegmentReader;

    .end local p1    # "reader":Lorg/apache/lucene/index/IndexReader;
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    iget-object v2, v2, Lorg/apache/lucene/search/FieldCacheImpl;->purgeCore:Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;

    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/SegmentReader;->addCoreClosedListener(Lorg/apache/lucene/index/SegmentReader$CoreClosedListener;)V

    .line 196
    :cond_0
    :goto_0
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 197
    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    :cond_1
    monitor-exit v3

    .line 203
    return-void

    .line 193
    .restart local p1    # "reader":Lorg/apache/lucene/index/IndexReader;
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/search/FieldCacheImpl$Cache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    iget-object v2, v2, Lorg/apache/lucene/search/FieldCacheImpl;->purgeReader:Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;

    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/IndexReader;->addReaderClosedListener(Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;)V

    goto :goto_0

    .line 202
    .end local v0    # "innerCache":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/search/FieldCacheImpl$Entry;Ljava/lang/Object;>;"
    .end local p1    # "reader":Lorg/apache/lucene/index/IndexReader;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
