.class Lorg/apache/lucene/search/FilterManager$FilterCleaner$1;
.super Ljava/lang/Object;
.source "FilterManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FilterManager$FilterCleaner;-><init>(Lorg/apache/lucene/search/FilterManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/Integer;",
        "Lorg/apache/lucene/search/FilterManager$FilterItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/search/FilterManager$FilterCleaner;

.field final synthetic val$this$0:Lorg/apache/lucene/search/FilterManager;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FilterManager$FilterCleaner;Lorg/apache/lucene/search/FilterManager;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner$1;->this$1:Lorg/apache/lucene/search/FilterManager$FilterCleaner;

    iput-object p2, p0, Lorg/apache/lucene/search/FilterManager$FilterCleaner$1;->val$this$0:Lorg/apache/lucene/search/FilterManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 159
    check-cast p1, Ljava/util/Map$Entry;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/util/Map$Entry;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FilterManager$FilterCleaner$1;->compare(Ljava/util/Map$Entry;Ljava/util/Map$Entry;)I

    move-result v0

    return v0
.end method

.method public compare(Ljava/util/Map$Entry;Ljava/util/Map$Entry;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/search/FilterManager$FilterItem;",
            ">;",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/search/FilterManager$FilterItem;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 160
    .local p1, "a":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lorg/apache/lucene/search/FilterManager$FilterItem;>;"
    .local p2, "b":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lorg/apache/lucene/search/FilterManager$FilterItem;>;"
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FilterManager$FilterItem;

    .line 161
    .local v0, "fia":Lorg/apache/lucene/search/FilterManager$FilterItem;
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/FilterManager$FilterItem;

    .line 162
    .local v1, "fib":Lorg/apache/lucene/search/FilterManager$FilterItem;
    iget-wide v2, v0, Lorg/apache/lucene/search/FilterManager$FilterItem;->timestamp:J

    iget-wide v4, v1, Lorg/apache/lucene/search/FilterManager$FilterItem;->timestamp:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 163
    const/4 v2, 0x0

    .line 170
    :goto_0
    return v2

    .line 166
    :cond_0
    iget-wide v2, v0, Lorg/apache/lucene/search/FilterManager$FilterItem;->timestamp:J

    iget-wide v4, v1, Lorg/apache/lucene/search/FilterManager$FilterItem;->timestamp:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 167
    const/4 v2, -0x1

    goto :goto_0

    .line 170
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method
