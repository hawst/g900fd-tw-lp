.class public Lorg/apache/lucene/search/WildcardTermEnum;
.super Lorg/apache/lucene/search/FilteredTermEnum;
.source "WildcardTermEnum.java"


# static fields
.field public static final WILDCARD_CHAR:C = '?'

.field public static final WILDCARD_STRING:C = '*'


# instance fields
.field endEnum:Z

.field final field:Ljava/lang/String;

.field final pre:Ljava/lang/String;

.field final preLen:I

.field final searchTerm:Lorg/apache/lucene/index/Term;

.field final text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/index/Term;)V
    .locals 7
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 47
    invoke-direct {p0}, Lorg/apache/lucene/search/FilteredTermEnum;-><init>()V

    .line 38
    iput-boolean v6, p0, Lorg/apache/lucene/search/WildcardTermEnum;->endEnum:Z

    .line 48
    iput-object p2, p0, Lorg/apache/lucene/search/WildcardTermEnum;->searchTerm:Lorg/apache/lucene/index/Term;

    .line 49
    iget-object v4, p0, Lorg/apache/lucene/search/WildcardTermEnum;->searchTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/search/WildcardTermEnum;->field:Ljava/lang/String;

    .line 50
    iget-object v4, p0, Lorg/apache/lucene/search/WildcardTermEnum;->searchTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v2

    .line 52
    .local v2, "searchTermText":Ljava/lang/String;
    const/16 v4, 0x2a

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 53
    .local v3, "sidx":I
    const/16 v4, 0x3f

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 54
    .local v0, "cidx":I
    move v1, v3

    .line 55
    .local v1, "idx":I
    if-ne v1, v5, :cond_1

    .line 56
    move v1, v0

    .line 61
    :cond_0
    :goto_0
    if-eq v1, v5, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/search/WildcardTermEnum;->searchTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    :goto_1
    iput-object v4, p0, Lorg/apache/lucene/search/WildcardTermEnum;->pre:Ljava/lang/String;

    .line 63
    iget-object v4, p0, Lorg/apache/lucene/search/WildcardTermEnum;->pre:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/search/WildcardTermEnum;->preLen:I

    .line 64
    iget v4, p0, Lorg/apache/lucene/search/WildcardTermEnum;->preLen:I

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/search/WildcardTermEnum;->text:Ljava/lang/String;

    .line 65
    new-instance v4, Lorg/apache/lucene/index/Term;

    iget-object v5, p0, Lorg/apache/lucene/search/WildcardTermEnum;->searchTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {v5}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/search/WildcardTermEnum;->pre:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v4}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/search/WildcardTermEnum;->setEnum(Lorg/apache/lucene/index/TermEnum;)V

    .line 66
    return-void

    .line 58
    :cond_1
    if-ltz v0, :cond_0

    .line 59
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_0

    .line 61
    :cond_2
    const-string/jumbo v4, ""

    goto :goto_1
.end method

.method public static final wildcardEquals(Ljava/lang/String;ILjava/lang/String;I)Z
    .locals 10
    .param p0, "pattern"    # Ljava/lang/String;
    .param p1, "patternIdx"    # I
    .param p2, "string"    # Ljava/lang/String;
    .param p3, "stringIdx"    # I

    .prologue
    .line 105
    move v2, p1

    .line 107
    .local v2, "p":I
    move v4, p3

    .line 110
    .local v4, "s":I
    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v4, v8, :cond_0

    const/4 v5, 0x1

    .line 112
    .local v5, "sEnd":Z
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v2, v8, :cond_1

    const/4 v3, 0x1

    .line 115
    .local v3, "pEnd":Z
    :goto_2
    if-eqz v5, :cond_5

    .line 118
    const/4 v1, 0x1

    .line 121
    .local v1, "justWildcardsLeft":Z
    move v6, v2

    .line 124
    .local v6, "wildcardSearchPos":I
    :goto_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v6, v8, :cond_4

    if-eqz v1, :cond_4

    .line 127
    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 131
    .local v7, "wildchar":C
    const/16 v8, 0x3f

    if-eq v7, v8, :cond_2

    const/16 v8, 0x2a

    if-eq v7, v8, :cond_2

    .line 133
    const/4 v1, 0x0

    goto :goto_3

    .line 110
    .end local v1    # "justWildcardsLeft":Z
    .end local v3    # "pEnd":Z
    .end local v5    # "sEnd":Z
    .end local v6    # "wildcardSearchPos":I
    .end local v7    # "wildchar":C
    :cond_0
    const/4 v5, 0x0

    goto :goto_1

    .line 112
    .restart local v5    # "sEnd":Z
    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    .line 138
    .restart local v1    # "justWildcardsLeft":Z
    .restart local v3    # "pEnd":Z
    .restart local v6    # "wildcardSearchPos":I
    .restart local v7    # "wildchar":C
    :cond_2
    const/16 v8, 0x3f

    if-ne v7, v8, :cond_3

    .line 139
    const/4 v8, 0x0

    .line 189
    .end local v1    # "justWildcardsLeft":Z
    .end local v6    # "wildcardSearchPos":I
    .end local v7    # "wildchar":C
    :goto_4
    return v8

    .line 143
    .restart local v1    # "justWildcardsLeft":Z
    .restart local v6    # "wildcardSearchPos":I
    .restart local v7    # "wildchar":C
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 149
    .end local v7    # "wildchar":C
    :cond_4
    if-eqz v1, :cond_5

    .line 151
    const/4 v8, 0x1

    goto :goto_4

    .line 157
    .end local v1    # "justWildcardsLeft":Z
    .end local v6    # "wildcardSearchPos":I
    :cond_5
    if-nez v5, :cond_6

    if-eqz v3, :cond_7

    .line 189
    :cond_6
    :goto_5
    const/4 v8, 0x0

    goto :goto_4

    .line 163
    :cond_7
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x3f

    if-ne v8, v9, :cond_9

    .line 107
    :cond_8
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 169
    :cond_9
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x2a

    if-ne v8, v9, :cond_c

    .line 172
    :goto_6
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v2, v8, :cond_a

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x2a

    if-ne v8, v9, :cond_a

    .line 173
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 175
    :cond_a
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    .local v0, "i":I
    :goto_7
    if-lt v0, v4, :cond_6

    .line 177
    invoke-static {p0, v2, p2, v0}, Lorg/apache/lucene/search/WildcardTermEnum;->wildcardEquals(Ljava/lang/String;ILjava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 179
    const/4 v8, 0x1

    goto :goto_4

    .line 175
    :cond_b
    add-int/lit8 v0, v0, -0x1

    goto :goto_7

    .line 184
    .end local v0    # "i":I
    :cond_c
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {p2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-eq v8, v9, :cond_8

    goto :goto_5
.end method


# virtual methods
.method public difference()F
    .locals 1

    .prologue
    .line 82
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public final endEnum()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lorg/apache/lucene/search/WildcardTermEnum;->endEnum:Z

    return v0
.end method

.method protected final termCompare(Lorg/apache/lucene/index/Term;)Z
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    const/4 v1, 0x0

    .line 70
    iget-object v2, p0, Lorg/apache/lucene/search/WildcardTermEnum;->field:Ljava/lang/String;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 71
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "searchText":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/lucene/search/WildcardTermEnum;->pre:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    iget-object v2, p0, Lorg/apache/lucene/search/WildcardTermEnum;->text:Ljava/lang/String;

    iget v3, p0, Lorg/apache/lucene/search/WildcardTermEnum;->preLen:I

    invoke-static {v2, v1, v0, v3}, Lorg/apache/lucene/search/WildcardTermEnum;->wildcardEquals(Ljava/lang/String;ILjava/lang/String;I)Z

    move-result v1

    .line 77
    .end local v0    # "searchText":Ljava/lang/String;
    :goto_0
    return v1

    .line 76
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/search/WildcardTermEnum;->endEnum:Z

    goto :goto_0
.end method
