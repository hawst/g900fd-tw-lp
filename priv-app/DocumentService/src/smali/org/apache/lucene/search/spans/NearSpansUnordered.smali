.class public Lorg/apache/lucene/search/spans/NearSpansUnordered;
.super Lorg/apache/lucene/search/spans/Spans;
.source "NearSpansUnordered.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;,
        Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;
    }
.end annotation


# instance fields
.field private first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

.field private firstTime:Z

.field private last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

.field private max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

.field private more:Z

.field private ordered:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;",
            ">;"
        }
    .end annotation
.end field

.field private query:Lorg/apache/lucene/search/spans/SpanNearQuery;

.field private queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

.field private slop:I

.field private subSpans:[Lorg/apache/lucene/search/spans/Spans;

.field private totalLength:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanNearQuery;Lorg/apache/lucene/index/IndexReader;)V
    .locals 5
    .param p1, "query"    # Lorg/apache/lucene/search/spans/SpanNearQuery;
    .param p2, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 135
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/Spans;-><init>()V

    .line 39
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->ordered:Ljava/util/List;

    .line 51
    iput-boolean v4, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    .line 52
    iput-boolean v4, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->firstTime:Z

    .line 136
    iput-object p1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->query:Lorg/apache/lucene/search/spans/SpanNearQuery;

    .line 137
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getSlop()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->slop:I

    .line 139
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getClauses()[Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v1

    .line 140
    .local v1, "clauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    new-instance v3, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    array-length v4, v1

    invoke-direct {v3, p0, v4}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;-><init>(Lorg/apache/lucene/search/spans/NearSpansUnordered;I)V

    iput-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    .line 141
    array-length v3, v1

    new-array v3, v3, [Lorg/apache/lucene/search/spans/Spans;

    iput-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    .line 142
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    .line 143
    new-instance v0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    aget-object v3, v1, v2

    invoke-virtual {v3, p2}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v3

    invoke-direct {v0, p0, v3, v2}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;-><init>(Lorg/apache/lucene/search/spans/NearSpansUnordered;Lorg/apache/lucene/search/spans/Spans;I)V

    .line 145
    .local v0, "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    iget-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->ordered:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    iget-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->spans:Lorg/apache/lucene/search/spans/Spans;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$300(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v4

    aput-object v4, v3, v2

    .line 142
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 148
    .end local v0    # "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    :cond_0
    return-void
.end method

.method static synthetic access$012(Lorg/apache/lucene/search/spans/NearSpansUnordered;I)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/spans/NearSpansUnordered;
    .param p1, "x1"    # I

    .prologue
    .line 36
    iget v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->totalLength:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->totalLength:I

    return v0
.end method

.method static synthetic access$020(Lorg/apache/lucene/search/spans/NearSpansUnordered;I)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/spans/NearSpansUnordered;
    .param p1, "x1"    # I

    .prologue
    .line 36
    iget v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->totalLength:I

    sub-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->totalLength:I

    return v0
.end method

.method static synthetic access$100(Lorg/apache/lucene/search/spans/NearSpansUnordered;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/spans/NearSpansUnordered;

    .prologue
    .line 36
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    return-object v0
.end method

.method static synthetic access$102(Lorg/apache/lucene/search/spans/NearSpansUnordered;Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    .locals 0
    .param p0, "x0"    # Lorg/apache/lucene/search/spans/NearSpansUnordered;
    .param p1, "x1"    # Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .prologue
    .line 36
    iput-object p1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    return-object p1
.end method

.method static synthetic access$202(Lorg/apache/lucene/search/spans/NearSpansUnordered;Z)Z
    .locals 0
    .param p0, "x0"    # Lorg/apache/lucene/search/spans/NearSpansUnordered;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    return p1
.end method

.method private addToList(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)V
    .locals 1
    .param p1, "cell"    # Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 285
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    # setter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0, p1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$402(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 289
    :goto_0
    iput-object p1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 290
    const/4 v0, 0x0

    # setter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {p1, v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$402(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 291
    return-void

    .line 288
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    goto :goto_0
.end method

.method private atMatch()Z
    .locals 2

    .prologue
    .line 315
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->end()I

    move-result v0

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->start()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->totalLength:I

    sub-int/2addr v0, v1

    iget v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->slop:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private firstToLast()V
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    # setter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0, v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$402(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 295
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    iput-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 296
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$400(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 297
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    const/4 v1, 0x0

    # setter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0, v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$402(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 298
    return-void
.end method

.method private initList(Z)V
    .locals 3
    .param p1, "next"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 274
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->ordered:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 275
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->ordered:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 276
    .local v0, "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    if-eqz p1, :cond_0

    .line 277
    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next()Z

    move-result v2

    iput-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    .line 278
    :cond_0
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v2, :cond_1

    .line 279
    invoke-direct {p0, v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->addToList(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)V

    .line 274
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 282
    .end local v0    # "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    :cond_2
    return-void
.end method

.method private listToQueue()V
    .locals 2

    .prologue
    .line 308
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->clear()V

    .line 309
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .local v0, "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    :goto_0
    if-eqz v0, :cond_0

    .line 310
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$400(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    goto :goto_0

    .line 312
    :cond_0
    return-void
.end method

.method private min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    return-object v0
.end method

.method private queueToList()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 301
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    iput-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .line 302
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->top()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->addToList(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)V

    goto :goto_0

    .line 305
    :cond_0
    return-void
.end method


# virtual methods
.method public doc()I
    .locals 1

    .prologue
    .line 230
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v0

    return v0
.end method

.method public end()I
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->end()I

    move-result v0

    return v0
.end method

.method public getPayload()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 244
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 245
    .local v1, "matchPayload":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .local v0, "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    :goto_0
    if-eqz v0, :cond_1

    .line 246
    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->isPayloadAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 247
    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->getPayload()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 245
    :cond_0
    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$400(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    goto :goto_0

    .line 250
    :cond_1
    return-object v1
.end method

.method public getSubSpans()[Lorg/apache/lucene/search/spans/Spans;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    return-object v0
.end method

.method public isPayloadAvailable()Z
    .locals 2

    .prologue
    .line 256
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    .line 257
    .local v0, "pointer":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    :goto_0
    if-eqz v0, :cond_1

    .line 258
    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->isPayloadAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 259
    const/4 v1, 0x1

    .line 264
    :goto_1
    return v1

    .line 261
    :cond_0
    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$400(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    goto :goto_0

    .line 264
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public next()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 154
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->firstTime:Z

    if-eqz v3, :cond_2

    .line 155
    invoke-direct {p0, v2}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->initList(Z)V

    .line 156
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->listToQueue()V

    .line 157
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->firstTime:Z

    .line 166
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v3, :cond_5

    .line 168
    const/4 v0, 0x0

    .line 170
    .local v0, "queueStale":Z
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->max:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 171
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queueToList()V

    .line 172
    const/4 v0, 0x1

    .line 177
    :cond_1
    :goto_1
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 178
    iget-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    iget-object v4, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->last:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    invoke-virtual {v4}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->skipTo(I)Z

    move-result v3

    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    .line 179
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->firstToLast()V

    .line 180
    const/4 v0, 0x1

    goto :goto_1

    .line 158
    .end local v0    # "queueStale":Z
    :cond_2
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v3, :cond_0

    .line 159
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 160
    iget-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->updateTop()Ljava/lang/Object;

    goto :goto_0

    .line 162
    :cond_3
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    goto :goto_0

    .line 183
    .restart local v0    # "queueStale":Z
    :cond_4
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-nez v3, :cond_6

    .line 201
    .end local v0    # "queueStale":Z
    :cond_5
    :goto_2
    return v1

    .line 187
    .restart local v0    # "queueStale":Z
    :cond_6
    if-eqz v0, :cond_7

    .line 188
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->listToQueue()V

    .line 189
    const/4 v0, 0x0

    .line 192
    :cond_7
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->atMatch()Z

    move-result v3

    if-eqz v3, :cond_8

    move v1, v2

    .line 193
    goto :goto_2

    .line 196
    :cond_8
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next()Z

    move-result v3

    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    .line 197
    iget-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v3, :cond_0

    .line 198
    iget-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->updateTop()Ljava/lang/Object;

    goto :goto_0
.end method

.method public skipTo(I)Z
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 206
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->firstTime:Z

    if-eqz v2, :cond_5

    .line 207
    invoke-direct {p0, v1}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->initList(Z)V

    .line 208
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->first:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    .local v0, "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    :goto_0
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->skipTo(I)Z

    move-result v2

    iput-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    .line 208
    # getter for: Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->next:Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    invoke-static {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->access$400(Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;)Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    goto :goto_0

    .line 211
    :cond_0
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v2, :cond_1

    .line 212
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->listToQueue()V

    .line 214
    :cond_1
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->firstTime:Z

    .line 224
    .end local v0    # "cell":Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;
    :cond_2
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->atMatch()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->next()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    return v1

    .line 216
    :cond_5
    :goto_1
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->doc()I

    move-result v2

    if-ge v2, p1, :cond_2

    .line 217
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->skipTo(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 218
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->queue:Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/NearSpansUnordered$CellQueue;->updateTop()Ljava/lang/Object;

    goto :goto_1

    .line 220
    :cond_6
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    goto :goto_1
.end method

.method public start()I
    .locals 1

    .prologue
    .line 232
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->min()Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/search/spans/NearSpansUnordered$SpansCell;->start()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 269
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->query:Lorg/apache/lucene/search/spans/SpanNearQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->firstTime:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "START"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/NearSpansUnordered;->more:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->doc()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->start()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansUnordered;->end()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "END"

    goto :goto_0
.end method
