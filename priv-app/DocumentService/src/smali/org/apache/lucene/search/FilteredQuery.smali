.class public Lorg/apache/lucene/search/FilteredQuery;
.super Lorg/apache/lucene/search/Query;
.source "FilteredQuery.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field filter:Lorg/apache/lucene/search/Filter;

.field query:Lorg/apache/lucene/search/Query;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/apache/lucene/search/FilteredQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FilteredQuery;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Filter;)V
    .locals 0
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "filter"    # Lorg/apache/lucene/search/Filter;

    .prologue
    .line 52
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 53
    iput-object p1, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    .line 54
    iput-object p2, p0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    .line 55
    return-void
.end method

.method static getFilteredScorer(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Filter;)Lorg/apache/lucene/search/Scorer;
    .locals 6
    .param p0, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p2, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p3, "wrapperWeight"    # Lorg/apache/lucene/search/Weight;
    .param p4, "filter"    # Lorg/apache/lucene/search/Filter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 134
    sget-boolean v4, Lorg/apache/lucene/search/FilteredQuery;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-nez p4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 136
    :cond_0
    invoke-virtual {p4, p0}, Lorg/apache/lucene/search/Filter;->getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;

    move-result-object v0

    .line 137
    .local v0, "filterDocIdSet":Lorg/apache/lucene/search/DocIdSet;
    if-nez v0, :cond_2

    .line 150
    :cond_1
    :goto_0
    return-object v3

    .line 142
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v1

    .line 143
    .local v1, "filterIter":Lorg/apache/lucene/search/DocIdSetIterator;
    if-eqz v1, :cond_1

    .line 149
    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p2, p0, v4, v5}, Lorg/apache/lucene/search/Weight;->scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;

    move-result-object v2

    .line 150
    .local v2, "scorer":Lorg/apache/lucene/search/Scorer;
    if-eqz v2, :cond_1

    new-instance v3, Lorg/apache/lucene/search/FilteredQuery$2;

    invoke-direct {v3, p1, p3, v1, v2}, Lorg/apache/lucene/search/FilteredQuery$2;-><init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/DocIdSetIterator;Lorg/apache/lucene/search/Scorer;)V

    goto :goto_0
.end method


# virtual methods
.method public createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;
    .locals 3
    .param p1, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v1

    .line 64
    .local v1, "weight":Lorg/apache/lucene/search/Weight;
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Query;->getSimilarity(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    .line 65
    .local v0, "similarity":Lorg/apache/lucene/search/Similarity;
    new-instance v2, Lorg/apache/lucene/search/FilteredQuery$1;

    invoke-direct {v2, p0, v1, v0}, Lorg/apache/lucene/search/FilteredQuery$1;-><init>(Lorg/apache/lucene/search/FilteredQuery;Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/search/Similarity;)V

    return-object v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 258
    instance-of v2, p1, Lorg/apache/lucene/search/FilteredQuery;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 259
    check-cast v0, Lorg/apache/lucene/search/FilteredQuery;

    .line 260
    .local v0, "fq":Lorg/apache/lucene/search/FilteredQuery;
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    iget-object v3, v0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/Query;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    iget-object v3, v0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/search/FilteredQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/FilteredQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 262
    .end local v0    # "fq":Lorg/apache/lucene/search/FilteredQuery;
    :cond_0
    return v1
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 240
    .local p1, "terms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/search/FilteredQuery;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/Query;->extractTerms(Ljava/util/Set;)V

    .line 241
    return-void
.end method

.method public getFilter()Lorg/apache/lucene/search/Filter;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    return-object v0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 268
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v0}, Lorg/apache/lucene/search/Query;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {p0}, Lorg/apache/lucene/search/FilteredQuery;->getBoost()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v2

    add-int/2addr v1, v2

    xor-int/2addr v0, v1

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 219
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v1

    .line 220
    .local v1, "rewritten":Lorg/apache/lucene/search/Query;
    iget-object v2, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    if-eq v1, v2, :cond_0

    .line 221
    invoke-virtual {p0}, Lorg/apache/lucene/search/FilteredQuery;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FilteredQuery;

    .line 222
    .local v0, "clone":Lorg/apache/lucene/search/FilteredQuery;
    iput-object v1, v0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    .line 225
    .end local v0    # "clone":Lorg/apache/lucene/search/FilteredQuery;
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 246
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 247
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "filtered("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery;->query:Lorg/apache/lucene/search/Query;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Query;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    const-string/jumbo v1, ")->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    iget-object v1, p0, Lorg/apache/lucene/search/FilteredQuery;->filter:Lorg/apache/lucene/search/Filter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 251
    invoke-virtual {p0}, Lorg/apache/lucene/search/FilteredQuery;->getBoost()F

    move-result v1

    invoke-static {v1}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
