.class public Lorg/apache/lucene/search/FieldValueFilter;
.super Lorg/apache/lucene/search/Filter;
.source "FieldValueFilter.java"


# instance fields
.field private final field:Ljava/lang/String;

.field private final negate:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/search/FieldValueFilter;-><init>(Ljava/lang/String;Z)V

    .line 43
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "negate"    # Z

    .prologue
    .line 55
    invoke-direct {p0}, Lorg/apache/lucene/search/Filter;-><init>()V

    .line 56
    iput-object p1, p0, Lorg/apache/lucene/search/FieldValueFilter;->field:Ljava/lang/String;

    .line 57
    iput-boolean p2, p0, Lorg/apache/lucene/search/FieldValueFilter;->negate:Z

    .line 58
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 126
    if-ne p0, p1, :cond_1

    .line 140
    :cond_0
    :goto_0
    return v1

    .line 128
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 129
    goto :goto_0

    .line 130
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 131
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 132
    check-cast v0, Lorg/apache/lucene/search/FieldValueFilter;

    .line 133
    .local v0, "other":Lorg/apache/lucene/search/FieldValueFilter;
    iget-object v3, p0, Lorg/apache/lucene/search/FieldValueFilter;->field:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 134
    iget-object v3, v0, Lorg/apache/lucene/search/FieldValueFilter;->field:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 135
    goto :goto_0

    .line 136
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/search/FieldValueFilter;->field:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/search/FieldValueFilter;->field:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 137
    goto :goto_0

    .line 138
    :cond_5
    iget-boolean v3, p0, Lorg/apache/lucene/search/FieldValueFilter;->negate:Z

    iget-boolean v4, v0, Lorg/apache/lucene/search/FieldValueFilter;->negate:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 139
    goto :goto_0
.end method

.method public field()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/search/FieldValueFilter;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getDocIdSet(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/DocIdSet;
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 78
    sget-object v2, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v3, p0, Lorg/apache/lucene/search/FieldValueFilter;->field:Ljava/lang/String;

    invoke-interface {v2, p1, v3}, Lorg/apache/lucene/search/FieldCache;->getDocsWithField(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/util/Bits;

    move-result-object v1

    .line 80
    .local v1, "docsWithField":Lorg/apache/lucene/util/Bits;
    iget-boolean v2, p0, Lorg/apache/lucene/search/FieldValueFilter;->negate:Z

    if-eqz v2, :cond_2

    .line 81
    instance-of v2, v1, Lorg/apache/lucene/util/Bits$MatchAllBits;

    if-eqz v2, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-object v0

    .line 84
    :cond_1
    new-instance v0, Lorg/apache/lucene/search/FieldValueFilter$1;

    invoke-direct {v0, p0, p1, v1}, Lorg/apache/lucene/search/FieldValueFilter$1;-><init>(Lorg/apache/lucene/search/FieldValueFilter;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/Bits;)V

    goto :goto_0

    .line 91
    :cond_2
    instance-of v2, v1, Lorg/apache/lucene/util/Bits$MatchNoBits;

    if-nez v2, :cond_0

    .line 94
    instance-of v2, v1, Lorg/apache/lucene/search/DocIdSet;

    if-eqz v2, :cond_3

    move-object v0, v1

    .line 97
    check-cast v0, Lorg/apache/lucene/search/DocIdSet;

    .line 98
    .local v0, "dis":Lorg/apache/lucene/search/DocIdSet;
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lorg/apache/lucene/search/FieldValueFilter$2;

    invoke-direct {v2, p0, v0, p1}, Lorg/apache/lucene/search/FieldValueFilter$2;-><init>(Lorg/apache/lucene/search/FieldValueFilter;Lorg/apache/lucene/search/DocIdSet;Lorg/apache/lucene/index/IndexReader;)V

    move-object v0, v2

    goto :goto_0

    .line 106
    .end local v0    # "dis":Lorg/apache/lucene/search/DocIdSet;
    :cond_3
    new-instance v0, Lorg/apache/lucene/search/FieldValueFilter$3;

    invoke-direct {v0, p0, p1, v1}, Lorg/apache/lucene/search/FieldValueFilter$3;-><init>(Lorg/apache/lucene/search/FieldValueFilter;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/Bits;)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 117
    const/16 v0, 0x1f

    .line 118
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 119
    .local v1, "result":I
    iget-object v2, p0, Lorg/apache/lucene/search/FieldValueFilter;->field:Ljava/lang/String;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 120
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/lucene/search/FieldValueFilter;->negate:Z

    if-eqz v2, :cond_1

    const/16 v2, 0x4cf

    :goto_1
    add-int v1, v3, v2

    .line 121
    return v1

    .line 119
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/FieldValueFilter;->field:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 120
    :cond_1
    const/16 v2, 0x4d5

    goto :goto_1
.end method

.method public negate()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lorg/apache/lucene/search/FieldValueFilter;->negate:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "FieldValueFilter [field="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/FieldValueFilter;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", negate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lorg/apache/lucene/search/FieldValueFilter;->negate:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
