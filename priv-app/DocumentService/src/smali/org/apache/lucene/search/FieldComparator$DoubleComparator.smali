.class public final Lorg/apache/lucene/search/FieldComparator$DoubleComparator;
.super Lorg/apache/lucene/search/FieldComparator$NumericComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DoubleComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator$NumericComparator",
        "<",
        "Ljava/lang/Double;",
        ">;"
    }
.end annotation


# instance fields
.field private bottom:D

.field private currentReaderValues:[D

.field private final parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;

.field private final values:[D


# direct methods
.method constructor <init>(ILjava/lang/String;Lorg/apache/lucene/search/FieldCache$Parser;Ljava/lang/Double;)V
    .locals 1
    .param p1, "numHits"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "parser"    # Lorg/apache/lucene/search/FieldCache$Parser;
    .param p4, "missingValue"    # Ljava/lang/Double;

    .prologue
    .line 325
    invoke-direct {p0, p2, p4}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;-><init>(Ljava/lang/String;Ljava/lang/Number;)V

    .line 326
    new-array v0, p1, [D

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->values:[D

    .line 327
    check-cast p3, Lorg/apache/lucene/search/FieldCache$DoubleParser;

    .end local p3    # "parser":Lorg/apache/lucene/search/FieldCache$Parser;
    iput-object p3, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    .line 328
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 5
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 332
    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->values:[D

    aget-wide v0, v4, p1

    .line 333
    .local v0, "v1":D
    iget-object v4, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->values:[D

    aget-wide v2, v4, p2

    .line 334
    .local v2, "v2":D
    cmpl-double v4, v0, v2

    if-lez v4, :cond_0

    .line 335
    const/4 v4, 0x1

    .line 339
    :goto_0
    return v4

    .line 336
    :cond_0
    cmpg-double v4, v0, v2

    if-gez v4, :cond_1

    .line 337
    const/4 v4, -0x1

    goto :goto_0

    .line 339
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public compareBottom(I)I
    .locals 4
    .param p1, "doc"    # I

    .prologue
    .line 345
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->currentReaderValues:[D

    aget-wide v0, v2, p1

    .line 348
    .local v0, "v2":D
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v2, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 349
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->missingValue:Ljava/lang/Number;

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 351
    :cond_0
    iget-wide v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->bottom:D

    cmpl-double v2, v2, v0

    if-lez v2, :cond_1

    .line 352
    const/4 v2, 0x1

    .line 356
    :goto_0
    return v2

    .line 353
    :cond_1
    iget-wide v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->bottom:D

    cmpg-double v2, v2, v0

    if-gez v2, :cond_2

    .line 354
    const/4 v2, -0x1

    goto :goto_0

    .line 356
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public copy(II)V
    .locals 4
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 362
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->currentReaderValues:[D

    aget-wide v0, v2, p2

    .line 365
    .local v0, "v2":D
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->docsWithField:Lorg/apache/lucene/util/Bits;

    invoke-interface {v2, p2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 366
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->missingValue:Ljava/lang/Number;

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 368
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->values:[D

    aput-wide v0, v2, p1

    .line 369
    return-void
.end method

.method public setBottom(I)V
    .locals 2
    .param p1, "bottom"    # I

    .prologue
    .line 381
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->values:[D

    aget-wide v0, v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->bottom:D

    .line 382
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 375
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->field:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->parser:Lorg/apache/lucene/search/FieldCache$DoubleParser;

    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->missingValue:Ljava/lang/Number;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, p1, v2, v3, v0}, Lorg/apache/lucene/search/FieldCache;->getDoubles(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$DoubleParser;Z)[D

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->currentReaderValues:[D

    .line 376
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$NumericComparator;->setNextReader(Lorg/apache/lucene/index/IndexReader;I)V

    .line 377
    return-void

    .line 375
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public value(I)Ljava/lang/Double;
    .locals 2
    .param p1, "slot"    # I

    .prologue
    .line 386
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->values:[D

    aget-wide v0, v0, p1

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 318
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$DoubleComparator;->value(I)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method
