.class public Lorg/apache/lucene/search/function/FieldScoreQuery;
.super Lorg/apache/lucene/search/function/ValueSourceQuery;
.source "FieldScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/function/FieldScoreQuery$Type;
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/search/function/FieldScoreQuery$Type;)V
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "type"    # Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

    .prologue
    .line 105
    invoke-static {p1, p2}, Lorg/apache/lucene/search/function/FieldScoreQuery;->getValueSource(Ljava/lang/String;Lorg/apache/lucene/search/function/FieldScoreQuery$Type;)Lorg/apache/lucene/search/function/ValueSource;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/function/ValueSourceQuery;-><init>(Lorg/apache/lucene/search/function/ValueSource;)V

    .line 106
    return-void
.end method

.method private static getValueSource(Ljava/lang/String;Lorg/apache/lucene/search/function/FieldScoreQuery$Type;)Lorg/apache/lucene/search/function/ValueSource;
    .locals 3
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "type"    # Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

    .prologue
    .line 110
    sget-object v0, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;->BYTE:Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

    if-ne p1, v0, :cond_0

    .line 111
    new-instance v0, Lorg/apache/lucene/search/function/ByteFieldSource;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/function/ByteFieldSource;-><init>(Ljava/lang/String;)V

    .line 120
    :goto_0
    return-object v0

    .line 113
    :cond_0
    sget-object v0, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;->SHORT:Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

    if-ne p1, v0, :cond_1

    .line 114
    new-instance v0, Lorg/apache/lucene/search/function/ShortFieldSource;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/function/ShortFieldSource;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :cond_1
    sget-object v0, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;->INT:Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

    if-ne p1, v0, :cond_2

    .line 117
    new-instance v0, Lorg/apache/lucene/search/function/IntFieldSource;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/function/IntFieldSource;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_2
    sget-object v0, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;->FLOAT:Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

    if-ne p1, v0, :cond_3

    .line 120
    new-instance v0, Lorg/apache/lucene/search/function/FloatFieldSource;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/function/FloatFieldSource;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 122
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is not a known Field Score Query Type!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
