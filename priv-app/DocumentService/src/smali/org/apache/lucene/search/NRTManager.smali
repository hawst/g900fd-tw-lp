.class public Lorg/apache/lucene/search/NRTManager;
.super Lorg/apache/lucene/search/ReferenceManager;
.source "NRTManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;,
        Lorg/apache/lucene/search/NRTManager$WaitingListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/ReferenceManager",
        "<",
        "Lorg/apache/lucene/search/IndexSearcher;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final MAX_SEARCHER_GEN:J = 0x7fffffffffffffffL


# instance fields
.field private final genLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private lastRefreshGen:J

.field private final newGeneration:Ljava/util/concurrent/locks/Condition;

.field private final searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

.field private volatile searchingGen:J

.field private final waitingListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/NRTManager$WaitingListener;",
            ">;"
        }
    .end annotation
.end field

.field private final writer:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lorg/apache/lucene/search/NRTManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/NRTManager;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;Lorg/apache/lucene/search/SearcherFactory;)V
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;
    .param p2, "searcherFactory"    # Lorg/apache/lucene/search/SearcherFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/NRTManager;-><init>(Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;Lorg/apache/lucene/search/SearcherFactory;Z)V

    .line 93
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;Lorg/apache/lucene/search/SearcherFactory;Z)V
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;
    .param p2, "searcherFactory"    # Lorg/apache/lucene/search/SearcherFactory;
    .param p3, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;-><init>()V

    .line 75
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/NRTManager;->waitingListeners:Ljava/util/List;

    .line 76
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 77
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/NRTManager;->newGeneration:Ljava/util/concurrent/locks/Condition;

    .line 103
    iput-object p1, p0, Lorg/apache/lucene/search/NRTManager;->writer:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    .line 104
    if-nez p2, :cond_0

    .line 105
    new-instance p2, Lorg/apache/lucene/search/SearcherFactory;

    .end local p2    # "searcherFactory":Lorg/apache/lucene/search/SearcherFactory;
    invoke-direct {p2}, Lorg/apache/lucene/search/SearcherFactory;-><init>()V

    .line 107
    .restart local p2    # "searcherFactory":Lorg/apache/lucene/search/SearcherFactory;
    :cond_0
    iput-object p2, p0, Lorg/apache/lucene/search/NRTManager;->searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

    .line 108
    invoke-virtual {p1}, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->getIndexWriter()Lorg/apache/lucene/index/IndexWriter;

    move-result-object v0

    invoke-static {v0, p3}, Lorg/apache/lucene/index/IndexReader;->open(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-static {p2, v0}, Lorg/apache/lucene/search/SearcherManager;->getSearcher(Lorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/NRTManager;->current:Ljava/lang/Object;

    .line 109
    return-void
.end method

.method private waitOnGenCondition(JLjava/util/concurrent/TimeUnit;)Z
    .locals 3
    .param p1, "time"    # J
    .param p3, "unit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 320
    sget-boolean v0, Lorg/apache/lucene/search/NRTManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 321
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    .line 322
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->newGeneration:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V

    .line 323
    const/4 v0, 0x1

    .line 325
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->newGeneration:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0, p1, p2, p3}, Ljava/util/concurrent/locks/Condition;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public addWaitingListener(Lorg/apache/lucene/search/NRTManager$WaitingListener;)V
    .locals 1
    .param p1, "l"    # Lorg/apache/lucene/search/NRTManager$WaitingListener;

    .prologue
    .line 132
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->waitingListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    return-void
.end method

.method protected declared-synchronized afterClose()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 370
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 373
    const-wide v0, 0x7fffffffffffffffL

    :try_start_1
    iput-wide v0, p0, Lorg/apache/lucene/search/NRTManager;->searchingGen:J

    .line 374
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->newGeneration:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 376
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 378
    monitor-exit p0

    return-void

    .line 376
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 370
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected afterRefresh()V
    .locals 4

    .prologue
    .line 354
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 356
    :try_start_0
    iget-wide v0, p0, Lorg/apache/lucene/search/NRTManager;->searchingGen:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 358
    sget-boolean v0, Lorg/apache/lucene/search/NRTManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/search/NRTManager;->lastRefreshGen:J

    iget-wide v2, p0, Lorg/apache/lucene/search/NRTManager;->searchingGen:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 364
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 359
    :cond_0
    :try_start_1
    iget-wide v0, p0, Lorg/apache/lucene/search/NRTManager;->lastRefreshGen:J

    iput-wide v0, p0, Lorg/apache/lucene/search/NRTManager;->searchingGen:J

    .line 362
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->newGeneration:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 364
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 366
    return-void
.end method

.method protected bridge synthetic decRef(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    check-cast p1, Lorg/apache/lucene/search/IndexSearcher;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/NRTManager;->decRef(Lorg/apache/lucene/search/IndexSearcher;)V

    return-void
.end method

.method protected decRef(Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "reference"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 114
    return-void
.end method

.method public getCurrentSearchingGen()J
    .locals 2

    .prologue
    .line 331
    iget-wide v0, p0, Lorg/apache/lucene/search/NRTManager;->searchingGen:J

    return-wide v0
.end method

.method public isSearcherCurrent()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 386
    invoke-virtual {p0}, Lorg/apache/lucene/search/NRTManager;->acquire()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/IndexSearcher;

    .line 388
    .local v0, "searcher":Lorg/apache/lucene/search/IndexSearcher;
    :try_start_0
    invoke-virtual {v0}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->isCurrent()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 390
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/NRTManager;->release(Ljava/lang/Object;)V

    .line 388
    return v1

    .line 390
    :catchall_0
    move-exception v1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/NRTManager;->release(Ljava/lang/Object;)V

    throw v1
.end method

.method protected bridge synthetic refreshIfNeeded(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    check-cast p1, Lorg/apache/lucene/search/IndexSearcher;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/NRTManager;->refreshIfNeeded(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v0

    return-object v0
.end method

.method protected refreshIfNeeded(Lorg/apache/lucene/search/IndexSearcher;)Lorg/apache/lucene/search/IndexSearcher;
    .locals 6
    .param p1, "referenceToRefresh"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 339
    iget-object v3, p0, Lorg/apache/lucene/search/NRTManager;->writer:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->getAndIncrementGeneration()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/search/NRTManager;->lastRefreshGen:J

    .line 340
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v2

    .line 341
    .local v2, "r":Lorg/apache/lucene/index/IndexReader;
    const/4 v1, 0x0

    .line 342
    .local v1, "newSearcher":Lorg/apache/lucene/search/IndexSearcher;
    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReader;->isCurrent()Z

    move-result v3

    if-nez v3, :cond_0

    .line 343
    invoke-static {v2}, Lorg/apache/lucene/index/IndexReader;->openIfChanged(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    .line 344
    .local v0, "newReader":Lorg/apache/lucene/index/IndexReader;
    if-eqz v0, :cond_0

    .line 345
    iget-object v3, p0, Lorg/apache/lucene/search/NRTManager;->searcherFactory:Lorg/apache/lucene/search/SearcherFactory;

    invoke-static {v3, v0}, Lorg/apache/lucene/search/SearcherManager;->getSearcher(Lorg/apache/lucene/search/SearcherFactory;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/IndexSearcher;

    move-result-object v1

    .line 349
    .end local v0    # "newReader":Lorg/apache/lucene/index/IndexReader;
    :cond_0
    return-object v1
.end method

.method public removeWaitingListener(Lorg/apache/lucene/search/NRTManager$WaitingListener;)V
    .locals 1
    .param p1, "l"    # Lorg/apache/lucene/search/NRTManager$WaitingListener;

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/search/NRTManager;->waitingListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 139
    return-void
.end method

.method protected bridge synthetic tryIncRef(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 72
    check-cast p1, Lorg/apache/lucene/search/IndexSearcher;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/NRTManager;->tryIncRef(Lorg/apache/lucene/search/IndexSearcher;)Z

    move-result v0

    return v0
.end method

.method protected tryIncRef(Lorg/apache/lucene/search/IndexSearcher;)Z
    .locals 1
    .param p1, "reference"    # Lorg/apache/lucene/search/IndexSearcher;

    .prologue
    .line 118
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->tryIncRef()Z

    move-result v0

    return v0
.end method

.method public waitForGeneration(J)V
    .locals 7
    .param p1, "targetGen"    # J

    .prologue
    .line 271
    const-wide/16 v4, -0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Lorg/apache/lucene/search/NRTManager;->waitForGeneration(JJLjava/util/concurrent/TimeUnit;)V

    .line 272
    return-void
.end method

.method public waitForGeneration(JJLjava/util/concurrent/TimeUnit;)V
    .locals 9
    .param p1, "targetGen"    # J
    .param p3, "time"    # J
    .param p5, "unit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 294
    :try_start_0
    iget-object v5, p0, Lorg/apache/lucene/search/NRTManager;->writer:Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;

    invoke-virtual {v5}, Lorg/apache/lucene/search/NRTManager$TrackingIndexWriter;->getGeneration()J

    move-result-wide v0

    .line 295
    .local v0, "curGen":J
    cmp-long v5, p1, v0

    if-lez v5, :cond_0

    .line 296
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "targetGen="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " was never returned by this NRTManager instance (current gen="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    .end local v0    # "curGen":J
    :catch_0
    move-exception v3

    .line 314
    .local v3, "ie":Ljava/lang/InterruptedException;
    new-instance v5, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v5, v3}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v5

    .line 298
    .end local v3    # "ie":Ljava/lang/InterruptedException;
    .restart local v0    # "curGen":J
    :cond_0
    :try_start_1
    iget-object v5, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->lockInterruptibly()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 300
    :try_start_2
    iget-wide v6, p0, Lorg/apache/lucene/search/NRTManager;->searchingGen:J

    cmp-long v5, p1, v6

    if-lez v5, :cond_2

    .line 301
    iget-object v5, p0, Lorg/apache/lucene/search/NRTManager;->waitingListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/NRTManager$WaitingListener;

    .line 302
    .local v4, "listener":Lorg/apache/lucene/search/NRTManager$WaitingListener;
    invoke-interface {v4, p1, p2}, Lorg/apache/lucene/search/NRTManager$WaitingListener;->waiting(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 311
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "listener":Lorg/apache/lucene/search/NRTManager$WaitingListener;
    :catchall_0
    move-exception v5

    :try_start_3
    iget-object v6, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v6}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v5
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 304
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_4
    iget-wide v6, p0, Lorg/apache/lucene/search/NRTManager;->searchingGen:J

    cmp-long v5, p1, v6

    if-lez v5, :cond_2

    .line 305
    invoke-direct {p0, p3, p4, p5}, Lorg/apache/lucene/search/NRTManager;->waitOnGenCondition(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v5

    if-nez v5, :cond_1

    .line 311
    :try_start_5
    iget-object v5, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 316
    .end local v2    # "i$":Ljava/util/Iterator;
    :goto_1
    return-void

    .line 311
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/search/NRTManager;->genLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_1
.end method
