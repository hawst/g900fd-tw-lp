.class public Lorg/apache/lucene/search/function/FieldScoreQuery$Type;
.super Ljava/lang/Object;
.source "FieldScoreQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/function/FieldScoreQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Type"
.end annotation


# static fields
.field public static final BYTE:Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

.field public static final FLOAT:Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

.field public static final INT:Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

.field public static final SHORT:Lorg/apache/lucene/search/function/FieldScoreQuery$Type;


# instance fields
.field private typeName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 74
    new-instance v0, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

    const-string/jumbo v1, "byte"

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;->BYTE:Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

    .line 77
    new-instance v0, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

    const-string/jumbo v1, "short"

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;->SHORT:Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

    .line 80
    new-instance v0, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

    const-string/jumbo v1, "int"

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;->INT:Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

    .line 83
    new-instance v0, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

    const-string/jumbo v1, "float"

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;->FLOAT:Lorg/apache/lucene/search/function/FieldScoreQuery$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;->typeName:Ljava/lang/String;

    .line 88
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/function/FieldScoreQuery$Type;->typeName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
