.class Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;
.super Lorg/apache/lucene/search/Weight;
.source "ValueSourceQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/function/ValueSourceQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ValueSourceWeight"
.end annotation


# instance fields
.field queryNorm:F

.field queryWeight:F

.field similarity:Lorg/apache/lucene/search/Similarity;

.field final synthetic this$0:Lorg/apache/lucene/search/function/ValueSourceQuery;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/function/ValueSourceQuery;Lorg/apache/lucene/search/Searcher;)V
    .locals 1
    .param p2, "searcher"    # Lorg/apache/lucene/search/Searcher;

    .prologue
    .line 70
    iput-object p1, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->this$0:Lorg/apache/lucene/search/function/ValueSourceQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 71
    invoke-virtual {p1, p2}, Lorg/apache/lucene/search/function/ValueSourceQuery;->getSimilarity(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 72
    return-void
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    iget-object v3, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->this$0:Lorg/apache/lucene/search/function/ValueSourceQuery;

    iget-object v3, v3, Lorg/apache/lucene/search/function/ValueSourceQuery;->valSrc:Lorg/apache/lucene/search/function/ValueSource;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/search/function/ValueSource;->getValues(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/function/DocValues;

    move-result-object v2

    .line 109
    .local v2, "vals":Lorg/apache/lucene/search/function/DocValues;
    iget v3, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->queryWeight:F

    invoke-virtual {v2, p2}, Lorg/apache/lucene/search/function/DocValues;->floatVal(I)F

    move-result v4

    mul-float v1, v3, v4

    .line 111
    .local v1, "sc":F
    new-instance v0, Lorg/apache/lucene/search/ComplexExplanation;

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->this$0:Lorg/apache/lucene/search/function/ValueSourceQuery;

    invoke-virtual {v5}, Lorg/apache/lucene/search/function/ValueSourceQuery;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", product of:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, v1, v4}, Lorg/apache/lucene/search/ComplexExplanation;-><init>(ZFLjava/lang/String;)V

    .line 114
    .local v0, "result":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v2, p2}, Lorg/apache/lucene/search/function/DocValues;->explain(I)Lorg/apache/lucene/search/Explanation;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 115
    new-instance v3, Lorg/apache/lucene/search/Explanation;

    iget-object v4, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->this$0:Lorg/apache/lucene/search/function/ValueSourceQuery;

    invoke-virtual {v4}, Lorg/apache/lucene/search/function/ValueSourceQuery;->getBoost()F

    move-result v4

    const-string/jumbo v5, "boost"

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 116
    new-instance v3, Lorg/apache/lucene/search/Explanation;

    iget v4, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->queryNorm:F

    const-string/jumbo v5, "queryNorm"

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    invoke-virtual {v0, v3}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 117
    return-object v0
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->this$0:Lorg/apache/lucene/search/function/ValueSourceQuery;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->queryWeight:F

    return v0
.end method

.method public normalize(F)V
    .locals 2
    .param p1, "norm"    # F

    .prologue
    .line 96
    iput p1, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->queryNorm:F

    .line 97
    iget v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->queryNorm:F

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->queryWeight:F

    .line 98
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    new-instance v0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;

    iget-object v1, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->this$0:Lorg/apache/lucene/search/function/ValueSourceQuery;

    iget-object v2, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    const/4 v5, 0x0

    move-object v3, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceScorer;-><init>(Lorg/apache/lucene/search/function/ValueSourceQuery;Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;Lorg/apache/lucene/search/function/ValueSourceQuery$1;)V

    return-object v0
.end method

.method public sumOfSquaredWeights()F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->this$0:Lorg/apache/lucene/search/function/ValueSourceQuery;

    invoke-virtual {v0}, Lorg/apache/lucene/search/function/ValueSourceQuery;->getBoost()F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->queryWeight:F

    .line 90
    iget v0, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/function/ValueSourceQuery$ValueSourceWeight;->queryWeight:F

    mul-float/2addr v0, v1

    return v0
.end method
