.class public Lorg/apache/lucene/search/spans/NearSpansOrdered;
.super Lorg/apache/lucene/search/spans/Spans;
.source "NearSpansOrdered.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final allowedSlop:I

.field private collectPayloads:Z

.field private firstTime:Z

.field private inSameDoc:Z

.field private matchDoc:I

.field private matchEnd:I

.field private matchPayload:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field private matchStart:I

.field private more:Z

.field private query:Lorg/apache/lucene/search/spans/SpanNearQuery;

.field private final spanDocComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/search/spans/Spans;",
            ">;"
        }
    .end annotation
.end field

.field private final subSpans:[Lorg/apache/lucene/search/spans/Spans;

.field private final subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lorg/apache/lucene/search/spans/NearSpansOrdered;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanNearQuery;Lorg/apache/lucene/index/IndexReader;)V
    .locals 1
    .param p1, "spanNearQuery"    # Lorg/apache/lucene/search/spans/SpanNearQuery;
    .param p2, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;-><init>(Lorg/apache/lucene/search/spans/SpanNearQuery;Lorg/apache/lucene/index/IndexReader;Z)V

    .line 82
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/spans/SpanNearQuery;Lorg/apache/lucene/index/IndexReader;Z)V
    .locals 5
    .param p1, "spanNearQuery"    # Lorg/apache/lucene/search/spans/SpanNearQuery;
    .param p2, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "collectPayloads"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 85
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/Spans;-><init>()V

    .line 56
    iput-boolean v4, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->firstTime:Z

    .line 57
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    .line 63
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    .line 65
    iput v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchDoc:I

    .line 66
    iput v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    .line 67
    iput v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchEnd:I

    .line 71
    new-instance v2, Lorg/apache/lucene/search/spans/NearSpansOrdered$1;

    invoke-direct {v2, p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered$1;-><init>(Lorg/apache/lucene/search/spans/NearSpansOrdered;)V

    iput-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->spanDocComparator:Ljava/util/Comparator;

    .line 78
    iput-boolean v4, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    .line 86
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getClauses()[Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v2

    array-length v2, v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_0

    .line 87
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Less than 2 clauses: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 90
    :cond_0
    iput-boolean p3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    .line 91
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getSlop()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->allowedSlop:I

    .line 92
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->getClauses()[Lorg/apache/lucene/search/spans/SpanQuery;

    move-result-object v0

    .line 93
    .local v0, "clauses":[Lorg/apache/lucene/search/spans/SpanQuery;
    array-length v2, v0

    new-array v2, v2, [Lorg/apache/lucene/search/spans/Spans;

    iput-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    .line 94
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchPayload:Ljava/util/List;

    .line 95
    array-length v2, v0

    new-array v2, v2, [Lorg/apache/lucene/search/spans/Spans;

    iput-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    .line 96
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 97
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v3, v0, v1

    invoke-virtual {v3, p2}, Lorg/apache/lucene/search/spans/SpanQuery;->getSpans(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/spans/Spans;

    move-result-object v3

    aput-object v3, v2, v1

    .line 98
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    iget-object v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v3, v3, v1

    aput-object v3, v2, v1

    .line 96
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 100
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->query:Lorg/apache/lucene/search/spans/SpanNearQuery;

    .line 101
    return-void
.end method

.method private advanceAfterOrdered()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->toSameDoc()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->stretchToOrder()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->shrinkToAfterShortestMatch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    const/4 v0, 0x1

    .line 187
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final docSpansOrdered(IIII)Z
    .locals 2
    .param p0, "start1"    # I
    .param p1, "end1"    # I
    .param p2, "start2"    # I
    .param p3, "end2"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 236
    if-ne p0, p2, :cond_2

    if-ge p1, p3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    if-lt p0, p2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static final docSpansOrdered(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/spans/Spans;)Z
    .locals 6
    .param p0, "spans1"    # Lorg/apache/lucene/search/spans/Spans;
    .param p1, "spans2"    # Lorg/apache/lucene/search/spans/Spans;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 225
    sget-boolean v4, Lorg/apache/lucene/search/spans/NearSpansOrdered;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v4

    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v5

    if-eq v4, v5, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "doc1 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " != doc2 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 226
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v0

    .line 227
    .local v0, "start1":I
    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v1

    .line 229
    .local v1, "start2":I
    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v4

    invoke-virtual {p1}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v5

    if-ge v4, v5, :cond_2

    :cond_1
    :goto_0
    return v2

    :cond_2
    move v2, v3

    goto :goto_0

    :cond_3
    if-lt v0, v1, :cond_1

    move v2, v3

    goto :goto_0
.end method

.method private shrinkToAfterShortestMatch()Z
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 264
    iget-object v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    iget-object v14, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v14, v14

    add-int/lit8 v14, v14, -0x1

    aget-object v13, v13, v14

    invoke-virtual {v13}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v13

    iput v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    .line 265
    iget-object v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    iget-object v14, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v14, v14

    add-int/lit8 v14, v14, -0x1

    aget-object v13, v13, v14

    invoke-virtual {v13}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v13

    iput v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchEnd:I

    .line 266
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 267
    .local v6, "possibleMatchPayloads":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    iget-object v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    iget-object v14, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v14, v14

    add-int/lit8 v14, v14, -0x1

    aget-object v13, v13, v14

    invoke-virtual {v13}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 268
    iget-object v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    iget-object v14, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v14, v14

    add-int/lit8 v14, v14, -0x1

    aget-object v13, v13, v14

    invoke-virtual {v13}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v13

    invoke-interface {v6, v13}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 271
    :cond_0
    const/4 v7, 0x0

    .line 273
    .local v7, "possiblePayload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    const/4 v4, 0x0

    .line 274
    .local v4, "matchSlop":I
    iget v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    .line 275
    .local v2, "lastStart":I
    iget v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchEnd:I

    .line 276
    .local v1, "lastEnd":I
    iget-object v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v13, v13

    add-int/lit8 v0, v13, -0x2

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_9

    .line 277
    iget-object v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v11, v13, v0

    .line 278
    .local v11, "prevSpans":Lorg/apache/lucene/search/spans/Spans;
    iget-boolean v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    if-eqz v13, :cond_1

    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 279
    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v5

    .line 280
    .local v5, "payload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    new-instance v7, Ljava/util/ArrayList;

    .end local v7    # "possiblePayload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v13

    invoke-direct {v7, v13}, Ljava/util/ArrayList;-><init>(I)V

    .line 281
    .restart local v7    # "possiblePayload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-interface {v7, v5}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 284
    .end local v5    # "payload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    :cond_1
    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v12

    .line 285
    .local v12, "prevStart":I
    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v10

    .line 287
    .local v10, "prevEnd":I
    :cond_2
    :goto_1
    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v13

    if-nez v13, :cond_5

    .line 288
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    .line 289
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    .line 311
    :cond_3
    :goto_2
    iget-boolean v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    if-eqz v13, :cond_4

    if-eqz v7, :cond_4

    .line 312
    invoke-interface {v6, v7}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 315
    :cond_4
    sget-boolean v13, Lorg/apache/lucene/search/spans/NearSpansOrdered;->$assertionsDisabled:Z

    if-nez v13, :cond_7

    iget v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    if-le v12, v13, :cond_7

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 291
    :cond_5
    iget v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchDoc:I

    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v14

    if-eq v13, v14, :cond_6

    .line 292
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    goto :goto_2

    .line 295
    :cond_6
    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->start()I

    move-result v9

    .line 296
    .local v9, "ppStart":I
    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->end()I

    move-result v8

    .line 297
    .local v8, "ppEnd":I
    invoke-static {v9, v8, v2, v1}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->docSpansOrdered(IIII)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 300
    move v12, v9

    .line 301
    move v10, v8

    .line 302
    iget-boolean v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    if-eqz v13, :cond_2

    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->isPayloadAvailable()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 303
    invoke-virtual {v11}, Lorg/apache/lucene/search/spans/Spans;->getPayload()Ljava/util/Collection;

    move-result-object v5

    .line 304
    .restart local v5    # "payload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    new-instance v7, Ljava/util/ArrayList;

    .end local v7    # "possiblePayload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v13

    invoke-direct {v7, v13}, Ljava/util/ArrayList;-><init>(I)V

    .line 305
    .restart local v7    # "possiblePayload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    invoke-interface {v7, v5}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 316
    .end local v5    # "payload":Ljava/util/Collection;, "Ljava/util/Collection<[B>;"
    .end local v8    # "ppEnd":I
    .end local v9    # "ppStart":I
    :cond_7
    iget v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    if-le v13, v10, :cond_8

    .line 317
    iget v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    sub-int/2addr v13, v10

    add-int/2addr v4, v13

    .line 323
    :cond_8
    iput v12, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    .line 324
    move v2, v12

    .line 325
    move v1, v10

    .line 276
    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_0

    .line 328
    .end local v10    # "prevEnd":I
    .end local v11    # "prevSpans":Lorg/apache/lucene/search/spans/Spans;
    .end local v12    # "prevStart":I
    :cond_9
    iget v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->allowedSlop:I

    if-gt v4, v13, :cond_a

    const/4 v3, 0x1

    .line 330
    .local v3, "match":Z
    :cond_a
    iget-boolean v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    if-eqz v13, :cond_b

    if-eqz v3, :cond_b

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v13

    if-lez v13, :cond_b

    .line 331
    iget-object v13, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchPayload:Ljava/util/List;

    invoke-interface {v13, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 334
    :cond_b
    return v3
.end method

.method private stretchToOrder()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 243
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchDoc:I

    .line 244
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 245
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v2, v2, v0

    invoke-static {v1, v2}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->docSpansOrdered(Lorg/apache/lucene/search/spans/Spans;Lorg/apache/lucene/search/spans/Spans;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 246
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v1

    if-nez v1, :cond_2

    .line 247
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    .line 248
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    .line 244
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 250
    :cond_2
    iget v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchDoc:I

    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 251
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    goto :goto_1

    .line 256
    :cond_3
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    return v1
.end method

.method private toSameDoc()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 193
    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    iget-object v6, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->spanDocComparator:Ljava/util/Comparator;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 194
    const/4 v0, 0x0

    .line 195
    .local v0, "firstIndex":I
    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    iget-object v6, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    aget-object v5, v5, v6

    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    .line 196
    .local v2, "maxDoc":I
    :cond_0
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v5

    if-eq v5, v2, :cond_2

    .line 197
    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v5, v5, v0

    invoke-virtual {v5, v2}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 198
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    .line 199
    iput-boolean v3, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    .line 214
    :goto_1
    return v3

    .line 202
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    .line 203
    add-int/lit8 v0, v0, 0x1

    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    array-length v5, v5

    if-ne v0, v5, :cond_0

    .line 204
    const/4 v0, 0x0

    goto :goto_0

    .line 207
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    array-length v5, v5

    if-ge v1, v5, :cond_4

    .line 211
    sget-boolean v5, Lorg/apache/lucene/search/spans/NearSpansOrdered;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v5

    if-eq v5, v2, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, " NearSpansOrdered.toSameDoc() spans "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v3, v6, v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "\n at doc "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpansByDoc:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, ", but should be at "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 207
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 213
    :cond_4
    iput-boolean v4, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    move v3, v4

    .line 214
    goto :goto_1
.end method


# virtual methods
.method public doc()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchDoc:I

    return v0
.end method

.method public end()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchEnd:I

    return v0
.end method

.method public getPayload()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchPayload:Ljava/util/List;

    return-object v0
.end method

.method public getSubSpans()[Lorg/apache/lucene/search/spans/Spans;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    return-object v0
.end method

.method public isPayloadAvailable()Z
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchPayload:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 135
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->firstTime:Z

    if-eqz v2, :cond_2

    .line 136
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->firstTime:Z

    .line 137
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 138
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->next()Z

    move-result v2

    if-nez v2, :cond_0

    .line 139
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    .line 148
    .end local v0    # "i":I
    :goto_1
    return v1

    .line 137
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 143
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    .line 145
    .end local v0    # "i":I
    :cond_2
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    if-eqz v1, :cond_3

    .line 146
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchPayload:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 148
    :cond_3
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->advanceAfterOrdered()Z

    move-result v1

    goto :goto_1
.end method

.method public skipTo(I)Z
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 154
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->firstTime:Z

    if-eqz v2, :cond_4

    .line 155
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->firstTime:Z

    .line 156
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 157
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 158
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    .line 174
    .end local v0    # "i":I
    :goto_1
    return v1

    .line 156
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 162
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    .line 171
    .end local v0    # "i":I
    :cond_2
    :goto_2
    iget-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->collectPayloads:Z

    if-eqz v1, :cond_3

    .line 172
    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchPayload:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 174
    :cond_3
    invoke-direct {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->advanceAfterOrdered()Z

    move-result v1

    goto :goto_1

    .line 163
    :cond_4
    iget-boolean v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/apache/lucene/search/spans/Spans;->doc()I

    move-result v2

    if-ge v2, p1, :cond_2

    .line 164
    iget-object v2, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->subSpans:[Lorg/apache/lucene/search/spans/Spans;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1}, Lorg/apache/lucene/search/spans/Spans;->skipTo(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 165
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->inSameDoc:Z

    goto :goto_2

    .line 167
    :cond_5
    iput-boolean v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    goto :goto_1
.end method

.method public start()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->matchStart:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 339
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->query:Lorg/apache/lucene/search/spans/SpanNearQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/spans/SpanNearQuery;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->firstTime:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "START"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/spans/NearSpansOrdered;->more:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->doc()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->start()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/search/spans/NearSpansOrdered;->end()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "END"

    goto :goto_0
.end method
