.class public abstract Lorg/apache/lucene/search/ReferenceManager;
.super Ljava/lang/Object;
.source "ReferenceManager.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<G:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Closeable;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final REFERENCE_MANAGER_IS_CLOSED_MSG:Ljava/lang/String; = "this ReferenceManager is closed"


# instance fields
.field protected volatile current:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TG;"
        }
    .end annotation
.end field

.field private final reopenLock:Ljava/util/concurrent/Semaphore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/lucene/search/ReferenceManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/ReferenceManager;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->reopenLock:Ljava/util/concurrent/Semaphore;

    return-void
.end method

.method private ensureOpen()V
    .locals 2

    .prologue
    .line 48
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->current:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string/jumbo v1, "this ReferenceManager is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_0
    return-void
.end method

.method private declared-synchronized swapReference(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TG;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    .local p1, "newReference":Ljava/lang/Object;, "TG;"
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;->ensureOpen()V

    .line 55
    iget-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->current:Ljava/lang/Object;

    .line 56
    .local v0, "oldReference":Ljava/lang/Object;, "TG;"
    iput-object p1, p0, Lorg/apache/lucene/search/ReferenceManager;->current:Ljava/lang/Object;

    .line 57
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/ReferenceManager;->release(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    monitor-exit p0

    return-void

    .line 54
    .end local v0    # "oldReference":Ljava/lang/Object;, "TG;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public final acquire()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TG;"
        }
    .end annotation

    .prologue
    .line 84
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->current:Ljava/lang/Object;

    .local v0, "ref":Ljava/lang/Object;, "TG;"
    if-nez v0, :cond_1

    .line 85
    new-instance v1, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string/jumbo v2, "this ReferenceManager is closed"

    invoke-direct {v1, v2}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 87
    :cond_1
    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/ReferenceManager;->tryIncRef(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    return-object v0
.end method

.method protected afterClose()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    return-void
.end method

.method protected afterRefresh()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    return-void
.end method

.method public final declared-synchronized close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/search/ReferenceManager;->current:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 102
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/ReferenceManager;->swapReference(Ljava/lang/Object;)V

    .line 103
    invoke-virtual {p0}, Lorg/apache/lucene/search/ReferenceManager;->afterClose()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    :cond_0
    monitor-exit p0

    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract decRef(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TG;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final maybeRefresh()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;->ensureOpen()V

    .line 132
    iget-object v4, p0, Lorg/apache/lucene/search/ReferenceManager;->reopenLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v4}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v0

    .line 133
    .local v0, "doTryRefresh":Z
    if-eqz v0, :cond_2

    .line 135
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/ReferenceManager;->acquire()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 137
    .local v2, "reference":Ljava/lang/Object;, "TG;"
    :try_start_1
    invoke-virtual {p0, v2}, Lorg/apache/lucene/search/ReferenceManager;->refreshIfNeeded(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 138
    .local v1, "newReference":Ljava/lang/Object;, "TG;"
    if-eqz v1, :cond_1

    .line 139
    sget-boolean v4, Lorg/apache/lucene/search/ReferenceManager;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-ne v1, v2, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    const-string/jumbo v5, "refreshIfNeeded should return null if refresh wasn\'t needed"

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    .end local v1    # "newReference":Ljava/lang/Object;, "TG;"
    :catchall_0
    move-exception v4

    :try_start_2
    invoke-virtual {p0, v2}, Lorg/apache/lucene/search/ReferenceManager;->release(Ljava/lang/Object;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 155
    .end local v2    # "reference":Ljava/lang/Object;, "TG;"
    :catchall_1
    move-exception v4

    iget-object v5, p0, Lorg/apache/lucene/search/ReferenceManager;->reopenLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v5}, Ljava/util/concurrent/Semaphore;->release()V

    throw v4

    .line 140
    .restart local v1    # "newReference":Ljava/lang/Object;, "TG;"
    .restart local v2    # "reference":Ljava/lang/Object;, "TG;"
    :cond_0
    const/4 v3, 0x0

    .line 142
    .local v3, "success":Z
    :try_start_3
    invoke-direct {p0, v1}, Lorg/apache/lucene/search/ReferenceManager;->swapReference(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 143
    const/4 v3, 0x1

    .line 145
    if-nez v3, :cond_1

    .line 146
    :try_start_4
    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/ReferenceManager;->release(Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 151
    .end local v3    # "success":Z
    :cond_1
    :try_start_5
    invoke-virtual {p0, v2}, Lorg/apache/lucene/search/ReferenceManager;->release(Ljava/lang/Object;)V

    .line 153
    invoke-virtual {p0}, Lorg/apache/lucene/search/ReferenceManager;->afterRefresh()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 155
    iget-object v4, p0, Lorg/apache/lucene/search/ReferenceManager;->reopenLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v4}, Ljava/util/concurrent/Semaphore;->release()V

    .line 159
    .end local v1    # "newReference":Ljava/lang/Object;, "TG;"
    .end local v2    # "reference":Ljava/lang/Object;, "TG;"
    :cond_2
    return v0

    .line 145
    .restart local v1    # "newReference":Ljava/lang/Object;, "TG;"
    .restart local v2    # "reference":Ljava/lang/Object;, "TG;"
    .restart local v3    # "success":Z
    :catchall_2
    move-exception v4

    if-nez v3, :cond_3

    .line 146
    :try_start_6
    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/ReferenceManager;->release(Ljava/lang/Object;)V

    .line 145
    :cond_3
    throw v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method protected abstract refreshIfNeeded(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TG;)TG;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final release(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TG;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 173
    .local p0, "this":Lorg/apache/lucene/search/ReferenceManager;, "Lorg/apache/lucene/search/ReferenceManager<TG;>;"
    .local p1, "reference":Ljava/lang/Object;, "TG;"
    sget-boolean v0, Lorg/apache/lucene/search/ReferenceManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 174
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/ReferenceManager;->decRef(Ljava/lang/Object;)V

    .line 175
    return-void
.end method

.method protected abstract tryIncRef(Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TG;)Z"
        }
    .end annotation
.end method
