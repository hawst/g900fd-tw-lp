.class final Lorg/apache/lucene/search/FieldCacheImpl$ShortCache;
.super Lorg/apache/lucene/search/FieldCacheImpl$Cache;
.source "FieldCacheImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCacheImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ShortCache"
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheImpl;)V
    .locals 0
    .param p1, "wrapper"    # Lorg/apache/lucene/search/FieldCacheImpl;

    .prologue
    .line 390
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldCacheImpl$Cache;-><init>(Lorg/apache/lucene/search/FieldCacheImpl;)V

    .line 391
    return-void
.end method


# virtual methods
.method protected createValue(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/FieldCacheImpl$Entry;Z)Ljava/lang/Object;
    .locals 16
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "entryKey"    # Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    .param p3, "setDocsWithField"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 396
    move-object/from16 v5, p2

    .line 397
    .local v5, "entry":Lorg/apache/lucene/search/FieldCacheImpl$Entry;
    iget-object v6, v5, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->field:Ljava/lang/String;

    .line 398
    .local v6, "field":Ljava/lang/String;
    iget-object v8, v5, Lorg/apache/lucene/search/FieldCacheImpl$Entry;->custom:Ljava/lang/Object;

    check-cast v8, Lorg/apache/lucene/search/FieldCache$ShortParser;

    .line 399
    .local v8, "parser":Lorg/apache/lucene/search/FieldCache$ShortParser;
    if-nez v8, :cond_1

    .line 400
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/search/FieldCacheImpl$ShortCache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    sget-object v15, Lorg/apache/lucene/search/FieldCache;->DEFAULT_SHORT_PARSER:Lorg/apache/lucene/search/FieldCache$ShortParser;

    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v14, v0, v6, v15, v1}, Lorg/apache/lucene/search/FieldCacheImpl;->getShorts(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/search/FieldCache$ShortParser;Z)[S

    move-result-object v9

    .line 433
    :cond_0
    :goto_0
    return-object v9

    .line 402
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v7

    .line 403
    .local v7, "maxDoc":I
    new-array v9, v7, [S

    .line 404
    .local v9, "retArray":[S
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/IndexReader;->termDocs()Lorg/apache/lucene/index/TermDocs;

    move-result-object v11

    .line 405
    .local v11, "termDocs":Lorg/apache/lucene/index/TermDocs;
    new-instance v14, Lorg/apache/lucene/index/Term;

    invoke-direct {v14, v6}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/IndexReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermEnum;

    move-result-object v12

    .line 406
    .local v12, "termEnum":Lorg/apache/lucene/index/TermEnum;
    const/4 v3, 0x0

    .line 409
    .local v3, "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :goto_1
    :try_start_0
    invoke-virtual {v12}, Lorg/apache/lucene/index/TermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v10

    .line 410
    .local v10, "term":Lorg/apache/lucene/index/Term;
    if-eqz v10, :cond_2

    invoke-virtual {v10}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v14

    if-eq v14, v6, :cond_3

    .line 427
    :cond_2
    :goto_2
    invoke-interface {v11}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 428
    .end local v10    # "term":Lorg/apache/lucene/index/Term;
    :goto_3
    invoke-virtual {v12}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 430
    if-eqz p3, :cond_0

    .line 431
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/search/FieldCacheImpl$ShortCache;->wrapper:Lorg/apache/lucene/search/FieldCacheImpl;

    move-object/from16 v0, p1

    invoke-virtual {v14, v0, v6, v3}, Lorg/apache/lucene/search/FieldCacheImpl;->setDocsWithField(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;Lorg/apache/lucene/util/Bits;)V

    goto :goto_0

    .line 411
    .restart local v10    # "term":Lorg/apache/lucene/index/Term;
    :cond_3
    :try_start_1
    invoke-virtual {v10}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v8, v14}, Lorg/apache/lucene/search/FieldCache$ShortParser;->parseShort(Ljava/lang/String;)S

    move-result v13

    .line 412
    .local v13, "termval":S
    invoke-interface {v11, v12}, Lorg/apache/lucene/index/TermDocs;->seek(Lorg/apache/lucene/index/TermEnum;)V
    :try_end_1
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v4, v3

    .line 413
    .end local v3    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .local v4, "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :goto_4
    :try_start_2
    invoke-interface {v11}, Lorg/apache/lucene/index/TermDocs;->next()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 414
    invoke-interface {v11}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v2

    .line 415
    .local v2, "docID":I
    aput-short v13, v9, v2

    .line 416
    if-eqz p3, :cond_7

    .line 417
    if-nez v4, :cond_6

    .line 419
    new-instance v3, Lorg/apache/lucene/util/FixedBitSet;

    invoke-direct {v3, v7}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V
    :try_end_2
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 421
    .end local v4    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v3    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :goto_5
    :try_start_3
    invoke-virtual {v3, v2}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V
    :try_end_3
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_6
    move-object v4, v3

    .line 423
    .end local v3    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v4    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_4

    .line 424
    .end local v2    # "docID":I
    :cond_4
    :try_start_4
    invoke-virtual {v12}, Lorg/apache/lucene/index/TermEnum;->next()Z
    :try_end_4
    .catch Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v14

    if-nez v14, :cond_5

    move-object v3, v4

    .end local v4    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v3    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_2

    .line 427
    .end local v10    # "term":Lorg/apache/lucene/index/Term;
    .end local v13    # "termval":S
    :catchall_0
    move-exception v14

    :goto_7
    invoke-interface {v11}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 428
    invoke-virtual {v12}, Lorg/apache/lucene/index/TermEnum;->close()V

    .line 427
    throw v14

    .line 425
    :catch_0
    move-exception v14

    .line 427
    :goto_8
    invoke-interface {v11}, Lorg/apache/lucene/index/TermDocs;->close()V

    goto :goto_3

    .end local v3    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v4    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v10    # "term":Lorg/apache/lucene/index/Term;
    .restart local v13    # "termval":S
    :catchall_1
    move-exception v14

    move-object v3, v4

    .end local v4    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v3    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_7

    .line 425
    .end local v3    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v4    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :catch_1
    move-exception v14

    move-object v3, v4

    .end local v4    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v3    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_8

    .end local v3    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v4    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :cond_5
    move-object v3, v4

    .end local v4    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v3    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_1

    .end local v3    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v2    # "docID":I
    .restart local v4    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :cond_6
    move-object v3, v4

    .end local v4    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v3    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_5

    .end local v3    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v4    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    :cond_7
    move-object v3, v4

    .end local v4    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v3    # "docsWithField":Lorg/apache/lucene/util/FixedBitSet;
    goto :goto_6
.end method
