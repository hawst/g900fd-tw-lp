.class public final Lorg/apache/lucene/search/FieldComparator$DocComparator;
.super Lorg/apache/lucene/search/FieldComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DocComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private bottom:I

.field private docBase:I

.field private final docIDs:[I


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "numHits"    # I

    .prologue
    .line 276
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldComparator;-><init>()V

    .line 277
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docIDs:[I

    .line 278
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 2
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 283
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docIDs:[I

    aget v0, v0, p1

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docIDs:[I

    aget v1, v1, p2

    sub-int/2addr v0, v1

    return v0
.end method

.method public compareBottom(I)I
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 289
    iget v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->bottom:I

    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docBase:I

    add-int/2addr v1, p1

    sub-int/2addr v0, v1

    return v0
.end method

.method public copy(II)V
    .locals 2
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 294
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docIDs:[I

    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docBase:I

    add-int/2addr v1, p2

    aput v1, v0, p1

    .line 295
    return-void
.end method

.method public setBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 307
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docIDs:[I

    aget v0, v0, p1

    iput v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->bottom:I

    .line 308
    return-void
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 0
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I

    .prologue
    .line 302
    iput p2, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docBase:I

    .line 303
    return-void
.end method

.method public value(I)Ljava/lang/Integer;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 312
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$DocComparator;->docIDs:[I

    aget v0, v0, p1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 271
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$DocComparator;->value(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
