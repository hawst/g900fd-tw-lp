.class Lorg/apache/lucene/search/FieldCache$7;
.super Ljava/lang/Object;
.source "FieldCache.java"

# interfaces
.implements Lorg/apache/lucene/search/FieldCache$IntParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public parseInt(Ljava/lang/String;)I
    .locals 2
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 240
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/lit8 v0, v1, -0x60

    .line 241
    .local v0, "shift":I
    if-lez v0, :cond_0

    const/16 v1, 0x1f

    if-gt v0, v1, :cond_0

    .line 242
    new-instance v1, Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException;

    invoke-direct {v1}, Lorg/apache/lucene/search/FieldCacheImpl$StopFillCacheException;-><init>()V

    throw v1

    .line 243
    :cond_0
    invoke-static {p1}, Lorg/apache/lucene/util/NumericUtils;->prefixCodedToInt(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method protected readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 246
    sget-object v0, Lorg/apache/lucene/search/FieldCache;->NUMERIC_UTILS_INT_PARSER:Lorg/apache/lucene/search/FieldCache$IntParser;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lorg/apache/lucene/search/FieldCache;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".NUMERIC_UTILS_INT_PARSER"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
