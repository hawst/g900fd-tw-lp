.class public Lorg/apache/lucene/search/TimeLimitingCollector$TimeExceededException;
.super Ljava/lang/RuntimeException;
.source "TimeLimitingCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TimeLimitingCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TimeExceededException"
.end annotation


# instance fields
.field private lastDocCollected:I

.field private timeAllowed:J

.field private timeElapsed:J


# direct methods
.method private constructor <init>(JJI)V
    .locals 3
    .param p1, "timeAllowed"    # J
    .param p3, "timeElapsed"    # J
    .param p5, "lastDocCollected"    # I

    .prologue
    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Elapsed time: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "Exceeded allowed search time: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ms."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 43
    iput-wide p1, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimeExceededException;->timeAllowed:J

    .line 44
    iput-wide p3, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimeExceededException;->timeElapsed:J

    .line 45
    iput p5, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimeExceededException;->lastDocCollected:I

    .line 46
    return-void
.end method

.method synthetic constructor <init>(JJILorg/apache/lucene/search/TimeLimitingCollector$1;)V
    .locals 1
    .param p1, "x0"    # J
    .param p3, "x1"    # J
    .param p5, "x2"    # I
    .param p6, "x3"    # Lorg/apache/lucene/search/TimeLimitingCollector$1;

    .prologue
    .line 37
    invoke-direct/range {p0 .. p5}, Lorg/apache/lucene/search/TimeLimitingCollector$TimeExceededException;-><init>(JJI)V

    return-void
.end method


# virtual methods
.method public getLastDocCollected()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimeExceededException;->lastDocCollected:I

    return v0
.end method

.method public getTimeAllowed()J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimeExceededException;->timeAllowed:J

    return-wide v0
.end method

.method public getTimeElapsed()J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lorg/apache/lucene/search/TimeLimitingCollector$TimeExceededException;->timeElapsed:J

    return-wide v0
.end method
