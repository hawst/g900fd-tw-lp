.class Lorg/apache/lucene/search/FieldCacheDocIdSet$1;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "FieldCacheDocIdSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/FieldCacheDocIdSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private doc:I

.field final synthetic this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

.field final synthetic val$maxDoc:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/FieldCacheDocIdSet;I)V
    .locals 1

    .prologue
    .line 76
    iput-object p1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iput p2, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->val$maxDoc:I

    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I

    .prologue
    .line 77
    iput p1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    :goto_0
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->val$maxDoc:I

    if-ge v0, v1, :cond_1

    .line 78
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/FieldCacheDocIdSet;->matchDoc(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    .line 82
    :goto_1
    return v0

    .line 77
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    goto :goto_0

    .line 82
    :cond_1
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    goto :goto_1
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    return v0
.end method

.method public nextDoc()I
    .locals 2

    .prologue
    .line 67
    :cond_0
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    .line 68
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->val$maxDoc:I

    if-lt v0, v1, :cond_1

    .line 69
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    .line 72
    :goto_0
    return v0

    .line 71
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->this$0:Lorg/apache/lucene/search/FieldCacheDocIdSet;

    iget v1, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/FieldCacheDocIdSet;->matchDoc(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    iget v0, p0, Lorg/apache/lucene/search/FieldCacheDocIdSet$1;->doc:I

    goto :goto_0
.end method
