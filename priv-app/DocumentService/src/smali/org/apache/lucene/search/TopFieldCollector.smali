.class public abstract Lorg/apache/lucene/search/TopFieldCollector;
.super Lorg/apache/lucene/search/TopDocsCollector;
.source "TopFieldCollector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/TopFieldCollector$1;,
        Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;,
        Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/TopDocsCollector",
        "<",
        "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
        ">;"
    }
.end annotation


# static fields
.field private static final EMPTY_SCOREDOCS:[Lorg/apache/lucene/search/ScoreDoc;


# instance fields
.field bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

.field docBase:I

.field private final fillFields:Z

.field maxScore:F

.field final numHits:I

.field queueFull:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 842
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/lucene/search/ScoreDoc;

    sput-object v0, Lorg/apache/lucene/search/TopFieldCollector;->EMPTY_SCOREDOCS:[Lorg/apache/lucene/search/ScoreDoc;

    return-void
.end method

.method private constructor <init>(Lorg/apache/lucene/util/PriorityQueue;IZ)V
    .locals 1
    .param p2, "numHits"    # I
    .param p3, "fillFields"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/PriorityQueue",
            "<",
            "Lorg/apache/lucene/search/FieldValueHitQueue$Entry;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 863
    .local p1, "pq":Lorg/apache/lucene/util/PriorityQueue;, "Lorg/apache/lucene/util/PriorityQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/TopDocsCollector;-><init>(Lorg/apache/lucene/util/PriorityQueue;)V

    .line 850
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->maxScore:F

    .line 853
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .line 864
    iput p2, p0, Lorg/apache/lucene/search/TopFieldCollector;->numHits:I

    .line 865
    iput-boolean p3, p0, Lorg/apache/lucene/search/TopFieldCollector;->fillFields:Z

    .line 866
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/PriorityQueue;IZLorg/apache/lucene/search/TopFieldCollector$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/apache/lucene/util/PriorityQueue;
    .param p2, "x1"    # I
    .param p3, "x2"    # Z
    .param p4, "x3"    # Lorg/apache/lucene/search/TopFieldCollector$1;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/search/TopFieldCollector;-><init>(Lorg/apache/lucene/util/PriorityQueue;IZ)V

    return-void
.end method

.method public static create(Lorg/apache/lucene/search/Sort;IZZZZ)Lorg/apache/lucene/search/TopFieldCollector;
    .locals 3
    .param p0, "sort"    # Lorg/apache/lucene/search/Sort;
    .param p1, "numHits"    # I
    .param p2, "fillFields"    # Z
    .param p3, "trackDocScores"    # Z
    .param p4, "trackMaxScore"    # Z
    .param p5, "docsScoredInOrder"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 908
    iget-object v1, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    array-length v1, v1

    if-nez v1, :cond_0

    .line 909
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Sort must contain at least one field"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 912
    :cond_0
    if-gtz p1, :cond_1

    .line 913
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "numHits must be > 0; please use TotalHitCountCollector if you just need the total hit count"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 916
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    invoke-static {v1, p1}, Lorg/apache/lucene/search/FieldValueHitQueue;->create([Lorg/apache/lucene/search/SortField;I)Lorg/apache/lucene/search/FieldValueHitQueue;

    move-result-object v0

    .line 917
    .local v0, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    invoke-virtual {v0}, Lorg/apache/lucene/search/FieldValueHitQueue;->getComparators()[Lorg/apache/lucene/search/FieldComparator;

    move-result-object v1

    array-length v1, v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    .line 918
    if-eqz p5, :cond_4

    .line 919
    if-eqz p4, :cond_2

    .line 920
    new-instance v1, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;

    invoke-direct {v1, v0, p1, p2}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    .line 952
    :goto_0
    return-object v1

    .line 921
    :cond_2
    if-eqz p3, :cond_3

    .line 922
    new-instance v1, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;

    invoke-direct {v1, v0, p1, p2}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorScoringNoMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 924
    :cond_3
    new-instance v1, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;

    invoke-direct {v1, v0, p1, p2}, Lorg/apache/lucene/search/TopFieldCollector$OneComparatorNonScoringCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 927
    :cond_4
    if-eqz p4, :cond_5

    .line 928
    new-instance v1, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringMaxScoreCollector;

    invoke-direct {v1, v0, p1, p2}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 929
    :cond_5
    if-eqz p3, :cond_6

    .line 930
    new-instance v1, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;

    invoke-direct {v1, v0, p1, p2}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorScoringNoMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 932
    :cond_6
    new-instance v1, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;

    invoke-direct {v1, v0, p1, p2}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderOneComparatorNonScoringCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 938
    :cond_7
    if-eqz p5, :cond_a

    .line 939
    if-eqz p4, :cond_8

    .line 940
    new-instance v1, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;

    invoke-direct {v1, v0, p1, p2}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 941
    :cond_8
    if-eqz p3, :cond_9

    .line 942
    new-instance v1, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;

    invoke-direct {v1, v0, p1, p2}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorScoringNoMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 944
    :cond_9
    new-instance v1, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;

    invoke-direct {v1, v0, p1, p2}, Lorg/apache/lucene/search/TopFieldCollector$MultiComparatorNonScoringCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 947
    :cond_a
    if-eqz p4, :cond_b

    .line 948
    new-instance v1, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;

    invoke-direct {v1, v0, p1, p2}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 949
    :cond_b
    if-eqz p3, :cond_c

    .line 950
    new-instance v1, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;

    invoke-direct {v1, v0, p1, p2}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorScoringNoMaxScoreCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0

    .line 952
    :cond_c
    new-instance v1, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;

    invoke-direct {v1, v0, p1, p2}, Lorg/apache/lucene/search/TopFieldCollector$OutOfOrderMultiComparatorNonScoringCollector;-><init>(Lorg/apache/lucene/search/FieldValueHitQueue;IZ)V

    goto :goto_0
.end method


# virtual methods
.method public acceptsDocsOutOfOrder()Z
    .locals 1

    .prologue
    .line 997
    const/4 v0, 0x0

    return v0
.end method

.method final add(IIF)V
    .locals 3
    .param p1, "slot"    # I
    .param p2, "doc"    # I
    .param p3, "score"    # F

    .prologue
    .line 958
    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    new-instance v1, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector;->docBase:I

    add-int/2addr v2, p2

    invoke-direct {v1, p1, v2, p3}, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;-><init>(IIF)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/PriorityQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    iput-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->bottom:Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .line 959
    iget v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->totalHits:I

    iget v1, p0, Lorg/apache/lucene/search/TopFieldCollector;->numHits:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->queueFull:Z

    .line 960
    return-void

    .line 959
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected newTopDocs([Lorg/apache/lucene/search/ScoreDoc;I)Lorg/apache/lucene/search/TopDocs;
    .locals 4
    .param p1, "results"    # [Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "start"    # I

    .prologue
    .line 985
    if-nez p1, :cond_0

    .line 986
    sget-object p1, Lorg/apache/lucene/search/TopFieldCollector;->EMPTY_SCOREDOCS:[Lorg/apache/lucene/search/ScoreDoc;

    .line 988
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->maxScore:F

    .line 992
    :cond_0
    new-instance v1, Lorg/apache/lucene/search/TopFieldDocs;

    iget v2, p0, Lorg/apache/lucene/search/TopFieldCollector;->totalHits:I

    iget-object v0, p0, Lorg/apache/lucene/search/TopFieldCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    check-cast v0, Lorg/apache/lucene/search/FieldValueHitQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/search/FieldValueHitQueue;->getFields()[Lorg/apache/lucene/search/SortField;

    move-result-object v0

    iget v3, p0, Lorg/apache/lucene/search/TopFieldCollector;->maxScore:F

    invoke-direct {v1, v2, p1, v0, v3}, Lorg/apache/lucene/search/TopFieldDocs;-><init>(I[Lorg/apache/lucene/search/ScoreDoc;[Lorg/apache/lucene/search/SortField;F)V

    return-object v1
.end method

.method protected populateResults([Lorg/apache/lucene/search/ScoreDoc;I)V
    .locals 6
    .param p1, "results"    # [Lorg/apache/lucene/search/ScoreDoc;
    .param p2, "howMany"    # I

    .prologue
    .line 969
    iget-boolean v3, p0, Lorg/apache/lucene/search/TopFieldCollector;->fillFields:Z

    if-eqz v3, :cond_0

    .line 971
    iget-object v2, p0, Lorg/apache/lucene/search/TopFieldCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    check-cast v2, Lorg/apache/lucene/search/FieldValueHitQueue;

    .line 972
    .local v2, "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    add-int/lit8 v1, p2, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 973
    invoke-virtual {v2}, Lorg/apache/lucene/search/FieldValueHitQueue;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/FieldValueHitQueue;->fillFields(Lorg/apache/lucene/search/FieldValueHitQueue$Entry;)Lorg/apache/lucene/search/FieldDoc;

    move-result-object v3

    aput-object v3, p1, v1

    .line 972
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 976
    .end local v1    # "i":I
    .end local v2    # "queue":Lorg/apache/lucene/search/FieldValueHitQueue;, "Lorg/apache/lucene/search/FieldValueHitQueue<Lorg/apache/lucene/search/FieldValueHitQueue$Entry;>;"
    :cond_0
    add-int/lit8 v1, p2, -0x1

    .restart local v1    # "i":I
    :goto_1
    if-ltz v1, :cond_1

    .line 977
    iget-object v3, p0, Lorg/apache/lucene/search/TopFieldCollector;->pq:Lorg/apache/lucene/util/PriorityQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/util/PriorityQueue;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;

    .line 978
    .local v0, "entry":Lorg/apache/lucene/search/FieldValueHitQueue$Entry;
    new-instance v3, Lorg/apache/lucene/search/FieldDoc;

    iget v4, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->doc:I

    iget v5, v0, Lorg/apache/lucene/search/FieldValueHitQueue$Entry;->score:F

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/search/FieldDoc;-><init>(IF)V

    aput-object v3, p1, v1

    .line 976
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 981
    .end local v0    # "entry":Lorg/apache/lucene/search/FieldValueHitQueue$Entry;
    :cond_1
    return-void
.end method
