.class final Lorg/apache/lucene/search/BooleanScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "BooleanScorer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/BooleanScorer$SubScorer;,
        Lorg/apache/lucene/search/BooleanScorer$BucketTable;,
        Lorg/apache/lucene/search/BooleanScorer$Bucket;,
        Lorg/apache/lucene/search/BooleanScorer$BucketScorer;,
        Lorg/apache/lucene/search/BooleanScorer$BooleanScorerCollector;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final PROHIBITED_MASK:I = 0x1


# instance fields
.field private bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

.field private final coordFactors:[F

.field private current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

.field private end:I

.field private final minNrShouldMatch:I

.field private scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lorg/apache/lucene/search/BooleanScorer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/BooleanScorer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/Weight;ZLorg/apache/lucene/search/Similarity;ILjava/util/List;Ljava/util/List;I)V
    .locals 8
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "disableCoord"    # Z
    .param p3, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p4, "minNrShouldMatch"    # I
    .param p7, "maxCoord"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Weight;",
            "Z",
            "Lorg/apache/lucene/search/Similarity;",
            "I",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/Scorer;",
            ">;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    .local p5, "optionalScorers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    .local p6, "prohibitedScorers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/Scorer;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 195
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    .line 196
    new-instance v0, Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    invoke-direct {v0}, Lorg/apache/lucene/search/BooleanScorer$BucketTable;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    .line 209
    iput p4, p0, Lorg/apache/lucene/search/BooleanScorer;->minNrShouldMatch:I

    .line 211
    if-eqz p5, :cond_1

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 212
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Scorer;

    .line 213
    .local v1, "scorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    const v2, 0x7fffffff

    if-eq v0, v2, :cond_0

    .line 214
    new-instance v0, Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->newCollector(I)Lorg/apache/lucene/search/Collector;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/BooleanScorer$SubScorer;-><init>(Lorg/apache/lucene/search/Scorer;ZZLorg/apache/lucene/search/Collector;Lorg/apache/lucene/search/BooleanScorer$SubScorer;)V

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    goto :goto_0

    .line 219
    .end local v1    # "scorer":Lorg/apache/lucene/search/Scorer;
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_1
    if-eqz p6, :cond_3

    invoke-interface {p6}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 220
    invoke-interface {p6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/search/Scorer;

    .line 221
    .restart local v1    # "scorer":Lorg/apache/lucene/search/Scorer;
    invoke-virtual {v1}, Lorg/apache/lucene/search/Scorer;->nextDoc()I

    move-result v0

    const v2, 0x7fffffff

    if-eq v0, v2, :cond_2

    .line 222
    new-instance v0, Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->newCollector(I)Lorg/apache/lucene/search/Collector;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/search/BooleanScorer$SubScorer;-><init>(Lorg/apache/lucene/search/Scorer;ZZLorg/apache/lucene/search/Collector;Lorg/apache/lucene/search/BooleanScorer$SubScorer;)V

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    goto :goto_1

    .line 227
    .end local v1    # "scorer":Lorg/apache/lucene/search/Scorer;
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/lucene/search/BooleanScorer;->coordFactors:[F

    .line 228
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    iget-object v0, p0, Lorg/apache/lucene/search/BooleanScorer;->coordFactors:[F

    array-length v0, v0

    if-ge v6, v0, :cond_5

    .line 229
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer;->coordFactors:[F

    if-eqz p2, :cond_4

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_3
    aput v0, v2, v6

    .line 228
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 229
    :cond_4
    invoke-virtual {p3, v6, p7}, Lorg/apache/lucene/search/Similarity;->coord(II)F

    move-result v0

    goto :goto_3

    .line 231
    :cond_5
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 300
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 305
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 310
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public score()F
    .locals 1

    .prologue
    .line 315
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public score(Lorg/apache/lucene/search/Collector;)V
    .locals 2
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 320
    const v0, 0x7fffffff

    const/4 v1, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/search/BooleanScorer;->score(Lorg/apache/lucene/search/Collector;II)Z

    .line 321
    return-void
.end method

.method protected score(Lorg/apache/lucene/search/Collector;II)Z
    .locals 8
    .param p1, "collector"    # Lorg/apache/lucene/search/Collector;
    .param p2, "max"    # I
    .param p3, "firstDocID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 237
    sget-boolean v5, Lorg/apache/lucene/search/BooleanScorer;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    const/4 v5, -0x1

    if-eq p3, v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 240
    :cond_0
    new-instance v0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;

    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-direct {v0, v5}, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;-><init>(Lorg/apache/lucene/search/Weight;)V

    .line 243
    .local v0, "bs":Lorg/apache/lucene/search/BooleanScorer$BucketScorer;
    invoke-virtual {p1, v0}, Lorg/apache/lucene/search/Collector;->setScorer(Lorg/apache/lucene/search/Scorer;)V

    .line 245
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    const/4 v6, 0x0

    iput-object v6, v5, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 247
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    if-eqz v5, :cond_4

    .line 250
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->bits:I

    and-int/lit8 v5, v5, 0x1

    if-nez v5, :cond_3

    .line 257
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->doc:I

    if-lt v5, p2, :cond_2

    .line 258
    iget-object v4, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 259
    .local v4, "tmp":Lorg/apache/lucene/search/BooleanScorer$Bucket;
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget-object v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->next:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 260
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    iget-object v5, v5, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v5, v4, Lorg/apache/lucene/search/BooleanScorer$Bucket;->next:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 261
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    iput-object v4, v5, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    goto :goto_0

    .line 265
    .end local v4    # "tmp":Lorg/apache/lucene/search/BooleanScorer$Bucket;
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->coord:I

    iget v6, p0, Lorg/apache/lucene/search/BooleanScorer;->minNrShouldMatch:I

    if-lt v5, v6, :cond_3

    .line 266
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->score:F

    iget-object v6, p0, Lorg/apache/lucene/search/BooleanScorer;->coordFactors:[F

    iget-object v7, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v7, v7, Lorg/apache/lucene/search/BooleanScorer$Bucket;->coord:I

    aget v6, v6, v7

    mul-float/2addr v5, v6

    iput v5, v0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;->score:F

    .line 267
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->doc:I

    iput v5, v0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;->doc:I

    .line 268
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->coord:I

    iput v5, v0, Lorg/apache/lucene/search/BooleanScorer$BucketScorer;->freq:I

    .line 269
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->doc:I

    invoke-virtual {p1, v5}, Lorg/apache/lucene/search/Collector;->collect(I)V

    .line 273
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget-object v5, v5, Lorg/apache/lucene/search/BooleanScorer$Bucket;->next:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    goto :goto_0

    .line 276
    :cond_4
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    iget-object v5, v5, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    if-eqz v5, :cond_5

    .line 277
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    iget-object v5, v5, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 278
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    iget-object v6, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iget-object v6, v6, Lorg/apache/lucene/search/BooleanScorer$Bucket;->next:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v6, v5, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 279
    const/4 v5, 0x1

    .line 295
    :goto_1
    return v5

    .line 283
    :cond_5
    const/4 v1, 0x0

    .line 284
    .local v1, "more":Z
    iget v5, p0, Lorg/apache/lucene/search/BooleanScorer;->end:I

    add-int/lit16 v5, v5, 0x800

    iput v5, p0, Lorg/apache/lucene/search/BooleanScorer;->end:I

    .line 285
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    .local v2, "sub":Lorg/apache/lucene/search/BooleanScorer$SubScorer;
    :goto_2
    if-eqz v2, :cond_7

    .line 286
    iget-object v5, v2, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v5}, Lorg/apache/lucene/search/Scorer;->docID()I

    move-result v3

    .line 287
    .local v3, "subScorerDocID":I
    const v5, 0x7fffffff

    if-eq v3, v5, :cond_6

    .line 288
    iget-object v5, v2, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    iget-object v6, v2, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->collector:Lorg/apache/lucene/search/Collector;

    iget v7, p0, Lorg/apache/lucene/search/BooleanScorer;->end:I

    invoke-virtual {v5, v6, v7, v3}, Lorg/apache/lucene/search/Scorer;->score(Lorg/apache/lucene/search/Collector;II)Z

    move-result v5

    or-int/2addr v1, v5

    .line 285
    :cond_6
    iget-object v2, v2, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->next:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    goto :goto_2

    .line 291
    .end local v3    # "subScorerDocID":I
    :cond_7
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->bucketTable:Lorg/apache/lucene/search/BooleanScorer$BucketTable;

    iget-object v5, v5, Lorg/apache/lucene/search/BooleanScorer$BucketTable;->first:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    iput-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    .line 293
    iget-object v5, p0, Lorg/apache/lucene/search/BooleanScorer;->current:Lorg/apache/lucene/search/BooleanScorer$Bucket;

    if-nez v5, :cond_1

    if-nez v1, :cond_1

    .line 295
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 325
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 326
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const-string/jumbo v2, "boolean("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    .local v1, "sub":Lorg/apache/lucene/search/BooleanScorer$SubScorer;
    :goto_0
    if-eqz v1, :cond_0

    .line 328
    iget-object v2, v1, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    iget-object v1, v1, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->next:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    goto :goto_0

    .line 331
    :cond_0
    const-string/jumbo v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method protected visitSubScorers(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/search/Scorer$ScorerVisitor;)V
    .locals 3
    .param p1, "parent"    # Lorg/apache/lucene/search/Query;
    .param p2, "relationship"    # Lorg/apache/lucene/search/BooleanClause$Occur;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/Query;",
            "Lorg/apache/lucene/search/BooleanClause$Occur;",
            "Lorg/apache/lucene/search/Scorer$ScorerVisitor",
            "<",
            "Lorg/apache/lucene/search/Query;",
            "Lorg/apache/lucene/search/Query;",
            "Lorg/apache/lucene/search/Scorer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 337
    .local p3, "visitor":Lorg/apache/lucene/search/Scorer$ScorerVisitor;, "Lorg/apache/lucene/search/Scorer$ScorerVisitor<Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/Scorer;>;"
    invoke-super {p0, p1, p2, p3}, Lorg/apache/lucene/search/Scorer;->visitSubScorers(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/search/Scorer$ScorerVisitor;)V

    .line 338
    iget-object v2, p0, Lorg/apache/lucene/search/BooleanScorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Weight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v0

    .line 339
    .local v0, "q":Lorg/apache/lucene/search/Query;
    iget-object v1, p0, Lorg/apache/lucene/search/BooleanScorer;->scorers:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    .line 340
    .local v1, "sub":Lorg/apache/lucene/search/BooleanScorer$SubScorer;
    :goto_0
    if-eqz v1, :cond_1

    .line 345
    iget-boolean v2, v1, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->prohibited:Z

    if-nez v2, :cond_0

    .line 346
    sget-object p2, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 353
    :goto_1
    iget-object v2, v1, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->scorer:Lorg/apache/lucene/search/Scorer;

    invoke-virtual {v2, v0, p2, p3}, Lorg/apache/lucene/search/Scorer;->visitSubScorers(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/search/BooleanClause$Occur;Lorg/apache/lucene/search/Scorer$ScorerVisitor;)V

    .line 354
    iget-object v1, v1, Lorg/apache/lucene/search/BooleanScorer$SubScorer;->next:Lorg/apache/lucene/search/BooleanScorer$SubScorer;

    goto :goto_0

    .line 351
    :cond_0
    sget-object p2, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    goto :goto_1

    .line 356
    :cond_1
    return-void
.end method
