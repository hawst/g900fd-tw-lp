.class public enum Lorg/apache/lucene/search/BooleanClause$Occur;
.super Ljava/lang/Enum;
.source "BooleanClause.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/BooleanClause;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4009
    name = "Occur"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/search/BooleanClause$Occur;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/lucene/search/BooleanClause$Occur;

.field public static final enum MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

.field public static final enum MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

.field public static final enum SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-instance v0, Lorg/apache/lucene/search/BooleanClause$Occur$1;

    const-string/jumbo v1, "MUST"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/BooleanClause$Occur$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 35
    new-instance v0, Lorg/apache/lucene/search/BooleanClause$Occur$2;

    const-string/jumbo v1, "SHOULD"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/search/BooleanClause$Occur$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 40
    new-instance v0, Lorg/apache/lucene/search/BooleanClause$Occur$3;

    const-string/jumbo v1, "MUST_NOT"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/search/BooleanClause$Occur$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    .line 24
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/lucene/search/BooleanClause$Occur;

    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST:Lorg/apache/lucene/search/BooleanClause$Occur;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->SHOULD:Lorg/apache/lucene/search/BooleanClause$Occur;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/search/BooleanClause$Occur;->MUST_NOT:Lorg/apache/lucene/search/BooleanClause$Occur;

    aput-object v1, v0, v4

    sput-object v0, Lorg/apache/lucene/search/BooleanClause$Occur;->$VALUES:[Lorg/apache/lucene/search/BooleanClause$Occur;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILorg/apache/lucene/search/BooleanClause$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lorg/apache/lucene/search/BooleanClause$1;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/search/BooleanClause$Occur;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/search/BooleanClause$Occur;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/BooleanClause$Occur;

    return-object v0
.end method

.method public static final values()[Lorg/apache/lucene/search/BooleanClause$Occur;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lorg/apache/lucene/search/BooleanClause$Occur;->$VALUES:[Lorg/apache/lucene/search/BooleanClause$Occur;

    invoke-virtual {v0}, [Lorg/apache/lucene/search/BooleanClause$Occur;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/search/BooleanClause$Occur;

    return-object v0
.end method
