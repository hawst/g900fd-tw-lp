.class public Lorg/apache/lucene/search/SearcherLifetimeManager;
.super Ljava/lang/Object;
.source "SearcherLifetimeManager.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/SearcherLifetimeManager$PruneByAge;,
        Lorg/apache/lucene/search/SearcherLifetimeManager$Pruner;,
        Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    }
.end annotation


# static fields
.field static final NANOS_PER_SEC:D = 1.0E9


# instance fields
.field private volatile closed:Z

.field private final searchers:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    .line 231
    return-void
.end method

.method private ensureOpen()V
    .locals 2

    .prologue
    .line 147
    iget-boolean v0, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->closed:Z

    if-eqz v0, :cond_0

    .line 148
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string/jumbo v1, "this SearcherLifetimeManager instance is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150
    :cond_0
    return-void
.end method


# virtual methods
.method public acquire(J)Lorg/apache/lucene/search/IndexSearcher;
    .locals 3
    .param p1, "version"    # J

    .prologue
    .line 200
    invoke-direct {p0}, Lorg/apache/lucene/search/SearcherLifetimeManager;->ensureOpen()V

    .line 201
    iget-object v1, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;

    .line 202
    .local v0, "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->tryIncRef()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 204
    iget-object v1, v0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    .line 207
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public declared-synchronized close()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 295
    monitor-enter p0

    const/4 v3, 0x1

    :try_start_0
    iput-boolean v3, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->closed:Z

    .line 296
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 300
    .local v1, "toClose":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;

    .line 301
    .local v2, "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    iget-object v3, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    iget-wide v4, v2, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->version:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 295
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "toClose":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;>;"
    .end local v2    # "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 304
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "toClose":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;>;"
    :cond_0
    :try_start_1
    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->close(Ljava/lang/Iterable;)V

    .line 307
    iget-object v3, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v3

    if-eqz v3, :cond_1

    .line 308
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string/jumbo v4, "another thread called record while this SearcherLifetimeManager instance was being closed; not all searchers were closed"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 310
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized prune(Lorg/apache/lucene/search/SearcherLifetimeManager$Pruner;)V
    .locals 14
    .param p1, "pruner"    # Lorg/apache/lucene/search/SearcherLifetimeManager$Pruner;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 258
    monitor-enter p0

    :try_start_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 259
    .local v8, "trackers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;>;"
    iget-object v9, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v9}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;

    .line 260
    .local v3, "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 258
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    .end local v8    # "trackers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;>;"
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 262
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v8    # "trackers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;>;"
    :cond_0
    :try_start_1
    invoke-static {v8}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 263
    const-wide/16 v4, 0x0

    .line 264
    .local v4, "lastRecordTimeSec":D
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    long-to-double v10, v10

    const-wide v12, 0x41cdcd6500000000L    # 1.0E9

    div-double v6, v10, v12

    .line 265
    .local v6, "now":D
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;

    .line 267
    .restart local v3    # "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    const-wide/16 v10, 0x0

    cmpl-double v9, v4, v10

    if-nez v9, :cond_2

    .line 268
    const-wide/16 v0, 0x0

    .line 276
    .local v0, "ageSec":D
    :goto_2
    iget-object v9, v3, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-interface {p1, v0, v1, v9}, Lorg/apache/lucene/search/SearcherLifetimeManager$Pruner;->doPrune(DLorg/apache/lucene/search/IndexSearcher;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 278
    iget-object v9, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    iget-wide v10, v3, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->version:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    invoke-virtual {v3}, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->close()V

    .line 281
    :cond_1
    iget-wide v4, v3, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->recordTimeSec:D
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 282
    goto :goto_1

    .line 270
    .end local v0    # "ageSec":D
    :cond_2
    sub-double v0, v6, v4

    .restart local v0    # "ageSec":D
    goto :goto_2

    .line 283
    .end local v0    # "ageSec":D
    .end local v3    # "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    :cond_3
    monitor-exit p0

    return-void
.end method

.method public record(Lorg/apache/lucene/search/IndexSearcher;)J
    .locals 6
    .param p1, "searcher"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    invoke-direct {p0}, Lorg/apache/lucene/search/SearcherLifetimeManager;->ensureOpen()V

    .line 170
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexReader;->getVersion()J

    move-result-wide v2

    .line 171
    .local v2, "version":J
    iget-object v1, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;

    .line 172
    .local v0, "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    if-nez v0, :cond_1

    .line 174
    new-instance v0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;

    .end local v0    # "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    invoke-direct {v0, p1}, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;-><init>(Lorg/apache/lucene/search/IndexSearcher;)V

    .line 175
    .restart local v0    # "tracker":Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;
    iget-object v1, p0, Lorg/apache/lucene/search/SearcherLifetimeManager;->searchers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 178
    invoke-virtual {v0}, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->close()V

    .line 184
    :cond_0
    return-wide v2

    .line 180
    :cond_1
    iget-object v1, v0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    if-eq v1, p1, :cond_0

    .line 181
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "the provided searcher has the same underlying reader version yet the searcher instance differs from before (new="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " vs old="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lorg/apache/lucene/search/SearcherLifetimeManager$SearcherTracker;->searcher:Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public release(Lorg/apache/lucene/search/IndexSearcher;)V
    .locals 1
    .param p1, "s"    # Lorg/apache/lucene/search/IndexSearcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 215
    invoke-virtual {p1}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 216
    return-void
.end method
