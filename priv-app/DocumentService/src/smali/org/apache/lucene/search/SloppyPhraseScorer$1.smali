.class Lorg/apache/lucene/search/SloppyPhraseScorer$1;
.super Ljava/lang/Object;
.source "SloppyPhraseScorer.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/SloppyPhraseScorer;->sortRptGroups(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/search/PhrasePositions;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/SloppyPhraseScorer;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/SloppyPhraseScorer;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lorg/apache/lucene/search/SloppyPhraseScorer$1;->this$0:Lorg/apache/lucene/search/SloppyPhraseScorer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 328
    check-cast p1, Lorg/apache/lucene/search/PhrasePositions;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/search/PhrasePositions;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/SloppyPhraseScorer$1;->compare(Lorg/apache/lucene/search/PhrasePositions;Lorg/apache/lucene/search/PhrasePositions;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/search/PhrasePositions;Lorg/apache/lucene/search/PhrasePositions;)I
    .locals 2
    .param p1, "pp1"    # Lorg/apache/lucene/search/PhrasePositions;
    .param p2, "pp2"    # Lorg/apache/lucene/search/PhrasePositions;

    .prologue
    .line 329
    iget v0, p1, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    iget v1, p2, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    sub-int/2addr v0, v1

    return v0
.end method
