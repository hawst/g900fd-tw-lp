.class Lorg/apache/lucene/search/TermQuery$TermWeight;
.super Lorg/apache/lucene/search/Weight;
.source "TermQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/TermQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TermWeight"
.end annotation


# instance fields
.field private final hash:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private idf:F

.field private idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

.field private queryNorm:F

.field private queryWeight:F

.field private final similarity:Lorg/apache/lucene/search/Similarity;

.field final synthetic this$0:Lorg/apache/lucene/search/TermQuery;

.field private value:F


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/TermQuery;Lorg/apache/lucene/search/Searcher;)V
    .locals 5
    .param p2, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iput-object p1, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    invoke-direct {p0}, Lorg/apache/lucene/search/Weight;-><init>()V

    .line 48
    invoke-virtual {p1, p2}, Lorg/apache/lucene/search/TermQuery;->getSimilarity(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Similarity;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    .line 49
    instance-of v2, p2, Lorg/apache/lucene/search/IndexSearcher;

    if-eqz v2, :cond_0

    .line 50
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->hash:Ljava/util/Set;

    move-object v2, p2

    .line 51
    check-cast v2, Lorg/apache/lucene/search/IndexSearcher;

    invoke-virtual {v2}, Lorg/apache/lucene/search/IndexSearcher;->getIndexReader()Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    .line 52
    .local v1, "ir":Lorg/apache/lucene/index/IndexReader;
    const/4 v2, 0x1

    new-array v0, v2, [I

    .line 53
    .local v0, "dfSum":[I
    new-instance v2, Lorg/apache/lucene/search/TermQuery$TermWeight$1;

    invoke-direct {v2, p0, v1, p1, v0}, Lorg/apache/lucene/search/TermQuery$TermWeight$1;-><init>(Lorg/apache/lucene/search/TermQuery$TermWeight;Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/TermQuery;[I)V

    invoke-virtual {v2}, Lorg/apache/lucene/search/TermQuery$TermWeight$1;->run()I

    .line 64
    iget-object v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {p1}, Lorg/apache/lucene/search/TermQuery;->access$000(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v3

    const/4 v4, 0x0

    aget v4, v0, v4

    invoke-virtual {v2, v3, p2, v4}, Lorg/apache/lucene/search/Similarity;->idfExplain(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/search/Searcher;I)Lorg/apache/lucene/search/Explanation$IDFExplanation;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

    .line 70
    .end local v0    # "dfSum":[I
    .end local v1    # "ir":Lorg/apache/lucene/index/IndexReader;
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

    invoke-virtual {v2}, Lorg/apache/lucene/search/Explanation$IDFExplanation;->getIdf()F

    move-result v2

    iput v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->idf:F

    .line 71
    return-void

    .line 66
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {p1}, Lorg/apache/lucene/search/TermQuery;->access$000(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Lorg/apache/lucene/search/Similarity;->idfExplain(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Explanation$IDFExplanation;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

    .line 67
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->hash:Ljava/util/Set;

    goto :goto_0
.end method

.method static synthetic access$100(Lorg/apache/lucene/search/TermQuery$TermWeight;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/TermQuery$TermWeight;

    .prologue
    .line 37
    iget-object v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->hash:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public explain(Lorg/apache/lucene/index/IndexReader;I)Lorg/apache/lucene/search/Explanation;
    .locals 16
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    new-instance v10, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v10}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 115
    .local v10, "result":Lorg/apache/lucene/search/ComplexExplanation;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "weight("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/search/TermQuery$TermWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " in "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "), product of:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 117
    new-instance v2, Lorg/apache/lucene/search/Explanation;

    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/lucene/search/TermQuery$TermWeight;->idf:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/search/TermQuery$TermWeight;->idfExp:Lorg/apache/lucene/search/Explanation$IDFExplanation;

    invoke-virtual {v15}, Lorg/apache/lucene/search/Explanation$IDFExplanation;->explain()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v2, v14, v15}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 120
    .local v2, "expl":Lorg/apache/lucene/search/Explanation;
    new-instance v8, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v8}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 121
    .local v8, "queryExpl":Lorg/apache/lucene/search/Explanation;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "queryWeight("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/search/TermQuery$TermWeight;->getQuery()Lorg/apache/lucene/search/Query;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "), product of:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v14}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 123
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    invoke-virtual {v14}, Lorg/apache/lucene/search/TermQuery;->getBoost()F

    move-result v14

    const-string/jumbo v15, "boost"

    invoke-direct {v1, v14, v15}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 124
    .local v1, "boostExpl":Lorg/apache/lucene/search/Explanation;
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    invoke-virtual {v14}, Lorg/apache/lucene/search/TermQuery;->getBoost()F

    move-result v14

    const/high16 v15, 0x3f800000    # 1.0f

    cmpl-float v14, v14, v15

    if-eqz v14, :cond_0

    .line 125
    invoke-virtual {v8, v1}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 126
    :cond_0
    invoke-virtual {v8, v2}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 128
    new-instance v9, Lorg/apache/lucene/search/Explanation;

    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/lucene/search/TermQuery$TermWeight;->queryNorm:F

    const-string/jumbo v15, "queryNorm"

    invoke-direct {v9, v14, v15}, Lorg/apache/lucene/search/Explanation;-><init>(FLjava/lang/String;)V

    .line 129
    .local v9, "queryNormExpl":Lorg/apache/lucene/search/Explanation;
    invoke-virtual {v8, v9}, Lorg/apache/lucene/search/Explanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 131
    invoke-virtual {v1}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v14

    invoke-virtual {v2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v15

    mul-float/2addr v14, v15

    invoke-virtual {v9}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v15

    mul-float/2addr v14, v15

    invoke-virtual {v8, v14}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 135
    invoke-virtual {v10, v8}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 138
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v14}, Lorg/apache/lucene/search/TermQuery;->access$000(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v14

    invoke-virtual {v14}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v3

    .line 139
    .local v3, "field":Ljava/lang/String;
    new-instance v4, Lorg/apache/lucene/search/ComplexExplanation;

    invoke-direct {v4}, Lorg/apache/lucene/search/ComplexExplanation;-><init>()V

    .line 140
    .local v4, "fieldExpl":Lorg/apache/lucene/search/ComplexExplanation;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "fieldWeight("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v15}, Lorg/apache/lucene/search/TermQuery;->access$000(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, " in "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "), product of:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v14}, Lorg/apache/lucene/search/ComplexExplanation;->setDescription(Ljava/lang/String;)V

    .line 143
    new-instance v13, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v13}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 144
    .local v13, "tfExplanation":Lorg/apache/lucene/search/Explanation;
    const/4 v12, 0x0

    .line 145
    .local v12, "tf":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v14}, Lorg/apache/lucene/search/TermQuery;->access$000(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/IndexReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v11

    .line 146
    .local v11, "termDocs":Lorg/apache/lucene/index/TermDocs;
    if-eqz v11, :cond_2

    .line 148
    :try_start_0
    move/from16 v0, p2

    invoke-interface {v11, v0}, Lorg/apache/lucene/index/TermDocs;->skipTo(I)Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-interface {v11}, Lorg/apache/lucene/index/TermDocs;->doc()I

    move-result v14

    move/from16 v0, p2

    if-ne v14, v0, :cond_1

    .line 149
    invoke-interface {v11}, Lorg/apache/lucene/index/TermDocs;->freq()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v12

    .line 152
    :cond_1
    invoke-interface {v11}, Lorg/apache/lucene/index/TermDocs;->close()V

    .line 154
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/search/TermQuery$TermWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    invoke-virtual {v14, v12}, Lorg/apache/lucene/search/Similarity;->tf(I)F

    move-result v14

    invoke-virtual {v13, v14}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 155
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "tf(termFreq("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v15}, Lorg/apache/lucene/search/TermQuery;->access$000(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ")="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 160
    :goto_0
    invoke-virtual {v4, v13}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 161
    invoke-virtual {v4, v2}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 163
    new-instance v6, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v6}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 164
    .local v6, "fieldNormExpl":Lorg/apache/lucene/search/Explanation;
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v7

    .line 165
    .local v7, "fieldNorms":[B
    if-eqz v7, :cond_3

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/search/TermQuery$TermWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    aget-byte v15, v7, p2

    invoke-virtual {v14, v15}, Lorg/apache/lucene/search/Similarity;->decodeNormValue(B)F

    move-result v5

    .line 167
    .local v5, "fieldNorm":F
    :goto_1
    invoke-virtual {v6, v5}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 168
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "fieldNorm(field="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ", doc="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 169
    invoke-virtual {v4, v6}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 171
    invoke-virtual {v13}, Lorg/apache/lucene/search/Explanation;->isMatch()Z

    move-result v14

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    invoke-virtual {v4, v14}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 172
    invoke-virtual {v13}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v14

    invoke-virtual {v2}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v15

    mul-float/2addr v14, v15

    invoke-virtual {v6}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v15

    mul-float/2addr v14, v15

    invoke-virtual {v4, v14}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 176
    invoke-virtual {v10, v4}, Lorg/apache/lucene/search/ComplexExplanation;->addDetail(Lorg/apache/lucene/search/Explanation;)V

    .line 177
    invoke-virtual {v4}, Lorg/apache/lucene/search/ComplexExplanation;->getMatch()Ljava/lang/Boolean;

    move-result-object v14

    invoke-virtual {v10, v14}, Lorg/apache/lucene/search/ComplexExplanation;->setMatch(Ljava/lang/Boolean;)V

    .line 180
    invoke-virtual {v8}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v14

    invoke-virtual {v4}, Lorg/apache/lucene/search/ComplexExplanation;->getValue()F

    move-result v15

    mul-float/2addr v14, v15

    invoke-virtual {v10, v14}, Lorg/apache/lucene/search/ComplexExplanation;->setValue(F)V

    .line 182
    invoke-virtual {v8}, Lorg/apache/lucene/search/Explanation;->getValue()F

    move-result v14

    const/high16 v15, 0x3f800000    # 1.0f

    cmpl-float v14, v14, v15

    if-nez v14, :cond_4

    .line 185
    .end local v4    # "fieldExpl":Lorg/apache/lucene/search/ComplexExplanation;
    :goto_2
    return-object v4

    .line 152
    .end local v5    # "fieldNorm":F
    .end local v6    # "fieldNormExpl":Lorg/apache/lucene/search/Explanation;
    .end local v7    # "fieldNorms":[B
    .restart local v4    # "fieldExpl":Lorg/apache/lucene/search/ComplexExplanation;
    :catchall_0
    move-exception v14

    invoke-interface {v11}, Lorg/apache/lucene/index/TermDocs;->close()V

    throw v14

    .line 157
    :cond_2
    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 158
    const-string/jumbo v14, "no matching term"

    invoke-virtual {v13, v14}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 165
    .restart local v6    # "fieldNormExpl":Lorg/apache/lucene/search/Explanation;
    .restart local v7    # "fieldNorms":[B
    :cond_3
    const/high16 v5, 0x3f800000    # 1.0f

    goto/16 :goto_1

    .restart local v5    # "fieldNorm":F
    :cond_4
    move-object v4, v10

    .line 185
    goto :goto_2
.end method

.method public getQuery()Lorg/apache/lucene/search/Query;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->value:F

    return v0
.end method

.method public normalize(F)V
    .locals 2
    .param p1, "queryNorm"    # F

    .prologue
    .line 90
    iput p1, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->queryNorm:F

    .line 91
    iget v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->queryWeight:F

    mul-float/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->queryWeight:F

    .line 92
    iget v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->idf:F

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->value:F

    .line 93
    return-void
.end method

.method public scorer(Lorg/apache/lucene/index/IndexReader;ZZ)Lorg/apache/lucene/search/Scorer;
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "scoreDocsInOrder"    # Z
    .param p3, "topScorer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 98
    iget-object v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->hash:Ljava/util/Set;

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexReader;->getSequentialSubReaders()[Lorg/apache/lucene/index/IndexReader;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->hash:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 107
    :cond_0
    :goto_0
    return-object v1

    .line 102
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v2}, Lorg/apache/lucene/search/TermQuery;->access$000(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/IndexReader;->termDocs(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/TermDocs;

    move-result-object v0

    .line 104
    .local v0, "termDocs":Lorg/apache/lucene/index/TermDocs;
    if-eqz v0, :cond_0

    .line 107
    new-instance v1, Lorg/apache/lucene/search/TermScorer;

    iget-object v2, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->similarity:Lorg/apache/lucene/search/Similarity;

    iget-object v3, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    # getter for: Lorg/apache/lucene/search/TermQuery;->term:Lorg/apache/lucene/index/Term;
    invoke-static {v3}, Lorg/apache/lucene/search/TermQuery;->access$000(Lorg/apache/lucene/search/TermQuery;)Lorg/apache/lucene/index/Term;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/IndexReader;->norms(Ljava/lang/String;)[B

    move-result-object v3

    invoke-direct {v1, p0, v0, v2, v3}, Lorg/apache/lucene/search/TermScorer;-><init>(Lorg/apache/lucene/search/Weight;Lorg/apache/lucene/index/TermDocs;Lorg/apache/lucene/search/Similarity;[B)V

    goto :goto_0
.end method

.method public sumOfSquaredWeights()F
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->idf:F

    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    invoke-virtual {v1}, Lorg/apache/lucene/search/TermQuery;->getBoost()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->queryWeight:F

    .line 85
    iget v0, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->queryWeight:F

    iget v1, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->queryWeight:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "weight("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/TermQuery$TermWeight;->this$0:Lorg/apache/lucene/search/TermQuery;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
