.class public abstract Lorg/apache/lucene/search/FilteredDocIdSetIterator;
.super Lorg/apache/lucene/search/DocIdSetIterator;
.source "FilteredDocIdSetIterator.java"


# instance fields
.field protected _innerIter:Lorg/apache/lucene/search/DocIdSetIterator;

.field private doc:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/DocIdSetIterator;)V
    .locals 2
    .param p1, "innerIter"    # Lorg/apache/lucene/search/DocIdSetIterator;

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/lucene/search/DocIdSetIterator;-><init>()V

    .line 37
    if-nez p1, :cond_0

    .line 38
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "null iterator"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->_innerIter:Lorg/apache/lucene/search/DocIdSetIterator;

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    .line 42
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 2
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v1, 0x7fffffff

    .line 69
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->_innerIter:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/search/DocIdSetIterator;->advance(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    .line 70
    iget v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    if-eq v0, v1, :cond_2

    .line 71
    iget v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->match(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    iget v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    .line 82
    :goto_0
    return v0

    .line 74
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->_innerIter:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    if-eq v0, v1, :cond_1

    .line 75
    iget v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->match(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    goto :goto_0

    .line 79
    :cond_1
    iget v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    goto :goto_0

    .line 82
    :cond_2
    iget v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    return v0
.end method

.method protected abstract match(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public nextDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->_innerIter:Lorg/apache/lucene/search/DocIdSetIterator;

    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_1

    .line 60
    iget v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    invoke-virtual {p0, v0}, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->match(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    .line 64
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lorg/apache/lucene/search/FilteredDocIdSetIterator;->doc:I

    goto :goto_0
.end method
