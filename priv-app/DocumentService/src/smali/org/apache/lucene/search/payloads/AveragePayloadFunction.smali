.class public Lorg/apache/lucene/search/payloads/AveragePayloadFunction;
.super Lorg/apache/lucene/search/payloads/PayloadFunction;
.source "AveragePayloadFunction.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/lucene/search/payloads/PayloadFunction;-><init>()V

    return-void
.end method


# virtual methods
.method public currentScore(ILjava/lang/String;IIIFF)F
    .locals 1
    .param p1, "docId"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "start"    # I
    .param p4, "end"    # I
    .param p5, "numPayloadsSeen"    # I
    .param p6, "currentScore"    # F
    .param p7, "currentPayloadScore"    # F

    .prologue
    .line 32
    add-float v0, p7, p6

    return v0
.end method

.method public docScore(ILjava/lang/String;IF)F
    .locals 1
    .param p1, "docId"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "numPayloadsSeen"    # I
    .param p4, "payloadScore"    # F

    .prologue
    .line 37
    if-lez p3, :cond_0

    int-to-float v0, p3

    div-float v0, p4, v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 58
    if-ne p0, p1, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v0

    .line 60
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 61
    goto :goto_0

    .line 62
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 63
    goto :goto_0
.end method

.method public explain(IIF)Lorg/apache/lucene/search/Explanation;
    .locals 3
    .param p1, "doc"    # I
    .param p2, "numPayloadsSeen"    # I
    .param p3, "payloadScore"    # F

    .prologue
    .line 41
    new-instance v1, Lorg/apache/lucene/search/Explanation;

    invoke-direct {v1}, Lorg/apache/lucene/search/Explanation;-><init>()V

    .line 42
    .local v1, "payloadBoost":Lorg/apache/lucene/search/Explanation;
    if-lez p2, :cond_0

    int-to-float v2, p2

    div-float v0, p3, v2

    .line 43
    .local v0, "avgPayloadScore":F
    :goto_0
    invoke-virtual {v1, v0}, Lorg/apache/lucene/search/Explanation;->setValue(F)V

    .line 44
    const-string/jumbo v2, "AveragePayloadFunction(...)"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Explanation;->setDescription(Ljava/lang/String;)V

    .line 45
    return-object v1

    .line 42
    .end local v0    # "avgPayloadScore":F
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 50
    const/16 v0, 0x1f

    .line 51
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 52
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/lit8 v1, v2, 0x1f

    .line 53
    return v1
.end method
