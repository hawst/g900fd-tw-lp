.class final Lorg/apache/lucene/search/SloppyPhraseScorer;
.super Lorg/apache/lucene/search/PhraseScorer;
.source "SloppyPhraseScorer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private checkedRpts:Z

.field private end:I

.field private hasMultiTermRpts:Z

.field private hasRpts:Z

.field private final numPostings:I

.field private final pq:Lorg/apache/lucene/search/PhraseQueue;

.field private rptGroups:[[Lorg/apache/lucene/search/PhrasePositions;

.field private rptStack:[Lorg/apache/lucene/search/PhrasePositions;

.field private final slop:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/search/SloppyPhraseScorer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/SloppyPhraseScorer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;Lorg/apache/lucene/search/Similarity;I[B)V
    .locals 2
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "postings"    # [Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
    .param p3, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p4, "slop"    # I
    .param p5, "norms"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3, p5}, Lorg/apache/lucene/search/PhraseScorer;-><init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;Lorg/apache/lucene/search/Similarity;[B)V

    .line 48
    iput p4, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->slop:I

    .line 49
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->numPostings:I

    .line 50
    new-instance v0, Lorg/apache/lucene/search/PhraseQueue;

    array-length v1, p2

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/PhraseQueue;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    .line 51
    return-void

    .line 49
    :cond_0
    array-length v0, p2

    goto :goto_0
.end method

.method private advancePP(Lorg/apache/lucene/search/PhrasePositions;)Z
    .locals 2
    .param p1, "pp"    # Lorg/apache/lucene/search/PhrasePositions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p1}, Lorg/apache/lucene/search/PhrasePositions;->nextPosition()Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    const/4 v0, 0x0

    .line 113
    :goto_0
    return v0

    .line 110
    :cond_0
    iget v0, p1, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iget v1, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    if-le v0, v1, :cond_1

    .line 111
    iget v0, p1, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iput v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    .line 113
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private advanceRepeatGroups()Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 255
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptGroups:[[Lorg/apache/lucene/search/PhrasePositions;

    .local v0, "arr$":[[Lorg/apache/lucene/search/PhrasePositions;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v6, :cond_7

    aget-object v9, v0, v2

    .line 256
    .local v9, "rg":[Lorg/apache/lucene/search/PhrasePositions;
    iget-boolean v11, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasMultiTermRpts:Z

    if-eqz v11, :cond_4

    .line 259
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v11, v9

    if-ge v1, v11, :cond_6

    .line 260
    const/4 v3, 0x1

    .line 261
    .local v3, "incr":I
    aget-object v7, v9, v1

    .line 263
    .local v7, "pp":Lorg/apache/lucene/search/PhrasePositions;
    :cond_0
    invoke-direct {p0, v7}, Lorg/apache/lucene/search/SloppyPhraseScorer;->collide(Lorg/apache/lucene/search/PhrasePositions;)I

    move-result v5

    .local v5, "k":I
    if-ltz v5, :cond_3

    .line 264
    aget-object v11, v9, v5

    invoke-direct {p0, v7, v11}, Lorg/apache/lucene/search/SloppyPhraseScorer;->lesser(Lorg/apache/lucene/search/PhrasePositions;Lorg/apache/lucene/search/PhrasePositions;)Lorg/apache/lucene/search/PhrasePositions;

    move-result-object v8

    .line 265
    .local v8, "pp2":Lorg/apache/lucene/search/PhrasePositions;
    invoke-direct {p0, v8}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advancePP(Lorg/apache/lucene/search/PhrasePositions;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 285
    .end local v1    # "i":I
    .end local v3    # "incr":I
    .end local v5    # "k":I
    .end local v7    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    .end local v8    # "pp2":Lorg/apache/lucene/search/PhrasePositions;
    .end local v9    # "rg":[Lorg/apache/lucene/search/PhrasePositions;
    :cond_1
    :goto_2
    return v10

    .line 268
    .restart local v1    # "i":I
    .restart local v3    # "incr":I
    .restart local v5    # "k":I
    .restart local v7    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    .restart local v8    # "pp2":Lorg/apache/lucene/search/PhrasePositions;
    .restart local v9    # "rg":[Lorg/apache/lucene/search/PhrasePositions;
    :cond_2
    iget v11, v8, Lorg/apache/lucene/search/PhrasePositions;->rptInd:I

    if-ge v11, v1, :cond_0

    .line 269
    const/4 v3, 0x0

    .line 259
    .end local v8    # "pp2":Lorg/apache/lucene/search/PhrasePositions;
    :cond_3
    add-int/2addr v1, v3

    goto :goto_1

    .line 276
    .end local v1    # "i":I
    .end local v3    # "incr":I
    .end local v5    # "k":I
    .end local v7    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    :cond_4
    const/4 v4, 0x1

    .local v4, "j":I
    :goto_3
    array-length v11, v9

    if-ge v4, v11, :cond_6

    .line 277
    const/4 v5, 0x0

    .restart local v5    # "k":I
    :goto_4
    if-ge v5, v4, :cond_5

    .line 278
    aget-object v11, v9, v4

    invoke-virtual {v11}, Lorg/apache/lucene/search/PhrasePositions;->nextPosition()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 277
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 276
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 255
    .end local v4    # "j":I
    .end local v5    # "k":I
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 285
    .end local v9    # "rg":[Lorg/apache/lucene/search/PhrasePositions;
    :cond_7
    const/4 v10, 0x1

    goto :goto_2
.end method

.method private advanceRpts(Lorg/apache/lucene/search/PhrasePositions;)Z
    .locals 14
    .param p1, "pp"    # Lorg/apache/lucene/search/PhrasePositions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 120
    iget v9, p1, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    if-gez v9, :cond_1

    .line 150
    :cond_0
    :goto_0
    return v8

    .line 123
    :cond_1
    iget-object v9, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptGroups:[[Lorg/apache/lucene/search/PhrasePositions;

    iget v10, p1, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    aget-object v7, v9, v10

    .line 124
    .local v7, "rg":[Lorg/apache/lucene/search/PhrasePositions;
    new-instance v0, Lorg/apache/lucene/util/OpenBitSet;

    array-length v9, v7

    int-to-long v10, v9

    invoke-direct {v0, v10, v11}, Lorg/apache/lucene/util/OpenBitSet;-><init>(J)V

    .line 125
    .local v0, "bits":Lorg/apache/lucene/util/OpenBitSet;
    iget v3, p1, Lorg/apache/lucene/search/PhrasePositions;->rptInd:I

    .line 127
    .local v3, "k0":I
    :cond_2
    :goto_1
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->collide(Lorg/apache/lucene/search/PhrasePositions;)I

    move-result v2

    .local v2, "k":I
    if-ltz v2, :cond_4

    .line 128
    aget-object v9, v7, v2

    invoke-direct {p0, p1, v9}, Lorg/apache/lucene/search/SloppyPhraseScorer;->lesser(Lorg/apache/lucene/search/PhrasePositions;Lorg/apache/lucene/search/PhrasePositions;)Lorg/apache/lucene/search/PhrasePositions;

    move-result-object p1

    .line 129
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advancePP(Lorg/apache/lucene/search/PhrasePositions;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 130
    const/4 v8, 0x0

    goto :goto_0

    .line 132
    :cond_3
    if-eq v2, v3, :cond_2

    .line 133
    int-to-long v10, v2

    invoke-virtual {v0, v10, v11}, Lorg/apache/lucene/util/OpenBitSet;->set(J)V

    goto :goto_1

    .line 138
    :cond_4
    const/4 v4, 0x0

    .line 139
    .local v4, "n":I
    :goto_2
    invoke-virtual {v0}, Lorg/apache/lucene/util/OpenBitSet;->cardinality()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-lez v9, :cond_6

    .line 140
    iget-object v9, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v9}, Lorg/apache/lucene/search/PhraseQueue;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/search/PhrasePositions;

    .line 141
    .local v6, "pp2":Lorg/apache/lucene/search/PhrasePositions;
    iget-object v9, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptStack:[Lorg/apache/lucene/search/PhrasePositions;

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "n":I
    .local v5, "n":I
    aput-object v6, v9, v4

    .line 142
    iget v9, v6, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    if-ltz v9, :cond_5

    iget v9, v6, Lorg/apache/lucene/search/PhrasePositions;->rptInd:I

    invoke-virtual {v0, v9}, Lorg/apache/lucene/util/OpenBitSet;->get(I)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 143
    iget v9, v6, Lorg/apache/lucene/search/PhrasePositions;->rptInd:I

    int-to-long v10, v9

    invoke-virtual {v0, v10, v11}, Lorg/apache/lucene/util/OpenBitSet;->clear(J)V

    :cond_5
    move v4, v5

    .line 145
    .end local v5    # "n":I
    .restart local v4    # "n":I
    goto :goto_2

    .line 147
    .end local v6    # "pp2":Lorg/apache/lucene/search/PhrasePositions;
    :cond_6
    add-int/lit8 v1, v4, -0x1

    .local v1, "i":I
    :goto_3
    if-ltz v1, :cond_0

    .line 148
    iget-object v9, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    iget-object v10, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptStack:[Lorg/apache/lucene/search/PhrasePositions;

    aget-object v10, v10, v1

    invoke-virtual {v9, v10}, Lorg/apache/lucene/search/PhraseQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    add-int/lit8 v1, v1, -0x1

    goto :goto_3
.end method

.method private collide(Lorg/apache/lucene/search/PhrasePositions;)I
    .locals 6
    .param p1, "pp"    # Lorg/apache/lucene/search/PhrasePositions;

    .prologue
    .line 164
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->tpPos(Lorg/apache/lucene/search/PhrasePositions;)I

    move-result v3

    .line 165
    .local v3, "tpPos":I
    iget-object v4, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptGroups:[[Lorg/apache/lucene/search/PhrasePositions;

    iget v5, p1, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    aget-object v2, v4, v5

    .line 166
    .local v2, "rg":[Lorg/apache/lucene/search/PhrasePositions;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 167
    aget-object v1, v2, v0

    .line 168
    .local v1, "pp2":Lorg/apache/lucene/search/PhrasePositions;
    if-eq v1, p1, :cond_0

    invoke-direct {p0, v1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->tpPos(Lorg/apache/lucene/search/PhrasePositions;)I

    move-result v4

    if-ne v4, v3, :cond_0

    .line 169
    iget v4, v1, Lorg/apache/lucene/search/PhrasePositions;->rptInd:I

    .line 172
    .end local v1    # "pp2":Lorg/apache/lucene/search/PhrasePositions;
    :goto_1
    return v4

    .line 166
    .restart local v1    # "pp2":Lorg/apache/lucene/search/PhrasePositions;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 172
    .end local v1    # "pp2":Lorg/apache/lucene/search/PhrasePositions;
    :cond_1
    const/4 v4, -0x1

    goto :goto_1
.end method

.method private fillQueue()V
    .locals 4

    .prologue
    .line 236
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/search/PhraseQueue;->clear()V

    .line 237
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    .local v0, "pp":Lorg/apache/lucene/search/PhrasePositions;
    const/4 v1, 0x0

    .local v1, "prev":Lorg/apache/lucene/search/PhrasePositions;
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    if-eq v1, v2, :cond_1

    .line 238
    iget v2, v0, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iget v3, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    if-le v2, v3, :cond_0

    .line 239
    iget v2, v0, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iput v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    .line 241
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/PhraseQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    move-object v1, v0

    iget-object v0, v0, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    goto :goto_0

    .line 243
    :cond_1
    return-void
.end method

.method private gatherRptGroups(Ljava/util/LinkedHashMap;)Ljava/util/ArrayList;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/search/PhrasePositions;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 344
    .local p1, "rptTerms":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    invoke-direct/range {p0 .. p1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->repeatingPPs(Ljava/util/HashMap;)[Lorg/apache/lucene/search/PhrasePositions;

    move-result-object v19

    .line 345
    .local v19, "rpp":[Lorg/apache/lucene/search/PhrasePositions;
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 346
    .local v17, "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lorg/apache/lucene/search/PhrasePositions;>;>;"
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasMultiTermRpts:Z

    move/from16 v24, v0

    if-nez v24, :cond_5

    .line 348
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v9, v0, :cond_b

    .line 349
    aget-object v15, v19, v9

    .line 350
    .local v15, "pp":Lorg/apache/lucene/search/PhrasePositions;
    iget v0, v15, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    move/from16 v24, v0

    if-ltz v24, :cond_1

    .line 348
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 351
    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/search/SloppyPhraseScorer;->tpPos(Lorg/apache/lucene/search/PhrasePositions;)I

    move-result v23

    .line 352
    .local v23, "tpPos":I
    add-int/lit8 v12, v9, 0x1

    .local v12, "j":I
    :goto_1
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v12, v0, :cond_0

    .line 353
    aget-object v16, v19, v12

    .line 354
    .local v16, "pp2":Lorg/apache/lucene/search/PhrasePositions;
    move-object/from16 v0, v16

    iget v0, v0, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    move/from16 v24, v0

    if-gez v24, :cond_2

    move-object/from16 v0, v16

    iget v0, v0, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    move/from16 v24, v0

    iget v0, v15, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->tpPos(Lorg/apache/lucene/search/PhrasePositions;)I

    move-result v24

    move/from16 v0, v24

    move/from16 v1, v23

    if-eq v0, v1, :cond_3

    .line 352
    :cond_2
    :goto_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 361
    :cond_3
    iget v7, v15, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    .line 362
    .local v7, "g":I
    if-gez v7, :cond_4

    .line 363
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 364
    iput v7, v15, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    .line 365
    new-instance v18, Ljava/util/ArrayList;

    const/16 v24, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 366
    .local v18, "rl":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/PhrasePositions;>;"
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 367
    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 369
    .end local v18    # "rl":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/PhrasePositions;>;"
    :cond_4
    move-object/from16 v0, v16

    iput v7, v0, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    .line 370
    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/util/ArrayList;

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 375
    .end local v7    # "g":I
    .end local v9    # "i":I
    .end local v12    # "j":I
    .end local v15    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    .end local v16    # "pp2":Lorg/apache/lucene/search/PhrasePositions;
    .end local v23    # "tpPos":I
    :cond_5
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 376
    .local v22, "tmp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashSet<Lorg/apache/lucene/search/PhrasePositions;>;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/SloppyPhraseScorer;->ppTermsBitSets([Lorg/apache/lucene/search/PhrasePositions;Ljava/util/HashMap;)Ljava/util/ArrayList;

    move-result-object v5

    .line 377
    .local v5, "bb":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/OpenBitSet;>;"
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lorg/apache/lucene/search/SloppyPhraseScorer;->unionTermGroups(Ljava/util/ArrayList;)V

    .line 378
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/search/SloppyPhraseScorer;->termGroups(Ljava/util/LinkedHashMap;Ljava/util/ArrayList;)Ljava/util/HashMap;

    move-result-object v21

    .line 379
    .local v21, "tg":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    new-instance v6, Ljava/util/HashSet;

    invoke-virtual/range {v21 .. v21}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v6, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 380
    .local v6, "distinctGroupIDs":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_3
    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v24

    move/from16 v0, v24

    if-ge v9, v0, :cond_6

    .line 381
    new-instance v24, Ljava/util/HashSet;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 383
    :cond_6
    move-object/from16 v3, v19

    .local v3, "arr$":[Lorg/apache/lucene/search/PhrasePositions;
    array-length v13, v3

    .local v13, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    move v11, v10

    .end local v3    # "arr$":[Lorg/apache/lucene/search/PhrasePositions;
    .end local v10    # "i$":I
    .end local v13    # "len$":I
    .local v11, "i$":I
    :goto_4
    if-ge v11, v13, :cond_a

    aget-object v15, v3, v11

    .line 384
    .restart local v15    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    iget-object v4, v15, Lorg/apache/lucene/search/PhrasePositions;->terms:[Lorg/apache/lucene/index/Term;

    .local v4, "arr$":[Lorg/apache/lucene/index/Term;
    array-length v14, v4

    .local v14, "len$":I
    const/4 v10, 0x0

    .end local v11    # "i$":I
    .restart local v10    # "i$":I
    :goto_5
    if-ge v10, v14, :cond_9

    aget-object v20, v4, v10

    .line 385
    .local v20, "t":Lorg/apache/lucene/index/Term;
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_8

    .line 386
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Integer;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 387
    .restart local v7    # "g":I
    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/util/HashSet;

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 388
    sget-boolean v24, Lorg/apache/lucene/search/SloppyPhraseScorer;->$assertionsDisabled:Z

    if-nez v24, :cond_7

    iget v0, v15, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    move/from16 v24, v0

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_7

    iget v0, v15, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-eq v0, v7, :cond_7

    new-instance v24, Ljava/lang/AssertionError;

    invoke-direct/range {v24 .. v24}, Ljava/lang/AssertionError;-><init>()V

    throw v24

    .line 389
    :cond_7
    iput v7, v15, Lorg/apache/lucene/search/PhrasePositions;->rptGroup:I

    .line 384
    .end local v7    # "g":I
    :cond_8
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 383
    .end local v20    # "t":Lorg/apache/lucene/index/Term;
    :cond_9
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    .end local v10    # "i$":I
    .restart local v11    # "i$":I
    goto :goto_4

    .line 393
    .end local v4    # "arr$":[Lorg/apache/lucene/index/Term;
    .end local v14    # "len$":I
    .end local v15    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    :cond_a
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .end local v11    # "i$":I
    .local v10, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashSet;

    .line 394
    .local v8, "hs":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/search/PhrasePositions;>;"
    new-instance v24, Ljava/util/ArrayList;

    move-object/from16 v0, v24

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 397
    .end local v5    # "bb":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/OpenBitSet;>;"
    .end local v6    # "distinctGroupIDs":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .end local v8    # "hs":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/apache/lucene/search/PhrasePositions;>;"
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v21    # "tg":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    .end local v22    # "tmp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashSet<Lorg/apache/lucene/search/PhrasePositions;>;>;"
    :cond_b
    return-object v17
.end method

.method private initComplex()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 219
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->placeFirstPositions()V

    .line 220
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advanceRepeatGroups()Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    const/4 v0, 0x0

    .line 224
    :goto_0
    return v0

    .line 223
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->fillQueue()V

    .line 224
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private initFirstTime()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 304
    iput-boolean v3, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->checkedRpts:Z

    .line 305
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->placeFirstPositions()V

    .line 307
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->repeatingTerms()Ljava/util/LinkedHashMap;

    move-result-object v1

    .line 308
    .local v1, "rptTerms":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasRpts:Z

    .line 310
    iget-boolean v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasRpts:Z

    if-eqz v2, :cond_1

    .line 311
    iget v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->numPostings:I

    new-array v2, v2, [Lorg/apache/lucene/search/PhrasePositions;

    iput-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptStack:[Lorg/apache/lucene/search/PhrasePositions;

    .line 312
    invoke-direct {p0, v1}, Lorg/apache/lucene/search/SloppyPhraseScorer;->gatherRptGroups(Ljava/util/LinkedHashMap;)Ljava/util/ArrayList;

    move-result-object v0

    .line 313
    .local v0, "rgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lorg/apache/lucene/search/PhrasePositions;>;>;"
    invoke-direct {p0, v0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->sortRptGroups(Ljava/util/ArrayList;)V

    .line 314
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advanceRepeatGroups()Z

    move-result v2

    if-nez v2, :cond_1

    .line 320
    .end local v0    # "rgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lorg/apache/lucene/search/PhrasePositions;>;>;"
    :goto_1
    return v4

    :cond_0
    move v2, v4

    .line 308
    goto :goto_0

    .line 319
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->fillQueue()V

    move v4, v3

    .line 320
    goto :goto_1
.end method

.method private initPhrasePositions()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    const/high16 v0, -0x80000000

    iput v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    .line 192
    iget-boolean v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->checkedRpts:Z

    if-nez v0, :cond_0

    .line 193
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->initFirstTime()Z

    move-result v0

    .line 199
    :goto_0
    return v0

    .line 195
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasRpts:Z

    if-nez v0, :cond_1

    .line 196
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->initSimple()V

    .line 197
    const/4 v0, 0x1

    goto :goto_0

    .line 199
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->initComplex()Z

    move-result v0

    goto :goto_0
.end method

.method private initSimple()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 205
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v2}, Lorg/apache/lucene/search/PhraseQueue;->clear()V

    .line 207
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    .local v0, "pp":Lorg/apache/lucene/search/PhrasePositions;
    const/4 v1, 0x0

    .local v1, "prev":Lorg/apache/lucene/search/PhrasePositions;
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    if-eq v1, v2, :cond_1

    .line 208
    invoke-virtual {v0}, Lorg/apache/lucene/search/PhrasePositions;->firstPosition()V

    .line 209
    iget v2, v0, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iget v3, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    if-le v2, v3, :cond_0

    .line 210
    iget v2, v0, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iput v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    .line 212
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/search/PhraseQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    move-object v1, v0

    iget-object v0, v0, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    goto :goto_0

    .line 214
    :cond_1
    return-void
.end method

.method private lesser(Lorg/apache/lucene/search/PhrasePositions;Lorg/apache/lucene/search/PhrasePositions;)Lorg/apache/lucene/search/PhrasePositions;
    .locals 2
    .param p1, "pp"    # Lorg/apache/lucene/search/PhrasePositions;
    .param p2, "pp2"    # Lorg/apache/lucene/search/PhrasePositions;

    .prologue
    .line 155
    iget v0, p1, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iget v1, p2, Lorg/apache/lucene/search/PhrasePositions;->position:I

    if-lt v0, v1, :cond_0

    iget v0, p1, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iget v1, p2, Lorg/apache/lucene/search/PhrasePositions;->position:I

    if-ne v0, v1, :cond_1

    iget v0, p1, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    iget v1, p2, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    if-ge v0, v1, :cond_1

    :cond_0
    move-object p2, p1

    .line 159
    .end local p2    # "pp2":Lorg/apache/lucene/search/PhrasePositions;
    :cond_1
    return-object p2
.end method

.method private placeFirstPositions()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 229
    iget-object v0, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    .local v0, "pp":Lorg/apache/lucene/search/PhrasePositions;
    const/4 v1, 0x0

    .local v1, "prev":Lorg/apache/lucene/search/PhrasePositions;
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    if-eq v1, v2, :cond_0

    .line 230
    invoke-virtual {v0}, Lorg/apache/lucene/search/PhrasePositions;->firstPosition()V

    .line 229
    move-object v1, v0

    iget-object v0, v0, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    goto :goto_0

    .line 232
    :cond_0
    return-void
.end method

.method private ppTermsBitSets([Lorg/apache/lucene/search/PhrasePositions;Ljava/util/HashMap;)Ljava/util/ArrayList;
    .locals 16
    .param p1, "rpp"    # [Lorg/apache/lucene/search/PhrasePositions;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lorg/apache/lucene/search/PhrasePositions;",
            "Ljava/util/HashMap",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/util/OpenBitSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 439
    .local p2, "tord":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    new-instance v5, Ljava/util/ArrayList;

    move-object/from16 v0, p1

    array-length v13, v0

    invoke-direct {v5, v13}, Ljava/util/ArrayList;-><init>(I)V

    .line 440
    .local v5, "bb":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/OpenBitSet;>;"
    move-object/from16 v2, p1

    .local v2, "arr$":[Lorg/apache/lucene/search/PhrasePositions;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    move v7, v6

    .end local v2    # "arr$":[Lorg/apache/lucene/search/PhrasePositions;
    .end local v6    # "i$":I
    .end local v8    # "len$":I
    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_2

    aget-object v11, v2, v7

    .line 441
    .local v11, "pp":Lorg/apache/lucene/search/PhrasePositions;
    new-instance v4, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual/range {p2 .. p2}, Ljava/util/HashMap;->size()I

    move-result v13

    int-to-long v14, v13

    invoke-direct {v4, v14, v15}, Lorg/apache/lucene/util/OpenBitSet;-><init>(J)V

    .line 443
    .local v4, "b":Lorg/apache/lucene/util/OpenBitSet;
    iget-object v3, v11, Lorg/apache/lucene/search/PhrasePositions;->terms:[Lorg/apache/lucene/index/Term;

    .local v3, "arr$":[Lorg/apache/lucene/index/Term;
    array-length v9, v3

    .local v9, "len$":I
    const/4 v6, 0x0

    .end local v7    # "i$":I
    .restart local v6    # "i$":I
    :goto_1
    if-ge v6, v9, :cond_1

    aget-object v12, v3, v6

    .line 444
    .local v12, "t":Lorg/apache/lucene/index/Term;
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    .local v10, "ord":Ljava/lang/Integer;
    if-eqz v10, :cond_0

    .line 445
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v13

    int-to-long v14, v13

    invoke-virtual {v4, v14, v15}, Lorg/apache/lucene/util/OpenBitSet;->set(J)V

    .line 443
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 448
    .end local v10    # "ord":Ljava/lang/Integer;
    .end local v12    # "t":Lorg/apache/lucene/index/Term;
    :cond_1
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    .end local v6    # "i$":I
    .restart local v7    # "i$":I
    goto :goto_0

    .line 450
    .end local v3    # "arr$":[Lorg/apache/lucene/index/Term;
    .end local v4    # "b":Lorg/apache/lucene/util/OpenBitSet;
    .end local v9    # "len$":I
    .end local v11    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    :cond_2
    return-object v5
.end method

.method private repeatingPPs(Ljava/util/HashMap;)[Lorg/apache/lucene/search/PhrasePositions;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;)[",
            "Lorg/apache/lucene/search/PhrasePositions;"
        }
    .end annotation

    .prologue
    .local p1, "rptTerms":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 424
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 425
    .local v5, "rp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/search/PhrasePositions;>;"
    iget-object v3, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    .local v3, "pp":Lorg/apache/lucene/search/PhrasePositions;
    const/4 v4, 0x0

    .local v4, "prev":Lorg/apache/lucene/search/PhrasePositions;
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    if-eq v4, v7, :cond_3

    .line 426
    iget-object v0, v3, Lorg/apache/lucene/search/PhrasePositions;->terms:[Lorg/apache/lucene/index/Term;

    .local v0, "arr$":[Lorg/apache/lucene/index/Term;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v6, v0, v1

    .line 427
    .local v6, "t":Lorg/apache/lucene/index/Term;
    invoke-virtual {p1, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 428
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    iget-boolean v10, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasMultiTermRpts:Z

    iget-object v7, v3, Lorg/apache/lucene/search/PhrasePositions;->terms:[Lorg/apache/lucene/index/Term;

    array-length v7, v7

    if-le v7, v8, :cond_1

    move v7, v8

    :goto_2
    or-int/2addr v7, v10

    iput-boolean v7, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasMultiTermRpts:Z

    .line 425
    .end local v6    # "t":Lorg/apache/lucene/index/Term;
    :cond_0
    move-object v4, v3

    iget-object v3, v3, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    goto :goto_0

    .restart local v6    # "t":Lorg/apache/lucene/index/Term;
    :cond_1
    move v7, v9

    .line 429
    goto :goto_2

    .line 426
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 434
    .end local v0    # "arr$":[Lorg/apache/lucene/index/Term;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v6    # "t":Lorg/apache/lucene/index/Term;
    :cond_3
    new-array v7, v9, [Lorg/apache/lucene/search/PhrasePositions;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lorg/apache/lucene/search/PhrasePositions;

    return-object v7
.end method

.method private repeatingTerms()Ljava/util/LinkedHashMap;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407
    new-instance v9, Ljava/util/LinkedHashMap;

    invoke-direct {v9}, Ljava/util/LinkedHashMap;-><init>()V

    .line 408
    .local v9, "tord":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 409
    .local v8, "tcnt":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    iget-object v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->min:Lorg/apache/lucene/search/PhrasePositions;

    .local v5, "pp":Lorg/apache/lucene/search/PhrasePositions;
    const/4 v6, 0x0

    .local v6, "prev":Lorg/apache/lucene/search/PhrasePositions;
    :goto_0
    iget-object v10, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->max:Lorg/apache/lucene/search/PhrasePositions;

    if-eq v6, v10, :cond_3

    .line 410
    iget-object v0, v5, Lorg/apache/lucene/search/PhrasePositions;->terms:[Lorg/apache/lucene/index/Term;

    .local v0, "arr$":[Lorg/apache/lucene/index/Term;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v7, v0, v3

    .line 411
    .local v7, "t":Lorg/apache/lucene/index/Term;
    invoke-virtual {v8, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 412
    .local v2, "cnt0":Ljava/lang/Integer;
    if-nez v2, :cond_1

    new-instance v1, Ljava/lang/Integer;

    const/4 v10, 0x1

    invoke-direct {v1, v10}, Ljava/lang/Integer;-><init>(I)V

    .line 413
    .local v1, "cnt":Ljava/lang/Integer;
    :goto_2
    invoke-virtual {v8, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_0

    .line 415
    invoke-virtual {v9}, Ljava/util/LinkedHashMap;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v7, v10}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 412
    .end local v1    # "cnt":Ljava/lang/Integer;
    :cond_1
    new-instance v1, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-direct {v1, v10}, Ljava/lang/Integer;-><init>(I)V

    goto :goto_2

    .line 409
    .end local v2    # "cnt0":Ljava/lang/Integer;
    .end local v7    # "t":Lorg/apache/lucene/index/Term;
    :cond_2
    move-object v6, v5

    iget-object v5, v5, Lorg/apache/lucene/search/PhrasePositions;->next:Lorg/apache/lucene/search/PhrasePositions;

    goto :goto_0

    .line 419
    .end local v0    # "arr$":[Lorg/apache/lucene/index/Term;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_3
    return-object v9
.end method

.method private sortRptGroups(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/search/PhrasePositions;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 326
    .local p1, "rgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lorg/apache/lucene/search/PhrasePositions;>;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [[Lorg/apache/lucene/search/PhrasePositions;

    iput-object v4, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptGroups:[[Lorg/apache/lucene/search/PhrasePositions;

    .line 327
    new-instance v0, Lorg/apache/lucene/search/SloppyPhraseScorer$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/search/SloppyPhraseScorer$1;-><init>(Lorg/apache/lucene/search/SloppyPhraseScorer;)V

    .line 332
    .local v0, "cmprtr":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/search/PhrasePositions;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptGroups:[[Lorg/apache/lucene/search/PhrasePositions;

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 333
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    const/4 v5, 0x0

    new-array v5, v5, [Lorg/apache/lucene/search/PhrasePositions;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/search/PhrasePositions;

    .line 334
    .local v3, "rg":[Lorg/apache/lucene/search/PhrasePositions;
    invoke-static {v3, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 335
    iget-object v4, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->rptGroups:[[Lorg/apache/lucene/search/PhrasePositions;

    aput-object v3, v4, v1

    .line 336
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    array-length v4, v3

    if-ge v2, v4, :cond_0

    .line 337
    aget-object v4, v3, v2

    iput v2, v4, Lorg/apache/lucene/search/PhrasePositions;->rptInd:I

    .line 336
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 332
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 340
    .end local v2    # "j":I
    .end local v3    # "rg":[Lorg/apache/lucene/search/PhrasePositions;
    :cond_1
    return-void
.end method

.method private termGroups(Ljava/util/LinkedHashMap;Ljava/util/ArrayList;)Ljava/util/HashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/util/OpenBitSet;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Lorg/apache/lucene/index/Term;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 473
    .local p1, "tord":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    .local p2, "bb":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/OpenBitSet;>;"
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 474
    .local v4, "tg":Ljava/util/HashMap;, "Ljava/util/HashMap<Lorg/apache/lucene/index/Term;Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [Lorg/apache/lucene/index/Term;

    invoke-interface {v5, v6}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/index/Term;

    .line 475
    .local v3, "t":[Lorg/apache/lucene/index/Term;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 476
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {v5}, Lorg/apache/lucene/util/OpenBitSet;->iterator()Lorg/apache/lucene/search/DocIdSetIterator;

    move-result-object v0

    .line 478
    .local v0, "bits":Lorg/apache/lucene/search/DocIdSetIterator;
    :goto_1
    invoke-virtual {v0}, Lorg/apache/lucene/search/DocIdSetIterator;->nextDoc()I

    move-result v2

    .local v2, "ord":I
    const v5, 0x7fffffff

    if-eq v2, v5, :cond_0

    .line 479
    aget-object v5, v3, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 475
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 482
    .end local v0    # "bits":Lorg/apache/lucene/search/DocIdSetIterator;
    .end local v2    # "ord":I
    :cond_1
    return-object v4
.end method

.method private final tpPos(Lorg/apache/lucene/search/PhrasePositions;)I
    .locals 2
    .param p1, "pp"    # Lorg/apache/lucene/search/PhrasePositions;

    .prologue
    .line 402
    iget v0, p1, Lorg/apache/lucene/search/PhrasePositions;->position:I

    iget v1, p1, Lorg/apache/lucene/search/PhrasePositions;->offset:I

    add-int/2addr v0, v1

    return v0
.end method

.method private unionTermGroups(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/util/OpenBitSet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 456
    .local p1, "bb":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/OpenBitSet;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_2

    .line 457
    const/4 v1, 0x1

    .line 458
    .local v1, "incr":I
    add-int/lit8 v2, v0, 0x1

    .line 459
    .local v2, "j":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 460
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/OpenBitSet;->intersects(Lorg/apache/lucene/util/OpenBitSet;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 461
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/OpenBitSet;->union(Lorg/apache/lucene/util/OpenBitSet;)V

    .line 462
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 463
    const/4 v1, 0x0

    goto :goto_1

    .line 465
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 456
    :cond_1
    add-int/2addr v0, v1

    goto :goto_0

    .line 469
    .end local v1    # "incr":I
    .end local v2    # "j":I
    :cond_2
    return-void
.end method


# virtual methods
.method protected phraseFreq()F
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->initPhrasePositions()Z

    move-result v5

    if-nez v5, :cond_1

    .line 74
    const/4 v0, 0x0

    .line 102
    :cond_0
    :goto_0
    return v0

    .line 76
    :cond_1
    const/4 v0, 0x0

    .line 77
    .local v0, "freq":F
    iget-object v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/search/PhraseQueue;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/search/PhrasePositions;

    .line 78
    .local v4, "pp":Lorg/apache/lucene/search/PhrasePositions;
    iget v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    iget v6, v4, Lorg/apache/lucene/search/PhrasePositions;->position:I

    sub-int v1, v5, v6

    .line 79
    .local v1, "matchLength":I
    iget-object v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/search/PhraseQueue;->top()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/search/PhrasePositions;

    iget v3, v5, Lorg/apache/lucene/search/PhrasePositions;->position:I

    .line 80
    .local v3, "next":I
    :cond_2
    :goto_1
    invoke-direct {p0, v4}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advancePP(Lorg/apache/lucene/search/PhrasePositions;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 81
    iget-boolean v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->hasRpts:Z

    if-eqz v5, :cond_4

    invoke-direct {p0, v4}, Lorg/apache/lucene/search/SloppyPhraseScorer;->advanceRpts(Lorg/apache/lucene/search/PhrasePositions;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 99
    :cond_3
    iget v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->slop:I

    if-gt v1, v5, :cond_0

    .line 100
    invoke-virtual {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v5

    invoke-virtual {v5, v1}, Lorg/apache/lucene/search/Similarity;->sloppyFreq(I)F

    move-result v5

    add-float/2addr v0, v5

    goto :goto_0

    .line 84
    :cond_4
    iget v5, v4, Lorg/apache/lucene/search/PhrasePositions;->position:I

    if-le v5, v3, :cond_6

    .line 85
    iget v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->slop:I

    if-gt v1, v5, :cond_5

    .line 86
    invoke-virtual {p0}, Lorg/apache/lucene/search/SloppyPhraseScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v5

    invoke-virtual {v5, v1}, Lorg/apache/lucene/search/Similarity;->sloppyFreq(I)F

    move-result v5

    add-float/2addr v0, v5

    .line 88
    :cond_5
    iget-object v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v5, v4}, Lorg/apache/lucene/search/PhraseQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    iget-object v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/search/PhraseQueue;->pop()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    check-cast v4, Lorg/apache/lucene/search/PhrasePositions;

    .line 90
    .restart local v4    # "pp":Lorg/apache/lucene/search/PhrasePositions;
    iget-object v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->pq:Lorg/apache/lucene/search/PhraseQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/search/PhraseQueue;->top()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/search/PhrasePositions;

    iget v3, v5, Lorg/apache/lucene/search/PhrasePositions;->position:I

    .line 91
    iget v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    iget v6, v4, Lorg/apache/lucene/search/PhrasePositions;->position:I

    sub-int v1, v5, v6

    goto :goto_1

    .line 93
    :cond_6
    iget v5, p0, Lorg/apache/lucene/search/SloppyPhraseScorer;->end:I

    iget v6, v4, Lorg/apache/lucene/search/PhrasePositions;->position:I

    sub-int v2, v5, v6

    .line 94
    .local v2, "matchLength2":I
    if-ge v2, v1, :cond_2

    .line 95
    move v1, v2

    goto :goto_1
.end method
