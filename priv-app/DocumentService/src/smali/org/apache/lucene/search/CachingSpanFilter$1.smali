.class Lorg/apache/lucene/search/CachingSpanFilter$1;
.super Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;
.source "CachingSpanFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/CachingSpanFilter;-><init>(Lorg/apache/lucene/search/SpanFilter;Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache",
        "<",
        "Lorg/apache/lucene/search/SpanFilterResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/CachingSpanFilter;


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/CachingSpanFilter;Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;)V
    .locals 0
    .param p2, "x0"    # Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    .prologue
    .line 55
    iput-object p1, p0, Lorg/apache/lucene/search/CachingSpanFilter$1;->this$0:Lorg/apache/lucene/search/CachingSpanFilter;

    invoke-direct {p0, p2}, Lorg/apache/lucene/search/CachingWrapperFilter$FilterCache;-><init>(Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic mergeDeletes(Lorg/apache/lucene/index/IndexReader;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 55
    check-cast p2, Lorg/apache/lucene/search/SpanFilterResult;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/CachingSpanFilter$1;->mergeDeletes(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/SpanFilterResult;)Lorg/apache/lucene/search/SpanFilterResult;

    move-result-object v0

    return-object v0
.end method

.method protected mergeDeletes(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/SpanFilterResult;)Lorg/apache/lucene/search/SpanFilterResult;
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "value"    # Lorg/apache/lucene/search/SpanFilterResult;

    .prologue
    .line 56
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "DeletesMode.DYNAMIC is not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
