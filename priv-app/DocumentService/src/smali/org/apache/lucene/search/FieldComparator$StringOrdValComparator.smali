.class public final Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;
.super Lorg/apache/lucene/search/FieldComparator;
.source "FieldComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/FieldComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StringOrdValComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/FieldComparator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private bottomOrd:I

.field private bottomSameReader:Z

.field private bottomSlot:I

.field private bottomValue:Ljava/lang/String;

.field private currentReaderGen:I

.field private final field:Ljava/lang/String;

.field private lookup:[Ljava/lang/String;

.field private order:[I

.field private final ords:[I

.field private final readerGen:[I

.field private final values:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 844
    const-class v0, Lorg/apache/lucene/search/FieldComparator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ILjava/lang/String;IZ)V
    .locals 1
    .param p1, "numHits"    # I
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "sortPos"    # I
    .param p4, "reversed"    # Z

    .prologue
    const/4 v0, -0x1

    .line 860
    invoke-direct {p0}, Lorg/apache/lucene/search/FieldComparator;-><init>()V

    .line 850
    iput v0, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->currentReaderGen:I

    .line 855
    iput v0, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSlot:I

    .line 861
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->ords:[I

    .line 862
    new-array v0, p1, [Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->values:[Ljava/lang/String;

    .line 863
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->readerGen:[I

    .line 864
    iput-object p2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->field:Ljava/lang/String;

    .line 865
    return-void
.end method


# virtual methods
.method public compare(II)I
    .locals 4
    .param p1, "slot1"    # I
    .param p2, "slot2"    # I

    .prologue
    .line 869
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->readerGen:[I

    aget v2, v2, p1

    iget-object v3, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->readerGen:[I

    aget v3, v3, p2

    if-ne v2, v3, :cond_0

    .line 870
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->ords:[I

    aget v2, v2, p1

    iget-object v3, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->ords:[I

    aget v3, v3, p2

    sub-int/2addr v2, v3

    .line 883
    :goto_0
    return v2

    .line 873
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->values:[Ljava/lang/String;

    aget-object v0, v2, p1

    .line 874
    .local v0, "val1":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->values:[Ljava/lang/String;

    aget-object v1, v2, p2

    .line 875
    .local v1, "val2":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 876
    if-nez v1, :cond_1

    .line 877
    const/4 v2, 0x0

    goto :goto_0

    .line 879
    :cond_1
    const/4 v2, -0x1

    goto :goto_0

    .line 880
    :cond_2
    if-nez v1, :cond_3

    .line 881
    const/4 v2, 0x1

    goto :goto_0

    .line 883
    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method public compareBottom(I)I
    .locals 3
    .param p1, "doc"    # I

    .prologue
    const/4 v1, -0x1

    .line 888
    sget-boolean v2, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSlot:I

    if-ne v2, v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 889
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->order:[I

    aget v0, v2, p1

    .line 890
    .local v0, "docOrd":I
    iget-boolean v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSameReader:Z

    if-eqz v2, :cond_2

    .line 892
    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomOrd:I

    sub-int/2addr v1, v0

    .line 899
    :cond_1
    :goto_0
    return v1

    .line 893
    :cond_2
    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomOrd:I

    if-lt v2, v0, :cond_1

    .line 897
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compareValues(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 844
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/String;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->compareValues(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public compareValues(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "val1"    # Ljava/lang/String;
    .param p2, "val2"    # Ljava/lang/String;

    .prologue
    .line 961
    if-nez p1, :cond_1

    .line 962
    if-nez p2, :cond_0

    .line 963
    const/4 v0, 0x0

    .line 969
    :goto_0
    return v0

    .line 965
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 966
    :cond_1
    if-nez p2, :cond_2

    .line 967
    const/4 v0, 0x1

    goto :goto_0

    .line 969
    :cond_2
    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public copy(II)V
    .locals 3
    .param p1, "slot"    # I
    .param p2, "doc"    # I

    .prologue
    .line 905
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->order:[I

    aget v0, v1, p2

    .line 906
    .local v0, "ord":I
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->ords:[I

    aput v0, v1, p1

    .line 907
    sget-boolean v1, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 908
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->values:[Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->lookup:[Ljava/lang/String;

    aget-object v2, v2, v0

    aput-object v2, v1, p1

    .line 909
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->readerGen:[I

    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->currentReaderGen:I

    aput v2, v1, p1

    .line 910
    return-void
.end method

.method public getBottomSlot()I
    .locals 1

    .prologue
    .line 977
    iget v0, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSlot:I

    return v0
.end method

.method public getField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 981
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->field:Ljava/lang/String;

    return-object v0
.end method

.method public getValues()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 973
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->values:[Ljava/lang/String;

    return-object v0
.end method

.method public setBottom(I)V
    .locals 6
    .param p1, "bottom"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 926
    iput p1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSlot:I

    .line 928
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->values:[Ljava/lang/String;

    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSlot:I

    aget-object v1, v1, v2

    iput-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomValue:Ljava/lang/String;

    .line 929
    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->currentReaderGen:I

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->readerGen:[I

    iget v3, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSlot:I

    aget v2, v2, v3

    if-ne v1, v2, :cond_0

    .line 930
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->ords:[I

    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSlot:I

    aget v1, v1, v2

    iput v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomOrd:I

    .line 931
    iput-boolean v5, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSameReader:Z

    .line 952
    :goto_0
    return-void

    .line 933
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomValue:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 934
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->ords:[I

    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSlot:I

    aput v4, v1, v2

    .line 935
    iput v4, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomOrd:I

    .line 936
    iput-boolean v5, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSameReader:Z

    .line 937
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->readerGen:[I

    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSlot:I

    iget v3, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->currentReaderGen:I

    aput v3, v1, v2

    goto :goto_0

    .line 939
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->lookup:[Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomValue:Ljava/lang/String;

    invoke-static {v1, v2}, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->binarySearch([Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 940
    .local v0, "index":I
    if-gez v0, :cond_2

    .line 941
    neg-int v1, v0

    add-int/lit8 v1, v1, -0x2

    iput v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomOrd:I

    .line 942
    iput-boolean v4, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSameReader:Z

    goto :goto_0

    .line 944
    :cond_2
    iput v0, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomOrd:I

    .line 946
    iput-boolean v5, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSameReader:Z

    .line 947
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->readerGen:[I

    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSlot:I

    iget v3, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->currentReaderGen:I

    aput v3, v1, v2

    .line 948
    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->ords:[I

    iget v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSlot:I

    iget v3, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomOrd:I

    aput v3, v1, v2

    goto :goto_0
.end method

.method public setNextReader(Lorg/apache/lucene/index/IndexReader;I)V
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "docBase"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 914
    sget-object v1, Lorg/apache/lucene/search/FieldCache;->DEFAULT:Lorg/apache/lucene/search/FieldCache;

    iget-object v2, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->field:Ljava/lang/String;

    invoke-interface {v1, p1, v2}, Lorg/apache/lucene/search/FieldCache;->getStringIndex(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/search/FieldCache$StringIndex;

    move-result-object v0

    .line 915
    .local v0, "currentReaderValues":Lorg/apache/lucene/search/FieldCache$StringIndex;
    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->currentReaderGen:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->currentReaderGen:I

    .line 916
    iget-object v1, v0, Lorg/apache/lucene/search/FieldCache$StringIndex;->order:[I

    iput-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->order:[I

    .line 917
    iget-object v1, v0, Lorg/apache/lucene/search/FieldCache$StringIndex;->lookup:[Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->lookup:[Ljava/lang/String;

    .line 918
    sget-boolean v1, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->lookup:[Ljava/lang/String;

    array-length v1, v1

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 919
    :cond_0
    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSlot:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 920
    iget v1, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->bottomSlot:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->setBottom(I)V

    .line 922
    :cond_1
    return-void
.end method

.method public bridge synthetic value(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 844
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->value(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public value(I)Ljava/lang/String;
    .locals 1
    .param p1, "slot"    # I

    .prologue
    .line 956
    iget-object v0, p0, Lorg/apache/lucene/search/FieldComparator$StringOrdValComparator;->values:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method
