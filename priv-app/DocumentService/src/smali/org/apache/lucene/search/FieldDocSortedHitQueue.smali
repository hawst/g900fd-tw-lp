.class Lorg/apache/lucene/search/FieldDocSortedHitQueue;
.super Lorg/apache/lucene/util/PriorityQueue;
.source "FieldDocSortedHitQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/PriorityQueue",
        "<",
        "Lorg/apache/lucene/search/FieldDoc;",
        ">;"
    }
.end annotation


# instance fields
.field volatile collators:[Ljava/text/Collator;

.field volatile comparators:[Lorg/apache/lucene/search/FieldComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/search/FieldComparator",
            "<*>;"
        }
    .end annotation
.end field

.field volatile fields:[Lorg/apache/lucene/search/SortField;


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Lorg/apache/lucene/util/PriorityQueue;-><init>()V

    .line 35
    iput-object v0, p0, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->fields:[Lorg/apache/lucene/search/SortField;

    .line 39
    iput-object v0, p0, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->collators:[Ljava/text/Collator;

    .line 41
    iput-object v0, p0, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    .line 50
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->initialize(I)V

    .line 51
    return-void
.end method

.method private hasCollators([Lorg/apache/lucene/search/SortField;)[Ljava/text/Collator;
    .locals 4
    .param p1, "fields"    # [Lorg/apache/lucene/search/SortField;

    .prologue
    .line 85
    if-nez p1, :cond_1

    const/4 v2, 0x0

    .line 92
    :cond_0
    return-object v2

    .line 86
    :cond_1
    array-length v3, p1

    new-array v2, v3, [Ljava/text/Collator;

    .line 87
    .local v2, "ret":[Ljava/text/Collator;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 88
    aget-object v3, p1, v0

    invoke-virtual {v3}, Lorg/apache/lucene/search/SortField;->getLocale()Ljava/util/Locale;

    move-result-object v1

    .line 89
    .local v1, "locale":Ljava/util/Locale;
    if-eqz v1, :cond_2

    .line 90
    invoke-static {v1}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v3

    aput-object v3, v2, v0

    .line 87
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method getFields()[Lorg/apache/lucene/search/SortField;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->fields:[Lorg/apache/lucene/search/SortField;

    return-object v0
.end method

.method protected bridge synthetic lessThan(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 33
    check-cast p1, Lorg/apache/lucene/search/FieldDoc;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/apache/lucene/search/FieldDoc;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->lessThan(Lorg/apache/lucene/search/FieldDoc;Lorg/apache/lucene/search/FieldDoc;)Z

    move-result v0

    return v0
.end method

.method protected final lessThan(Lorg/apache/lucene/search/FieldDoc;Lorg/apache/lucene/search/FieldDoc;)Z
    .locals 11
    .param p1, "docA"    # Lorg/apache/lucene/search/FieldDoc;
    .param p2, "docB"    # Lorg/apache/lucene/search/FieldDoc;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 105
    iget-object v9, p0, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->fields:[Lorg/apache/lucene/search/SortField;

    array-length v3, v9

    .line 106
    .local v3, "n":I
    const/4 v0, 0x0

    .line 107
    .local v0, "c":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_6

    if-nez v0, :cond_6

    .line 108
    iget-object v9, p0, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->fields:[Lorg/apache/lucene/search/SortField;

    aget-object v9, v9, v2

    invoke-virtual {v9}, Lorg/apache/lucene/search/SortField;->getType()I

    move-result v6

    .line 109
    .local v6, "type":I
    const/4 v9, 0x3

    if-ne v6, v9, :cond_5

    .line 110
    iget-object v9, p1, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    aget-object v4, v9, v2

    check-cast v4, Ljava/lang/String;

    .line 111
    .local v4, "s1":Ljava/lang/String;
    iget-object v9, p2, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    aget-object v5, v9, v2

    check-cast v5, Ljava/lang/String;

    .line 115
    .local v5, "s2":Ljava/lang/String;
    if-nez v4, :cond_2

    .line 116
    if-nez v5, :cond_1

    move v0, v7

    .line 129
    .end local v4    # "s1":Ljava/lang/String;
    .end local v5    # "s2":Ljava/lang/String;
    :goto_1
    iget-object v9, p0, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->fields:[Lorg/apache/lucene/search/SortField;

    aget-object v9, v9, v2

    invoke-virtual {v9}, Lorg/apache/lucene/search/SortField;->getReverse()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 130
    neg-int v0, v0

    .line 107
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 116
    .restart local v4    # "s1":Ljava/lang/String;
    .restart local v5    # "s2":Ljava/lang/String;
    :cond_1
    const/4 v0, -0x1

    goto :goto_1

    .line 117
    :cond_2
    if-nez v5, :cond_3

    .line 118
    const/4 v0, 0x1

    goto :goto_1

    .line 119
    :cond_3
    iget-object v9, p0, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->fields:[Lorg/apache/lucene/search/SortField;

    aget-object v9, v9, v2

    invoke-virtual {v9}, Lorg/apache/lucene/search/SortField;->getLocale()Ljava/util/Locale;

    move-result-object v9

    if-nez v9, :cond_4

    .line 120
    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 122
    :cond_4
    iget-object v9, p0, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->collators:[Ljava/text/Collator;

    aget-object v9, v9, v2

    invoke-virtual {v9, v4, v5}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 125
    .end local v4    # "s1":Ljava/lang/String;
    .end local v5    # "s2":Ljava/lang/String;
    :cond_5
    iget-object v9, p0, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v1, v9, v2

    .line 126
    .local v1, "comp":Lorg/apache/lucene/search/FieldComparator;, "Lorg/apache/lucene/search/FieldComparator<Ljava/lang/Object;>;"
    iget-object v9, p1, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    aget-object v9, v9, v2

    iget-object v10, p2, Lorg/apache/lucene/search/FieldDoc;->fields:[Ljava/lang/Object;

    aget-object v10, v10, v2

    invoke-virtual {v1, v9, v10}, Lorg/apache/lucene/search/FieldComparator;->compareValues(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_1

    .line 135
    .end local v1    # "comp":Lorg/apache/lucene/search/FieldComparator;, "Lorg/apache/lucene/search/FieldComparator<Ljava/lang/Object;>;"
    .end local v6    # "type":I
    :cond_6
    if-nez v0, :cond_8

    .line 136
    iget v9, p1, Lorg/apache/lucene/search/FieldDoc;->doc:I

    iget v10, p2, Lorg/apache/lucene/search/FieldDoc;->doc:I

    if-le v9, v10, :cond_7

    move v7, v8

    .line 138
    :cond_7
    :goto_2
    return v7

    :cond_8
    if-lez v0, :cond_9

    :goto_3
    move v7, v8

    goto :goto_2

    :cond_9
    move v8, v7

    goto :goto_3
.end method

.method setFields([Lorg/apache/lucene/search/SortField;)V
    .locals 4
    .param p1, "fields"    # [Lorg/apache/lucene/search/SortField;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    iput-object p1, p0, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->fields:[Lorg/apache/lucene/search/SortField;

    .line 65
    invoke-direct {p0, p1}, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->hasCollators([Lorg/apache/lucene/search/SortField;)[Ljava/text/Collator;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->collators:[Ljava/text/Collator;

    .line 66
    array-length v1, p1

    new-array v1, v1, [Lorg/apache/lucene/search/FieldComparator;

    iput-object v1, p0, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    .line 67
    const/4 v0, 0x0

    .local v0, "fieldIDX":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 68
    iget-object v1, p0, Lorg/apache/lucene/search/FieldDocSortedHitQueue;->comparators:[Lorg/apache/lucene/search/FieldComparator;

    aget-object v2, p1, v0

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0}, Lorg/apache/lucene/search/SortField;->getComparator(II)Lorg/apache/lucene/search/FieldComparator;

    move-result-object v2

    aput-object v2, v1, v0

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_0
    return-void
.end method
