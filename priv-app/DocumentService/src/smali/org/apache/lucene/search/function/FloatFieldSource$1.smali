.class Lorg/apache/lucene/search/function/FloatFieldSource$1;
.super Lorg/apache/lucene/search/function/DocValues;
.source "FloatFieldSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/search/function/FloatFieldSource;->getCachedFieldValues(Lorg/apache/lucene/search/FieldCache;Ljava/lang/String;Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/function/DocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/search/function/FloatFieldSource;

.field final synthetic val$arr:[F


# direct methods
.method constructor <init>(Lorg/apache/lucene/search/function/FloatFieldSource;[F)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lorg/apache/lucene/search/function/FloatFieldSource$1;->this$0:Lorg/apache/lucene/search/function/FloatFieldSource;

    iput-object p2, p0, Lorg/apache/lucene/search/function/FloatFieldSource$1;->val$arr:[F

    invoke-direct {p0}, Lorg/apache/lucene/search/function/DocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public floatVal(I)F
    .locals 1
    .param p1, "doc"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/search/function/FloatFieldSource$1;->val$arr:[F

    aget v0, v0, p1

    return v0
.end method

.method getInnerArray()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/lucene/search/function/FloatFieldSource$1;->val$arr:[F

    return-object v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 2
    .param p1, "doc"    # I

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/lucene/search/function/FloatFieldSource$1;->this$0:Lorg/apache/lucene/search/function/FloatFieldSource;

    invoke-virtual {v1}, Lorg/apache/lucene/search/function/FloatFieldSource;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/function/FloatFieldSource$1;->val$arr:[F

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
