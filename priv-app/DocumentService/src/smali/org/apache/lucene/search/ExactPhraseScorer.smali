.class final Lorg/apache/lucene/search/ExactPhraseScorer;
.super Lorg/apache/lucene/search/Scorer;
.source "ExactPhraseScorer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final CHUNK:I = 0x1000

.field private static final SCORE_CACHE_SIZE:I = 0x20


# instance fields
.field private final chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

.field private final counts:[I

.field private docID:I

.field private final endMinus1:I

.field private freq:I

.field private gen:I

.field private final gens:[I

.field noDocs:Z

.field private final norms:[B

.field private final scoreCache:[F

.field private final value:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lorg/apache/lucene/search/ExactPhraseScorer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/search/ExactPhraseScorer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/search/Weight;[Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;Lorg/apache/lucene/search/Similarity;[B)V
    .locals 9
    .param p1, "weight"    # Lorg/apache/lucene/search/Weight;
    .param p2, "postings"    # [Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
    .param p3, "similarity"    # Lorg/apache/lucene/search/Similarity;
    .param p4, "norms"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x1000

    const/16 v8, 0x20

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 65
    invoke-direct {p0, p3, p1}, Lorg/apache/lucene/search/Scorer;-><init>(Lorg/apache/lucene/search/Similarity;Lorg/apache/lucene/search/Weight;)V

    .line 30
    new-array v4, v8, [F

    iput-object v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->scoreCache:[F

    .line 37
    new-array v4, v5, [I

    iput-object v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->counts:[I

    .line 38
    new-array v4, v5, [I

    iput-object v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gens:[I

    .line 60
    const/4 v4, -0x1

    iput v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 66
    iput-object p4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->norms:[B

    .line 67
    invoke-virtual {p1}, Lorg/apache/lucene/search/Weight;->getValue()F

    move-result v4

    iput v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->value:F

    .line 69
    array-length v4, p2

    new-array v4, v4, [Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    iput-object v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    .line 71
    array-length v4, p2

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->endMinus1:I

    .line 73
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p2

    if-ge v0, v4, :cond_3

    .line 81
    aget-object v4, p2, v0

    iget v4, v4, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    aget-object v5, p2, v3

    iget v5, v5, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->docFreq:I

    mul-int/lit8 v5, v5, 0x5

    if-le v4, v5, :cond_1

    move v1, v2

    .line 82
    .local v1, "useAdvance":Z
    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    new-instance v5, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v6, p2, v0

    iget-object v6, v6, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->postings:Lorg/apache/lucene/index/TermPositions;

    aget-object v7, p2, v0

    iget v7, v7, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->position:I

    neg-int v7, v7

    invoke-direct {v5, v6, v7, v1}, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;-><init>(Lorg/apache/lucene/index/TermPositions;IZ)V

    aput-object v5, v4, v0

    .line 83
    if-lez v0, :cond_2

    aget-object v4, p2, v0

    iget-object v4, v4, Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;->postings:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v4}, Lorg/apache/lucene/index/TermPositions;->next()Z

    move-result v4

    if-nez v4, :cond_2

    .line 84
    iput-boolean v2, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->noDocs:Z

    .line 92
    .end local v1    # "useAdvance":Z
    :cond_0
    return-void

    :cond_1
    move v1, v3

    .line 81
    goto :goto_1

    .line 73
    .restart local v1    # "useAdvance":Z
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89
    .end local v1    # "useAdvance":Z
    :cond_3
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v8, :cond_0

    .line 90
    iget-object v2, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->scoreCache:[F

    invoke-virtual {p0}, Lorg/apache/lucene/search/ExactPhraseScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v3

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Lorg/apache/lucene/search/Similarity;->tf(F)F

    move-result v3

    iget v4, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->value:F

    mul-float/2addr v3, v4

    aput v3, v2, v0

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private phraseFreq()I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 239
    iput v10, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    .line 242
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    array-length v8, v8

    if-ge v5, v8, :cond_0

    .line 243
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v8, v5

    .line 244
    .local v3, "cs":Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;
    iget-object v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v8}, Lorg/apache/lucene/index/TermPositions;->freq()I

    move-result v8

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posLimit:I

    .line 245
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->offset:I

    iget-object v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v9}, Lorg/apache/lucene/index/TermPositions;->nextPosition()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    .line 246
    iput v11, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    .line 247
    const/4 v8, -0x1

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->lastPos:I

    .line 242
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 250
    .end local v3    # "cs":Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;
    :cond_0
    const/4 v2, 0x0

    .line 251
    .local v2, "chunkStart":I
    const/16 v1, 0x1000

    .line 254
    .local v1, "chunkEnd":I
    const/4 v4, 0x0

    .line 259
    .local v4, "end":Z
    :goto_1
    if-nez v4, :cond_f

    .line 261
    iget v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    .line 263
    iget v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    if-nez v8, :cond_1

    .line 265
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gens:[I

    invoke-static {v8, v10}, Ljava/util/Arrays;->fill([II)V

    .line 266
    iget v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    .line 271
    :cond_1
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v8, v10

    .line 272
    .restart local v3    # "cs":Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;
    :goto_2
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    if-ge v8, v1, :cond_4

    .line 273
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    iget v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->lastPos:I

    if-le v8, v9, :cond_3

    .line 274
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->lastPos:I

    .line 275
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    sub-int v6, v8, v2

    .line 276
    .local v6, "posIndex":I
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->counts:[I

    aput v11, v8, v6

    .line 277
    sget-boolean v8, Lorg/apache/lucene/search/ExactPhraseScorer;->$assertionsDisabled:Z

    if-nez v8, :cond_2

    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gens:[I

    aget v8, v8, v6

    iget v9, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    if-ne v8, v9, :cond_2

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 278
    :cond_2
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gens:[I

    iget v9, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    aput v9, v8, v6

    .line 281
    .end local v6    # "posIndex":I
    :cond_3
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    iget v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posLimit:I

    if-ne v8, v9, :cond_8

    .line 282
    const/4 v4, 0x1

    .line 291
    :cond_4
    const/4 v0, 0x1

    .line 292
    .local v0, "any":Z
    const/4 v7, 0x1

    .local v7, "t":I
    :goto_3
    iget v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->endMinus1:I

    if-ge v7, v8, :cond_7

    .line 293
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v8, v7

    .line 294
    const/4 v0, 0x0

    .line 295
    :goto_4
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    if-ge v8, v1, :cond_6

    .line 296
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    iget v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->lastPos:I

    if-le v8, v9, :cond_5

    .line 297
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->lastPos:I

    .line 298
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    sub-int v6, v8, v2

    .line 299
    .restart local v6    # "posIndex":I
    if-ltz v6, :cond_5

    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gens:[I

    aget v8, v8, v6

    iget v9, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    if-ne v8, v9, :cond_5

    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->counts:[I

    aget v8, v8, v6

    if-ne v8, v7, :cond_5

    .line 301
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->counts:[I

    aget v9, v8, v6

    add-int/lit8 v9, v9, 0x1

    aput v9, v8, v6

    .line 302
    const/4 v0, 0x1

    .line 306
    .end local v6    # "posIndex":I
    :cond_5
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    iget v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posLimit:I

    if-ne v8, v9, :cond_9

    .line 307
    const/4 v4, 0x1

    .line 314
    :cond_6
    if-nez v0, :cond_a

    .line 319
    :cond_7
    if-nez v0, :cond_b

    .line 321
    add-int/lit16 v2, v2, 0x1000

    .line 322
    add-int/lit16 v1, v1, 0x1000

    .line 323
    goto/16 :goto_1

    .line 285
    .end local v0    # "any":Z
    .end local v7    # "t":I
    :cond_8
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    .line 286
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->offset:I

    iget-object v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v9}, Lorg/apache/lucene/index/TermPositions;->nextPosition()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    goto/16 :goto_2

    .line 310
    .restart local v0    # "any":Z
    .restart local v7    # "t":I
    :cond_9
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    .line 311
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->offset:I

    iget-object v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v9}, Lorg/apache/lucene/index/TermPositions;->nextPosition()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    goto :goto_4

    .line 292
    :cond_a
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 329
    :cond_b
    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    iget v9, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->endMinus1:I

    aget-object v3, v8, v9

    .line 330
    :goto_5
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    if-ge v8, v1, :cond_d

    .line 331
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    iget v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->lastPos:I

    if-le v8, v9, :cond_c

    .line 332
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->lastPos:I

    .line 333
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    sub-int v6, v8, v2

    .line 334
    .restart local v6    # "posIndex":I
    if-ltz v6, :cond_c

    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gens:[I

    aget v8, v8, v6

    iget v9, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->gen:I

    if-ne v8, v9, :cond_c

    iget-object v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->counts:[I

    aget v8, v8, v6

    iget v9, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->endMinus1:I

    if-ne v8, v9, :cond_c

    .line 335
    iget v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    .line 339
    .end local v6    # "posIndex":I
    :cond_c
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    iget v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posLimit:I

    if-ne v8, v9, :cond_e

    .line 340
    const/4 v4, 0x1

    .line 348
    :cond_d
    add-int/lit16 v2, v2, 0x1000

    .line 349
    add-int/lit16 v1, v1, 0x1000

    .line 350
    goto/16 :goto_1

    .line 343
    :cond_e
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posUpto:I

    .line 344
    iget v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->offset:I

    iget-object v9, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v9}, Lorg/apache/lucene/index/TermPositions;->nextPosition()I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->pos:I

    goto :goto_5

    .line 352
    .end local v0    # "any":Z
    .end local v3    # "cs":Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;
    .end local v7    # "t":I
    :cond_f
    iget v8, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    return v8
.end method


# virtual methods
.method public advance(I)I
    .locals 6
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v5, 0x7fffffff

    const/4 v4, 0x0

    .line 166
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v3, p1}, Lorg/apache/lucene/index/TermPositions;->skipTo(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 167
    iput v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 168
    iget v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 204
    :goto_0
    return v3

    .line 170
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v3}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v0

    .line 175
    .local v0, "doc":I
    :goto_1
    const/4 v2, 0x1

    .line 176
    .local v2, "i":I
    :goto_2
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    array-length v3, v3

    if-ge v2, v3, :cond_3

    .line 177
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v3, v2

    iget-object v3, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v3}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v1

    .line 178
    .local v1, "doc2":I
    if-ge v1, v0, :cond_2

    .line 179
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v3, v2

    iget-object v3, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v3, v0}, Lorg/apache/lucene/index/TermPositions;->skipTo(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 180
    iput v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 181
    iget v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    goto :goto_0

    .line 183
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v3, v2

    iget-object v3, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v3}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v1

    .line 186
    :cond_2
    if-le v1, v0, :cond_4

    .line 192
    .end local v1    # "doc2":I
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    array-length v3, v3

    if-ne v2, v3, :cond_5

    .line 195
    iput v0, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 196
    invoke-direct {p0}, Lorg/apache/lucene/search/ExactPhraseScorer;->phraseFreq()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    .line 197
    iget v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    if-eqz v3, :cond_5

    .line 198
    iget v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    goto :goto_0

    .line 189
    .restart local v1    # "doc2":I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 190
    goto :goto_2

    .line 202
    .end local v1    # "doc2":I
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v3}, Lorg/apache/lucene/index/TermPositions;->next()Z

    move-result v3

    if-nez v3, :cond_6

    .line 203
    iput v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 204
    iget v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    goto :goto_0

    .line 206
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v3}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v0

    .line 208
    goto :goto_1
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    return v0
.end method

.method public freq()F
    .locals 1

    .prologue
    .line 218
    iget v0, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    int-to-float v0, v0

    return v0
.end method

.method public nextDoc()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const v6, 0x7fffffff

    .line 99
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v5, v5, v7

    iget-object v5, v5, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v5}, Lorg/apache/lucene/index/TermPositions;->next()Z

    move-result v5

    if-nez v5, :cond_1

    .line 100
    iput v6, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 101
    iget v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 156
    :goto_0
    return v5

    .line 104
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v5, v5, v7

    iget-object v5, v5, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v5}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v1

    .line 107
    .local v1, "doc":I
    const/4 v3, 0x1

    .line 108
    .local v3, "i":I
    :goto_1
    iget-object v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    array-length v5, v5

    if-ge v3, v5, :cond_4

    .line 109
    iget-object v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    aget-object v0, v5, v3

    .line 110
    .local v0, "cs":Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;
    iget-object v5, v0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v5}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v2

    .line 111
    .local v2, "doc2":I
    iget-boolean v5, v0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->useAdvance:Z

    if-eqz v5, :cond_5

    .line 112
    if-ge v2, v1, :cond_3

    .line 113
    iget-object v5, v0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v5, v1}, Lorg/apache/lucene/index/TermPositions;->skipTo(I)Z

    move-result v5

    if-nez v5, :cond_2

    .line 114
    iput v6, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 115
    iget v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    goto :goto_0

    .line 117
    :cond_2
    iget-object v5, v0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v5}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v2

    .line 143
    :cond_3
    :goto_2
    if-le v2, v1, :cond_9

    .line 149
    .end local v0    # "cs":Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;
    .end local v2    # "doc2":I
    :cond_4
    iget-object v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->chunkStates:[Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;

    array-length v5, v5

    if-ne v3, v5, :cond_0

    .line 152
    iput v1, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 154
    invoke-direct {p0}, Lorg/apache/lucene/search/ExactPhraseScorer;->phraseFreq()I

    move-result v5

    iput v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    .line 155
    iget v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    if-eqz v5, :cond_0

    .line 156
    iget v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    goto :goto_0

    .line 121
    .restart local v0    # "cs":Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;
    .restart local v2    # "doc2":I
    :cond_5
    const/4 v4, 0x0

    .line 122
    .local v4, "iter":I
    :goto_3
    if-ge v2, v1, :cond_3

    .line 125
    add-int/lit8 v4, v4, 0x1

    const/16 v5, 0x32

    if-ne v4, v5, :cond_7

    .line 126
    iget-object v5, v0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v5, v1}, Lorg/apache/lucene/index/TermPositions;->skipTo(I)Z

    move-result v5

    if-nez v5, :cond_6

    .line 127
    iput v6, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 128
    iget v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    goto :goto_0

    .line 130
    :cond_6
    iget-object v5, v0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v5}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v2

    .line 132
    goto :goto_2

    .line 134
    :cond_7
    iget-object v5, v0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v5}, Lorg/apache/lucene/index/TermPositions;->next()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 135
    iget-object v5, v0, Lorg/apache/lucene/search/ExactPhraseScorer$ChunkState;->posEnum:Lorg/apache/lucene/index/TermPositions;

    invoke-interface {v5}, Lorg/apache/lucene/index/TermPositions;->doc()I

    move-result v2

    goto :goto_3

    .line 137
    :cond_8
    iput v6, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    .line 138
    iget v5, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    goto :goto_0

    .line 146
    .end local v4    # "iter":I
    :cond_9
    add-int/lit8 v3, v3, 0x1

    .line 147
    goto :goto_1
.end method

.method public score()F
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 229
    iget v1, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    const/16 v2, 0x20

    if-ge v1, v2, :cond_0

    .line 230
    iget-object v1, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->scoreCache:[F

    iget v2, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    aget v0, v1, v2

    .line 234
    .local v0, "raw":F
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->norms:[B

    if-nez v1, :cond_1

    .end local v0    # "raw":F
    :goto_1
    return v0

    .line 232
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/search/ExactPhraseScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->freq:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Similarity;->tf(F)F

    move-result v1

    iget v2, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->value:F

    mul-float v0, v1, v2

    .restart local v0    # "raw":F
    goto :goto_0

    .line 234
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/search/ExactPhraseScorer;->getSimilarity()Lorg/apache/lucene/search/Similarity;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->norms:[B

    iget v3, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->docID:I

    aget-byte v2, v2, v3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Similarity;->decodeNormValue(B)F

    move-result v1

    mul-float/2addr v0, v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ExactPhraseScorer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/ExactPhraseScorer;->weight:Lorg/apache/lucene/search/Weight;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
