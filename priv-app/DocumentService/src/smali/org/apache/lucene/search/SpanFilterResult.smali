.class public Lorg/apache/lucene/search/SpanFilterResult;
.super Ljava/lang/Object;
.source "SpanFilterResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/SpanFilterResult$StartEnd;,
        Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;
    }
.end annotation


# instance fields
.field private docIdSet:Lorg/apache/lucene/search/DocIdSet;

.field private positions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/DocIdSet;Ljava/util/List;)V
    .locals 0
    .param p1, "docIdSet"    # Lorg/apache/lucene/search/DocIdSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/search/DocIdSet;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p2, "positions":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lorg/apache/lucene/search/SpanFilterResult;->docIdSet:Lorg/apache/lucene/search/DocIdSet;

    .line 40
    iput-object p2, p0, Lorg/apache/lucene/search/SpanFilterResult;->positions:Ljava/util/List;

    .line 41
    return-void
.end method


# virtual methods
.method public getDocIdSet()Lorg/apache/lucene/search/DocIdSet;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/lucene/search/SpanFilterResult;->docIdSet:Lorg/apache/lucene/search/DocIdSet;

    return-object v0
.end method

.method public getPositions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/search/SpanFilterResult$PositionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/search/SpanFilterResult;->positions:Ljava/util/List;

    return-object v0
.end method
