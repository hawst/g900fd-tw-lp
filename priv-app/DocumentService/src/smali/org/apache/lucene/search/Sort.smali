.class public Lorg/apache/lucene/search/Sort;
.super Ljava/lang/Object;
.source "Sort.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final INDEXORDER:Lorg/apache/lucene/search/Sort;

.field public static final RELEVANCE:Lorg/apache/lucene/search/Sort;


# instance fields
.field fields:[Lorg/apache/lucene/search/SortField;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 109
    new-instance v0, Lorg/apache/lucene/search/Sort;

    invoke-direct {v0}, Lorg/apache/lucene/search/Sort;-><init>()V

    sput-object v0, Lorg/apache/lucene/search/Sort;->RELEVANCE:Lorg/apache/lucene/search/Sort;

    .line 112
    new-instance v0, Lorg/apache/lucene/search/Sort;

    sget-object v1, Lorg/apache/lucene/search/SortField;->FIELD_DOC:Lorg/apache/lucene/search/SortField;

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/Sort;-><init>(Lorg/apache/lucene/search/SortField;)V

    sput-object v0, Lorg/apache/lucene/search/Sort;->INDEXORDER:Lorg/apache/lucene/search/Sort;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lorg/apache/lucene/search/SortField;->FIELD_SCORE:Lorg/apache/lucene/search/SortField;

    invoke-direct {p0, v0}, Lorg/apache/lucene/search/Sort;-><init>(Lorg/apache/lucene/search/SortField;)V

    .line 124
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/search/SortField;)V
    .locals 0
    .param p1, "field"    # Lorg/apache/lucene/search/SortField;

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/Sort;->setSort(Lorg/apache/lucene/search/SortField;)V

    .line 129
    return-void
.end method

.method public varargs constructor <init>([Lorg/apache/lucene/search/SortField;)V
    .locals 0
    .param p1, "fields"    # [Lorg/apache/lucene/search/SortField;

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    invoke-virtual {p0, p1}, Lorg/apache/lucene/search/Sort;->setSort([Lorg/apache/lucene/search/SortField;)V

    .line 134
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 170
    if-ne p0, p1, :cond_0

    const/4 v1, 0x1

    .line 173
    :goto_0
    return v1

    .line 171
    :cond_0
    instance-of v1, p1, Lorg/apache/lucene/search/Sort;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 172
    check-cast v0, Lorg/apache/lucene/search/Sort;

    .line 173
    .local v0, "other":Lorg/apache/lucene/search/Sort;
    iget-object v1, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    iget-object v2, v0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getSort()[Lorg/apache/lucene/search/SortField;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 179
    const v0, 0x45aaf665

    iget-object v1, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public setSort(Lorg/apache/lucene/search/SortField;)V
    .locals 2
    .param p1, "field"    # Lorg/apache/lucene/search/SortField;

    .prologue
    .line 138
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/lucene/search/SortField;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    .line 139
    return-void
.end method

.method public varargs setSort([Lorg/apache/lucene/search/SortField;)V
    .locals 0
    .param p1, "fields"    # [Lorg/apache/lucene/search/SortField;

    .prologue
    .line 143
    iput-object p1, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    .line 144
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    .local v0, "buffer":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 159
    iget-object v2, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/apache/lucene/search/SortField;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    add-int/lit8 v2, v1, 0x1

    iget-object v3, p0, Lorg/apache/lucene/search/Sort;->fields:[Lorg/apache/lucene/search/SortField;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 161
    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 158
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 164
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
