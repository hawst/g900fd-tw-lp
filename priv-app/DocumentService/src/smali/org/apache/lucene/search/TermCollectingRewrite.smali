.class abstract Lorg/apache/lucene/search/TermCollectingRewrite;
.super Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;
.source "TermCollectingRewrite.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q:",
        "Lorg/apache/lucene/search/Query;",
        ">",
        "Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    .local p0, "this":Lorg/apache/lucene/search/TermCollectingRewrite;, "Lorg/apache/lucene/search/TermCollectingRewrite<TQ;>;"
    invoke-direct {p0}, Lorg/apache/lucene/search/MultiTermQuery$RewriteMethod;-><init>()V

    .line 46
    return-void
.end method


# virtual methods
.method protected abstract addClause(Lorg/apache/lucene/search/Query;Lorg/apache/lucene/index/Term;F)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TQ;",
            "Lorg/apache/lucene/index/Term;",
            "F)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected final collectTerms(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;)V
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p2, "query"    # Lorg/apache/lucene/search/MultiTermQuery;
    .param p3, "collector"    # Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lorg/apache/lucene/search/TermCollectingRewrite;, "Lorg/apache/lucene/search/TermCollectingRewrite<TQ;>;"
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/search/TermCollectingRewrite;->getTermsEnum(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/search/MultiTermQuery;)Lorg/apache/lucene/search/FilteredTermEnum;

    move-result-object v0

    .line 37
    .local v0, "enumerator":Lorg/apache/lucene/search/FilteredTermEnum;
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lorg/apache/lucene/search/FilteredTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v1

    .line 38
    .local v1, "t":Lorg/apache/lucene/index/Term;
    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lorg/apache/lucene/search/FilteredTermEnum;->difference()F

    move-result v2

    invoke-interface {p3, v1, v2}, Lorg/apache/lucene/search/TermCollectingRewrite$TermCollector;->collect(Lorg/apache/lucene/index/Term;F)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 42
    :cond_1
    :goto_0
    invoke-virtual {v0}, Lorg/apache/lucene/search/FilteredTermEnum;->close()V

    .line 44
    return-void

    .line 40
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Lorg/apache/lucene/search/FilteredTermEnum;->next()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 42
    .end local v1    # "t":Lorg/apache/lucene/index/Term;
    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/FilteredTermEnum;->close()V

    throw v2
.end method

.method protected abstract getTopLevelQuery()Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TQ;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
