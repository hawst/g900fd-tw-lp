.class public final enum Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;
.super Ljava/lang/Enum;
.source "CachingWrapperFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/search/CachingWrapperFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DeletesMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

.field public static final enum DYNAMIC:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

.field public static final enum IGNORE:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

.field public static final enum RECACHE:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62
    new-instance v0, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    const-string/jumbo v1, "IGNORE"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->IGNORE:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    new-instance v0, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    const-string/jumbo v1, "RECACHE"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->RECACHE:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    new-instance v0, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    const-string/jumbo v1, "DYNAMIC"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->DYNAMIC:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    sget-object v1, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->IGNORE:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->RECACHE:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->DYNAMIC:Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    aput-object v1, v0, v4

    sput-object v0, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->$VALUES:[Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 62
    const-class v0, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    return-object v0
.end method

.method public static final values()[Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->$VALUES:[Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    invoke-virtual {v0}, [Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/search/CachingWrapperFilter$DeletesMode;

    return-object v0
.end method
