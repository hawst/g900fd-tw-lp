.class public Lorg/apache/lucene/search/PhraseQuery;
.super Lorg/apache/lucene/search/Query;
.source "PhraseQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;,
        Lorg/apache/lucene/search/PhraseQuery$PostingsAndFreq;
    }
.end annotation


# instance fields
.field private field:Ljava/lang/String;

.field private maxPosition:I

.field private positions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private slop:I

.field private terms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Lorg/apache/lucene/search/Query;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    .line 41
    iput v1, p0, Lorg/apache/lucene/search/PhraseQuery;->maxPosition:I

    .line 42
    iput v1, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    .line 45
    return-void
.end method

.method static synthetic access$000(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/PhraseQuery;

    .prologue
    .line 37
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lorg/apache/lucene/search/PhraseQuery;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/PhraseQuery;

    .prologue
    .line 37
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lorg/apache/lucene/search/PhraseQuery;)I
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/PhraseQuery;

    .prologue
    .line 37
    iget v0, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    return v0
.end method

.method static synthetic access$300(Lorg/apache/lucene/search/PhraseQuery;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/apache/lucene/search/PhraseQuery;

    .prologue
    .line 37
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public add(Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 70
    const/4 v0, 0x0

    .line 71
    .local v0, "position":I
    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 72
    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 74
    :cond_0
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/search/PhraseQuery;->add(Lorg/apache/lucene/index/Term;I)V

    .line 75
    return-void
.end method

.method public add(Lorg/apache/lucene/index/Term;I)V
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "position"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 88
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;

    .line 92
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    iget v0, p0, Lorg/apache/lucene/search/PhraseQuery;->maxPosition:I

    if-le p2, v0, :cond_1

    iput p2, p0, Lorg/apache/lucene/search/PhraseQuery;->maxPosition:I

    .line 95
    :cond_1
    return-void

    .line 89
    :cond_2
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;

    if-eq v0, v1, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "All phrase terms must be in the same field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;
    .locals 4
    .param p1, "searcher"    # Lorg/apache/lucene/search/Searcher;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 355
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 356
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/Term;

    .line 357
    .local v0, "term":Lorg/apache/lucene/index/Term;
    new-instance v1, Lorg/apache/lucene/search/TermQuery;

    invoke-direct {v1, v0}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 358
    .local v1, "termQuery":Lorg/apache/lucene/search/Query;
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/search/Query;->setBoost(F)V

    .line 359
    invoke-virtual {v1, p1}, Lorg/apache/lucene/search/Query;->createWeight(Lorg/apache/lucene/search/Searcher;)Lorg/apache/lucene/search/Weight;

    move-result-object v2

    .line 361
    .end local v0    # "term":Lorg/apache/lucene/index/Term;
    .end local v1    # "termQuery":Lorg/apache/lucene/search/Query;
    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;

    invoke-direct {v2, p0, p1}, Lorg/apache/lucene/search/PhraseQuery$PhraseWeight;-><init>(Lorg/apache/lucene/search/PhraseQuery;Lorg/apache/lucene/search/Searcher;)V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 419
    instance-of v2, p1, Lorg/apache/lucene/search/PhraseQuery;

    if-nez v2, :cond_1

    .line 422
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 421
    check-cast v0, Lorg/apache/lucene/search/PhraseQuery;

    .line 422
    .local v0, "other":Lorg/apache/lucene/search/PhraseQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    iget v3, v0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    iget-object v3, v0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    iget-object v3, v0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public extractTerms(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 369
    .local p1, "queryTerms":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/Term;>;"
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 370
    return-void
.end method

.method public getPositions()[I
    .locals 3

    .prologue
    .line 106
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v1, v2, [I

    .line 107
    .local v1, "result":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 108
    iget-object v2, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v1, v0

    .line 107
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 109
    :cond_0
    return-object v1
.end method

.method public getSlop()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    return v0
.end method

.method public getTerms()[Lorg/apache/lucene/index/Term;
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-array v1, v1, [Lorg/apache/lucene/index/Term;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/lucene/index/Term;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 431
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    iget v1, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;
    .locals 3
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 115
    new-instance v0, Lorg/apache/lucene/search/TermQuery;

    iget-object v1, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/Term;

    invoke-direct {v0, v1}, Lorg/apache/lucene/search/TermQuery;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 116
    .local v0, "tq":Lorg/apache/lucene/search/TermQuery;
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/search/TermQuery;->setBoost(F)V

    .line 119
    .end local v0    # "tq":Lorg/apache/lucene/search/TermQuery;
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/lucene/search/Query;->rewrite(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/search/Query;

    move-result-object v0

    goto :goto_0
.end method

.method public setSlop(I)V
    .locals 0
    .param p1, "s"    # I

    .prologue
    .line 61
    iput p1, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    return-void
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "f"    # Ljava/lang/String;

    .prologue
    .line 375
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 376
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 377
    iget-object v5, p0, Lorg/apache/lucene/search/PhraseQuery;->field:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    const-string/jumbo v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    :cond_0
    const-string/jumbo v5, "\""

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 382
    iget v5, p0, Lorg/apache/lucene/search/PhraseQuery;->maxPosition:I

    add-int/lit8 v5, v5, 0x1

    new-array v2, v5, [Ljava/lang/String;

    .line 383
    .local v2, "pieces":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 384
    iget-object v5, p0, Lorg/apache/lucene/search/PhraseQuery;->positions:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 385
    .local v3, "pos":I
    aget-object v4, v2, v3

    .line 386
    .local v4, "s":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 387
    iget-object v5, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/Term;

    invoke-virtual {v5}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v4

    .line 391
    :goto_1
    aput-object v4, v2, v3

    .line 383
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 389
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "|"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v5, p0, Lorg/apache/lucene/search/PhraseQuery;->terms:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/Term;

    invoke-virtual {v5}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 393
    .end local v3    # "pos":I
    .end local v4    # "s":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x0

    :goto_2
    array-length v5, v2

    if-ge v1, v5, :cond_5

    .line 394
    if-lez v1, :cond_3

    .line 395
    const/16 v5, 0x20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 397
    :cond_3
    aget-object v4, v2, v1

    .line 398
    .restart local v4    # "s":Ljava/lang/String;
    if-nez v4, :cond_4

    .line 399
    const/16 v5, 0x3f

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 393
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 401
    :cond_4
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 404
    .end local v4    # "s":Ljava/lang/String;
    :cond_5
    const-string/jumbo v5, "\""

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    iget v5, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    if-eqz v5, :cond_6

    .line 407
    const-string/jumbo v5, "~"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    iget v5, p0, Lorg/apache/lucene/search/PhraseQuery;->slop:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 411
    :cond_6
    invoke-virtual {p0}, Lorg/apache/lucene/search/PhraseQuery;->getBoost()F

    move-result v5

    invoke-static {v5}, Lorg/apache/lucene/util/ToStringUtils;->boost(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 413
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
